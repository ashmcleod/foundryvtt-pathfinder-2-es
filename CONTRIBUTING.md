## Contribuir al proyecto

Puedes ayudar al proyecto y convertirte fácilmente en uno de sus colaboradores.  

También puedes unirte a la comunidad desde la aplicación de discord "Foundry VTT Español", que tiene una sala dedicada al sistema PF2.

Contribuir es cosa de todos. Siempre necesitamos correctores y traductores, e incluso usuarios que nos informen de errores o problemas de visualización.  

En los datos actuales, también tienes un diccionario VO/VE y un diccionario VE/VO. Muestra todo lo que está traducido y lo que aún no lo está. Se genera automáticamente con cada actualización.

Si observa que un archivo que necesita aún no está traducido, pulse sobre él para editarlo. El repositorio del proyecto tiene un editor incorporado, así que todo lo que necesitas hacer es crear una cuenta para contribuir. El archivo se enviará a los colaboradores del sistema, que podrán integrar su traducción.

Si está editando un fichero, cuando se abra incluirá normalmente los siguientes campos: 

* **Name:** Este es el nombre del archivo en inglés y el nombre que se muestra en inglés en la interfaz de Foundry para esta habilidad, dote, conjuro, etc. No lo toques. Si detectas un error tipográfico, no dudes en comunicarlo en la sala de discordia. Se remitirá a los anglófonos, que realizarán los cambios.

* **Nombre:** Aquí debe introducir el nombre que ha traducido si no está ya traducido. Si ya existe en la versión española, no se toca porque primero hay que comprobar si no aparece también en otra parte de los datos para asegurarse de modificarlos todos.

* **Estado:** El estado se utiliza para generar las tablas necesarias para medir el progreso de la traducción y ver dónde es necesario intervenir. 
** El indicador _oficial_ indica que el archivo está actualizado y corresponde a la traducción oficial realizada por la editorial española (Devir). Sin embargo, es posible que el contenido ya haya sido objeto de una fe de erratas que aún no ha sido tenida en cuenta por el editor de la versión española del libro. En caso de discrepancias, confíe en el contenido en inglés. Del mismo modo, el reprocesamiento se realiza para eliminar referencias a páginas, por ejemplo.
** El estado _libre_ indica que el texto propuesto procede de una traducción libre. Puede tratarse de una traducción oficial modificada por ser errónea o engañosa, de una fe de erratas aplicada o de una traducción realizada antes de que Devir traduzca el texto. El proyecto lleva un año y medio de adelanto.
** El estado _ninguna_ indica que el archivo no se ha traducido nunca (o que el anterior colaborador despistado olvidó cambiar el estado).

** Si la traducción la hace usted, sustituya _ninguna_ por **libre**.
** Si pone la traducción oficial hecha por Devir al español, entonces sustituye el estado por **oficial**. 

Si el estado indica **modificada**, significa que el archivo en inglés ha sufrido cambios desde la última vez que se extrajo. Puede tratarse de cambios en la estructura, etiquetas, inserción de fórmulas de automatización, introducción de efectos codificados en el sistema Foundry para facilitar el juego en la plataforma. 

En este caso, debajo del estado, debería haber otro campo llamado **Estado original:** Este campo, por tanto, sólo existe si el fichero ha sido traducido anteriormente. Indica si la traducción anterior procede de una traducción libre o de una traducción oficial. En caso de que edite este archivo, tiene que borrar cambiado para restaurar el estado original borrando la línea Estado original después de hacer las modificaciones. Por lo tanto, para trabajar en un archivo modificado, debes asegurarte de identificar cualquier cambio que se haya producido en el va entre las dos extracciones utilizando las herramientas de historial de git. Si no los utiliza, debe comprobarlo todo, incluidos los enlaces. Es mejor asegurarse de que lo coges todo antes de volver a cambiar el estado a su estado anterior, ya que la bandera de cambiado sirve para llamar la atención de los que mantienen el sistema.

* **PrereqEN:** Este campo sólo existe para las dotes en este momento y corresponde a los prerrequisitos cuando existen. No hay que tocarlo porque el sistema buscará los datos útiles automáticamente durante la extracción.

* **PrereqES:** Si no hay nada y el campo Prereq está rellenado, tendrá que rellenar los prerrequisitos después de traducirlos. Si hay más de una, debe separarlas con una barra vertical (que puede obtenerse pulsando AltGr+6).

------ **Descripción (en)** ------
seguido de un texto en inglés. Incluye un formato con etiquetas de código que sirve de plantilla pero que no debe modificarse, ya que lo genera la extracción automática.

------ **Descripción (es)** ------
Si no hay traducción, este campo está vacío y tendrá que rellenarlo completamente poniendo de nuevo las etiquetas presentes en inglés.

Hay otros campos extraídos que funcionan según el mismo principio. El inglés arriba y el español abajo.

Traduce el texto utilizando el Diccionario para las palabras clave y las técnicas, si es posible. Incluye todas las palabras que pueden enlazarse y referenciarse a partir de la traducción inglesa.

Puede consultar fácilmente un archivo ya traducido para ver cómo está compuesto.

Si es necesario, puede obtener ayuda de varios miembros de la comunidad hispana en la sala de Discord Foundry VTT Español

* **Algunas convenciones y observaciones** :

** La mayúscula sólo se utiliza en la primera palabra del nombre de la habilidad que está traduciendo y en los nombres propios. Así: _Levantar el escudo_

** La primera letra de las palabras que corresponden a un término técnico del juego suele ir en mayúscula. Así : _**A**tcas a una criatura **O**bservado._ Esto permite al lector saber que se está dirigiendo a un término técnico del juego.

** Las etiquetas < span >< /span > suelen eliminarse cuando no van seguidas de otras instrucciones de codificación y simplemente encierran texto. No debería quedar ninguna después de realizar los cambios, pero a veces puede haber etiquetas huérfanas.

** Se eliminan absolutamente todas las referencias a las páginas de los libros, ya que estamos navegando por Internet y es fácil crear enlaces.

** Cuando se hace referencia a que un conjuro aparece "en este libro" se debe sustituir por "en el libro básico".

** En ve, en una descripción, en la medida de lo posible, las listas creadas se vuelven a poner en orden alfabético después de la traducción o por nivel y luego en orden alfabético si se clasifica por nivel siempre que sea posible. En caso de duda, no dude en pedir consejo antes de hacerlo.

** En textos descriptivos: 1/día se convierte en **una vez al día**. 1 min. se convierte en **1 minuto**. Tenemos espacio, cosa que no ocurre con los libros físicos. El inglés ocupa menos espacio y los traductores a menudo tienen que reducirlo al mínimo para respetar la paginación.

** Hay etiquetas para los enlaces que reconocerás fácilmente porque tienen la siguiente forma `@Compendium[pf2e.feats-srd.muMOxZyduEFv8UT6]{Nombre en español}`. Al cruzarlas, sólo se reemplaza lo que está entre las llaves {nombre mostrado}. A continuación, puede poner sin preocupación el contenido del corchete en femenino, plural o incluso conjugado. Los enlaces entre ficheros se realizan haciendo referencia al ID de cada fichero en los compendios. El ID se indica entre los [corchetes]. Esta identificación no debe modificarse nunca.

** Hay etiquetas que permiten, bajo Foundry, tirar los dados desde el archivo. Tienen una estructura muy particular `[[/r 1d4[perforante]]]{1d4 daño perforante}` que no tocas a menos que seas capaz de codificarlos. Además, hay etiquetas para efectos que son de interés para quienes utilizan Foundry. Traduces el nombre del efecto cuando encuentras uno entre `{text to translate}`. Lo que hay entre `{` se traducirá `}` y será mostrado por el sistema. Lo que está entre corchetes no se traduce. 

** Otras estructuras denominadas rollos en línea son más complejas de modificar con fines de prueba. Pide consejo en el canal de Discord antes de tocarlos.