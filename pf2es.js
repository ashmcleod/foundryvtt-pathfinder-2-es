class Translator {
    static get() {
        if (!Translator.instance) {
            Translator.instance = new Translator();
        }
        return Translator.instance;
    }

    // Initialize translator
    async initialize() {

        // Signalize translator is ready
        Hooks.callAll("pf2FR.ready");
    }

    constructor() {
        this.initialize();
    }

}

Hooks.once("init", () => {
    game.langFRPf2e = Translator.get();

    game.settings.register("pf2-compendium-es", "name-display", {
        name: "Qué modo deseas?",
        hint: "Aquí puede elegir cómo se traducirán y mostrarán los nombres de los actores, objetos y diarios de los compendios.",
        scope: "world",
        type: String,
        choices: {
            "ve-vo": "VE (VO)",
            "vo-ve": "VO (VE)",
            "ve": "VE",
            "vo": "VO"
        },
        default: "ve-vo",
        config: true,
        onChange: foundry.utils.debouncedReload
    });
    var dir_path = "babele/"

    if(game.settings.get('pf2-compendium-es', 'name-display') == "ve"){
         dir_path = "babele/es/"
    }else{
         dir_path = "babele-alt/"+game.settings.get('pf2-compendium-es', 'name-display')+"/es/"
    }

    Babele.get().register({
        module: "pf2-compendium-es",
        lang: 'es',
        dir: dir_path
    });
});

Hooks.once("babele.ready", () => {
    game.pf2e.ConditionManager.initialize();

    if (game.modules.get("lang-pf2-compendium-es")?.active){
        ui.notifications.error("El paquete \"Sistema PF2 Español\" todavía está instalado en esta parte; ya no es útil y, por lo tanto, se puede desinstalar.")
    }
});

