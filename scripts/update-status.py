#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import yaml
import os
import datetime

from libdata import *
from statuses import *

ROOT=".."
packs = getPacks()

def handlePack(pack_id):
  dirpath= "%s/data/%s" % (ROOT, pack_id)

  statusContentOK = "| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |\n" + "|-----------|----------------|----------------|:------:|\n"
  statusContentChanged = "| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |\n" + "|-----------|----------------|----------------|:------:|\n"
  statusContentEmpty = "| Fichero   | Nombre (EN)    | Estado |\n" + "|-----------|----------------|:------:|\n"
  statusContentAT = "| Fichero   | Nombre (EN)    | Estado |\n" + "|-----------|----------------|:------:|\n"
  statusContentNOK = "| Fichero   | Nombre (EN)    |\n" + "|-----------|----------------|\n"

  longueur_initiale = len(statusContentNOK)

  files = [f for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f))]
  files = sorted(files, key=str.casefold)
  stats = {}

  for f in files:
    data = fileToData(os.path.join(dirpath,f))

    if data['status'] in stats:
      stats[data['status']]+=1
    else:
      stats[data['status']]=1

    if data['status'] == ST_NINGUNA or (data['status']== ST_MODIFICADA and len(data['nameES']) == 0):
      statusContentNOK += "|[%s](%s/%s)|%s|\n" % (f, pack_id, f, data['nameEN'])
    elif data['status'] == ST_AUTO_TRAD or data['status'] == ST_AUTO_GOOGTRAD:
      statusContentAT += "|[%s](%s/%s)|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['status'])
    elif data['status'] == ST_MODIFICADA:
      statusContentChanged += "|[%s](%s/%s)|%s|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['nameES'], data['status'])
    elif data['status'] == ST_VACIA:
      statusContentEmpty += "|[%s](%s/%s)|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['status'])
    else:
      statusContentOK += "|[%s](%s/%s)|%s|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['nameES'], data['status'])

  content = "# Estado de la traducción (%s)\n\n" % pack_id
  for s in stats:
    content += " * **%s**: %d\n" % (s, stats[s])

  content += "\n\nDÚltima actualización: %s *(hora de Canadá/Montreal)*" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
  content += "\n\nEste archivo se genera automáticamente. ¡NO MODIFICAR!"
  if ST_NINGUNA in stats and stats[ST_NINGUNA] > 0 or len(statusContentNOK)!=longueur_initiale:
    content += "\n## Lista de traducciones pendientes\n\n"
    content += statusContentNOK
  if ST_AUTO_TRAD in stats and stats[ST_AUTO_TRAD] > 0 or ST_AUTO_GOOGTRAD in stats and stats[ST_AUTO_GOOGTRAD] > 0:
    content += "\n## Lista de traducciones automáticas que deben corregirse/retraducirse\n\n"
    content += statusContentAT
  if ST_MODIFICADA in stats and stats[ST_MODIFICADA] > 0:
    content += "\n## Lista de elementos modificados en VO y que deben comprobarse\n\n"
    content += statusContentChanged
  if ST_VACIA in stats and stats[ST_VACIA] > 0:
    content += "\n## Lista de elementos vacíos que no se pueden traducir\n\n"
    content += statusContentEmpty
  content += "\n## Lista de traducciones realizadas\n\n"
  content += statusContentOK

  with open("%s/data/status-%s.md" % (ROOT, pack_id), 'w', encoding='utf-8') as f:
    f.write(content)


for p in packs:
  handlePack(p["id"])

  if 'items' in p:
    handlePack(p["id"]+"-items")

  if 'pages' in p:
    handlePack(p["id"]+"-pages")
