#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re
import requests
import json
import html
import time
import logging

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ExpectedConditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from dataclasses import dataclass
from statuses import *


logging.basicConfig(filename='translation.log', level=logging.INFO)

@dataclass
class TranslationData:
    data: str
    status: str

#
# Esta función crea un controlador Selenium para la conexión a DeepL Translator
#
def translator_driver():
    options = webdriver.FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)
    try:
        driver.get("https://www.deepl.com/en/translator#en/es/thing")
        time.sleep(3)
        return driver
    except Exception as e:
        print(e)
        driver.quit()
        raise


# Esta función intenta una traducción con DeepL y luego con GoogleTranslate si se produce un error
#
def full_trad(driver, data):
    try:
        tradDesc = dirtyTranslate(driver, data)
        status = "auto-trad"
        logging.info("Success !")
    # caso de un fichero sin descripción
    except EmptyDescriptionException as e:
        logging.error("Error while translating : EmptyDescriptionException")
        tradDesc = ""
        status = ST_VACIA
    except EmptyTranslationException as e:
        logging.error("Error while translating : EmptyTranslationException")
        tradDesc = ""
        status = ST_NINGUNA
    except Exception as e:
        exception_name = type(e).__name__
        logging.error("Error while translating: %s" % exception_name)
        if exception_name == "TimeoutException":
            logging.error("The text is very long and the timeout for the \
                translation has been exceeded, or the connection has been blocked \
                because of too many requests to a free account.")
        else:
            raise
        # si se produce un error, intente traducir automáticamente con DeepL Translator
        try:
            tradDesc = dirtyTranslate(driver, data, True)
            status = "auto-trad"
        except Exception as e:
            logging.error("Error while translating with API : %s" % exception_name)
            logging.error("Even API Translate fails here, this file is hopeless ... ")
            tradDesc = ""
            status = ST_NINGUNA
    return TranslationData(tradDesc, status)


#
# Esta función intenta una traducción automática
# con DeepL Translator
#
from selenium.webdriver.common.keys import Keys
# import deepl

def dirtyTranslate(driver, data, api = False):
    # if api:
    #     auth_key = os.environ.get('DEEPL_KEY', "")
    #     print("Translating with API %s" % auth_key)
    #     translator = deepl.Translator(auth_key)

    #     result = translator.translate_text(data, target_lang="ES")
    #     return cleanTrad(result.text)  # "Bonjour, le monde !"

    length = len(data)
    if (length < 10):
        raise EmptyDescriptionException()
        return ""
    inputer = driver.find_element_by_class_name('lmt__source_textarea')
    #inputer.clear()
    WebDriverWait(driver, 20).until(
       ExpectedConditions.element_to_be_clickable((By.CLASS_NAME, 'lmt__source_textarea')))
    driver.execute_script('document.querySelector("#translator-source-clear-button") && document.querySelector("#translator-source-clear-button").click();')
    webdriver.ActionChains(driver).click(inputer).perform()
    
    webdriver.ActionChains(driver).send_keys(data).perform()
    WebDriverWait(driver, 20).until(
        ExpectedConditions.text_to_be_present_in_element_value((By.CLASS_NAME, 'lmt__target_textarea'), " "))
    if length > 8000:
        time.sleep(82)
    elif length > 1000:
        time.sleep(22)
    elif length > 50:
        time.sleep(2)
    output = driver.find_element_by_class_name('lmt__target_textarea')
    transData = cleanTrad(output.get_attribute("value"))
    #output.clear()
    length = len(transData)
    if (length < 10):
        raise EmptyTranslationException()
        return ""
    return transData




#
# Limpia la traducción corrigiendo términos mal traducidos o
# que no debían traducirse
#
def cleanTrad(data):
    data = data.replace("&gt ;", "&gt")
    data = data.replace("Activate", "Activación")
    data = data.replace("Interact", "Interactúa")
    data = data.replace("60 feet", "18 metros")
    data = data.replace("30 feet", "9 metros")
    data = data.replace("Traducido con www.DeepL.com/Translator (versión gratuita)", "")
    return data


#
# Esta función intenta una traducción automática 
# en Google Translate
#
def dirtyGoogleTranslate(data):
  length = len(data)
  if (length<10):
    raise EmptyDescriptionException()
    return ""
  options = webdriver.FirefoxOptions()
  options.add_argument("--headless")
  driver = webdriver.Firefox(options=options)
  driver.get("https://translate.google.com/?sl=en&tl=es&format=html&op=translate")
  inputer = driver.find_element_by_xpath("//textarea[@jsname='BJE2fc']")
  inputer.send_keys(data)
  output = WebDriverWait(driver, timeout=120).until(lambda d: d.find_element_by_xpath("//div[@class='J0lOec']"))
  transData = output.text
  driver.quit()
  transData = cleanGoogleTrad(transData)
  return transData


#
# Limpia la traducción corrigiendo términos mal traducidos o
# que no debían traducirse
#
def cleanGoogleTrad(data):
  data = data.replace("</ ", "</")
  data = data.replace("<Strong>", "<strong>")
  data = data.replace("</Strong>", "</strong>")
  data = re.sub("<[f,F]+uert[e]{0,1}>", "<strong>", data)
  data = re.sub("</[f,F]+uert[e]{0,1}>", "</strong>", data)
  data = re.sub("[0-9]+D[0-9]+", lambda s: s.group(0).replace("D", "d"), data)
  data = data.replace("</P>", "</p>")
  data = data.replace("[[/ R", "[[/r")
#   data = data.replace("aumentar", "Intensificar")
#   data = data.replace("#feu", "#fire")
#   data = data.replace("#acide", "#acid")
  return data

class EmptyDescriptionException(Exception):
    """
  Excepción levantada si el artículo traducido no tiene descripción
  """

    def __init__(self, message="There is no description in this file. Check for missing data."):
        self.message = message

class EmptyTranslationException(Exception):
    """
  Excepción levantada si la traducción se recibe vacía
  """

    def __init__(self, message="There is no translation received."):
        self.message = message
