#!/usr/bin/python3
# -*- coding: utf-8 -*-
from libdata import readFolder, dataToFile, getPacks
from statuses import *
import hashlib
import json
import glob
import os
import re
import functools

ROOT = "../"

replaces_re = re.compile("#+([a-f0-9]{32})#+(?:XXX|([^#]+)?)?#+")
with open("%sstrings/replaces.json" % ROOT) as json_file:
    replaces = json.load(json_file)

def my_replace(match):
    global replaces
    md5 = match.group(1)
    label = match.group(2) or ""
    if md5 in replaces:
      print("Replaced %s with %s" % (match.group(0), "%s%s" % (replaces[md5], label)))
      return "%s%s" % (replaces[md5], label)
    return match.group(0)

def check(pack_id):
    global errors, inline_re
    print (f'Checking {pack_id}')
    folderData = readFolder("%sdata/%s/" % (ROOT, pack_id))
    existing = folderData[0]
    existingByName = folderData[1]

    # ========================
    # create or update entries
    # ========================
    for id in existing:
        updated = False
        filename = existing[id]["filename"]
        filepath = "%sdata/%s/%s" % (ROOT, pack_id, filename)
        #print("\t" + filename)
        file = open(filepath, "r+")
        contents = file.read()

        fixed = replaces_re.sub(my_replace, contents)
        file.seek(0)
        file.write(fixed)
        file.truncate()
        file.close()

packs = getPacks()
for p in packs:
  check(p["id"])
  if "items" in p:
    check(p["id"]+"-items")
  if "pages" in p:
    check(p["id"]+"-pages")
