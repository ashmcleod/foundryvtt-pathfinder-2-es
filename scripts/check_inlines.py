#!/usr/bin/python3
# -*- coding: utf-8 -*-
from libdata import readFolder, dataToFile, getPacks
from statuses import *
import hashlib
import json
import glob
import os
import re
import functools

ROOT = "../"

inline_types = {"UUID"}
founds = []
check_content_re = re.compile("((?P<key>[^|]+):(?P<value>[^|]+))")
inline_re = re.compile("@([a-zA-Z0-9.]+)\[([^\]]+)\](?:{([^}]+)})?")

class NotInList(Exception):
  def __init__(self, key, message="Key not in list"):
      self.key = key
      self.message = "%s not in list" % self.key
      super().__init__(self.message)
  pass

def fixInline(p):
  global incorrect_inlines
  if p["inline"] not in ["Check", "Compendium", "Localize", "actor.level", "Template", "UUID", "actor.abilities.cha.mod", "RollTable"]:
    p["errors"].append("inline")
    incorrect_inlines.add(p["inline"])
  return p

def fixCheckContent(c):
  k = c.group("key")
  v = c.group("value")
  print(k)
  if k not in ["type"]:
    incorrect_check_keys.add(k)
    return (k + ":" + v, ["NotInList"])
  return (k + ":" + v, [])

def fixContent(p):
  global incorrect_check_keys, check_content_re
  inline = p["inline"]
  if inline == "Check":
    contentMap = list(check_content_re.finditer(p["content"]))
    print(str(contentMap))
    if len(contentMap) == 0:
      p["errors"].append("content")
      return p
    fixedCheckContent = list(map(fixCheckContent, contentMap))
    if len(list(filter(lambda c: len(c[1]) > 0, contentMap))):
      p["errors"].append("content")
      return p
    p["content"] = "|".join(map(lambda c: c[0], fixedCheckContent))
  return p

def reducer(acc, item):
  acc[item["original"]] = ("@%s[%s]" % (item["inline"], item["content"])) + ("{%s}" % item["label"] if item["label"] is not None else "")
  return acc

def matchToPool(m):
  return {"original": m[0], "inline": m.groups()[0], "content": m.groups()[1], "label": m.groups()[2], "errors": []}

def check(pack_id):
    global errors, inline_re
    print (f'Checking {pack_id}')
    folderData = readFolder("%sdata/%s/" % (ROOT, pack_id))
    existing = folderData[0]
    existingByName = folderData[1]

    # ========================
    # create or update entries
    # ========================
    for id in existing:
        updated = False
        filename = existing[id]["filename"]
        filepath = "%sdata/%s/%s" % (ROOT, pack_id, filename)
        #print("\t" + filename)
        file = open(filepath, "r+")
        contents = file.read()
        matches = list(inline_re.finditer(contents))
        n = existing[id]["nameEN"] if "nameEN" in existing[id] else ""
        pool = map(matchToPool, matches)
        inlineFixed = list(map(fixInline, pool))
        contentFixed = map(fixContent, inlineFixed)

        errors.append(filter(lambda x: len(x["errors"]) > 0, contentFixed))
        replacements = functools.reduce(reducer, contentFixed, {})
        # if replacements:
        #   for key, value in replacements.items():
        #     contents = contents.replace(key, value)
        #   #replacer = StringReplacer(replacements, True)
        #   #fixed = replacer(contents)
        #   file.seek(0)
        #   file.write(contents)
        #   file.truncate()
        file.close()

        # if len(matches) > 0:
        #   print("\t" + n)
        # for m in matches:
        #   map = fix(m)
        #   inline_types.add(m.group(1))
        #   if m.group(1) == "actor.nivel":
        #     founds.append(m[0])
        #   print("\t\t" + m[0] + " " + m.group(1) + " " + m.group(2))


packs = getPacks()
errors = []
incorrect_inlines = set()
incorrect_check_keys = set()
for p in packs:
  check(p["id"])
  if "items" in p:
    check(p["id"]+"-items")
  if "pages" in p:
    check(p["id"]+"-pages")

print(str(incorrect_inlines))
print(str(incorrect_check_keys))
