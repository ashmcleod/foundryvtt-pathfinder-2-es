# Estado de la traducción (pathfinder-dark-archive-items)

 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[GReYolkvohIlgdbM.htm](pathfinder-dark-archive-items/GReYolkvohIlgdbM.htm)|Downpour|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
