# Estado de la traducción (pathfinder-bestiary-2-items)

 * **modificada**: 3016
 * **ninguna**: 300
 * **vacía**: 107


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0BT4rsp6NVFOXHYe.htm](pathfinder-bestiary-2-items/0BT4rsp6NVFOXHYe.htm)|Dispel Magic (At will)|
|[0mexpNYfz6rpTLXa.htm](pathfinder-bestiary-2-items/0mexpNYfz6rpTLXa.htm)|Kukri|+1,striking|
|[0ODNsZ6th57SXfiA.htm](pathfinder-bestiary-2-items/0ODNsZ6th57SXfiA.htm)|Color Spray (At Will)|
|[1Hex7UaPya8easf7.htm](pathfinder-bestiary-2-items/1Hex7UaPya8easf7.htm)|Jump (At will)|
|[1JQ0EDkjIezvUDEb.htm](pathfinder-bestiary-2-items/1JQ0EDkjIezvUDEb.htm)|Tree Shape (At will) (Appears as a burnt, dead tree)|
|[1RcuxgvvscX2wpNJ.htm](pathfinder-bestiary-2-items/1RcuxgvvscX2wpNJ.htm)|Paranoia (At Will)|
|[2k1TYJ74qItzuakS.htm](pathfinder-bestiary-2-items/2k1TYJ74qItzuakS.htm)|Detect Alignment (Lawful only)|
|[2mXUYLr8NYh6u9sO.htm](pathfinder-bestiary-2-items/2mXUYLr8NYh6u9sO.htm)|Tongues (Constant)|
|[2q5Hz0vUEER1aViF.htm](pathfinder-bestiary-2-items/2q5Hz0vUEER1aViF.htm)|Invisibility (At Will) (Self Only)|
|[2s2y3NKDRdpdH6gx.htm](pathfinder-bestiary-2-items/2s2y3NKDRdpdH6gx.htm)|Fire Shield (Constant)|
|[2tizdcbdj6rD6U6d.htm](pathfinder-bestiary-2-items/2tizdcbdj6rD6U6d.htm)|True Seeing (Constant)|
|[2VooFbfGeZCEGVLM.htm](pathfinder-bestiary-2-items/2VooFbfGeZCEGVLM.htm)|Dimension Door (At will)|
|[30vWGptXw7CTKs0b.htm](pathfinder-bestiary-2-items/30vWGptXw7CTKs0b.htm)|Sling|+1|
|[36CmhzOplKDvSnh4.htm](pathfinder-bestiary-2-items/36CmhzOplKDvSnh4.htm)|Illusory Disguise (At will)|
|[38kb57Hhd2E3vn6A.htm](pathfinder-bestiary-2-items/38kb57Hhd2E3vn6A.htm)|Detect Alignment (At will) (Good only)|
|[3cOLTqxhMIIVbLaC.htm](pathfinder-bestiary-2-items/3cOLTqxhMIIVbLaC.htm)|Circle of Protection (Against evil only)|
|[3de6fgPOs2joOR89.htm](pathfinder-bestiary-2-items/3de6fgPOs2joOR89.htm)|Air Walk (Constant)|
|[3KU5Y0WaS2TkXwb0.htm](pathfinder-bestiary-2-items/3KU5Y0WaS2TkXwb0.htm)|+2 Greater Resilient Full Plate|
|[3M2SYWM5VaJw4Y1F.htm](pathfinder-bestiary-2-items/3M2SYWM5VaJw4Y1F.htm)|Ventriloquism (At will)|
|[3n0E4BJ7jYbNm4ux.htm](pathfinder-bestiary-2-items/3n0E4BJ7jYbNm4ux.htm)|True Seeing (Constant)|
|[3NM1dClgOcqEF0sP.htm](pathfinder-bestiary-2-items/3NM1dClgOcqEF0sP.htm)|Meld into Stone (At will)|
|[4dJ4DHIq0hvHbkYj.htm](pathfinder-bestiary-2-items/4dJ4DHIq0hvHbkYj.htm)|Dimension Door (Self Only)|
|[4irhhdrSS6R8cngA.htm](pathfinder-bestiary-2-items/4irhhdrSS6R8cngA.htm)|Glitterdust (At Will)|
|[4O37hYlL21BWJ3M8.htm](pathfinder-bestiary-2-items/4O37hYlL21BWJ3M8.htm)|Detect Alignment (At will) (Evil only)|
|[4Pg0aWLpvKg5oEEp.htm](pathfinder-bestiary-2-items/4Pg0aWLpvKg5oEEp.htm)|Air Walk (Constant)|
|[4rEcF7higflYgkOC.htm](pathfinder-bestiary-2-items/4rEcF7higflYgkOC.htm)|Dimension Door (At will)|
|[4SMYBQGP5HVl75M7.htm](pathfinder-bestiary-2-items/4SMYBQGP5HVl75M7.htm)|+1 Resilient Breastplate|
|[4TTBHqodJtZulkHb.htm](pathfinder-bestiary-2-items/4TTBHqodJtZulkHb.htm)|Divine Wrath (Chaotic)|
|[4wGOi1Y9QjvOIUWd.htm](pathfinder-bestiary-2-items/4wGOi1Y9QjvOIUWd.htm)|Speak with Animals (Constant)|
|[56O2v35BcXiX1Ksi.htm](pathfinder-bestiary-2-items/56O2v35BcXiX1Ksi.htm)|Talking Corpse (At will)|
|[5AQMHCwBWHE6c3p1.htm](pathfinder-bestiary-2-items/5AQMHCwBWHE6c3p1.htm)|Endure Elements (Constant)|
|[5b4HNF0LotCdB3Bv.htm](pathfinder-bestiary-2-items/5b4HNF0LotCdB3Bv.htm)|+1 Composite Longbow|+1|
|[5FQFNi5wNLjJfVVi.htm](pathfinder-bestiary-2-items/5FQFNi5wNLjJfVVi.htm)|Divine Wrath (At will)|
|[5kLTjDQN7XI3SO30.htm](pathfinder-bestiary-2-items/5kLTjDQN7XI3SO30.htm)|Freedom of Movement (Constant)|
|[5kY50OvZXGwzw1RS.htm](pathfinder-bestiary-2-items/5kY50OvZXGwzw1RS.htm)|Divine Decree (Chaotic only)|
|[5oQstrPyIsACaBES.htm](pathfinder-bestiary-2-items/5oQstrPyIsACaBES.htm)|See Invisibility (Constant)|
|[5pXnmJAJ3WP1fA9Z.htm](pathfinder-bestiary-2-items/5pXnmJAJ3WP1fA9Z.htm)|Speak with Plants (Constant)|
|[5QK8En6tcwzzpyVx.htm](pathfinder-bestiary-2-items/5QK8En6tcwzzpyVx.htm)|Produce Flame (6th)|
|[5u6wc1mpLV2mconA.htm](pathfinder-bestiary-2-items/5u6wc1mpLV2mconA.htm)|See Invisibility (Constant)|
|[6JJdqYYhIoqVcIMD.htm](pathfinder-bestiary-2-items/6JJdqYYhIoqVcIMD.htm)|Primal Call (doesn't require secondary casters)|
|[6MFcZa19cUunLJRu.htm](pathfinder-bestiary-2-items/6MFcZa19cUunLJRu.htm)|Blink (Constant)|
|[6vHpvXfI8ykMKIr4.htm](pathfinder-bestiary-2-items/6vHpvXfI8ykMKIr4.htm)|Air Walk (Constant)|
|[71Z5UP7uzcvxSIy9.htm](pathfinder-bestiary-2-items/71Z5UP7uzcvxSIy9.htm)|Plane Shift (To the Material Plane, Plane of Fire, or Plane of Earth only)|
|[7FgPojTX7HEIApDW.htm](pathfinder-bestiary-2-items/7FgPojTX7HEIApDW.htm)|Blank Formula Book|
|[7p38bExu8L9jgQ5N.htm](pathfinder-bestiary-2-items/7p38bExu8L9jgQ5N.htm)|See Invisibility (Constant)|
|[7rYfyAQXss9duKBu.htm](pathfinder-bestiary-2-items/7rYfyAQXss9duKBu.htm)|Suggestion (At Will)|
|[7UB9CyvhkK4ZhIuW.htm](pathfinder-bestiary-2-items/7UB9CyvhkK4ZhIuW.htm)|Detect Alignment (At will) (Lawful only)|
|[8gcJAk8CnEreKNfO.htm](pathfinder-bestiary-2-items/8gcJAk8CnEreKNfO.htm)|Wall of Fire (At Will)|
|[8lQzvlhmZk1A0Cgw.htm](pathfinder-bestiary-2-items/8lQzvlhmZk1A0Cgw.htm)|Magic Missile (At will)|
|[8vvlKfwG2KuafsIJ.htm](pathfinder-bestiary-2-items/8vvlKfwG2KuafsIJ.htm)|Calm Emotions (At will)|
|[92vCOzCAnkOL6uGy.htm](pathfinder-bestiary-2-items/92vCOzCAnkOL6uGy.htm)|Gust of Wind (At will)|
|[9ejFXBhPxifD2amI.htm](pathfinder-bestiary-2-items/9ejFXBhPxifD2amI.htm)|True Seeing (Constant)|
|[A0HFdfwZ7P5RQBJw.htm](pathfinder-bestiary-2-items/A0HFdfwZ7P5RQBJw.htm)|Charm (At will)|
|[A1Jg6isNRXtE1HSR.htm](pathfinder-bestiary-2-items/A1Jg6isNRXtE1HSR.htm)|Water Walk (Constant)|
|[AaUkJlN6zMw3tT9b.htm](pathfinder-bestiary-2-items/AaUkJlN6zMw3tT9b.htm)|Obscuring Mist (At will)|
|[Aeu3hLK3x9HeXzhD.htm](pathfinder-bestiary-2-items/Aeu3hLK3x9HeXzhD.htm)|Darkness (At Will)|
|[aF866brRh1uTW7EA.htm](pathfinder-bestiary-2-items/aF866brRh1uTW7EA.htm)|+1 Striking Falchion|+1,striking|
|[ajiWSW5GFwz1c2fZ.htm](pathfinder-bestiary-2-items/ajiWSW5GFwz1c2fZ.htm)|Polar Ray (At will)|
|[anKjfgYsttUshhxM.htm](pathfinder-bestiary-2-items/anKjfgYsttUshhxM.htm)|True Seeing (Constant)|
|[anSe09sUoNwtRyCS.htm](pathfinder-bestiary-2-items/anSe09sUoNwtRyCS.htm)|Mind Probe (At will)|
|[aO9yC0TQ3hBNeO78.htm](pathfinder-bestiary-2-items/aO9yC0TQ3hBNeO78.htm)|Obscuring Mist (At Will)|
|[ARgSXbi7YTyqxDpw.htm](pathfinder-bestiary-2-items/ARgSXbi7YTyqxDpw.htm)|Dimension Door (At will)|
|[ARiXN5DX2BVzOvCn.htm](pathfinder-bestiary-2-items/ARiXN5DX2BVzOvCn.htm)|Dimension Door (At will)|
|[aTbUZmqX3wxWV8hR.htm](pathfinder-bestiary-2-items/aTbUZmqX3wxWV8hR.htm)|Dimension Door (At will)|
|[aTZDvupAiIZ4j3su.htm](pathfinder-bestiary-2-items/aTZDvupAiIZ4j3su.htm)|+3 Greater Striking Warhammer|+3,greaterStriking|
|[aV7tH63nZZOcXqzb.htm](pathfinder-bestiary-2-items/aV7tH63nZZOcXqzb.htm)|Tongues (Constant)|
|[AVCJ2SYe5pptCOsw.htm](pathfinder-bestiary-2-items/AVCJ2SYe5pptCOsw.htm)|Fear (At will)|
|[B2XsgB3WjM0q9j54.htm](pathfinder-bestiary-2-items/B2XsgB3WjM0q9j54.htm)|Create Undead (Shadows Only)|
|[b88vNXkalAte3tvf.htm](pathfinder-bestiary-2-items/b88vNXkalAte3tvf.htm)|Restoration (At will)|
|[BDA6jFei5doNakee.htm](pathfinder-bestiary-2-items/BDA6jFei5doNakee.htm)|Dimension Door (At will)|
|[BkBEhqLM3mXNI13M.htm](pathfinder-bestiary-2-items/BkBEhqLM3mXNI13M.htm)|Dimension Door (At Will)|
|[bkk440fw7A5zFFVh.htm](pathfinder-bestiary-2-items/bkk440fw7A5zFFVh.htm)|Illusory Object (At will)|
|[BNtTt4rbMDmj6cYA.htm](pathfinder-bestiary-2-items/BNtTt4rbMDmj6cYA.htm)|Dispel Magic (At will)|
|[boE5n1kGWU3PGXxn.htm](pathfinder-bestiary-2-items/boE5n1kGWU3PGXxn.htm)|Burning Hands (At Will)|
|[BrL7yO7TVS8Vo0MQ.htm](pathfinder-bestiary-2-items/BrL7yO7TVS8Vo0MQ.htm)|+1 Longbow|+1|
|[buj6GwDfNq7DUnUI.htm](pathfinder-bestiary-2-items/buj6GwDfNq7DUnUI.htm)|Hypnotic Pattern (At Will)|
|[BuPE1MFIbw6ZThvx.htm](pathfinder-bestiary-2-items/BuPE1MFIbw6ZThvx.htm)|Detect Alignment (Good Only) (At Will)|
|[bXd6K0aIwu8oNq40.htm](pathfinder-bestiary-2-items/bXd6K0aIwu8oNq40.htm)|Freedom of Movement (Constant)|
|[c19tAQpDeptWNyHM.htm](pathfinder-bestiary-2-items/c19tAQpDeptWNyHM.htm)|Hydraulic Push (At Will)|
|[C3660uLElTZLdhwb.htm](pathfinder-bestiary-2-items/C3660uLElTZLdhwb.htm)|Freedom of Movement (Constant)|
|[c8DVpWzqdTXj4S64.htm](pathfinder-bestiary-2-items/c8DVpWzqdTXj4S64.htm)|True Seeing (Constant)|
|[CbjDPYYSPr0LQHyI.htm](pathfinder-bestiary-2-items/CbjDPYYSPr0LQHyI.htm)|Invisibility (At will) (Self only)|
|[cfgD8ZaLXL5H6PyY.htm](pathfinder-bestiary-2-items/cfgD8ZaLXL5H6PyY.htm)|Dispel Magic (At will)|
|[Cj6s7OuIDPePwy5R.htm](pathfinder-bestiary-2-items/Cj6s7OuIDPePwy5R.htm)|Gust of Wind (At will)|
|[cMrDWgOf9PHIIIZT.htm](pathfinder-bestiary-2-items/cMrDWgOf9PHIIIZT.htm)|True Seeing (Constant)|
|[cMsllioMOMLPtc53.htm](pathfinder-bestiary-2-items/cMsllioMOMLPtc53.htm)|True Seeing (Constant)|
|[cOqmT9M74QEp6QXX.htm](pathfinder-bestiary-2-items/cOqmT9M74QEp6QXX.htm)|Control Water (At will)|
|[cPd8aBLUXVC2uVQc.htm](pathfinder-bestiary-2-items/cPd8aBLUXVC2uVQc.htm)|Burning Hands (At Will)|
|[CTBtapRqTyHVFJWP.htm](pathfinder-bestiary-2-items/CTBtapRqTyHVFJWP.htm)|Dimension Door (At will)|
|[CTeOUny3G8ZgKMct.htm](pathfinder-bestiary-2-items/CTeOUny3G8ZgKMct.htm)|Plane Shift (At Will; Self Plus Skiff and Passengers Only; Astral, Ethereal, and Evil Planes Only)|
|[Cvcdr8l0DxG0Fszv.htm](pathfinder-bestiary-2-items/Cvcdr8l0DxG0Fszv.htm)|Detect Alignment (At will) (Lawful only)|
|[cXiAbItR9q9FYi5R.htm](pathfinder-bestiary-2-items/cXiAbItR9q9FYi5R.htm)|Feather Fall (Self only)|
|[CZh0sJkqcSdR6tyg.htm](pathfinder-bestiary-2-items/CZh0sJkqcSdR6tyg.htm)|Illusory Disguise (At will)|
|[D1Dv5T7CBXZtJx8F.htm](pathfinder-bestiary-2-items/D1Dv5T7CBXZtJx8F.htm)|Darkness (At Will)|
|[d3GX5vViofz5foXq.htm](pathfinder-bestiary-2-items/d3GX5vViofz5foXq.htm)|Dimension Door (At will)|
|[d5mwrX7WIXXz8R97.htm](pathfinder-bestiary-2-items/d5mwrX7WIXXz8R97.htm)|Plane Shift (Self Only)|
|[DbvMliTitDLEnD8H.htm](pathfinder-bestiary-2-items/DbvMliTitDLEnD8H.htm)|Dimension Door (At will)|
|[Dc4o0DNBB2mCvOPt.htm](pathfinder-bestiary-2-items/Dc4o0DNBB2mCvOPt.htm)|Freedom of Movement (Constant)|
|[DfffftC2BwZMqeyZ.htm](pathfinder-bestiary-2-items/DfffftC2BwZMqeyZ.htm)|Obscuring Mist (At will)|
|[E2cmnuR9GJnpvSmN.htm](pathfinder-bestiary-2-items/E2cmnuR9GJnpvSmN.htm)|+1 Resilient Full Plate|
|[eaAeSDb88zeKI7ii.htm](pathfinder-bestiary-2-items/eaAeSDb88zeKI7ii.htm)|Teleport (Self and rider only)|
|[eF4sqCqXrBkfqjib.htm](pathfinder-bestiary-2-items/eF4sqCqXrBkfqjib.htm)|Lesser Smokestick|
|[ehkSYzB2HyKwd9xA.htm](pathfinder-bestiary-2-items/ehkSYzB2HyKwd9xA.htm)|Hide|
|[eiif4YWDshtMsiDY.htm](pathfinder-bestiary-2-items/eiif4YWDshtMsiDY.htm)|Mind Blank (Constant)|
|[eRnSGx9tKMwZStVB.htm](pathfinder-bestiary-2-items/eRnSGx9tKMwZStVB.htm)|Lesser Alchemist's Fire|
|[eXhIeUv9hE9Bnejj.htm](pathfinder-bestiary-2-items/eXhIeUv9hE9Bnejj.htm)|Invisibility (At will) (Self only)|
|[ExTn8RH5Xp1BVvJk.htm](pathfinder-bestiary-2-items/ExTn8RH5Xp1BVvJk.htm)|Illusory Disguise (At Will)|
|[Faz79fumsbTV2o3W.htm](pathfinder-bestiary-2-items/Faz79fumsbTV2o3W.htm)|Mirror Image (At will)|
|[fh90HLxqhj0ijf1r.htm](pathfinder-bestiary-2-items/fh90HLxqhj0ijf1r.htm)|True Seeing (Constant)|
|[fjNBgwnXVFm9Yc36.htm](pathfinder-bestiary-2-items/fjNBgwnXVFm9Yc36.htm)|Augury (At will)|
|[fJsYqRu8WagF6tZb.htm](pathfinder-bestiary-2-items/fJsYqRu8WagF6tZb.htm)|Invisibility (At will) (Self only)|
|[FkEMFgT2F1YJ8CJN.htm](pathfinder-bestiary-2-items/FkEMFgT2F1YJ8CJN.htm)|Invisibility (At will) (Self only)|
|[FtmumZg3l3SZaIex.htm](pathfinder-bestiary-2-items/FtmumZg3l3SZaIex.htm)|Mirror Image (At will)|
|[ftyJjjKcXegKOZO6.htm](pathfinder-bestiary-2-items/ftyJjjKcXegKOZO6.htm)|Elemental Form (Fire Elemental only)|
|[fUotsbKaORew1RjV.htm](pathfinder-bestiary-2-items/fUotsbKaORew1RjV.htm)|Invisibility (Self only)|
|[fVJKjuH7SYz6QQHp.htm](pathfinder-bestiary-2-items/fVJKjuH7SYz6QQHp.htm)|Dimension Door (At will)|
|[Fzzx1tAOIDLb7AC0.htm](pathfinder-bestiary-2-items/Fzzx1tAOIDLb7AC0.htm)|Tongues (Constant)|
|[g0gdzzzvux4ApSD5.htm](pathfinder-bestiary-2-items/g0gdzzzvux4ApSD5.htm)|Glyph of Warding (At will)|
|[g2euZN5HL0HEXaye.htm](pathfinder-bestiary-2-items/g2euZN5HL0HEXaye.htm)|True Seeing (Constant)|
|[GS4ThRRm7cy5BuYJ.htm](pathfinder-bestiary-2-items/GS4ThRRm7cy5BuYJ.htm)|Oar (Functions as Mace)|
|[GXXWozDHxwpERohz.htm](pathfinder-bestiary-2-items/GXXWozDHxwpERohz.htm)|Imprisonment (Temporal stasis only)|
|[h0EZFRSOjyHLlf5b.htm](pathfinder-bestiary-2-items/h0EZFRSOjyHLlf5b.htm)|Detect Alignment (Constant) (Evil only)|
|[H1AVYB6nbAL4PkmI.htm](pathfinder-bestiary-2-items/H1AVYB6nbAL4PkmI.htm)|+3 Greater Striking Sickle|+3,greaterStriking|
|[h67Ean1BYGruHE1T.htm](pathfinder-bestiary-2-items/h67Ean1BYGruHE1T.htm)|Entangle (At will)|
|[hljYIMEGFNQ7w61Z.htm](pathfinder-bestiary-2-items/hljYIMEGFNQ7w61Z.htm)|True Seeing (Constant)|
|[HSgvSQ1CqyZNesYm.htm](pathfinder-bestiary-2-items/HSgvSQ1CqyZNesYm.htm)|Water Walk (Constant)|
|[HsTNpm0qjSAwEMqC.htm](pathfinder-bestiary-2-items/HsTNpm0qjSAwEMqC.htm)|Tongues (Constant)|
|[hTa7eD0P303Y4ykY.htm](pathfinder-bestiary-2-items/hTa7eD0P303Y4ykY.htm)|Plane Shift (Self only)|
|[hYDyNv5iN58yeeg1.htm](pathfinder-bestiary-2-items/hYDyNv5iN58yeeg1.htm)|Suggestion (At will)|
|[HyVnK39Qbmr02ZZS.htm](pathfinder-bestiary-2-items/HyVnK39Qbmr02ZZS.htm)|Religious Symbol of Ydersius|
|[hzBBpY4XdhIZXued.htm](pathfinder-bestiary-2-items/hzBBpY4XdhIZXued.htm)|True Seeing (Constant)|
|[I1dP96sPZylIIzoF.htm](pathfinder-bestiary-2-items/I1dP96sPZylIIzoF.htm)|Detect Alignment (At will) (Good only)|
|[iDFRh2jZ2ogL63IM.htm](pathfinder-bestiary-2-items/iDFRh2jZ2ogL63IM.htm)|Dimension Door (At Will)|
|[IGYasXoP7pT26KvN.htm](pathfinder-bestiary-2-items/IGYasXoP7pT26KvN.htm)|Shape Stone (At will)|
|[IHpzMqwVP4Qwisa6.htm](pathfinder-bestiary-2-items/IHpzMqwVP4Qwisa6.htm)|Sunburst (At will)|
|[iHz93nPS055VlpTQ.htm](pathfinder-bestiary-2-items/iHz93nPS055VlpTQ.htm)|+1 Crossbow|+1|
|[IlW1FymNgiZZvNAV.htm](pathfinder-bestiary-2-items/IlW1FymNgiZZvNAV.htm)|Illusory Disguise (At will)|
|[ippiazWAmHg5l3F6.htm](pathfinder-bestiary-2-items/ippiazWAmHg5l3F6.htm)|+1 Striking Bastard Sword|+1,striking|
|[iQ847whFi4kDiEDu.htm](pathfinder-bestiary-2-items/iQ847whFi4kDiEDu.htm)|Detect Alignment (Constant) (Evil only)|
|[iQNp3D21Ql8PiDZA.htm](pathfinder-bestiary-2-items/iQNp3D21Ql8PiDZA.htm)|+3 Major Striking Greatsword|+3,majorStriking|
|[ISGa39OKNaOBS5Rb.htm](pathfinder-bestiary-2-items/ISGa39OKNaOBS5Rb.htm)|Tangling Creepers (At will)|
|[IV7eERHkT9RjZVgM.htm](pathfinder-bestiary-2-items/IV7eERHkT9RjZVgM.htm)|Speak with Animals (Constant)|
|[ivWNCekJdUWL913h.htm](pathfinder-bestiary-2-items/ivWNCekJdUWL913h.htm)|Invisibility (At Will) (Self Only)|
|[iznQySJVx9S3aKAQ.htm](pathfinder-bestiary-2-items/iznQySJVx9S3aKAQ.htm)|+2 Striking Katana|+2,striking|
|[J5vFJlvjFoW7YiFc.htm](pathfinder-bestiary-2-items/J5vFJlvjFoW7YiFc.htm)|+1 Striking Spiked Chain|+1,striking|
|[J8gjFlA8Gb7zM2ZT.htm](pathfinder-bestiary-2-items/J8gjFlA8Gb7zM2ZT.htm)|Possession (range touch)|
|[jF73rxKIbYkxXiUW.htm](pathfinder-bestiary-2-items/jF73rxKIbYkxXiUW.htm)|+1 Striking Bo Staff|+1,striking|
|[JLbHt0LM9WGhFhGj.htm](pathfinder-bestiary-2-items/JLbHt0LM9WGhFhGj.htm)|Summon Animal (Swarm creatures only)|
|[jRw5SkXrC7GWOpXb.htm](pathfinder-bestiary-2-items/jRw5SkXrC7GWOpXb.htm)|Remove Fear (At will)|
|[jw75q6Yh45aJWTbt.htm](pathfinder-bestiary-2-items/jw75q6Yh45aJWTbt.htm)|Tongues (Constant)|
|[jZget5HVHnwqjGr2.htm](pathfinder-bestiary-2-items/jZget5HVHnwqjGr2.htm)|Ventriloquism (At will)|
|[jZN7KNxo55eAkwom.htm](pathfinder-bestiary-2-items/jZN7KNxo55eAkwom.htm)|Mirror Image (At will)|
|[k4Emo2tqftGwEdIo.htm](pathfinder-bestiary-2-items/k4Emo2tqftGwEdIo.htm)|+1 Resilient Breastplate|
|[K5z1UHEznk6ZV8GU.htm](pathfinder-bestiary-2-items/K5z1UHEznk6ZV8GU.htm)|Dimension Door (At will)|
|[k8jXY7QubcoSOAYR.htm](pathfinder-bestiary-2-items/k8jXY7QubcoSOAYR.htm)|Freedom of Movement (Constant)|
|[KceVAfrBOyOgCGUj.htm](pathfinder-bestiary-2-items/KceVAfrBOyOgCGUj.htm)|Dimension Door (At will)|
|[kfWXgpb3mBY8x5bX.htm](pathfinder-bestiary-2-items/kfWXgpb3mBY8x5bX.htm)|Ethereal Jaunt (At will)|
|[KJp32QDhePr24crV.htm](pathfinder-bestiary-2-items/KJp32QDhePr24crV.htm)|+2 Striking Bastard Sword|+2,striking|
|[KRlaXOK9VXp9mlCh.htm](pathfinder-bestiary-2-items/KRlaXOK9VXp9mlCh.htm)|+2 Greater Striking Starknife|+2,greaterStriking|
|[kxdTx8XfluDNDeWc.htm](pathfinder-bestiary-2-items/kxdTx8XfluDNDeWc.htm)|Dimension Door (At will)|
|[LdMlE7uRJEkPdKUA.htm](pathfinder-bestiary-2-items/LdMlE7uRJEkPdKUA.htm)|Shadow Walk (At will)|
|[LFMYOAZVijC6GmzA.htm](pathfinder-bestiary-2-items/LFMYOAZVijC6GmzA.htm)|Dimension Door (At will)|
|[LHtq8h9QsdHppn66.htm](pathfinder-bestiary-2-items/LHtq8h9QsdHppn66.htm)|Dimension Door (At will)|
|[lIhRNM5msYgcZJCL.htm](pathfinder-bestiary-2-items/lIhRNM5msYgcZJCL.htm)|True Seeing (Constant)|
|[lPHvBGYu9z42HqQj.htm](pathfinder-bestiary-2-items/lPHvBGYu9z42HqQj.htm)|Air Walk (Constant)|
|[lQzCQf7gDhv2S88W.htm](pathfinder-bestiary-2-items/lQzCQf7gDhv2S88W.htm)|Invisibility (At Will) (Self Only)|
|[lSIa7PfqOouOg8nP.htm](pathfinder-bestiary-2-items/lSIa7PfqOouOg8nP.htm)|Dispel Magic (At will)|
|[lsRbdvha9BWdhNaL.htm](pathfinder-bestiary-2-items/lsRbdvha9BWdhNaL.htm)|Spiked Chain|+2,greaterStriking,unholy|
|[LvgsOjqlWG5jltps.htm](pathfinder-bestiary-2-items/LvgsOjqlWG5jltps.htm)|Gust of Wind (At Will)|
|[lWOARoBijDLtXC58.htm](pathfinder-bestiary-2-items/lWOARoBijDLtXC58.htm)|Dimension Door (At will)|
|[lzENfKBITeCmLlWo.htm](pathfinder-bestiary-2-items/lzENfKBITeCmLlWo.htm)|Invisibility (At will) (Self only)|
|[ManCGXxwaYWyKQzU.htm](pathfinder-bestiary-2-items/ManCGXxwaYWyKQzU.htm)|Gaseous Form (At will)|
|[mANOW81qmnDzn8ZY.htm](pathfinder-bestiary-2-items/mANOW81qmnDzn8ZY.htm)|+3 Major Striking Longbow|+3,majorStriking|
|[MBviE2DY2Z7xnOS6.htm](pathfinder-bestiary-2-items/MBviE2DY2Z7xnOS6.htm)|Mace|+1,striking|
|[MgMNZTUTynKhqwV7.htm](pathfinder-bestiary-2-items/MgMNZTUTynKhqwV7.htm)|Detect Magic (Constant) (10th)|
|[mjBHvn4bdXCaG65R.htm](pathfinder-bestiary-2-items/mjBHvn4bdXCaG65R.htm)|Dimension Door (At will)|
|[mkhUcjzL4QL7a659.htm](pathfinder-bestiary-2-items/mkhUcjzL4QL7a659.htm)|Shatter (At will)|
|[MKjUJB31R5kiPnhA.htm](pathfinder-bestiary-2-items/MKjUJB31R5kiPnhA.htm)|Tongues (Constant)|
|[mLsKErBN02PEpFEb.htm](pathfinder-bestiary-2-items/mLsKErBN02PEpFEb.htm)|Augury (At will)|
|[moM05QmLZeKsreVi.htm](pathfinder-bestiary-2-items/moM05QmLZeKsreVi.htm)|Detect Alignment (At will) (Good Only)|
|[MPie4KH85kEeFZ7y.htm](pathfinder-bestiary-2-items/MPie4KH85kEeFZ7y.htm)|Feather Fall (Self only)|
|[mRiJSf4IJmyEQ9Rp.htm](pathfinder-bestiary-2-items/mRiJSf4IJmyEQ9Rp.htm)|Fish Hook|
|[mxpkzQTw0tzSwaUL.htm](pathfinder-bestiary-2-items/mxpkzQTw0tzSwaUL.htm)|Dimension Door (At Will)|
|[mzVH7SLTrdxDaZ3j.htm](pathfinder-bestiary-2-items/mzVH7SLTrdxDaZ3j.htm)|True Seeing (Constant)|
|[NjjDQDdWwJEhS8MF.htm](pathfinder-bestiary-2-items/NjjDQDdWwJEhS8MF.htm)|Dimensional Anchor (At will)|
|[NPRORdlBK4J6mXJz.htm](pathfinder-bestiary-2-items/NPRORdlBK4J6mXJz.htm)|Plane Shift (Self only) (To the Material Plane or Shadow Plane only)|
|[NS4Sroct6rpeddeR.htm](pathfinder-bestiary-2-items/NS4Sroct6rpeddeR.htm)|Acid Arrow (At will)|
|[nv8kWBvEcv3diNEd.htm](pathfinder-bestiary-2-items/nv8kWBvEcv3diNEd.htm)|Detect Alignment (At will) (Lawful only)|
|[nzHApOLkuTxQ7ZED.htm](pathfinder-bestiary-2-items/nzHApOLkuTxQ7ZED.htm)|Air Walk (Constant)|
|[O09US7v1AHXJRsXA.htm](pathfinder-bestiary-2-items/O09US7v1AHXJRsXA.htm)|Dimension Door (At will)|
|[OFeWf6HdNT5mBfJj.htm](pathfinder-bestiary-2-items/OFeWf6HdNT5mBfJj.htm)|Freedom of Movement (Constant)|
|[ofun5QHmnppWzdgV.htm](pathfinder-bestiary-2-items/ofun5QHmnppWzdgV.htm)|Remove Fear (At will)|
|[oIJY4ycXpaF4IjIM.htm](pathfinder-bestiary-2-items/oIJY4ycXpaF4IjIM.htm)|See Invisibility (Constant)|
|[OlEMHzm2vPkpFx3l.htm](pathfinder-bestiary-2-items/OlEMHzm2vPkpFx3l.htm)|Burning Hands (At Will)|
|[olvPYkQEIgtlun6u.htm](pathfinder-bestiary-2-items/olvPYkQEIgtlun6u.htm)|Plane Shift (Self only) (To Shadow Plane only)|
|[OMt6ba5mjMd0d6WH.htm](pathfinder-bestiary-2-items/OMt6ba5mjMd0d6WH.htm)|Detect Alignment (At will) (Evil only)|
|[ot1UCPxTvpQLgwz3.htm](pathfinder-bestiary-2-items/ot1UCPxTvpQLgwz3.htm)|Detect Alignment (At will) (Good only)|
|[OthOWZLzsugOgJdj.htm](pathfinder-bestiary-2-items/OthOWZLzsugOgJdj.htm)|Darkness (At will)|
|[pIbaFSljYvozaZdG.htm](pathfinder-bestiary-2-items/pIbaFSljYvozaZdG.htm)|Tambourine (Handheld Musical Instrument)|
|[pLU70j6ysOj7eVT8.htm](pathfinder-bestiary-2-items/pLU70j6ysOj7eVT8.htm)|Air Walk (Constant)|
|[PvAhmTacSiuheD52.htm](pathfinder-bestiary-2-items/PvAhmTacSiuheD52.htm)|Dimension Door (At will)|
|[pX8XlXGbLapNVOQT.htm](pathfinder-bestiary-2-items/pX8XlXGbLapNVOQT.htm)|Freedom of Movement (Constant)|
|[pyDtidglBpduEzvf.htm](pathfinder-bestiary-2-items/pyDtidglBpduEzvf.htm)|Read Omens (At will)|
|[pZNjrGx8hawqDqpa.htm](pathfinder-bestiary-2-items/pZNjrGx8hawqDqpa.htm)|True Seeing (Constant)|
|[Q3rKZnaS5mmSsg1J.htm](pathfinder-bestiary-2-items/Q3rKZnaS5mmSsg1J.htm)|Detect Poison (At will)|
|[Q61EcGc9xdoDN1Kb.htm](pathfinder-bestiary-2-items/Q61EcGc9xdoDN1Kb.htm)|Speak with Plants (Constant)|
|[QFVCFmJDRYL0iTIf.htm](pathfinder-bestiary-2-items/QFVCFmJDRYL0iTIf.htm)|Air Walk (Constant)|
|[qgSdrZvWfACQ129g.htm](pathfinder-bestiary-2-items/qgSdrZvWfACQ129g.htm)|True Seeing (Constant)|
|[QRGAhXT66Xuc32WM.htm](pathfinder-bestiary-2-items/QRGAhXT66Xuc32WM.htm)|Invisibility (Self only)|
|[qT91ezbxbLydVEdW.htm](pathfinder-bestiary-2-items/qT91ezbxbLydVEdW.htm)|Tongues (Constant)|
|[QTK9hewzSwMZUHHY.htm](pathfinder-bestiary-2-items/QTK9hewzSwMZUHHY.htm)|Invisibility (Self only)|
|[qUW0IwnKj9NAkPZu.htm](pathfinder-bestiary-2-items/qUW0IwnKj9NAkPZu.htm)|Control Weather (Doesn't require secondary casters)|
|[QX2KJ9zKwYbtaPba.htm](pathfinder-bestiary-2-items/QX2KJ9zKwYbtaPba.htm)|Invisibility (At will) (Self only)|
|[qxGCsUCI43v3qoRC.htm](pathfinder-bestiary-2-items/qxGCsUCI43v3qoRC.htm)|Primal Phenomenon (Once per year)|
|[qZDyd8KGBqXEYFHU.htm](pathfinder-bestiary-2-items/qZDyd8KGBqXEYFHU.htm)|Lesser Elixir of Life|
|[qZpXsMwmncbpw5Dw.htm](pathfinder-bestiary-2-items/qZpXsMwmncbpw5Dw.htm)|Invisibility (At will)|
|[R1yZI6IRZc1d6a6H.htm](pathfinder-bestiary-2-items/R1yZI6IRZc1d6a6H.htm)|Invisibility (At will) (Self only)|
|[r9Vti0pP2In1fPgO.htm](pathfinder-bestiary-2-items/r9Vti0pP2In1fPgO.htm)|Tongues (Constant)|
|[rChIZqOgM2sc4XDf.htm](pathfinder-bestiary-2-items/rChIZqOgM2sc4XDf.htm)|Dimensional Anchor (At will)|
|[rcWb1X7tL04A2SNN.htm](pathfinder-bestiary-2-items/rcWb1X7tL04A2SNN.htm)|Darkness (At Will)|
|[rEHDvZqglITeLKYB.htm](pathfinder-bestiary-2-items/rEHDvZqglITeLKYB.htm)|Pass Without Trace (Constant)|
|[rijZTMFR5Ee6MsQm.htm](pathfinder-bestiary-2-items/rijZTMFR5Ee6MsQm.htm)|Plane Shift (To Ethereal Plane or Material Plane only) (Self only)|
|[rJ7AdRxufg0jAASh.htm](pathfinder-bestiary-2-items/rJ7AdRxufg0jAASh.htm)|Gust of Wind (At Will)|
|[RM8PNh1GNliHv6hz.htm](pathfinder-bestiary-2-items/RM8PNh1GNliHv6hz.htm)|Tongues (Constant)|
|[RmX8OdrO08JM0AmG.htm](pathfinder-bestiary-2-items/RmX8OdrO08JM0AmG.htm)|Wall of Fire (At Will)|
|[RojqXvZ5IxWWuBv7.htm](pathfinder-bestiary-2-items/RojqXvZ5IxWWuBv7.htm)|Dimension Door (At Will)|
|[RTunxYN9r4ztOeQC.htm](pathfinder-bestiary-2-items/RTunxYN9r4ztOeQC.htm)|Plane Shift (To Material Plane or Shadow Plane only)|
|[sCQvzLM4bDnpiOMd.htm](pathfinder-bestiary-2-items/sCQvzLM4bDnpiOMd.htm)|True Seeing (Constant)|
|[SFLxOb7tFxjJz3mj.htm](pathfinder-bestiary-2-items/SFLxOb7tFxjJz3mj.htm)|Mind Reading (At will)|
|[sgAI2bN5p9QHXare.htm](pathfinder-bestiary-2-items/sgAI2bN5p9QHXare.htm)|Invisibility (At will) (Self only)|
|[sinEXpm7xMjuylIj.htm](pathfinder-bestiary-2-items/sinEXpm7xMjuylIj.htm)|Detect Magic (Constant)|
|[SOdblsm3svF3C9dl.htm](pathfinder-bestiary-2-items/SOdblsm3svF3C9dl.htm)|Dimension Door (At Will)|
|[SwC3pb24zjBolDvy.htm](pathfinder-bestiary-2-items/SwC3pb24zjBolDvy.htm)|Gust of Wind (At Will)|
|[SZFg9OyW2ROGepvG.htm](pathfinder-bestiary-2-items/SZFg9OyW2ROGepvG.htm)|Dimension Door (At will)|
|[T1jTn0jRuZqV93CM.htm](pathfinder-bestiary-2-items/T1jTn0jRuZqV93CM.htm)|Baleful Polymorph (At will)|
|[T4ROoWQsVb4LvmqB.htm](pathfinder-bestiary-2-items/T4ROoWQsVb4LvmqB.htm)|Mind Probe (At will)|
|[TanTXAIbWmvfNYpc.htm](pathfinder-bestiary-2-items/TanTXAIbWmvfNYpc.htm)|Tongues (Constant)|
|[TgdLSsUbxoCtvEgV.htm](pathfinder-bestiary-2-items/TgdLSsUbxoCtvEgV.htm)|+1 Scythe|+1|
|[tLyD1GZ9Oz7JtF5e.htm](pathfinder-bestiary-2-items/tLyD1GZ9Oz7JtF5e.htm)|Dimension Door (Only when in bright light, and only to an area in bright light)|
|[tN60VOoNQEIM0XvA.htm](pathfinder-bestiary-2-items/tN60VOoNQEIM0XvA.htm)|Longspear|+1,striking|
|[tOHJkoP70gzyQa0q.htm](pathfinder-bestiary-2-items/tOHJkoP70gzyQa0q.htm)|Bastard Sword|+1,striking|
|[toXHeAIIzSZkh0pV.htm](pathfinder-bestiary-2-items/toXHeAIIzSZkh0pV.htm)|Detect Alignment (At will) (Good only)|
|[TSzAkKreLHo2kCCD.htm](pathfinder-bestiary-2-items/TSzAkKreLHo2kCCD.htm)|Virtuoso Trumpet|
|[TYl58bz88u92DVdX.htm](pathfinder-bestiary-2-items/TYl58bz88u92DVdX.htm)|Longspear|+1|
|[TzbvlBarJ1dDLVDB.htm](pathfinder-bestiary-2-items/TzbvlBarJ1dDLVDB.htm)|+1 Striking Staff|+1,striking|
|[u3M9UJPCkvUJAvTB.htm](pathfinder-bestiary-2-items/u3M9UJPCkvUJAvTB.htm)|Dispel Magic (At will)|
|[uBJYf08dJnWbAhyX.htm](pathfinder-bestiary-2-items/uBJYf08dJnWbAhyX.htm)|True Seeing (Constant)|
|[UEPKPYWJrNSP0Jqk.htm](pathfinder-bestiary-2-items/UEPKPYWJrNSP0Jqk.htm)|Shell Armor (Hide)|
|[Um9w32ZfDOZ1WfA3.htm](pathfinder-bestiary-2-items/Um9w32ZfDOZ1WfA3.htm)|Tree Stride (At will)|
|[Ump4s3SQmPTp8c0K.htm](pathfinder-bestiary-2-items/Ump4s3SQmPTp8c0K.htm)|Wall of Fire (At Will)|
|[UpvwE0gzoIhEpwom.htm](pathfinder-bestiary-2-items/UpvwE0gzoIhEpwom.htm)|+2 Greater Striking Glaive|+2,greaterStriking|
|[V2Qq9JcpjIeBP5L4.htm](pathfinder-bestiary-2-items/V2Qq9JcpjIeBP5L4.htm)|Composite Longbow|+1|
|[v3MNBWrVRN9SQv9V.htm](pathfinder-bestiary-2-items/v3MNBWrVRN9SQv9V.htm)|Water Walk (Constant)|
|[v4FHOiOqxoo52YTS.htm](pathfinder-bestiary-2-items/v4FHOiOqxoo52YTS.htm)|True Seeing (Constant)|
|[VFViUEQww0BRVE03.htm](pathfinder-bestiary-2-items/VFViUEQww0BRVE03.htm)|Shape Stone (At will)|
|[VgQyT5O8X59xTXWs.htm](pathfinder-bestiary-2-items/VgQyT5O8X59xTXWs.htm)|Summon Elemental (Water Elementals only)|
|[VjrbFMGP6O3PLlyp.htm](pathfinder-bestiary-2-items/VjrbFMGP6O3PLlyp.htm)|Tongues (Constant)|
|[VpnCsS6gaS1m1Imz.htm](pathfinder-bestiary-2-items/VpnCsS6gaS1m1Imz.htm)|Tongues (Constant)|
|[vQBJ6UOCVxfLLwKL.htm](pathfinder-bestiary-2-items/vQBJ6UOCVxfLLwKL.htm)|Control Water (At will)|
|[Vz2xdhsoi4Vg6Gej.htm](pathfinder-bestiary-2-items/Vz2xdhsoi4Vg6Gej.htm)|True Seeing (Constant)|
|[wbilAnBHlo9UFygH.htm](pathfinder-bestiary-2-items/wbilAnBHlo9UFygH.htm)|True Seeing (Constant)|
|[wFwYkevdZGRjwibC.htm](pathfinder-bestiary-2-items/wFwYkevdZGRjwibC.htm)|Charm (plant creatures only)|
|[Wj4PwJFZfcVXdLml.htm](pathfinder-bestiary-2-items/Wj4PwJFZfcVXdLml.htm)|Mind Reading (At will)|
|[wJAzn02M6Mv8HeZj.htm](pathfinder-bestiary-2-items/wJAzn02M6Mv8HeZj.htm)|Levitate (At will) (Self only)|
|[wJkawABLUtrnlz3Y.htm](pathfinder-bestiary-2-items/wJkawABLUtrnlz3Y.htm)|+1 Ranseur|+1|
|[X8HPyH2dWuDAiYT3.htm](pathfinder-bestiary-2-items/X8HPyH2dWuDAiYT3.htm)|Speak with Plants (Constant)|
|[x9WaNwW8LODf3mt3.htm](pathfinder-bestiary-2-items/x9WaNwW8LODf3mt3.htm)|Tongues (Constant)|
|[xCOiURbOFmMct0Ad.htm](pathfinder-bestiary-2-items/xCOiURbOFmMct0Ad.htm)|Detect Alignment (Constant) (All Alignments Simultaneously)|
|[xm7v6OJXM7xTHseI.htm](pathfinder-bestiary-2-items/xm7v6OJXM7xTHseI.htm)|Invisibility (At will) (Self only)|
|[xrKfhlv0heNZjJQl.htm](pathfinder-bestiary-2-items/xrKfhlv0heNZjJQl.htm)|+1 Morningstar|+1|
|[XSu8EpdK6CVQXvZe.htm](pathfinder-bestiary-2-items/XSu8EpdK6CVQXvZe.htm)|True Seeing (Constant)|
|[xuzZonbgefgsArAz.htm](pathfinder-bestiary-2-items/xuzZonbgefgsArAz.htm)|Summon Entity (Will-o'-Wisp Only)|
|[xWKBg0GE28Tpu1bC.htm](pathfinder-bestiary-2-items/xWKBg0GE28Tpu1bC.htm)|Plane Shift (At will)|
|[y12tfuxEBFbR9qgU.htm](pathfinder-bestiary-2-items/y12tfuxEBFbR9qgU.htm)|Tongues (Constant)|
|[y1vZf9cXWJAE7Ig5.htm](pathfinder-bestiary-2-items/y1vZf9cXWJAE7Ig5.htm)|Ventriloquism (At will)|
|[y7xDGYy2BAsv28Gu.htm](pathfinder-bestiary-2-items/y7xDGYy2BAsv28Gu.htm)|Freedom of Movement (Constant)|
|[y8TePj5x2rTRtyMK.htm](pathfinder-bestiary-2-items/y8TePj5x2rTRtyMK.htm)|Dimension Door (At will)|
|[YBf7cE1gXS2MxYx2.htm](pathfinder-bestiary-2-items/YBf7cE1gXS2MxYx2.htm)|Greatsword|+2,greaterStriking|
|[ybPZgyruroPBZ9gc.htm](pathfinder-bestiary-2-items/ybPZgyruroPBZ9gc.htm)|Tongues (Constant)|
|[ybZoLDhRr9Dy4RFI.htm](pathfinder-bestiary-2-items/ybZoLDhRr9Dy4RFI.htm)|Unseen Servant (At will)|
|[yfqMmmrCpEGsye6G.htm](pathfinder-bestiary-2-items/yfqMmmrCpEGsye6G.htm)|Dimension Door (At will)|
|[ygKRMmKOt3k4RrRs.htm](pathfinder-bestiary-2-items/ygKRMmKOt3k4RrRs.htm)|Spellwrack (At will)|
|[yhlcSZF88RwErXcb.htm](pathfinder-bestiary-2-items/yhlcSZF88RwErXcb.htm)|Divine Aura (Chaotic only)|
|[ysmXZKOimV2eOJdk.htm](pathfinder-bestiary-2-items/ysmXZKOimV2eOJdk.htm)|See Invisibility (Constant)|
|[YtRTdI3UAqH6vZVL.htm](pathfinder-bestiary-2-items/YtRTdI3UAqH6vZVL.htm)|Elemental Form (Water only)|
|[YVBJxmLTHes9asum.htm](pathfinder-bestiary-2-items/YVBJxmLTHes9asum.htm)|Invisibility (At will) (Self only)|
|[z0tSvLqh0IK6Y4TT.htm](pathfinder-bestiary-2-items/z0tSvLqh0IK6Y4TT.htm)|Floating Disk (At will)|
|[Z9Qm5DGAbFw04kMI.htm](pathfinder-bestiary-2-items/Z9Qm5DGAbFw04kMI.htm)|Dimension Door (At will)|
|[ZbNsXG0791f7GcKW.htm](pathfinder-bestiary-2-items/ZbNsXG0791f7GcKW.htm)|+2 Greater Striking Longspear|+2,greaterStriking|
|[zD1p5wo8W7il4Xw4.htm](pathfinder-bestiary-2-items/zD1p5wo8W7il4Xw4.htm)|Tongues (Constant)|
|[zkvNDt3HOWSmCOid.htm](pathfinder-bestiary-2-items/zkvNDt3HOWSmCOid.htm)|+1 Striking Gaff|
|[zmdWFf8TYR11NSvB.htm](pathfinder-bestiary-2-items/zmdWFf8TYR11NSvB.htm)|Illusory Object (At will)|
|[zmGi1QNStSaYC6ai.htm](pathfinder-bestiary-2-items/zmGi1QNStSaYC6ai.htm)|True Seeing (Constant)|
|[zpCGLXqUH07afHBV.htm](pathfinder-bestiary-2-items/zpCGLXqUH07afHBV.htm)|Wall of Wind (At Will)|
|[zpkYVcLlyvOVqOdx.htm](pathfinder-bestiary-2-items/zpkYVcLlyvOVqOdx.htm)|Telekinetic Haul (At will)|
|[zSdkC5bHVECvhK79.htm](pathfinder-bestiary-2-items/zSdkC5bHVECvhK79.htm)|Dispel Magic (At will)|
|[zSJNdELBxJk00I4X.htm](pathfinder-bestiary-2-items/zSJNdELBxJk00I4X.htm)|Darkness (At Will)|
|[ZwTQYhVwvbqvzaHX.htm](pathfinder-bestiary-2-items/ZwTQYhVwvbqvzaHX.htm)|Air Walk (Constant)|
|[ZXR3Dqh3vpkb4MJB.htm](pathfinder-bestiary-2-items/ZXR3Dqh3vpkb4MJB.htm)|Enthrall (At will)|
|[ZYBGyu05L6Ve0V1r.htm](pathfinder-bestiary-2-items/ZYBGyu05L6Ve0V1r.htm)|Shadow Siphon (At will)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00kCy3dtoRU8EnP9.htm](pathfinder-bestiary-2-items/00kCy3dtoRU8EnP9.htm)|Longspear|Longspear|modificada|
|[00zSyT0a2tdDQnmZ.htm](pathfinder-bestiary-2-items/00zSyT0a2tdDQnmZ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[03MMQCPuK2UJR1Z6.htm](pathfinder-bestiary-2-items/03MMQCPuK2UJR1Z6.htm)|Graft Flesh|Graft Flesh|modificada|
|[05N8sEltz4b9htCX.htm](pathfinder-bestiary-2-items/05N8sEltz4b9htCX.htm)|Dagger|Daga|modificada|
|[072UUA8WXrWV6tPc.htm](pathfinder-bestiary-2-items/072UUA8WXrWV6tPc.htm)|Horn|Cuerno|modificada|
|[090u49BTBz9TdQi7.htm](pathfinder-bestiary-2-items/090u49BTBz9TdQi7.htm)|Regeneration 15 (deactivated by cold iron)|Regeneración 15 (desactivado por hierro frío)|modificada|
|[0a9bxpI79rMpqyXh.htm](pathfinder-bestiary-2-items/0a9bxpI79rMpqyXh.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[0BVpj3wkSrFlhgdK.htm](pathfinder-bestiary-2-items/0BVpj3wkSrFlhgdK.htm)|Agile Swimmer|Agile Swimmer|modificada|
|[0D7DkzCzV4iv9Q0r.htm](pathfinder-bestiary-2-items/0D7DkzCzV4iv9Q0r.htm)|Grab|Agarrado|modificada|
|[0DDWtKSDSWdXnkgj.htm](pathfinder-bestiary-2-items/0DDWtKSDSWdXnkgj.htm)|Bloodfire Fever|Fiebre de fuego de sangre|modificada|
|[0dNqhk5KI3nAaTlu.htm](pathfinder-bestiary-2-items/0dNqhk5KI3nAaTlu.htm)|Demon Hunter|Cazador de demoniosíaco|modificada|
|[0DOPtcRBzrEKku1I.htm](pathfinder-bestiary-2-items/0DOPtcRBzrEKku1I.htm)|Cloud Walk|Cloud Walk|modificada|
|[0DR36G32rpPd9PIN.htm](pathfinder-bestiary-2-items/0DR36G32rpPd9PIN.htm)|Whispered Despair|Desesperación Susurrada|modificada|
|[0dTEwPV9TXqDzFno.htm](pathfinder-bestiary-2-items/0dTEwPV9TXqDzFno.htm)|Soulsense|Sentido de las almas|modificada|
|[0eRwevoxA5zdvqb9.htm](pathfinder-bestiary-2-items/0eRwevoxA5zdvqb9.htm)|Breath Weapon|Breath Weapon|modificada|
|[0F96cEU48FezVgKB.htm](pathfinder-bestiary-2-items/0F96cEU48FezVgKB.htm)|Bite|Muerdemuerde|modificada|
|[0FwbXedAmkMCm8OR.htm](pathfinder-bestiary-2-items/0FwbXedAmkMCm8OR.htm)|Tremorsense 30 feet|Tremorsense 30 pies|modificada|
|[0G6ZKuqCpYAAIPki.htm](pathfinder-bestiary-2-items/0G6ZKuqCpYAAIPki.htm)|Constant Spells|Constant Spells|modificada|
|[0gNZbOZYyi010Pow.htm](pathfinder-bestiary-2-items/0gNZbOZYyi010Pow.htm)|Curse of Stolen Breath|Maldición del aliento robado|modificada|
|[0hkOF2WHkLhHAEjQ.htm](pathfinder-bestiary-2-items/0hkOF2WHkLhHAEjQ.htm)|Descend on a Web|Descender por una telara|modificada|
|[0IEitR672ZSYf8Jn.htm](pathfinder-bestiary-2-items/0IEitR672ZSYf8Jn.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[0IfzoP16zRxLRIJ3.htm](pathfinder-bestiary-2-items/0IfzoP16zRxLRIJ3.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0IwQxSLxEqwPcB3q.htm](pathfinder-bestiary-2-items/0IwQxSLxEqwPcB3q.htm)|Steam Vision|Visión de Vapor|modificada|
|[0jpjkgun2zt97YLL.htm](pathfinder-bestiary-2-items/0jpjkgun2zt97YLL.htm)|Deceptive Reposition|Reposicionamiento engaso.|modificada|
|[0llDJD2BXJrr95lm.htm](pathfinder-bestiary-2-items/0llDJD2BXJrr95lm.htm)|Smoke Form|Forma de Humo|modificada|
|[0MEzU0e7MJtwV7WA.htm](pathfinder-bestiary-2-items/0MEzU0e7MJtwV7WA.htm)|Truespeech|Truespeech|modificada|
|[0mzc60K0kDFbeLL4.htm](pathfinder-bestiary-2-items/0mzc60K0kDFbeLL4.htm)|Fast Swallow|Tragar de golpe|modificada|
|[0NKzpP896UuTepW0.htm](pathfinder-bestiary-2-items/0NKzpP896UuTepW0.htm)|Jaws|Fauces|modificada|
|[0Nlywj3cWzzzNsFd.htm](pathfinder-bestiary-2-items/0Nlywj3cWzzzNsFd.htm)|Bastard Sword|Espada Bastarda|modificada|
|[0Q0S3Hd1xOjt3Ked.htm](pathfinder-bestiary-2-items/0Q0S3Hd1xOjt3Ked.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[0q3s792vZI0wANuc.htm](pathfinder-bestiary-2-items/0q3s792vZI0wANuc.htm)|Tentacle|Tentáculo|modificada|
|[0QCPtkVtKzuuVVaK.htm](pathfinder-bestiary-2-items/0QCPtkVtKzuuVVaK.htm)|Bladed Limb|Bladed Limb|modificada|
|[0s0zoTqBFyl1ZTjC.htm](pathfinder-bestiary-2-items/0s0zoTqBFyl1ZTjC.htm)|Frightful Presence|Frightful Presence|modificada|
|[0s7Elyqvdpp0ck2s.htm](pathfinder-bestiary-2-items/0s7Elyqvdpp0ck2s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0SmjbTrLS51rwGcc.htm](pathfinder-bestiary-2-items/0SmjbTrLS51rwGcc.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[0t8zkWyTQVqJBcMG.htm](pathfinder-bestiary-2-items/0t8zkWyTQVqJBcMG.htm)|Impaling Push|Empellón empalador|modificada|
|[0TCobRYKQCosWffG.htm](pathfinder-bestiary-2-items/0TCobRYKQCosWffG.htm)|Claw|Garra|modificada|
|[0ToPGrofmnFDeqOe.htm](pathfinder-bestiary-2-items/0ToPGrofmnFDeqOe.htm)|Slithering Attack|Slithering Attack|modificada|
|[0UKsJtM3fEth21iR.htm](pathfinder-bestiary-2-items/0UKsJtM3fEth21iR.htm)|Lifesense|Lifesense|modificada|
|[0USgUg4kKiE4rcKV.htm](pathfinder-bestiary-2-items/0USgUg4kKiE4rcKV.htm)|Club|Club|modificada|
|[0uU20iNCnRv4spmg.htm](pathfinder-bestiary-2-items/0uU20iNCnRv4spmg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0uYLHfv1lvXE4qG4.htm](pathfinder-bestiary-2-items/0uYLHfv1lvXE4qG4.htm)|Constant Spells|Constant Spells|modificada|
|[0v7H9KCVbUUdkdU7.htm](pathfinder-bestiary-2-items/0v7H9KCVbUUdkdU7.htm)|Cloud Form|Cloud Form|modificada|
|[0vByzvft4IIwjZh2.htm](pathfinder-bestiary-2-items/0vByzvft4IIwjZh2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0VNX7MbrCqRSKYkE.htm](pathfinder-bestiary-2-items/0VNX7MbrCqRSKYkE.htm)|Shadow Evade|Sombra Evade|modificada|
|[0vrX1z0w3GFCktsF.htm](pathfinder-bestiary-2-items/0vrX1z0w3GFCktsF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0wGgpxEStAnmfi0k.htm](pathfinder-bestiary-2-items/0wGgpxEStAnmfi0k.htm)|Foot|Pie|modificada|
|[0xGnS3VP3WvvpnSI.htm](pathfinder-bestiary-2-items/0xGnS3VP3WvvpnSI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0XjG2HkUAja2e7pE.htm](pathfinder-bestiary-2-items/0XjG2HkUAja2e7pE.htm)|Jaws|Fauces|modificada|
|[0xLF0LRnkscCWi8p.htm](pathfinder-bestiary-2-items/0xLF0LRnkscCWi8p.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[0Y47YuHl29LzryME.htm](pathfinder-bestiary-2-items/0Y47YuHl29LzryME.htm)|Trunk|Tronco|modificada|
|[0zl5InxQA7HuiemZ.htm](pathfinder-bestiary-2-items/0zl5InxQA7HuiemZ.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[109W7aPnwhNHkrHE.htm](pathfinder-bestiary-2-items/109W7aPnwhNHkrHE.htm)|Claw|Garra|modificada|
|[12e8csvZLJdg2OS8.htm](pathfinder-bestiary-2-items/12e8csvZLJdg2OS8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[19Ayra6rX2M49iPG.htm](pathfinder-bestiary-2-items/19Ayra6rX2M49iPG.htm)|Feed on Blood|Alimentarse de sangre|modificada|
|[19bztZWgNzgUNmTs.htm](pathfinder-bestiary-2-items/19bztZWgNzgUNmTs.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[1A17VJOGWG0BFypA.htm](pathfinder-bestiary-2-items/1A17VJOGWG0BFypA.htm)|Club|Club|modificada|
|[1abnSJGdHEvG6Lyl.htm](pathfinder-bestiary-2-items/1abnSJGdHEvG6Lyl.htm)|Bramble Jump|Salto de Zarza|modificada|
|[1BBbgSS3coktFqQA.htm](pathfinder-bestiary-2-items/1BBbgSS3coktFqQA.htm)|Constant Spells|Constant Spells|modificada|
|[1BD5jRohvhbGdDwq.htm](pathfinder-bestiary-2-items/1BD5jRohvhbGdDwq.htm)|Fangs|Colmillos|modificada|
|[1BrHXN7JmOnmyeOk.htm](pathfinder-bestiary-2-items/1BrHXN7JmOnmyeOk.htm)|Grab|Agarrado|modificada|
|[1ChXT4DL7N5JhHEq.htm](pathfinder-bestiary-2-items/1ChXT4DL7N5JhHEq.htm)|Tail|Tail|modificada|
|[1deHNHPL9laFtZgR.htm](pathfinder-bestiary-2-items/1deHNHPL9laFtZgR.htm)|Tremorsense|Tremorsense|modificada|
|[1DfgmZUiWL8m4tiP.htm](pathfinder-bestiary-2-items/1DfgmZUiWL8m4tiP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[1dUOZTn32vZg4Reo.htm](pathfinder-bestiary-2-items/1dUOZTn32vZg4Reo.htm)|Jaws|Fauces|modificada|
|[1eb07HNvWyOOELI3.htm](pathfinder-bestiary-2-items/1eb07HNvWyOOELI3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1ENGsx3BESdgdHnk.htm](pathfinder-bestiary-2-items/1ENGsx3BESdgdHnk.htm)|Lightning Drinker|Lightning Drinker|modificada|
|[1FR8UtVfDAC3RoWh.htm](pathfinder-bestiary-2-items/1FR8UtVfDAC3RoWh.htm)|Mortasheen|Mortasheen|modificada|
|[1fTRsJAGDuFgPeXF.htm](pathfinder-bestiary-2-items/1fTRsJAGDuFgPeXF.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[1gwUpyZY3w0LloLq.htm](pathfinder-bestiary-2-items/1gwUpyZY3w0LloLq.htm)|Devil's Howl|Aullido del Diablo|modificada|
|[1IvCoMHs0qGTlw8J.htm](pathfinder-bestiary-2-items/1IvCoMHs0qGTlw8J.htm)|Ripping Gaze|Mirada desgarradora|modificada|
|[1jqzGUINJJfVCKfu.htm](pathfinder-bestiary-2-items/1jqzGUINJJfVCKfu.htm)|Smoke Vision|Visión de Humo|modificada|
|[1Kmvtn64DZibqUls.htm](pathfinder-bestiary-2-items/1Kmvtn64DZibqUls.htm)|Elemental Assault|Asalto elemental|modificada|
|[1LmOy2NMwi2RqSKT.htm](pathfinder-bestiary-2-items/1LmOy2NMwi2RqSKT.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[1lSOq5grKxoZhTWw.htm](pathfinder-bestiary-2-items/1lSOq5grKxoZhTWw.htm)|Constant Spells|Constant Spells|modificada|
|[1lYZ7v9sRWUarCls.htm](pathfinder-bestiary-2-items/1lYZ7v9sRWUarCls.htm)|Wing Deflection|Desvío con el ala|modificada|
|[1M2I2905EVmXay9H.htm](pathfinder-bestiary-2-items/1M2I2905EVmXay9H.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[1MWjpvCpwBy0BFwq.htm](pathfinder-bestiary-2-items/1MWjpvCpwBy0BFwq.htm)|Emperor Cobra Venom|Emperador Cobra Veneno|modificada|
|[1qBTVbPZxVREvpnT.htm](pathfinder-bestiary-2-items/1qBTVbPZxVREvpnT.htm)|Telepathy (Touch only)|Telepatía (solo tacto)|modificada|
|[1RA0WbiyBjX7Iegz.htm](pathfinder-bestiary-2-items/1RA0WbiyBjX7Iegz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1RcwWRNvn63MNFhB.htm](pathfinder-bestiary-2-items/1RcwWRNvn63MNFhB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1Re8gJ0YTH6TxfLa.htm](pathfinder-bestiary-2-items/1Re8gJ0YTH6TxfLa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1RpmI2FY6QNDsOVh.htm](pathfinder-bestiary-2-items/1RpmI2FY6QNDsOVh.htm)|Scent|Scent|modificada|
|[1UzzAnKlRhYDkzDO.htm](pathfinder-bestiary-2-items/1UzzAnKlRhYDkzDO.htm)|Telepathy|Telepatía|modificada|
|[1V1IFVEwiJA8p72B.htm](pathfinder-bestiary-2-items/1V1IFVEwiJA8p72B.htm)|Baleful Glow|Baleful Glow|modificada|
|[1vC7HEUnMXCc5wme.htm](pathfinder-bestiary-2-items/1vC7HEUnMXCc5wme.htm)|Positive Energy Affinity|Afinidad Energética Positiva|modificada|
|[1w9sOvRdSaIEpLNe.htm](pathfinder-bestiary-2-items/1w9sOvRdSaIEpLNe.htm)|Tail|Tail|modificada|
|[1ynGuwCvWvX0EOiE.htm](pathfinder-bestiary-2-items/1ynGuwCvWvX0EOiE.htm)|Frightful Presence|Frightful Presence|modificada|
|[1Z3FZ39brRJH2cIs.htm](pathfinder-bestiary-2-items/1Z3FZ39brRJH2cIs.htm)|Recall Weapon|Recall Weapon|modificada|
|[1zSROTRkSluSvmzR.htm](pathfinder-bestiary-2-items/1zSROTRkSluSvmzR.htm)|Swallow Whole|Engullir Todo|modificada|
|[1zvM86hRPPnGLOEW.htm](pathfinder-bestiary-2-items/1zvM86hRPPnGLOEW.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[20d4bCP4x4uHaNkd.htm](pathfinder-bestiary-2-items/20d4bCP4x4uHaNkd.htm)|Claw|Garra|modificada|
|[223kLr85XBsNkrd0.htm](pathfinder-bestiary-2-items/223kLr85XBsNkrd0.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[27ggnHd24eNbhg9e.htm](pathfinder-bestiary-2-items/27ggnHd24eNbhg9e.htm)|Darkvision|Visión en la oscuridad|modificada|
|[28TZFvjgwHVKS489.htm](pathfinder-bestiary-2-items/28TZFvjgwHVKS489.htm)|Filament|Filamento|modificada|
|[28xyMG8bDPJrcRsP.htm](pathfinder-bestiary-2-items/28xyMG8bDPJrcRsP.htm)|Death-Stealing Gaze|Mirada que roba la muerte|modificada|
|[2a2eONlkTBanM6W0.htm](pathfinder-bestiary-2-items/2a2eONlkTBanM6W0.htm)|Drain Life|Drenar Vida|modificada|
|[2a4l18G9fqQ03iFA.htm](pathfinder-bestiary-2-items/2a4l18G9fqQ03iFA.htm)|Tail|Tail|modificada|
|[2ABnFaydFGtobnZC.htm](pathfinder-bestiary-2-items/2ABnFaydFGtobnZC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2aU81mgG4GTqpajQ.htm](pathfinder-bestiary-2-items/2aU81mgG4GTqpajQ.htm)|Foot|Pie|modificada|
|[2avn7E9oKYvrvNGl.htm](pathfinder-bestiary-2-items/2avn7E9oKYvrvNGl.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[2B1o26m73tHgV6UJ.htm](pathfinder-bestiary-2-items/2B1o26m73tHgV6UJ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[2bevQZIcuccKMxmq.htm](pathfinder-bestiary-2-items/2bevQZIcuccKMxmq.htm)|Jaws|Fauces|modificada|
|[2BHwzsnFNAsPoULp.htm](pathfinder-bestiary-2-items/2BHwzsnFNAsPoULp.htm)|Fangs|Colmillos|modificada|
|[2bthXOLfHVbIarRo.htm](pathfinder-bestiary-2-items/2bthXOLfHVbIarRo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2D3ZN6sbGBpo3oKC.htm](pathfinder-bestiary-2-items/2D3ZN6sbGBpo3oKC.htm)|Scent|Scent|modificada|
|[2dHPDZu3BYN1GauB.htm](pathfinder-bestiary-2-items/2dHPDZu3BYN1GauB.htm)|No Breath|No Breath|modificada|
|[2doPyPIBLoDzjzk9.htm](pathfinder-bestiary-2-items/2doPyPIBLoDzjzk9.htm)|Paralysis|Parálisis|modificada|
|[2ESgkLReH2zyxUIl.htm](pathfinder-bestiary-2-items/2ESgkLReH2zyxUIl.htm)|Pestilential Aura|Aura pestilente|modificada|
|[2FR8eBARbXfskcJR.htm](pathfinder-bestiary-2-items/2FR8eBARbXfskcJR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[2GNdGYBXQOD8VlC7.htm](pathfinder-bestiary-2-items/2GNdGYBXQOD8VlC7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2hG90mAiBnKHWoxc.htm](pathfinder-bestiary-2-items/2hG90mAiBnKHWoxc.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[2hIYkkGgRCsVdWhY.htm](pathfinder-bestiary-2-items/2hIYkkGgRCsVdWhY.htm)|Constant Spells|Constant Spells|modificada|
|[2HOhVTSCzGdbcfjr.htm](pathfinder-bestiary-2-items/2HOhVTSCzGdbcfjr.htm)|Jaws|Fauces|modificada|
|[2hyOsL9cmbYO1koQ.htm](pathfinder-bestiary-2-items/2hyOsL9cmbYO1koQ.htm)|Frightful Presence|Frightful Presence|modificada|
|[2IZWaM2O4amLWbye.htm](pathfinder-bestiary-2-items/2IZWaM2O4amLWbye.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[2J8nRu7ryhdxrn6Q.htm](pathfinder-bestiary-2-items/2J8nRu7ryhdxrn6Q.htm)|Jaws|Fauces|modificada|
|[2JNLdz4nnbNkOkbO.htm](pathfinder-bestiary-2-items/2JNLdz4nnbNkOkbO.htm)|Claw|Garra|modificada|
|[2KNgUCdMbbr8kaE7.htm](pathfinder-bestiary-2-items/2KNgUCdMbbr8kaE7.htm)|Barbed Tongue|Lengua de púas|modificada|
|[2KSiu60tktdQLiFE.htm](pathfinder-bestiary-2-items/2KSiu60tktdQLiFE.htm)|Ultimate Sacrifice|Ultimate Sacrifice|modificada|
|[2M8fHq1jWvJybtt9.htm](pathfinder-bestiary-2-items/2M8fHq1jWvJybtt9.htm)|Frightful Presence|Frightful Presence|modificada|
|[2MrVVGinZvjfZN22.htm](pathfinder-bestiary-2-items/2MrVVGinZvjfZN22.htm)|Tremorsense|Tremorsense|modificada|
|[2NXWhRmjG27cSo87.htm](pathfinder-bestiary-2-items/2NXWhRmjG27cSo87.htm)|Starknife|Starknife|modificada|
|[2O7YGmWG9wjbFcRM.htm](pathfinder-bestiary-2-items/2O7YGmWG9wjbFcRM.htm)|Scent|Scent|modificada|
|[2oF9dlmEoHZ6o4U9.htm](pathfinder-bestiary-2-items/2oF9dlmEoHZ6o4U9.htm)|Bastard Sword|Espada Bastarda|modificada|
|[2OIZKsg9vZrNrBBw.htm](pathfinder-bestiary-2-items/2OIZKsg9vZrNrBBw.htm)|Basidirond Spores|Basidirond Spores|modificada|
|[2ooN4XI8c3aPZmLa.htm](pathfinder-bestiary-2-items/2ooN4XI8c3aPZmLa.htm)|Grab|Agarrado|modificada|
|[2oTY3Ohbh0C59vOv.htm](pathfinder-bestiary-2-items/2oTY3Ohbh0C59vOv.htm)|Curse of Drowning|Maldición del ahogamiento|modificada|
|[2PLVvoyanKPQQdYK.htm](pathfinder-bestiary-2-items/2PLVvoyanKPQQdYK.htm)|Mist Cloud|Nube de Niebla|modificada|
|[2R71xX6zTcFcPntf.htm](pathfinder-bestiary-2-items/2R71xX6zTcFcPntf.htm)|Greater Constrict|Mayor Restricción|modificada|
|[2rJdPiiHOrVnLUyK.htm](pathfinder-bestiary-2-items/2rJdPiiHOrVnLUyK.htm)|Fish Hook|Fish Hook|modificada|
|[2SEpmpzrfI3iTOKV.htm](pathfinder-bestiary-2-items/2SEpmpzrfI3iTOKV.htm)|Jaws|Fauces|modificada|
|[2ShdE5g6oMqrL464.htm](pathfinder-bestiary-2-items/2ShdE5g6oMqrL464.htm)|Scent|Scent|modificada|
|[2SKe9OcSUJ5AXTPf.htm](pathfinder-bestiary-2-items/2SKe9OcSUJ5AXTPf.htm)|Archon's Door|Puerta del arconte|modificada|
|[2svyNegNoV205Fbp.htm](pathfinder-bestiary-2-items/2svyNegNoV205Fbp.htm)|Icy Demise|Icy Demise|modificada|
|[2szfgkG5m4pZQ6z8.htm](pathfinder-bestiary-2-items/2szfgkG5m4pZQ6z8.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2tplJgSAMF7CmPNN.htm](pathfinder-bestiary-2-items/2tplJgSAMF7CmPNN.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[2uyZOjTUnURaOWQp.htm](pathfinder-bestiary-2-items/2uyZOjTUnURaOWQp.htm)|Tail|Tail|modificada|
|[2VxyCqJZcASXlsyq.htm](pathfinder-bestiary-2-items/2VxyCqJZcASXlsyq.htm)|Calcification|Calcificación|modificada|
|[2waLtCSTbFL4Q6ta.htm](pathfinder-bestiary-2-items/2waLtCSTbFL4Q6ta.htm)|Regeneration 15 (Deactivated by Acid or Fire)|Regeneración 15 (Desactivado por Ácido o Fuego)|modificada|
|[2wgeHmf6T4ScZpAk.htm](pathfinder-bestiary-2-items/2wgeHmf6T4ScZpAk.htm)|Gray Ooze Acid|Gray Ooze Acid|modificada|
|[2wJro1SjxNwEf7TQ.htm](pathfinder-bestiary-2-items/2wJro1SjxNwEf7TQ.htm)|Hatchet|Hacha|modificada|
|[2wZs6OKCKOsMAU1a.htm](pathfinder-bestiary-2-items/2wZs6OKCKOsMAU1a.htm)|Throw Rock|Arrojar roca|modificada|
|[2X26aYIXJXcffHKZ.htm](pathfinder-bestiary-2-items/2X26aYIXJXcffHKZ.htm)|Throw Rock|Arrojar roca|modificada|
|[2xM4lSOsw2nfs2tu.htm](pathfinder-bestiary-2-items/2xM4lSOsw2nfs2tu.htm)|Mauler|Vapulear|modificada|
|[2YDmS6mB4lt2kYvF.htm](pathfinder-bestiary-2-items/2YDmS6mB4lt2kYvF.htm)|Jaws|Fauces|modificada|
|[2yuX2a5ufNBYmWHK.htm](pathfinder-bestiary-2-items/2yuX2a5ufNBYmWHK.htm)|Fangs|Colmillos|modificada|
|[3141UeEi3hFz0moy.htm](pathfinder-bestiary-2-items/3141UeEi3hFz0moy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[32RMKlAce7FfxW8e.htm](pathfinder-bestiary-2-items/32RMKlAce7FfxW8e.htm)|Darkvision|Visión en la oscuridad|modificada|
|[34NmTbXjw24GQVju.htm](pathfinder-bestiary-2-items/34NmTbXjw24GQVju.htm)|Mind-Rending Sting|Aguijón desgarra-mentes de la Rasgadura.|modificada|
|[37D2wZ5XwqQ5zjsE.htm](pathfinder-bestiary-2-items/37D2wZ5XwqQ5zjsE.htm)|Negative Healing|Curación negativa|modificada|
|[38cGo2E7vYZ2PNb5.htm](pathfinder-bestiary-2-items/38cGo2E7vYZ2PNb5.htm)|+2 Status Bonus to Saves vs. Fear|Bonificador de situación +2 a las salvaciones contra el miedo.|modificada|
|[38rv8OhglPWCKDoM.htm](pathfinder-bestiary-2-items/38rv8OhglPWCKDoM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[3AK8xWwQNAfn8GZR.htm](pathfinder-bestiary-2-items/3AK8xWwQNAfn8GZR.htm)|Bite|Muerdemuerde|modificada|
|[3AqKPdexSndZxd5X.htm](pathfinder-bestiary-2-items/3AqKPdexSndZxd5X.htm)|Rend|Rasgadura|modificada|
|[3bHndJ6sRVOw7gwC.htm](pathfinder-bestiary-2-items/3bHndJ6sRVOw7gwC.htm)|Improved Grab|Agarrado mejorado|modificada|
|[3BSxL95mz6QjoWi1.htm](pathfinder-bestiary-2-items/3BSxL95mz6QjoWi1.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[3c01H24sjgfJHyvI.htm](pathfinder-bestiary-2-items/3c01H24sjgfJHyvI.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[3cCdl7Qgc7kTT6at.htm](pathfinder-bestiary-2-items/3cCdl7Qgc7kTT6at.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3cCzIUJ7cXhefnQ1.htm](pathfinder-bestiary-2-items/3cCzIUJ7cXhefnQ1.htm)|Whirling Death|Muerte giratoria|modificada|
|[3CKGWY5tOmbNJznT.htm](pathfinder-bestiary-2-items/3CKGWY5tOmbNJznT.htm)|Fist|Puño|modificada|
|[3DQQAVyagZHfe2VD.htm](pathfinder-bestiary-2-items/3DQQAVyagZHfe2VD.htm)|Improved Grab|Agarrado mejorado|modificada|
|[3EzYscL7vFw5pVbn.htm](pathfinder-bestiary-2-items/3EzYscL7vFw5pVbn.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[3f1Y7j76I98KPIWK.htm](pathfinder-bestiary-2-items/3f1Y7j76I98KPIWK.htm)|Ice Missile|Misil de Hielo|modificada|
|[3F8tYJDQeeHlPY6p.htm](pathfinder-bestiary-2-items/3F8tYJDQeeHlPY6p.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3gCf4mj9ynEcSXVi.htm](pathfinder-bestiary-2-items/3gCf4mj9ynEcSXVi.htm)|Constrict|Restringir|modificada|
|[3gEkJauWCduRIDhx.htm](pathfinder-bestiary-2-items/3gEkJauWCduRIDhx.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[3GOGRqIhDzmef7he.htm](pathfinder-bestiary-2-items/3GOGRqIhDzmef7he.htm)|Hallucinogenic Cloud|Nube alucinógena|modificada|
|[3IExZ0tOTpB3Neuc.htm](pathfinder-bestiary-2-items/3IExZ0tOTpB3Neuc.htm)|Whirling Frenzy|Whirling Frenzy|modificada|
|[3IQSdTZPneapXwVf.htm](pathfinder-bestiary-2-items/3IQSdTZPneapXwVf.htm)|Change Shape|Change Shape|modificada|
|[3jVmH3McNsX8B0Od.htm](pathfinder-bestiary-2-items/3jVmH3McNsX8B0Od.htm)|Jaws|Fauces|modificada|
|[3KyuJoHTxD577UTr.htm](pathfinder-bestiary-2-items/3KyuJoHTxD577UTr.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[3LE8g6DKV4fZYrQq.htm](pathfinder-bestiary-2-items/3LE8g6DKV4fZYrQq.htm)|Regeneration 20 (deactivated by cold iron)|Regeneración 20 (desactivado por hierro frío)|modificada|
|[3MKqU2vGsb11kD4j.htm](pathfinder-bestiary-2-items/3MKqU2vGsb11kD4j.htm)|Negative Healing|Curación negativa|modificada|
|[3mOfCebY1QT6qHAG.htm](pathfinder-bestiary-2-items/3mOfCebY1QT6qHAG.htm)|Diligent Assault|Asalto Diligente|modificada|
|[3PMo2s4e4A2PWtKx.htm](pathfinder-bestiary-2-items/3PMo2s4e4A2PWtKx.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[3R5GHgg08mxLJg23.htm](pathfinder-bestiary-2-items/3R5GHgg08mxLJg23.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[3ryftdIzkfXriKLV.htm](pathfinder-bestiary-2-items/3ryftdIzkfXriKLV.htm)|Scent|Scent|modificada|
|[3s66FP7raqgSCnP1.htm](pathfinder-bestiary-2-items/3s66FP7raqgSCnP1.htm)|Mucus Trail|Mucus Trail|modificada|
|[3t3ulXs6r1EL76F0.htm](pathfinder-bestiary-2-items/3t3ulXs6r1EL76F0.htm)|Breath Weapon|Breath Weapon|modificada|
|[3t8p1OnCR5iUrhxt.htm](pathfinder-bestiary-2-items/3t8p1OnCR5iUrhxt.htm)|Pounce|Abalanzarse|modificada|
|[3tFe6InN38WdT6ps.htm](pathfinder-bestiary-2-items/3tFe6InN38WdT6ps.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3Ut3Piu4ATI3yyUU.htm](pathfinder-bestiary-2-items/3Ut3Piu4ATI3yyUU.htm)|Regeneration 25 (Deactivated by Good or Silver)|Regeneración 25 (Desactivado por Bien o Plata)|modificada|
|[3vycv9ThXOQbgNBf.htm](pathfinder-bestiary-2-items/3vycv9ThXOQbgNBf.htm)|Instinctual Tinker|Trastear instintivo|modificada|
|[3WqEPpUcLFd5pJv6.htm](pathfinder-bestiary-2-items/3WqEPpUcLFd5pJv6.htm)|Protean Anatomy 6|Protean Anatomy 6|modificada|
|[3YdQIsd5sKYMGTIc.htm](pathfinder-bestiary-2-items/3YdQIsd5sKYMGTIc.htm)|Regurgitation|Regurgitar|modificada|
|[3z9Sgd2sbQUaFIUK.htm](pathfinder-bestiary-2-items/3z9Sgd2sbQUaFIUK.htm)|Dagger|Daga|modificada|
|[3zjA9MZy3tcHkLRD.htm](pathfinder-bestiary-2-items/3zjA9MZy3tcHkLRD.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[40BuHxitDl0hJDTK.htm](pathfinder-bestiary-2-items/40BuHxitDl0hJDTK.htm)|Toxic Skin|Piel tóxica|modificada|
|[42LsnRw0ara1e2xW.htm](pathfinder-bestiary-2-items/42LsnRw0ara1e2xW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[43BphUu4vcCAeieN.htm](pathfinder-bestiary-2-items/43BphUu4vcCAeieN.htm)|Carbuncle Empathy|Carbuncle Empathy|modificada|
|[43gvQaRJhhjmfG3Z.htm](pathfinder-bestiary-2-items/43gvQaRJhhjmfG3Z.htm)|Blood Drain|Drenar sangre|modificada|
|[43K5JpJ2j1VBI70k.htm](pathfinder-bestiary-2-items/43K5JpJ2j1VBI70k.htm)|Darkvision|Visión en la oscuridad|modificada|
|[44O3eEgI5o0mZpbU.htm](pathfinder-bestiary-2-items/44O3eEgI5o0mZpbU.htm)|Telepathy|Telepatía|modificada|
|[44tsCgB5uAVisAQ3.htm](pathfinder-bestiary-2-items/44tsCgB5uAVisAQ3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[49xv61tdfV6J6P29.htm](pathfinder-bestiary-2-items/49xv61tdfV6J6P29.htm)|Discorporate|Discorporate|modificada|
|[49zcmiZjqimM6a7b.htm](pathfinder-bestiary-2-items/49zcmiZjqimM6a7b.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[4AlhbRnSYawCodcB.htm](pathfinder-bestiary-2-items/4AlhbRnSYawCodcB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4au76ROiAjb1dHdY.htm](pathfinder-bestiary-2-items/4au76ROiAjb1dHdY.htm)|Draconic Resistance|Resistencia dracónico|modificada|
|[4b9FtRKJfWmjYbQR.htm](pathfinder-bestiary-2-items/4b9FtRKJfWmjYbQR.htm)|Focus Gaze|Centrar mirada|modificada|
|[4BH9uwuYigIuBuzB.htm](pathfinder-bestiary-2-items/4BH9uwuYigIuBuzB.htm)|Lightning Blast|Lightning Blast|modificada|
|[4CALESo8Bdrqe3vE.htm](pathfinder-bestiary-2-items/4CALESo8Bdrqe3vE.htm)|Spill Venom|Derramar Veneno|modificada|
|[4cuSJxJLUg7icXmj.htm](pathfinder-bestiary-2-items/4cuSJxJLUg7icXmj.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[4CyhH5ZkHNlxJtIP.htm](pathfinder-bestiary-2-items/4CyhH5ZkHNlxJtIP.htm)|Regeneration 10 (deactivated by cold iron)|Regeneración 10 (desactivado por hierro frío).|modificada|
|[4d37i0818MYTgBAF.htm](pathfinder-bestiary-2-items/4d37i0818MYTgBAF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[4DpvbfaL4gw6x5Iy.htm](pathfinder-bestiary-2-items/4DpvbfaL4gw6x5Iy.htm)|Scent|Scent|modificada|
|[4eg3EowtqBauiDtw.htm](pathfinder-bestiary-2-items/4eg3EowtqBauiDtw.htm)|Constant Spells|Constant Spells|modificada|
|[4eJg9N0UmfD2Yt6C.htm](pathfinder-bestiary-2-items/4eJg9N0UmfD2Yt6C.htm)|Fist|Puño|modificada|
|[4f8Xn8AVpubItk3h.htm](pathfinder-bestiary-2-items/4f8Xn8AVpubItk3h.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[4fUKQrOX75rYdFV8.htm](pathfinder-bestiary-2-items/4fUKQrOX75rYdFV8.htm)|Entropy Sense|Entropy Sense|modificada|
|[4J4DJgMerRgq1mfr.htm](pathfinder-bestiary-2-items/4J4DJgMerRgq1mfr.htm)|Claw|Garra|modificada|
|[4jUkeNIE92J0asLl.htm](pathfinder-bestiary-2-items/4jUkeNIE92J0asLl.htm)|Negative Healing|Curación negativa|modificada|
|[4KxhR1H4vfF5teNe.htm](pathfinder-bestiary-2-items/4KxhR1H4vfF5teNe.htm)|Breath Weapon|Breath Weapon|modificada|
|[4LgyNabFsMIjCO9s.htm](pathfinder-bestiary-2-items/4LgyNabFsMIjCO9s.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[4MEkxras2sA4aWfg.htm](pathfinder-bestiary-2-items/4MEkxras2sA4aWfg.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[4mqhPMJ1Yzc6mTFk.htm](pathfinder-bestiary-2-items/4mqhPMJ1Yzc6mTFk.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[4NE1llNXfbsRiHmP.htm](pathfinder-bestiary-2-items/4NE1llNXfbsRiHmP.htm)|Savage Jaws|Fauces de Salvajismo|modificada|
|[4Ok3T9qk3DEMSviw.htm](pathfinder-bestiary-2-items/4Ok3T9qk3DEMSviw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4oL2L4CT1FG6IAEu.htm](pathfinder-bestiary-2-items/4oL2L4CT1FG6IAEu.htm)|Grab|Agarrado|modificada|
|[4OphdUKxBpxPyBKN.htm](pathfinder-bestiary-2-items/4OphdUKxBpxPyBKN.htm)|Tail Drag|Tail Drag|modificada|
|[4Ot5qt44391NJqRn.htm](pathfinder-bestiary-2-items/4Ot5qt44391NJqRn.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[4PAeJHUBsggCTLYw.htm](pathfinder-bestiary-2-items/4PAeJHUBsggCTLYw.htm)|Tail|Tail|modificada|
|[4PafOhYr090tdIQE.htm](pathfinder-bestiary-2-items/4PafOhYr090tdIQE.htm)|Aging Strikes|Golpes de Envejecimiento|modificada|
|[4PmFH52nJw8E0Xwl.htm](pathfinder-bestiary-2-items/4PmFH52nJw8E0Xwl.htm)|Grab|Agarrado|modificada|
|[4R3SAD3faLDXH2p1.htm](pathfinder-bestiary-2-items/4R3SAD3faLDXH2p1.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[4SqlNn1QJhVGrfJb.htm](pathfinder-bestiary-2-items/4SqlNn1QJhVGrfJb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4tddCK0S1FejzHcG.htm](pathfinder-bestiary-2-items/4tddCK0S1FejzHcG.htm)|Wild Empathy|Empatía salvaje|modificada|
|[4tJ8KAPk36CVF7E2.htm](pathfinder-bestiary-2-items/4tJ8KAPk36CVF7E2.htm)|Savage Jaws|Fauces de Salvajismo|modificada|
|[4ujIS89zTfcMbbaX.htm](pathfinder-bestiary-2-items/4ujIS89zTfcMbbaX.htm)|Jaws|Fauces|modificada|
|[4UMDwIzgdxumhvIf.htm](pathfinder-bestiary-2-items/4UMDwIzgdxumhvIf.htm)|Swarm Mind|Swarm Mind|modificada|
|[4Uoy7JHv0Ea9SrCr.htm](pathfinder-bestiary-2-items/4Uoy7JHv0Ea9SrCr.htm)|Baffling Bluff|Baffling Bluff|modificada|
|[4w3vvxZvUVrs2iWm.htm](pathfinder-bestiary-2-items/4w3vvxZvUVrs2iWm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4W4LiMhJykn0tAnC.htm](pathfinder-bestiary-2-items/4W4LiMhJykn0tAnC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4z5ULy6UhFHErn4I.htm](pathfinder-bestiary-2-items/4z5ULy6UhFHErn4I.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4zBBMGUTIvxbdVBf.htm](pathfinder-bestiary-2-items/4zBBMGUTIvxbdVBf.htm)|Fist|Puño|modificada|
|[4ZdMoPLM5L40W4TK.htm](pathfinder-bestiary-2-items/4ZdMoPLM5L40W4TK.htm)|Claw|Garra|modificada|
|[4zEyOGGm0vwVrtmi.htm](pathfinder-bestiary-2-items/4zEyOGGm0vwVrtmi.htm)|Cunning|Astucia|modificada|
|[52OH24WzpzdT3ptJ.htm](pathfinder-bestiary-2-items/52OH24WzpzdT3ptJ.htm)|Envisioning|Visualizado|modificada|
|[54mSt8tnFXfSUjhU.htm](pathfinder-bestiary-2-items/54mSt8tnFXfSUjhU.htm)|Cling|Cling|modificada|
|[54xjK61C3DBrqHe7.htm](pathfinder-bestiary-2-items/54xjK61C3DBrqHe7.htm)|Constant Spells|Constant Spells|modificada|
|[58HvfTTw56mTEDsj.htm](pathfinder-bestiary-2-items/58HvfTTw56mTEDsj.htm)|Breath Weapon|Breath Weapon|modificada|
|[5aqMBttykiS0SKAb.htm](pathfinder-bestiary-2-items/5aqMBttykiS0SKAb.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[5ASzxj6oyeWu6gWM.htm](pathfinder-bestiary-2-items/5ASzxj6oyeWu6gWM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5cC1uj7hBSGN8tix.htm](pathfinder-bestiary-2-items/5cC1uj7hBSGN8tix.htm)|Lifesense|Lifesense|modificada|
|[5CgVbeYygEXhZf6y.htm](pathfinder-bestiary-2-items/5CgVbeYygEXhZf6y.htm)|Bully's Bludgeon|Bully's Bludgeon|modificada|
|[5d1k1jwifXVc23o0.htm](pathfinder-bestiary-2-items/5d1k1jwifXVc23o0.htm)|Claw|Garra|modificada|
|[5djM8sO4U2PY23bG.htm](pathfinder-bestiary-2-items/5djM8sO4U2PY23bG.htm)|Unstoppable|Imparable|modificada|
|[5Ds8GMXoIjWg9j7Q.htm](pathfinder-bestiary-2-items/5Ds8GMXoIjWg9j7Q.htm)|Tentacle|Tentáculo|modificada|
|[5e5FND78feSKXP99.htm](pathfinder-bestiary-2-items/5e5FND78feSKXP99.htm)|Impaling Chain|Cadena Empaladora|modificada|
|[5E9Da5IYp7uvbZKT.htm](pathfinder-bestiary-2-items/5E9Da5IYp7uvbZKT.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[5EcCWK4rV7lqrPqY.htm](pathfinder-bestiary-2-items/5EcCWK4rV7lqrPqY.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[5f6mJgvaVADXCaAz.htm](pathfinder-bestiary-2-items/5f6mJgvaVADXCaAz.htm)|Scent|Scent|modificada|
|[5fPhzfjVfDH0Ssgz.htm](pathfinder-bestiary-2-items/5fPhzfjVfDH0Ssgz.htm)|Terrifying Gaze|Mirada aterradora|modificada|
|[5g1Keo0L3GpJZyQp.htm](pathfinder-bestiary-2-items/5g1Keo0L3GpJZyQp.htm)|Curse of the Wereboar|Curse of the Wereboar|modificada|
|[5gLIoGOoPN1ReZMD.htm](pathfinder-bestiary-2-items/5gLIoGOoPN1ReZMD.htm)|Wing|Ala|modificada|
|[5hqdjh4Jn5wtzqGs.htm](pathfinder-bestiary-2-items/5hqdjh4Jn5wtzqGs.htm)|Tendril|Tendril|modificada|
|[5IixHzakyftZgS0g.htm](pathfinder-bestiary-2-items/5IixHzakyftZgS0g.htm)|Speed Surge|Arranque de velocidad|modificada|
|[5IQ8tiyhCdSfyPEo.htm](pathfinder-bestiary-2-items/5IQ8tiyhCdSfyPEo.htm)|Grab|Agarrado|modificada|
|[5kEI04PwOkPj2l0j.htm](pathfinder-bestiary-2-items/5kEI04PwOkPj2l0j.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[5lCoU2D6djePIJZT.htm](pathfinder-bestiary-2-items/5lCoU2D6djePIJZT.htm)|Web Sense|Web Sense|modificada|
|[5lPaA6GKal2YHAjj.htm](pathfinder-bestiary-2-items/5lPaA6GKal2YHAjj.htm)|Jaws|Fauces|modificada|
|[5nazHNTE9SHWxEH5.htm](pathfinder-bestiary-2-items/5nazHNTE9SHWxEH5.htm)|Claw|Garra|modificada|
|[5OLDndk87U2qEJ5p.htm](pathfinder-bestiary-2-items/5OLDndk87U2qEJ5p.htm)|Pincer|Pinza|modificada|
|[5p5UO2vVsNmY2INe.htm](pathfinder-bestiary-2-items/5p5UO2vVsNmY2INe.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[5PvfqR4GcUsfw2mR.htm](pathfinder-bestiary-2-items/5PvfqR4GcUsfw2mR.htm)|Rend|Rasgadura|modificada|
|[5qArY8oNtsZLv3Lg.htm](pathfinder-bestiary-2-items/5qArY8oNtsZLv3Lg.htm)|Jaws|Fauces|modificada|
|[5qgx9qhXAceS6uLh.htm](pathfinder-bestiary-2-items/5qgx9qhXAceS6uLh.htm)|Enraged Cunning|Astucia furibunda|modificada|
|[5QKXjFlhvGGdNcxf.htm](pathfinder-bestiary-2-items/5QKXjFlhvGGdNcxf.htm)|Claw|Garra|modificada|
|[5QMBo1Vy2JZTTsNV.htm](pathfinder-bestiary-2-items/5QMBo1Vy2JZTTsNV.htm)|Tremorsense|Tremorsense|modificada|
|[5rN2CIypbGA4N34M.htm](pathfinder-bestiary-2-items/5rN2CIypbGA4N34M.htm)|Fangs|Colmillos|modificada|
|[5RsDoTAVa9W8gKnx.htm](pathfinder-bestiary-2-items/5RsDoTAVa9W8gKnx.htm)|Draconic Resistance|Resistencia dracónico|modificada|
|[5rxGka9M6pYlIoVb.htm](pathfinder-bestiary-2-items/5rxGka9M6pYlIoVb.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[5SaRw4TGk2D0VBTZ.htm](pathfinder-bestiary-2-items/5SaRw4TGk2D0VBTZ.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[5t9D4gKM7JVpBSf6.htm](pathfinder-bestiary-2-items/5t9D4gKM7JVpBSf6.htm)|Wild Empathy|Empatía salvaje|modificada|
|[5tiASMinDzyHUAEv.htm](pathfinder-bestiary-2-items/5tiASMinDzyHUAEv.htm)|Jaws|Fauces|modificada|
|[5tqNBlspN6txKrz9.htm](pathfinder-bestiary-2-items/5tqNBlspN6txKrz9.htm)|Jaws|Fauces|modificada|
|[5UAZUiojo21FxrFI.htm](pathfinder-bestiary-2-items/5UAZUiojo21FxrFI.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[5UFt8Dv6NCEOpZlE.htm](pathfinder-bestiary-2-items/5UFt8Dv6NCEOpZlE.htm)|Constant Spells|Constant Spells|modificada|
|[5Vw9VTB3I1sie1g1.htm](pathfinder-bestiary-2-items/5Vw9VTB3I1sie1g1.htm)|Sarglagon Venom|Sarglagon Venom|modificada|
|[5WLytdwKYQa3JLh7.htm](pathfinder-bestiary-2-items/5WLytdwKYQa3JLh7.htm)|Malleable|Maleable|modificada|
|[5XOy3zqts39I3bzo.htm](pathfinder-bestiary-2-items/5XOy3zqts39I3bzo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5xuP57kLx1Xu16ZO.htm](pathfinder-bestiary-2-items/5xuP57kLx1Xu16ZO.htm)|Fist|Puño|modificada|
|[5YDssRfPpgId4gTE.htm](pathfinder-bestiary-2-items/5YDssRfPpgId4gTE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5Ygh7NnWAZBamFaS.htm](pathfinder-bestiary-2-items/5Ygh7NnWAZBamFaS.htm)|Jaws|Fauces|modificada|
|[5yWmBrLhwYvStUFi.htm](pathfinder-bestiary-2-items/5yWmBrLhwYvStUFi.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[60d6TRr07HgWYE5B.htm](pathfinder-bestiary-2-items/60d6TRr07HgWYE5B.htm)|Current|Actual|modificada|
|[61EkmT4Ba451hMDC.htm](pathfinder-bestiary-2-items/61EkmT4Ba451hMDC.htm)|Jaws|Fauces|modificada|
|[61GyOh9BdFBPZCUD.htm](pathfinder-bestiary-2-items/61GyOh9BdFBPZCUD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[61nmK3yvcoeX2PbG.htm](pathfinder-bestiary-2-items/61nmK3yvcoeX2PbG.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[68hstcNYAfYsk65j.htm](pathfinder-bestiary-2-items/68hstcNYAfYsk65j.htm)|Claw|Garra|modificada|
|[68IxixgMddYHirAR.htm](pathfinder-bestiary-2-items/68IxixgMddYHirAR.htm)|Constrict|Restringir|modificada|
|[68RVtURJJY5N5dTK.htm](pathfinder-bestiary-2-items/68RVtURJJY5N5dTK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6a1pfDWvzEsw0JWE.htm](pathfinder-bestiary-2-items/6a1pfDWvzEsw0JWE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6A6lheQTnhAryrxG.htm](pathfinder-bestiary-2-items/6A6lheQTnhAryrxG.htm)|Constant Spells|Constant Spells|modificada|
|[6B5ZXZm5DwAILPJ2.htm](pathfinder-bestiary-2-items/6B5ZXZm5DwAILPJ2.htm)|Dance of Death|Danza de la Muerte|modificada|
|[6BxCWHRHwlGUEbIO.htm](pathfinder-bestiary-2-items/6BxCWHRHwlGUEbIO.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[6CGO8ENM5nFAurjN.htm](pathfinder-bestiary-2-items/6CGO8ENM5nFAurjN.htm)|Ranseur|Ranseur|modificada|
|[6dcoGxoSd8A6QFv2.htm](pathfinder-bestiary-2-items/6dcoGxoSd8A6QFv2.htm)|Twisting Tail|Enredar con la cola|modificada|
|[6gE7wfS2bLW0PuDW.htm](pathfinder-bestiary-2-items/6gE7wfS2bLW0PuDW.htm)|Kiss of Death|Beso de la Muerte|modificada|
|[6gwNFiImkw2H0QWA.htm](pathfinder-bestiary-2-items/6gwNFiImkw2H0QWA.htm)|Grab|Agarrado|modificada|
|[6i7GkvP5XmSn8g6G.htm](pathfinder-bestiary-2-items/6i7GkvP5XmSn8g6G.htm)|Breath Weapon|Breath Weapon|modificada|
|[6ITLmA9FU1LuwZNY.htm](pathfinder-bestiary-2-items/6ITLmA9FU1LuwZNY.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6jVS5MIWVjrSMBki.htm](pathfinder-bestiary-2-items/6jVS5MIWVjrSMBki.htm)|Telepathy|Telepatía|modificada|
|[6leNSciOB3vrO7El.htm](pathfinder-bestiary-2-items/6leNSciOB3vrO7El.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6lnr0C4QK4LAkOa1.htm](pathfinder-bestiary-2-items/6lnr0C4QK4LAkOa1.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[6LuECgP1a3GGskbB.htm](pathfinder-bestiary-2-items/6LuECgP1a3GGskbB.htm)|Jaws|Fauces|modificada|
|[6mCb1o0EW6yQ0NfB.htm](pathfinder-bestiary-2-items/6mCb1o0EW6yQ0NfB.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6Mfkl1CejuYcVcXf.htm](pathfinder-bestiary-2-items/6Mfkl1CejuYcVcXf.htm)|Seed|Semilla|modificada|
|[6mmZWINTcnrrNQcU.htm](pathfinder-bestiary-2-items/6mmZWINTcnrrNQcU.htm)|Constant Spells|Constant Spells|modificada|
|[6N8zQ0TKFxKlkOTG.htm](pathfinder-bestiary-2-items/6N8zQ0TKFxKlkOTG.htm)|Claim Wealth|Reclamar Riqueza|modificada|
|[6NgDd3kvRPxleBuE.htm](pathfinder-bestiary-2-items/6NgDd3kvRPxleBuE.htm)|Blinding Sand|Arena cegadora|modificada|
|[6no2LBNfSHP8w9DB.htm](pathfinder-bestiary-2-items/6no2LBNfSHP8w9DB.htm)|Negative Healing|Curación negativa|modificada|
|[6oTpSA3IXew7MulO.htm](pathfinder-bestiary-2-items/6oTpSA3IXew7MulO.htm)|Hand Of Fate|Hand Of Fate|modificada|
|[6Psn5RSYm8UGDpwM.htm](pathfinder-bestiary-2-items/6Psn5RSYm8UGDpwM.htm)|Pod Prison|Prisión Pod|modificada|
|[6qaFJjq9JjlhZbaw.htm](pathfinder-bestiary-2-items/6qaFJjq9JjlhZbaw.htm)|Ogre Spider Venom|Veneno de Araña Ogro|modificada|
|[6qDqRA2Nq93rsvx9.htm](pathfinder-bestiary-2-items/6qDqRA2Nq93rsvx9.htm)|Greater Electrolocation|Electrolocación Mayor|modificada|
|[6rslY3YCHH1EYxYL.htm](pathfinder-bestiary-2-items/6rslY3YCHH1EYxYL.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[6Sms9YOa60rjevVP.htm](pathfinder-bestiary-2-items/6Sms9YOa60rjevVP.htm)|Jaws|Fauces|modificada|
|[6t0iHm0ru1bcVGTw.htm](pathfinder-bestiary-2-items/6t0iHm0ru1bcVGTw.htm)|Entangling Tendrils|Zarcillos enmarados|modificada|
|[6U0xCdudnhZPawQv.htm](pathfinder-bestiary-2-items/6U0xCdudnhZPawQv.htm)|Web War Flail|Matraca de guerra telara|modificada|
|[6UhOo5jcpQz76GcZ.htm](pathfinder-bestiary-2-items/6UhOo5jcpQz76GcZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6VEsckJo4laa2uls.htm](pathfinder-bestiary-2-items/6VEsckJo4laa2uls.htm)|Claw|Garra|modificada|
|[6vnexEfGJBfKrtHp.htm](pathfinder-bestiary-2-items/6vnexEfGJBfKrtHp.htm)|Tusk Sweep|Tusk Barrido|modificada|
|[6xKgrVRYAjGEO0p7.htm](pathfinder-bestiary-2-items/6xKgrVRYAjGEO0p7.htm)|Flame Jet|Flame Jet|modificada|
|[6XMcWttuyRBueX73.htm](pathfinder-bestiary-2-items/6XMcWttuyRBueX73.htm)|Knockdown|Derribo|modificada|
|[6Yl0sNRGgIqfvRj8.htm](pathfinder-bestiary-2-items/6Yl0sNRGgIqfvRj8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6ZtjeB9jdEW4o2QJ.htm](pathfinder-bestiary-2-items/6ZtjeB9jdEW4o2QJ.htm)|Bite|Muerdemuerde|modificada|
|[71Xez8c9broSGKbf.htm](pathfinder-bestiary-2-items/71Xez8c9broSGKbf.htm)|Quill Barrage|Quill Barrage|modificada|
|[72YvqHEAOHV34zLK.htm](pathfinder-bestiary-2-items/72YvqHEAOHV34zLK.htm)|Mucus|Mucus|modificada|
|[76jTiA45mzSvkuz1.htm](pathfinder-bestiary-2-items/76jTiA45mzSvkuz1.htm)|Antler|Antler|modificada|
|[7AMBKbBXmPFp0RlP.htm](pathfinder-bestiary-2-items/7AMBKbBXmPFp0RlP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7B1Q0p1BKuDwOEoo.htm](pathfinder-bestiary-2-items/7B1Q0p1BKuDwOEoo.htm)|Smoke Vision|Visión de Humo|modificada|
|[7cdnWouJZ6KDhfxj.htm](pathfinder-bestiary-2-items/7cdnWouJZ6KDhfxj.htm)|Club|Club|modificada|
|[7D3xnmgdDZGumRA4.htm](pathfinder-bestiary-2-items/7D3xnmgdDZGumRA4.htm)|Breath Weapon|Breath Weapon|modificada|
|[7dOMKFcaiBcHkgt1.htm](pathfinder-bestiary-2-items/7dOMKFcaiBcHkgt1.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[7dxhRfITiiUW00kr.htm](pathfinder-bestiary-2-items/7dxhRfITiiUW00kr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7EYorPSwB5MGbm8A.htm](pathfinder-bestiary-2-items/7EYorPSwB5MGbm8A.htm)|Constant Spells|Constant Spells|modificada|
|[7FksKj3ieU9yJEu0.htm](pathfinder-bestiary-2-items/7FksKj3ieU9yJEu0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7GHhkRMg6SxILo1W.htm](pathfinder-bestiary-2-items/7GHhkRMg6SxILo1W.htm)|Lifesense|Lifesense|modificada|
|[7Hht50p2jc8ZG7Xq.htm](pathfinder-bestiary-2-items/7Hht50p2jc8ZG7Xq.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[7HuZlfRX4ZQI1GwE.htm](pathfinder-bestiary-2-items/7HuZlfRX4ZQI1GwE.htm)|Blade|Blade|modificada|
|[7i3BZvYdZOVTki7v.htm](pathfinder-bestiary-2-items/7i3BZvYdZOVTki7v.htm)|Throw Rock|Arrojar roca|modificada|
|[7ibCw6e1BSbQ94NW.htm](pathfinder-bestiary-2-items/7ibCw6e1BSbQ94NW.htm)|Lurker's Glow|Lurker's Glow|modificada|
|[7j5B40DMWHBTShLl.htm](pathfinder-bestiary-2-items/7j5B40DMWHBTShLl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7j5ta80hcHB60jDE.htm](pathfinder-bestiary-2-items/7j5ta80hcHB60jDE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[7kGu1ORgE4Nay79B.htm](pathfinder-bestiary-2-items/7kGu1ORgE4Nay79B.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[7MiqMH6uob5zPB4e.htm](pathfinder-bestiary-2-items/7MiqMH6uob5zPB4e.htm)|Spotlight|Spotlight|modificada|
|[7mVYoMlm7knbjqzo.htm](pathfinder-bestiary-2-items/7mVYoMlm7knbjqzo.htm)|Tail|Tail|modificada|
|[7NjsgQKMg0FvxDmJ.htm](pathfinder-bestiary-2-items/7NjsgQKMg0FvxDmJ.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[7PG4MVtAvT1kH55R.htm](pathfinder-bestiary-2-items/7PG4MVtAvT1kH55R.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[7pxsxbazh1PpK8A4.htm](pathfinder-bestiary-2-items/7pxsxbazh1PpK8A4.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[7Qq2wYdCdaBf6zs4.htm](pathfinder-bestiary-2-items/7Qq2wYdCdaBf6zs4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7QTkBSoDwgvNiBiC.htm](pathfinder-bestiary-2-items/7QTkBSoDwgvNiBiC.htm)|Chain|Cadena|modificada|
|[7RlCpEdZ5Lw3Omd2.htm](pathfinder-bestiary-2-items/7RlCpEdZ5Lw3Omd2.htm)|Gust|Gust|modificada|
|[7rYYffte0B1eDFDt.htm](pathfinder-bestiary-2-items/7rYYffte0B1eDFDt.htm)|Telepathy|Telepatía|modificada|
|[7RZaK7BB8AYtaXJb.htm](pathfinder-bestiary-2-items/7RZaK7BB8AYtaXJb.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7TyKfwJXlkKJmSp1.htm](pathfinder-bestiary-2-items/7TyKfwJXlkKJmSp1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7wi6tn6G6vLyc1tN.htm](pathfinder-bestiary-2-items/7wi6tn6G6vLyc1tN.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[7x3Iu1Xd8r44ZR48.htm](pathfinder-bestiary-2-items/7x3Iu1Xd8r44ZR48.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7XnqXwEoetjKQZg4.htm](pathfinder-bestiary-2-items/7XnqXwEoetjKQZg4.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[8BGsf6kEIrCtKGgo.htm](pathfinder-bestiary-2-items/8BGsf6kEIrCtKGgo.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[8CRzC395t7TeEnQO.htm](pathfinder-bestiary-2-items/8CRzC395t7TeEnQO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8F42ZUECGwbl9nmU.htm](pathfinder-bestiary-2-items/8F42ZUECGwbl9nmU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8g9m2H7zav2ekCFO.htm](pathfinder-bestiary-2-items/8g9m2H7zav2ekCFO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8GCV9NHdglsydwmB.htm](pathfinder-bestiary-2-items/8GCV9NHdglsydwmB.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[8GQKAYkj3NIJHurF.htm](pathfinder-bestiary-2-items/8GQKAYkj3NIJHurF.htm)|Children of the Night|Niños de la Noche|modificada|
|[8H0a3j4x2wevxTU0.htm](pathfinder-bestiary-2-items/8H0a3j4x2wevxTU0.htm)|Rock|Roca|modificada|
|[8J1VrbhC0fQxs0Xu.htm](pathfinder-bestiary-2-items/8J1VrbhC0fQxs0Xu.htm)|Muddy Field|Campo Fangoso|modificada|
|[8JhtIGFYB5PcTSIt.htm](pathfinder-bestiary-2-items/8JhtIGFYB5PcTSIt.htm)|Ice Stride|Zancada de hielo|modificada|
|[8JPlEU0ps7X2hfP6.htm](pathfinder-bestiary-2-items/8JPlEU0ps7X2hfP6.htm)|Flower|Flor|modificada|
|[8Mkj5lFXbki11UgV.htm](pathfinder-bestiary-2-items/8Mkj5lFXbki11UgV.htm)|Infernal Mindlink|Enlace mental infernal|modificada|
|[8nLUnVkKYxkXvAZs.htm](pathfinder-bestiary-2-items/8nLUnVkKYxkXvAZs.htm)|Constrict|Restringir|modificada|
|[8og5NF6ejbQcG1bF.htm](pathfinder-bestiary-2-items/8og5NF6ejbQcG1bF.htm)|Rusting Grasp|Presa oxidante|modificada|
|[8PnlI5NzEir5W7Il.htm](pathfinder-bestiary-2-items/8PnlI5NzEir5W7Il.htm)|Derghodaemon's Stare|Mirada de Derghodaemon|modificada|
|[8PpUoIkvn5HDKBBp.htm](pathfinder-bestiary-2-items/8PpUoIkvn5HDKBBp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8prRtWLrblxto9pO.htm](pathfinder-bestiary-2-items/8prRtWLrblxto9pO.htm)|Poison Gasp|Bocanada venenosa|modificada|
|[8qlQbmcwGPfRmLYu.htm](pathfinder-bestiary-2-items/8qlQbmcwGPfRmLYu.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[8qNsivFnCww0jFZo.htm](pathfinder-bestiary-2-items/8qNsivFnCww0jFZo.htm)|Electric Missile|Misil Eléctrico|modificada|
|[8qYHl9oKTV79IZ6r.htm](pathfinder-bestiary-2-items/8qYHl9oKTV79IZ6r.htm)|Constrict|Restringir|modificada|
|[8r06lqp1MccJLeUV.htm](pathfinder-bestiary-2-items/8r06lqp1MccJLeUV.htm)|Drain Blood|Drenar sangre|modificada|
|[8r5XXqPZOhI0JSfB.htm](pathfinder-bestiary-2-items/8r5XXqPZOhI0JSfB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8RH2spsc79xmzAXn.htm](pathfinder-bestiary-2-items/8RH2spsc79xmzAXn.htm)|Allergen Aura|Allergen Aura|modificada|
|[8RpmS3nHFfxBiGaY.htm](pathfinder-bestiary-2-items/8RpmS3nHFfxBiGaY.htm)|Swarm Mind|Swarm Mind|modificada|
|[8SCH000Zv6X9BFPf.htm](pathfinder-bestiary-2-items/8SCH000Zv6X9BFPf.htm)|Fist|Puño|modificada|
|[8SI23rDwK2QYJaxP.htm](pathfinder-bestiary-2-items/8SI23rDwK2QYJaxP.htm)|Catch Rock|Atrapar roca|modificada|
|[8sphOZAnVIS4eVZ6.htm](pathfinder-bestiary-2-items/8sphOZAnVIS4eVZ6.htm)|Wing|Ala|modificada|
|[8tPI0ksCv4zvd6WV.htm](pathfinder-bestiary-2-items/8tPI0ksCv4zvd6WV.htm)|Corkscrew|Sacacorchos|modificada|
|[8UGnwgNXjFG8VCZU.htm](pathfinder-bestiary-2-items/8UGnwgNXjFG8VCZU.htm)|Hoof|Hoof|modificada|
|[8UmPplmBw6ibPa9v.htm](pathfinder-bestiary-2-items/8UmPplmBw6ibPa9v.htm)|Explosion|Explosión|modificada|
|[8UOucEsn2oF9O2uX.htm](pathfinder-bestiary-2-items/8UOucEsn2oF9O2uX.htm)|Frightful Presence|Frightful Presence|modificada|
|[8wrcvKK8xk5AIIva.htm](pathfinder-bestiary-2-items/8wrcvKK8xk5AIIva.htm)|Muddy Field|Campo Fangoso|modificada|
|[8WsmugIanltHKUg1.htm](pathfinder-bestiary-2-items/8WsmugIanltHKUg1.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[8Xjni4fPUr5P0Dl9.htm](pathfinder-bestiary-2-items/8Xjni4fPUr5P0Dl9.htm)|Jellyfish Venom|Jellyfish Venom|modificada|
|[8y1uIv1RL7FsHZzu.htm](pathfinder-bestiary-2-items/8y1uIv1RL7FsHZzu.htm)|Swallow Whole|Engullir Todo|modificada|
|[8yvIS9Nt5mat9qnA.htm](pathfinder-bestiary-2-items/8yvIS9Nt5mat9qnA.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[8yVRt0M6F9WbgdWV.htm](pathfinder-bestiary-2-items/8yVRt0M6F9WbgdWV.htm)|Cairn Linnorm Venom|Veneno del linnorm del túmulo|modificada|
|[8Zo1SkheQdiIdsNs.htm](pathfinder-bestiary-2-items/8Zo1SkheQdiIdsNs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[90V4TwzjxTH7bK7r.htm](pathfinder-bestiary-2-items/90V4TwzjxTH7bK7r.htm)|Spear|Lanza|modificada|
|[92YIjzpVzdYw3ZQC.htm](pathfinder-bestiary-2-items/92YIjzpVzdYw3ZQC.htm)|Change Shape|Change Shape|modificada|
|[967GKOeV0EqG2hzn.htm](pathfinder-bestiary-2-items/967GKOeV0EqG2hzn.htm)|Grab|Agarrado|modificada|
|[981IVfc2IS4Qnd3U.htm](pathfinder-bestiary-2-items/981IVfc2IS4Qnd3U.htm)|Aura of Sobs|Aura de sollozos|modificada|
|[98N9e0IqCp7WoRHr.htm](pathfinder-bestiary-2-items/98N9e0IqCp7WoRHr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[98pkTquG5m1vBxbt.htm](pathfinder-bestiary-2-items/98pkTquG5m1vBxbt.htm)|Tail|Tail|modificada|
|[9AHoo6l3g2enrpEz.htm](pathfinder-bestiary-2-items/9AHoo6l3g2enrpEz.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[9atqlz9j100vC4uo.htm](pathfinder-bestiary-2-items/9atqlz9j100vC4uo.htm)|Jaws|Fauces|modificada|
|[9bkFg1e33lRT8urN.htm](pathfinder-bestiary-2-items/9bkFg1e33lRT8urN.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[9bMQUXIt8V0ivYCg.htm](pathfinder-bestiary-2-items/9bMQUXIt8V0ivYCg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9bSMDZ283g6Zq6IU.htm](pathfinder-bestiary-2-items/9bSMDZ283g6Zq6IU.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[9cHX93V0XpVb7b7b.htm](pathfinder-bestiary-2-items/9cHX93V0XpVb7b7b.htm)|Lashing Tongues|Lenguas fustigantes|modificada|
|[9EJRdPBHkHMyY0yk.htm](pathfinder-bestiary-2-items/9EJRdPBHkHMyY0yk.htm)|Trample|Trample|modificada|
|[9eONvr5by0xfPFcf.htm](pathfinder-bestiary-2-items/9eONvr5by0xfPFcf.htm)|Witchflame Caress|Witchflame Caress|modificada|
|[9FcMZnkENPj4z3q0.htm](pathfinder-bestiary-2-items/9FcMZnkENPj4z3q0.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[9G77nycfkHc93HGi.htm](pathfinder-bestiary-2-items/9G77nycfkHc93HGi.htm)|Dagger|Daga|modificada|
|[9H6kMOzk61FQZoC9.htm](pathfinder-bestiary-2-items/9H6kMOzk61FQZoC9.htm)|Tail|Tail|modificada|
|[9hRQJlQwiVSfsdc2.htm](pathfinder-bestiary-2-items/9hRQJlQwiVSfsdc2.htm)|Undulate|Undulate|modificada|
|[9k7o784yI4Bka8ys.htm](pathfinder-bestiary-2-items/9k7o784yI4Bka8ys.htm)|Crossbow|Ballesta|modificada|
|[9KZUjyVAnGC6gEbC.htm](pathfinder-bestiary-2-items/9KZUjyVAnGC6gEbC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9O71o5KF846nZWVa.htm](pathfinder-bestiary-2-items/9O71o5KF846nZWVa.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[9oJwSlJ6tyt3KuRl.htm](pathfinder-bestiary-2-items/9oJwSlJ6tyt3KuRl.htm)|Spear|Lanza|modificada|
|[9ONTjzUo2qngSIHO.htm](pathfinder-bestiary-2-items/9ONTjzUo2qngSIHO.htm)|Grab|Agarrado|modificada|
|[9Q05lkXgY0PrqSQz.htm](pathfinder-bestiary-2-items/9Q05lkXgY0PrqSQz.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9rLnLLOFCIgXRAK8.htm](pathfinder-bestiary-2-items/9rLnLLOFCIgXRAK8.htm)|Jaws|Fauces|modificada|
|[9tmJIJ6vy9JEPaL3.htm](pathfinder-bestiary-2-items/9tmJIJ6vy9JEPaL3.htm)|Tail|Tail|modificada|
|[9tpWx4QY08q4Nv5G.htm](pathfinder-bestiary-2-items/9tpWx4QY08q4Nv5G.htm)|Telepathy|Telepatía|modificada|
|[9tukJ8Am3f00bHUu.htm](pathfinder-bestiary-2-items/9tukJ8Am3f00bHUu.htm)|Savage Jaws|Fauces de Salvajismo|modificada|
|[9UD8fQB0IDkl7w1X.htm](pathfinder-bestiary-2-items/9UD8fQB0IDkl7w1X.htm)|Staggering Servitude|Servidumbre aturdidora|modificada|
|[9UqGqGPYi6N8yk6X.htm](pathfinder-bestiary-2-items/9UqGqGPYi6N8yk6X.htm)|Storm of Vines|Tormenta de Vides|modificada|
|[9wolaUuOroFk0cOg.htm](pathfinder-bestiary-2-items/9wolaUuOroFk0cOg.htm)|Frightful Presence|Frightful Presence|modificada|
|[9WzRmRmcle5Fmp7N.htm](pathfinder-bestiary-2-items/9WzRmRmcle5Fmp7N.htm)|Alter Weather|Clima alterado|modificada|
|[9Xg9EXT1XEoi38zM.htm](pathfinder-bestiary-2-items/9Xg9EXT1XEoi38zM.htm)|Spear|Lanza|modificada|
|[9XxEDCpiqnYKds7V.htm](pathfinder-bestiary-2-items/9XxEDCpiqnYKds7V.htm)|Draconic Resistance|Resistencia dracónico|modificada|
|[9yu44cM7W0Xg2Q36.htm](pathfinder-bestiary-2-items/9yu44cM7W0Xg2Q36.htm)|Planar Incarnation - Astral Plane|Encarnación Planar - Plano Astral|modificada|
|[9zIzERdEdvnnEjwe.htm](pathfinder-bestiary-2-items/9zIzERdEdvnnEjwe.htm)|Claw|Garra|modificada|
|[A18yDTtE84tJ1jhJ.htm](pathfinder-bestiary-2-items/A18yDTtE84tJ1jhJ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[a29GFo7lbKhSTAYm.htm](pathfinder-bestiary-2-items/a29GFo7lbKhSTAYm.htm)|Fast Healing 2 (when touching ice or snow)|Curación rápida 2 (al tocar hielo o nieve)|modificada|
|[A2KzHDp53CpDVtv5.htm](pathfinder-bestiary-2-items/A2KzHDp53CpDVtv5.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[A2lqtFMyanvkN6wM.htm](pathfinder-bestiary-2-items/A2lqtFMyanvkN6wM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[a3cYrmsIGVchwgtR.htm](pathfinder-bestiary-2-items/a3cYrmsIGVchwgtR.htm)|Pseudopod|Pseudópodo|modificada|
|[a3nL0MNgY6YXvvP9.htm](pathfinder-bestiary-2-items/a3nL0MNgY6YXvvP9.htm)|Jaws|Fauces|modificada|
|[A3RVgZkJQrtRoYi2.htm](pathfinder-bestiary-2-items/A3RVgZkJQrtRoYi2.htm)|Tusk|Tusk|modificada|
|[A46dQA87Lnn71IzJ.htm](pathfinder-bestiary-2-items/A46dQA87Lnn71IzJ.htm)|Gory Rend|Gory Rasgadura|modificada|
|[A5NdjSB3IdJS5DAb.htm](pathfinder-bestiary-2-items/A5NdjSB3IdJS5DAb.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[a8kHbScnPzkgg3j3.htm](pathfinder-bestiary-2-items/a8kHbScnPzkgg3j3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[a9NvSrt50HRCakM8.htm](pathfinder-bestiary-2-items/a9NvSrt50HRCakM8.htm)|Telepathy|Telepatía|modificada|
|[A9SIfdrB8Hk28o23.htm](pathfinder-bestiary-2-items/A9SIfdrB8Hk28o23.htm)|Jaws|Fauces|modificada|
|[AasR87d76iiM7CAd.htm](pathfinder-bestiary-2-items/AasR87d76iiM7CAd.htm)|Mandibles|Mandíbulas|modificada|
|[AAyJQ8CViWWMXWPU.htm](pathfinder-bestiary-2-items/AAyJQ8CViWWMXWPU.htm)|Jaws|Fauces|modificada|
|[Ab1sWqDegn71eEve.htm](pathfinder-bestiary-2-items/Ab1sWqDegn71eEve.htm)|Magma Tomb|Tumba de Magma|modificada|
|[Ab4r9QqPgjwOMjaW.htm](pathfinder-bestiary-2-items/Ab4r9QqPgjwOMjaW.htm)|Tail|Tail|modificada|
|[AbCU1QPQN43QSP6d.htm](pathfinder-bestiary-2-items/AbCU1QPQN43QSP6d.htm)|Speed Surge|Arranque de velocidad|modificada|
|[abGmJgduMKED2VeX.htm](pathfinder-bestiary-2-items/abGmJgduMKED2VeX.htm)|Chupar|Chupar|modificada|
|[aBIEnRecRBiBmpU4.htm](pathfinder-bestiary-2-items/aBIEnRecRBiBmpU4.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[AbJYjoA74dUGnY4h.htm](pathfinder-bestiary-2-items/AbJYjoA74dUGnY4h.htm)|Grab|Agarrado|modificada|
|[ABqHj13JWyqTUohA.htm](pathfinder-bestiary-2-items/ABqHj13JWyqTUohA.htm)|Misty Tendril|Misty Tendril|modificada|
|[acasfdr7NXNxUTjJ.htm](pathfinder-bestiary-2-items/acasfdr7NXNxUTjJ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[AcMqqRGXMA4crSZt.htm](pathfinder-bestiary-2-items/AcMqqRGXMA4crSZt.htm)|Burning Lash|Burning Lash|modificada|
|[adAFWbGgq7jnZcih.htm](pathfinder-bestiary-2-items/adAFWbGgq7jnZcih.htm)|Tail|Tail|modificada|
|[aDF1WE7DJM17CR29.htm](pathfinder-bestiary-2-items/aDF1WE7DJM17CR29.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ADUDwILKarUV2YPp.htm](pathfinder-bestiary-2-items/ADUDwILKarUV2YPp.htm)|Slow|Lentificado/a|modificada|
|[aeyCi4yAR1t8VtMz.htm](pathfinder-bestiary-2-items/aeyCi4yAR1t8VtMz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[aFiPts0pbbuzRaLf.htm](pathfinder-bestiary-2-items/aFiPts0pbbuzRaLf.htm)|Sudden Shove|Empujar de repente|modificada|
|[AfkzOHwQLaNCyPZt.htm](pathfinder-bestiary-2-items/AfkzOHwQLaNCyPZt.htm)|Jaws|Fauces|modificada|
|[AFlgcQtl6dJr1rtT.htm](pathfinder-bestiary-2-items/AFlgcQtl6dJr1rtT.htm)|Wolverine Rage|Wolverine Furia|modificada|
|[AFv5FWlpT2zyryiG.htm](pathfinder-bestiary-2-items/AFv5FWlpT2zyryiG.htm)|Swarm Mind|Swarm Mind|modificada|
|[AfwRVW7rPHMx2CVt.htm](pathfinder-bestiary-2-items/AfwRVW7rPHMx2CVt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[AGDYcCgG4mkmvihe.htm](pathfinder-bestiary-2-items/AGDYcCgG4mkmvihe.htm)|Breath Weapon|Breath Weapon|modificada|
|[AGm4GoMUxx5xAmpo.htm](pathfinder-bestiary-2-items/AGm4GoMUxx5xAmpo.htm)|Swarm Mind|Swarm Mind|modificada|
|[agTNdZe3k9VHuHav.htm](pathfinder-bestiary-2-items/agTNdZe3k9VHuHav.htm)|Constant Spells|Constant Spells|modificada|
|[AGZBKBnJMkp5NrlX.htm](pathfinder-bestiary-2-items/AGZBKBnJMkp5NrlX.htm)|Fangs|Colmillos|modificada|
|[aHamTcS3vkrZLHGN.htm](pathfinder-bestiary-2-items/aHamTcS3vkrZLHGN.htm)|Instant Suggestion|Sugestión instantónea.|modificada|
|[AhfsEZ9fBUS6e02P.htm](pathfinder-bestiary-2-items/AhfsEZ9fBUS6e02P.htm)|Poisonous Warts|Verrugas venenosas|modificada|
|[AHglfvlj1P2w1uM8.htm](pathfinder-bestiary-2-items/AHglfvlj1P2w1uM8.htm)|Nightmare Tendril|Pesadilla Tendril|modificada|
|[aHkhx52xZZTtPI25.htm](pathfinder-bestiary-2-items/aHkhx52xZZTtPI25.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[AhkuOBFHV5aW2vlI.htm](pathfinder-bestiary-2-items/AhkuOBFHV5aW2vlI.htm)|Grab|Agarrado|modificada|
|[Ai8J0A4OuhkDr7Cx.htm](pathfinder-bestiary-2-items/Ai8J0A4OuhkDr7Cx.htm)|Confusing Gaze|Mirada confusión|modificada|
|[AiGQomtSZwNKsTr9.htm](pathfinder-bestiary-2-items/AiGQomtSZwNKsTr9.htm)|Speedy Sabotage|Sabotaje veloz|modificada|
|[aisovzPKcYvcneL7.htm](pathfinder-bestiary-2-items/aisovzPKcYvcneL7.htm)|Throw Rock|Arrojar roca|modificada|
|[AJ6r1HXEEIIyvlnh.htm](pathfinder-bestiary-2-items/AJ6r1HXEEIIyvlnh.htm)|Regeneration 20 (Deactivated by Cold)|Regeneración 20 (Desactivado por el frío)|modificada|
|[AJ75IwgP2pUKWDa2.htm](pathfinder-bestiary-2-items/AJ75IwgP2pUKWDa2.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[aJhXMbTAQPFT4ySw.htm](pathfinder-bestiary-2-items/aJhXMbTAQPFT4ySw.htm)|Scurry|Scurry|modificada|
|[aJhxWGibV6WOF98e.htm](pathfinder-bestiary-2-items/aJhxWGibV6WOF98e.htm)|Beckoning Call|Llamada atrayente|modificada|
|[AjxbFK9De8rRPATh.htm](pathfinder-bestiary-2-items/AjxbFK9De8rRPATh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[AjxhxqAsiBgHNq9P.htm](pathfinder-bestiary-2-items/AjxhxqAsiBgHNq9P.htm)|Frightful Presence|Frightful Presence|modificada|
|[aKGDMqsWvw2cOIdE.htm](pathfinder-bestiary-2-items/aKGDMqsWvw2cOIdE.htm)|Power Attack|Ataque poderoso|modificada|
|[AKHyZQZIqFBrYLLC.htm](pathfinder-bestiary-2-items/AKHyZQZIqFBrYLLC.htm)|Tilting Strike|Golpe basculante|modificada|
|[AKOKPl5zfFti3EZh.htm](pathfinder-bestiary-2-items/AKOKPl5zfFti3EZh.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[ALbbJNuSFbicqOfo.htm](pathfinder-bestiary-2-items/ALbbJNuSFbicqOfo.htm)|Ripping Disarm|Desarme desgarrador|modificada|
|[AldxbbHYC1u6qB4A.htm](pathfinder-bestiary-2-items/AldxbbHYC1u6qB4A.htm)|Vulnerable to Gentle Repose|Vulnerable al apacible descanso|modificada|
|[Am3HvCv3WfgNu9sm.htm](pathfinder-bestiary-2-items/Am3HvCv3WfgNu9sm.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[AmgNn4ZAhXxVpdtv.htm](pathfinder-bestiary-2-items/AmgNn4ZAhXxVpdtv.htm)|Jaws|Fauces|modificada|
|[AMnTAvqDDSvhK4rN.htm](pathfinder-bestiary-2-items/AMnTAvqDDSvhK4rN.htm)|Salt Water Vulnerability|Vulnerabilidad al agua salada|modificada|
|[aNHpJbgvbOZz2Kpc.htm](pathfinder-bestiary-2-items/aNHpJbgvbOZz2Kpc.htm)|Fist|Puño|modificada|
|[AoE6IxAfyxcUD4Jo.htm](pathfinder-bestiary-2-items/AoE6IxAfyxcUD4Jo.htm)|Fist|Puño|modificada|
|[aOFgYZJ7qXDOoWDH.htm](pathfinder-bestiary-2-items/aOFgYZJ7qXDOoWDH.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[AoPor6Ll5rYacPzT.htm](pathfinder-bestiary-2-items/AoPor6Ll5rYacPzT.htm)|Club|Club|modificada|
|[APChG7D4rcYDlQ8m.htm](pathfinder-bestiary-2-items/APChG7D4rcYDlQ8m.htm)|Skrik Nettle Venom|Veneno de Ortiga Skrik|modificada|
|[APd8zemC3kgQClDQ.htm](pathfinder-bestiary-2-items/APd8zemC3kgQClDQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[aPrPuIOSdVOf8LwX.htm](pathfinder-bestiary-2-items/aPrPuIOSdVOf8LwX.htm)|Scent|Scent|modificada|
|[Aq9Qe7xGRm2JMoFA.htm](pathfinder-bestiary-2-items/Aq9Qe7xGRm2JMoFA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[aQDe7We9BV7qqiIm.htm](pathfinder-bestiary-2-items/aQDe7We9BV7qqiIm.htm)|Envelop|Amortajar|modificada|
|[AQeKaKMvD6zIOMpq.htm](pathfinder-bestiary-2-items/AQeKaKMvD6zIOMpq.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[aQUfPBPzpqLlkRM0.htm](pathfinder-bestiary-2-items/aQUfPBPzpqLlkRM0.htm)|Stygian Inquisitor|Inquisidor Estigio|modificada|
|[Ar1bSS3EJOZsaTMx.htm](pathfinder-bestiary-2-items/Ar1bSS3EJOZsaTMx.htm)|Jaws|Fauces|modificada|
|[Ar58HUSjCtqUCBWd.htm](pathfinder-bestiary-2-items/Ar58HUSjCtqUCBWd.htm)|Jaws|Fauces|modificada|
|[arLLxzC2PfddWyB8.htm](pathfinder-bestiary-2-items/arLLxzC2PfddWyB8.htm)|Whirlwind Blast|Ráfaga Torbellino|modificada|
|[ARmbgc05YhfVz4Vz.htm](pathfinder-bestiary-2-items/ARmbgc05YhfVz4Vz.htm)|Scent|Scent|modificada|
|[aRmGycM99xyVkKnJ.htm](pathfinder-bestiary-2-items/aRmGycM99xyVkKnJ.htm)|Caustic Blood|Caustic Blood|modificada|
|[aRTvrHip6K9YL4qq.htm](pathfinder-bestiary-2-items/aRTvrHip6K9YL4qq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[aSfa8TllpzF0cq1p.htm](pathfinder-bestiary-2-items/aSfa8TllpzF0cq1p.htm)|Jaws|Fauces|modificada|
|[aSFQMPlhrSWqprUV.htm](pathfinder-bestiary-2-items/aSFQMPlhrSWqprUV.htm)|Tremorsense|Tremorsense|modificada|
|[aT4z6HhNe6Rlop3y.htm](pathfinder-bestiary-2-items/aT4z6HhNe6Rlop3y.htm)|Tail|Tail|modificada|
|[atrOWDXVF87vS3KI.htm](pathfinder-bestiary-2-items/atrOWDXVF87vS3KI.htm)|Curse of the Crooked Cane|Maldición del bastón curvo|modificada|
|[aTTUtcIk2CfsuqEX.htm](pathfinder-bestiary-2-items/aTTUtcIk2CfsuqEX.htm)|Purity Vulnerability|Vulnerabilidad de Pureza|modificada|
|[AtW9S7uW8rnbEEtY.htm](pathfinder-bestiary-2-items/AtW9S7uW8rnbEEtY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[AU14pBO1YZPWAEH5.htm](pathfinder-bestiary-2-items/AU14pBO1YZPWAEH5.htm)|Telepathy|Telepatía|modificada|
|[aUo4Cxd0YTiFEabx.htm](pathfinder-bestiary-2-items/aUo4Cxd0YTiFEabx.htm)|Fist|Puño|modificada|
|[AUp2Wnd4EQNf6jKb.htm](pathfinder-bestiary-2-items/AUp2Wnd4EQNf6jKb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[auQwzWmfz2NSN0Rk.htm](pathfinder-bestiary-2-items/auQwzWmfz2NSN0Rk.htm)|Swarm Mind|Swarm Mind|modificada|
|[AuRaykn2A42pUIob.htm](pathfinder-bestiary-2-items/AuRaykn2A42pUIob.htm)|Jaws|Fauces|modificada|
|[avWzQnKIoQnTzxDr.htm](pathfinder-bestiary-2-items/avWzQnKIoQnTzxDr.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[AWGPMKkIerwV9Q8R.htm](pathfinder-bestiary-2-items/AWGPMKkIerwV9Q8R.htm)|Freezing Breath|Aliento gélido|modificada|
|[AWNveQn0EQ1aYORU.htm](pathfinder-bestiary-2-items/AWNveQn0EQ1aYORU.htm)|Rise Up|Levántate|modificada|
|[aWo3aVa2XXkGsrLY.htm](pathfinder-bestiary-2-items/aWo3aVa2XXkGsrLY.htm)|Final Spite|Final Spite|modificada|
|[awrAfsHw9vT0Y612.htm](pathfinder-bestiary-2-items/awrAfsHw9vT0Y612.htm)|Grab|Agarrado|modificada|
|[Ax2F85HRXfPDtO6x.htm](pathfinder-bestiary-2-items/Ax2F85HRXfPDtO6x.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ax8IJyIaesSgfUVD.htm](pathfinder-bestiary-2-items/ax8IJyIaesSgfUVD.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[aYcG8JHXY8UJvVcF.htm](pathfinder-bestiary-2-items/aYcG8JHXY8UJvVcF.htm)|Sticky Feet|Pies Pegajosos|modificada|
|[ayFG0wkvOvyHre59.htm](pathfinder-bestiary-2-items/ayFG0wkvOvyHre59.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[AySAWmXmOwHrjkuG.htm](pathfinder-bestiary-2-items/AySAWmXmOwHrjkuG.htm)|Vine|Vid|modificada|
|[AytjsrleVTlWADIi.htm](pathfinder-bestiary-2-items/AytjsrleVTlWADIi.htm)|Draining Presence|Presencia drenante|modificada|
|[AYVzOquWiUdDuNN3.htm](pathfinder-bestiary-2-items/AYVzOquWiUdDuNN3.htm)|Improved Grab|Agarrado mejorado|modificada|
|[azVkPps7BUkHzups.htm](pathfinder-bestiary-2-items/azVkPps7BUkHzups.htm)|Baleful Shriek|Baleful Shriek|modificada|
|[B0ACGOBzm6iH5fdI.htm](pathfinder-bestiary-2-items/B0ACGOBzm6iH5fdI.htm)|Push|Push|modificada|
|[B0vz4xw1pLJ9FeyD.htm](pathfinder-bestiary-2-items/B0vz4xw1pLJ9FeyD.htm)|Pounce|Abalanzarse|modificada|
|[B26U4dSQygOAKMgv.htm](pathfinder-bestiary-2-items/B26U4dSQygOAKMgv.htm)|Archon's Door|Puerta del arconte|modificada|
|[B2rF8mguredueRbP.htm](pathfinder-bestiary-2-items/B2rF8mguredueRbP.htm)|Hoof|Hoof|modificada|
|[b342tA8Gkde6f6zo.htm](pathfinder-bestiary-2-items/b342tA8Gkde6f6zo.htm)|Jaws|Fauces|modificada|
|[B3HRE3VYFmesNO4i.htm](pathfinder-bestiary-2-items/B3HRE3VYFmesNO4i.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[B4lZzVAkZocFD4OC.htm](pathfinder-bestiary-2-items/B4lZzVAkZocFD4OC.htm)|Hoof|Hoof|modificada|
|[b67N2gXUAWetXEVH.htm](pathfinder-bestiary-2-items/b67N2gXUAWetXEVH.htm)|Grab|Agarrado|modificada|
|[B6PM6Ak7BCncLxYT.htm](pathfinder-bestiary-2-items/B6PM6Ak7BCncLxYT.htm)|Trample|Trample|modificada|
|[B73QNqTjGdZQ9uba.htm](pathfinder-bestiary-2-items/B73QNqTjGdZQ9uba.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[B7bDAaNEMatWBkDD.htm](pathfinder-bestiary-2-items/B7bDAaNEMatWBkDD.htm)|Camouflage|Camuflaje|modificada|
|[b7ci3cZP3HOyvnjf.htm](pathfinder-bestiary-2-items/b7ci3cZP3HOyvnjf.htm)|Constant Spells|Constant Spells|modificada|
|[b9pe5dbiY9560pAA.htm](pathfinder-bestiary-2-items/b9pe5dbiY9560pAA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[b9sqjjGGkWII8xxu.htm](pathfinder-bestiary-2-items/b9sqjjGGkWII8xxu.htm)|Mimic Form|Mimic Form|modificada|
|[BA1HIu1Y9Xs74B9s.htm](pathfinder-bestiary-2-items/BA1HIu1Y9Xs74B9s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ba5N4O9vroJScmAq.htm](pathfinder-bestiary-2-items/ba5N4O9vroJScmAq.htm)|Misty Form|Misty Form|modificada|
|[BAJs3gJ34CxRZX5W.htm](pathfinder-bestiary-2-items/BAJs3gJ34CxRZX5W.htm)|Fair Competition|Competencia leal|modificada|
|[BbeKEw6uMpYbSxue.htm](pathfinder-bestiary-2-items/BbeKEw6uMpYbSxue.htm)|Mandibles|Mandíbulas|modificada|
|[bBQMy1lr5xW6rBgj.htm](pathfinder-bestiary-2-items/bBQMy1lr5xW6rBgj.htm)|Temporal Strike|Golpe Temporal|modificada|
|[BbzYprMJVagq4zOn.htm](pathfinder-bestiary-2-items/BbzYprMJVagq4zOn.htm)|Gust|Gust|modificada|
|[bC1xvgHpQUnix9Bt.htm](pathfinder-bestiary-2-items/bC1xvgHpQUnix9Bt.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[bcYwNYN6KGrhljcb.htm](pathfinder-bestiary-2-items/bcYwNYN6KGrhljcb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BczEX9fZ2q5qXxKI.htm](pathfinder-bestiary-2-items/BczEX9fZ2q5qXxKI.htm)|Rock Tunneler|Excavar roca|modificada|
|[BdJy1HqrIlkJp8rF.htm](pathfinder-bestiary-2-items/BdJy1HqrIlkJp8rF.htm)|Animated Hair|Pelo Animado|modificada|
|[BdObP5U0rM3c59uD.htm](pathfinder-bestiary-2-items/BdObP5U0rM3c59uD.htm)|Scent|Scent|modificada|
|[BdPJLN1AK4im8nIu.htm](pathfinder-bestiary-2-items/BdPJLN1AK4im8nIu.htm)|Breath Weapon|Breath Weapon|modificada|
|[bEAhPwJXnrqxPt5i.htm](pathfinder-bestiary-2-items/bEAhPwJXnrqxPt5i.htm)|Blend with Light|Mezclar con la luz|modificada|
|[bEnEcBaXcq0xrckT.htm](pathfinder-bestiary-2-items/bEnEcBaXcq0xrckT.htm)|Flawless Hearing|Flawless Hearing|modificada|
|[bezkLxOASRaFHd9O.htm](pathfinder-bestiary-2-items/bezkLxOASRaFHd9O.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[BfQDGpd8cSNGgENd.htm](pathfinder-bestiary-2-items/BfQDGpd8cSNGgENd.htm)|Retract Tongue|Retraer Lengua|modificada|
|[BfXMGYIBKEZIaKVV.htm](pathfinder-bestiary-2-items/BfXMGYIBKEZIaKVV.htm)|Evisceration|Evisceración|modificada|
|[bIfz2rvBHXjIjmyK.htm](pathfinder-bestiary-2-items/bIfz2rvBHXjIjmyK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BIIDNJTjlW3lJ1b2.htm](pathfinder-bestiary-2-items/BIIDNJTjlW3lJ1b2.htm)|Volcanic Veins|Venas Volcánicas|modificada|
|[BIjOxLctW7PgHsOt.htm](pathfinder-bestiary-2-items/BIjOxLctW7PgHsOt.htm)|Compsognathus Venom|Compsognathus Venom|modificada|
|[bj3LBfUDYYqFHskT.htm](pathfinder-bestiary-2-items/bj3LBfUDYYqFHskT.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[BJelgf4pdEHXMd9u.htm](pathfinder-bestiary-2-items/BJelgf4pdEHXMd9u.htm)|Change Shape|Change Shape|modificada|
|[bJUTz62klSlUEuTo.htm](pathfinder-bestiary-2-items/bJUTz62klSlUEuTo.htm)|Drink Blood|Beber Sangre|modificada|
|[bJVD9JQb7YrlkdAs.htm](pathfinder-bestiary-2-items/bJVD9JQb7YrlkdAs.htm)|Void Child|Niño Vacío|modificada|
|[bJzQGcgwU0vilj1V.htm](pathfinder-bestiary-2-items/bJzQGcgwU0vilj1V.htm)|Culdewen's Curse|Maldición de Culdewen|modificada|
|[bk1rrI0FiHN6P4an.htm](pathfinder-bestiary-2-items/bk1rrI0FiHN6P4an.htm)|Rapid Stinging|Rapid Stinging|modificada|
|[Bk63QeRIxXqBysrQ.htm](pathfinder-bestiary-2-items/Bk63QeRIxXqBysrQ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[bKdGH4QNZyOm2rhg.htm](pathfinder-bestiary-2-items/bKdGH4QNZyOm2rhg.htm)|Land the Fish|Land the Fish|modificada|
|[bKkmonWLmkkxYnh2.htm](pathfinder-bestiary-2-items/bKkmonWLmkkxYnh2.htm)|Camouflage|Camuflaje|modificada|
|[BkLQtw75VewmqYSQ.htm](pathfinder-bestiary-2-items/BkLQtw75VewmqYSQ.htm)|Jaws|Fauces|modificada|
|[BkmdLBAIea36UOWC.htm](pathfinder-bestiary-2-items/BkmdLBAIea36UOWC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BKpd3Xf4hwQIvXYT.htm](pathfinder-bestiary-2-items/BKpd3Xf4hwQIvXYT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bl1xW54BlfiFnU8h.htm](pathfinder-bestiary-2-items/bl1xW54BlfiFnU8h.htm)|Catch Rock|Atrapar roca|modificada|
|[bLGN7Biu5pB1bqtL.htm](pathfinder-bestiary-2-items/bLGN7Biu5pB1bqtL.htm)|Tongue|Lengua|modificada|
|[BmhdDSnmoRYLlefv.htm](pathfinder-bestiary-2-items/BmhdDSnmoRYLlefv.htm)|Consumptive Aura|Aura consumista|modificada|
|[bMLV80N79GMyKDyo.htm](pathfinder-bestiary-2-items/bMLV80N79GMyKDyo.htm)|Hand Crossbow (Serpentfolk Venom)|Ballesta de mano (Serpentfolk Venom)|modificada|
|[BmSUiM1ndU9nWVml.htm](pathfinder-bestiary-2-items/BmSUiM1ndU9nWVml.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BMv1jkvT4dSbEL8X.htm](pathfinder-bestiary-2-items/BMv1jkvT4dSbEL8X.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[bMW69DQKTMLqLasp.htm](pathfinder-bestiary-2-items/bMW69DQKTMLqLasp.htm)|Rider's Bond|Rider's Bond|modificada|
|[bMX8ZvBxeu0Ty5cl.htm](pathfinder-bestiary-2-items/bMX8ZvBxeu0Ty5cl.htm)|Devour Soul|Devorar alma|modificada|
|[bMypsTuAesTEDTll.htm](pathfinder-bestiary-2-items/bMypsTuAesTEDTll.htm)|Constant Spells|Constant Spells|modificada|
|[bN7RCHuZpCHYpb2L.htm](pathfinder-bestiary-2-items/bN7RCHuZpCHYpb2L.htm)|Enraged Growth|Crecimiento Enfurecido|modificada|
|[bNbPPpEgnT6EWV5B.htm](pathfinder-bestiary-2-items/bNbPPpEgnT6EWV5B.htm)|Claw|Garra|modificada|
|[BNs4e17E8TlmQdRh.htm](pathfinder-bestiary-2-items/BNs4e17E8TlmQdRh.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[Bo7Y34vNrq537uhA.htm](pathfinder-bestiary-2-items/Bo7Y34vNrq537uhA.htm)|Tail|Tail|modificada|
|[boC55Edo1J7Heynq.htm](pathfinder-bestiary-2-items/boC55Edo1J7Heynq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bOe2DBNsKt2ZttfX.htm](pathfinder-bestiary-2-items/bOe2DBNsKt2ZttfX.htm)|Spectral Hand|Mano espectral|modificada|
|[BOGJAY8I1abQRm7S.htm](pathfinder-bestiary-2-items/BOGJAY8I1abQRm7S.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[boOcmJoGp5w8sRkS.htm](pathfinder-bestiary-2-items/boOcmJoGp5w8sRkS.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Bpm9xvzcp3dBpCvp.htm](pathfinder-bestiary-2-items/Bpm9xvzcp3dBpCvp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[bQ7xqBShE3D6pEMf.htm](pathfinder-bestiary-2-items/bQ7xqBShE3D6pEMf.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[BQDQsCBmg1gB3VSr.htm](pathfinder-bestiary-2-items/BQDQsCBmg1gB3VSr.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[bQyms0FzVu1O5MrR.htm](pathfinder-bestiary-2-items/bQyms0FzVu1O5MrR.htm)|Gnaw Metal|Gnaw Metal|modificada|
|[bR35AECKKuKfjL3N.htm](pathfinder-bestiary-2-items/bR35AECKKuKfjL3N.htm)|Constant Spells|Constant Spells|modificada|
|[bR7DFykvg3BJRRmV.htm](pathfinder-bestiary-2-items/bR7DFykvg3BJRRmV.htm)|Dream Spider Venom|Sueño Araña Veneno|modificada|
|[BRAhQrI56OQkBhNy.htm](pathfinder-bestiary-2-items/BRAhQrI56OQkBhNy.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[bRIQtOJZdj1Z9gcD.htm](pathfinder-bestiary-2-items/bRIQtOJZdj1Z9gcD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[brun3T7XF65PG2xu.htm](pathfinder-bestiary-2-items/brun3T7XF65PG2xu.htm)|Woodland Stride|Paso forestal|modificada|
|[bS27ihuZ0Xs3mU7a.htm](pathfinder-bestiary-2-items/bS27ihuZ0Xs3mU7a.htm)|Swipe|Vaivén|modificada|
|[BsKlvqGTEEIQo1cx.htm](pathfinder-bestiary-2-items/BsKlvqGTEEIQo1cx.htm)|+4 Bonus on Perception to Detect Illusions|Bonificación +4 a la percepción para detectar ilusión.|modificada|
|[BsxGfhkuyAGxetGI.htm](pathfinder-bestiary-2-items/BsxGfhkuyAGxetGI.htm)|Frightful Presence|Frightful Presence|modificada|
|[BtLt92JdieUMFygu.htm](pathfinder-bestiary-2-items/BtLt92JdieUMFygu.htm)|Regeneration 20 (Deactivated by Acid or Sonic)|Regeneración 20 (Desactivado por Ácido o Sonic)|modificada|
|[bu97rur2OngJhlHv.htm](pathfinder-bestiary-2-items/bu97rur2OngJhlHv.htm)|Titan Centipede Venom|Veneno de Ciempiés Titán|modificada|
|[BVXX982lJiCTVido.htm](pathfinder-bestiary-2-items/BVXX982lJiCTVido.htm)|Bloodletting|Sangría|modificada|
|[bW6X64ql7gjX9zqQ.htm](pathfinder-bestiary-2-items/bW6X64ql7gjX9zqQ.htm)|Entangling Tendrils|Zarcillos enmarados|modificada|
|[bwZ7BYouujVO39kJ.htm](pathfinder-bestiary-2-items/bwZ7BYouujVO39kJ.htm)|Draconic Resistance|Resistencia dracónico|modificada|
|[bxjYZ892TLi9AizW.htm](pathfinder-bestiary-2-items/bxjYZ892TLi9AizW.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[BXp3Iqh8AykJCF9i.htm](pathfinder-bestiary-2-items/BXp3Iqh8AykJCF9i.htm)|Talon|Talon|modificada|
|[ByGE43xowJglhHxP.htm](pathfinder-bestiary-2-items/ByGE43xowJglhHxP.htm)|Reactive Strikes|Golpes reactivos|modificada|
|[bYqHLnt7oqthGfZi.htm](pathfinder-bestiary-2-items/bYqHLnt7oqthGfZi.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[BYqZd7dW5USVQMnx.htm](pathfinder-bestiary-2-items/BYqZd7dW5USVQMnx.htm)|Rip and Tear|Rip and Tear|modificada|
|[bysIJ8d5SoGqSABO.htm](pathfinder-bestiary-2-items/bysIJ8d5SoGqSABO.htm)|Implant|Implante|modificada|
|[bYuZrlYxVWLd0lD3.htm](pathfinder-bestiary-2-items/bYuZrlYxVWLd0lD3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[BZfKFoVLNm5bmKHc.htm](pathfinder-bestiary-2-items/BZfKFoVLNm5bmKHc.htm)|Mimic Shadow|Sombra Mímica|modificada|
|[c1COFBWgk0baYBNq.htm](pathfinder-bestiary-2-items/c1COFBWgk0baYBNq.htm)|Constant Spells|Constant Spells|modificada|
|[c2nQF7qTMD3mwnrM.htm](pathfinder-bestiary-2-items/c2nQF7qTMD3mwnrM.htm)|Stingray Venom|Stingray Venom|modificada|
|[c4mDZBMrkh9kwIHn.htm](pathfinder-bestiary-2-items/c4mDZBMrkh9kwIHn.htm)|Negative Healing|Curación negativa|modificada|
|[C4MfMpmGULlKKhXy.htm](pathfinder-bestiary-2-items/C4MfMpmGULlKKhXy.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[C6IiDSyc4X1neQwS.htm](pathfinder-bestiary-2-items/C6IiDSyc4X1neQwS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[C6LpIB1Gy5mjBk2G.htm](pathfinder-bestiary-2-items/C6LpIB1Gy5mjBk2G.htm)|Jaws|Fauces|modificada|
|[c6VHX9kLiVuag6R0.htm](pathfinder-bestiary-2-items/c6VHX9kLiVuag6R0.htm)|Foot|Pie|modificada|
|[c74H6Oxpa5sZzsN5.htm](pathfinder-bestiary-2-items/c74H6Oxpa5sZzsN5.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[C7C9dLMFwNtAccbA.htm](pathfinder-bestiary-2-items/C7C9dLMFwNtAccbA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[c7V9HgCpZq3Unkja.htm](pathfinder-bestiary-2-items/c7V9HgCpZq3Unkja.htm)|Web|Telara|modificada|
|[C91ggah5Ye65LrMH.htm](pathfinder-bestiary-2-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|Leng Ruby|modificada|
|[c9Aq0MMPKh2wKXXy.htm](pathfinder-bestiary-2-items/c9Aq0MMPKh2wKXXy.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[CAKfIK6WRhkgpCW2.htm](pathfinder-bestiary-2-items/CAKfIK6WRhkgpCW2.htm)|Living Footsteps|Pasos Vivientes|modificada|
|[CApABKQSTABj5rMd.htm](pathfinder-bestiary-2-items/CApABKQSTABj5rMd.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[cb6s0BQEA2EVpswg.htm](pathfinder-bestiary-2-items/cb6s0BQEA2EVpswg.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[cCv1bOaozCwd4fB5.htm](pathfinder-bestiary-2-items/cCv1bOaozCwd4fB5.htm)|Claw|Garra|modificada|
|[CDAurAI8XPZBeHsL.htm](pathfinder-bestiary-2-items/CDAurAI8XPZBeHsL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[CDI8zDOfYxM1bdM6.htm](pathfinder-bestiary-2-items/CDI8zDOfYxM1bdM6.htm)|Aura of Order's Ruin|Aura de Ruina del Orden|modificada|
|[cDS4azJaDaRZITrQ.htm](pathfinder-bestiary-2-items/cDS4azJaDaRZITrQ.htm)|Cold Vulnerability|Vulnerabilidad al frío|modificada|
|[cE9GHCJxhOHcgxgi.htm](pathfinder-bestiary-2-items/cE9GHCJxhOHcgxgi.htm)|Shattering Ice|Hielo desmenuzado|modificada|
|[CeExWr00cFSdrZ4J.htm](pathfinder-bestiary-2-items/CeExWr00cFSdrZ4J.htm)|Buck|Encabritarse|modificada|
|[CEREyXabBiMwUJlQ.htm](pathfinder-bestiary-2-items/CEREyXabBiMwUJlQ.htm)|Push|Push|modificada|
|[CFncnCOk0MBxjpim.htm](pathfinder-bestiary-2-items/CFncnCOk0MBxjpim.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CGd3B4KwGynCutU4.htm](pathfinder-bestiary-2-items/CGd3B4KwGynCutU4.htm)|Vulnerable to Shape Wood|Vulnerable a moldear la madera|modificada|
|[cGXZfGlgZYjzMBP8.htm](pathfinder-bestiary-2-items/cGXZfGlgZYjzMBP8.htm)|Tremorsense|Tremorsense|modificada|
|[cHiKqfceiHjpXqNg.htm](pathfinder-bestiary-2-items/cHiKqfceiHjpXqNg.htm)|Constant Spells|Constant Spells|modificada|
|[chnUQOapjPm4tXAS.htm](pathfinder-bestiary-2-items/chnUQOapjPm4tXAS.htm)|Calming Presence|Presencia Calmante|modificada|
|[ChobeFNcesw2DX01.htm](pathfinder-bestiary-2-items/ChobeFNcesw2DX01.htm)|Fangs|Colmillos|modificada|
|[CHpqAmXq4vOr2TMj.htm](pathfinder-bestiary-2-items/CHpqAmXq4vOr2TMj.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[chTQW4y1K5vGGjVn.htm](pathfinder-bestiary-2-items/chTQW4y1K5vGGjVn.htm)|Foot|Pie|modificada|
|[CiQKBNNHPATwPDUX.htm](pathfinder-bestiary-2-items/CiQKBNNHPATwPDUX.htm)|Shortsword|Espada corta|modificada|
|[cJ2jYiJRZR43AJCJ.htm](pathfinder-bestiary-2-items/cJ2jYiJRZR43AJCJ.htm)|Double Slash|Doble barra|modificada|
|[Cj8UIWLOhEQ5yNgA.htm](pathfinder-bestiary-2-items/Cj8UIWLOhEQ5yNgA.htm)|Body Strike|Golpe al cuerpo|modificada|
|[cJf7dfR7gJ442K2d.htm](pathfinder-bestiary-2-items/cJf7dfR7gJ442K2d.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[cJFgwvR7wtsldJBw.htm](pathfinder-bestiary-2-items/cJFgwvR7wtsldJBw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cjFyZppcDxVUaRJD.htm](pathfinder-bestiary-2-items/cjFyZppcDxVUaRJD.htm)|Drink Blood|Beber Sangre|modificada|
|[cjXnGrEMkDDki8sC.htm](pathfinder-bestiary-2-items/cjXnGrEMkDDki8sC.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[cjYLiElA3muRcJuo.htm](pathfinder-bestiary-2-items/cjYLiElA3muRcJuo.htm)|Grotesque Gift|Grotesque Gift|modificada|
|[CkFeQyjFwtsMRJY8.htm](pathfinder-bestiary-2-items/CkFeQyjFwtsMRJY8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ckpzKVXKEEwbtEV4.htm](pathfinder-bestiary-2-items/ckpzKVXKEEwbtEV4.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Cl2tanuHvlyyksIz.htm](pathfinder-bestiary-2-items/Cl2tanuHvlyyksIz.htm)|Frightful Presence|Frightful Presence|modificada|
|[cLUNjff4ATgKuIj8.htm](pathfinder-bestiary-2-items/cLUNjff4ATgKuIj8.htm)|Wicked Bite|Muerdemuerde|modificada|
|[cLWUZTt9knGddxzH.htm](pathfinder-bestiary-2-items/cLWUZTt9knGddxzH.htm)|Constant Spells|Constant Spells|modificada|
|[clxrPF7TPJ4sS2A6.htm](pathfinder-bestiary-2-items/clxrPF7TPJ4sS2A6.htm)|Extend Mandibles|Extend Mandibles|modificada|
|[CmBRVSLQzMzJ4WUR.htm](pathfinder-bestiary-2-items/CmBRVSLQzMzJ4WUR.htm)|Lifesense 120 feet|Lifesense 120 pies|modificada|
|[cMrE7oz3K11aJZtC.htm](pathfinder-bestiary-2-items/cMrE7oz3K11aJZtC.htm)|Glimpse of Stolen Flesh|Atisbo de carne robada|modificada|
|[CNilhCQNrj60lElM.htm](pathfinder-bestiary-2-items/CNilhCQNrj60lElM.htm)|Beak|Beak|modificada|
|[cNnj1mdRc6n47fUX.htm](pathfinder-bestiary-2-items/cNnj1mdRc6n47fUX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[coCCw9uVbyTHhKfl.htm](pathfinder-bestiary-2-items/coCCw9uVbyTHhKfl.htm)|Jaws|Fauces|modificada|
|[cOypeEDeaznFsSdO.htm](pathfinder-bestiary-2-items/cOypeEDeaznFsSdO.htm)|Ravenous Attack|Ravenous Attack|modificada|
|[Cp1loMnqFXw0uGPS.htm](pathfinder-bestiary-2-items/Cp1loMnqFXw0uGPS.htm)|Jaws|Fauces|modificada|
|[cPD60wC9FBtIdzPL.htm](pathfinder-bestiary-2-items/cPD60wC9FBtIdzPL.htm)|Head Regrowth|Crecer cabezas|modificada|
|[CpDQG0xz9auLn9gr.htm](pathfinder-bestiary-2-items/CpDQG0xz9auLn9gr.htm)|Double Punch|Doble Puñetazo|modificada|
|[cpSC9RcShIFK28EU.htm](pathfinder-bestiary-2-items/cpSC9RcShIFK28EU.htm)|Snout|Hocico|modificada|
|[CpTRDv42ZCA5jHZL.htm](pathfinder-bestiary-2-items/CpTRDv42ZCA5jHZL.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[CpVMSme8RWwiT0UY.htm](pathfinder-bestiary-2-items/CpVMSme8RWwiT0UY.htm)|Fast Healing 2 (in boiling water or steam)|Curación rápida 2 (en agua hirviendo o vapor)|modificada|
|[CQ9x9wMLcGSsM5wK.htm](pathfinder-bestiary-2-items/CQ9x9wMLcGSsM5wK.htm)|Web|Telara|modificada|
|[cQH5uYYBW0hseijL.htm](pathfinder-bestiary-2-items/cQH5uYYBW0hseijL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cqI7kqooduGDEqz6.htm](pathfinder-bestiary-2-items/cqI7kqooduGDEqz6.htm)|Warhammer|Warhammer|modificada|
|[cQTIij6l7HjJwqQW.htm](pathfinder-bestiary-2-items/cQTIij6l7HjJwqQW.htm)|Telepathy|Telepatía|modificada|
|[CquGR1meZf8Szzmo.htm](pathfinder-bestiary-2-items/CquGR1meZf8Szzmo.htm)|Splinter|Splinter|modificada|
|[CR1qaOcpKevyIuQj.htm](pathfinder-bestiary-2-items/CR1qaOcpKevyIuQj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cRTNLnVuzisUG7kl.htm](pathfinder-bestiary-2-items/cRTNLnVuzisUG7kl.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[CRuPtzSYQoeDAWbH.htm](pathfinder-bestiary-2-items/CRuPtzSYQoeDAWbH.htm)|Swarm Mind|Swarm Mind|modificada|
|[cS4RQnXm1y1xugCt.htm](pathfinder-bestiary-2-items/cS4RQnXm1y1xugCt.htm)|Claw|Garra|modificada|
|[CSHQvYrfiR0GmczO.htm](pathfinder-bestiary-2-items/CSHQvYrfiR0GmczO.htm)|Brine Spit|Salmuera|modificada|
|[CsNzwFbWatN4ivRP.htm](pathfinder-bestiary-2-items/CsNzwFbWatN4ivRP.htm)|Hurl Weapon|Arma Arrojadiza|modificada|
|[CsrgxIJmjkumjsBJ.htm](pathfinder-bestiary-2-items/CsrgxIJmjkumjsBJ.htm)|Trample|Trample|modificada|
|[CSSsFGyexhD5YUX3.htm](pathfinder-bestiary-2-items/CSSsFGyexhD5YUX3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CswqTh16aa0KDlDF.htm](pathfinder-bestiary-2-items/CswqTh16aa0KDlDF.htm)|Necrophidic Paralysis|Necrophidic Paralysis|modificada|
|[CsYqT7LTtX34B3Yz.htm](pathfinder-bestiary-2-items/CsYqT7LTtX34B3Yz.htm)|Breath Weapon|Breath Weapon|modificada|
|[CTeYc8gFkSPfWH7F.htm](pathfinder-bestiary-2-items/CTeYc8gFkSPfWH7F.htm)|Jaws|Fauces|modificada|
|[CThMS3QhSA3Obh7Z.htm](pathfinder-bestiary-2-items/CThMS3QhSA3Obh7Z.htm)|Breath Weapon|Breath Weapon|modificada|
|[Ctng3iJDIMkFh490.htm](pathfinder-bestiary-2-items/Ctng3iJDIMkFh490.htm)|Tail|Tail|modificada|
|[ctRDT8yPATnd3gxe.htm](pathfinder-bestiary-2-items/ctRDT8yPATnd3gxe.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[cttVijuQGlQJk5Ux.htm](pathfinder-bestiary-2-items/cttVijuQGlQJk5Ux.htm)|Grab|Agarrado|modificada|
|[CUcADH9CrR1t2kkL.htm](pathfinder-bestiary-2-items/CUcADH9CrR1t2kkL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cuHB5zR7MVWma2MD.htm](pathfinder-bestiary-2-items/cuHB5zR7MVWma2MD.htm)|Cloud Form|Cloud Form|modificada|
|[CUl4eRbkbnzMJ2kZ.htm](pathfinder-bestiary-2-items/CUl4eRbkbnzMJ2kZ.htm)|Vulnerable to Endure Elements|Vulnerable a soportar los elementos|modificada|
|[CuMZTwnGZC3P7sm5.htm](pathfinder-bestiary-2-items/CuMZTwnGZC3P7sm5.htm)|Spiked Tail|Spiked Tail|modificada|
|[cUtNYZ935fPsgj8x.htm](pathfinder-bestiary-2-items/cUtNYZ935fPsgj8x.htm)|Brine Spit|Salmuera|modificada|
|[cVEH8toDyk2cQnEC.htm](pathfinder-bestiary-2-items/cVEH8toDyk2cQnEC.htm)|Breath Weapon|Breath Weapon|modificada|
|[cvtGewKiksGkDNKz.htm](pathfinder-bestiary-2-items/cvtGewKiksGkDNKz.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[CwEVrNGC8oHJQrv0.htm](pathfinder-bestiary-2-items/CwEVrNGC8oHJQrv0.htm)|Isqulugia|Isqulugia|modificada|
|[cWj2CNZYq3EWCKZF.htm](pathfinder-bestiary-2-items/cWj2CNZYq3EWCKZF.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[CwjWGtOVvGZH0bP8.htm](pathfinder-bestiary-2-items/CwjWGtOVvGZH0bP8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CwNuoWOgc2U8qCLd.htm](pathfinder-bestiary-2-items/CwNuoWOgc2U8qCLd.htm)|Knockdown|Derribo|modificada|
|[CwSvR6GqNWvMMxwR.htm](pathfinder-bestiary-2-items/CwSvR6GqNWvMMxwR.htm)|Water Stride|Zancada acuática|modificada|
|[CwxYoKcoC0dSFuhX.htm](pathfinder-bestiary-2-items/CwxYoKcoC0dSFuhX.htm)|Mind-Numbing Touch|Mind-Numbing Touch|modificada|
|[CXc8oBEPlsp8HDTd.htm](pathfinder-bestiary-2-items/CXc8oBEPlsp8HDTd.htm)|Staff|Báculo|modificada|
|[CxhKccyCVc1NDZFs.htm](pathfinder-bestiary-2-items/CxhKccyCVc1NDZFs.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Cxm87AR1TJom2drt.htm](pathfinder-bestiary-2-items/Cxm87AR1TJom2drt.htm)|Double Claw|Doble Garra|modificada|
|[CYtByJ7KaQiXTAWv.htm](pathfinder-bestiary-2-items/CYtByJ7KaQiXTAWv.htm)|Retributive Strike|Golpe retributivo|modificada|
|[czBySBdDeH7WSORn.htm](pathfinder-bestiary-2-items/czBySBdDeH7WSORn.htm)|Spell Pilfer|Spell Pilfer|modificada|
|[D0L9Y3ZIku07uMOk.htm](pathfinder-bestiary-2-items/D0L9Y3ZIku07uMOk.htm)|Fated|Fated|modificada|
|[D13sjvjuSrxFEe35.htm](pathfinder-bestiary-2-items/D13sjvjuSrxFEe35.htm)|No Breath|No Breath|modificada|
|[D1QeVZDcMpO7hdmk.htm](pathfinder-bestiary-2-items/D1QeVZDcMpO7hdmk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D1R03gCQbWQLLgtL.htm](pathfinder-bestiary-2-items/D1R03gCQbWQLLgtL.htm)|Splinter Volley|Splinter Volea|modificada|
|[d2h0zilfslOI1NGH.htm](pathfinder-bestiary-2-items/d2h0zilfslOI1NGH.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[D3ePCbVgWGtm6c2e.htm](pathfinder-bestiary-2-items/D3ePCbVgWGtm6c2e.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[D3z26xV27YGuSjtp.htm](pathfinder-bestiary-2-items/D3z26xV27YGuSjtp.htm)|Glaive|Glaive|modificada|
|[d5CWT1AwKpYtsajX.htm](pathfinder-bestiary-2-items/d5CWT1AwKpYtsajX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D5jAYC0XRC68X1gB.htm](pathfinder-bestiary-2-items/D5jAYC0XRC68X1gB.htm)|Greater Constrict|Mayor Restricción|modificada|
|[D5p47qiv8jIAcjoL.htm](pathfinder-bestiary-2-items/D5p47qiv8jIAcjoL.htm)|Claw|Garra|modificada|
|[d68HrWYDDAvuRFIR.htm](pathfinder-bestiary-2-items/d68HrWYDDAvuRFIR.htm)|Redirect Fire|Redirigir fuego|modificada|
|[d7HHzvbGGdWbPEMo.htm](pathfinder-bestiary-2-items/d7HHzvbGGdWbPEMo.htm)|Claw|Garra|modificada|
|[d7lY0T9fiGJzH2O1.htm](pathfinder-bestiary-2-items/d7lY0T9fiGJzH2O1.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[D7nU92x03rn3cnoU.htm](pathfinder-bestiary-2-items/D7nU92x03rn3cnoU.htm)|Jaws|Fauces|modificada|
|[D89GcjloR9CLNbq8.htm](pathfinder-bestiary-2-items/D89GcjloR9CLNbq8.htm)|Elemental Bulwark|Baluarte elemental|modificada|
|[D8xygwrc0WQBOje8.htm](pathfinder-bestiary-2-items/D8xygwrc0WQBOje8.htm)|Negative Healing|Curación negativa|modificada|
|[Da6b96ZUkpdT5y8i.htm](pathfinder-bestiary-2-items/Da6b96ZUkpdT5y8i.htm)|Reel In|Arrastrar presa|modificada|
|[DA9qmvZn2NX8sUkj.htm](pathfinder-bestiary-2-items/DA9qmvZn2NX8sUkj.htm)|Jet|Jet|modificada|
|[Dax6qfY1KhJqryzV.htm](pathfinder-bestiary-2-items/Dax6qfY1KhJqryzV.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[dbDXIpmXMwGGSF0O.htm](pathfinder-bestiary-2-items/dbDXIpmXMwGGSF0O.htm)|Breath Weapon|Breath Weapon|modificada|
|[Dbrk210Q9t44P8vP.htm](pathfinder-bestiary-2-items/Dbrk210Q9t44P8vP.htm)|Claw|Garra|modificada|
|[DBzHso7FlJtYCeiU.htm](pathfinder-bestiary-2-items/DBzHso7FlJtYCeiU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[DC3068Ne4eQKeeW1.htm](pathfinder-bestiary-2-items/DC3068Ne4eQKeeW1.htm)|Create Spawn|Elaborar Spawn|modificada|
|[dCgNsIZTnOJTOkca.htm](pathfinder-bestiary-2-items/dCgNsIZTnOJTOkca.htm)|Telepathy|Telepatía|modificada|
|[DcQizfyqoLcJASk7.htm](pathfinder-bestiary-2-items/DcQizfyqoLcJASk7.htm)|Surprise Attacker|Atacante por sorpresa|modificada|
|[DDI0cQeKx8SLhTUR.htm](pathfinder-bestiary-2-items/DDI0cQeKx8SLhTUR.htm)|Swift Swimmer|Nadador rápido|modificada|
|[de7GnRXapzBCdDoL.htm](pathfinder-bestiary-2-items/de7GnRXapzBCdDoL.htm)|Infuse Weapon|Infundir armas|modificada|
|[de8ASlT2fs4snv5M.htm](pathfinder-bestiary-2-items/de8ASlT2fs4snv5M.htm)|Knockdown|Derribo|modificada|
|[DEac5zIyNqdQYdnc.htm](pathfinder-bestiary-2-items/DEac5zIyNqdQYdnc.htm)|Grab|Agarrado|modificada|
|[dEH0bojR7ga9l8vX.htm](pathfinder-bestiary-2-items/dEH0bojR7ga9l8vX.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[dEiJsJoECS4hvDnB.htm](pathfinder-bestiary-2-items/dEiJsJoECS4hvDnB.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[deytOmtiZjHFzGRU.htm](pathfinder-bestiary-2-items/deytOmtiZjHFzGRU.htm)|Athach Venom|Athach Venom|modificada|
|[DEz7gcf0AumMmDO8.htm](pathfinder-bestiary-2-items/DEz7gcf0AumMmDO8.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[dF7LLSCwf5DeGcNU.htm](pathfinder-bestiary-2-items/dF7LLSCwf5DeGcNU.htm)|Rend|Rasgadura|modificada|
|[DfbcYHdJzDXVkgEs.htm](pathfinder-bestiary-2-items/DfbcYHdJzDXVkgEs.htm)|Swallow Whole|Engullir Todo|modificada|
|[DfefDz8ZxlBybpoa.htm](pathfinder-bestiary-2-items/DfefDz8ZxlBybpoa.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[dFKXMQ8FbHDHNhTC.htm](pathfinder-bestiary-2-items/dFKXMQ8FbHDHNhTC.htm)|Quick Bomber|Bombardero rápido|modificada|
|[dfO2vkmoVzinZBdb.htm](pathfinder-bestiary-2-items/dfO2vkmoVzinZBdb.htm)|Constant Spells|Constant Spells|modificada|
|[dFTh6F51ONxZqLHO.htm](pathfinder-bestiary-2-items/dFTh6F51ONxZqLHO.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[DfyikbxEMRuzW77m.htm](pathfinder-bestiary-2-items/DfyikbxEMRuzW77m.htm)|Swarm Shape|Forma de Enjambre|modificada|
|[DFZPLEk0MQcqSi9v.htm](pathfinder-bestiary-2-items/DFZPLEk0MQcqSi9v.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[dG6wdlMkywKsurgA.htm](pathfinder-bestiary-2-items/dG6wdlMkywKsurgA.htm)|Fist|Puño|modificada|
|[DI1RbgDDypEQJCMq.htm](pathfinder-bestiary-2-items/DI1RbgDDypEQJCMq.htm)|Steal Breath|Sustraer Aliento|modificada|
|[DJ4DzpxUnMokZPId.htm](pathfinder-bestiary-2-items/DJ4DzpxUnMokZPId.htm)|Attack of Opportunity (Tentacle Only)|Ataque de oportunidad (sólo tentáculo)|modificada|
|[dj9YzVTLAtPASMHB.htm](pathfinder-bestiary-2-items/dj9YzVTLAtPASMHB.htm)|Creeping Cold|Creeping Cold|modificada|
|[djpxVgfWCno4NZwq.htm](pathfinder-bestiary-2-items/djpxVgfWCno4NZwq.htm)|Breath Weapon|Breath Weapon|modificada|
|[dkeTBVoAjjtnQkuW.htm](pathfinder-bestiary-2-items/dkeTBVoAjjtnQkuW.htm)|Scent|Scent|modificada|
|[dkFTU2VIAk9BBNw3.htm](pathfinder-bestiary-2-items/dkFTU2VIAk9BBNw3.htm)|Greater Constrict|Mayor Restricción|modificada|
|[dkgK1et0DFUYLpwT.htm](pathfinder-bestiary-2-items/dkgK1et0DFUYLpwT.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[dl10m47AKrgF3eVj.htm](pathfinder-bestiary-2-items/dl10m47AKrgF3eVj.htm)|Pour Down Throat|Verterse por la garganta|modificada|
|[DL16zUuUzuYySm31.htm](pathfinder-bestiary-2-items/DL16zUuUzuYySm31.htm)|Wing|Ala|modificada|
|[DLcmdOFICEUjArKs.htm](pathfinder-bestiary-2-items/DLcmdOFICEUjArKs.htm)|Jaws|Fauces|modificada|
|[DlQoJTMEjgmsWgCa.htm](pathfinder-bestiary-2-items/DlQoJTMEjgmsWgCa.htm)|Mandibles|Mandíbulas|modificada|
|[DmYndlktCCjOahA2.htm](pathfinder-bestiary-2-items/DmYndlktCCjOahA2.htm)|Spiritual Warden|Spiritual Warden|modificada|
|[dn95vUYkWbZHUAEc.htm](pathfinder-bestiary-2-items/dn95vUYkWbZHUAEc.htm)|Swallow Whole|Engullir Todo|modificada|
|[dNIO5C35HeSGtxDD.htm](pathfinder-bestiary-2-items/dNIO5C35HeSGtxDD.htm)|See Invisibility|Ver lo invisible|modificada|
|[dO0urnF4xfwl5Yp8.htm](pathfinder-bestiary-2-items/dO0urnF4xfwl5Yp8.htm)|Ravenous Embrace|Abrazo voraz|modificada|
|[dOcDYCNGkTD6GGcZ.htm](pathfinder-bestiary-2-items/dOcDYCNGkTD6GGcZ.htm)|Splinter Spray|Rociada de astillas|modificada|
|[DOihMsQlvxGBJLxK.htm](pathfinder-bestiary-2-items/DOihMsQlvxGBJLxK.htm)|Coven Spells|Coven Spells|modificada|
|[dOkhtnPU3mvJbRar.htm](pathfinder-bestiary-2-items/dOkhtnPU3mvJbRar.htm)|Tail|Tail|modificada|
|[dp75b5be8kghUzzi.htm](pathfinder-bestiary-2-items/dp75b5be8kghUzzi.htm)|Ventral Tube|Tubo Ventral|modificada|
|[DPcRdZIL38qcDj2I.htm](pathfinder-bestiary-2-items/DPcRdZIL38qcDj2I.htm)|Snatch|Arrebatar|modificada|
|[DpIIIQSKrhsFKToW.htm](pathfinder-bestiary-2-items/DpIIIQSKrhsFKToW.htm)|Holy Beam|Rayo Sagrado|modificada|
|[dplDPboVsTPEngpW.htm](pathfinder-bestiary-2-items/dplDPboVsTPEngpW.htm)|Beak|Beak|modificada|
|[DQRNNHu5GqCVAno4.htm](pathfinder-bestiary-2-items/DQRNNHu5GqCVAno4.htm)|Fangs|Colmillos|modificada|
|[DQT6UjypWovf9CmL.htm](pathfinder-bestiary-2-items/DQT6UjypWovf9CmL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[drwHha4i2p4cqBa7.htm](pathfinder-bestiary-2-items/drwHha4i2p4cqBa7.htm)|Tail|Tail|modificada|
|[DryXaAfUqz1HN8lZ.htm](pathfinder-bestiary-2-items/DryXaAfUqz1HN8lZ.htm)|Fist|Puño|modificada|
|[dSkIJ0gsh7XlH2OA.htm](pathfinder-bestiary-2-items/dSkIJ0gsh7XlH2OA.htm)|Claw|Garra|modificada|
|[DsM9HKBiut2efUZZ.htm](pathfinder-bestiary-2-items/DsM9HKBiut2efUZZ.htm)|Swarming Slither|Deslizarse de plaga|modificada|
|[DSxcWM211sMMf5q3.htm](pathfinder-bestiary-2-items/DSxcWM211sMMf5q3.htm)|Regurgitate Gastrolith|Regurgitar Gastrolito|modificada|
|[Dsz0LBZEAi6Jkmmo.htm](pathfinder-bestiary-2-items/Dsz0LBZEAi6Jkmmo.htm)|Pincer|Pinza|modificada|
|[DT1P11EFBYdmCUeQ.htm](pathfinder-bestiary-2-items/DT1P11EFBYdmCUeQ.htm)|Claw|Garra|modificada|
|[DteZBAInF0ur5LIY.htm](pathfinder-bestiary-2-items/DteZBAInF0ur5LIY.htm)|Shape Flesh|Shape Flesh|modificada|
|[DtSPqabFqLrQDfGC.htm](pathfinder-bestiary-2-items/DtSPqabFqLrQDfGC.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[dTZURdZLg8n5enSz.htm](pathfinder-bestiary-2-items/dTZURdZLg8n5enSz.htm)|Lifesense|Lifesense|modificada|
|[DU6e2UkT2fc49Zk3.htm](pathfinder-bestiary-2-items/DU6e2UkT2fc49Zk3.htm)|Lesser Alchemist's Fire|Fuego de alquimista menor|modificada|
|[dUfRUEqARe3yHUZM.htm](pathfinder-bestiary-2-items/dUfRUEqARe3yHUZM.htm)|Venom Spray|Venom Spray|modificada|
|[DUJLW3KPZ4HNw8je.htm](pathfinder-bestiary-2-items/DUJLW3KPZ4HNw8je.htm)|+4 Status to Will Saves vs. Mental|+4 de situación a las salvaciones de Voluntad contra Mental.|modificada|
|[DuUso0h1BlAS0Ch3.htm](pathfinder-bestiary-2-items/DuUso0h1BlAS0Ch3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DvGdYVJB2tQSEu99.htm](pathfinder-bestiary-2-items/DvGdYVJB2tQSEu99.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[DVIJX5RWBUIXJUk5.htm](pathfinder-bestiary-2-items/DVIJX5RWBUIXJUk5.htm)|Scent|Scent|modificada|
|[DVQrLVPCYEJyvk5s.htm](pathfinder-bestiary-2-items/DVQrLVPCYEJyvk5s.htm)|Destructive Harmonics|Armonías Destructivas|modificada|
|[dW28Q4hMeEWpDtay.htm](pathfinder-bestiary-2-items/dW28Q4hMeEWpDtay.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[dx1pnJnp1Gvp5XLN.htm](pathfinder-bestiary-2-items/dx1pnJnp1Gvp5XLN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DxgiMoyx7wyltYqp.htm](pathfinder-bestiary-2-items/DxgiMoyx7wyltYqp.htm)|Drink Blood|Beber Sangre|modificada|
|[dXKhtDCdlXRIWq2O.htm](pathfinder-bestiary-2-items/dXKhtDCdlXRIWq2O.htm)|Fangs|Colmillos|modificada|
|[dydhZgaxaoJL5dXx.htm](pathfinder-bestiary-2-items/dydhZgaxaoJL5dXx.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[dYdv3Ex4iHyirwUo.htm](pathfinder-bestiary-2-items/dYdv3Ex4iHyirwUo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Dye4kplXn2HWokpZ.htm](pathfinder-bestiary-2-items/Dye4kplXn2HWokpZ.htm)|Constant Spells|Constant Spells|modificada|
|[DyfwddYc7CkBo0HK.htm](pathfinder-bestiary-2-items/DyfwddYc7CkBo0HK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[dyis1hyzjJyWz9Sg.htm](pathfinder-bestiary-2-items/dyis1hyzjJyWz9Sg.htm)|Rock|Roca|modificada|
|[dynvtBdr79KGnqBo.htm](pathfinder-bestiary-2-items/dynvtBdr79KGnqBo.htm)|Can't Catch Me|Can't Catch Me|modificada|
|[dYvQOwzgoNuS9cG8.htm](pathfinder-bestiary-2-items/dYvQOwzgoNuS9cG8.htm)|Body Thief|Ladrón de cuerpos|modificada|
|[dZoSOjCNPr4EIzGG.htm](pathfinder-bestiary-2-items/dZoSOjCNPr4EIzGG.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[E1FAcXClkBLXXYC9.htm](pathfinder-bestiary-2-items/E1FAcXClkBLXXYC9.htm)|Claw|Garra|modificada|
|[E283z5pYuHL8XdzA.htm](pathfinder-bestiary-2-items/E283z5pYuHL8XdzA.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[e45FiBhbCxHP7Had.htm](pathfinder-bestiary-2-items/e45FiBhbCxHP7Had.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[E4bINR3oniilC0ba.htm](pathfinder-bestiary-2-items/E4bINR3oniilC0ba.htm)|Shell Defense|Defensa con caparazón|modificada|
|[E5KXLULpkukpqygI.htm](pathfinder-bestiary-2-items/E5KXLULpkukpqygI.htm)|Claw|Garra|modificada|
|[e7rgKN11dqX1fsSq.htm](pathfinder-bestiary-2-items/e7rgKN11dqX1fsSq.htm)|Focus Gaze|Centrar mirada|modificada|
|[E7WX10Amh46NUWCC.htm](pathfinder-bestiary-2-items/E7WX10Amh46NUWCC.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[E89TNZAj6eZb2Z9E.htm](pathfinder-bestiary-2-items/E89TNZAj6eZb2Z9E.htm)|Tentacle|Tentáculo|modificada|
|[E8ICm8MwDDWyjgKR.htm](pathfinder-bestiary-2-items/E8ICm8MwDDWyjgKR.htm)|Rock|Roca|modificada|
|[e8r1Ct8F3Oee48fo.htm](pathfinder-bestiary-2-items/e8r1Ct8F3Oee48fo.htm)|Tremorsense 60 feet|Tremorsense 60 pies|modificada|
|[E9aF47rwjTOQ1OTC.htm](pathfinder-bestiary-2-items/E9aF47rwjTOQ1OTC.htm)|Scent|Scent|modificada|
|[E9d31Pl4TeWAIjWG.htm](pathfinder-bestiary-2-items/E9d31Pl4TeWAIjWG.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[E9FVtTu2qyp5HgnY.htm](pathfinder-bestiary-2-items/E9FVtTu2qyp5HgnY.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[e9VfAq0loJF4o49U.htm](pathfinder-bestiary-2-items/e9VfAq0loJF4o49U.htm)|Throw Rock|Arrojar roca|modificada|
|[EaiT42eaTUXQMPIu.htm](pathfinder-bestiary-2-items/EaiT42eaTUXQMPIu.htm)|Fist|Puño|modificada|
|[EakALyMQUSW9bDgO.htm](pathfinder-bestiary-2-items/EakALyMQUSW9bDgO.htm)|Painsight|Painsight|modificada|
|[earmbFXXQH89tgAG.htm](pathfinder-bestiary-2-items/earmbFXXQH89tgAG.htm)|Warhammer|Warhammer|modificada|
|[EAsAGjtMDBLWo0ZO.htm](pathfinder-bestiary-2-items/EAsAGjtMDBLWo0ZO.htm)|+1 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[eaXk793zko55RRmG.htm](pathfinder-bestiary-2-items/eaXk793zko55RRmG.htm)|Icy Deflection|Desvío gélido|modificada|
|[eb6RXroslhq0fMd9.htm](pathfinder-bestiary-2-items/eb6RXroslhq0fMd9.htm)|Extra Reaction|Reacción adicional|modificada|
|[eBkXc5S4Spo7zZ3w.htm](pathfinder-bestiary-2-items/eBkXc5S4Spo7zZ3w.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EBnYEijsgTINPs3Y.htm](pathfinder-bestiary-2-items/EBnYEijsgTINPs3Y.htm)|Sickle|Hoz|modificada|
|[eBR0R2mRoSAc5xwp.htm](pathfinder-bestiary-2-items/eBR0R2mRoSAc5xwp.htm)|Claw|Garra|modificada|
|[ed300vUjktB7yhD7.htm](pathfinder-bestiary-2-items/ed300vUjktB7yhD7.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[ED7Sb3O9SLxFnTRr.htm](pathfinder-bestiary-2-items/ED7Sb3O9SLxFnTRr.htm)|Holy Greatsword|Sagrada Gran Espada|modificada|
|[eDciSvI59Etta4Vh.htm](pathfinder-bestiary-2-items/eDciSvI59Etta4Vh.htm)|All-Around Vision|All-Around Vision|modificada|
|[edczs6WrO42Ttglo.htm](pathfinder-bestiary-2-items/edczs6WrO42Ttglo.htm)|Constrict|Restringir|modificada|
|[edEJeTPVOOPPaCiE.htm](pathfinder-bestiary-2-items/edEJeTPVOOPPaCiE.htm)|Improved Grab|Agarrado mejorado|modificada|
|[EDGqAUdAUwkbCzQK.htm](pathfinder-bestiary-2-items/EDGqAUdAUwkbCzQK.htm)|Spew Mud|Spew Mud|modificada|
|[eE4KRcdYFo35NqFR.htm](pathfinder-bestiary-2-items/eE4KRcdYFo35NqFR.htm)|Capsize|Volcar|modificada|
|[EelpjpvvyRcCSTCg.htm](pathfinder-bestiary-2-items/EelpjpvvyRcCSTCg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EEQTDYHn0GwNCqRL.htm](pathfinder-bestiary-2-items/EEQTDYHn0GwNCqRL.htm)|Retributive Strike|Golpe retributivo|modificada|
|[eESVKEI54htLp14a.htm](pathfinder-bestiary-2-items/eESVKEI54htLp14a.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[EEVNlijtDn2A6Y9J.htm](pathfinder-bestiary-2-items/EEVNlijtDn2A6Y9J.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[EF6jFpXwBUM3i2gf.htm](pathfinder-bestiary-2-items/EF6jFpXwBUM3i2gf.htm)|Fist|Puño|modificada|
|[eF8APed2EwYOr5dF.htm](pathfinder-bestiary-2-items/eF8APed2EwYOr5dF.htm)|Rupturing Venom|Brecha Veneno|modificada|
|[eFiZ1wgGbxx9AVu1.htm](pathfinder-bestiary-2-items/eFiZ1wgGbxx9AVu1.htm)|Septic Malaria|Septic Malaria|modificada|
|[efL2PrxRnB9GLHmZ.htm](pathfinder-bestiary-2-items/efL2PrxRnB9GLHmZ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[eFL7EhhQB0m79mqu.htm](pathfinder-bestiary-2-items/eFL7EhhQB0m79mqu.htm)|Claw|Garra|modificada|
|[eFQH0YR5OMYZyD7n.htm](pathfinder-bestiary-2-items/eFQH0YR5OMYZyD7n.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[EG42MTaDcKnzDi6h.htm](pathfinder-bestiary-2-items/EG42MTaDcKnzDi6h.htm)|Jaws|Fauces|modificada|
|[eG5iE2SpiXdvquWh.htm](pathfinder-bestiary-2-items/eG5iE2SpiXdvquWh.htm)|Funereal Dirge|Funereal Dirge|modificada|
|[EguLo1LUzA4CQ1Qi.htm](pathfinder-bestiary-2-items/EguLo1LUzA4CQ1Qi.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[EHjIsHVK3BSAzKZW.htm](pathfinder-bestiary-2-items/EHjIsHVK3BSAzKZW.htm)|Focus Gaze|Centrar mirada|modificada|
|[EhmJLz0h6EUzbywV.htm](pathfinder-bestiary-2-items/EhmJLz0h6EUzbywV.htm)|Haul Away|Haul Away|modificada|
|[ehOgYBBQ86ktUisv.htm](pathfinder-bestiary-2-items/ehOgYBBQ86ktUisv.htm)|Grab|Agarrado|modificada|
|[ehUknW9WZvN9eNjb.htm](pathfinder-bestiary-2-items/ehUknW9WZvN9eNjb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ehx0cPu5CtggKCGE.htm](pathfinder-bestiary-2-items/Ehx0cPu5CtggKCGE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[eiefYuDeu60ebYAN.htm](pathfinder-bestiary-2-items/eiefYuDeu60ebYAN.htm)|+1 Circumstance Bonus on all Saving Throws|Bonificador +1 de Circunstancia en todas las Tiradas de Salvación.|modificada|
|[EIhaPhUMiN88cQT6.htm](pathfinder-bestiary-2-items/EIhaPhUMiN88cQT6.htm)|Aura of Righteousness|Aura de rectitud|modificada|
|[EILiYrpIKrSD8lNU.htm](pathfinder-bestiary-2-items/EILiYrpIKrSD8lNU.htm)|Bite|Muerdemuerde|modificada|
|[eIV0ZcgzUZI0e8VM.htm](pathfinder-bestiary-2-items/eIV0ZcgzUZI0e8VM.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[ej0lyLnZuPKMNMnI.htm](pathfinder-bestiary-2-items/ej0lyLnZuPKMNMnI.htm)|Focused Flames|Flamígera enfocada|modificada|
|[ej30s3gZXMNhiGRr.htm](pathfinder-bestiary-2-items/ej30s3gZXMNhiGRr.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EjSQcYiJF0lhuXqk.htm](pathfinder-bestiary-2-items/EjSQcYiJF0lhuXqk.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[EK9tHuUdNF8lzwwK.htm](pathfinder-bestiary-2-items/EK9tHuUdNF8lzwwK.htm)|Club|Club|modificada|
|[eKrrD4BYO7xl9vPo.htm](pathfinder-bestiary-2-items/eKrrD4BYO7xl9vPo.htm)|Fearful Attack|Ataque de miedo|modificada|
|[eKSzSgT3W8A9oNzs.htm](pathfinder-bestiary-2-items/eKSzSgT3W8A9oNzs.htm)|Multiple Opportunities|Oportunidades múltiple|modificada|
|[El38bC5IzKKtBdcW.htm](pathfinder-bestiary-2-items/El38bC5IzKKtBdcW.htm)|Steal Voice|Sustraer Voz|modificada|
|[ElDo5C9tGNaFEyUo.htm](pathfinder-bestiary-2-items/ElDo5C9tGNaFEyUo.htm)|Tail|Tail|modificada|
|[ELI8vuFBdrN99fE3.htm](pathfinder-bestiary-2-items/ELI8vuFBdrN99fE3.htm)|Tendril|Tendril|modificada|
|[elLeiMASiyy7jPxz.htm](pathfinder-bestiary-2-items/elLeiMASiyy7jPxz.htm)|Spirit Touch|Spirit Touch|modificada|
|[eLovXAlbDKmZARNK.htm](pathfinder-bestiary-2-items/eLovXAlbDKmZARNK.htm)|Constant Spells|Constant Spells|modificada|
|[eN9AmHu7Wb5ZVY5F.htm](pathfinder-bestiary-2-items/eN9AmHu7Wb5ZVY5F.htm)|Whiffling|Whiffling|modificada|
|[ENtN1odPFGnOKcBk.htm](pathfinder-bestiary-2-items/ENtN1odPFGnOKcBk.htm)|Telepathy|Telepatía|modificada|
|[Eo3e73BdyU4oMYEq.htm](pathfinder-bestiary-2-items/Eo3e73BdyU4oMYEq.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[eO5aWydhyXcR1nGE.htm](pathfinder-bestiary-2-items/eO5aWydhyXcR1nGE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EO9iiH6RJCSv55DM.htm](pathfinder-bestiary-2-items/EO9iiH6RJCSv55DM.htm)|Root|Enraizarse|modificada|
|[eohTEKJnqW3cF8U8.htm](pathfinder-bestiary-2-items/eohTEKJnqW3cF8U8.htm)|Force Bolt|Rayo de fuerza|modificada|
|[eoLK3mAvvXiMTLnH.htm](pathfinder-bestiary-2-items/eoLK3mAvvXiMTLnH.htm)|Twist the Hook|Retorcer el Gancho|modificada|
|[eOpB0SxDMEU65Oyy.htm](pathfinder-bestiary-2-items/eOpB0SxDMEU65Oyy.htm)|Blood Drain|Drenar sangre|modificada|
|[ephUPtGMB5sA3OWQ.htm](pathfinder-bestiary-2-items/ephUPtGMB5sA3OWQ.htm)|Tremorsense|Tremorsense|modificada|
|[EPlZiO8molN6azkO.htm](pathfinder-bestiary-2-items/EPlZiO8molN6azkO.htm)|Spittle|Escupitajo|modificada|
|[EPXehWRhcO5caYov.htm](pathfinder-bestiary-2-items/EPXehWRhcO5caYov.htm)|Shift Fate|Cambiante Destino|modificada|
|[eQAeRO9fMIqYp05Q.htm](pathfinder-bestiary-2-items/eQAeRO9fMIqYp05Q.htm)|Sense Fate|Sense Fate|modificada|
|[eQGUATVlHv8iZ97x.htm](pathfinder-bestiary-2-items/eQGUATVlHv8iZ97x.htm)|Weak Acid|Ácido débil|modificada|
|[eqGZhn9lsZI6JDYE.htm](pathfinder-bestiary-2-items/eqGZhn9lsZI6JDYE.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Eqk7JKBf2mGkZbCf.htm](pathfinder-bestiary-2-items/Eqk7JKBf2mGkZbCf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eQwYRUkwKCcTxqhO.htm](pathfinder-bestiary-2-items/eQwYRUkwKCcTxqhO.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Er19GkrGFluxc3FN.htm](pathfinder-bestiary-2-items/Er19GkrGFluxc3FN.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[eR2U38j5LSvSk5t6.htm](pathfinder-bestiary-2-items/eR2U38j5LSvSk5t6.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[eR8AFynWU8vfsTA9.htm](pathfinder-bestiary-2-items/eR8AFynWU8vfsTA9.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[ErIhDSVTBd7aGHeC.htm](pathfinder-bestiary-2-items/ErIhDSVTBd7aGHeC.htm)|Stinger|Aguijón|modificada|
|[ERMHc3Xc7e0s0qGD.htm](pathfinder-bestiary-2-items/ERMHc3Xc7e0s0qGD.htm)|Withering Touch|Toque de marchitamiento|modificada|
|[ERoazbmYgnuBSYsk.htm](pathfinder-bestiary-2-items/ERoazbmYgnuBSYsk.htm)|Xill Paralysis|Xill Paralysis|modificada|
|[ERoYflv6bErPQNP2.htm](pathfinder-bestiary-2-items/ERoYflv6bErPQNP2.htm)|Tail|Tail|modificada|
|[ESiP5kSLjg2exhAh.htm](pathfinder-bestiary-2-items/ESiP5kSLjg2exhAh.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[esTHEOJBat1zXzXY.htm](pathfinder-bestiary-2-items/esTHEOJBat1zXzXY.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[eTAHPb22Vfb1ZIIA.htm](pathfinder-bestiary-2-items/eTAHPb22Vfb1ZIIA.htm)|Grab|Agarrado|modificada|
|[eTTwVRhxdeVx0PRD.htm](pathfinder-bestiary-2-items/eTTwVRhxdeVx0PRD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[eu0SQkCWm2vp3Yy6.htm](pathfinder-bestiary-2-items/eu0SQkCWm2vp3Yy6.htm)|Claw|Garra|modificada|
|[eUBZ8xXpWH2Gvg39.htm](pathfinder-bestiary-2-items/eUBZ8xXpWH2Gvg39.htm)|Kukri|Kukri|modificada|
|[eugKdHe7dmAkjXP0.htm](pathfinder-bestiary-2-items/eugKdHe7dmAkjXP0.htm)|Witchflame Bolt|Witchflame Bolt|modificada|
|[EungEyymZcbKAl61.htm](pathfinder-bestiary-2-items/EungEyymZcbKAl61.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EuS6s6eoD5RfY7jI.htm](pathfinder-bestiary-2-items/EuS6s6eoD5RfY7jI.htm)|Cloud Walk|Cloud Walk|modificada|
|[Ev5luBuqFKW5s9MN.htm](pathfinder-bestiary-2-items/Ev5luBuqFKW5s9MN.htm)|Water Glide|Deslizamiento de Agua|modificada|
|[eVlxUKYnPRCGVP58.htm](pathfinder-bestiary-2-items/eVlxUKYnPRCGVP58.htm)|Wavesense|Wavesense|modificada|
|[EVtI5ReFhXPbGsa8.htm](pathfinder-bestiary-2-items/EVtI5ReFhXPbGsa8.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EW5LH6Dw5jEKEVOU.htm](pathfinder-bestiary-2-items/EW5LH6Dw5jEKEVOU.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[Ew7CBGn58KzAoHsQ.htm](pathfinder-bestiary-2-items/Ew7CBGn58KzAoHsQ.htm)|Bite|Muerdemuerde|modificada|
|[eWih1h98kRb5uo3E.htm](pathfinder-bestiary-2-items/eWih1h98kRb5uo3E.htm)|Telepathy|Telepatía|modificada|
|[ewIhUHZqcxVqsG7x.htm](pathfinder-bestiary-2-items/ewIhUHZqcxVqsG7x.htm)|Dagger|Daga|modificada|
|[EWOsMDPiDiwsUVSL.htm](pathfinder-bestiary-2-items/EWOsMDPiDiwsUVSL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ewPaxkwRS28GoetB.htm](pathfinder-bestiary-2-items/ewPaxkwRS28GoetB.htm)|Jaws|Fauces|modificada|
|[EXLaDRArP4bsh7XH.htm](pathfinder-bestiary-2-items/EXLaDRArP4bsh7XH.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ExsxFy6Bmcgsy0GX.htm](pathfinder-bestiary-2-items/ExsxFy6Bmcgsy0GX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[eyEiaf2KwHBA5miS.htm](pathfinder-bestiary-2-items/eyEiaf2KwHBA5miS.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[EYtHIPWgwGTdjCBp.htm](pathfinder-bestiary-2-items/EYtHIPWgwGTdjCBp.htm)|Fascination of Flame|Fascinación de Flamígera|modificada|
|[ez4TlFP9ir8diYv7.htm](pathfinder-bestiary-2-items/ez4TlFP9ir8diYv7.htm)|Scuttle|Scuttle|modificada|
|[ezme3D83xTKiLkhR.htm](pathfinder-bestiary-2-items/ezme3D83xTKiLkhR.htm)|Final End|Final Final|modificada|
|[eZpQaoruvmngTW6T.htm](pathfinder-bestiary-2-items/eZpQaoruvmngTW6T.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[eZt77JcDJLXdqusp.htm](pathfinder-bestiary-2-items/eZt77JcDJLXdqusp.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[f1adZO1FX8WkPN1h.htm](pathfinder-bestiary-2-items/f1adZO1FX8WkPN1h.htm)|Violet Rot|Putridez violeta|modificada|
|[F1orhEfNkIuGNlEh.htm](pathfinder-bestiary-2-items/F1orhEfNkIuGNlEh.htm)|Buck|Encabritarse|modificada|
|[F2cUCIHpa06Qkqy2.htm](pathfinder-bestiary-2-items/F2cUCIHpa06Qkqy2.htm)|Squeeze|Escurrirse|modificada|
|[F2mPoxLl7PTJpc3I.htm](pathfinder-bestiary-2-items/F2mPoxLl7PTJpc3I.htm)|All-Around Vision|All-Around Vision|modificada|
|[f3X4SLxmMSok3p65.htm](pathfinder-bestiary-2-items/f3X4SLxmMSok3p65.htm)|Fate Drain|Drenado de Destino|modificada|
|[F40AJYqQ0BCC2TF0.htm](pathfinder-bestiary-2-items/F40AJYqQ0BCC2TF0.htm)|Chain|Cadena|modificada|
|[f4JPwXqfZM8fy4Zp.htm](pathfinder-bestiary-2-items/f4JPwXqfZM8fy4Zp.htm)|Absorb Force|Absorber Fuerza|modificada|
|[F4z6xO5BYtoag9hK.htm](pathfinder-bestiary-2-items/F4z6xO5BYtoag9hK.htm)|Jaws|Fauces|modificada|
|[f61dlQmeP2vZs1Go.htm](pathfinder-bestiary-2-items/f61dlQmeP2vZs1Go.htm)|Darkvision|Visión en la oscuridad|modificada|
|[F7GzmZlJ6PXy8Cm5.htm](pathfinder-bestiary-2-items/F7GzmZlJ6PXy8Cm5.htm)|Smoke Vision|Visión de Humo|modificada|
|[f7niepbS8VyknoBK.htm](pathfinder-bestiary-2-items/f7niepbS8VyknoBK.htm)|Scintillating Aura|Scintillating Aura|modificada|
|[fakAfk3puTMpw1vT.htm](pathfinder-bestiary-2-items/fakAfk3puTMpw1vT.htm)|Slow Metabolism|Metabolismo lentificado/a|modificada|
|[FapwAUPhHeXc1NKj.htm](pathfinder-bestiary-2-items/FapwAUPhHeXc1NKj.htm)|Warlord's Training|Warlord's Training|modificada|
|[FBiCgn8XESVcHSPx.htm](pathfinder-bestiary-2-items/FBiCgn8XESVcHSPx.htm)|Jaws|Fauces|modificada|
|[FBO25NSysfQagiQl.htm](pathfinder-bestiary-2-items/FBO25NSysfQagiQl.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[fBpvD0xb43MGDESG.htm](pathfinder-bestiary-2-items/fBpvD0xb43MGDESG.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[FBYVc1nHfCqd2hLL.htm](pathfinder-bestiary-2-items/FBYVc1nHfCqd2hLL.htm)|Morningstar|Morningstar|modificada|
|[Fc0hTwlWHz8gcaaN.htm](pathfinder-bestiary-2-items/Fc0hTwlWHz8gcaaN.htm)|Tail|Tail|modificada|
|[FcafMwq1Bll5o21K.htm](pathfinder-bestiary-2-items/FcafMwq1Bll5o21K.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[fcCuiTHr7ed8LhqK.htm](pathfinder-bestiary-2-items/fcCuiTHr7ed8LhqK.htm)|Fangs|Colmillos|modificada|
|[FcLtb9IKlH2X8wBl.htm](pathfinder-bestiary-2-items/FcLtb9IKlH2X8wBl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FcnwHTEpIdO7oCXf.htm](pathfinder-bestiary-2-items/FcnwHTEpIdO7oCXf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Fd4JoDpXliyIpcvN.htm](pathfinder-bestiary-2-items/Fd4JoDpXliyIpcvN.htm)|Horn|Cuerno|modificada|
|[fDD7cIf8ZsYLF8fv.htm](pathfinder-bestiary-2-items/fDD7cIf8ZsYLF8fv.htm)|Impaling Critical|Empalar Crítico|modificada|
|[fDEMKsDuC7SNDbx6.htm](pathfinder-bestiary-2-items/fDEMKsDuC7SNDbx6.htm)|Unbodied Possession|Posesión sin cuerpo.|modificada|
|[fDOQq8mfa2PhlHcF.htm](pathfinder-bestiary-2-items/fDOQq8mfa2PhlHcF.htm)|Bore into Brain|Bore into Brain|modificada|
|[FdZZPAWhuFULerJd.htm](pathfinder-bestiary-2-items/FdZZPAWhuFULerJd.htm)|Trumpet Blast|Trompeta|modificada|
|[fEKfgiJvpDVUGTI9.htm](pathfinder-bestiary-2-items/fEKfgiJvpDVUGTI9.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[fFFM3BNDIa0yq0lg.htm](pathfinder-bestiary-2-items/fFFM3BNDIa0yq0lg.htm)|Regeneration 20 (Deactivated by Good or Silver)|Regeneración 20 (Desactivado por Bien o Plata)|modificada|
|[FFRvTDGH1FEnAKjH.htm](pathfinder-bestiary-2-items/FFRvTDGH1FEnAKjH.htm)|Splintering Death|Muerte astillada|modificada|
|[ffxbLuQioRQZCSba.htm](pathfinder-bestiary-2-items/ffxbLuQioRQZCSba.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fg8mmyOMmhcaaO4s.htm](pathfinder-bestiary-2-items/fg8mmyOMmhcaaO4s.htm)|Fast Healing 2 (when touching mud or slime)|Curación rápida 2 (al tocar barro o limo)|modificada|
|[FH5MdxzW5lzQE9Hf.htm](pathfinder-bestiary-2-items/FH5MdxzW5lzQE9Hf.htm)|Regeneration 7 (deactivated by acid or fire)|Regeneración 7 (desactivada por ácido o fuego).|modificada|
|[fHcej8vlhHnCFoab.htm](pathfinder-bestiary-2-items/fHcej8vlhHnCFoab.htm)|Tremorsense|Tremorsense|modificada|
|[FHQiK6c7jZUggMXg.htm](pathfinder-bestiary-2-items/FHQiK6c7jZUggMXg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FhV61jMMGaGgOnmc.htm](pathfinder-bestiary-2-items/FhV61jMMGaGgOnmc.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[FhwufRLfw0dtrKFI.htm](pathfinder-bestiary-2-items/FhwufRLfw0dtrKFI.htm)|Kneecapper|Kneecapper|modificada|
|[FHyqgRwAZouh37Gw.htm](pathfinder-bestiary-2-items/FHyqgRwAZouh37Gw.htm)|Fangs|Colmillos|modificada|
|[FIBgwJl6ghhZJB6Y.htm](pathfinder-bestiary-2-items/FIBgwJl6ghhZJB6Y.htm)|Quetz Couatl Venom|Quetz Couatl Veneno|modificada|
|[fiJwhvqSCYbkgaJg.htm](pathfinder-bestiary-2-items/fiJwhvqSCYbkgaJg.htm)|Swear|Swear|modificada|
|[fiKhFiynAYN8X5if.htm](pathfinder-bestiary-2-items/fiKhFiynAYN8X5if.htm)|Gemsight|Gemsight|modificada|
|[filzxUgupqvL1ZQq.htm](pathfinder-bestiary-2-items/filzxUgupqvL1ZQq.htm)|Gouging Lunge|Acometer|modificada|
|[FjdhY5Rvi0x7AdX0.htm](pathfinder-bestiary-2-items/FjdhY5Rvi0x7AdX0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fJJc56GDg0EB3oit.htm](pathfinder-bestiary-2-items/fJJc56GDg0EB3oit.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[fJR0vMezUrIu4nlW.htm](pathfinder-bestiary-2-items/fJR0vMezUrIu4nlW.htm)|Tail Sting|Tail Sting|modificada|
|[fJwNiBKQ1mE1NNbz.htm](pathfinder-bestiary-2-items/fJwNiBKQ1mE1NNbz.htm)|Dual Tusks|Dobles colmillos|modificada|
|[FKd8OWhPrCEcBKFC.htm](pathfinder-bestiary-2-items/FKd8OWhPrCEcBKFC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fKeX8OGCBc8j0eJu.htm](pathfinder-bestiary-2-items/fKeX8OGCBc8j0eJu.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FKgN8Ya6AS8vCOry.htm](pathfinder-bestiary-2-items/FKgN8Ya6AS8vCOry.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FKpHGfQVea81uMo2.htm](pathfinder-bestiary-2-items/FKpHGfQVea81uMo2.htm)|Motionsense|Motionsense|modificada|
|[fKqimVsLOPsE2xN0.htm](pathfinder-bestiary-2-items/fKqimVsLOPsE2xN0.htm)|Menace to Magic|Amenaza para la magia|modificada|
|[FlbbYL5EQllXYKjc.htm](pathfinder-bestiary-2-items/FlbbYL5EQllXYKjc.htm)|Susceptible to Desiccation|Susceptible a la desecación|modificada|
|[flCrHBNNesdvv5af.htm](pathfinder-bestiary-2-items/flCrHBNNesdvv5af.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[fm5cOYL11qhzHV3G.htm](pathfinder-bestiary-2-items/fm5cOYL11qhzHV3G.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[fM7GchX13PhXHMdp.htm](pathfinder-bestiary-2-items/fM7GchX13PhXHMdp.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[fmHvsMYoy6LDxLkK.htm](pathfinder-bestiary-2-items/fmHvsMYoy6LDxLkK.htm)|Tentacle Mouth|Tentacle Mouth|modificada|
|[FN5QndPNQPuzc0V3.htm](pathfinder-bestiary-2-items/FN5QndPNQPuzc0V3.htm)|Coven Spells|Coven Spells|modificada|
|[FNBiiooVEkzVDOTE.htm](pathfinder-bestiary-2-items/FNBiiooVEkzVDOTE.htm)|Regeneration 10 (Deactivated by Good or Silver)|Regeneración 10 (Desactivado por Bien o Plata)|modificada|
|[FnRuOLk9xVqNWtrD.htm](pathfinder-bestiary-2-items/FnRuOLk9xVqNWtrD.htm)|Constant Spells|Constant Spells|modificada|
|[FNSLMfCQHDGPvjsv.htm](pathfinder-bestiary-2-items/FNSLMfCQHDGPvjsv.htm)|Sudden Retreat|Retirada repentina|modificada|
|[FnwUcB0l68viXYtQ.htm](pathfinder-bestiary-2-items/FnwUcB0l68viXYtQ.htm)|Fish Hook|Fish Hook|modificada|
|[fO8E1sNI1DHDU5Dq.htm](pathfinder-bestiary-2-items/fO8E1sNI1DHDU5Dq.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[foIpp0GPCbsHL0dl.htm](pathfinder-bestiary-2-items/foIpp0GPCbsHL0dl.htm)|Cave Scorpion Venom|Veneno de escorpión de cueva|modificada|
|[fon9lS38svdiFHZJ.htm](pathfinder-bestiary-2-items/fon9lS38svdiFHZJ.htm)|Greedy Grab|Agarrado codicioso|modificada|
|[Fp4obYTKAlnrpeFi.htm](pathfinder-bestiary-2-items/Fp4obYTKAlnrpeFi.htm)|Change Shape|Change Shape|modificada|
|[FpcxdJ4oqnyX8sBs.htm](pathfinder-bestiary-2-items/FpcxdJ4oqnyX8sBs.htm)|Long Neck|Cuello largo|modificada|
|[fPGmCfWuF8oqK8hX.htm](pathfinder-bestiary-2-items/fPGmCfWuF8oqK8hX.htm)|Otherworldly Laugh|Risa de otro mundo|modificada|
|[FqRKnuudm93xTutr.htm](pathfinder-bestiary-2-items/FqRKnuudm93xTutr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[fqXSW2GPcFXzcz2P.htm](pathfinder-bestiary-2-items/fqXSW2GPcFXzcz2P.htm)|Constant Spells|Constant Spells|modificada|
|[FQZGypLzxjxM8v3a.htm](pathfinder-bestiary-2-items/FQZGypLzxjxM8v3a.htm)|Black Flame Knives|Cuchillos de Llama Negra|modificada|
|[FrDnLG8ehjXFC6Ey.htm](pathfinder-bestiary-2-items/FrDnLG8ehjXFC6Ey.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[frIks1GEvtVQAPPw.htm](pathfinder-bestiary-2-items/frIks1GEvtVQAPPw.htm)|Vine|Vid|modificada|
|[FrTdGO6BqbRvBLu9.htm](pathfinder-bestiary-2-items/FrTdGO6BqbRvBLu9.htm)|Fangs|Colmillos|modificada|
|[fRtY0qdhDDIRBRWj.htm](pathfinder-bestiary-2-items/fRtY0qdhDDIRBRWj.htm)|Fist|Puño|modificada|
|[fRuyuldjZkDBFASw.htm](pathfinder-bestiary-2-items/fRuyuldjZkDBFASw.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[FsgtuZo9KF7fxlp5.htm](pathfinder-bestiary-2-items/FsgtuZo9KF7fxlp5.htm)|Jaws|Fauces|modificada|
|[FTeRIF51hvaZAOzR.htm](pathfinder-bestiary-2-items/FTeRIF51hvaZAOzR.htm)|Smoke Vision|Visión de Humo|modificada|
|[FTil5NFptMbuNK7y.htm](pathfinder-bestiary-2-items/FTil5NFptMbuNK7y.htm)|Dagger|Daga|modificada|
|[ftJSskhqjZaMBNyZ.htm](pathfinder-bestiary-2-items/ftJSskhqjZaMBNyZ.htm)|Jet|Jet|modificada|
|[Fu1B7hcvnk9S79DA.htm](pathfinder-bestiary-2-items/Fu1B7hcvnk9S79DA.htm)|Commander's Aura|Aura de comandante|modificada|
|[FUbGjkLswPM3bxUL.htm](pathfinder-bestiary-2-items/FUbGjkLswPM3bxUL.htm)|Jaws|Fauces|modificada|
|[FUdyjRGkgKWx4So9.htm](pathfinder-bestiary-2-items/FUdyjRGkgKWx4So9.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[FuTchIYowgou1HIa.htm](pathfinder-bestiary-2-items/FuTchIYowgou1HIa.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[fuxGM2FMqoHqXPJ4.htm](pathfinder-bestiary-2-items/fuxGM2FMqoHqXPJ4.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[fUyM5YcNhV5dlwTV.htm](pathfinder-bestiary-2-items/fUyM5YcNhV5dlwTV.htm)|Ice Burrow|Madriguera de Hielo|modificada|
|[fVaZtygxTBUfppLk.htm](pathfinder-bestiary-2-items/fVaZtygxTBUfppLk.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[FVDPg3HIsGffnOjd.htm](pathfinder-bestiary-2-items/FVDPg3HIsGffnOjd.htm)|Shameful Touch|Toque bochornoso|modificada|
|[FWHFEirBNdz22BqV.htm](pathfinder-bestiary-2-items/FWHFEirBNdz22BqV.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[fx1cIpK2KgNbm7Rz.htm](pathfinder-bestiary-2-items/fx1cIpK2KgNbm7Rz.htm)|Magma Swim|Nadar Magma|modificada|
|[FXPT5cs0LSYkZOku.htm](pathfinder-bestiary-2-items/FXPT5cs0LSYkZOku.htm)|Claw|Garra|modificada|
|[FXSgcv7xK9yqmz6Q.htm](pathfinder-bestiary-2-items/FXSgcv7xK9yqmz6Q.htm)|Jaws|Fauces|modificada|
|[fxuqMIJcFuZQworq.htm](pathfinder-bestiary-2-items/fxuqMIJcFuZQworq.htm)|Fangs|Colmillos|modificada|
|[fxZztiCJybeYYeOI.htm](pathfinder-bestiary-2-items/fxZztiCJybeYYeOI.htm)|Captivating Dance|Danzante cautivador|modificada|
|[FYiiFhvRvIKkTu5v.htm](pathfinder-bestiary-2-items/FYiiFhvRvIKkTu5v.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[fysusPtWaa25MvUW.htm](pathfinder-bestiary-2-items/fysusPtWaa25MvUW.htm)|Claw|Garra|modificada|
|[FZaWBsctyw8LDdfD.htm](pathfinder-bestiary-2-items/FZaWBsctyw8LDdfD.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[fZOolgYWKm1a27op.htm](pathfinder-bestiary-2-items/fZOolgYWKm1a27op.htm)|Purity Vulnerability|Vulnerabilidad de Pureza|modificada|
|[G0D3kQHUd2mouA90.htm](pathfinder-bestiary-2-items/G0D3kQHUd2mouA90.htm)|Dominate Animal|Dominar Animal|modificada|
|[G0pgg8gIhgw18USv.htm](pathfinder-bestiary-2-items/G0pgg8gIhgw18USv.htm)|Trample|Trample|modificada|
|[g2gB3rKzB7uL7Kxn.htm](pathfinder-bestiary-2-items/g2gB3rKzB7uL7Kxn.htm)|Claw|Garra|modificada|
|[G2PIvLQqH98WKNaK.htm](pathfinder-bestiary-2-items/G2PIvLQqH98WKNaK.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[g3Gb5EYfu1jFqndC.htm](pathfinder-bestiary-2-items/g3Gb5EYfu1jFqndC.htm)|Ink Cloud|Nube de Tinta|modificada|
|[G3Qoqz5IMBvs9EYb.htm](pathfinder-bestiary-2-items/G3Qoqz5IMBvs9EYb.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[G412UtVloe7PIpa4.htm](pathfinder-bestiary-2-items/G412UtVloe7PIpa4.htm)|+2 Status to All Saves vs. Disease and Poison|+2 situación a todas las salvaciones contra enfermedad y veneno.|modificada|
|[g41jvLpQ6exKQBgd.htm](pathfinder-bestiary-2-items/g41jvLpQ6exKQBgd.htm)|Spines|Spines|modificada|
|[g56aPVqelhyIjRuv.htm](pathfinder-bestiary-2-items/g56aPVqelhyIjRuv.htm)|+4 Status to All Saves vs. Sonic|+4 de situación a todas las salvaciones contra Sonic.|modificada|
|[G5e1Cf5jNqFiqF6y.htm](pathfinder-bestiary-2-items/G5e1Cf5jNqFiqF6y.htm)|Grab|Agarrado|modificada|
|[G5NJxlLVYCrXX8Jv.htm](pathfinder-bestiary-2-items/G5NJxlLVYCrXX8Jv.htm)|Twisting Tail|Enredar con la cola|modificada|
|[g5TUPRPR9gHfXrfU.htm](pathfinder-bestiary-2-items/g5TUPRPR9gHfXrfU.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[G6O55FgFD95SRjPP.htm](pathfinder-bestiary-2-items/G6O55FgFD95SRjPP.htm)|Claw|Garra|modificada|
|[G7P9My2HYy017m1O.htm](pathfinder-bestiary-2-items/G7P9My2HYy017m1O.htm)|36 AC vs. Non-Magical|36 CA vs. No mágico|modificada|
|[g9enKXrvCUkwck8W.htm](pathfinder-bestiary-2-items/g9enKXrvCUkwck8W.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[gADgaaM8ZZlYZcXg.htm](pathfinder-bestiary-2-items/gADgaaM8ZZlYZcXg.htm)|Jaws|Fauces|modificada|
|[gAdwnZ4OP8sPpuOg.htm](pathfinder-bestiary-2-items/gAdwnZ4OP8sPpuOg.htm)|Painsight|Painsight|modificada|
|[GBBCSikxVuTyL14m.htm](pathfinder-bestiary-2-items/GBBCSikxVuTyL14m.htm)|Foot|Pie|modificada|
|[Gbcpa6lIhj2KUyH4.htm](pathfinder-bestiary-2-items/Gbcpa6lIhj2KUyH4.htm)|Rampant Growth|Crecimiento desenfrenado|modificada|
|[gBFFibeJFk4MB72C.htm](pathfinder-bestiary-2-items/gBFFibeJFk4MB72C.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[GbfpFzRZunqiRxI5.htm](pathfinder-bestiary-2-items/GbfpFzRZunqiRxI5.htm)|Catch Rock|Atrapar roca|modificada|
|[GBMDJeuQSv0Q6T5o.htm](pathfinder-bestiary-2-items/GBMDJeuQSv0Q6T5o.htm)|Breath Weapon|Breath Weapon|modificada|
|[gbsLwd2FBQal3xdZ.htm](pathfinder-bestiary-2-items/gbsLwd2FBQal3xdZ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[GCC6Zf1NaPihfEJ0.htm](pathfinder-bestiary-2-items/GCC6Zf1NaPihfEJ0.htm)|Tangle Spores|Tangle Spores|modificada|
|[gcdD70PvUw3sfPCK.htm](pathfinder-bestiary-2-items/gcdD70PvUw3sfPCK.htm)|Painful Harmonics|Painful Harmonics|modificada|
|[GCGNJYiHP5GMdDAQ.htm](pathfinder-bestiary-2-items/GCGNJYiHP5GMdDAQ.htm)|Exit Body|Cuerpo de salida|modificada|
|[GcML4HwZyqVy7XVw.htm](pathfinder-bestiary-2-items/GcML4HwZyqVy7XVw.htm)|Talon|Talon|modificada|
|[GCu6OFgRjem6F0sI.htm](pathfinder-bestiary-2-items/GCu6OFgRjem6F0sI.htm)|Witchflame|Witchflame|modificada|
|[gCwKF48GST0z9EEa.htm](pathfinder-bestiary-2-items/gCwKF48GST0z9EEa.htm)|Grab|Agarrado|modificada|
|[GcxVApKgeD5WO9Wh.htm](pathfinder-bestiary-2-items/GcxVApKgeD5WO9Wh.htm)|Desiccating Bite|Muerdemuerde Desecante|modificada|
|[gd4pTa9mCV6qrbcN.htm](pathfinder-bestiary-2-items/gd4pTa9mCV6qrbcN.htm)|Undulate|Undulate|modificada|
|[gEADE2ZlQ0vRnps7.htm](pathfinder-bestiary-2-items/gEADE2ZlQ0vRnps7.htm)|Stench|Hedor|modificada|
|[GEb3KusxBDvL8CkC.htm](pathfinder-bestiary-2-items/GEb3KusxBDvL8CkC.htm)|Stench|Hedor|modificada|
|[GeIadA4vgUJvJ926.htm](pathfinder-bestiary-2-items/GeIadA4vgUJvJ926.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[GFW3IASGrcBSyETb.htm](pathfinder-bestiary-2-items/GFW3IASGrcBSyETb.htm)|Worm Chill|Gelidez de gusano|modificada|
|[gGIkwe9BtDFc6qdx.htm](pathfinder-bestiary-2-items/gGIkwe9BtDFc6qdx.htm)|Tremorsense|Tremorsense|modificada|
|[ggo5fsSPiaSiT5ot.htm](pathfinder-bestiary-2-items/ggo5fsSPiaSiT5ot.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gGqDItdgQIf2RRPw.htm](pathfinder-bestiary-2-items/gGqDItdgQIf2RRPw.htm)|Falchion|Falchion|modificada|
|[gH06P6rp2bswuGya.htm](pathfinder-bestiary-2-items/gH06P6rp2bswuGya.htm)|Jaws|Fauces|modificada|
|[GHbCLo2MkEFt1qai.htm](pathfinder-bestiary-2-items/GHbCLo2MkEFt1qai.htm)|Scimitar|Cimitarra|modificada|
|[ghbtjQahExs1AvpE.htm](pathfinder-bestiary-2-items/ghbtjQahExs1AvpE.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GHcUVfhfs6mcm7mS.htm](pathfinder-bestiary-2-items/GHcUVfhfs6mcm7mS.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[gHUajYINDeCp7360.htm](pathfinder-bestiary-2-items/gHUajYINDeCp7360.htm)|Echolocation 120 feet|Ecolocalización 120 pies|modificada|
|[gI0Pt3FCseRtdPpl.htm](pathfinder-bestiary-2-items/gI0Pt3FCseRtdPpl.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[GIj7axAUfLMpl5DN.htm](pathfinder-bestiary-2-items/GIj7axAUfLMpl5DN.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[GimuDrJ7GoLAne8i.htm](pathfinder-bestiary-2-items/GimuDrJ7GoLAne8i.htm)|Avoid the Swat|Evitar ser aplastada|modificada|
|[gINTMtPPDP3qyAhd.htm](pathfinder-bestiary-2-items/gINTMtPPDP3qyAhd.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[GIop4yLHoN44KXN5.htm](pathfinder-bestiary-2-items/GIop4yLHoN44KXN5.htm)|Trample|Trample|modificada|
|[giVESjdTpw33NygU.htm](pathfinder-bestiary-2-items/giVESjdTpw33NygU.htm)|Norn Shears|Cizalla de Norn|modificada|
|[gixaFzYqsyV9bsLi.htm](pathfinder-bestiary-2-items/gixaFzYqsyV9bsLi.htm)|Cairn Wight Spawn|Cairn Wight Spawn|modificada|
|[GJECjnP6onECplMu.htm](pathfinder-bestiary-2-items/GJECjnP6onECplMu.htm)|Fangs|Colmillos|modificada|
|[gJMFRPJuO2fffH1c.htm](pathfinder-bestiary-2-items/gJMFRPJuO2fffH1c.htm)|Deflecting Cloud|Nube deflectora|modificada|
|[GjmjXrm0BYOCXs5E.htm](pathfinder-bestiary-2-items/GjmjXrm0BYOCXs5E.htm)|Telepathy|Telepatía|modificada|
|[gkiQGmXDkxDHGY7j.htm](pathfinder-bestiary-2-items/gkiQGmXDkxDHGY7j.htm)|Mask of Power|Máscara de Poder|modificada|
|[GkMCAxjpf2Y5nfdv.htm](pathfinder-bestiary-2-items/GkMCAxjpf2Y5nfdv.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[gKqI8aZDucHlpC9C.htm](pathfinder-bestiary-2-items/gKqI8aZDucHlpC9C.htm)|Cowering Fear|Miedo acobardado|modificada|
|[gKujbrE9tdbREcZi.htm](pathfinder-bestiary-2-items/gKujbrE9tdbREcZi.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GL5oMe9C6pcXxqcU.htm](pathfinder-bestiary-2-items/GL5oMe9C6pcXxqcU.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[gL7E79l3yS72fXPo.htm](pathfinder-bestiary-2-items/gL7E79l3yS72fXPo.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[gm4ckzxne8UYVaZg.htm](pathfinder-bestiary-2-items/gm4ckzxne8UYVaZg.htm)|Feral Possession|Posesión Feral|modificada|
|[GmbRFl9ktqpdH1WE.htm](pathfinder-bestiary-2-items/GmbRFl9ktqpdH1WE.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gmjig3Ov1aG6tBOx.htm](pathfinder-bestiary-2-items/gmjig3Ov1aG6tBOx.htm)|Mandibles|Mandíbulas|modificada|
|[gmtlsmLh9eNdpNrk.htm](pathfinder-bestiary-2-items/gmtlsmLh9eNdpNrk.htm)|Mud Puddle|Charco de barro|modificada|
|[GnAoSJeodjtFoanm.htm](pathfinder-bestiary-2-items/GnAoSJeodjtFoanm.htm)|Feral Possession|Posesión Feral|modificada|
|[gNhak3Nf3ve1ouhK.htm](pathfinder-bestiary-2-items/gNhak3Nf3ve1ouhK.htm)|Claw|Garra|modificada|
|[gnL5MFzMfrbfAapT.htm](pathfinder-bestiary-2-items/gnL5MFzMfrbfAapT.htm)|Holy Armaments|Armamento Sagrada|modificada|
|[gOjlMNwWXWNHohGo.htm](pathfinder-bestiary-2-items/gOjlMNwWXWNHohGo.htm)|Mark Quarry|Mark Quarry|modificada|
|[goYkQxQdewpmD26j.htm](pathfinder-bestiary-2-items/goYkQxQdewpmD26j.htm)|Jaws|Fauces|modificada|
|[GoYV1mj4XKxDiBOM.htm](pathfinder-bestiary-2-items/GoYV1mj4XKxDiBOM.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[gozNFecLMRX00Wzz.htm](pathfinder-bestiary-2-items/gozNFecLMRX00Wzz.htm)|Frightful Presence|Frightful Presence|modificada|
|[gPcAYqbYyOADoHwc.htm](pathfinder-bestiary-2-items/gPcAYqbYyOADoHwc.htm)|Top-Heavy|Top-Heavy|modificada|
|[gPCpNSHZ6SvXUhij.htm](pathfinder-bestiary-2-items/gPCpNSHZ6SvXUhij.htm)|Painful Strikes|Golpes Dolorosos|modificada|
|[GpD6rwvIzYFSvtUC.htm](pathfinder-bestiary-2-items/GpD6rwvIzYFSvtUC.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[GPRRfLJKZALqfKud.htm](pathfinder-bestiary-2-items/GPRRfLJKZALqfKud.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GPSk2dGRxJdcBtW7.htm](pathfinder-bestiary-2-items/GPSk2dGRxJdcBtW7.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[gpZkw015Ns4Dbplk.htm](pathfinder-bestiary-2-items/gpZkw015Ns4Dbplk.htm)|Drowning Touch|Drowning Touch|modificada|
|[GQ4af1lP9hWhVhkx.htm](pathfinder-bestiary-2-items/GQ4af1lP9hWhVhkx.htm)|Frightful Presence|Frightful Presence|modificada|
|[gQnxWI3DPgkoWjmj.htm](pathfinder-bestiary-2-items/gQnxWI3DPgkoWjmj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[grFGZyZ8952tQ9yF.htm](pathfinder-bestiary-2-items/grFGZyZ8952tQ9yF.htm)|Entombing Breath|Entombing Breath|modificada|
|[GRiHxWvHx9rf0zcF.htm](pathfinder-bestiary-2-items/GRiHxWvHx9rf0zcF.htm)|Expel Infestation|Expulsar infestación|modificada|
|[GryMTmkSRqTAW3ol.htm](pathfinder-bestiary-2-items/GryMTmkSRqTAW3ol.htm)|Wing|Ala|modificada|
|[gS1EUP1J8sgiDyR1.htm](pathfinder-bestiary-2-items/gS1EUP1J8sgiDyR1.htm)|Consume Death|Consume Muerte|modificada|
|[GsaoA7w1bfpGuWvA.htm](pathfinder-bestiary-2-items/GsaoA7w1bfpGuWvA.htm)|Tentacle|Tentáculo|modificada|
|[gsfeTAQtmgDaEQI7.htm](pathfinder-bestiary-2-items/gsfeTAQtmgDaEQI7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GSglY8uncoV8v0jg.htm](pathfinder-bestiary-2-items/GSglY8uncoV8v0jg.htm)|Jet|Jet|modificada|
|[gSm7LVM6KgueNn8S.htm](pathfinder-bestiary-2-items/gSm7LVM6KgueNn8S.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Gsx5q76S7ZPL3iXd.htm](pathfinder-bestiary-2-items/Gsx5q76S7ZPL3iXd.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[GtJ8PtIPDvGSAIHU.htm](pathfinder-bestiary-2-items/GtJ8PtIPDvGSAIHU.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[GTW3qSprQnTyrwNU.htm](pathfinder-bestiary-2-items/GTW3qSprQnTyrwNU.htm)|Claw|Garra|modificada|
|[gu96K11Wqu5OcYew.htm](pathfinder-bestiary-2-items/gu96K11Wqu5OcYew.htm)|Jaws|Fauces|modificada|
|[guUOsE03Ck5JEoBu.htm](pathfinder-bestiary-2-items/guUOsE03Ck5JEoBu.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GVFq98KDosC4rgkt.htm](pathfinder-bestiary-2-items/GVFq98KDosC4rgkt.htm)|Jaws|Fauces|modificada|
|[gVLqh4wDw9v6njkz.htm](pathfinder-bestiary-2-items/gVLqh4wDw9v6njkz.htm)|Jaws|Fauces|modificada|
|[GVP9BghiDShSeHpL.htm](pathfinder-bestiary-2-items/GVP9BghiDShSeHpL.htm)|Alter Dweomer|Alter Dweomer|modificada|
|[gVRJ2KRIYk9slxtI.htm](pathfinder-bestiary-2-items/gVRJ2KRIYk9slxtI.htm)|Longbow|Arco largo|modificada|
|[gvU3ZMNnU7PcbF4v.htm](pathfinder-bestiary-2-items/gvU3ZMNnU7PcbF4v.htm)|Shield Block|Bloquear con escudo|modificada|
|[GWbkONUjqsTF8hP7.htm](pathfinder-bestiary-2-items/GWbkONUjqsTF8hP7.htm)|Claw|Garra|modificada|
|[GwBXsOeV4FFdxDzQ.htm](pathfinder-bestiary-2-items/GwBXsOeV4FFdxDzQ.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[gWsyeIZTyzTRH7z1.htm](pathfinder-bestiary-2-items/gWsyeIZTyzTRH7z1.htm)|Telepathy|Telepatía|modificada|
|[GY6jeRcNdp5Fd9OB.htm](pathfinder-bestiary-2-items/GY6jeRcNdp5Fd9OB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GYapjknILGOMiV84.htm](pathfinder-bestiary-2-items/GYapjknILGOMiV84.htm)|-2 Status Bonus on Saves vs. Curses|Bonificador de situación de -2 a las salvaciones contra maldiciones.|modificada|
|[gYBpkJDYJYKODQRK.htm](pathfinder-bestiary-2-items/gYBpkJDYJYKODQRK.htm)|Gem Gaze|Gem Gaze|modificada|
|[GyMOxyS6WyN6PNVC.htm](pathfinder-bestiary-2-items/GyMOxyS6WyN6PNVC.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[GYPODdG0fWWAPk5y.htm](pathfinder-bestiary-2-items/GYPODdG0fWWAPk5y.htm)|Frond|Frond|modificada|
|[GYQMiGEbQopeX3bK.htm](pathfinder-bestiary-2-items/GYQMiGEbQopeX3bK.htm)|Flowing Hair|Flowing Hair|modificada|
|[gyVj6MqUbxY54dB9.htm](pathfinder-bestiary-2-items/gyVj6MqUbxY54dB9.htm)|Light of Avarice|Luz de la Avaricia|modificada|
|[gYXpVKd4NC2QDwe3.htm](pathfinder-bestiary-2-items/gYXpVKd4NC2QDwe3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Gz3flOxBg3X75q6s.htm](pathfinder-bestiary-2-items/Gz3flOxBg3X75q6s.htm)|Constant Spells|Constant Spells|modificada|
|[GZeUcgFbUA7B68BW.htm](pathfinder-bestiary-2-items/GZeUcgFbUA7B68BW.htm)|Telepathy|Telepatía|modificada|
|[gZfWtX8zkOoe49o7.htm](pathfinder-bestiary-2-items/gZfWtX8zkOoe49o7.htm)|Regeneration 25 (Deactivated by Vorpal Weapons)|Regeneración 25 (Desactivado por armas Vorpalinas)|modificada|
|[gzQlpS0D81W4iCub.htm](pathfinder-bestiary-2-items/gzQlpS0D81W4iCub.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[h0VU0sBf5jz6KCu1.htm](pathfinder-bestiary-2-items/h0VU0sBf5jz6KCu1.htm)|The Sea's Revenge|La Venganza del Mar|modificada|
|[h10snW2DCZ2hS7fP.htm](pathfinder-bestiary-2-items/h10snW2DCZ2hS7fP.htm)|Claw|Garra|modificada|
|[h155XUVgiUJ5onse.htm](pathfinder-bestiary-2-items/h155XUVgiUJ5onse.htm)|Blink Resistances|Resistencias de intermitencia|modificada|
|[h1bIOksYIZLff4Jb.htm](pathfinder-bestiary-2-items/h1bIOksYIZLff4Jb.htm)|Jaws|Fauces|modificada|
|[H1CrtJTYHt1zROG7.htm](pathfinder-bestiary-2-items/H1CrtJTYHt1zROG7.htm)|Rend|Rasgadura|modificada|
|[h20KMmp1wuDSm1Oa.htm](pathfinder-bestiary-2-items/h20KMmp1wuDSm1Oa.htm)|Seed Spray|Spray de semillas|modificada|
|[h282lP6zRpC6R83o.htm](pathfinder-bestiary-2-items/h282lP6zRpC6R83o.htm)|Swarming Infestation|Swarming Infestation|modificada|
|[H2rT9wdiejx3F1b0.htm](pathfinder-bestiary-2-items/H2rT9wdiejx3F1b0.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[h35hRTaiSXOfVZFA.htm](pathfinder-bestiary-2-items/h35hRTaiSXOfVZFA.htm)|Echolocation|Ecolocalización|modificada|
|[h3krrxTbAiZjVs8m.htm](pathfinder-bestiary-2-items/h3krrxTbAiZjVs8m.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[H3y3IbgMpQWevqRk.htm](pathfinder-bestiary-2-items/H3y3IbgMpQWevqRk.htm)|Drain Life|Drenar Vida|modificada|
|[h4MdAekw9ZoGZc2Q.htm](pathfinder-bestiary-2-items/h4MdAekw9ZoGZc2Q.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[H5H2qTd7RH9FnyiA.htm](pathfinder-bestiary-2-items/H5H2qTd7RH9FnyiA.htm)|Enliven Foliage|Animar follaje|modificada|
|[H5oOhIYYYiEvkna6.htm](pathfinder-bestiary-2-items/H5oOhIYYYiEvkna6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[h5vOq3kLWQKQrs6h.htm](pathfinder-bestiary-2-items/h5vOq3kLWQKQrs6h.htm)|Swallow Whole|Engullir Todo|modificada|
|[h61KXB1CbfC2hTh2.htm](pathfinder-bestiary-2-items/h61KXB1CbfC2hTh2.htm)|Swallow Whole|Engullir Todo|modificada|
|[h68Rp5GCPIpGGeyq.htm](pathfinder-bestiary-2-items/h68Rp5GCPIpGGeyq.htm)|Breath Weapon|Breath Weapon|modificada|
|[h69jYWXNKYbuTyzE.htm](pathfinder-bestiary-2-items/h69jYWXNKYbuTyzE.htm)|Breath Weapon|Breath Weapon|modificada|
|[H6d3brisQR3MDHO6.htm](pathfinder-bestiary-2-items/H6d3brisQR3MDHO6.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[H6WOpA1s4iRwT9Ik.htm](pathfinder-bestiary-2-items/H6WOpA1s4iRwT9Ik.htm)|Scent|Scent|modificada|
|[H7is94BX3CDB78hl.htm](pathfinder-bestiary-2-items/H7is94BX3CDB78hl.htm)|Fist|Puño|modificada|
|[H7lzbseyLDhs5Tp3.htm](pathfinder-bestiary-2-items/H7lzbseyLDhs5Tp3.htm)|Sound Mimicry|Imitación de sonido|modificada|
|[H7ovqSFCx0c885Da.htm](pathfinder-bestiary-2-items/H7ovqSFCx0c885Da.htm)|Tentacles|Tentáculos|modificada|
|[h7vIylds9BVXZM8F.htm](pathfinder-bestiary-2-items/h7vIylds9BVXZM8F.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[H7ZdigvTaI99Ab76.htm](pathfinder-bestiary-2-items/H7ZdigvTaI99Ab76.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H8b5ywUVrI7pjhB8.htm](pathfinder-bestiary-2-items/H8b5ywUVrI7pjhB8.htm)|Shortsword|Espada corta|modificada|
|[H97yITJBuctoMctx.htm](pathfinder-bestiary-2-items/H97yITJBuctoMctx.htm)|Change Shape|Change Shape|modificada|
|[H9cKHc9OAHiXKDzf.htm](pathfinder-bestiary-2-items/H9cKHc9OAHiXKDzf.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[H9Hu5uk0AogKrXF4.htm](pathfinder-bestiary-2-items/H9Hu5uk0AogKrXF4.htm)|Regeneration 15 (deactivated by evil)|Regeneración 15 (desactivado por maligno)|modificada|
|[h9Ugyirm1B1g6pt6.htm](pathfinder-bestiary-2-items/h9Ugyirm1B1g6pt6.htm)|Jaws|Fauces|modificada|
|[hA360VLZ8uHZCZxF.htm](pathfinder-bestiary-2-items/hA360VLZ8uHZCZxF.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[hA4DPWIY7hQbSa0i.htm](pathfinder-bestiary-2-items/hA4DPWIY7hQbSa0i.htm)|Dart|Dardo|modificada|
|[HAh9hAsU8XCNWPOU.htm](pathfinder-bestiary-2-items/HAh9hAsU8XCNWPOU.htm)|Organ of Endless Water|Órgano de Agua Interminable|modificada|
|[HAxp8tP3tYXocejD.htm](pathfinder-bestiary-2-items/HAxp8tP3tYXocejD.htm)|Claw|Garra|modificada|
|[HaXqTHQH97u8nuJK.htm](pathfinder-bestiary-2-items/HaXqTHQH97u8nuJK.htm)|Fist|Puño|modificada|
|[HaYQ8Cd4C0hYSFdc.htm](pathfinder-bestiary-2-items/HaYQ8Cd4C0hYSFdc.htm)|Leap Attack|Salto sin carrerilla|modificada|
|[hbf277SQgtMCjjWF.htm](pathfinder-bestiary-2-items/hbf277SQgtMCjjWF.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[hBvEzmMtwWP1eG7b.htm](pathfinder-bestiary-2-items/hBvEzmMtwWP1eG7b.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[HcJNEFmJC7L4Ku0M.htm](pathfinder-bestiary-2-items/HcJNEFmJC7L4Ku0M.htm)|Rock|Roca|modificada|
|[Hd5SG091X71rJV1D.htm](pathfinder-bestiary-2-items/Hd5SG091X71rJV1D.htm)|Confusing Display|Exhibición desconcertante|modificada|
|[hDdpyAhdnjpO1Bp9.htm](pathfinder-bestiary-2-items/hDdpyAhdnjpO1Bp9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[HDyQbtYQNxLarefS.htm](pathfinder-bestiary-2-items/HDyQbtYQNxLarefS.htm)|Grab|Agarrado|modificada|
|[he0x5StuDg4yge3c.htm](pathfinder-bestiary-2-items/he0x5StuDg4yge3c.htm)|Claw|Garra|modificada|
|[hegoW9J1eMFWn0I6.htm](pathfinder-bestiary-2-items/hegoW9J1eMFWn0I6.htm)|Grab|Agarrado|modificada|
|[hEsUevgpSNSIcNzB.htm](pathfinder-bestiary-2-items/hEsUevgpSNSIcNzB.htm)|Pounce|Abalanzarse|modificada|
|[HFAfo9jbjahhbw5J.htm](pathfinder-bestiary-2-items/HFAfo9jbjahhbw5J.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[hFDR91zB1cjh1UHv.htm](pathfinder-bestiary-2-items/hFDR91zB1cjh1UHv.htm)|Dead Tree|Árbol Muerto|modificada|
|[hg3lMJ6qrkw8WpZ3.htm](pathfinder-bestiary-2-items/hg3lMJ6qrkw8WpZ3.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[HGVXqBgfTKK0Gu4n.htm](pathfinder-bestiary-2-items/HGVXqBgfTKK0Gu4n.htm)|Trunk|Tronco|modificada|
|[HHnlE5IJhxgzBkin.htm](pathfinder-bestiary-2-items/HHnlE5IJhxgzBkin.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hHqx5bavcH4BYz5s.htm](pathfinder-bestiary-2-items/hHqx5bavcH4BYz5s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HHRUhEQ6DJPe6kyX.htm](pathfinder-bestiary-2-items/HHRUhEQ6DJPe6kyX.htm)|Claw|Garra|modificada|
|[HHscPOBzNRjg9IJ5.htm](pathfinder-bestiary-2-items/HHscPOBzNRjg9IJ5.htm)|Smoke Vision|Visión de Humo|modificada|
|[HhUiMTB8Qa8ySlVM.htm](pathfinder-bestiary-2-items/HhUiMTB8Qa8ySlVM.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[HilJh22elkeshhLj.htm](pathfinder-bestiary-2-items/HilJh22elkeshhLj.htm)|Wing|Ala|modificada|
|[HIn5PSEUsrEdOYWT.htm](pathfinder-bestiary-2-items/HIn5PSEUsrEdOYWT.htm)|Trunk|Tronco|modificada|
|[HIRmU6WblxuDmWc6.htm](pathfinder-bestiary-2-items/HIRmU6WblxuDmWc6.htm)|Fleshgout|Desprender carne|modificada|
|[hkgYTbnzCzYBfP7k.htm](pathfinder-bestiary-2-items/hkgYTbnzCzYBfP7k.htm)|Voice Imitation|Imitación de voz|modificada|
|[Hkm57fRjKQG62Gkr.htm](pathfinder-bestiary-2-items/Hkm57fRjKQG62Gkr.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[HL1pjayMZJpK1JID.htm](pathfinder-bestiary-2-items/HL1pjayMZJpK1JID.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[HL22Gfjw3DN4v0Sx.htm](pathfinder-bestiary-2-items/HL22Gfjw3DN4v0Sx.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[HLcMADTgT1nixxce.htm](pathfinder-bestiary-2-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|Aliento de la ciénaga|modificada|
|[HLOEvF4GMvQEPHct.htm](pathfinder-bestiary-2-items/HLOEvF4GMvQEPHct.htm)|Camouflaged Step|Camuflaje Paso|modificada|
|[hm7ls5Yo0n7QOWMl.htm](pathfinder-bestiary-2-items/hm7ls5Yo0n7QOWMl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Hmb2WHTATQHONj7o.htm](pathfinder-bestiary-2-items/Hmb2WHTATQHONj7o.htm)|Tail|Tail|modificada|
|[HMmnA0DwFNibve0c.htm](pathfinder-bestiary-2-items/HMmnA0DwFNibve0c.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[HmU4KBbkb30WqDpU.htm](pathfinder-bestiary-2-items/HmU4KBbkb30WqDpU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Hn5PgfCFMwbKdN20.htm](pathfinder-bestiary-2-items/Hn5PgfCFMwbKdN20.htm)|Self-Loathing|Autodesprecio|modificada|
|[hNAE5swsHtvsyity.htm](pathfinder-bestiary-2-items/hNAE5swsHtvsyity.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HNfXE0yusiYA2juN.htm](pathfinder-bestiary-2-items/HNfXE0yusiYA2juN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HnkrKvCxJTmladAk.htm](pathfinder-bestiary-2-items/HnkrKvCxJTmladAk.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[HOVV49IZbhN2hIwN.htm](pathfinder-bestiary-2-items/HOVV49IZbhN2hIwN.htm)|Scent|Scent|modificada|
|[HphLKNRNlCVczAuz.htm](pathfinder-bestiary-2-items/HphLKNRNlCVczAuz.htm)|Claw|Garra|modificada|
|[hPjYRtGf041nnmLw.htm](pathfinder-bestiary-2-items/hPjYRtGf041nnmLw.htm)|Stinger|Aguijón|modificada|
|[HpLHoXbsdK98PiDh.htm](pathfinder-bestiary-2-items/HpLHoXbsdK98PiDh.htm)|Solid Refrain|Estribillo Sólido|modificada|
|[HPPj6Nf4GtdwCAEx.htm](pathfinder-bestiary-2-items/HPPj6Nf4GtdwCAEx.htm)|No Hearing|No Hearing|modificada|
|[hPXSQxaDWNJRZg36.htm](pathfinder-bestiary-2-items/hPXSQxaDWNJRZg36.htm)|Constrict|Restringir|modificada|
|[HQ205X1b6CWTHXfU.htm](pathfinder-bestiary-2-items/HQ205X1b6CWTHXfU.htm)|Slow|Lentificado/a|modificada|
|[hQdMdV8u5o7eWZEV.htm](pathfinder-bestiary-2-items/hQdMdV8u5o7eWZEV.htm)|Jaws|Fauces|modificada|
|[HQfx53oV60UYo0lg.htm](pathfinder-bestiary-2-items/HQfx53oV60UYo0lg.htm)|Gremlin Snare|Gremlin Snare|modificada|
|[HQTX3r7nEv9YINQh.htm](pathfinder-bestiary-2-items/HQTX3r7nEv9YINQh.htm)|Ferocity|Ferocidad|modificada|
|[HqvJxFfKrYlRxHWC.htm](pathfinder-bestiary-2-items/HqvJxFfKrYlRxHWC.htm)|Rend|Rasgadura|modificada|
|[HqwhBLhYDbCBbv8f.htm](pathfinder-bestiary-2-items/HqwhBLhYDbCBbv8f.htm)|Constant Spells|Constant Spells|modificada|
|[hr8UQcvJzWfTNGoH.htm](pathfinder-bestiary-2-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 5|Curación rápida planar 5|modificada|
|[HR9jZiX9oYUmGloZ.htm](pathfinder-bestiary-2-items/HR9jZiX9oYUmGloZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hREXXxR78zjxAtPW.htm](pathfinder-bestiary-2-items/hREXXxR78zjxAtPW.htm)|Shoal Linnorm Venom|Veneno del linnorm del bajío|modificada|
|[HRJpFIoBwbJ1rwnG.htm](pathfinder-bestiary-2-items/HRJpFIoBwbJ1rwnG.htm)|Telepathy|Telepatía|modificada|
|[HrUGB7et2xcTkWSB.htm](pathfinder-bestiary-2-items/HrUGB7et2xcTkWSB.htm)|Mandibles|Mandíbulas|modificada|
|[hS50gj87pD1XnQk9.htm](pathfinder-bestiary-2-items/hS50gj87pD1XnQk9.htm)|Claw Storm|Tormenta de garras|modificada|
|[Hsh9yw0IW6kN2n8t.htm](pathfinder-bestiary-2-items/Hsh9yw0IW6kN2n8t.htm)|Electrified Blood|Sangre electrificada|modificada|
|[hSqvFZoc1oaDpRVt.htm](pathfinder-bestiary-2-items/hSqvFZoc1oaDpRVt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hSvaFFK9ERUSfUry.htm](pathfinder-bestiary-2-items/hSvaFFK9ERUSfUry.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[hswGSODfM3HdK8zC.htm](pathfinder-bestiary-2-items/hswGSODfM3HdK8zC.htm)|Stench|Hedor|modificada|
|[HT78d5cKcguY6IcL.htm](pathfinder-bestiary-2-items/HT78d5cKcguY6IcL.htm)|Pounce|Abalanzarse|modificada|
|[HTGkYCrWWOoIvAja.htm](pathfinder-bestiary-2-items/HTGkYCrWWOoIvAja.htm)|Serpentfolk Venom|Serpentfolk Venom|modificada|
|[hTqMUAAnd9iTtvQW.htm](pathfinder-bestiary-2-items/hTqMUAAnd9iTtvQW.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[hTuKXeGWvUSUobx7.htm](pathfinder-bestiary-2-items/hTuKXeGWvUSUobx7.htm)|Ball Lightning Breath|Aliento de relámpago de bola|modificada|
|[Hu1SdyNVG4WJmNcx.htm](pathfinder-bestiary-2-items/Hu1SdyNVG4WJmNcx.htm)|Soul Spells|Hechizos de Alma|modificada|
|[Hu5L9AYzwJsXxIDY.htm](pathfinder-bestiary-2-items/Hu5L9AYzwJsXxIDY.htm)|Tremorsense 30 feet|Tremorsense 30 pies|modificada|
|[hV3hgI3sPR9ZrQeK.htm](pathfinder-bestiary-2-items/hV3hgI3sPR9ZrQeK.htm)|Improved Grab|Agarrado mejorado|modificada|
|[HViM44dkUyHEWK31.htm](pathfinder-bestiary-2-items/HViM44dkUyHEWK31.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[HvL4pznzaaZwsFkA.htm](pathfinder-bestiary-2-items/HvL4pznzaaZwsFkA.htm)|Claw|Garra|modificada|
|[hVmOEk3NrvvmFkKg.htm](pathfinder-bestiary-2-items/hVmOEk3NrvvmFkKg.htm)|Painsight|Painsight|modificada|
|[hvQmC6GTFeQoVTYG.htm](pathfinder-bestiary-2-items/hvQmC6GTFeQoVTYG.htm)|Breath Weapon|Breath Weapon|modificada|
|[hw7F7aaLP2U1MYZy.htm](pathfinder-bestiary-2-items/hw7F7aaLP2U1MYZy.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[hWE70sgsZXn7ZEa7.htm](pathfinder-bestiary-2-items/hWE70sgsZXn7ZEa7.htm)|Greatsword|Greatsword|modificada|
|[hWi2Ff37mvsMC81Y.htm](pathfinder-bestiary-2-items/hWi2Ff37mvsMC81Y.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[HWKLKpUPvbnvqEcp.htm](pathfinder-bestiary-2-items/HWKLKpUPvbnvqEcp.htm)|Horn|Cuerno|modificada|
|[hwNl8SFYu6kOmwly.htm](pathfinder-bestiary-2-items/hwNl8SFYu6kOmwly.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[hwnMtKzru4aBoOu3.htm](pathfinder-bestiary-2-items/hwnMtKzru4aBoOu3.htm)|Change Shape|Change Shape|modificada|
|[hwQlpkEec2QqnSxR.htm](pathfinder-bestiary-2-items/hwQlpkEec2QqnSxR.htm)|Regeneration 25 (deactivated by acid or cold)|Regeneración 25 (desactivado por ácido o frío)|modificada|
|[HWUmjgOASJXWhmgn.htm](pathfinder-bestiary-2-items/HWUmjgOASJXWhmgn.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[hwX8mlw73yOCgwYw.htm](pathfinder-bestiary-2-items/hwX8mlw73yOCgwYw.htm)|Braincloud|Braincloud|modificada|
|[hwXxwT85s7KyKoWq.htm](pathfinder-bestiary-2-items/hwXxwT85s7KyKoWq.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[hwZzmzZjVpUb64cH.htm](pathfinder-bestiary-2-items/hwZzmzZjVpUb64cH.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[hxGtO1RrJyi0gVvQ.htm](pathfinder-bestiary-2-items/hxGtO1RrJyi0gVvQ.htm)|Scent|Scent|modificada|
|[HXNPKNUC1fSgSuTM.htm](pathfinder-bestiary-2-items/HXNPKNUC1fSgSuTM.htm)|Mohrg Spawn|Mohrg Spawn|modificada|
|[HXybLZsE68ejwZbJ.htm](pathfinder-bestiary-2-items/HXybLZsE68ejwZbJ.htm)|Limb Extension|Extensión de extremidades|modificada|
|[hy3Lxy9hFGa73bQP.htm](pathfinder-bestiary-2-items/hy3Lxy9hFGa73bQP.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[hychIP30Jt5HOYED.htm](pathfinder-bestiary-2-items/hychIP30Jt5HOYED.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[hykxILVZRpdseaRk.htm](pathfinder-bestiary-2-items/hykxILVZRpdseaRk.htm)|Regeneration 25 (Deactivated by Acid or Fire)|Regeneración 25 (Desactivado por Ácido o Fuego)|modificada|
|[hyT0NpcWHdpgo5lt.htm](pathfinder-bestiary-2-items/hyT0NpcWHdpgo5lt.htm)|Dagger|Daga|modificada|
|[hyuJvmXFcS4R6bfc.htm](pathfinder-bestiary-2-items/hyuJvmXFcS4R6bfc.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hzr2Jl7pTcCFNWlF.htm](pathfinder-bestiary-2-items/hzr2Jl7pTcCFNWlF.htm)|Greataxe|Greataxe|modificada|
|[I06QLdoJMbnJD3Pu.htm](pathfinder-bestiary-2-items/I06QLdoJMbnJD3Pu.htm)|Create Spawn|Elaborar Spawn|modificada|
|[i0ue3QRUsYmGAJDm.htm](pathfinder-bestiary-2-items/i0ue3QRUsYmGAJDm.htm)|Improved Grab|Agarrado mejorado|modificada|
|[I1cXiDVfmHepyrXf.htm](pathfinder-bestiary-2-items/I1cXiDVfmHepyrXf.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[i1qGIf7bGKRu3RUq.htm](pathfinder-bestiary-2-items/i1qGIf7bGKRu3RUq.htm)|Tail|Tail|modificada|
|[I1XiJaJSXxln1l7V.htm](pathfinder-bestiary-2-items/I1XiJaJSXxln1l7V.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[i2d0NrqSfbRm2l1Y.htm](pathfinder-bestiary-2-items/i2d0NrqSfbRm2l1Y.htm)|Shocking Douse|Extinguir electrizante|modificada|
|[I2ICHtXckQoNKHcm.htm](pathfinder-bestiary-2-items/I2ICHtXckQoNKHcm.htm)|Knockdown|Derribo|modificada|
|[I2sDX6PHhRSYYUZN.htm](pathfinder-bestiary-2-items/I2sDX6PHhRSYYUZN.htm)|Fist|Puño|modificada|
|[i3JP3msktn7Ld1pK.htm](pathfinder-bestiary-2-items/i3JP3msktn7Ld1pK.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[i47viSV57dRQC5ZG.htm](pathfinder-bestiary-2-items/i47viSV57dRQC5ZG.htm)|Tremorsense|Tremorsense|modificada|
|[I4ZjVogUXmOSRkNn.htm](pathfinder-bestiary-2-items/I4ZjVogUXmOSRkNn.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[I6QwINd18HEwJGWW.htm](pathfinder-bestiary-2-items/I6QwINd18HEwJGWW.htm)|Frightful Presence|Frightful Presence|modificada|
|[i8KkA2JGZTsQSxta.htm](pathfinder-bestiary-2-items/i8KkA2JGZTsQSxta.htm)|Engulf|Envolver|modificada|
|[i8TVY4kXRvQIaLLG.htm](pathfinder-bestiary-2-items/i8TVY4kXRvQIaLLG.htm)|Pollen Touch|Polen Touch|modificada|
|[i9n1VIjQaHqI1V9X.htm](pathfinder-bestiary-2-items/i9n1VIjQaHqI1V9X.htm)|Paralyzing Display|Pantalla paralizante|modificada|
|[i9N4Fg8nJJdpMQUf.htm](pathfinder-bestiary-2-items/i9N4Fg8nJJdpMQUf.htm)|Tongue|Lengua|modificada|
|[i9Rco0paxz9ohGlm.htm](pathfinder-bestiary-2-items/i9Rco0paxz9ohGlm.htm)|Scent|Scent|modificada|
|[iaO10SxubynwqoL4.htm](pathfinder-bestiary-2-items/iaO10SxubynwqoL4.htm)|Prepared Primal Spells|Prepared Primal Spells|modificada|
|[icFkORI0Y4bNqBbP.htm](pathfinder-bestiary-2-items/icFkORI0Y4bNqBbP.htm)|Tail|Tail|modificada|
|[iCMR05ML275h0Gek.htm](pathfinder-bestiary-2-items/iCMR05ML275h0Gek.htm)|Rebirth|Renacimiento|modificada|
|[IcWvzNL316a1QTak.htm](pathfinder-bestiary-2-items/IcWvzNL316a1QTak.htm)|Claw|Garra|modificada|
|[IeAmIUQMEYZdSbaq.htm](pathfinder-bestiary-2-items/IeAmIUQMEYZdSbaq.htm)|Grab|Agarrado|modificada|
|[ieuwqdM4zXO2i22Z.htm](pathfinder-bestiary-2-items/ieuwqdM4zXO2i22Z.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[IFp4VK6X80nXiTJu.htm](pathfinder-bestiary-2-items/IFp4VK6X80nXiTJu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IHM0ZFUpH2qKKM7E.htm](pathfinder-bestiary-2-items/IHM0ZFUpH2qKKM7E.htm)|Regeneration 10 (deactivated by good or silver)|Regeneración 10 (desactivado por bueno o plata).|modificada|
|[IHn483eD21ShK5j4.htm](pathfinder-bestiary-2-items/IHn483eD21ShK5j4.htm)|Infuse Weapons|Infundir armas|modificada|
|[iHR7yRqYeZST3uB5.htm](pathfinder-bestiary-2-items/iHR7yRqYeZST3uB5.htm)|Constrict|Restringir|modificada|
|[IieLHPPUSUSDsN8d.htm](pathfinder-bestiary-2-items/IieLHPPUSUSDsN8d.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[iINgbpwr9zD0DJ0d.htm](pathfinder-bestiary-2-items/iINgbpwr9zD0DJ0d.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[Ij85zXU5FdthHI5o.htm](pathfinder-bestiary-2-items/Ij85zXU5FdthHI5o.htm)|Stolen Death|Muerte Robada|modificada|
|[IJc6CxwwAtsS27ry.htm](pathfinder-bestiary-2-items/IJc6CxwwAtsS27ry.htm)|Tremorsense|Tremorsense|modificada|
|[iJjnT8l7EIEaChZu.htm](pathfinder-bestiary-2-items/iJjnT8l7EIEaChZu.htm)|Camouflage|Camuflaje|modificada|
|[iKbhKQt4aD8JDvDd.htm](pathfinder-bestiary-2-items/iKbhKQt4aD8JDvDd.htm)|Absorb Flame|Absorber Flamígera|modificada|
|[iKjpMK3lSakpUkFY.htm](pathfinder-bestiary-2-items/iKjpMK3lSakpUkFY.htm)|Jaws|Fauces|modificada|
|[iKpGYrRKqAjGDO9B.htm](pathfinder-bestiary-2-items/iKpGYrRKqAjGDO9B.htm)|Constrict|Restringir|modificada|
|[IKRXYrbZOj22lXn8.htm](pathfinder-bestiary-2-items/IKRXYrbZOj22lXn8.htm)|Ferocity|Ferocidad|modificada|
|[IM4bochbTpbrXxfY.htm](pathfinder-bestiary-2-items/IM4bochbTpbrXxfY.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[imFNV40cUextfevX.htm](pathfinder-bestiary-2-items/imFNV40cUextfevX.htm)|Grasping Foliage|Grasping Foliage|modificada|
|[ImVU4G37cmGJMDoA.htm](pathfinder-bestiary-2-items/ImVU4G37cmGJMDoA.htm)|Sickle|Hoz|modificada|
|[InHy3ZKl3MHOyTX4.htm](pathfinder-bestiary-2-items/InHy3ZKl3MHOyTX4.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[injH2iWqg5QjQiqs.htm](pathfinder-bestiary-2-items/injH2iWqg5QjQiqs.htm)|Blade|Blade|modificada|
|[InntbXg8SWCGpsZF.htm](pathfinder-bestiary-2-items/InntbXg8SWCGpsZF.htm)|Telepathy|Telepatía|modificada|
|[IoE3s3hLaVewsjSQ.htm](pathfinder-bestiary-2-items/IoE3s3hLaVewsjSQ.htm)|Storm Aura|Aura de tormenta|modificada|
|[IoKSTbFElqXPiGiX.htm](pathfinder-bestiary-2-items/IoKSTbFElqXPiGiX.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Ipe3znrfNfiAvRCl.htm](pathfinder-bestiary-2-items/Ipe3znrfNfiAvRCl.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Ipf389K0Mqyu4zcP.htm](pathfinder-bestiary-2-items/Ipf389K0Mqyu4zcP.htm)|Scent|Scent|modificada|
|[ipGw6L7E6GUS565c.htm](pathfinder-bestiary-2-items/ipGw6L7E6GUS565c.htm)|Hypnosis|Hipnosis|modificada|
|[iPIBtZVZ5TaPEFp6.htm](pathfinder-bestiary-2-items/iPIBtZVZ5TaPEFp6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IPLCR3K5wWzQiSHK.htm](pathfinder-bestiary-2-items/IPLCR3K5wWzQiSHK.htm)|Discharge|Descarga|modificada|
|[IpYr4SLkhnzKvpdC.htm](pathfinder-bestiary-2-items/IpYr4SLkhnzKvpdC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[iqDrSwSZaUvRJKuG.htm](pathfinder-bestiary-2-items/iqDrSwSZaUvRJKuG.htm)|Grab|Agarrado|modificada|
|[IqEYrNctOMJsWe6k.htm](pathfinder-bestiary-2-items/IqEYrNctOMJsWe6k.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[IQsStyO8N3SIqIw2.htm](pathfinder-bestiary-2-items/IQsStyO8N3SIqIw2.htm)|Scent|Scent|modificada|
|[Ir6SQ9XsXTFTSPKi.htm](pathfinder-bestiary-2-items/Ir6SQ9XsXTFTSPKi.htm)|Protected by the Ancestors|Protegido por los Ancestros|modificada|
|[iRcU8dloUGz1q7VC.htm](pathfinder-bestiary-2-items/iRcU8dloUGz1q7VC.htm)|Claw|Garra|modificada|
|[iRmxw9CTYdvk3WOX.htm](pathfinder-bestiary-2-items/iRmxw9CTYdvk3WOX.htm)|Gatekeeper Aura|Aura de guardián|modificada|
|[irQzM9Rv3zR8bh6X.htm](pathfinder-bestiary-2-items/irQzM9Rv3zR8bh6X.htm)|Whirling Slice|Tajo giratorio|modificada|
|[IRXDgrccWGdvA4YK.htm](pathfinder-bestiary-2-items/IRXDgrccWGdvA4YK.htm)|Ferocity|Ferocidad|modificada|
|[iRZgPtkf6Lw3MzYm.htm](pathfinder-bestiary-2-items/iRZgPtkf6Lw3MzYm.htm)|Constant Spells|Constant Spells|modificada|
|[IS4BmsREr7eTZUPO.htm](pathfinder-bestiary-2-items/IS4BmsREr7eTZUPO.htm)|Claw|Garra|modificada|
|[is74odjgQwqelbqi.htm](pathfinder-bestiary-2-items/is74odjgQwqelbqi.htm)|Volcanic Purge|Purga volcánica|modificada|
|[ISiKvYdK9qbVBsG0.htm](pathfinder-bestiary-2-items/ISiKvYdK9qbVBsG0.htm)|Fangs|Colmillos|modificada|
|[ISU9cvmrd38iqIo6.htm](pathfinder-bestiary-2-items/ISU9cvmrd38iqIo6.htm)|Kimono|Kimono|modificada|
|[isUs3BDvVUggXstu.htm](pathfinder-bestiary-2-items/isUs3BDvVUggXstu.htm)|Shadow Traveler|Sombra Viajera|modificada|
|[iSVPM2m77YXV78mJ.htm](pathfinder-bestiary-2-items/iSVPM2m77YXV78mJ.htm)|Badger Rage|Furia del tejón|modificada|
|[iSvQy8CUUD5u4nsG.htm](pathfinder-bestiary-2-items/iSvQy8CUUD5u4nsG.htm)|Mandibles|Mandíbulas|modificada|
|[iT766eWfAydCiJYK.htm](pathfinder-bestiary-2-items/iT766eWfAydCiJYK.htm)|Mandibles|Mandíbulas|modificada|
|[iThujq9Fhag6Jad2.htm](pathfinder-bestiary-2-items/iThujq9Fhag6Jad2.htm)|Grip Throat|Grip Throat|modificada|
|[itoyZJLBh98BavI1.htm](pathfinder-bestiary-2-items/itoyZJLBh98BavI1.htm)|Entrench|Atrincherarse|modificada|
|[iTVvlmvMKiOcRmTQ.htm](pathfinder-bestiary-2-items/iTVvlmvMKiOcRmTQ.htm)|Ferocity|Ferocidad|modificada|
|[IukwgU6DfXFrQy49.htm](pathfinder-bestiary-2-items/IukwgU6DfXFrQy49.htm)|Inflict Warpwave|Infligir Onda Warp|modificada|
|[iuqPDrdlLQjQi1NU.htm](pathfinder-bestiary-2-items/iuqPDrdlLQjQi1NU.htm)|Bastard Sword|Espada Bastarda|modificada|
|[IvFL4oT7wjrxQZqw.htm](pathfinder-bestiary-2-items/IvFL4oT7wjrxQZqw.htm)|Claw|Garra|modificada|
|[iVrKx2VXmKTgxWvi.htm](pathfinder-bestiary-2-items/iVrKx2VXmKTgxWvi.htm)|Claw|Garra|modificada|
|[IvU2EF3nyrIZUdjp.htm](pathfinder-bestiary-2-items/IvU2EF3nyrIZUdjp.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[IVUIIQ9KDB8DHfvL.htm](pathfinder-bestiary-2-items/IVUIIQ9KDB8DHfvL.htm)|Jaws|Fauces|modificada|
|[ivy8kuAr2Ml8VdAF.htm](pathfinder-bestiary-2-items/ivy8kuAr2Ml8VdAF.htm)|Cold Adaptation|Adaptación al frío|modificada|
|[IW1NCko3S3NUTExU.htm](pathfinder-bestiary-2-items/IW1NCko3S3NUTExU.htm)|Knockdown|Derribo|modificada|
|[iw7xcu7OkpYHYyQ2.htm](pathfinder-bestiary-2-items/iw7xcu7OkpYHYyQ2.htm)|Greater Web Sense|Mayor sentido telara|modificada|
|[IWFZQHALE4qFDWJv.htm](pathfinder-bestiary-2-items/IWFZQHALE4qFDWJv.htm)|Snow Vision|Snow Vision|modificada|
|[IWIgUhii4TYY31l7.htm](pathfinder-bestiary-2-items/IWIgUhii4TYY31l7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[iWjW5DElqzkofyBS.htm](pathfinder-bestiary-2-items/iWjW5DElqzkofyBS.htm)|Fist|Puño|modificada|
|[IwPCTJvgvDQC90Yj.htm](pathfinder-bestiary-2-items/IwPCTJvgvDQC90Yj.htm)|Hurl Net|Lanzar red|modificada|
|[IWSVjUBcLTretOIM.htm](pathfinder-bestiary-2-items/IWSVjUBcLTretOIM.htm)|Claw|Garra|modificada|
|[IWyA3McLAMxHc0jq.htm](pathfinder-bestiary-2-items/IWyA3McLAMxHc0jq.htm)|Capsize|Volcar|modificada|
|[iWYCpNJjdadjk8JX.htm](pathfinder-bestiary-2-items/iWYCpNJjdadjk8JX.htm)|Briny Wound|Herida Hiriente|modificada|
|[Ixt0Bpsg2FgwTGiy.htm](pathfinder-bestiary-2-items/Ixt0Bpsg2FgwTGiy.htm)|Dispelling Roar|Disipar Rugido|modificada|
|[IxVJURBO9NY3Og9k.htm](pathfinder-bestiary-2-items/IxVJURBO9NY3Og9k.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ixXRVHF3jg0IMuKw.htm](pathfinder-bestiary-2-items/ixXRVHF3jg0IMuKw.htm)|Leprechaun Magic|Leprechaun Magic|modificada|
|[iYT6OEbdoPMem0XE.htm](pathfinder-bestiary-2-items/iYT6OEbdoPMem0XE.htm)|Children of the Night|Niños de la Noche|modificada|
|[IZ5Q9oQE8jylYahf.htm](pathfinder-bestiary-2-items/IZ5Q9oQE8jylYahf.htm)|Ultrasonic Blast|Ráfaga Ultrasónica|modificada|
|[iZA7ek0jbfpt5FDo.htm](pathfinder-bestiary-2-items/iZA7ek0jbfpt5FDo.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[izA7GPk45mKcDG02.htm](pathfinder-bestiary-2-items/izA7GPk45mKcDG02.htm)|Giant Ant Venom|Veneno de Hormiga Gigante|modificada|
|[IzbfeuZKm6hz1cWS.htm](pathfinder-bestiary-2-items/IzbfeuZKm6hz1cWS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IZKppOuI7oVJKs5K.htm](pathfinder-bestiary-2-items/IZKppOuI7oVJKs5K.htm)|Scent|Scent|modificada|
|[IZVxmKtbXiti9BZx.htm](pathfinder-bestiary-2-items/IZVxmKtbXiti9BZx.htm)|Cacophonous Roar|Cacophonous Roar|modificada|
|[iZxDrmolCkhIxWO6.htm](pathfinder-bestiary-2-items/iZxDrmolCkhIxWO6.htm)|Ranged Trip|Derribar a distancia|modificada|
|[J0PFKoQWakUEDyIJ.htm](pathfinder-bestiary-2-items/J0PFKoQWakUEDyIJ.htm)|Shortsword|Espada corta|modificada|
|[J0ri0JZTtI3g3kxN.htm](pathfinder-bestiary-2-items/J0ri0JZTtI3g3kxN.htm)|Change Shape|Change Shape|modificada|
|[J1OKxBmPeSehcjj9.htm](pathfinder-bestiary-2-items/J1OKxBmPeSehcjj9.htm)|Breath Weapon|Breath Weapon|modificada|
|[J2LUtQlsnbpMEQ1r.htm](pathfinder-bestiary-2-items/J2LUtQlsnbpMEQ1r.htm)|Tail|Tail|modificada|
|[J2UWT1TZOpg7ODbW.htm](pathfinder-bestiary-2-items/J2UWT1TZOpg7ODbW.htm)|Stoke the Fervent|Avivar a los fervientes|modificada|
|[j2XC7qI2EMqGvGSs.htm](pathfinder-bestiary-2-items/j2XC7qI2EMqGvGSs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[j45FbeObppNsRIhN.htm](pathfinder-bestiary-2-items/j45FbeObppNsRIhN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j4OHhvOuFnXfbDjB.htm](pathfinder-bestiary-2-items/j4OHhvOuFnXfbDjB.htm)|Spell Feedback|Spell Feedback|modificada|
|[J4SHx8p5wCH9xlMo.htm](pathfinder-bestiary-2-items/J4SHx8p5wCH9xlMo.htm)|Clench Jaws|Cerrar la mandíbula|modificada|
|[J7vnMM93liaoRzjV.htm](pathfinder-bestiary-2-items/J7vnMM93liaoRzjV.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[J83izDOsrGU2y7y6.htm](pathfinder-bestiary-2-items/J83izDOsrGU2y7y6.htm)|Lance Arm|Lance Arm|modificada|
|[j8BUnBP4Ahs3QmLx.htm](pathfinder-bestiary-2-items/j8BUnBP4Ahs3QmLx.htm)|Swarm Mind|Swarm Mind|modificada|
|[j8GeF2Jp36lFRgyF.htm](pathfinder-bestiary-2-items/j8GeF2Jp36lFRgyF.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[j8i5xeCEDDiWbh3z.htm](pathfinder-bestiary-2-items/j8i5xeCEDDiWbh3z.htm)|Fist|Puño|modificada|
|[J8KaQAnTGQqYov9e.htm](pathfinder-bestiary-2-items/J8KaQAnTGQqYov9e.htm)|Xill Eggs|Xill Eggs|modificada|
|[j8Wq9vNUrxbBHNGK.htm](pathfinder-bestiary-2-items/j8Wq9vNUrxbBHNGK.htm)|Throw Rock|Arrojar roca|modificada|
|[J9iEljd9P9EPB4wm.htm](pathfinder-bestiary-2-items/J9iEljd9P9EPB4wm.htm)|Grab|Agarrado|modificada|
|[j9L9BPR6t7eHpHAt.htm](pathfinder-bestiary-2-items/j9L9BPR6t7eHpHAt.htm)|Breath Weapon|Breath Weapon|modificada|
|[JAh7SWOTsIXvrckz.htm](pathfinder-bestiary-2-items/JAh7SWOTsIXvrckz.htm)|Water Travel|Water Travel|modificada|
|[JakacQ9SCpgoCpCp.htm](pathfinder-bestiary-2-items/JakacQ9SCpgoCpCp.htm)|Shocking Burst|Ráfaga Electrizante|modificada|
|[jBybuNj39jNUTdkz.htm](pathfinder-bestiary-2-items/jBybuNj39jNUTdkz.htm)|Scent Demons 60 feet|Perfumar demoníacos 60 pies.|modificada|
|[jC3hvioFdJ495oDv.htm](pathfinder-bestiary-2-items/jC3hvioFdJ495oDv.htm)|Crystallize Flesh|Cristalizar la carne|modificada|
|[JcDvmYqI4o86358N.htm](pathfinder-bestiary-2-items/JcDvmYqI4o86358N.htm)|Grievous Strike|Golpe lacerante|modificada|
|[JCHWJ2nJ8ZmLOBs5.htm](pathfinder-bestiary-2-items/JCHWJ2nJ8ZmLOBs5.htm)|Breath Weapon|Breath Weapon|modificada|
|[jCQO2RfqPLKqDeuF.htm](pathfinder-bestiary-2-items/jCQO2RfqPLKqDeuF.htm)|Slow Susceptibility|Susceptibilidad lentificada/a|modificada|
|[Jd5I0ORcv8JckftL.htm](pathfinder-bestiary-2-items/Jd5I0ORcv8JckftL.htm)|Stormsight|Stormsight|modificada|
|[jdfDA1e2B3iIQzhU.htm](pathfinder-bestiary-2-items/jdfDA1e2B3iIQzhU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[JdsDXPVL1v4JEQt5.htm](pathfinder-bestiary-2-items/JdsDXPVL1v4JEQt5.htm)|Wing|Ala|modificada|
|[jDvNUVTVAmMni2Va.htm](pathfinder-bestiary-2-items/jDvNUVTVAmMni2Va.htm)|Aura of Vitality|Aura de Vitalidad|modificada|
|[JdyDKp2bgIArfQmi.htm](pathfinder-bestiary-2-items/JdyDKp2bgIArfQmi.htm)|Improved Grab|Agarrado mejorado|modificada|
|[jEaI3soXVIxnAcPe.htm](pathfinder-bestiary-2-items/jEaI3soXVIxnAcPe.htm)|Breath Weapon|Breath Weapon|modificada|
|[jeV0r6dAZqnh6sD8.htm](pathfinder-bestiary-2-items/jeV0r6dAZqnh6sD8.htm)|Claw|Garra|modificada|
|[jfL2tU9w9oSiQlmg.htm](pathfinder-bestiary-2-items/jfL2tU9w9oSiQlmg.htm)|Enhance Venom|Enhance Venom|modificada|
|[JfR3IkhT8g6aU9ry.htm](pathfinder-bestiary-2-items/JfR3IkhT8g6aU9ry.htm)|Scythe Branch|Rama Guadaña|modificada|
|[jG3O965IQDZgxg9B.htm](pathfinder-bestiary-2-items/jG3O965IQDZgxg9B.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[JGCkaVnAvnwAxWOp.htm](pathfinder-bestiary-2-items/JGCkaVnAvnwAxWOp.htm)|Mauler|Vapulear|modificada|
|[JGEy5BcX7MQ17sdA.htm](pathfinder-bestiary-2-items/JGEy5BcX7MQ17sdA.htm)|Vrykolakas Vulnerabilities|Vulnerabilidades brucolacas|modificada|
|[jGhebYMT6hWNMixj.htm](pathfinder-bestiary-2-items/jGhebYMT6hWNMixj.htm)|Constant Spells|Constant Spells|modificada|
|[jgUPBrXeWbjnI6f1.htm](pathfinder-bestiary-2-items/jgUPBrXeWbjnI6f1.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[JgV2QrmvVWjF51JT.htm](pathfinder-bestiary-2-items/JgV2QrmvVWjF51JT.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[JGXYh66auv2dCHDv.htm](pathfinder-bestiary-2-items/JGXYh66auv2dCHDv.htm)|Volcanic Eruption|Erupción volcánica|modificada|
|[JHlhwEGUybavtbTq.htm](pathfinder-bestiary-2-items/JHlhwEGUybavtbTq.htm)|Living Form|Forma viva|modificada|
|[JIAc2IH8Ot3uIXfD.htm](pathfinder-bestiary-2-items/JIAc2IH8Ot3uIXfD.htm)|Trample|Trample|modificada|
|[jICQ6Y5JgzCUZXC3.htm](pathfinder-bestiary-2-items/jICQ6Y5JgzCUZXC3.htm)|Hurled Barb|Hurled Barb|modificada|
|[JIKJPPU6aBax4pjD.htm](pathfinder-bestiary-2-items/JIKJPPU6aBax4pjD.htm)|Mist Vision|Mist Vision|modificada|
|[JImKJVdKy1YbBpwf.htm](pathfinder-bestiary-2-items/JImKJVdKy1YbBpwf.htm)|Mandibles|Mandíbulas|modificada|
|[jISdvlh9f3wIBxZ8.htm](pathfinder-bestiary-2-items/jISdvlh9f3wIBxZ8.htm)|Freezing Blood|Sangre congelante|modificada|
|[jJ8meK8Q70uy8CHT.htm](pathfinder-bestiary-2-items/jJ8meK8Q70uy8CHT.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[jJAZZAqxbrSxGLXv.htm](pathfinder-bestiary-2-items/jJAZZAqxbrSxGLXv.htm)|Wing|Ala|modificada|
|[jJMWnNtJ3dst8bcj.htm](pathfinder-bestiary-2-items/jJMWnNtJ3dst8bcj.htm)|Drain Vigor|Drena Vigor|modificada|
|[JJTobEFnvJPV524S.htm](pathfinder-bestiary-2-items/JJTobEFnvJPV524S.htm)|Vorpal Fear|Miedo Vorpalina|modificada|
|[JK7loVXQ7UJJt3F9.htm](pathfinder-bestiary-2-items/JK7loVXQ7UJJt3F9.htm)|Fists of Thunder and Lightning|Puños de Trueno y Rayo|modificada|
|[jK8jhXaoNnEEDRLV.htm](pathfinder-bestiary-2-items/jK8jhXaoNnEEDRLV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jKCuP9xlPQoqlnGq.htm](pathfinder-bestiary-2-items/jKCuP9xlPQoqlnGq.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[JkVCc433MJHRl0qG.htm](pathfinder-bestiary-2-items/JkVCc433MJHRl0qG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JLgWzofKmiq0ajpJ.htm](pathfinder-bestiary-2-items/JLgWzofKmiq0ajpJ.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[JLM9pHvLtJxtkDH5.htm](pathfinder-bestiary-2-items/JLM9pHvLtJxtkDH5.htm)|Breath Weapon|Breath Weapon|modificada|
|[JlQUb1sbsYfmz5Vi.htm](pathfinder-bestiary-2-items/JlQUb1sbsYfmz5Vi.htm)|Cloak in Embers|Cloak in Embers|modificada|
|[JLqzN2RUPFKa8beH.htm](pathfinder-bestiary-2-items/JLqzN2RUPFKa8beH.htm)|Reflect Spell|Reflejar conjuros|modificada|
|[jM9qG2FB1AcJMMiL.htm](pathfinder-bestiary-2-items/jM9qG2FB1AcJMMiL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JMoMmAd0UH6b2Ckg.htm](pathfinder-bestiary-2-items/JMoMmAd0UH6b2Ckg.htm)|Telepathy|Telepatía|modificada|
|[jN9qLXiQfaOBlFfC.htm](pathfinder-bestiary-2-items/jN9qLXiQfaOBlFfC.htm)|Ramming Speed|Velocidad de embestida|modificada|
|[jNQXKdaz3dr7h6s3.htm](pathfinder-bestiary-2-items/jNQXKdaz3dr7h6s3.htm)|Jaws|Fauces|modificada|
|[JoBMwImgYPvlh4zo.htm](pathfinder-bestiary-2-items/JoBMwImgYPvlh4zo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[joBz08Vy0TnC5ELN.htm](pathfinder-bestiary-2-items/joBz08Vy0TnC5ELN.htm)|Jaws|Fauces|modificada|
|[JOeW89FGrajDYQsW.htm](pathfinder-bestiary-2-items/JOeW89FGrajDYQsW.htm)|Vrykolakas Vulnerabilities|Vulnerabilidades brucolacas|modificada|
|[jOuIuz89du1DGTFh.htm](pathfinder-bestiary-2-items/jOuIuz89du1DGTFh.htm)|Tongue|Lengua|modificada|
|[Joyek0dGuGD8dX9d.htm](pathfinder-bestiary-2-items/Joyek0dGuGD8dX9d.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[JpDIWcdXAS2ZFRY1.htm](pathfinder-bestiary-2-items/JpDIWcdXAS2ZFRY1.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[jpeqjLFSGfQaHqwb.htm](pathfinder-bestiary-2-items/jpeqjLFSGfQaHqwb.htm)|Bite|Muerdemuerde|modificada|
|[jppcxZjbxQ0e8W30.htm](pathfinder-bestiary-2-items/jppcxZjbxQ0e8W30.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jQ0BoxOUmGdOkGIf.htm](pathfinder-bestiary-2-items/jQ0BoxOUmGdOkGIf.htm)|Constrict|Restringir|modificada|
|[Jqh53awrV2X4sb81.htm](pathfinder-bestiary-2-items/Jqh53awrV2X4sb81.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[JqoRYjfcXOg9zHSr.htm](pathfinder-bestiary-2-items/JqoRYjfcXOg9zHSr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JqWD8UssH8Z7OeZz.htm](pathfinder-bestiary-2-items/JqWD8UssH8Z7OeZz.htm)|Limb Extension|Extensión de extremidades|modificada|
|[jr6lRjG0ZeZgW116.htm](pathfinder-bestiary-2-items/jr6lRjG0ZeZgW116.htm)|Soul Scream|Soul Scream|modificada|
|[JRi9pHjxAIEGIlv1.htm](pathfinder-bestiary-2-items/JRi9pHjxAIEGIlv1.htm)|Tentacle|Tentáculo|modificada|
|[JS74Gdm0nVhPpG4k.htm](pathfinder-bestiary-2-items/JS74Gdm0nVhPpG4k.htm)|Mindwarping|Mindwarping|modificada|
|[jskRIHujbQ0xJ8oy.htm](pathfinder-bestiary-2-items/jskRIHujbQ0xJ8oy.htm)|Frightful Presence|Frightful Presence|modificada|
|[JSpF7t2LpyYp70eW.htm](pathfinder-bestiary-2-items/JSpF7t2LpyYp70eW.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Jspfa7SNI6gNhPK4.htm](pathfinder-bestiary-2-items/Jspfa7SNI6gNhPK4.htm)|Coven|Coven|modificada|
|[jtcA1Dklz2pAKWhI.htm](pathfinder-bestiary-2-items/jtcA1Dklz2pAKWhI.htm)|Ghost Bane|Ghost Bane|modificada|
|[jtcRrpxqpR8quMII.htm](pathfinder-bestiary-2-items/jtcRrpxqpR8quMII.htm)|Quick Invisibility|Invisibilidad Rápida|modificada|
|[JtEaK5YV5AgypBAF.htm](pathfinder-bestiary-2-items/JtEaK5YV5AgypBAF.htm)|Change Shape|Change Shape|modificada|
|[jTkhIBakBrDpEBuu.htm](pathfinder-bestiary-2-items/jTkhIBakBrDpEBuu.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[JTKyH2bv7vq73eF0.htm](pathfinder-bestiary-2-items/JTKyH2bv7vq73eF0.htm)|Whiptail Centipede Venom|Veneno de Ciempiés Cola de Látigo|modificada|
|[jToSRHKjIQK8vQjE.htm](pathfinder-bestiary-2-items/jToSRHKjIQK8vQjE.htm)|Claw|Garra|modificada|
|[jTPTmoj95k6FZSf8.htm](pathfinder-bestiary-2-items/jTPTmoj95k6FZSf8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jURll9yGhDX2eIFe.htm](pathfinder-bestiary-2-items/jURll9yGhDX2eIFe.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[JVezHGtBp5knZ21Z.htm](pathfinder-bestiary-2-items/JVezHGtBp5knZ21Z.htm)|Jaws|Fauces|modificada|
|[jVgZ19s2KzgHUo1a.htm](pathfinder-bestiary-2-items/jVgZ19s2KzgHUo1a.htm)|Tentacle|Tentáculo|modificada|
|[jVnAGrnHMY0ZZTAt.htm](pathfinder-bestiary-2-items/jVnAGrnHMY0ZZTAt.htm)|Negative Healing|Curación negativa|modificada|
|[JVxGmdUEcMuZmGlg.htm](pathfinder-bestiary-2-items/JVxGmdUEcMuZmGlg.htm)|Light Pulse|Pulso de luz|modificada|
|[Jw5v3XqSesAoiBsp.htm](pathfinder-bestiary-2-items/Jw5v3XqSesAoiBsp.htm)|Reactive Shock|Electrizante Reactivo|modificada|
|[Jw8Rmo5ZFh9ouq86.htm](pathfinder-bestiary-2-items/Jw8Rmo5ZFh9ouq86.htm)|Ice Burrow|Madriguera de Hielo|modificada|
|[JWaVcBKV9F51VWja.htm](pathfinder-bestiary-2-items/JWaVcBKV9F51VWja.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[jWc6dwkDJvfVXXtk.htm](pathfinder-bestiary-2-items/jWc6dwkDJvfVXXtk.htm)|Trample|Trample|modificada|
|[jwprc3z4QABxyA1y.htm](pathfinder-bestiary-2-items/jwprc3z4QABxyA1y.htm)|Pyrexic Malaria|Malaria Pirexica|modificada|
|[JwstH3hUkDgNqT21.htm](pathfinder-bestiary-2-items/JwstH3hUkDgNqT21.htm)|Stinger|Aguijón|modificada|
|[jWt4W1OUjOcKKBNI.htm](pathfinder-bestiary-2-items/jWt4W1OUjOcKKBNI.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[jwZ2PMloPddIckyK.htm](pathfinder-bestiary-2-items/jwZ2PMloPddIckyK.htm)|Spirit Touch|Spirit Touch|modificada|
|[jXcwI039VI8MCBIE.htm](pathfinder-bestiary-2-items/jXcwI039VI8MCBIE.htm)|Rock|Roca|modificada|
|[JxE1ccJO8ctrpivc.htm](pathfinder-bestiary-2-items/JxE1ccJO8ctrpivc.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[jXl0E4vOdMPzQ3Hi.htm](pathfinder-bestiary-2-items/jXl0E4vOdMPzQ3Hi.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[jzbdXMuXge2oT2ol.htm](pathfinder-bestiary-2-items/jzbdXMuXge2oT2ol.htm)|Wolverine Rage|Wolverine Furia|modificada|
|[JzdZZiLkLG0WmOid.htm](pathfinder-bestiary-2-items/JzdZZiLkLG0WmOid.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[jzI4ObWhN1kXyFjY.htm](pathfinder-bestiary-2-items/jzI4ObWhN1kXyFjY.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[jZkG8pIe4dAne06Q.htm](pathfinder-bestiary-2-items/jZkG8pIe4dAne06Q.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[jzVssayBq4mzMU9u.htm](pathfinder-bestiary-2-items/jzVssayBq4mzMU9u.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JzWnn3KoGale3jgX.htm](pathfinder-bestiary-2-items/JzWnn3KoGale3jgX.htm)|Claw|Garra|modificada|
|[k0yAlAVkZbGrXfX3.htm](pathfinder-bestiary-2-items/k0yAlAVkZbGrXfX3.htm)|Jaws|Fauces|modificada|
|[K1woMBH8Ut9J50As.htm](pathfinder-bestiary-2-items/K1woMBH8Ut9J50As.htm)|Foot|Pie|modificada|
|[k2d7PB0MeYOIfjcj.htm](pathfinder-bestiary-2-items/k2d7PB0MeYOIfjcj.htm)|Filth Fever|Filth Fever|modificada|
|[k2HAX2N3qaYcbpb8.htm](pathfinder-bestiary-2-items/k2HAX2N3qaYcbpb8.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[K32GntqM2YIDwGgh.htm](pathfinder-bestiary-2-items/K32GntqM2YIDwGgh.htm)|Scent|Scent|modificada|
|[k3o7ZpWc6J6flWCQ.htm](pathfinder-bestiary-2-items/k3o7ZpWc6J6flWCQ.htm)|Breach|Emerger|modificada|
|[K45PxGQvJlwB2Y0j.htm](pathfinder-bestiary-2-items/K45PxGQvJlwB2Y0j.htm)|Shield Block|Bloquear con escudo|modificada|
|[k4tbzCH8jMb0d1Qk.htm](pathfinder-bestiary-2-items/k4tbzCH8jMb0d1Qk.htm)|Leng Spider Venom|Leng Araña Veneno|modificada|
|[K5BMaVGszyeB9sm8.htm](pathfinder-bestiary-2-items/K5BMaVGszyeB9sm8.htm)|Constant Spells|Constant Spells|modificada|
|[k6pOje8wFsRd6e1G.htm](pathfinder-bestiary-2-items/k6pOje8wFsRd6e1G.htm)|Pincer|Pinza|modificada|
|[k6tK5Cya0h5SEzNQ.htm](pathfinder-bestiary-2-items/k6tK5Cya0h5SEzNQ.htm)|Rend|Rasgadura|modificada|
|[k7IjtCkUuIzOv0dz.htm](pathfinder-bestiary-2-items/k7IjtCkUuIzOv0dz.htm)|Frightful Presence|Frightful Presence|modificada|
|[K7yXQ4PZPINLQXnj.htm](pathfinder-bestiary-2-items/K7yXQ4PZPINLQXnj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[K80GtqxqKN7Og68w.htm](pathfinder-bestiary-2-items/K80GtqxqKN7Og68w.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[K8JEIxdXOOyv0Mm6.htm](pathfinder-bestiary-2-items/K8JEIxdXOOyv0Mm6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[k8MjR75C6SWEa6gd.htm](pathfinder-bestiary-2-items/k8MjR75C6SWEa6gd.htm)|Flytrap Mouth|Flytrap Mouth|modificada|
|[k8sBT0SGwUD3ZUuE.htm](pathfinder-bestiary-2-items/k8sBT0SGwUD3ZUuE.htm)|+2 Status to All Saves vs. Divine Magic|+2 situación a todas las salvaciones contra magia divina.|modificada|
|[k8uRKWJkW8KHtabq.htm](pathfinder-bestiary-2-items/k8uRKWJkW8KHtabq.htm)|Grab|Agarrado|modificada|
|[kAgFhMe8TiBlKO3j.htm](pathfinder-bestiary-2-items/kAgFhMe8TiBlKO3j.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KBUEqfW6m7bI5a0g.htm](pathfinder-bestiary-2-items/KBUEqfW6m7bI5a0g.htm)|Thunderstrike|Thunderstrike|modificada|
|[kbVyC3rbr7rfXsxu.htm](pathfinder-bestiary-2-items/kbVyC3rbr7rfXsxu.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[kBxRAEuGBWNPM636.htm](pathfinder-bestiary-2-items/kBxRAEuGBWNPM636.htm)|Language Adaptation|Adaptación idiomática|modificada|
|[kbzaYm7nhy7Ip1Tj.htm](pathfinder-bestiary-2-items/kbzaYm7nhy7Ip1Tj.htm)|Grab|Agarrado|modificada|
|[kbzgtxi6mitCaLGP.htm](pathfinder-bestiary-2-items/kbzgtxi6mitCaLGP.htm)|Grant Desire|Grant Desire|modificada|
|[kCNQjJs87QDFBoGR.htm](pathfinder-bestiary-2-items/kCNQjJs87QDFBoGR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[kcRvfKLlgq9sdmsj.htm](pathfinder-bestiary-2-items/kcRvfKLlgq9sdmsj.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[KdGuOd6jczb7aCb5.htm](pathfinder-bestiary-2-items/KdGuOd6jczb7aCb5.htm)|Scent|Scent|modificada|
|[KDUeQyj1J71Vb3Q1.htm](pathfinder-bestiary-2-items/KDUeQyj1J71Vb3Q1.htm)|Absorb Wraith|Absorber Espectro|modificada|
|[KdVfLfgHwYXdIYXG.htm](pathfinder-bestiary-2-items/KdVfLfgHwYXdIYXG.htm)|Jaws|Fauces|modificada|
|[kDZcpk5GUe5jUU73.htm](pathfinder-bestiary-2-items/kDZcpk5GUe5jUU73.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[keXbMIiyh6Q6tjbr.htm](pathfinder-bestiary-2-items/keXbMIiyh6Q6tjbr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KF7pVWIzuG4o2wMP.htm](pathfinder-bestiary-2-items/KF7pVWIzuG4o2wMP.htm)|Holy Mace|Maza Sagrada|modificada|
|[KF8TJluc2GTgVJhl.htm](pathfinder-bestiary-2-items/KF8TJluc2GTgVJhl.htm)|Paddle|Paddle|modificada|
|[kFLwkDqwtyueOpyG.htm](pathfinder-bestiary-2-items/kFLwkDqwtyueOpyG.htm)|Breath Weapon|Breath Weapon|modificada|
|[kFNxTYgL056it5oI.htm](pathfinder-bestiary-2-items/kFNxTYgL056it5oI.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[kFT6c7OlCSLY8QF1.htm](pathfinder-bestiary-2-items/kFT6c7OlCSLY8QF1.htm)|Regeneration 15 (deactivated by acid or fire)|Regeneración 15 (desactivado por ácido o fuego)|modificada|
|[kGJwFA2RdVWHs6ty.htm](pathfinder-bestiary-2-items/kGJwFA2RdVWHs6ty.htm)|Fly Pox|Viruela de mosca|modificada|
|[kGnrd8Q7x3x8myE3.htm](pathfinder-bestiary-2-items/kGnrd8Q7x3x8myE3.htm)|Tremorsense|Tremorsense|modificada|
|[KgYaEP7QLp7DpYYS.htm](pathfinder-bestiary-2-items/KgYaEP7QLp7DpYYS.htm)|Constant Spells|Constant Spells|modificada|
|[kGZni5n9AHCuIa7c.htm](pathfinder-bestiary-2-items/kGZni5n9AHCuIa7c.htm)|Vicious Criticals|Críticos despiadados|modificada|
|[khgGPSxgTQcgev3o.htm](pathfinder-bestiary-2-items/khgGPSxgTQcgev3o.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[kHid4k2E9vYIzPDP.htm](pathfinder-bestiary-2-items/kHid4k2E9vYIzPDP.htm)|Claw|Garra|modificada|
|[khwh43CDMROkIfgf.htm](pathfinder-bestiary-2-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[ki1JmOjsmc5eRI42.htm](pathfinder-bestiary-2-items/ki1JmOjsmc5eRI42.htm)|Jaws|Fauces|modificada|
|[KIhN4LqmxLYnSLJj.htm](pathfinder-bestiary-2-items/KIhN4LqmxLYnSLJj.htm)|Change Shape|Change Shape|modificada|
|[kIvuH6nOKhB0zVQI.htm](pathfinder-bestiary-2-items/kIvuH6nOKhB0zVQI.htm)|Jaws|Fauces|modificada|
|[kj7lFKS7xdj7h83n.htm](pathfinder-bestiary-2-items/kj7lFKS7xdj7h83n.htm)|Change Shape|Change Shape|modificada|
|[kjaeBRLTBCSOl9cS.htm](pathfinder-bestiary-2-items/kjaeBRLTBCSOl9cS.htm)|Swallow Whole|Engullir Todo|modificada|
|[kJAxB9HM1zsPkPDC.htm](pathfinder-bestiary-2-items/kJAxB9HM1zsPkPDC.htm)|Tentacle|Tentáculo|modificada|
|[KJKzoE0ci2sMwyFX.htm](pathfinder-bestiary-2-items/KJKzoE0ci2sMwyFX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[KJVejaONCwwGt9ka.htm](pathfinder-bestiary-2-items/KJVejaONCwwGt9ka.htm)|Buck|Encabritarse|modificada|
|[Kk860My0b7DPBnAo.htm](pathfinder-bestiary-2-items/Kk860My0b7DPBnAo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KkNlymsNYCYGKgtS.htm](pathfinder-bestiary-2-items/KkNlymsNYCYGKgtS.htm)|Rend|Rasgadura|modificada|
|[kKubgrbJBPv5ke6D.htm](pathfinder-bestiary-2-items/kKubgrbJBPv5ke6D.htm)|Tail|Tail|modificada|
|[kkVojFishI8AxnVn.htm](pathfinder-bestiary-2-items/kkVojFishI8AxnVn.htm)|Slime Rot|Slime Rot|modificada|
|[kli2QsKUO2VSz2oy.htm](pathfinder-bestiary-2-items/kli2QsKUO2VSz2oy.htm)|Breath Weapon|Breath Weapon|modificada|
|[kM1AoSQF8Q3phCJt.htm](pathfinder-bestiary-2-items/kM1AoSQF8Q3phCJt.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[KmdxRqIdBe3pc6XL.htm](pathfinder-bestiary-2-items/KmdxRqIdBe3pc6XL.htm)|Trample|Trample|modificada|
|[Kmh2fOosCcnUFq1X.htm](pathfinder-bestiary-2-items/Kmh2fOosCcnUFq1X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kN2uT5XwOWMjkzA5.htm](pathfinder-bestiary-2-items/kN2uT5XwOWMjkzA5.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[kO1NYbSXz3VNkr4L.htm](pathfinder-bestiary-2-items/kO1NYbSXz3VNkr4L.htm)|Claw|Garra|modificada|
|[KO7ETr6PiZ0HJeHK.htm](pathfinder-bestiary-2-items/KO7ETr6PiZ0HJeHK.htm)|Holy Greatsword|Sagrada Gran Espada|modificada|
|[KovaVbwNiKoGj4DJ.htm](pathfinder-bestiary-2-items/KovaVbwNiKoGj4DJ.htm)|Greater Constrict|Mayor Restricción|modificada|
|[KpGCXx2t9NN3a85q.htm](pathfinder-bestiary-2-items/KpGCXx2t9NN3a85q.htm)|Fascinating Display|Pantalla fascinante|modificada|
|[KQ5qTss6bA4fQLVK.htm](pathfinder-bestiary-2-items/KQ5qTss6bA4fQLVK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[KQAihkBcgo2L9lGv.htm](pathfinder-bestiary-2-items/KQAihkBcgo2L9lGv.htm)|Breath Weapon|Breath Weapon|modificada|
|[KQBI90QlI48BEPP3.htm](pathfinder-bestiary-2-items/KQBI90QlI48BEPP3.htm)|Giant Toad Poison|Veneno de sapo gigante|modificada|
|[kqeAoYGDfSoyJUyC.htm](pathfinder-bestiary-2-items/kqeAoYGDfSoyJUyC.htm)|Web|Telara|modificada|
|[KqYkMZZ4hM5T9rej.htm](pathfinder-bestiary-2-items/KqYkMZZ4hM5T9rej.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kRAvWqrWJqwc6KU3.htm](pathfinder-bestiary-2-items/kRAvWqrWJqwc6KU3.htm)|Jaws|Fauces|modificada|
|[kryZwH2P5QUcvtyI.htm](pathfinder-bestiary-2-items/kryZwH2P5QUcvtyI.htm)|Split|Split|modificada|
|[ksfJahOG38JYy8bt.htm](pathfinder-bestiary-2-items/ksfJahOG38JYy8bt.htm)|Scent|Scent|modificada|
|[KswFDiMoMctr0zFY.htm](pathfinder-bestiary-2-items/KswFDiMoMctr0zFY.htm)|Draining Glance|Mirada drenada|modificada|
|[kSySgkElvNOAzAXn.htm](pathfinder-bestiary-2-items/kSySgkElvNOAzAXn.htm)|Enveloping Kimono|Kimono Amortajar|modificada|
|[KtaZ7mxQIQwm19ZM.htm](pathfinder-bestiary-2-items/KtaZ7mxQIQwm19ZM.htm)|Dimensional Tether|Dimensional Tether|modificada|
|[KTc5frqwQnvcjRY9.htm](pathfinder-bestiary-2-items/KTc5frqwQnvcjRY9.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ktdazJyPHLCoWXur.htm](pathfinder-bestiary-2-items/ktdazJyPHLCoWXur.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[KtIqgI2Oe8cbrguF.htm](pathfinder-bestiary-2-items/KtIqgI2Oe8cbrguF.htm)|Worm Trill|Worm Trill|modificada|
|[KtjrcbTPGagKVhSc.htm](pathfinder-bestiary-2-items/KtjrcbTPGagKVhSc.htm)|Desiccating Bite|Muerdemuerde Desecante|modificada|
|[KTxjm0shb9zyrh0z.htm](pathfinder-bestiary-2-items/KTxjm0shb9zyrh0z.htm)|Drain Life|Drenar Vida|modificada|
|[kUtQvCE4ePTqjluS.htm](pathfinder-bestiary-2-items/kUtQvCE4ePTqjluS.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kuV1kCq4ZEiH9DzH.htm](pathfinder-bestiary-2-items/kuV1kCq4ZEiH9DzH.htm)|Vulnerable to Curved Space|Vulnerable al espacio curvo|modificada|
|[kuW4IJohw5LunECQ.htm](pathfinder-bestiary-2-items/kuW4IJohw5LunECQ.htm)|Inhale Vitality|Inhalar vitalidad|modificada|
|[Kv1pkf5Ma30A8n58.htm](pathfinder-bestiary-2-items/Kv1pkf5Ma30A8n58.htm)|Sickening Display|Exhibición nauseabunda|modificada|
|[kV8qUZKG2gXQGCKq.htm](pathfinder-bestiary-2-items/kV8qUZKG2gXQGCKq.htm)|Tremorsense|Tremorsense|modificada|
|[kVhpKM2gcWYaxWvb.htm](pathfinder-bestiary-2-items/kVhpKM2gcWYaxWvb.htm)|Rock|Roca|modificada|
|[kVPXtEyRgZTAdwiv.htm](pathfinder-bestiary-2-items/kVPXtEyRgZTAdwiv.htm)|Wing|Ala|modificada|
|[KvQg0sUQsjltpY39.htm](pathfinder-bestiary-2-items/KvQg0sUQsjltpY39.htm)|Thunderbolt|Rayo|modificada|
|[kvsmf0ENkLJSdBP2.htm](pathfinder-bestiary-2-items/kvsmf0ENkLJSdBP2.htm)|Dominate Animal|Dominar Animal|modificada|
|[kxiMCVyD0e1fsT96.htm](pathfinder-bestiary-2-items/kxiMCVyD0e1fsT96.htm)|Soul Ward|Soul Ward|modificada|
|[kxo11YQf1psAw6Ig.htm](pathfinder-bestiary-2-items/kxo11YQf1psAw6Ig.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KXvcedAJNDy7eaCz.htm](pathfinder-bestiary-2-items/KXvcedAJNDy7eaCz.htm)|Foot|Pie|modificada|
|[Ky9wR2NCUma8mVz1.htm](pathfinder-bestiary-2-items/Ky9wR2NCUma8mVz1.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[KYPHjX89IxHU4Sz3.htm](pathfinder-bestiary-2-items/KYPHjX89IxHU4Sz3.htm)|Claw|Garra|modificada|
|[kzg3qoq55PGMljc3.htm](pathfinder-bestiary-2-items/kzg3qoq55PGMljc3.htm)|Spear Frog Venom|Lanza Rana Veneno|modificada|
|[kzI56kmfH5aQquo7.htm](pathfinder-bestiary-2-items/kzI56kmfH5aQquo7.htm)|Ice Claw|Ice Claw|modificada|
|[kziZybKPrc34cUoX.htm](pathfinder-bestiary-2-items/kziZybKPrc34cUoX.htm)|Jungle Stride|Zancada selvática|modificada|
|[Kzxltf72FUB0IXJp.htm](pathfinder-bestiary-2-items/Kzxltf72FUB0IXJp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[l0EHPtcSyp31Bf57.htm](pathfinder-bestiary-2-items/l0EHPtcSyp31Bf57.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[l0ivHsOCu0wN1yYF.htm](pathfinder-bestiary-2-items/l0ivHsOCu0wN1yYF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[l0J79vRF4TUP2DXP.htm](pathfinder-bestiary-2-items/l0J79vRF4TUP2DXP.htm)|Tail|Tail|modificada|
|[L17R2FDChGWE5pgD.htm](pathfinder-bestiary-2-items/L17R2FDChGWE5pgD.htm)|Void Death|Void Death|modificada|
|[L19PpXXqJh02tp7Q.htm](pathfinder-bestiary-2-items/L19PpXXqJh02tp7Q.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[l25YrJUMqQDtE3NT.htm](pathfinder-bestiary-2-items/l25YrJUMqQDtE3NT.htm)|Jaws|Fauces|modificada|
|[L35cRaqjhItrc6Po.htm](pathfinder-bestiary-2-items/L35cRaqjhItrc6Po.htm)|Claw|Garra|modificada|
|[l3FDPjYAvQMi4tfJ.htm](pathfinder-bestiary-2-items/l3FDPjYAvQMi4tfJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[l4eGICy0QK1EEERs.htm](pathfinder-bestiary-2-items/l4eGICy0QK1EEERs.htm)|Jaws|Fauces|modificada|
|[l5R6X6sffDy41u9i.htm](pathfinder-bestiary-2-items/l5R6X6sffDy41u9i.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[l6mRpAGFCZ7gTIb1.htm](pathfinder-bestiary-2-items/l6mRpAGFCZ7gTIb1.htm)|Rasping Tongue|Rasping Tongue|modificada|
|[l6ptuF1kw5FfVHfl.htm](pathfinder-bestiary-2-items/l6ptuF1kw5FfVHfl.htm)|Pus Burst|Pus Burst|modificada|
|[l6Q79Qlb9vVTOERz.htm](pathfinder-bestiary-2-items/l6Q79Qlb9vVTOERz.htm)|Hurled Weapon|Arma Arrojadiza|modificada|
|[l8iqEFTf53TY4e9n.htm](pathfinder-bestiary-2-items/l8iqEFTf53TY4e9n.htm)|Fist|Puño|modificada|
|[L8tZphaG7qnpPLpQ.htm](pathfinder-bestiary-2-items/L8tZphaG7qnpPLpQ.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[L8z6SDNKm57PB639.htm](pathfinder-bestiary-2-items/L8z6SDNKm57PB639.htm)|Claw|Garra|modificada|
|[L97RozqSBXjCukbA.htm](pathfinder-bestiary-2-items/L97RozqSBXjCukbA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[L9XO3TUsAurnPduF.htm](pathfinder-bestiary-2-items/L9XO3TUsAurnPduF.htm)|Jaws|Fauces|modificada|
|[lA1KEaw1II1i4twA.htm](pathfinder-bestiary-2-items/lA1KEaw1II1i4twA.htm)|Boar Charge|Carga de jabalí|modificada|
|[LA4f99Yw7Dk1e16d.htm](pathfinder-bestiary-2-items/LA4f99Yw7Dk1e16d.htm)|Fast Healing 2 (in dust or sand)|Curación rápida 2 (en polvo o arena)|modificada|
|[LaDtFmUzWtxdlYPv.htm](pathfinder-bestiary-2-items/LaDtFmUzWtxdlYPv.htm)|Scent|Scent|modificada|
|[laXpsxFAPFs8EXkA.htm](pathfinder-bestiary-2-items/laXpsxFAPFs8EXkA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[lB6z7CfDLMd5lPpG.htm](pathfinder-bestiary-2-items/lB6z7CfDLMd5lPpG.htm)|Starknife|Starknife|modificada|
|[lbG9FKqt6rVqqKrM.htm](pathfinder-bestiary-2-items/lbG9FKqt6rVqqKrM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LbkW4zjvlyreuGX1.htm](pathfinder-bestiary-2-items/LbkW4zjvlyreuGX1.htm)|Fangs|Colmillos|modificada|
|[lbNjzpti2ihhuipU.htm](pathfinder-bestiary-2-items/lbNjzpti2ihhuipU.htm)|Pounce|Abalanzarse|modificada|
|[LbTesuVUtAbIzLRY.htm](pathfinder-bestiary-2-items/LbTesuVUtAbIzLRY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LBtPHwiQcsiBIHG7.htm](pathfinder-bestiary-2-items/LBtPHwiQcsiBIHG7.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[Lc0XTAOFODosmzys.htm](pathfinder-bestiary-2-items/Lc0XTAOFODosmzys.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[lcdESp8kjoyZyuYE.htm](pathfinder-bestiary-2-items/lcdESp8kjoyZyuYE.htm)|Jaws|Fauces|modificada|
|[lCfKYASIe4pKTiK3.htm](pathfinder-bestiary-2-items/lCfKYASIe4pKTiK3.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[LcG0o3PUyemEv5oK.htm](pathfinder-bestiary-2-items/LcG0o3PUyemEv5oK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LcHF4msXaAScrZFk.htm](pathfinder-bestiary-2-items/LcHF4msXaAScrZFk.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[LcmY1jYUhcSTLbed.htm](pathfinder-bestiary-2-items/LcmY1jYUhcSTLbed.htm)|Stormflight|Stormflight|modificada|
|[lD6n6NYRsC4x82eE.htm](pathfinder-bestiary-2-items/lD6n6NYRsC4x82eE.htm)|Fire Missile|Misil de Fuego|modificada|
|[LDLwyqZVioLuytc9.htm](pathfinder-bestiary-2-items/LDLwyqZVioLuytc9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[le0p8FsNfX4jkVzn.htm](pathfinder-bestiary-2-items/le0p8FsNfX4jkVzn.htm)|Amplify Voltage|Amplificar Tensión|modificada|
|[lflIfQcsvaJJEVL7.htm](pathfinder-bestiary-2-items/lflIfQcsvaJJEVL7.htm)|Proboscis|Proboscis|modificada|
|[LfTrwLmDBA7f2MCm.htm](pathfinder-bestiary-2-items/LfTrwLmDBA7f2MCm.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[LFvM7oAwEJD1soNx.htm](pathfinder-bestiary-2-items/LFvM7oAwEJD1soNx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[lgHFmVhsmZOzCgFm.htm](pathfinder-bestiary-2-items/lgHFmVhsmZOzCgFm.htm)|Returning Starknife|Starknife Retornante|modificada|
|[lgkWnI9fHF9WMU2Q.htm](pathfinder-bestiary-2-items/lgkWnI9fHF9WMU2Q.htm)|Scent|Scent|modificada|
|[LGmgWdigRZKEMwNq.htm](pathfinder-bestiary-2-items/LGmgWdigRZKEMwNq.htm)|Swallow Whole|Engullir Todo|modificada|
|[LGq3APZstgY9LM9o.htm](pathfinder-bestiary-2-items/LGq3APZstgY9LM9o.htm)|Claw|Garra|modificada|
|[Lh6yYbwltdDOrlaP.htm](pathfinder-bestiary-2-items/Lh6yYbwltdDOrlaP.htm)|Pincers|Pinzas|modificada|
|[lhaBLbyRbWQceU9t.htm](pathfinder-bestiary-2-items/lhaBLbyRbWQceU9t.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[lIhEV7aWkpaAEZbm.htm](pathfinder-bestiary-2-items/lIhEV7aWkpaAEZbm.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[lIiFggTlCNveUaJM.htm](pathfinder-bestiary-2-items/lIiFggTlCNveUaJM.htm)|Vrykolakas Vulnerabilities|Vulnerabilidades brucolacas|modificada|
|[lIutGZGuGlSN1uvq.htm](pathfinder-bestiary-2-items/lIutGZGuGlSN1uvq.htm)|Fist|Puño|modificada|
|[lJ35tuNxwPc194AM.htm](pathfinder-bestiary-2-items/lJ35tuNxwPc194AM.htm)|Jaws|Fauces|modificada|
|[lJ7R0wPVIxspGA72.htm](pathfinder-bestiary-2-items/lJ7R0wPVIxspGA72.htm)|Smoke Form|Forma de Humo|modificada|
|[LJ98Uh0Kt9H0AdSx.htm](pathfinder-bestiary-2-items/LJ98Uh0Kt9H0AdSx.htm)|Grab|Agarrado|modificada|
|[ljlBgsFIBKMNCZPZ.htm](pathfinder-bestiary-2-items/ljlBgsFIBKMNCZPZ.htm)|Camouflage|Camuflaje|modificada|
|[LkKOdtSgJydv1rSu.htm](pathfinder-bestiary-2-items/LkKOdtSgJydv1rSu.htm)|Flytrap Hand|Mano Atrapamoscas|modificada|
|[LKQ6v7y7Jr6HEaPh.htm](pathfinder-bestiary-2-items/LKQ6v7y7Jr6HEaPh.htm)|Scent|Scent|modificada|
|[lL32oAteqyTNi0nT.htm](pathfinder-bestiary-2-items/lL32oAteqyTNi0nT.htm)|Hallucinatory Brine|Salmuera Alucinante|modificada|
|[LmNbsiKPocWzw1cu.htm](pathfinder-bestiary-2-items/LmNbsiKPocWzw1cu.htm)|Blood Berries|Bayas de sangre|modificada|
|[LMv9Jl8QyuoKhMpw.htm](pathfinder-bestiary-2-items/LMv9Jl8QyuoKhMpw.htm)|Spirit Touch|Spirit Touch|modificada|
|[Lmym2mXsN2UYnCLA.htm](pathfinder-bestiary-2-items/Lmym2mXsN2UYnCLA.htm)|Regeneration 5 (deactivated by good or silver)|Regeneración 5 (desactivado por bueno o plata).|modificada|
|[LN7JypAnmAunS0lg.htm](pathfinder-bestiary-2-items/LN7JypAnmAunS0lg.htm)|Twist the Blade|Twist the Blade|modificada|
|[LNbjPm5a6CTl4lko.htm](pathfinder-bestiary-2-items/LNbjPm5a6CTl4lko.htm)|Infernal Eye|Infernal Eye|modificada|
|[lNktyL9mwqYhh5mh.htm](pathfinder-bestiary-2-items/lNktyL9mwqYhh5mh.htm)|Scent|Scent|modificada|
|[loahwrlFxvmp2CXy.htm](pathfinder-bestiary-2-items/loahwrlFxvmp2CXy.htm)|Jet|Jet|modificada|
|[LOsTy4ZSFM2RSN4k.htm](pathfinder-bestiary-2-items/LOsTy4ZSFM2RSN4k.htm)|Rev Up|Rev Up|modificada|
|[loVBGCMa9ni2Xgvw.htm](pathfinder-bestiary-2-items/loVBGCMa9ni2Xgvw.htm)|Scent|Scent|modificada|
|[lOVrcn3AJGm4KWma.htm](pathfinder-bestiary-2-items/lOVrcn3AJGm4KWma.htm)|Eyes Of Flame|Eyes Of Flamígera|modificada|
|[lP5ul9ZfSW3FEFpa.htm](pathfinder-bestiary-2-items/lP5ul9ZfSW3FEFpa.htm)|Swarm Mind|Swarm Mind|modificada|
|[LPf9DIArTBdgTKrW.htm](pathfinder-bestiary-2-items/LPf9DIArTBdgTKrW.htm)|Push|Push|modificada|
|[lPGPRMZYecowSzGA.htm](pathfinder-bestiary-2-items/lPGPRMZYecowSzGA.htm)|Regeneration 10 (Deactivated by Cold Iron)|Regeneración 10 (Desactivado por Cold Iron)|modificada|
|[lPlAVVeLD4qqvZ8q.htm](pathfinder-bestiary-2-items/lPlAVVeLD4qqvZ8q.htm)|Trample|Trample|modificada|
|[LPLiXKoxMuJu4ifi.htm](pathfinder-bestiary-2-items/LPLiXKoxMuJu4ifi.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[LpnkFiQmJ2nxDYm5.htm](pathfinder-bestiary-2-items/LpnkFiQmJ2nxDYm5.htm)|Blinding Beams|Rayos cegadores|modificada|
|[lPnyzUu12dzRoAGk.htm](pathfinder-bestiary-2-items/lPnyzUu12dzRoAGk.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lPV8I6T6I09MjTy3.htm](pathfinder-bestiary-2-items/lPV8I6T6I09MjTy3.htm)|Jaws|Fauces|modificada|
|[LpxpKBeM4lLjGR0o.htm](pathfinder-bestiary-2-items/LpxpKBeM4lLjGR0o.htm)|Pincer|Pinza|modificada|
|[LQBlltvEb0rVnJYA.htm](pathfinder-bestiary-2-items/LQBlltvEb0rVnJYA.htm)|Heliotrope|Heliotropo|modificada|
|[lqG3xn6Di5qPS55E.htm](pathfinder-bestiary-2-items/lqG3xn6Di5qPS55E.htm)|Drain Soul|Drena Alma|modificada|
|[LqIsXOTw0kTELToc.htm](pathfinder-bestiary-2-items/LqIsXOTw0kTELToc.htm)|Antler|Antler|modificada|
|[LqnyBEmxdZr7oIfJ.htm](pathfinder-bestiary-2-items/LqnyBEmxdZr7oIfJ.htm)|Light Sickness|Enfermedad de la luz|modificada|
|[LqorFNvTu3dYKgMS.htm](pathfinder-bestiary-2-items/LqorFNvTu3dYKgMS.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[LqV3ag2bSeXmlk7D.htm](pathfinder-bestiary-2-items/LqV3ag2bSeXmlk7D.htm)|Boar Empathy|Empatía con los jabalíes|modificada|
|[lQZGx8hWnzcG2R6l.htm](pathfinder-bestiary-2-items/lQZGx8hWnzcG2R6l.htm)|Dazzling Brilliance|Dazzling Brilliance|modificada|
|[LRAuzrNMjhe4ZYR9.htm](pathfinder-bestiary-2-items/LRAuzrNMjhe4ZYR9.htm)|Jaws|Fauces|modificada|
|[LRu9rZ2D08Nr5Wa4.htm](pathfinder-bestiary-2-items/LRu9rZ2D08Nr5Wa4.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[lSdAwlGX28qPPhdE.htm](pathfinder-bestiary-2-items/lSdAwlGX28qPPhdE.htm)|Scent|Scent|modificada|
|[lSfQ3gfSsjhNS3HW.htm](pathfinder-bestiary-2-items/lSfQ3gfSsjhNS3HW.htm)|Tremorsense|Tremorsense|modificada|
|[lssPHJEGxZtK2T3A.htm](pathfinder-bestiary-2-items/lssPHJEGxZtK2T3A.htm)|Jaws|Fauces|modificada|
|[lt0GXB5vawxWhRLk.htm](pathfinder-bestiary-2-items/lt0GXB5vawxWhRLk.htm)|Fist|Puño|modificada|
|[Lt44trsI3w4Nptsb.htm](pathfinder-bestiary-2-items/Lt44trsI3w4Nptsb.htm)|Jaws|Fauces|modificada|
|[Lt5U1QzZ5Wx4rMiJ.htm](pathfinder-bestiary-2-items/Lt5U1QzZ5Wx4rMiJ.htm)|Caster Link|Caster Link|modificada|
|[LTcf2lgijfe0Cv96.htm](pathfinder-bestiary-2-items/LTcf2lgijfe0Cv96.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LUG7zInRwnS0c3vJ.htm](pathfinder-bestiary-2-items/LUG7zInRwnS0c3vJ.htm)|Cloud Walk|Cloud Walk|modificada|
|[lugrDlIHRd8kGDKs.htm](pathfinder-bestiary-2-items/lugrDlIHRd8kGDKs.htm)|Burning Swarm|Enjambre Ardiente|modificada|
|[lUhvPsYzVAYDYIIZ.htm](pathfinder-bestiary-2-items/lUhvPsYzVAYDYIIZ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[LUKxBKPHrzVKWWNz.htm](pathfinder-bestiary-2-items/LUKxBKPHrzVKWWNz.htm)|Tongue|Lengua|modificada|
|[Lv1GZd4xpvCGfjkz.htm](pathfinder-bestiary-2-items/Lv1GZd4xpvCGfjkz.htm)|Pollen Touch|Polen Touch|modificada|
|[LV58uBPdmRP5wve2.htm](pathfinder-bestiary-2-items/LV58uBPdmRP5wve2.htm)|Negative Healing|Curación negativa|modificada|
|[LvD2kKe3HyLRK3Gr.htm](pathfinder-bestiary-2-items/LvD2kKe3HyLRK3Gr.htm)|Flare Hood|Capucha de Bengala|modificada|
|[LVDdXXZ03SwDUNZ8.htm](pathfinder-bestiary-2-items/LVDdXXZ03SwDUNZ8.htm)|Jaws|Fauces|modificada|
|[lVmzxYHnjQBBZTN4.htm](pathfinder-bestiary-2-items/lVmzxYHnjQBBZTN4.htm)|Claw|Garra|modificada|
|[lvSGUnxmrOKOF2tT.htm](pathfinder-bestiary-2-items/lvSGUnxmrOKOF2tT.htm)|Grab|Agarrado|modificada|
|[LwAD4kqFgkuoaJ0b.htm](pathfinder-bestiary-2-items/LwAD4kqFgkuoaJ0b.htm)|Change Shape|Change Shape|modificada|
|[LwBTFYgYLRf8gXzt.htm](pathfinder-bestiary-2-items/LwBTFYgYLRf8gXzt.htm)|Spell Reflection|Reflejar conjuros|modificada|
|[lWr6fzaJ1ya4uUVM.htm](pathfinder-bestiary-2-items/lWr6fzaJ1ya4uUVM.htm)|Deflect Arrow|Desviar flecha|modificada|
|[lWwRydUGrcy5WmZf.htm](pathfinder-bestiary-2-items/lWwRydUGrcy5WmZf.htm)|Change Shape|Change Shape|modificada|
|[lXe9Yv9DRWtrP5OH.htm](pathfinder-bestiary-2-items/lXe9Yv9DRWtrP5OH.htm)|Shattering Harmonics|Estallando Armonías|modificada|
|[LXMgaHhqx5CpVHAu.htm](pathfinder-bestiary-2-items/LXMgaHhqx5CpVHAu.htm)|Thorn Volley|Descarga de espinas volea|modificada|
|[lxsWZN09WFvanWVx.htm](pathfinder-bestiary-2-items/lxsWZN09WFvanWVx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[LXXg3Endpf7Vtn3r.htm](pathfinder-bestiary-2-items/LXXg3Endpf7Vtn3r.htm)|Constrict|Restringir|modificada|
|[lxYAhoN0aTLMmh5g.htm](pathfinder-bestiary-2-items/lxYAhoN0aTLMmh5g.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[LxYfnrKWPcZkzYgL.htm](pathfinder-bestiary-2-items/LxYfnrKWPcZkzYgL.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[lY7Ggtouj0iOizqY.htm](pathfinder-bestiary-2-items/lY7Ggtouj0iOizqY.htm)|Chain|Cadena|modificada|
|[Ly9IqnJW6Zy3JQ4y.htm](pathfinder-bestiary-2-items/Ly9IqnJW6Zy3JQ4y.htm)|Pseudopod|Pseudópodo|modificada|
|[LYeMRFsmLZRCk3uE.htm](pathfinder-bestiary-2-items/LYeMRFsmLZRCk3uE.htm)|Sudden Retreat|Retirada repentina|modificada|
|[LyhMcP0ruqAc5kCP.htm](pathfinder-bestiary-2-items/LyhMcP0ruqAc5kCP.htm)|Fist|Puño|modificada|
|[LYl70MYcQUy11TXM.htm](pathfinder-bestiary-2-items/LYl70MYcQUy11TXM.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[lZ2HxJLCCy5TITMC.htm](pathfinder-bestiary-2-items/lZ2HxJLCCy5TITMC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lZ9B89A9j1PjYcdf.htm](pathfinder-bestiary-2-items/lZ9B89A9j1PjYcdf.htm)|Volcanic Purge|Purga volcánica|modificada|
|[lzbAKcAmDlWaRGOF.htm](pathfinder-bestiary-2-items/lzbAKcAmDlWaRGOF.htm)|Telepathy|Telepatía|modificada|
|[LzoVt6l20KHm15wv.htm](pathfinder-bestiary-2-items/LzoVt6l20KHm15wv.htm)|Radiant Beam|Radiant Beam|modificada|
|[LZTLNHZnYh3Eg4aU.htm](pathfinder-bestiary-2-items/LZTLNHZnYh3Eg4aU.htm)|Indispensable Savvy|Indispensable Savvy|modificada|
|[m079lrhvpcahyNqy.htm](pathfinder-bestiary-2-items/m079lrhvpcahyNqy.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[m0MCsCQ2HU7X7Rr2.htm](pathfinder-bestiary-2-items/m0MCsCQ2HU7X7Rr2.htm)|Claw|Garra|modificada|
|[M14sGTx3h9uOKf65.htm](pathfinder-bestiary-2-items/M14sGTx3h9uOKf65.htm)|Darkvision|Visión en la oscuridad|modificada|
|[m1Ku7DrcoF8JnIil.htm](pathfinder-bestiary-2-items/m1Ku7DrcoF8JnIil.htm)|Beak|Beak|modificada|
|[m1YzMXBeebVN4Cp1.htm](pathfinder-bestiary-2-items/m1YzMXBeebVN4Cp1.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[m39B0WvlTUpZ3tLS.htm](pathfinder-bestiary-2-items/m39B0WvlTUpZ3tLS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M3YBIgGHTHFIcGRF.htm](pathfinder-bestiary-2-items/M3YBIgGHTHFIcGRF.htm)|Icicle|Icicle|modificada|
|[M5bYj5m7ymoWlabG.htm](pathfinder-bestiary-2-items/M5bYj5m7ymoWlabG.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[M6lP0K1hMi81OdrJ.htm](pathfinder-bestiary-2-items/M6lP0K1hMi81OdrJ.htm)|Beak|Beak|modificada|
|[M7sZToYvFNrig8TH.htm](pathfinder-bestiary-2-items/M7sZToYvFNrig8TH.htm)|Constant Spells|Constant Spells|modificada|
|[m8k2uRU1UVmPgF03.htm](pathfinder-bestiary-2-items/m8k2uRU1UVmPgF03.htm)|Regeneration 10 (Deactivated by Cold)|Regeneración|modificada|
|[m9kHAZ6q8lhsA9dC.htm](pathfinder-bestiary-2-items/m9kHAZ6q8lhsA9dC.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[ma50pIyMGEpOtjhi.htm](pathfinder-bestiary-2-items/ma50pIyMGEpOtjhi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mBjJ5gUJvowlXzIH.htm](pathfinder-bestiary-2-items/mBjJ5gUJvowlXzIH.htm)|Breath Weapon|Breath Weapon|modificada|
|[mcIvTT80sYixnl9W.htm](pathfinder-bestiary-2-items/mcIvTT80sYixnl9W.htm)|Nauseating Display|Nauseating Display|modificada|
|[mcOn5wJPK3EDRCeU.htm](pathfinder-bestiary-2-items/mcOn5wJPK3EDRCeU.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[mcp20ANc2q921IjQ.htm](pathfinder-bestiary-2-items/mcp20ANc2q921IjQ.htm)|Claw|Garra|modificada|
|[mcsK2cK4pMrDIfZJ.htm](pathfinder-bestiary-2-items/mcsK2cK4pMrDIfZJ.htm)|Curse of Endless Storms|Maldición de las tormentas interminables|modificada|
|[md179yuEkGpC91XD.htm](pathfinder-bestiary-2-items/md179yuEkGpC91XD.htm)|Claw|Garra|modificada|
|[MdeM9TcCMZeHBGEs.htm](pathfinder-bestiary-2-items/MdeM9TcCMZeHBGEs.htm)|Constant Spells|Constant Spells|modificada|
|[mdEZdShzE63nlTQK.htm](pathfinder-bestiary-2-items/mdEZdShzE63nlTQK.htm)|Salt Water Vulnerability|Vulnerabilidad al agua salada|modificada|
|[MDX9dw99EkjeyPM0.htm](pathfinder-bestiary-2-items/MDX9dw99EkjeyPM0.htm)|Grab|Agarrado|modificada|
|[MeT5lDeI05fBDoW3.htm](pathfinder-bestiary-2-items/MeT5lDeI05fBDoW3.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[MEtJibVB3AjSqu2W.htm](pathfinder-bestiary-2-items/MEtJibVB3AjSqu2W.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[meXB4fiPOKy6eWwH.htm](pathfinder-bestiary-2-items/meXB4fiPOKy6eWwH.htm)|Longspear|Longspear|modificada|
|[mey0HoXRCPNAYCqa.htm](pathfinder-bestiary-2-items/mey0HoXRCPNAYCqa.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Mg0QNEtZXzZdfn88.htm](pathfinder-bestiary-2-items/Mg0QNEtZXzZdfn88.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mgA0es28Vb0TAr40.htm](pathfinder-bestiary-2-items/mgA0es28Vb0TAr40.htm)|Menacing Growl|Gruñido amenazador|modificada|
|[mgF7v4Mg6J6e9WVv.htm](pathfinder-bestiary-2-items/mgF7v4Mg6J6e9WVv.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[mGkbLuXtisEvFNNs.htm](pathfinder-bestiary-2-items/mGkbLuXtisEvFNNs.htm)|Fastening Leap|Salto sin carrerilla|modificada|
|[MgWi09fDy6GgzHJM.htm](pathfinder-bestiary-2-items/MgWi09fDy6GgzHJM.htm)|Sinister Bite|Muerdemuerde Siniestro|modificada|
|[MH7wNM2dBDOtmzVO.htm](pathfinder-bestiary-2-items/MH7wNM2dBDOtmzVO.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[mHMefxDyPXjW4mKt.htm](pathfinder-bestiary-2-items/mHMefxDyPXjW4mKt.htm)|Grab|Agarrado|modificada|
|[MhPq4EOlBLppwQeu.htm](pathfinder-bestiary-2-items/MhPq4EOlBLppwQeu.htm)|Cocytan Filth|Cocytan Filth|modificada|
|[MiX1rLReSocMJdta.htm](pathfinder-bestiary-2-items/MiX1rLReSocMJdta.htm)|Cold Lethargy|Letargo Frío|modificada|
|[Mj4CtMHXmN85o3tJ.htm](pathfinder-bestiary-2-items/Mj4CtMHXmN85o3tJ.htm)|Focus Gaze|Centrar mirada|modificada|
|[MJIrkULpmp4soA0F.htm](pathfinder-bestiary-2-items/MJIrkULpmp4soA0F.htm)|Rolling Charge|Rolling Charge|modificada|
|[MJktJ33e2ejK5I1o.htm](pathfinder-bestiary-2-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|Muerdemuerde|modificada|
|[mjWf3inPCmf7NzIh.htm](pathfinder-bestiary-2-items/mjWf3inPCmf7NzIh.htm)|Bite|Muerdemuerde|modificada|
|[mKh47XLxTvXG71TF.htm](pathfinder-bestiary-2-items/mKh47XLxTvXG71TF.htm)|Soul Crush|Soul Crush|modificada|
|[MKjlk8JpKxpWjpPW.htm](pathfinder-bestiary-2-items/MKjlk8JpKxpWjpPW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mKNSE9q3LtnCb49y.htm](pathfinder-bestiary-2-items/mKNSE9q3LtnCb49y.htm)|Scythe|Guadaña|modificada|
|[MkwxsOzEbMfQNQfG.htm](pathfinder-bestiary-2-items/MkwxsOzEbMfQNQfG.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ml69ZMPwkVsbCTC7.htm](pathfinder-bestiary-2-items/ml69ZMPwkVsbCTC7.htm)|Storm of Tentacles|Tormenta de Tentáculos|modificada|
|[mLaikYbAsRTru36r.htm](pathfinder-bestiary-2-items/mLaikYbAsRTru36r.htm)|Arms|Brazos|modificada|
|[MLD9Fi3IkozFjlXZ.htm](pathfinder-bestiary-2-items/MLD9Fi3IkozFjlXZ.htm)|Penetrating Strike|Golpe Penetrante|modificada|
|[mlnT94tqOlTGgqXM.htm](pathfinder-bestiary-2-items/mlnT94tqOlTGgqXM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mLqIKRgLPV3i0SWY.htm](pathfinder-bestiary-2-items/mLqIKRgLPV3i0SWY.htm)|Claw|Garra|modificada|
|[mlQrjH8yonwvq4Td.htm](pathfinder-bestiary-2-items/mlQrjH8yonwvq4Td.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[MLvT2QA997KCHesX.htm](pathfinder-bestiary-2-items/MLvT2QA997KCHesX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mmb4NGB4HueXi8Ty.htm](pathfinder-bestiary-2-items/mmb4NGB4HueXi8Ty.htm)|Bite|Muerdemuerde|modificada|
|[mMCpgVRLUjGayWTB.htm](pathfinder-bestiary-2-items/mMCpgVRLUjGayWTB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mMsy3KDfbrGBdYgB.htm](pathfinder-bestiary-2-items/mMsy3KDfbrGBdYgB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mnNaEVfu4nEmEwem.htm](pathfinder-bestiary-2-items/mnNaEVfu4nEmEwem.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[mNQAWCGl6DU8cQUP.htm](pathfinder-bestiary-2-items/mNQAWCGl6DU8cQUP.htm)|Jagged Jaws|Fauces dentadas|modificada|
|[mObSHLoIirMcASES.htm](pathfinder-bestiary-2-items/mObSHLoIirMcASES.htm)|Jaws|Fauces|modificada|
|[mOfIGDW3g48Ft7w8.htm](pathfinder-bestiary-2-items/mOfIGDW3g48Ft7w8.htm)|Catch Rock|Atrapar roca|modificada|
|[MopioBBWXr3np4t0.htm](pathfinder-bestiary-2-items/MopioBBWXr3np4t0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MPbG99HGshwvN1vm.htm](pathfinder-bestiary-2-items/MPbG99HGshwvN1vm.htm)|Grab|Agarrado|modificada|
|[MPJFQqOrPfQloQLr.htm](pathfinder-bestiary-2-items/MPJFQqOrPfQloQLr.htm)|Death Throes|Estertores de muerte|modificada|
|[mPQXusMKyetxlwjx.htm](pathfinder-bestiary-2-items/mPQXusMKyetxlwjx.htm)|Scent|Scent|modificada|
|[mqgxK9jf9zGXE672.htm](pathfinder-bestiary-2-items/mqgxK9jf9zGXE672.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[MqN8V1YL2EczTFIG.htm](pathfinder-bestiary-2-items/MqN8V1YL2EczTFIG.htm)|Trident|Trident|modificada|
|[mqqrT74LZMyFyuic.htm](pathfinder-bestiary-2-items/mqqrT74LZMyFyuic.htm)|Jaws|Fauces|modificada|
|[mqsn67bFzywwGQ10.htm](pathfinder-bestiary-2-items/mqsn67bFzywwGQ10.htm)|Toxic Bite|Mordisco tóxico|modificada|
|[mQUpigRG6jKJbeLx.htm](pathfinder-bestiary-2-items/mQUpigRG6jKJbeLx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[MRVG5RH41oEJ41N3.htm](pathfinder-bestiary-2-items/MRVG5RH41oEJ41N3.htm)|Fist|Puño|modificada|
|[mS3DC54HONPgp9Pg.htm](pathfinder-bestiary-2-items/mS3DC54HONPgp9Pg.htm)|Sunlight Vulnerability|Vulnerabilidad a la luz del sol|modificada|
|[MSbm9DhCPOKdhf7m.htm](pathfinder-bestiary-2-items/MSbm9DhCPOKdhf7m.htm)|Drink Flesh|Beber carne|modificada|
|[msIyAn6AsHKdGxMu.htm](pathfinder-bestiary-2-items/msIyAn6AsHKdGxMu.htm)|Thundering Bite|Muerdemuerde Tronante|modificada|
|[mSpMpLaDOLKbXWns.htm](pathfinder-bestiary-2-items/mSpMpLaDOLKbXWns.htm)|Piscovenom|Piscovenom|modificada|
|[mTE12ORozb0LZ81C.htm](pathfinder-bestiary-2-items/mTE12ORozb0LZ81C.htm)|Rock|Roca|modificada|
|[Mtkrvum6bboSrXtq.htm](pathfinder-bestiary-2-items/Mtkrvum6bboSrXtq.htm)|Sanguine Mauling|Zarpazo doble sanguíneo|modificada|
|[MtM4AVZZ5pbVijPC.htm](pathfinder-bestiary-2-items/MtM4AVZZ5pbVijPC.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[mTsKSxGS9jWZPCar.htm](pathfinder-bestiary-2-items/mTsKSxGS9jWZPCar.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Mu6RmJmoPp5iM9ld.htm](pathfinder-bestiary-2-items/Mu6RmJmoPp5iM9ld.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[MUg2W7zTfRWFuMZY.htm](pathfinder-bestiary-2-items/MUg2W7zTfRWFuMZY.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mUjeve67uKUXgiEz.htm](pathfinder-bestiary-2-items/mUjeve67uKUXgiEz.htm)|Regeneration 15 (Deactivated by Cold Iron)|Regeneración 15 (Desactivado por Cold Iron)|modificada|
|[mUpNn76jylw99mXr.htm](pathfinder-bestiary-2-items/mUpNn76jylw99mXr.htm)|Frightful Presence|Frightful Presence|modificada|
|[MuzXu4bDzodvaZ7w.htm](pathfinder-bestiary-2-items/MuzXu4bDzodvaZ7w.htm)|Watery Transparency|Transparencia Acuosa|modificada|
|[MvqmGM4ha7aQt1CR.htm](pathfinder-bestiary-2-items/MvqmGM4ha7aQt1CR.htm)|Change Shape|Change Shape|modificada|
|[MVRIjPsJ2e4QYHM9.htm](pathfinder-bestiary-2-items/MVRIjPsJ2e4QYHM9.htm)|Wing Deflection|Desvío con el ala|modificada|
|[MvwGFLtsnf8ZFqhG.htm](pathfinder-bestiary-2-items/MvwGFLtsnf8ZFqhG.htm)|Root|Enraizarse|modificada|
|[MvxbkaVb0xvmWEgv.htm](pathfinder-bestiary-2-items/MvxbkaVb0xvmWEgv.htm)|Constant Spells|Constant Spells|modificada|
|[MWff0Ro0CLotg930.htm](pathfinder-bestiary-2-items/MWff0Ro0CLotg930.htm)|Constrict|Restringir|modificada|
|[mwrBvwI1EzNpPqAp.htm](pathfinder-bestiary-2-items/mwrBvwI1EzNpPqAp.htm)|Fist|Puño|modificada|
|[MwuE7Ua1AaKVpFBo.htm](pathfinder-bestiary-2-items/MwuE7Ua1AaKVpFBo.htm)|Peluda Venom|Peluda Venom|modificada|
|[mXh2p5FT7dkLC4KJ.htm](pathfinder-bestiary-2-items/mXh2p5FT7dkLC4KJ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[mXvv89TdLwyRKCuM.htm](pathfinder-bestiary-2-items/mXvv89TdLwyRKCuM.htm)|Tentacle Mouth|Tentacle Mouth|modificada|
|[mY1TZIEvZh0l0OBD.htm](pathfinder-bestiary-2-items/mY1TZIEvZh0l0OBD.htm)|Throw Rock|Arrojar roca|modificada|
|[MY7iImbmJzLQjtsK.htm](pathfinder-bestiary-2-items/MY7iImbmJzLQjtsK.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[MYTvAqtXzKikO4tg.htm](pathfinder-bestiary-2-items/MYTvAqtXzKikO4tg.htm)|Stinger|Aguijón|modificada|
|[myXt3SwrT2L8VMW4.htm](pathfinder-bestiary-2-items/myXt3SwrT2L8VMW4.htm)|Stygian Guardian|Guardián Estigio|modificada|
|[mZb7eFQ1yel73da5.htm](pathfinder-bestiary-2-items/mZb7eFQ1yel73da5.htm)|Soulsense|Sentido de las almas|modificada|
|[mzcBU4P7MDB5Kav4.htm](pathfinder-bestiary-2-items/mzcBU4P7MDB5Kav4.htm)|Frightful Presence|Frightful Presence|modificada|
|[mzdRGmCygfrC7SXm.htm](pathfinder-bestiary-2-items/mzdRGmCygfrC7SXm.htm)|Tick Fever|Tick Fever|modificada|
|[MZE5l2QagOF7ICxV.htm](pathfinder-bestiary-2-items/MZE5l2QagOF7ICxV.htm)|Scent|Scent|modificada|
|[MzmPJ00qwdaxKPhE.htm](pathfinder-bestiary-2-items/MzmPJ00qwdaxKPhE.htm)|Grab|Agarrado|modificada|
|[MZYIlatBEX5yoG4G.htm](pathfinder-bestiary-2-items/MZYIlatBEX5yoG4G.htm)|Bite|Muerdemuerde|modificada|
|[N1bssKPKGqtiKwNk.htm](pathfinder-bestiary-2-items/N1bssKPKGqtiKwNk.htm)|Magma Swim|Nadar Magma|modificada|
|[N1nMBoBunIANgCSx.htm](pathfinder-bestiary-2-items/N1nMBoBunIANgCSx.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[n4WaiTMOMHY2C2Rc.htm](pathfinder-bestiary-2-items/n4WaiTMOMHY2C2Rc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[N4XGBKf3QWAL89KZ.htm](pathfinder-bestiary-2-items/N4XGBKf3QWAL89KZ.htm)|Change Shape|Change Shape|modificada|
|[n5nOO8opkHkd9ZDW.htm](pathfinder-bestiary-2-items/n5nOO8opkHkd9ZDW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[N5T2rvrlqZmn4pNy.htm](pathfinder-bestiary-2-items/N5T2rvrlqZmn4pNy.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[N5U9pyyTitN5UDgN.htm](pathfinder-bestiary-2-items/N5U9pyyTitN5UDgN.htm)|Jaws|Fauces|modificada|
|[n5uTPxg5zF7PKMZF.htm](pathfinder-bestiary-2-items/n5uTPxg5zF7PKMZF.htm)|Yank|Yank|modificada|
|[n61gabHGuCegRfKD.htm](pathfinder-bestiary-2-items/n61gabHGuCegRfKD.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[n6zbDHFEoD3NyVNv.htm](pathfinder-bestiary-2-items/n6zbDHFEoD3NyVNv.htm)|Hypostome|Hypostome|modificada|
|[N8OPQrQXn7YLXcis.htm](pathfinder-bestiary-2-items/N8OPQrQXn7YLXcis.htm)|Constant Spells|Constant Spells|modificada|
|[n94Dl501br4UFyL1.htm](pathfinder-bestiary-2-items/n94Dl501br4UFyL1.htm)|Throw Rock|Arrojar roca|modificada|
|[N9in8b8yuAVDLoyD.htm](pathfinder-bestiary-2-items/N9in8b8yuAVDLoyD.htm)|Motion Sense|Sentido del movimiento|modificada|
|[N9O6ClFh2arDGR4l.htm](pathfinder-bestiary-2-items/N9O6ClFh2arDGR4l.htm)|Pseudopod|Pseudópodo|modificada|
|[N9qNd1NMgEeyNwcF.htm](pathfinder-bestiary-2-items/N9qNd1NMgEeyNwcF.htm)|Spell Deflection|Spell Deflection|modificada|
|[NA3aEVVR6sNkB8KH.htm](pathfinder-bestiary-2-items/NA3aEVVR6sNkB8KH.htm)|Ice Shard|Esquirla de Hielo|modificada|
|[nBbFutvNlcdf1cgZ.htm](pathfinder-bestiary-2-items/nBbFutvNlcdf1cgZ.htm)|Drown|Drown|modificada|
|[Nbea4sMv3ZuYKikA.htm](pathfinder-bestiary-2-items/Nbea4sMv3ZuYKikA.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[nceXlZk44iBehzvl.htm](pathfinder-bestiary-2-items/nceXlZk44iBehzvl.htm)|Tail|Tail|modificada|
|[NCoI7vXSD3LDfPTM.htm](pathfinder-bestiary-2-items/NCoI7vXSD3LDfPTM.htm)|Rapid Rake|Rastrillo rápido|modificada|
|[NCrNtLcBDV1QPiXn.htm](pathfinder-bestiary-2-items/NCrNtLcBDV1QPiXn.htm)|Rend|Rasgadura|modificada|
|[nd0M4055DPNvjNvf.htm](pathfinder-bestiary-2-items/nd0M4055DPNvjNvf.htm)|Manifest Shawl|Manto Manifiesto|modificada|
|[nD0QzJqbAecWv9ls.htm](pathfinder-bestiary-2-items/nD0QzJqbAecWv9ls.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ND8R6Z5H3eqFpdDz.htm](pathfinder-bestiary-2-items/ND8R6Z5H3eqFpdDz.htm)|Regeneration 40 (deactivated by acid or fire)|Regeneración 40 (desactivado por ácido o fuego)|modificada|
|[NDhBBYbkfa48lqzj.htm](pathfinder-bestiary-2-items/NDhBBYbkfa48lqzj.htm)|Mist Vision|Mist Vision|modificada|
|[NDQPoKh0SP7tR1Ss.htm](pathfinder-bestiary-2-items/NDQPoKh0SP7tR1Ss.htm)|Tentacle Stab|Tentacle Stab|modificada|
|[Ne1X80xmcoz2KP6a.htm](pathfinder-bestiary-2-items/Ne1X80xmcoz2KP6a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NEE6cuy1OUxkBe2Q.htm](pathfinder-bestiary-2-items/NEE6cuy1OUxkBe2Q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[neGWW2ZDslwlxS5Y.htm](pathfinder-bestiary-2-items/neGWW2ZDslwlxS5Y.htm)|Tremorsense|Tremorsense|modificada|
|[newbZPBnDfEIJX5y.htm](pathfinder-bestiary-2-items/newbZPBnDfEIJX5y.htm)|Feeding Tendril|Alimentarse Zarcillo|modificada|
|[NEWDCp6ZApgJEtMx.htm](pathfinder-bestiary-2-items/NEWDCp6ZApgJEtMx.htm)|Dispelling Field|Campo de disipación|modificada|
|[nEZNMODnNpwrnUPC.htm](pathfinder-bestiary-2-items/nEZNMODnNpwrnUPC.htm)|Final Judgment|Juicio Final|modificada|
|[nflrLT1POlEJZu3H.htm](pathfinder-bestiary-2-items/nflrLT1POlEJZu3H.htm)|Claw|Garra|modificada|
|[ngIT9cdBAAOK2wzV.htm](pathfinder-bestiary-2-items/ngIT9cdBAAOK2wzV.htm)|Independent Brains|Cerebros independientes|modificada|
|[nhcCpe4q2AuE5rBl.htm](pathfinder-bestiary-2-items/nhcCpe4q2AuE5rBl.htm)|Fire Jelly Venom|Veneno de gelatina de fuego|modificada|
|[NHDhjNbpULppyjkP.htm](pathfinder-bestiary-2-items/NHDhjNbpULppyjkP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nhn4UFoQny0X750B.htm](pathfinder-bestiary-2-items/nhn4UFoQny0X750B.htm)|Breath Weapon|Breath Weapon|modificada|
|[nHTCIVLK6QSJqHd3.htm](pathfinder-bestiary-2-items/nHTCIVLK6QSJqHd3.htm)|Poisonous Pustules|Pústulas Venenosas|modificada|
|[nIDe1OaPgWDPyBej.htm](pathfinder-bestiary-2-items/nIDe1OaPgWDPyBej.htm)|Text Immersion|Inmersión en el texto|modificada|
|[nj1sfrFbZHDVVn3y.htm](pathfinder-bestiary-2-items/nj1sfrFbZHDVVn3y.htm)|Darkvision|Visión en la oscuridad|modificada|
|[njNLaG6BmpA7LInp.htm](pathfinder-bestiary-2-items/njNLaG6BmpA7LInp.htm)|Jaws|Fauces|modificada|
|[NJS22AHrPTckdEPS.htm](pathfinder-bestiary-2-items/NJS22AHrPTckdEPS.htm)|Sense Murderer|Sense Murderer|modificada|
|[nKHp4Fk7EvpSS3N9.htm](pathfinder-bestiary-2-items/nKHp4Fk7EvpSS3N9.htm)|Jaws|Fauces|modificada|
|[nKL6rZMmn2WdRk3g.htm](pathfinder-bestiary-2-items/nKL6rZMmn2WdRk3g.htm)|Cling|Cling|modificada|
|[nKM6FVNsXbBwEPNq.htm](pathfinder-bestiary-2-items/nKM6FVNsXbBwEPNq.htm)|Gnaw|Gnaw|modificada|
|[nKqb3ksLlia2sFBZ.htm](pathfinder-bestiary-2-items/nKqb3ksLlia2sFBZ.htm)|Infused Items|Equipos infundidos|modificada|
|[Nl0sqXtr54YzMawN.htm](pathfinder-bestiary-2-items/Nl0sqXtr54YzMawN.htm)|Consume Death|Consume Muerte|modificada|
|[nLocPM8hKYMstnGr.htm](pathfinder-bestiary-2-items/nLocPM8hKYMstnGr.htm)|Change Shape|Change Shape|modificada|
|[nM4ThlQUyejxc6bH.htm](pathfinder-bestiary-2-items/nM4ThlQUyejxc6bH.htm)|Stone Step|Paso de Piedra|modificada|
|[nMVv9cAH945ESnI9.htm](pathfinder-bestiary-2-items/nMVv9cAH945ESnI9.htm)|Claw|Garra|modificada|
|[nneVdsY6tUBGsScQ.htm](pathfinder-bestiary-2-items/nneVdsY6tUBGsScQ.htm)|Head Regrowth|Crecer cabezas|modificada|
|[nnl33KWF4lCqql97.htm](pathfinder-bestiary-2-items/nnl33KWF4lCqql97.htm)|Easy to Influence|Fácil de influenciar|modificada|
|[NnMlHBLwjpkBjx4v.htm](pathfinder-bestiary-2-items/NnMlHBLwjpkBjx4v.htm)|Claw|Garra|modificada|
|[NNPWYZUGqcGbUeZI.htm](pathfinder-bestiary-2-items/NNPWYZUGqcGbUeZI.htm)|Summon Aquatic Ally|Invocar Aliado Acuático|modificada|
|[NOvlBahz080PaM4R.htm](pathfinder-bestiary-2-items/NOvlBahz080PaM4R.htm)|Tail|Tail|modificada|
|[NP8RWxt3VPo7LZwb.htm](pathfinder-bestiary-2-items/NP8RWxt3VPo7LZwb.htm)|Beak|Beak|modificada|
|[npIgzch2HZkhFifI.htm](pathfinder-bestiary-2-items/npIgzch2HZkhFifI.htm)|Claw|Garra|modificada|
|[nplfqbEUtcOBlV0b.htm](pathfinder-bestiary-2-items/nplfqbEUtcOBlV0b.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[nPZlZs1rfnERketf.htm](pathfinder-bestiary-2-items/nPZlZs1rfnERketf.htm)|Undying Vendetta|Undying Vendetta|modificada|
|[NqdW9zuvmBwHZScr.htm](pathfinder-bestiary-2-items/NqdW9zuvmBwHZScr.htm)|Archon's Door|Puerta del arconte|modificada|
|[nQJ4bCNSLy7MFyXP.htm](pathfinder-bestiary-2-items/nQJ4bCNSLy7MFyXP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[nQuUyJS1AvKyFrA0.htm](pathfinder-bestiary-2-items/nQuUyJS1AvKyFrA0.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[nQV7pNdpGD2GDrgl.htm](pathfinder-bestiary-2-items/nQV7pNdpGD2GDrgl.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[NrafRXoflRlcVm6H.htm](pathfinder-bestiary-2-items/NrafRXoflRlcVm6H.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nrHgXy7XizFWsPFe.htm](pathfinder-bestiary-2-items/nrHgXy7XizFWsPFe.htm)|Dweomer Leap|Dweomer Salto sin carrerilla|modificada|
|[nrXh0T6f3LTaRSGo.htm](pathfinder-bestiary-2-items/nrXh0T6f3LTaRSGo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[NSdZME2VBfcmBDQA.htm](pathfinder-bestiary-2-items/NSdZME2VBfcmBDQA.htm)|Claw|Garra|modificada|
|[nSgOSBLM92y28WsS.htm](pathfinder-bestiary-2-items/nSgOSBLM92y28WsS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NshCFgqhon7NfWbq.htm](pathfinder-bestiary-2-items/NshCFgqhon7NfWbq.htm)|Claw|Garra|modificada|
|[NSLdgH5UqohAav11.htm](pathfinder-bestiary-2-items/NSLdgH5UqohAav11.htm)|Tiger Empathy|Empatía con los tigres|modificada|
|[NsRn4K2Tli8P3WKm.htm](pathfinder-bestiary-2-items/NsRn4K2Tli8P3WKm.htm)|Ignore Pain|Ignore Pain|modificada|
|[nszzMLENLPQ4zPoa.htm](pathfinder-bestiary-2-items/nszzMLENLPQ4zPoa.htm)|Tenacious Flames|Tenaz Flamígera|modificada|
|[NTWIBx9lvUwV7F1W.htm](pathfinder-bestiary-2-items/NTWIBx9lvUwV7F1W.htm)|Mundane Appearance|Mundane Appearance|modificada|
|[numgrwQUdlF3fy51.htm](pathfinder-bestiary-2-items/numgrwQUdlF3fy51.htm)|Tail|Tail|modificada|
|[nuNIIa81vZ9PT2RL.htm](pathfinder-bestiary-2-items/nuNIIa81vZ9PT2RL.htm)|Dazzling Burst|Ráfaga Deslumbrante|modificada|
|[NUrrmWCh8zDwdKFw.htm](pathfinder-bestiary-2-items/NUrrmWCh8zDwdKFw.htm)|Grab|Agarrado|modificada|
|[nuV1GkHwu5pqVXnI.htm](pathfinder-bestiary-2-items/nuV1GkHwu5pqVXnI.htm)|Barb|Barb|modificada|
|[nvEMR0N4HLFwMTMQ.htm](pathfinder-bestiary-2-items/nvEMR0N4HLFwMTMQ.htm)|Tremorsense|Tremorsense|modificada|
|[nVMaCED96f0puvS1.htm](pathfinder-bestiary-2-items/nVMaCED96f0puvS1.htm)|Apocalypse Breath|Apocalypse Breath|modificada|
|[NWD8KA0NNOrEyrvq.htm](pathfinder-bestiary-2-items/NWD8KA0NNOrEyrvq.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[nxagDqd1qxC8w8dl.htm](pathfinder-bestiary-2-items/nxagDqd1qxC8w8dl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[nxblqBzJUsncnJ9t.htm](pathfinder-bestiary-2-items/nxblqBzJUsncnJ9t.htm)|Noxious Burst|Noxious Burst|modificada|
|[NXk6QjCfO8eW6Txp.htm](pathfinder-bestiary-2-items/NXk6QjCfO8eW6Txp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[nyh1lXyTGoUctOzo.htm](pathfinder-bestiary-2-items/nyh1lXyTGoUctOzo.htm)|Double Chomp|Doble Chomp|modificada|
|[nYQgnHNVs60rcvUn.htm](pathfinder-bestiary-2-items/nYQgnHNVs60rcvUn.htm)|Mechanical Vulnerability|Vulnerabilidad Mecánica|modificada|
|[NYqhdSbPLiwcvxjs.htm](pathfinder-bestiary-2-items/NYqhdSbPLiwcvxjs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Nz33adJeSgdyaAw6.htm](pathfinder-bestiary-2-items/Nz33adJeSgdyaAw6.htm)|Hoof|Hoof|modificada|
|[Nz9xljPpg09kcxBl.htm](pathfinder-bestiary-2-items/Nz9xljPpg09kcxBl.htm)|Bite|Muerdemuerde|modificada|
|[NZhUsB0mdsYLr6UX.htm](pathfinder-bestiary-2-items/NZhUsB0mdsYLr6UX.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[NzMuO5EzlGKw4nGq.htm](pathfinder-bestiary-2-items/NzMuO5EzlGKw4nGq.htm)|Mouth|Boca|modificada|
|[NZRABrBe5xEzlWlP.htm](pathfinder-bestiary-2-items/NZRABrBe5xEzlWlP.htm)|Arrow of Mortality|Arrow of Mortality|modificada|
|[O0uewbc9915uqqlD.htm](pathfinder-bestiary-2-items/O0uewbc9915uqqlD.htm)|Tail|Tail|modificada|
|[O0YSsVnEBCg6oJnT.htm](pathfinder-bestiary-2-items/O0YSsVnEBCg6oJnT.htm)|Horns|Cuernos|modificada|
|[O10CpdkrcpxqCHHH.htm](pathfinder-bestiary-2-items/O10CpdkrcpxqCHHH.htm)|Circle of Protection|Círculo de protección.|modificada|
|[o19lnL3xo6DpdVgZ.htm](pathfinder-bestiary-2-items/o19lnL3xo6DpdVgZ.htm)|Twisting Tail|Enredar con la cola|modificada|
|[o38MzuC03KOCw8wS.htm](pathfinder-bestiary-2-items/o38MzuC03KOCw8wS.htm)|Jaws That Bite|Mandíbulas que muerden|modificada|
|[o3nbXHISVkXC32B1.htm](pathfinder-bestiary-2-items/o3nbXHISVkXC32B1.htm)|Grab|Agarrado|modificada|
|[o40ddeHAtx8cJ0uY.htm](pathfinder-bestiary-2-items/o40ddeHAtx8cJ0uY.htm)|Mind Lash|Mind Lash|modificada|
|[O4cx0ZHztve08mHY.htm](pathfinder-bestiary-2-items/O4cx0ZHztve08mHY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[O4epuzV9B2OOYJXy.htm](pathfinder-bestiary-2-items/O4epuzV9B2OOYJXy.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[o4iEKaEBIwAr18XF.htm](pathfinder-bestiary-2-items/o4iEKaEBIwAr18XF.htm)|+4 Status to Will Saves vs. Mental|+4 de situación a las salvaciones de Voluntad contra Mental.|modificada|
|[o4uNCohL2RA2ShrG.htm](pathfinder-bestiary-2-items/o4uNCohL2RA2ShrG.htm)|Negative Healing|Curación negativa|modificada|
|[O5P2324cJfYLLpmm.htm](pathfinder-bestiary-2-items/O5P2324cJfYLLpmm.htm)|Glass Armor|Glass Armor|modificada|
|[o6CQEcEj0w3RIaZB.htm](pathfinder-bestiary-2-items/o6CQEcEj0w3RIaZB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[o6dcBgCrq9s6bVuO.htm](pathfinder-bestiary-2-items/o6dcBgCrq9s6bVuO.htm)|Swallow Whole|Engullir Todo|modificada|
|[oa0MYylUzdTPYei2.htm](pathfinder-bestiary-2-items/oa0MYylUzdTPYei2.htm)|Claw|Garra|modificada|
|[OAaAcan3IQyoJJ4X.htm](pathfinder-bestiary-2-items/OAaAcan3IQyoJJ4X.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[oabwo3FEBY2CjPRM.htm](pathfinder-bestiary-2-items/oabwo3FEBY2CjPRM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OACmutVuYdHoFbvw.htm](pathfinder-bestiary-2-items/OACmutVuYdHoFbvw.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[oApMO7ZwPJkW1mn1.htm](pathfinder-bestiary-2-items/oApMO7ZwPJkW1mn1.htm)|Improved Push|Empuje mejorado|modificada|
|[oAxtReNr1iwTRmKX.htm](pathfinder-bestiary-2-items/oAxtReNr1iwTRmKX.htm)|Negate Levitation|Negar levitación|modificada|
|[OB8l4HRZ5Zc4G42p.htm](pathfinder-bestiary-2-items/OB8l4HRZ5Zc4G42p.htm)|Scent|Scent|modificada|
|[OBbctD8lNz3bU0yf.htm](pathfinder-bestiary-2-items/OBbctD8lNz3bU0yf.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[obCe64sZedFgSWGQ.htm](pathfinder-bestiary-2-items/obCe64sZedFgSWGQ.htm)|Luring Cry|Grito atrayente|modificada|
|[oBMQTX6kkJaK35ed.htm](pathfinder-bestiary-2-items/oBMQTX6kkJaK35ed.htm)|Claw|Garra|modificada|
|[OBn7syfr2t30QJaa.htm](pathfinder-bestiary-2-items/OBn7syfr2t30QJaa.htm)|Bite|Muerdemuerde|modificada|
|[ObPZcbNErqRJbu6p.htm](pathfinder-bestiary-2-items/ObPZcbNErqRJbu6p.htm)|Holy Longbow|Arco Largo Sagrado|modificada|
|[oBq0zVqb67SoY8qy.htm](pathfinder-bestiary-2-items/oBq0zVqb67SoY8qy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oBV1Ybwqj4dpXeff.htm](pathfinder-bestiary-2-items/oBV1Ybwqj4dpXeff.htm)|Spirit Touch|Spirit Touch|modificada|
|[OBXLT5RlRI6AauZ1.htm](pathfinder-bestiary-2-items/OBXLT5RlRI6AauZ1.htm)|Breath Weapon|Breath Weapon|modificada|
|[oc2KzcT2MuHsRZ1k.htm](pathfinder-bestiary-2-items/oc2KzcT2MuHsRZ1k.htm)|Frozen Strike|Golpe congelado|modificada|
|[oC4rXv11i7sVgw4k.htm](pathfinder-bestiary-2-items/oC4rXv11i7sVgw4k.htm)|Radula|Radula|modificada|
|[OC4UGWpbAZyLbFb6.htm](pathfinder-bestiary-2-items/OC4UGWpbAZyLbFb6.htm)|Breath Weapon|Breath Weapon|modificada|
|[oC5vZAEFIISSmofG.htm](pathfinder-bestiary-2-items/oC5vZAEFIISSmofG.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Od1YX31hXRHCzHS3.htm](pathfinder-bestiary-2-items/Od1YX31hXRHCzHS3.htm)|Rip and Tear|Rip and Tear|modificada|
|[odZw9mK4mxnlp6qz.htm](pathfinder-bestiary-2-items/odZw9mK4mxnlp6qz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oE4IMiBnWcfVyg7X.htm](pathfinder-bestiary-2-items/oE4IMiBnWcfVyg7X.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Oe7KNrqiGTrNfkV9.htm](pathfinder-bestiary-2-items/Oe7KNrqiGTrNfkV9.htm)|Jaws|Fauces|modificada|
|[OeHuAJLeUtdJFHfo.htm](pathfinder-bestiary-2-items/OeHuAJLeUtdJFHfo.htm)|Rhinoceros Charge|Carga Rinoceronte|modificada|
|[Oem6sNhw2eizgyTW.htm](pathfinder-bestiary-2-items/Oem6sNhw2eizgyTW.htm)|Constant Spells|Constant Spells|modificada|
|[Oevi96zftiaBjAOH.htm](pathfinder-bestiary-2-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[OEYNgSplNvqswLPK.htm](pathfinder-bestiary-2-items/OEYNgSplNvqswLPK.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[Of0URrwSMG72XvUL.htm](pathfinder-bestiary-2-items/Of0URrwSMG72XvUL.htm)|Lifesense|Lifesense|modificada|
|[ofz49oiBkargWzh6.htm](pathfinder-bestiary-2-items/ofz49oiBkargWzh6.htm)|Jaws|Fauces|modificada|
|[oG07EBWnvzNPv4hk.htm](pathfinder-bestiary-2-items/oG07EBWnvzNPv4hk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OhlQui33tv2Wq8Ur.htm](pathfinder-bestiary-2-items/OhlQui33tv2Wq8Ur.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[oifiHjSHVsJGyQiE.htm](pathfinder-bestiary-2-items/oifiHjSHVsJGyQiE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oIucCyy3YvC5FoCA.htm](pathfinder-bestiary-2-items/oIucCyy3YvC5FoCA.htm)|Stone Stride|Zancada de Piedra|modificada|
|[OJA9CljFVtmCuzlZ.htm](pathfinder-bestiary-2-items/OJA9CljFVtmCuzlZ.htm)|Prudent Asterism|Asterismo Prudente|modificada|
|[OJg4VMV8aMk40kBm.htm](pathfinder-bestiary-2-items/OJg4VMV8aMk40kBm.htm)|Shadow Blend|Mezcla de Sombra|modificada|
|[oJLAT2NTlht6zFtA.htm](pathfinder-bestiary-2-items/oJLAT2NTlht6zFtA.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ojlZFCKNrXd0MjTG.htm](pathfinder-bestiary-2-items/ojlZFCKNrXd0MjTG.htm)|Blood Siphon|Blood Siphon|modificada|
|[oKEblFTjM5UV6Lwa.htm](pathfinder-bestiary-2-items/oKEblFTjM5UV6Lwa.htm)|Steep Weapon|Arma empinada|modificada|
|[OKH1R1FoBSEm3oKP.htm](pathfinder-bestiary-2-items/OKH1R1FoBSEm3oKP.htm)|Enraged Growth|Crecimiento Enfurecido|modificada|
|[OKh9LnvuAtiEOz1f.htm](pathfinder-bestiary-2-items/OKh9LnvuAtiEOz1f.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[okXnipjpQrYq6vGT.htm](pathfinder-bestiary-2-items/okXnipjpQrYq6vGT.htm)|Constrict|Restringir|modificada|
|[OL5iKgpKK0q9NrD1.htm](pathfinder-bestiary-2-items/OL5iKgpKK0q9NrD1.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[OlCiQCdanvjrjD3s.htm](pathfinder-bestiary-2-items/OlCiQCdanvjrjD3s.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[OlnWMyIX3qedGUTu.htm](pathfinder-bestiary-2-items/OlnWMyIX3qedGUTu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OLwbn1vkUwxatzY5.htm](pathfinder-bestiary-2-items/OLwbn1vkUwxatzY5.htm)|Lifesense|Lifesense|modificada|
|[OlwLivQfLQIh0ut7.htm](pathfinder-bestiary-2-items/OlwLivQfLQIh0ut7.htm)|Magic Sense|Sentido mágico|modificada|
|[oLxdFwYmbkBdHCgB.htm](pathfinder-bestiary-2-items/oLxdFwYmbkBdHCgB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ome4gBGaz20PH3OU.htm](pathfinder-bestiary-2-items/ome4gBGaz20PH3OU.htm)|Blood Draining Bites|Muerdemuerde Drenando sangre|modificada|
|[oMj0SiABPgORm6t3.htm](pathfinder-bestiary-2-items/oMj0SiABPgORm6t3.htm)|Breath Weapon|Breath Weapon|modificada|
|[OMTKXoF2bm81nkS5.htm](pathfinder-bestiary-2-items/OMTKXoF2bm81nkS5.htm)|Longsword|Longsword|modificada|
|[onjhPnLTGiThJjTI.htm](pathfinder-bestiary-2-items/onjhPnLTGiThJjTI.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[oNmHvve4Vcwj6IiO.htm](pathfinder-bestiary-2-items/oNmHvve4Vcwj6IiO.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[oNpExBZGMP135Gh8.htm](pathfinder-bestiary-2-items/oNpExBZGMP135Gh8.htm)|Capsize|Volcar|modificada|
|[ope3wpbKSAJOWCJD.htm](pathfinder-bestiary-2-items/ope3wpbKSAJOWCJD.htm)|Bony Hand|Mano huesuda|modificada|
|[opNwYrwABCLTV4vZ.htm](pathfinder-bestiary-2-items/opNwYrwABCLTV4vZ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[OpUEqqVcx7trG4mu.htm](pathfinder-bestiary-2-items/OpUEqqVcx7trG4mu.htm)|Kukri|Kukri|modificada|
|[oPWhso0y4ezQpGaJ.htm](pathfinder-bestiary-2-items/oPWhso0y4ezQpGaJ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[oQcnbbUjvuJ6yIaK.htm](pathfinder-bestiary-2-items/oQcnbbUjvuJ6yIaK.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[oqcxPwDIMJ2bu4Ul.htm](pathfinder-bestiary-2-items/oqcxPwDIMJ2bu4Ul.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[OqJWwPCeEzKsYH8X.htm](pathfinder-bestiary-2-items/OqJWwPCeEzKsYH8X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OQMwQiE7vGLPMasL.htm](pathfinder-bestiary-2-items/OQMwQiE7vGLPMasL.htm)|Blood Drain|Drenar sangre|modificada|
|[oQNL2zFwRBLQE7vn.htm](pathfinder-bestiary-2-items/oQNL2zFwRBLQE7vn.htm)|Horns|Cuernos|modificada|
|[oqYW7IkKIsygycAY.htm](pathfinder-bestiary-2-items/oqYW7IkKIsygycAY.htm)|Reflexive Grab|Agarrado reflexivo|modificada|
|[OR27Y1Vkv8bMFjHO.htm](pathfinder-bestiary-2-items/OR27Y1Vkv8bMFjHO.htm)|Dagger|Daga|modificada|
|[orQ7bvmsL70g6qk9.htm](pathfinder-bestiary-2-items/orQ7bvmsL70g6qk9.htm)|Jaws|Fauces|modificada|
|[osi4ctAQErnOa2BZ.htm](pathfinder-bestiary-2-items/osi4ctAQErnOa2BZ.htm)|Heart Ripper|Arrancacorazones|modificada|
|[OSiq1CXChzTT7udW.htm](pathfinder-bestiary-2-items/OSiq1CXChzTT7udW.htm)|Spectral Corruption|Corrupción espectral|modificada|
|[osogPaHrRMKqt4Fw.htm](pathfinder-bestiary-2-items/osogPaHrRMKqt4Fw.htm)|Trample|Trample|modificada|
|[ot1RNObQMdnpWSrb.htm](pathfinder-bestiary-2-items/ot1RNObQMdnpWSrb.htm)|Catch Rock|Atrapar roca|modificada|
|[Ot3tRiGqqRheUAKs.htm](pathfinder-bestiary-2-items/Ot3tRiGqqRheUAKs.htm)|Crystallize Flesh|Cristalizar la carne|modificada|
|[otBr1P6MKia09fCt.htm](pathfinder-bestiary-2-items/otBr1P6MKia09fCt.htm)|Catch Rock|Atrapar roca|modificada|
|[OUEVYrICDhOs5NML.htm](pathfinder-bestiary-2-items/OUEVYrICDhOs5NML.htm)|Savage Jaws|Fauces de Salvajismo|modificada|
|[oUmLwF6HrGb0Y9yj.htm](pathfinder-bestiary-2-items/oUmLwF6HrGb0Y9yj.htm)|Regeneration 20 (deactivated by evil)|Regeneración 20 (desactivado por maligno).|modificada|
|[Ox2rTRtNAQouxryx.htm](pathfinder-bestiary-2-items/Ox2rTRtNAQouxryx.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[OxEJ1NOQewlXZjZh.htm](pathfinder-bestiary-2-items/OxEJ1NOQewlXZjZh.htm)|Claw|Garra|modificada|
|[OXfHyEExXIhlnUp7.htm](pathfinder-bestiary-2-items/OXfHyEExXIhlnUp7.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OxPw5pr9S1edjy8S.htm](pathfinder-bestiary-2-items/OxPw5pr9S1edjy8S.htm)|Lunar Naga Venom|Veneno Naga Lunar|modificada|
|[OxZGoHvhVubVXMm4.htm](pathfinder-bestiary-2-items/OxZGoHvhVubVXMm4.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[OYFgzf5Q5j6UeJhf.htm](pathfinder-bestiary-2-items/OYFgzf5Q5j6UeJhf.htm)|Jaws|Fauces|modificada|
|[OygWVWtANgUBztcL.htm](pathfinder-bestiary-2-items/OygWVWtANgUBztcL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[OymgeOQy6WJ5jvF1.htm](pathfinder-bestiary-2-items/OymgeOQy6WJ5jvF1.htm)|Scent|Scent|modificada|
|[OYTKATaPDdautdNt.htm](pathfinder-bestiary-2-items/OYTKATaPDdautdNt.htm)|Wing|Ala|modificada|
|[oYtncLyCKuBAQThk.htm](pathfinder-bestiary-2-items/oYtncLyCKuBAQThk.htm)|Clawing Fear|Zarpazo de miedo|modificada|
|[Oz0PpydQ8KCagat3.htm](pathfinder-bestiary-2-items/Oz0PpydQ8KCagat3.htm)|Tail|Tail|modificada|
|[OZmNooOK98jFnTUU.htm](pathfinder-bestiary-2-items/OZmNooOK98jFnTUU.htm)|Barbed Tentacles|Tentáculos de púas|modificada|
|[OzuFfQ0SSo89GdPZ.htm](pathfinder-bestiary-2-items/OzuFfQ0SSo89GdPZ.htm)|Hoof|Hoof|modificada|
|[P0EyFCnT4og9udgU.htm](pathfinder-bestiary-2-items/P0EyFCnT4og9udgU.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[P0VFneuZMPCfZ35e.htm](pathfinder-bestiary-2-items/P0VFneuZMPCfZ35e.htm)|Constant Spells|Constant Spells|modificada|
|[p18BSmeidbDDfJ2D.htm](pathfinder-bestiary-2-items/p18BSmeidbDDfJ2D.htm)|Kind Word|Palabra amable|modificada|
|[p1rxtsKx33vjytn0.htm](pathfinder-bestiary-2-items/p1rxtsKx33vjytn0.htm)|Thulgant Venom|Veneno Thulgant|modificada|
|[p23AKjDuFSYrPgZz.htm](pathfinder-bestiary-2-items/p23AKjDuFSYrPgZz.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[P48WRIeVuuAB8JWK.htm](pathfinder-bestiary-2-items/P48WRIeVuuAB8JWK.htm)|Death Flood|Death Flood|modificada|
|[P4BV5OJUyj3rfr1E.htm](pathfinder-bestiary-2-items/P4BV5OJUyj3rfr1E.htm)|Telepathy|Telepatía|modificada|
|[p5OQLHvMklzKmrbR.htm](pathfinder-bestiary-2-items/p5OQLHvMklzKmrbR.htm)|Scimitar|Cimitarra|modificada|
|[P5TjcVC1dN5jLmFb.htm](pathfinder-bestiary-2-items/P5TjcVC1dN5jLmFb.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[P5tslJuQziddpFpV.htm](pathfinder-bestiary-2-items/P5tslJuQziddpFpV.htm)|Tooth Grind|Triturar con los dientes|modificada|
|[P6oJVM0MDCy82QgU.htm](pathfinder-bestiary-2-items/P6oJVM0MDCy82QgU.htm)|Shadow Blending|Mezcla de Sombra|modificada|
|[p8Evyqv8pdGxIXIQ.htm](pathfinder-bestiary-2-items/p8Evyqv8pdGxIXIQ.htm)|Rend|Rasgadura|modificada|
|[P8P9UmFHbSmMnTnd.htm](pathfinder-bestiary-2-items/P8P9UmFHbSmMnTnd.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[P92CxO5qbGv9LPbu.htm](pathfinder-bestiary-2-items/P92CxO5qbGv9LPbu.htm)|Snip Thread|Snip Thread|modificada|
|[p9eUNDT8mPVTSS1D.htm](pathfinder-bestiary-2-items/p9eUNDT8mPVTSS1D.htm)|Dimensional Wormhole|Agujero de gusano dimensional|modificada|
|[p9ZGDxOXXXlq8PuH.htm](pathfinder-bestiary-2-items/p9ZGDxOXXXlq8PuH.htm)|Jaws|Fauces|modificada|
|[Pb3h2ToW8cyiBXN7.htm](pathfinder-bestiary-2-items/Pb3h2ToW8cyiBXN7.htm)|Tentacle|Tentáculo|modificada|
|[PBcYVDE3HZByWRI7.htm](pathfinder-bestiary-2-items/PBcYVDE3HZByWRI7.htm)|Black Scorpion Venom|Veneno Escorpión Negro|modificada|
|[PbnYiMI8IGyrAIej.htm](pathfinder-bestiary-2-items/PbnYiMI8IGyrAIej.htm)|Chain|Cadena|modificada|
|[PbQYJStaqe1nhRUn.htm](pathfinder-bestiary-2-items/PbQYJStaqe1nhRUn.htm)|Frightful Presence|Frightful Presence|modificada|
|[Pd7VzQqoVyctHCLg.htm](pathfinder-bestiary-2-items/Pd7VzQqoVyctHCLg.htm)|Magma Swim|Nadar Magma|modificada|
|[PDKjx7hYxc0mQaVe.htm](pathfinder-bestiary-2-items/PDKjx7hYxc0mQaVe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[PDPYyeFFpfksUoXK.htm](pathfinder-bestiary-2-items/PDPYyeFFpfksUoXK.htm)|Shadowcloak|Capa sombría|modificada|
|[PDq2G4IiMdipukDg.htm](pathfinder-bestiary-2-items/PDq2G4IiMdipukDg.htm)|Beak|Beak|modificada|
|[pE48e3bWFRDs19oA.htm](pathfinder-bestiary-2-items/pE48e3bWFRDs19oA.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[pepIdjNqJtUTl9Wn.htm](pathfinder-bestiary-2-items/pepIdjNqJtUTl9Wn.htm)|Endless Nightmare|Pesadilla sin fin|modificada|
|[PF6tfwJNh5sVOavs.htm](pathfinder-bestiary-2-items/PF6tfwJNh5sVOavs.htm)|Thrashing Retreat|Retirada a golpes|modificada|
|[PFAC6dSt4RN7aRDz.htm](pathfinder-bestiary-2-items/PFAC6dSt4RN7aRDz.htm)|Stinger|Aguijón|modificada|
|[pfYqWHlnkQUBpJIo.htm](pathfinder-bestiary-2-items/pfYqWHlnkQUBpJIo.htm)|Electric Torpor|Torpor Eléctrico|modificada|
|[pfzWyL0gCgqD1xRz.htm](pathfinder-bestiary-2-items/pfzWyL0gCgqD1xRz.htm)|Telepathy|Telepatía|modificada|
|[pGaSnoNG83zKVZkX.htm](pathfinder-bestiary-2-items/pGaSnoNG83zKVZkX.htm)|Deflecting Cloud|Nube deflectora|modificada|
|[PGeuRIZhMLp88mYc.htm](pathfinder-bestiary-2-items/PGeuRIZhMLp88mYc.htm)|Fist|Puño|modificada|
|[PgZtqgPtRR9DRaGz.htm](pathfinder-bestiary-2-items/PgZtqgPtRR9DRaGz.htm)|Vine|Vid|modificada|
|[pHckdhGspQesS1nl.htm](pathfinder-bestiary-2-items/pHckdhGspQesS1nl.htm)|Ravage|Arruinar|modificada|
|[pheseepJ6Q1YF17G.htm](pathfinder-bestiary-2-items/pheseepJ6Q1YF17G.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pJ6eBDUVUixzX9UL.htm](pathfinder-bestiary-2-items/pJ6eBDUVUixzX9UL.htm)|Animate Chains|Animar Cadenas|modificada|
|[PjBdBCA31o7FcQvI.htm](pathfinder-bestiary-2-items/PjBdBCA31o7FcQvI.htm)|Telepathy|Telepatía|modificada|
|[pke6bvuCCSuidMVb.htm](pathfinder-bestiary-2-items/pke6bvuCCSuidMVb.htm)|Poisonous Touch|Toque venenoso|modificada|
|[pKF3w8Hj8QFO6ZMv.htm](pathfinder-bestiary-2-items/pKF3w8Hj8QFO6ZMv.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[PKHHueFUrRyFJPxL.htm](pathfinder-bestiary-2-items/PKHHueFUrRyFJPxL.htm)|Rend|Rasgadura|modificada|
|[pkqZFkDwe6SFvUXX.htm](pathfinder-bestiary-2-items/pkqZFkDwe6SFvUXX.htm)|Serpentfolk Venom|Serpentfolk Venom|modificada|
|[pL08UYEZoKBSYUzv.htm](pathfinder-bestiary-2-items/pL08UYEZoKBSYUzv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pLNcYnrUhpwxDo9b.htm](pathfinder-bestiary-2-items/pLNcYnrUhpwxDo9b.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[PLo6RE4lIKmw9DQL.htm](pathfinder-bestiary-2-items/PLo6RE4lIKmw9DQL.htm)|Blurred Form|Forma borrosa|modificada|
|[PM9U1Ca1DQ2ZA7ZJ.htm](pathfinder-bestiary-2-items/PM9U1Ca1DQ2ZA7ZJ.htm)|Negative Healing|Curación negativa|modificada|
|[PMbE9CQyp5uM7Whu.htm](pathfinder-bestiary-2-items/PMbE9CQyp5uM7Whu.htm)|Improved Grab|Agarrado mejorado|modificada|
|[pmfr4MB1abUN5RbW.htm](pathfinder-bestiary-2-items/pmfr4MB1abUN5RbW.htm)|Feed|Alimentarse|modificada|
|[pMGUO0z7GhIrgGlF.htm](pathfinder-bestiary-2-items/pMGUO0z7GhIrgGlF.htm)|Reactive Slime|Baba Reactiva|modificada|
|[pmHXQt1EgMIVwD1i.htm](pathfinder-bestiary-2-items/pmHXQt1EgMIVwD1i.htm)|Exquisite Pain|Dolor Exquisito|modificada|
|[PMVviZt2FjDHt7QV.htm](pathfinder-bestiary-2-items/PMVviZt2FjDHt7QV.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[pntbmMiiU6IArfJX.htm](pathfinder-bestiary-2-items/pntbmMiiU6IArfJX.htm)|Grab|Agarrado|modificada|
|[pnvcZVssnfksWBzv.htm](pathfinder-bestiary-2-items/pnvcZVssnfksWBzv.htm)|Quill Thrust|Acometida de pluma|modificada|
|[POIZ15lxh5FqbzaK.htm](pathfinder-bestiary-2-items/POIZ15lxh5FqbzaK.htm)|Fist|Puño|modificada|
|[POLjXIhCSeQBrvcV.htm](pathfinder-bestiary-2-items/POLjXIhCSeQBrvcV.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[POVANlwhTG8kc8av.htm](pathfinder-bestiary-2-items/POVANlwhTG8kc8av.htm)|Claw|Garra|modificada|
|[pP0EwdXTDTjgDpZA.htm](pathfinder-bestiary-2-items/pP0EwdXTDTjgDpZA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PpVuZdKsS2ms3K3h.htm](pathfinder-bestiary-2-items/PpVuZdKsS2ms3K3h.htm)|Shadow Stride|Sombra Zancada|modificada|
|[Pq25te8zoxBzUMRN.htm](pathfinder-bestiary-2-items/Pq25te8zoxBzUMRN.htm)|Jaws|Fauces|modificada|
|[PQ87JPdxzlCptXn6.htm](pathfinder-bestiary-2-items/PQ87JPdxzlCptXn6.htm)|Tick Fever|Tick Fever|modificada|
|[pqUWKgysApGeGzWo.htm](pathfinder-bestiary-2-items/pqUWKgysApGeGzWo.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[pRC5IW45o7eWNos1.htm](pathfinder-bestiary-2-items/pRC5IW45o7eWNos1.htm)|Jaws|Fauces|modificada|
|[PrFbn0CsipqWtp4z.htm](pathfinder-bestiary-2-items/PrFbn0CsipqWtp4z.htm)|Flytrap Toxin|Toxina Atrapamoscas|modificada|
|[pRxhn354ZvirgsU6.htm](pathfinder-bestiary-2-items/pRxhn354ZvirgsU6.htm)|Recall Venom|Recall Venom|modificada|
|[PskJTiEThXOJo4hH.htm](pathfinder-bestiary-2-items/PskJTiEThXOJo4hH.htm)|Claw|Garra|modificada|
|[pSl7SmKRkTYkQI1N.htm](pathfinder-bestiary-2-items/pSl7SmKRkTYkQI1N.htm)|Stone Curse|Maldición de Piedra|modificada|
|[PSqd8jopT7V35lJt.htm](pathfinder-bestiary-2-items/PSqd8jopT7V35lJt.htm)|Hands of the Murderer|Las manos del asesino|modificada|
|[psRycYQDP439XLGm.htm](pathfinder-bestiary-2-items/psRycYQDP439XLGm.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[pT1xpkQICFklLesg.htm](pathfinder-bestiary-2-items/pT1xpkQICFklLesg.htm)|Tendril|Tendril|modificada|
|[pTjXZs6ZwSF31KLb.htm](pathfinder-bestiary-2-items/pTjXZs6ZwSF31KLb.htm)|Drink Blood|Beber Sangre|modificada|
|[pTowxMM6c9JCvVGF.htm](pathfinder-bestiary-2-items/pTowxMM6c9JCvVGF.htm)|Mind Bolt|Rayo Mental|modificada|
|[pu67MUMU2zgOU5zE.htm](pathfinder-bestiary-2-items/pu67MUMU2zgOU5zE.htm)|Scent|Scent|modificada|
|[puOX1L9eYq450jRo.htm](pathfinder-bestiary-2-items/puOX1L9eYq450jRo.htm)|Scent|Scent|modificada|
|[pUwo9wumwrjtvid1.htm](pathfinder-bestiary-2-items/pUwo9wumwrjtvid1.htm)|Infernal Wound|Herida infernal|modificada|
|[pV2UvWTozyuKQHGx.htm](pathfinder-bestiary-2-items/pV2UvWTozyuKQHGx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[pVbWqNxbj50FvZ38.htm](pathfinder-bestiary-2-items/pVbWqNxbj50FvZ38.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[pvidfrziJK8Olr4p.htm](pathfinder-bestiary-2-items/pvidfrziJK8Olr4p.htm)|Piercing Shot|Piercing Shot|modificada|
|[pViUeTjTLhTubzOw.htm](pathfinder-bestiary-2-items/pViUeTjTLhTubzOw.htm)|Coven|Coven|modificada|
|[Pvp0MGWk24EJhPud.htm](pathfinder-bestiary-2-items/Pvp0MGWk24EJhPud.htm)|Tentacle|Tentáculo|modificada|
|[pvqYJHUkKVxUx3Q2.htm](pathfinder-bestiary-2-items/pvqYJHUkKVxUx3Q2.htm)|Claw|Garra|modificada|
|[pvTRbzLubEXLA5D5.htm](pathfinder-bestiary-2-items/pvTRbzLubEXLA5D5.htm)|Claw|Garra|modificada|
|[PWBjM1JecEFdjzo0.htm](pathfinder-bestiary-2-items/PWBjM1JecEFdjzo0.htm)|Savage Jaws|Fauces de Salvajismo|modificada|
|[pwHxPg0sBxXkBROU.htm](pathfinder-bestiary-2-items/pwHxPg0sBxXkBROU.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[PwSEiS1mqX6rqjJa.htm](pathfinder-bestiary-2-items/PwSEiS1mqX6rqjJa.htm)|Bo Staff|Báculo Bo|modificada|
|[PxJ09KlgYVpdaklS.htm](pathfinder-bestiary-2-items/PxJ09KlgYVpdaklS.htm)|Tail|Tail|modificada|
|[pXklOYevcG58P7sJ.htm](pathfinder-bestiary-2-items/pXklOYevcG58P7sJ.htm)|Beak|Beak|modificada|
|[pXVSNEwIRE2uBaw6.htm](pathfinder-bestiary-2-items/pXVSNEwIRE2uBaw6.htm)|Wing Flash|Alas cegadoras|modificada|
|[Pxxc4fNbFR6zlG0y.htm](pathfinder-bestiary-2-items/Pxxc4fNbFR6zlG0y.htm)|Web Trap|Trampa telara|modificada|
|[Pxxr235CKBdgLZ6y.htm](pathfinder-bestiary-2-items/Pxxr235CKBdgLZ6y.htm)|Jaws|Fauces|modificada|
|[Py8ycO501wOs8ujU.htm](pathfinder-bestiary-2-items/Py8ycO501wOs8ujU.htm)|Legs|Piernas|modificada|
|[pYBvVxfTXqJhnpWd.htm](pathfinder-bestiary-2-items/pYBvVxfTXqJhnpWd.htm)|Claw|Garra|modificada|
|[pYiWNLrYh7MbvSRl.htm](pathfinder-bestiary-2-items/pYiWNLrYh7MbvSRl.htm)|Improved Grab|Agarrado mejorado|modificada|
|[PySanquTNKBp5Zkm.htm](pathfinder-bestiary-2-items/PySanquTNKBp5Zkm.htm)|Breath Weapon|Breath Weapon|modificada|
|[pzMdTIEh6A57hKG7.htm](pathfinder-bestiary-2-items/pzMdTIEh6A57hKG7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[pztBMGaCTnjDqJCY.htm](pathfinder-bestiary-2-items/pztBMGaCTnjDqJCY.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[q0kSVZllWCY1CwD2.htm](pathfinder-bestiary-2-items/q0kSVZllWCY1CwD2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Q0VRtsqm6etoZxCa.htm](pathfinder-bestiary-2-items/Q0VRtsqm6etoZxCa.htm)|Shadow Doubles|Sombra Dobles|modificada|
|[Q0zvzLaylr0AYBGR.htm](pathfinder-bestiary-2-items/Q0zvzLaylr0AYBGR.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[Q1JyYPsUlKes2ELl.htm](pathfinder-bestiary-2-items/Q1JyYPsUlKes2ELl.htm)|Split|Split|modificada|
|[q2KJwlgmVEKQMJPb.htm](pathfinder-bestiary-2-items/q2KJwlgmVEKQMJPb.htm)|Maintain Disguise|Mantenedor Disfraz|modificada|
|[Q3683RHwk5skFzWi.htm](pathfinder-bestiary-2-items/Q3683RHwk5skFzWi.htm)|Dispelling Strike|Golpe disipador|modificada|
|[Q57wXsK3JdmLYoDR.htm](pathfinder-bestiary-2-items/Q57wXsK3JdmLYoDR.htm)|Blade of Justice|Filo de la justicia|modificada|
|[q5XLED0zxjExmJp2.htm](pathfinder-bestiary-2-items/q5XLED0zxjExmJp2.htm)|Tail|Tail|modificada|
|[Q6cQBmJGmIcB0lWr.htm](pathfinder-bestiary-2-items/Q6cQBmJGmIcB0lWr.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Q6Z6sPX8M68AUOXI.htm](pathfinder-bestiary-2-items/Q6Z6sPX8M68AUOXI.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Q7cHvTaIWyRwhMYl.htm](pathfinder-bestiary-2-items/Q7cHvTaIWyRwhMYl.htm)|Web Trap|Trampa telara|modificada|
|[Q7E6X7VR38S4QJfL.htm](pathfinder-bestiary-2-items/Q7E6X7VR38S4QJfL.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[Q7nRRCF8dXONTqKt.htm](pathfinder-bestiary-2-items/Q7nRRCF8dXONTqKt.htm)|Shock|Electrizante|modificada|
|[Q8lUTVhcaklSOhJn.htm](pathfinder-bestiary-2-items/Q8lUTVhcaklSOhJn.htm)|Scent|Scent|modificada|
|[QACVidUBGQoHHpqW.htm](pathfinder-bestiary-2-items/QACVidUBGQoHHpqW.htm)|Mark Quarry|Mark Quarry|modificada|
|[QAiOrfL7q6tF2GUV.htm](pathfinder-bestiary-2-items/QAiOrfL7q6tF2GUV.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[QaqXp54jr3aKQjWN.htm](pathfinder-bestiary-2-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|Curación negativa|modificada|
|[qARQ5Sntem4thRiy.htm](pathfinder-bestiary-2-items/qARQ5Sntem4thRiy.htm)|Improved Grab|Agarrado mejorado|modificada|
|[QaWyk1afljYBga0c.htm](pathfinder-bestiary-2-items/QaWyk1afljYBga0c.htm)|Wind Gust|Ráfaga de viento|modificada|
|[QB6MLb6aO9rvCPbS.htm](pathfinder-bestiary-2-items/QB6MLb6aO9rvCPbS.htm)|Constant Spells|Constant Spells|modificada|
|[qBIJw1s6aIrNQQaJ.htm](pathfinder-bestiary-2-items/qBIJw1s6aIrNQQaJ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[QbjjKmqSkCf93Xw5.htm](pathfinder-bestiary-2-items/QbjjKmqSkCf93Xw5.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qbm10J9qDUiFCQDn.htm](pathfinder-bestiary-2-items/qbm10J9qDUiFCQDn.htm)|Arc Lightning|Arco de relámpagos|modificada|
|[qbn93YsujSFI6TMZ.htm](pathfinder-bestiary-2-items/qbn93YsujSFI6TMZ.htm)|Camouflage|Camuflaje|modificada|
|[QBq3x8HUuGt0wVZm.htm](pathfinder-bestiary-2-items/QBq3x8HUuGt0wVZm.htm)|Sense Blood (Imprecise) 60 feet|Sentido de la Sangre (Impreciso) 60 pies|modificada|
|[qcCotjUp5yYyWkjT.htm](pathfinder-bestiary-2-items/qcCotjUp5yYyWkjT.htm)|Claw|Garra|modificada|
|[qCZyXHHj5OMACPYY.htm](pathfinder-bestiary-2-items/qCZyXHHj5OMACPYY.htm)|Taiga Linnorm Venom|Veneno de linnorm de la taiga|modificada|
|[QD2iwjozawGYYqOf.htm](pathfinder-bestiary-2-items/QD2iwjozawGYYqOf.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[qdKGH0iZztLLCCPz.htm](pathfinder-bestiary-2-items/qdKGH0iZztLLCCPz.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[qebLFwBJf98hFcSv.htm](pathfinder-bestiary-2-items/qebLFwBJf98hFcSv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qEh64WzRoljm53JB.htm](pathfinder-bestiary-2-items/qEh64WzRoljm53JB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qeIQpw65yitumRkB.htm](pathfinder-bestiary-2-items/qeIQpw65yitumRkB.htm)|Jaws|Fauces|modificada|
|[qfAGiyqMgrNFUoSw.htm](pathfinder-bestiary-2-items/qfAGiyqMgrNFUoSw.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[qfIg2ulL65DhV6X2.htm](pathfinder-bestiary-2-items/qfIg2ulL65DhV6X2.htm)|Sprint|Sprint|modificada|
|[qfkx0yR9a3Qh4LsT.htm](pathfinder-bestiary-2-items/qfkx0yR9a3Qh4LsT.htm)|Double Attack|Double Attack|modificada|
|[qfYFz5QrJfsbmJng.htm](pathfinder-bestiary-2-items/qfYFz5QrJfsbmJng.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Qgjku7yO52q21NCG.htm](pathfinder-bestiary-2-items/Qgjku7yO52q21NCG.htm)|Temporal Flurry|Ráfaga Temporal|modificada|
|[qgyml1ToPqFdJIKa.htm](pathfinder-bestiary-2-items/qgyml1ToPqFdJIKa.htm)|Drag Below|Arrastrar Abajo|modificada|
|[qh8LFXVLWjILfZZx.htm](pathfinder-bestiary-2-items/qh8LFXVLWjILfZZx.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[Qhdl7tHPD3bhAihm.htm](pathfinder-bestiary-2-items/Qhdl7tHPD3bhAihm.htm)|Lash Out|Arremeter|modificada|
|[QHdmWY86UyD9ECHR.htm](pathfinder-bestiary-2-items/QHdmWY86UyD9ECHR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qHEzK4MngkQB6vhD.htm](pathfinder-bestiary-2-items/qHEzK4MngkQB6vhD.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[qhgcqwFp1hE2jFbu.htm](pathfinder-bestiary-2-items/qhgcqwFp1hE2jFbu.htm)|Claw|Garra|modificada|
|[QHvw3fI9gFNac33v.htm](pathfinder-bestiary-2-items/QHvw3fI9gFNac33v.htm)|Fangs|Colmillos|modificada|
|[QItaAvzvKUUHn205.htm](pathfinder-bestiary-2-items/QItaAvzvKUUHn205.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[QIzXNCE3GPZBj5sy.htm](pathfinder-bestiary-2-items/QIzXNCE3GPZBj5sy.htm)|Shortsword|Espada corta|modificada|
|[Qj1AizQTHLeRQxPV.htm](pathfinder-bestiary-2-items/Qj1AizQTHLeRQxPV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QjBqDZJ1zOlHWSEG.htm](pathfinder-bestiary-2-items/QjBqDZJ1zOlHWSEG.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[qJQGpBBEACoUjdFY.htm](pathfinder-bestiary-2-items/qJQGpBBEACoUjdFY.htm)|Claw|Garra|modificada|
|[QK7BnQXUv5A7jm84.htm](pathfinder-bestiary-2-items/QK7BnQXUv5A7jm84.htm)|Warden of Erebus|Guardián de Erebus|modificada|
|[ql5PVdfgunDKXI5N.htm](pathfinder-bestiary-2-items/ql5PVdfgunDKXI5N.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QleXAKxC6oYMhWIK.htm](pathfinder-bestiary-2-items/QleXAKxC6oYMhWIK.htm)|Roll|Rollo|modificada|
|[qlRNyTqzf6kMvBEV.htm](pathfinder-bestiary-2-items/qlRNyTqzf6kMvBEV.htm)|Quill|Quill|modificada|
|[QmK15RaSe9yfdYIR.htm](pathfinder-bestiary-2-items/QmK15RaSe9yfdYIR.htm)|Vulnerable Tail|Cola Vulnerable|modificada|
|[QNLWcv6w045fE1oE.htm](pathfinder-bestiary-2-items/QNLWcv6w045fE1oE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[QNMEwPCmUmz3bSOW.htm](pathfinder-bestiary-2-items/QNMEwPCmUmz3bSOW.htm)|Knockdown|Derribo|modificada|
|[QNvBVG4wk0atHVo9.htm](pathfinder-bestiary-2-items/QNvBVG4wk0atHVo9.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[QNy9Hrx2ib5tkQKt.htm](pathfinder-bestiary-2-items/QNy9Hrx2ib5tkQKt.htm)|Shadow Blend|Mezcla de Sombra|modificada|
|[QoOV8oVWiooAilxA.htm](pathfinder-bestiary-2-items/QoOV8oVWiooAilxA.htm)|Sea Snake Venom|Veneno de culebra marina|modificada|
|[qoqJdxMXA4DPeMal.htm](pathfinder-bestiary-2-items/qoqJdxMXA4DPeMal.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[qoSUaPSmr9BrJU8V.htm](pathfinder-bestiary-2-items/qoSUaPSmr9BrJU8V.htm)|Breath Weapon|Breath Weapon|modificada|
|[QoW8rIq8ikH4Ebpr.htm](pathfinder-bestiary-2-items/QoW8rIq8ikH4Ebpr.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[qpAN2omg7HvCwOzk.htm](pathfinder-bestiary-2-items/qpAN2omg7HvCwOzk.htm)|Undetectable|No detectado/a|modificada|
|[QPrrVmViA5QTr6tE.htm](pathfinder-bestiary-2-items/QPrrVmViA5QTr6tE.htm)|Bastion Aura|Aura Bastión|modificada|
|[QQgEGkDjynHqQ53g.htm](pathfinder-bestiary-2-items/QQgEGkDjynHqQ53g.htm)|Constant Spells|Constant Spells|modificada|
|[QqggW4FlHDKMwZT4.htm](pathfinder-bestiary-2-items/QqggW4FlHDKMwZT4.htm)|Push|Push|modificada|
|[QQSJHx4JIqEThZoC.htm](pathfinder-bestiary-2-items/QQSJHx4JIqEThZoC.htm)|Mandibles|Mandíbulas|modificada|
|[QrWMNrtokpcMTBMi.htm](pathfinder-bestiary-2-items/QrWMNrtokpcMTBMi.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qs95uKqjbpoxk48D.htm](pathfinder-bestiary-2-items/qs95uKqjbpoxk48D.htm)|Jaws|Fauces|modificada|
|[QSAMSi294JHAumur.htm](pathfinder-bestiary-2-items/QSAMSi294JHAumur.htm)|Rock|Roca|modificada|
|[QSxWcw9SiRdGtKM7.htm](pathfinder-bestiary-2-items/QSxWcw9SiRdGtKM7.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[QTFJOKoZto5yRgDd.htm](pathfinder-bestiary-2-items/QTFJOKoZto5yRgDd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QTnEqxIt2MaL0WlZ.htm](pathfinder-bestiary-2-items/QTnEqxIt2MaL0WlZ.htm)|Spirit Naga Venom|Spirit Naga Venom|modificada|
|[QTxOTyLCST6Zaqb3.htm](pathfinder-bestiary-2-items/QTxOTyLCST6Zaqb3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qu9kOE72tZH2eGQ1.htm](pathfinder-bestiary-2-items/qu9kOE72tZH2eGQ1.htm)|Constant Spells|Constant Spells|modificada|
|[quht5jPh2LiFjJft.htm](pathfinder-bestiary-2-items/quht5jPh2LiFjJft.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[QuK27vpZ1PXmKcRW.htm](pathfinder-bestiary-2-items/QuK27vpZ1PXmKcRW.htm)|Fist|Puño|modificada|
|[qUtjLPzfgoNg1aAP.htm](pathfinder-bestiary-2-items/qUtjLPzfgoNg1aAP.htm)|Mandibles|Mandíbulas|modificada|
|[QUwEfng9Ey9oqGwE.htm](pathfinder-bestiary-2-items/QUwEfng9Ey9oqGwE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QUWT5qx8jyGSWpIM.htm](pathfinder-bestiary-2-items/QUWT5qx8jyGSWpIM.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[QuXauTCFwPyxjFZ6.htm](pathfinder-bestiary-2-items/QuXauTCFwPyxjFZ6.htm)|Accursed Breath|Aliento maldito|modificada|
|[qV74gNViCaiEVUy2.htm](pathfinder-bestiary-2-items/qV74gNViCaiEVUy2.htm)|Capsize|Volcar|modificada|
|[QVlwvsf0cmEupKV2.htm](pathfinder-bestiary-2-items/QVlwvsf0cmEupKV2.htm)|Tremorsense|Tremorsense|modificada|
|[qWb7LRbSfICoEdcJ.htm](pathfinder-bestiary-2-items/qWb7LRbSfICoEdcJ.htm)|Constant Spells|Constant Spells|modificada|
|[qWcmE7mc3EOqNR3L.htm](pathfinder-bestiary-2-items/qWcmE7mc3EOqNR3L.htm)|Tentacle|Tentáculo|modificada|
|[qwrA1CcDM9QaJlc5.htm](pathfinder-bestiary-2-items/qwrA1CcDM9QaJlc5.htm)|Pounce|Abalanzarse|modificada|
|[QwsbmnGAaEHYU0ut.htm](pathfinder-bestiary-2-items/QwsbmnGAaEHYU0ut.htm)|Lightning Bolt|Relámpago|modificada|
|[Qx0Uh3ktQIh1dVKT.htm](pathfinder-bestiary-2-items/Qx0Uh3ktQIh1dVKT.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[qxh5yKErE5FpGPnf.htm](pathfinder-bestiary-2-items/qxh5yKErE5FpGPnf.htm)|Knockdown|Derribo|modificada|
|[qxkuhugvyoxDpt7N.htm](pathfinder-bestiary-2-items/qxkuhugvyoxDpt7N.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[qXrvqRXGNuAyfORa.htm](pathfinder-bestiary-2-items/qXrvqRXGNuAyfORa.htm)|Wrench Spirit|Arrancar el espíritu|modificada|
|[qXSjj6mmQyNoxHDN.htm](pathfinder-bestiary-2-items/qXSjj6mmQyNoxHDN.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[qxsT5YmSCH2dqJAT.htm](pathfinder-bestiary-2-items/qxsT5YmSCH2dqJAT.htm)|Amalgam|Amalgama|modificada|
|[QxUxl1vPtZ72uSOi.htm](pathfinder-bestiary-2-items/QxUxl1vPtZ72uSOi.htm)|Mist Vision|Mist Vision|modificada|
|[QXYKDwpj06Pd7sEo.htm](pathfinder-bestiary-2-items/QXYKDwpj06Pd7sEo.htm)|Scent|Scent|modificada|
|[QY55zAJAyvaoflJY.htm](pathfinder-bestiary-2-items/QY55zAJAyvaoflJY.htm)|Nature Empathy|Nature Empathy|modificada|
|[qZaBb2z6mE2zLL2u.htm](pathfinder-bestiary-2-items/qZaBb2z6mE2zLL2u.htm)|Create Object|Elaborar objeto|modificada|
|[Qzr8MBWWm4DE5P8H.htm](pathfinder-bestiary-2-items/Qzr8MBWWm4DE5P8H.htm)|Discorporate|Discorporate|modificada|
|[QZup3O3zcKH6TFZd.htm](pathfinder-bestiary-2-items/QZup3O3zcKH6TFZd.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[R1voQq09UIwdTVP5.htm](pathfinder-bestiary-2-items/R1voQq09UIwdTVP5.htm)|Mercy Vulnerability|Vulnerabilidad de Merced|modificada|
|[R2a5AYr0RbUoKiK3.htm](pathfinder-bestiary-2-items/R2a5AYr0RbUoKiK3.htm)|Sard Venom|Sard Venom|modificada|
|[R2ziTTF38PwKFQr7.htm](pathfinder-bestiary-2-items/R2ziTTF38PwKFQr7.htm)|Frightful Presence|Frightful Presence|modificada|
|[R30Gee8hs5TPUNYY.htm](pathfinder-bestiary-2-items/R30Gee8hs5TPUNYY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r3hGo7TAQ3TtGwLY.htm](pathfinder-bestiary-2-items/r3hGo7TAQ3TtGwLY.htm)|Electrolocation|Electrolocation|modificada|
|[R3HSbrKclMuhH014.htm](pathfinder-bestiary-2-items/R3HSbrKclMuhH014.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[r421CNwt0qBGoUVK.htm](pathfinder-bestiary-2-items/r421CNwt0qBGoUVK.htm)|Retributive Strike|Golpe retributivo|modificada|
|[R4jXbSxWyqNtYJyp.htm](pathfinder-bestiary-2-items/R4jXbSxWyqNtYJyp.htm)|Thumb Spike|Punta de pulgar|modificada|
|[r5Mw7uM8br1L49y5.htm](pathfinder-bestiary-2-items/r5Mw7uM8br1L49y5.htm)|Wing|Ala|modificada|
|[r62684lc4BkUMn7p.htm](pathfinder-bestiary-2-items/r62684lc4BkUMn7p.htm)|Solidify Mist|Solidificar Niebla|modificada|
|[r7aL4DHJpi1R2n6Z.htm](pathfinder-bestiary-2-items/r7aL4DHJpi1R2n6Z.htm)|Improved Grab|Agarrado mejorado|modificada|
|[R8bzIOrsR2PFpnSQ.htm](pathfinder-bestiary-2-items/R8bzIOrsR2PFpnSQ.htm)|Noxious Fumes|Noxious Fumes|modificada|
|[R8vIKnaOA34FiWMY.htm](pathfinder-bestiary-2-items/R8vIKnaOA34FiWMY.htm)|Water Jet|Chorro de Agua|modificada|
|[r8zcJu9p3TzgNHoU.htm](pathfinder-bestiary-2-items/r8zcJu9p3TzgNHoU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[R9YnxMf9012f1ajN.htm](pathfinder-bestiary-2-items/R9YnxMf9012f1ajN.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[rA2rDDvXjf2Cfr4w.htm](pathfinder-bestiary-2-items/rA2rDDvXjf2Cfr4w.htm)|Grab|Agarrado|modificada|
|[ra2VPYkOtDIX5nt5.htm](pathfinder-bestiary-2-items/ra2VPYkOtDIX5nt5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rajOpAe3FLG1J8uc.htm](pathfinder-bestiary-2-items/rajOpAe3FLG1J8uc.htm)|Starknife|Starknife|modificada|
|[rASGf5YkW2lQXMFQ.htm](pathfinder-bestiary-2-items/rASGf5YkW2lQXMFQ.htm)|Trample|Trample|modificada|
|[rasNjBJHTsaP5LFC.htm](pathfinder-bestiary-2-items/rasNjBJHTsaP5LFC.htm)|Tail|Tail|modificada|
|[rB0kJuC158tvKsmj.htm](pathfinder-bestiary-2-items/rB0kJuC158tvKsmj.htm)|Garbled Thoughts|Pensamientos confusos|modificada|
|[rBN7VRWeDcgG0c1C.htm](pathfinder-bestiary-2-items/rBN7VRWeDcgG0c1C.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rCDCEl5KfuYha1bo.htm](pathfinder-bestiary-2-items/rCDCEl5KfuYha1bo.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[rCDEvA0MWLJZe3ba.htm](pathfinder-bestiary-2-items/rCDEvA0MWLJZe3ba.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[rCFV63sVu1lfxToK.htm](pathfinder-bestiary-2-items/rCFV63sVu1lfxToK.htm)|Negative Healing|Curación negativa|modificada|
|[RCjZk0ilv6kHRuT5.htm](pathfinder-bestiary-2-items/RCjZk0ilv6kHRuT5.htm)|Curse of the Weretiger|Maldición del Weretiger|modificada|
|[rDl9ZNp0ry5Tek3B.htm](pathfinder-bestiary-2-items/rDl9ZNp0ry5Tek3B.htm)|Dagger|Daga|modificada|
|[rdri6pl6hY1glOfy.htm](pathfinder-bestiary-2-items/rdri6pl6hY1glOfy.htm)|Cold Healing|Curación por frío|modificada|
|[reBkTZBqwYPgsiFb.htm](pathfinder-bestiary-2-items/reBkTZBqwYPgsiFb.htm)|Tail|Tail|modificada|
|[ReQtBIuf6WBTNwEH.htm](pathfinder-bestiary-2-items/ReQtBIuf6WBTNwEH.htm)|Thorn|Espina|modificada|
|[reVIS3aes83WAuF2.htm](pathfinder-bestiary-2-items/reVIS3aes83WAuF2.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[RF4DrP2dezs9NRn8.htm](pathfinder-bestiary-2-items/RF4DrP2dezs9NRn8.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RFcZ8BNKEh3WWl6j.htm](pathfinder-bestiary-2-items/RFcZ8BNKEh3WWl6j.htm)|Whispering Wounds|Heridas susurrantes|modificada|
|[RFGL24YZ4ufhBCRM.htm](pathfinder-bestiary-2-items/RFGL24YZ4ufhBCRM.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RFPvW7U5w3NRyp3q.htm](pathfinder-bestiary-2-items/RFPvW7U5w3NRyp3q.htm)|Envisioning|Visualizado|modificada|
|[RGaUdIjwMpgnNYGg.htm](pathfinder-bestiary-2-items/RGaUdIjwMpgnNYGg.htm)|Pain Starvation|Hambruna de dolor|modificada|
|[rGG9CbH3CnQNbztb.htm](pathfinder-bestiary-2-items/rGG9CbH3CnQNbztb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rGvREWDBZrWzdUhd.htm](pathfinder-bestiary-2-items/rGvREWDBZrWzdUhd.htm)|Beak|Beak|modificada|
|[RHRtFPGTQDqJ0cER.htm](pathfinder-bestiary-2-items/RHRtFPGTQDqJ0cER.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Rid4PCaJM2edDiKh.htm](pathfinder-bestiary-2-items/Rid4PCaJM2edDiKh.htm)|Forest Step|Paso arbóreo|modificada|
|[RjfMaoY3Fp7Ouab8.htm](pathfinder-bestiary-2-items/RjfMaoY3Fp7Ouab8.htm)|Tremorsense|Tremorsense|modificada|
|[RJUajmwcmdSx0tQb.htm](pathfinder-bestiary-2-items/RJUajmwcmdSx0tQb.htm)|Mesmerizing Melody|Melodía Hipnotizadora|modificada|
|[rkayE4XQbhVQPYCS.htm](pathfinder-bestiary-2-items/rkayE4XQbhVQPYCS.htm)|Crossbow|Ballesta|modificada|
|[rkxRbGfC97VpayNy.htm](pathfinder-bestiary-2-items/rkxRbGfC97VpayNy.htm)|Triumvirate|Triunvirato|modificada|
|[RkzJDhpWzxqQXiAq.htm](pathfinder-bestiary-2-items/RkzJDhpWzxqQXiAq.htm)|Strafing Rush|Embestida castigadora|modificada|
|[rl4i6PCiSzEqRVyf.htm](pathfinder-bestiary-2-items/rl4i6PCiSzEqRVyf.htm)|Claw|Garra|modificada|
|[RLJbJxbajEsFLapw.htm](pathfinder-bestiary-2-items/RLJbJxbajEsFLapw.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[rLT3Kld8E7p9hG1y.htm](pathfinder-bestiary-2-items/rLT3Kld8E7p9hG1y.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[RMCsutKfNLtikzWK.htm](pathfinder-bestiary-2-items/RMCsutKfNLtikzWK.htm)|Constant Spells|Constant Spells|modificada|
|[rMTD8eLYVnpfvpCd.htm](pathfinder-bestiary-2-items/rMTD8eLYVnpfvpCd.htm)|Wing|Ala|modificada|
|[rN5DBFuJma8wPkT1.htm](pathfinder-bestiary-2-items/rN5DBFuJma8wPkT1.htm)|Grab|Agarrado|modificada|
|[RNDT57aD8UzBIRVk.htm](pathfinder-bestiary-2-items/RNDT57aD8UzBIRVk.htm)|Pack Attack|Ataque en manada|modificada|
|[RniBjxUV8FfeP05P.htm](pathfinder-bestiary-2-items/RniBjxUV8FfeP05P.htm)|Blue-Ringed Octopus Venom|Veneno de pulpo de anillos azules|modificada|
|[rnvfOLxvrsqRTH2s.htm](pathfinder-bestiary-2-items/rnvfOLxvrsqRTH2s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[roCv7KuzhfzP7hQD.htm](pathfinder-bestiary-2-items/roCv7KuzhfzP7hQD.htm)|Claw|Garra|modificada|
|[ROfUBzcM8hE66OL5.htm](pathfinder-bestiary-2-items/ROfUBzcM8hE66OL5.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ROXOsK91xVXF9I3t.htm](pathfinder-bestiary-2-items/ROXOsK91xVXF9I3t.htm)|Claw|Garra|modificada|
|[rOyG05jMYSlAcSS5.htm](pathfinder-bestiary-2-items/rOyG05jMYSlAcSS5.htm)|Swiftness|Celeridad|modificada|
|[Rp0zPvS2PAgfYdfn.htm](pathfinder-bestiary-2-items/Rp0zPvS2PAgfYdfn.htm)|Fist|Puño|modificada|
|[rp6F9XDfGE9qOef0.htm](pathfinder-bestiary-2-items/rp6F9XDfGE9qOef0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rPjj6ZCE5pwwVZg1.htm](pathfinder-bestiary-2-items/rPjj6ZCE5pwwVZg1.htm)|Jaws|Fauces|modificada|
|[rpp6hEDYHYJ2EJ15.htm](pathfinder-bestiary-2-items/rpp6hEDYHYJ2EJ15.htm)|Claw|Garra|modificada|
|[rQn75QJUJH2hQKpE.htm](pathfinder-bestiary-2-items/rQn75QJUJH2hQKpE.htm)|Chain of Malebolge|Cadena de Malebolge|modificada|
|[RrDmY99gWZJtFtjo.htm](pathfinder-bestiary-2-items/RrDmY99gWZJtFtjo.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[rRJmbyj1idfcUw82.htm](pathfinder-bestiary-2-items/rRJmbyj1idfcUw82.htm)|Grab|Agarrado|modificada|
|[RRuGVF2WBzHvPAD4.htm](pathfinder-bestiary-2-items/RRuGVF2WBzHvPAD4.htm)|Vine|Vid|modificada|
|[rsf1q6ooMNJFjdfh.htm](pathfinder-bestiary-2-items/rsf1q6ooMNJFjdfh.htm)|No Hearing|No Hearing|modificada|
|[RSov7mEf6h3jgcfL.htm](pathfinder-bestiary-2-items/RSov7mEf6h3jgcfL.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[rSpxgtewFq1FNp4L.htm](pathfinder-bestiary-2-items/rSpxgtewFq1FNp4L.htm)|Retune|Volver a afinar|modificada|
|[RSXc4NTQGKjlqt3X.htm](pathfinder-bestiary-2-items/RSXc4NTQGKjlqt3X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RTbPNwtAI2pRCjBX.htm](pathfinder-bestiary-2-items/RTbPNwtAI2pRCjBX.htm)|Sling|Sling|modificada|
|[RTUQPDiCd9wKvFow.htm](pathfinder-bestiary-2-items/RTUQPDiCd9wKvFow.htm)|Tentacle Encage|Tentacle Encage|modificada|
|[RUcTjfOyyFNcxgIK.htm](pathfinder-bestiary-2-items/RUcTjfOyyFNcxgIK.htm)|Reactive Chomp|Mordisco reactivo|modificada|
|[RUPrm8gIOS2f852a.htm](pathfinder-bestiary-2-items/RUPrm8gIOS2f852a.htm)|Ferocity|Ferocidad|modificada|
|[rUvfMcCzgvir0ElO.htm](pathfinder-bestiary-2-items/rUvfMcCzgvir0ElO.htm)|Jaws|Fauces|modificada|
|[RvyDbBs5muX2yHdI.htm](pathfinder-bestiary-2-items/RvyDbBs5muX2yHdI.htm)|Stolen Death|Muerte Robada|modificada|
|[RwPr1Ik2tKeFrL0r.htm](pathfinder-bestiary-2-items/RwPr1Ik2tKeFrL0r.htm)|Swiftness|Celeridad|modificada|
|[RwVJwyaaTesWbfgR.htm](pathfinder-bestiary-2-items/RwVJwyaaTesWbfgR.htm)|Manipulate Flames|Manipular llamasígera.|modificada|
|[rwYZxmONVe2JmBPn.htm](pathfinder-bestiary-2-items/rwYZxmONVe2JmBPn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rx9aGcoGrLLVPzhV.htm](pathfinder-bestiary-2-items/rx9aGcoGrLLVPzhV.htm)|Withering Opportunity|Oportunidad de marchitamiento|modificada|
|[RXUhJpHPbpDMUlbJ.htm](pathfinder-bestiary-2-items/RXUhJpHPbpDMUlbJ.htm)|Change Shape|Change Shape|modificada|
|[RZEi7CewQZtkaHnE.htm](pathfinder-bestiary-2-items/RZEi7CewQZtkaHnE.htm)|Bleeding Critical|Sangrado Crítico|modificada|
|[s0ekhcyN4jpM6jLQ.htm](pathfinder-bestiary-2-items/s0ekhcyN4jpM6jLQ.htm)|Rock|Roca|modificada|
|[S0ETALXebRGPPFPv.htm](pathfinder-bestiary-2-items/S0ETALXebRGPPFPv.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[s1CEAQqJM8T6nqR4.htm](pathfinder-bestiary-2-items/s1CEAQqJM8T6nqR4.htm)|Claw|Garra|modificada|
|[s23ZMm1sJc7XLyKW.htm](pathfinder-bestiary-2-items/s23ZMm1sJc7XLyKW.htm)|Seed Spray|Spray de semillas|modificada|
|[S2jsW7P3mTUPKBHZ.htm](pathfinder-bestiary-2-items/S2jsW7P3mTUPKBHZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s2VxShdmP0XyPcUl.htm](pathfinder-bestiary-2-items/s2VxShdmP0XyPcUl.htm)|Tremorsense|Tremorsense|modificada|
|[S31hZB9EKGtsEZ7B.htm](pathfinder-bestiary-2-items/S31hZB9EKGtsEZ7B.htm)|Orrery|Orrery|modificada|
|[S3MoMWkoEovX2zha.htm](pathfinder-bestiary-2-items/S3MoMWkoEovX2zha.htm)|Fist|Puño|modificada|
|[s43Ks62ECjo1B5kX.htm](pathfinder-bestiary-2-items/s43Ks62ECjo1B5kX.htm)|Claw|Garra|modificada|
|[s4eMY3meCcZpGDrL.htm](pathfinder-bestiary-2-items/s4eMY3meCcZpGDrL.htm)|Improved Grab|Agarrado mejorado|modificada|
|[S6Avv1gbTIidozSY.htm](pathfinder-bestiary-2-items/S6Avv1gbTIidozSY.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[s7GGq81IzCx73D2B.htm](pathfinder-bestiary-2-items/s7GGq81IzCx73D2B.htm)|Call to Blood|Llamada a la Sangre|modificada|
|[S7QInhdroOaf7k84.htm](pathfinder-bestiary-2-items/S7QInhdroOaf7k84.htm)|Tremorsense|Tremorsense|modificada|
|[s8TihgQDAypdPOee.htm](pathfinder-bestiary-2-items/s8TihgQDAypdPOee.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S8XYYEnqxZeY5C7K.htm](pathfinder-bestiary-2-items/S8XYYEnqxZeY5C7K.htm)|Daemonic Famine|Hambre daimónico|modificada|
|[s9Def6M7z5ceOIoj.htm](pathfinder-bestiary-2-items/s9Def6M7z5ceOIoj.htm)|Pounce|Abalanzarse|modificada|
|[s9PF3C48raLmLaNG.htm](pathfinder-bestiary-2-items/s9PF3C48raLmLaNG.htm)|Scent|Scent|modificada|
|[sA3oeWc14avc7CPO.htm](pathfinder-bestiary-2-items/sA3oeWc14avc7CPO.htm)|Wrath of Fate|Wrath of Fate|modificada|
|[sabgo6OVGbFhpOOA.htm](pathfinder-bestiary-2-items/sabgo6OVGbFhpOOA.htm)|Compel Condemned|Compeler a los condenados|modificada|
|[SaS7qHeGtLYWa1rX.htm](pathfinder-bestiary-2-items/SaS7qHeGtLYWa1rX.htm)|Tail Claw|Garra de Cola|modificada|
|[SaUKY4rTuI0N39ad.htm](pathfinder-bestiary-2-items/SaUKY4rTuI0N39ad.htm)|Scent|Scent|modificada|
|[SaVJfclmAsjFCBke.htm](pathfinder-bestiary-2-items/SaVJfclmAsjFCBke.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[SB54Z5VO15GfJd0X.htm](pathfinder-bestiary-2-items/SB54Z5VO15GfJd0X.htm)|Rend|Rasgadura|modificada|
|[sBfOfFAVZjz8e2Sk.htm](pathfinder-bestiary-2-items/sBfOfFAVZjz8e2Sk.htm)|Grab|Agarrado|modificada|
|[sbPAeP8uZ3oiRj7q.htm](pathfinder-bestiary-2-items/sbPAeP8uZ3oiRj7q.htm)|Tangling Chains|Enredando Cadenas|modificada|
|[sc9sTDDxNEMsTklK.htm](pathfinder-bestiary-2-items/sc9sTDDxNEMsTklK.htm)|Mist Vision|Mist Vision|modificada|
|[scNTrSPuw9K5tRtq.htm](pathfinder-bestiary-2-items/scNTrSPuw9K5tRtq.htm)|Wind Form|Forma de viento|modificada|
|[SDF2zWv6zGkmcQH9.htm](pathfinder-bestiary-2-items/SDF2zWv6zGkmcQH9.htm)|Bite|Muerdemuerde|modificada|
|[sDghWo7WoJwJ3FPJ.htm](pathfinder-bestiary-2-items/sDghWo7WoJwJ3FPJ.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[sdQC9PYloWzpJd9P.htm](pathfinder-bestiary-2-items/sdQC9PYloWzpJd9P.htm)|Grab|Agarrado|modificada|
|[Sei3FrQPmhMHMkTo.htm](pathfinder-bestiary-2-items/Sei3FrQPmhMHMkTo.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[Sep8lU0AriLntZmy.htm](pathfinder-bestiary-2-items/Sep8lU0AriLntZmy.htm)|Vanth's Curse|La Maldición de Vanth|modificada|
|[sfq8i9rLngOZhIeU.htm](pathfinder-bestiary-2-items/sfq8i9rLngOZhIeU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[sfWdhP3dRBdtmiol.htm](pathfinder-bestiary-2-items/sfWdhP3dRBdtmiol.htm)|Blood Drain|Drenar sangre|modificada|
|[Sg3CFomZepZdIWcH.htm](pathfinder-bestiary-2-items/Sg3CFomZepZdIWcH.htm)|Death Gaze|Mirada de la Muerte|modificada|
|[sGGppAnKyKB4kipq.htm](pathfinder-bestiary-2-items/sGGppAnKyKB4kipq.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[sguXTwhSIa1swJ1p.htm](pathfinder-bestiary-2-items/sguXTwhSIa1swJ1p.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[SHBwJIixLi2Gr8Lx.htm](pathfinder-bestiary-2-items/SHBwJIixLi2Gr8Lx.htm)|Savage Assault|Asalto Salvajismo|modificada|
|[SIJWWzmQ2ZLmATdy.htm](pathfinder-bestiary-2-items/SIJWWzmQ2ZLmATdy.htm)|Claw|Garra|modificada|
|[sIL9AEoQV1qcrskK.htm](pathfinder-bestiary-2-items/sIL9AEoQV1qcrskK.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[SITpZWO5sj03l95G.htm](pathfinder-bestiary-2-items/SITpZWO5sj03l95G.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[sjk7gsZB7YAEVqjS.htm](pathfinder-bestiary-2-items/sjk7gsZB7YAEVqjS.htm)|Focus Gaze|Centrar mirada|modificada|
|[sJmlL0pxIhzEUZxO.htm](pathfinder-bestiary-2-items/sJmlL0pxIhzEUZxO.htm)|Trample|Trample|modificada|
|[SLi7KeKZZiNBPdi5.htm](pathfinder-bestiary-2-items/SLi7KeKZZiNBPdi5.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[sLqxJoKDLd1T2p2i.htm](pathfinder-bestiary-2-items/sLqxJoKDLd1T2p2i.htm)|Solid Blow|Golpe Sólido|modificada|
|[SLu0NlKYzhM3DVPZ.htm](pathfinder-bestiary-2-items/SLu0NlKYzhM3DVPZ.htm)|Aura of Doom|Aura de Perdición|modificada|
|[SLw6k2GamdDXVK6d.htm](pathfinder-bestiary-2-items/SLw6k2GamdDXVK6d.htm)|Scent|Scent|modificada|
|[sMafmLEOWxv5RH1h.htm](pathfinder-bestiary-2-items/sMafmLEOWxv5RH1h.htm)|Jaws|Fauces|modificada|
|[sMNeAt05PFEaDVIK.htm](pathfinder-bestiary-2-items/sMNeAt05PFEaDVIK.htm)|Entropy Sense|Entropy Sense|modificada|
|[sMQewzNATahHc65F.htm](pathfinder-bestiary-2-items/sMQewzNATahHc65F.htm)|Death Implosion|Implosión de la Muerte|modificada|
|[Smr9bqHYQl1EI48C.htm](pathfinder-bestiary-2-items/Smr9bqHYQl1EI48C.htm)|Dragon Heat|Calor dracónico|modificada|
|[sN3JKaIDG5kFRAvC.htm](pathfinder-bestiary-2-items/sN3JKaIDG5kFRAvC.htm)|Natural Camouflage|Camuflaje Natural|modificada|
|[snHGvNo3Y3eluuUN.htm](pathfinder-bestiary-2-items/snHGvNo3Y3eluuUN.htm)|Compel Courage|Obligar a Coraje|modificada|
|[snsO2nPGYHe6pZx0.htm](pathfinder-bestiary-2-items/snsO2nPGYHe6pZx0.htm)|Create Web Weaponry|Crear armamento de telara.|modificada|
|[SNtv0J2CRa2RFhi9.htm](pathfinder-bestiary-2-items/SNtv0J2CRa2RFhi9.htm)|Entropic Touch|Entropic Touch|modificada|
|[sNUoqbThXZbfj5S7.htm](pathfinder-bestiary-2-items/sNUoqbThXZbfj5S7.htm)|Fade from View|Esfumarse a plena vista|modificada|
|[so21bQRtejWVxF5S.htm](pathfinder-bestiary-2-items/so21bQRtejWVxF5S.htm)|Twisting Tail|Enredar con la cola|modificada|
|[So7Gr127G3zbRJAt.htm](pathfinder-bestiary-2-items/So7Gr127G3zbRJAt.htm)|Fist|Puño|modificada|
|[SOapd5GiFsybvQAC.htm](pathfinder-bestiary-2-items/SOapd5GiFsybvQAC.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[SOgonEKuJHWkgeNc.htm](pathfinder-bestiary-2-items/SOgonEKuJHWkgeNc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sP3rroIKBiOI5Xug.htm](pathfinder-bestiary-2-items/sP3rroIKBiOI5Xug.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[SPkIUm3665kXInNU.htm](pathfinder-bestiary-2-items/SPkIUm3665kXInNU.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[SpQLyhSHuzpd81GZ.htm](pathfinder-bestiary-2-items/SpQLyhSHuzpd81GZ.htm)|Jaws|Fauces|modificada|
|[SQM0OqkayL2Oywmm.htm](pathfinder-bestiary-2-items/SQM0OqkayL2Oywmm.htm)|Tail|Tail|modificada|
|[SqmhCTwOKtI5c7BT.htm](pathfinder-bestiary-2-items/SqmhCTwOKtI5c7BT.htm)|Mote of Light|Mota de luz|modificada|
|[sqrdrxJ0d19wPysW.htm](pathfinder-bestiary-2-items/sqrdrxJ0d19wPysW.htm)|Catch Rock|Atrapar roca|modificada|
|[sR5alX5hZopxwTLw.htm](pathfinder-bestiary-2-items/sR5alX5hZopxwTLw.htm)|Web Bola|Telara de Boleadoras|modificada|
|[sRJgd3DpiEABxebP.htm](pathfinder-bestiary-2-items/sRJgd3DpiEABxebP.htm)|Deflecting Gale|Vendaval protector|modificada|
|[SRNjrRDQg8BEgVDx.htm](pathfinder-bestiary-2-items/SRNjrRDQg8BEgVDx.htm)|Ferocity|Ferocidad|modificada|
|[sSAVmevencpx75UB.htm](pathfinder-bestiary-2-items/sSAVmevencpx75UB.htm)|Pestilential Aura|Aura pestilente|modificada|
|[ssitSO6O1esw1YHS.htm](pathfinder-bestiary-2-items/ssitSO6O1esw1YHS.htm)|Frightful Presence|Frightful Presence|modificada|
|[sSkLpLlTDlkKIKAw.htm](pathfinder-bestiary-2-items/sSkLpLlTDlkKIKAw.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[st5T6nXfjzQXbUOB.htm](pathfinder-bestiary-2-items/st5T6nXfjzQXbUOB.htm)|Lancing Charge|Lancing Charge|modificada|
|[stgGnScdknvWx6D2.htm](pathfinder-bestiary-2-items/stgGnScdknvWx6D2.htm)|Regeneration 15 (Deactivated by Acid or Fire)|Regeneración 15 (Desactivado por Ácido o Fuego)|modificada|
|[stKoZGNXuFFgUzxE.htm](pathfinder-bestiary-2-items/stKoZGNXuFFgUzxE.htm)|Fist|Puño|modificada|
|[storTBRDZBrko2mc.htm](pathfinder-bestiary-2-items/storTBRDZBrko2mc.htm)|Swarm Mind|Swarm Mind|modificada|
|[stSp4FJbtoFzokE9.htm](pathfinder-bestiary-2-items/stSp4FJbtoFzokE9.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Stt8YZwfNCRzNgkQ.htm](pathfinder-bestiary-2-items/Stt8YZwfNCRzNgkQ.htm)|Powerful Charge|Carga Poderosa|modificada|
|[Su8xRpdikP4lRabN.htm](pathfinder-bestiary-2-items/Su8xRpdikP4lRabN.htm)|Fist|Puño|modificada|
|[SU9JTCOQpNZUjegs.htm](pathfinder-bestiary-2-items/SU9JTCOQpNZUjegs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[suaF5gRPb1TibfpQ.htm](pathfinder-bestiary-2-items/suaF5gRPb1TibfpQ.htm)|Hooked|Enganchado|modificada|
|[sUall0U5UbL2KkEE.htm](pathfinder-bestiary-2-items/sUall0U5UbL2KkEE.htm)|Reef Octopus Venom|Veneno de pulpo de arrecife|modificada|
|[SuO1iTdYnYyeXOGq.htm](pathfinder-bestiary-2-items/SuO1iTdYnYyeXOGq.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[sUR4J0lzNGl0KfsK.htm](pathfinder-bestiary-2-items/sUR4J0lzNGl0KfsK.htm)|Tusk|Tusk|modificada|
|[SvghWJwmAo36V4Sx.htm](pathfinder-bestiary-2-items/SvghWJwmAo36V4Sx.htm)|Grab|Agarrado|modificada|
|[sw8oj6ADIgUM0I7C.htm](pathfinder-bestiary-2-items/sw8oj6ADIgUM0I7C.htm)|Arm|Brazo|modificada|
|[sW9lqWGbyXr7xeXh.htm](pathfinder-bestiary-2-items/sW9lqWGbyXr7xeXh.htm)|Swallow Whole|Engullir Todo|modificada|
|[swAJMaqujVUFqFKT.htm](pathfinder-bestiary-2-items/swAJMaqujVUFqFKT.htm)|Host Scent|Oler huésped|modificada|
|[swAWlgrj8JoJq6xB.htm](pathfinder-bestiary-2-items/swAWlgrj8JoJq6xB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sWbaqjXgO4EigI5j.htm](pathfinder-bestiary-2-items/sWbaqjXgO4EigI5j.htm)|Lay Web Trap|Mentir trampa telara|modificada|
|[swPjSgGgOFitRVM8.htm](pathfinder-bestiary-2-items/swPjSgGgOFitRVM8.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[swUZOpPbIdXVMQ1X.htm](pathfinder-bestiary-2-items/swUZOpPbIdXVMQ1X.htm)|Horn|Cuerno|modificada|
|[SXISaBxf4jsnV512.htm](pathfinder-bestiary-2-items/SXISaBxf4jsnV512.htm)|Breath Weapon|Breath Weapon|modificada|
|[sy1d3enT3AxVwNJ9.htm](pathfinder-bestiary-2-items/sy1d3enT3AxVwNJ9.htm)|Telepathy|Telepatía|modificada|
|[sy7gZgu0yKjVGQB8.htm](pathfinder-bestiary-2-items/sy7gZgu0yKjVGQB8.htm)|Ferocity|Ferocidad|modificada|
|[sYzEwjtXD4GsP03b.htm](pathfinder-bestiary-2-items/sYzEwjtXD4GsP03b.htm)|Hibernation|Hibernación|modificada|
|[sZ3V72jPCOJ8oK7p.htm](pathfinder-bestiary-2-items/sZ3V72jPCOJ8oK7p.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[SZrUoVGK2e5HE1dm.htm](pathfinder-bestiary-2-items/SZrUoVGK2e5HE1dm.htm)|Brood Leech Swarm Venom|Plaga de crías de sanguijuela Veneno|modificada|
|[T0KOWsF7QwJ1oDQq.htm](pathfinder-bestiary-2-items/T0KOWsF7QwJ1oDQq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[t0On0v7gyVoScNyY.htm](pathfinder-bestiary-2-items/t0On0v7gyVoScNyY.htm)|Staggering Sail|Staggering Sail|modificada|
|[T0vnmUhrqmtMKLQD.htm](pathfinder-bestiary-2-items/T0vnmUhrqmtMKLQD.htm)|Hostile Duet|Dúo hostil|modificada|
|[T30LeVWTmp9xhLg7.htm](pathfinder-bestiary-2-items/T30LeVWTmp9xhLg7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[t3cnDaUX26zIroa8.htm](pathfinder-bestiary-2-items/t3cnDaUX26zIroa8.htm)|Focus Gaze|Centrar mirada|modificada|
|[T4cC9H9vxNTUqnco.htm](pathfinder-bestiary-2-items/T4cC9H9vxNTUqnco.htm)|Deflecting Cloud|Nube deflectora|modificada|
|[T4TOIiPyHfghQy04.htm](pathfinder-bestiary-2-items/T4TOIiPyHfghQy04.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[T5aqHU7uB2IvWwrg.htm](pathfinder-bestiary-2-items/T5aqHU7uB2IvWwrg.htm)|Surgical Rend|Rasgadura Quirúrgica|modificada|
|[T7L2FMHmpDoURw0s.htm](pathfinder-bestiary-2-items/T7L2FMHmpDoURw0s.htm)|Clinging Suckers|Chupadores Aferrados|modificada|
|[T7rl8xICznuEuEmm.htm](pathfinder-bestiary-2-items/T7rl8xICznuEuEmm.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[T7rTM38nfnCsn85I.htm](pathfinder-bestiary-2-items/T7rTM38nfnCsn85I.htm)|Jaws|Fauces|modificada|
|[T7tMkEUVA0Pq90Yo.htm](pathfinder-bestiary-2-items/T7tMkEUVA0Pq90Yo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[t8m638wy4P8pa4PZ.htm](pathfinder-bestiary-2-items/t8m638wy4P8pa4PZ.htm)|Jaws|Fauces|modificada|
|[t8ToZMzymYUk47az.htm](pathfinder-bestiary-2-items/t8ToZMzymYUk47az.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[t9eUo7KSFGGelyru.htm](pathfinder-bestiary-2-items/t9eUo7KSFGGelyru.htm)|Trample|Trample|modificada|
|[t9ReCb126y5KXuru.htm](pathfinder-bestiary-2-items/t9ReCb126y5KXuru.htm)|Opportune Witchflame|Opportune Witchflame|modificada|
|[TA0G6wsaR62bthsu.htm](pathfinder-bestiary-2-items/TA0G6wsaR62bthsu.htm)|Constant Spells|Constant Spells|modificada|
|[tAdSWqK8N7bxwjab.htm](pathfinder-bestiary-2-items/tAdSWqK8N7bxwjab.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[taEoZD9fGbhrr6G9.htm](pathfinder-bestiary-2-items/taEoZD9fGbhrr6G9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TAIcUgt3JKg5c7fV.htm](pathfinder-bestiary-2-items/TAIcUgt3JKg5c7fV.htm)|Scarecrow's Leer|Scarecrow's Leer|modificada|
|[TBp1tYUPtYoOEonO.htm](pathfinder-bestiary-2-items/TBp1tYUPtYoOEonO.htm)|Aquatic Echolocation|Ecolocalización acuática|modificada|
|[TBsuxObKIaeVagO5.htm](pathfinder-bestiary-2-items/TBsuxObKIaeVagO5.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tByLKFHCnMANfCJV.htm](pathfinder-bestiary-2-items/tByLKFHCnMANfCJV.htm)|Fist|Puño|modificada|
|[tcE3BBuNT8JtsDJQ.htm](pathfinder-bestiary-2-items/tcE3BBuNT8JtsDJQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TcRZ95uAXroBAwRv.htm](pathfinder-bestiary-2-items/TcRZ95uAXroBAwRv.htm)|Verdurous Ooze Acid|Ácido Cieno verduroso|modificada|
|[tCt2b8QlMXslGLNM.htm](pathfinder-bestiary-2-items/tCt2b8QlMXslGLNM.htm)|Feel the Blades|Sentir las cuchillas|modificada|
|[tCTtMSu5Ius1bg6U.htm](pathfinder-bestiary-2-items/tCTtMSu5Ius1bg6U.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[tdnc6MMJXidby82v.htm](pathfinder-bestiary-2-items/tdnc6MMJXidby82v.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TdOAA9HgzD5psJwW.htm](pathfinder-bestiary-2-items/TdOAA9HgzD5psJwW.htm)|Pull Filament|Tirar Filamento|modificada|
|[teonM0iI5aOvey0N.htm](pathfinder-bestiary-2-items/teonM0iI5aOvey0N.htm)|Fast Healing 1|Curación rápida 1|modificada|
|[TeoUKrxXSlxbQGds.htm](pathfinder-bestiary-2-items/TeoUKrxXSlxbQGds.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[tF3KNEXj3uSK3cil.htm](pathfinder-bestiary-2-items/tF3KNEXj3uSK3cil.htm)|Throw Rock|Arrojar roca|modificada|
|[TF4ZUPn2jeFMb9Fz.htm](pathfinder-bestiary-2-items/TF4ZUPn2jeFMb9Fz.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[TfHuj8LvtfwsXfXQ.htm](pathfinder-bestiary-2-items/TfHuj8LvtfwsXfXQ.htm)|Tentacle Arm|Brazo Tentáculo|modificada|
|[TfK9D77zOoU9jjxj.htm](pathfinder-bestiary-2-items/TfK9D77zOoU9jjxj.htm)|Grab|Agarrado|modificada|
|[tFkVmhokDW1zCny4.htm](pathfinder-bestiary-2-items/tFkVmhokDW1zCny4.htm)|Wing|Ala|modificada|
|[TFsDNtIHhwteQUz9.htm](pathfinder-bestiary-2-items/TFsDNtIHhwteQUz9.htm)|Web Sense|Web Sense|modificada|
|[tG4nT3YD8KPOsT5n.htm](pathfinder-bestiary-2-items/tG4nT3YD8KPOsT5n.htm)|Grab|Agarrado|modificada|
|[tH3f4GsDerHRNPik.htm](pathfinder-bestiary-2-items/tH3f4GsDerHRNPik.htm)|Change Shape|Change Shape|modificada|
|[THkJV9WgvlzdR1Ll.htm](pathfinder-bestiary-2-items/THkJV9WgvlzdR1Ll.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[THUDJyCpb4at07bz.htm](pathfinder-bestiary-2-items/THUDJyCpb4at07bz.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[tIiXnBCscCuQi4WY.htm](pathfinder-bestiary-2-items/tIiXnBCscCuQi4WY.htm)|Favored Host|Favored Host|modificada|
|[tikGFRXVhGNo3ahM.htm](pathfinder-bestiary-2-items/tikGFRXVhGNo3ahM.htm)|Hammer|Martillo|modificada|
|[tIQTZDimC6ewCWZ7.htm](pathfinder-bestiary-2-items/tIQTZDimC6ewCWZ7.htm)|Jaws|Fauces|modificada|
|[tisbaQ4bvix6pdXR.htm](pathfinder-bestiary-2-items/tisbaQ4bvix6pdXR.htm)|Tail|Tail|modificada|
|[tIvtO7TbZgSNlr3F.htm](pathfinder-bestiary-2-items/tIvtO7TbZgSNlr3F.htm)|Constant Spells|Constant Spells|modificada|
|[tjcUq4Pdk5hq6KqX.htm](pathfinder-bestiary-2-items/tjcUq4Pdk5hq6KqX.htm)|Boneshatter|Huesos frágiles|modificada|
|[tjNFJu8XOANHpzYB.htm](pathfinder-bestiary-2-items/tjNFJu8XOANHpzYB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[tjoCXlFjIMoeCEAO.htm](pathfinder-bestiary-2-items/tjoCXlFjIMoeCEAO.htm)|Improved Grab|Agarrado mejorado|modificada|
|[tjQCRx8UN4LQPnwJ.htm](pathfinder-bestiary-2-items/tjQCRx8UN4LQPnwJ.htm)|Witchflame Kindling|Le de llama de bruja|modificada|
|[tjv4Luss5zUtzkrn.htm](pathfinder-bestiary-2-items/tjv4Luss5zUtzkrn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Tjvf8RD2s6FEQelo.htm](pathfinder-bestiary-2-items/Tjvf8RD2s6FEQelo.htm)|Rock|Roca|modificada|
|[tKbplFnGrHjbOkKF.htm](pathfinder-bestiary-2-items/tKbplFnGrHjbOkKF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TKjiZlqXKYAz5MTX.htm](pathfinder-bestiary-2-items/TKjiZlqXKYAz5MTX.htm)|All-Around Vision|All-Around Vision|modificada|
|[tkqzuUsreLrNt0sh.htm](pathfinder-bestiary-2-items/tkqzuUsreLrNt0sh.htm)|Aquatic Echolocation|Ecolocalización acuática|modificada|
|[tlcUEtC6qZlc69zv.htm](pathfinder-bestiary-2-items/tlcUEtC6qZlc69zv.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[TlRvAbAXlF8M2LXl.htm](pathfinder-bestiary-2-items/TlRvAbAXlF8M2LXl.htm)|Jaws|Fauces|modificada|
|[TM2S9CZ5bTyxKkXp.htm](pathfinder-bestiary-2-items/TM2S9CZ5bTyxKkXp.htm)|Unnerving Gaze|Unnerving Gaze|modificada|
|[tmi63gM84pc6s5qK.htm](pathfinder-bestiary-2-items/tmi63gM84pc6s5qK.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[TNAO4qk6h9SrlrOP.htm](pathfinder-bestiary-2-items/TNAO4qk6h9SrlrOP.htm)|Sever Fate|Sever Fate|modificada|
|[TnT0MH1u6Ixly1UZ.htm](pathfinder-bestiary-2-items/TnT0MH1u6Ixly1UZ.htm)|Pseudopod|Pseudópodo|modificada|
|[tnyUum6cGLUvry3c.htm](pathfinder-bestiary-2-items/tnyUum6cGLUvry3c.htm)|Jaws|Fauces|modificada|
|[tO053EeFkQRNiOBR.htm](pathfinder-bestiary-2-items/tO053EeFkQRNiOBR.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[tO16N6Get5HonzDM.htm](pathfinder-bestiary-2-items/tO16N6Get5HonzDM.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[tO90cMW3Jx22uW0F.htm](pathfinder-bestiary-2-items/tO90cMW3Jx22uW0F.htm)|Arm|Brazo|modificada|
|[tOMw33x9d2qXTKwi.htm](pathfinder-bestiary-2-items/tOMw33x9d2qXTKwi.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[tONGWAraS1nSA49Y.htm](pathfinder-bestiary-2-items/tONGWAraS1nSA49Y.htm)|Wicked Bite|Muerdemuerde|modificada|
|[TOzSzGQn4MrKg3wy.htm](pathfinder-bestiary-2-items/TOzSzGQn4MrKg3wy.htm)|Igniting Assault|Encender Asalto|modificada|
|[tPa52hjkpaRccFn3.htm](pathfinder-bestiary-2-items/tPa52hjkpaRccFn3.htm)|Tentacle|Tentáculo|modificada|
|[TpfBgK9H4iQNfMgS.htm](pathfinder-bestiary-2-items/TpfBgK9H4iQNfMgS.htm)|Sting|Sting|modificada|
|[TPKGkvgCAQz8TKeC.htm](pathfinder-bestiary-2-items/TPKGkvgCAQz8TKeC.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tpQDVH3NwAokh2NC.htm](pathfinder-bestiary-2-items/tpQDVH3NwAokh2NC.htm)|Breath Weapon|Breath Weapon|modificada|
|[tqSmdxFVDmY2CLns.htm](pathfinder-bestiary-2-items/tqSmdxFVDmY2CLns.htm)|Heavy Aura|Heavy Aura|modificada|
|[TQzV6HYAwG8ru5Jc.htm](pathfinder-bestiary-2-items/TQzV6HYAwG8ru5Jc.htm)|Serpentfolk Venom|Serpentfolk Venom|modificada|
|[tRLWCHj0DJ8Jb78B.htm](pathfinder-bestiary-2-items/tRLWCHj0DJ8Jb78B.htm)|Focus Gaze|Centrar mirada|modificada|
|[tssacopT88cz2z8v.htm](pathfinder-bestiary-2-items/tssacopT88cz2z8v.htm)|Tail|Tail|modificada|
|[tTPvWYYvp1PgybyK.htm](pathfinder-bestiary-2-items/tTPvWYYvp1PgybyK.htm)|Swift Tracker|Rastreador rápido|modificada|
|[TtqhvUO0vvhjbIFp.htm](pathfinder-bestiary-2-items/TtqhvUO0vvhjbIFp.htm)|Exorcism Vulnerability|Vulnerabilidad Exorcismo|modificada|
|[TUaSPYJP2yuCkKh9.htm](pathfinder-bestiary-2-items/TUaSPYJP2yuCkKh9.htm)|Wing Deflection|Desvío con el ala|modificada|
|[tuQ8FrXL4iChAQA1.htm](pathfinder-bestiary-2-items/tuQ8FrXL4iChAQA1.htm)|Sunlight Petrification|Petrificación a la luz del sol|modificada|
|[tV582k6fcdjhRPvI.htm](pathfinder-bestiary-2-items/tV582k6fcdjhRPvI.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[tVBujCEfyKPKjA2Q.htm](pathfinder-bestiary-2-items/tVBujCEfyKPKjA2Q.htm)|Flailing Tentacles|Flailing Tentacles|modificada|
|[TvDY3FqPJsHE5Bz7.htm](pathfinder-bestiary-2-items/TvDY3FqPJsHE5Bz7.htm)|Stinger|Aguijón|modificada|
|[TVOxysBVf9C1u5wF.htm](pathfinder-bestiary-2-items/TVOxysBVf9C1u5wF.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[TW4FUgjb7YgD89jt.htm](pathfinder-bestiary-2-items/TW4FUgjb7YgD89jt.htm)|Ancestral Guardian|Guardián Ancestral|modificada|
|[TWEbrceKTc0P1GrD.htm](pathfinder-bestiary-2-items/TWEbrceKTc0P1GrD.htm)|Ember Ball|Bola de Ascuas|modificada|
|[twuwqjXSZmaqi54S.htm](pathfinder-bestiary-2-items/twuwqjXSZmaqi54S.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[TxgMLxy2ap1ZIMWz.htm](pathfinder-bestiary-2-items/TxgMLxy2ap1ZIMWz.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tXoIGwvSGdRcv6jU.htm](pathfinder-bestiary-2-items/tXoIGwvSGdRcv6jU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[TxRwH6kafpwqJtmQ.htm](pathfinder-bestiary-2-items/TxRwH6kafpwqJtmQ.htm)|Truespeech|Truespeech|modificada|
|[tygj3hMG88FeH6W1.htm](pathfinder-bestiary-2-items/tygj3hMG88FeH6W1.htm)|Draconic Resistance|Resistencia dracónico|modificada|
|[TYoj6whlYb3mxSVJ.htm](pathfinder-bestiary-2-items/TYoj6whlYb3mxSVJ.htm)|Constant Spells|Constant Spells|modificada|
|[tZ28zS8D4Ibrkmjl.htm](pathfinder-bestiary-2-items/tZ28zS8D4Ibrkmjl.htm)|Thunderbolt|Rayo|modificada|
|[TZC0DNY97EGRDFbD.htm](pathfinder-bestiary-2-items/TZC0DNY97EGRDFbD.htm)|Horns|Cuernos|modificada|
|[tZhV19HIQBeAjdki.htm](pathfinder-bestiary-2-items/tZhV19HIQBeAjdki.htm)|Jaws|Fauces|modificada|
|[TzpdMinu4WoEyAY9.htm](pathfinder-bestiary-2-items/TzpdMinu4WoEyAY9.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[TzQpE2J6oD8ijpuC.htm](pathfinder-bestiary-2-items/TzQpE2J6oD8ijpuC.htm)|Splinter|Splinter|modificada|
|[u12odfdEyxk5GPY5.htm](pathfinder-bestiary-2-items/u12odfdEyxk5GPY5.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[u1o4MqU6HFWR8rVh.htm](pathfinder-bestiary-2-items/u1o4MqU6HFWR8rVh.htm)|Electrolocation|Electrolocation|modificada|
|[U1rCcNDpkbvZuuCb.htm](pathfinder-bestiary-2-items/U1rCcNDpkbvZuuCb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[U291oOsn8aI5u9Za.htm](pathfinder-bestiary-2-items/U291oOsn8aI5u9Za.htm)|Darkvision|Visión en la oscuridad|modificada|
|[u3s5y4pE6L6HfEAq.htm](pathfinder-bestiary-2-items/u3s5y4pE6L6HfEAq.htm)|Malleability|Maleabilidad|modificada|
|[u4BcgBmwi4zY27Ei.htm](pathfinder-bestiary-2-items/u4BcgBmwi4zY27Ei.htm)|Clacking Exoskeleton|Clacking Exoskeleton|modificada|
|[U4uu4gkB12yKgjWl.htm](pathfinder-bestiary-2-items/U4uu4gkB12yKgjWl.htm)|Jaws|Fauces|modificada|
|[u58XzFLTCWAEQCJN.htm](pathfinder-bestiary-2-items/u58XzFLTCWAEQCJN.htm)|Writhing Arms|Brazos retorcidos|modificada|
|[u5ajL2rEGMJeJmYt.htm](pathfinder-bestiary-2-items/u5ajL2rEGMJeJmYt.htm)|Telepathy|Telepatía|modificada|
|[U5OMtZB6VwyAnL04.htm](pathfinder-bestiary-2-items/U5OMtZB6VwyAnL04.htm)|Constant Spells|Constant Spells|modificada|
|[U64zKLjgQG2MzhAS.htm](pathfinder-bestiary-2-items/U64zKLjgQG2MzhAS.htm)|Wind's Guidance|La orientación divina del viento|modificada|
|[U69lb7BfCPLfTsfG.htm](pathfinder-bestiary-2-items/U69lb7BfCPLfTsfG.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[u6jbIfe9u8XJ6Pe0.htm](pathfinder-bestiary-2-items/u6jbIfe9u8XJ6Pe0.htm)|Claw|Garra|modificada|
|[u6kTO6HI4KtrGtds.htm](pathfinder-bestiary-2-items/u6kTO6HI4KtrGtds.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[U6lHO1793L8PN4Hc.htm](pathfinder-bestiary-2-items/U6lHO1793L8PN4Hc.htm)|Grab|Agarrado|modificada|
|[U6mlSBSkzcgtivnv.htm](pathfinder-bestiary-2-items/U6mlSBSkzcgtivnv.htm)|Scent|Scent|modificada|
|[u6T8tsZkOcFjvrzG.htm](pathfinder-bestiary-2-items/u6T8tsZkOcFjvrzG.htm)|Negative Healing|Curación negativa|modificada|
|[u6tuxFGclP3Cf3u2.htm](pathfinder-bestiary-2-items/u6tuxFGclP3Cf3u2.htm)|Otherworldly Mind|Mente de otro mundo|modificada|
|[u7R9PDTQDTEJeQAX.htm](pathfinder-bestiary-2-items/u7R9PDTQDTEJeQAX.htm)|Keen Hearing|Afilada Audición|modificada|
|[U7WoIw4znbFwFj7t.htm](pathfinder-bestiary-2-items/U7WoIw4znbFwFj7t.htm)|Scent|Scent|modificada|
|[u9uoDYlFsBGEdg0u.htm](pathfinder-bestiary-2-items/u9uoDYlFsBGEdg0u.htm)|Sleep Gas|Gas somnífero|modificada|
|[U9ZtZBoz8oQqwrWa.htm](pathfinder-bestiary-2-items/U9ZtZBoz8oQqwrWa.htm)|Regeneration 15 (Deactivated by Acid or Cold)|Regeneración 15 (Desactivado por Ácido o Frío)|modificada|
|[uAKDE9WbARcraQJx.htm](pathfinder-bestiary-2-items/uAKDE9WbARcraQJx.htm)|Clobbering Charge|Clobbering Charge|modificada|
|[uAlQecVurdHtCKds.htm](pathfinder-bestiary-2-items/uAlQecVurdHtCKds.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[UatC9BxVJopPgwJd.htm](pathfinder-bestiary-2-items/UatC9BxVJopPgwJd.htm)|Rotting Curse|Rotting Curse|modificada|
|[UbbiLTnNP4WGrar5.htm](pathfinder-bestiary-2-items/UbbiLTnNP4WGrar5.htm)|Mold Mulch|Mold Mulch|modificada|
|[ubHtu9kCddCNuEjt.htm](pathfinder-bestiary-2-items/ubHtu9kCddCNuEjt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ubj0ypG0da0nRirb.htm](pathfinder-bestiary-2-items/ubj0ypG0da0nRirb.htm)|Fade into the Light|Fade into the Light|modificada|
|[ubttzSAG6qyejH25.htm](pathfinder-bestiary-2-items/ubttzSAG6qyejH25.htm)|Grab|Agarrado|modificada|
|[UcQu3REAJGDNFfE0.htm](pathfinder-bestiary-2-items/UcQu3REAJGDNFfE0.htm)|Captivating Pollen|Polen cautivador|modificada|
|[UcRrxW2Ay0vhDBrn.htm](pathfinder-bestiary-2-items/UcRrxW2Ay0vhDBrn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uD7Sv3Sf3fTDECXG.htm](pathfinder-bestiary-2-items/uD7Sv3Sf3fTDECXG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uDrkrfwjoPByW5Zs.htm](pathfinder-bestiary-2-items/uDrkrfwjoPByW5Zs.htm)|Warpwaves|Warpwaves|modificada|
|[uDxnASCZGKnXfoQw.htm](pathfinder-bestiary-2-items/uDxnASCZGKnXfoQw.htm)|Trample|Trample|modificada|
|[UE4WlNSLrotRauZG.htm](pathfinder-bestiary-2-items/UE4WlNSLrotRauZG.htm)|Lightning-Struck Curse|Maldición del Golpe de Rayo|modificada|
|[UEAEQewstiRA9wbc.htm](pathfinder-bestiary-2-items/UEAEQewstiRA9wbc.htm)|Paralytic Perfection|Paralytic Perfection|modificada|
|[uGdTa0kjSsx6O3yO.htm](pathfinder-bestiary-2-items/uGdTa0kjSsx6O3yO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[ugIVlLPaqyvekXgN.htm](pathfinder-bestiary-2-items/ugIVlLPaqyvekXgN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UGoT9Ax8vT4G0yrI.htm](pathfinder-bestiary-2-items/UGoT9Ax8vT4G0yrI.htm)|Dagger|Daga|modificada|
|[UgqU2m5UQKRKewoU.htm](pathfinder-bestiary-2-items/UgqU2m5UQKRKewoU.htm)|Osyluth Venom|Osyluth Venom|modificada|
|[uh97rOTxqm9aQTwC.htm](pathfinder-bestiary-2-items/uh97rOTxqm9aQTwC.htm)|Consume Berries|Consume Bayas|modificada|
|[UHFNjGmcb7jCzvBE.htm](pathfinder-bestiary-2-items/UHFNjGmcb7jCzvBE.htm)|Attach|Aferrarse|modificada|
|[UhnqMxX22JlPHncP.htm](pathfinder-bestiary-2-items/UhnqMxX22JlPHncP.htm)|Tentacle|Tentáculo|modificada|
|[uhW4OwaKUkyMz6W3.htm](pathfinder-bestiary-2-items/uhW4OwaKUkyMz6W3.htm)|Capsize|Volcar|modificada|
|[UIgdlwmkkFjjq6Kl.htm](pathfinder-bestiary-2-items/UIgdlwmkkFjjq6Kl.htm)|Unsettled Aura|Aura inquieta|modificada|
|[uIPRSgEue1olkirr.htm](pathfinder-bestiary-2-items/uIPRSgEue1olkirr.htm)|Constant Spells|Constant Spells|modificada|
|[Ujc2l5skhi8bH1Ue.htm](pathfinder-bestiary-2-items/Ujc2l5skhi8bH1Ue.htm)|Holy Armaments|Armamento Sagrada|modificada|
|[UjEJBgE9VWnbTMRQ.htm](pathfinder-bestiary-2-items/UjEJBgE9VWnbTMRQ.htm)|Negative Healing|Curación negativa|modificada|
|[ujOn2cOoDTqdrh70.htm](pathfinder-bestiary-2-items/ujOn2cOoDTqdrh70.htm)|Claw|Garra|modificada|
|[ujQKL0Ru853GDYUK.htm](pathfinder-bestiary-2-items/ujQKL0Ru853GDYUK.htm)|Grab|Agarrado|modificada|
|[UjQN53iNiipJJkHh.htm](pathfinder-bestiary-2-items/UjQN53iNiipJJkHh.htm)|Branch|Rama|modificada|
|[uJuFK46gaxSPryGB.htm](pathfinder-bestiary-2-items/uJuFK46gaxSPryGB.htm)|Brine Spit|Salmuera|modificada|
|[UK2ayZO6tmbwgHBL.htm](pathfinder-bestiary-2-items/UK2ayZO6tmbwgHBL.htm)|Jaws|Fauces|modificada|
|[uKp0N4KDIGVFsJ1T.htm](pathfinder-bestiary-2-items/uKp0N4KDIGVFsJ1T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ULCc7i4GlRFlcgmM.htm](pathfinder-bestiary-2-items/ULCc7i4GlRFlcgmM.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ulrrbkt4l6SBi1z8.htm](pathfinder-bestiary-2-items/ulrrbkt4l6SBi1z8.htm)|Painsight|Painsight|modificada|
|[uLuSAexGwbZSdlkF.htm](pathfinder-bestiary-2-items/uLuSAexGwbZSdlkF.htm)|Black Flame Knife|Cuchillo Negro Flamígera|modificada|
|[uLVKT1uotgIuFDA2.htm](pathfinder-bestiary-2-items/uLVKT1uotgIuFDA2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[um06dCu0E8W9LRDd.htm](pathfinder-bestiary-2-items/um06dCu0E8W9LRDd.htm)|Entropy Sense (Imprecise) 30 feet|Sentido de Entropía (Impreciso) 30 pies|modificada|
|[umE4aR6ccDxLqz3l.htm](pathfinder-bestiary-2-items/umE4aR6ccDxLqz3l.htm)|Hand Crossbow (Hunting Spider Venom)|Ballesta de mano (Veneno de ara cazadora).|modificada|
|[UmF9ATrgifYyt9He.htm](pathfinder-bestiary-2-items/UmF9ATrgifYyt9He.htm)|Pounce|Abalanzarse|modificada|
|[uMPmwIYK4s395qvB.htm](pathfinder-bestiary-2-items/uMPmwIYK4s395qvB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uMr0g5ycTJTsxsHh.htm](pathfinder-bestiary-2-items/uMr0g5ycTJTsxsHh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uMVIaXXt18rDrOPn.htm](pathfinder-bestiary-2-items/uMVIaXXt18rDrOPn.htm)|Ravenous Breath Weapon|Arma de aliento famélico|modificada|
|[UN8cAG3SmdTtuDWe.htm](pathfinder-bestiary-2-items/UN8cAG3SmdTtuDWe.htm)|Smoke Slash|Tajo de Humo|modificada|
|[uNH8ZKqCgsh6qYis.htm](pathfinder-bestiary-2-items/uNH8ZKqCgsh6qYis.htm)|Bay|Bahía|modificada|
|[uot20BQOy52S73qO.htm](pathfinder-bestiary-2-items/uot20BQOy52S73qO.htm)|Steal Breath|Sustraer Aliento|modificada|
|[Uoxf8sSuS6pG7QQF.htm](pathfinder-bestiary-2-items/Uoxf8sSuS6pG7QQF.htm)|Pod Spawn|Pod Spawn|modificada|
|[up0HVVMUk86V0DdT.htm](pathfinder-bestiary-2-items/up0HVVMUk86V0DdT.htm)|Burning Rush|Embestida Ardiente|modificada|
|[Up9zgawdSde0SLPP.htm](pathfinder-bestiary-2-items/Up9zgawdSde0SLPP.htm)|All-Around Vision|All-Around Vision|modificada|
|[UpD8OqCE3oJ41eCz.htm](pathfinder-bestiary-2-items/UpD8OqCE3oJ41eCz.htm)|Constant Spells|Constant Spells|modificada|
|[UPUXBFdc12OlpoBV.htm](pathfinder-bestiary-2-items/UPUXBFdc12OlpoBV.htm)|Gaff|Gaff|modificada|
|[uQ1bRoDBDEldN87d.htm](pathfinder-bestiary-2-items/uQ1bRoDBDEldN87d.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uQDUAoudAP9pgxja.htm](pathfinder-bestiary-2-items/uQDUAoudAP9pgxja.htm)|Soul Crush|Soul Crush|modificada|
|[uQFiXxjHXOCxLHe8.htm](pathfinder-bestiary-2-items/uQFiXxjHXOCxLHe8.htm)|Vulnerability to Supernatural Darkness|Vulnerabilidad a la oscuridad sobrenatural.|modificada|
|[UQft7JhW6zhtWCy2.htm](pathfinder-bestiary-2-items/UQft7JhW6zhtWCy2.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[Ur31aU2yqO6lWSV2.htm](pathfinder-bestiary-2-items/Ur31aU2yqO6lWSV2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[uR5sStuJbBbR9XK6.htm](pathfinder-bestiary-2-items/uR5sStuJbBbR9XK6.htm)|Hatchet|Hacha|modificada|
|[urEbuoDUEyOj6f0A.htm](pathfinder-bestiary-2-items/urEbuoDUEyOj6f0A.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[URm0Bj51PHUjgnXS.htm](pathfinder-bestiary-2-items/URm0Bj51PHUjgnXS.htm)|Breath Weapon|Breath Weapon|modificada|
|[Us7BcRckPUy5WuaZ.htm](pathfinder-bestiary-2-items/Us7BcRckPUy5WuaZ.htm)|Swallow Whole|Engullir Todo|modificada|
|[UScYAVgddobTyqOw.htm](pathfinder-bestiary-2-items/UScYAVgddobTyqOw.htm)|Fist|Puño|modificada|
|[usIKpszaYrCWfN8r.htm](pathfinder-bestiary-2-items/usIKpszaYrCWfN8r.htm)|Shadow Breath|Aliento de Sombra|modificada|
|[UTaJ0rJXzssLeINs.htm](pathfinder-bestiary-2-items/UTaJ0rJXzssLeINs.htm)|Motion Sense|Sentido del movimiento|modificada|
|[uTYt777rmxzqgumS.htm](pathfinder-bestiary-2-items/uTYt777rmxzqgumS.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[UuDDHSI5hH9lHpZv.htm](pathfinder-bestiary-2-items/UuDDHSI5hH9lHpZv.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[uuI2Msz0ObGGd1kj.htm](pathfinder-bestiary-2-items/uuI2Msz0ObGGd1kj.htm)|Scent|Scent|modificada|
|[Uus1jecX7kww8spI.htm](pathfinder-bestiary-2-items/Uus1jecX7kww8spI.htm)|Fast Healing 2|Curación rápida 2|modificada|
|[UuUlz5A5IwoGhe6n.htm](pathfinder-bestiary-2-items/UuUlz5A5IwoGhe6n.htm)|Frightful Strike|Golpe Espantoso|modificada|
|[UUVUKTE7xQsPxDNy.htm](pathfinder-bestiary-2-items/UUVUKTE7xQsPxDNy.htm)|Bone Shard|Bone Shard|modificada|
|[UuxtWMTDYGzdEzIA.htm](pathfinder-bestiary-2-items/UuxtWMTDYGzdEzIA.htm)|Glowing Touch|Glowing Touch|modificada|
|[uvi6KWWlZsgw6kYw.htm](pathfinder-bestiary-2-items/uvi6KWWlZsgw6kYw.htm)|Sting|Sting|modificada|
|[UvIa41hvZRkj3zM0.htm](pathfinder-bestiary-2-items/UvIa41hvZRkj3zM0.htm)|Tremorsense|Tremorsense|modificada|
|[UVvsMOpzGvjOBeG1.htm](pathfinder-bestiary-2-items/UVvsMOpzGvjOBeG1.htm)|Protean Anatomy 15|Protean Anatomy 15|modificada|
|[uW30KxL4NUlDPAi5.htm](pathfinder-bestiary-2-items/uW30KxL4NUlDPAi5.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[uXmhD5XQjEMFuZSJ.htm](pathfinder-bestiary-2-items/uXmhD5XQjEMFuZSJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uY2J1JrUYcVbFp2j.htm](pathfinder-bestiary-2-items/uY2J1JrUYcVbFp2j.htm)|Stunning Display|Pantalla impresionante|modificada|
|[UYeaGbpn427tN1hJ.htm](pathfinder-bestiary-2-items/UYeaGbpn427tN1hJ.htm)|Oar|Remo|modificada|
|[uYHwkATnIk2mUvIu.htm](pathfinder-bestiary-2-items/uYHwkATnIk2mUvIu.htm)|Tentacle|Tentáculo|modificada|
|[uYqNB5URDvfKjzHe.htm](pathfinder-bestiary-2-items/uYqNB5URDvfKjzHe.htm)|Morningstar|Morningstar|modificada|
|[uyXWXmerHoFxX1jU.htm](pathfinder-bestiary-2-items/uyXWXmerHoFxX1jU.htm)|Boiling Rain|Lluvia Hirviente|modificada|
|[uzaPYD2SNbrlxuM9.htm](pathfinder-bestiary-2-items/uzaPYD2SNbrlxuM9.htm)|Jaws|Fauces|modificada|
|[uzav7YxjDugksaRl.htm](pathfinder-bestiary-2-items/uzav7YxjDugksaRl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[UZc2Evb7hxJkfGtx.htm](pathfinder-bestiary-2-items/UZc2Evb7hxJkfGtx.htm)|Breath Weapon|Breath Weapon|modificada|
|[uze31xboBPgWCYgP.htm](pathfinder-bestiary-2-items/uze31xboBPgWCYgP.htm)|Shortsword|Espada corta|modificada|
|[uzTaJDrIHuQpGZCH.htm](pathfinder-bestiary-2-items/uzTaJDrIHuQpGZCH.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[V01q458qlTH6dA7i.htm](pathfinder-bestiary-2-items/V01q458qlTH6dA7i.htm)|Darkvision|Visión en la oscuridad|modificada|
|[V0hJjrywdDfeOIZf.htm](pathfinder-bestiary-2-items/V0hJjrywdDfeOIZf.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[v0kg2s0AeP7Pv7zn.htm](pathfinder-bestiary-2-items/v0kg2s0AeP7Pv7zn.htm)|Ravenous Jaws|Mandíbulas voraces|modificada|
|[v1Nae4WDwZunhsHt.htm](pathfinder-bestiary-2-items/v1Nae4WDwZunhsHt.htm)|Regeneration 15 (Deactivated by Chaotic)|Regeneración 15 (Desactivado por Caótico).|modificada|
|[v2by5D3cMGyVu78a.htm](pathfinder-bestiary-2-items/v2by5D3cMGyVu78a.htm)|Ritual Gate|Puerta Ritual|modificada|
|[V4aoUtsug8TREjXX.htm](pathfinder-bestiary-2-items/V4aoUtsug8TREjXX.htm)|Swarm Spit|Swarm Spit|modificada|
|[V4PELj4i5rSeImC7.htm](pathfinder-bestiary-2-items/V4PELj4i5rSeImC7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[V4TPpwFX1g237Ekq.htm](pathfinder-bestiary-2-items/V4TPpwFX1g237Ekq.htm)|Claw|Garra|modificada|
|[V4WvWjukxtroI8Xs.htm](pathfinder-bestiary-2-items/V4WvWjukxtroI8Xs.htm)|Swallow Whole|Engullir Todo|modificada|
|[V5AYyUYH7UjuXk8T.htm](pathfinder-bestiary-2-items/V5AYyUYH7UjuXk8T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[V5c6DTpIF11FCsWw.htm](pathfinder-bestiary-2-items/V5c6DTpIF11FCsWw.htm)|Drain Life|Drenar Vida|modificada|
|[V5mIlHkiRCxhndVN.htm](pathfinder-bestiary-2-items/V5mIlHkiRCxhndVN.htm)|Feeding Frenzy|Frenesí alimenticio|modificada|
|[V5Py4m6Gm8m9Z8I2.htm](pathfinder-bestiary-2-items/V5Py4m6Gm8m9Z8I2.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[V5YgaBwWcZPdAj1X.htm](pathfinder-bestiary-2-items/V5YgaBwWcZPdAj1X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[v65HAbd7O3gZv2VP.htm](pathfinder-bestiary-2-items/v65HAbd7O3gZv2VP.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[v6dHZF6ZJriIUH9V.htm](pathfinder-bestiary-2-items/v6dHZF6ZJriIUH9V.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[v6WETOqHL0U4nwur.htm](pathfinder-bestiary-2-items/v6WETOqHL0U4nwur.htm)|Telepathy|Telepatía|modificada|
|[V7yL5WF6xEMuHMIf.htm](pathfinder-bestiary-2-items/V7yL5WF6xEMuHMIf.htm)|Grab|Agarrado|modificada|
|[v8A5DhitrEJIAAL1.htm](pathfinder-bestiary-2-items/v8A5DhitrEJIAAL1.htm)|Consume Soul|Consumir alma|modificada|
|[V8J7D9aK5N4MpaBI.htm](pathfinder-bestiary-2-items/V8J7D9aK5N4MpaBI.htm)|Lifesense|Lifesense|modificada|
|[V90pHqyo14mQzVuc.htm](pathfinder-bestiary-2-items/V90pHqyo14mQzVuc.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[V9cC85qOc8a3vbBc.htm](pathfinder-bestiary-2-items/V9cC85qOc8a3vbBc.htm)|Abyssal Rot|Abyssal Rot|modificada|
|[V9Ei1dwq0RUilrjT.htm](pathfinder-bestiary-2-items/V9Ei1dwq0RUilrjT.htm)|Telepathy 100 feet (With Spectral Thralls Only)|Telepatía 100 pies (Solo con Thralls espectrales).|modificada|
|[V9VYMbFdFCqt2IqA.htm](pathfinder-bestiary-2-items/V9VYMbFdFCqt2IqA.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[vApfqAgqE1h1XDQe.htm](pathfinder-bestiary-2-items/vApfqAgqE1h1XDQe.htm)|Motion Sense|Sentido del movimiento|modificada|
|[vAqYhN2FTor4CPFU.htm](pathfinder-bestiary-2-items/vAqYhN2FTor4CPFU.htm)|Tendril|Tendril|modificada|
|[VaxEKl3gTn1iAnvf.htm](pathfinder-bestiary-2-items/VaxEKl3gTn1iAnvf.htm)|Claw|Garra|modificada|
|[vb0fFWLf39zgAI4N.htm](pathfinder-bestiary-2-items/vb0fFWLf39zgAI4N.htm)|Spear|Lanza|modificada|
|[vBhPWXgs8q8icc85.htm](pathfinder-bestiary-2-items/vBhPWXgs8q8icc85.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[vbL3yHO5ysgCJOcD.htm](pathfinder-bestiary-2-items/vbL3yHO5ysgCJOcD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vC8tzGqfa79am1Xm.htm](pathfinder-bestiary-2-items/vC8tzGqfa79am1Xm.htm)|Tusk|Tusk|modificada|
|[vCEb36Sv06OtmLaz.htm](pathfinder-bestiary-2-items/vCEb36Sv06OtmLaz.htm)|Rhinoceros Charge|Carga Rinoceronte|modificada|
|[vCp6u7IXfs7VJRFF.htm](pathfinder-bestiary-2-items/vCp6u7IXfs7VJRFF.htm)|Scimitar|Cimitarra|modificada|
|[VD3WAa6HAnP9FYx6.htm](pathfinder-bestiary-2-items/VD3WAa6HAnP9FYx6.htm)|Ferocity|Ferocidad|modificada|
|[vd8w1UZaLid8WPrA.htm](pathfinder-bestiary-2-items/vd8w1UZaLid8WPrA.htm)|Claw|Garra|modificada|
|[vda7kHsnuwSQWtXd.htm](pathfinder-bestiary-2-items/vda7kHsnuwSQWtXd.htm)|Grab|Agarrado|modificada|
|[VDa8nmlZXUFf1Y7z.htm](pathfinder-bestiary-2-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|Fauces|modificada|
|[VdCpMDU3TyIElL0X.htm](pathfinder-bestiary-2-items/VdCpMDU3TyIElL0X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VdCXQ44TAdADVOEd.htm](pathfinder-bestiary-2-items/VdCXQ44TAdADVOEd.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[VDFewubyHTGzI0bL.htm](pathfinder-bestiary-2-items/VDFewubyHTGzI0bL.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[VDW4W2k1N88INhBp.htm](pathfinder-bestiary-2-items/VDW4W2k1N88INhBp.htm)|Breath Weapon|Breath Weapon|modificada|
|[VDwn97NFRX24iTjv.htm](pathfinder-bestiary-2-items/VDwn97NFRX24iTjv.htm)|Claw|Garra|modificada|
|[vEMwAfv76TagQTy9.htm](pathfinder-bestiary-2-items/vEMwAfv76TagQTy9.htm)|Hurl Net|Lanzar red|modificada|
|[Vf0GMSeZbKtccykV.htm](pathfinder-bestiary-2-items/Vf0GMSeZbKtccykV.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[VFprYdFvs654DtJ6.htm](pathfinder-bestiary-2-items/VFprYdFvs654DtJ6.htm)|Grab|Agarrado|modificada|
|[VGBhMtipbBzkkHbO.htm](pathfinder-bestiary-2-items/VGBhMtipbBzkkHbO.htm)|Lifesense|Lifesense|modificada|
|[VHBwi6X1gEpQ8YsT.htm](pathfinder-bestiary-2-items/VHBwi6X1gEpQ8YsT.htm)|Flaming Bastard Sword|Espada Bastarda Flamígera|modificada|
|[VhRB4rDZ9bnYht7Z.htm](pathfinder-bestiary-2-items/VhRB4rDZ9bnYht7Z.htm)|Constrict|Restringir|modificada|
|[vHtf6k3iRMSkYTZd.htm](pathfinder-bestiary-2-items/vHtf6k3iRMSkYTZd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vID81IzA60CV6yD2.htm](pathfinder-bestiary-2-items/vID81IzA60CV6yD2.htm)|Frightful Presence|Frightful Presence|modificada|
|[vIzThDo5KtpM1fnu.htm](pathfinder-bestiary-2-items/vIzThDo5KtpM1fnu.htm)|Fangs|Colmillos|modificada|
|[vKcQxEXLQx45iyeq.htm](pathfinder-bestiary-2-items/vKcQxEXLQx45iyeq.htm)|Grimstalker Sap|Grimstalker Sap|modificada|
|[VKqHnTDh4E88cAGN.htm](pathfinder-bestiary-2-items/VKqHnTDh4E88cAGN.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[VKysBYdqHBzZ3aOg.htm](pathfinder-bestiary-2-items/VKysBYdqHBzZ3aOg.htm)|Breath Weapon|Breath Weapon|modificada|
|[VL2af2GD7VsPiI1u.htm](pathfinder-bestiary-2-items/VL2af2GD7VsPiI1u.htm)|Jaws|Fauces|modificada|
|[vl5TAyZBV7YfjwyO.htm](pathfinder-bestiary-2-items/vl5TAyZBV7YfjwyO.htm)|Scent|Scent|modificada|
|[VLQ2Vi92N8KogGpN.htm](pathfinder-bestiary-2-items/VLQ2Vi92N8KogGpN.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[vLuAkberNsYeDSD0.htm](pathfinder-bestiary-2-items/vLuAkberNsYeDSD0.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[VLwTahhYo1OObMUv.htm](pathfinder-bestiary-2-items/VLwTahhYo1OObMUv.htm)|Greater Constrict|Mayor Restricción|modificada|
|[vLZK0xL9O8b27GxA.htm](pathfinder-bestiary-2-items/vLZK0xL9O8b27GxA.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[Vm3TYdn6BMYCQ0MR.htm](pathfinder-bestiary-2-items/Vm3TYdn6BMYCQ0MR.htm)|Claw|Garra|modificada|
|[vm77jtXXqIjODOmX.htm](pathfinder-bestiary-2-items/vm77jtXXqIjODOmX.htm)|Rebuke Soul|Rebuke Soul|modificada|
|[vMozJKWMEqEZvBOl.htm](pathfinder-bestiary-2-items/vMozJKWMEqEZvBOl.htm)|Jaws|Fauces|modificada|
|[vMUHP7DevP54tmz4.htm](pathfinder-bestiary-2-items/vMUHP7DevP54tmz4.htm)|Chameleon Skin|Piel de Camaleón|modificada|
|[vmyBkhvE34HW6vpl.htm](pathfinder-bestiary-2-items/vmyBkhvE34HW6vpl.htm)|Glowing Mucus|Glowing Mucus|modificada|
|[vMYnNlSJnYJepYe8.htm](pathfinder-bestiary-2-items/vMYnNlSJnYJepYe8.htm)|Impaling Barb|Empaling Barb|modificada|
|[VN24JoB39HXPdS3r.htm](pathfinder-bestiary-2-items/VN24JoB39HXPdS3r.htm)|Ravenous Repast|Ravenous Repast|modificada|
|[VnUmVnOCMIOvu7ja.htm](pathfinder-bestiary-2-items/VnUmVnOCMIOvu7ja.htm)|Flaming Armaments|Armamento flamígero|modificada|
|[vOhew2ogdaZmv4TY.htm](pathfinder-bestiary-2-items/vOhew2ogdaZmv4TY.htm)|Claw|Garra|modificada|
|[voMMMHUogUu7LSeF.htm](pathfinder-bestiary-2-items/voMMMHUogUu7LSeF.htm)|Mocking Touch|Toque burlón|modificada|
|[vorcYiRkw5YIs0Qy.htm](pathfinder-bestiary-2-items/vorcYiRkw5YIs0Qy.htm)|Trample|Trample|modificada|
|[VOzKuytxsOHWyPEJ.htm](pathfinder-bestiary-2-items/VOzKuytxsOHWyPEJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vP6KGkCy0KpMQarQ.htm](pathfinder-bestiary-2-items/vP6KGkCy0KpMQarQ.htm)|Sense Portal|Sense Portal|modificada|
|[VPVWe9JZVLpTixZC.htm](pathfinder-bestiary-2-items/VPVWe9JZVLpTixZC.htm)|Change Shape|Change Shape|modificada|
|[VpzTcikdwnBsqtN8.htm](pathfinder-bestiary-2-items/VpzTcikdwnBsqtN8.htm)|Regeneration 2 (Deactivated by Good or Silver)|Regeneración 2 (Desactivado por Bueno o Plata)|modificada|
|[vPzVhI5rLf9ZTI5N.htm](pathfinder-bestiary-2-items/vPzVhI5rLf9ZTI5N.htm)|Constant Spells|Constant Spells|modificada|
|[VQZuQhcA0A15y8kQ.htm](pathfinder-bestiary-2-items/VQZuQhcA0A15y8kQ.htm)|Trample|Trample|modificada|
|[VRDZl6y3aAge5VyP.htm](pathfinder-bestiary-2-items/VRDZl6y3aAge5VyP.htm)|Raise Serpent|Raise Serpent|modificada|
|[vrswuJTbGgycV77r.htm](pathfinder-bestiary-2-items/vrswuJTbGgycV77r.htm)|Vomit|Vómito|modificada|
|[VrxOb3MaxvXl1OR3.htm](pathfinder-bestiary-2-items/VrxOb3MaxvXl1OR3.htm)|Claw|Garra|modificada|
|[vtC1BWVjxbgjcJ2H.htm](pathfinder-bestiary-2-items/vtC1BWVjxbgjcJ2H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VTh3x19pApL0tjtU.htm](pathfinder-bestiary-2-items/VTh3x19pApL0tjtU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[VTmtaRNVlzTpmhif.htm](pathfinder-bestiary-2-items/VTmtaRNVlzTpmhif.htm)|Scimitar|Cimitarra|modificada|
|[VtQnDFC3P9Efec5Z.htm](pathfinder-bestiary-2-items/VtQnDFC3P9Efec5Z.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vu2E8xDlE75Jm0Mt.htm](pathfinder-bestiary-2-items/vu2E8xDlE75Jm0Mt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vu4nBmfeMF19jeah.htm](pathfinder-bestiary-2-items/vu4nBmfeMF19jeah.htm)|Radiant Blow|Golpe Radiante|modificada|
|[vu53NeOG28PzBXPL.htm](pathfinder-bestiary-2-items/vu53NeOG28PzBXPL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vuAvJlvQLWub8gyW.htm](pathfinder-bestiary-2-items/vuAvJlvQLWub8gyW.htm)|Strangling Fingers|Dedos Estranguladores|modificada|
|[vvGfXGSxCsVkyVlu.htm](pathfinder-bestiary-2-items/vvGfXGSxCsVkyVlu.htm)|Scent|Scent|modificada|
|[vVt7KeVPbqT0jHZQ.htm](pathfinder-bestiary-2-items/vVt7KeVPbqT0jHZQ.htm)|Clutch|Embrague|modificada|
|[vVucSfThIT1iEEjO.htm](pathfinder-bestiary-2-items/vVucSfThIT1iEEjO.htm)|Hook Shake|Sacudir con garfio|modificada|
|[VWC3NNUYpN6qbf09.htm](pathfinder-bestiary-2-items/VWC3NNUYpN6qbf09.htm)|Breath Weapon|Breath Weapon|modificada|
|[VwEDItuYmaW2BAkI.htm](pathfinder-bestiary-2-items/VwEDItuYmaW2BAkI.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[VwEyLSgIQwBeoPyV.htm](pathfinder-bestiary-2-items/VwEyLSgIQwBeoPyV.htm)|Nature's Infusion|Infusión de la Naturaleza|modificada|
|[VxDq9bJ1tuymcMTt.htm](pathfinder-bestiary-2-items/VxDq9bJ1tuymcMTt.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VyJyNz5V3NR5Gylr.htm](pathfinder-bestiary-2-items/VyJyNz5V3NR5Gylr.htm)|Axe Vulnerability 5|Vulnerabilidad a las hachas 5|modificada|
|[vYo1emHFZe2lLLBo.htm](pathfinder-bestiary-2-items/vYo1emHFZe2lLLBo.htm)|Constant Spells|Constant Spells|modificada|
|[vZ2zVHToKsyYJOTI.htm](pathfinder-bestiary-2-items/vZ2zVHToKsyYJOTI.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[VzK5CD8g2ofWRIkk.htm](pathfinder-bestiary-2-items/VzK5CD8g2ofWRIkk.htm)|Improved Grab|Agarrado mejorado|modificada|
|[VzMnFSYbgzk4CXes.htm](pathfinder-bestiary-2-items/VzMnFSYbgzk4CXes.htm)|Trackless|Trackless|modificada|
|[W0kFuPmuWmicQuUc.htm](pathfinder-bestiary-2-items/W0kFuPmuWmicQuUc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W0OH5fsPmoPURYsD.htm](pathfinder-bestiary-2-items/W0OH5fsPmoPURYsD.htm)|Claw|Garra|modificada|
|[w0XMZ6GmkzJcCRyO.htm](pathfinder-bestiary-2-items/w0XMZ6GmkzJcCRyO.htm)|Piercing Shriek|Piercing Shriek|modificada|
|[W1abGwpcG9yJOB9j.htm](pathfinder-bestiary-2-items/W1abGwpcG9yJOB9j.htm)|Constant Spells|Constant Spells|modificada|
|[W1bCVUm0sI8Wl7Bs.htm](pathfinder-bestiary-2-items/W1bCVUm0sI8Wl7Bs.htm)|Ghost Bane|Ghost Bane|modificada|
|[W1PoKy0P1n1NYE7q.htm](pathfinder-bestiary-2-items/W1PoKy0P1n1NYE7q.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[w1UuelNH4N0BPFBe.htm](pathfinder-bestiary-2-items/w1UuelNH4N0BPFBe.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[W3rITonxbyLEARlP.htm](pathfinder-bestiary-2-items/W3rITonxbyLEARlP.htm)|Breath Weapon|Breath Weapon|modificada|
|[W5c599K6o0Z10LCq.htm](pathfinder-bestiary-2-items/W5c599K6o0Z10LCq.htm)|Jaws|Fauces|modificada|
|[W6gUfzomGXvlnDjI.htm](pathfinder-bestiary-2-items/W6gUfzomGXvlnDjI.htm)|Telepathy|Telepatía|modificada|
|[W7bXRQuNuOabEMJg.htm](pathfinder-bestiary-2-items/W7bXRQuNuOabEMJg.htm)|Negative Healing|Curación negativa|modificada|
|[w7j7MOc866dBtx82.htm](pathfinder-bestiary-2-items/w7j7MOc866dBtx82.htm)|Scent|Scent|modificada|
|[w7tx24OC6vP2T7De.htm](pathfinder-bestiary-2-items/w7tx24OC6vP2T7De.htm)|Spray Pollen|Spray Polen|modificada|
|[W7Wjl7sNOZeiCQDu.htm](pathfinder-bestiary-2-items/W7Wjl7sNOZeiCQDu.htm)|Splinter|Splinter|modificada|
|[W86zITZjQ9sKIM3N.htm](pathfinder-bestiary-2-items/W86zITZjQ9sKIM3N.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[W8H9WsxFN6hIlghm.htm](pathfinder-bestiary-2-items/W8H9WsxFN6hIlghm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W8NWW39lZmTUYk0e.htm](pathfinder-bestiary-2-items/W8NWW39lZmTUYk0e.htm)|Water Sprint|Carrera acuática|modificada|
|[W9urt04ZY9TmtKEz.htm](pathfinder-bestiary-2-items/W9urt04ZY9TmtKEz.htm)|Jaws|Fauces|modificada|
|[W9vOqV3RGCnKCbHu.htm](pathfinder-bestiary-2-items/W9vOqV3RGCnKCbHu.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[wA3oz36wHS6e53Sa.htm](pathfinder-bestiary-2-items/wA3oz36wHS6e53Sa.htm)|Destructive Smash|Porrazo destructivo|modificada|
|[WbbgLFwKjXAiRHsC.htm](pathfinder-bestiary-2-items/WbbgLFwKjXAiRHsC.htm)|Whipping Tentacles|Tentáculos Azotadores|modificada|
|[Wbgjzf3goWRWkItA.htm](pathfinder-bestiary-2-items/Wbgjzf3goWRWkItA.htm)|Claw|Garra|modificada|
|[wcSlJpWbxhokd7Vc.htm](pathfinder-bestiary-2-items/wcSlJpWbxhokd7Vc.htm)|Pseudopod|Pseudópodo|modificada|
|[wdSBw6qGzwcD9eZu.htm](pathfinder-bestiary-2-items/wdSBw6qGzwcD9eZu.htm)|Gory Hydration|Hidratación Gory|modificada|
|[WdzuTa2nPDcswvjz.htm](pathfinder-bestiary-2-items/WdzuTa2nPDcswvjz.htm)|Disgorged Mucus|Disgorged Mucus|modificada|
|[wea1MnyUIhvtedhE.htm](pathfinder-bestiary-2-items/wea1MnyUIhvtedhE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[weN7yMZk4CWGAypb.htm](pathfinder-bestiary-2-items/weN7yMZk4CWGAypb.htm)|Beak|Beak|modificada|
|[Wez6v2oJoatkzzVp.htm](pathfinder-bestiary-2-items/Wez6v2oJoatkzzVp.htm)|Throw Rock|Arrojar roca|modificada|
|[wEzrLhyzfEwcfMlc.htm](pathfinder-bestiary-2-items/wEzrLhyzfEwcfMlc.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[wFvcOmqLVFa5RuAY.htm](pathfinder-bestiary-2-items/wFvcOmqLVFa5RuAY.htm)|Longspear|Longspear|modificada|
|[whb2OZIXgagZrUoX.htm](pathfinder-bestiary-2-items/whb2OZIXgagZrUoX.htm)|Corrosive Surface|Superficie corrosiva|modificada|
|[WHISh35la4f2ULAj.htm](pathfinder-bestiary-2-items/WHISh35la4f2ULAj.htm)|Stunning Chain|Cadena Aturdidora|modificada|
|[wHsOiFn1P7QcaaJ2.htm](pathfinder-bestiary-2-items/wHsOiFn1P7QcaaJ2.htm)|Stolen Identity|Identidad Robada|modificada|
|[WIlbTCI5PaEEizfq.htm](pathfinder-bestiary-2-items/WIlbTCI5PaEEizfq.htm)|Hidden Movement|Movimiento oculto|modificada|
|[WIPFxMzyJ87R3t7k.htm](pathfinder-bestiary-2-items/WIPFxMzyJ87R3t7k.htm)|Supernatural Speed|Velocidad Sobrenatural|modificada|
|[WjdPeEIa3jf7j8U2.htm](pathfinder-bestiary-2-items/WjdPeEIa3jf7j8U2.htm)|Constrict|Restringir|modificada|
|[WJHihy42QBNSUonW.htm](pathfinder-bestiary-2-items/WJHihy42QBNSUonW.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[WKRlqiMNA7n19s8U.htm](pathfinder-bestiary-2-items/WKRlqiMNA7n19s8U.htm)|Hoof|Hoof|modificada|
|[wKTyf3x01s0VjUOw.htm](pathfinder-bestiary-2-items/wKTyf3x01s0VjUOw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WKWzTrWyiPJNihmZ.htm](pathfinder-bestiary-2-items/WKWzTrWyiPJNihmZ.htm)|Thoughtsense|Percibir pensamientos|modificada|
|[WLcykyc9hp2QiTO4.htm](pathfinder-bestiary-2-items/WLcykyc9hp2QiTO4.htm)|Serpentfolk Venom|Serpentfolk Venom|modificada|
|[wleIqE8aRKNBcgOU.htm](pathfinder-bestiary-2-items/wleIqE8aRKNBcgOU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WleXuHzXfn7II29b.htm](pathfinder-bestiary-2-items/WleXuHzXfn7II29b.htm)|Yamaraj Venom|Yamaraj Venom|modificada|
|[wlmBL32OvWdlwr0B.htm](pathfinder-bestiary-2-items/wlmBL32OvWdlwr0B.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[WmPbJSiwT9lndOlq.htm](pathfinder-bestiary-2-items/WmPbJSiwT9lndOlq.htm)|Grab|Agarrado|modificada|
|[wnZi8QCOMG86Db4l.htm](pathfinder-bestiary-2-items/wnZi8QCOMG86Db4l.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[WocBezkZj15D3pxg.htm](pathfinder-bestiary-2-items/WocBezkZj15D3pxg.htm)|Tremorsense 30 feet|Tremorsense 30 pies|modificada|
|[wOmBOkeQmHwUzF7p.htm](pathfinder-bestiary-2-items/wOmBOkeQmHwUzF7p.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WopfIMXZtm7V3YUE.htm](pathfinder-bestiary-2-items/WopfIMXZtm7V3YUE.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[WOqo2UC99RfWFqA0.htm](pathfinder-bestiary-2-items/WOqo2UC99RfWFqA0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wOW7iWJBYxXBP4Dm.htm](pathfinder-bestiary-2-items/wOW7iWJBYxXBP4Dm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WPlqW6SIzbL8jscs.htm](pathfinder-bestiary-2-items/WPlqW6SIzbL8jscs.htm)|Focused Gaze|Centrar mirada|modificada|
|[wPLUpFiWxDX00EQJ.htm](pathfinder-bestiary-2-items/wPLUpFiWxDX00EQJ.htm)|Horn|Cuerno|modificada|
|[WpPjPwdGV3sTxsSN.htm](pathfinder-bestiary-2-items/WpPjPwdGV3sTxsSN.htm)|Claw|Garra|modificada|
|[WpPQsA9BHFxmr54k.htm](pathfinder-bestiary-2-items/WpPQsA9BHFxmr54k.htm)|Skull|Cráneo|modificada|
|[WPVTRIdQSTbDZImk.htm](pathfinder-bestiary-2-items/WPVTRIdQSTbDZImk.htm)|Feral Possession|Posesión Feral|modificada|
|[Wq62yRjG1oubi4Cf.htm](pathfinder-bestiary-2-items/Wq62yRjG1oubi4Cf.htm)|Grab|Agarrado|modificada|
|[WqAfn6xBB95rkCnJ.htm](pathfinder-bestiary-2-items/WqAfn6xBB95rkCnJ.htm)|Barbed Chain|Cadena de púas|modificada|
|[wQeIEyoNfxwL2mn1.htm](pathfinder-bestiary-2-items/wQeIEyoNfxwL2mn1.htm)|Grab|Agarrado|modificada|
|[wqGETtOyNtSqKkGC.htm](pathfinder-bestiary-2-items/wqGETtOyNtSqKkGC.htm)|Grab|Agarrado|modificada|
|[wrisTXltKN2ggF1B.htm](pathfinder-bestiary-2-items/wrisTXltKN2ggF1B.htm)|Mandragora Venom|Mandragora Venom|modificada|
|[Wrwp9diMkabKorJn.htm](pathfinder-bestiary-2-items/Wrwp9diMkabKorJn.htm)|Beak|Beak|modificada|
|[ws9YoHlMJ3zrjnM1.htm](pathfinder-bestiary-2-items/ws9YoHlMJ3zrjnM1.htm)|Tail Sweep|Barrido de Cola|modificada|
|[WsGuoYeAA0yWHOXE.htm](pathfinder-bestiary-2-items/WsGuoYeAA0yWHOXE.htm)|Pall of Shadow|Féretro sombrío|modificada|
|[wSYxanQKAbTVaazk.htm](pathfinder-bestiary-2-items/wSYxanQKAbTVaazk.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[wTAXkMHuxjJpD8OH.htm](pathfinder-bestiary-2-items/wTAXkMHuxjJpD8OH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WtMCu7aRag8Z4ewh.htm](pathfinder-bestiary-2-items/WtMCu7aRag8Z4ewh.htm)|Swamp Stride|Swamp Stride|modificada|
|[WtPKkX18bEjTQBFy.htm](pathfinder-bestiary-2-items/WtPKkX18bEjTQBFy.htm)|Soul Harvest|Soul Harvest|modificada|
|[WTUxYAzq1Nr3FFwN.htm](pathfinder-bestiary-2-items/WTUxYAzq1Nr3FFwN.htm)|Jaws|Fauces|modificada|
|[wtvDPCazUVSsqJ2O.htm](pathfinder-bestiary-2-items/wtvDPCazUVSsqJ2O.htm)|Negative Healing|Curación negativa|modificada|
|[WU1jExSnDYhncwTy.htm](pathfinder-bestiary-2-items/WU1jExSnDYhncwTy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wugLYHXdCUg05j8X.htm](pathfinder-bestiary-2-items/wugLYHXdCUg05j8X.htm)|Sportlebore Infestation|Infestación de sportlebore|modificada|
|[WuwpqYb5cVctrCUQ.htm](pathfinder-bestiary-2-items/WuwpqYb5cVctrCUQ.htm)|Claw|Garra|modificada|
|[WVChw6DFgtcfsrAf.htm](pathfinder-bestiary-2-items/WVChw6DFgtcfsrAf.htm)|Swarming Beaks|Swarming Beaks|modificada|
|[wveW1LFrPr2tdNpt.htm](pathfinder-bestiary-2-items/wveW1LFrPr2tdNpt.htm)|Tentacle|Tentáculo|modificada|
|[Wvss2Toe2q47z9GS.htm](pathfinder-bestiary-2-items/Wvss2Toe2q47z9GS.htm)|Leg|Pierna|modificada|
|[wwbxcuqrl4U2FZiP.htm](pathfinder-bestiary-2-items/wwbxcuqrl4U2FZiP.htm)|Claw|Garra|modificada|
|[wWilHEuB4DGYHQdO.htm](pathfinder-bestiary-2-items/wWilHEuB4DGYHQdO.htm)|Jaws|Fauces|modificada|
|[Wx80gWdCaIlQR8Yn.htm](pathfinder-bestiary-2-items/Wx80gWdCaIlQR8Yn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WXEuGWzgAk5JMfEE.htm](pathfinder-bestiary-2-items/WXEuGWzgAk5JMfEE.htm)|Vulnerable to Prone|Vulnerable a Prone|modificada|
|[WxKg83hAxh4B0VJo.htm](pathfinder-bestiary-2-items/WxKg83hAxh4B0VJo.htm)|Flaming Ghost Touch Longspear|Flamígera Toque fantasmal Longspear|modificada|
|[wy1mkkY0rhH0B5oL.htm](pathfinder-bestiary-2-items/wy1mkkY0rhH0B5oL.htm)|Defender of the Seas|Defender of the Seas|modificada|
|[wY6wIEuFcKjcTbGS.htm](pathfinder-bestiary-2-items/wY6wIEuFcKjcTbGS.htm)|Improved Grab|Agarrado mejorado|modificada|
|[wY7gOSBgpUJ4wcUI.htm](pathfinder-bestiary-2-items/wY7gOSBgpUJ4wcUI.htm)|Shortbow|Arco corto|modificada|
|[Wyf2nzKbV2LhVCMO.htm](pathfinder-bestiary-2-items/Wyf2nzKbV2LhVCMO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[wyti8UM7xKPOJOv6.htm](pathfinder-bestiary-2-items/wyti8UM7xKPOJOv6.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[wYU3mjAP9KQSofkn.htm](pathfinder-bestiary-2-items/wYU3mjAP9KQSofkn.htm)|Ghost Bane|Ghost Bane|modificada|
|[wyy8wN5QiLr4fV2L.htm](pathfinder-bestiary-2-items/wyy8wN5QiLr4fV2L.htm)|Fjord Linnorm Venom|Veneno de Linnorm del fiordo|modificada|
|[WyzkB1NHDFF0YsAm.htm](pathfinder-bestiary-2-items/WyzkB1NHDFF0YsAm.htm)|Improved Grab|Agarrado mejorado|modificada|
|[WzWOnae18jvHd2C0.htm](pathfinder-bestiary-2-items/WzWOnae18jvHd2C0.htm)|Sadistic Strike|Golpe Sádico|modificada|
|[x0HpOyXvVG8ecvWu.htm](pathfinder-bestiary-2-items/x0HpOyXvVG8ecvWu.htm)|Constrict|Restringir|modificada|
|[x0tIw8t8mhFgH7ab.htm](pathfinder-bestiary-2-items/x0tIw8t8mhFgH7ab.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[x183YuDiYXsrOlKc.htm](pathfinder-bestiary-2-items/x183YuDiYXsrOlKc.htm)|Bone-Chilling Screech|Bone-Chilling Screech|modificada|
|[x1avaIq02Cr7LmNs.htm](pathfinder-bestiary-2-items/x1avaIq02Cr7LmNs.htm)|+2 Status Bonus on Saves vs. Enchantment and Illusion Effects|Bonificador de situación +2 a las salvaciones contra efectos de encantamiento e ilusión.|modificada|
|[X1Hh7DXiCGoPXh2j.htm](pathfinder-bestiary-2-items/X1Hh7DXiCGoPXh2j.htm)|Jaws|Fauces|modificada|
|[x1kqxURcucjG3BaP.htm](pathfinder-bestiary-2-items/x1kqxURcucjG3BaP.htm)|Change Shape|Change Shape|modificada|
|[x2IF1MX8EodRyzeW.htm](pathfinder-bestiary-2-items/x2IF1MX8EodRyzeW.htm)|Rend|Rasgadura|modificada|
|[x3aksJFYweZk98OP.htm](pathfinder-bestiary-2-items/x3aksJFYweZk98OP.htm)|Retributive Strike|Golpe retributivo|modificada|
|[x3sRV0Fub8ixWfBV.htm](pathfinder-bestiary-2-items/x3sRV0Fub8ixWfBV.htm)|Toss|Toss|modificada|
|[X3x7z0KsDNoqFpey.htm](pathfinder-bestiary-2-items/X3x7z0KsDNoqFpey.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[X4D3vNjnicVrqgiG.htm](pathfinder-bestiary-2-items/X4D3vNjnicVrqgiG.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[X4O8mzTQLp0lBKqY.htm](pathfinder-bestiary-2-items/X4O8mzTQLp0lBKqY.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[x4vlJSFL6WnsbK82.htm](pathfinder-bestiary-2-items/x4vlJSFL6WnsbK82.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[x5lmNBtvBd1z0Azh.htm](pathfinder-bestiary-2-items/x5lmNBtvBd1z0Azh.htm)|Temporal Reversion|Reversión Temporal|modificada|
|[X5uycRcoYkauIGYQ.htm](pathfinder-bestiary-2-items/X5uycRcoYkauIGYQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[x6cH9h8wAkR78uX9.htm](pathfinder-bestiary-2-items/x6cH9h8wAkR78uX9.htm)|Fist|Puño|modificada|
|[X6lteRmVzKPr3P02.htm](pathfinder-bestiary-2-items/X6lteRmVzKPr3P02.htm)|Jaws|Fauces|modificada|
|[x7o1u8JU3bkG8XjR.htm](pathfinder-bestiary-2-items/x7o1u8JU3bkG8XjR.htm)|Claw|Garra|modificada|
|[x7QZQO50H5BxEvkk.htm](pathfinder-bestiary-2-items/x7QZQO50H5BxEvkk.htm)|Starvation Vulnerability|Starvation Vulnerability|modificada|
|[X8BOiH4KPnC3n94o.htm](pathfinder-bestiary-2-items/X8BOiH4KPnC3n94o.htm)|Regeneration 15 (Deactivated by Acid or Fire)|Regeneración 15 (ácido o fuego)|modificada|
|[x8IEFlb3LI5XDtkm.htm](pathfinder-bestiary-2-items/x8IEFlb3LI5XDtkm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[X8NPEY2f1Z9LKe53.htm](pathfinder-bestiary-2-items/X8NPEY2f1Z9LKe53.htm)|Scent|Scent|modificada|
|[x8pzzu8VRuRa5sdg.htm](pathfinder-bestiary-2-items/x8pzzu8VRuRa5sdg.htm)|Stench|Hedor|modificada|
|[x97vWevUTEMqeOCJ.htm](pathfinder-bestiary-2-items/x97vWevUTEMqeOCJ.htm)|Painsight|Painsight|modificada|
|[X9B0ICfO4bmtJ9pv.htm](pathfinder-bestiary-2-items/X9B0ICfO4bmtJ9pv.htm)|Death-Stealing Gaze|Mirada que roba la muerte|modificada|
|[X9SDgPuuF9AwEVsK.htm](pathfinder-bestiary-2-items/X9SDgPuuF9AwEVsK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xa0P6lS6jAgU3iPX.htm](pathfinder-bestiary-2-items/xa0P6lS6jAgU3iPX.htm)|Archon's Door|Puerta del arconte|modificada|
|[xA6uZnqBzxOvjJYs.htm](pathfinder-bestiary-2-items/xA6uZnqBzxOvjJYs.htm)|Tongue|Lengua|modificada|
|[XaAIKVkn76K7fFFS.htm](pathfinder-bestiary-2-items/XaAIKVkn76K7fFFS.htm)|Change Shape|Change Shape|modificada|
|[XaaNqqz2ZFyt0c4o.htm](pathfinder-bestiary-2-items/XaaNqqz2ZFyt0c4o.htm)|Specious Suggestion|Sugestión engaóosa|modificada|
|[xAlvzCWOjUqsTXiK.htm](pathfinder-bestiary-2-items/xAlvzCWOjUqsTXiK.htm)|Scent|Scent|modificada|
|[xAtGnv1FFFsyE06f.htm](pathfinder-bestiary-2-items/xAtGnv1FFFsyE06f.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[xAxdPG2ftegyTaUj.htm](pathfinder-bestiary-2-items/xAxdPG2ftegyTaUj.htm)|Water Jet|Chorro de Agua|modificada|
|[Xb5fg9Wf3Iiz5qeu.htm](pathfinder-bestiary-2-items/Xb5fg9Wf3Iiz5qeu.htm)|Wing|Ala|modificada|
|[xBrIiF2AXw27mMmW.htm](pathfinder-bestiary-2-items/xBrIiF2AXw27mMmW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Xc15ip4V31NZTNdq.htm](pathfinder-bestiary-2-items/Xc15ip4V31NZTNdq.htm)|Frightful Presence|Frightful Presence|modificada|
|[XCa5wvlPu21MNSQl.htm](pathfinder-bestiary-2-items/XCa5wvlPu21MNSQl.htm)|Horn|Cuerno|modificada|
|[XcF1XgnCOdQvrvbz.htm](pathfinder-bestiary-2-items/XcF1XgnCOdQvrvbz.htm)|Talon|Talon|modificada|
|[xCffJ5CYclgmYzmC.htm](pathfinder-bestiary-2-items/xCffJ5CYclgmYzmC.htm)|Sticky Filament|Sticky Filament|modificada|
|[XCFOQ29kYuqzushV.htm](pathfinder-bestiary-2-items/XCFOQ29kYuqzushV.htm)|Claw|Garra|modificada|
|[XcpCwjEjd7OKbKnx.htm](pathfinder-bestiary-2-items/XcpCwjEjd7OKbKnx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[XCr9jxxE59Egb2R1.htm](pathfinder-bestiary-2-items/XCr9jxxE59Egb2R1.htm)|Woodland Ambush|Emboscar en el bosque|modificada|
|[xcs2mYc8h9YshZle.htm](pathfinder-bestiary-2-items/xcs2mYc8h9YshZle.htm)|Attack of Opportunity (Hoof Only)|Ataque de oportunidad (sólo pezuña).|modificada|
|[xdv7JO5TXVfEGxJr.htm](pathfinder-bestiary-2-items/xdv7JO5TXVfEGxJr.htm)|Tenacious Stance|Posición Tenaz|modificada|
|[XErVBE2Mmnzz099t.htm](pathfinder-bestiary-2-items/XErVBE2Mmnzz099t.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xgEzGbuJSMIIkD79.htm](pathfinder-bestiary-2-items/xgEzGbuJSMIIkD79.htm)|Frightful Presence|Frightful Presence|modificada|
|[xgpvgBd8BjAbDY4N.htm](pathfinder-bestiary-2-items/xgpvgBd8BjAbDY4N.htm)|Puddled Ambush|Emboscada encharcada|modificada|
|[XGTj8sfNa1YvUTdZ.htm](pathfinder-bestiary-2-items/XGTj8sfNa1YvUTdZ.htm)|Captivating Lure|Captivating Lure|modificada|
|[xgZ1NJFG41NW7CFG.htm](pathfinder-bestiary-2-items/xgZ1NJFG41NW7CFG.htm)|Telepathy|Telepatía|modificada|
|[XHAPOvUbe2eKOW7E.htm](pathfinder-bestiary-2-items/XHAPOvUbe2eKOW7E.htm)|Whirlwind Throw|Lanzamiento Torbellino|modificada|
|[XhhOGnxq5cUHkcEd.htm](pathfinder-bestiary-2-items/XhhOGnxq5cUHkcEd.htm)|Burble|Burble|modificada|
|[XHjjW9wEW3pVVm5K.htm](pathfinder-bestiary-2-items/XHjjW9wEW3pVVm5K.htm)|Tendriculos Venom|Veneno Tendriculos|modificada|
|[XHjNDrJBXaCEsi2i.htm](pathfinder-bestiary-2-items/XHjNDrJBXaCEsi2i.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[XHSGBobf5LZDw5R5.htm](pathfinder-bestiary-2-items/XHSGBobf5LZDw5R5.htm)|Jaws|Fauces|modificada|
|[XhyjVYavWAJIwq7b.htm](pathfinder-bestiary-2-items/XhyjVYavWAJIwq7b.htm)|Jaws|Fauces|modificada|
|[xijGvHanHygeHGCM.htm](pathfinder-bestiary-2-items/xijGvHanHygeHGCM.htm)|Hurl Blade|Hurl Blade|modificada|
|[xiOTeaVvejTPBrhk.htm](pathfinder-bestiary-2-items/xiOTeaVvejTPBrhk.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XJ7A5MPGGNFZpxXP.htm](pathfinder-bestiary-2-items/XJ7A5MPGGNFZpxXP.htm)|Negative Healing|Curación negativa|modificada|
|[xJOBW00PxizZ4M8X.htm](pathfinder-bestiary-2-items/xJOBW00PxizZ4M8X.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[XjvtH7IilCnzJpIN.htm](pathfinder-bestiary-2-items/XjvtH7IilCnzJpIN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xKGmyTmllHIjiYO0.htm](pathfinder-bestiary-2-items/xKGmyTmllHIjiYO0.htm)|Earth Shaker|Agitador de Tierra|modificada|
|[XMbOWL8A6tcWyW18.htm](pathfinder-bestiary-2-items/XMbOWL8A6tcWyW18.htm)|Radiant Wings|Alas Radiantes|modificada|
|[xmBUGoRIvL3UEoqs.htm](pathfinder-bestiary-2-items/xmBUGoRIvL3UEoqs.htm)|Breath Weapon|Breath Weapon|modificada|
|[XmKUjz23s0dNFOYW.htm](pathfinder-bestiary-2-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|Podredumbre del pantano|modificada|
|[Xmq08HzwUTnliDuF.htm](pathfinder-bestiary-2-items/Xmq08HzwUTnliDuF.htm)|Fist|Puño|modificada|
|[XMTEsGgReo4d8QMB.htm](pathfinder-bestiary-2-items/XMTEsGgReo4d8QMB.htm)|Katana|Katana|modificada|
|[xmYe9vSGZokROsE8.htm](pathfinder-bestiary-2-items/xmYe9vSGZokROsE8.htm)|Blinding Soul|Alma cegadora|modificada|
|[xnaTB4eWK8MokhN6.htm](pathfinder-bestiary-2-items/xnaTB4eWK8MokhN6.htm)|Claw|Garra|modificada|
|[xNfSi1bsS4hwfcXU.htm](pathfinder-bestiary-2-items/xNfSi1bsS4hwfcXU.htm)|Carnivorous Blob Acid|Carnivorous Blob Acid|modificada|
|[Xnn4xjhGBTSE9KtK.htm](pathfinder-bestiary-2-items/Xnn4xjhGBTSE9KtK.htm)|Tail|Tail|modificada|
|[xOfT1xQ4weGRsYHu.htm](pathfinder-bestiary-2-items/xOfT1xQ4weGRsYHu.htm)|Radiant Blast|Ráfaga Radiante|modificada|
|[xomzu6MJJ9pOCVdY.htm](pathfinder-bestiary-2-items/xomzu6MJJ9pOCVdY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xoV74Ad4ZTLkT5re.htm](pathfinder-bestiary-2-items/xoV74Ad4ZTLkT5re.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[XPEavSJTuusA8TlU.htm](pathfinder-bestiary-2-items/XPEavSJTuusA8TlU.htm)|All-Around Vision|All-Around Vision|modificada|
|[xpfZ2YkQuu4I8kW0.htm](pathfinder-bestiary-2-items/xpfZ2YkQuu4I8kW0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[xPid5CwljZFADBGF.htm](pathfinder-bestiary-2-items/xPid5CwljZFADBGF.htm)|Trample|Trample|modificada|
|[XplLVQ3DKSYJCuYL.htm](pathfinder-bestiary-2-items/XplLVQ3DKSYJCuYL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[xPTfLI21EUbrSp4Z.htm](pathfinder-bestiary-2-items/xPTfLI21EUbrSp4Z.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[XpTtuP0So5dN4MVl.htm](pathfinder-bestiary-2-items/XpTtuP0So5dN4MVl.htm)|Improved Grab|Agarrado mejorado|modificada|
|[xq3V1P1OXiEGcqZs.htm](pathfinder-bestiary-2-items/xq3V1P1OXiEGcqZs.htm)|Angled Entry|Angled Entry|modificada|
|[xqB9FvxJUsDUDs15.htm](pathfinder-bestiary-2-items/xqB9FvxJUsDUDs15.htm)|Thorny Vine|Thorny Vine|modificada|
|[XqbsPpZkRYLwrQDg.htm](pathfinder-bestiary-2-items/XqbsPpZkRYLwrQDg.htm)|Tresses|Tresses|modificada|
|[XqpyTWP6g2DVmNqf.htm](pathfinder-bestiary-2-items/XqpyTWP6g2DVmNqf.htm)|Jaws|Fauces|modificada|
|[XRAQMcQEssAaMi02.htm](pathfinder-bestiary-2-items/XRAQMcQEssAaMi02.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[xRcOZkgwBwJLmsbr.htm](pathfinder-bestiary-2-items/xRcOZkgwBwJLmsbr.htm)|Constrict|Restringir|modificada|
|[XrJk8sW9W9FE8rtL.htm](pathfinder-bestiary-2-items/XrJk8sW9W9FE8rtL.htm)|Guardian Spirit|Espíritu Guardián|modificada|
|[xRPyNMWHvevzaNHI.htm](pathfinder-bestiary-2-items/xRPyNMWHvevzaNHI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xS1M3nE7TiguYe63.htm](pathfinder-bestiary-2-items/xS1M3nE7TiguYe63.htm)|Arm|Brazo|modificada|
|[XSN5GyOi1NAbLqve.htm](pathfinder-bestiary-2-items/XSN5GyOi1NAbLqve.htm)|Holy Armaments|Armamento Sagrada|modificada|
|[XSrKkFryn35jFMnz.htm](pathfinder-bestiary-2-items/XSrKkFryn35jFMnz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XucZtbvC1QhYV0HW.htm](pathfinder-bestiary-2-items/XucZtbvC1QhYV0HW.htm)|Ferocity|Ferocidad|modificada|
|[XUErBTnVDmhG16lW.htm](pathfinder-bestiary-2-items/XUErBTnVDmhG16lW.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[xumH4auLNLHoCwRg.htm](pathfinder-bestiary-2-items/xumH4auLNLHoCwRg.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[xURZboPgZokTtHAT.htm](pathfinder-bestiary-2-items/xURZboPgZokTtHAT.htm)|Grab|Agarrado|modificada|
|[XuX1YKa36ctJmZJH.htm](pathfinder-bestiary-2-items/XuX1YKa36ctJmZJH.htm)|Explosive Rebirth|Renacimiento Explosión|modificada|
|[XWdqUKdwgQ9Cn7vC.htm](pathfinder-bestiary-2-items/XWdqUKdwgQ9Cn7vC.htm)|Club|Club|modificada|
|[xwvyEIcVrXBhC3JM.htm](pathfinder-bestiary-2-items/xwvyEIcVrXBhC3JM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[xx6zXcdGLkuW5RmE.htm](pathfinder-bestiary-2-items/xx6zXcdGLkuW5RmE.htm)|Flooding Thrust|Flooding Thrust|modificada|
|[xxEXe18G17Aqb8EC.htm](pathfinder-bestiary-2-items/xxEXe18G17Aqb8EC.htm)|Tail|Tail|modificada|
|[xxK8rXRDWK5Rwj9i.htm](pathfinder-bestiary-2-items/xxK8rXRDWK5Rwj9i.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[xXv6eUeKYXjO80rc.htm](pathfinder-bestiary-2-items/xXv6eUeKYXjO80rc.htm)|Swoop|Swoop|modificada|
|[XyAMzK7AwDLSUd1X.htm](pathfinder-bestiary-2-items/XyAMzK7AwDLSUd1X.htm)|Water Blast|Water Blast|modificada|
|[XZ1UeBvCwRe6I510.htm](pathfinder-bestiary-2-items/XZ1UeBvCwRe6I510.htm)|Magma Spit|Magma Spit|modificada|
|[xzuxIMk5QK7iPQ9j.htm](pathfinder-bestiary-2-items/xzuxIMk5QK7iPQ9j.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[y0g8S6A5Uri1G8xe.htm](pathfinder-bestiary-2-items/y0g8S6A5Uri1G8xe.htm)|Drill|Taladro|modificada|
|[Y24jpiGNojJwV4RQ.htm](pathfinder-bestiary-2-items/Y24jpiGNojJwV4RQ.htm)|Improved Grab|Agarrado mejorado|modificada|
|[Y2i3TiK6GDsS9UIv.htm](pathfinder-bestiary-2-items/Y2i3TiK6GDsS9UIv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Y3O7AOvH1wkpSV2Z.htm](pathfinder-bestiary-2-items/Y3O7AOvH1wkpSV2Z.htm)|Bite|Muerdemuerde|modificada|
|[y5b7lQEAyBanNTYQ.htm](pathfinder-bestiary-2-items/y5b7lQEAyBanNTYQ.htm)|Constrict|Restringir|modificada|
|[y5Va61hQuxXJ9dhb.htm](pathfinder-bestiary-2-items/y5Va61hQuxXJ9dhb.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Y6Rqq2ahFM07WKRm.htm](pathfinder-bestiary-2-items/Y6Rqq2ahFM07WKRm.htm)|Swarming Stance|Posición de Enjambre|modificada|
|[y7LC1NdSY22WtjWg.htm](pathfinder-bestiary-2-items/y7LC1NdSY22WtjWg.htm)|Claw|Garra|modificada|
|[Y7LW92BfHBvUX6RP.htm](pathfinder-bestiary-2-items/Y7LW92BfHBvUX6RP.htm)|Forfeiture Aversion|Aversión a la confiscación|modificada|
|[y7wcFxhYodNrBPwL.htm](pathfinder-bestiary-2-items/y7wcFxhYodNrBPwL.htm)|Wraith Spawn|Engendro Espectro|modificada|
|[Y7ZMEhAtrNgNLJAW.htm](pathfinder-bestiary-2-items/Y7ZMEhAtrNgNLJAW.htm)|Claw|Garra|modificada|
|[Y810sLO1OzrK8e4z.htm](pathfinder-bestiary-2-items/Y810sLO1OzrK8e4z.htm)|Claw|Garra|modificada|
|[y9b4KGpxjtDbtBFO.htm](pathfinder-bestiary-2-items/y9b4KGpxjtDbtBFO.htm)|Grab|Agarrado|modificada|
|[Y9QK094iVscecdmW.htm](pathfinder-bestiary-2-items/Y9QK094iVscecdmW.htm)|Animate Weapon|Arma animada|modificada|
|[yAPG78TDrcEUnBIP.htm](pathfinder-bestiary-2-items/yAPG78TDrcEUnBIP.htm)|Protean Anatomy 8|Protean Anatomy 8|modificada|
|[yb7yQZeKyk9xJFnV.htm](pathfinder-bestiary-2-items/yb7yQZeKyk9xJFnV.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[YBhZi6V0xuvRTBkT.htm](pathfinder-bestiary-2-items/YBhZi6V0xuvRTBkT.htm)|Maul|Zarpazo doble|modificada|
|[YBRIxpMXUrxJfeV1.htm](pathfinder-bestiary-2-items/YBRIxpMXUrxJfeV1.htm)|Grab|Agarrado|modificada|
|[YBrJf0B8ODS54eSM.htm](pathfinder-bestiary-2-items/YBrJf0B8ODS54eSM.htm)|Constant Spells|Constant Spells|modificada|
|[YcPEa5pOVPtksEtZ.htm](pathfinder-bestiary-2-items/YcPEa5pOVPtksEtZ.htm)|Frightful Presence|Frightful Presence|modificada|
|[YdcYSwBDfJFctOhL.htm](pathfinder-bestiary-2-items/YdcYSwBDfJFctOhL.htm)|Pounce|Abalanzarse|modificada|
|[YEQydLBRNZJVJBAN.htm](pathfinder-bestiary-2-items/YEQydLBRNZJVJBAN.htm)|Dagger|Daga|modificada|
|[YerPyajhgUANJwRq.htm](pathfinder-bestiary-2-items/YerPyajhgUANJwRq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yf4k8ikFfqMlrB2E.htm](pathfinder-bestiary-2-items/yf4k8ikFfqMlrB2E.htm)|Scent|Scent|modificada|
|[YfFUNMAKaJ0wspVA.htm](pathfinder-bestiary-2-items/YfFUNMAKaJ0wspVA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yFIIVeKjDVqU6qlw.htm](pathfinder-bestiary-2-items/yFIIVeKjDVqU6qlw.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[yFWK77O4rMtMcEmp.htm](pathfinder-bestiary-2-items/yFWK77O4rMtMcEmp.htm)|Swallow Whole|Engullir Todo|modificada|
|[yH19s0CEMs71KZgv.htm](pathfinder-bestiary-2-items/yH19s0CEMs71KZgv.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[yHiEUWvnlHVJLlCA.htm](pathfinder-bestiary-2-items/yHiEUWvnlHVJLlCA.htm)|Draining Strike|Golpe Drenante|modificada|
|[YHLI3DUcgA33ovkw.htm](pathfinder-bestiary-2-items/YHLI3DUcgA33ovkw.htm)|Jaws|Fauces|modificada|
|[YHmyiWDMvEgujfl1.htm](pathfinder-bestiary-2-items/YHmyiWDMvEgujfl1.htm)|Bully's Bludgeon|Bully's Bludgeon|modificada|
|[YhTp7RIQLVLYYNUn.htm](pathfinder-bestiary-2-items/YhTp7RIQLVLYYNUn.htm)|Sticky Feet|Pies Pegajosos|modificada|
|[yI8fil9Hp8Ob0BcY.htm](pathfinder-bestiary-2-items/yI8fil9Hp8Ob0BcY.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[yiiBFzSFMWspwb05.htm](pathfinder-bestiary-2-items/yiiBFzSFMWspwb05.htm)|Talon|Talon|modificada|
|[yinuvv1DAaHpt2dq.htm](pathfinder-bestiary-2-items/yinuvv1DAaHpt2dq.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[yJ6tSdgHei2VkcFn.htm](pathfinder-bestiary-2-items/yJ6tSdgHei2VkcFn.htm)|Swift Claw|Garra Celeridad|modificada|
|[yjgVEFL2BwjC3Ylc.htm](pathfinder-bestiary-2-items/yjgVEFL2BwjC3Ylc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[YJzGe5kr1qCZKV34.htm](pathfinder-bestiary-2-items/YJzGe5kr1qCZKV34.htm)|Magma Scorpion Venom|Veneno de Escorpión de Magma|modificada|
|[yKM6d3CSPA4ymKUk.htm](pathfinder-bestiary-2-items/yKM6d3CSPA4ymKUk.htm)|Constant Spells|Constant Spells|modificada|
|[yKOp4n7Sk96x65EX.htm](pathfinder-bestiary-2-items/yKOp4n7Sk96x65EX.htm)|Jaws|Fauces|modificada|
|[YkuWnKDR7iQkfOh8.htm](pathfinder-bestiary-2-items/YkuWnKDR7iQkfOh8.htm)|Megalania Venom|Megalania Venom|modificada|
|[ykVZX3vwjXXl9WoS.htm](pathfinder-bestiary-2-items/ykVZX3vwjXXl9WoS.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yl8CvIWxZx0c6fcn.htm](pathfinder-bestiary-2-items/yl8CvIWxZx0c6fcn.htm)|Tail|Tail|modificada|
|[YlO5up5kkSrroKf6.htm](pathfinder-bestiary-2-items/YlO5up5kkSrroKf6.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[Ym0WhQCJHDGT3v4z.htm](pathfinder-bestiary-2-items/Ym0WhQCJHDGT3v4z.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[YMLwRoRwaGIKGsjv.htm](pathfinder-bestiary-2-items/YMLwRoRwaGIKGsjv.htm)|Foot|Pie|modificada|
|[ymt7FhkmnZp7ABEM.htm](pathfinder-bestiary-2-items/ymt7FhkmnZp7ABEM.htm)|Scent|Scent|modificada|
|[yn2c3QvZMZCfu55r.htm](pathfinder-bestiary-2-items/yn2c3QvZMZCfu55r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YnfFdQs1dxCIBHYJ.htm](pathfinder-bestiary-2-items/YnfFdQs1dxCIBHYJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yNsJmPBe7WVLrPiX.htm](pathfinder-bestiary-2-items/yNsJmPBe7WVLrPiX.htm)|Magic-Warping Aura|Aura Mágica-Warping|modificada|
|[yoc8sNwSb16TykKF.htm](pathfinder-bestiary-2-items/yoc8sNwSb16TykKF.htm)|Claw|Garra|modificada|
|[yokGmVR1dAkRTCmE.htm](pathfinder-bestiary-2-items/yokGmVR1dAkRTCmE.htm)|Trample|Trample|modificada|
|[yooFGlf4aPfCD1vV.htm](pathfinder-bestiary-2-items/yooFGlf4aPfCD1vV.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[yqx6BirfDar1gC35.htm](pathfinder-bestiary-2-items/yqx6BirfDar1gC35.htm)|Tremorsense|Tremorsense|modificada|
|[YRhZxIyDuKIuiHky.htm](pathfinder-bestiary-2-items/YRhZxIyDuKIuiHky.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[YrMHTjkFMDgVtW5i.htm](pathfinder-bestiary-2-items/YrMHTjkFMDgVtW5i.htm)|Grab|Agarrado|modificada|
|[yRTlpaC9IkrTE5xd.htm](pathfinder-bestiary-2-items/yRTlpaC9IkrTE5xd.htm)|Grab|Agarrado|modificada|
|[yRuXhvVShtatFwyU.htm](pathfinder-bestiary-2-items/yRuXhvVShtatFwyU.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[YRXnBeKMp7iUfdDz.htm](pathfinder-bestiary-2-items/YRXnBeKMp7iUfdDz.htm)|Shell Defense|Defensa con caparazón|modificada|
|[YS1pxmdE8Ebmyy23.htm](pathfinder-bestiary-2-items/YS1pxmdE8Ebmyy23.htm)|Find Target|Buscar objetivo|modificada|
|[Ys2OTavkoyvMU7QG.htm](pathfinder-bestiary-2-items/Ys2OTavkoyvMU7QG.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ysLsaBnQ2SlAerXO.htm](pathfinder-bestiary-2-items/ysLsaBnQ2SlAerXO.htm)|Fatal Faker|Falsificador fatal|modificada|
|[YsR1W3Gw985Xzr24.htm](pathfinder-bestiary-2-items/YsR1W3Gw985Xzr24.htm)|Grab|Agarrado|modificada|
|[ysswRv1XkPgse3iC.htm](pathfinder-bestiary-2-items/ysswRv1XkPgse3iC.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[ysSzKfSqDhv0OTY7.htm](pathfinder-bestiary-2-items/ysSzKfSqDhv0OTY7.htm)|Fist|Puño|modificada|
|[YSVs60IJmBC8CFXN.htm](pathfinder-bestiary-2-items/YSVs60IJmBC8CFXN.htm)|Pack Attack|Ataque en manada|modificada|
|[ySwL52AguRVZG0uO.htm](pathfinder-bestiary-2-items/ySwL52AguRVZG0uO.htm)|Claw|Garra|modificada|
|[yu67ZekTRXDanQoC.htm](pathfinder-bestiary-2-items/yu67ZekTRXDanQoC.htm)|Boiled by Light|Hervido por la luz|modificada|
|[YUHuvUIG5Z4hJRM4.htm](pathfinder-bestiary-2-items/YUHuvUIG5Z4hJRM4.htm)|Scent|Scent|modificada|
|[YuIDIcSKbqQHoLea.htm](pathfinder-bestiary-2-items/YuIDIcSKbqQHoLea.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[YV7bdMpIOclaEmMg.htm](pathfinder-bestiary-2-items/YV7bdMpIOclaEmMg.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[YvhSv5FSqLRo98YJ.htm](pathfinder-bestiary-2-items/YvhSv5FSqLRo98YJ.htm)|Horn|Cuerno|modificada|
|[YvIUXJiNq5FoRcoe.htm](pathfinder-bestiary-2-items/YvIUXJiNq5FoRcoe.htm)|Rend|Rasgadura|modificada|
|[yW0bi5z1ngsdOjTv.htm](pathfinder-bestiary-2-items/yW0bi5z1ngsdOjTv.htm)|Constant Spells|Constant Spells|modificada|
|[YwEDGRZpJYEj2PKw.htm](pathfinder-bestiary-2-items/YwEDGRZpJYEj2PKw.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[YwNfN5E1fuqbDqG5.htm](pathfinder-bestiary-2-items/YwNfN5E1fuqbDqG5.htm)|Mindwarping Tide|Marea de Guerra Mental|modificada|
|[YXm2W59CMTMhb25j.htm](pathfinder-bestiary-2-items/YXm2W59CMTMhb25j.htm)|Jaws|Fauces|modificada|
|[yxVTa3OmhUhlgNqp.htm](pathfinder-bestiary-2-items/yxVTa3OmhUhlgNqp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YyBAAbT6VQ7kpDW4.htm](pathfinder-bestiary-2-items/YyBAAbT6VQ7kpDW4.htm)|Jaws|Fauces|modificada|
|[YYnsAUH8Foo8uFnr.htm](pathfinder-bestiary-2-items/YYnsAUH8Foo8uFnr.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[yZCKHSiJwqO7N1sy.htm](pathfinder-bestiary-2-items/yZCKHSiJwqO7N1sy.htm)|Claws That Catch|Garras que atrapan|modificada|
|[yZgxFF5bgFKbTkUO.htm](pathfinder-bestiary-2-items/yZgxFF5bgFKbTkUO.htm)|Swallow Whole|Engullir Todo|modificada|
|[yZN2Ny1Owqd7mZr4.htm](pathfinder-bestiary-2-items/yZN2Ny1Owqd7mZr4.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[yZqrximA9jX4lr29.htm](pathfinder-bestiary-2-items/yZqrximA9jX4lr29.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Z08HAZUvnJBGvImG.htm](pathfinder-bestiary-2-items/Z08HAZUvnJBGvImG.htm)|Swarm Mind|Swarm Mind|modificada|
|[Z1OJoOqn3eSaMnv3.htm](pathfinder-bestiary-2-items/Z1OJoOqn3eSaMnv3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Z1uxEMSqDnZV9SCY.htm](pathfinder-bestiary-2-items/Z1uxEMSqDnZV9SCY.htm)|Glow|Glow|modificada|
|[z3NGGrVV1V5tnFUX.htm](pathfinder-bestiary-2-items/z3NGGrVV1V5tnFUX.htm)|Bodak Spawn|Bodak Spawn|modificada|
|[z3wK3ehfyd0WrBic.htm](pathfinder-bestiary-2-items/z3wK3ehfyd0WrBic.htm)|Scent|Scent|modificada|
|[Z487oTtboxJKexiF.htm](pathfinder-bestiary-2-items/Z487oTtboxJKexiF.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Z4dJO56WCJUBTpyf.htm](pathfinder-bestiary-2-items/Z4dJO56WCJUBTpyf.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Z5Ph0zkg0xMl6iK6.htm](pathfinder-bestiary-2-items/Z5Ph0zkg0xMl6iK6.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Z5SqMzsLOhD9YS8w.htm](pathfinder-bestiary-2-items/Z5SqMzsLOhD9YS8w.htm)|Jaws|Fauces|modificada|
|[Z60YSknQUrMbR4mB.htm](pathfinder-bestiary-2-items/Z60YSknQUrMbR4mB.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[z8CeXCEQdHlFMc0u.htm](pathfinder-bestiary-2-items/z8CeXCEQdHlFMc0u.htm)|Attack of Opportunity (Tentacle Only)|Ataque de oportunidad (sólo tentáculo)|modificada|
|[z8DPFDp5ksGcM6bT.htm](pathfinder-bestiary-2-items/z8DPFDp5ksGcM6bT.htm)|Constrict|Restringir|modificada|
|[Z8OBbNUnb8R13SCr.htm](pathfinder-bestiary-2-items/Z8OBbNUnb8R13SCr.htm)|Improved Grab|Agarrado mejorado|modificada|
|[zA4dkHLp5wB4ZlmE.htm](pathfinder-bestiary-2-items/zA4dkHLp5wB4ZlmE.htm)|Jaws|Fauces|modificada|
|[ZAKbxZPkQhVGL09z.htm](pathfinder-bestiary-2-items/ZAKbxZPkQhVGL09z.htm)|Telepathy|Telepatía|modificada|
|[zaLf6csWyorRHycA.htm](pathfinder-bestiary-2-items/zaLf6csWyorRHycA.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[zbGgj0YXNPQnaWxc.htm](pathfinder-bestiary-2-items/zbGgj0YXNPQnaWxc.htm)|Foot|Pie|modificada|
|[zbxStPzCzfkdF5cZ.htm](pathfinder-bestiary-2-items/zbxStPzCzfkdF5cZ.htm)|Claw|Garra|modificada|
|[zCBg1ZpignZBuhdE.htm](pathfinder-bestiary-2-items/zCBg1ZpignZBuhdE.htm)|Telepathy|Telepatía|modificada|
|[zcMAmOWmYshvH5lq.htm](pathfinder-bestiary-2-items/zcMAmOWmYshvH5lq.htm)|Grab|Agarrado|modificada|
|[ZCOJIf8Nr9RxA4Pj.htm](pathfinder-bestiary-2-items/ZCOJIf8Nr9RxA4Pj.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[ZcS1mFdodf2L8MND.htm](pathfinder-bestiary-2-items/ZcS1mFdodf2L8MND.htm)|Painsight|Painsight|modificada|
|[zcW44MHFdHDFSbQ5.htm](pathfinder-bestiary-2-items/zcW44MHFdHDFSbQ5.htm)|Jaws|Fauces|modificada|
|[ZD69ngyFRQ3FHnUr.htm](pathfinder-bestiary-2-items/ZD69ngyFRQ3FHnUr.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ZDDAw2k12iKwWmyi.htm](pathfinder-bestiary-2-items/ZDDAw2k12iKwWmyi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zDdJpAQ7FmnjLelY.htm](pathfinder-bestiary-2-items/zDdJpAQ7FmnjLelY.htm)|Jaws|Fauces|modificada|
|[ZdHLSjspChUCaebu.htm](pathfinder-bestiary-2-items/ZdHLSjspChUCaebu.htm)|Rhoka Sword|Espada Rhoka|modificada|
|[ZDqmS7tceuWsp4M1.htm](pathfinder-bestiary-2-items/ZDqmS7tceuWsp4M1.htm)|Claw|Garra|modificada|
|[zdW63lmShrSpZWkm.htm](pathfinder-bestiary-2-items/zdW63lmShrSpZWkm.htm)|Otherworldly Vision|Otherworldly Vision|modificada|
|[ZDynxQcPOYUqgupw.htm](pathfinder-bestiary-2-items/ZDynxQcPOYUqgupw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zE1qMdAfyFxOdrrK.htm](pathfinder-bestiary-2-items/zE1qMdAfyFxOdrrK.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[zEg1F3xL2SCNwVj7.htm](pathfinder-bestiary-2-items/zEg1F3xL2SCNwVj7.htm)|Weak Acid|Ácido débil|modificada|
|[zeyB4l2CYjcqiDrH.htm](pathfinder-bestiary-2-items/zeyB4l2CYjcqiDrH.htm)|Claw|Garra|modificada|
|[ZfwjAt3nb27vE9CP.htm](pathfinder-bestiary-2-items/ZfwjAt3nb27vE9CP.htm)|Tentacle|Tentáculo|modificada|
|[zgKk3c4d3SCNJutG.htm](pathfinder-bestiary-2-items/zgKk3c4d3SCNJutG.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[zglT7mSqeYfcswev.htm](pathfinder-bestiary-2-items/zglT7mSqeYfcswev.htm)|Hair Snare|Hair Snare|modificada|
|[Zh1eW0zEMgFfvRik.htm](pathfinder-bestiary-2-items/Zh1eW0zEMgFfvRik.htm)|Grab|Agarrado|modificada|
|[zhHzZbCi9Wd6r7hj.htm](pathfinder-bestiary-2-items/zhHzZbCi9Wd6r7hj.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[zhobc1PADu4qQ2LB.htm](pathfinder-bestiary-2-items/zhobc1PADu4qQ2LB.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[zhw2nm585TNwCDjL.htm](pathfinder-bestiary-2-items/zhw2nm585TNwCDjL.htm)|Telepathy|Telepatía|modificada|
|[zHWFkxssQtSSZH42.htm](pathfinder-bestiary-2-items/zHWFkxssQtSSZH42.htm)|Claw|Garra|modificada|
|[ZiB2QG6oo1w3v4H2.htm](pathfinder-bestiary-2-items/ZiB2QG6oo1w3v4H2.htm)|Aura of Protection|Aura de protección.|modificada|
|[zIgTjSPfr6rcIZod.htm](pathfinder-bestiary-2-items/zIgTjSPfr6rcIZod.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[ziM5W7Pshzb5HJ4L.htm](pathfinder-bestiary-2-items/ziM5W7Pshzb5HJ4L.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zJ48k7LkzzGzYZXF.htm](pathfinder-bestiary-2-items/zJ48k7LkzzGzYZXF.htm)|Paralysis|Parálisis|modificada|
|[ZJGndV534YfOPIyl.htm](pathfinder-bestiary-2-items/ZJGndV534YfOPIyl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zjVbARImBBAx6EKv.htm](pathfinder-bestiary-2-items/zjVbARImBBAx6EKv.htm)|Vile Touch|Toque Vil|modificada|
|[zJyMYGQVmegoEJJR.htm](pathfinder-bestiary-2-items/zJyMYGQVmegoEJJR.htm)|Horn Sweep|Barrido de Cuerno|modificada|
|[zKvBvpelIhRXPoSW.htm](pathfinder-bestiary-2-items/zKvBvpelIhRXPoSW.htm)|Thoughtsense|Percibir pensamientos|modificada|
|[zLE5uQwBNSB8wfkR.htm](pathfinder-bestiary-2-items/zLE5uQwBNSB8wfkR.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[zMgcdf4D6sTVSdON.htm](pathfinder-bestiary-2-items/zMgcdf4D6sTVSdON.htm)|Scent|Scent|modificada|
|[zMMs9T5D7z0TyE6H.htm](pathfinder-bestiary-2-items/zMMs9T5D7z0TyE6H.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ZMxrbMShYOtO3gEG.htm](pathfinder-bestiary-2-items/ZMxrbMShYOtO3gEG.htm)|Infiltration Tools|Herramientas de infiltración|modificada|
|[znBZYCY2qwfdxbV3.htm](pathfinder-bestiary-2-items/znBZYCY2qwfdxbV3.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[zNeDe02hwHzcyE1s.htm](pathfinder-bestiary-2-items/zNeDe02hwHzcyE1s.htm)|Squirming Embrace|Abrazo Retorcido|modificada|
|[znnYZfV4tw03wze8.htm](pathfinder-bestiary-2-items/znnYZfV4tw03wze8.htm)|Wing|Ala|modificada|
|[Znvln0bJmtXz7d7o.htm](pathfinder-bestiary-2-items/Znvln0bJmtXz7d7o.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZnwcqtkzrHYsNa9W.htm](pathfinder-bestiary-2-items/ZnwcqtkzrHYsNa9W.htm)|Jaws|Fauces|modificada|
|[zoC1ICbBEHFarVuk.htm](pathfinder-bestiary-2-items/zoC1ICbBEHFarVuk.htm)|Telepathy|Telepatía|modificada|
|[ZoL2vMSlQqYIUJAX.htm](pathfinder-bestiary-2-items/ZoL2vMSlQqYIUJAX.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[zp27Hiz99lnmpDcm.htm](pathfinder-bestiary-2-items/zp27Hiz99lnmpDcm.htm)|Constant Spells|Constant Spells|modificada|
|[ZPQhdXF3s5ZiNwMc.htm](pathfinder-bestiary-2-items/ZPQhdXF3s5ZiNwMc.htm)|Cinder|Cinder|modificada|
|[ZRC02nGn0m7GLUtZ.htm](pathfinder-bestiary-2-items/ZRC02nGn0m7GLUtZ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Zt3p0OLPOAG0F5xm.htm](pathfinder-bestiary-2-items/Zt3p0OLPOAG0F5xm.htm)|Grab|Agarrado|modificada|
|[zTHlNe6zIRCEQ1Gh.htm](pathfinder-bestiary-2-items/zTHlNe6zIRCEQ1Gh.htm)|Club|Club|modificada|
|[Zttbe7aDN1s4zibB.htm](pathfinder-bestiary-2-items/Zttbe7aDN1s4zibB.htm)|Regeneration 15 (deactivated by chaotic)|Regeneración 15 (desactivado por caótico)|modificada|
|[ZUHGleM4wU1ST5zV.htm](pathfinder-bestiary-2-items/ZUHGleM4wU1ST5zV.htm)|Trident|Trident|modificada|
|[zuum0VinuCKcAh3D.htm](pathfinder-bestiary-2-items/zuum0VinuCKcAh3D.htm)|Light Hammer|Martillo ligero|modificada|
|[zuY7RRh4sY8cLE7O.htm](pathfinder-bestiary-2-items/zuY7RRh4sY8cLE7O.htm)|Scent|Scent|modificada|
|[ZuY8xngBTZhm5ag1.htm](pathfinder-bestiary-2-items/ZuY8xngBTZhm5ag1.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zUZAQv67fQN1itGn.htm](pathfinder-bestiary-2-items/zUZAQv67fQN1itGn.htm)|Negative Healing|Curación negativa|modificada|
|[zvArmvrursLxbgGq.htm](pathfinder-bestiary-2-items/zvArmvrursLxbgGq.htm)|Gnaw Flesh|Roer Carne|modificada|
|[zVAwB8xvMUpj9BcX.htm](pathfinder-bestiary-2-items/zVAwB8xvMUpj9BcX.htm)|Jaws|Fauces|modificada|
|[Zvjunjzq6eA7veaV.htm](pathfinder-bestiary-2-items/Zvjunjzq6eA7veaV.htm)|Jaws|Fauces|modificada|
|[zvpqR9uOambi8VIJ.htm](pathfinder-bestiary-2-items/zvpqR9uOambi8VIJ.htm)|Jaws|Fauces|modificada|
|[ZvqnxVWvm9QdJZVi.htm](pathfinder-bestiary-2-items/ZvqnxVWvm9QdJZVi.htm)|Blood Scent|Blood Scent|modificada|
|[ZvUTgfQlDSMlOtEP.htm](pathfinder-bestiary-2-items/ZvUTgfQlDSMlOtEP.htm)|Entangling Tresses|Mechones enmarantes|modificada|
|[zVVCyjKNSDxZD2Oy.htm](pathfinder-bestiary-2-items/zVVCyjKNSDxZD2Oy.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[ZvvD1xFO55olkqpZ.htm](pathfinder-bestiary-2-items/ZvvD1xFO55olkqpZ.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[ZVXPGw1qzr4AJYst.htm](pathfinder-bestiary-2-items/ZVXPGw1qzr4AJYst.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZwbFx8gGrqv10yY5.htm](pathfinder-bestiary-2-items/ZwbFx8gGrqv10yY5.htm)|Foot|Pie|modificada|
|[zWcMhsfvgxLXuw4D.htm](pathfinder-bestiary-2-items/zWcMhsfvgxLXuw4D.htm)|Constrict|Restringir|modificada|
|[ZWe69IC3j34b3dqx.htm](pathfinder-bestiary-2-items/ZWe69IC3j34b3dqx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ZwOinMaTzmRM28rJ.htm](pathfinder-bestiary-2-items/ZwOinMaTzmRM28rJ.htm)|Shadow Scream|Grito de Sombra|modificada|
|[zwROdG6waBZIlf4v.htm](pathfinder-bestiary-2-items/zwROdG6waBZIlf4v.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Zxd709xPY5MAZhIj.htm](pathfinder-bestiary-2-items/Zxd709xPY5MAZhIj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zYBZpthuiwKFQuoR.htm](pathfinder-bestiary-2-items/zYBZpthuiwKFQuoR.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[zYVeJXwYqCDo3GsP.htm](pathfinder-bestiary-2-items/zYVeJXwYqCDo3GsP.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ZZAHwe62kuHct1sT.htm](pathfinder-bestiary-2-items/ZZAHwe62kuHct1sT.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zZd6BogevnvvWpQx.htm](pathfinder-bestiary-2-items/zZd6BogevnvvWpQx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[zZs4I9L8EkJWK9UP.htm](pathfinder-bestiary-2-items/zZs4I9L8EkJWK9UP.htm)|Swift Tracker|Rastreador rápido|modificada|
|[zzv3YiVwAfvq91jl.htm](pathfinder-bestiary-2-items/zzv3YiVwAfvq91jl.htm)|Fist|Puño|modificada|
|[ZzXV8so5F6pu1j3r.htm](pathfinder-bestiary-2-items/ZzXV8so5F6pu1j3r.htm)|Low-Light Vision|Visión con poca luz|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0D7BuUymOeEx7VEM.htm](pathfinder-bestiary-2-items/0D7BuUymOeEx7VEM.htm)|Athletics|vacía|
|[0DWjvOoTNOT0cP4O.htm](pathfinder-bestiary-2-items/0DWjvOoTNOT0cP4O.htm)|Stealth|vacía|
|[1gu2mF6EgXl6v3eM.htm](pathfinder-bestiary-2-items/1gu2mF6EgXl6v3eM.htm)|Crafting|vacía|
|[1n37HSEuc8iJMxun.htm](pathfinder-bestiary-2-items/1n37HSEuc8iJMxun.htm)|Soul Gem|vacía|
|[1yoCf7iLNmn7SD5D.htm](pathfinder-bestiary-2-items/1yoCf7iLNmn7SD5D.htm)|Stealth|vacía|
|[27VDcYxOuStCRZ4n.htm](pathfinder-bestiary-2-items/27VDcYxOuStCRZ4n.htm)|Sailing Lore|vacía|
|[2C1Wu7Y8e0BUVKgV.htm](pathfinder-bestiary-2-items/2C1Wu7Y8e0BUVKgV.htm)|One or more Lore skills related to a specific plane|vacía|
|[2I1FvVi46bCoWd1l.htm](pathfinder-bestiary-2-items/2I1FvVi46bCoWd1l.htm)|Gold Lore|vacía|
|[2Kexngs6KXliJ7qM.htm](pathfinder-bestiary-2-items/2Kexngs6KXliJ7qM.htm)|Torture Lore|vacía|
|[2wByf3NmPbP0YRzy.htm](pathfinder-bestiary-2-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|vacía|
|[3ekp1Wmg9yNIy9iX.htm](pathfinder-bestiary-2-items/3ekp1Wmg9yNIy9iX.htm)|Crafting|vacía|
|[3IKSOIIWyaxP7Xbh.htm](pathfinder-bestiary-2-items/3IKSOIIWyaxP7Xbh.htm)|Stealth|vacía|
|[48eItql3Drlle3Rb.htm](pathfinder-bestiary-2-items/48eItql3Drlle3Rb.htm)|Athletics|vacía|
|[4OYFMbbC38lWGl4D.htm](pathfinder-bestiary-2-items/4OYFMbbC38lWGl4D.htm)|Warfare Lore|vacía|
|[4s8ELNWXmsGfFB7V.htm](pathfinder-bestiary-2-items/4s8ELNWXmsGfFB7V.htm)|Astronomy Lore|vacía|
|[5H5M7ucca4W3guPv.htm](pathfinder-bestiary-2-items/5H5M7ucca4W3guPv.htm)|Abaddon Lore|vacía|
|[5ImSwYK5kXJKpZbH.htm](pathfinder-bestiary-2-items/5ImSwYK5kXJKpZbH.htm)|Gemstone|vacía|
|[6dch35LAYgze01qE.htm](pathfinder-bestiary-2-items/6dch35LAYgze01qE.htm)|Stealth|vacía|
|[7iLw2emMidGNxyzJ.htm](pathfinder-bestiary-2-items/7iLw2emMidGNxyzJ.htm)|Stealth|vacía|
|[7ovDTTZtTBqn2eJ6.htm](pathfinder-bestiary-2-items/7ovDTTZtTBqn2eJ6.htm)|Performance|vacía|
|[8czzhMhJOn9zi2gL.htm](pathfinder-bestiary-2-items/8czzhMhJOn9zi2gL.htm)|Sack of Rocks|vacía|
|[8EaMfqK0UpEoUqcg.htm](pathfinder-bestiary-2-items/8EaMfqK0UpEoUqcg.htm)|Acrobatics|vacía|
|[9E67q4C5Qrwk0OqK.htm](pathfinder-bestiary-2-items/9E67q4C5Qrwk0OqK.htm)|Survival|vacía|
|[9fV5FkAeZkNfBxHH.htm](pathfinder-bestiary-2-items/9fV5FkAeZkNfBxHH.htm)|Legal Lore|vacía|
|[bHdiDlV2uqMjzq0I.htm](pathfinder-bestiary-2-items/bHdiDlV2uqMjzq0I.htm)|Astronomy Lore|vacía|
|[BlCEwmbLQjpsyFMQ.htm](pathfinder-bestiary-2-items/BlCEwmbLQjpsyFMQ.htm)|Torture Lore|vacía|
|[bo6bsp0fKrJ9ddNK.htm](pathfinder-bestiary-2-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|vacía|
|[BxpAAUmfrga656x8.htm](pathfinder-bestiary-2-items/BxpAAUmfrga656x8.htm)|Sack of Rocks|vacía|
|[c4ByZjVeW0km1U7W.htm](pathfinder-bestiary-2-items/c4ByZjVeW0km1U7W.htm)|Stealth|vacía|
|[cbhhzA952sTW7Nlb.htm](pathfinder-bestiary-2-items/cbhhzA952sTW7Nlb.htm)|Stealth|vacía|
|[CdF08U1FHM2c2DIB.htm](pathfinder-bestiary-2-items/CdF08U1FHM2c2DIB.htm)|Athletics|vacía|
|[D5coiskeAG5F1V0f.htm](pathfinder-bestiary-2-items/D5coiskeAG5F1V0f.htm)|Lore (All)|vacía|
|[dmYyiqu2DVxTAvbq.htm](pathfinder-bestiary-2-items/dmYyiqu2DVxTAvbq.htm)|Torture Lore|vacía|
|[e3XyV4HjxKBZucFh.htm](pathfinder-bestiary-2-items/e3XyV4HjxKBZucFh.htm)|Stealth|vacía|
|[gfmpjwebcEUzSXoN.htm](pathfinder-bestiary-2-items/gfmpjwebcEUzSXoN.htm)|Stealth|vacía|
|[Hc2v6MiajU4rABkT.htm](pathfinder-bestiary-2-items/Hc2v6MiajU4rABkT.htm)|Rock|vacía|
|[HH2FpUMFgxmQCgXz.htm](pathfinder-bestiary-2-items/HH2FpUMFgxmQCgXz.htm)|Torture Lore|vacía|
|[HUJdrgu9JAWlm7bC.htm](pathfinder-bestiary-2-items/HUJdrgu9JAWlm7bC.htm)|Planar Lore|vacía|
|[hX2hdrvbUdBKffJD.htm](pathfinder-bestiary-2-items/hX2hdrvbUdBKffJD.htm)|Stealth|vacía|
|[Ic32ezDxqx8TwcBo.htm](pathfinder-bestiary-2-items/Ic32ezDxqx8TwcBo.htm)|Stealth|vacía|
|[ICtnIf0c4SkY2NnB.htm](pathfinder-bestiary-2-items/ICtnIf0c4SkY2NnB.htm)|Athletics|vacía|
|[ihi2vOTXGjRtjggj.htm](pathfinder-bestiary-2-items/ihi2vOTXGjRtjggj.htm)|Performance|vacía|
|[ii5QAwwyYnQHUwH2.htm](pathfinder-bestiary-2-items/ii5QAwwyYnQHUwH2.htm)|Athletics|vacía|
|[IP586mLtCybAAuh2.htm](pathfinder-bestiary-2-items/IP586mLtCybAAuh2.htm)|Steeped Weapon (7-10)|vacía|
|[iqXZDQJZ5iZnj2D0.htm](pathfinder-bestiary-2-items/iqXZDQJZ5iZnj2D0.htm)|Stealth|vacía|
|[j086JS96ezs39PtC.htm](pathfinder-bestiary-2-items/j086JS96ezs39PtC.htm)|Desert Lore|vacía|
|[jaF864bUYkmqmrC8.htm](pathfinder-bestiary-2-items/jaF864bUYkmqmrC8.htm)|Stealth|vacía|
|[jljKVq6TmhKOQUSh.htm](pathfinder-bestiary-2-items/jljKVq6TmhKOQUSh.htm)|Stealth|vacía|
|[jsZXrLmJJQhLrRL0.htm](pathfinder-bestiary-2-items/jsZXrLmJJQhLrRL0.htm)|Stealth|vacía|
|[JzkjkxNi7EFlzsmZ.htm](pathfinder-bestiary-2-items/JzkjkxNi7EFlzsmZ.htm)|Stealth|vacía|
|[khAWacLm5F3BV7QU.htm](pathfinder-bestiary-2-items/khAWacLm5F3BV7QU.htm)|Axis Lore|vacía|
|[kHGiew7m0qu0wSCx.htm](pathfinder-bestiary-2-items/kHGiew7m0qu0wSCx.htm)|Stealth|vacía|
|[kk0U4TyvtgBAzAAR.htm](pathfinder-bestiary-2-items/kk0U4TyvtgBAzAAR.htm)|Stealth|vacía|
|[kVbm9Ksg6rWgtnjS.htm](pathfinder-bestiary-2-items/kVbm9Ksg6rWgtnjS.htm)|Abyss Lore|vacía|
|[KwNXLXzskiVB9RLh.htm](pathfinder-bestiary-2-items/KwNXLXzskiVB9RLh.htm)|Stealth|vacía|
|[kynHGhmwcc8qJ5T8.htm](pathfinder-bestiary-2-items/kynHGhmwcc8qJ5T8.htm)|Stealth|vacía|
|[L0IZRmIosB8Icqo0.htm](pathfinder-bestiary-2-items/L0IZRmIosB8Icqo0.htm)|Athletics|vacía|
|[L3qJCQDZiZu5wjEW.htm](pathfinder-bestiary-2-items/L3qJCQDZiZu5wjEW.htm)|Stealth|vacía|
|[LCzHgVdDCfLFTsdX.htm](pathfinder-bestiary-2-items/LCzHgVdDCfLFTsdX.htm)|Axis Lore|vacía|
|[LTW5DHnhGB6HMte6.htm](pathfinder-bestiary-2-items/LTW5DHnhGB6HMte6.htm)|Stealth|vacía|
|[LUdvij5KTX4oc3yH.htm](pathfinder-bestiary-2-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|vacía|
|[MgWcgCTxwxAVsQWD.htm](pathfinder-bestiary-2-items/MgWcgCTxwxAVsQWD.htm)|Styx Lore|vacía|
|[mKdD7XKIAXJx8KnC.htm](pathfinder-bestiary-2-items/mKdD7XKIAXJx8KnC.htm)|Athletics|vacía|
|[NEQLZhd63z6qQPXJ.htm](pathfinder-bestiary-2-items/NEQLZhd63z6qQPXJ.htm)|Stealth|vacía|
|[neZCdGIJBKYWKN5Q.htm](pathfinder-bestiary-2-items/neZCdGIJBKYWKN5Q.htm)|Deception|vacía|
|[nm9xf6FWIfjkblnF.htm](pathfinder-bestiary-2-items/nm9xf6FWIfjkblnF.htm)|Forest Lore|vacía|
|[nwepl5Un9QYNK5Gk.htm](pathfinder-bestiary-2-items/nwepl5Un9QYNK5Gk.htm)|Crafting|vacía|
|[nWSidIDFacSNApv8.htm](pathfinder-bestiary-2-items/nWSidIDFacSNApv8.htm)|Stealth|vacía|
|[OOgOPxe66wl8FqEm.htm](pathfinder-bestiary-2-items/OOgOPxe66wl8FqEm.htm)|Athletics|vacía|
|[OQE9ap0Mj3pykdzk.htm](pathfinder-bestiary-2-items/OQE9ap0Mj3pykdzk.htm)|Acrobatics|vacía|
|[OT4mRkq8mC5ESE4d.htm](pathfinder-bestiary-2-items/OT4mRkq8mC5ESE4d.htm)|Stealth|vacía|
|[PRT7NwigGETAdytk.htm](pathfinder-bestiary-2-items/PRT7NwigGETAdytk.htm)|Torture Lore|vacía|
|[q0drSu1C5WWWuGEa.htm](pathfinder-bestiary-2-items/q0drSu1C5WWWuGEa.htm)|Genealogy Lore|vacía|
|[QaRhnU95T3Ie2dFP.htm](pathfinder-bestiary-2-items/QaRhnU95T3Ie2dFP.htm)|Stealth|vacía|
|[qjQkjjoCkG0wYEyP.htm](pathfinder-bestiary-2-items/qjQkjjoCkG0wYEyP.htm)|Soul Gem|vacía|
|[rET5SfCVJHTK7d5a.htm](pathfinder-bestiary-2-items/rET5SfCVJHTK7d5a.htm)|Athletics|vacía|
|[rs8cxUHAMnuhP7nv.htm](pathfinder-bestiary-2-items/rs8cxUHAMnuhP7nv.htm)|Stealth|vacía|
|[SAE0s9ZeUW0uDBDR.htm](pathfinder-bestiary-2-items/SAE0s9ZeUW0uDBDR.htm)|Conch Shell|vacía|
|[SEbaazZFQvxYSyns.htm](pathfinder-bestiary-2-items/SEbaazZFQvxYSyns.htm)|Gem Lore|vacía|
|[SlJZiCxXuBW155d5.htm](pathfinder-bestiary-2-items/SlJZiCxXuBW155d5.htm)|Athletics|vacía|
|[Sv1H3JJKwu64RfI1.htm](pathfinder-bestiary-2-items/Sv1H3JJKwu64RfI1.htm)|Athletics|vacía|
|[T2RZcW0Xl08THP7f.htm](pathfinder-bestiary-2-items/T2RZcW0Xl08THP7f.htm)|Games Lore|vacía|
|[T3B78udYCNXsT35C.htm](pathfinder-bestiary-2-items/T3B78udYCNXsT35C.htm)|Boneyard Lore|vacía|
|[tJgewoTin1AzHV0a.htm](pathfinder-bestiary-2-items/tJgewoTin1AzHV0a.htm)|Stealth|vacía|
|[Tml8KbX9Hdipnofj.htm](pathfinder-bestiary-2-items/Tml8KbX9Hdipnofj.htm)|Stealth|vacía|
|[TWwOYkc75X6kXsmZ.htm](pathfinder-bestiary-2-items/TWwOYkc75X6kXsmZ.htm)|Warfare Lore|vacía|
|[TZThNzsi8RUQFhrT.htm](pathfinder-bestiary-2-items/TZThNzsi8RUQFhrT.htm)|Stealth|vacía|
|[UaG0xt5nSwesLzma.htm](pathfinder-bestiary-2-items/UaG0xt5nSwesLzma.htm)|Stealth|vacía|
|[ujbUCBlQf3V81dRB.htm](pathfinder-bestiary-2-items/ujbUCBlQf3V81dRB.htm)|Torture Lore|vacía|
|[vcS0yHPna8VNFFeW.htm](pathfinder-bestiary-2-items/vcS0yHPna8VNFFeW.htm)|Elysium Lore|vacía|
|[ViIeviMg3Zbi0PdS.htm](pathfinder-bestiary-2-items/ViIeviMg3Zbi0PdS.htm)|Crafting|vacía|
|[vOVPKQFfOLMWV7F2.htm](pathfinder-bestiary-2-items/vOVPKQFfOLMWV7F2.htm)|Stealth|vacía|
|[w2BCKUoJdnaAE7ty.htm](pathfinder-bestiary-2-items/w2BCKUoJdnaAE7ty.htm)|Stealth|vacía|
|[We8X2oHUEJqX1ikE.htm](pathfinder-bestiary-2-items/We8X2oHUEJqX1ikE.htm)|Warfare Lore|vacía|
|[wmiM8DltOKYLxzz0.htm](pathfinder-bestiary-2-items/wmiM8DltOKYLxzz0.htm)|Stealth|vacía|
|[wtTc3hKG8EYcuNer.htm](pathfinder-bestiary-2-items/wtTc3hKG8EYcuNer.htm)|Sea Lore|vacía|
|[XhhI5NeGekRv5yOP.htm](pathfinder-bestiary-2-items/XhhI5NeGekRv5yOP.htm)|Stealth|vacía|
|[XIeSqagI3L9jCuKJ.htm](pathfinder-bestiary-2-items/XIeSqagI3L9jCuKJ.htm)|Crafting|vacía|
|[xPRIbGrSs4dfXGUK.htm](pathfinder-bestiary-2-items/xPRIbGrSs4dfXGUK.htm)|Soul Gem|vacía|
|[xrD7IZz445jQXJ8L.htm](pathfinder-bestiary-2-items/xrD7IZz445jQXJ8L.htm)|Athletics|vacía|
|[y7wtukvra9IyHCPL.htm](pathfinder-bestiary-2-items/y7wtukvra9IyHCPL.htm)|Boneyard Lore|vacía|
|[YjPhCJNyJk71S8rV.htm](pathfinder-bestiary-2-items/YjPhCJNyJk71S8rV.htm)|Warfare Lore|vacía|
|[ysZ3MQQymY3E22BQ.htm](pathfinder-bestiary-2-items/ysZ3MQQymY3E22BQ.htm)|Survival|vacía|
|[Z3rqV4GSnIVC3AJ3.htm](pathfinder-bestiary-2-items/Z3rqV4GSnIVC3AJ3.htm)|Sack of Rocks|vacía|
|[Zi1IXK4SmJsyEThu.htm](pathfinder-bestiary-2-items/Zi1IXK4SmJsyEThu.htm)|Athletics|vacía|
|[zie4H1Ei8xqTDKSd.htm](pathfinder-bestiary-2-items/zie4H1Ei8xqTDKSd.htm)|Boneyard Lore|vacía|
|[zsRr07XFkA5e8PQ3.htm](pathfinder-bestiary-2-items/zsRr07XFkA5e8PQ3.htm)|Stealth|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
