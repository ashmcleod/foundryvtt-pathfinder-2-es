# Estado de la traducción (hazards-items)

 * **modificada**: 79


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0fjRyLkdj1a2kfGM.htm](hazards-items/0fjRyLkdj1a2kfGM.htm)|Falling Scythes|Guadañas Caídas|modificada|
|[0FpzSygcs7G688GT.htm](hazards-items/0FpzSygcs7G688GT.htm)|Quietus|Quietus|modificada|
|[0KrOaCLFSEcIdcej.htm](hazards-items/0KrOaCLFSEcIdcej.htm)|Spear|Lanza|modificada|
|[17gJOHIRtWXfYMKa.htm](hazards-items/17gJOHIRtWXfYMKa.htm)|Sap Vitality|Sap Vitality|modificada|
|[1bvXbELRBoJTBqsR.htm](hazards-items/1bvXbELRBoJTBqsR.htm)|Spring|Primavera|modificada|
|[1M9OasbKWAPME9IC.htm](hazards-items/1M9OasbKWAPME9IC.htm)|Summon Monster|Invocar Monstruo|modificada|
|[34y07VryqAVeJhfG.htm](hazards-items/34y07VryqAVeJhfG.htm)|Slam Shut|Slam Shut|modificada|
|[3oSIIlS5PZwciZ3k.htm](hazards-items/3oSIIlS5PZwciZ3k.htm)|Poisoned Dart|Dardo Envenenado|modificada|
|[4QF1yNgj2FX7rO4C.htm](hazards-items/4QF1yNgj2FX7rO4C.htm)|Total Decapitation|Decapitación total|modificada|
|[6wV9i41Jd4HThaAi.htm](hazards-items/6wV9i41Jd4HThaAi.htm)|Speed|Velocidad|modificada|
|[8yNeU3TLczTIlFjP.htm](hazards-items/8yNeU3TLczTIlFjP.htm)|Lunging Dead|Acometer Muerto|modificada|
|[aJouqXgwCGFTWYHD.htm](hazards-items/aJouqXgwCGFTWYHD.htm)|Objects|Objetos|modificada|
|[aNYeaG5N3o5PQ9IB.htm](hazards-items/aNYeaG5N3o5PQ9IB.htm)|Dissolving Ambush|Emboscar disolvente|modificada|
|[B486xwl3ZYYCqNyw.htm](hazards-items/B486xwl3ZYYCqNyw.htm)|Spear|Lanza|modificada|
|[Bov9feGVYpn5nmUl.htm](hazards-items/Bov9feGVYpn5nmUl.htm)|Prelude|Preludio|modificada|
|[caUgV11OydLsLw1j.htm](hazards-items/caUgV11OydLsLw1j.htm)|Forbid Entry|Prohibir Entrada|modificada|
|[Cc30YjKZ7qSxVjSC.htm](hazards-items/Cc30YjKZ7qSxVjSC.htm)|Curse the Intruders|Maldecir a los Intrusos|modificada|
|[CHZ0RRKgpq6eI2AS.htm](hazards-items/CHZ0RRKgpq6eI2AS.htm)|Ensnare|Enredar|modificada|
|[ddB24NYRgK5YbbcR.htm](hazards-items/ddB24NYRgK5YbbcR.htm)|In the Beginning|In the Beginning|modificada|
|[DE7tTstJaGz9oAi4.htm](hazards-items/DE7tTstJaGz9oAi4.htm)|Rockslide|Rockslide|modificada|
|[E8X8ETVNwXuUF08C.htm](hazards-items/E8X8ETVNwXuUF08C.htm)|Searing Agony|Agonía abrasadora|modificada|
|[e90G0Brsf6uhlGp6.htm](hazards-items/e90G0Brsf6uhlGp6.htm)|Rend Magic|Magia Rasgadura|modificada|
|[eauYGISAGprcOwH4.htm](hazards-items/eauYGISAGprcOwH4.htm)|Devour|Devorar|modificada|
|[eLyrxG4My2vDsmHg.htm](hazards-items/eLyrxG4My2vDsmHg.htm)|No MAP|No MAP|modificada|
|[eTpo3PPNbe4sFYhD.htm](hazards-items/eTpo3PPNbe4sFYhD.htm)|Spore Explosion|Explosión de Esporas.|modificada|
|[eY731rvsYJZ2o5Sh.htm](hazards-items/eY731rvsYJZ2o5Sh.htm)|Flesset Poison|Veneno Flesset|modificada|
|[Ey8CknllSG5csxdG.htm](hazards-items/Ey8CknllSG5csxdG.htm)|Submerge|Sumergir|modificada|
|[F0I1pLJQm1AQtXPH.htm](hazards-items/F0I1pLJQm1AQtXPH.htm)|Rising Pillar|Pilar Creciente|modificada|
|[f9aVsy30iGDJTtb4.htm](hazards-items/f9aVsy30iGDJTtb4.htm)|Snap Shut|Snap Shut|modificada|
|[FzfgLXTt6R8bYmTP.htm](hazards-items/FzfgLXTt6R8bYmTP.htm)|Electrocution|Electrocución|modificada|
|[gxjS3APksgflCuua.htm](hazards-items/gxjS3APksgflCuua.htm)|Burst Free|Ráfaga Libre|modificada|
|[h1kUAmChVAo2xByF.htm](hazards-items/h1kUAmChVAo2xByF.htm)|Clockwork Fist|Puño Mecánico|modificada|
|[hEtVGo9EpKcXlhPI.htm](hazards-items/hEtVGo9EpKcXlhPI.htm)|Spectral Impale|Empalar espectral|modificada|
|[HhzDfHd26GkdqhME.htm](hazards-items/HhzDfHd26GkdqhME.htm)|Shock|Electrizante|modificada|
|[hK2a0NJwqQgjs15q.htm](hazards-items/hK2a0NJwqQgjs15q.htm)|Mark for Damnation|Mark for Damnation|modificada|
|[hkSWLZaKG41zef8w.htm](hazards-items/hkSWLZaKG41zef8w.htm)|Continuous Barrage|Andanada continua|modificada|
|[hoSnbd89PgLy85mP.htm](hazards-items/hoSnbd89PgLy85mP.htm)|Snowdrop|Snowdrop|modificada|
|[HoWRGPkBlJjsL9zl.htm](hazards-items/HoWRGPkBlJjsL9zl.htm)|Cladis Poison|Cladis Poison|modificada|
|[i9qsvHuq9IdeRohO.htm](hazards-items/i9qsvHuq9IdeRohO.htm)|Call of the Ground|Llamada, la|modificada|
|[iji2yCS44gpqc5p3.htm](hazards-items/iji2yCS44gpqc5p3.htm)|Pitfall|Trampa de caída|modificada|
|[j3QvHDCU1gvOInG4.htm](hazards-items/j3QvHDCU1gvOInG4.htm)|Deadfall|Deadfall|modificada|
|[JEXj6WkaqB0iscnL.htm](hazards-items/JEXj6WkaqB0iscnL.htm)|Reflection of Evil|Reflejo del maligno|modificada|
|[jSBfyDGUXAPbIJTT.htm](hazards-items/jSBfyDGUXAPbIJTT.htm)|Whirling Blades|Whirling Blades|modificada|
|[K7Ya0U5jerfvH0vw.htm](hazards-items/K7Ya0U5jerfvH0vw.htm)|Into the Great Beyond|Hacia el Gran Más Allá|modificada|
|[KaYeYfefttO5X9OF.htm](hazards-items/KaYeYfefttO5X9OF.htm)|Burn It All|¡Quemadlo todo!|modificada|
|[KFCwJINs8Uu4riRs.htm](hazards-items/KFCwJINs8Uu4riRs.htm)|Dart Volley|Volea de dardos|modificada|
|[lY83oUjx0DLxDByK.htm](hazards-items/lY83oUjx0DLxDByK.htm)|Pitfall|Trampa de caída|modificada|
|[LZTjCUaLv54zwB8W.htm](hazards-items/LZTjCUaLv54zwB8W.htm)|Web Noose|Web Noose|modificada|
|[mPVesRXUMN4Ws9IR.htm](hazards-items/mPVesRXUMN4Ws9IR.htm)|Decapitation|Decapitación|modificada|
|[MQ3CKYIdF6zrMg0x.htm](hazards-items/MQ3CKYIdF6zrMg0x.htm)|Leech Warmth|Calor de sanguijuela|modificada|
|[nCcx1LK7XrbDnVXI.htm](hazards-items/nCcx1LK7XrbDnVXI.htm)|Sportlebore Infestation|Infestación de sportlebore|modificada|
|[NcyiM0WihYrncbSF.htm](hazards-items/NcyiM0WihYrncbSF.htm)|Emit Cold|Emit Cold|modificada|
|[NjmHPcx6IEA14Yul.htm](hazards-items/NjmHPcx6IEA14Yul.htm)|Powder Burst|Ráfaga de pólvora|modificada|
|[nqcgUfOiRWy6Kti7.htm](hazards-items/nqcgUfOiRWy6Kti7.htm)|Scream|Grita|modificada|
|[oaNkRH35bkjdSm6P.htm](hazards-items/oaNkRH35bkjdSm6P.htm)|Scythe|Guadaña|modificada|
|[oZGdTS4jvoAj1myH.htm](hazards-items/oZGdTS4jvoAj1myH.htm)|Profane Chant|Canto Profano|modificada|
|[pLuxXYKq1Wi28Czn.htm](hazards-items/pLuxXYKq1Wi28Czn.htm)|Hammer|Martillo|modificada|
|[Pz2JfI4HEqH5iEwA.htm](hazards-items/Pz2JfI4HEqH5iEwA.htm)|Filth Fever|Filth Fever|modificada|
|[Q2074Gj6y8XUUgAk.htm](hazards-items/Q2074Gj6y8XUUgAk.htm)|Awaken|Awaken|modificada|
|[qBQpsAk64L9c0sJ2.htm](hazards-items/qBQpsAk64L9c0sJ2.htm)|Titanic Flytrap Toxin|Toxina atrapamoscas titánico|modificada|
|[QrgnumULVnz8pWfK.htm](hazards-items/QrgnumULVnz8pWfK.htm)|Unmask|Desenmascarar|modificada|
|[sBSa8YzXeDYM0ODl.htm](hazards-items/sBSa8YzXeDYM0ODl.htm)|Shriek|Shriek|modificada|
|[slQb54KbvJ8vJpRb.htm](hazards-items/slQb54KbvJ8vJpRb.htm)|Flume Activation|Flume Activación|modificada|
|[SoObyOgEreU0KLDs.htm](hazards-items/SoObyOgEreU0KLDs.htm)|Adrift in Time|A la deriva en el tiempo|modificada|
|[SvqJNCRH2eI1jZk1.htm](hazards-items/SvqJNCRH2eI1jZk1.htm)|Steam Blast|Ráfaga de vapor|modificada|
|[SvVSBExFgbjDrBvW.htm](hazards-items/SvVSBExFgbjDrBvW.htm)|Agitate|Agitar|modificada|
|[T0k2Rnyz49qG1Gkv.htm](hazards-items/T0k2Rnyz49qG1Gkv.htm)|Saw Blade|Hoja de sierra|modificada|
|[TxhvkFafggYWv5D7.htm](hazards-items/TxhvkFafggYWv5D7.htm)|Baleful Polymorph|polimorfismo funesto|modificada|
|[tyAeSBnHmP9Nuh2k.htm](hazards-items/tyAeSBnHmP9Nuh2k.htm)|Yellow Mold Spores|Esporas de Moho amarillo|modificada|
|[uJpg7vUJ1qag7R3t.htm](hazards-items/uJpg7vUJ1qag7R3t.htm)|Fireball|Bola de fuego|modificada|
|[V7tWWuB9NSEsMEae.htm](hazards-items/V7tWWuB9NSEsMEae.htm)|Special|Especial|modificada|
|[vFBoAHwlrVTu9wKm.htm](hazards-items/vFBoAHwlrVTu9wKm.htm)|Spine|Spine|modificada|
|[VNaaW87S24zzxXZh.htm](hazards-items/VNaaW87S24zzxXZh.htm)|Noose|Lazo|modificada|
|[vS5xxWVCLY1BNE4O.htm](hazards-items/vS5xxWVCLY1BNE4O.htm)|Spinning Blade|Spinning Blade|modificada|
|[wdA0nAhiSTY7znrs.htm](hazards-items/wdA0nAhiSTY7znrs.htm)|Shadow Barbs|Sombra Barbs|modificada|
|[WJtDPJzNrJO19H6k.htm](hazards-items/WJtDPJzNrJO19H6k.htm)|Infinite Pitfall|Trampa de caída infinita|modificada|
|[WleycA2DC7Pz8RYR.htm](hazards-items/WleycA2DC7Pz8RYR.htm)|Mimic Food|Mimic Food|modificada|
|[Yw05wWhZB9fLdatc.htm](hazards-items/Yw05wWhZB9fLdatc.htm)|Jaws|Fauces|modificada|
|[ZS4b3rYv9jOgbMzm.htm](hazards-items/ZS4b3rYv9jOgbMzm.htm)|Wheel Spin|Giro de la rueda|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
