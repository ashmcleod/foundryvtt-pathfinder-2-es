# Estado de la traducción (ancestryfeatures)

 * **modificada**: 38
 * **oficial**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[feat-00-8sxtjVsk9HBY5yAv.htm](ancestryfeatures/feat-00-8sxtjVsk9HBY5yAv.htm)|Glowing Horn|Cuerno Brillante|modificada|
|[feat-00-AUlPRySCqE6o6LHH.htm](ancestryfeatures/feat-00-AUlPRySCqE6o6LHH.htm)|Eyes in Back|Ojos en la Espalda|modificada|
|[feat-00-AzGJN1wwLFaLJIeo.htm](ancestryfeatures/feat-00-AzGJN1wwLFaLJIeo.htm)|Aquatic Adaptation|Adaptación acuática|modificada|
|[feat-00-BgHrucbZ9TH92RDv.htm](ancestryfeatures/feat-00-BgHrucbZ9TH92RDv.htm)|Sunlight Healing|Curación por luz solar|modificada|
|[feat-00-BHPDeqQHqi7ukCUW.htm](ancestryfeatures/feat-00-BHPDeqQHqi7ukCUW.htm)|Constructed (Poppet)|Construido (Poppet)|modificada|
|[feat-00-bHUrm79wJCcI7L3A.htm](ancestryfeatures/feat-00-bHUrm79wJCcI7L3A.htm)|Sunlight|Sunlight|modificada|
|[feat-00-dCp517IUFJk8JvQc.htm](ancestryfeatures/feat-00-dCp517IUFJk8JvQc.htm)|Fangs|Colmillos|modificada|
|[feat-00-dkZ8RxdQFJrdxwQo.htm](ancestryfeatures/feat-00-dkZ8RxdQFJrdxwQo.htm)|Revulsion|Revulsión|modificada|
|[feat-00-DRtaqOHXTRtGRIUT.htm](ancestryfeatures/feat-00-DRtaqOHXTRtGRIUT.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[feat-00-dtNsRAhCRfteA1ev.htm](ancestryfeatures/feat-00-dtNsRAhCRfteA1ev.htm)|Blunt Snout|Hocico romo|modificada|
|[feat-00-E28a45fUC2OkXZXY.htm](ancestryfeatures/feat-00-E28a45fUC2OkXZXY.htm)|Constructed Body|Cuerpo construido|modificada|
|[feat-00-egpiSWBrNBb1Fmig.htm](ancestryfeatures/feat-00-egpiSWBrNBb1Fmig.htm)|Draconic Exemplar|Ejemplar dracónico|modificada|
|[feat-00-Eyuqu6eIaoGCjnMv.htm](ancestryfeatures/feat-00-Eyuqu6eIaoGCjnMv.htm)|Clan Dagger|Daga de Clan|modificada|
|[feat-00-HHVQDp61ehcpdiU8.htm](ancestryfeatures/feat-00-HHVQDp61ehcpdiU8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[feat-00-IXyXCMBldrU5G60e.htm](ancestryfeatures/feat-00-IXyXCMBldrU5G60e.htm)|Innate Venom|Veneno Innato|modificada|
|[feat-00-jatezR4bENwhC6GL.htm](ancestryfeatures/feat-00-jatezR4bENwhC6GL.htm)|Bite|Muerdemuerde|modificada|
|[feat-00-jS8GamSTl8yMLL4F.htm](ancestryfeatures/feat-00-jS8GamSTl8yMLL4F.htm)|Fangs (Nagaji)|Colmillos (Nagaji)|modificada|
|[feat-00-kMgyOI4kBIEtFvhb.htm](ancestryfeatures/feat-00-kMgyOI4kBIEtFvhb.htm)|Swim|Nadar|modificada|
|[feat-00-mEDTJi7d1bTEiwUD.htm](ancestryfeatures/feat-00-mEDTJi7d1bTEiwUD.htm)|Unusual Anatomy|Anatomía inusual|modificada|
|[feat-00-mnhmhOKWLiOD0lev.htm](ancestryfeatures/feat-00-mnhmhOKWLiOD0lev.htm)|Constructed|Construido|modificada|
|[feat-00-N0zhJ0whkDJPlftl.htm](ancestryfeatures/feat-00-N0zhJ0whkDJPlftl.htm)|Photosynthesis|Fotosíntesis|modificada|
|[feat-00-NfkxFWUeG6g41e8w.htm](ancestryfeatures/feat-00-NfkxFWUeG6g41e8w.htm)|Claws|Garras|modificada|
|[feat-00-oCIO7UJqbpTkI62l.htm](ancestryfeatures/feat-00-oCIO7UJqbpTkI62l.htm)|Wings|Alas|modificada|
|[feat-00-PtvzTm2gjdCKao4I.htm](ancestryfeatures/feat-00-PtvzTm2gjdCKao4I.htm)|Prehensile Tail|Cola prensil|modificada|
|[feat-00-qJD3PJdoSXFrZEwr.htm](ancestryfeatures/feat-00-qJD3PJdoSXFrZEwr.htm)|Sharp Beak|Pico afilado|modificada|
|[feat-00-qKh6MxgE0cwde6mC.htm](ancestryfeatures/feat-00-qKh6MxgE0cwde6mC.htm)|Flammable|Inflamable|modificada|
|[feat-00-QyBfftocP1i43Qrp.htm](ancestryfeatures/feat-00-QyBfftocP1i43Qrp.htm)|Empathic Sense|Empathic Sense|modificada|
|[feat-00-R6rcqRsBR0KIho5n.htm](ancestryfeatures/feat-00-R6rcqRsBR0KIho5n.htm)|Change Shape (Anadi)|Cambiar Forma (Anadi)|modificada|
|[feat-00-RYrY7o0i6s7KW9io.htm](ancestryfeatures/feat-00-RYrY7o0i6s7KW9io.htm)|Automaton Core|Automaton Core|modificada|
|[feat-00-SAbzItAI4uwbdnQk.htm](ancestryfeatures/feat-00-SAbzItAI4uwbdnQk.htm)|Basic Undead Benefits|Beneficios básicos de muerto viviente|modificada|
|[feat-00-Sm3tKetM6kddTio3.htm](ancestryfeatures/feat-00-Sm3tKetM6kddTio3.htm)|Plant Nourishment|Alimentación Vegetal|modificada|
|[feat-00-TRw4oBZBFZG96jKO.htm](ancestryfeatures/feat-00-TRw4oBZBFZG96jKO.htm)|Magical Strikes|Golpes Mágicos|modificada|
|[feat-00-uSAYmU7PO2QoOWhB.htm](ancestryfeatures/feat-00-uSAYmU7PO2QoOWhB.htm)|Emotionally Unaware|Emotionally Unaware|modificada|
|[feat-00-vPhPgzpRjYDMT9Kq.htm](ancestryfeatures/feat-00-vPhPgzpRjYDMT9Kq.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[feat-00-vt67b8uoNEbskcBv.htm](ancestryfeatures/feat-00-vt67b8uoNEbskcBv.htm)|Hydration|Hidratación|modificada|
|[feat-00-y1EmCv2cEb5hXBwx.htm](ancestryfeatures/feat-00-y1EmCv2cEb5hXBwx.htm)|Keen Eyes|Ojos Afilados|modificada|
|[feat-00-YGk41WV42aTM7CQV.htm](ancestryfeatures/feat-00-YGk41WV42aTM7CQV.htm)|Advanced Undead Benefits|Beneficios avanzados de muerto viviente|modificada|
|[feat-00-Ymg6WqeJqOyLJLEr.htm](ancestryfeatures/feat-00-Ymg6WqeJqOyLJLEr.htm)|Change Shape (Kitsune)|Cambiar Forma (Kitsune)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[feat-00-sL1hHxrHdMNIZVAd.htm](ancestryfeatures/feat-00-sL1hHxrHdMNIZVAd.htm)|Land on Your Feet|Caer de pie|oficial|
