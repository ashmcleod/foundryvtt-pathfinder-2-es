# Estado de la traducción (heritages)

 * **modificada**: 200
 * **oficial**: 27


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0Iv6LfT3UEt8taj5.htm](heritages/0Iv6LfT3UEt8taj5.htm)|Warden Human (BB)|Warden Humano (BB)|modificada|
|[0TFf82gcfxXG9A54.htm](heritages/0TFf82gcfxXG9A54.htm)|Spellkeeper Shisk|Shisk guardián de los conjuros|modificada|
|[0vaeOoECfVD5EGbq.htm](heritages/0vaeOoECfVD5EGbq.htm)|Warrenbred Hobgoblin|Warrenbred Hobgoblin|modificada|
|[1dYDucCIaZpCJqBc.htm](heritages/1dYDucCIaZpCJqBc.htm)|Arctic Elf|Elfo ártico|modificada|
|[1wVDYY9Wue0G5R9Q.htm](heritages/1wVDYY9Wue0G5R9Q.htm)|Whisper Elf|Elfo de los susurros|modificada|
|[2cii5ZkBsJ4DYdd2.htm](heritages/2cii5ZkBsJ4DYdd2.htm)|Cactus Leshy|Cactus Leshy (s), leshys (pl.)|modificada|
|[2hLDilS6qbjHxgVS.htm](heritages/2hLDilS6qbjHxgVS.htm)|Dogtooth Tengu|Dogtooth Tengu|modificada|
|[2jsWmnKtidCTpaQV.htm](heritages/2jsWmnKtidCTpaQV.htm)|Venomshield Nagaji|Venomshield Nagaji|modificada|
|[2kMltxs2rmxRSxfV.htm](heritages/2kMltxs2rmxRSxfV.htm)|Hunter Automaton|Cazador Autómata|modificada|
|[2kSzKDtwbcILZTIe.htm](heritages/2kSzKDtwbcILZTIe.htm)|Snaptongue Grippli|Gripli lengualarga|modificada|
|[32oX6hHUY6K8N70Q.htm](heritages/32oX6hHUY6K8N70Q.htm)|Charhide Goblin|Goblin piel ignífuga|modificada|
|[3F5ffk7cmnrBhPcT.htm](heritages/3F5ffk7cmnrBhPcT.htm)|Liminal Catfolk|Liminal Catfolk|modificada|
|[3reGfXH0S82hM7Gp.htm](heritages/3reGfXH0S82hM7Gp.htm)|Ganzi|Ganzi|modificada|
|[5A1wMPdzN1OWE4cY.htm](heritages/5A1wMPdzN1OWE4cY.htm)|Caveclimber Kobold|Caveclimber Kobold|modificada|
|[5CqsBKCZuGON53Hk.htm](heritages/5CqsBKCZuGON53Hk.htm)|Forge Dwarf|Enano de la forja|modificada|
|[6JKdAZGa8odFzleS.htm](heritages/6JKdAZGa8odFzleS.htm)|Farsight Goloma|Goloma de vista penetrante|modificada|
|[6KhxY5ArGFhLF7r1.htm](heritages/6KhxY5ArGFhLF7r1.htm)|Thorned Rose|Thorned Rose|modificada|
|[6rIIsZg3tOyIU3g3.htm](heritages/6rIIsZg3tOyIU3g3.htm)|Frilled Lizardfolk|Frilled Lizardfolk|modificada|
|[6xxXtgj3fcCi53lt.htm](heritages/6xxXtgj3fcCi53lt.htm)|Sandstrider Lizardfolk|Sandstrider Lizardfolk|modificada|
|[7gGcpQMqnZhBDZLI.htm](heritages/7gGcpQMqnZhBDZLI.htm)|Adaptive Anadi|Anadi adaptable|modificada|
|[7kHg780SAsu2FNfP.htm](heritages/7kHg780SAsu2FNfP.htm)|Stuffed Poppet|Stuffed Poppet|modificada|
|[7lFPhRMAFXQsXUP2.htm](heritages/7lFPhRMAFXQsXUP2.htm)|Snow Rat|Rata de Nieve|modificada|
|[7wdeVadvchdM0aPK.htm](heritages/7wdeVadvchdM0aPK.htm)|Mistbreath Azarketi|Mistbreath Azarketi|modificada|
|[7ZDCShtRg5QZggrU.htm](heritages/7ZDCShtRg5QZggrU.htm)|Inured Azarketi|Inured Azarketi|modificada|
|[85tRKGZUTFa6pKpG.htm](heritages/85tRKGZUTFa6pKpG.htm)|Oathkeeper Dwarf|Enano Guardián del Juramento|modificada|
|[87h0jepQuzIbN7jN.htm](heritages/87h0jepQuzIbN7jN.htm)|Fungus Leshy|Leshy de los hongos|modificada|
|[8wGUh9RsMUamOKjh.htm](heritages/8wGUh9RsMUamOKjh.htm)|Tailed Goblin|Goblin con cola|modificada|
|[9Iu1gFEuvVz9zaYU.htm](heritages/9Iu1gFEuvVz9zaYU.htm)|Spined Azarketi|Spined Azarketi|modificada|
|[9mS8EGLlGUOzSAzP.htm](heritages/9mS8EGLlGUOzSAzP.htm)|Nightglider Strix|Nightglider Strix|modificada|
|[a6F2WjYU8D0suT8T.htm](heritages/a6F2WjYU8D0suT8T.htm)|Razortooth Goblin|Dientes dientes afilados de goblin|modificada|
|[AVZJI5wP2X5o0LL3.htm](heritages/AVZJI5wP2X5o0LL3.htm)|Trogloshi|Trogloshi|modificada|
|[B89BCo6LtI3SJq54.htm](heritages/B89BCo6LtI3SJq54.htm)|Sweetbreath Gnoll|Gnoll de aliento dulce|modificada|
|[BFOsMnWfXL1oaWkY.htm](heritages/BFOsMnWfXL1oaWkY.htm)|Steelskin Hobgoblin|Hobgoblin Piel de Acero|modificada|
|[BhbwjTFw2V67XF35.htm](heritages/BhbwjTFw2V67XF35.htm)|Keen-Venom Vishkanya|Afilada-Venom Vishkanya|modificada|
|[BjuZKA7lzFSjKbif.htm](heritages/BjuZKA7lzFSjKbif.htm)|Polyglot Android|Polyglot Android|modificada|
|[bKr34Uvxc2XClr9q.htm](heritages/bKr34Uvxc2XClr9q.htm)|Strong Oak|Roble Fuerte|modificada|
|[bmA9JK06rnOKpNLr.htm](heritages/bmA9JK06rnOKpNLr.htm)|Poisonhide Grippli|Gripli pielvenenosa|modificada|
|[By1y7HCfVPgX2GmI.htm](heritages/By1y7HCfVPgX2GmI.htm)|Ragdyan Vanara|Ragdyan Vanara|modificada|
|[CCwTBSNTw0caN1jd.htm](heritages/CCwTBSNTw0caN1jd.htm)|Mutated Fleshwarp|Mutated Fleshwarp|modificada|
|[cCy8vsZENlwiAyZ6.htm](heritages/cCy8vsZENlwiAyZ6.htm)|Twilight Halfling|Mediano del crepúsculoúsculo|modificada|
|[cDElVLonQvUK3vVk.htm](heritages/cDElVLonQvUK3vVk.htm)|Whipfang Nagaji|Whipfang Nagaji|modificada|
|[ClshvrjvBTMm3INX.htm](heritages/ClshvrjvBTMm3INX.htm)|Bandaagee Vanara|Bandaagee Vanara|modificada|
|[CMf0qluB0LXWReew.htm](heritages/CMf0qluB0LXWReew.htm)|Ancient Ash|Ceniza Antigua|modificada|
|[cnbwtbDmlD0KoLqY.htm](heritages/cnbwtbDmlD0KoLqY.htm)|Insightful Goloma|Goloma perspicaz|modificada|
|[CnCFZfuKzAYqy61e.htm](heritages/CnCFZfuKzAYqy61e.htm)|Athamasi|Athamasi|modificada|
|[Csezts78L4FMaskB.htm](heritages/Csezts78L4FMaskB.htm)|Lethoci|Lethoci|modificada|
|[Cv7BOjuziOQ0PO9r.htm](heritages/Cv7BOjuziOQ0PO9r.htm)|Windup Poppet|Windup Poppet|modificada|
|[cwOUw7kofcAiY01I.htm](heritages/cwOUw7kofcAiY01I.htm)|Snaring Anadi|Anadi atrapador|modificada|
|[CzOHITB2ihLGqMuJ.htm](heritages/CzOHITB2ihLGqMuJ.htm)|Runtboss Hobgoblin|Runtboss Hobgoblin|modificada|
|[CZx9HMmoOwcpkLY8.htm](heritages/CZx9HMmoOwcpkLY8.htm)|Root Leshy|Enraizarse Leshy (s), leshys (pl.)|modificada|
|[d0bNxgGqvaCkFlhN.htm](heritages/d0bNxgGqvaCkFlhN.htm)|Umbral Gnome|Gnomo sombrío umbral|modificada|
|[D3hTAqgwSank8OyO.htm](heritages/D3hTAqgwSank8OyO.htm)|Fey-Touched Gnome|Gnomo tocado por las feéricas|modificada|
|[d7NC4C19AgkspQQg.htm](heritages/d7NC4C19AgkspQQg.htm)|Battle-Trained Human (BB)|Humano entrenado en batalla (BB)|modificada|
|[daaXga11ov9YQVNq.htm](heritages/daaXga11ov9YQVNq.htm)|Polychromatic Anadi|Anadi policromático|modificada|
|[dJeiekfqGQ8dkwsO.htm](heritages/dJeiekfqGQ8dkwsO.htm)|Wetlander Lizardfolk|Wetlander Lizardfolk|modificada|
|[dQqurQys37aJYb26.htm](heritages/dQqurQys37aJYb26.htm)|Leaf Leshy|Leshy de las hojas|modificada|
|[dwKCwwtWetvPmJks.htm](heritages/dwKCwwtWetvPmJks.htm)|Rainfall Orc|Orco de lluvia|modificada|
|[EEvA4uj8h3zDiAfP.htm](heritages/EEvA4uj8h3zDiAfP.htm)|Treedweller Goblin|Treedweller Goblin|modificada|
|[EHDYVhJcZ9uPUjfZ.htm](heritages/EHDYVhJcZ9uPUjfZ.htm)|Toy Poppet|Toy Poppet|modificada|
|[EoWwvDdoMqN5x0c9.htm](heritages/EoWwvDdoMqN5x0c9.htm)|Rite of Light|Rito de la luz|modificada|
|[Eq42wZ5OTweJLnLU.htm](heritages/Eq42wZ5OTweJLnLU.htm)|Gutsy Halfling|Mediano con agallas|modificada|
|[etkuQkjkNLPLnjkA.htm](heritages/etkuQkjkNLPLnjkA.htm)|Wellspring Gnome|Gnomo manantial|modificada|
|[evXJISqyhl3fHE9u.htm](heritages/evXJISqyhl3fHE9u.htm)|Vine Leshy|Vine Leshy (s), leshys (pl.)|modificada|
|[faLb2rczsrxAuOTt.htm](heritages/faLb2rczsrxAuOTt.htm)|Rite of Knowing|Rito del conocimiento|modificada|
|[Fgysc0A1pFQE8PMA.htm](heritages/Fgysc0A1pFQE8PMA.htm)|Lorekeeper Shisk|Lorekeeper Shisk|modificada|
|[FLWUYM2XxYwnIDQf.htm](heritages/FLWUYM2XxYwnIDQf.htm)|Xyloshi|Xyloshi|modificada|
|[fWT7Mo2vFC10H4Wq.htm](heritages/fWT7Mo2vFC10H4Wq.htm)|Songbird Strix|Songbird Strix|modificada|
|[G8jfMayPv4vZvAVr.htm](heritages/G8jfMayPv4vZvAVr.htm)|Sylph|Sílfide|modificada|
|[G9Gwfi8ZIva52uGp.htm](heritages/G9Gwfi8ZIva52uGp.htm)|Jinxed Halfling|Jinxed Halfling|modificada|
|[GAn2cdhBE9Bqa85s.htm](heritages/GAn2cdhBE9Bqa85s.htm)|Beastkin|Beastkin|modificada|
|[gfXSF1TafBAmZo2u.htm](heritages/gfXSF1TafBAmZo2u.htm)|Grig|Grig|modificada|
|[gKLTlzAVapXTQ86V.htm](heritages/gKLTlzAVapXTQ86V.htm)|Reflection|Reflexión|modificada|
|[GlejQr3rBh3sn8sL.htm](heritages/GlejQr3rBh3sn8sL.htm)|River Azarketi|Río Azarketi|modificada|
|[Gmk7oNITvMVBy78Z.htm](heritages/Gmk7oNITvMVBy78Z.htm)|Undine|Undine|modificada|
|[GpnHIonrLN8TFZci.htm](heritages/GpnHIonrLN8TFZci.htm)|Rite of Invocation|Rito de la invocación|modificada|
|[gQyPU441J3rGt8mD.htm](heritages/gQyPU441J3rGt8mD.htm)|Snow Goblin|Goblin de la nieve|modificada|
|[gyoN45SVfRZwHMkk.htm](heritages/gyoN45SVfRZwHMkk.htm)|Irongut Goblin|Goblin tripas de hierro|modificada|
|[h2VKMYAlUIFAAXVG.htm](heritages/h2VKMYAlUIFAAXVG.htm)|Nyktera|Nyktera|modificada|
|[hFBwsVcSnNCJoimo.htm](heritages/hFBwsVcSnNCJoimo.htm)|Versatile Heritage|Herencia Versátil|modificada|
|[HFHSh2RWuxa4GhhQ.htm](heritages/HFHSh2RWuxa4GhhQ.htm)|Benthic Azarketi|Benthic Azarketi|modificada|
|[hTl3uc6y1kTuo9ac.htm](heritages/hTl3uc6y1kTuo9ac.htm)|Seaweed Leshy|Alga Leshy (s), leshys (pl.)|modificada|
|[hzW7VoRDYsKJB8ku.htm](heritages/hzW7VoRDYsKJB8ku.htm)|Surgewise Fleshwarp|Surgewise Fleshwarp|modificada|
|[idGDjqi1q3Ft8bAZ.htm](heritages/idGDjqi1q3Ft8bAZ.htm)|Nomadic Halfling|Mediano nómada|modificada|
|[ievKYUc53q0mroGp.htm](heritages/ievKYUc53q0mroGp.htm)|Lotus Leshy|Lotus Leshy (s), leshys (pl.)|modificada|
|[IFg2tqmAFFnU8UNU.htm](heritages/IFg2tqmAFFnU8UNU.htm)|Celestial Envoy Kitsune|Enviado Celestial Kitsune|modificada|
|[ikNJZRxUjcRLisO6.htm](heritages/ikNJZRxUjcRLisO6.htm)|Elfbane Hobgoblin|Elfbane Hobgoblin|modificada|
|[iOREr80Q0SsvPP8B.htm](heritages/iOREr80Q0SsvPP8B.htm)|Sacred Nagaji|Sagrado Nagaji|modificada|
|[isJhIPhT4MsjJvoq.htm](heritages/isJhIPhT4MsjJvoq.htm)|Fishseeker Shoony|Fishseeker Shoony|modificada|
|[ITgkqfnAOJCbcIys.htm](heritages/ITgkqfnAOJCbcIys.htm)|Oread|Oread|modificada|
|[iY2CCqoMc2bRdoas.htm](heritages/iY2CCqoMc2bRdoas.htm)|Created Fleshwarp|Elaborado Fleshwarp|modificada|
|[J0eAmntxXywr9sGt.htm](heritages/J0eAmntxXywr9sGt.htm)|Mage Automaton|Mago Autómata|modificada|
|[j0R1SyJP8k4G2Hkn.htm](heritages/j0R1SyJP8k4G2Hkn.htm)|Scavenger Strix|Scavenger Strix|modificada|
|[j1nzBc9Pui7vsJ9o.htm](heritages/j1nzBc9Pui7vsJ9o.htm)|Sharpshooter Automaton|Autómata Tirador|modificada|
|[Je15UGsLWYaaGJSW.htm](heritages/Je15UGsLWYaaGJSW.htm)|Ghost Poppet|Poppet Fantasma|modificada|
|[jEtVesbqYcWGbBYk.htm](heritages/jEtVesbqYcWGbBYk.htm)|Seer Elf|Elfo vidente|modificada|
|[K124fCpU03SJvmeP.htm](heritages/K124fCpU03SJvmeP.htm)|Warmarch Hobgoblin|Warmarch Hobgoblin|modificada|
|[k4AU5tjtngDOIqrB.htm](heritages/k4AU5tjtngDOIqrB.htm)|Deep Fetchling|Deep Fetchling|modificada|
|[KaokXdiE3ewXprdL.htm](heritages/KaokXdiE3ewXprdL.htm)|Pine Leshy|Pine Leshy (s), leshys (pl.)|modificada|
|[KbG2BZ3Sbr3xU1sW.htm](heritages/KbG2BZ3Sbr3xU1sW.htm)|Pixie|Pixie|modificada|
|[KcozzlkFAqShDEzo.htm](heritages/KcozzlkFAqShDEzo.htm)|Stronggut Shisk|Shisk tripasfuertes|modificada|
|[kHHcvJBJNiPJTuna.htm](heritages/kHHcvJBJNiPJTuna.htm)|Wisp Fetchling|Wisp Fetchling|modificada|
|[kHT9dFJt5yTjeYoB.htm](heritages/kHT9dFJt5yTjeYoB.htm)|Frozen Wind Kitsune|Kitsune Viento Helado|modificada|
|[kiKxnKd7Dfegk9dM.htm](heritages/kiKxnKd7Dfegk9dM.htm)|Desert Elf|Elfo del Desierto|modificada|
|[KJ2dSDXP9d5hJHzd.htm](heritages/KJ2dSDXP9d5hJHzd.htm)|Frightful Goloma|Goloma aterrador|modificada|
|[KO33MNyY9VqNQmbZ.htm](heritages/KO33MNyY9VqNQmbZ.htm)|Wintertouched Human|Humano Wintertouched|modificada|
|[Kq3k1Z6IWGVsLrmg.htm](heritages/Kq3k1Z6IWGVsLrmg.htm)|Lahkgyan Vanara|Lahkgyan Vanara|modificada|
|[kRDsVbhdBVeSlpBa.htm](heritages/kRDsVbhdBVeSlpBa.htm)|Anvil Dwarf|Enano Yunque|modificada|
|[kTlJqhC7ZSE8P8lu.htm](heritages/kTlJqhC7ZSE8P8lu.htm)|Venomous Anadi|Anadi venenoso|modificada|
|[kXp8qRh5AgtD4Izi.htm](heritages/kXp8qRh5AgtD4Izi.htm)|Witch Gnoll|Gnoll Brujo|modificada|
|[L6zfGzLMDLHbZ7VV.htm](heritages/L6zfGzLMDLHbZ7VV.htm)|Fruit Leshy|Fruit Leshy (s), leshys (pl.)|modificada|
|[lDT5h3f5GXRj42Ir.htm](heritages/lDT5h3f5GXRj42Ir.htm)|Stonestep Shisk|Shisk paso de piedra|modificada|
|[lj5iHaiY0IwCCptd.htm](heritages/lj5iHaiY0IwCCptd.htm)|Aphorite|Aforita|modificada|
|[LlUEmCDOLSZaGOyI.htm](heritages/LlUEmCDOLSZaGOyI.htm)|Titan Nagaji|Titan Nagaji|modificada|
|[Lp7ywxabmm88Gei6.htm](heritages/Lp7ywxabmm88Gei6.htm)|Observant Halfling|Halfling observador|modificada|
|[LU4i3qXtyzeTGWZQ.htm](heritages/LU4i3qXtyzeTGWZQ.htm)|Luminous Sprite|Sprite luminoso|modificada|
|[mBH1L01kYmB8EL56.htm](heritages/mBH1L01kYmB8EL56.htm)|Empty Sky Kitsune|Empty Sky Kitsune|modificada|
|[MeMAAtUlZmFgKSMF.htm](heritages/MeMAAtUlZmFgKSMF.htm)|Elemental Heart Dwarf|Enano de corazón elemental.|modificada|
|[MhXHEh7utEfxBwmc.htm](heritages/MhXHEh7utEfxBwmc.htm)|Thickcoat Shoony|Thickcoat Shoony|modificada|
|[Mj7uHxxVkRUlOFwJ.htm](heritages/Mj7uHxxVkRUlOFwJ.htm)|Hillock Halfling|Mediano de las colinas|modificada|
|[Mmezbef0c1fbJaVV.htm](heritages/Mmezbef0c1fbJaVV.htm)|Impersonator Android|Androide Imitador|modificada|
|[mnhpCk9dIwMuFegM.htm](heritages/mnhpCk9dIwMuFegM.htm)|Paddler Shoony|Remador Shoony|modificada|
|[MQx7miBXUmOHycqJ.htm](heritages/MQx7miBXUmOHycqJ.htm)|Laborer Android|Androide bracero|modificada|
|[MtH5bq0MhaMQbJEL.htm](heritages/MtH5bq0MhaMQbJEL.htm)|Murkeyed Azarketi|Murkeyed Azarketi|modificada|
|[MUujYQYWg6PNVaaN.htm](heritages/MUujYQYWg6PNVaaN.htm)|Predator Strix|Predator Strix|modificada|
|[mZqaKQkvadBbNubM.htm](heritages/mZqaKQkvadBbNubM.htm)|Compact Skeleton|Esqueleto Compacto|modificada|
|[n2DKA0OQQcfvZRly.htm](heritages/n2DKA0OQQcfvZRly.htm)|Technological Fleshwarp|Tecnológico Fleshwarp|modificada|
|[n2eJEjA8pnOMiuCm.htm](heritages/n2eJEjA8pnOMiuCm.htm)|Smokeworker Hobgoblin|Hobgoblin Trabajador del Humo|modificada|
|[N36ZR4lh9eCazDaN.htm](heritages/N36ZR4lh9eCazDaN.htm)|Half-Elf|Semielfo|modificada|
|[Nd9hdX8rdYyRozw8.htm](heritages/Nd9hdX8rdYyRozw8.htm)|Ancient Elf|Ancient Elf|modificada|
|[NfIAGatB1KIzt8G7.htm](heritages/NfIAGatB1KIzt8G7.htm)|Cavern Elf|Elfo de las cavernas|modificada|
|[nUCRd8tmz3C6LM0T.htm](heritages/nUCRd8tmz3C6LM0T.htm)|Wajaghand Vanara|Wajaghand Vanara|modificada|
|[nW1gi13E62Feto2w.htm](heritages/nW1gi13E62Feto2w.htm)|Woodland Elf|Elfo silvano|modificada|
|[NWbdAN5gDse0ad7C.htm](heritages/NWbdAN5gDse0ad7C.htm)|Dark Fields Kitsune|Kitsune de los Campos Oscuros|modificada|
|[NWsZ0cIeghyzk9bU.htm](heritages/NWsZ0cIeghyzk9bU.htm)|Sharp-Eared Catfolk|Sharp-Eared Catfolk|modificada|
|[nXQxlmjH24Eb8h2Q.htm](heritages/nXQxlmjH24Eb8h2Q.htm)|Battle-Ready Orc|Battle-Ready Orc|modificada|
|[OBxrlZKg0IC5n238.htm](heritages/OBxrlZKg0IC5n238.htm)|Venom-Resistant Vishkanya|Vishkanya resistente al veneno|modificada|
|[ohOJHeFenX97sBHf.htm](heritages/ohOJHeFenX97sBHf.htm)|Scalekeeper Vishkanya|Scalekeeper Vishkanya|modificada|
|[OoUqJJB77VfWbWRM.htm](heritages/OoUqJJB77VfWbWRM.htm)|Cliffscale Lizardfolk|Cliffscale Lizardfolk|modificada|
|[OtqOC3ElpF444qMe.htm](heritages/OtqOC3ElpF444qMe.htm)|Discarded Fleshwarp|Fleshwarp descartado|modificada|
|[P8Rl3dUsq8AzXLHC.htm](heritages/P8Rl3dUsq8AzXLHC.htm)|Sturdy Skeleton|Esqueleto Robusto|modificada|
|[PbXqlzRdQbKLQx1R.htm](heritages/PbXqlzRdQbKLQx1R.htm)|Old-Blood Vishkanya|Old-Blood Vishkanya|modificada|
|[pJ395g22dKNoufIK.htm](heritages/pJ395g22dKNoufIK.htm)|Nascent|Nascent|modificada|
|[PQuJEYI0UFl8W7fH.htm](heritages/PQuJEYI0UFl8W7fH.htm)|Cataphract Fleshwarp|Cataphract Fleshwarp|modificada|
|[PwxbD5VSJ0Yroqvp.htm](heritages/PwxbD5VSJ0Yroqvp.htm)|Liminal Fetchling|Liminal Fetchling|modificada|
|[pZ1u2ScWrBXSaAqQ.htm](heritages/pZ1u2ScWrBXSaAqQ.htm)|Winter Orc|Orco de Invierno|modificada|
|[q2omqJ9t0skGTYki.htm](heritages/q2omqJ9t0skGTYki.htm)|Rite of Reinforcement|Rito de refuerzo|modificada|
|[qbWaybAX1LK7kUyY.htm](heritages/qbWaybAX1LK7kUyY.htm)|Thalassic Azarketi|Thalassic Azarketi|modificada|
|[rFdVYKtHsZzRCsSd.htm](heritages/rFdVYKtHsZzRCsSd.htm)|Stickytoe Grippli|Gripli piespegajosos|modificada|
|[RKz7Z5pefXKiv9JE.htm](heritages/RKz7Z5pefXKiv9JE.htm)|Suli|Suli|modificada|
|[rQJBtQ9uKUzK9ktK.htm](heritages/rQJBtQ9uKUzK9ktK.htm)|Shortshanks Hobgoblin|Shortshanks Hobgoblin|modificada|
|[RuQSx0QsirIKxwKY.htm](heritages/RuQSx0QsirIKxwKY.htm)|Warrior Android|Androide combatiente|modificada|
|[RxNBBMFZwPA3Vlg3.htm](heritages/RxNBBMFZwPA3Vlg3.htm)|Shoreline Strix|Shoreline Strix|modificada|
|[RZHr0olieS6YdYE9.htm](heritages/RZHr0olieS6YdYE9.htm)|Fodder Skeleton|Fodder Skeleton|modificada|
|[S1062No0sYH35AhN.htm](heritages/S1062No0sYH35AhN.htm)|Tactile Azarketi|Tactile Azarketi|modificada|
|[sEnMG5zbnXdJvVPz.htm](heritages/sEnMG5zbnXdJvVPz.htm)|Cloudleaper Lizardfolk|Cloudleaper Lizardfolk|modificada|
|[sGzhnQpgWErX1bmx.htm](heritages/sGzhnQpgWErX1bmx.htm)|Vigilant Goloma|Goloma atento|modificada|
|[SqEcb1c3yeoJMxm0.htm](heritages/SqEcb1c3yeoJMxm0.htm)|Great Gnoll|Gran gnoll|modificada|
|[Svk2CHwvurK1QQhD.htm](heritages/Svk2CHwvurK1QQhD.htm)|Ifrit|Ifrit|modificada|
|[TDc9MXLXkgEFoKdD.htm](heritages/TDc9MXLXkgEFoKdD.htm)|Flexible Catfolk|Flexible Catfolk|modificada|
|[tf3edMCyS15GhFPx.htm](heritages/tf3edMCyS15GhFPx.htm)|Hooded Nagaji|Nagaji encapuchado|modificada|
|[tLd8Qg82AwEbbmgX.htm](heritages/tLd8Qg82AwEbbmgX.htm)|Half-Orc|Semiorco|modificada|
|[TQFE10VFvh9wb8zb.htm](heritages/TQFE10VFvh9wb8zb.htm)|Woodstalker Lizardfolk|Woodstalker Lizardfolk|modificada|
|[tXC5Gwn9D5x0ouJh.htm](heritages/tXC5Gwn9D5x0ouJh.htm)|Sensate Gnome|Gnomo sensible|modificada|
|[TYvzNoL5ldmB5F76.htm](heritages/TYvzNoL5ldmB5F76.htm)|Melixie|Melixie|modificada|
|[U882U2NUUGL6u3rL.htm](heritages/U882U2NUUGL6u3rL.htm)|Tunnel Rat|Rata de Túnel|modificada|
|[udMXXjFirjARYr4p.htm](heritages/udMXXjFirjARYr4p.htm)|Ant Gnoll|Gnoll hormiga|modificada|
|[ULj56ZoW7dWdnBvu.htm](heritages/ULj56ZoW7dWdnBvu.htm)|Unbreakable Goblin|Goblin irrompible|modificada|
|[UV2sABrTC5teOXTE.htm](heritages/UV2sABrTC5teOXTE.htm)|Strong-Blooded Dwarf|Enano de sangre fuerte|modificada|
|[uxtcKTkD62SmrUoh.htm](heritages/uxtcKTkD62SmrUoh.htm)|Shapewrought Fleshwarp|Shapewrought Fleshwarp|modificada|
|[VAo6NnrCEAAOUSkc.htm](heritages/VAo6NnrCEAAOUSkc.htm)|Resolute Fetchling|Resolute Fetchling|modificada|
|[vDEfNzjLpGJU54cz.htm](heritages/vDEfNzjLpGJU54cz.htm)|Quillcoat Shisk|Shisk abrigo de plumas|modificada|
|[VgL18yU7pysdoZZG.htm](heritages/VgL18yU7pysdoZZG.htm)|Artisan Android|Artesano Android|modificada|
|[ViKRoVgog172r163.htm](heritages/ViKRoVgog172r163.htm)|Vivacious Gnome|Gnomo Vivaz|modificada|
|[VqgrYMaAwnNjT9Mn.htm](heritages/VqgrYMaAwnNjT9Mn.htm)|Enchanting Lily|Lirio encantador|modificada|
|[vrU3lmDO7FYzmuQc.htm](heritages/vrU3lmDO7FYzmuQc.htm)|Prismatic Vishkanya|Prismatic Vishkanya|modificada|
|[VSyOvtgJ1ZNpIVgC.htm](heritages/VSyOvtgJ1ZNpIVgC.htm)|Rock Dwarf|Enano de la roca|modificada|
|[VTtXwBxrfRUXSL38.htm](heritages/VTtXwBxrfRUXSL38.htm)|Death Warden Dwarf|Enano guardián de la muerte|modificada|
|[VvEAFoxuddYNBmNc.htm](heritages/VvEAFoxuddYNBmNc.htm)|Grave Orc|Grave Orc|modificada|
|[VYfpTUuXJM3iBOz0.htm](heritages/VYfpTUuXJM3iBOz0.htm)|Unseen Lizardfolk|Unseen Lizardfolk|modificada|
|[w5801ArZQCU8IXnU.htm](heritages/w5801ArZQCU8IXnU.htm)|Wishborn Poppet|Wishborn Poppet|modificada|
|[wB8xiQB4RDbzOOvR.htm](heritages/wB8xiQB4RDbzOOvR.htm)|Monstrous Skeleton|Esqueleto Monstruoso|modificada|
|[WEzgrxBRFBGdj8Hx.htm](heritages/WEzgrxBRFBGdj8Hx.htm)|Wavediver Tengu|Wavediver Tengu|modificada|
|[wHO5luJMODbDLXNi.htm](heritages/wHO5luJMODbDLXNi.htm)|Bright Fetchling|Bright Fetchling|modificada|
|[Wk4HyaZtC1j221i1.htm](heritages/Wk4HyaZtC1j221i1.htm)|Earthly Wilds Kitsune|Kitsune de las Tierras Salvajes|modificada|
|[wNnsjird4OQe0s6p.htm](heritages/wNnsjird4OQe0s6p.htm)|Gourd Leshy|Leshy de los frutos de la calabaza|modificada|
|[WxcbLvufI6JBpLt0.htm](heritages/WxcbLvufI6JBpLt0.htm)|Spindly Anadi|Anadi larguirucho|modificada|
|[x5S4MNQ0aqUmgHcC.htm](heritages/x5S4MNQ0aqUmgHcC.htm)|Vicious Goloma|Goloma feroz|modificada|
|[xaTTN5anLEBzWCzv.htm](heritages/xaTTN5anLEBzWCzv.htm)|Windweb Grippli|Gripli atrapabrisas|modificada|
|[XeXWsvcWU3Zaj5WC.htm](heritages/XeXWsvcWU3Zaj5WC.htm)|Chameleon Gnome|Gnomo camaleón|modificada|
|[xtRIYizCjLg9qe1Z.htm](heritages/xtRIYizCjLg9qe1Z.htm)|Wildwood Halfling|Mediano de las florestas salvajes|modificada|
|[y24ykEUfpIu5Gp6D.htm](heritages/y24ykEUfpIu5Gp6D.htm)|Warrior Automaton|Autómata combatiente|modificada|
|[yL6944LrPo2HNdEJ.htm](heritages/yL6944LrPo2HNdEJ.htm)|Ancient-Blooded Dwarf|Enano de sangre antigua|modificada|
|[yVtcyAbLmWCIHHZi.htm](heritages/yVtcyAbLmWCIHHZi.htm)|Rite of Passage|Rito de paso|modificada|
|[ywNXVLZtwrAStyh1.htm](heritages/ywNXVLZtwrAStyh1.htm)|Elusive Vishkanya|Elusive Vishkanya|modificada|
|[z4cvqtpkkAYoFpHa.htm](heritages/z4cvqtpkkAYoFpHa.htm)|Bloodhound Shoony|Bloodhound Shoony|modificada|
|[zcO93E8gAW1tDYKk.htm](heritages/zcO93E8gAW1tDYKk.htm)|Draxie|Draxie|modificada|
|[zPhArF36ZVgLeVUU.htm](heritages/zPhArF36ZVgLeVUU.htm)|Ancient Scale Azarketi|Ancient Scale Azarketi|modificada|
|[zVf0Hlp5xG0Q7kmc.htm](heritages/zVf0Hlp5xG0Q7kmc.htm)|Skilled Heritage|Herencia hábil|modificada|
|[ZW8GX14n3ZGievK1.htm](heritages/ZW8GX14n3ZGievK1.htm)|Tunnelflood Kobold|Kobold de túnel|modificada|
|[ZZKZkeSP5TuT62IA.htm](heritages/ZZKZkeSP5TuT62IA.htm)|Duskwalker|Crepuscular|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[1lv7RMp7t5iqeUFT.htm](heritages/1lv7RMp7t5iqeUFT.htm)|Hunting Catfolk|Félido cazador|oficial|
|[1oLMOmLpurfWTTff.htm](heritages/1oLMOmLpurfWTTff.htm)|Aasimar|Aasimar|oficial|
|[35k2aujXYvqUCSS1.htm](heritages/35k2aujXYvqUCSS1.htm)|Cavern Kobold|Kobold de las cavernas|oficial|
|[6dMd4JG0ndrObEUj.htm](heritages/6dMd4JG0ndrObEUj.htm)|Winter Catfolk|Félido invernal|oficial|
|[7p9HtLzWBHc18JDW.htm](heritages/7p9HtLzWBHc18JDW.htm)|Deep Rat|Rata de las profundidades|oficial|
|[7vHLPleFpSqKAjWG.htm](heritages/7vHLPleFpSqKAjWG.htm)|Stormtossed Tengu|Tengu arrojado por la tormenta|oficial|
|[8Gsa8KFsHizEwSHU.htm](heritages/8Gsa8KFsHizEwSHU.htm)|Badlands Orc|Orco de las tierras baldías|oficial|
|[bLhIBwqdjTiVJ5qm.htm](heritages/bLhIBwqdjTiVJ5qm.htm)|Clawed Catfolk|Félido con garras|oficial|
|[eFsD7W6hnK33jlDQ.htm](heritages/eFsD7W6hnK33jlDQ.htm)|Sewer Rat|Rata de alcantarilla|oficial|
|[EKY9v7SF1hVsUdbH.htm](heritages/EKY9v7SF1hVsUdbH.htm)|Changeling|Changeling|oficial|
|[fROPRHGyUn4PgcER.htm](heritages/fROPRHGyUn4PgcER.htm)|Longsnout Rat|Rata de hocico largo|oficial|
|[g4FRxyuHndZu4KTo.htm](heritages/g4FRxyuHndZu4KTo.htm)|Jinxed Tengu|Tengu maldito|oficial|
|[hOPOyyt7qZXYYCOU.htm](heritages/hOPOyyt7qZXYYCOU.htm)|Nine Lives Catfolk|Félido de nueve vidas|oficial|
|[HpqQ5VQ0w4HqYgVC.htm](heritages/HpqQ5VQ0w4HqYgVC.htm)|Jungle Catfolk|Félido selvático|oficial|
|[m9rrlchS10xHFA2G.htm](heritages/m9rrlchS10xHFA2G.htm)|Venomtail Kobold|Kóbold de cola venenosa|oficial|
|[MTTU2t7x6TjvUDnE.htm](heritages/MTTU2t7x6TjvUDnE.htm)|Hold-Scarred Orc|Orco marcado por el clan|oficial|
|[P8BP1un5BTrwXoBy.htm](heritages/P8BP1un5BTrwXoBy.htm)|Dragonscaled Kobold|Kobold de escamas de dragón|oficial|
|[ptpK6H1rM4Bu3ry4.htm](heritages/ptpK6H1rM4Bu3ry4.htm)|Mountainkeeper Tengu|Tengu montañero|oficial|
|[qM566kCXljkOpocA.htm](heritages/qM566kCXljkOpocA.htm)|Taloned Tengu|Tengu con garras|oficial|
|[rKV11HWREwjjMIum.htm](heritages/rKV11HWREwjjMIum.htm)|Skyborn Tengu|Tengu nacido del cielo|oficial|
|[tarfuEXmi0E0Enfy.htm](heritages/tarfuEXmi0E0Enfy.htm)|Shadow Rat|Rata de las sombras|oficial|
|[twayjFuXbsvyHUwy.htm](heritages/twayjFuXbsvyHUwy.htm)|Desert Rat|Rata del Desierto|oficial|
|[UaD5VDoFlILEmbyz.htm](heritages/UaD5VDoFlILEmbyz.htm)|Dhampir|Dhampiro|oficial|
|[VRyX00OuPGsJSurM.htm](heritages/VRyX00OuPGsJSurM.htm)|Spellscale Kobold|Kóbold escama de conjuros|oficial|
|[WaCn0mcivFv1omNK.htm](heritages/WaCn0mcivFv1omNK.htm)|Strongjaw Kobold|Kóbold de mandíobula fuerte|oficial|
|[wGMVflH4t1UXrNn5.htm](heritages/wGMVflH4t1UXrNn5.htm)|Tiefling|Tiflin|oficial|
|[wn4EbYk1QN3tyFhh.htm](heritages/wn4EbYk1QN3tyFhh.htm)|Deep Orc|Orco de las profundidades|oficial|
