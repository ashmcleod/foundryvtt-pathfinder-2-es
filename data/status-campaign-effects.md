# Estado de la traducción (campaign-effects)

 * **modificada**: 23
 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[W8MD8WCjVC4pLlqn.htm](campaign-effects/W8MD8WCjVC4pLlqn.htm)|Effect: Heart Break|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|Efecto: Reflejos mejorados|modificada|
|[5DBYunGL2HV8aekO.htm](campaign-effects/5DBYunGL2HV8aekO.htm)|Resonant Reflection: Reflection of Stone|Reflexión Resonante: Reflejo de Piedra|modificada|
|[8eMGYNilWV3dFAUI.htm](campaign-effects/8eMGYNilWV3dFAUI.htm)|Resonant Reflection: Reflection of Life|Reflejo Resonante: Reflejo de Vida|modificada|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|Mixed Drink: Orientación divina|modificada|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|Efecto: Boca y Garganta Quemadas (No Cantar ni Gritar)|modificada|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|Efecto: Reflejo de Vida (Curación Rápida)|modificada|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|Mixed Drink: Suerte|modificada|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|Bebida Mezclada: Prestidigitación|modificada|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|Mixed Drink: Luces Danzantes|modificada|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|Efecto: Calambres estomacales extremos|modificada|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|Efecto: Lengua Quemada (Lingüístico)|modificada|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|Efecto: Esperanza o Desesperación (Éxito Crítico)|modificada|
|[NwF6m7SyaqwjdQxT.htm](campaign-effects/NwF6m7SyaqwjdQxT.htm)|Effect: Manipulate Luck - Drusilla (Good)|Efecto: Manipular la suerte - Drusilla (Bien)|modificada|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|Efecto: Boca y garganta quemadas (Lingüístico).|modificada|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|Efecto: Esperanza o Desesperación (Fallo o Fallo Crítico).|modificada|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|Efecto: Dolor de cabeza inmediato e intenso.|modificada|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened awareness|Efecto: Conciencia aumentada|modificada|
|[scgEAXAqHEr3Egeb.htm](campaign-effects/scgEAXAqHEr3Egeb.htm)|Resonant Reflection: Reflection of Water|Reflejo Resonante: Reflejo del agua|modificada|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|Bendición de Geb|modificada|
|[uDQw7YPMiKPXCbaV.htm](campaign-effects/uDQw7YPMiKPXCbaV.htm)|Resonant Reflection: Reflection of Light|Reflejo Resonante: Reflejo de Luz|modificada|
|[UXBOvlJbnI76UoGp.htm](campaign-effects/UXBOvlJbnI76UoGp.htm)|Resonant Reflection: Reflection of Storm|Reflexión Resonante: Reflejo de Tormenta|modificada|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|Efecto: Lengua quemada (No se puede cantar ni gritar).|modificada|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|Efecto: Afilada perspicacia|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
