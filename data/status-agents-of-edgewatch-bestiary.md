# Estado de la traducción (agents-of-edgewatch-bestiary)

 * **modificada**: 187


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[07AGJt4ZRjwH85Xp.htm](agents-of-edgewatch-bestiary/07AGJt4ZRjwH85Xp.htm)|Mother Venom|Mother Venom|modificada|
|[0ti3f4fdcB5D2bLB.htm](agents-of-edgewatch-bestiary/0ti3f4fdcB5D2bLB.htm)|Casino Bouncer|Casino Bouncer|modificada|
|[0UbehYHzOGlNK8Hc.htm](agents-of-edgewatch-bestiary/0UbehYHzOGlNK8Hc.htm)|Baatamidar|Baatamidar|modificada|
|[10fEM7T48FUZRo6l.htm](agents-of-edgewatch-bestiary/10fEM7T48FUZRo6l.htm)|Barnacle Ghoul|Barnacle Ghoul|modificada|
|[181ucNY1zpp2Lz3x.htm](agents-of-edgewatch-bestiary/181ucNY1zpp2Lz3x.htm)|Grunka|Grunka|modificada|
|[1bPc2rjR4MghbMwD.htm](agents-of-edgewatch-bestiary/1bPc2rjR4MghbMwD.htm)|Obrousian|Obrousian|modificada|
|[1eMDYXXf2leLTYHV.htm](agents-of-edgewatch-bestiary/1eMDYXXf2leLTYHV.htm)|Eberark|Eberark|modificada|
|[1G4OdEHRPF8GMHK8.htm](agents-of-edgewatch-bestiary/1G4OdEHRPF8GMHK8.htm)|Amateur Chemist|Químico aficionado|modificada|
|[1UIJ4oknQ7btkUCb.htm](agents-of-edgewatch-bestiary/1UIJ4oknQ7btkUCb.htm)|Plunger Chute|Desatascador|modificada|
|[1UtsScuNjmgwMZRn.htm](agents-of-edgewatch-bestiary/1UtsScuNjmgwMZRn.htm)|Excorion Paragon|Excorion Paragon|modificada|
|[1xGAktpj6N2Ugh0r.htm](agents-of-edgewatch-bestiary/1xGAktpj6N2Ugh0r.htm)|Shatterling|Shatterling|modificada|
|[2uxm1SxZXaG0ynCp.htm](agents-of-edgewatch-bestiary/2uxm1SxZXaG0ynCp.htm)|Pulping Golem|Golem despulpador|modificada|
|[3GNq4Vy40vk9zwMQ.htm](agents-of-edgewatch-bestiary/3GNq4Vy40vk9zwMQ.htm)|Graem|Graem|modificada|
|[3If8bjsXeA2413dB.htm](agents-of-edgewatch-bestiary/3If8bjsXeA2413dB.htm)|Hegessik|Hegessik|modificada|
|[3JEiwAFwkEjCDEYa.htm](agents-of-edgewatch-bestiary/3JEiwAFwkEjCDEYa.htm)|Kolo Harvan|Kolo Harvan|modificada|
|[3LLHyUz0Nabuw46L.htm](agents-of-edgewatch-bestiary/3LLHyUz0Nabuw46L.htm)|Boiling Fountains|Fuentes Hirvientes|modificada|
|[3XbjmNeUtPLzxDge.htm](agents-of-edgewatch-bestiary/3XbjmNeUtPLzxDge.htm)|Penqual|Penqual|modificada|
|[4BXo2a305RHmspMX.htm](agents-of-edgewatch-bestiary/4BXo2a305RHmspMX.htm)|Twisted Jack|Twisted Jack|modificada|
|[4ffoPNBKdEwBmYgL.htm](agents-of-edgewatch-bestiary/4ffoPNBKdEwBmYgL.htm)|Battle Leader Rekarek|Líder de batalla Rekarek|modificada|
|[4TB1eo3O22khMyDU.htm](agents-of-edgewatch-bestiary/4TB1eo3O22khMyDU.htm)|Sad Liza|Sad Liza|modificada|
|[54i6ithgmhDk7Dne.htm](agents-of-edgewatch-bestiary/54i6ithgmhDk7Dne.htm)|Rigged Cubby|Rigged Cubby|modificada|
|[5pBr5aWUb7yGCqN7.htm](agents-of-edgewatch-bestiary/5pBr5aWUb7yGCqN7.htm)|Washboard Dog Tough|Washboard Dog Dureza|modificada|
|[5u7luLMeRNJ4en65.htm](agents-of-edgewatch-bestiary/5u7luLMeRNJ4en65.htm)|Bone Skipper Swarm|Bone Skipper Swarm|modificada|
|[64TX3n5xufgjqbwK.htm](agents-of-edgewatch-bestiary/64TX3n5xufgjqbwK.htm)|Poisoned Dart Statue|Estatua Dardo Envenenado|modificada|
|[6c5CnrSxMYEgP6Fz.htm](agents-of-edgewatch-bestiary/6c5CnrSxMYEgP6Fz.htm)|Kekker|Kekker|modificada|
|[7bMhDg4DNqoPlRsF.htm](agents-of-edgewatch-bestiary/7bMhDg4DNqoPlRsF.htm)|Avarek|Avarek|modificada|
|[7FJ3SQuHOUcGjm1x.htm](agents-of-edgewatch-bestiary/7FJ3SQuHOUcGjm1x.htm)|Eyeball Tank|Tanque globo ocular|modificada|
|[7LN8clGKJWTpxISR.htm](agents-of-edgewatch-bestiary/7LN8clGKJWTpxISR.htm)|Blackfingers Acolyte|Acólito de los Dedos Negros|modificada|
|[7Q35HATCilOb1xXX.htm](agents-of-edgewatch-bestiary/7Q35HATCilOb1xXX.htm)|Living Paints|Pinturas Vivas|modificada|
|[82wJQmzPlKttl0sc.htm](agents-of-edgewatch-bestiary/82wJQmzPlKttl0sc.htm)|Clockwork Arms|Clockwork Arms|modificada|
|[8AdsRaXftoR1beTk.htm](agents-of-edgewatch-bestiary/8AdsRaXftoR1beTk.htm)|Frefferth|Frefferth|modificada|
|[8eBXEszbl4gOHGdU.htm](agents-of-edgewatch-bestiary/8eBXEszbl4gOHGdU.htm)|Wynsal Starborn|Wynsal Starborn|modificada|
|[8GQ7dq7s9CetOlkg.htm](agents-of-edgewatch-bestiary/8GQ7dq7s9CetOlkg.htm)|Gang Tough|Dureza de Pandilla|modificada|
|[8HTPdiH6yEk0jlNF.htm](agents-of-edgewatch-bestiary/8HTPdiH6yEk0jlNF.htm)|Pickled Punk|Pickled Punk|modificada|
|[8jjEOs99Bmo1v0Qc.htm](agents-of-edgewatch-bestiary/8jjEOs99Bmo1v0Qc.htm)|Teraphant|Teraphant|modificada|
|[8LlqjWcqiFq7YMmQ.htm](agents-of-edgewatch-bestiary/8LlqjWcqiFq7YMmQ.htm)|Agent of the Gray Queen|Agente de la Reina Gris|modificada|
|[8PRqUfkHLvu9ufGL.htm](agents-of-edgewatch-bestiary/8PRqUfkHLvu9ufGL.htm)|Skinsaw Murderer|Skinsaw Murderer|modificada|
|[8VAGXECLAk91hoAv.htm](agents-of-edgewatch-bestiary/8VAGXECLAk91hoAv.htm)|Field Of Opposition|Campo De Oposición|modificada|
|[94yI4TrNag0jMaYy.htm](agents-of-edgewatch-bestiary/94yI4TrNag0jMaYy.htm)|Lie-Master|Mentir-Maestro|modificada|
|[9M1YJxTXqya55HDx.htm](agents-of-edgewatch-bestiary/9M1YJxTXqya55HDx.htm)|Copper Hand Rogue|Copper Hand Rogue|modificada|
|[9vt9Dr2a8MkkD83z.htm](agents-of-edgewatch-bestiary/9vt9Dr2a8MkkD83z.htm)|Chalky|Chalky|modificada|
|[a3pTQfLzsJThQvI9.htm](agents-of-edgewatch-bestiary/a3pTQfLzsJThQvI9.htm)|Calennia|Calennia|modificada|
|[aDgVmO3afhIgXQSN.htm](agents-of-edgewatch-bestiary/aDgVmO3afhIgXQSN.htm)|Ravenile Rager|Ravenile Rager|modificada|
|[ai7Q9vBHHAGj7uFE.htm](agents-of-edgewatch-bestiary/ai7Q9vBHHAGj7uFE.htm)|Svartalfar Killer|Svartalfar Killer|modificada|
|[aLhAofozzdTuqfcg.htm](agents-of-edgewatch-bestiary/aLhAofozzdTuqfcg.htm)|Acidic Poison Cloud Trap|Trampa de nube de veneno ácido|modificada|
|[ApEn56YEz9xavgug.htm](agents-of-edgewatch-bestiary/ApEn56YEz9xavgug.htm)|Life Magnet|Imán de vida|modificada|
|[AYNIAAxV7TbIKPI4.htm](agents-of-edgewatch-bestiary/AYNIAAxV7TbIKPI4.htm)|Skitterstitch|Skitterstitch|modificada|
|[AZ6AaYdx2NwLWSwh.htm](agents-of-edgewatch-bestiary/AZ6AaYdx2NwLWSwh.htm)|Blackfinger's Prayer|Blackfinger's Prayer|modificada|
|[azyIfDNNW44jY8YX.htm](agents-of-edgewatch-bestiary/azyIfDNNW44jY8YX.htm)|Barrel Launcher|Barrel Launcher|modificada|
|[B4kIfCimsz8wfc0k.htm](agents-of-edgewatch-bestiary/B4kIfCimsz8wfc0k.htm)|Lord Guirden|Lord Guirden|modificada|
|[b7AnzEzoMGFpM33z.htm](agents-of-edgewatch-bestiary/b7AnzEzoMGFpM33z.htm)|Ink Drowning Vats|Tinas de ahogamiento de tinta|modificada|
|[BCMkxoQB4xK4BniG.htm](agents-of-edgewatch-bestiary/BCMkxoQB4xK4BniG.htm)|Secret-Keeper|Guardián Secreto|modificada|
|[BLRsSDFSMbZHcGDQ.htm](agents-of-edgewatch-bestiary/BLRsSDFSMbZHcGDQ.htm)|Black Whale Guard|Guardia Ballena Negra|modificada|
|[BoFg19e3N8WiNa3Z.htm](agents-of-edgewatch-bestiary/BoFg19e3N8WiNa3Z.htm)|The Stabbing Beast|La Bestia Apuñaladora|modificada|
|[BuPf7xtqfwCjNOQv.htm](agents-of-edgewatch-bestiary/BuPf7xtqfwCjNOQv.htm)|Reginald Vancaskerkin|Reginald Vancaskerkin|modificada|
|[Bv1s6xJ55HS3Gxgs.htm](agents-of-edgewatch-bestiary/Bv1s6xJ55HS3Gxgs.htm)|Grick|Grick|modificada|
|[ce2SoJ7nRNZ1AoK6.htm](agents-of-edgewatch-bestiary/ce2SoJ7nRNZ1AoK6.htm)|Hidden Chute|Escondite Chute|modificada|
|[cLlOUUpCIAQwUuOP.htm](agents-of-edgewatch-bestiary/cLlOUUpCIAQwUuOP.htm)|Il'setsya Wyrmtouched|Il'setsya Wyrmtouched|modificada|
|[CLntGVs7cAIL9Trk.htm](agents-of-edgewatch-bestiary/CLntGVs7cAIL9Trk.htm)|Scathka|Scathka|modificada|
|[cQMM2Ld0IBM9GcDo.htm](agents-of-edgewatch-bestiary/cQMM2Ld0IBM9GcDo.htm)|Norgorberite Poisoner|Envenenador de Norgorberita|modificada|
|[CTKAJG7oQaIQWCiB.htm](agents-of-edgewatch-bestiary/CTKAJG7oQaIQWCiB.htm)|Exploding Statue|Estatua Explosiva|modificada|
|[D36nL0YjCVHfjBNw.htm](agents-of-edgewatch-bestiary/D36nL0YjCVHfjBNw.htm)|Clockwork Chopper|Clockwork Chopper|modificada|
|[d3TzpCuRJF78xHZK.htm](agents-of-edgewatch-bestiary/d3TzpCuRJF78xHZK.htm)|Black Whale Guard (F3)|Guardia de la ballena negra (F3, garrote de pesadilla)|modificada|
|[D5UF1i0f1IRcQcNB.htm](agents-of-edgewatch-bestiary/D5UF1i0f1IRcQcNB.htm)|Ghaele Of Kharnas|Ghaele de Kharnas|modificada|
|[DKReNCapWWubM3pm.htm](agents-of-edgewatch-bestiary/DKReNCapWWubM3pm.htm)|Shikwashim Mercenary|Mercenario Shikwashim|modificada|
|[E279VPhAy1a4ihqI.htm](agents-of-edgewatch-bestiary/E279VPhAy1a4ihqI.htm)|Living Mural|Mural viviente|modificada|
|[E7NEf3kmsY3YjRrz.htm](agents-of-edgewatch-bestiary/E7NEf3kmsY3YjRrz.htm)|Kapral|Kapral|modificada|
|[e96H6PLW11NCRK1h.htm](agents-of-edgewatch-bestiary/e96H6PLW11NCRK1h.htm)|Greater Planar Rift|Grieta planaria mayor|modificada|
|[EGQgqBfV80ll3pcf.htm](agents-of-edgewatch-bestiary/EGQgqBfV80ll3pcf.htm)|Chaos Gulgamodh|Caos Gulgamodh|modificada|
|[eQc0ADMuHl1JzL8z.htm](agents-of-edgewatch-bestiary/eQc0ADMuHl1JzL8z.htm)|Shredskin|Shredskin|modificada|
|[eqwAdGsAk5JZKxUY.htm](agents-of-edgewatch-bestiary/eqwAdGsAk5JZKxUY.htm)|Gage Carlyle|Gage Carlyle|modificada|
|[FmiOJ9HEdCBDB89z.htm](agents-of-edgewatch-bestiary/FmiOJ9HEdCBDB89z.htm)|Burning Chandelier Trap|Burning Chandelier Trap|modificada|
|[Fmsw7P5CF3uHtD5W.htm](agents-of-edgewatch-bestiary/Fmsw7P5CF3uHtD5W.htm)|Veksciralenix|Veksciralenix|modificada|
|[fV5VIoXMtixmI3Wc.htm](agents-of-edgewatch-bestiary/fV5VIoXMtixmI3Wc.htm)|Clockwork Assassin|Clockwork Assassin|modificada|
|[fvOjtzuRNpmpEHXA.htm](agents-of-edgewatch-bestiary/fvOjtzuRNpmpEHXA.htm)|Binumir|Binumir|modificada|
|[gd0sVQtCHbhP8iHI.htm](agents-of-edgewatch-bestiary/gd0sVQtCHbhP8iHI.htm)|Grospek Lavarsus|Grospek Lavarsus|modificada|
|[gFVazlIgCZivKzKF.htm](agents-of-edgewatch-bestiary/gFVazlIgCZivKzKF.htm)|Inky Tendrils|Inky Tendrils|modificada|
|[gIcReNQOZceZBBlw.htm](agents-of-edgewatch-bestiary/gIcReNQOZceZBBlw.htm)|Jonis Flakfatter|Jonis Flakfatter|modificada|
|[giVkjJeVZjhbR6eA.htm](agents-of-edgewatch-bestiary/giVkjJeVZjhbR6eA.htm)|Gas Trap|Trampa de Gas|modificada|
|[gkRxUi9VrbPWOPGC.htm](agents-of-edgewatch-bestiary/gkRxUi9VrbPWOPGC.htm)|Daemonic Infector|Infector daimónico|modificada|
|[gsn4NsJLwZCQUwgf.htm](agents-of-edgewatch-bestiary/gsn4NsJLwZCQUwgf.htm)|Almiraj|Almiraj|modificada|
|[GWO6vweLGT2J6q62.htm](agents-of-edgewatch-bestiary/GWO6vweLGT2J6q62.htm)|Miriel Grayleaf|Miriel Grayleaf|modificada|
|[GYhV5eYNDO1Llbv2.htm](agents-of-edgewatch-bestiary/GYhV5eYNDO1Llbv2.htm)|Venom Mage|Venom Mage|modificada|
|[H4bY8v6e3drOIoUe.htm](agents-of-edgewatch-bestiary/H4bY8v6e3drOIoUe.htm)|Grabble Forden|Grabble Forden|modificada|
|[HF0ymYmKC6KydPQ1.htm](agents-of-edgewatch-bestiary/HF0ymYmKC6KydPQ1.htm)|Franca Laurentz|Franca Laurentz|modificada|
|[HifZEgdCuZearOG2.htm](agents-of-edgewatch-bestiary/HifZEgdCuZearOG2.htm)|Clockwork Amalgam|Amalgama de Relojería|modificada|
|[HXiA7x1jWnB1BqUy.htm](agents-of-edgewatch-bestiary/HXiA7x1jWnB1BqUy.htm)|Summoning Rune (Barbazu Devil)|Runa de convocación (Demonio Barbazu)|modificada|
|[hzThZ50RRdfTYTKc.htm](agents-of-edgewatch-bestiary/hzThZ50RRdfTYTKc.htm)|Clockwork Poison Bomb|Bomba Venenosa Mecánica|modificada|
|[IKfYWE1NLVNLCXZm.htm](agents-of-edgewatch-bestiary/IKfYWE1NLVNLCXZm.htm)|Blune's Illusory Toady|Blune's Illusory Toady|modificada|
|[JdMqHbaTwtOHVE7Y.htm](agents-of-edgewatch-bestiary/JdMqHbaTwtOHVE7Y.htm)|Diobel Sweeper Chemist|Diobel Sweeper Chemist|modificada|
|[JFcEtt18SGlb5uxm.htm](agents-of-edgewatch-bestiary/JFcEtt18SGlb5uxm.htm)|Supplicant Statues|Estatuas Suplicantes|modificada|
|[jgSS31hwrQ1n4jVF.htm](agents-of-edgewatch-bestiary/jgSS31hwrQ1n4jVF.htm)|Waxworks Onslaught Trap|Waxworks Onslaught Trap|modificada|
|[JhYJUNlcxiurcZcl.htm](agents-of-edgewatch-bestiary/JhYJUNlcxiurcZcl.htm)|Water Elemental Vessel (I2)|Recipiente elemental de agua (I2)|modificada|
|[jJ1UTNLiRCoN1O3i.htm](agents-of-edgewatch-bestiary/jJ1UTNLiRCoN1O3i.htm)|Hendrid Pratchett|Hendrid Pratchett|modificada|
|[JmHyHwaPMNKXzBts.htm](agents-of-edgewatch-bestiary/JmHyHwaPMNKXzBts.htm)|Zealborn|Zealborn|modificada|
|[JnNPdvOXtkGQHyfQ.htm](agents-of-edgewatch-bestiary/JnNPdvOXtkGQHyfQ.htm)|Gloaming Will-o'-Wisp|Gloaming Will-o'-Wisp|modificada|
|[JsyO7yBf6YaF4YwF.htm](agents-of-edgewatch-bestiary/JsyO7yBf6YaF4YwF.htm)|Daemonic Rumormonger|Rumormonger daimónico|modificada|
|[JuCLvgvxYbSRXqON.htm](agents-of-edgewatch-bestiary/JuCLvgvxYbSRXqON.htm)|Copper Hand Illusionist|Ilusionista mano de cobre|modificada|
|[JZUYQzQtzIwOGYvd.htm](agents-of-edgewatch-bestiary/JZUYQzQtzIwOGYvd.htm)|Ralso|Ralso|modificada|
|[k1s8syi5E2FoR3Q3.htm](agents-of-edgewatch-bestiary/k1s8syi5E2FoR3Q3.htm)|Freezing Alarm|Alarma de congelación|modificada|
|[kcvU9CatCyBUJRr2.htm](agents-of-edgewatch-bestiary/kcvU9CatCyBUJRr2.htm)|Rhevanna|Rhevanna|modificada|
|[kidMmEzwgoBcHdnR.htm](agents-of-edgewatch-bestiary/kidMmEzwgoBcHdnR.htm)|The Laughing Fiend's Greeting|El saludo del demonio de la risa|modificada|
|[l3AqEkQwRJS8TY7f.htm](agents-of-edgewatch-bestiary/l3AqEkQwRJS8TY7f.htm)|Canopy Drop|Canopy Drop|modificada|
|[LAamprMlzk7k5auj.htm](agents-of-edgewatch-bestiary/LAamprMlzk7k5auj.htm)|Hestriviniaas|Hestriviniaas|modificada|
|[LACpbwnVT7m2ZqBi.htm](agents-of-edgewatch-bestiary/LACpbwnVT7m2ZqBi.htm)|The Winder|The Winder|modificada|
|[LAun416if9NFg3X2.htm](agents-of-edgewatch-bestiary/LAun416if9NFg3X2.htm)|Explosive Barrels|Barriles Explosión|modificada|
|[lgbJbeSeC1f8otvH.htm](agents-of-edgewatch-bestiary/lgbJbeSeC1f8otvH.htm)|Daemonic Skinner|Desollador daimónico|modificada|
|[LkaB8RH73DY4TO9V.htm](agents-of-edgewatch-bestiary/LkaB8RH73DY4TO9V.htm)|Priest of Blackfingers|Sacerdote de Dedos Negros|modificada|
|[lzmGdArS3kjJOqT6.htm](agents-of-edgewatch-bestiary/lzmGdArS3kjJOqT6.htm)|Lyrma Swampwalker|Lyrma Swampwalker|modificada|
|[M8ONVV7yl4uu0zcz.htm](agents-of-edgewatch-bestiary/M8ONVV7yl4uu0zcz.htm)|Ixusoth|Ixusoth|modificada|
|[MfW7O5Ba8r2GR9ZQ.htm](agents-of-edgewatch-bestiary/MfW7O5Ba8r2GR9ZQ.htm)|Sordesdaemon|Sordesdaemon|modificada|
|[MONwgTbrcZFzr6vC.htm](agents-of-edgewatch-bestiary/MONwgTbrcZFzr6vC.htm)|Antaro Boldblade|Antaro Boldblade|modificada|
|[MqfZxoxFwzqAXhTP.htm](agents-of-edgewatch-bestiary/MqfZxoxFwzqAXhTP.htm)|Prospecti Statue|Estatua Prospecti|modificada|
|[MQnhyCM9LInNYtl0.htm](agents-of-edgewatch-bestiary/MQnhyCM9LInNYtl0.htm)|Carvey|Carvey|modificada|
|[N1IpUIW2Ry7uBN3G.htm](agents-of-edgewatch-bestiary/N1IpUIW2Ry7uBN3G.htm)|Tiderunner Aquamancer|Tiderunner Aquamancer|modificada|
|[nfULZbf8OOUDdrZM.htm](agents-of-edgewatch-bestiary/nfULZbf8OOUDdrZM.htm)|Water Elemental Vessel|Recipiente elemental de agua|modificada|
|[nTn2szBbqQNdXhOr.htm](agents-of-edgewatch-bestiary/nTn2szBbqQNdXhOr.htm)|Skebs|Skebs|modificada|
|[oFCgiGFipWeit9sl.htm](agents-of-edgewatch-bestiary/oFCgiGFipWeit9sl.htm)|Lusca|Lusca|modificada|
|[oTa25rAxytm03T3X.htm](agents-of-edgewatch-bestiary/oTa25rAxytm03T3X.htm)|Shristi Melipdra|Shristi Melipdra|modificada|
|[oWASKud0jwlGSfJg.htm](agents-of-edgewatch-bestiary/oWASKud0jwlGSfJg.htm)|Pelmo|Pelmo|modificada|
|[P9Wg0sGcNkemOvm3.htm](agents-of-edgewatch-bestiary/P9Wg0sGcNkemOvm3.htm)|Slithering Rift|Grieta Escurridiza|modificada|
|[pE5GxoB1FtXBqnF7.htm](agents-of-edgewatch-bestiary/pE5GxoB1FtXBqnF7.htm)|Gref|Gref|modificada|
|[PEjcy9CxelKC3Kp6.htm](agents-of-edgewatch-bestiary/PEjcy9CxelKC3Kp6.htm)|Zrukbat|Zrukbat|modificada|
|[pfHOwcITyC4gdCVu.htm](agents-of-edgewatch-bestiary/pfHOwcITyC4gdCVu.htm)|Garrote Master Assassin|Garrote Maestro Asesino|modificada|
|[phkvSUK6WXxgJOoC.htm](agents-of-edgewatch-bestiary/phkvSUK6WXxgJOoC.htm)|Alchemist Aspirant|Aspirante a Alquimista|modificada|
|[pK1tlsgkmzkaaCe5.htm](agents-of-edgewatch-bestiary/pK1tlsgkmzkaaCe5.htm)|Iroran Skeleton|Esqueleto Iroran|modificada|
|[pTkvg8OITTl6lsJY.htm](agents-of-edgewatch-bestiary/pTkvg8OITTl6lsJY.htm)|Minchgorm|Minchgorm|modificada|
|[q61A6EsMnDDtnqxH.htm](agents-of-edgewatch-bestiary/q61A6EsMnDDtnqxH.htm)|Ulressia The Blessed|Ulressia La Bendecida|modificada|
|[QBlBYsxySiBMxf22.htm](agents-of-edgewatch-bestiary/QBlBYsxySiBMxf22.htm)|Zeal-damned Ghoul|Zeal-damned Ghoul|modificada|
|[QHcBauLnHzfwkDBK.htm](agents-of-edgewatch-bestiary/QHcBauLnHzfwkDBK.htm)|Summoning Rune (Cinder Rat)|Runa de convocación (rata de ceniza).|modificada|
|[QQkbvOCif8Bm1wws.htm](agents-of-edgewatch-bestiary/QQkbvOCif8Bm1wws.htm)|Izfiitar|Izfiitar|modificada|
|[QRcIVhQV2vlpADSf.htm](agents-of-edgewatch-bestiary/QRcIVhQV2vlpADSf.htm)|Camarach|Camarach|modificada|
|[qSL6PtsHPekEEEjx.htm](agents-of-edgewatch-bestiary/qSL6PtsHPekEEEjx.htm)|Overdrive Imentesh|Overdrive Imentesh|modificada|
|[qUhxXE2yT0qwSJQm.htm](agents-of-edgewatch-bestiary/qUhxXE2yT0qwSJQm.htm)|Agradaemon|Agradaemon|modificada|
|[qY1NrXKmL0y18qoz.htm](agents-of-edgewatch-bestiary/qY1NrXKmL0y18qoz.htm)|Bolar Of Stonemoor|Bolar de Stonemoor|modificada|
|[qy53ECS2agScE7G3.htm](agents-of-edgewatch-bestiary/qy53ECS2agScE7G3.htm)|Kharnas's Lesser Glyph|Glifo menor de Kharnas|modificada|
|[r3j5KEvULFP3fZS7.htm](agents-of-edgewatch-bestiary/r3j5KEvULFP3fZS7.htm)|Grimwold|Grimwold|modificada|
|[rAUaIxp3QFgT3bzl.htm](agents-of-edgewatch-bestiary/rAUaIxp3QFgT3bzl.htm)|Needling Stairs|Needling Stairs|modificada|
|[rDYDTCVUa5GS3uTE.htm](agents-of-edgewatch-bestiary/rDYDTCVUa5GS3uTE.htm)|Iron Maiden Trap|Trampa Doncella de Hierro|modificada|
|[rGTq4qItRB5H7nEk.htm](agents-of-edgewatch-bestiary/rGTq4qItRB5H7nEk.htm)|Graveknight Of Kharnas|Graveknight Of Kharnas|modificada|
|[Rinxhe1cRXKEsXuW.htm](agents-of-edgewatch-bestiary/Rinxhe1cRXKEsXuW.htm)|Tyrroicese|Tyrroicese|modificada|
|[rm0iJOMwruWSE93I.htm](agents-of-edgewatch-bestiary/rm0iJOMwruWSE93I.htm)|Olansa Terimor|Olansa Terimor|modificada|
|[rM6ix6XTroJod3Vr.htm](agents-of-edgewatch-bestiary/rM6ix6XTroJod3Vr.htm)|Fayati Alummur|Fayati Alummur|modificada|
|[rsKf8ixrl3yBq1gb.htm](agents-of-edgewatch-bestiary/rsKf8ixrl3yBq1gb.htm)|Starwatch Commando|Vigilante estelar Comando|modificada|
|[RtWlzHaOrfFdJyJY.htm](agents-of-edgewatch-bestiary/RtWlzHaOrfFdJyJY.htm)|Alchemical Horror|Horror alquímico|modificada|
|[rXePwiS4iecWNMGU.htm](agents-of-edgewatch-bestiary/rXePwiS4iecWNMGU.htm)|Grinlowe|Grinlowe|modificada|
|[RyXA4wOGY8lenKVw.htm](agents-of-edgewatch-bestiary/RyXA4wOGY8lenKVw.htm)|Nenchuuj|Nenchuuj|modificada|
|[S9JJsUNSeeoIClON.htm](agents-of-edgewatch-bestiary/S9JJsUNSeeoIClON.htm)|Poison Eater|Come veneno|modificada|
|[SgkV5RtcK72d0HwI.htm](agents-of-edgewatch-bestiary/SgkV5RtcK72d0HwI.htm)|Excorion|Excorion|modificada|
|[sn9Pjkr2jlMEqc3E.htm](agents-of-edgewatch-bestiary/sn9Pjkr2jlMEqc3E.htm)|Eunice|Eunice|modificada|
|[Sq0Kb92nGkqj19Xx.htm](agents-of-edgewatch-bestiary/Sq0Kb92nGkqj19Xx.htm)|Miogimo|Miogimo|modificada|
|[sUrJ7jxzBiJTbwVo.htm](agents-of-edgewatch-bestiary/sUrJ7jxzBiJTbwVo.htm)|Blune Bandersworth|Blune Bandersworth|modificada|
|[syUXLdUsEDYgni5R.htm](agents-of-edgewatch-bestiary/syUXLdUsEDYgni5R.htm)|Wrent Dicaspiron|Wrent Dicaspiron|modificada|
|[T6RsavB4ZZGdlNuA.htm](agents-of-edgewatch-bestiary/T6RsavB4ZZGdlNuA.htm)|Hands Of The Forgotten|Manos de los olvidados|modificada|
|[t9m4ikMZsDwo9TQ1.htm](agents-of-edgewatch-bestiary/t9m4ikMZsDwo9TQ1.htm)|Maurrisa Jonne|Maurrisa Jonne|modificada|
|[TIaZIUb9Mq9B4Mf2.htm](agents-of-edgewatch-bestiary/TIaZIUb9Mq9B4Mf2.htm)|Bloody Berleth|Bloody Berleth|modificada|
|[tm8vQ7gdAe9zVdDg.htm](agents-of-edgewatch-bestiary/tm8vQ7gdAe9zVdDg.htm)|Vaultbreaker Ooze|Vaultbreaker Ooze|modificada|
|[tt5eaS28C4PrHmZD.htm](agents-of-edgewatch-bestiary/tt5eaS28C4PrHmZD.htm)|The Inkmaster|El Inkmaster|modificada|
|[u1cuwAE3xzhYW4Mi.htm](agents-of-edgewatch-bestiary/u1cuwAE3xzhYW4Mi.htm)|False Door Trap|Trampa de puerta falsa|modificada|
|[uFU6dQfcNeKq68YT.htm](agents-of-edgewatch-bestiary/uFU6dQfcNeKq68YT.htm)|Vargouille|Vargouille|modificada|
|[Uhi3wX4KveuMSARt.htm](agents-of-edgewatch-bestiary/Uhi3wX4KveuMSARt.htm)|Giant Joro Spider|Araña gigante Joro|modificada|
|[ukL9sApDCIWsVL64.htm](agents-of-edgewatch-bestiary/ukL9sApDCIWsVL64.htm)|Boiling Tub Trap|Boiling Tub Trap|modificada|
|[UkmM3bjBoBld0uzS.htm](agents-of-edgewatch-bestiary/UkmM3bjBoBld0uzS.htm)|Flying Guillotine|Guillotina voladora|modificada|
|[vPMmTtvl5UPOcCoa.htm](agents-of-edgewatch-bestiary/vPMmTtvl5UPOcCoa.htm)|Myrna Rath|Myrna Rath|modificada|
|[vwxNCuBksHYU2Dwf.htm](agents-of-edgewatch-bestiary/vwxNCuBksHYU2Dwf.htm)|Hundun Chaos Mage|Hundun Mago del Caos|modificada|
|[w2J6GpuMYM24U4sb.htm](agents-of-edgewatch-bestiary/w2J6GpuMYM24U4sb.htm)|Clockwork Injector|Inyector de relojería|modificada|
|[Wb4Md6byPhBWe56J.htm](agents-of-edgewatch-bestiary/Wb4Md6byPhBWe56J.htm)|Cobbleswarm (AoE)|Cobbleswarm (AoE)|modificada|
|[WDTdWiC9Rdl6rqh8.htm](agents-of-edgewatch-bestiary/WDTdWiC9Rdl6rqh8.htm)|Myrucarx|Myrucarx|modificada|
|[Wk2T0Wr8Sebo4br5.htm](agents-of-edgewatch-bestiary/Wk2T0Wr8Sebo4br5.htm)|Mr. Snips|Mr. Snips|modificada|
|[WplBGSeB9pK9AULX.htm](agents-of-edgewatch-bestiary/WplBGSeB9pK9AULX.htm)|The Rabbit Prince|El Príncipe Conejo|modificada|
|[wsVW8MdOTeGgGM59.htm](agents-of-edgewatch-bestiary/wsVW8MdOTeGgGM59.htm)|Child Of Venom|Niño de Veneno|modificada|
|[WXwvWHRpwK17YABQ.htm](agents-of-edgewatch-bestiary/WXwvWHRpwK17YABQ.htm)|Bregdi|Bregdi|modificada|
|[XDt87cqF85zWnlC8.htm](agents-of-edgewatch-bestiary/XDt87cqF85zWnlC8.htm)|Siege Shard|Esquirla de asedio|modificada|
|[Xi53GFvTgBApltjp.htm](agents-of-edgewatch-bestiary/Xi53GFvTgBApltjp.htm)|Giant Bone Skipper|Saltarín Hueso Gigante|modificada|
|[xkkj0TW6BKNT3Bg4.htm](agents-of-edgewatch-bestiary/xkkj0TW6BKNT3Bg4.htm)|Diobel Sweeper Tough|Barredora Diobel Duro|modificada|
|[xN3FDmrCKWW0psBu.htm](agents-of-edgewatch-bestiary/xN3FDmrCKWW0psBu.htm)|Sleepless Sun Veteran|Sleepless Sun Veterano|modificada|
|[XTHcALqbg5kgxtPw.htm](agents-of-edgewatch-bestiary/XTHcALqbg5kgxtPw.htm)|Arcane Feedback Trap|Arcane Feedback Trap|modificada|
|[xw7S8108irh2D1Uw.htm](agents-of-edgewatch-bestiary/xw7S8108irh2D1Uw.htm)|Dart Barrage|Dart Barrage|modificada|
|[y0bIU9FCWHOJxUzG.htm](agents-of-edgewatch-bestiary/y0bIU9FCWHOJxUzG.htm)|Bloody Barber Goon|Bloody Barber Goon|modificada|
|[y1tw2ohNagqQJ6RV.htm](agents-of-edgewatch-bestiary/y1tw2ohNagqQJ6RV.htm)|Skinsaw Seamer|Skinsaw Seamer|modificada|
|[yR6p0KVvZ3tPflRt.htm](agents-of-edgewatch-bestiary/yR6p0KVvZ3tPflRt.htm)|Kemeneles|Kemeneles|modificada|
|[ySpOZlKUbcxWhKQ6.htm](agents-of-edgewatch-bestiary/ySpOZlKUbcxWhKQ6.htm)|Ofalth Zombie|Zombie pocilguero|modificada|
|[Z6R8YjgX8Jvt9Ds4.htm](agents-of-edgewatch-bestiary/Z6R8YjgX8Jvt9Ds4.htm)|Avsheros the Betrayer|Avsheros el Traidor|modificada|
|[zj2sCM8tQSMG9Qm6.htm](agents-of-edgewatch-bestiary/zj2sCM8tQSMG9Qm6.htm)|Najra Lizard|Najra Lizard|modificada|
|[ZL2qLXwomKfBB8Eu.htm](agents-of-edgewatch-bestiary/ZL2qLXwomKfBB8Eu.htm)|Dreadsong Dancer|Dreadsong Dancer|modificada|
|[zrh3MrS68H2gPlVs.htm](agents-of-edgewatch-bestiary/zrh3MrS68H2gPlVs.htm)|Tenome|Tenome|modificada|
|[ZY3q7AV1qbwWwNl2.htm](agents-of-edgewatch-bestiary/ZY3q7AV1qbwWwNl2.htm)|Mobana|Mobana|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
