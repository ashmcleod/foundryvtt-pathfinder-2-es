# Estado de la traducción (travel-guide-bestiary-items)

 * **ninguna**: 3
 * **vacía**: 2


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[28P5kFSjVh3w1R9o.htm](travel-guide-bestiary-items/28P5kFSjVh3w1R9o.htm)|Scent (Imprecise) 30 feet|
|[g7nl0QahmH5JaunK.htm](travel-guide-bestiary-items/g7nl0QahmH5JaunK.htm)|Low-Light Vision|
|[Mjlek9OZ25Tga7Cg.htm](travel-guide-bestiary-items/Mjlek9OZ25Tga7Cg.htm)|Filth Fever|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[vBFHfrDtlZ5vYw2q.htm](travel-guide-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|vacía|
|[yLnwKltbJHYpn8qC.htm](travel-guide-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
