# Estado de la traducción (age-of-ashes-bestiary)

 * **modificada**: 129


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[080v247YtmFRxT3l.htm](age-of-ashes-bestiary/080v247YtmFRxT3l.htm)|Bshez "Sand Claws" Shak|Bshez "Garras de Arena" Shak|modificada|
|[1lkay2gwgEquq0NF.htm](age-of-ashes-bestiary/1lkay2gwgEquq0NF.htm)|Scarlet Triad Sneak|Scarlet Triad Movimiento furtivo|modificada|
|[1tDEWL9mAIXvTYik.htm](age-of-ashes-bestiary/1tDEWL9mAIXvTYik.htm)|Warbal Bumblebrasher|Warbal Bumblebrasher|modificada|
|[2FbG5nLE91b6BNiB.htm](age-of-ashes-bestiary/2FbG5nLE91b6BNiB.htm)|Duergar Slave Lord|Duergar Slave Lord|modificada|
|[30FX1WFr0RkGeueX.htm](age-of-ashes-bestiary/30FX1WFr0RkGeueX.htm)|Inizra Arumelo|Inizra Arumelo|modificada|
|[4vEQ8zRXC51Qo4Mv.htm](age-of-ashes-bestiary/4vEQ8zRXC51Qo4Mv.htm)|Bloody Blade Mercenary|Bloody Blade Mercenary|modificada|
|[53lk7ek73j65A09B.htm](age-of-ashes-bestiary/53lk7ek73j65A09B.htm)|Candlaron's Echo|Eco de Candlaron|modificada|
|[595MAHwB4JWtzLAA.htm](age-of-ashes-bestiary/595MAHwB4JWtzLAA.htm)|Soulbound Ruin|Soulbound Ruin|modificada|
|[5dSVk2y88SLsPPON.htm](age-of-ashes-bestiary/5dSVk2y88SLsPPON.htm)|Hermean Mutant|Hermean Mutant|modificada|
|[5DwiLZJf6KxpIjDq.htm](age-of-ashes-bestiary/5DwiLZJf6KxpIjDq.htm)|Grippli Archer|Arquero Grippli|modificada|
|[5jFR05QRixpOChRy.htm](age-of-ashes-bestiary/5jFR05QRixpOChRy.htm)|Deculi|Deculi|modificada|
|[5r2S6FeE6D1Oh66T.htm](age-of-ashes-bestiary/5r2S6FeE6D1Oh66T.htm)|Tarrasque, The Armageddon Engine|Tarrasque, El Motor del Armagedón|modificada|
|[60aeSJvu09ZM3SPx.htm](age-of-ashes-bestiary/60aeSJvu09ZM3SPx.htm)|Racharak|Racharak|modificada|
|[6AN7eagk2WrWc4im.htm](age-of-ashes-bestiary/6AN7eagk2WrWc4im.htm)|Mengkare|Mengkare|modificada|
|[7WNFNE3SVMRGbBDf.htm](age-of-ashes-bestiary/7WNFNE3SVMRGbBDf.htm)|Scarlet Triad Sniper|Francotirador de la Tríada Escarlata|modificada|
|[91BSCAJA1Oto2ctf.htm](age-of-ashes-bestiary/91BSCAJA1Oto2ctf.htm)|Blood Boar|Jabalí de sangre|modificada|
|[9oNwQuwKGUuG9G9g.htm](age-of-ashes-bestiary/9oNwQuwKGUuG9G9g.htm)|Laslunn|Laslunn|modificada|
|[aaDiR0EIWRQx8wdy.htm](age-of-ashes-bestiary/aaDiR0EIWRQx8wdy.htm)|Calmont|Calmont|modificada|
|[Afq4Osh3W1k9Bcsh.htm](age-of-ashes-bestiary/Afq4Osh3W1k9Bcsh.htm)|Promise Guard|Guardia Promesa|modificada|
|[Aii18rhNPMLW4Pxh.htm](age-of-ashes-bestiary/Aii18rhNPMLW4Pxh.htm)|Skeletal Hellknight|Skeletal Hellknight|modificada|
|[aiNE06bxKD6jfoLd.htm](age-of-ashes-bestiary/aiNE06bxKD6jfoLd.htm)|Uri Zandivar|Uri Zandivar|modificada|
|[ajt6K2LyAxhJ8GuP.htm](age-of-ashes-bestiary/ajt6K2LyAxhJ8GuP.htm)|Black Powder Bomb|Bomba de pólvora negra|modificada|
|[AtiN3EsRpHn5qbuv.htm](age-of-ashes-bestiary/AtiN3EsRpHn5qbuv.htm)|Immortal Ichor|Ichor Inmortal|modificada|
|[axCgEgk7n1yXpgdp.htm](age-of-ashes-bestiary/axCgEgk7n1yXpgdp.htm)|Caustic Vapor|Vapor cáustico|modificada|
|[BudeoAoJ4dOxjj0K.htm](age-of-ashes-bestiary/BudeoAoJ4dOxjj0K.htm)|Dahak's Shell|Caparazón de Dahak|modificada|
|[C4PD4p4I9byZ8yp6.htm](age-of-ashes-bestiary/C4PD4p4I9byZ8yp6.htm)|King Harral|Rey Harral|modificada|
|[CJVRPFTKuJroiD5C.htm](age-of-ashes-bestiary/CJVRPFTKuJroiD5C.htm)|Xevalorg|Xevalorg|modificada|
|[CmbcgWZgOSDmo2Pm.htm](age-of-ashes-bestiary/CmbcgWZgOSDmo2Pm.htm)|Precentor|Precentor|modificada|
|[dG5DBgrxlaimsWOS.htm](age-of-ashes-bestiary/dG5DBgrxlaimsWOS.htm)|Falrok|Falrok|modificada|
|[dlW3UaXVpnzjd6xe.htm](age-of-ashes-bestiary/dlW3UaXVpnzjd6xe.htm)|Heuberk Thropp|Heuberk Thropp|modificada|
|[dTikLHqGfiSYemuZ.htm](age-of-ashes-bestiary/dTikLHqGfiSYemuZ.htm)|Veshumirix|Veshumirix|modificada|
|[DxlauoGqSVDZWBOM.htm](age-of-ashes-bestiary/DxlauoGqSVDZWBOM.htm)|Acidic Needle Launcher|Acidic Needle Launcher|modificada|
|[egTpOr4Wc0L5e0iY.htm](age-of-ashes-bestiary/egTpOr4Wc0L5e0iY.htm)|Scarlet Triad Enforcer|Scarlet Triad Enforcer|modificada|
|[Ejxngh2tHFseZQHW.htm](age-of-ashes-bestiary/Ejxngh2tHFseZQHW.htm)|Vaklish|Vaklish|modificada|
|[El7eSPV9aRi98Ufy.htm](age-of-ashes-bestiary/El7eSPV9aRi98Ufy.htm)|Mercenary Sailor|Marinero mercenario|modificada|
|[EUYisYdY3fcGL8zp.htm](age-of-ashes-bestiary/EUYisYdY3fcGL8zp.htm)|Lesser Dragonstorm|Tormenta de dragones menor|modificada|
|[Ev930dfPpwCR8Zju.htm](age-of-ashes-bestiary/Ev930dfPpwCR8Zju.htm)|Ilgreth|Ilgreth|modificada|
|[F9BHPV16N4eEL0TP.htm](age-of-ashes-bestiary/F9BHPV16N4eEL0TP.htm)|Dragonstorm Fire Giant|Dragonstorm Gigante de Fuego|modificada|
|[Fe1lYhCUY4UO4Plw.htm](age-of-ashes-bestiary/Fe1lYhCUY4UO4Plw.htm)|Talamira|Talamira|modificada|
|[gF8Qy1k8gPBEUBAd.htm](age-of-ashes-bestiary/gF8Qy1k8gPBEUBAd.htm)|Scarlet Triad Mage|Mago de la Tríada Escarlata|modificada|
|[H0pP1GqpMfX1WEiQ.htm](age-of-ashes-bestiary/H0pP1GqpMfX1WEiQ.htm)|Grauladon|Grauladon|modificada|
|[h7apR3QSxVZmy2nt.htm](age-of-ashes-bestiary/h7apR3QSxVZmy2nt.htm)|Dahak's Skull|Cráneo de Dahak|modificada|
|[H7NO4Q7ctHnfGKeJ.htm](age-of-ashes-bestiary/H7NO4Q7ctHnfGKeJ.htm)|Weathered Wail|Lamento intemperie|modificada|
|[HbgvZUYFJd2VfwmQ.htm](age-of-ashes-bestiary/HbgvZUYFJd2VfwmQ.htm)|Tree of Dreadful Dreams|Árbol de los Sueños Espantosos|modificada|
|[HI2xA7LCpNPkpV03.htm](age-of-ashes-bestiary/HI2xA7LCpNPkpV03.htm)|Wailing Crystals|Cristales de Lamento|modificada|
|[hPPdfkiRZ1LUpN2h.htm](age-of-ashes-bestiary/hPPdfkiRZ1LUpN2h.htm)|Damurdiel's Vengeance|Venganza de Damurdiel|modificada|
|[IdpkEJycZgrkvdSn.htm](age-of-ashes-bestiary/IdpkEJycZgrkvdSn.htm)|Phantom Bells|Campanas Fantasma|modificada|
|[Ig1joUmSHSNL6QVU.htm](age-of-ashes-bestiary/Ig1joUmSHSNL6QVU.htm)|Ingnovim's Assistant|Asistente de Ingnovim|modificada|
|[j4BCcTvIMtoUPRzw.htm](age-of-ashes-bestiary/j4BCcTvIMtoUPRzw.htm)|Aluum Enforcer|Aluum Enforcer|modificada|
|[ja9KowUPZjhwhJ1x.htm](age-of-ashes-bestiary/ja9KowUPZjhwhJ1x.htm)|Echoes of Betrayal|Ecos de traición|modificada|
|[JD6kdfZveObBe1mR.htm](age-of-ashes-bestiary/JD6kdfZveObBe1mR.htm)|Xotanispawn|Engendro de Xotani|modificada|
|[jO2fGZayk4R1AzYK.htm](age-of-ashes-bestiary/jO2fGZayk4R1AzYK.htm)|Bida|Bida|modificada|
|[jZc4PrsX3HCJnkXx.htm](age-of-ashes-bestiary/jZc4PrsX3HCJnkXx.htm)|Emaliza Zandivar|Emaliza Zandivar|modificada|
|[kciXaXw31gHA3gZl.htm](age-of-ashes-bestiary/kciXaXw31gHA3gZl.htm)|Jahsi|Jahsi|modificada|
|[KEbmwWwmpAmIoBcm.htm](age-of-ashes-bestiary/KEbmwWwmpAmIoBcm.htm)|Mental Scream Trap|Mental Scream Trap|modificada|
|[kLX36WXp6rjTt71z.htm](age-of-ashes-bestiary/kLX36WXp6rjTt71z.htm)|Rinnarv Bontimar|Rinnarv Bontimar|modificada|
|[KROWh0CteDFFGsih.htm](age-of-ashes-bestiary/KROWh0CteDFFGsih.htm)|Hellcrown|Hellcrown|modificada|
|[KzKQYgiUZ3hJo473.htm](age-of-ashes-bestiary/KzKQYgiUZ3hJo473.htm)|Kalavakus|Kalavakus|modificada|
|[L4K4V09tQVXj7ZiI.htm](age-of-ashes-bestiary/L4K4V09tQVXj7ZiI.htm)|Dmiri Yoltosha|Dmiri Yoltosha|modificada|
|[L7IJO5z82nEN9IjM.htm](age-of-ashes-bestiary/L7IJO5z82nEN9IjM.htm)|Lazurite-Infused Stone Golem|Golem de piedra infundido con lazurita|modificada|
|[l7WhZMoYlQC8lELj.htm](age-of-ashes-bestiary/l7WhZMoYlQC8lELj.htm)|Dragonstorm|Dragonstorm|modificada|
|[lcYcBIFmfKFt8Hcs.htm](age-of-ashes-bestiary/lcYcBIFmfKFt8Hcs.htm)|Ingnovim Tluss|Ingnovim Tluss|modificada|
|[lDgabn0WtDKbLtfc.htm](age-of-ashes-bestiary/lDgabn0WtDKbLtfc.htm)|Luminous Ward|Luminous Ward|modificada|
|[lRqptquQcM6ZcQ4O.htm](age-of-ashes-bestiary/lRqptquQcM6ZcQ4O.htm)|Ishti|Ishti|modificada|
|[Lx5JiVOGWnzzjCrW.htm](age-of-ashes-bestiary/Lx5JiVOGWnzzjCrW.htm)|Scarlet Triad Bruiser|Bruiser Tríada Escarlata|modificada|
|[MFTMMXiwvW1aqOzs.htm](age-of-ashes-bestiary/MFTMMXiwvW1aqOzs.htm)|Kralgurn|Kralgurn|modificada|
|[MOyYEls8wNGHoe8F.htm](age-of-ashes-bestiary/MOyYEls8wNGHoe8F.htm)|Doorwarden|Doorwarden|modificada|
|[MWQmOXxGcbaMsXDD.htm](age-of-ashes-bestiary/MWQmOXxGcbaMsXDD.htm)|Hezle|Hezle|modificada|
|[mY494hn9sKVD9q8C.htm](age-of-ashes-bestiary/mY494hn9sKVD9q8C.htm)|Jaggaki|Jaggaki|modificada|
|[mYJ6NNthl702Pz2s.htm](age-of-ashes-bestiary/mYJ6NNthl702Pz2s.htm)|Scarlet Triad Agent|Agente de la Tríada Escarlata|modificada|
|[n6FQeNsDgKaDIF7b.htm](age-of-ashes-bestiary/n6FQeNsDgKaDIF7b.htm)|Spiritbound Aluum|Spiritbound Aluum|modificada|
|[N8vOjTD2SqS2b9Sy.htm](age-of-ashes-bestiary/N8vOjTD2SqS2b9Sy.htm)|Lesser Manifestation Of Dahak|Manifestación menor de Dahak|modificada|
|[NK49bh04355Lgz5r.htm](age-of-ashes-bestiary/NK49bh04355Lgz5r.htm)|Nketiah|Nketiah|modificada|
|[nQ2eBOpK71I8D2JC.htm](age-of-ashes-bestiary/nQ2eBOpK71I8D2JC.htm)|Scarlet Triad Poisoner|Envenenador de la Tríada Escarlata|modificada|
|[NrMgYvnIHFxeFEhX.htm](age-of-ashes-bestiary/NrMgYvnIHFxeFEhX.htm)|Dalos|Dalos|modificada|
|[NwLrwNqwedh95iry.htm](age-of-ashes-bestiary/NwLrwNqwedh95iry.htm)|Scarlet Triad Thug|Matón de la Tríada Escarlata|modificada|
|[NwNu2yvMnbvPvpY8.htm](age-of-ashes-bestiary/NwNu2yvMnbvPvpY8.htm)|Ekujae Guardian|Ekujae Guardian|modificada|
|[oHUIqVPBpHAbN3qO.htm](age-of-ashes-bestiary/oHUIqVPBpHAbN3qO.htm)|Gloomglow Mushrooms|Gloomglow Setas|modificada|
|[OJH8y8LqmgbkN8ce.htm](age-of-ashes-bestiary/OJH8y8LqmgbkN8ce.htm)|Ghastly Bear|Ghastly Bear|modificada|
|[oPXRJxxWrY2kz2qf.htm](age-of-ashes-bestiary/oPXRJxxWrY2kz2qf.htm)|Belmazog|Belmazog|modificada|
|[Oqj8XIWBb29NZ8QX.htm](age-of-ashes-bestiary/Oqj8XIWBb29NZ8QX.htm)|Tixitog|Tixitog|modificada|
|[Pmi71NUPAIE3YMws.htm](age-of-ashes-bestiary/Pmi71NUPAIE3YMws.htm)|Vision of Dahak|Visión de Dahak|modificada|
|[qemmmfu7exswGMGZ.htm](age-of-ashes-bestiary/qemmmfu7exswGMGZ.htm)|Kelda Halrig|Kelda Halrig|modificada|
|[QKkvnlqrhgLHuP1t.htm](age-of-ashes-bestiary/QKkvnlqrhgLHuP1t.htm)|Mialari Docur|Mialari Docur|modificada|
|[QoS5lEJqji2h490F.htm](age-of-ashes-bestiary/QoS5lEJqji2h490F.htm)|Lifeleech Crystal Patches|Parches de Cristal de Lifeleech|modificada|
|[qouYGPM8mE4KUCTe.htm](age-of-ashes-bestiary/qouYGPM8mE4KUCTe.htm)|Rusty Mae|Rusty Mae|modificada|
|[r02b4f1XNq7OihhD.htm](age-of-ashes-bestiary/r02b4f1XNq7OihhD.htm)|Renali|Renali|modificada|
|[rd8Scu1YBSyKFwnk.htm](age-of-ashes-bestiary/rd8Scu1YBSyKFwnk.htm)|Emperor Bird|Pájaro Emperador|modificada|
|[roRmUbaC9kJC7met.htm](age-of-ashes-bestiary/roRmUbaC9kJC7met.htm)|Endless Elven Aging|Endless Elven Aging|modificada|
|[scPhFbBqlmD2fQHa.htm](age-of-ashes-bestiary/scPhFbBqlmD2fQHa.htm)|Dragonscarred Dead|Muerto con cicatrices de dragón|modificada|
|[sqXPqmuUYOgxspV3.htm](age-of-ashes-bestiary/sqXPqmuUYOgxspV3.htm)|Barzillai's Hounds|Sabuesos de Barzillai|modificada|
|[sQZDwS08l6Agsryq.htm](age-of-ashes-bestiary/sQZDwS08l6Agsryq.htm)|Barushak Il-Varashma|Barushak Il-Varashma|modificada|
|[SUQJPKt8N7rxx7Q1.htm](age-of-ashes-bestiary/SUQJPKt8N7rxx7Q1.htm)|Spiked Doorframe|Spiked Doorframe|modificada|
|[SVh7cPwyGgmZORVz.htm](age-of-ashes-bestiary/SVh7cPwyGgmZORVz.htm)|Scarlet Triad Boss|Scarlet Triad Boss|modificada|
|[SVLUUPOXDINbKyFL.htm](age-of-ashes-bestiary/SVLUUPOXDINbKyFL.htm)|Malarunk|Malarunk|modificada|
|[sY47PB9b7nCJjgRq.htm](age-of-ashes-bestiary/sY47PB9b7nCJjgRq.htm)|Manifestation Of Dahak|Manifestación De Dahak|modificada|
|[T3UVfAfcvAMe9rih.htm](age-of-ashes-bestiary/T3UVfAfcvAMe9rih.htm)|Charau-ka Dragon Priest|Charau-ka Dragon Priest|modificada|
|[teabm1YiRAKdUaEQ.htm](age-of-ashes-bestiary/teabm1YiRAKdUaEQ.htm)|Seismic Spears Trap|Trampa de lanzas sísmicas|modificada|
|[tGyPrTGSpndXKU88.htm](age-of-ashes-bestiary/tGyPrTGSpndXKU88.htm)|Nolly Peltry|Nolly Peltry|modificada|
|[tr3qlFJVHqloE9zI.htm](age-of-ashes-bestiary/tr3qlFJVHqloE9zI.htm)|Forge-Spurned|Forge-Spurned|modificada|
|[U2QGjhcg5QFFkHwv.htm](age-of-ashes-bestiary/U2QGjhcg5QFFkHwv.htm)|Saggorak Poltergeist|Saggorak Poltergeist|modificada|
|[u9QIJIpkSgW5VRIG.htm](age-of-ashes-bestiary/u9QIJIpkSgW5VRIG.htm)|Town Hall Fire|Incendio en el Ayuntamiento|modificada|
|[UD7EwTQG2Sbl4d8R.htm](age-of-ashes-bestiary/UD7EwTQG2Sbl4d8R.htm)|Voz Lirayne|Voz Lirayne|modificada|
|[UQDxkvqXKg03ESTQ.htm](age-of-ashes-bestiary/UQDxkvqXKg03ESTQ.htm)|Gerhard Pendergrast|Gerhard Pendergrast|modificada|
|[VQiBBu6D2srK6Fmj.htm](age-of-ashes-bestiary/VQiBBu6D2srK6Fmj.htm)|Wrath of the Destroyer|Ira del Destructor|modificada|
|[w1Pqv0YmG4MevpQc.htm](age-of-ashes-bestiary/w1Pqv0YmG4MevpQc.htm)|Corrupt Guard|Guardia Corrupto|modificada|
|[We1Nq7CrqDQHEZxY.htm](age-of-ashes-bestiary/We1Nq7CrqDQHEZxY.htm)|Quarry Sluiceway|Cantera Sluiceway|modificada|
|[WouSA1UsIzvBTOix.htm](age-of-ashes-bestiary/WouSA1UsIzvBTOix.htm)|Carnivorous Crystal|Cristal Carnívoro|modificada|
|[X2edjYEqAHsdQc6L.htm](age-of-ashes-bestiary/X2edjYEqAHsdQc6L.htm)|Grippli Greenspeaker|Grippli Greenspeaker|modificada|
|[X4oeJnv1KcpHFO6i.htm](age-of-ashes-bestiary/X4oeJnv1KcpHFO6i.htm)|Xotani, The Firebleeder|Xotani, El Que Sangra Fuego|modificada|
|[X6TTBlHIfJZ43OqR.htm](age-of-ashes-bestiary/X6TTBlHIfJZ43OqR.htm)|Dragonshard Guardian|Guardián Dragonshard|modificada|
|[X7zSx8LFh2ZDnOYy.htm](age-of-ashes-bestiary/X7zSx8LFh2ZDnOYy.htm)|Teyam Ishtori|Teyam Ishtori|modificada|
|[XHJC4G6bfPiVXGKE.htm](age-of-ashes-bestiary/XHJC4G6bfPiVXGKE.htm)|Ilssrah Embermead|Ilssrah Embermead|modificada|
|[XNso2IMnhnfHcMCn.htm](age-of-ashes-bestiary/XNso2IMnhnfHcMCn.htm)|Zuferian|Zuferian|modificada|
|[xPV49ZBCwDdCq5eI.htm](age-of-ashes-bestiary/xPV49ZBCwDdCq5eI.htm)|Vazgorlu|Vazgorlu|modificada|
|[xrxjjjRMKzwsYbGm.htm](age-of-ashes-bestiary/xrxjjjRMKzwsYbGm.htm)|Accursed Forge-Spurned|Forja maldita|modificada|
|[xXtGS9uBa9z43X7y.htm](age-of-ashes-bestiary/xXtGS9uBa9z43X7y.htm)|Graveshell|Graveshell|modificada|
|[YdLf7y2ZHudpjWx9.htm](age-of-ashes-bestiary/YdLf7y2ZHudpjWx9.htm)|Living Sap|Savia viva|modificada|
|[YIXAcvSyI1C94r9l.htm](age-of-ashes-bestiary/YIXAcvSyI1C94r9l.htm)|Zephyr Guard|Guardia del Céfiro|modificada|
|[yLhO5vUrPQF42Lh8.htm](age-of-ashes-bestiary/yLhO5vUrPQF42Lh8.htm)|Charau-ka|Charau-ka|modificada|
|[yrU0xi4eKBmcGudo.htm](age-of-ashes-bestiary/yrU0xi4eKBmcGudo.htm)|Animated Dragonstorm|Tormenta de dragones animada|modificada|
|[YV3wA2Kgjp74l7YJ.htm](age-of-ashes-bestiary/YV3wA2Kgjp74l7YJ.htm)|Alak Stagram|Alak Stagram|modificada|
|[z3zWW4jLADAei1uo.htm](age-of-ashes-bestiary/z3zWW4jLADAei1uo.htm)|Ash Web|Ash Web|modificada|
|[z4rlpEsE2KgzpOCc.htm](age-of-ashes-bestiary/z4rlpEsE2KgzpOCc.htm)|Remnant of Barzillai|Los remanentes de Barzillai|modificada|
|[zdJHl3xMY7n2Lwlf.htm](age-of-ashes-bestiary/zdJHl3xMY7n2Lwlf.htm)|Trapped Lathe|Torno Atrapado|modificada|
|[zNIjGSxkG8xyDLgR.htm](age-of-ashes-bestiary/zNIjGSxkG8xyDLgR.htm)|Dragon Pillar|Pilar del Dragón|modificada|
|[ZqL6MP4XxDys9dmW.htm](age-of-ashes-bestiary/ZqL6MP4XxDys9dmW.htm)|Aiudara Wraith|Espectro de Aiudara|modificada|
|[ZRGRAqkDEZbRn8x3.htm](age-of-ashes-bestiary/ZRGRAqkDEZbRn8x3.htm)|Crucidaemon|Crucidaimonion|modificada|
|[ZtSb3mHZ5sD2uqHd.htm](age-of-ashes-bestiary/ZtSb3mHZ5sD2uqHd.htm)|Mud Spider|Araña de barro|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
