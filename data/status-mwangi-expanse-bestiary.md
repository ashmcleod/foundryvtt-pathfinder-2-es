# Estado de la traducción (mwangi-expanse-bestiary)

 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[XSWDwaLEyshEKSZE.htm](mwangi-expanse-bestiary/XSWDwaLEyshEKSZE.htm)|K'nonna|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
