# Estado de la traducción (spell-effects)

 * **modificada**: 366
 * **ninguna**: 18
 * **oficial**: 4


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[4ag0OHKfjROmR4Pm.htm](spell-effects/4ag0OHKfjROmR4Pm.htm)|Spell Effect: Anticipate Peril|
|[4vlorajqpFcS5Ozi.htm](spell-effects/4vlorajqpFcS5Ozi.htm)|Spell Effect: Flashy Disappearance|
|[5p3bKvWsJgo83FS1.htm](spell-effects/5p3bKvWsJgo83FS1.htm)|Aura: Protective Ward|
|[b8JCq6n1STl3Wkwy.htm](spell-effects/b8JCq6n1STl3Wkwy.htm)|Spell Effect: Illusory Shroud|
|[bBD7HFzBPlSxYrtW.htm](spell-effects/bBD7HFzBPlSxYrtW.htm)|Spell Effect: Wish-Twisted Form (Failure)|
|[EScdpppYsf9KhG4D.htm](spell-effects/EScdpppYsf9KhG4D.htm)|Spell Effect: Ghostly Weapon|
|[F1APSdrw5uv672hf.htm](spell-effects/F1APSdrw5uv672hf.htm)|Spell Effect: Battlefield Persistence|
|[HF1r1psnITHD52B9.htm](spell-effects/HF1r1psnITHD52B9.htm)|Aura: Dread Aura|
|[JHpYudY14g0H4VWU.htm](spell-effects/JHpYudY14g0H4VWU.htm)|Spell Effect: Stoneskin|
|[nLige83aiMBh0ylb.htm](spell-effects/nLige83aiMBh0ylb.htm)|Spell Effect: Musical Accompaniment|
|[qzZmVjtc9feqoQwA.htm](spell-effects/qzZmVjtc9feqoQwA.htm)|Spell Effect: Wish-Twisted Form (Success)|
|[rjM25qfw5BKj9h97.htm](spell-effects/rjM25qfw5BKj9h97.htm)|Spell Effect: Entangle|
|[ubHqpJwUwygkc2dR.htm](spell-effects/ubHqpJwUwygkc2dR.htm)|Spell Effect: Dread Aura|
|[VJpRUgSDtAO2TSRR.htm](spell-effects/VJpRUgSDtAO2TSRR.htm)|Spell Effect: Glimpse Weakness|
|[vUjRvriyuHDZrsgc.htm](spell-effects/vUjRvriyuHDZrsgc.htm)|Spell Effect: Ghostly Shift|
|[VVSOzHV6Rz2YNHRl.htm](spell-effects/VVSOzHV6Rz2YNHRl.htm)|Spell Effect: Contagious Idea (Pleasant Thought)|
|[Y6aNYnGVXdAMvL7Y.htm](spell-effects/Y6aNYnGVXdAMvL7Y.htm)|Spell Effect: Thermal Stasis|
|[zRKw95WMezr6TgiT.htm](spell-effects/zRKw95WMezr6TgiT.htm)|Spell Effect: Moon Frenzy|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[06zdFoxzuTpPPGyJ.htm](spell-effects/06zdFoxzuTpPPGyJ.htm)|Spell Effect: Rejuvenating Flames|Efecto de Hechizo: Flamígera rejuvenecedora.|modificada|
|[0bfqYkNaWsdTmtrc.htm](spell-effects/0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|Efecto de Hechizo: Compañero Juvenil|modificada|
|[0Cyf07wboRp4CmcQ.htm](spell-effects/0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|Efecto de Hechizo: Forma de dinosaurio (Ankylosaurus)|modificada|
|[0gv9D5RlrF5cKA3I.htm](spell-effects/0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|Efecto de Hechizo: Adaptar yo (visión en la oscuridad)|modificada|
|[0o984LjzIFXxeXIF.htm](spell-effects/0o984LjzIFXxeXIF.htm)|Spell Effect: Evolution Surge (Amphibious)|Efecto de Hechizo: Oleaje de Evolución (Anfibio)|modificada|
|[0OC945wcZ4H4akLz.htm](spell-effects/0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|Efecto de Hechizo: Visaje del Invocador|modificada|
|[0q2716S34XL1y9Hh.htm](spell-effects/0q2716S34XL1y9Hh.htm)|Spell Effect: Rapid Adaptation (Underground)|Efecto de Hechizo: Adaptación Rápida (Subterráneo)|modificada|
|[0QVufU5o3xIxiHmP.htm](spell-effects/0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|Efecto de Hechizo: Forma aérea (Pájaro)|modificada|
|[0R42NyuEZMVALjQs.htm](spell-effects/0R42NyuEZMVALjQs.htm)|Spell Effect: Traveler's Transit (Swim)|Efecto de Hechizo: Tránsito del viajero (Nadar).|modificada|
|[0s6YaL3IjqECmjab.htm](spell-effects/0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Wyrm|Efecto de Hechizo: Rugido del Wyrm|modificada|
|[0W87OkYi3qCwNGSj.htm](spell-effects/0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|Efecto de Hechizo: Forma daimónico (Piscodaemon)|modificada|
|[0yy4t4UY1HqrEo70.htm](spell-effects/0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|Efecto de Hechizo: Forma de Diablo (Barbazu)|modificada|
|[14AFzcwkN019dzcl.htm](spell-effects/14AFzcwkN019dzcl.htm)|Spell Effect: Shifting Form (Claws)|Efecto de Hechizo: Alterar forma (Garras)|modificada|
|[14m4s0FeRSqRlHwL.htm](spell-effects/14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|Efecto de Hechizo: Contramedidas arcanas|modificada|
|[16QrxIYal7PJFL2W.htm](spell-effects/16QrxIYal7PJFL2W.htm)|Spell Effect: Euphoric Renewal|Efecto de Hechizo: Renovación Eufórica|modificada|
|[1gTnFmfLUQnCo3TK.htm](spell-effects/1gTnFmfLUQnCo3TK.htm)|Spell Effect: Dread Ambience (Success)|Efecto de Hechizo: Ambiente de Terror (Éxito)|modificada|
|[1kelGCsoXyGRqMd9.htm](spell-effects/1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|Efecto de Hechizo: Edicto diabólico|modificada|
|[1n84AqLtsdT8I64W.htm](spell-effects/1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|Efecto de Hechizo: Forma daimónico (Ceustodaemon)|modificada|
|[1RsScTvNdGD9zGWe.htm](spell-effects/1RsScTvNdGD9zGWe.htm)|Spell Effect: Fire Shield|Efecto de Hechizo: Escudo de fuego|modificada|
|[1VuHjj32wge2gPOr.htm](spell-effects/1VuHjj32wge2gPOr.htm)|Spell Effect: Animal Feature (Wings)|Efecto de Hechizo: Animal Feature (Wings)|modificada|
|[20egTKICPMhibqgn.htm](spell-effects/20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|Efecto de Hechizo: Forma demoníaca (Vrock)|modificada|
|[28NvrpZmELvyrHUt.htm](spell-effects/28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|Efecto de Hechizo: Gravedad Variable (Gravedad Alta)|modificada|
|[2KQSsrzUqAxSXOdd.htm](spell-effects/2KQSsrzUqAxSXOdd.htm)|Spell Effect: Dancing Shield|Efecto de Hechizo: Escudo Danzante|modificada|
|[2sMXAGZfdqiy10kk.htm](spell-effects/2sMXAGZfdqiy10kk.htm)|Spell Effect: Clinging Ice|Efecto de Hechizo: Clinging Ice|modificada|
|[2Ss5VblfZNHg1HjN.htm](spell-effects/2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|Efecto de Hechizo: Forma Cósmica (Sol)|modificada|
|[2SWUzp4JuNK5EX0J.htm](spell-effects/2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|Efecto de Hechizo: Adaptarse (Nadar)|modificada|
|[2wfrhRLmmgPSKbAZ.htm](spell-effects/2wfrhRLmmgPSKbAZ.htm)|Spell Effect: Animal Feature (Claws)|Efecto de Hechizo: Característica Animal (Garras)|modificada|
|[3HEiYVhqypfc4IsP.htm](spell-effects/3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|Efecto de Hechizo: Salvaguardar secreto|modificada|
|[3Ktyd5F9lOPo4myk.htm](spell-effects/3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|Efecto de hechizo: Disfraz ilusorio.|modificada|
|[3LyOkV25p7wA181H.htm](spell-effects/3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|Efecto: Inmunidad a la orientación divina.|modificada|
|[3qHKBDF7lrHw8jFK.htm](spell-effects/3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Efecto del Hechizo: Orientación divina|modificada|
|[3vWfew0TIrcGRjLZ.htm](spell-effects/3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|Efecto de Hechizo: Forma de Ángel (Deva Monádica).|modificada|
|[3zdBGENpmaze1bpq.htm](spell-effects/3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|Efecto de Hechizo: Forma de Légamo (Cubo Gelatinoso)|modificada|
|[41WThj17MZBXTO2X.htm](spell-effects/41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|Efecto de Hechizo: Aumentar (Aumentado 4º)|modificada|
|[46vCC77mBNBWtmx3.htm](spell-effects/46vCC77mBNBWtmx3.htm)|Spell Effect: Litany of Righteousness|Efecto de Hechizo: Letanía de rectitud.|modificada|
|[4FD4vJqVpuuJjk9Q.htm](spell-effects/4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|Efecto de Hechizo: Cambio de Mente (Éxito Crítico)|modificada|
|[4iakL7fDcZ8RT6Tu.htm](spell-effects/4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|Efecto de Hechizo: Rostro en la multitud|modificada|
|[4ktNx3cVz5GkcGJa.htm](spell-effects/4ktNx3cVz5GkcGJa.htm)|Spell Effect: Untwisting Iron Augmentation|Efecto de Hechizo: Aumento de Hierro sin Torsión|modificada|
|[4Lo2qb5PmavSsLNk.htm](spell-effects/4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|Efecto de Hechizo: Égida de energía|modificada|
|[4Lt69zSeYElEfDrZ.htm](spell-effects/4Lt69zSeYElEfDrZ.htm)|Spell Effect: Dread Ambience (Critical Failure)|Efecto de Hechizo: Dread Ambience (Fallo Crítico)|modificada|
|[4VOZP2ArmS12nvz8.htm](spell-effects/4VOZP2ArmS12nvz8.htm)|Spell Effect: Draw Ire (Critical Failure)|Efecto de Hechizo: Dibujar Ire (Fallo Crítico)|modificada|
|[542Keo6txtq7uvqe.htm](spell-effects/542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|Efecto de Hechizo: Forma de dinosaurio (Tiranosaurio)|modificada|
|[57lnrCzGUcNUBP2O.htm](spell-effects/57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|Efecto de Hechizo: Avance atlético|modificada|
|[5MI2c9IgxfSeGZQo.htm](spell-effects/5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|Efecto de Hechizo: Salto del viento|modificada|
|[5R3ewWLFkgqTvZsc.htm](spell-effects/5R3ewWLFkgqTvZsc.htm)|Spell Effect: Elemental Gift (Fire)|Efecto de Hechizo: Don Elemental (Fuego)|modificada|
|[5xCheSMgtQhQZm00.htm](spell-effects/5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|Efecto de Hechizo: Jardín de la Muerte (Éxito Crítico)|modificada|
|[5yCL7InrJDHpaQjz.htm](spell-effects/5yCL7InrJDHpaQjz.htm)|Spell Effect: Ant Haul|Efecto de Hechizo: Capacidad de carga de la hormiga|modificada|
|[6BjslHgY01cNbKp5.htm](spell-effects/6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|Efecto de Hechizo: Armadura de Huesos|modificada|
|[6Ev2ytJZfr2t33iy.htm](spell-effects/6Ev2ytJZfr2t33iy.htm)|Spell Effect: Evolution Surge (Sight)|Efecto de Hechizo: Oleaje de Evolución (Vista)|modificada|
|[6GAztnHuQSwAp1k1.htm](spell-effects/6GAztnHuQSwAp1k1.htm)|Spell Effect: Adaptive Ablation|Efecto de Hechizo: Ablación Adaptativa|modificada|
|[6IvTWcispcDaw88N.htm](spell-effects/6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|Efecto de Hechizo: Forma de insecto (Hormiga).|modificada|
|[6qvLnIkWAoGvTIWy.htm](spell-effects/6qvLnIkWAoGvTIWy.htm)|Spell Effect: Community Repair (Critical Failure)|Efecto de Hechizo: Reparar Comunidad (Fallo Crítico)|modificada|
|[6SG8wVmppv4oXZtx.htm](spell-effects/6SG8wVmppv4oXZtx.htm)|Spell Effect: Mantle of the Magma Heart (Fiery Grasp)|Efecto de Hechizo: Manto del Corazón de Magma (Fiery Grasp)|modificada|
|[70qdCBokXBvKIUIQ.htm](spell-effects/70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|Efecto de Hechizo: Visión de Debilidad|modificada|
|[782NyomkDHyfsUn6.htm](spell-effects/782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Efecto de Hechizo: Forma de insecto (Araña)|modificada|
|[7tfF8ifVvOKNud8t.htm](spell-effects/7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|Efecto de Hechizo: Forma de Légamo (Légamo Gris)|modificada|
|[7tYv9lY3ksSUny2h.htm](spell-effects/7tYv9lY3ksSUny2h.htm)|Spell Effect: Gray Shadow|Efecto de Hechizo: Sombra Gris|modificada|
|[7vIUF5zbvHzVcJA0.htm](spell-effects/7vIUF5zbvHzVcJA0.htm)|Spell Effect: Longstrider (8 hours)|Efecto de Hechizo: Zancada prodigiosa (8 horas)|modificada|
|[7zJPd2BsFl82qFRV.htm](spell-effects/7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression (+3)|Efecto de Hechizo: Warding Aggression (+3)|modificada|
|[7zy4W2RXQiMEr6cp.htm](spell-effects/7zy4W2RXQiMEr6cp.htm)|Spell Effect: Dragon Claws|Efecto de Hechizo: Garras de dragón|modificada|
|[81TfqzTfIqkQA4Dy.htm](spell-effects/81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|Efecto de Hechizo: Tronante Dominancia|modificada|
|[8adLKKzJy49USYJt.htm](spell-effects/8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|Efecto de Hechizo: Canción de Fuerza|modificada|
|[8aNZhlkzRTRKlKag.htm](spell-effects/8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Gold)|Efecto de Hechizo: Forma de dragón (Oro)|modificada|
|[8ecGfjmxnBY3WWao.htm](spell-effects/8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|Efecto de Hechizo: Espesura de Cuchillos|modificada|
|[8eWLR0WCf5258z8X.htm](spell-effects/8eWLR0WCf5258z8X.htm)|Spell Effect: Elemental Form (Earth)|Efecto de Hechizo: Forma elemental (Tierra)|modificada|
|[8gqb5FMTaArsKdWB.htm](spell-effects/8gqb5FMTaArsKdWB.htm)|Spell Effect: Prismatic Armor|Efecto de Hechizo: Armadura Prismática|modificada|
|[8GUkKvCeI0xljCOk.htm](spell-effects/8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|Efecto de Hechizo: Vuelo del viento de la tormenta|modificada|
|[8olfnTmWh0GGPDqX.htm](spell-effects/8olfnTmWh0GGPDqX.htm)|Spell Effect: Ki Strike|Efecto de Hechizo: Golpe de ki|modificada|
|[8wCVSzWYcURWewbd.htm](spell-effects/8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|Efecto del hechizo: Maldición bestial (fallo).|modificada|
|[8XaSpienzVXLmcfp.htm](spell-effects/8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|Efecto del hechizo: Inspirar heroismo (Fuerza, +3)|modificada|
|[9yzlmYUdvdQshTDF.htm](spell-effects/9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|Efecto de Hechizo: Megáfono|modificada|
|[9ZIP6gWSp9OTEu8i.htm](spell-effects/9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|Efecto de Hechizo: Biblioteca de Bolsillo|modificada|
|[a3uZckqOY9zQWzZ2.htm](spell-effects/a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|Efecto de Hechizo: Leer el Aire|modificada|
|[A48jNUOAmCljx8Ru.htm](spell-effects/A48jNUOAmCljx8Ru.htm)|Spell Effect: Shifting Form (Darkvision)|Efecto de Hechizo: Alterar forma (visión en la oscuridad)|modificada|
|[a5rWrWwuevTzs9Io.htm](spell-effects/a5rWrWwuevTzs9Io.htm)|Spell Effect: Wild Shape|Efecto de Hechizo: Forma salvaje|modificada|
|[A61eVVVyUuaUl3tz.htm](spell-effects/A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|Efecto de Hechizo: Marca celestial|modificada|
|[aDOL3OAEWf3ka9oT.htm](spell-effects/aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|Efecto de Hechizo: Blood Ward|modificada|
|[afJCG4vC5WF5h5IB.htm](spell-effects/afJCG4vC5WF5h5IB.htm)|Spell Effect: Clawsong (Damage Increase D8)|Efecto de Hechizo: Clawsong (Aumento de Daño D8)|modificada|
|[AJkRUIdYLnt4QOOg.htm](spell-effects/AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|Efecto de Hechizo: Tentar al Destino|modificada|
|[AM49w68oKykc2fHI.htm](spell-effects/AM49w68oKykc2fHI.htm)|Spell Effect: Rapid Adaptation (Plains)|Efecto de Hechizo: Adaptación Rápida (Llanuras)|modificada|
|[amTa9jSml9ioKduN.htm](spell-effects/amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|Efecto del hechizo: forma de insecto (escarabajo).|modificada|
|[an4yZ6dyIDOFa1wa.htm](spell-effects/an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|Efecto de Hechizo: Palabras de calma|modificada|
|[AnawxScxqUiRuGTm.htm](spell-effects/AnawxScxqUiRuGTm.htm)|Spell Effect: Divine Vessel 9th level (Evil)|Efecto de Hechizo: Recipiente divino 9º nivel (Maligno).|modificada|
|[AnCRD7kDcG0DDGKn.htm](spell-effects/AnCRD7kDcG0DDGKn.htm)|Spell Effect: Fungal Infestation (Failure)|Efecto de Hechizo: Infestación fúngica (Fallo).|modificada|
|[b5OyBdc0bolgWZZT.htm](spell-effects/b5OyBdc0bolgWZZT.htm)|Spell Effect: Tempest Form (Air)|Efecto de Hechizo: Forma de Tempestad (Aire)|modificada|
|[b8bfWIICHOsGVzjp.htm](spell-effects/b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|Efecto de Hechizo: Forma monstruosa (Fénix)|modificada|
|[Bc2Bwuan3716eAyY.htm](spell-effects/Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|Efecto de Hechizo: Fuente de Serenidad|modificada|
|[Bd86oAvK3RLN076H.htm](spell-effects/Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|Efecto de Hechizo: Forma de Angel (Deva Movanica)|modificada|
|[BDMEqBsumguTrMXa.htm](spell-effects/BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|Efecto de Hechizo: Forma de Diablo (Erinys)|modificada|
|[beReeFroAx24hj83.htm](spell-effects/beReeFroAx24hj83.htm)|Spell Effect: Inspire Courage|Efecto de Hechizo: Inspirar valor|modificada|
|[BfaFe1cI9IkpvmmY.htm](spell-effects/BfaFe1cI9IkpvmmY.htm)|Spell Effect: Countless Eyes|Efecto de Hechizo: Ojos innumerables|modificada|
|[BKam63zT98iWMJH7.htm](spell-effects/BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|Efecto del hechizo: Inspirar heroísmo (Defensa, +3).|modificada|
|[blBXnWb1Y8q8YYMh.htm](spell-effects/blBXnWb1Y8q8YYMh.htm)|Spell Effect: Primal Summons (Fire)|Efecto de Hechizo: Convocación primigenia (Fuego).|modificada|
|[bOjuEX3qj7XAOoDF.htm](spell-effects/bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Efecto del hechizo: forma de insecto (escorpión).|modificada|
|[BsGZdgiEElNlgZVv.htm](spell-effects/BsGZdgiEElNlgZVv.htm)|Spell Effect: Draw Ire (Failure)|Efecto de Hechizo: Dibujar Ire (Fallo)|modificada|
|[BT1ofB6RvRocQOWO.htm](spell-effects/BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|Efecto de Hechizo: Forma de animal (Toro)|modificada|
|[buXx8Azr4BYWPtFg.htm](spell-effects/buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|Efecto del hechizo: Vendetta de sangre (Fallo)|modificada|
|[byXkHIKFwuKrZ55M.htm](spell-effects/byXkHIKFwuKrZ55M.htm)|Spell Effect: Shifting Form (Scent)|Efecto de Hechizo: Alterar forma (Olor)|modificada|
|[C3RdbEQTvawqKAhw.htm](spell-effects/C3RdbEQTvawqKAhw.htm)|Spell Effect: Tempest Form (Water)|Efecto de Hechizo: Forma de Tempestad (Agua)|modificada|
|[c4cIfS2974nUJDPt.htm](spell-effects/c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|Efecto de Hechizo: Forma feérica (dríada)|modificada|
|[ceEA7nBGNmoR8Sjj.htm](spell-effects/ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|Efecto de Hechizo: Letanía del Interés Propio|modificada|
|[Chol7ExtoN2T36mP.htm](spell-effects/Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|Efecto del hechizo: Inspirar heroísmo (Defensa, +2).|modificada|
|[con2Hzt47JjpuUej.htm](spell-effects/con2Hzt47JjpuUej.htm)|Spell Effect: Resist Energy|Efecto de Hechizo: Resistir energía.|modificada|
|[cSoL5aMy3PCzM4Yv.htm](spell-effects/cSoL5aMy3PCzM4Yv.htm)|Spell Effect: Return the Favor|Efecto de Hechizo: Retornar el Favor|modificada|
|[cTBYHfiXDOA09G4b.htm](spell-effects/cTBYHfiXDOA09G4b.htm)|Spell Effect: Traveler's Transit (Fly)|Efecto de Hechizo: Tránsito del viajero (Volar).|modificada|
|[CTdEsMIwVYqqkH50.htm](spell-effects/CTdEsMIwVYqqkH50.htm)|Spell Effect: Litany of Depravity|Efecto de Hechizo: Letanía de Depravación|modificada|
|[ctMxYPGEpstvhW9C.htm](spell-effects/ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|Efecto de Hechizo: Custodia amenazadora|modificada|
|[cVVZXNbV0nElVOPZ.htm](spell-effects/cVVZXNbV0nElVOPZ.htm)|Spell Effect: Light|Efecto de Hechizo: Luz|modificada|
|[CWC2fPmlgixoIKy5.htm](spell-effects/CWC2fPmlgixoIKy5.htm)|Spell Effect: Clawsong (Damage Increase D6)|Efecto de Hechizo: Clawsong (Aumento de Daño D6)|modificada|
|[cwetyC5o4dRyFWJZ.htm](spell-effects/cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|Efecto de Hechizo: Generosidad del Nigromante|modificada|
|[czteoX2cggQzfkK9.htm](spell-effects/czteoX2cggQzfkK9.htm)|Spell Effect: Evolution Surge (Climb)|Efecto de Hechizo: Oleaje de Evolución (Trepar)|modificada|
|[D0Qj5tC1hGUjzQc4.htm](spell-effects/D0Qj5tC1hGUjzQc4.htm)|Spell Effect: Elemental Motion (Water)|Efecto de Hechizo: Movimiento elemental (Agua)|modificada|
|[DBaMtFHRPEg1JeLs.htm](spell-effects/DBaMtFHRPEg1JeLs.htm)|Spell Effect: Mind Blank|Spell Effect: Mente en Blanco|modificada|
|[dCQCzapIk53xmDo5.htm](spell-effects/dCQCzapIk53xmDo5.htm)|Spell Effect: Animal Feature (Cat Eyes)|Efecto de Hechizo: Característica Animal (Ojos de Gato)|modificada|
|[deG1dtfuQph03Kkg.htm](spell-effects/deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|Efecto de Hechizo: Shillelagh|modificada|
|[DENMzySYANjUBs4O.htm](spell-effects/DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Efecto del hechizo: Forma de insecto (Ciempiés).|modificada|
|[dEsaufFnfYihu5Ex.htm](spell-effects/dEsaufFnfYihu5Ex.htm)|Spell Effect: Discern Secrets (Sense Motive)|Efecto de Hechizo: Discernir Secretos (Sentido Motivo)|modificada|
|[DHYWmMGmKOpRSqza.htm](spell-effects/DHYWmMGmKOpRSqza.htm)|Spell Effect: Chromatic Armor|Efecto de Hechizo: Armadura Cromática|modificada|
|[dIftJU6Ki2QSLCOD.htm](spell-effects/dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel 9th level (Chaotic)|Efecto de Hechizo: Recepáculo divino 9º nivel (Caótico).|modificada|
|[DliizYpHcmBG130w.htm](spell-effects/DliizYpHcmBG130w.htm)|Spell Effect: Elemental Form (Air)|Efecto de Hechizo: Forma elemental (Aire)|modificada|
|[DLwTvjjnqs2sNGuG.htm](spell-effects/DLwTvjjnqs2sNGuG.htm)|Spell Effect: Inspire Defense|Efecto del hechizo: Inspirar defensa|modificada|
|[DrNpuMj14wVj4bWF.htm](spell-effects/DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Copper)|Efecto de Hechizo: Forma de dragón (Cobre).|modificada|
|[dWbg2gACxMkSnZag.htm](spell-effects/dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|Efecto de Hechizo: Custodia protectora.|modificada|
|[DwM5qcFp4JgKhXrY.htm](spell-effects/DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|Efecto de Hechizo: Forma feérica (Unicornio)|modificada|
|[dXq7z633ve4E0nlX.htm](spell-effects/dXq7z633ve4E0nlX.htm)|Spell Effect: Regenerate|Efecto de Hechizo: Regenerar|modificada|
|[ei9MIyZbIaP4AZmh.htm](spell-effects/ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|Efecto de Hechizo: Flame Wispígera|modificada|
|[Eik8Fj8nGo2GLcbn.htm](spell-effects/Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|Efecto de Hechizo: Forma monstruosa (Serpiente marina)|modificada|
|[EKdqKCuyWSkpXpyJ.htm](spell-effects/EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|Efecto de Hechizo: Forma de exudado (Budín negro)|modificada|
|[eotqxEWIgaK7nMpD.htm](spell-effects/eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|Efecto del hechizo: Blunt the Final Blade (Éxito crítico).|modificada|
|[ETgzIIv3M2zvclAR.htm](spell-effects/ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Blue)|Efecto de Hechizo: Forma de dragón (Azul)|modificada|
|[EUxTav62IXTz5CxW.htm](spell-effects/EUxTav62IXTz5CxW.htm)|Spell Effect: Nature Incarnate (Kaiju)|Efecto de Hechizo: Naturaleza encarnada (Kaiju).|modificada|
|[evK8JR3j2iWGWaug.htm](spell-effects/evK8JR3j2iWGWaug.htm)|Spell Effect: Divine Vessel (Evil)|Efecto de Hechizo: Recipiente divino (maligno).|modificada|
|[F10ofwC0k1ELIaV4.htm](spell-effects/F10ofwC0k1ELIaV4.htm)|Spell Effect: Impeccable Flow (Critical Failure Effect)|Efecto de hechizo: Flujo impecable (Efecto de fallo crítico).|modificada|
|[F4DTpDXNu5IliyhJ.htm](spell-effects/F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|Efecto de Hechizo: Forma de animal (Ciervo)|modificada|
|[fCIT9YgGUwIc3Z9G.htm](spell-effects/fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|Efecto de Hechizo: Atraer el Rayo|modificada|
|[FD9Ce5pqcZYstcMI.htm](spell-effects/FD9Ce5pqcZYstcMI.htm)|Spell Effect: Blessing of Defiance|Efecto de Hechizo: Bendición de Desafío|modificada|
|[fEhCbATDNlt6c1Ug.htm](spell-effects/fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|Efecto de Hechizo: Extraer Veneno|modificada|
|[fGK6zJ7mWz9D5QYo.htm](spell-effects/fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation (Aquatic Base Swim Speed)|Efecto de Hechizo: Adaptación Rápida (Velocidad de Nadar Base Acuática).|modificada|
|[fIloZhZVH1xTnX4B.htm](spell-effects/fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|Efecto de Hechizo: Forma de planta (Broza movediza).|modificada|
|[Fjnm1l59KH5YJ7G9.htm](spell-effects/Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|Efecto del hechizo: Inspirar heroismo (Fuerza, +2)|modificada|
|[fKeZDm8kpDFK5HWp.htm](spell-effects/fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|Efecto de Hechizo: Forma de Diablo (Sarglagon)|modificada|
|[fLOQMycP5tmXgPv9.htm](spell-effects/fLOQMycP5tmXgPv9.htm)|Spell Effect: Elemental Gift (Earth)|Efecto de Hechizo: Don elemental (Tierra)|modificada|
|[fpGDAz2v5PG0zUSl.htm](spell-effects/fpGDAz2v5PG0zUSl.htm)|Spell Effect: True Strike|Spell Effect: Golpe verdadero.|modificada|
|[FR4ucNi2ceHZdrpB.htm](spell-effects/FR4ucNi2ceHZdrpB.htm)|Spell Effect: Warding Aggression (+1)|Efecto de Hechizo: Warding Aggression (+1)|modificada|
|[FT5Tt2DKBRutDqbV.htm](spell-effects/FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience (Critical Success)|Efecto de Hechizo: Ambiente de Terror (Éxito Crítico)|modificada|
|[fvIlSZPwojixVvyZ.htm](spell-effects/fvIlSZPwojixVvyZ.htm)|Spell Effect: Lucky Number|Efecto de Hechizo: Número de la Suerte|modificada|
|[fwaAe71qfnK7SiOB.htm](spell-effects/fwaAe71qfnK7SiOB.htm)|Spell Effect: Primal Summons (Air)|Efecto de Hechizo: Convocación primigenia (Aire).|modificada|
|[g4E9l4uA62LcRBJS.htm](spell-effects/g4E9l4uA62LcRBJS.htm)|Spell Effect: Clawsong (Versatile Piercing)|Efecto de Hechizo: Clawsong (Versatile Piercing)|modificada|
|[GDzn5DToE62ZOTrP.htm](spell-effects/GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel (Chaotic)|Efecto de Hechizo: Recepáculo divino (caótico).|modificada|
|[GhGoZdAZtzZTYCzj.htm](spell-effects/GhGoZdAZtzZTYCzj.htm)|Spell Effect: Animal Feature (Jaws)|Efecto de Hechizo: Característica Animal (Fauces)|modificada|
|[GhNVAYtoF5hK3AlD.htm](spell-effects/GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of Corruption|Efecto de Hechizo: Toque de Corrupción|modificada|
|[gKGErrsS1WoAyWub.htm](spell-effects/gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|Efecto de Hechizo: Forma aberrante (Gogiteth)|modificada|
|[GlggmEqkGVj1noOD.htm](spell-effects/GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|Efecto del hechizo: Embotellar la tormenta|modificada|
|[GnWkI3T3LYRlm3X8.htm](spell-effects/GnWkI3T3LYRlm3X8.htm)|Spell Effect: Magic Weapon|Efecto de Hechizo: Arma mágica|modificada|
|[gQnDKDeBTtjwOWAk.htm](spell-effects/gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|Efecto de Hechizo: Forma de animal (Oso)|modificada|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Efecto del hechizo: Bendecir|modificada|
|[gX8O0ArQXbEVDUbW.htm](spell-effects/gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|Efecto de Hechizo: Abrazar el foso|modificada|
|[h0CKGrgjGNSg21BW.htm](spell-effects/h0CKGrgjGNSg21BW.htm)|Spell Effect: Boost Eidolon|Efecto de Hechizo: Impulsar Eidolon|modificada|
|[h2JzNunzO8hXiNV3.htm](spell-effects/h2JzNunzO8hXiNV3.htm)|Spell Effect: Lifelink Surge|Efecto de Hechizo: Oleaje Lifelink|modificada|
|[H6ndYYYlADWwqVQb.htm](spell-effects/H6ndYYYlADWwqVQb.htm)|Spell Effect: Dragon Form (White)|Efecto de Hechizo: Forma de dragón (Blanco)|modificada|
|[HDKJAUXMbtxnBdgR.htm](spell-effects/HDKJAUXMbtxnBdgR.htm)|Spell Effect: Tempest Form (Mist)|Efecto de Hechizo: Forma Tempestad (Niebla)|modificada|
|[hdOb5Iu6Zd3pHoGI.htm](spell-effects/hdOb5Iu6Zd3pHoGI.htm)|Spell Effect: Discern Secrets (Recall Knowledge)|Efecto de Hechizo: Discernir secretos (Recordar conocimiento)|modificada|
|[heAj9paC8ZRh7QEj.htm](spell-effects/heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|Spell Effect: Forma feérica (Redcap)|modificada|
|[HEbbxKtBzsLhFead.htm](spell-effects/HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|Efecto Hechizo: Forma de Diablo (Osyluth)|modificada|
|[hkLhZsH3T6jc9S1y.htm](spell-effects/hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|Efecto de Hechizo: Velo de Sue|modificada|
|[hnfQyf05IIa7WPBB.htm](spell-effects/hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|Efecto de Hechizo: Forma demoníaca (Nabasu)|modificada|
|[HoCUCi2jL1OLfXWR.htm](spell-effects/HoCUCi2jL1OLfXWR.htm)|Spell Effect: Unblinking Flame Aura|Efecto de Hechizo: Aura de llamas sin parpadear.|modificada|
|[HoOujAdQWCN4E6sQ.htm](spell-effects/HoOujAdQWCN4E6sQ.htm)|Spell Effect: Barkskin|Efecto del hechizo: piel robliza|modificada|
|[HtaDbgTIzdiTiKLX.htm](spell-effects/HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|Efecto de Hechizo: Triplicar la marcha|modificada|
|[hXtK08bTnDBSzGTJ.htm](spell-effects/hXtK08bTnDBSzGTJ.htm)|Spell Effect: Iron Gut|Efecto de Hechizo: Tripa de Hierro|modificada|
|[hya8NfBB1GJofTXm.htm](spell-effects/hya8NfBB1GJofTXm.htm)|Spell Effect: Unblinking Flame Ignition|Efecto de Hechizo: Encendido de Llama sin parpadear.|modificada|
|[I4PsUAaYSUJ8pwKC.htm](spell-effects/I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|Efecto de Hechizo: Rayo de escarcha|modificada|
|[i9YITDcrq1nKjV5l.htm](spell-effects/i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody|Efecto del hechizo: Melodía infecciosa.|modificada|
|[ib58LaffEUIypuzL.htm](spell-effects/ib58LaffEUIypuzL.htm)|Spell Effect: Evolution Surge (Huge)|Efecto de Hechizo: Oleaje de Evolución (Enorme)|modificada|
|[iiV80Kexj6vPmzqU.htm](spell-effects/iiV80Kexj6vPmzqU.htm)|Spell Effect: Rapid Adaptation (Arctic)|Efecto de Hechizo: Adaptación Rápida (Ártico)|modificada|
|[iJ7TVW5tDnZG9DG8.htm](spell-effects/iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|Efecto de Hechizo: Ventaja competitiva|modificada|
|[inNfTmtWpsxeGBI9.htm](spell-effects/inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|Efecto de Hechizo: Visión en la oscuridad (24 horas)|modificada|
|[iOKhr2El8R6cz6YI.htm](spell-effects/iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Efecto de Hechizo: Forma de dinosaurio (Triceratops)|modificada|
|[iqtjMVl6rGQhX2k8.htm](spell-effects/iqtjMVl6rGQhX2k8.htm)|Spell Effect: Elemental Motion (Air)|Efecto de Hechizo: Movimiento elemental (Aire)|modificada|
|[itmiGioGNuVvt4QE.htm](spell-effects/itmiGioGNuVvt4QE.htm)|Spell Effect: Rapid Adaptation (Aquatic Speed Bonus)|Efecto de Hechizo: Adaptación Rápida (Bonificación de Velocidad Acuática).|modificada|
|[IWD5RehCxZVfgrX9.htm](spell-effects/IWD5RehCxZVfgrX9.htm)|Spell Effect: Elephant Form|Efecto de Hechizo: Forma de Elefante|modificada|
|[IXS15IQXYCZ8vsmX.htm](spell-effects/IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|Efecto de Hechizo: Visión en la oscuridad.|modificada|
|[iZYjxY0qYvg5yPP3.htm](spell-effects/iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|Efecto de Hechizo: Alas angelicales|modificada|
|[j2LhQ7kEQhq3J3zZ.htm](spell-effects/j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|Efecto de Hechizo: Forma de animal (Rana)|modificada|
|[J60rN48XzBGHmR6m.htm](spell-effects/J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|Efecto de Hechizo: Elemento Encarnado (Aire)|modificada|
|[j6po934p4jcUVC6l.htm](spell-effects/j6po934p4jcUVC6l.htm)|Spell Effect: Shifting Form (Speed)|Efecto de Hechizo: Alterar forma (Velocidad).|modificada|
|[j9l4LDnAwg9xzYsy.htm](spell-effects/j9l4LDnAwg9xzYsy.htm)|Spell Effect: Life Connection|Efecto de Hechizo: Conexión de Vida|modificada|
|[Jemq5UknGdMO7b73.htm](spell-effects/Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Efecto de Hechizo: Escudo|modificada|
|[JhihziXQuoteftdd.htm](spell-effects/JhihziXQuoteftdd.htm)|Spell Effect: Lay on Hands (Vs. Undead)|Efecto del Hechizo: Imposición de manos (Vs. muertos vivientes).|modificada|
|[jj0P4eGVpmdwZjlA.htm](spell-effects/jj0P4eGVpmdwZjlA.htm)|Spell Effect: Instant Armor|Efecto del Hechizo: Armadura Instantánea|modificada|
|[jp88SCE3VCRAyE6x.htm](spell-effects/jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|Efecto de Hechizo: Elemento encarnado (Tierra)|modificada|
|[JqrTrvwV7pYStMXz.htm](spell-effects/JqrTrvwV7pYStMXz.htm)|Spell Effect: Levitate|Efecto de Hechizo: Levitar|modificada|
|[JrNHFNxJayevlv2G.htm](spell-effects/JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|Efecto de Hechizo: Forma de planta (Atrapamoscas)|modificada|
|[jtMo6qS47hPx6EbR.htm](spell-effects/jtMo6qS47hPx6EbR.htm)|Spell Effect: Rapid Adaptation (Forest)|Efecto de Hechizo: Adaptación Rápida (Bosque)|modificada|
|[jtW3VfI5Kktuy3GH.htm](spell-effects/jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Bronze)|Efecto de Hechizo: Forma de dragón (Bronce)|modificada|
|[jvwKRHtOiPAm4uAP.htm](spell-effects/jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|Efecto de Hechizo: Forma aérea (Murciélago).|modificada|
|[jy4edd6pvJvJgOSP.htm](spell-effects/jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings (Own Speed)|Efecto de Hechizo: Alas de dragón (Velocidad propia).|modificada|
|[KcBqo33ekJHxZLHo.htm](spell-effects/KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|Efecto de Hechizo: Forma feérica (Naiad)|modificada|
|[KkDRRDuycXwKPa6n.htm](spell-effects/KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|Efecto de Hechizo: Forma de dinosaurio (Brontosaurio)|modificada|
|[KMF2t3qqzyFP0rxL.htm](spell-effects/KMF2t3qqzyFP0rxL.htm)|Spell Effect: Divine Vessel (Good)|Efecto de Hechizo: Recipiente divino (Bueno).|modificada|
|[kMoOWWBqDYmPcYyS.htm](spell-effects/kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|Efecto de Hechizo: Baño de Sangre|modificada|
|[KtAJN4Qr2poTL6BB.htm](spell-effects/KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|Efecto del hechizo: Maldición bestial (Fallo crítico).|modificada|
|[kxMBdANwCcF841uA.htm](spell-effects/kxMBdANwCcF841uA.htm)|Spell Effect: Elemental Form (Water)|Efecto de Hechizo: Forma elemental (Agua)|modificada|
|[kZ39XWJA3RBDTnqG.htm](spell-effects/kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|Efecto del hechizo: Inspirar heroísmo (Valor, +2)|modificada|
|[kz3mlFwb9tV9bFwu.htm](spell-effects/kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|Efecto de Hechizo: Forma de animal (Serpiente)|modificada|
|[l8HkOKfiUqd3BUwT.htm](spell-effects/l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|Efecto de Hechizo: Forma Ancestral|modificada|
|[l9HRQggofFGIxEse.htm](spell-effects/l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|Efecto del hechizo: Heroísmo.|modificada|
|[lEU3DH1tGjAigpEt.htm](spell-effects/lEU3DH1tGjAigpEt.htm)|Spell Effect: Energy Absorption|Efecto de Hechizo: Absorción de energía.|modificada|
|[LHREWCGPkWsc4GGJ.htm](spell-effects/LHREWCGPkWsc4GGJ.htm)|Spell Effect: Faerie Dust (Failure)|Efecto de Hechizo: Polvo feérico (Fallo)|modificada|
|[lIl0yYdS9zojOZhe.htm](spell-effects/lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|Efecto de Hechizo: Forma Vivificante|modificada|
|[LldX5hnNhKzGtOS0.htm](spell-effects/LldX5hnNhKzGtOS0.htm)|Spell Effect: Elemental Absorption|Efecto de Hechizo: Absorción Elemental|modificada|
|[llrOM8rPP9nxIuEN.htm](spell-effects/llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|Efecto de Hechizo: Forma de insecto (Mantis)|modificada|
|[lmAwCy7isFvLYdGd.htm](spell-effects/lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|Efecto de Hechizo: Elemento Encarnado (Fuego)|modificada|
|[LMXxICrByo7XZ3Q3.htm](spell-effects/LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|Efecto de Hechizo: Aguacero|modificada|
|[LMzFBnOEPzDGzHg4.htm](spell-effects/LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|Spell Effect: Anatomía inusual.|modificada|
|[LT5AV9vSN3T9x3J9.htm](spell-effects/LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|Efecto Hechizo: Cuerpo Corrosivo|modificada|
|[lTL5VwNrZ5xiitGV.htm](spell-effects/lTL5VwNrZ5xiitGV.htm)|Spell effect: Nudge the Odds|Efecto de hechizo: Nudge the Odds|modificada|
|[LXf1Cqi1zyo4DaLv.htm](spell-effects/LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|Efecto de Hechizo: Encoger|modificada|
|[lyLMiauxIVUM3oF1.htm](spell-effects/lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|Efecto de Hechizo: Imposición de manos|modificada|
|[lywUJljQy2SxRZQt.htm](spell-effects/lywUJljQy2SxRZQt.htm)|Spell Effect: Nature Incarnate (Green Man)|Efecto de Hechizo: Naturaleza encarnada (Hombre verde).|modificada|
|[m1tQTBrolf7uZBW0.htm](spell-effects/m1tQTBrolf7uZBW0.htm)|Spell Effect: Discern Secrets (Seek)|Efecto de Hechizo: Discernir Secretos (Buscar)|modificada|
|[m6x0IvoeX0a0bZiQ.htm](spell-effects/m6x0IvoeX0a0bZiQ.htm)|Spell Effect: Unbreaking Wave Vapor|Efecto de Hechizo: Vapor de Onda Irrompible|modificada|
|[mAofA4oy3cRdT71K.htm](spell-effects/mAofA4oy3cRdT71K.htm)|Spell Effect: Penumbral Disguise|Efecto de Hechizo: Disfraz Penumbral|modificada|
|[mCb9mWAmgWPQrkTY.htm](spell-effects/mCb9mWAmgWPQrkTY.htm)|Spell Effect: Barkskin (Arboreal's Revenge)|Efecto de Hechizo: Piel robliza (Venganza arboral (pl. -es))|modificada|
|[Me470HI6inX3Bovh.htm](spell-effects/Me470HI6inX3Bovh.htm)|Spell Effect: Guided Introspection|Efecto de Hechizo: Introspección Guiada|modificada|
|[mhklZ6wjfty0bF44.htm](spell-effects/mhklZ6wjfty0bF44.htm)|Spell Effect: Speaking Sky|Efecto de Hechizo: Cielo Parlante|modificada|
|[MJSoRFfEdM4j5mNG.htm](spell-effects/MJSoRFfEdM4j5mNG.htm)|Spell Effect: Sweet Dream (Glamour)|Efecto de Hechizo: Dulce Sue (Glamour)|modificada|
|[mmJNE57hC7G3SPae.htm](spell-effects/mmJNE57hC7G3SPae.htm)|Spell Effect: Silence|Efecto de Hechizo: Silencio|modificada|
|[Mp7252yAsSA8lCEA.htm](spell-effects/Mp7252yAsSA8lCEA.htm)|Spell Effect: One With the Land|Efecto de Hechizo: Uno con la Tierra|modificada|
|[MqZ6FScbfGtXB8tt.htm](spell-effects/MqZ6FScbfGtXB8tt.htm)|Spell Effect: Magic Fang|Efecto de Hechizo: Colmillo mágico|modificada|
|[mr6mlkUMeStdChxi.htm](spell-effects/mr6mlkUMeStdChxi.htm)|Spell Effect: Animal Feature (Owl Eyes)|Efecto de Hechizo: Animal Feature (Owl Eyes)|modificada|
|[mrSulUdNbwzGSwfu.htm](spell-effects/mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaw|Efecto de Hechizo: Mandíbulas del glotón.|modificada|
|[MuRBCiZn5IKeaoxi.htm](spell-effects/MuRBCiZn5IKeaoxi.htm)|Spell Effect: Fly|Efecto de Hechizo: Volar|modificada|
|[mzDgsuuo5wCgqyxR.htm](spell-effects/mzDgsuuo5wCgqyxR.htm)|Spell Effect: Mirecloak|Spell Effect: Mirecloak|modificada|
|[N1EM3jRyT8PCG1Py.htm](spell-effects/N1EM3jRyT8PCG1Py.htm)|Spell Effect: Traveler's Transit (Climb)|Efecto de Hechizo: Tránsito del viajero (Trepar).|modificada|
|[n6NK7wqhTxWr3ij8.htm](spell-effects/n6NK7wqhTxWr3ij8.htm)|Spell Effect: Warding Aggression (+2)|Efecto de Hechizo: Warding Agresión (+2)|modificada|
|[nbW4udOUTrCGL3Gf.htm](spell-effects/nbW4udOUTrCGL3Gf.htm)|Spell Effect: Shifting Form (Climb Speed)|Efecto de Hechizo: Alterar forma cambiante (Velocidad de Trepar).|modificada|
|[ndj0TpLxyzbyzcm4.htm](spell-effects/ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|Efecto de Hechizo: Necrotizar (Piernas)|modificada|
|[nemThuhp3praALY6.htm](spell-effects/nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|Efecto de Hechizo: Convicción fervorosa|modificada|
|[nHXKK4pRXAzrLdEP.htm](spell-effects/nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take Its Course (Affliction, Help)|Efecto de Hechizo: Sigue su curso (Aflicción, Ayuda)|modificada|
|[nIryhRgeiacQw1Em.htm](spell-effects/nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|Efecto de Hechizo: Flores amansadoras|modificada|
|[nkk4O5fyzrC0057i.htm](spell-effects/nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|Efecto del Hechizo: Amansar|modificada|
|[npFFTAxN44WWrGnM.htm](spell-effects/npFFTAxN44WWrGnM.htm)|Spell Effect: Wash Your Luck|Efecto de Hechizo: Lava Tu Suerte|modificada|
|[NQZ88IoKeMBsfjp7.htm](spell-effects/NQZ88IoKeMBsfjp7.htm)|Spell Effect: Life Boost|Efecto de Hechizo: Aumento de Vida|modificada|
|[nU4SxAk6XreHUi5h.htm](spell-effects/nU4SxAk6XreHUi5h.htm)|Spell Effect: Infectious Enthusiasm|Efecto de Hechizo: Entusiasmo Infeccioso|modificada|
|[nUJSdm4fy6fcwsvv.htm](spell-effects/nUJSdm4fy6fcwsvv.htm)|Spell Effect: Lashunta's Life Bubble|Efecto de Hechizo: Burbuja de Vida de Lashunta|modificada|
|[nWEx5kpkE8YlBZvy.htm](spell-effects/nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Green)|Efecto de Hechizo: Forma de dragón (Verde)|modificada|
|[NXzo2kdgVixIZ2T1.htm](spell-effects/NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|Efecto de Hechizo: Apex Companion|modificada|
|[OeCn76SB92GPOZwr.htm](spell-effects/OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Brass)|Efecto de Hechizo: Forma de dragón (latón).|modificada|
|[oJbcmpBSHwmx6FD4.htm](spell-effects/oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|Efecto de Hechizo: Forma de dinosaurio (Deinonychus)|modificada|
|[otK6eG3b3FV7n2xP.htm](spell-effects/otK6eG3b3FV7n2xP.htm)|Spell Effect: Swampcall (Critical Failure)|Efecto de Hechizo: Swampcall (Fallo Crítico)|modificada|
|[OxJEUhim6xzsHIyi.htm](spell-effects/OxJEUhim6xzsHIyi.htm)|Spell Effect: Divine Vessel 9th level (Good)|Efecto de Hechizo: Recipiente divino 9º nivel (Bueno).|modificada|
|[p8F3MVUkGmpsUDOn.htm](spell-effects/p8F3MVUkGmpsUDOn.htm)|Spell Effect: Untwisting Iron Pillar|Efecto de Hechizo: Pilar de Hierro sin Torsión|modificada|
|[pcK88HqL6LjBNH2h.htm](spell-effects/pcK88HqL6LjBNH2h.htm)|Spell Effect: Faerie Dust (Critical Failure)|Efecto de Hechizo: Polvo feérico (fallo crítico).|modificada|
|[PDoTV4EhJp63FEaG.htm](spell-effects/PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|Efecto de Hechizo: Dibujar Ire (Éxito)|modificada|
|[Pfllo68qdQjC4Qv6.htm](spell-effects/Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|Efecto de Hechizo: Escudo Prismático|modificada|
|[PhBrHvBwvq8rni9C.htm](spell-effects/PhBrHvBwvq8rni9C.htm)|Spell Effect: Evolution Surge (Large)|Efecto de Hechizo: Oleaje de Evolución (Grande)|modificada|
|[phIoucsDa3iplMm2.htm](spell-effects/phIoucsDa3iplMm2.htm)|Spell Effect: Elemental Form (Fire)|Efecto de Hechizo: Forma elemental (Fuego)|modificada|
|[PkofF4bxkDUgeIoE.htm](spell-effects/PkofF4bxkDUgeIoE.htm)|Spell Effect: Touch of Corruption (Healing)|Efecto de Hechizo: Toque de Corrupción (Curar)|modificada|
|[PNEGSVYhMKf6kQZ6.htm](spell-effects/PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|Efecto del Hechizo: Llamada a las Armas|modificada|
|[pocsoEi84Mr2buOc.htm](spell-effects/pocsoEi84Mr2buOc.htm)|Spell Effect: Evolution Surge (Scent)|Efecto de Hechizo: Oleaje de Evolución|modificada|
|[PpkOZVoHkBZUmddx.htm](spell-effects/PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|Efecto de Hechizo: Forma de Légamo (Jalea Ocre)|modificada|
|[pPMldkAbPVOSOPIF.htm](spell-effects/pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|Efecto de Hechizo: Protección Acompaóante.|modificada|
|[ppVKJY6AYggn2Fma.htm](spell-effects/ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|Efecto de hechizo: Buenas bayas|modificada|
|[PQHP7Oph3BQX1GhF.htm](spell-effects/PQHP7Oph3BQX1GhF.htm)|Spell Effect: Longstrider|Efecto de Hechizo: Zancada prodigiosa|modificada|
|[ptOqsN5FS0nQh7RW.htm](spell-effects/ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|Efecto de Hechizo: Forma de animal (Gato)|modificada|
|[q4EEYltjqpRGiLsP.htm](spell-effects/q4EEYltjqpRGiLsP.htm)|Spell Effect: Elemental Motion (Fire)|Efecto de Hechizo: Movimiento elemental (Fuego).|modificada|
|[qbOpis7pIkXJbM2B.htm](spell-effects/qbOpis7pIkXJbM2B.htm)|Spell Effect: Elemental Motion (Earth)|Efecto de Hechizo: Movimiento elemental (Tierra).|modificada|
|[QF6RDlCoTvkVHRo4.htm](spell-effects/QF6RDlCoTvkVHRo4.htm)|Effect: Shield Immunity|Efecto: Inmunidad al escudo|modificada|
|[qhNUfwpkD8BRw4zj.htm](spell-effects/qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|Efecto de Hechizo: Esconderse Mágicamente|modificada|
|[QJRaVbulmpOzWi6w.htm](spell-effects/QJRaVbulmpOzWi6w.htm)|Spell Effect: Girzanje's March|Efecto de Hechizo: Marcha de Girzanje|modificada|
|[qkwb5DD3zmKwvbk0.htm](spell-effects/qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mage Armor|Efecto de Hechizo: Armadura de mago|modificada|
|[qlz0sJIvqc0FdUdr.htm](spell-effects/qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|Efecto de Hechizo: Oleaje de arma.|modificada|
|[qo7DoF11Xl9gqmFc.htm](spell-effects/qo7DoF11Xl9gqmFc.htm)|Spell Effect: Rapid Adaptation (Desert)|Efecto de Hechizo: Adaptación Rápida (Desierto)|modificada|
|[qPaEEhczUWCQo6ux.htm](spell-effects/qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|Efecto de Hechizo: Forma de animal (Tiburón)|modificada|
|[qQLHPbUFASKFky1W.htm](spell-effects/qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|Efecto de Hechizo: Hiperenfoque|modificada|
|[Qr5rgoZvI4KmFY0N.htm](spell-effects/Qr5rgoZvI4KmFY0N.htm)|Spell Effect: Calm Emotions|Efecto de Hechizo: Calmar emociones|modificada|
|[qRyynMOflGajgAR3.htm](spell-effects/qRyynMOflGajgAR3.htm)|Spell Effect: Mantle of the Magma Heart (Enlarging Eruption)|Efecto de Hechizo: Manto del Corazón de Magma (Erupción que agranda)|modificada|
|[R27azQfzeFuFc48G.htm](spell-effects/R27azQfzeFuFc48G.htm)|Spell Effect: Take Its Course (Affliction, Hinder)|Efecto de Hechizo: Sigue su curso (Aflicción, Entorpecer)|modificada|
|[R3j6song8sYLY5vG.htm](spell-effects/R3j6song8sYLY5vG.htm)|Spell Effect: Community Repair (Critical Success)|Efecto de Hechizo: Reparar Comunidad (Éxito Crítico)|modificada|
|[RawLEPwyT5CtCZ4D.htm](spell-effects/RawLEPwyT5CtCZ4D.htm)|Spell Effect: Protection|Efecto de Hechizo: Protección.|modificada|
|[rEsgDhunQ5Yx8KZx.htm](spell-effects/rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Purple Worm)|Efecto de Hechizo: Forma monstruosa (Gusano morado)|modificada|
|[rHXOZAFBdRXIlxt5.htm](spell-effects/rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Black)|Efecto de Hechizo: Forma de dragón (Negro)|modificada|
|[RLUhIyqH84Dle4vo.htm](spell-effects/RLUhIyqH84Dle4vo.htm)|Spell Effect: Fungal Infestation (Critical Failure)|Efecto de Hechizo: Infestación fúngica (Fallo Crítico).|modificada|
|[ROuGwXEvFznzGE9k.htm](spell-effects/ROuGwXEvFznzGE9k.htm)|Spell Effect: Swampcall (Failure)|Efecto de Hechizo: Swampcall (Fallo)|modificada|
|[rQaltMIEi2bn1Z4k.htm](spell-effects/rQaltMIEi2bn1Z4k.htm)|Spell Effect: Ki Form|Efecto de Hechizo: Forma Ki|modificada|
|[rTVZ0zwiKeslRw6p.htm](spell-effects/rTVZ0zwiKeslRw6p.htm)|Spell Effect: Wild Morph|Efecto de Hechizo: Morfismo salvaje|modificada|
|[s6CwkSsMDGfUmotn.htm](spell-effects/s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|Spell Effect: Custodia contra la muerte|modificada|
|[S75DOLjKaSJGMc0D.htm](spell-effects/S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|Efecto de Hechizo: Forma feérica (Elananx)|modificada|
|[sccNh8j1PKLHCKh1.htm](spell-effects/sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|Efecto de Hechizo: Forma Angelical (Coral)|modificada|
|[ScF0ECWnfXMHYLDL.htm](spell-effects/ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|Efecto de Hechizo: Forma daimónico (Leukodaemon)|modificada|
|[sDN9b4bjCGH2nQnG.htm](spell-effects/sDN9b4bjCGH2nQnG.htm)|Spell Effect: Rapid Adaptation (Mountain)|Efecto de Hechizo: Adaptación Rápida (Montaña)|modificada|
|[sE2txm68yZSFMV3v.htm](spell-effects/sE2txm68yZSFMV3v.htm)|Spell Effect: Sweet Dream (Voyaging)|Efecto de Hechizo: Dulce Sue (Viajar)|modificada|
|[sfJyQKmoxSRo6FyP.htm](spell-effects/sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|Efecto de Hechizo: Forma aberrante (Gug)|modificada|
|[sILRkGTwoBywy0BU.htm](spell-effects/sILRkGTwoBywy0BU.htm)|Spell Effect: Gaseous Form|Spell Effect: Forma gaseosa|modificada|
|[SjfDoeymtnYKoGUD.htm](spell-effects/SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|Efecto de Hechizo: Forma aberrante (Otyugh)|modificada|
|[slI9P4jUp3ERPCqX.htm](spell-effects/slI9P4jUp3ERPCqX.htm)|Spell Effect: Impeccable Flow|Efecto de Hechizo: Flujo Impecable|modificada|
|[sN3mQ7YrPBogEJRn.htm](spell-effects/sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|Efecto de Hechizo: Forma de animal (Canino)|modificada|
|[sPCWrhUHqlbGhYSD.htm](spell-effects/sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|Efecto de Hechizo: Aumentar|modificada|
|[sXe7cPazOJbX41GU.htm](spell-effects/sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|Efecto de Hechizo: Forma demoníaca (Hezrou)|modificada|
|[T3t9776ataHzrmTs.htm](spell-effects/T3t9776ataHzrmTs.htm)|Spell Effect: Inside Ropes (3rd level)|Efecto de Hechizo: Cuerdas Interiores (3er nivel)|modificada|
|[T5bk6UH7yuYog1Fp.htm](spell-effects/T5bk6UH7yuYog1Fp.htm)|Spell Effect: See Invisibility|Efecto de Hechizo: Ver lo invisible.|modificada|
|[T6XnxvsgvvOrpien.htm](spell-effects/T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|Efecto de Hechizo: Forma de dinosaurio (Estegosaurio)|modificada|
|[TAAWbJgfESltn2we.htm](spell-effects/TAAWbJgfESltn2we.htm)|Spell Effect: Primal Summons (Water)|Efecto de Hechizo: Convocación primigenia (Agua).|modificada|
|[tBgwWblDp1xdxN4D.htm](spell-effects/tBgwWblDp1xdxN4D.htm)|Spell Effect: Divine Vessel (Lawful)|Efecto de Hechizo: Recepción divino (Legal)|modificada|
|[tfdDpf9xSWgQer5g.htm](spell-effects/tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|Efecto Hechizo: Forma Cósmica (Luna)|modificada|
|[ThFug45WHkQQXcoF.htm](spell-effects/ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|Efecto de Hechizo: Pies ligeros|modificada|
|[tjC6JeZgLDPIMHjG.htm](spell-effects/tjC6JeZgLDPIMHjG.htm)|Spell Effect: Malignant Sustenance|Efecto de Hechizo: Sustento maligno|modificada|
|[TjGHxli0edXI6rAg.htm](spell-effects/TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|Efecto de Hechizo: Schadenfreude (Éxito)|modificada|
|[tk3go5Cl6Qt130Dk.htm](spell-effects/tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|Efecto de Hechizo: Forma de animal (Mono)|modificada|
|[tNjimcyUwn8afeH6.htm](spell-effects/tNjimcyUwn8afeH6.htm)|Spell Effect: Gravity Weapon|Efecto Hechizo: Arma de Gravedad|modificada|
|[TpVkVALUBrBQjULn.htm](spell-effects/TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|Efecto de Hechizo: Avivar el Corazón|modificada|
|[tu8FyCtmL3YYR2jL.htm](spell-effects/tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|Efecto de Hechizo: Forma de planta (arboral (pl. -es))|modificada|
|[TUyEeLyqdJL6PwbH.htm](spell-effects/TUyEeLyqdJL6PwbH.htm)|Spell Effect: Dragon Form (Silver)|Efecto de Hechizo: Forma de dragón (Plata)|modificada|
|[TwtUIEyenrtAbeiX.htm](spell-effects/TwtUIEyenrtAbeiX.htm)|Spell Effect: Tanglefoot|Efecto de Hechizo: Mara|modificada|
|[u0AznkjTZAVnCMNp.htm](spell-effects/u0AznkjTZAVnCMNp.htm)|Spell Effect: Evolution Surge (Fly)|Efecto de Hechizo: Oleaje de Evolución (Volar)|modificada|
|[U2eD42cGwdvQMdN0.htm](spell-effects/U2eD42cGwdvQMdN0.htm)|Spell Effect: Fiery Body (9th Level)|Efecto de Hechizo: Cuerpo flamígero (9º nivel).|modificada|
|[uDOxq24S7IT2EcXv.htm](spell-effects/uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|Efecto de Hechizo: Memoria de Objeto (Arma)|modificada|
|[UH2sT6eW5e31Xytd.htm](spell-effects/UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|Efecto de Hechizo: Dutiful Challenge|modificada|
|[uIMaMzd6pcKmMNPJ.htm](spell-effects/uIMaMzd6pcKmMNPJ.htm)|Spell Effect: Fiery Body|Efecto de Hechizo: Cuerpo flamígero|modificada|
|[Uj9VFXoVMH0mTTdt.htm](spell-effects/Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|Efecto de Hechizo: Organsight|modificada|
|[UjoNm3lrhlg4ctAQ.htm](spell-effects/UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|Efecto de Hechizo: Forma aérea (Pterosaurio)|modificada|
|[Um25D1qLtZWOSBny.htm](spell-effects/Um25D1qLtZWOSBny.htm)|Spell Effect: Shifting Form (Swim Speed)|Efecto de Hechizo: Alterar forma (Velocidad Nadar)|modificada|
|[UtIOWubq7akdHMOh.htm](spell-effects/UtIOWubq7akdHMOh.htm)|Spell Effect: Augment Summoning|Efecto de Hechizo: Aumentar convocación.|modificada|
|[UTLp7omqsiC36bso.htm](spell-effects/UTLp7omqsiC36bso.htm)|Spell Effect: Bane|Efecto del hechizo: Perdiciónón|modificada|
|[uUlFic5lnr2FpNiG.htm](spell-effects/uUlFic5lnr2FpNiG.htm)|Spell Effect: Lashunta's Life Bubble (Heightened)|Efecto de Hechizo: Burbuja de Vida de Lashunta (Aumentada)|modificada|
|[UVrEe0nukiSmiwfF.htm](spell-effects/UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|Efecto de Hechizo: Reforzar Eidolon|modificada|
|[UXdt1WVA66oZOoZS.htm](spell-effects/UXdt1WVA66oZOoZS.htm)|Spell Effect: Flame Barrier|Efecto de Hechizo: Barrera de llamas.|modificada|
|[v051JKN0Dj3ve5cF.htm](spell-effects/v051JKN0Dj3ve5cF.htm)|Spell Effect: Sweet Dream (Insight)|Efecto de Hechizo: Dulce Sue (Insight)|modificada|
|[v09uwq1eHEAy2bgh.htm](spell-effects/v09uwq1eHEAy2bgh.htm)|Spell Effect: Unbreaking Wave Barrier|Efecto de Hechizo: Barrera de Onda Irrompible|modificada|
|[V4a9pZHNUlddAwTA.htm](spell-effects/V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Red)|Efecto de Hechizo: Forma de dragón (Rojo).|modificada|
|[V7jAnItnVqtfCAKt.htm](spell-effects/V7jAnItnVqtfCAKt.htm)|Spell Effect: Dragon Wings (60 Feet)|Efecto de Hechizo: Alas de dragón (60 pies).|modificada|
|[VFereWC1agrwgzPL.htm](spell-effects/VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|Efecto del hechizo: Inspirar heroísmo (Valor, +3).|modificada|
|[ViBlOrd6hno3DiPP.htm](spell-effects/ViBlOrd6hno3DiPP.htm)|Spell Effect: Stumbling Curse|Efecto de Hechizo: Maldición de Tropiezo|modificada|
|[W0PjCMyGOpKAuyKX.htm](spell-effects/W0PjCMyGOpKAuyKX.htm)|Spell Effect: Weapon Surge (Major Striking)|Efecto de Hechizo: Oleaje de arma (Golpe superior).|modificada|
|[w1HwO7huxJoK0gHY.htm](spell-effects/w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|Efecto de Hechizo: Elemento Encarnado (Agua)|modificada|
|[W4lb3417rNDd9tCq.htm](spell-effects/W4lb3417rNDd9tCq.htm)|Spell Effect: Righteous Might|Efecto de Hechizo: Poder Justiciero|modificada|
|[w9HpSwBLByNyRXvi.htm](spell-effects/w9HpSwBLByNyRXvi.htm)|Spell Effect: Blessing of Defiance (2 Action)|Efecto de hechizo: bendición de desafío (2 acciones).|modificada|
|[WEpgIGFwtRb3ef1x.htm](spell-effects/WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|Efecto de Hechizo: Forma de Angel (Balisse)|modificada|
|[wKdIf5x7zztiFHpL.htm](spell-effects/wKdIf5x7zztiFHpL.htm)|Spell Effect: Divine Vessel 9th level (Lawful)|Efecto de Hechizo: Recepción divino de 9º nivel (Legal).|modificada|
|[wqh8D9kHGItZBvtQ.htm](spell-effects/wqh8D9kHGItZBvtQ.htm)|Spell Effect: Mantle of the Frozen Heart (Icy Claws)|Efecto de Hechizo: Manto del corazón helado (Garras heladas).|modificada|
|[WWtSEJGwKY4bQpUn.htm](spell-effects/WWtSEJGwKY4bQpUn.htm)|Spell Effect: Vital Beacon|Efecto de Hechizo: Faro vital|modificada|
|[X1kkbRrh4zJuDGjl.htm](spell-effects/X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|Efecto de Hechizo: Forma demoníaca (Babau)|modificada|
|[X7RD0JRxhJV9u2LC.htm](spell-effects/X7RD0JRxhJV9u2LC.htm)|Spell Effect: Disrupting Weapons|Efecto de Hechizo: Disruptora de armas|modificada|
|[xgZxYqjDPNtsQ3Qp.htm](spell-effects/xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|Efecto de Hechizo: Forma aérea (Avispa)|modificada|
|[xKJVqN1ETnNH3tFg.htm](spell-effects/xKJVqN1ETnNH3tFg.htm)|Spell Effect: Corrosive Body (Heightened 9th)|Efecto de Hechizo: Cuerpo Corrosivo (Potenciado 9º)|modificada|
|[Xl48OsJ47oDVZAVQ.htm](spell-effects/Xl48OsJ47oDVZAVQ.htm)|Spell Effect: Primal Summons (Earth)|Efecto de Hechizo: Convocación primigenia (Tierra).|modificada|
|[Xlwt1wpjEKWBLUjK.htm](spell-effects/Xlwt1wpjEKWBLUjK.htm)|Spell Effect: Animal Feature (Fish Tail)|Efecto de Hechizo: Característica Animal (Cola de Pez)|modificada|
|[XMBoKRRyooKnGkHk.htm](spell-effects/XMBoKRRyooKnGkHk.htm)|Spell Effect: Practice Makes Perfect|Efecto de Hechizo: La práctica hace al maestro|modificada|
|[XMOWgJV8UXWmpgk0.htm](spell-effects/XMOWgJV8UXWmpgk0.htm)|Spell Effect: Evolution Surge (Speed)|Efecto de Hechizo: Oleaje de evolución (Velocidad)|modificada|
|[xPVOvWNJORvm8EwP.htm](spell-effects/xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|Efecto de Hechizo: Imitar muerto viviente|modificada|
|[xsy1yaCj0SVsn502.htm](spell-effects/xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|Efecto de Hechizo: Forma aberrante (Chuul)|modificada|
|[XT3AyRfx4xeXfAjP.htm](spell-effects/XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|Efecto de Hechizo: Mejora física.|modificada|
|[XTgxkQkhlap66e54.htm](spell-effects/XTgxkQkhlap66e54.htm)|Spell Effect: Iron Gut (3rd Level)|Efecto de hechizo: Tripa de hierro (3er nivel)|modificada|
|[y9PJdDYFemhk6Z5o.htm](spell-effects/y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|Efecto de Hechizo: Pies ágiles|modificada|
|[YCno88Te0nfwFgVo.htm](spell-effects/YCno88Te0nfwFgVo.htm)|Spell Effect: Mantle of the Frozen Heart (Ice Glide)|Efecto de Hechizo: Manto del Corazón Helado (Deslizamiento de Hielo)|modificada|
|[ydsLEGjY89Akc4oZ.htm](spell-effects/ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|Efecto de Hechizo: Forma de peste|modificada|
|[yke7fAUDxUNzouQc.htm](spell-effects/yke7fAUDxUNzouQc.htm)|Spell Effect: Elemental Gift (Air)|Efecto de Hechizo: Don Elemental (Aire)|modificada|
|[yq6iu0Qxg3YEbb6s.htm](spell-effects/yq6iu0Qxg3YEbb6s.htm)|Spell Effect: Elemental Gift (Water)|Efecto de Hechizo: Don Elemental (Agua)|modificada|
|[zdpTPf157rXl3tEJ.htm](spell-effects/zdpTPf157rXl3tEJ.htm)|Spell Effect: Clawsong (Deadly D8)|Efecto de Hechizo: Clawsong (Letal D8)|modificada|
|[ZHVtJKnur9PAF5TO.htm](spell-effects/ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|Efecto de Hechizo: Poder duradero|modificada|
|[zIRnnuj4lARq43DA.htm](spell-effects/zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|Efecto de Hechizo: Forma daimónico (Meladaemon)|modificada|
|[zjFN1cJEl3AMKiVs.htm](spell-effects/zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|Efecto de Hechizo: Nymph's Token|modificada|
|[ZlsuhS9J0S3PuvCO.htm](spell-effects/ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Blink|Efecto del hechizo: intermitencia|modificada|
|[zPGVOLz6xhsQN35C.htm](spell-effects/zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|Efecto de Hechizo: Envenom Companion|modificada|
|[zpxIwEjnLUSO1B4z.htm](spell-effects/zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|Efecto de Hechizo: Recepáculo de la magia.|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[6embuvXCpS3YOD5u.htm](spell-effects/6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|Efecto de Conjuro: Toque Resistente|oficial|
|[alyNtkHLNnt98Ewz.htm](spell-effects/alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|Efecto de conjuro: Toque Acelerador|oficial|
|[MjtPtndJx31q2N9R.htm](spell-effects/MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|Efecto de conjuro: Toque Amplificador|oficial|
|[oaRt210JV4GZIHmJ.htm](spell-effects/oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|Efecto de conjuro: Toque revitalizador|oficial|
