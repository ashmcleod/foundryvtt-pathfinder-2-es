# Estado de la traducción (bestiary-ability-glossary-srd)

 * **modificada**: 49


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0B39GdScyZMPWalX.htm](bestiary-ability-glossary-srd/0B39GdScyZMPWalX.htm)|Power Attack|Ataque poderoso|modificada|
|[2YRDYVnC1eljaXKK.htm](bestiary-ability-glossary-srd/2YRDYVnC1eljaXKK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[3JOi2cMcGhT3eze1.htm](bestiary-ability-glossary-srd/3JOi2cMcGhT3eze1.htm)|Rend|Rasgadura|modificada|
|[4Ho2xMPEC05aSxzr.htm](bestiary-ability-glossary-srd/4Ho2xMPEC05aSxzr.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[52CdldlWMiVTZk1F.htm](bestiary-ability-glossary-srd/52CdldlWMiVTZk1F.htm)|Coven|Coven|modificada|
|[6l7e7CHQLESlI57U.htm](bestiary-ability-glossary-srd/6l7e7CHQLESlI57U.htm)|Improved Push|Empuje mejorado|modificada|
|[9qV49KjZujZnSp6w.htm](bestiary-ability-glossary-srd/9qV49KjZujZnSp6w.htm)|All-Around Vision|All-Around Vision|modificada|
|[a0tx6exmB9v9CUsj.htm](bestiary-ability-glossary-srd/a0tx6exmB9v9CUsj.htm)|Throw Rock|Arrojar roca|modificada|
|[AWvNPE4U0kEJSL1T.htm](bestiary-ability-glossary-srd/AWvNPE4U0kEJSL1T.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[baA0nSMhQyFyJIia.htm](bestiary-ability-glossary-srd/baA0nSMhQyFyJIia.htm)|Swarm Mind|Swarm Mind|modificada|
|[BCLvAx4Pz4MLa2pu.htm](bestiary-ability-glossary-srd/BCLvAx4Pz4MLa2pu.htm)|Knockdown|Derribo|modificada|
|[eQM5hQ1W3d1uen97.htm](bestiary-ability-glossary-srd/eQM5hQ1W3d1uen97.htm)|Change Shape|Change Shape|modificada|
|[etMnv73EIdEZrYYu.htm](bestiary-ability-glossary-srd/etMnv73EIdEZrYYu.htm)|Frightful Presence|Frightful Presence|modificada|
|[fFu2sZz4KB01fVRc.htm](bestiary-ability-glossary-srd/fFu2sZz4KB01fVRc.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[fJSNOw4zHGbIm4bZ.htm](bestiary-ability-glossary-srd/fJSNOw4zHGbIm4bZ.htm)|Fast Healing|Curación rápida|modificada|
|[g26YiEIfSHCpLocV.htm](bestiary-ability-glossary-srd/g26YiEIfSHCpLocV.htm)|Constrict|Restringir|modificada|
|[HBrBrUzjfvj2gDXB.htm](bestiary-ability-glossary-srd/HBrBrUzjfvj2gDXB.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[hFtNbo1LKYCoDy2O.htm](bestiary-ability-glossary-srd/hFtNbo1LKYCoDy2O.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[I0HYG0ctCLP5JRsW.htm](bestiary-ability-glossary-srd/I0HYG0ctCLP5JRsW.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[i18TlebMzwONyPhI.htm](bestiary-ability-glossary-srd/i18TlebMzwONyPhI.htm)|Improved Grab|Agarrado mejorado|modificada|
|[IQtb58p4EaeUzTN1.htm](bestiary-ability-glossary-srd/IQtb58p4EaeUzTN1.htm)|Retributive Strike|Golpe retributivo|modificada|
|[j2wsK6IsW5yMW1jW.htm](bestiary-ability-glossary-srd/j2wsK6IsW5yMW1jW.htm)|Tremorsense|Tremorsense|modificada|
|[JcaIJLGGmst79f1Y.htm](bestiary-ability-glossary-srd/JcaIJLGGmst79f1Y.htm)|Thoughtsense|Percibir pensamientos|modificada|
|[kakyXBG5WA8c6Zfd.htm](bestiary-ability-glossary-srd/kakyXBG5WA8c6Zfd.htm)|Constant Spells|Constant Spells|modificada|
|[kdhbPaBMK1d1fpbA.htm](bestiary-ability-glossary-srd/kdhbPaBMK1d1fpbA.htm)|Telepathy|Telepatía|modificada|
|[kFv54DisTfCMTBEY.htm](bestiary-ability-glossary-srd/kFv54DisTfCMTBEY.htm)|Poison|Poison|modificada|
|[kquBnQ0kObZztnBc.htm](bestiary-ability-glossary-srd/kquBnQ0kObZztnBc.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[LbnsT8yXchHMQT3t.htm](bestiary-ability-glossary-srd/LbnsT8yXchHMQT3t.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[lR9R5Vld8Eu2Dha5.htm](bestiary-ability-glossary-srd/lR9R5Vld8Eu2Dha5.htm)|Regeneration|Regeneración|modificada|
|[m4HQ2o5aPxjXf2kY.htm](bestiary-ability-glossary-srd/m4HQ2o5aPxjXf2kY.htm)|Shield Block|Bloquear con escudo|modificada|
|[N1kstYbHScxgUQtN.htm](bestiary-ability-glossary-srd/N1kstYbHScxgUQtN.htm)|Ferocity|Ferocidad|modificada|
|[nZMQh4AaBr291TUf.htm](bestiary-ability-glossary-srd/nZMQh4AaBr291TUf.htm)|Buck|Encabritarse|modificada|
|[OmV6E3aELvq9CkK1.htm](bestiary-ability-glossary-srd/OmV6E3aELvq9CkK1.htm)|Greater Constrict|Mayor Restricción|modificada|
|[OvqohW9YuahnFaiX.htm](bestiary-ability-glossary-srd/OvqohW9YuahnFaiX.htm)|Form Up|Form Up|modificada|
|[qCCLZhnp2HhP3Ex6.htm](bestiary-ability-glossary-srd/qCCLZhnp2HhP3Ex6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RJbI07QSiYp0SF9A.htm](bestiary-ability-glossary-srd/RJbI07QSiYp0SF9A.htm)|Troop Defenses|Troop Defenses|modificada|
|[rqfnQ5VHT5hxm25r.htm](bestiary-ability-glossary-srd/rqfnQ5VHT5hxm25r.htm)|Scent|Scent|modificada|
|[sebk9XseMCRkDqRg.htm](bestiary-ability-glossary-srd/sebk9XseMCRkDqRg.htm)|Lifesense|Lifesense|modificada|
|[t6cx9FOODmeZQNYl.htm](bestiary-ability-glossary-srd/t6cx9FOODmeZQNYl.htm)|Push|Push|modificada|
|[Tkd8sH4pwFIPzqTr.htm](bestiary-ability-glossary-srd/Tkd8sH4pwFIPzqTr.htm)|Grab|Agarrado|modificada|
|[TTCw5NusiSSkJU1x.htm](bestiary-ability-glossary-srd/TTCw5NusiSSkJU1x.htm)|Negative Healing|Curación negativa|modificada|
|[uG0Z8PsyZtsYuvGR.htm](bestiary-ability-glossary-srd/uG0Z8PsyZtsYuvGR.htm)|Catch Rock|Atrapar roca|modificada|
|[uJSseLa57HZYSMUu.htm](bestiary-ability-glossary-srd/uJSseLa57HZYSMUu.htm)|Swallow Whole|Engullir Todo|modificada|
|[UNah0bxXxkcZjxO3.htm](bestiary-ability-glossary-srd/UNah0bxXxkcZjxO3.htm)|Trample|Trample|modificada|
|[v61oEQaDdcRpaZ9X.htm](bestiary-ability-glossary-srd/v61oEQaDdcRpaZ9X.htm)|Aura|Aura|modificada|
|[VdSMQ6yRZ3YXNXHL.htm](bestiary-ability-glossary-srd/VdSMQ6yRZ3YXNXHL.htm)|Wavesense|Wavesense|modificada|
|[wCnsRCHvtZkZTmO0.htm](bestiary-ability-glossary-srd/wCnsRCHvtZkZTmO0.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[Z025dWgZtawbuRLI.htm](bestiary-ability-glossary-srd/Z025dWgZtawbuRLI.htm)|Disease|Enfermedad|modificada|
|[zU3Ovaet4xQ5Gmvy.htm](bestiary-ability-glossary-srd/zU3Ovaet4xQ5Gmvy.htm)|Engulf|Envolver|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
