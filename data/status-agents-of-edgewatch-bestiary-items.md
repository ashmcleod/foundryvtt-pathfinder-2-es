# Estado de la traducción (agents-of-edgewatch-bestiary-items)

 * **modificada**: 1193
 * **ninguna**: 280
 * **vacía**: 71


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0bwOzjr3mbDPuipM.htm](agents-of-edgewatch-bestiary-items/0bwOzjr3mbDPuipM.htm)|+1 Resilient Studded Leather Armor|
|[0drWrweDSPvYFB5H.htm](agents-of-edgewatch-bestiary-items/0drWrweDSPvYFB5H.htm)|Grease (At Will)|
|[0k8i82stB4T6FXRO.htm](agents-of-edgewatch-bestiary-items/0k8i82stB4T6FXRO.htm)|Teleport (At Will) (Self Only) (Only within Terimor's Tower)|
|[0PH5hCnJKSwOQaXU.htm](agents-of-edgewatch-bestiary-items/0PH5hCnJKSwOQaXU.htm)|Plane Shift (At Will)|
|[0T6nq4hOPXD6M3WD.htm](agents-of-edgewatch-bestiary-items/0T6nq4hOPXD6M3WD.htm)|Geas (Ritual)|
|[0tydSelE4MWNlyip.htm](agents-of-edgewatch-bestiary-items/0tydSelE4MWNlyip.htm)|Project Image (At Will) (See Project False Image)|
|[0XaZ3Z7sUkm2igan.htm](agents-of-edgewatch-bestiary-items/0XaZ3Z7sUkm2igan.htm)|Plane Shift (To Material or Shadow)|
|[10AgsfzzM00h8cy6.htm](agents-of-edgewatch-bestiary-items/10AgsfzzM00h8cy6.htm)|+1 Studded Leather Armor|
|[16pPUkcG7WYEAXq0.htm](agents-of-edgewatch-bestiary-items/16pPUkcG7WYEAXq0.htm)|+1 Striking Rapier|+1,striking|
|[1GUcU2gihTGiShZN.htm](agents-of-edgewatch-bestiary-items/1GUcU2gihTGiShZN.htm)|Spellbook|
|[1jGFJIbrEUb7ObhO.htm](agents-of-edgewatch-bestiary-items/1jGFJIbrEUb7ObhO.htm)|No MAP|
|[1PFeWPAaM2Q1d2xI.htm](agents-of-edgewatch-bestiary-items/1PFeWPAaM2Q1d2xI.htm)|+1 Leather Armor|
|[1wK9nN0xbT78u94L.htm](agents-of-edgewatch-bestiary-items/1wK9nN0xbT78u94L.htm)|+1 Rapier|
|[1zoRmOnSV1QGpitd.htm](agents-of-edgewatch-bestiary-items/1zoRmOnSV1QGpitd.htm)|Formula Book|
|[2A9TNsqY3jsY4TNh.htm](agents-of-edgewatch-bestiary-items/2A9TNsqY3jsY4TNh.htm)|Blade of the Rabbit Prince|+2,greaterStriking,dancing|
|[2RZMlb4O32zwGdui.htm](agents-of-edgewatch-bestiary-items/2RZMlb4O32zwGdui.htm)|+3 Greater Striking speed Handwraps of Mighty Blows|+3,greaterStriking,speed|
|[2tusZaB9hZtQXlNQ.htm](agents-of-edgewatch-bestiary-items/2tusZaB9hZtQXlNQ.htm)|+3 Glamered Major Resilient Chain Shirt|
|[2uY1I5T072VajQhI.htm](agents-of-edgewatch-bestiary-items/2uY1I5T072VajQhI.htm)|+1 Resilient Chain Shirt|
|[2xpGs13m2vIfqQVv.htm](agents-of-edgewatch-bestiary-items/2xpGs13m2vIfqQVv.htm)|Rapier|+3,majorStriking,dancing|
|[3AnmcIzCnPYcis7x.htm](agents-of-edgewatch-bestiary-items/3AnmcIzCnPYcis7x.htm)|Discern Lies (At Will)|
|[3lBVtsmd41MtSQbc.htm](agents-of-edgewatch-bestiary-items/3lBVtsmd41MtSQbc.htm)|Creation (At Will)|
|[3McCXRG5PfuW5INo.htm](agents-of-edgewatch-bestiary-items/3McCXRG5PfuW5INo.htm)|Dimension Door (At Will)|
|[3pri8PxDjOXdPKhA.htm](agents-of-edgewatch-bestiary-items/3pri8PxDjOXdPKhA.htm)|Dagger|+3,majorStriking,speed,wounding|
|[3XpnxOq9HQF7125Y.htm](agents-of-edgewatch-bestiary-items/3XpnxOq9HQF7125Y.htm)|Religious Symbol (Norgorber)|
|[4C7CFkhUaTzr7SKU.htm](agents-of-edgewatch-bestiary-items/4C7CFkhUaTzr7SKU.htm)|Dispel Magic (At Will)|
|[4CnFUwgw4jLxhJv8.htm](agents-of-edgewatch-bestiary-items/4CnFUwgw4jLxhJv8.htm)|Ventriloquism (At Will)|
|[4JayvL1rUcZUDXLI.htm](agents-of-edgewatch-bestiary-items/4JayvL1rUcZUDXLI.htm)|Tongues (Constant)|
|[4N0fkv95qWG2IZho.htm](agents-of-edgewatch-bestiary-items/4N0fkv95qWG2IZho.htm)|See Invisibility (Constant)|
|[4WUOiPGMPvWHJiET.htm](agents-of-edgewatch-bestiary-items/4WUOiPGMPvWHJiET.htm)|Dagger|+1,vorpal|
|[4YUNqNUzackL1Umo.htm](agents-of-edgewatch-bestiary-items/4YUNqNUzackL1Umo.htm)|Dagger|+2,striking|
|[59NYvJMmj1tMm5lr.htm](agents-of-edgewatch-bestiary-items/59NYvJMmj1tMm5lr.htm)|+1 Chain Shirt|
|[5M80BJ9pKuzYQcdJ.htm](agents-of-edgewatch-bestiary-items/5M80BJ9pKuzYQcdJ.htm)|Divine Wrath (Chaotic Only)|
|[5psrOH2hlyhUw5o3.htm](agents-of-edgewatch-bestiary-items/5psrOH2hlyhUw5o3.htm)|Dagger|+1,striking|
|[63422RZgKHyF8wVz.htm](agents-of-edgewatch-bestiary-items/63422RZgKHyF8wVz.htm)|+2 Resilient Studded Leather Armor|
|[67Ri2rSkdGl0BjqE.htm](agents-of-edgewatch-bestiary-items/67Ri2rSkdGl0BjqE.htm)|+1 Striking Shortsword|+1,striking|
|[6HFLIEIDtd4x3Xku.htm](agents-of-edgewatch-bestiary-items/6HFLIEIDtd4x3Xku.htm)|Explosive Bolt|
|[6s9SsMwkQ6GffJq2.htm](agents-of-edgewatch-bestiary-items/6s9SsMwkQ6GffJq2.htm)|Tongues (Constant)|
|[6sLcZ7H9ziNPmic1.htm](agents-of-edgewatch-bestiary-items/6sLcZ7H9ziNPmic1.htm)|+3 Wounding Greater Striking Cleaver (Large)|+3,greaterStriking,wounding|
|[6WVHNyvW3D5qCjFd.htm](agents-of-edgewatch-bestiary-items/6WVHNyvW3D5qCjFd.htm)|Divine Wrath (Chaotic Only)|
|[7HvTKcBHlvslLf1z.htm](agents-of-edgewatch-bestiary-items/7HvTKcBHlvslLf1z.htm)|Religious Symbol of Norgorber|
|[7lJgK8oZf1BP5nSa.htm](agents-of-edgewatch-bestiary-items/7lJgK8oZf1BP5nSa.htm)|Air Walk (Constant)|
|[7PZ30IOoip4691vx.htm](agents-of-edgewatch-bestiary-items/7PZ30IOoip4691vx.htm)|Spiked Chain|+3,greaterStriking|
|[7ZNJd077dCQukzxw.htm](agents-of-edgewatch-bestiary-items/7ZNJd077dCQukzxw.htm)|+1 Striking Composite Shortbow|
|[8dOcRUbCXfwb7Scp.htm](agents-of-edgewatch-bestiary-items/8dOcRUbCXfwb7Scp.htm)|Hardened Harrow Deck|+2,greaterStriking|
|[8mxc9kYnnRowCRTO.htm](agents-of-edgewatch-bestiary-items/8mxc9kYnnRowCRTO.htm)|+2 Greater Striking Longsword|+2,greaterStriking|
|[8rZciZP1Mbjm3tYQ.htm](agents-of-edgewatch-bestiary-items/8rZciZP1Mbjm3tYQ.htm)|Cloudkill (At Will)|
|[9AyozNIqEfsgGSRR.htm](agents-of-edgewatch-bestiary-items/9AyozNIqEfsgGSRR.htm)|Punishing Winds (Coven)|
|[9dXPyOM0qtYzbkJK.htm](agents-of-edgewatch-bestiary-items/9dXPyOM0qtYzbkJK.htm)|Magic Missile (At Will)|
|[9gFnxK8QlnLCkXJE.htm](agents-of-edgewatch-bestiary-items/9gFnxK8QlnLCkXJE.htm)|+1 Studded Leather Armor|
|[9oHiz2CZQ9FImqOK.htm](agents-of-edgewatch-bestiary-items/9oHiz2CZQ9FImqOK.htm)|Standard Skeleton Key|
|[9pE6wI4xNseKFpEP.htm](agents-of-edgewatch-bestiary-items/9pE6wI4xNseKFpEP.htm)|+1 Striking Shovel|+1,striking|
|[9RzqXxzPYa7KcoJg.htm](agents-of-edgewatch-bestiary-items/9RzqXxzPYa7KcoJg.htm)|True Seeing (Constant)|
|[9TY6byP3620OyD3n.htm](agents-of-edgewatch-bestiary-items/9TY6byP3620OyD3n.htm)|+2 Resilient Hellknight Plate|
|[9ZQhqWZ47kC3uARx.htm](agents-of-edgewatch-bestiary-items/9ZQhqWZ47kC3uARx.htm)|Dispel Magic (At Will)|
|[a6favCLO6Tji38BC.htm](agents-of-edgewatch-bestiary-items/a6favCLO6Tji38BC.htm)|Jade Bauble (affixed to Main-Gauche)|
|[Abe9y5VYOqym9Xnp.htm](agents-of-edgewatch-bestiary-items/Abe9y5VYOqym9Xnp.htm)|Darkvision Elixir (Lesser)|
|[AeD1CnnuOvuIxl0M.htm](agents-of-edgewatch-bestiary-items/AeD1CnnuOvuIxl0M.htm)|Spellstrike Ammunition (Type III) (Blindness) (DC 24)|
|[AEvfMGGIxfjL0K6w.htm](agents-of-edgewatch-bestiary-items/AEvfMGGIxfjL0K6w.htm)|Maul|+1|
|[AhneRV8i356zRma8.htm](agents-of-edgewatch-bestiary-items/AhneRV8i356zRma8.htm)|+1 Morningstar|+1|
|[aHSHgpoD2O4PDuNo.htm](agents-of-edgewatch-bestiary-items/aHSHgpoD2O4PDuNo.htm)|Mindlink (At Will)|
|[aLH0uLhIjacpKBNC.htm](agents-of-edgewatch-bestiary-items/aLH0uLhIjacpKBNC.htm)|Sneak Attack|
|[AqhrMnFrY3pwzrRG.htm](agents-of-edgewatch-bestiary-items/AqhrMnFrY3pwzrRG.htm)|Plane Shift (Coven)|
|[Arkf00iuzt1k76zG.htm](agents-of-edgewatch-bestiary-items/Arkf00iuzt1k76zG.htm)|Dispel Magic (At Will)|
|[aSpBwekiCyfTg7kk.htm](agents-of-edgewatch-bestiary-items/aSpBwekiCyfTg7kk.htm)|Ray of Enfeeblement (at will)|
|[aY23eLtS1QF1tT2T.htm](agents-of-edgewatch-bestiary-items/aY23eLtS1QF1tT2T.htm)|Bolts (Poisoned with Giant Scorpion Venom)|
|[AYw9g2rE8HcHXT4w.htm](agents-of-edgewatch-bestiary-items/AYw9g2rE8HcHXT4w.htm)|Detect Alignment (At Will) (Evil Only)|
|[AzdiNOa5Z1NBbH8L.htm](agents-of-edgewatch-bestiary-items/AzdiNOa5Z1NBbH8L.htm)|Nightmare (At Will)|
|[B0b4PRwpQ2Gi88F1.htm](agents-of-edgewatch-bestiary-items/B0b4PRwpQ2Gi88F1.htm)|Speak with Animals (Constant)|
|[b34amLfOE4xwYCRE.htm](agents-of-edgewatch-bestiary-items/b34amLfOE4xwYCRE.htm)|Leather Armor|
|[b7d4FMN2Fj0m71FK.htm](agents-of-edgewatch-bestiary-items/b7d4FMN2Fj0m71FK.htm)|Religious Symbol (norgorber)|
|[BM5ZghYk9LHvtj01.htm](agents-of-edgewatch-bestiary-items/BM5ZghYk9LHvtj01.htm)|True Seeing (Constant)|
|[bWLWCT07ZGCjCWXZ.htm](agents-of-edgewatch-bestiary-items/bWLWCT07ZGCjCWXZ.htm)|Baleful Polymorph (At Will)|
|[BwXqyvY8ZxaJq5Iw.htm](agents-of-edgewatch-bestiary-items/BwXqyvY8ZxaJq5Iw.htm)|Sneak Attack|
|[CnynVi7OZhHOIToa.htm](agents-of-edgewatch-bestiary-items/CnynVi7OZhHOIToa.htm)|+1 Striking Crossbow|+1,striking|
|[D0yr87wChBTvU7sm.htm](agents-of-edgewatch-bestiary-items/D0yr87wChBTvU7sm.htm)|+1 Studded Leather Armor|
|[d3GHBeQ70Fhuf2qy.htm](agents-of-edgewatch-bestiary-items/d3GHBeQ70Fhuf2qy.htm)|+1 Spear|+1|
|[dDbS7aNivWy3A1QB.htm](agents-of-edgewatch-bestiary-items/dDbS7aNivWy3A1QB.htm)|+2 Striking Spear|+2,striking|
|[dgrgw8Xe71mphkCY.htm](agents-of-edgewatch-bestiary-items/dgrgw8Xe71mphkCY.htm)|Sleep (at will)|
|[DwAwYDiHEH6B0cXp.htm](agents-of-edgewatch-bestiary-items/DwAwYDiHEH6B0cXp.htm)|Lightning Bolt (Coven)|
|[e43f6ELGQuh0SRwH.htm](agents-of-edgewatch-bestiary-items/e43f6ELGQuh0SRwH.htm)|+1 Chain Mail|
|[e7gUeRcn1eVnpZbY.htm](agents-of-edgewatch-bestiary-items/e7gUeRcn1eVnpZbY.htm)|Dimension Door (At Will)|
|[EFNvt6sGgOLoeJ68.htm](agents-of-edgewatch-bestiary-items/EFNvt6sGgOLoeJ68.htm)|+1 Greatpick|+1|
|[eHvlYGMN9JoTffVd.htm](agents-of-edgewatch-bestiary-items/eHvlYGMN9JoTffVd.htm)|Freedom of Movement (Constant)|
|[eiGG0G8cZOge4itT.htm](agents-of-edgewatch-bestiary-items/eiGG0G8cZOge4itT.htm)|Confusion (At Will)|
|[EjxAC6le3nK3Lfmx.htm](agents-of-edgewatch-bestiary-items/EjxAC6le3nK3Lfmx.htm)|+1 Striking Longsword|+1,striking|
|[EX3fkaLU5A7IcCrF.htm](agents-of-edgewatch-bestiary-items/EX3fkaLU5A7IcCrF.htm)|Gust of Wind (At Will)|
|[ezg7dGuKp0ENYDMs.htm](agents-of-edgewatch-bestiary-items/ezg7dGuKp0ENYDMs.htm)|Modified Spiked Chain|
|[FEiqbVup7LWCgX8z.htm](agents-of-edgewatch-bestiary-items/FEiqbVup7LWCgX8z.htm)|Detect Alignment (Constant) (Good Only)|
|[fer5YKRtixZJgTUl.htm](agents-of-edgewatch-bestiary-items/fer5YKRtixZJgTUl.htm)|+2 Resilient Studded Leather Armor|
|[fEuC1zQHNYV4wSBZ.htm](agents-of-edgewatch-bestiary-items/fEuC1zQHNYV4wSBZ.htm)|Reaper's Lancet (Bonded Item)|+1,striking|
|[flH3CHUzHXY7AFcz.htm](agents-of-edgewatch-bestiary-items/flH3CHUzHXY7AFcz.htm)|Scroll of Monstrosity Form (Level 8)|
|[fOaMNANOvNl4qS6h.htm](agents-of-edgewatch-bestiary-items/fOaMNANOvNl4qS6h.htm)|Project Image (at will)|
|[fxUNhEKO0dwda5AK.htm](agents-of-edgewatch-bestiary-items/fxUNhEKO0dwda5AK.htm)|+2 Greater Striking Dagger|+2,greaterStriking|
|[G20FyMLLePqGvHWu.htm](agents-of-edgewatch-bestiary-items/G20FyMLLePqGvHWu.htm)|Invisibility Potion|
|[g3Y3XnbOh4QHQnJj.htm](agents-of-edgewatch-bestiary-items/g3Y3XnbOh4QHQnJj.htm)|Magic Missile (at will)|
|[gfqAvWIZzyudPCy1.htm](agents-of-edgewatch-bestiary-items/gfqAvWIZzyudPCy1.htm)|Hallucinatory Terrain (At Will) (See Reshape Reality)|
|[ggAeDkCyOjPH2bhe.htm](agents-of-edgewatch-bestiary-items/ggAeDkCyOjPH2bhe.htm)|Black Sapphire|
|[Gjpuxi3isLodpRhs.htm](agents-of-edgewatch-bestiary-items/Gjpuxi3isLodpRhs.htm)|+1 Dagger|+1|
|[gLzGh53z2MGTb9dt.htm](agents-of-edgewatch-bestiary-items/gLzGh53z2MGTb9dt.htm)|True Seeing (Constant)|
|[GRBBK98z5cdbknpi.htm](agents-of-edgewatch-bestiary-items/GRBBK98z5cdbknpi.htm)|Formula Book|
|[GT3cJAnRfudy4dFc.htm](agents-of-edgewatch-bestiary-items/GT3cJAnRfudy4dFc.htm)|Invisibility (Self Only)|
|[GxUTTFPLkoN92z94.htm](agents-of-edgewatch-bestiary-items/GxUTTFPLkoN92z94.htm)|Detect Alignment (At Will) (Good Only)|
|[h50dYGzB0ZbzzDna.htm](agents-of-edgewatch-bestiary-items/h50dYGzB0ZbzzDna.htm)|+1 Longsword (Huge)|+1|
|[h7DCS7VEYpLZIEuD.htm](agents-of-edgewatch-bestiary-items/h7DCS7VEYpLZIEuD.htm)|+1 Striking Cleaver|+1,striking|
|[hAYUkM8ZmcrNbDcD.htm](agents-of-edgewatch-bestiary-items/hAYUkM8ZmcrNbDcD.htm)|Nightmare Cudgel|+1,striking|
|[HIKWZFplyyGRlfD3.htm](agents-of-edgewatch-bestiary-items/HIKWZFplyyGRlfD3.htm)|+2 Leather Armor|
|[htak5QRDAtx9sXI1.htm](agents-of-edgewatch-bestiary-items/htak5QRDAtx9sXI1.htm)|Locate (At Will)|
|[hwPxq98MdBLlWfAG.htm](agents-of-edgewatch-bestiary-items/hwPxq98MdBLlWfAG.htm)|Arrows With Shadow Essence|
|[hwWZSwxCvpgbQsMo.htm](agents-of-edgewatch-bestiary-items/hwWZSwxCvpgbQsMo.htm)|+1 Studded Leather Armor|
|[i4jm2tfwbjCeRC49.htm](agents-of-edgewatch-bestiary-items/i4jm2tfwbjCeRC49.htm)|+1 Striking Main-Gauche|+1,striking|
|[IebsPOkW0otKibJd.htm](agents-of-edgewatch-bestiary-items/IebsPOkW0otKibJd.htm)|Zone of Truth (At Will)|
|[IFtGiv3KwyArYGb5.htm](agents-of-edgewatch-bestiary-items/IFtGiv3KwyArYGb5.htm)|+1 Striking Temple Sword|+1,striking|
|[IMUPwDL0xKzelqlk.htm](agents-of-edgewatch-bestiary-items/IMUPwDL0xKzelqlk.htm)|Invisibility (At Will) (Self Only)|
|[irX5ienm71nSqEkF.htm](agents-of-edgewatch-bestiary-items/irX5ienm71nSqEkF.htm)|Dimension Door (At Will)|
|[ISpewirLysxAmJy3.htm](agents-of-edgewatch-bestiary-items/ISpewirLysxAmJy3.htm)|Warp Mind (At Will)|
|[IVI8CReUCZDFuZ5G.htm](agents-of-edgewatch-bestiary-items/IVI8CReUCZDFuZ5G.htm)|Detect Alignment (At Will) (Self Only)|
|[iWbKRtyiuqoqFwtY.htm](agents-of-edgewatch-bestiary-items/iWbKRtyiuqoqFwtY.htm)|Divine Wrath (Chaotic)|
|[Ix6CmOh95QwuaILp.htm](agents-of-edgewatch-bestiary-items/Ix6CmOh95QwuaILp.htm)|Mask of Terror (Constant) (See Tainted Backlash)|
|[iZAmnxfocOSmVb6v.htm](agents-of-edgewatch-bestiary-items/iZAmnxfocOSmVb6v.htm)|Summon Animal (Scorpions Only)|
|[JABmfDIIkLC6Isuf.htm](agents-of-edgewatch-bestiary-items/JABmfDIIkLC6Isuf.htm)|+2 Striking Dagger|+2,striking|
|[JdUO3WrBu0Sk5jmK.htm](agents-of-edgewatch-bestiary-items/JdUO3WrBu0Sk5jmK.htm)|Lightning Bolt (Coven)|
|[jfuiz3F7XOvLGRe4.htm](agents-of-edgewatch-bestiary-items/jfuiz3F7XOvLGRe4.htm)|Nightmare Cudgel|+1,striking|
|[JFUKB07rWCB4DUqo.htm](agents-of-edgewatch-bestiary-items/JFUKB07rWCB4DUqo.htm)|Teleport (At Will) (Self Only)|
|[jIZG2KPV0trDBaEX.htm](agents-of-edgewatch-bestiary-items/jIZG2KPV0trDBaEX.htm)|True Seeing (Constant)|
|[Jm2UMRCeDz88zPar.htm](agents-of-edgewatch-bestiary-items/Jm2UMRCeDz88zPar.htm)|Detect Alignment (At Will) (Lawful Only)|
|[jO9r6hau3IftVQbV.htm](agents-of-edgewatch-bestiary-items/jO9r6hau3IftVQbV.htm)|Fear (At Will)|
|[JpnevGqEKEck5xic.htm](agents-of-edgewatch-bestiary-items/JpnevGqEKEck5xic.htm)|+1 Leather Armor|
|[JppFZ0rQ39tus2ES.htm](agents-of-edgewatch-bestiary-items/JppFZ0rQ39tus2ES.htm)|Dimension Door (At Will)|
|[jqEKlXk90JA9eaPb.htm](agents-of-edgewatch-bestiary-items/jqEKlXk90JA9eaPb.htm)|Dispel Magic (At Will)|
|[jQNdJLLudvCqQFJq.htm](agents-of-edgewatch-bestiary-items/jQNdJLLudvCqQFJq.htm)|+2 Striking Dagger|+2,striking|
|[jXWHzmNiJtir9ovE.htm](agents-of-edgewatch-bestiary-items/jXWHzmNiJtir9ovE.htm)|Spellbook|
|[k1SvVANX3lFbXmgX.htm](agents-of-edgewatch-bestiary-items/k1SvVANX3lFbXmgX.htm)|Warhammer|+3,greaterStriking|
|[kbJBDbzeN6ARU68R.htm](agents-of-edgewatch-bestiary-items/kbJBDbzeN6ARU68R.htm)|Freedom of Movement (Constant)|
|[KGlehm1OPjnmc76I.htm](agents-of-edgewatch-bestiary-items/KGlehm1OPjnmc76I.htm)|Plane Shift (At Will) (Self Only)|
|[kI9jvukm7qAExPLC.htm](agents-of-edgewatch-bestiary-items/kI9jvukm7qAExPLC.htm)|Dimension Door (At Will)|
|[kNVRsfd1VsAPCAHl.htm](agents-of-edgewatch-bestiary-items/kNVRsfd1VsAPCAHl.htm)|Tongues (Constant)|
|[KtwDvOZ828wbD0jX.htm](agents-of-edgewatch-bestiary-items/KtwDvOZ828wbD0jX.htm)|Simple Injury Poison|
|[kzZi2RNVaUR4P6Dr.htm](agents-of-edgewatch-bestiary-items/kzZi2RNVaUR4P6Dr.htm)|Chain Lightning (Coven)|
|[LG9U1FcxaazlRbSL.htm](agents-of-edgewatch-bestiary-items/LG9U1FcxaazlRbSL.htm)|Dimensional Anchor (At Will)|
|[llp6T68rrWMMol0X.htm](agents-of-edgewatch-bestiary-items/llp6T68rrWMMol0X.htm)|Sneak Attack|
|[LnD04vz00vmBnjh5.htm](agents-of-edgewatch-bestiary-items/LnD04vz00vmBnjh5.htm)|+1 Chain Mail|
|[m07lGlvw1Lb15LwE.htm](agents-of-edgewatch-bestiary-items/m07lGlvw1Lb15LwE.htm)|Chain Lightning (Coven)|
|[M4MVjziBc8RhM4Dq.htm](agents-of-edgewatch-bestiary-items/M4MVjziBc8RhM4Dq.htm)|Tongues (Constant)|
|[M5cdeT2X42bGwPsK.htm](agents-of-edgewatch-bestiary-items/M5cdeT2X42bGwPsK.htm)|Invisibility (At Will; Self)|
|[MAhPy7k54Vmkmez7.htm](agents-of-edgewatch-bestiary-items/MAhPy7k54Vmkmez7.htm)|+1 Striking Shortsword|+1,striking|
|[MAVDlSJJ5XEJydH5.htm](agents-of-edgewatch-bestiary-items/MAVDlSJJ5XEJydH5.htm)|Detect Poison (At Will)|
|[mAxmh2QsWaEcHLIE.htm](agents-of-edgewatch-bestiary-items/mAxmh2QsWaEcHLIE.htm)|Detect Alignment (At Will) (Lawful Only)|
|[meAWyAhj8zesf3vv.htm](agents-of-edgewatch-bestiary-items/meAWyAhj8zesf3vv.htm)|Divine Wrath (Chaotic Only)|
|[mfewT8MWlHlBhBXV.htm](agents-of-edgewatch-bestiary-items/mfewT8MWlHlBhBXV.htm)|+1 Striking Shortsword|+1,striking|
|[mFFY1IVFVsXFDBCg.htm](agents-of-edgewatch-bestiary-items/mFFY1IVFVsXFDBCg.htm)|Tongues (Constant)|
|[mg5QKfm3VAuiQlXq.htm](agents-of-edgewatch-bestiary-items/mg5QKfm3VAuiQlXq.htm)|Dimension Door (At Will)|
|[MgINl4TZ5LClQxRj.htm](agents-of-edgewatch-bestiary-items/MgINl4TZ5LClQxRj.htm)|Teleport (At Will) (Self Only)|
|[MiXVxfkOiIFiL4kI.htm](agents-of-edgewatch-bestiary-items/MiXVxfkOiIFiL4kI.htm)|+1 Striking Dagger|+1,striking|
|[MjTG6JP6vMA8bV3d.htm](agents-of-edgewatch-bestiary-items/MjTG6JP6vMA8bV3d.htm)|Sneak Attack|
|[MKC1DsmJmuF2Loyk.htm](agents-of-edgewatch-bestiary-items/MKC1DsmJmuF2Loyk.htm)|Discern Lies (At Will)|
|[MLLTa4joQzrpvpIZ.htm](agents-of-edgewatch-bestiary-items/MLLTa4joQzrpvpIZ.htm)|Tongues (Constant)|
|[mma040p1v9uTLr6C.htm](agents-of-edgewatch-bestiary-items/mma040p1v9uTLr6C.htm)|Punishing Winds (Coven)|
|[MnRbSfAKIrT2ar4k.htm](agents-of-edgewatch-bestiary-items/MnRbSfAKIrT2ar4k.htm)|Chilling Darkness (At Will)|
|[MOaduge4IHL6IZ8k.htm](agents-of-edgewatch-bestiary-items/MOaduge4IHL6IZ8k.htm)|Detect Alignment (All Alignments; constant)|
|[mUYIbIl6uHRqjdxW.htm](agents-of-edgewatch-bestiary-items/mUYIbIl6uHRqjdxW.htm)|Spellbook|
|[mygshytVYxHhG1rn.htm](agents-of-edgewatch-bestiary-items/mygshytVYxHhG1rn.htm)|Invisibility (at will)|
|[N1xIUudfGRCvSmcx.htm](agents-of-edgewatch-bestiary-items/N1xIUudfGRCvSmcx.htm)|Dispel Magic (At Will)|
|[N7tigM3cgdlHJfqp.htm](agents-of-edgewatch-bestiary-items/N7tigM3cgdlHJfqp.htm)|Shining Bolt|
|[nB6gLufTvx4td3WH.htm](agents-of-edgewatch-bestiary-items/nB6gLufTvx4td3WH.htm)|Meld into Stone (At Will)|
|[NCSmuA0MRlQynOaO.htm](agents-of-edgewatch-bestiary-items/NCSmuA0MRlQynOaO.htm)|+2 Striking Crossbow|+2,striking|
|[Ndy9jtqnE8FTUfu6.htm](agents-of-edgewatch-bestiary-items/Ndy9jtqnE8FTUfu6.htm)|+1 Dagger|+1|
|[NgyduN3t56zNFCiI.htm](agents-of-edgewatch-bestiary-items/NgyduN3t56zNFCiI.htm)|Dimension Door (At Will)|
|[nIDi8mwAQaTD3hmj.htm](agents-of-edgewatch-bestiary-items/nIDi8mwAQaTD3hmj.htm)|+1 Striking Dagger|+1,striking|
|[nILh61j6DlzHrRSh.htm](agents-of-edgewatch-bestiary-items/nILh61j6DlzHrRSh.htm)|Divine Aura (Chaotic Only)|
|[nJsNcaSTO2fdeZHz.htm](agents-of-edgewatch-bestiary-items/nJsNcaSTO2fdeZHz.htm)|Detect Alignment (At Will) (Good Only)|
|[nUbnbXqOAEtycKyP.htm](agents-of-edgewatch-bestiary-items/nUbnbXqOAEtycKyP.htm)|+2 Resilient Full Plate|
|[o8RJn5jPVvF4Ekkt.htm](agents-of-edgewatch-bestiary-items/o8RJn5jPVvF4Ekkt.htm)|Ethereal Jaunt (at will)|
|[oCNXpWiLMOxihZJI.htm](agents-of-edgewatch-bestiary-items/oCNXpWiLMOxihZJI.htm)|+1 Striking Club|+1,striking|
|[oEHGOxor2V2wjqVL.htm](agents-of-edgewatch-bestiary-items/oEHGOxor2V2wjqVL.htm)|+2 Greater Striking War Razor|+2,greaterStriking|
|[oGBXwdMGWSTTqAD3.htm](agents-of-edgewatch-bestiary-items/oGBXwdMGWSTTqAD3.htm)|Nightmare (At Will)|
|[oiOOlXJBDbAdEc0d.htm](agents-of-edgewatch-bestiary-items/oiOOlXJBDbAdEc0d.htm)|Suggestion (At Will)|
|[OIyJUyKDxlE4ehrC.htm](agents-of-edgewatch-bestiary-items/OIyJUyKDxlE4ehrC.htm)|+1 Resilient Chain Shirt|
|[Oj9apypJGBT0BDsP.htm](agents-of-edgewatch-bestiary-items/Oj9apypJGBT0BDsP.htm)|Tongues (Constant)|
|[oypauyBHpYp3vd8b.htm](agents-of-edgewatch-bestiary-items/oypauyBHpYp3vd8b.htm)|Tongues (Constant)|
|[Ozqb1L0ETYnwmpbe.htm](agents-of-edgewatch-bestiary-items/Ozqb1L0ETYnwmpbe.htm)|Dagger|+2,greaterStriking,warpglass,anarchic|
|[pdZtZuMW2rdfWKrO.htm](agents-of-edgewatch-bestiary-items/pdZtZuMW2rdfWKrO.htm)|Air Walk (Constant)|
|[pfa26TywSZvzUcS7.htm](agents-of-edgewatch-bestiary-items/pfa26TywSZvzUcS7.htm)|Shatter (At Will)|
|[PL1PV6Sclath6M4Q.htm](agents-of-edgewatch-bestiary-items/PL1PV6Sclath6M4Q.htm)|Confusion (At Will)|
|[PN6ovO5KggUscxvM.htm](agents-of-edgewatch-bestiary-items/PN6ovO5KggUscxvM.htm)|Dream Message (at will)|
|[pnR32uZEWOhyRpjC.htm](agents-of-edgewatch-bestiary-items/pnR32uZEWOhyRpjC.htm)|Arrows|
|[pqU7Jju4NexrS8E1.htm](agents-of-edgewatch-bestiary-items/pqU7Jju4NexrS8E1.htm)|+1 Striking Sickle|+1|
|[pR42ZYfKNvfvZpV0.htm](agents-of-edgewatch-bestiary-items/pR42ZYfKNvfvZpV0.htm)|Charm (At Will)|
|[PTHHSvW8TM9Qoy4r.htm](agents-of-edgewatch-bestiary-items/PTHHSvW8TM9Qoy4r.htm)|+1 Striking Shortsword|+1,striking|
|[PVvftAKriwhrgoaR.htm](agents-of-edgewatch-bestiary-items/PVvftAKriwhrgoaR.htm)|True Seeing (Constant)|
|[pWF0uGOYSVrS0HHX.htm](agents-of-edgewatch-bestiary-items/pWF0uGOYSVrS0HHX.htm)|Sneak Attack|
|[pYKn9kyIrPzcHCf2.htm](agents-of-edgewatch-bestiary-items/pYKn9kyIrPzcHCf2.htm)|Detect Alignment (Constant)|
|[PZhVqvnw5wJJ8ZUG.htm](agents-of-edgewatch-bestiary-items/PZhVqvnw5wJJ8ZUG.htm)|True Seeing (Constant)|
|[Q5Dp50LJ1xa6jTip.htm](agents-of-edgewatch-bestiary-items/Q5Dp50LJ1xa6jTip.htm)|Detect Alignment (At Will) (Self Only)|
|[QcinC6e1RLBc6QTh.htm](agents-of-edgewatch-bestiary-items/QcinC6e1RLBc6QTh.htm)|Wyvern Poison (Applied to Rapier)|
|[qiA634EHSQUbF2DC.htm](agents-of-edgewatch-bestiary-items/qiA634EHSQUbF2DC.htm)|Illusory Disguise (At Will)|
|[qt1Be4Dqvl0gOcPw.htm](agents-of-edgewatch-bestiary-items/qt1Be4Dqvl0gOcPw.htm)|Freedom of Movement (Constant)|
|[rfWEzJifktPB3Mm1.htm](agents-of-edgewatch-bestiary-items/rfWEzJifktPB3Mm1.htm)|+1 Spell Storing Dagger (contains crisis of faith)|+1,spellStoring|
|[rluA9W0Io05ThwVb.htm](agents-of-edgewatch-bestiary-items/rluA9W0Io05ThwVb.htm)|Hypercognition (At Will)|
|[RM8aXzSKoAOdlPS9.htm](agents-of-edgewatch-bestiary-items/RM8aXzSKoAOdlPS9.htm)|Cane of the Maelstrom|+3,greaterStriking,anarchic|
|[rnQzo6H4yYb7RyAO.htm](agents-of-edgewatch-bestiary-items/rnQzo6H4yYb7RyAO.htm)|+1 Striking Scimitar|+1,striking|
|[rOeLgR7C3mjuvjay.htm](agents-of-edgewatch-bestiary-items/rOeLgR7C3mjuvjay.htm)|Mask of Terror (Self Only)|
|[RtZnZTmc4LjwxnH6.htm](agents-of-edgewatch-bestiary-items/RtZnZTmc4LjwxnH6.htm)|Invisibility (At Will) (Self Only)|
|[rUGUrNdhsxVMFs5r.htm](agents-of-edgewatch-bestiary-items/rUGUrNdhsxVMFs5r.htm)|Sneak Attack|
|[s0mhXAW1axJFfGed.htm](agents-of-edgewatch-bestiary-items/s0mhXAW1axJFfGed.htm)|Plane Shift (Coven)|
|[s3e8tlqX6GRkply1.htm](agents-of-edgewatch-bestiary-items/s3e8tlqX6GRkply1.htm)|+1 Striking Morningstar|+1,striking|
|[s3uuMJdCKgalWmvT.htm](agents-of-edgewatch-bestiary-items/s3uuMJdCKgalWmvT.htm)|Rapier|+1,striking|
|[sCeV7i5ftZVT9d83.htm](agents-of-edgewatch-bestiary-items/sCeV7i5ftZVT9d83.htm)|+2 Greater Striking axiomatic Scourge|+2,greaterStriking,axiomatic|
|[sjXOUlhh7Alii1i5.htm](agents-of-edgewatch-bestiary-items/sjXOUlhh7Alii1i5.htm)|Spellstrike Ammunition (Type I) (Magic Missile)|
|[sM7FYzOcg86Va8C5.htm](agents-of-edgewatch-bestiary-items/sM7FYzOcg86Va8C5.htm)|Religious Symbol of Norgorber|
|[SPNVzVFEBCiD0lpi.htm](agents-of-edgewatch-bestiary-items/SPNVzVFEBCiD0lpi.htm)|Bind Soul (at will)|
|[sts2SfYI0HiynY2c.htm](agents-of-edgewatch-bestiary-items/sts2SfYI0HiynY2c.htm)|+1 Striking Handwraps of Mighty Blows|+1,striking|
|[suCqLCq5KEa9ayQl.htm](agents-of-edgewatch-bestiary-items/suCqLCq5KEa9ayQl.htm)|Hallucinatory Terrain (See Reshape Reality)|
|[SxMzACgZ0pCmEWLt.htm](agents-of-edgewatch-bestiary-items/SxMzACgZ0pCmEWLt.htm)|Detect Alignment (At Will) (Lawful Only)|
|[T2IQDvzdXdy0PsfM.htm](agents-of-edgewatch-bestiary-items/T2IQDvzdXdy0PsfM.htm)|Tongues (constant)|
|[T5RHi6ahRuMa28yC.htm](agents-of-edgewatch-bestiary-items/T5RHi6ahRuMa28yC.htm)|Dimension Door (At Will)|
|[t7QQ6bJJDEkmFn6V.htm](agents-of-edgewatch-bestiary-items/t7QQ6bJJDEkmFn6V.htm)|Teleport (At Will) (Self Only)|
|[tCJQN9rOxSt30455.htm](agents-of-edgewatch-bestiary-items/tCJQN9rOxSt30455.htm)|Lawkeeper (+1 Striking Pacifying Club)|+1,striking,pacifying|
|[tF3OooF3TgShCxrF.htm](agents-of-edgewatch-bestiary-items/tF3OooF3TgShCxrF.htm)|+1 Striking Composite Shortbow|+1,striking|
|[thcjBip224elpVpB.htm](agents-of-edgewatch-bestiary-items/thcjBip224elpVpB.htm)|Creation (At Will)|
|[TkChtqTZI6N3wsQJ.htm](agents-of-edgewatch-bestiary-items/TkChtqTZI6N3wsQJ.htm)|Lightning Bolt (Coven)|
|[tteW7w29vx8cJw1T.htm](agents-of-edgewatch-bestiary-items/tteW7w29vx8cJw1T.htm)|Religious Symbol (Silver) of Norgorber|
|[tUeNkpqkh3G2EHfK.htm](agents-of-edgewatch-bestiary-items/tUeNkpqkh3G2EHfK.htm)|Dimension Door (At Will)|
|[tUvBQ5lDcV8DclDv.htm](agents-of-edgewatch-bestiary-items/tUvBQ5lDcV8DclDv.htm)|Slow (At Will)|
|[U2KRThEyzVE9Nm4D.htm](agents-of-edgewatch-bestiary-items/U2KRThEyzVE9Nm4D.htm)|Fly (Constant)|
|[UAxLReG8qB0bCtOc.htm](agents-of-edgewatch-bestiary-items/UAxLReG8qB0bCtOc.htm)|Dagger|+1,striking|
|[UeMQ10mABQZgRQWs.htm](agents-of-edgewatch-bestiary-items/UeMQ10mABQZgRQWs.htm)|Enthrall (At Will)|
|[uH2uHZ8f5Tx8hv1Z.htm](agents-of-edgewatch-bestiary-items/uH2uHZ8f5Tx8hv1Z.htm)|Freedom of Movement (Constant)|
|[uhvQvppURJpb0G3x.htm](agents-of-edgewatch-bestiary-items/uhvQvppURJpb0G3x.htm)|Spider Climb (Constant)|
|[USBYZSdxOeIK915f.htm](agents-of-edgewatch-bestiary-items/USBYZSdxOeIK915f.htm)|Freedom of Movement (Constant)|
|[Uxhs2XTuVqdcp4Iu.htm](agents-of-edgewatch-bestiary-items/Uxhs2XTuVqdcp4Iu.htm)|+2 Resilient Studded Leather Armor|
|[UZnWMgTWlt7r2rLT.htm](agents-of-edgewatch-bestiary-items/UZnWMgTWlt7r2rLT.htm)|Detect Magic (Constant)|
|[v8mqftrhdYYjlA4r.htm](agents-of-edgewatch-bestiary-items/v8mqftrhdYYjlA4r.htm)|Mask of Terror (See Crush of Hundreds)|
|[VAMq1HrYGd3KjkVh.htm](agents-of-edgewatch-bestiary-items/VAMq1HrYGd3KjkVh.htm)|Religious Symbol (Silver) of Norgorber|
|[vDYmQ2EDgaCKLrcg.htm](agents-of-edgewatch-bestiary-items/vDYmQ2EDgaCKLrcg.htm)|Alietta (+1 Striking Bastard Sword)|+1,striking|
|[vFxSfT5XARA6xoPF.htm](agents-of-edgewatch-bestiary-items/vFxSfT5XARA6xoPF.htm)|Ring of Manical Devices (Greater) (Fireball)|
|[vJTFGBX7WTtNkvIk.htm](agents-of-edgewatch-bestiary-items/vJTFGBX7WTtNkvIk.htm)|Chain Lightning (Coven)|
|[VNhWmP9iCwh5JnM0.htm](agents-of-edgewatch-bestiary-items/VNhWmP9iCwh5JnM0.htm)|Hunting Spider Venom (in Reaper's Lancet)|
|[vO5AJsTqiXeo9wUC.htm](agents-of-edgewatch-bestiary-items/vO5AJsTqiXeo9wUC.htm)|Water Walk (Constant)|
|[VotSkrKBPgtsqniV.htm](agents-of-edgewatch-bestiary-items/VotSkrKBPgtsqniV.htm)|+1 Hide|
|[VRzOD1frRuYIc8tK.htm](agents-of-edgewatch-bestiary-items/VRzOD1frRuYIc8tK.htm)|Punishing Winds (Coven)|
|[vu2GpFz94zOMhxuS.htm](agents-of-edgewatch-bestiary-items/vu2GpFz94zOMhxuS.htm)|Bind Soul (At Will)|
|[vuhWE8hU2cJiROF7.htm](agents-of-edgewatch-bestiary-items/vuhWE8hU2cJiROF7.htm)|True Seeing (Constant)|
|[VvH0EWE3C9hq7GdI.htm](agents-of-edgewatch-bestiary-items/VvH0EWE3C9hq7GdI.htm)|Wand of Widening (7th-Level Prismatic Spray)|
|[vZtOzGEcRgDrGcO4.htm](agents-of-edgewatch-bestiary-items/vZtOzGEcRgDrGcO4.htm)|+2 Greater Striking Hand Crossbow|+2,greaterStriking|
|[VzwOdAoGJR09TZhY.htm](agents-of-edgewatch-bestiary-items/VzwOdAoGJR09TZhY.htm)|+1 Striking Sap|+1,striking|
|[wACHliwpU57yeD9j.htm](agents-of-edgewatch-bestiary-items/wACHliwpU57yeD9j.htm)|True Seeing (Constant)|
|[wNvbgICDYZWvAlXQ.htm](agents-of-edgewatch-bestiary-items/wNvbgICDYZWvAlXQ.htm)|Plane Shift (Coven)|
|[WQTHEu7733ExGLgU.htm](agents-of-edgewatch-bestiary-items/WQTHEu7733ExGLgU.htm)|Detect Alignment (Constant)|
|[WSuS0KkjwvYp3vJr.htm](agents-of-edgewatch-bestiary-items/WSuS0KkjwvYp3vJr.htm)|+1 Leather Armor|
|[WVFJxumxiqgHdubA.htm](agents-of-edgewatch-bestiary-items/WVFJxumxiqgHdubA.htm)|Freedom of Movement (Constant)|
|[wY0J8uzVo7sVl4Sh.htm](agents-of-edgewatch-bestiary-items/wY0J8uzVo7sVl4Sh.htm)|Invisibility (At Will) (Self Only)|
|[X8cGGECu7Z03n3qr.htm](agents-of-edgewatch-bestiary-items/X8cGGECu7Z03n3qr.htm)|Dimension Door (At Will)|
|[xBdXIs3nfvpb8eRW.htm](agents-of-edgewatch-bestiary-items/xBdXIs3nfvpb8eRW.htm)|Dispel Magic (At Will)|
|[xCMHgYeYwL1Xccw2.htm](agents-of-edgewatch-bestiary-items/xCMHgYeYwL1Xccw2.htm)|Darkness (At Will)|
|[XGjh2lMNBPUOOYF4.htm](agents-of-edgewatch-bestiary-items/XGjh2lMNBPUOOYF4.htm)|Freedom of Movement (Constant)|
|[xkML6VHwHWx9E7rB.htm](agents-of-edgewatch-bestiary-items/xkML6VHwHWx9E7rB.htm)|+2 Striking Staff|+2,striking|
|[XLDEaSN0pl6RvjW4.htm](agents-of-edgewatch-bestiary-items/XLDEaSN0pl6RvjW4.htm)|True Seeing (Constant)|
|[XORGUr7fUhiU44lM.htm](agents-of-edgewatch-bestiary-items/XORGUr7fUhiU44lM.htm)|Spellbook|
|[XQXyh2fMab1x1Hjh.htm](agents-of-edgewatch-bestiary-items/XQXyh2fMab1x1Hjh.htm)|Giant Centipede Venom|
|[xrbkCxRQw572x4E9.htm](agents-of-edgewatch-bestiary-items/xrbkCxRQw572x4E9.htm)|Katana|+2,greaterStriking|
|[XUGNOucjelWU8HVC.htm](agents-of-edgewatch-bestiary-items/XUGNOucjelWU8HVC.htm)|+1 Shortsword|+1|
|[xxxZ0reFCEDypJhH.htm](agents-of-edgewatch-bestiary-items/xxxZ0reFCEDypJhH.htm)|Deafening Music Box|
|[xZ3ieTwpHIr5Ysgl.htm](agents-of-edgewatch-bestiary-items/xZ3ieTwpHIr5Ysgl.htm)|+1 Leather Armor|
|[ycb3x5FbTCDnmeGO.htm](agents-of-edgewatch-bestiary-items/ycb3x5FbTCDnmeGO.htm)|+2 Greater Striking Longsword|+2,greaterStriking|
|[yGw5lNOT9ohwQKqD.htm](agents-of-edgewatch-bestiary-items/yGw5lNOT9ohwQKqD.htm)|Detect Alignment (At Will) (Good Only)|
|[Yhl4kR7AquUzvkOr.htm](agents-of-edgewatch-bestiary-items/Yhl4kR7AquUzvkOr.htm)|+2 Greater Resilient Breastplate|
|[YkiuloNIynm9Dv8Z.htm](agents-of-edgewatch-bestiary-items/YkiuloNIynm9Dv8Z.htm)|+3 Major Striking Greatsword (Large)|+3,majorStriking|
|[yLWlmhmOztmNTDmd.htm](agents-of-edgewatch-bestiary-items/yLWlmhmOztmNTDmd.htm)|Dimension Door (At Will)|
|[ynah7STMOceErzSF.htm](agents-of-edgewatch-bestiary-items/ynah7STMOceErzSF.htm)|Divine Wrath (Chaotic Only)|
|[z1QK6YTYHTWWeQ9e.htm](agents-of-edgewatch-bestiary-items/z1QK6YTYHTWWeQ9e.htm)|+1 Striking Elven Curve Blade|+1,striking|
|[z84wqzko1RPXKwRr.htm](agents-of-edgewatch-bestiary-items/z84wqzko1RPXKwRr.htm)|+1 Striking Crossbow|+1,striking|
|[z8zotHW7l00PLxXV.htm](agents-of-edgewatch-bestiary-items/z8zotHW7l00PLxXV.htm)|Fear (At Will)|
|[ZA5mG7dkhLhwVl8M.htm](agents-of-edgewatch-bestiary-items/ZA5mG7dkhLhwVl8M.htm)|Illusory Creature (At Will)|
|[ZfMUXnwfQvzxGYlX.htm](agents-of-edgewatch-bestiary-items/ZfMUXnwfQvzxGYlX.htm)|+1 Striking Heavy Crossbow|+1,striking|
|[zhz25p52KqU33EK3.htm](agents-of-edgewatch-bestiary-items/zhz25p52KqU33EK3.htm)|Mind Blank (At Will)|
|[ZiSkIySccRkzOzMB.htm](agents-of-edgewatch-bestiary-items/ZiSkIySccRkzOzMB.htm)|Air Walk (At Will)|
|[ziZ2pnXpYBJNPeyC.htm](agents-of-edgewatch-bestiary-items/ziZ2pnXpYBJNPeyC.htm)|Freedom of Movement (Constant)|
|[ZjGRahNezwvDzYrI.htm](agents-of-edgewatch-bestiary-items/ZjGRahNezwvDzYrI.htm)|Tongues (Constant)|
|[zS2CuNrGV5vDoSO9.htm](agents-of-edgewatch-bestiary-items/zS2CuNrGV5vDoSO9.htm)|Hydraulic Push (At Will)|
|[ZTzu0yf3fKrlY2sf.htm](agents-of-edgewatch-bestiary-items/ZTzu0yf3fKrlY2sf.htm)|+1 Full Plate|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00AnAb9FZBwVZGjI.htm](agents-of-edgewatch-bestiary-items/00AnAb9FZBwVZGjI.htm)|Jaws|Fauces|modificada|
|[02vYXhCEPmoLEU7V.htm](agents-of-edgewatch-bestiary-items/02vYXhCEPmoLEU7V.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[034N8wrms1VjpEX4.htm](agents-of-edgewatch-bestiary-items/034N8wrms1VjpEX4.htm)|Terrible Justice|Terrible Justice|modificada|
|[09M4q5fVCuvOwcWk.htm](agents-of-edgewatch-bestiary-items/09M4q5fVCuvOwcWk.htm)|Poison Ink|Tinta Venenosa|modificada|
|[0AtXXmHLOdGkB70d.htm](agents-of-edgewatch-bestiary-items/0AtXXmHLOdGkB70d.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[0DxZM2ZhwpKrUolW.htm](agents-of-edgewatch-bestiary-items/0DxZM2ZhwpKrUolW.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[0Fzs97xBu4GMOwjA.htm](agents-of-edgewatch-bestiary-items/0Fzs97xBu4GMOwjA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0GDvJYPFAaQoah9E.htm](agents-of-edgewatch-bestiary-items/0GDvJYPFAaQoah9E.htm)|Syringe|Jeringa|modificada|
|[0Jid4uAhNoUlXWoo.htm](agents-of-edgewatch-bestiary-items/0Jid4uAhNoUlXWoo.htm)|Guildmaster's Lead|Guildmaster's Lead|modificada|
|[0jNbTDLglMGWVCtF.htm](agents-of-edgewatch-bestiary-items/0jNbTDLglMGWVCtF.htm)|Pincer|Pinza|modificada|
|[0jRv23fTxJTfzMQx.htm](agents-of-edgewatch-bestiary-items/0jRv23fTxJTfzMQx.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[0KI7dYfaduuCOAlE.htm](agents-of-edgewatch-bestiary-items/0KI7dYfaduuCOAlE.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[0kL2ZW2fqmtImuCu.htm](agents-of-edgewatch-bestiary-items/0kL2ZW2fqmtImuCu.htm)|Grab|Agarrado|modificada|
|[0mlWlB8x9buvEw3g.htm](agents-of-edgewatch-bestiary-items/0mlWlB8x9buvEw3g.htm)|Swarm Mind|Swarm Mind|modificada|
|[0rqwxWSqNK0Ikmdw.htm](agents-of-edgewatch-bestiary-items/0rqwxWSqNK0Ikmdw.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[0TIsrEfH3mfhYqMB.htm](agents-of-edgewatch-bestiary-items/0TIsrEfH3mfhYqMB.htm)|Eye-Opener|Abrir los ojos|modificada|
|[0tqsvUhGYvPuB2ga.htm](agents-of-edgewatch-bestiary-items/0tqsvUhGYvPuB2ga.htm)|Reshape Reality|Reshape Reality|modificada|
|[0UpmO63mWa667Q99.htm](agents-of-edgewatch-bestiary-items/0UpmO63mWa667Q99.htm)|Tactical Aura|Aura Táctica|modificada|
|[0vSVpFhBgh2bSdVc.htm](agents-of-edgewatch-bestiary-items/0vSVpFhBgh2bSdVc.htm)|Fist|Puño|modificada|
|[0yo458L980s9JXH6.htm](agents-of-edgewatch-bestiary-items/0yo458L980s9JXH6.htm)|Pest Haven|Pest Haven|modificada|
|[10AhR44dLRwNyyyT.htm](agents-of-edgewatch-bestiary-items/10AhR44dLRwNyyyT.htm)|Alchemical Rupture|Fractura alquímica|modificada|
|[126ppmFKAd49r2hi.htm](agents-of-edgewatch-bestiary-items/126ppmFKAd49r2hi.htm)|Najra Swarm Attack|Najra Swarm Attack|modificada|
|[157GGkuQcvXmLwPu.htm](agents-of-edgewatch-bestiary-items/157GGkuQcvXmLwPu.htm)|Hammering Flurry|Ráfaga de martillazos|modificada|
|[1a9J9jdVUsp4RIxQ.htm](agents-of-edgewatch-bestiary-items/1a9J9jdVUsp4RIxQ.htm)|Sky and Heaven Stance|Posición del cielo y el cielo.|modificada|
|[1bSj6FFb2A1ZOyrZ.htm](agents-of-edgewatch-bestiary-items/1bSj6FFb2A1ZOyrZ.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[1cQlKfzWAd8gXGFa.htm](agents-of-edgewatch-bestiary-items/1cQlKfzWAd8gXGFa.htm)|Malefic Binding|Malefic Binding|modificada|
|[1d7P7fxvDCam2ALx.htm](agents-of-edgewatch-bestiary-items/1d7P7fxvDCam2ALx.htm)|Vein Walker|Vein Walker|modificada|
|[1DABV9KyoaCfVd3F.htm](agents-of-edgewatch-bestiary-items/1DABV9KyoaCfVd3F.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1eq3XxcWMvDVBheO.htm](agents-of-edgewatch-bestiary-items/1eq3XxcWMvDVBheO.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[1GcnjXs1m3fitCxq.htm](agents-of-edgewatch-bestiary-items/1GcnjXs1m3fitCxq.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[1h5CBWyXY0MAHGtm.htm](agents-of-edgewatch-bestiary-items/1h5CBWyXY0MAHGtm.htm)|Bloody Sneak Attack|Sangriento Movimiento furtivo|modificada|
|[1HjWqCIXUjmknlZE.htm](agents-of-edgewatch-bestiary-items/1HjWqCIXUjmknlZE.htm)|Adamantine Strikes|Golpes adamantinos|modificada|
|[1mBHiJQyWY7LwsCN.htm](agents-of-edgewatch-bestiary-items/1mBHiJQyWY7LwsCN.htm)|Rend|Rasgadura|modificada|
|[1OKW1epSXihm5XRb.htm](agents-of-edgewatch-bestiary-items/1OKW1epSXihm5XRb.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[1pf3cBO6a0unyMKL.htm](agents-of-edgewatch-bestiary-items/1pf3cBO6a0unyMKL.htm)|Jaws|Fauces|modificada|
|[1qQjPJIus3hZOUXY.htm](agents-of-edgewatch-bestiary-items/1qQjPJIus3hZOUXY.htm)|Powerful Swipe|Vaivén Poderoso|modificada|
|[1Rk88GORg0fjreo8.htm](agents-of-edgewatch-bestiary-items/1Rk88GORg0fjreo8.htm)|Chain Up|Chain Up|modificada|
|[1sozJvnrR9nNQ2qu.htm](agents-of-edgewatch-bestiary-items/1sozJvnrR9nNQ2qu.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[1UFXWKQIPIkKudjR.htm](agents-of-edgewatch-bestiary-items/1UFXWKQIPIkKudjR.htm)|Fangs|Colmillos|modificada|
|[1vHKAVMhE8gmaKS2.htm](agents-of-edgewatch-bestiary-items/1vHKAVMhE8gmaKS2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[1wPZqdlI8xQ4yfN9.htm](agents-of-edgewatch-bestiary-items/1wPZqdlI8xQ4yfN9.htm)|Fangs|Colmillos|modificada|
|[22ts3ClOQFeLq0Jt.htm](agents-of-edgewatch-bestiary-items/22ts3ClOQFeLq0Jt.htm)|Water Shield|Escudo de agua|modificada|
|[268BBCle1rCc1g1d.htm](agents-of-edgewatch-bestiary-items/268BBCle1rCc1g1d.htm)|Cannon Fusillade|Cannon Fusillade|modificada|
|[26ZZztmP0cFmWp6x.htm](agents-of-edgewatch-bestiary-items/26ZZztmP0cFmWp6x.htm)|Precise Tremorsense 40 feet|Precisión de temblores 40 pies.|modificada|
|[281TWDcEhOImRl44.htm](agents-of-edgewatch-bestiary-items/281TWDcEhOImRl44.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[29elTy7XDcAju8yK.htm](agents-of-edgewatch-bestiary-items/29elTy7XDcAju8yK.htm)|Ghaele's Gaze|Mirada del ghaele|modificada|
|[29naBTIemmuYyMKB.htm](agents-of-edgewatch-bestiary-items/29naBTIemmuYyMKB.htm)|Skip Between|Saltar entre|modificada|
|[2EL49f3yp855RUpq.htm](agents-of-edgewatch-bestiary-items/2EL49f3yp855RUpq.htm)|Quick Movements|Movimientos Rápidos|modificada|
|[2FxUvyDjnESqE9bl.htm](agents-of-edgewatch-bestiary-items/2FxUvyDjnESqE9bl.htm)|Quick Brew|Brebaje Rápido|modificada|
|[2gGx394noy8wbdH3.htm](agents-of-edgewatch-bestiary-items/2gGx394noy8wbdH3.htm)|Constrict|Restringir|modificada|
|[2JJtxgkrLOcIfYQz.htm](agents-of-edgewatch-bestiary-items/2JJtxgkrLOcIfYQz.htm)|Mirror Dart|Dardo Espejo|modificada|
|[2JN5a5P81ZqQkOsD.htm](agents-of-edgewatch-bestiary-items/2JN5a5P81ZqQkOsD.htm)|Painful Light|Luz Dolorosa|modificada|
|[2pMaO2Z0N8Vx4Yy8.htm](agents-of-edgewatch-bestiary-items/2pMaO2Z0N8Vx4Yy8.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[2S79HfbIxhTAsZFh.htm](agents-of-edgewatch-bestiary-items/2S79HfbIxhTAsZFh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2TFdxsGPr1xaX73k.htm](agents-of-edgewatch-bestiary-items/2TFdxsGPr1xaX73k.htm)|Syringe|Jeringa|modificada|
|[2tZ1okZGWrxi91Ba.htm](agents-of-edgewatch-bestiary-items/2tZ1okZGWrxi91Ba.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[2Vevg87ax4Fd8BnK.htm](agents-of-edgewatch-bestiary-items/2Vevg87ax4Fd8BnK.htm)|Shield Bash|Escudo Bash|modificada|
|[2w1WQr635JVnsORb.htm](agents-of-edgewatch-bestiary-items/2w1WQr635JVnsORb.htm)|Swift Sneak|Movimiento furtivo rápido|modificada|
|[2wSPUQgCOkfTQCUJ.htm](agents-of-edgewatch-bestiary-items/2wSPUQgCOkfTQCUJ.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[2Wzd91SDBc89gsI9.htm](agents-of-edgewatch-bestiary-items/2Wzd91SDBc89gsI9.htm)|Tail|Tail|modificada|
|[2yp2PpkHqWXuA5k0.htm](agents-of-edgewatch-bestiary-items/2yp2PpkHqWXuA5k0.htm)|Change Shape|Change Shape|modificada|
|[2zmTtiyeZNvKPOcI.htm](agents-of-edgewatch-bestiary-items/2zmTtiyeZNvKPOcI.htm)|Ink Blood|Ink Blood|modificada|
|[2zo8BFj6mkav8MPT.htm](agents-of-edgewatch-bestiary-items/2zo8BFj6mkav8MPT.htm)|Ferocious Devotion|Ferocious Devotion|modificada|
|[3039pLEWxpDZZrH0.htm](agents-of-edgewatch-bestiary-items/3039pLEWxpDZZrH0.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[31SjrOIjFWX8if4N.htm](agents-of-edgewatch-bestiary-items/31SjrOIjFWX8if4N.htm)|Poison Weapon|Arma envenenada|modificada|
|[32FbUxGNpWinXjEq.htm](agents-of-edgewatch-bestiary-items/32FbUxGNpWinXjEq.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[345q6v1fOXwCpJpM.htm](agents-of-edgewatch-bestiary-items/345q6v1fOXwCpJpM.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[35yXdy43dCClA3wy.htm](agents-of-edgewatch-bestiary-items/35yXdy43dCClA3wy.htm)|Smooth Operator|Smooth Operator|modificada|
|[3aasDsiXoE4xtSVZ.htm](agents-of-edgewatch-bestiary-items/3aasDsiXoE4xtSVZ.htm)|Distracting Shadows|Sombras distractoras|modificada|
|[3BvCMBxHiHXEMq2P.htm](agents-of-edgewatch-bestiary-items/3BvCMBxHiHXEMq2P.htm)|Bonesense (Imprecise) 30 feet|Bonesense (Impreciso) 30 pies|modificada|
|[3bYjif3A9K5vszf9.htm](agents-of-edgewatch-bestiary-items/3bYjif3A9K5vszf9.htm)|Claw|Garra|modificada|
|[3dd754R1VUxFyZiT.htm](agents-of-edgewatch-bestiary-items/3dd754R1VUxFyZiT.htm)|Secret of Rebirth|Secret of Rebirth|modificada|
|[3fSPm7dDpsyRRKqg.htm](agents-of-edgewatch-bestiary-items/3fSPm7dDpsyRRKqg.htm)|Fast Healing 8|Curación rápida 8|modificada|
|[3g9fOfW6T5VklwH5.htm](agents-of-edgewatch-bestiary-items/3g9fOfW6T5VklwH5.htm)|Compression|Compresión|modificada|
|[3jd4JNaR2XiNoddM.htm](agents-of-edgewatch-bestiary-items/3jd4JNaR2XiNoddM.htm)|Jaws|Fauces|modificada|
|[3jh7PEda42lpDxXR.htm](agents-of-edgewatch-bestiary-items/3jh7PEda42lpDxXR.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[3N2FHMvzMRnvpsId.htm](agents-of-edgewatch-bestiary-items/3N2FHMvzMRnvpsId.htm)|Human Hunter|Cazador humano|modificada|
|[3SoJ3oq6Q1vAEy8o.htm](agents-of-edgewatch-bestiary-items/3SoJ3oq6Q1vAEy8o.htm)|Grab|Agarrado|modificada|
|[3sUb7J0bD92GSjki.htm](agents-of-edgewatch-bestiary-items/3sUb7J0bD92GSjki.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[3TDeHZ2NkeZhRp86.htm](agents-of-edgewatch-bestiary-items/3TDeHZ2NkeZhRp86.htm)|Despair Ray|Despair Ray|modificada|
|[3VSwbSPRGAYY8tg5.htm](agents-of-edgewatch-bestiary-items/3VSwbSPRGAYY8tg5.htm)|Songblade|Songblade|modificada|
|[3vUeNTCG7OjIBa45.htm](agents-of-edgewatch-bestiary-items/3vUeNTCG7OjIBa45.htm)|Forced Regeneration|Regeneración vigorosa|modificada|
|[3xd9C3zcdYxijFhU.htm](agents-of-edgewatch-bestiary-items/3xd9C3zcdYxijFhU.htm)|Trample|Trample|modificada|
|[46OKgjftfLLd55Dz.htm](agents-of-edgewatch-bestiary-items/46OKgjftfLLd55Dz.htm)|Katana|Katana|modificada|
|[4CShIdKgZz5cAVsW.htm](agents-of-edgewatch-bestiary-items/4CShIdKgZz5cAVsW.htm)|Occult Rituals|Rituales de Ocultismo|modificada|
|[4dEZdXNmp0qZ2uWV.htm](agents-of-edgewatch-bestiary-items/4dEZdXNmp0qZ2uWV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4EqqjuXvtgi5CQMY.htm](agents-of-edgewatch-bestiary-items/4EqqjuXvtgi5CQMY.htm)|Skyward Slash|Skyward Slash|modificada|
|[4fuCvW4VqkpcxMz7.htm](agents-of-edgewatch-bestiary-items/4fuCvW4VqkpcxMz7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[4G2h2y2d0Zz3woFP.htm](agents-of-edgewatch-bestiary-items/4G2h2y2d0Zz3woFP.htm)|Wave|Wave|modificada|
|[4jItoqJFtYwZm9bE.htm](agents-of-edgewatch-bestiary-items/4jItoqJFtYwZm9bE.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[4LPTg9hErKNAmmv0.htm](agents-of-edgewatch-bestiary-items/4LPTg9hErKNAmmv0.htm)|Negative Healing|Curación negativa|modificada|
|[4LuKFdOn742r53ct.htm](agents-of-edgewatch-bestiary-items/4LuKFdOn742r53ct.htm)|Dagger|Daga|modificada|
|[4mfr0BXdQQ9ZuRDr.htm](agents-of-edgewatch-bestiary-items/4mfr0BXdQQ9ZuRDr.htm)|Deepen the Wound|Profundizar la Herida|modificada|
|[4nJSWcGlNE3k6uH0.htm](agents-of-edgewatch-bestiary-items/4nJSWcGlNE3k6uH0.htm)|Black Ink Delirium|Delirio de tinta negra|modificada|
|[4p4BNBKvDMPM8yRG.htm](agents-of-edgewatch-bestiary-items/4p4BNBKvDMPM8yRG.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[4QRkjb5aTrVTZYRO.htm](agents-of-edgewatch-bestiary-items/4QRkjb5aTrVTZYRO.htm)|Jealous Musician|Músico Celoso|modificada|
|[4RXnnP8CpvFi5RjK.htm](agents-of-edgewatch-bestiary-items/4RXnnP8CpvFi5RjK.htm)|All-Around Vision|All-Around Vision|modificada|
|[4T0hd15HFgEoLTEv.htm](agents-of-edgewatch-bestiary-items/4T0hd15HFgEoLTEv.htm)|Slow|Lentificado/a|modificada|
|[4TtLSpRhmqV1Dvp1.htm](agents-of-edgewatch-bestiary-items/4TtLSpRhmqV1Dvp1.htm)|Tail Whip|Coletazo|modificada|
|[4UhxcsjIkaaA0FnK.htm](agents-of-edgewatch-bestiary-items/4UhxcsjIkaaA0FnK.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[4UovGCnsjker1UNi.htm](agents-of-edgewatch-bestiary-items/4UovGCnsjker1UNi.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[4uyjbjTFC0HefTzh.htm](agents-of-edgewatch-bestiary-items/4uyjbjTFC0HefTzh.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[4VKUGZEqMWaJycr4.htm](agents-of-edgewatch-bestiary-items/4VKUGZEqMWaJycr4.htm)|Shield Block|Bloquear con escudo|modificada|
|[4Wo8cEyKXFYXahnb.htm](agents-of-edgewatch-bestiary-items/4Wo8cEyKXFYXahnb.htm)|Entropy Sense (Imprecise) 60 feet|Sentido de Entropía (Impreciso) 60 pies|modificada|
|[53IhzCwi4y8yO9XX.htm](agents-of-edgewatch-bestiary-items/53IhzCwi4y8yO9XX.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[571EUWSacME4MxgN.htm](agents-of-edgewatch-bestiary-items/571EUWSacME4MxgN.htm)|Morningstar|Morningstar|modificada|
|[57lDKo6dcd3jNhcT.htm](agents-of-edgewatch-bestiary-items/57lDKo6dcd3jNhcT.htm)|Jaws|Fauces|modificada|
|[5aBlY3dBYmIMAZwP.htm](agents-of-edgewatch-bestiary-items/5aBlY3dBYmIMAZwP.htm)|Swinging Chandelier|Araña oscilante|modificada|
|[5B9QSKBjBa9tC0SG.htm](agents-of-edgewatch-bestiary-items/5B9QSKBjBa9tC0SG.htm)|Stunning Strike|Golpe Aturdidor|modificada|
|[5bx4G4sDLKNJ0Fim.htm](agents-of-edgewatch-bestiary-items/5bx4G4sDLKNJ0Fim.htm)|Frightful Presence|Frightful Presence|modificada|
|[5DSmWdkGY2DeiYD8.htm](agents-of-edgewatch-bestiary-items/5DSmWdkGY2DeiYD8.htm)|Attach|Aferrarse|modificada|
|[5GTOdw4CBKeecBvX.htm](agents-of-edgewatch-bestiary-items/5GTOdw4CBKeecBvX.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[5hCSuILSsdP2wKtN.htm](agents-of-edgewatch-bestiary-items/5hCSuILSsdP2wKtN.htm)|Cheek Pouches|Carrilleras|modificada|
|[5HdfiD2BmZU6LzvL.htm](agents-of-edgewatch-bestiary-items/5HdfiD2BmZU6LzvL.htm)|Whirlwind Strike|Golpe de torbellino|modificada|
|[5Hk5r8bi7Nulj5pY.htm](agents-of-edgewatch-bestiary-items/5Hk5r8bi7Nulj5pY.htm)|Negative Healing|Curación negativa|modificada|
|[5inXNyZGoLUMii53.htm](agents-of-edgewatch-bestiary-items/5inXNyZGoLUMii53.htm)|Slither|Slither|modificada|
|[5izc2RKfDYtjIicX.htm](agents-of-edgewatch-bestiary-items/5izc2RKfDYtjIicX.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[5qAf1qV6aq6q3pQl.htm](agents-of-edgewatch-bestiary-items/5qAf1qV6aq6q3pQl.htm)|Little Favors|Pequeños Favores|modificada|
|[5rYhqUb2YVDE98ag.htm](agents-of-edgewatch-bestiary-items/5rYhqUb2YVDE98ag.htm)|Warpwave Spell|Hechizo Onda Warp|modificada|
|[5vpT5j3f28sZPdRn.htm](agents-of-edgewatch-bestiary-items/5vpT5j3f28sZPdRn.htm)|Collapse|Colapso|modificada|
|[5wnaaHpUx6f7lycE.htm](agents-of-edgewatch-bestiary-items/5wnaaHpUx6f7lycE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5wVIixUHmaim2A4K.htm](agents-of-edgewatch-bestiary-items/5wVIixUHmaim2A4K.htm)|Consume Flesh|Consumir carne|modificada|
|[5Yk1UPJggdSuj7Kw.htm](agents-of-edgewatch-bestiary-items/5Yk1UPJggdSuj7Kw.htm)|Claw|Garra|modificada|
|[5Z1XSibUkx9LaIrv.htm](agents-of-edgewatch-bestiary-items/5Z1XSibUkx9LaIrv.htm)|Claws|Garras|modificada|
|[6BA0nR5VEDa0Aw15.htm](agents-of-edgewatch-bestiary-items/6BA0nR5VEDa0Aw15.htm)|Surface-Bound|Surface-Bound|modificada|
|[6cFodLsS9nMte5ts.htm](agents-of-edgewatch-bestiary-items/6cFodLsS9nMte5ts.htm)|Fill Tank|Tanque de llenado|modificada|
|[6DnRipdoNRQWMUyZ.htm](agents-of-edgewatch-bestiary-items/6DnRipdoNRQWMUyZ.htm)|Spit|Escupe|modificada|
|[6gBFZhqoL9eg2XDz.htm](agents-of-edgewatch-bestiary-items/6gBFZhqoL9eg2XDz.htm)|Collective Attack|Ataque Colectivo|modificada|
|[6hx8YYYNNoWo6xwu.htm](agents-of-edgewatch-bestiary-items/6hx8YYYNNoWo6xwu.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6M89EOsieWisWkK3.htm](agents-of-edgewatch-bestiary-items/6M89EOsieWisWkK3.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[6nFnVyRyLOu3DldL.htm](agents-of-edgewatch-bestiary-items/6nFnVyRyLOu3DldL.htm)|Crossbow|Ballesta|modificada|
|[6Nu42xqROJzl0XrC.htm](agents-of-edgewatch-bestiary-items/6Nu42xqROJzl0XrC.htm)|Hampering Strike|Golpe entorpecedor|modificada|
|[6pCE969IqFRVqLzJ.htm](agents-of-edgewatch-bestiary-items/6pCE969IqFRVqLzJ.htm)|Engulf|Envolver|modificada|
|[6pcwqNViNq5cqn8U.htm](agents-of-edgewatch-bestiary-items/6pcwqNViNq5cqn8U.htm)|Claw|Garra|modificada|
|[6PRImWQWZEOuSAeI.htm](agents-of-edgewatch-bestiary-items/6PRImWQWZEOuSAeI.htm)|Dagger|Daga|modificada|
|[6Tw3xiLUkr4AYhGN.htm](agents-of-edgewatch-bestiary-items/6Tw3xiLUkr4AYhGN.htm)|Quick Bomber|Bombardero rápido|modificada|
|[6u1eMdIQZV0dWPQU.htm](agents-of-edgewatch-bestiary-items/6u1eMdIQZV0dWPQU.htm)|Opportune Dodge|Esquiva Oportuna|modificada|
|[6YAq6Oj3jOtxCRWq.htm](agents-of-edgewatch-bestiary-items/6YAq6Oj3jOtxCRWq.htm)|Claw|Garra|modificada|
|[70miglztK7B97hcW.htm](agents-of-edgewatch-bestiary-items/70miglztK7B97hcW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[71ypVkmQCnSo1C8X.htm](agents-of-edgewatch-bestiary-items/71ypVkmQCnSo1C8X.htm)|Claw|Garra|modificada|
|[735O6tGVpk8PcXiV.htm](agents-of-edgewatch-bestiary-items/735O6tGVpk8PcXiV.htm)|Innate Arcane Spells|Hechizos Arcanos Innatos|modificada|
|[745PY8vNnU4dRB3J.htm](agents-of-edgewatch-bestiary-items/745PY8vNnU4dRB3J.htm)|Diabolic Certitude|Certeza diabólica|modificada|
|[76fFiqfOfbkyXm8t.htm](agents-of-edgewatch-bestiary-items/76fFiqfOfbkyXm8t.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[79KeB3EaNuz3Wjd2.htm](agents-of-edgewatch-bestiary-items/79KeB3EaNuz3Wjd2.htm)|Spatial Riptide|Riptide espacial|modificada|
|[7ArMrPYZDTroaUsN.htm](agents-of-edgewatch-bestiary-items/7ArMrPYZDTroaUsN.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7biZlvn3vY2e7oEj.htm](agents-of-edgewatch-bestiary-items/7biZlvn3vY2e7oEj.htm)|Divert Strike|Desviar Golpe|modificada|
|[7GSRG0kO6Z9IWhXQ.htm](agents-of-edgewatch-bestiary-items/7GSRG0kO6Z9IWhXQ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7iz6trwOzT3IfpHj.htm](agents-of-edgewatch-bestiary-items/7iz6trwOzT3IfpHj.htm)|Constant Spells|Constant Spells|modificada|
|[7KBnvkWiiKe2PzQ5.htm](agents-of-edgewatch-bestiary-items/7KBnvkWiiKe2PzQ5.htm)|Horn|Cuerno|modificada|
|[7kENjgZrG04Exwhk.htm](agents-of-edgewatch-bestiary-items/7kENjgZrG04Exwhk.htm)|Telepathy 100 Feet|Telepatía de 100 pies.|modificada|
|[7kep2BZKcI8m29QV.htm](agents-of-edgewatch-bestiary-items/7kep2BZKcI8m29QV.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[7l3dJqmfIwNmUyfj.htm](agents-of-edgewatch-bestiary-items/7l3dJqmfIwNmUyfj.htm)|Negative Healing|Curación negativa|modificada|
|[7m8XF9B86oB7by1T.htm](agents-of-edgewatch-bestiary-items/7m8XF9B86oB7by1T.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7nwDQbdYpJ5U6EZs.htm](agents-of-edgewatch-bestiary-items/7nwDQbdYpJ5U6EZs.htm)|Call Toxins|Llamada Toxinas|modificada|
|[7OjXQSbCokE3yjiM.htm](agents-of-edgewatch-bestiary-items/7OjXQSbCokE3yjiM.htm)|Shovel|Pala|modificada|
|[7PJ0xQKU3T5EtaPW.htm](agents-of-edgewatch-bestiary-items/7PJ0xQKU3T5EtaPW.htm)|Heaven's Thunder|Tronante del Cielo|modificada|
|[7qqes4mzlHueunJX.htm](agents-of-edgewatch-bestiary-items/7qqes4mzlHueunJX.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[7RZClAttcFohXVTd.htm](agents-of-edgewatch-bestiary-items/7RZClAttcFohXVTd.htm)|Little Favors|Pequeños Favores|modificada|
|[7UeREmeUuipS0pKu.htm](agents-of-edgewatch-bestiary-items/7UeREmeUuipS0pKu.htm)|Dart|Dardo|modificada|
|[7UsBYA3xYXL2TFT2.htm](agents-of-edgewatch-bestiary-items/7UsBYA3xYXL2TFT2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7vOzE1FHZEotvswQ.htm](agents-of-edgewatch-bestiary-items/7vOzE1FHZEotvswQ.htm)|+2 Status Bonus on Saves vs. Shove|Bonificación de +2 a la situación en las salvaciones contra empujón.|modificada|
|[84E8AqCBiRIBEHuG.htm](agents-of-edgewatch-bestiary-items/84E8AqCBiRIBEHuG.htm)|Dagger|Daga|modificada|
|[87HtA52J9Oqfjsft.htm](agents-of-edgewatch-bestiary-items/87HtA52J9Oqfjsft.htm)|Illusory Persona|Persona Ilusoria|modificada|
|[8aw6p0tL8IYj3lNK.htm](agents-of-edgewatch-bestiary-items/8aw6p0tL8IYj3lNK.htm)|Tail|Tail|modificada|
|[8bbFiMBoHc5BG8QA.htm](agents-of-edgewatch-bestiary-items/8bbFiMBoHc5BG8QA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8dM3dH69X49kSo82.htm](agents-of-edgewatch-bestiary-items/8dM3dH69X49kSo82.htm)|Dagger|Daga|modificada|
|[8IxYcWmidpRnNMrc.htm](agents-of-edgewatch-bestiary-items/8IxYcWmidpRnNMrc.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[8j0TGPf1kZlcIwpn.htm](agents-of-edgewatch-bestiary-items/8j0TGPf1kZlcIwpn.htm)|Steal Memories|Sustraer Recuerdos|modificada|
|[8JslG7YLjptMOVC0.htm](agents-of-edgewatch-bestiary-items/8JslG7YLjptMOVC0.htm)|Sudden Betrayal|Traición inesperada|modificada|
|[8JzS4wK4EWHWFJlY.htm](agents-of-edgewatch-bestiary-items/8JzS4wK4EWHWFJlY.htm)|Chopper|Chopper|modificada|
|[8K2BcPcACmOHHydT.htm](agents-of-edgewatch-bestiary-items/8K2BcPcACmOHHydT.htm)|Dreadsong|Dreadsong|modificada|
|[8k84XkMz9ZT6F3TB.htm](agents-of-edgewatch-bestiary-items/8k84XkMz9ZT6F3TB.htm)|Rapier|Estoque|modificada|
|[8kuBPd4vWsUhaCao.htm](agents-of-edgewatch-bestiary-items/8kuBPd4vWsUhaCao.htm)|Blinding Stream|Arroyo cegador|modificada|
|[8N0oYLqMb0kjQ009.htm](agents-of-edgewatch-bestiary-items/8N0oYLqMb0kjQ009.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[8NRxk6m07y9TpopE.htm](agents-of-edgewatch-bestiary-items/8NRxk6m07y9TpopE.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[8QIEpGKWvwKK8mSZ.htm](agents-of-edgewatch-bestiary-items/8QIEpGKWvwKK8mSZ.htm)|Scraping Clamor|Clamor Rascadura|modificada|
|[8QnTTWcdqDNyOtIl.htm](agents-of-edgewatch-bestiary-items/8QnTTWcdqDNyOtIl.htm)|Miasma of Pollution|Miasma of Pollution|modificada|
|[8qsC3gMbkSTTQOdY.htm](agents-of-edgewatch-bestiary-items/8qsC3gMbkSTTQOdY.htm)|Impale|Empalar|modificada|
|[8Qz8Y3OYthQCPvSH.htm](agents-of-edgewatch-bestiary-items/8Qz8Y3OYthQCPvSH.htm)|Constant Spells|Constant Spells|modificada|
|[8RfMfocN311lqv8N.htm](agents-of-edgewatch-bestiary-items/8RfMfocN311lqv8N.htm)|Spell-Imbued Blade|Spell-Imbued Blade|modificada|
|[8RQS62riVfrsiZjr.htm](agents-of-edgewatch-bestiary-items/8RQS62riVfrsiZjr.htm)|Radiant Ray|Radiant Ray|modificada|
|[8rukLsOFemgZkuGO.htm](agents-of-edgewatch-bestiary-items/8rukLsOFemgZkuGO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[8sPdH0UwMRq9RTHB.htm](agents-of-edgewatch-bestiary-items/8sPdH0UwMRq9RTHB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8tU7XeCCuWdlIydb.htm](agents-of-edgewatch-bestiary-items/8tU7XeCCuWdlIydb.htm)|Fist|Puño|modificada|
|[8ubCXo0Jx35U5xxc.htm](agents-of-edgewatch-bestiary-items/8ubCXo0Jx35U5xxc.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[8xAxPvW0rww5NovC.htm](agents-of-edgewatch-bestiary-items/8xAxPvW0rww5NovC.htm)|Greater Warpwave Strike|Golpe de Onda Warp mayor|modificada|
|[90dz4KzJNBG1WfGw.htm](agents-of-edgewatch-bestiary-items/90dz4KzJNBG1WfGw.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[92BXBHbmRG68ifPd.htm](agents-of-edgewatch-bestiary-items/92BXBHbmRG68ifPd.htm)|Amplify Spell|Amplificar Hechizo|modificada|
|[933yJ4bfZhr9zTIX.htm](agents-of-edgewatch-bestiary-items/933yJ4bfZhr9zTIX.htm)|Collapse|Colapso|modificada|
|[95kvqPPSJNio8vAK.htm](agents-of-edgewatch-bestiary-items/95kvqPPSJNio8vAK.htm)|Berserk|Bersérker|modificada|
|[97KS2uPGBbRl3fOQ.htm](agents-of-edgewatch-bestiary-items/97KS2uPGBbRl3fOQ.htm)|Engulf|Envolver|modificada|
|[9Af2sRyOtaTDufyy.htm](agents-of-edgewatch-bestiary-items/9Af2sRyOtaTDufyy.htm)|Maul|Zarpazo doble|modificada|
|[9cRBZZnKohlEmRzR.htm](agents-of-edgewatch-bestiary-items/9cRBZZnKohlEmRzR.htm)|Dagger|Daga|modificada|
|[9duTJhK5paVZrMkr.htm](agents-of-edgewatch-bestiary-items/9duTJhK5paVZrMkr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9dxZ2UN0t2f32r0f.htm](agents-of-edgewatch-bestiary-items/9dxZ2UN0t2f32r0f.htm)|Bury in Offal|Bury in Offal|modificada|
|[9fvVPY7pFKhpLQwU.htm](agents-of-edgewatch-bestiary-items/9fvVPY7pFKhpLQwU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[9gEiH150yl4bKC6n.htm](agents-of-edgewatch-bestiary-items/9gEiH150yl4bKC6n.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[9gGdxZjwIsTVIuxu.htm](agents-of-edgewatch-bestiary-items/9gGdxZjwIsTVIuxu.htm)|Alchemical Formulas (5th)|Fórmulas Alquímicas (5ª)|modificada|
|[9HOzCgOAKeHP5EhB.htm](agents-of-edgewatch-bestiary-items/9HOzCgOAKeHP5EhB.htm)|Venom Explosion|Explosión de Veneno|modificada|
|[9hvKp9EJbq6Log0q.htm](agents-of-edgewatch-bestiary-items/9hvKp9EJbq6Log0q.htm)|Consume Flesh|Consumir carne|modificada|
|[9jJY47nOXFMztxEo.htm](agents-of-edgewatch-bestiary-items/9jJY47nOXFMztxEo.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[9KEf9z8ZVsw7qXJg.htm](agents-of-edgewatch-bestiary-items/9KEf9z8ZVsw7qXJg.htm)|Bloody Jab|Bloody Jab|modificada|
|[9LK5ORluncHekkZ5.htm](agents-of-edgewatch-bestiary-items/9LK5ORluncHekkZ5.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[9m6cGcYmQptppNHf.htm](agents-of-edgewatch-bestiary-items/9m6cGcYmQptppNHf.htm)|Change Shape|Change Shape|modificada|
|[9n6E9GRbgNOMXBmN.htm](agents-of-edgewatch-bestiary-items/9n6E9GRbgNOMXBmN.htm)|Grab|Agarrado|modificada|
|[9oInACsH1lSDnaHW.htm](agents-of-edgewatch-bestiary-items/9oInACsH1lSDnaHW.htm)|Spinning Blade|Spinning Blade|modificada|
|[9ShSRecAk97rJkLS.htm](agents-of-edgewatch-bestiary-items/9ShSRecAk97rJkLS.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[9sMCsySJHETUYLfH.htm](agents-of-edgewatch-bestiary-items/9sMCsySJHETUYLfH.htm)|Staff|Báculo|modificada|
|[9WqBdZ30eNWIpXaC.htm](agents-of-edgewatch-bestiary-items/9WqBdZ30eNWIpXaC.htm)|Fangs|Colmillos|modificada|
|[A04N1HzBOQCcYnM2.htm](agents-of-edgewatch-bestiary-items/A04N1HzBOQCcYnM2.htm)|Blackfinger Blight|Blackfinger Blight|modificada|
|[a1Es9ETdZdfagQRX.htm](agents-of-edgewatch-bestiary-items/a1Es9ETdZdfagQRX.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[A2LL8i2v8LsE6Ilp.htm](agents-of-edgewatch-bestiary-items/A2LL8i2v8LsE6Ilp.htm)|Cleaver|Cleaver|modificada|
|[a34FGdPF5cxVbX4J.htm](agents-of-edgewatch-bestiary-items/a34FGdPF5cxVbX4J.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[A4evoVscksRyehl1.htm](agents-of-edgewatch-bestiary-items/A4evoVscksRyehl1.htm)|Greater Planar Rift|Grieta planaria mayor|modificada|
|[A4o54wh2xPLY1V7q.htm](agents-of-edgewatch-bestiary-items/A4o54wh2xPLY1V7q.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[a5d4jfdl2s8CiAsa.htm](agents-of-edgewatch-bestiary-items/a5d4jfdl2s8CiAsa.htm)|Breath of Lies|Aliento de Mentiras|modificada|
|[A6EBzxn9xWiWavNB.htm](agents-of-edgewatch-bestiary-items/A6EBzxn9xWiWavNB.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[a6jNbmkq4m7CmbSA.htm](agents-of-edgewatch-bestiary-items/a6jNbmkq4m7CmbSA.htm)|Lawkeeper|Lawkeeper|modificada|
|[A7VgvFTQFpm1Cc79.htm](agents-of-edgewatch-bestiary-items/A7VgvFTQFpm1Cc79.htm)|Smoke Bomb Launcher|Lanzador de Bomba de humo|modificada|
|[A8r84vdl4zcvpCMg.htm](agents-of-edgewatch-bestiary-items/A8r84vdl4zcvpCMg.htm)|Free Blade|Espada indomable|modificada|
|[aap2jSjnDkxEKZUI.htm](agents-of-edgewatch-bestiary-items/aap2jSjnDkxEKZUI.htm)|Fist|Puño|modificada|
|[abpRv79lZLaukj8M.htm](agents-of-edgewatch-bestiary-items/abpRv79lZLaukj8M.htm)|Warhammer|Warhammer|modificada|
|[aBQGxKqOuBqF1FkF.htm](agents-of-edgewatch-bestiary-items/aBQGxKqOuBqF1FkF.htm)|Double Stab|Doble Puñalada|modificada|
|[ACezcH9GTIcXGlSB.htm](agents-of-edgewatch-bestiary-items/ACezcH9GTIcXGlSB.htm)|Mortal Shell|Caparazón Mortal|modificada|
|[AD2GrziGoOFfzrk7.htm](agents-of-edgewatch-bestiary-items/AD2GrziGoOFfzrk7.htm)|Contingency Plan|Plan de contingencia|modificada|
|[aGg1kw2fRdZFdeew.htm](agents-of-edgewatch-bestiary-items/aGg1kw2fRdZFdeew.htm)|Stone Fist|Puño de Piedra|modificada|
|[AHGv0wFC9jb2bwlQ.htm](agents-of-edgewatch-bestiary-items/AHGv0wFC9jb2bwlQ.htm)|Claw|Garra|modificada|
|[AIJka2Zew5I1e7Lo.htm](agents-of-edgewatch-bestiary-items/AIJka2Zew5I1e7Lo.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[AiOnTNJ8o76YiJze.htm](agents-of-edgewatch-bestiary-items/AiOnTNJ8o76YiJze.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[aJI86W6ShmBc7gPN.htm](agents-of-edgewatch-bestiary-items/aJI86W6ShmBc7gPN.htm)|Entropy Sense (Imprecise) 120 feet|Sentido de la Entropía (Impreciso) 120 pies|modificada|
|[akEAcbm2fRfGZg8v.htm](agents-of-edgewatch-bestiary-items/akEAcbm2fRfGZg8v.htm)|Conjure Swords|Conjurar espadas|modificada|
|[AkqOlXLB6K0JLoqz.htm](agents-of-edgewatch-bestiary-items/AkqOlXLB6K0JLoqz.htm)|Wail of the Betrayed|Lamento del traicionado|modificada|
|[aMNM0X0dVvVRdlZT.htm](agents-of-edgewatch-bestiary-items/aMNM0X0dVvVRdlZT.htm)|Vulnerable to Neutralize Poison|Vulnerable a neutralizar veneno|modificada|
|[ANZcdjjZfZOfuJYL.htm](agents-of-edgewatch-bestiary-items/ANZcdjjZfZOfuJYL.htm)|Jaws|Fauces|modificada|
|[apIdQFYWsMBgRVKk.htm](agents-of-edgewatch-bestiary-items/apIdQFYWsMBgRVKk.htm)|Pincer|Pinza|modificada|
|[apKzqJtSb9Y34OBk.htm](agents-of-edgewatch-bestiary-items/apKzqJtSb9Y34OBk.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[AQMbWGVUpkE6QLbs.htm](agents-of-edgewatch-bestiary-items/AQMbWGVUpkE6QLbs.htm)|Kiss|Kiss|modificada|
|[aqwHyoUR6cj9y6s8.htm](agents-of-edgewatch-bestiary-items/aqwHyoUR6cj9y6s8.htm)|Focused Gaze|Centrar mirada|modificada|
|[aSPT9O0IpzqjiqG0.htm](agents-of-edgewatch-bestiary-items/aSPT9O0IpzqjiqG0.htm)|Seal Room|Seal Room|modificada|
|[aSvIfWvkrhDYQZn3.htm](agents-of-edgewatch-bestiary-items/aSvIfWvkrhDYQZn3.htm)|Regeneration 20 (Deactivated by Fire or Acid)|Regeneración 20 (Desactivado por Fuego o Ácido).|modificada|
|[AsXbDxRSKFmiQ6jb.htm](agents-of-edgewatch-bestiary-items/AsXbDxRSKFmiQ6jb.htm)|Enfeebling Bite|Mordisco debilitante|modificada|
|[aSzrKKXR7hl8ZVhX.htm](agents-of-edgewatch-bestiary-items/aSzrKKXR7hl8ZVhX.htm)|Stone Spike|Púas de piedra|modificada|
|[at6MabtHqhZPINQ3.htm](agents-of-edgewatch-bestiary-items/at6MabtHqhZPINQ3.htm)|Suction|Succión|modificada|
|[AT8OH6P1ZPmkA0CR.htm](agents-of-edgewatch-bestiary-items/AT8OH6P1ZPmkA0CR.htm)|Paralysis|Parálisis|modificada|
|[AuIi30v2cCwEK9Oy.htm](agents-of-edgewatch-bestiary-items/AuIi30v2cCwEK9Oy.htm)|Flog Mercilessly|Azotar sin piedad|modificada|
|[aV20X0Mp1CqH1tqz.htm](agents-of-edgewatch-bestiary-items/aV20X0Mp1CqH1tqz.htm)|Fan of Daggers|Abanico de Dagas|modificada|
|[AV34BEM8xG7BwDYs.htm](agents-of-edgewatch-bestiary-items/AV34BEM8xG7BwDYs.htm)|Crossbow|Ballesta|modificada|
|[Av9NysCBnGty9we0.htm](agents-of-edgewatch-bestiary-items/Av9NysCBnGty9we0.htm)|Furious Wallop|Furious Wallop|modificada|
|[aVFV0hNNlEPetZWI.htm](agents-of-edgewatch-bestiary-items/aVFV0hNNlEPetZWI.htm)|Poisoner's Staff (Major)|Báculo de Envenenador (superior)|modificada|
|[AvJUZJ1PD1HyuHdf.htm](agents-of-edgewatch-bestiary-items/AvJUZJ1PD1HyuHdf.htm)|Crossbow|Ballesta|modificada|
|[aVyixje5UBQb21sX.htm](agents-of-edgewatch-bestiary-items/aVyixje5UBQb21sX.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[awh4If0CSlamWfUO.htm](agents-of-edgewatch-bestiary-items/awh4If0CSlamWfUO.htm)|Improved Grab|Agarrado mejorado|modificada|
|[awO8dArKZVWslq53.htm](agents-of-edgewatch-bestiary-items/awO8dArKZVWslq53.htm)|Claw|Garra|modificada|
|[aZlGemIJeXm9jJEE.htm](agents-of-edgewatch-bestiary-items/aZlGemIJeXm9jJEE.htm)|Knife Thrower|Lanzacuchillos|modificada|
|[aZvRC9a03MwZf7rr.htm](agents-of-edgewatch-bestiary-items/aZvRC9a03MwZf7rr.htm)|Frightening Critical|Frightening Critical|modificada|
|[b0iAXGEWNNI2u5Tg.htm](agents-of-edgewatch-bestiary-items/b0iAXGEWNNI2u5Tg.htm)|War Razor|War Razor|modificada|
|[B0YPC9dAo09oqw5y.htm](agents-of-edgewatch-bestiary-items/B0YPC9dAo09oqw5y.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[b1QZ33BjtQXdUVJj.htm](agents-of-edgewatch-bestiary-items/b1QZ33BjtQXdUVJj.htm)|Cannon Arm|Brazo de cañón|modificada|
|[B3Ppv6QafImenNGu.htm](agents-of-edgewatch-bestiary-items/B3Ppv6QafImenNGu.htm)|Claw|Garra|modificada|
|[B6M6fTBtJwAQDYCs.htm](agents-of-edgewatch-bestiary-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|Swarm Mind|modificada|
|[B8N51wc1bGZzdZ7d.htm](agents-of-edgewatch-bestiary-items/B8N51wc1bGZzdZ7d.htm)|Hateful Gaze|Hateful Gaze|modificada|
|[b8sV2RLnt3RmLO2t.htm](agents-of-edgewatch-bestiary-items/b8sV2RLnt3RmLO2t.htm)|Upward Stab|Puñalada hacia arriba|modificada|
|[b9gVzcE3UM4ixbBV.htm](agents-of-edgewatch-bestiary-items/b9gVzcE3UM4ixbBV.htm)|Tendrils Come Alive|Tendrils Come Alive|modificada|
|[bA6R5JBcAw7StwCO.htm](agents-of-edgewatch-bestiary-items/bA6R5JBcAw7StwCO.htm)|Cane of the Maelstrom|Bastón De La Vorágine|modificada|
|[Ba9gUN2kmGTDwJ7v.htm](agents-of-edgewatch-bestiary-items/Ba9gUN2kmGTDwJ7v.htm)|Bloody Mayhem|Bloody Mayhem|modificada|
|[BasU6Iuc7hRxhoYG.htm](agents-of-edgewatch-bestiary-items/BasU6Iuc7hRxhoYG.htm)|Form Tool|Herramienta de Forma|modificada|
|[BcDHhgCwCxAUCy1q.htm](agents-of-edgewatch-bestiary-items/BcDHhgCwCxAUCy1q.htm)|Jaws|Fauces|modificada|
|[BdWPHFjkOXFGTDgi.htm](agents-of-edgewatch-bestiary-items/BdWPHFjkOXFGTDgi.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[BeG2KYyzwrCfE6KK.htm](agents-of-edgewatch-bestiary-items/BeG2KYyzwrCfE6KK.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[BEoBeAAFDzMQN6GW.htm](agents-of-edgewatch-bestiary-items/BEoBeAAFDzMQN6GW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bfsuL0uIFF1F4Guy.htm](agents-of-edgewatch-bestiary-items/bfsuL0uIFF1F4Guy.htm)|Fangs|Colmillos|modificada|
|[BGk06cTCmFh4ZooN.htm](agents-of-edgewatch-bestiary-items/BGk06cTCmFh4ZooN.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[BHZkmFQkiTD4QBd2.htm](agents-of-edgewatch-bestiary-items/BHZkmFQkiTD4QBd2.htm)|Dagger|Daga|modificada|
|[bi6VcgRWUdub6hl4.htm](agents-of-edgewatch-bestiary-items/bi6VcgRWUdub6hl4.htm)|Subsume|Subsume|modificada|
|[Bi7jDnQux0YzmGpg.htm](agents-of-edgewatch-bestiary-items/Bi7jDnQux0YzmGpg.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[biy9pcVadULFQhYw.htm](agents-of-edgewatch-bestiary-items/biy9pcVadULFQhYw.htm)|Reaper's Lancet Sheath|Reaper's Lancet Sheath|modificada|
|[Blq1uGh1lHrneL8W.htm](agents-of-edgewatch-bestiary-items/Blq1uGh1lHrneL8W.htm)|Constant Spells|Constant Spells|modificada|
|[bLsJ5WlwMamd2QGg.htm](agents-of-edgewatch-bestiary-items/bLsJ5WlwMamd2QGg.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[Bm63scXZG8oZql5K.htm](agents-of-edgewatch-bestiary-items/Bm63scXZG8oZql5K.htm)|Fist|Puño|modificada|
|[bmbqZlQmEmeXm2DS.htm](agents-of-edgewatch-bestiary-items/bmbqZlQmEmeXm2DS.htm)|Treacherous Veil|Velo engaso|modificada|
|[BofxZCq1wekuufql.htm](agents-of-edgewatch-bestiary-items/BofxZCq1wekuufql.htm)|Joro Spider Venom|Joro Spider Venom|modificada|
|[bplHxNRZHOun4UiH.htm](agents-of-edgewatch-bestiary-items/bplHxNRZHOun4UiH.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[bpTCeqf0WRsdxC9t.htm](agents-of-edgewatch-bestiary-items/bpTCeqf0WRsdxC9t.htm)|Venom Stream|Venom Stream|modificada|
|[bqTN2IAncaogzgo9.htm](agents-of-edgewatch-bestiary-items/bqTN2IAncaogzgo9.htm)|Weapon Master|Maestro de armas|modificada|
|[BrOfryWmR4ITLzIV.htm](agents-of-edgewatch-bestiary-items/BrOfryWmR4ITLzIV.htm)|Flame Dart|Dardo de Flamígera|modificada|
|[bSa3Y6h5y9hcXmYa.htm](agents-of-edgewatch-bestiary-items/bSa3Y6h5y9hcXmYa.htm)|Katar|Katar|modificada|
|[BSAdAEHe8VdvYOjg.htm](agents-of-edgewatch-bestiary-items/BSAdAEHe8VdvYOjg.htm)|Bottled Lightning|Rayo Embotellado|modificada|
|[bsbrnl0pxRuL6acz.htm](agents-of-edgewatch-bestiary-items/bsbrnl0pxRuL6acz.htm)|Hose|Manguera|modificada|
|[BSfiT0dLYi9Xp9gb.htm](agents-of-edgewatch-bestiary-items/BSfiT0dLYi9Xp9gb.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[BsQZslLQ0FJcYne3.htm](agents-of-edgewatch-bestiary-items/BsQZslLQ0FJcYne3.htm)|Reshape Reality|Reshape Reality|modificada|
|[BT64pCzbpph5pC4r.htm](agents-of-edgewatch-bestiary-items/BT64pCzbpph5pC4r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BTVYmpw94v3oWTGl.htm](agents-of-edgewatch-bestiary-items/BTVYmpw94v3oWTGl.htm)|Constant Spells|Constant Spells|modificada|
|[bVqJIi6r8cTALSZa.htm](agents-of-edgewatch-bestiary-items/bVqJIi6r8cTALSZa.htm)|Dagger|Daga|modificada|
|[BW9xnDoY4BohVAGZ.htm](agents-of-edgewatch-bestiary-items/BW9xnDoY4BohVAGZ.htm)|Focus Spells|Conjuros de foco|modificada|
|[bWtvDdGa2KvPHNmx.htm](agents-of-edgewatch-bestiary-items/bWtvDdGa2KvPHNmx.htm)|Grab|Agarrado|modificada|
|[Bx3FBCXmpwC6wpPZ.htm](agents-of-edgewatch-bestiary-items/Bx3FBCXmpwC6wpPZ.htm)|Fist|Puño|modificada|
|[byUSs0eF5Ctqs4xY.htm](agents-of-edgewatch-bestiary-items/byUSs0eF5Ctqs4xY.htm)|Sea Hag's Bargain|Trato con la saga marina maléfico|modificada|
|[bZAhdsbYFuZ2syFl.htm](agents-of-edgewatch-bestiary-items/bZAhdsbYFuZ2syFl.htm)|Easy to Call|Fácil de Llamar|modificada|
|[C01m9GnZW7b5Uk9K.htm](agents-of-edgewatch-bestiary-items/C01m9GnZW7b5Uk9K.htm)|Shadow Step|Paso de Sombra|modificada|
|[C2rrPgxtXj2SQdVM.htm](agents-of-edgewatch-bestiary-items/C2rrPgxtXj2SQdVM.htm)|Dagger|Daga|modificada|
|[C5DSJUvGx6T45XYO.htm](agents-of-edgewatch-bestiary-items/C5DSJUvGx6T45XYO.htm)|Attack of Opportunity (Stinger Only)|Ataque de oportunidad (sólo aguijón).|modificada|
|[c6pExwVTDS5Qyrw4.htm](agents-of-edgewatch-bestiary-items/c6pExwVTDS5Qyrw4.htm)|Constant Spells|Constant Spells|modificada|
|[C8jCG9WTW2fP6H0b.htm](agents-of-edgewatch-bestiary-items/C8jCG9WTW2fP6H0b.htm)|Poison Frenzy|Poison Frenzy|modificada|
|[C8jd840fpmYd3wDB.htm](agents-of-edgewatch-bestiary-items/C8jd840fpmYd3wDB.htm)|Suffocating Fumes|Humos Asfixiantes|modificada|
|[CaQuzIFC9BNqHDUQ.htm](agents-of-edgewatch-bestiary-items/CaQuzIFC9BNqHDUQ.htm)|Dagger|Daga|modificada|
|[CBZ2xBopPRbaMVV0.htm](agents-of-edgewatch-bestiary-items/CBZ2xBopPRbaMVV0.htm)|Capsize|Volcar|modificada|
|[CcCVlFjqLYk2DTlc.htm](agents-of-edgewatch-bestiary-items/CcCVlFjqLYk2DTlc.htm)|+1 Status to Will Saves vs. Mental|+1 situación a las salvaciones de voluntad contra mental.|modificada|
|[CCz3eaJWNZAPwqBS.htm](agents-of-edgewatch-bestiary-items/CCz3eaJWNZAPwqBS.htm)|Master Brawler|Maestro Brawler|modificada|
|[cdoPISuZdSbxOwT1.htm](agents-of-edgewatch-bestiary-items/cdoPISuZdSbxOwT1.htm)|Spell Choke|Hechizo Choke|modificada|
|[ce1K4Mc00gFvS1v8.htm](agents-of-edgewatch-bestiary-items/ce1K4Mc00gFvS1v8.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[cerEbvB6QEc8UcuU.htm](agents-of-edgewatch-bestiary-items/cerEbvB6QEc8UcuU.htm)|No Escape|Sin huida posible|modificada|
|[cg0RVhBfO8MxjPpI.htm](agents-of-edgewatch-bestiary-items/cg0RVhBfO8MxjPpI.htm)|Superior Senses|Sentidos Superiores|modificada|
|[CgEMdYYlkqnUPBfB.htm](agents-of-edgewatch-bestiary-items/CgEMdYYlkqnUPBfB.htm)|Perfect Will|Voluntad Perfecta|modificada|
|[chuMKB6cHYONy4NC.htm](agents-of-edgewatch-bestiary-items/chuMKB6cHYONy4NC.htm)|Toxic Gas|Gas Tóxico|modificada|
|[CIh6yDe29eBqfdsD.htm](agents-of-edgewatch-bestiary-items/CIh6yDe29eBqfdsD.htm)|Haphazard Hack|Hack Haphazard|modificada|
|[ciQ62q9OynsChQSL.htm](agents-of-edgewatch-bestiary-items/ciQ62q9OynsChQSL.htm)|Waves of Sorrow|Waves of Sorrow|modificada|
|[CK9zFRMfz5AvtehE.htm](agents-of-edgewatch-bestiary-items/CK9zFRMfz5AvtehE.htm)|Focus Spells|Conjuros de foco|modificada|
|[CkJTSqPlqI5Fj2b7.htm](agents-of-edgewatch-bestiary-items/CkJTSqPlqI5Fj2b7.htm)|Mirror Hand|Mirror Hand|modificada|
|[ckqlqEZcPT6ZKZTJ.htm](agents-of-edgewatch-bestiary-items/ckqlqEZcPT6ZKZTJ.htm)|Club|Club|modificada|
|[cNdgAyzrxvT3Z68j.htm](agents-of-edgewatch-bestiary-items/cNdgAyzrxvT3Z68j.htm)|+2 Status to All Saves vs. Composition Spells|+2 de situación a todas las salvaciones contra conjuros de composición.|modificada|
|[cneQw5areaRAqZhr.htm](agents-of-edgewatch-bestiary-items/cneQw5areaRAqZhr.htm)|Shortsword|Espada corta|modificada|
|[cNLxEiiU06b04C55.htm](agents-of-edgewatch-bestiary-items/cNLxEiiU06b04C55.htm)|Tentacle|Tentáculo|modificada|
|[cny7NT0gNZmXhDv6.htm](agents-of-edgewatch-bestiary-items/cny7NT0gNZmXhDv6.htm)|Tentacle|Tentáculo|modificada|
|[CpdC4SJqhh9UFhSi.htm](agents-of-edgewatch-bestiary-items/CpdC4SJqhh9UFhSi.htm)|Jaws|Fauces|modificada|
|[cSFNzQ52byIdvdWb.htm](agents-of-edgewatch-bestiary-items/cSFNzQ52byIdvdWb.htm)|Broken Quills|Plumas Rotas|modificada|
|[Ct2gfJE7OAo8ZR44.htm](agents-of-edgewatch-bestiary-items/Ct2gfJE7OAo8ZR44.htm)|Constant Spells|Constant Spells|modificada|
|[Ctkj7r3YYAFSQEKx.htm](agents-of-edgewatch-bestiary-items/Ctkj7r3YYAFSQEKx.htm)|Disgorge Portal|Disgorge Portal|modificada|
|[CU5CXS5c5trwAd9g.htm](agents-of-edgewatch-bestiary-items/CU5CXS5c5trwAd9g.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[CUtiSdjK9OwxIfKR.htm](agents-of-edgewatch-bestiary-items/CUtiSdjK9OwxIfKR.htm)|Shatter Defenses|Destrozar defensas|modificada|
|[CUYM17Clq1YTEBxz.htm](agents-of-edgewatch-bestiary-items/CUYM17Clq1YTEBxz.htm)|Maddening Whispers|Maddening Whispers|modificada|
|[cvCrwykv9kKHfIwG.htm](agents-of-edgewatch-bestiary-items/cvCrwykv9kKHfIwG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CVEw8LgJymgw5uI2.htm](agents-of-edgewatch-bestiary-items/CVEw8LgJymgw5uI2.htm)|Rend|Rasgadura|modificada|
|[CVTZnv3ZcE4LbDPg.htm](agents-of-edgewatch-bestiary-items/CVTZnv3ZcE4LbDPg.htm)|Breath Weapon|Breath Weapon|modificada|
|[cW925U5mT8WZIcql.htm](agents-of-edgewatch-bestiary-items/cW925U5mT8WZIcql.htm)|Shortbow|Arco corto|modificada|
|[CWcYZHN9MSfxTR2k.htm](agents-of-edgewatch-bestiary-items/CWcYZHN9MSfxTR2k.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[CwNHaEk7qrMHHyza.htm](agents-of-edgewatch-bestiary-items/CwNHaEk7qrMHHyza.htm)|Constant Spells|Constant Spells|modificada|
|[CWOEh5RnFG5cysRk.htm](agents-of-edgewatch-bestiary-items/CWOEh5RnFG5cysRk.htm)|Tentacle Trip|Derribar Tentáculo|modificada|
|[cX2EH9YzIFFgfGra.htm](agents-of-edgewatch-bestiary-items/cX2EH9YzIFFgfGra.htm)|Dagger|Daga|modificada|
|[cXt1NhRBZEXIv1ju.htm](agents-of-edgewatch-bestiary-items/cXt1NhRBZEXIv1ju.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[cYpzRpaINHeW2XxC.htm](agents-of-edgewatch-bestiary-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|Arma envenenada|modificada|
|[czNwyIcrJKwQNfoZ.htm](agents-of-edgewatch-bestiary-items/czNwyIcrJKwQNfoZ.htm)|Constant Spells|Constant Spells|modificada|
|[D27lYuRNmEYB5vjo.htm](agents-of-edgewatch-bestiary-items/D27lYuRNmEYB5vjo.htm)|Wipe Away Cracks|Wipe Away Cracks|modificada|
|[DBzG2AKUenn28L2w.htm](agents-of-edgewatch-bestiary-items/DBzG2AKUenn28L2w.htm)|War Razor|War Razor|modificada|
|[dcDZ5fYx6PsHPo4t.htm](agents-of-edgewatch-bestiary-items/dcDZ5fYx6PsHPo4t.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[DdFilDnuHDObT81V.htm](agents-of-edgewatch-bestiary-items/DdFilDnuHDObT81V.htm)|Freeze Floor|Congelar Suelo|modificada|
|[dDvj6Zila83uSsch.htm](agents-of-edgewatch-bestiary-items/dDvj6Zila83uSsch.htm)|Flying Blade|Hoja Voladora|modificada|
|[deQl75TOphyii6YU.htm](agents-of-edgewatch-bestiary-items/deQl75TOphyii6YU.htm)|Planar Coven|Planar Coven|modificada|
|[DF3OKBUIdabo7i9j.htm](agents-of-edgewatch-bestiary-items/DF3OKBUIdabo7i9j.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[dff7pIEmGTfCShQo.htm](agents-of-edgewatch-bestiary-items/dff7pIEmGTfCShQo.htm)|Hardness 5|Dureza 5|modificada|
|[dffHcWQsvtoeLOID.htm](agents-of-edgewatch-bestiary-items/dffHcWQsvtoeLOID.htm)|Divine Rituals|Rituales Divinos|modificada|
|[dfm82amOgBxSgH3m.htm](agents-of-edgewatch-bestiary-items/dfm82amOgBxSgH3m.htm)|Longspear|Longspear|modificada|
|[DGsILrvl3LJIeZIu.htm](agents-of-edgewatch-bestiary-items/DGsILrvl3LJIeZIu.htm)|Enshroud|Enshroud|modificada|
|[Dh4LCrV41VJjACPD.htm](agents-of-edgewatch-bestiary-items/Dh4LCrV41VJjACPD.htm)|Arcane Cannon|Arcane Cannon|modificada|
|[dhD7eewlB6LvvS2M.htm](agents-of-edgewatch-bestiary-items/dhD7eewlB6LvvS2M.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dhPK5EdGfI5j8Yse.htm](agents-of-edgewatch-bestiary-items/dhPK5EdGfI5j8Yse.htm)|Thunk 'n' Slice|Thunk 'n' Slice|modificada|
|[DJUjRfb1YyWOvrSi.htm](agents-of-edgewatch-bestiary-items/DJUjRfb1YyWOvrSi.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[DLJfYIHzmCQGAW0s.htm](agents-of-edgewatch-bestiary-items/DLJfYIHzmCQGAW0s.htm)|Rabbit Punch|Rabbit Punch|modificada|
|[Dm6lDovIDGIcISuP.htm](agents-of-edgewatch-bestiary-items/Dm6lDovIDGIcISuP.htm)|Magnetize the Living|Magnetizar a los vivos|modificada|
|[DNsLOiJgbt3epj8c.htm](agents-of-edgewatch-bestiary-items/DNsLOiJgbt3epj8c.htm)|Mob Rush|Mob Embestida|modificada|
|[DNVNuZGCllMw1hmA.htm](agents-of-edgewatch-bestiary-items/DNVNuZGCllMw1hmA.htm)|Drowning Grasp|Drowning Grasp|modificada|
|[DOYd8fquer3sIStG.htm](agents-of-edgewatch-bestiary-items/DOYd8fquer3sIStG.htm)|Entropic Feedback|Retroalimentación entrópica|modificada|
|[dPoLgUKZRxlWnRba.htm](agents-of-edgewatch-bestiary-items/dPoLgUKZRxlWnRba.htm)|Swallow Whole|Engullir Todo|modificada|
|[DREnRy1SAHY6dCiU.htm](agents-of-edgewatch-bestiary-items/DREnRy1SAHY6dCiU.htm)|Alchemical Formulas (10th)|Alchemical Formulas (10th)|modificada|
|[drfXFnc49cl52C2X.htm](agents-of-edgewatch-bestiary-items/drfXFnc49cl52C2X.htm)|Foot|Pie|modificada|
|[DrsLgW5tbXlYEqJ1.htm](agents-of-edgewatch-bestiary-items/DrsLgW5tbXlYEqJ1.htm)|Alien Presence|Alien Presence|modificada|
|[dRVd3bgtgiGOetEC.htm](agents-of-edgewatch-bestiary-items/dRVd3bgtgiGOetEC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DSVJsEHiWFPACcNX.htm](agents-of-edgewatch-bestiary-items/DSVJsEHiWFPACcNX.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[dwuIf4lpimWqI6eV.htm](agents-of-edgewatch-bestiary-items/dwuIf4lpimWqI6eV.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Dz4rpuIMNqJy4V90.htm](agents-of-edgewatch-bestiary-items/Dz4rpuIMNqJy4V90.htm)|Inky Imitator|Inky Imitator|modificada|
|[E0iLnf03NhzlNZTT.htm](agents-of-edgewatch-bestiary-items/E0iLnf03NhzlNZTT.htm)|Moderate Alchemist's Fire|Fuego de alquimista moderado|modificada|
|[E0zQqvJo9nRAZs7T.htm](agents-of-edgewatch-bestiary-items/E0zQqvJo9nRAZs7T.htm)|Blowtorch|Soplete|modificada|
|[E3ItzWH7n7tJ4AB0.htm](agents-of-edgewatch-bestiary-items/E3ItzWH7n7tJ4AB0.htm)|Entropy Sense (Imprecise) 60 feet|Sentido de Entropía (Impreciso) 60 pies|modificada|
|[e5D3G73TDu6XWskj.htm](agents-of-edgewatch-bestiary-items/e5D3G73TDu6XWskj.htm)|Twisting Reach|Alcance Retorcido|modificada|
|[E5E4EFfOXmYDktze.htm](agents-of-edgewatch-bestiary-items/E5E4EFfOXmYDktze.htm)|War Razor|War Razor|modificada|
|[E85UiB4OeqzsCKWf.htm](agents-of-edgewatch-bestiary-items/E85UiB4OeqzsCKWf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[E9Cr9P2L3cp3NzRD.htm](agents-of-edgewatch-bestiary-items/E9Cr9P2L3cp3NzRD.htm)|Aura of Angry Crystals|Aura de Cristales Furiosos|modificada|
|[EaAkzJuaOgph4evs.htm](agents-of-edgewatch-bestiary-items/EaAkzJuaOgph4evs.htm)|Jaws|Fauces|modificada|
|[EaBeGYjdT1VVqvT6.htm](agents-of-edgewatch-bestiary-items/EaBeGYjdT1VVqvT6.htm)|Split|Split|modificada|
|[eaCjNNNqsgrBB55K.htm](agents-of-edgewatch-bestiary-items/eaCjNNNqsgrBB55K.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[EbIhMOd0cHtkhNcM.htm](agents-of-edgewatch-bestiary-items/EbIhMOd0cHtkhNcM.htm)|Zealous Restoration|Restablecimiento Celoso|modificada|
|[EbuMoQ5idyQJDEXD.htm](agents-of-edgewatch-bestiary-items/EbuMoQ5idyQJDEXD.htm)|Paralyzing Gaze|Mirada paralizante|modificada|
|[ecUXFzmWTDeBzbIu.htm](agents-of-edgewatch-bestiary-items/ecUXFzmWTDeBzbIu.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ED7Sb3O9SLxFnTRr.htm](agents-of-edgewatch-bestiary-items/ED7Sb3O9SLxFnTRr.htm)|Unholy Greatsword|Espada Grande Profana|modificada|
|[EEMf91Mnk5n3dYup.htm](agents-of-edgewatch-bestiary-items/EEMf91Mnk5n3dYup.htm)|Frantic Grasp|Frantic Grasp|modificada|
|[EF6rT8DI1QtqCDmn.htm](agents-of-edgewatch-bestiary-items/EF6rT8DI1QtqCDmn.htm)|Improved Grab|Agarrado mejorado|modificada|
|[EFGYVjmnPR7oeSdb.htm](agents-of-edgewatch-bestiary-items/EFGYVjmnPR7oeSdb.htm)|Spear|Lanza|modificada|
|[efKlG6Tj9bAd6SQ9.htm](agents-of-edgewatch-bestiary-items/efKlG6Tj9bAd6SQ9.htm)|Treasure Sense (Imprecise) 30 feet|Sentido del Tesoro (Impreciso) 30 pies|modificada|
|[EGOkkUlxQlgkV8Ao.htm](agents-of-edgewatch-bestiary-items/EGOkkUlxQlgkV8Ao.htm)|Dagger|Daga|modificada|
|[eGuMBSFkjeJsBOLy.htm](agents-of-edgewatch-bestiary-items/eGuMBSFkjeJsBOLy.htm)|Fangs|Colmillos|modificada|
|[eIDlmbUtMfTc108y.htm](agents-of-edgewatch-bestiary-items/eIDlmbUtMfTc108y.htm)|Throw Shadow Blade|Lanza Hoja de Sombra|modificada|
|[EjUqgzIjC8XDdB6h.htm](agents-of-edgewatch-bestiary-items/EjUqgzIjC8XDdB6h.htm)|Dagger|Daga|modificada|
|[eJY1bfWIhxuD4h0H.htm](agents-of-edgewatch-bestiary-items/eJY1bfWIhxuD4h0H.htm)|Delay Condition|Estado negativo de retardo (normalmente se abrevia a estado).|modificada|
|[ekdPtlR0pjDVDM4T.htm](agents-of-edgewatch-bestiary-items/ekdPtlR0pjDVDM4T.htm)|Go for the Eyes|Go for the Eyes|modificada|
|[EKp5irpqgnTwEyjX.htm](agents-of-edgewatch-bestiary-items/EKp5irpqgnTwEyjX.htm)|Backdrop|Telón de fondo|modificada|
|[EKw9z5qbMiwdPSwH.htm](agents-of-edgewatch-bestiary-items/EKw9z5qbMiwdPSwH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ELoLmxNU0ywpt3HM.htm](agents-of-edgewatch-bestiary-items/ELoLmxNU0ywpt3HM.htm)|Stone Spike|Púas de piedra|modificada|
|[ElQzIcZqeoNNQayH.htm](agents-of-edgewatch-bestiary-items/ElQzIcZqeoNNQayH.htm)|Dagger|Daga|modificada|
|[emBPB2m0d0BQpI2N.htm](agents-of-edgewatch-bestiary-items/emBPB2m0d0BQpI2N.htm)|Leaching Glare|Leaching Glare|modificada|
|[EMKkZjLFHbAYJztn.htm](agents-of-edgewatch-bestiary-items/EMKkZjLFHbAYJztn.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[eosKGTfGo3nA3EVl.htm](agents-of-edgewatch-bestiary-items/eosKGTfGo3nA3EVl.htm)|Recalibrate|Recalibrar|modificada|
|[ePfqkUvCF1uDrS24.htm](agents-of-edgewatch-bestiary-items/ePfqkUvCF1uDrS24.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ePXkUAkuSGauQ7Jv.htm](agents-of-edgewatch-bestiary-items/ePXkUAkuSGauQ7Jv.htm)|Supersonic Hearing|Oído supersónico|modificada|
|[Eq6IkJnS6klg5Kqj.htm](agents-of-edgewatch-bestiary-items/Eq6IkJnS6klg5Kqj.htm)|Target Culprit|Objetivo Culpable|modificada|
|[er6I32kkHdLAxCXI.htm](agents-of-edgewatch-bestiary-items/er6I32kkHdLAxCXI.htm)|Stamper|Stamper|modificada|
|[EtuysfUGpQnBFAaU.htm](agents-of-edgewatch-bestiary-items/EtuysfUGpQnBFAaU.htm)|Rapier|Estoque|modificada|
|[eTWouqSZqarzUigD.htm](agents-of-edgewatch-bestiary-items/eTWouqSZqarzUigD.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[eVjcGt7GJnLCOU3R.htm](agents-of-edgewatch-bestiary-items/eVjcGt7GJnLCOU3R.htm)|Constrict|Restringir|modificada|
|[EWsYBE6UDRYE7p46.htm](agents-of-edgewatch-bestiary-items/EWsYBE6UDRYE7p46.htm)|Skitterstitch Venom|Veneno Skitterstitch|modificada|
|[EXg9UirCZfna0Tsv.htm](agents-of-edgewatch-bestiary-items/EXg9UirCZfna0Tsv.htm)|Hook Claw|Garra Gancho|modificada|
|[EyiD5AxylSuM4vcO.htm](agents-of-edgewatch-bestiary-items/EyiD5AxylSuM4vcO.htm)|Dagger|Daga|modificada|
|[eYUQ4i2nFz1iBeTO.htm](agents-of-edgewatch-bestiary-items/eYUQ4i2nFz1iBeTO.htm)|Grab|Agarrado|modificada|
|[eyUSuPWwgWZeCeA7.htm](agents-of-edgewatch-bestiary-items/eyUSuPWwgWZeCeA7.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Ez5LQ3xID56lShet.htm](agents-of-edgewatch-bestiary-items/Ez5LQ3xID56lShet.htm)|Infector|Infector|modificada|
|[f0fmwIGLi4szm1lO.htm](agents-of-edgewatch-bestiary-items/f0fmwIGLi4szm1lO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[F0wNdQAwukZzTRRS.htm](agents-of-edgewatch-bestiary-items/F0wNdQAwukZzTRRS.htm)|Alchemical Torrent|Torrente Alquímico|modificada|
|[f3cXxnZGDx1KqvCZ.htm](agents-of-edgewatch-bestiary-items/f3cXxnZGDx1KqvCZ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[f52vv2ucD59hRBEO.htm](agents-of-edgewatch-bestiary-items/f52vv2ucD59hRBEO.htm)|Constrict|Restringir|modificada|
|[f6aAjLfElEhbsLCC.htm](agents-of-edgewatch-bestiary-items/f6aAjLfElEhbsLCC.htm)|Dirty Bomb|Dirty Bomb|modificada|
|[f9SGRyWKHiuofcI9.htm](agents-of-edgewatch-bestiary-items/f9SGRyWKHiuofcI9.htm)|Ravenous Jaws|Mandíbulas voraces|modificada|
|[fBWMN8Yh6mm05krO.htm](agents-of-edgewatch-bestiary-items/fBWMN8Yh6mm05krO.htm)|First Step|Primer Paso|modificada|
|[fC6dc9AdYYKBeuWn.htm](agents-of-edgewatch-bestiary-items/fC6dc9AdYYKBeuWn.htm)|Daemonic Pledge|Juramento daimónico|modificada|
|[FcWnKeqz5Ffy5vxu.htm](agents-of-edgewatch-bestiary-items/FcWnKeqz5Ffy5vxu.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[fgprZ9vs1zfRUz3o.htm](agents-of-edgewatch-bestiary-items/fgprZ9vs1zfRUz3o.htm)|Dispel Vulnerability|Disipar Vulnerabilidad|modificada|
|[FH3nsGdSWs8rbUYr.htm](agents-of-edgewatch-bestiary-items/FH3nsGdSWs8rbUYr.htm)|Poisonous Cloud|Nube Venenosa|modificada|
|[FHYZijWKqxyPWq4b.htm](agents-of-edgewatch-bestiary-items/FHYZijWKqxyPWq4b.htm)|Construct Armor|Construir Armadura|modificada|
|[fJd0mVVeFIuCWuEo.htm](agents-of-edgewatch-bestiary-items/fJd0mVVeFIuCWuEo.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[fJjXPvTiys4MFIsw.htm](agents-of-edgewatch-bestiary-items/fJjXPvTiys4MFIsw.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[FK02FgrkiyUcMuJp.htm](agents-of-edgewatch-bestiary-items/FK02FgrkiyUcMuJp.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[fkNlQGLkTlnATC6Y.htm](agents-of-edgewatch-bestiary-items/fkNlQGLkTlnATC6Y.htm)|Swallow Whole|Engullir Todo|modificada|
|[fkyMMwCkFLHVH5BV.htm](agents-of-edgewatch-bestiary-items/fkyMMwCkFLHVH5BV.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[Fm6lAJIPw6ObpzHZ.htm](agents-of-edgewatch-bestiary-items/Fm6lAJIPw6ObpzHZ.htm)|Pull Under|Pull Under|modificada|
|[FMCAwZcfOhS476dW.htm](agents-of-edgewatch-bestiary-items/FMCAwZcfOhS476dW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fMk2JkXosTd2BjxC.htm](agents-of-edgewatch-bestiary-items/fMk2JkXosTd2BjxC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fmyoJEwSeBRrb9Ik.htm](agents-of-edgewatch-bestiary-items/fmyoJEwSeBRrb9Ik.htm)|+1 Status Bonus on Saves vs. Divine Magic|Bonificador de situación +1 a las salvaciones contra magia divina.|modificada|
|[fPhaooKcxBRh5Bsx.htm](agents-of-edgewatch-bestiary-items/fPhaooKcxBRh5Bsx.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[FqjfY1OlrUKVd4mw.htm](agents-of-edgewatch-bestiary-items/FqjfY1OlrUKVd4mw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fqPyHyqdqW4NMFk3.htm](agents-of-edgewatch-bestiary-items/fqPyHyqdqW4NMFk3.htm)|Infused Items|Equipos infundidos|modificada|
|[fRDoPX53siUmNijE.htm](agents-of-edgewatch-bestiary-items/fRDoPX53siUmNijE.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[fRnsLPos2xrH1GzQ.htm](agents-of-edgewatch-bestiary-items/fRnsLPos2xrH1GzQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FsASThbyptq4QfWi.htm](agents-of-edgewatch-bestiary-items/FsASThbyptq4QfWi.htm)|Trample|Trample|modificada|
|[fSTbX2DA6uu9MkuN.htm](agents-of-edgewatch-bestiary-items/fSTbX2DA6uu9MkuN.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[fu5LQi1ag0KXxEby.htm](agents-of-edgewatch-bestiary-items/fu5LQi1ag0KXxEby.htm)|Prickly Defense|Defensa espinosa|modificada|
|[FvZvIio9Hz2dvltI.htm](agents-of-edgewatch-bestiary-items/FvZvIio9Hz2dvltI.htm)|Duck and Weave|Duck and Weave|modificada|
|[FXTugA9GorPxMpXh.htm](agents-of-edgewatch-bestiary-items/FXTugA9GorPxMpXh.htm)|Designate Apostate|Designate Apostate|modificada|
|[FY9YGIGBEeh0FNKi.htm](agents-of-edgewatch-bestiary-items/FY9YGIGBEeh0FNKi.htm)|Mother Venom Poison|Veneno Madre Veneno|modificada|
|[G406qwAGsa4C7jmR.htm](agents-of-edgewatch-bestiary-items/G406qwAGsa4C7jmR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[g407ooIFn82B2iXk.htm](agents-of-edgewatch-bestiary-items/g407ooIFn82B2iXk.htm)|Claw|Garra|modificada|
|[g67xAp632gfQapHl.htm](agents-of-edgewatch-bestiary-items/g67xAp632gfQapHl.htm)|Overwhelming Anguish|Angustia Abrumadora|modificada|
|[g8bRqHNpfEqsTYyx.htm](agents-of-edgewatch-bestiary-items/g8bRqHNpfEqsTYyx.htm)|Internal Spell Strike|Golpe de Hechizo Interno|modificada|
|[G8ORuPOn8J7KCfJc.htm](agents-of-edgewatch-bestiary-items/G8ORuPOn8J7KCfJc.htm)|Prepared Divine Spells|Hechizos divinos preparados.|modificada|
|[gCFv5lyk3Wfoe8GB.htm](agents-of-edgewatch-bestiary-items/gCFv5lyk3Wfoe8GB.htm)|Javelin|Javelin|modificada|
|[gczvNivaLBoVgpla.htm](agents-of-edgewatch-bestiary-items/gczvNivaLBoVgpla.htm)|Mirror Jump|Salto Espejo|modificada|
|[GElFkGSpAoSzMnqX.htm](agents-of-edgewatch-bestiary-items/GElFkGSpAoSzMnqX.htm)|Push|Push|modificada|
|[GGJBUd4JmwxnZtYj.htm](agents-of-edgewatch-bestiary-items/GGJBUd4JmwxnZtYj.htm)|Pincer|Pinza|modificada|
|[gi71u7E3lwMd8bQT.htm](agents-of-edgewatch-bestiary-items/gi71u7E3lwMd8bQT.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[GjE2VyfYPjLsNFGi.htm](agents-of-edgewatch-bestiary-items/GjE2VyfYPjLsNFGi.htm)|Jaws|Fauces|modificada|
|[GJh2y3nd5dKEWe8I.htm](agents-of-edgewatch-bestiary-items/GJh2y3nd5dKEWe8I.htm)|Displacement|Desplazamiento|modificada|
|[glyWTuwYyEk8AaC1.htm](agents-of-edgewatch-bestiary-items/glyWTuwYyEk8AaC1.htm)|Dubious Shifting|Cambiante dudoso|modificada|
|[gMEgsUUpGodwiTBG.htm](agents-of-edgewatch-bestiary-items/gMEgsUUpGodwiTBG.htm)|Club|Club|modificada|
|[gMURrHnMDkhqOaJp.htm](agents-of-edgewatch-bestiary-items/gMURrHnMDkhqOaJp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[GneEI6sPR4fF7SgD.htm](agents-of-edgewatch-bestiary-items/GneEI6sPR4fF7SgD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[GnobVyc3rnNPsKdE.htm](agents-of-edgewatch-bestiary-items/GnobVyc3rnNPsKdE.htm)|Grab|Agarrado|modificada|
|[GnvMkeIBg6ZpYXzJ.htm](agents-of-edgewatch-bestiary-items/GnvMkeIBg6ZpYXzJ.htm)|Pseudopod Burst|Pseudopod Burst|modificada|
|[gPHdCqQsilHM5yrV.htm](agents-of-edgewatch-bestiary-items/gPHdCqQsilHM5yrV.htm)|Improved Grab|Agarrado mejorado|modificada|
|[gpQwZhqK70OriQ2S.htm](agents-of-edgewatch-bestiary-items/gpQwZhqK70OriQ2S.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Gqv7Fk6y6KrpOB3W.htm](agents-of-edgewatch-bestiary-items/Gqv7Fk6y6KrpOB3W.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[GrbNOULOdg2VBphU.htm](agents-of-edgewatch-bestiary-items/GrbNOULOdg2VBphU.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[gTlSQCqmrYTzoJgt.htm](agents-of-edgewatch-bestiary-items/gTlSQCqmrYTzoJgt.htm)|Jaws|Fauces|modificada|
|[GtoDWNmX34925xMc.htm](agents-of-edgewatch-bestiary-items/GtoDWNmX34925xMc.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[GTs7sVH9yhH8mKkM.htm](agents-of-edgewatch-bestiary-items/GTs7sVH9yhH8mKkM.htm)|Nonlethal Training|Entrenamiento No letal|modificada|
|[GtVYqqSXE71FA0xR.htm](agents-of-edgewatch-bestiary-items/GtVYqqSXE71FA0xR.htm)|Thunderstone|Thunderstone|modificada|
|[GUrHLtuqnl4x3KHH.htm](agents-of-edgewatch-bestiary-items/GUrHLtuqnl4x3KHH.htm)|Burst of Speed|Ráfaga de Velocidad|modificada|
|[GutcPWbxQjMoihFA.htm](agents-of-edgewatch-bestiary-items/GutcPWbxQjMoihFA.htm)|Water Step|Paso acuático|modificada|
|[Gv8gIlACS3hfoYJF.htm](agents-of-edgewatch-bestiary-items/Gv8gIlACS3hfoYJF.htm)|Greater Holy Blade|Filo sagrado mayor.|modificada|
|[GVWSny9n33dg608R.htm](agents-of-edgewatch-bestiary-items/GVWSny9n33dg608R.htm)|Dagger|Daga|modificada|
|[Gy4RoLuOaQU79ESu.htm](agents-of-edgewatch-bestiary-items/Gy4RoLuOaQU79ESu.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[gzMwqFLG38aJHUIa.htm](agents-of-edgewatch-bestiary-items/gzMwqFLG38aJHUIa.htm)|Purple Worm Venom|Veneno de gusano púrpura|modificada|
|[gZWXLF5Vb3kNdm0I.htm](agents-of-edgewatch-bestiary-items/gZWXLF5Vb3kNdm0I.htm)|Focus Spells|Conjuros de foco|modificada|
|[H1Aapl7br22RF2Pe.htm](agents-of-edgewatch-bestiary-items/H1Aapl7br22RF2Pe.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[H2cjvx6x95qaiQFo.htm](agents-of-edgewatch-bestiary-items/H2cjvx6x95qaiQFo.htm)|Marrow Rot|Pudrición de médula|modificada|
|[h8NOsLMsLkhSoPDm.htm](agents-of-edgewatch-bestiary-items/h8NOsLMsLkhSoPDm.htm)|Change Shape|Change Shape|modificada|
|[haax0MkiNqA6MN2L.htm](agents-of-edgewatch-bestiary-items/haax0MkiNqA6MN2L.htm)|Rallying Call|Llamada de reunión|modificada|
|[HaPAHwncLeNsBABj.htm](agents-of-edgewatch-bestiary-items/HaPAHwncLeNsBABj.htm)|Acid Spit|Acid Spit|modificada|
|[HAvR3sxXnbRxAmUE.htm](agents-of-edgewatch-bestiary-items/HAvR3sxXnbRxAmUE.htm)|Claw|Garra|modificada|
|[hbg1FrfQIQ6EAWuz.htm](agents-of-edgewatch-bestiary-items/hbg1FrfQIQ6EAWuz.htm)|Opportune Backstab|Pulada trapera oportuna|modificada|
|[HCuDpfMGf5F5xT6L.htm](agents-of-edgewatch-bestiary-items/HCuDpfMGf5F5xT6L.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[hDbNl3kiEPn8AUos.htm](agents-of-edgewatch-bestiary-items/hDbNl3kiEPn8AUos.htm)|Capsize|Volcar|modificada|
|[HeE0E8xiY1o4FYiX.htm](agents-of-edgewatch-bestiary-items/HeE0E8xiY1o4FYiX.htm)|Dagger|Daga|modificada|
|[HffwSLkW7mUAJAlB.htm](agents-of-edgewatch-bestiary-items/HffwSLkW7mUAJAlB.htm)|Wing|Ala|modificada|
|[HGIf9bu9E1RjDKaF.htm](agents-of-edgewatch-bestiary-items/HGIf9bu9E1RjDKaF.htm)|Dominate|Dominar|modificada|
|[HhajioTT4qQe67v3.htm](agents-of-edgewatch-bestiary-items/HhajioTT4qQe67v3.htm)|Improved Grab|Agarrado mejorado|modificada|
|[HHDLYWqh0LHr49Uu.htm](agents-of-edgewatch-bestiary-items/HHDLYWqh0LHr49Uu.htm)|Stitch Skin|Stitch Skin|modificada|
|[hhg7o2qV6JntAswy.htm](agents-of-edgewatch-bestiary-items/hhg7o2qV6JntAswy.htm)|+2 Status Bonus on Saves vs. Trip|Bonificador +2 de situación en las salvaciones contra Derribar.|modificada|
|[HIbGLn4unRGk8EZL.htm](agents-of-edgewatch-bestiary-items/HIbGLn4unRGk8EZL.htm)|Dart Volley|Volea de dardos|modificada|
|[hJJc012AkwlepsfP.htm](agents-of-edgewatch-bestiary-items/hJJc012AkwlepsfP.htm)|Odorless|Inodoro|modificada|
|[HjL44xV6BvZNBNH8.htm](agents-of-edgewatch-bestiary-items/HjL44xV6BvZNBNH8.htm)|Frost Vial|Gélida de frasco de escarcha|modificada|
|[HkUjt1e6CiwdfHoy.htm](agents-of-edgewatch-bestiary-items/HkUjt1e6CiwdfHoy.htm)|Breath Weapon|Breath Weapon|modificada|
|[HLhPjN1be3uh5emL.htm](agents-of-edgewatch-bestiary-items/HLhPjN1be3uh5emL.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[HLLn9CHqmRrWk4IN.htm](agents-of-edgewatch-bestiary-items/HLLn9CHqmRrWk4IN.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[HMCS1B66LSvBKspC.htm](agents-of-edgewatch-bestiary-items/HMCS1B66LSvBKspC.htm)|Splatter|Splatter|modificada|
|[hnspRHRc7JzzGcNY.htm](agents-of-edgewatch-bestiary-items/hnspRHRc7JzzGcNY.htm)|Knockdown|Derribo|modificada|
|[HNYjS5WAb54frbpw.htm](agents-of-edgewatch-bestiary-items/HNYjS5WAb54frbpw.htm)|Charming Liar|Mentiroso encantador|modificada|
|[hoABDC4bNqLsgmbp.htm](agents-of-edgewatch-bestiary-items/hoABDC4bNqLsgmbp.htm)|Dagger|Daga|modificada|
|[hozDdAMhO9qf2Buo.htm](agents-of-edgewatch-bestiary-items/hozDdAMhO9qf2Buo.htm)|Decapitate|Decapitar|modificada|
|[Hq1vagZwNS6eDSNA.htm](agents-of-edgewatch-bestiary-items/Hq1vagZwNS6eDSNA.htm)|Tainted Backlash|Tainted Backlash|modificada|
|[hrbhUgMW0KZvWovb.htm](agents-of-edgewatch-bestiary-items/hrbhUgMW0KZvWovb.htm)|Katar Specialist|Especialista Katar|modificada|
|[HrV9IU9zNImvgkv7.htm](agents-of-edgewatch-bestiary-items/HrV9IU9zNImvgkv7.htm)|Grab|Agarrado|modificada|
|[hScPOOUe2XVGeyIa.htm](agents-of-edgewatch-bestiary-items/hScPOOUe2XVGeyIa.htm)|Bronze Fist|Puño de Bronce|modificada|
|[Hss992fc1sCOcDVo.htm](agents-of-edgewatch-bestiary-items/Hss992fc1sCOcDVo.htm)|Electric Reflexes|Reflejos eléctricos|modificada|
|[Ht4guo51Eky0gFrz.htm](agents-of-edgewatch-bestiary-items/Ht4guo51Eky0gFrz.htm)|Partial Amphibian|Anfibio Parcial|modificada|
|[hup5Hnb2EZPEeNg7.htm](agents-of-edgewatch-bestiary-items/hup5Hnb2EZPEeNg7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hvjSsYJt6z0pbDL0.htm](agents-of-edgewatch-bestiary-items/hvjSsYJt6z0pbDL0.htm)|Mandibles|Mandíbulas|modificada|
|[hvsbuonawLt5vOLp.htm](agents-of-edgewatch-bestiary-items/hvsbuonawLt5vOLp.htm)|Brutish Shove|Empujón brutal|modificada|
|[hvTnI184eIoLfwPq.htm](agents-of-edgewatch-bestiary-items/hvTnI184eIoLfwPq.htm)|Berserk Slam|Porrazo bersérker|modificada|
|[hwDOgihBpaGyHO9Q.htm](agents-of-edgewatch-bestiary-items/hwDOgihBpaGyHO9Q.htm)|Dagger|Daga|modificada|
|[hxG5VYuZdf33T05P.htm](agents-of-edgewatch-bestiary-items/hxG5VYuZdf33T05P.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[HxSPrzqufbKb8gOo.htm](agents-of-edgewatch-bestiary-items/HxSPrzqufbKb8gOo.htm)|Negative Healing|Curación negativa|modificada|
|[hytdhHAVEMZ2IlnQ.htm](agents-of-edgewatch-bestiary-items/hytdhHAVEMZ2IlnQ.htm)|Claws|Garras|modificada|
|[hzLoXfk0XZ09nziz.htm](agents-of-edgewatch-bestiary-items/hzLoXfk0XZ09nziz.htm)|Terrifying Gaze|Mirada aterradora|modificada|
|[i0POO0dwNFDOcYuV.htm](agents-of-edgewatch-bestiary-items/i0POO0dwNFDOcYuV.htm)|Summon Devil|Invocar Diablo|modificada|
|[i1fagw7VusZxwIdz.htm](agents-of-edgewatch-bestiary-items/i1fagw7VusZxwIdz.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[I23rYMszuvKL97k7.htm](agents-of-edgewatch-bestiary-items/I23rYMszuvKL97k7.htm)|Protean Anatomy|Anatomía Proteica|modificada|
|[I5UOcgBhEV16sOvv.htm](agents-of-edgewatch-bestiary-items/I5UOcgBhEV16sOvv.htm)|Studied Strike|Golpe Estudiado|modificada|
|[I7zUPwVMtrP17lu5.htm](agents-of-edgewatch-bestiary-items/I7zUPwVMtrP17lu5.htm)|Eschew Materials|Abstención de materiales|modificada|
|[IBTLrjDIdwriv1Sh.htm](agents-of-edgewatch-bestiary-items/IBTLrjDIdwriv1Sh.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[iccpDuEAwMrtLyBh.htm](agents-of-edgewatch-bestiary-items/iccpDuEAwMrtLyBh.htm)|Wire Catch|Wire Catch|modificada|
|[IcNV9BTE0VD3ZNXy.htm](agents-of-edgewatch-bestiary-items/IcNV9BTE0VD3ZNXy.htm)|Intimidating Stare|Mirada Intimidación|modificada|
|[IDckQwFfttdnib8W.htm](agents-of-edgewatch-bestiary-items/IDckQwFfttdnib8W.htm)|Poisoned Dart|Dardo Envenenado|modificada|
|[Idet44bdRqy8cmfx.htm](agents-of-edgewatch-bestiary-items/Idet44bdRqy8cmfx.htm)|Dagger|Daga|modificada|
|[IdUgaBFlawtCWK5m.htm](agents-of-edgewatch-bestiary-items/IdUgaBFlawtCWK5m.htm)|Constant Spells|Constant Spells|modificada|
|[IEuErW2Q6KaGlk21.htm](agents-of-edgewatch-bestiary-items/IEuErW2Q6KaGlk21.htm)|Hook and Flay|Enganchar y Desollar|modificada|
|[If0QHT3SwMga7arR.htm](agents-of-edgewatch-bestiary-items/If0QHT3SwMga7arR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IfCbrxEtWCoJCOgv.htm](agents-of-edgewatch-bestiary-items/IfCbrxEtWCoJCOgv.htm)|Longsword|Longsword|modificada|
|[iFX6Z8UsTWukjUxp.htm](agents-of-edgewatch-bestiary-items/iFX6Z8UsTWukjUxp.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[iG9uOtBJolj3P4CM.htm](agents-of-edgewatch-bestiary-items/iG9uOtBJolj3P4CM.htm)|Spew Cloud|Spew Cloud|modificada|
|[igIwGgzOBvjtlLdx.htm](agents-of-edgewatch-bestiary-items/igIwGgzOBvjtlLdx.htm)|Transparent|Transparente|modificada|
|[IH1SiADzFCDakX8w.htm](agents-of-edgewatch-bestiary-items/IH1SiADzFCDakX8w.htm)|Out You Go|Out You Go|modificada|
|[II1iKHEPt0qdurXn.htm](agents-of-edgewatch-bestiary-items/II1iKHEPt0qdurXn.htm)|Plunder Life|Saquear la vida|modificada|
|[IiO8ytUL6tJgUAXR.htm](agents-of-edgewatch-bestiary-items/IiO8ytUL6tJgUAXR.htm)|Vargouille Venom|Vargouille Venom|modificada|
|[iJUSlDT7ZVFtjNSy.htm](agents-of-edgewatch-bestiary-items/iJUSlDT7ZVFtjNSy.htm)|Spider Swarm Host|Anfitrión del Enjambre de Arañas|modificada|
|[Ikix1zKqDpYhzRYA.htm](agents-of-edgewatch-bestiary-items/Ikix1zKqDpYhzRYA.htm)|Infector|Infector|modificada|
|[IKQ2kDBIPfAILZjf.htm](agents-of-edgewatch-bestiary-items/IKQ2kDBIPfAILZjf.htm)|Alchemical Bomb|Bomba alquímica|modificada|
|[ikVC2siqOrYTYZtS.htm](agents-of-edgewatch-bestiary-items/ikVC2siqOrYTYZtS.htm)|Second Chance|Segunda oportunidad|modificada|
|[Iky2gPwt5DqOgbkl.htm](agents-of-edgewatch-bestiary-items/Iky2gPwt5DqOgbkl.htm)|Push 10 feet|Empuje 10 pies|modificada|
|[IlMxWLhPQQrxWb4s.htm](agents-of-edgewatch-bestiary-items/IlMxWLhPQQrxWb4s.htm)|Wind|Wind|modificada|
|[INFGwX7PUOvEy0iD.htm](agents-of-edgewatch-bestiary-items/INFGwX7PUOvEy0iD.htm)|Trail of Flame|Rastro de Flamígera|modificada|
|[iNUgfYO7f8c2RdLm.htm](agents-of-edgewatch-bestiary-items/iNUgfYO7f8c2RdLm.htm)|Rend (jaws)|Rasgadura (Fauces)|modificada|
|[iOt4Y0EmIGeBhl6u.htm](agents-of-edgewatch-bestiary-items/iOt4Y0EmIGeBhl6u.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iprvaUuVaBlST8Su.htm](agents-of-edgewatch-bestiary-items/iprvaUuVaBlST8Su.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[iqSjrlDup1I8i5VX.htm](agents-of-edgewatch-bestiary-items/iqSjrlDup1I8i5VX.htm)|Bloody Feet|Bloody Feet|modificada|
|[ISXykb5wK1DoaIOw.htm](agents-of-edgewatch-bestiary-items/ISXykb5wK1DoaIOw.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[ITQk9n2tp7J5FDQa.htm](agents-of-edgewatch-bestiary-items/ITQk9n2tp7J5FDQa.htm)|Change Shape|Change Shape|modificada|
|[iUGSF5wsY0hhEr0g.htm](agents-of-edgewatch-bestiary-items/iUGSF5wsY0hhEr0g.htm)|Grab|Agarrado|modificada|
|[iV2kvaEO0RYmWI8X.htm](agents-of-edgewatch-bestiary-items/iV2kvaEO0RYmWI8X.htm)|Blade|Blade|modificada|
|[IVbYmIS1u62AEFZm.htm](agents-of-edgewatch-bestiary-items/IVbYmIS1u62AEFZm.htm)|Scalding Spray|Scalding Spray|modificada|
|[iYdGVR75KvoZWFO3.htm](agents-of-edgewatch-bestiary-items/iYdGVR75KvoZWFO3.htm)|Toxic Mastery|Toxic Mastery|modificada|
|[IZqqliLOZ1AtFOJF.htm](agents-of-edgewatch-bestiary-items/IZqqliLOZ1AtFOJF.htm)|Sap|Sap|modificada|
|[j2lpHYGglR2Lc1Ut.htm](agents-of-edgewatch-bestiary-items/j2lpHYGglR2Lc1Ut.htm)|Darkvision|Visión en la oscuridad|modificada|
|[J6CumHMGaEuUCyrp.htm](agents-of-edgewatch-bestiary-items/J6CumHMGaEuUCyrp.htm)|Open Hatch|Escotilla Abierta|modificada|
|[j72OO3UZ0KrTLLO6.htm](agents-of-edgewatch-bestiary-items/j72OO3UZ0KrTLLO6.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[j7j4zEpOdEbRtRuk.htm](agents-of-edgewatch-bestiary-items/j7j4zEpOdEbRtRuk.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[j7oe7252bXGLrvgk.htm](agents-of-edgewatch-bestiary-items/j7oe7252bXGLrvgk.htm)|Reaper's Lancet Blade|Hoja Lanceta del Segador|modificada|
|[jC7yNSXPp6Y4dqOF.htm](agents-of-edgewatch-bestiary-items/jC7yNSXPp6Y4dqOF.htm)|Nightmare Assault|Asalto de pesadilla|modificada|
|[jcfMZ2vEUL2nEfFQ.htm](agents-of-edgewatch-bestiary-items/jcfMZ2vEUL2nEfFQ.htm)|Surreptitious Siege|Asedio subrepticio|modificada|
|[JCwAfoQqJDmPTVLF.htm](agents-of-edgewatch-bestiary-items/JCwAfoQqJDmPTVLF.htm)|Crystal Sense (Imprecise) 30 feet|Sentido cristales (Impreciso) 30 pies.|modificada|
|[jdam6t7WxAcXyrY6.htm](agents-of-edgewatch-bestiary-items/jdam6t7WxAcXyrY6.htm)|Claw|Garra|modificada|
|[jDE564VbgKyhi1ch.htm](agents-of-edgewatch-bestiary-items/jDE564VbgKyhi1ch.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[jfhtS3mRJ8Vf1K2F.htm](agents-of-edgewatch-bestiary-items/jfhtS3mRJ8Vf1K2F.htm)|Longsword|Longsword|modificada|
|[jgogVBpbS3zZv6XG.htm](agents-of-edgewatch-bestiary-items/jgogVBpbS3zZv6XG.htm)|Rapier|Estoque|modificada|
|[JHDUCoAwvNv120KU.htm](agents-of-edgewatch-bestiary-items/JHDUCoAwvNv120KU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jirCmoCjYEZm147H.htm](agents-of-edgewatch-bestiary-items/jirCmoCjYEZm147H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JJiuY4l7O3Ujlg8S.htm](agents-of-edgewatch-bestiary-items/JJiuY4l7O3Ujlg8S.htm)|Speed|Velocidad|modificada|
|[JJluFLD9A2iKiNi4.htm](agents-of-edgewatch-bestiary-items/JJluFLD9A2iKiNi4.htm)|Double Punch|Doble Puñetazo|modificada|
|[jJSKOhaRoCp1WwP4.htm](agents-of-edgewatch-bestiary-items/jJSKOhaRoCp1WwP4.htm)|Inhabit Vessel|Habitar Nave|modificada|
|[jkHMXDAED128Pydk.htm](agents-of-edgewatch-bestiary-items/jkHMXDAED128Pydk.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[Jkps95l9DKtpNrr8.htm](agents-of-edgewatch-bestiary-items/Jkps95l9DKtpNrr8.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[JNMs0qEBFo57y0tr.htm](agents-of-edgewatch-bestiary-items/JNMs0qEBFo57y0tr.htm)|Rend|Rasgadura|modificada|
|[JnTr0G2JvwVYzdfR.htm](agents-of-edgewatch-bestiary-items/JnTr0G2JvwVYzdfR.htm)|Detaining Strike|Golpe de Detención|modificada|
|[jq9NuOTE6q7jHwO1.htm](agents-of-edgewatch-bestiary-items/jq9NuOTE6q7jHwO1.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[JqbE9PdaDo8dWRhu.htm](agents-of-edgewatch-bestiary-items/JqbE9PdaDo8dWRhu.htm)|Jaws|Fauces|modificada|
|[JQLaH8N9rTPhEzqj.htm](agents-of-edgewatch-bestiary-items/JQLaH8N9rTPhEzqj.htm)|Sword Cane Duelist|Bastón estoque conjuro|modificada|
|[JRKYJD9kEhVvnpPF.htm](agents-of-edgewatch-bestiary-items/JRKYJD9kEhVvnpPF.htm)|Jaws|Fauces|modificada|
|[JUNDTV3wwrM9gFjE.htm](agents-of-edgewatch-bestiary-items/JUNDTV3wwrM9gFjE.htm)|Crossbow|Ballesta|modificada|
|[JXdEhHs5I4scpvbs.htm](agents-of-edgewatch-bestiary-items/JXdEhHs5I4scpvbs.htm)|Shortsword|Espada corta|modificada|
|[JXIun04dusvrOS6n.htm](agents-of-edgewatch-bestiary-items/JXIun04dusvrOS6n.htm)|Kiss of the Speakers|Beso de los altavoces|modificada|
|[jybYCirNRpxTm5fl.htm](agents-of-edgewatch-bestiary-items/jybYCirNRpxTm5fl.htm)|Fist|Puño|modificada|
|[jzOEMi9hAVAe1K1V.htm](agents-of-edgewatch-bestiary-items/jzOEMi9hAVAe1K1V.htm)|Poisoned Needle|Aguja envenenada|modificada|
|[jZQm4Bhn7fdp3YRx.htm](agents-of-edgewatch-bestiary-items/jZQm4Bhn7fdp3YRx.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[k0BFzCiBWr8fRZ4m.htm](agents-of-edgewatch-bestiary-items/k0BFzCiBWr8fRZ4m.htm)|Obscuring Grab|Agarre Obscuro|modificada|
|[k1KchqR9i8uSyT6e.htm](agents-of-edgewatch-bestiary-items/k1KchqR9i8uSyT6e.htm)|Combustible|Combustible|modificada|
|[k1kPrQ3Krhcipl4E.htm](agents-of-edgewatch-bestiary-items/k1kPrQ3Krhcipl4E.htm)|Fist|Puño|modificada|
|[k1m2BVdO1iqRouch.htm](agents-of-edgewatch-bestiary-items/k1m2BVdO1iqRouch.htm)|Emotion Sense 120 feet|Emotion Sense 120 pies|modificada|
|[K287MomJCMfPXPLf.htm](agents-of-edgewatch-bestiary-items/K287MomJCMfPXPLf.htm)|Smoke Vision|Visión de Humo|modificada|
|[K2NRmCiVWR4uMsRg.htm](agents-of-edgewatch-bestiary-items/K2NRmCiVWR4uMsRg.htm)|Vanish in Reflections|Desaparecer en Reflejos|modificada|
|[K2xZ73LiKk2uqR28.htm](agents-of-edgewatch-bestiary-items/K2xZ73LiKk2uqR28.htm)|Blackfinger's Prayer|Blackfinger's Prayer|modificada|
|[K4f9RbGiZucMUGD8.htm](agents-of-edgewatch-bestiary-items/K4f9RbGiZucMUGD8.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[K68kyyTQ4cOmTtUk.htm](agents-of-edgewatch-bestiary-items/K68kyyTQ4cOmTtUk.htm)|Mind-Numbing Grasp|Mind-Numbing Grasp|modificada|
|[k6q40GuzzYWifu3y.htm](agents-of-edgewatch-bestiary-items/k6q40GuzzYWifu3y.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[K7CFCrn5nlo3dzku.htm](agents-of-edgewatch-bestiary-items/K7CFCrn5nlo3dzku.htm)|Scourge|Scourge|modificada|
|[kckDvYIQBBGcjLNo.htm](agents-of-edgewatch-bestiary-items/kckDvYIQBBGcjLNo.htm)|Scimitar|Cimitarra|modificada|
|[kcrnWNWqaiPR3tg9.htm](agents-of-edgewatch-bestiary-items/kcrnWNWqaiPR3tg9.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[KcYjsnv1Da60rZqp.htm](agents-of-edgewatch-bestiary-items/KcYjsnv1Da60rZqp.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[kEiDcm42Prsww2ja.htm](agents-of-edgewatch-bestiary-items/kEiDcm42Prsww2ja.htm)|Alchemical Reaction|Alchemical Reaction|modificada|
|[kELQNLDmBgZ4uIU5.htm](agents-of-edgewatch-bestiary-items/kELQNLDmBgZ4uIU5.htm)|Improved Grab|Agarrado mejorado|modificada|
|[KeuEtlVRXnSoTjPo.htm](agents-of-edgewatch-bestiary-items/KeuEtlVRXnSoTjPo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KFdVbNhK30Xo6GCp.htm](agents-of-edgewatch-bestiary-items/KFdVbNhK30Xo6GCp.htm)|Ooze Tendril|Ooze Tendril|modificada|
|[Kfw3xJtFTIHlmDoM.htm](agents-of-edgewatch-bestiary-items/Kfw3xJtFTIHlmDoM.htm)|Improved Grab|Agarrado mejorado|modificada|
|[kfZVz80JupwQ5DE1.htm](agents-of-edgewatch-bestiary-items/kfZVz80JupwQ5DE1.htm)|Bloody Chain Aura|Aura de Cadena Sangrienta|modificada|
|[kIPI4tNPGahloPyK.htm](agents-of-edgewatch-bestiary-items/kIPI4tNPGahloPyK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kK15wEyRUz3vGzjl.htm](agents-of-edgewatch-bestiary-items/kK15wEyRUz3vGzjl.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[KkQu3TqvmvRwh5Gx.htm](agents-of-edgewatch-bestiary-items/KkQu3TqvmvRwh5Gx.htm)|Crush of Hundreds|Aplastar Cientos|modificada|
|[kMMwsDVDDuFjQhm3.htm](agents-of-edgewatch-bestiary-items/kMMwsDVDDuFjQhm3.htm)|Fist|Puño|modificada|
|[KOfo0zVfgBhhWGjT.htm](agents-of-edgewatch-bestiary-items/KOfo0zVfgBhhWGjT.htm)|Quill Cannon|Quill Cannon|modificada|
|[Kp4jOhCQVcuNVP2j.htm](agents-of-edgewatch-bestiary-items/Kp4jOhCQVcuNVP2j.htm)|Vital Transfusion|Transfusión Vital|modificada|
|[kPfYHs4PPwutz153.htm](agents-of-edgewatch-bestiary-items/kPfYHs4PPwutz153.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[krn8dqAmFwPEmo0O.htm](agents-of-edgewatch-bestiary-items/krn8dqAmFwPEmo0O.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KRYmDgL0v3sgSPhE.htm](agents-of-edgewatch-bestiary-items/KRYmDgL0v3sgSPhE.htm)|Explosion|Explosión|modificada|
|[kS3RkK66eSUF1gOu.htm](agents-of-edgewatch-bestiary-items/kS3RkK66eSUF1gOu.htm)|Tail|Tail|modificada|
|[kspfnXbpzKtPxBp2.htm](agents-of-edgewatch-bestiary-items/kspfnXbpzKtPxBp2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kVdw249CmzAbj17U.htm](agents-of-edgewatch-bestiary-items/kVdw249CmzAbj17U.htm)|Riptide|Riptide|modificada|
|[KWEh7hh4al6ViDpu.htm](agents-of-edgewatch-bestiary-items/KWEh7hh4al6ViDpu.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[KyPmodj8yfI4cI99.htm](agents-of-edgewatch-bestiary-items/KyPmodj8yfI4cI99.htm)|Overtake Soul|Overtake Soul|modificada|
|[KZ4Ixv66QE0EQIct.htm](agents-of-edgewatch-bestiary-items/KZ4Ixv66QE0EQIct.htm)|Pseudopod|Pseudópodo|modificada|
|[kzNiR7bLBxL4aWfR.htm](agents-of-edgewatch-bestiary-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|Desenvainado rápido|modificada|
|[kZPMz3gjTr4SFKIs.htm](agents-of-edgewatch-bestiary-items/kZPMz3gjTr4SFKIs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[l5qCiwvxOStA4H26.htm](agents-of-edgewatch-bestiary-items/l5qCiwvxOStA4H26.htm)|Claw|Garra|modificada|
|[l5xkjpvgvFHWi6Yj.htm](agents-of-edgewatch-bestiary-items/l5xkjpvgvFHWi6Yj.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[l7wT4LDdfJXJETHf.htm](agents-of-edgewatch-bestiary-items/l7wT4LDdfJXJETHf.htm)|Clockwork Brain|Clockwork Brain|modificada|
|[LaDAOhsZJlBZ3eiZ.htm](agents-of-edgewatch-bestiary-items/LaDAOhsZJlBZ3eiZ.htm)|Shortsword|Espada corta|modificada|
|[laNWVOBt5DoTHn3G.htm](agents-of-edgewatch-bestiary-items/laNWVOBt5DoTHn3G.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[LBQk96wXoxYrTUZi.htm](agents-of-edgewatch-bestiary-items/LBQk96wXoxYrTUZi.htm)|Distort Magic|Distorsionar Magia|modificada|
|[LCEc1hLLB843Ltt3.htm](agents-of-edgewatch-bestiary-items/LCEc1hLLB843Ltt3.htm)|Fist|Puño|modificada|
|[LCKRgv7IeeWygjY3.htm](agents-of-edgewatch-bestiary-items/LCKRgv7IeeWygjY3.htm)|Spirit Blast Ray|Rayo explosión espiritual|modificada|
|[LFIJhcEKJhKijVm1.htm](agents-of-edgewatch-bestiary-items/LFIJhcEKJhKijVm1.htm)|Shock|Electrizante|modificada|
|[lftFJteRXqRBmg0d.htm](agents-of-edgewatch-bestiary-items/lftFJteRXqRBmg0d.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[lHgISCDkIa7T34nH.htm](agents-of-edgewatch-bestiary-items/lHgISCDkIa7T34nH.htm)|Spawn Mirror Duplicate|Duplicado de Espejo|modificada|
|[LiFDw1yyHumRLJFc.htm](agents-of-edgewatch-bestiary-items/LiFDw1yyHumRLJFc.htm)|Grab|Agarrado|modificada|
|[lJYPy9JbnZy8cvV0.htm](agents-of-edgewatch-bestiary-items/lJYPy9JbnZy8cvV0.htm)|Dagger|Daga|modificada|
|[lMrRe942X6FotrEf.htm](agents-of-edgewatch-bestiary-items/lMrRe942X6FotrEf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lmVllcsiFHe8ybRy.htm](agents-of-edgewatch-bestiary-items/lmVllcsiFHe8ybRy.htm)|Wretched Weeps|Wretched Weeps|modificada|
|[LnwbIbIzWsV3rQvP.htm](agents-of-edgewatch-bestiary-items/LnwbIbIzWsV3rQvP.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[loHKBZQtnioHzbFm.htm](agents-of-edgewatch-bestiary-items/loHKBZQtnioHzbFm.htm)|Quick Bomber|Bombardero rápido|modificada|
|[LPMvePNhEQEvIeA1.htm](agents-of-edgewatch-bestiary-items/LPMvePNhEQEvIeA1.htm)|Alchemical Injection|Inyección alquímica|modificada|
|[lpqsopfJ3k9tHtel.htm](agents-of-edgewatch-bestiary-items/lpqsopfJ3k9tHtel.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[LQEhpFQTxwqQ8b2q.htm](agents-of-edgewatch-bestiary-items/LQEhpFQTxwqQ8b2q.htm)|Deflective Dodge|Esquiva deflectiva|modificada|
|[LveVTPzs4hEN09k6.htm](agents-of-edgewatch-bestiary-items/LveVTPzs4hEN09k6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LvmDMsbTpm0IyK9u.htm](agents-of-edgewatch-bestiary-items/LvmDMsbTpm0IyK9u.htm)|Fast Swallow|Tragar de golpe|modificada|
|[lVTqn24kqmGrjWJI.htm](agents-of-edgewatch-bestiary-items/lVTqn24kqmGrjWJI.htm)|Spit|Escupe|modificada|
|[lVvJa2dDy1MeSjQ1.htm](agents-of-edgewatch-bestiary-items/lVvJa2dDy1MeSjQ1.htm)|Splash Poison|Salpicadura Veneno|modificada|
|[LvYtP53GFOEynRGV.htm](agents-of-edgewatch-bestiary-items/LvYtP53GFOEynRGV.htm)|Skip Between|Saltar entre|modificada|
|[LWnz2t8s48aN2oK3.htm](agents-of-edgewatch-bestiary-items/LWnz2t8s48aN2oK3.htm)|Harrow Card|Harrow Card|modificada|
|[LxrfittzDR3SPd3W.htm](agents-of-edgewatch-bestiary-items/LxrfittzDR3SPd3W.htm)|Fill Lungs|Llenar Pulmones|modificada|
|[LYl3vW92ci5HFVXk.htm](agents-of-edgewatch-bestiary-items/LYl3vW92ci5HFVXk.htm)|Alchemical Chambers|Compartimentos alquímicos|modificada|
|[LyRSaBlkqwNMCaQT.htm](agents-of-edgewatch-bestiary-items/LyRSaBlkqwNMCaQT.htm)|Slam|Slam|modificada|
|[lYvWF5v5Vah9KWsN.htm](agents-of-edgewatch-bestiary-items/lYvWF5v5Vah9KWsN.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[LzeE4Oi1C9zb69rd.htm](agents-of-edgewatch-bestiary-items/LzeE4Oi1C9zb69rd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LzNIxnRUBN5s28I4.htm](agents-of-edgewatch-bestiary-items/LzNIxnRUBN5s28I4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lZznKQkFobPXBxDT.htm](agents-of-edgewatch-bestiary-items/lZznKQkFobPXBxDT.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[M0P9lsWNp222StTi.htm](agents-of-edgewatch-bestiary-items/M0P9lsWNp222StTi.htm)|Pseudopod|Pseudópodo|modificada|
|[M0y7VtVeytggUvVq.htm](agents-of-edgewatch-bestiary-items/M0y7VtVeytggUvVq.htm)|Jaws|Fauces|modificada|
|[M203mlcBNFvAqyIJ.htm](agents-of-edgewatch-bestiary-items/M203mlcBNFvAqyIJ.htm)|Stone Robes|Túnicas de piedra|modificada|
|[M2i1Ktvj26SGDm7s.htm](agents-of-edgewatch-bestiary-items/M2i1Ktvj26SGDm7s.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[m6L9nqkoxL1pP5V6.htm](agents-of-edgewatch-bestiary-items/m6L9nqkoxL1pP5V6.htm)|Attack of Opportunity (Trip Only)|Ataque de oportunidad (sólo Derribar)|modificada|
|[M7E6dxLskMLaX43G.htm](agents-of-edgewatch-bestiary-items/M7E6dxLskMLaX43G.htm)|Dimensional Pit|Dimensional Pit|modificada|
|[m81fVuHWpnJwd8cc.htm](agents-of-edgewatch-bestiary-items/m81fVuHWpnJwd8cc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[M9ciwVyPg7uZSM44.htm](agents-of-edgewatch-bestiary-items/M9ciwVyPg7uZSM44.htm)|Web|Telara|modificada|
|[Ma9MtDSiauZ5onNI.htm](agents-of-edgewatch-bestiary-items/Ma9MtDSiauZ5onNI.htm)|Nimble Swim|Nadar Ágil|modificada|
|[mBGuEJPcBGcngTxE.htm](agents-of-edgewatch-bestiary-items/mBGuEJPcBGcngTxE.htm)|Horn|Cuerno|modificada|
|[MClpWaT5c8jTmcFK.htm](agents-of-edgewatch-bestiary-items/MClpWaT5c8jTmcFK.htm)|Flush|Flush|modificada|
|[McLV2Lx51f0UF1YX.htm](agents-of-edgewatch-bestiary-items/McLV2Lx51f0UF1YX.htm)|Poison Ink|Tinta Venenosa|modificada|
|[mdqTsawy6o4sNLqp.htm](agents-of-edgewatch-bestiary-items/mdqTsawy6o4sNLqp.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[mDzfLBnuMo5KYFSr.htm](agents-of-edgewatch-bestiary-items/mDzfLBnuMo5KYFSr.htm)|Cast Down|Derribar|modificada|
|[mHKZKkSzSU0RL4MI.htm](agents-of-edgewatch-bestiary-items/mHKZKkSzSU0RL4MI.htm)|Sap|Sap|modificada|
|[MIGvbijvRTovVpJE.htm](agents-of-edgewatch-bestiary-items/MIGvbijvRTovVpJE.htm)|Vomit Blood|Vomitar Sangre|modificada|
|[mIVZhiAlmqc8WWY7.htm](agents-of-edgewatch-bestiary-items/mIVZhiAlmqc8WWY7.htm)|Flay|Flay|modificada|
|[mjEOGwxDnDNV6VB0.htm](agents-of-edgewatch-bestiary-items/mjEOGwxDnDNV6VB0.htm)|Swarming|Enjambre|modificada|
|[mk8H61X7q0MpYFgq.htm](agents-of-edgewatch-bestiary-items/mk8H61X7q0MpYFgq.htm)|Prepared Divine Spells|Hechizos divinos preparados.|modificada|
|[Mlm12XchuKW2Zkqk.htm](agents-of-edgewatch-bestiary-items/Mlm12XchuKW2Zkqk.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[mmdbAeOOVVjLALCx.htm](agents-of-edgewatch-bestiary-items/mmdbAeOOVVjLALCx.htm)|Rapid Repair|Reparación Rápida|modificada|
|[mNgkZuWHNH6FJsSb.htm](agents-of-edgewatch-bestiary-items/mNgkZuWHNH6FJsSb.htm)|Trickster's Ace|As del embaucamiento|modificada|
|[mOEFev3A3LvUINAz.htm](agents-of-edgewatch-bestiary-items/mOEFev3A3LvUINAz.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[mOEnnV4W8qmCRdYl.htm](agents-of-edgewatch-bestiary-items/mOEnnV4W8qmCRdYl.htm)|Claw|Garra|modificada|
|[MpraNid9IzBbGn7L.htm](agents-of-edgewatch-bestiary-items/MpraNid9IzBbGn7L.htm)|Dagger|Daga|modificada|
|[MQ2Rp3l2BaBlZCiE.htm](agents-of-edgewatch-bestiary-items/MQ2Rp3l2BaBlZCiE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mqpmTykaUCnVLmSj.htm](agents-of-edgewatch-bestiary-items/mqpmTykaUCnVLmSj.htm)|Claw|Garra|modificada|
|[mRbro73gnpsZOfZa.htm](agents-of-edgewatch-bestiary-items/mRbro73gnpsZOfZa.htm)|Claw|Garra|modificada|
|[mRPwn3ixbA1j5Wfz.htm](agents-of-edgewatch-bestiary-items/mRPwn3ixbA1j5Wfz.htm)|Study Target|Objetivo del estudio|modificada|
|[MRSyx2ZsaFXN3kSn.htm](agents-of-edgewatch-bestiary-items/MRSyx2ZsaFXN3kSn.htm)|Power Attack|Ataque poderoso|modificada|
|[mryw4DfSBV3Xx30n.htm](agents-of-edgewatch-bestiary-items/mryw4DfSBV3Xx30n.htm)|Grab|Agarrado|modificada|
|[mTP4bYZoCnN514ZB.htm](agents-of-edgewatch-bestiary-items/mTP4bYZoCnN514ZB.htm)|Spit|Escupe|modificada|
|[MvGg9ZU03PHCDYj9.htm](agents-of-edgewatch-bestiary-items/MvGg9ZU03PHCDYj9.htm)|Quick Stitch|Puntada rápida|modificada|
|[MwIhQXzCMNpAds0Q.htm](agents-of-edgewatch-bestiary-items/MwIhQXzCMNpAds0Q.htm)|Constant Spells|Constant Spells|modificada|
|[mYdklqhaT2LcRlN3.htm](agents-of-edgewatch-bestiary-items/mYdklqhaT2LcRlN3.htm)|Determination|Determinación|modificada|
|[mYFvnq9cvcGTpyjV.htm](agents-of-edgewatch-bestiary-items/mYFvnq9cvcGTpyjV.htm)|Constant Spells|Constant Spells|modificada|
|[MyTJRF05gqbWnyPq.htm](agents-of-edgewatch-bestiary-items/MyTJRF05gqbWnyPq.htm)|Metallify|Metallify|modificada|
|[MZuAw11uvBDLHKj7.htm](agents-of-edgewatch-bestiary-items/MZuAw11uvBDLHKj7.htm)|Shortsword|Espada corta|modificada|
|[mZVd3ZcajfGKAm79.htm](agents-of-edgewatch-bestiary-items/mZVd3ZcajfGKAm79.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[n0iAPmFsdZlSVqHV.htm](agents-of-edgewatch-bestiary-items/n0iAPmFsdZlSVqHV.htm)|Dagger|Daga|modificada|
|[n4IYsyn6jwdp0itB.htm](agents-of-edgewatch-bestiary-items/n4IYsyn6jwdp0itB.htm)|Blade Legs|Blade Legs|modificada|
|[N5zf8LJKXg8KdskQ.htm](agents-of-edgewatch-bestiary-items/N5zf8LJKXg8KdskQ.htm)|Kharnas's Blessing|Bendición de Kharnas|modificada|
|[n72S7jEvm5AQeVky.htm](agents-of-edgewatch-bestiary-items/n72S7jEvm5AQeVky.htm)|Dagger|Daga|modificada|
|[N85UwJsuAe8nwuWV.htm](agents-of-edgewatch-bestiary-items/N85UwJsuAe8nwuWV.htm)|Spell Circle|Círculo de Hechizos|modificada|
|[n8gYgKZb2msziuyh.htm](agents-of-edgewatch-bestiary-items/n8gYgKZb2msziuyh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[N8sxUVjIH1fEWXxu.htm](agents-of-edgewatch-bestiary-items/N8sxUVjIH1fEWXxu.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[n9d7kqBHgMv3tOmv.htm](agents-of-edgewatch-bestiary-items/n9d7kqBHgMv3tOmv.htm)|Choking Smog|Smog asfixiante|modificada|
|[n9oO2EI8HwHSgtLl.htm](agents-of-edgewatch-bestiary-items/n9oO2EI8HwHSgtLl.htm)|Terinav Root Poison|Enraizamiento Terinav Veneno|modificada|
|[nA0QdxrRaEjv6RhP.htm](agents-of-edgewatch-bestiary-items/nA0QdxrRaEjv6RhP.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[nA5lgWKn3AryAuFs.htm](agents-of-edgewatch-bestiary-items/nA5lgWKn3AryAuFs.htm)|Twisted Desires|Deseos corrompidos|modificada|
|[Nbx9ouRvtXU7r7XP.htm](agents-of-edgewatch-bestiary-items/Nbx9ouRvtXU7r7XP.htm)|Dart Volley|Volea de dardos|modificada|
|[nCwbF6k6DjALgFuy.htm](agents-of-edgewatch-bestiary-items/nCwbF6k6DjALgFuy.htm)|Rage|Furia|modificada|
|[NDjY88hxyNPqDwPf.htm](agents-of-edgewatch-bestiary-items/NDjY88hxyNPqDwPf.htm)|Quick Bomber|Bombardero rápido|modificada|
|[Ng3bVzRpGnsUr9AL.htm](agents-of-edgewatch-bestiary-items/Ng3bVzRpGnsUr9AL.htm)|Dagger|Daga|modificada|
|[NGB5wLXcmJCijnEn.htm](agents-of-edgewatch-bestiary-items/NGB5wLXcmJCijnEn.htm)|Jaws|Fauces|modificada|
|[NGOG0Y8bjtNkNrjz.htm](agents-of-edgewatch-bestiary-items/NGOG0Y8bjtNkNrjz.htm)|Spear|Lanza|modificada|
|[NHDyB7FzdAXPJvx3.htm](agents-of-edgewatch-bestiary-items/NHDyB7FzdAXPJvx3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nHj4mI5oMJrWP9jm.htm](agents-of-edgewatch-bestiary-items/nHj4mI5oMJrWP9jm.htm)|Grab|Agarrado|modificada|
|[NhWlBh7zqwLP19mb.htm](agents-of-edgewatch-bestiary-items/NhWlBh7zqwLP19mb.htm)|Mindtwisting Utterance|Mindtwisting Utterance|modificada|
|[nhz5F428cTKGkc9v.htm](agents-of-edgewatch-bestiary-items/nhz5F428cTKGkc9v.htm)|Inexorable March|Marcha inexorable|modificada|
|[NI333m4B4OMeocLf.htm](agents-of-edgewatch-bestiary-items/NI333m4B4OMeocLf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NILfejzFJ409TN4i.htm](agents-of-edgewatch-bestiary-items/NILfejzFJ409TN4i.htm)|Poison Conversion|Poison Conversion|modificada|
|[niqal0ySwUPyqG0P.htm](agents-of-edgewatch-bestiary-items/niqal0ySwUPyqG0P.htm)|Eye Probe|Eye Probe|modificada|
|[NIWpD2eqbaBc5M8m.htm](agents-of-edgewatch-bestiary-items/NIWpD2eqbaBc5M8m.htm)|Fist|Puño|modificada|
|[Nl7SDGVnqgpHC581.htm](agents-of-edgewatch-bestiary-items/Nl7SDGVnqgpHC581.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nO9dKSqf8ZjJRuCy.htm](agents-of-edgewatch-bestiary-items/nO9dKSqf8ZjJRuCy.htm)|Change Shape|Change Shape|modificada|
|[NPjuafWJNiaGNe5E.htm](agents-of-edgewatch-bestiary-items/NPjuafWJNiaGNe5E.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[nqhGLlFeLoFFBN9g.htm](agents-of-edgewatch-bestiary-items/nqhGLlFeLoFFBN9g.htm)|Light Form|Forma de luz|modificada|
|[nqTM0MryJthdq3ZY.htm](agents-of-edgewatch-bestiary-items/nqTM0MryJthdq3ZY.htm)|Extra Reaction|Reacción adicional|modificada|
|[NRUwbsWBSVtCGgrp.htm](agents-of-edgewatch-bestiary-items/NRUwbsWBSVtCGgrp.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[nRzUgQJS26xT7C1q.htm](agents-of-edgewatch-bestiary-items/nRzUgQJS26xT7C1q.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[NsvkBq0sahLrYsTg.htm](agents-of-edgewatch-bestiary-items/NsvkBq0sahLrYsTg.htm)|Elven Curve Blade|Hoja curva élfica|modificada|
|[Ntt0pX48FlDnOFb3.htm](agents-of-edgewatch-bestiary-items/Ntt0pX48FlDnOFb3.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[nurJkCpZvHiPZo76.htm](agents-of-edgewatch-bestiary-items/nurJkCpZvHiPZo76.htm)|Project False Image|Proyectar imagen falsa|modificada|
|[NvOUKM0pOaTFyrWw.htm](agents-of-edgewatch-bestiary-items/NvOUKM0pOaTFyrWw.htm)|Close Cutter|Close Cutter|modificada|
|[nvUzHv6therpBM38.htm](agents-of-edgewatch-bestiary-items/nvUzHv6therpBM38.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[nW0moOpMRYhgNi82.htm](agents-of-edgewatch-bestiary-items/nW0moOpMRYhgNi82.htm)|Trunk|Tronco|modificada|
|[nXrzkclnA7IMvnFu.htm](agents-of-edgewatch-bestiary-items/nXrzkclnA7IMvnFu.htm)|Pitfall and Plunger|Trampa de caída y desatascador|modificada|
|[nXZygeRYzYRESgcP.htm](agents-of-edgewatch-bestiary-items/nXZygeRYzYRESgcP.htm)|Fist|Puño|modificada|
|[nyahaCV93O7YYyau.htm](agents-of-edgewatch-bestiary-items/nyahaCV93O7YYyau.htm)|Breach Vulnerability|Vulnerabilidad Emerger|modificada|
|[NyD5wXkVBGB2Wu0I.htm](agents-of-edgewatch-bestiary-items/NyD5wXkVBGB2Wu0I.htm)|Protean Anatomy 20|Protean Anatomy 20|modificada|
|[o03yd3s67EeP9WOh.htm](agents-of-edgewatch-bestiary-items/o03yd3s67EeP9WOh.htm)|Prescient Revision|Prescient Revision|modificada|
|[o11O0K7oIAQznUJo.htm](agents-of-edgewatch-bestiary-items/o11O0K7oIAQznUJo.htm)|Bloody Spew|Vómito Sangriento|modificada|
|[O19b688nMXlfxdQP.htm](agents-of-edgewatch-bestiary-items/O19b688nMXlfxdQP.htm)|Twin Takedown|Derribo gemelo|modificada|
|[o1pfLDPNAQy8FT0p.htm](agents-of-edgewatch-bestiary-items/o1pfLDPNAQy8FT0p.htm)|Point Blank|Point Blank|modificada|
|[o1vU8G41mZhtjXsb.htm](agents-of-edgewatch-bestiary-items/o1vU8G41mZhtjXsb.htm)|Sling|Sling|modificada|
|[O3FvbwzFjYGBuqWx.htm](agents-of-edgewatch-bestiary-items/O3FvbwzFjYGBuqWx.htm)|Shield Block|Bloquear con escudo|modificada|
|[O6wQ2EWNzgJGn6Ta.htm](agents-of-edgewatch-bestiary-items/O6wQ2EWNzgJGn6Ta.htm)|Hidden Paragon|Parangón oculto|modificada|
|[O97E3bY0dRr5pLCH.htm](agents-of-edgewatch-bestiary-items/O97E3bY0dRr5pLCH.htm)|Inflict Warpwave|Infligir Onda Warp|modificada|
|[oaYvNv83OSKkOI6V.htm](agents-of-edgewatch-bestiary-items/oaYvNv83OSKkOI6V.htm)|Performance Anxiety|Ansiedad de Interpretar|modificada|
|[oAz0VaKaZ2Hym3GL.htm](agents-of-edgewatch-bestiary-items/oAz0VaKaZ2Hym3GL.htm)|Dagger|Daga|modificada|
|[ob3CKat2QrsjJqn4.htm](agents-of-edgewatch-bestiary-items/ob3CKat2QrsjJqn4.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[oBDa2RafFTX6BBua.htm](agents-of-edgewatch-bestiary-items/oBDa2RafFTX6BBua.htm)|Grab|Agarrado|modificada|
|[ObuiuOqfP6nhvs9S.htm](agents-of-edgewatch-bestiary-items/ObuiuOqfP6nhvs9S.htm)|Tug|Tug|modificada|
|[OCmlinuwWYcoVWLc.htm](agents-of-edgewatch-bestiary-items/OCmlinuwWYcoVWLc.htm)|Scurry|Scurry|modificada|
|[odph4tUxJEzbuuiq.htm](agents-of-edgewatch-bestiary-items/odph4tUxJEzbuuiq.htm)|Impaler|Empalador|modificada|
|[OdVJynq0IATGWujh.htm](agents-of-edgewatch-bestiary-items/OdVJynq0IATGWujh.htm)|Foot|Pie|modificada|
|[oEHHDWURCYEb1t7W.htm](agents-of-edgewatch-bestiary-items/oEHHDWURCYEb1t7W.htm)|Jaws|Fauces|modificada|
|[OESsshXWIJSX1LE7.htm](agents-of-edgewatch-bestiary-items/OESsshXWIJSX1LE7.htm)|Blood-Fueled Titter|Blood-Fueled Titter|modificada|
|[of0eB3PZMwreV0L4.htm](agents-of-edgewatch-bestiary-items/of0eB3PZMwreV0L4.htm)|Furious Pacifier|Chupete Furioso|modificada|
|[OF5xPm4it251t0zo.htm](agents-of-edgewatch-bestiary-items/OF5xPm4it251t0zo.htm)|Reflective Plating|Revestimiento reflectante|modificada|
|[OF6N85WER1HxJgCK.htm](agents-of-edgewatch-bestiary-items/OF6N85WER1HxJgCK.htm)|Integrated Launcher|Lanzador integrado|modificada|
|[ofiTdcCz7bSlRXmy.htm](agents-of-edgewatch-bestiary-items/ofiTdcCz7bSlRXmy.htm)|Marrow Rot|Pudrición de médula|modificada|
|[oFx3vt7MvXsfxzSp.htm](agents-of-edgewatch-bestiary-items/oFx3vt7MvXsfxzSp.htm)|Fist|Puño|modificada|
|[OfYcuvZbwxbjF6cG.htm](agents-of-edgewatch-bestiary-items/OfYcuvZbwxbjF6cG.htm)|Vein Walker|Vein Walker|modificada|
|[oIOsV3m1VCQehtGp.htm](agents-of-edgewatch-bestiary-items/oIOsV3m1VCQehtGp.htm)|Magic Horn|Cuerno mágico|modificada|
|[OIT5GvqodAryKJpv.htm](agents-of-edgewatch-bestiary-items/OIT5GvqodAryKJpv.htm)|Sling|Sling|modificada|
|[ojlesPsjiDHjASWl.htm](agents-of-edgewatch-bestiary-items/ojlesPsjiDHjASWl.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[oJWZY02OG1m5LrP6.htm](agents-of-edgewatch-bestiary-items/oJWZY02OG1m5LrP6.htm)|Telekinetic Reach|Telekinetic Reach|modificada|
|[oKdyZTnDb5xbqpF3.htm](agents-of-edgewatch-bestiary-items/oKdyZTnDb5xbqpF3.htm)|Hamstring|Hamstring|modificada|
|[OmGZxI6hAdkJlkwY.htm](agents-of-edgewatch-bestiary-items/OmGZxI6hAdkJlkwY.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OMRu84X8qswYsTyJ.htm](agents-of-edgewatch-bestiary-items/OMRu84X8qswYsTyJ.htm)|Fangs|Colmillos|modificada|
|[OMtflrg7VM1ogHqZ.htm](agents-of-edgewatch-bestiary-items/OMtflrg7VM1ogHqZ.htm)|Tegresin's Greeting|Saludo de Tegresin|modificada|
|[OofTe2WDiLiEdYTm.htm](agents-of-edgewatch-bestiary-items/OofTe2WDiLiEdYTm.htm)|Halfling Luck|Suerte del mediano|modificada|
|[oOj2D6OXRFRGfvRp.htm](agents-of-edgewatch-bestiary-items/oOj2D6OXRFRGfvRp.htm)|Vomit Blood|Vomitar Sangre|modificada|
|[OpV99ABQlIY3FF4G.htm](agents-of-edgewatch-bestiary-items/OpV99ABQlIY3FF4G.htm)|Escort from the Premises|Escolta del Local|modificada|
|[oq1IG7RwkiKMV9mP.htm](agents-of-edgewatch-bestiary-items/oq1IG7RwkiKMV9mP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[OqEu80bVGGZXPfVs.htm](agents-of-edgewatch-bestiary-items/OqEu80bVGGZXPfVs.htm)|Devastating Blast|Explosión devastadora|modificada|
|[oQgehOwZMcJQ5i4V.htm](agents-of-edgewatch-bestiary-items/oQgehOwZMcJQ5i4V.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Os5QOeYEAod9Yg1T.htm](agents-of-edgewatch-bestiary-items/Os5QOeYEAod9Yg1T.htm)|Tentacle|Tentáculo|modificada|
|[osA98Km4yrxpF38T.htm](agents-of-edgewatch-bestiary-items/osA98Km4yrxpF38T.htm)|Steel Quill|Steel Quill|modificada|
|[ossSXTeiyXQFkTmA.htm](agents-of-edgewatch-bestiary-items/ossSXTeiyXQFkTmA.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OTSaRNEQPlVfldSC.htm](agents-of-edgewatch-bestiary-items/OTSaRNEQPlVfldSC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[oTX4OOEDagBTll1x.htm](agents-of-edgewatch-bestiary-items/oTX4OOEDagBTll1x.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[oXzm9KqLtbgKvm14.htm](agents-of-edgewatch-bestiary-items/oXzm9KqLtbgKvm14.htm)|Summon Elementals|Convocar elementales|modificada|
|[ozjGo4jmzdNhJfAA.htm](agents-of-edgewatch-bestiary-items/ozjGo4jmzdNhJfAA.htm)|Shortsword|Espada corta|modificada|
|[P2D3nIr4b8QtzEFx.htm](agents-of-edgewatch-bestiary-items/P2D3nIr4b8QtzEFx.htm)|Overflowing Boiling Water|Desbordamiento de agua hirviendo|modificada|
|[p777SbqGil3xrtzd.htm](agents-of-edgewatch-bestiary-items/p777SbqGil3xrtzd.htm)|Flay|Flay|modificada|
|[p8i5dtNaUJaS4W11.htm](agents-of-edgewatch-bestiary-items/p8i5dtNaUJaS4W11.htm)|Battle Axe|Hacha de batalla|modificada|
|[p8PqMvUhFUkgJL4b.htm](agents-of-edgewatch-bestiary-items/p8PqMvUhFUkgJL4b.htm)|Fist|Puño|modificada|
|[Pc7yhV1GBS53uadp.htm](agents-of-edgewatch-bestiary-items/Pc7yhV1GBS53uadp.htm)|Crossbow|Ballesta|modificada|
|[PCytOdylz3vMj2WE.htm](agents-of-edgewatch-bestiary-items/PCytOdylz3vMj2WE.htm)|Claw|Garra|modificada|
|[pEH5ZGr2J9sZg6ce.htm](agents-of-edgewatch-bestiary-items/pEH5ZGr2J9sZg6ce.htm)|Stabbing Beast Venom|Veneno de Bestia Apuñaladora|modificada|
|[pEr8VvYetu8yNn5t.htm](agents-of-edgewatch-bestiary-items/pEr8VvYetu8yNn5t.htm)|Efficient Winding|Efficient Winding|modificada|
|[PFD814sBXDi1v2I7.htm](agents-of-edgewatch-bestiary-items/PFD814sBXDi1v2I7.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[PfYigOWR2ynFziwC.htm](agents-of-edgewatch-bestiary-items/PfYigOWR2ynFziwC.htm)|Spear|Lanza|modificada|
|[pgSTLlpCYo6JjJeZ.htm](agents-of-edgewatch-bestiary-items/pgSTLlpCYo6JjJeZ.htm)|Improved Grab|Agarrado mejorado|modificada|
|[phWjGBiRvnlb7wDJ.htm](agents-of-edgewatch-bestiary-items/phWjGBiRvnlb7wDJ.htm)|Constrict|Restringir|modificada|
|[PigzABnoSkZuUAGP.htm](agents-of-edgewatch-bestiary-items/PigzABnoSkZuUAGP.htm)|Expanded Splash|Salpicadura extendida|modificada|
|[pJcNWcLAWBXtvefA.htm](agents-of-edgewatch-bestiary-items/pJcNWcLAWBXtvefA.htm)|Clockwork Reconstruction|Reconstrucción Reloj|modificada|
|[PJdaCe5La2FhB5AN.htm](agents-of-edgewatch-bestiary-items/PJdaCe5La2FhB5AN.htm)|Swift Leap|Salto veloz|modificada|
|[pjqAB7UZDOSJ8Dvg.htm](agents-of-edgewatch-bestiary-items/pjqAB7UZDOSJ8Dvg.htm)|Scissors|Tijeras|modificada|
|[pk7WhgmEvUXgGRiz.htm](agents-of-edgewatch-bestiary-items/pk7WhgmEvUXgGRiz.htm)|No MAP|No MAP|modificada|
|[pl4CFdDU1X88olbS.htm](agents-of-edgewatch-bestiary-items/pl4CFdDU1X88olbS.htm)|Captive Rake|Desgarrar cautivo|modificada|
|[PlFu6E3ZCVF3JCam.htm](agents-of-edgewatch-bestiary-items/PlFu6E3ZCVF3JCam.htm)|Nightmare Rider|Jinete de pesadilla|modificada|
|[pNlE1um147bT0E6P.htm](agents-of-edgewatch-bestiary-items/pNlE1um147bT0E6P.htm)|Animated Statues|Estatua Animada|modificada|
|[pnx6alHWYJ3kLnBW.htm](agents-of-edgewatch-bestiary-items/pnx6alHWYJ3kLnBW.htm)|Proven Devotion|Devoción Probada|modificada|
|[PoQgBkVgVkqOOV5e.htm](agents-of-edgewatch-bestiary-items/PoQgBkVgVkqOOV5e.htm)|Fling Offal|Lanzar por los aires despojos|modificada|
|[PQM8ukSoaiRa6PDB.htm](agents-of-edgewatch-bestiary-items/PQM8ukSoaiRa6PDB.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[psPKDFy6x0wj7ko0.htm](agents-of-edgewatch-bestiary-items/psPKDFy6x0wj7ko0.htm)|Cleaver|Cleaver|modificada|
|[Ptf4oQIP2T6623QL.htm](agents-of-edgewatch-bestiary-items/Ptf4oQIP2T6623QL.htm)|Leaping Charge|Carga en salto|modificada|
|[PtGiCuINX35GsQBg.htm](agents-of-edgewatch-bestiary-items/PtGiCuINX35GsQBg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pu72YVc5Xv4KyIqf.htm](agents-of-edgewatch-bestiary-items/pu72YVc5Xv4KyIqf.htm)|Dagger|Daga|modificada|
|[pUqjSa0MTNAe7fLX.htm](agents-of-edgewatch-bestiary-items/pUqjSa0MTNAe7fLX.htm)|Guillotine Blade|Cuchilla Guillotina|modificada|
|[PvgGyV08NoFmrceo.htm](agents-of-edgewatch-bestiary-items/PvgGyV08NoFmrceo.htm)|Jaws|Fauces|modificada|
|[PvjXCmuXDpKKHSIf.htm](agents-of-edgewatch-bestiary-items/PvjXCmuXDpKKHSIf.htm)|Innate Primal Spells|Innate Primal Spells|modificada|
|[pWqtIfqDJ0Fag2CW.htm](agents-of-edgewatch-bestiary-items/pWqtIfqDJ0Fag2CW.htm)|Waylay|Waylay|modificada|
|[PWYoCF0aSsWVwxCI.htm](agents-of-edgewatch-bestiary-items/PWYoCF0aSsWVwxCI.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[PxaUCfocdE8pfDRJ.htm](agents-of-edgewatch-bestiary-items/PxaUCfocdE8pfDRJ.htm)|Dagger|Daga|modificada|
|[pxtdz9Cb1KUHXsrM.htm](agents-of-edgewatch-bestiary-items/pxtdz9Cb1KUHXsrM.htm)|Corpse Wallow|Corpse Wallow|modificada|
|[py2SpejCikEONHQm.htm](agents-of-edgewatch-bestiary-items/py2SpejCikEONHQm.htm)|Constant Spells|Constant Spells|modificada|
|[q0zeFHZ9WaJ92IKN.htm](agents-of-edgewatch-bestiary-items/q0zeFHZ9WaJ92IKN.htm)|Jaws|Fauces|modificada|
|[Q10SWwitCSafoJ4L.htm](agents-of-edgewatch-bestiary-items/Q10SWwitCSafoJ4L.htm)|Shared Diversion|Desvío compartido|modificada|
|[Q1MxJ1mg3fC5zwcl.htm](agents-of-edgewatch-bestiary-items/Q1MxJ1mg3fC5zwcl.htm)|Grab|Agarrado|modificada|
|[q47XrkqT8jn5rAqq.htm](agents-of-edgewatch-bestiary-items/q47XrkqT8jn5rAqq.htm)|Grab|Agarrado|modificada|
|[Q4P1CH4AQAlYpZ0X.htm](agents-of-edgewatch-bestiary-items/Q4P1CH4AQAlYpZ0X.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[q4WljID3qrZ5Fpnr.htm](agents-of-edgewatch-bestiary-items/q4WljID3qrZ5Fpnr.htm)|Wind-Up|Wind-Up|modificada|
|[qALAo7rXGBdfG3m4.htm](agents-of-edgewatch-bestiary-items/qALAo7rXGBdfG3m4.htm)|Dagger|Daga|modificada|
|[Qc7N0GKOoFezaIK1.htm](agents-of-edgewatch-bestiary-items/Qc7N0GKOoFezaIK1.htm)|Fist|Puño|modificada|
|[QCKNcu26UUfwBynU.htm](agents-of-edgewatch-bestiary-items/QCKNcu26UUfwBynU.htm)|Calculated Splash|Salpicadura calculada|modificada|
|[qCVeUkgwYXq7lu8z.htm](agents-of-edgewatch-bestiary-items/qCVeUkgwYXq7lu8z.htm)|Bloody Handprint|Bloody Handprint|modificada|
|[QDbM7WGGXh21h9Xw.htm](agents-of-edgewatch-bestiary-items/QDbM7WGGXh21h9Xw.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[qFILNq2kmX5xF0Dt.htm](agents-of-edgewatch-bestiary-items/qFILNq2kmX5xF0Dt.htm)|Black Ink Delirium|Delirio de tinta negra|modificada|
|[Qg91eQ3wUA0CISzT.htm](agents-of-edgewatch-bestiary-items/Qg91eQ3wUA0CISzT.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[QgE3ujjsFZSthy7z.htm](agents-of-edgewatch-bestiary-items/QgE3ujjsFZSthy7z.htm)|Illusory Copies|Copias Ilusorias|modificada|
|[qgStk0bh308sgMGP.htm](agents-of-edgewatch-bestiary-items/qgStk0bh308sgMGP.htm)|Dagger|Daga|modificada|
|[qgu0AUWtGhup6Nyf.htm](agents-of-edgewatch-bestiary-items/qgu0AUWtGhup6Nyf.htm)|Spine|Spine|modificada|
|[qHyTxio8Rf0fGBM5.htm](agents-of-edgewatch-bestiary-items/qHyTxio8Rf0fGBM5.htm)|Constant Spells|Constant Spells|modificada|
|[qi5ozQVZsQZavdpC.htm](agents-of-edgewatch-bestiary-items/qi5ozQVZsQZavdpC.htm)|Moderate Acid Flask|Frasco de ácido moderado|modificada|
|[QJApLqpe9NsQa7B2.htm](agents-of-edgewatch-bestiary-items/QJApLqpe9NsQa7B2.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[qLsi1de5hGC6uHjL.htm](agents-of-edgewatch-bestiary-items/qLsi1de5hGC6uHjL.htm)|Spell Ambush|Emboscada sortílega.|modificada|
|[QMeKA8gRtzqiRWQ5.htm](agents-of-edgewatch-bestiary-items/QMeKA8gRtzqiRWQ5.htm)|Tail|Tail|modificada|
|[QMmywYM7lzMllSGS.htm](agents-of-edgewatch-bestiary-items/QMmywYM7lzMllSGS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qnVCozwzjFK6gieU.htm](agents-of-edgewatch-bestiary-items/qnVCozwzjFK6gieU.htm)|Dagger|Daga|modificada|
|[qOiRJ4goVAPViPIt.htm](agents-of-edgewatch-bestiary-items/qOiRJ4goVAPViPIt.htm)|Hurl|Hurl|modificada|
|[qoxTaaEWLHDHtzML.htm](agents-of-edgewatch-bestiary-items/qoxTaaEWLHDHtzML.htm)|Claw|Garra|modificada|
|[QozBQD2QZP7kMYOG.htm](agents-of-edgewatch-bestiary-items/QozBQD2QZP7kMYOG.htm)|Control Body|Cuerpo de control|modificada|
|[QP06eTmf8XusaVPV.htm](agents-of-edgewatch-bestiary-items/QP06eTmf8XusaVPV.htm)|Explode|Explotar|modificada|
|[QprgJpKB6Wfofr3t.htm](agents-of-edgewatch-bestiary-items/QprgJpKB6Wfofr3t.htm)|Focus Spells|Conjuros de foco|modificada|
|[QSKXWJ8br4qwHbtf.htm](agents-of-edgewatch-bestiary-items/QSKXWJ8br4qwHbtf.htm)|Paralysis|Parálisis|modificada|
|[quBDtUjWWKx7qiFU.htm](agents-of-edgewatch-bestiary-items/quBDtUjWWKx7qiFU.htm)|Innate Primal Spells|Innate Primal Spells|modificada|
|[Quf28S7PJstH1L0b.htm](agents-of-edgewatch-bestiary-items/Quf28S7PJstH1L0b.htm)|Spill Eyeballs|Derramar globos oculares|modificada|
|[qUO8L9YDXGIF28f3.htm](agents-of-edgewatch-bestiary-items/qUO8L9YDXGIF28f3.htm)|Feeblemind Ray|Rayo debilidad mental|modificada|
|[QURekRpCFthbGcMS.htm](agents-of-edgewatch-bestiary-items/QURekRpCFthbGcMS.htm)|Scatterbrain Palm|Scatterbrain Palm|modificada|
|[Qv8OyAidG7v9lnwp.htm](agents-of-edgewatch-bestiary-items/Qv8OyAidG7v9lnwp.htm)|Tear Flesh|Desgarrar Carne|modificada|
|[qVB5KclnPFeoDGPo.htm](agents-of-edgewatch-bestiary-items/qVB5KclnPFeoDGPo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r1GptaN7xdyOuJwe.htm](agents-of-edgewatch-bestiary-items/r1GptaN7xdyOuJwe.htm)|Tail|Tail|modificada|
|[R1OmPKgnrpEPWGr6.htm](agents-of-edgewatch-bestiary-items/R1OmPKgnrpEPWGr6.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[r2CSyvxtD75InoFP.htm](agents-of-edgewatch-bestiary-items/r2CSyvxtD75InoFP.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[r47iaCjc2jC8iNnL.htm](agents-of-edgewatch-bestiary-items/r47iaCjc2jC8iNnL.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[r7vHhfdRSulBJtov.htm](agents-of-edgewatch-bestiary-items/r7vHhfdRSulBJtov.htm)|Truth Vulnerability|Vulnerabilidad a la verdad|modificada|
|[RB7QxnNxpj30rjOL.htm](agents-of-edgewatch-bestiary-items/RB7QxnNxpj30rjOL.htm)|Constant Spells|Constant Spells|modificada|
|[rbbCn6nAeaPo4dUD.htm](agents-of-edgewatch-bestiary-items/rbbCn6nAeaPo4dUD.htm)|Pseudopod|Pseudópodo|modificada|
|[rBy8DTqkrMtivM2r.htm](agents-of-edgewatch-bestiary-items/rBy8DTqkrMtivM2r.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[rDNFst5KVOAvifdy.htm](agents-of-edgewatch-bestiary-items/rDNFst5KVOAvifdy.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[rDNKRU6LA2e9G96b.htm](agents-of-edgewatch-bestiary-items/rDNKRU6LA2e9G96b.htm)|Jaws|Fauces|modificada|
|[REB6Z7mAMxxSKiun.htm](agents-of-edgewatch-bestiary-items/REB6Z7mAMxxSKiun.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RfaP98Gi8H94iPk3.htm](agents-of-edgewatch-bestiary-items/RfaP98Gi8H94iPk3.htm)|Crossbow|Ballesta|modificada|
|[RgfNXAlieDWY02I4.htm](agents-of-edgewatch-bestiary-items/RgfNXAlieDWY02I4.htm)|Alchemical Bomb|Bomba alquímica|modificada|
|[RGHyYuZOOFXsZGfS.htm](agents-of-edgewatch-bestiary-items/RGHyYuZOOFXsZGfS.htm)|Fist|Puño|modificada|
|[rhQHRT4MPNImnl8e.htm](agents-of-edgewatch-bestiary-items/rhQHRT4MPNImnl8e.htm)|True Appearance|Apariencia verdadera|modificada|
|[RI07SPUsr3EfIXQX.htm](agents-of-edgewatch-bestiary-items/RI07SPUsr3EfIXQX.htm)|Tail|Tail|modificada|
|[RI1LAadtF6vq96g2.htm](agents-of-edgewatch-bestiary-items/RI1LAadtF6vq96g2.htm)|Claw|Garra|modificada|
|[Rif86iiefabTTeXm.htm](agents-of-edgewatch-bestiary-items/Rif86iiefabTTeXm.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[RiF9Uw43dbvL8TEa.htm](agents-of-edgewatch-bestiary-items/RiF9Uw43dbvL8TEa.htm)|Consume Poison|Consumir Veneno|modificada|
|[rJILDneSIHcw62xJ.htm](agents-of-edgewatch-bestiary-items/rJILDneSIHcw62xJ.htm)|Dagger|Daga|modificada|
|[rJu8a6w24iFChezA.htm](agents-of-edgewatch-bestiary-items/rJu8a6w24iFChezA.htm)|Transfusion Aura|Transfusion Aura|modificada|
|[rJXLqXZbLYPfyPpc.htm](agents-of-edgewatch-bestiary-items/rJXLqXZbLYPfyPpc.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[RkUX9VYzuGsIycUb.htm](agents-of-edgewatch-bestiary-items/RkUX9VYzuGsIycUb.htm)|Blood Scent|Blood Scent|modificada|
|[rLFEG0L8t229JOcM.htm](agents-of-edgewatch-bestiary-items/rLFEG0L8t229JOcM.htm)|Jaws|Fauces|modificada|
|[rlsHyYTiGdVwRStH.htm](agents-of-edgewatch-bestiary-items/rlsHyYTiGdVwRStH.htm)|Grab|Agarrado|modificada|
|[rlZ9VxWPwn9XfGgL.htm](agents-of-edgewatch-bestiary-items/rlZ9VxWPwn9XfGgL.htm)|Breath Weapon|Breath Weapon|modificada|
|[rMKwSTFsZNjo251o.htm](agents-of-edgewatch-bestiary-items/rMKwSTFsZNjo251o.htm)|Dueling Parry|Parada de duelo|modificada|
|[RPBXXyVoR76aOb4H.htm](agents-of-edgewatch-bestiary-items/RPBXXyVoR76aOb4H.htm)|Rapier|Estoque|modificada|
|[RPGIrySxh4Qa0yaP.htm](agents-of-edgewatch-bestiary-items/RPGIrySxh4Qa0yaP.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[rpruEIwmxeOzTZOp.htm](agents-of-edgewatch-bestiary-items/rpruEIwmxeOzTZOp.htm)|Dream Haunting|Acoso onírico del Sueño|modificada|
|[rPZdz4pgzoaAanDn.htm](agents-of-edgewatch-bestiary-items/rPZdz4pgzoaAanDn.htm)|Swallow Whole|Engullir Todo|modificada|
|[RQ0R47s8yyK8gYS8.htm](agents-of-edgewatch-bestiary-items/RQ0R47s8yyK8gYS8.htm)|Stretching Step|Estiramiento Paso|modificada|
|[rr5bpEab2TNVmDUt.htm](agents-of-edgewatch-bestiary-items/rr5bpEab2TNVmDUt.htm)|Constrict|Restringir|modificada|
|[RSzJjCjwpz8jIS3J.htm](agents-of-edgewatch-bestiary-items/RSzJjCjwpz8jIS3J.htm)|Iron Golem Poison|Veneno de gólem de hierro|modificada|
|[rtBp1EzJQp3x5EsU.htm](agents-of-edgewatch-bestiary-items/rtBp1EzJQp3x5EsU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rwp43QY136LRMqdW.htm](agents-of-edgewatch-bestiary-items/rwp43QY136LRMqdW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rWzEv0M5A3nxCW6P.htm](agents-of-edgewatch-bestiary-items/rWzEv0M5A3nxCW6P.htm)|Constant Spells|Constant Spells|modificada|
|[rxkGSEBo9iXMnETl.htm](agents-of-edgewatch-bestiary-items/rxkGSEBo9iXMnETl.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[rY3uWiPQqOCSmYTu.htm](agents-of-edgewatch-bestiary-items/rY3uWiPQqOCSmYTu.htm)|Lore Master|Lore Master|modificada|
|[Ry9m2AVrebalVrSx.htm](agents-of-edgewatch-bestiary-items/Ry9m2AVrebalVrSx.htm)|Fist|Puño|modificada|
|[RywUmURTSevVYI7y.htm](agents-of-edgewatch-bestiary-items/RywUmURTSevVYI7y.htm)|Poisoner's Staff|Báculo de envenenador|modificada|
|[rZ6LtOq5W7Ef8R1W.htm](agents-of-edgewatch-bestiary-items/rZ6LtOq5W7Ef8R1W.htm)|Whirling Slashes|Whirling Slashes|modificada|
|[S00gbcUTroHxccVJ.htm](agents-of-edgewatch-bestiary-items/S00gbcUTroHxccVJ.htm)|Pummeling Assault|Pummeling Assault|modificada|
|[s1576Kzeq8JMVNz7.htm](agents-of-edgewatch-bestiary-items/s1576Kzeq8JMVNz7.htm)|Empathetic Response|Respuesta empática|modificada|
|[s4YFi2Scm3NoZSHU.htm](agents-of-edgewatch-bestiary-items/s4YFi2Scm3NoZSHU.htm)|Darting Flurry|Ráfaga de Dardos|modificada|
|[s5ejDTLYAXnRuoub.htm](agents-of-edgewatch-bestiary-items/s5ejDTLYAXnRuoub.htm)|Jaws|Fauces|modificada|
|[s6jGCfKSGTptJo1G.htm](agents-of-edgewatch-bestiary-items/s6jGCfKSGTptJo1G.htm)|Blinding Bile|Bilis cegadora|modificada|
|[s7eTzERmLr5TUCJL.htm](agents-of-edgewatch-bestiary-items/s7eTzERmLr5TUCJL.htm)|Jaws|Fauces|modificada|
|[s7QApXyE5DjGF9aY.htm](agents-of-edgewatch-bestiary-items/s7QApXyE5DjGF9aY.htm)|Morningstar|Morningstar|modificada|
|[S8lzbxGXfumHXYYq.htm](agents-of-edgewatch-bestiary-items/S8lzbxGXfumHXYYq.htm)|Flay|Flay|modificada|
|[sbYDSGZlPbw5odWZ.htm](agents-of-edgewatch-bestiary-items/sbYDSGZlPbw5odWZ.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[SczLJpfQwYYTKEvD.htm](agents-of-edgewatch-bestiary-items/SczLJpfQwYYTKEvD.htm)|Watery Simulacra|Watery Simulacra|modificada|
|[sDzBK6rs6LhvAFPu.htm](agents-of-edgewatch-bestiary-items/sDzBK6rs6LhvAFPu.htm)|All-Around Vision|All-Around Vision|modificada|
|[SEDzDCcVcLItgSUy.htm](agents-of-edgewatch-bestiary-items/SEDzDCcVcLItgSUy.htm)|Reap|Segar|modificada|
|[Sf1u8xUKN1LLV2FB.htm](agents-of-edgewatch-bestiary-items/Sf1u8xUKN1LLV2FB.htm)|Grab|Agarrado|modificada|
|[sFeJf8GnNPDabG1D.htm](agents-of-edgewatch-bestiary-items/sFeJf8GnNPDabG1D.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[SGQATSbzafV9SYlH.htm](agents-of-edgewatch-bestiary-items/SGQATSbzafV9SYlH.htm)|Coordinated Distraction|Distracción Coordinada|modificada|
|[sgRdRygQY6ggn4fy.htm](agents-of-edgewatch-bestiary-items/sgRdRygQY6ggn4fy.htm)|Change Shape|Change Shape|modificada|
|[SGuyP6Rc9SZRoRYv.htm](agents-of-edgewatch-bestiary-items/SGuyP6Rc9SZRoRYv.htm)|Lesser Alchemist's Fire|Fuego de alquimista menor|modificada|
|[shJIHzhh4j6cG3tv.htm](agents-of-edgewatch-bestiary-items/shJIHzhh4j6cG3tv.htm)|Flurry of Claws|Ráfaga de Garras|modificada|
|[siibhLVqReWpmq6r.htm](agents-of-edgewatch-bestiary-items/siibhLVqReWpmq6r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SJraLZmz7x1C04ki.htm](agents-of-edgewatch-bestiary-items/SJraLZmz7x1C04ki.htm)|Vulnerable to Rust|Vulnerable a Rust|modificada|
|[SLWi3J2O0e3QXfYB.htm](agents-of-edgewatch-bestiary-items/SLWi3J2O0e3QXfYB.htm)|Clobber|Clobber|modificada|
|[SMg5BMoXiwN9nxmJ.htm](agents-of-edgewatch-bestiary-items/SMg5BMoXiwN9nxmJ.htm)|Running Reload|Recarga a la carrera|modificada|
|[SMwhRQyOrogJbUal.htm](agents-of-edgewatch-bestiary-items/SMwhRQyOrogJbUal.htm)|Lifedrinker|Absorbedor vida|modificada|
|[snJ95Lkh1TmrNndQ.htm](agents-of-edgewatch-bestiary-items/snJ95Lkh1TmrNndQ.htm)|Splash of Color|Splash of Color|modificada|
|[SNqbJ3OBpkCDVRSr.htm](agents-of-edgewatch-bestiary-items/SNqbJ3OBpkCDVRSr.htm)|Shortsword|Espada corta|modificada|
|[sODZwisWeLEBKV8P.htm](agents-of-edgewatch-bestiary-items/sODZwisWeLEBKV8P.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[SoEJRrsphmHa4aoG.htm](agents-of-edgewatch-bestiary-items/SoEJRrsphmHa4aoG.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[spdJfofzwtFFM3Cs.htm](agents-of-edgewatch-bestiary-items/spdJfofzwtFFM3Cs.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[SriOV0CxqQFmpPCv.htm](agents-of-edgewatch-bestiary-items/SriOV0CxqQFmpPCv.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[STPSnFrl7yD39Fni.htm](agents-of-edgewatch-bestiary-items/STPSnFrl7yD39Fni.htm)|Holy Greatsword|Sagrada Gran Espada|modificada|
|[sTXhA6HFH3RVnqjV.htm](agents-of-edgewatch-bestiary-items/sTXhA6HFH3RVnqjV.htm)|Final Blow|Golpe Final|modificada|
|[sVBakTcoJU3kPGlY.htm](agents-of-edgewatch-bestiary-items/sVBakTcoJU3kPGlY.htm)|Whirlwind Kick|Patada Torbellino|modificada|
|[SvCc6HA58iZaOg9T.htm](agents-of-edgewatch-bestiary-items/SvCc6HA58iZaOg9T.htm)|Unrelativity Field|Unrelativity Field|modificada|
|[sZjoXHynGmaJPLpi.htm](agents-of-edgewatch-bestiary-items/sZjoXHynGmaJPLpi.htm)|Combustible|Combustible|modificada|
|[sZRQVUQ7xiN6ZTK9.htm](agents-of-edgewatch-bestiary-items/sZRQVUQ7xiN6ZTK9.htm)|Grab|Agarrado|modificada|
|[T2NZbbINwDvovbim.htm](agents-of-edgewatch-bestiary-items/T2NZbbINwDvovbim.htm)|Darkvision|Visión en la oscuridad|modificada|
|[T2pFSnOpz24hsKkz.htm](agents-of-edgewatch-bestiary-items/T2pFSnOpz24hsKkz.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[T4zHj7DFiaCBwf6f.htm](agents-of-edgewatch-bestiary-items/T4zHj7DFiaCBwf6f.htm)|Go Dark|Oscurecerse|modificada|
|[t5jAYbO0kLhWne6I.htm](agents-of-edgewatch-bestiary-items/t5jAYbO0kLhWne6I.htm)|Fist|Puño|modificada|
|[t5skSqnTS4aKuuan.htm](agents-of-edgewatch-bestiary-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[t7R0LcoxHegwmHyq.htm](agents-of-edgewatch-bestiary-items/t7R0LcoxHegwmHyq.htm)|Rapier|Estoque|modificada|
|[T8yXNvNM2mPGZfoq.htm](agents-of-edgewatch-bestiary-items/T8yXNvNM2mPGZfoq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[TADC3gK2VlA4Jp9l.htm](agents-of-edgewatch-bestiary-items/TADC3gK2VlA4Jp9l.htm)|Mirror Duplicate|Espejo Duplicado|modificada|
|[TAic4n8t4egJtNJX.htm](agents-of-edgewatch-bestiary-items/TAic4n8t4egJtNJX.htm)|Deny Advantage|Denegar ventaja|modificada|
|[TBb2TTPl5fRKJQJU.htm](agents-of-edgewatch-bestiary-items/TBb2TTPl5fRKJQJU.htm)|Grasping Bites|Muerdemuerde|modificada|
|[Tbf45tR5JDVDyJCc.htm](agents-of-edgewatch-bestiary-items/Tbf45tR5JDVDyJCc.htm)|Rotting Aura|Rotting Aura|modificada|
|[tBGcGGksEkKqFPe7.htm](agents-of-edgewatch-bestiary-items/tBGcGGksEkKqFPe7.htm)|Alietta|Alietta|modificada|
|[TcOqYdc5uITUBa9m.htm](agents-of-edgewatch-bestiary-items/TcOqYdc5uITUBa9m.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[TeXV6pOiXpdWsvDV.htm](agents-of-edgewatch-bestiary-items/TeXV6pOiXpdWsvDV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TfGlX9oclwIPR62r.htm](agents-of-edgewatch-bestiary-items/TfGlX9oclwIPR62r.htm)|Toxin-Inured|Toxin-Inured|modificada|
|[Tfnx5jxpKaKtZ7ES.htm](agents-of-edgewatch-bestiary-items/Tfnx5jxpKaKtZ7ES.htm)|Choose Weakness|Elegir debilidad|modificada|
|[TFvB8Q4vaggsZeH2.htm](agents-of-edgewatch-bestiary-items/TFvB8Q4vaggsZeH2.htm)|Infused Items|Equipos infundidos|modificada|
|[TGZnX69H3vH84pPZ.htm](agents-of-edgewatch-bestiary-items/TGZnX69H3vH84pPZ.htm)|Ooze Tendril|Ooze Tendril|modificada|
|[ThVw3eJTb5RzjlG8.htm](agents-of-edgewatch-bestiary-items/ThVw3eJTb5RzjlG8.htm)|Rapier Hand|Rapier Hand|modificada|
|[tIl9HHbyeahh21jc.htm](agents-of-edgewatch-bestiary-items/tIl9HHbyeahh21jc.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[Tillz0YYXHueJmui.htm](agents-of-edgewatch-bestiary-items/Tillz0YYXHueJmui.htm)|Dimensional Slide|Dimensional Slide|modificada|
|[TiZR6umibymo5txc.htm](agents-of-edgewatch-bestiary-items/TiZR6umibymo5txc.htm)|Sickle|Hoz|modificada|
|[Tk7abWUpvDDV6Qg0.htm](agents-of-edgewatch-bestiary-items/Tk7abWUpvDDV6Qg0.htm)|Succor Vulnerability|Vulnerabilidad al auxilio|modificada|
|[TLLEMhBLZebrCg2V.htm](agents-of-edgewatch-bestiary-items/TLLEMhBLZebrCg2V.htm)|Clutching Cobbles|Clutching Cobbles|modificada|
|[TmEWLqRkXgHmnIV4.htm](agents-of-edgewatch-bestiary-items/TmEWLqRkXgHmnIV4.htm)|Change Shape|Change Shape|modificada|
|[tMhjT7tf9r9vZX4C.htm](agents-of-edgewatch-bestiary-items/tMhjT7tf9r9vZX4C.htm)|Vargouille Transformation|Vargouille Transformation|modificada|
|[tnOFl9MIpqf5OkVX.htm](agents-of-edgewatch-bestiary-items/tnOFl9MIpqf5OkVX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Tq4JjTyPFg3Q57Z4.htm](agents-of-edgewatch-bestiary-items/Tq4JjTyPFg3Q57Z4.htm)|Trident|Trident|modificada|
|[tvVxQhJ0X1FuWMhv.htm](agents-of-edgewatch-bestiary-items/tvVxQhJ0X1FuWMhv.htm)|Dagger|Daga|modificada|
|[tWZVevRequPfr7TC.htm](agents-of-edgewatch-bestiary-items/tWZVevRequPfr7TC.htm)|Innate Primal Spells|Innate Primal Spells|modificada|
|[TxhGff0LtzQslOci.htm](agents-of-edgewatch-bestiary-items/TxhGff0LtzQslOci.htm)|Easy to Call|Fácil de Llamar|modificada|
|[TXU2NotAQBux9iMZ.htm](agents-of-edgewatch-bestiary-items/TXU2NotAQBux9iMZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tyx67m7w5wjz8Xc5.htm](agents-of-edgewatch-bestiary-items/tyx67m7w5wjz8Xc5.htm)|Spear|Lanza|modificada|
|[tZNd4FllHFLFPMgx.htm](agents-of-edgewatch-bestiary-items/tZNd4FllHFLFPMgx.htm)|Claw|Garra|modificada|
|[U0v9mqG5NfAe3NgW.htm](agents-of-edgewatch-bestiary-items/U0v9mqG5NfAe3NgW.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[U0xgJtIHGK7aaxUB.htm](agents-of-edgewatch-bestiary-items/U0xgJtIHGK7aaxUB.htm)|Negative Healing|Curación negativa|modificada|
|[U1E1Xka1zLQxC4tT.htm](agents-of-edgewatch-bestiary-items/U1E1Xka1zLQxC4tT.htm)|Activate Attractor|Activar Atractor|modificada|
|[u1ffp4yNEVawu9dE.htm](agents-of-edgewatch-bestiary-items/u1ffp4yNEVawu9dE.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[u4BY6CXSH5AXUTMv.htm](agents-of-edgewatch-bestiary-items/u4BY6CXSH5AXUTMv.htm)|Continuous Barrage|Andanada continua|modificada|
|[u4Or7r9xHL9MH7uK.htm](agents-of-edgewatch-bestiary-items/u4Or7r9xHL9MH7uK.htm)|Negative Healing|Curación negativa|modificada|
|[U5JrBAObBwJKEgSJ.htm](agents-of-edgewatch-bestiary-items/U5JrBAObBwJKEgSJ.htm)|Claw|Garra|modificada|
|[U64bIK9noEw8M2Ro.htm](agents-of-edgewatch-bestiary-items/U64bIK9noEw8M2Ro.htm)|Blood Chain|Cadena de sangre|modificada|
|[U7PjzQ5AjISpvEhR.htm](agents-of-edgewatch-bestiary-items/U7PjzQ5AjISpvEhR.htm)|Greatpick|Greatpick|modificada|
|[U8JHbhefcDKeNERu.htm](agents-of-edgewatch-bestiary-items/U8JHbhefcDKeNERu.htm)|Constant Spells|Constant Spells|modificada|
|[UAnT1rt4pcCFs6br.htm](agents-of-edgewatch-bestiary-items/UAnT1rt4pcCFs6br.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[udFVDwS0vDvYW3tw.htm](agents-of-edgewatch-bestiary-items/udFVDwS0vDvYW3tw.htm)|Bloody Rebel|Bloody Rebel|modificada|
|[uffmVAC7FAC8GDmU.htm](agents-of-edgewatch-bestiary-items/uffmVAC7FAC8GDmU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[UFTgVHYhhpDzdMpd.htm](agents-of-edgewatch-bestiary-items/UFTgVHYhhpDzdMpd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uIpZm7bCNAs8c7BV.htm](agents-of-edgewatch-bestiary-items/uIpZm7bCNAs8c7BV.htm)|Vorpal Garrote|Vorpal Garrote|modificada|
|[UiWgpQaBrzfpuuvN.htm](agents-of-edgewatch-bestiary-items/UiWgpQaBrzfpuuvN.htm)|Dagger|Daga|modificada|
|[ukjOJLhlz5hOZcuy.htm](agents-of-edgewatch-bestiary-items/ukjOJLhlz5hOZcuy.htm)|Crystallize Blood|Cristalizar Sangre|modificada|
|[uLs1qWcXHyZE14d2.htm](agents-of-edgewatch-bestiary-items/uLs1qWcXHyZE14d2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uOIdVHoijKlxOrxM.htm](agents-of-edgewatch-bestiary-items/uOIdVHoijKlxOrxM.htm)|Abyssal Plague|Plaga abisal|modificada|
|[UOmYO7g7ej3NwdzH.htm](agents-of-edgewatch-bestiary-items/UOmYO7g7ej3NwdzH.htm)|Protean Anatomy 25|Protean Anatomy 25|modificada|
|[uP5k61RUIateqmiN.htm](agents-of-edgewatch-bestiary-items/uP5k61RUIateqmiN.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[UP6qek6RNSS32oOG.htm](agents-of-edgewatch-bestiary-items/UP6qek6RNSS32oOG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UTB8Ay3kwYWUQkpJ.htm](agents-of-edgewatch-bestiary-items/UTB8Ay3kwYWUQkpJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[utBhBKO5t1Pl4TVo.htm](agents-of-edgewatch-bestiary-items/utBhBKO5t1Pl4TVo.htm)|Planar Coven|Planar Coven|modificada|
|[UUw22Ccx1nqHZgFR.htm](agents-of-edgewatch-bestiary-items/UUw22Ccx1nqHZgFR.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[uv8lYUKGJVNvxD5Q.htm](agents-of-edgewatch-bestiary-items/uv8lYUKGJVNvxD5Q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uVoFX3A8pbUaPYi4.htm](agents-of-edgewatch-bestiary-items/uVoFX3A8pbUaPYi4.htm)|Divine Rituals|Rituales Divinos|modificada|
|[uW2CbK6jDKtg1sG4.htm](agents-of-edgewatch-bestiary-items/uW2CbK6jDKtg1sG4.htm)|Storm Bringer|Portador de Tormentas|modificada|
|[UX4F0eelHvADT40O.htm](agents-of-edgewatch-bestiary-items/UX4F0eelHvADT40O.htm)|Gearblade|Gearblade|modificada|
|[uXNPc5LpmbvMlOa5.htm](agents-of-edgewatch-bestiary-items/uXNPc5LpmbvMlOa5.htm)|Breathe Death|Respira Muerte|modificada|
|[UylfhYpRBAz42DrH.htm](agents-of-edgewatch-bestiary-items/UylfhYpRBAz42DrH.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[UYn0m6msQksZeSqS.htm](agents-of-edgewatch-bestiary-items/UYn0m6msQksZeSqS.htm)|Exorcism|Exorcismo|modificada|
|[uypEiarY3kwsOyuI.htm](agents-of-edgewatch-bestiary-items/uypEiarY3kwsOyuI.htm)|Divine Rituals|Rituales Divinos|modificada|
|[uYvjUwSl8dqCBiVf.htm](agents-of-edgewatch-bestiary-items/uYvjUwSl8dqCBiVf.htm)|Temple Sword|Temple Sword|modificada|
|[uzwUjI3gD8tO8Chh.htm](agents-of-edgewatch-bestiary-items/uzwUjI3gD8tO8Chh.htm)|Vulnerable to Flesh to Stone|Vulnerable a De la carne a la piedra|modificada|
|[V3S7DLZ4aWxgcg1R.htm](agents-of-edgewatch-bestiary-items/V3S7DLZ4aWxgcg1R.htm)|Poison Ink|Tinta Venenosa|modificada|
|[vA2R6FhhaArEEKGJ.htm](agents-of-edgewatch-bestiary-items/vA2R6FhhaArEEKGJ.htm)|Claw|Garra|modificada|
|[va3CnUqjuHNbajzr.htm](agents-of-edgewatch-bestiary-items/va3CnUqjuHNbajzr.htm)|Impromptu Toxin|Toxina Improvisada|modificada|
|[vCow6RPYhJCBoynk.htm](agents-of-edgewatch-bestiary-items/vCow6RPYhJCBoynk.htm)|Staff|Báculo|modificada|
|[VdYfW7jwgrSYwMvK.htm](agents-of-edgewatch-bestiary-items/VdYfW7jwgrSYwMvK.htm)|Grab|Agarrado|modificada|
|[VeiaoxMMsn7EH0Ri.htm](agents-of-edgewatch-bestiary-items/VeiaoxMMsn7EH0Ri.htm)|Purple Worm Sting|Aguijón del gusano púrpura|modificada|
|[vfIc0za5DwDp2KUV.htm](agents-of-edgewatch-bestiary-items/vfIc0za5DwDp2KUV.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[vGPvwYP6vUTp3ILa.htm](agents-of-edgewatch-bestiary-items/vGPvwYP6vUTp3ILa.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[Vgy7PoKunyfdAFR8.htm](agents-of-edgewatch-bestiary-items/Vgy7PoKunyfdAFR8.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Vhh4ycX46oiBHKeS.htm](agents-of-edgewatch-bestiary-items/Vhh4ycX46oiBHKeS.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[VHpzidpp88FeTXoa.htm](agents-of-edgewatch-bestiary-items/VHpzidpp88FeTXoa.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[viGvtlLGXHhsAVh8.htm](agents-of-edgewatch-bestiary-items/viGvtlLGXHhsAVh8.htm)|Quick Stow|Estiba Rápida|modificada|
|[Vj7wAYFFJXEJe0Ag.htm](agents-of-edgewatch-bestiary-items/Vj7wAYFFJXEJe0Ag.htm)|Stinger|Aguijón|modificada|
|[vJdzS91Cmz3WZZXD.htm](agents-of-edgewatch-bestiary-items/vJdzS91Cmz3WZZXD.htm)|Soul Siphon|Succionar almas|modificada|
|[vL5RasoMgasgdDwB.htm](agents-of-edgewatch-bestiary-items/vL5RasoMgasgdDwB.htm)|Innate Arcane Spells|Hechizos Arcanos Innatos|modificada|
|[VL9GemmMcofwTOB1.htm](agents-of-edgewatch-bestiary-items/VL9GemmMcofwTOB1.htm)|Chaos Hand|Chaos Hand|modificada|
|[vLQku20flQOsiRfM.htm](agents-of-edgewatch-bestiary-items/vLQku20flQOsiRfM.htm)|Jaws|Fauces|modificada|
|[vObvjZsVBRVevJEo.htm](agents-of-edgewatch-bestiary-items/vObvjZsVBRVevJEo.htm)|Grab|Agarrado|modificada|
|[Vqq99svjAY5zIGFW.htm](agents-of-edgewatch-bestiary-items/Vqq99svjAY5zIGFW.htm)|Dagger|Daga|modificada|
|[VrU5JQWGpeNs37xp.htm](agents-of-edgewatch-bestiary-items/VrU5JQWGpeNs37xp.htm)|Constant Spells|Constant Spells|modificada|
|[VSaPPvdgJDTYrZHJ.htm](agents-of-edgewatch-bestiary-items/VSaPPvdgJDTYrZHJ.htm)|Knockdown|Derribo|modificada|
|[VsMHrDx86310JmtX.htm](agents-of-edgewatch-bestiary-items/VsMHrDx86310JmtX.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[vu11UJ4Yclcvha1F.htm](agents-of-edgewatch-bestiary-items/vu11UJ4Yclcvha1F.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[VV329B6w0rioBP7u.htm](agents-of-edgewatch-bestiary-items/VV329B6w0rioBP7u.htm)|Shortsword|Espada corta|modificada|
|[vvMB5rLp5hejSAmy.htm](agents-of-edgewatch-bestiary-items/vvMB5rLp5hejSAmy.htm)|Arrogant Taunts|Burlas Arrogantes|modificada|
|[VVMmL1VxeAzF1IBI.htm](agents-of-edgewatch-bestiary-items/VVMmL1VxeAzF1IBI.htm)|Demanding Orders|Exigencia de Órdenes|modificada|
|[Vw3bmxYidV4ocAb1.htm](agents-of-edgewatch-bestiary-items/Vw3bmxYidV4ocAb1.htm)|Drain Wand|Varita de drenaje|modificada|
|[vW6qQ6HvTQdopnBl.htm](agents-of-edgewatch-bestiary-items/vW6qQ6HvTQdopnBl.htm)|Bloody Sneak Attack|Sangriento Movimiento furtivo|modificada|
|[Vy9EtFybJQqo55xn.htm](agents-of-edgewatch-bestiary-items/Vy9EtFybJQqo55xn.htm)|Hypnotic Glow|Resplandor Hipnótico|modificada|
|[Vyd3GhdzugQrKNRt.htm](agents-of-edgewatch-bestiary-items/Vyd3GhdzugQrKNRt.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[vynPyIOZZDBVXGPa.htm](agents-of-edgewatch-bestiary-items/vynPyIOZZDBVXGPa.htm)|Scent|Scent|modificada|
|[VyojjmuQFpZR3NHk.htm](agents-of-edgewatch-bestiary-items/VyojjmuQFpZR3NHk.htm)|Statue Shortsword|Estatua Espada Corta|modificada|
|[vYvC5Vb8SsSJOIxk.htm](agents-of-edgewatch-bestiary-items/vYvC5Vb8SsSJOIxk.htm)|Innate Arcane Spells|Hechizos Arcanos Innatos|modificada|
|[VzGeaeMNgzVgxvGo.htm](agents-of-edgewatch-bestiary-items/VzGeaeMNgzVgxvGo.htm)|Lusca Venom|Lusca Venom|modificada|
|[vZXFIePI3zP2Og1M.htm](agents-of-edgewatch-bestiary-items/vZXFIePI3zP2Og1M.htm)|Web Trap|Trampa telara|modificada|
|[W211sY7aEqXIhFeB.htm](agents-of-edgewatch-bestiary-items/W211sY7aEqXIhFeB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[w3uomQxkiwXJF2hj.htm](agents-of-edgewatch-bestiary-items/w3uomQxkiwXJF2hj.htm)|Light Ray|Rayo de luz|modificada|
|[w53ToXCIso2kLmSW.htm](agents-of-edgewatch-bestiary-items/w53ToXCIso2kLmSW.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[w5G4Ty8RBA7MDSTS.htm](agents-of-edgewatch-bestiary-items/w5G4Ty8RBA7MDSTS.htm)|Pitfall|Trampa de caída|modificada|
|[W5xPRbSycfvcG1bj.htm](agents-of-edgewatch-bestiary-items/W5xPRbSycfvcG1bj.htm)|Dart|Dardo|modificada|
|[WaEIc7jlHfXb63gH.htm](agents-of-edgewatch-bestiary-items/WaEIc7jlHfXb63gH.htm)|Blasphemous Arms|Blasphemous Arms|modificada|
|[wAYzRxbZPCt548k7.htm](agents-of-edgewatch-bestiary-items/wAYzRxbZPCt548k7.htm)|Dual Assault|Dual Assault|modificada|
|[wazlV57b7ZEUGjn6.htm](agents-of-edgewatch-bestiary-items/wazlV57b7ZEUGjn6.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[Wbny5fScmVtKCw6w.htm](agents-of-edgewatch-bestiary-items/Wbny5fScmVtKCw6w.htm)|Whirring Doom|Whirring Doom|modificada|
|[wBPluFhMyu7RSQgs.htm](agents-of-edgewatch-bestiary-items/wBPluFhMyu7RSQgs.htm)|Light Mace|Maza de luz|modificada|
|[Wc0IrjjTSicQkUfW.htm](agents-of-edgewatch-bestiary-items/Wc0IrjjTSicQkUfW.htm)|Wind Up|Wind Up|modificada|
|[WCFZ8IEU5wGPGAZK.htm](agents-of-edgewatch-bestiary-items/WCFZ8IEU5wGPGAZK.htm)|Opportune Backstab|Pulada trapera oportuna|modificada|
|[wCtIIrUW3W5fxJS5.htm](agents-of-edgewatch-bestiary-items/wCtIIrUW3W5fxJS5.htm)|Psychokinetic Trumpet|Trompeta Psicoquinética|modificada|
|[wdrujBr7WxOu0zlx.htm](agents-of-edgewatch-bestiary-items/wdrujBr7WxOu0zlx.htm)|Swallow Whole|Engullir Todo|modificada|
|[WeIVzFTJRVOOXc8w.htm](agents-of-edgewatch-bestiary-items/WeIVzFTJRVOOXc8w.htm)|Magic Susceptibility|Susceptibilidad Mágica|modificada|
|[wFFcTblSQv1w7t5j.htm](agents-of-edgewatch-bestiary-items/wFFcTblSQv1w7t5j.htm)|Lava Bomb|Bomba de lava|modificada|
|[wG13bULump9xDoJR.htm](agents-of-edgewatch-bestiary-items/wG13bULump9xDoJR.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Wg2JQdgjmSOEpeH4.htm](agents-of-edgewatch-bestiary-items/Wg2JQdgjmSOEpeH4.htm)|Longsword|Longsword|modificada|
|[wGuVhWBsG7ERdKlr.htm](agents-of-edgewatch-bestiary-items/wGuVhWBsG7ERdKlr.htm)|Extending Chandeliers|Extender Candelabros|modificada|
|[wh8aWKGdA2HjMBi7.htm](agents-of-edgewatch-bestiary-items/wh8aWKGdA2HjMBi7.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[wHtIgMdmPGGys5hu.htm](agents-of-edgewatch-bestiary-items/wHtIgMdmPGGys5hu.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[WibchVOYf3MLcdy7.htm](agents-of-edgewatch-bestiary-items/WibchVOYf3MLcdy7.htm)|Spike|Spike|modificada|
|[WJcrXfekpCSuZzoT.htm](agents-of-edgewatch-bestiary-items/WJcrXfekpCSuZzoT.htm)|[Two Actions] Water Jet|[Dos Acciones] Chorro de Agua|modificada|
|[wjucZTZEvK64kwlz.htm](agents-of-edgewatch-bestiary-items/wjucZTZEvK64kwlz.htm)|Exile|Exile|modificada|
|[WjzMU3WoQ0I6gVRQ.htm](agents-of-edgewatch-bestiary-items/WjzMU3WoQ0I6gVRQ.htm)|Bloody Spew|Vómito Sangriento|modificada|
|[wKQPyzZESm0w8vni.htm](agents-of-edgewatch-bestiary-items/wKQPyzZESm0w8vni.htm)|Protean Anatomy|Anatomía Proteica|modificada|
|[WlrYnJI19Nc7qelL.htm](agents-of-edgewatch-bestiary-items/WlrYnJI19Nc7qelL.htm)|Longsword|Longsword|modificada|
|[WNRDgclsqI4zJ8Zn.htm](agents-of-edgewatch-bestiary-items/WNRDgclsqI4zJ8Zn.htm)|Greater Flaming Greataxe|Mayor Flamígera Greataxe|modificada|
|[wPp48cMhLkEqI4UE.htm](agents-of-edgewatch-bestiary-items/wPp48cMhLkEqI4UE.htm)|Binding Blast|Binding Blast|modificada|
|[wpS81V45xNq0wZpT.htm](agents-of-edgewatch-bestiary-items/wpS81V45xNq0wZpT.htm)|Venomous Spit|Venomous Spit|modificada|
|[WQ6JLrvLYwJnndgB.htm](agents-of-edgewatch-bestiary-items/WQ6JLrvLYwJnndgB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[WQakf5K0a5EhkRkV.htm](agents-of-edgewatch-bestiary-items/WQakf5K0a5EhkRkV.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[wqR1FOPVr3IfWeO6.htm](agents-of-edgewatch-bestiary-items/wqR1FOPVr3IfWeO6.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[WSvQFfsRQ4UpAb4L.htm](agents-of-edgewatch-bestiary-items/WSvQFfsRQ4UpAb4L.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[wtewhF2XR7w6V0Wm.htm](agents-of-edgewatch-bestiary-items/wtewhF2XR7w6V0Wm.htm)|Incendiary Spit|Escupe Incendiario|modificada|
|[wTQI2GpK1QxS5tKd.htm](agents-of-edgewatch-bestiary-items/wTQI2GpK1QxS5tKd.htm)|Constant Spells|Constant Spells|modificada|
|[WUdhVBp748zzwj4z.htm](agents-of-edgewatch-bestiary-items/WUdhVBp748zzwj4z.htm)|Negative Healing|Curación negativa|modificada|
|[wuytt71AuRJcaryt.htm](agents-of-edgewatch-bestiary-items/wuytt71AuRJcaryt.htm)|Sidestep|Paso lateral|modificada|
|[wvGApQMwNilwaRG1.htm](agents-of-edgewatch-bestiary-items/wvGApQMwNilwaRG1.htm)|Massive Scythe|Guadaña Masiva|modificada|
|[wvqzKo67ZU0rKlN6.htm](agents-of-edgewatch-bestiary-items/wvqzKo67ZU0rKlN6.htm)|Jaws|Fauces|modificada|
|[wW8lBy7vi5CcDHTN.htm](agents-of-edgewatch-bestiary-items/wW8lBy7vi5CcDHTN.htm)|Retch of Foulness|Retch of Foulness|modificada|
|[wWBBBQdGjY5Kvg0N.htm](agents-of-edgewatch-bestiary-items/wWBBBQdGjY5Kvg0N.htm)|Constant Spells|Constant Spells|modificada|
|[WWLbZOyOAUEoJrVO.htm](agents-of-edgewatch-bestiary-items/WWLbZOyOAUEoJrVO.htm)|Fast Swallow|Tragar de golpe|modificada|
|[WxDAGBp5686PFyEA.htm](agents-of-edgewatch-bestiary-items/WxDAGBp5686PFyEA.htm)|Claw|Garra|modificada|
|[x2ySAWMOcn67UNgo.htm](agents-of-edgewatch-bestiary-items/x2ySAWMOcn67UNgo.htm)|Innate Primal Spells|Innate Primal Spells|modificada|
|[X6LedzeSnW4xbem7.htm](agents-of-edgewatch-bestiary-items/X6LedzeSnW4xbem7.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[X7yV3UIdROaskajA.htm](agents-of-edgewatch-bestiary-items/X7yV3UIdROaskajA.htm)|Suicidal Obedience|Obediencia Suicida|modificada|
|[x85871wfT5AGGHSb.htm](agents-of-edgewatch-bestiary-items/x85871wfT5AGGHSb.htm)|Predictive Impediment|Impedimento Predictivo|modificada|
|[X8bVkdHSJwnQfIoj.htm](agents-of-edgewatch-bestiary-items/X8bVkdHSJwnQfIoj.htm)|Mirror Senses|Mirror Senses|modificada|
|[xaK2BOT4rtFista9.htm](agents-of-edgewatch-bestiary-items/xaK2BOT4rtFista9.htm)|Blade of the Rabbit Prince|Hoja del Príncipe Conejo|modificada|
|[xB5y0S4TFwSTIZuN.htm](agents-of-edgewatch-bestiary-items/xB5y0S4TFwSTIZuN.htm)|Swallow Whole|Engullir Todo|modificada|
|[XbpdSS6FXHoqmiIy.htm](agents-of-edgewatch-bestiary-items/XbpdSS6FXHoqmiIy.htm)|Rend|Rasgadura|modificada|
|[Xc5ErxpysKbiLVjW.htm](agents-of-edgewatch-bestiary-items/Xc5ErxpysKbiLVjW.htm)|Tentacle|Tentáculo|modificada|
|[XC6rH5AiexqlIUrJ.htm](agents-of-edgewatch-bestiary-items/XC6rH5AiexqlIUrJ.htm)|Crowded Mob|Crowded Mob|modificada|
|[xcCXMK0pQFPWRPA9.htm](agents-of-edgewatch-bestiary-items/xcCXMK0pQFPWRPA9.htm)|Grab|Agarrado|modificada|
|[XCR8NvW2d1UwWElX.htm](agents-of-edgewatch-bestiary-items/XCR8NvW2d1UwWElX.htm)|Arm Activation|Activación del Brazo|modificada|
|[XEmgx6gs9UBM9Fqd.htm](agents-of-edgewatch-bestiary-items/XEmgx6gs9UBM9Fqd.htm)|Storm of Claws|Tormenta de Garras|modificada|
|[xhZKk4oQRgs08i0T.htm](agents-of-edgewatch-bestiary-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[XjKcKhw6OKjMZYZd.htm](agents-of-edgewatch-bestiary-items/XjKcKhw6OKjMZYZd.htm)|Jaws|Fauces|modificada|
|[xkC8ZjLdXCpQeb8J.htm](agents-of-edgewatch-bestiary-items/xkC8ZjLdXCpQeb8J.htm)|Regeneration 20 (Deactivated by Evil)|Regeneración 20 (Desactivado por maligno)|modificada|
|[XKe1YQHD2E6lSSDn.htm](agents-of-edgewatch-bestiary-items/XKe1YQHD2E6lSSDn.htm)|Shriek|Shriek|modificada|
|[xm43sElSiUh9TZcn.htm](agents-of-edgewatch-bestiary-items/xm43sElSiUh9TZcn.htm)|Prepared Divine Spells|Hechizos divinos preparados.|modificada|
|[xpaIY6o97hpi3r6c.htm](agents-of-edgewatch-bestiary-items/xpaIY6o97hpi3r6c.htm)|Club|Club|modificada|
|[XpTvRTQsAv9H9zpJ.htm](agents-of-edgewatch-bestiary-items/XpTvRTQsAv9H9zpJ.htm)|Constrict|Restringir|modificada|
|[XPZi9QdRnv0anTSq.htm](agents-of-edgewatch-bestiary-items/XPZi9QdRnv0anTSq.htm)|Greater Flaming Javelin|Jabalina Flamígera mayor.|modificada|
|[XrbjsVj4ZB7Fx9Dk.htm](agents-of-edgewatch-bestiary-items/XrbjsVj4ZB7Fx9Dk.htm)|Dagger|Daga|modificada|
|[XrNCDIzaEOy3x2U7.htm](agents-of-edgewatch-bestiary-items/XrNCDIzaEOy3x2U7.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[XrrgRKZ5Cs3t7Wzc.htm](agents-of-edgewatch-bestiary-items/XrrgRKZ5Cs3t7Wzc.htm)|Entropy Sense (Imprecise) 60 feet|Sentido de Entropía (Impreciso) 60 pies|modificada|
|[XsO5LFwrPWmXxkjs.htm](agents-of-edgewatch-bestiary-items/XsO5LFwrPWmXxkjs.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[XVBhNwitUtia7zY6.htm](agents-of-edgewatch-bestiary-items/XVBhNwitUtia7zY6.htm)|+2 Circumstance Bonus on Saves vs. Poison|+2 Bonificador de Circunstancia en Salvaciones contra Veneno|modificada|
|[xYJikLuNmwyBRVgt.htm](agents-of-edgewatch-bestiary-items/xYJikLuNmwyBRVgt.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[xzlfpLa29fzw4xH5.htm](agents-of-edgewatch-bestiary-items/xzlfpLa29fzw4xH5.htm)|Swipe|Vaivén|modificada|
|[xzuxIMk5QK7iPQ9j.htm](agents-of-edgewatch-bestiary-items/xzuxIMk5QK7iPQ9j.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[y4FvJN07K6t62yW1.htm](agents-of-edgewatch-bestiary-items/y4FvJN07K6t62yW1.htm)|Rolling Assault|Asalto Rodante|modificada|
|[Y7LWmp8wJgF4O24C.htm](agents-of-edgewatch-bestiary-items/Y7LWmp8wJgF4O24C.htm)|Slam Shut|Slam Shut|modificada|
|[Y7P7UpMZajGcxNz4.htm](agents-of-edgewatch-bestiary-items/Y7P7UpMZajGcxNz4.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Y83KtMbjPwV7yApV.htm](agents-of-edgewatch-bestiary-items/Y83KtMbjPwV7yApV.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[Y86gdLAapKCoO1Pl.htm](agents-of-edgewatch-bestiary-items/Y86gdLAapKCoO1Pl.htm)|Barrel|Barril|modificada|
|[y8wwK8yOkMWsAZoY.htm](agents-of-edgewatch-bestiary-items/y8wwK8yOkMWsAZoY.htm)|Tail|Tail|modificada|
|[Y974djSHleUUJ1cg.htm](agents-of-edgewatch-bestiary-items/Y974djSHleUUJ1cg.htm)|Claws|Garras|modificada|
|[yAHJOd3dkfOae1Kf.htm](agents-of-edgewatch-bestiary-items/yAHJOd3dkfOae1Kf.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[yAihXkZJ3wXkWXOm.htm](agents-of-edgewatch-bestiary-items/yAihXkZJ3wXkWXOm.htm)|Jaws|Fauces|modificada|
|[Yb2OjQReLk3HgLoc.htm](agents-of-edgewatch-bestiary-items/Yb2OjQReLk3HgLoc.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[YDAmsDKjMjg1NU8x.htm](agents-of-edgewatch-bestiary-items/YDAmsDKjMjg1NU8x.htm)|Head Hunter|Cazacabezas|modificada|
|[YE6DApvJToive72E.htm](agents-of-edgewatch-bestiary-items/YE6DApvJToive72E.htm)|Paint the Masses|Pintar a las masas|modificada|
|[YEhIEhTt74V6zF2F.htm](agents-of-edgewatch-bestiary-items/YEhIEhTt74V6zF2F.htm)|Pseudopod|Pseudópodo|modificada|
|[YH6BaTJoWcGrOFjD.htm](agents-of-edgewatch-bestiary-items/YH6BaTJoWcGrOFjD.htm)|Crossbow|Ballesta|modificada|
|[Yh9EKYQqgpz6wFJx.htm](agents-of-edgewatch-bestiary-items/Yh9EKYQqgpz6wFJx.htm)|Tail|Tail|modificada|
|[YhPJNRCs20xqntX6.htm](agents-of-edgewatch-bestiary-items/YhPJNRCs20xqntX6.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[yhVKDXeyQEVchPJW.htm](agents-of-edgewatch-bestiary-items/yhVKDXeyQEVchPJW.htm)|Dismember|Desmembrar|modificada|
|[yIeOGWTZXqk546wz.htm](agents-of-edgewatch-bestiary-items/yIeOGWTZXqk546wz.htm)|Claw|Garra|modificada|
|[yJXSa8oHuL9yaPqg.htm](agents-of-edgewatch-bestiary-items/yJXSa8oHuL9yaPqg.htm)|Wary|Wary|modificada|
|[ykHarPFAY9t176Uv.htm](agents-of-edgewatch-bestiary-items/ykHarPFAY9t176Uv.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ylBiUlJT4zFUEOca.htm](agents-of-edgewatch-bestiary-items/ylBiUlJT4zFUEOca.htm)|Dagger|Daga|modificada|
|[YlkEautzPcw7ota2.htm](agents-of-edgewatch-bestiary-items/YlkEautzPcw7ota2.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[YMK6yeIqUwKCrXpF.htm](agents-of-edgewatch-bestiary-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[yMXbFPudLdFxCrfJ.htm](agents-of-edgewatch-bestiary-items/yMXbFPudLdFxCrfJ.htm)|Prehensile Tail|Cola prensil|modificada|
|[YNkhegfdFfE7yfGn.htm](agents-of-edgewatch-bestiary-items/YNkhegfdFfE7yfGn.htm)|Formation Attack|Ataque en Formación|modificada|
|[YoEkR6sBza4UBMYy.htm](agents-of-edgewatch-bestiary-items/YoEkR6sBza4UBMYy.htm)|Essence Drain|Drenar esencia|modificada|
|[yoqc9IMPx8HSR6k1.htm](agents-of-edgewatch-bestiary-items/yoqc9IMPx8HSR6k1.htm)|Grab|Agarrado|modificada|
|[yOvz4I15CCPk78ns.htm](agents-of-edgewatch-bestiary-items/yOvz4I15CCPk78ns.htm)|Smoke Vision|Visión de Humo|modificada|
|[yqKz1nsBwvpywBaj.htm](agents-of-edgewatch-bestiary-items/yqKz1nsBwvpywBaj.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[yqYxldud8C52XbLb.htm](agents-of-edgewatch-bestiary-items/yqYxldud8C52XbLb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Yra4blZoY3d1QYtS.htm](agents-of-edgewatch-bestiary-items/Yra4blZoY3d1QYtS.htm)|Constant Spells|Constant Spells|modificada|
|[Ys0t5quGKBZoMSOI.htm](agents-of-edgewatch-bestiary-items/Ys0t5quGKBZoMSOI.htm)|Pseudopod|Pseudópodo|modificada|
|[Ys9PpeeVpBUmvYSZ.htm](agents-of-edgewatch-bestiary-items/Ys9PpeeVpBUmvYSZ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ysA7ztYuAl3O3J7A.htm](agents-of-edgewatch-bestiary-items/ysA7ztYuAl3O3J7A.htm)|Nightmare Cudgel|Garrote de pesadilla|modificada|
|[yTV624ydUotNS7Qa.htm](agents-of-edgewatch-bestiary-items/yTV624ydUotNS7Qa.htm)|Vital Transfusion|Transfusión Vital|modificada|
|[yut11i3oSOwRk5mZ.htm](agents-of-edgewatch-bestiary-items/yut11i3oSOwRk5mZ.htm)|Najra Lizard Venom|Najra Lizard Venom|modificada|
|[yVDBxObK8ZPhiFLN.htm](agents-of-edgewatch-bestiary-items/yVDBxObK8ZPhiFLN.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[yVT8Le2gpdDtMHay.htm](agents-of-edgewatch-bestiary-items/yVT8Le2gpdDtMHay.htm)|Shield Block|Bloquear con escudo|modificada|
|[YW2WcfCcpCi1cDl2.htm](agents-of-edgewatch-bestiary-items/YW2WcfCcpCi1cDl2.htm)|Thoughtsense (Imprecise) 120 feet|Percibir pensamientos (Impreciso) 120 pies|modificada|
|[yX6VKlUamzU5KOQr.htm](agents-of-edgewatch-bestiary-items/yX6VKlUamzU5KOQr.htm)|Bonesense (Imprecise) 30 feet|Bonesense (Impreciso) 30 pies|modificada|
|[YXf0epw5INfNsVTi.htm](agents-of-edgewatch-bestiary-items/YXf0epw5INfNsVTi.htm)|Vermicular Movement|Movimiento Vermicular|modificada|
|[YY5ytAHtfhcxwxto.htm](agents-of-edgewatch-bestiary-items/YY5ytAHtfhcxwxto.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[yyZMl8O8UfRO0cwF.htm](agents-of-edgewatch-bestiary-items/yyZMl8O8UfRO0cwF.htm)|Tremorsense (Imprecise) 120 feet|Sentido del Temblor (Impreciso) 120 pies|modificada|
|[yzD6DHeUdHo1tXNp.htm](agents-of-edgewatch-bestiary-items/yzD6DHeUdHo1tXNp.htm)|Summon Steed|Convocar corcel|modificada|
|[YzsKsO21XfN1KAry.htm](agents-of-edgewatch-bestiary-items/YzsKsO21XfN1KAry.htm)|Dread Gaze|Mirada de fatalidad|modificada|
|[yzzm5eV3r1RGpar6.htm](agents-of-edgewatch-bestiary-items/yzzm5eV3r1RGpar6.htm)|+1 Status Bonus on Saves vs. Incapacitation Effects|Bonificador de situación de +1 a las salvaciones contra efectos de Incapacitación.|modificada|
|[z53waMQlGY7ahvCA.htm](agents-of-edgewatch-bestiary-items/z53waMQlGY7ahvCA.htm)|Hook and Flay|Enganchar y Desollar|modificada|
|[Z5GiE6QWjQtsaPbJ.htm](agents-of-edgewatch-bestiary-items/Z5GiE6QWjQtsaPbJ.htm)|Capture Subject|Sujeto de Captura|modificada|
|[z7mNGwtluLbrFYpO.htm](agents-of-edgewatch-bestiary-items/z7mNGwtluLbrFYpO.htm)|Sling|Sling|modificada|
|[ZaivUgHp2bK6CmBQ.htm](agents-of-edgewatch-bestiary-items/ZaivUgHp2bK6CmBQ.htm)|Protean Anatomy 10|Protean Anatomy 10|modificada|
|[zaQRIy856WuUiHBw.htm](agents-of-edgewatch-bestiary-items/zaQRIy856WuUiHBw.htm)|Overdrive Engine|Motor Overdrive|modificada|
|[zBa632069HwaM4cT.htm](agents-of-edgewatch-bestiary-items/zBa632069HwaM4cT.htm)|Dagger|Daga|modificada|
|[ZBfbG1IkIDHWmhM2.htm](agents-of-edgewatch-bestiary-items/ZBfbG1IkIDHWmhM2.htm)|Wall Run|Correr por la pared|modificada|
|[ZbNsjTdTl9JRvGxk.htm](agents-of-edgewatch-bestiary-items/ZbNsjTdTl9JRvGxk.htm)|Bullyrag Beatdown|Bullyrag Beatdown|modificada|
|[Zc0ocF6ln2ziU1iE.htm](agents-of-edgewatch-bestiary-items/Zc0ocF6ln2ziU1iE.htm)|Agonizing Wail|Lamento Agonizante|modificada|
|[ZCDanTgpw20dpNUH.htm](agents-of-edgewatch-bestiary-items/ZCDanTgpw20dpNUH.htm)|Regeneration 30 (Deactivated by Lawful)|Regeneración 30 (Desactivado por Lawful)|modificada|
|[zchaebxlIBynZinm.htm](agents-of-edgewatch-bestiary-items/zchaebxlIBynZinm.htm)|Toxic Fumes|Toxic Fumes|modificada|
|[zDBvkCFJMYUXSbxN.htm](agents-of-edgewatch-bestiary-items/zDBvkCFJMYUXSbxN.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[zdKpdDk0vI7szcPH.htm](agents-of-edgewatch-bestiary-items/zdKpdDk0vI7szcPH.htm)|Dagger|Daga|modificada|
|[zdtAExquN9fICX40.htm](agents-of-edgewatch-bestiary-items/zdtAExquN9fICX40.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[Ze4gxM4HMIEzvJgg.htm](agents-of-edgewatch-bestiary-items/Ze4gxM4HMIEzvJgg.htm)|Main-Gauche|Main-Gauche|modificada|
|[ZEbYuhDFGc3fADE1.htm](agents-of-edgewatch-bestiary-items/ZEbYuhDFGc3fADE1.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[zeH3OrBLioIRoYb5.htm](agents-of-edgewatch-bestiary-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|Denegar ventaja|modificada|
|[zehVjK1f2C0jpnAB.htm](agents-of-edgewatch-bestiary-items/zehVjK1f2C0jpnAB.htm)|Innate Occult Spells|Innato Ocultismo Hechizos|modificada|
|[ZFx4kB8JAYfSX4xr.htm](agents-of-edgewatch-bestiary-items/ZFx4kB8JAYfSX4xr.htm)|Extend Legs|Extend Legs|modificada|
|[zghmY4aHHsaTIPLa.htm](agents-of-edgewatch-bestiary-items/zghmY4aHHsaTIPLa.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[zGvdZdakNLZQ0BMF.htm](agents-of-edgewatch-bestiary-items/zGvdZdakNLZQ0BMF.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[ZHs955g8FhuO2EvW.htm](agents-of-edgewatch-bestiary-items/ZHs955g8FhuO2EvW.htm)|Shield Block|Bloquear con escudo|modificada|
|[Zi59HbzpU7nXEarp.htm](agents-of-edgewatch-bestiary-items/Zi59HbzpU7nXEarp.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[zJ4cmL8xK1QGb5IF.htm](agents-of-edgewatch-bestiary-items/zJ4cmL8xK1QGb5IF.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ZjdHv1h2KMIjC0bA.htm](agents-of-edgewatch-bestiary-items/ZjdHv1h2KMIjC0bA.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[ZJvsVjbPWteUphTK.htm](agents-of-edgewatch-bestiary-items/ZJvsVjbPWteUphTK.htm)|Concussive Beatdown|Concussive Beatdown|modificada|
|[ZkE9BJn0zrQtzRMW.htm](agents-of-edgewatch-bestiary-items/ZkE9BJn0zrQtzRMW.htm)|Writhing Arms|Brazos retorcidos|modificada|
|[zMxrSkK2U9CDEp0u.htm](agents-of-edgewatch-bestiary-items/zMxrSkK2U9CDEp0u.htm)|Bone Drink|Bone Drink|modificada|
|[zn2a2GvxqbknqXbo.htm](agents-of-edgewatch-bestiary-items/zn2a2GvxqbknqXbo.htm)|Entropy Sense (Imprecise) 60 feet|Sentido de Entropía (Impreciso) 60 pies|modificada|
|[ZNWERIfXN1fj8u4P.htm](agents-of-edgewatch-bestiary-items/ZNWERIfXN1fj8u4P.htm)|Chain Expert|Experto en cadenas|modificada|
|[ZoDu12928nPzUoAD.htm](agents-of-edgewatch-bestiary-items/ZoDu12928nPzUoAD.htm)|Zealous Restoration|Restablecimiento Celoso|modificada|
|[zqI0OQ5w5KAQApsl.htm](agents-of-edgewatch-bestiary-items/zqI0OQ5w5KAQApsl.htm)|Shortsword|Espada corta|modificada|
|[ZtFghl2g5noJE7Cw.htm](agents-of-edgewatch-bestiary-items/ZtFghl2g5noJE7Cw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ztgpLrKdJjctex5J.htm](agents-of-edgewatch-bestiary-items/ztgpLrKdJjctex5J.htm)|Pollution Infusion|Infusión de Contaminación|modificada|
|[zvkxJ9ZreNjX3yJL.htm](agents-of-edgewatch-bestiary-items/zvkxJ9ZreNjX3yJL.htm)|Flurry of Pods|Ráfaga de vainas|modificada|
|[ZX0T6lbLJyKn8KuN.htm](agents-of-edgewatch-bestiary-items/ZX0T6lbLJyKn8KuN.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[ZX4MxqemrRYlaJEf.htm](agents-of-edgewatch-bestiary-items/ZX4MxqemrRYlaJEf.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[zX6RvKVyn16ug6vJ.htm](agents-of-edgewatch-bestiary-items/zX6RvKVyn16ug6vJ.htm)|Change Shape|Change Shape|modificada|
|[zxw1AsQiZy4HbIBa.htm](agents-of-edgewatch-bestiary-items/zxw1AsQiZy4HbIBa.htm)|Motion Sense 100 feet|Sentido del movimiento 100 pies|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Jf7Xd3lnLkiXGLW.htm](agents-of-edgewatch-bestiary-items/0Jf7Xd3lnLkiXGLW.htm)|Library Lore|vacía|
|[0zLmXZjs6O5tPrZB.htm](agents-of-edgewatch-bestiary-items/0zLmXZjs6O5tPrZB.htm)|Warfare Lore|vacía|
|[30qgIKOMzbO7zHQj.htm](agents-of-edgewatch-bestiary-items/30qgIKOMzbO7zHQj.htm)|Norgorber Lore|vacía|
|[3gaOTD4tVefgIrVA.htm](agents-of-edgewatch-bestiary-items/3gaOTD4tVefgIrVA.htm)|Sleep Poison|vacía|
|[4jX0nsNm9fRKO9C8.htm](agents-of-edgewatch-bestiary-items/4jX0nsNm9fRKO9C8.htm)|Vault Key|vacía|
|[4nrWl1NPbkOzx2Nq.htm](agents-of-edgewatch-bestiary-items/4nrWl1NPbkOzx2Nq.htm)|Victor's Medallion from the Challenge of Sky and Heaven|vacía|
|[4ZZA8573arWP2MKX.htm](agents-of-edgewatch-bestiary-items/4ZZA8573arWP2MKX.htm)|Aquatic Lore|vacía|
|[64us7Ph5ZQlOh8bU.htm](agents-of-edgewatch-bestiary-items/64us7Ph5ZQlOh8bU.htm)|Sailing Lore|vacía|
|[6OAI0pZw5WRcrpWw.htm](agents-of-edgewatch-bestiary-items/6OAI0pZw5WRcrpWw.htm)|Dream Lore|vacía|
|[7sl37aa7OG77O2mr.htm](agents-of-edgewatch-bestiary-items/7sl37aa7OG77O2mr.htm)|Norgorber Lore|vacía|
|[8k6vKZQ086Gc6Yq2.htm](agents-of-edgewatch-bestiary-items/8k6vKZQ086Gc6Yq2.htm)|Hell Lore|vacía|
|[8R9Z6J7bjth011q9.htm](agents-of-edgewatch-bestiary-items/8R9Z6J7bjth011q9.htm)|Norgorber Lore|vacía|
|[9bgmrU7Aa4AWtibm.htm](agents-of-edgewatch-bestiary-items/9bgmrU7Aa4AWtibm.htm)|Legal Lore|vacía|
|[aJfhxNbUXiAHn566.htm](agents-of-edgewatch-bestiary-items/aJfhxNbUXiAHn566.htm)|Warfare Lore|vacía|
|[aYZ47LUpvGry49gd.htm](agents-of-edgewatch-bestiary-items/aYZ47LUpvGry49gd.htm)|Ooze Lore|vacía|
|[blAoRBmaeuiqIsGU.htm](agents-of-edgewatch-bestiary-items/blAoRBmaeuiqIsGU.htm)|Games Lore|vacía|
|[bN92RThudov2eNQm.htm](agents-of-edgewatch-bestiary-items/bN92RThudov2eNQm.htm)|Legal Lore|vacía|
|[CMV3xH3xyqqm0yJQ.htm](agents-of-edgewatch-bestiary-items/CMV3xH3xyqqm0yJQ.htm)|Athletics|vacía|
|[CnW4ILs2EPOYJl20.htm](agents-of-edgewatch-bestiary-items/CnW4ILs2EPOYJl20.htm)|Legal Lore|vacía|
|[cXoj85cKnrKN2Ktf.htm](agents-of-edgewatch-bestiary-items/cXoj85cKnrKN2Ktf.htm)|Metal Wire|vacía|
|[Dpf0j9EikZZPVrz7.htm](agents-of-edgewatch-bestiary-items/Dpf0j9EikZZPVrz7.htm)|High Priest Robes|vacía|
|[eapXoUtrhnFlcZ2a.htm](agents-of-edgewatch-bestiary-items/eapXoUtrhnFlcZ2a.htm)|Gang Lore|vacía|
|[eXrMwXA19tZs74SH.htm](agents-of-edgewatch-bestiary-items/eXrMwXA19tZs74SH.htm)|Apartment Key|vacía|
|[f8FrD7VC9NmfhQ3W.htm](agents-of-edgewatch-bestiary-items/f8FrD7VC9NmfhQ3W.htm)|Warfare Lore|vacía|
|[f91HgG3ITNdNw2Fh.htm](agents-of-edgewatch-bestiary-items/f91HgG3ITNdNw2Fh.htm)|Garrote Bolts|vacía|
|[FHNon1tZFK4IcVPe.htm](agents-of-edgewatch-bestiary-items/FHNon1tZFK4IcVPe.htm)|Legal Lore|vacía|
|[fORoOGa8xpUS4qM6.htm](agents-of-edgewatch-bestiary-items/fORoOGa8xpUS4qM6.htm)|Dark Tapestry Lore|vacía|
|[GPQ3x53OnRv11SW7.htm](agents-of-edgewatch-bestiary-items/GPQ3x53OnRv11SW7.htm)|Dragon-Shaped Hookah|vacía|
|[HePUOrJQdIAXDxrv.htm](agents-of-edgewatch-bestiary-items/HePUOrJQdIAXDxrv.htm)|Gang Lore|vacía|
|[JsmcVomHbSSmqqvS.htm](agents-of-edgewatch-bestiary-items/JsmcVomHbSSmqqvS.htm)|Absalom Lore|vacía|
|[kfBkomhbVA2SgJJO.htm](agents-of-edgewatch-bestiary-items/kfBkomhbVA2SgJJO.htm)|Athletics|vacía|
|[leCv6XyPY49ANtH9.htm](agents-of-edgewatch-bestiary-items/leCv6XyPY49ANtH9.htm)|Foreign Guard Uniform|vacía|
|[M0jhr5pNpF64FnCK.htm](agents-of-edgewatch-bestiary-items/M0jhr5pNpF64FnCK.htm)|Legal Lore|vacía|
|[Mt5gA0Ecgq48bbxm.htm](agents-of-edgewatch-bestiary-items/Mt5gA0Ecgq48bbxm.htm)|Bardic Lore|vacía|
|[NeqCKB9myPD8TeNF.htm](agents-of-edgewatch-bestiary-items/NeqCKB9myPD8TeNF.htm)|Harrow Lore|vacía|
|[nN0X2KE7CFWOXOcW.htm](agents-of-edgewatch-bestiary-items/nN0X2KE7CFWOXOcW.htm)|Tinted Goggles|vacía|
|[NPDwckg35b7RN3Hb.htm](agents-of-edgewatch-bestiary-items/NPDwckg35b7RN3Hb.htm)|Sports Lore|vacía|
|[nsnL0aVILG6H6pb2.htm](agents-of-edgewatch-bestiary-items/nsnL0aVILG6H6pb2.htm)|Maelstrom Lore|vacía|
|[NZtjQV6Cb5pHneZI.htm](agents-of-edgewatch-bestiary-items/NZtjQV6Cb5pHneZI.htm)|Engineering Lore|vacía|
|[OGtpbVhb7Iohn4OQ.htm](agents-of-edgewatch-bestiary-items/OGtpbVhb7Iohn4OQ.htm)|Stealth|vacía|
|[OksVpY2e7p3fhW8q.htm](agents-of-edgewatch-bestiary-items/OksVpY2e7p3fhW8q.htm)|Dream Lore|vacía|
|[oQJvVNt4Rn1ZygrC.htm](agents-of-edgewatch-bestiary-items/oQJvVNt4Rn1ZygrC.htm)|Deception|vacía|
|[P7QkpppdP81lVkfJ.htm](agents-of-edgewatch-bestiary-items/P7QkpppdP81lVkfJ.htm)|Poison Lore|vacía|
|[p9JgBbLy85y8zTXK.htm](agents-of-edgewatch-bestiary-items/p9JgBbLy85y8zTXK.htm)|Norgorber Lore|vacía|
|[Q0NhoV8dgyILx0Gd.htm](agents-of-edgewatch-bestiary-items/Q0NhoV8dgyILx0Gd.htm)|Underworld Lore|vacía|
|[r7L1O9PM4aSBRzrz.htm](agents-of-edgewatch-bestiary-items/r7L1O9PM4aSBRzrz.htm)|Tinted Goggles|vacía|
|[rc2xxGTBv3TdEinV.htm](agents-of-edgewatch-bestiary-items/rc2xxGTBv3TdEinV.htm)|Crafting|vacía|
|[RDwGhyZoG46giLcH.htm](agents-of-edgewatch-bestiary-items/RDwGhyZoG46giLcH.htm)|Absalom Lore|vacía|
|[rfWEXR6igt2DS6gk.htm](agents-of-edgewatch-bestiary-items/rfWEXR6igt2DS6gk.htm)|Tin Crown|vacía|
|[rlDEDPkmEQiODiMh.htm](agents-of-edgewatch-bestiary-items/rlDEDPkmEQiODiMh.htm)|Norgorber Lore|vacía|
|[rm9LdZwyao3hq2RK.htm](agents-of-edgewatch-bestiary-items/rm9LdZwyao3hq2RK.htm)|Absalom Lore|vacía|
|[RZJPsQgzSjhqSzZx.htm](agents-of-edgewatch-bestiary-items/RZJPsQgzSjhqSzZx.htm)|Engineering Lore|vacía|
|[Sv86fg09Ov7lNslX.htm](agents-of-edgewatch-bestiary-items/Sv86fg09Ov7lNslX.htm)|Assorted Rags and Textiles|vacía|
|[T5JT4UBwqBoN3X7e.htm](agents-of-edgewatch-bestiary-items/T5JT4UBwqBoN3X7e.htm)|Gang Lore|vacía|
|[Tr0RBSqA2yKIpS47.htm](agents-of-edgewatch-bestiary-items/Tr0RBSqA2yKIpS47.htm)|Farming Lore|vacía|
|[Ts4PjUT1udov76YX.htm](agents-of-edgewatch-bestiary-items/Ts4PjUT1udov76YX.htm)|Absalom Lore|vacía|
|[tZ9QMTStg7ZyDCGS.htm](agents-of-edgewatch-bestiary-items/tZ9QMTStg7ZyDCGS.htm)|Underworld Lore|vacía|
|[U3Bd6ShOOPqzffIg.htm](agents-of-edgewatch-bestiary-items/U3Bd6ShOOPqzffIg.htm)|Legal Lore|vacía|
|[V00sOZ0n6SQD1gNl.htm](agents-of-edgewatch-bestiary-items/V00sOZ0n6SQD1gNl.htm)|Poison Lore|vacía|
|[VhwvrjXUG0odeyH5.htm](agents-of-edgewatch-bestiary-items/VhwvrjXUG0odeyH5.htm)|Tinted Goggles|vacía|
|[Vlw7l0vJKqGsX2Ig.htm](agents-of-edgewatch-bestiary-items/Vlw7l0vJKqGsX2Ig.htm)|Warfare Lore|vacía|
|[vMASeC7y1dcAleR5.htm](agents-of-edgewatch-bestiary-items/vMASeC7y1dcAleR5.htm)|Games Lore|vacía|
|[VZSNVQOWLrMXeg03.htm](agents-of-edgewatch-bestiary-items/VZSNVQOWLrMXeg03.htm)|Alchemy Lore|vacía|
|[WkhPyo0GeiUUil24.htm](agents-of-edgewatch-bestiary-items/WkhPyo0GeiUUil24.htm)|Architecture Lore|vacía|
|[wXT4WRYgfh7qLzdK.htm](agents-of-edgewatch-bestiary-items/wXT4WRYgfh7qLzdK.htm)|Key to the Skinsaw Sanctum Vault|vacía|
|[X6Z3xLOINbH82TQ9.htm](agents-of-edgewatch-bestiary-items/X6Z3xLOINbH82TQ9.htm)|Badge|vacía|
|[Y6JaGSxghkwvmU0v.htm](agents-of-edgewatch-bestiary-items/Y6JaGSxghkwvmU0v.htm)|Infused Reagents|vacía|
|[Yid1THCUAuHFu0h8.htm](agents-of-edgewatch-bestiary-items/Yid1THCUAuHFu0h8.htm)|Survival|vacía|
|[z0sQG5rpiS29mSKB.htm](agents-of-edgewatch-bestiary-items/z0sQG5rpiS29mSKB.htm)|Law Lore|vacía|
|[Z3kggQWxBiytny0l.htm](agents-of-edgewatch-bestiary-items/Z3kggQWxBiytny0l.htm)|Norgorber Lore|vacía|
|[zKzd8af55LIetqCL.htm](agents-of-edgewatch-bestiary-items/zKzd8af55LIetqCL.htm)|Poison Lore|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
