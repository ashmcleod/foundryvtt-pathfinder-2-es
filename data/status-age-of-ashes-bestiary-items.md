# Estado de la traducción (age-of-ashes-bestiary-items)

 * **modificada**: 966
 * **ninguna**: 186
 * **vacía**: 66


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0a45Roiy20vW4l7O.htm](age-of-ashes-bestiary-items/0a45Roiy20vW4l7O.htm)|Invisibility (At Will)|
|[0D5GZXOXBKSd2dFV.htm](age-of-ashes-bestiary-items/0D5GZXOXBKSd2dFV.htm)|Dagger|+1,cold-iron,disrupting|
|[0DQIaDtqyVpcJATv.htm](age-of-ashes-bestiary-items/0DQIaDtqyVpcJATv.htm)|+2 Greater Resilient Chain Shirt|
|[0fpr7D6z7lRzEK6s.htm](age-of-ashes-bestiary-items/0fpr7D6z7lRzEK6s.htm)|Dagger|silver|
|[0lgDXPlAG6oZy8ks.htm](age-of-ashes-bestiary-items/0lgDXPlAG6oZy8ks.htm)|Shortbow|+1|
|[1oZUGa8uGtIoemuM.htm](age-of-ashes-bestiary-items/1oZUGa8uGtIoemuM.htm)|Scholar's Robes|
|[23aybhsSs40twjme.htm](age-of-ashes-bestiary-items/23aybhsSs40twjme.htm)|Wall of Fire (At Will)|
|[31Y7LmRVIBxfyRRK.htm](age-of-ashes-bestiary-items/31Y7LmRVIBxfyRRK.htm)|Heavy Crossbow|+1,striking|
|[3ZIUyyCzEz5jwled.htm](age-of-ashes-bestiary-items/3ZIUyyCzEz5jwled.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|
|[4tYBLforD5bIHqLq.htm](age-of-ashes-bestiary-items/4tYBLforD5bIHqLq.htm)|Illusory Disguise (At Will)|
|[4YYh61RgLRJVFW9O.htm](age-of-ashes-bestiary-items/4YYh61RgLRJVFW9O.htm)|Elven Curve Blade|+3,majorStriking|
|[50JvEJYUrD0euZsR.htm](age-of-ashes-bestiary-items/50JvEJYUrD0euZsR.htm)|Light Mace|+2,striking|
|[578QS3EJCggOjkwq.htm](age-of-ashes-bestiary-items/578QS3EJCggOjkwq.htm)|Composite Longbow|+1,striking|
|[5i5r7EFzXOifPlnY.htm](age-of-ashes-bestiary-items/5i5r7EFzXOifPlnY.htm)|Invisibility (Self Only)|
|[62jCcD7uSVwAG83f.htm](age-of-ashes-bestiary-items/62jCcD7uSVwAG83f.htm)|Mace|+1,striking|
|[68LYg123W8ittlmS.htm](age-of-ashes-bestiary-items/68LYg123W8ittlmS.htm)|Command (At Will)|
|[6gQEeIYepYV659hU.htm](age-of-ashes-bestiary-items/6gQEeIYepYV659hU.htm)|Religious Symbol of Droskar|
|[6nqWpnR0S7DfLCp7.htm](age-of-ashes-bestiary-items/6nqWpnR0S7DfLCp7.htm)|Shortbow|+1|
|[6R4b8uNayGGrHJNm.htm](age-of-ashes-bestiary-items/6R4b8uNayGGrHJNm.htm)|Illusory Object (At Will)|
|[7s7604XlS2m6VCsl.htm](age-of-ashes-bestiary-items/7s7604XlS2m6VCsl.htm)|+3 Greater Resilient Full Plate|
|[7zonVNduNgQTkcUe.htm](age-of-ashes-bestiary-items/7zonVNduNgQTkcUe.htm)|Acid Flask (Major) [Infused]|
|[8ImDtLzIZFTzCASM.htm](age-of-ashes-bestiary-items/8ImDtLzIZFTzCASM.htm)|+1 Resilient Breastplate|
|[8vIB5BOiDuY5uD2a.htm](age-of-ashes-bestiary-items/8vIB5BOiDuY5uD2a.htm)|Regeneration 25 (deactivated by Good or Silver)|
|[9FXyozNfJ4cDMvMy.htm](age-of-ashes-bestiary-items/9FXyozNfJ4cDMvMy.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|
|[9yiCjxLdJV4KSZ7b.htm](age-of-ashes-bestiary-items/9yiCjxLdJV4KSZ7b.htm)|Giant Scorpion Venom (Applied to Bolts)|
|[Aebmgw6Y4cwVgvM0.htm](age-of-ashes-bestiary-items/Aebmgw6Y4cwVgvM0.htm)|Shield Spikes|+2,greaterStriking|
|[ajoryTcPkySSPZU4.htm](age-of-ashes-bestiary-items/ajoryTcPkySSPZU4.htm)|Darkness (At Will)|
|[AjstPqHuQD2eALaa.htm](age-of-ashes-bestiary-items/AjstPqHuQD2eALaa.htm)|Whip|+2,striking,corrosive|
|[ajsXyQpKZDv0k3qJ.htm](age-of-ashes-bestiary-items/ajsXyQpKZDv0k3qJ.htm)|Ring of Fire Resistance|
|[AlC9QRa4quIAs8NK.htm](age-of-ashes-bestiary-items/AlC9QRa4quIAs8NK.htm)|Cinderclaw Gauntlet|+1,striking|
|[AyzlkA8BDbzOj0oI.htm](age-of-ashes-bestiary-items/AyzlkA8BDbzOj0oI.htm)|Longbow|+1,striking|
|[b0MtgA6YzYyKSJQr.htm](age-of-ashes-bestiary-items/b0MtgA6YzYyKSJQr.htm)|+1 Resilient Leather Armor|
|[BFNXF7LIEPpdAngu.htm](age-of-ashes-bestiary-items/BFNXF7LIEPpdAngu.htm)|Composite Longbow|+1,striking|
|[bKMNN43L5lX1I6nY.htm](age-of-ashes-bestiary-items/bKMNN43L5lX1I6nY.htm)|+1 Chain Shirt|
|[bLCC2BkHqAMGHsin.htm](age-of-ashes-bestiary-items/bLCC2BkHqAMGHsin.htm)|Kukri|+2,greaterStriking,thundering|
|[BmogbygVHyhJ5dIR.htm](age-of-ashes-bestiary-items/BmogbygVHyhJ5dIR.htm)|Dagger|+2,striking|
|[bQGNTrxdCyY34M1f.htm](age-of-ashes-bestiary-items/bQGNTrxdCyY34M1f.htm)|Thunderstone (Greater) [Infused]|
|[brN9awmqYtvkfScZ.htm](age-of-ashes-bestiary-items/brN9awmqYtvkfScZ.htm)|Scroll of True Strike (Level 1)|
|[c68riQyo1R1CLItH.htm](age-of-ashes-bestiary-items/c68riQyo1R1CLItH.htm)|+1 Leather Armor|
|[cCgcYbkQbcYvPXDB.htm](age-of-ashes-bestiary-items/cCgcYbkQbcYvPXDB.htm)|+1 Clothing (Explorer's)|
|[cH86pXBWXoCzfFuA.htm](age-of-ashes-bestiary-items/cH86pXBWXoCzfFuA.htm)|Rapier|+1,striking|
|[cjGAqJPJESzxQoEi.htm](age-of-ashes-bestiary-items/cjGAqJPJESzxQoEi.htm)|Maul|+1,striking|
|[CNMsxGsJc8C2klMn.htm](age-of-ashes-bestiary-items/CNMsxGsJc8C2klMn.htm)|Air Walk (constant)|
|[cS1gFj0x5OkGUnhg.htm](age-of-ashes-bestiary-items/cS1gFj0x5OkGUnhg.htm)|Wyvern Poison [Applied to 4 Bolts]|
|[d42IPyLbN0f6KFtq.htm](age-of-ashes-bestiary-items/d42IPyLbN0f6KFtq.htm)|Whip|+1,striking|
|[dBFmE3lCilk6yZX5.htm](age-of-ashes-bestiary-items/dBFmE3lCilk6yZX5.htm)|Dagger|+2,greaterStriking,corrosive|
|[dcKVjyzEepP9jZIg.htm](age-of-ashes-bestiary-items/dcKVjyzEepP9jZIg.htm)|Light Hammer|+1,striking,returning|
|[dgaMs1IhlvBQeUzA.htm](age-of-ashes-bestiary-items/dgaMs1IhlvBQeUzA.htm)|Gold Jewelry|
|[DrODOsMSTXjJpF5S.htm](age-of-ashes-bestiary-items/DrODOsMSTXjJpF5S.htm)|Halberd|+1|
|[DtgWIHkl45Oz0I8c.htm](age-of-ashes-bestiary-items/DtgWIHkl45Oz0I8c.htm)|Telekinetic Maneuver (At Will)|
|[DW0ZzC1c6DBVQbus.htm](age-of-ashes-bestiary-items/DW0ZzC1c6DBVQbus.htm)|Blink (At Will)|
|[edOwB8VQ6gUQ1xnZ.htm](age-of-ashes-bestiary-items/edOwB8VQ6gUQ1xnZ.htm)|Telekinetic Maneuver (At Will)|
|[EenrewXIgDjYNZCt.htm](age-of-ashes-bestiary-items/EenrewXIgDjYNZCt.htm)|+2 Resilient Studded Leather Armor|
|[EFxhJV27viaJeJvR.htm](age-of-ashes-bestiary-items/EFxhJV27viaJeJvR.htm)|Longsword|+2,greaterStriking|
|[EHn8GE3CrQAw7HfX.htm](age-of-ashes-bestiary-items/EHn8GE3CrQAw7HfX.htm)|Nightmare (see Dream Haunting)|
|[EJfbSXCysRWwqSsI.htm](age-of-ashes-bestiary-items/EJfbSXCysRWwqSsI.htm)|Ventriloquism (At Will)|
|[eM2vlUZYXujfEHUB.htm](age-of-ashes-bestiary-items/eM2vlUZYXujfEHUB.htm)|Bloodseeker Beak (Affixed to Rapier)|
|[FbmiYuEnbNstnXfj.htm](age-of-ashes-bestiary-items/FbmiYuEnbNstnXfj.htm)|Fleshroaster|+2,striking,flaming|
|[FGLOUpHNkmleuOIx.htm](age-of-ashes-bestiary-items/FGLOUpHNkmleuOIx.htm)|Frost Vial (Greater) [Infused]|
|[FiaGipQmzzGWwPnw.htm](age-of-ashes-bestiary-items/FiaGipQmzzGWwPnw.htm)|Mind Reading (At Will)|
|[FLhJ8AT87HUFtG5w.htm](age-of-ashes-bestiary-items/FLhJ8AT87HUFtG5w.htm)|+1 Leather Armor|
|[fQSHG7Wkydbvpqji.htm](age-of-ashes-bestiary-items/fQSHG7Wkydbvpqji.htm)|Alchemist's Fire (Major) [Infused]|
|[fS06Q6X2UX1yRC1w.htm](age-of-ashes-bestiary-items/fS06Q6X2UX1yRC1w.htm)|+1 Resilient Full Plate|
|[fTtXaKs3sTtekS9Y.htm](age-of-ashes-bestiary-items/fTtXaKs3sTtekS9Y.htm)|Dimension Door (At Will)|
|[fZeOgtuKuu2UdKeD.htm](age-of-ashes-bestiary-items/fZeOgtuKuu2UdKeD.htm)|Elixir of Life (True) [Infused]|
|[g9GO4tLTHh7ZeC4L.htm](age-of-ashes-bestiary-items/g9GO4tLTHh7ZeC4L.htm)|Soul Chain|+2,greaterStriking,adamantine,flaming|
|[gHkFG2cVrCRf6Hui.htm](age-of-ashes-bestiary-items/gHkFG2cVrCRf6Hui.htm)|Maul|+2,striking,thundering|
|[gIKh3WQBoiyrW9gn.htm](age-of-ashes-bestiary-items/gIKh3WQBoiyrW9gn.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|
|[gjRN8wkee3LER65n.htm](age-of-ashes-bestiary-items/gjRN8wkee3LER65n.htm)|Formula Book|
|[Gr4iC9v8WDwl1THc.htm](age-of-ashes-bestiary-items/Gr4iC9v8WDwl1THc.htm)|Dagger|+1|
|[GXl3eIHoy2IwAZsz.htm](age-of-ashes-bestiary-items/GXl3eIHoy2IwAZsz.htm)|Morningstar|+1,striking|
|[h7xMfnmPBbeB5lZh.htm](age-of-ashes-bestiary-items/h7xMfnmPBbeB5lZh.htm)|Soul Chain|+1,striking|
|[HDdDDHhvuKuOWIXX.htm](age-of-ashes-bestiary-items/HDdDDHhvuKuOWIXX.htm)|Mind Blank (Constant)|
|[HJwVm2TXnBSmdy28.htm](age-of-ashes-bestiary-items/HJwVm2TXnBSmdy28.htm)|Fire Shield (Constant)|
|[hna2NNtT5gHtT3b2.htm](age-of-ashes-bestiary-items/hna2NNtT5gHtT3b2.htm)|True Seeing (Constant)|
|[hPj1RAdI4c2AUrOO.htm](age-of-ashes-bestiary-items/hPj1RAdI4c2AUrOO.htm)|Bestial Mutagen (Major) [Infused]|
|[hUz1Bt93sajJTnQg.htm](age-of-ashes-bestiary-items/hUz1Bt93sajJTnQg.htm)|Bottled Lightning (Major) [Infused]|
|[i8XuwjgGCvTaFORz.htm](age-of-ashes-bestiary-items/i8XuwjgGCvTaFORz.htm)|Frost Vial (Major) [Infused]|
|[iF9jSPAE3daPYnue.htm](age-of-ashes-bestiary-items/iF9jSPAE3daPYnue.htm)|Wand of Crushing Despair (Level 5)|
|[iFQmsWh8PXGOmX3E.htm](age-of-ashes-bestiary-items/iFQmsWh8PXGOmX3E.htm)|Spell Turning (At Will)|
|[Ig3nqmVhvxcS8az0.htm](age-of-ashes-bestiary-items/Ig3nqmVhvxcS8az0.htm)|Shoddy Blunderbuss|
|[ikFTzuigWjVB9prY.htm](age-of-ashes-bestiary-items/ikFTzuigWjVB9prY.htm)|Hatchet|+1,striking|
|[iwjhcDyX2SKcHon0.htm](age-of-ashes-bestiary-items/iwjhcDyX2SKcHon0.htm)|Blowgun|+1|
|[j9OR4GAOwpFolkP9.htm](age-of-ashes-bestiary-items/j9OR4GAOwpFolkP9.htm)|Plane Shift (To and From the Vazgorlu's Demiplane Only)|
|[JD2ON040dRP85QHM.htm](age-of-ashes-bestiary-items/JD2ON040dRP85QHM.htm)|Levitate (At Will)|
|[jEFmMCQkUOlH3g51.htm](age-of-ashes-bestiary-items/jEFmMCQkUOlH3g51.htm)|Ruby and Sapphire ring|
|[jPZb7g1lsQyPQQw3.htm](age-of-ashes-bestiary-items/jPZb7g1lsQyPQQw3.htm)|Crossbow|+1,striking|
|[JrQMzt6NgfnCAaOv.htm](age-of-ashes-bestiary-items/JrQMzt6NgfnCAaOv.htm)|Composite Shortbow|+2,striking|
|[jx7FmzYu8clgoANu.htm](age-of-ashes-bestiary-items/jx7FmzYu8clgoANu.htm)|Dimension Door (At Will)|
|[jZUi2l7sM6u3fmPW.htm](age-of-ashes-bestiary-items/jZUi2l7sM6u3fmPW.htm)|Dagger|+2,striking,returning|
|[kCTibCnGM3LjnVUm.htm](age-of-ashes-bestiary-items/kCTibCnGM3LjnVUm.htm)|Scimitar|+3,majorStriking,speed|
|[kD6AcE8oSOr3UKnh.htm](age-of-ashes-bestiary-items/kD6AcE8oSOr3UKnh.htm)|Scroll of Discern Location (Level 8)|
|[keOsYquWeSxZCihV.htm](age-of-ashes-bestiary-items/keOsYquWeSxZCihV.htm)|Broken Greatsword|
|[kGULFoB74ac0dOIO.htm](age-of-ashes-bestiary-items/kGULFoB74ac0dOIO.htm)|Scroll of Sleep (Level 1)|
|[kpp6uAfQZcHA99Ot.htm](age-of-ashes-bestiary-items/kpp6uAfQZcHA99Ot.htm)|Acid Flask (Greater) [Infused]|
|[kSkZEEGE3SBLK5M1.htm](age-of-ashes-bestiary-items/kSkZEEGE3SBLK5M1.htm)|Detect Alignment (Constant) (Good Only)|
|[kuz9wy1VxfKf0baT.htm](age-of-ashes-bestiary-items/kuz9wy1VxfKf0baT.htm)|+1 Resilient Scale Mail|
|[kW1j0YXBTuxfEsOM.htm](age-of-ashes-bestiary-items/kW1j0YXBTuxfEsOM.htm)|+1 Resilient Leather Armor|
|[kwZwMAb5UWqN9UlE.htm](age-of-ashes-bestiary-items/kwZwMAb5UWqN9UlE.htm)|Synesthesia (At Will)|
|[Kz6JoF3brQdpyel6.htm](age-of-ashes-bestiary-items/Kz6JoF3brQdpyel6.htm)|Wand of Vampiric Exsanguination (Level 6)|
|[l4sEHZaqyz1kUQ95.htm](age-of-ashes-bestiary-items/l4sEHZaqyz1kUQ95.htm)|Dagger|+3,majorStriking|
|[lBHMoim0EVa3GvjV.htm](age-of-ashes-bestiary-items/lBHMoim0EVa3GvjV.htm)|Detect Magic (Constant)|
|[LDF2M6FvzjfuWrGp.htm](age-of-ashes-bestiary-items/LDF2M6FvzjfuWrGp.htm)|Mind Reading (At Will)|
|[lff494GXiIjHCMDA.htm](age-of-ashes-bestiary-items/lff494GXiIjHCMDA.htm)|Weeping Midnight (applied to one arrow)|
|[lk1R5BJiKisYShQK.htm](age-of-ashes-bestiary-items/lk1R5BJiKisYShQK.htm)|See Invisibility (Constant)|
|[lKe6LQN6pD3r55Wq.htm](age-of-ashes-bestiary-items/lKe6LQN6pD3r55Wq.htm)|Rapier|+1,striking|
|[M2OxBMZbqLpIs5U3.htm](age-of-ashes-bestiary-items/M2OxBMZbqLpIs5U3.htm)|+2 Resilient Leather Armor|
|[m69H7bbz05FeE4YE.htm](age-of-ashes-bestiary-items/m69H7bbz05FeE4YE.htm)|+1 Leather Armor|
|[m8WR1jKIWEJbbaKO.htm](age-of-ashes-bestiary-items/m8WR1jKIWEJbbaKO.htm)|Phantom Pain (At Will)|
|[MmnbEXy9FWwuhWIU.htm](age-of-ashes-bestiary-items/MmnbEXy9FWwuhWIU.htm)|Dagger|+2,greaterStriking|
|[Mp6f5v7pvptmPQVp.htm](age-of-ashes-bestiary-items/Mp6f5v7pvptmPQVp.htm)|+2 Greater Resilient Full Plate|
|[MZVij16ep6tHatWC.htm](age-of-ashes-bestiary-items/MZVij16ep6tHatWC.htm)|+1 Striking Staff of Necromancy (Greater)|+1,striking|
|[NKKJNwEod8IO6M9D.htm](age-of-ashes-bestiary-items/NKKJNwEod8IO6M9D.htm)|Sterling Blacksmith's Tools|
|[NNiuCix0bI7jl6FB.htm](age-of-ashes-bestiary-items/NNiuCix0bI7jl6FB.htm)|Shortbow|+2,striking|
|[NtNZz3pZ90BCanve.htm](age-of-ashes-bestiary-items/NtNZz3pZ90BCanve.htm)|Burning Hands (At Will)|
|[NXVmpsr0ZCjlPFnB.htm](age-of-ashes-bestiary-items/NXVmpsr0ZCjlPFnB.htm)|+2 Resilient Scale Mail|
|[OBr4iMQYSp8jHCDH.htm](age-of-ashes-bestiary-items/OBr4iMQYSp8jHCDH.htm)|+2 Greater Resilient Full Plate|
|[OCml3BrX8mx0m7zq.htm](age-of-ashes-bestiary-items/OCml3BrX8mx0m7zq.htm)|True Seeing (Constant)|
|[oeNy6QRmjjdgIkfT.htm](age-of-ashes-bestiary-items/oeNy6QRmjjdgIkfT.htm)|+2 Resilient Full Plate|
|[oFwQEUiMJeXE9xX7.htm](age-of-ashes-bestiary-items/oFwQEUiMJeXE9xX7.htm)|Fly (Constant)|
|[OHsCxCSp0K0AQCsz.htm](age-of-ashes-bestiary-items/OHsCxCSp0K0AQCsz.htm)|Enlarge (Self Only)|
|[oktku6eQTO0fa63B.htm](age-of-ashes-bestiary-items/oktku6eQTO0fa63B.htm)|Scimitar|+2,striking|
|[OMliTjyRbQBsIAzC.htm](age-of-ashes-bestiary-items/OMliTjyRbQBsIAzC.htm)|Dimension Door (At Will)|
|[oqM1d6hyqJXDeB6F.htm](age-of-ashes-bestiary-items/oqM1d6hyqJXDeB6F.htm)|Wasp Trapped in Amber|
|[ou3LZoiUQhZ13e1n.htm](age-of-ashes-bestiary-items/ou3LZoiUQhZ13e1n.htm)|Telekinetic Haul (At Will)|
|[ou4yHaSTmuMZhC5y.htm](age-of-ashes-bestiary-items/ou4yHaSTmuMZhC5y.htm)|Scimitar|+1,striking|
|[oUAmM1vrjuPEVqvT.htm](age-of-ashes-bestiary-items/oUAmM1vrjuPEVqvT.htm)|Longsword|+2,greaterStriking|
|[ouAmQ8tGIKalQy5V.htm](age-of-ashes-bestiary-items/ouAmQ8tGIKalQy5V.htm)|Juggernaut Mutagen (Major) [Infused]|
|[OuGn9uqYP90nZvNX.htm](age-of-ashes-bestiary-items/OuGn9uqYP90nZvNX.htm)|Summon Fiend (Augur Velstrac Only)|
|[oy3YXIlxGdlgBh3N.htm](age-of-ashes-bestiary-items/oy3YXIlxGdlgBh3N.htm)|+1 Resilient Studded Leather Armor|
|[PA0WqIGLWwPFkdAP.htm](age-of-ashes-bestiary-items/PA0WqIGLWwPFkdAP.htm)|Telekinetic Maneuver (At Will)|
|[pcMdAOs6T1huhf1k.htm](age-of-ashes-bestiary-items/pcMdAOs6T1huhf1k.htm)|Greatsword|+2,greaterStriking|
|[pqI4LFJJ8zEZDSlf.htm](age-of-ashes-bestiary-items/pqI4LFJJ8zEZDSlf.htm)|Javelin|+2,greaterStriking|
|[PUIN2KruDbWQEf29.htm](age-of-ashes-bestiary-items/PUIN2KruDbWQEf29.htm)|Scimitar|+2,greaterStriking|
|[Pw6xjk3Ocpe5x0m4.htm](age-of-ashes-bestiary-items/Pw6xjk3Ocpe5x0m4.htm)|Composite Shortbow|+2,greaterStriking|
|[qCH0ksRH4qTOdKil.htm](age-of-ashes-bestiary-items/qCH0ksRH4qTOdKil.htm)|Warhammer|+2,striking,adamantine|
|[qIJ5KclBfbHjmuzk.htm](age-of-ashes-bestiary-items/qIJ5KclBfbHjmuzk.htm)|The Extraplanar Registry|
|[QKwduCpw7Bm9RQv2.htm](age-of-ashes-bestiary-items/QKwduCpw7Bm9RQv2.htm)|Hand Crossbow|+1|
|[qWDx8yQWBVM8oIuO.htm](age-of-ashes-bestiary-items/qWDx8yQWBVM8oIuO.htm)|Shortsword|+2,striking|
|[rHJhscZyJ7wDsGiW.htm](age-of-ashes-bestiary-items/rHJhscZyJ7wDsGiW.htm)|Freedom of Movement (Constant)|
|[RIE5Ws1CBYpHQtCZ.htm](age-of-ashes-bestiary-items/RIE5Ws1CBYpHQtCZ.htm)|Staff|+3,majorStriking|
|[rNS9IXUZDALDQoiJ.htm](age-of-ashes-bestiary-items/rNS9IXUZDALDQoiJ.htm)|Dagger|cold-iron|
|[rVWeTEhRpSWDcpVJ.htm](age-of-ashes-bestiary-items/rVWeTEhRpSWDcpVJ.htm)|Spellbook|
|[rzMtRbnW5Hj2wyGz.htm](age-of-ashes-bestiary-items/rzMtRbnW5Hj2wyGz.htm)|Manacles (Good) (Marked with the Symbol of the Scarlet Triad)|
|[sQoyJLg3FlAvbzAa.htm](age-of-ashes-bestiary-items/sQoyJLg3FlAvbzAa.htm)|+1 Leather Armor|
|[TJEWisVqlHMJM2xw.htm](age-of-ashes-bestiary-items/TJEWisVqlHMJM2xw.htm)|Telekinetic Haul (At Will)|
|[TPKOsROi9hAsKuHY.htm](age-of-ashes-bestiary-items/TPKOsROi9hAsKuHY.htm)|Composite Shortbow|+1|
|[tSQ2I9ViRu4ocrSQ.htm](age-of-ashes-bestiary-items/tSQ2I9ViRu4ocrSQ.htm)|Summon Fiend (Hell Hound Only)|
|[TZaSvXMYX3TRNNtO.htm](age-of-ashes-bestiary-items/TZaSvXMYX3TRNNtO.htm)|Warhammer|+2,greaterStriking,greaterFlaming|
|[UasVzRT5NmsYorFO.htm](age-of-ashes-bestiary-items/UasVzRT5NmsYorFO.htm)|Invisibility (Self Only)|
|[uBfa75DofLNXMiso.htm](age-of-ashes-bestiary-items/uBfa75DofLNXMiso.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|
|[UGsIrubFW2RiOSNY.htm](age-of-ashes-bestiary-items/UGsIrubFW2RiOSNY.htm)|Composite Longbow|+2,striking|
|[UKJ4sJuhWqcOOeYg.htm](age-of-ashes-bestiary-items/UKJ4sJuhWqcOOeYg.htm)|+2 Greater Resilient Chain Mail|
|[UuBYVwcGmnkDWnT3.htm](age-of-ashes-bestiary-items/UuBYVwcGmnkDWnT3.htm)|+1 Dagger (Bonded Item)|+1|
|[uZATYiIAsLS32uLU.htm](age-of-ashes-bestiary-items/uZATYiIAsLS32uLU.htm)|+2 Resilient Adamantine Breastplate|
|[v3tty7UlOB215KUf.htm](age-of-ashes-bestiary-items/v3tty7UlOB215KUf.htm)|Enlarge (Self Only)|
|[VihElpjLJNASMTWo.htm](age-of-ashes-bestiary-items/VihElpjLJNASMTWo.htm)|Nolly's Hoe (+1 Striking Anarchic Halfling Sling Staff)|+1,striking,anarchic|
|[vnegbgEyYoSvmXKq.htm](age-of-ashes-bestiary-items/vnegbgEyYoSvmXKq.htm)|Religious Symbol of Dahak (Gold)|
|[VO094UobV7Jjl6sN.htm](age-of-ashes-bestiary-items/VO094UobV7Jjl6sN.htm)|Longbow|+1|
|[vXOWe8El9JVygflC.htm](age-of-ashes-bestiary-items/vXOWe8El9JVygflC.htm)|Greatclub|+1,striking|
|[w1d8C43oMLZdCVjG.htm](age-of-ashes-bestiary-items/w1d8C43oMLZdCVjG.htm)|Rapier|+1|
|[W24hKhRcyY9H8AQZ.htm](age-of-ashes-bestiary-items/W24hKhRcyY9H8AQZ.htm)|Formula Book|
|[wDV5VP9FUeUWN0DC.htm](age-of-ashes-bestiary-items/wDV5VP9FUeUWN0DC.htm)|Shortsword|+1|
|[wHrD91zmg0bTBIc1.htm](age-of-ashes-bestiary-items/wHrD91zmg0bTBIc1.htm)|Command (At Will)|
|[wNbZrLNPqxS1Uudq.htm](age-of-ashes-bestiary-items/wNbZrLNPqxS1Uudq.htm)|Sap|+1,striking|
|[WugSxYfeO8nxDg3a.htm](age-of-ashes-bestiary-items/WugSxYfeO8nxDg3a.htm)|Religious Symbol of Desna|
|[x41htpR9Ou0sBLB3.htm](age-of-ashes-bestiary-items/x41htpR9Ou0sBLB3.htm)|+2 Greater Resilient Hide Armor|
|[X5l0KEVyuHvwriD3.htm](age-of-ashes-bestiary-items/X5l0KEVyuHvwriD3.htm)|Manacles (Good) (Marked with the Symbol of the Scarlet Triad)|
|[xCHDeiUPDjUNFsyM.htm](age-of-ashes-bestiary-items/xCHDeiUPDjUNFsyM.htm)|Charm (At Will)|
|[Xf6HIqF2ZwvI5TDe.htm](age-of-ashes-bestiary-items/Xf6HIqF2ZwvI5TDe.htm)|Flail|+1|
|[xGyMivb2Rzai1kKv.htm](age-of-ashes-bestiary-items/xGyMivb2Rzai1kKv.htm)|Dagger|+1,striking,silver|
|[xJSY7tPzY307UaTz.htm](age-of-ashes-bestiary-items/xJSY7tPzY307UaTz.htm)|Pick|+1|
|[XSkEHa3VqTvwMo3i.htm](age-of-ashes-bestiary-items/XSkEHa3VqTvwMo3i.htm)|+1 Resilient Breastplate|
|[y1qzP8PTqVnXarTq.htm](age-of-ashes-bestiary-items/y1qzP8PTqVnXarTq.htm)|+2 Greater Resilient Full Plate|
|[Y2PZ0DuMi8XMN6hp.htm](age-of-ashes-bestiary-items/Y2PZ0DuMi8XMN6hp.htm)|Fly (Constant)|
|[Y4NlUcT2NJz7cDOb.htm](age-of-ashes-bestiary-items/Y4NlUcT2NJz7cDOb.htm)|True Seeing (constant)|
|[yA62c4LqjlDWm2Wb.htm](age-of-ashes-bestiary-items/yA62c4LqjlDWm2Wb.htm)|Plane Shift (At Will) (Self Only) (to the Material or Shadow Plane only)|
|[yGOn243Z3mSuZbji.htm](age-of-ashes-bestiary-items/yGOn243Z3mSuZbji.htm)|Freedom of Movement (Constant)|
|[yQWrahMGXUIRdJBO.htm](age-of-ashes-bestiary-items/yQWrahMGXUIRdJBO.htm)|Sound Burst (At Will)|
|[ZcyYi8CE2APPPjbe.htm](age-of-ashes-bestiary-items/ZcyYi8CE2APPPjbe.htm)|Rapier|+1,striking|
|[ZDJ1U0JvyJNUofit.htm](age-of-ashes-bestiary-items/ZDJ1U0JvyJNUofit.htm)|Air Walk (Constant)|
|[ZfxZgIiinmttVMGy.htm](age-of-ashes-bestiary-items/ZfxZgIiinmttVMGy.htm)|Light Hammer|+1,striking|
|[ZmNv9H5jTJd7onVz.htm](age-of-ashes-bestiary-items/ZmNv9H5jTJd7onVz.htm)|Spear|+3,majorStriking,returning|
|[ZpFKjHuyvoYiqXhM.htm](age-of-ashes-bestiary-items/ZpFKjHuyvoYiqXhM.htm)|Crossbow|+1|
|[ZqyK0k31aaaiua44.htm](age-of-ashes-bestiary-items/ZqyK0k31aaaiua44.htm)|Longsword|+1,striking|
|[zWiqjRZ48bqk1oCo.htm](age-of-ashes-bestiary-items/zWiqjRZ48bqk1oCo.htm)|Composite Longbow|+1,striking,flaming|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[055E78h70itsjy7c.htm](age-of-ashes-bestiary-items/055E78h70itsjy7c.htm)|Grab|Agarrado|modificada|
|[06QX0MyzRFRMZtye.htm](age-of-ashes-bestiary-items/06QX0MyzRFRMZtye.htm)|Scimitar|Cimitarra|modificada|
|[09IvgWSDnJI3o0MQ.htm](age-of-ashes-bestiary-items/09IvgWSDnJI3o0MQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0AiUaTUE96AdfKo2.htm](age-of-ashes-bestiary-items/0AiUaTUE96AdfKo2.htm)|Hunt Prey|Perseguir presa|modificada|
|[0At2kB40YzOYoP4s.htm](age-of-ashes-bestiary-items/0At2kB40YzOYoP4s.htm)|Manly Left Hook|Manly Left Hook|modificada|
|[0dYMOi8x5GT5a1Yq.htm](age-of-ashes-bestiary-items/0dYMOi8x5GT5a1Yq.htm)|1-2 Acid Rain|1-2 Lluvia ácida|modificada|
|[0Eqa5Be8keCH6y7q.htm](age-of-ashes-bestiary-items/0Eqa5Be8keCH6y7q.htm)|Light Mace|Maza de luz|modificada|
|[0ew9olScumzeKG3i.htm](age-of-ashes-bestiary-items/0ew9olScumzeKG3i.htm)|Dart|Dardo|modificada|
|[0fMOfuFCNYpqOsRI.htm](age-of-ashes-bestiary-items/0fMOfuFCNYpqOsRI.htm)|Maul|Zarpazo doble|modificada|
|[0J60fBJDJuBW0TbY.htm](age-of-ashes-bestiary-items/0J60fBJDJuBW0TbY.htm)|Dagger|Daga|modificada|
|[0JCPDdK7GNe3JLNR.htm](age-of-ashes-bestiary-items/0JCPDdK7GNe3JLNR.htm)|Tail|Tail|modificada|
|[0mC5JVblweUZ0WxU.htm](age-of-ashes-bestiary-items/0mC5JVblweUZ0WxU.htm)|Jaws|Fauces|modificada|
|[0N1KAl4hWrBUvv2S.htm](age-of-ashes-bestiary-items/0N1KAl4hWrBUvv2S.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0n2OnJwSpxVhh6zy.htm](age-of-ashes-bestiary-items/0n2OnJwSpxVhh6zy.htm)|+3 Status to All Saves vs. Divine Magic|+3 situación a todas las salvaciones contra magia divina.|modificada|
|[0pbel7KeTVy39RkF.htm](age-of-ashes-bestiary-items/0pbel7KeTVy39RkF.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0rm1ESKWoiQPJPo1.htm](age-of-ashes-bestiary-items/0rm1ESKWoiQPJPo1.htm)|Shrieking Frenzy|Shrieking Frenzy|modificada|
|[0RsLlXY365ytSohG.htm](age-of-ashes-bestiary-items/0RsLlXY365ytSohG.htm)|Countermeasures|Contramedidas|modificada|
|[0rU9AWHWD11ECFwB.htm](age-of-ashes-bestiary-items/0rU9AWHWD11ECFwB.htm)|Boundless Reprisals|Represalia ilimitada|modificada|
|[0tHs3BRTN0UIM4j7.htm](age-of-ashes-bestiary-items/0tHs3BRTN0UIM4j7.htm)|Ash Web Spores|Esporas de telara de ceniza|modificada|
|[0tyhJYUBZLs3j7c9.htm](age-of-ashes-bestiary-items/0tyhJYUBZLs3j7c9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[0vtEhVnugRtcZgdh.htm](age-of-ashes-bestiary-items/0vtEhVnugRtcZgdh.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[0YBsTxxadKi2UTcv.htm](age-of-ashes-bestiary-items/0YBsTxxadKi2UTcv.htm)|Scoundrel|Granuja|modificada|
|[0z7Fxpm5UuCECxe5.htm](age-of-ashes-bestiary-items/0z7Fxpm5UuCECxe5.htm)|Efficient Capture|Captura eficiente|modificada|
|[117QFjXSLlaZ1zZl.htm](age-of-ashes-bestiary-items/117QFjXSLlaZ1zZl.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[15vIK60zfhRa5WGH.htm](age-of-ashes-bestiary-items/15vIK60zfhRa5WGH.htm)|Uncanny Bombs|Bombas asombrosas|modificada|
|[15Yz15Mslx43Yuro.htm](age-of-ashes-bestiary-items/15Yz15Mslx43Yuro.htm)|Body Slam|Body Slam|modificada|
|[197Q2S7Pdx8jKLm7.htm](age-of-ashes-bestiary-items/197Q2S7Pdx8jKLm7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[19cNQjbQEVxmiger.htm](age-of-ashes-bestiary-items/19cNQjbQEVxmiger.htm)|Telekinetic Whirlwind|Remolino telecinético|modificada|
|[1AvPepGSMr92x8Jz.htm](age-of-ashes-bestiary-items/1AvPepGSMr92x8Jz.htm)|Fist|Puño|modificada|
|[1BZlxGhTFq59fpA6.htm](age-of-ashes-bestiary-items/1BZlxGhTFq59fpA6.htm)|Infused Items|Equipos infundidos|modificada|
|[1h7ULVYpJvCMccHA.htm](age-of-ashes-bestiary-items/1h7ULVYpJvCMccHA.htm)|Jaws|Fauces|modificada|
|[1mhGkpmF4fQNlduu.htm](age-of-ashes-bestiary-items/1mhGkpmF4fQNlduu.htm)|Rust|Rust|modificada|
|[1o1raLwrZRGxcwrA.htm](age-of-ashes-bestiary-items/1o1raLwrZRGxcwrA.htm)|Destructive Frenzy|Frenesí Destructivo|modificada|
|[1pNOXTSqIPHQk8Lw.htm](age-of-ashes-bestiary-items/1pNOXTSqIPHQk8Lw.htm)|Scent (Imprecise) 120 feet|Olor (Impreciso) 120 pies|modificada|
|[1pOgJ4dFgPo6yfdm.htm](age-of-ashes-bestiary-items/1pOgJ4dFgPo6yfdm.htm)|Dagger|Daga|modificada|
|[1QF8m8mUgLiyrAeW.htm](age-of-ashes-bestiary-items/1QF8m8mUgLiyrAeW.htm)|Swift Capture|Celeridad de Captura|modificada|
|[1qFhgFYCKIDaF8mk.htm](age-of-ashes-bestiary-items/1qFhgFYCKIDaF8mk.htm)|Devour Soul|Devorar alma|modificada|
|[1rRyEALEgAvhDMWc.htm](age-of-ashes-bestiary-items/1rRyEALEgAvhDMWc.htm)|Imitate Door|Imitar Puerta|modificada|
|[1sYunHFicjStIbRN.htm](age-of-ashes-bestiary-items/1sYunHFicjStIbRN.htm)|Begin the Hunt|Comienza la caza|modificada|
|[1WAPJkfVFJzVJi9o.htm](age-of-ashes-bestiary-items/1WAPJkfVFJzVJi9o.htm)|Claw|Garra|modificada|
|[1wkVUb6UVwlesFiF.htm](age-of-ashes-bestiary-items/1wkVUb6UVwlesFiF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1WQFCxrBbvgqJCnn.htm](age-of-ashes-bestiary-items/1WQFCxrBbvgqJCnn.htm)|Claw|Garra|modificada|
|[1wtSED5UzPWNuVfD.htm](age-of-ashes-bestiary-items/1wtSED5UzPWNuVfD.htm)|Negative Healing|Curación negativa|modificada|
|[1Zo0SRU37QPsakyi.htm](age-of-ashes-bestiary-items/1Zo0SRU37QPsakyi.htm)|Thrown Rock|Arrojar roca|modificada|
|[21SVgDPf6zOVZqq8.htm](age-of-ashes-bestiary-items/21SVgDPf6zOVZqq8.htm)|Silver Dagger|Daga de plata|modificada|
|[21tiRXk96SAqD3GL.htm](age-of-ashes-bestiary-items/21tiRXk96SAqD3GL.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[23MQbc0sTY6HeULo.htm](age-of-ashes-bestiary-items/23MQbc0sTY6HeULo.htm)|Rapier|Estoque|modificada|
|[28PBBJfMs67GTIf3.htm](age-of-ashes-bestiary-items/28PBBJfMs67GTIf3.htm)|Leg|Pierna|modificada|
|[29nZUXJwv5knWe9B.htm](age-of-ashes-bestiary-items/29nZUXJwv5knWe9B.htm)|Shoddy Blunderbuss|Trabuco|modificada|
|[2AlP13lCWmjbH1ea.htm](age-of-ashes-bestiary-items/2AlP13lCWmjbH1ea.htm)|Staff|Báculo|modificada|
|[2j69CqOoQwIanSEG.htm](age-of-ashes-bestiary-items/2j69CqOoQwIanSEG.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[2md78psqZkdESFj7.htm](age-of-ashes-bestiary-items/2md78psqZkdESFj7.htm)|Dagger|Daga|modificada|
|[2mthLx7yReK93i3n.htm](age-of-ashes-bestiary-items/2mthLx7yReK93i3n.htm)|Grab|Agarrado|modificada|
|[2MUmPkWIDTgvArGY.htm](age-of-ashes-bestiary-items/2MUmPkWIDTgvArGY.htm)|Greater Alchemist's Fire|Mayor Fuego de Alquimista|modificada|
|[2MzdlMtLJGxFzqB3.htm](age-of-ashes-bestiary-items/2MzdlMtLJGxFzqB3.htm)|Jaws|Fauces|modificada|
|[2nAk1Aknjdxt219t.htm](age-of-ashes-bestiary-items/2nAk1Aknjdxt219t.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2OGTeZ56MmSCLl9P.htm](age-of-ashes-bestiary-items/2OGTeZ56MmSCLl9P.htm)|Terrifying Croak|Aterrador Croak|modificada|
|[2shpOEob027noRHp.htm](age-of-ashes-bestiary-items/2shpOEob027noRHp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2tkYDAIULPFGgxZg.htm](age-of-ashes-bestiary-items/2tkYDAIULPFGgxZg.htm)|3-4 Freezing Wind|3-4 Freezing Wind|modificada|
|[2Xd0tA3QaFBV8Nwk.htm](age-of-ashes-bestiary-items/2Xd0tA3QaFBV8Nwk.htm)|Silver Dagger|Daga de plata|modificada|
|[3Evl5c2LrCI0TdM8.htm](age-of-ashes-bestiary-items/3Evl5c2LrCI0TdM8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3f4ao6e0shuKxhhj.htm](age-of-ashes-bestiary-items/3f4ao6e0shuKxhhj.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[3fAiqgS5r4NDvcsP.htm](age-of-ashes-bestiary-items/3fAiqgS5r4NDvcsP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[3h72FdcRfSluIGqq.htm](age-of-ashes-bestiary-items/3h72FdcRfSluIGqq.htm)|Jaws|Fauces|modificada|
|[3LczZ5phdTxD5h93.htm](age-of-ashes-bestiary-items/3LczZ5phdTxD5h93.htm)|Regeneration 20 (Deactivated by Cold)|Regeneración 20 (Desactivado por el frío)|modificada|
|[3nm3Uxd0Ev8aBu6h.htm](age-of-ashes-bestiary-items/3nm3Uxd0Ev8aBu6h.htm)|Fist|Puño|modificada|
|[3oioSefge5pvcqam.htm](age-of-ashes-bestiary-items/3oioSefge5pvcqam.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[3s5bvgFh0mAe2sgw.htm](age-of-ashes-bestiary-items/3s5bvgFh0mAe2sgw.htm)|Attack of Opportunity (Jaws only)|Ataque de oportunidad (sólo Fauces)|modificada|
|[3T41i9CjiNKKumzp.htm](age-of-ashes-bestiary-items/3T41i9CjiNKKumzp.htm)|Drain Life|Drenar Vida|modificada|
|[3tkJJs6AuFOwEB2K.htm](age-of-ashes-bestiary-items/3tkJJs6AuFOwEB2K.htm)|Resanguinate|Resanguinate|modificada|
|[3Tll1xyzUd3udZZZ.htm](age-of-ashes-bestiary-items/3Tll1xyzUd3udZZZ.htm)|Dragon Heat|Calor dracónico|modificada|
|[3U0dwrOeAIosNI9e.htm](age-of-ashes-bestiary-items/3U0dwrOeAIosNI9e.htm)|Swallow Whole|Engullir Todo|modificada|
|[3ueUOMbC3RvbqrJs.htm](age-of-ashes-bestiary-items/3ueUOMbC3RvbqrJs.htm)|Slice Legs|Cortar piernas|modificada|
|[3xCCKuQS0ngUiwBo.htm](age-of-ashes-bestiary-items/3xCCKuQS0ngUiwBo.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[40tzhYIHgsaEat6r.htm](age-of-ashes-bestiary-items/40tzhYIHgsaEat6r.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[42HRKo2P9cQi4d4N.htm](age-of-ashes-bestiary-items/42HRKo2P9cQi4d4N.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[43THokQpj0pCe1oU.htm](age-of-ashes-bestiary-items/43THokQpj0pCe1oU.htm)|Contingency|Contingencia|modificada|
|[454nSdnfQdhFZ4cJ.htm](age-of-ashes-bestiary-items/454nSdnfQdhFZ4cJ.htm)|Alchemical Formulas|Alchemical Formulas|modificada|
|[49agY84bFYE0480h.htm](age-of-ashes-bestiary-items/49agY84bFYE0480h.htm)|Cold Iron Dagger|Daga de Hierro Frío|modificada|
|[4IWeNCrddBnixyyR.htm](age-of-ashes-bestiary-items/4IWeNCrddBnixyyR.htm)|Dragonstorm Blade|Dragonstorm Blade|modificada|
|[4K4yW5oQfO6cRjez.htm](age-of-ashes-bestiary-items/4K4yW5oQfO6cRjez.htm)|Breath-Seared Sword|Breath-Seared Sword|modificada|
|[4LjhH5zCnNYIijXd.htm](age-of-ashes-bestiary-items/4LjhH5zCnNYIijXd.htm)|Fist|Puño|modificada|
|[4RTJGyTlfqna9dXb.htm](age-of-ashes-bestiary-items/4RTJGyTlfqna9dXb.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[4So35zJiQ6sRy14O.htm](age-of-ashes-bestiary-items/4So35zJiQ6sRy14O.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[4U9ib14fmpR2wh3D.htm](age-of-ashes-bestiary-items/4U9ib14fmpR2wh3D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4VBtm8sTSFf6jv0w.htm](age-of-ashes-bestiary-items/4VBtm8sTSFf6jv0w.htm)|Claw|Garra|modificada|
|[4Wv4ON3tD5f4zi8r.htm](age-of-ashes-bestiary-items/4Wv4ON3tD5f4zi8r.htm)|Shortbow|Arco corto|modificada|
|[4x2IFkW9CrMXOEC5.htm](age-of-ashes-bestiary-items/4x2IFkW9CrMXOEC5.htm)|Dual Abuse|Abuso Dual|modificada|
|[53jaWddHn7SXaQse.htm](age-of-ashes-bestiary-items/53jaWddHn7SXaQse.htm)|Claw|Garra|modificada|
|[5Fp56J3PmkRq827t.htm](age-of-ashes-bestiary-items/5Fp56J3PmkRq827t.htm)|Fast Swallow|Tragar de golpe|modificada|
|[5G4MLloILn0eWN2G.htm](age-of-ashes-bestiary-items/5G4MLloILn0eWN2G.htm)|Divine Providence|Divina providencia|modificada|
|[5MhFpMwgTDRkEcvj.htm](age-of-ashes-bestiary-items/5MhFpMwgTDRkEcvj.htm)|Heat Surge|Oleaje de Calor|modificada|
|[5PFC36aGHaF9Jyoe.htm](age-of-ashes-bestiary-items/5PFC36aGHaF9Jyoe.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[5s7kaeGdKYRnxQlF.htm](age-of-ashes-bestiary-items/5s7kaeGdKYRnxQlF.htm)|Jaws|Fauces|modificada|
|[5SF4QUg5rtkR7Sy2.htm](age-of-ashes-bestiary-items/5SF4QUg5rtkR7Sy2.htm)|Mirror Shield|Escudo espejo|modificada|
|[5UfaoE8p4Wo5eUod.htm](age-of-ashes-bestiary-items/5UfaoE8p4Wo5eUod.htm)|Dagger|Daga|modificada|
|[5uXkMRUCPP1LhCN7.htm](age-of-ashes-bestiary-items/5uXkMRUCPP1LhCN7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[61z76N54xAG9rCLF.htm](age-of-ashes-bestiary-items/61z76N54xAG9rCLF.htm)|Constant Spells|Constant Spells|modificada|
|[6CJRM2LT3a7O3Hhp.htm](age-of-ashes-bestiary-items/6CJRM2LT3a7O3Hhp.htm)|Constrict|Restringir|modificada|
|[6CWSsZ6ciRweK8gJ.htm](age-of-ashes-bestiary-items/6CWSsZ6ciRweK8gJ.htm)|Longsword|Longsword|modificada|
|[6hxF1odU5xuRbrDX.htm](age-of-ashes-bestiary-items/6hxF1odU5xuRbrDX.htm)|Paralysis|Parálisis|modificada|
|[6js1QaHEpOIDiNH1.htm](age-of-ashes-bestiary-items/6js1QaHEpOIDiNH1.htm)|Fist|Puño|modificada|
|[6K2DndSSiWoNraQj.htm](age-of-ashes-bestiary-items/6K2DndSSiWoNraQj.htm)|Opportune Backstab|Pulada trapera oportuna|modificada|
|[6kF6A19eKeAk7fOO.htm](age-of-ashes-bestiary-items/6kF6A19eKeAk7fOO.htm)|Jaws|Fauces|modificada|
|[6kjg8IiIh8GmUJa7.htm](age-of-ashes-bestiary-items/6kjg8IiIh8GmUJa7.htm)|Mobility|Movilidad|modificada|
|[6Lt3GJTTuXRvxjPp.htm](age-of-ashes-bestiary-items/6Lt3GJTTuXRvxjPp.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[6m1q9eJjaPr21j44.htm](age-of-ashes-bestiary-items/6m1q9eJjaPr21j44.htm)|Redirect Energy|Redirigir Energía|modificada|
|[6mPULTjHntSqyOmv.htm](age-of-ashes-bestiary-items/6mPULTjHntSqyOmv.htm)|Javelin|Javelin|modificada|
|[6RB1tHnIwHnJMvAR.htm](age-of-ashes-bestiary-items/6RB1tHnIwHnJMvAR.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[6SHvoR38wFsY94R8.htm](age-of-ashes-bestiary-items/6SHvoR38wFsY94R8.htm)|Negative Healing|Curación negativa|modificada|
|[6sPaytqHJB4OlRwk.htm](age-of-ashes-bestiary-items/6sPaytqHJB4OlRwk.htm)|Dagger|Daga|modificada|
|[6xNQdZfi7r9teJPg.htm](age-of-ashes-bestiary-items/6xNQdZfi7r9teJPg.htm)|Fist|Puño|modificada|
|[6XZ1oUx9X7nxnnLP.htm](age-of-ashes-bestiary-items/6XZ1oUx9X7nxnnLP.htm)|Breath Weapon|Breath Weapon|modificada|
|[7067hk1ZShJt1wS4.htm](age-of-ashes-bestiary-items/7067hk1ZShJt1wS4.htm)|Thrown Weapon Mastery|Maestría con las armas arrojadizas|modificada|
|[70hKpMI6lW9A6okT.htm](age-of-ashes-bestiary-items/70hKpMI6lW9A6okT.htm)|Unnerving Gaze|Unnerving Gaze|modificada|
|[7FqmJPlXVaPfi1Q8.htm](age-of-ashes-bestiary-items/7FqmJPlXVaPfi1Q8.htm)|Bomb Barrage|Bomb Barrage|modificada|
|[7im3eC8rElLqJkOE.htm](age-of-ashes-bestiary-items/7im3eC8rElLqJkOE.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[7imiZn40Mr9yePd1.htm](age-of-ashes-bestiary-items/7imiZn40Mr9yePd1.htm)|Hunted Shot|Tiro cazado|modificada|
|[7LPUMs664rIwb4pr.htm](age-of-ashes-bestiary-items/7LPUMs664rIwb4pr.htm)|Terrifying Squeal|Chillido Aterrador|modificada|
|[7nBGTwZnc14evBxB.htm](age-of-ashes-bestiary-items/7nBGTwZnc14evBxB.htm)|Enternal Damnation|Condenación Enterna|modificada|
|[7nMzSCqgZtUZbgud.htm](age-of-ashes-bestiary-items/7nMzSCqgZtUZbgud.htm)|Frightful Presence|Frightful Presence|modificada|
|[7OH0rFP8UnDTxfdD.htm](age-of-ashes-bestiary-items/7OH0rFP8UnDTxfdD.htm)|Throw Rock|Arrojar roca|modificada|
|[7P75nDnE4avWwiZu.htm](age-of-ashes-bestiary-items/7P75nDnE4avWwiZu.htm)|Breath Weapon|Breath Weapon|modificada|
|[7sAyxHFuVZ6ydyvs.htm](age-of-ashes-bestiary-items/7sAyxHFuVZ6ydyvs.htm)|Regeneration 30 (Deactivated by Cold Iron)|Regeneración 30 (Desactivado por Hierro Frío)|modificada|
|[7SEc5hjoyVGbbNng.htm](age-of-ashes-bestiary-items/7SEc5hjoyVGbbNng.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[7sEHksBXglrNJ2nG.htm](age-of-ashes-bestiary-items/7sEHksBXglrNJ2nG.htm)|Jaws|Fauces|modificada|
|[7wgXd0InH2pMtroD.htm](age-of-ashes-bestiary-items/7wgXd0InH2pMtroD.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[7wokY8VtRk0KvDhV.htm](age-of-ashes-bestiary-items/7wokY8VtRk0KvDhV.htm)|Manifest|Manifiesto|modificada|
|[7WZNigBilIpl5GhG.htm](age-of-ashes-bestiary-items/7WZNigBilIpl5GhG.htm)|Flurry of Daggers|Ráfaga de Dagas|modificada|
|[7YRi5wuplNhSsBsZ.htm](age-of-ashes-bestiary-items/7YRi5wuplNhSsBsZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[85LHfl8B6bj27rPs.htm](age-of-ashes-bestiary-items/85LHfl8B6bj27rPs.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[85uLuXBsMIG8xwb8.htm](age-of-ashes-bestiary-items/85uLuXBsMIG8xwb8.htm)|Fangs|Colmillos|modificada|
|[8BX0w5bbGwyCsIQB.htm](age-of-ashes-bestiary-items/8BX0w5bbGwyCsIQB.htm)|Jaws|Fauces|modificada|
|[8BZpNNw0UfSHhtxH.htm](age-of-ashes-bestiary-items/8BZpNNw0UfSHhtxH.htm)|Mask Settlement|Mask Settlement|modificada|
|[8dMHcMp0PP7ZO27V.htm](age-of-ashes-bestiary-items/8dMHcMp0PP7ZO27V.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[8Hel9Nl2oYP8kVQc.htm](age-of-ashes-bestiary-items/8Hel9Nl2oYP8kVQc.htm)|Sickle|Hoz|modificada|
|[8hzvjvbfBWJpAFep.htm](age-of-ashes-bestiary-items/8hzvjvbfBWJpAFep.htm)|Jaws|Fauces|modificada|
|[8KfKjg5fTmB7ZCal.htm](age-of-ashes-bestiary-items/8KfKjg5fTmB7ZCal.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8l7ngHz7yzWzu3vM.htm](age-of-ashes-bestiary-items/8l7ngHz7yzWzu3vM.htm)|Elven Curve Blade|Hoja curva élfica|modificada|
|[8nd5VVeytqa77pD8.htm](age-of-ashes-bestiary-items/8nd5VVeytqa77pD8.htm)|Rapier|Estoque|modificada|
|[8NJr1zJWkswMN2qH.htm](age-of-ashes-bestiary-items/8NJr1zJWkswMN2qH.htm)|Reactive|Reactive|modificada|
|[8OAdLpknHQWsmvmE.htm](age-of-ashes-bestiary-items/8OAdLpknHQWsmvmE.htm)|Orange Eye Beam|Rayo Ojo Naranja|modificada|
|[8sgTj0l0kmAK5fpR.htm](age-of-ashes-bestiary-items/8sgTj0l0kmAK5fpR.htm)|Collapse Ceiling|Desplomar techo|modificada|
|[8V6geW11QKk6SncS.htm](age-of-ashes-bestiary-items/8V6geW11QKk6SncS.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[8XB58su9CoZLqKH7.htm](age-of-ashes-bestiary-items/8XB58su9CoZLqKH7.htm)|Fist|Puño|modificada|
|[8XwJYwcx1a2Md13M.htm](age-of-ashes-bestiary-items/8XwJYwcx1a2Md13M.htm)|Tail|Tail|modificada|
|[91EDD4ZPxtMkhm0e.htm](age-of-ashes-bestiary-items/91EDD4ZPxtMkhm0e.htm)|Negative Healing|Curación negativa|modificada|
|[95BHm1GQDEedURfw.htm](age-of-ashes-bestiary-items/95BHm1GQDEedURfw.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[97RfnojA9doZCy1G.htm](age-of-ashes-bestiary-items/97RfnojA9doZCy1G.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[9A3DxTCqkhPOFzSd.htm](age-of-ashes-bestiary-items/9A3DxTCqkhPOFzSd.htm)|Weapon Supremacy|Supremacía con las armas|modificada|
|[9APnpN5PuTxG8v98.htm](age-of-ashes-bestiary-items/9APnpN5PuTxG8v98.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[9b70qoAMc6GR4eCV.htm](age-of-ashes-bestiary-items/9b70qoAMc6GR4eCV.htm)|Negative Healing|Curación negativa|modificada|
|[9dzKJk6ywUvYJxGr.htm](age-of-ashes-bestiary-items/9dzKJk6ywUvYJxGr.htm)|Efficient Capture|Captura eficiente|modificada|
|[9EtKLNxC3l6ph7LD.htm](age-of-ashes-bestiary-items/9EtKLNxC3l6ph7LD.htm)|Eight Coils|Ocho Retorcer|modificada|
|[9jVBK8J30sirxvYJ.htm](age-of-ashes-bestiary-items/9jVBK8J30sirxvYJ.htm)|Javelin|Javelin|modificada|
|[9kCNk71urnz8zzJ8.htm](age-of-ashes-bestiary-items/9kCNk71urnz8zzJ8.htm)|Frighten|Asustar|modificada|
|[9Ogunu3AaCrgsxpU.htm](age-of-ashes-bestiary-items/9Ogunu3AaCrgsxpU.htm)|Telekinetic Crystalline Assault|Asalto cristalino telecinético|modificada|
|[9RaYMbm66QxVktif.htm](age-of-ashes-bestiary-items/9RaYMbm66QxVktif.htm)|Shipboard Savvy|Experiencia en embarcaciones|modificada|
|[9SKMKDVUzRsDh3cL.htm](age-of-ashes-bestiary-items/9SKMKDVUzRsDh3cL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9uKEXs9NLIvxXisk.htm](age-of-ashes-bestiary-items/9uKEXs9NLIvxXisk.htm)|Green Eye Beam|Rayo Ojo Verde|modificada|
|[9V1OCxFjilGcKV8s.htm](age-of-ashes-bestiary-items/9V1OCxFjilGcKV8s.htm)|Major Frost Vial|Gélida Vial de escarcha, superior|modificada|
|[9x8nl6F6ULV0kslr.htm](age-of-ashes-bestiary-items/9x8nl6F6ULV0kslr.htm)|Tongue|Lengua|modificada|
|[9yNmVUtApEo0Ky5V.htm](age-of-ashes-bestiary-items/9yNmVUtApEo0Ky5V.htm)|Dead Spells|Hechizos Muertos|modificada|
|[A4k38xHP7GlSzTCt.htm](age-of-ashes-bestiary-items/A4k38xHP7GlSzTCt.htm)|Whip|Látigo|modificada|
|[A4w2Db2zI5g3QPOa.htm](age-of-ashes-bestiary-items/A4w2Db2zI5g3QPOa.htm)|Negative Healing|Curación negativa|modificada|
|[abiEy2lj68FImtR8.htm](age-of-ashes-bestiary-items/abiEy2lj68FImtR8.htm)|Enslave Soul|Enslave Soul|modificada|
|[AdsHpmWCgQto5myp.htm](age-of-ashes-bestiary-items/AdsHpmWCgQto5myp.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[AGJUwsQFIjEtEsuB.htm](age-of-ashes-bestiary-items/AGJUwsQFIjEtEsuB.htm)|Natural Invisibility|Invisibilidad Natural|modificada|
|[aGM9Fk35YNBZ55lZ.htm](age-of-ashes-bestiary-items/aGM9Fk35YNBZ55lZ.htm)|Eternal Damnation|Condenación eterna|modificada|
|[agUYP6p6kB6OegCM.htm](age-of-ashes-bestiary-items/agUYP6p6kB6OegCM.htm)|Verdant Staff|Bastón frondoso|modificada|
|[agvYAJn23dKfPmcI.htm](age-of-ashes-bestiary-items/agvYAJn23dKfPmcI.htm)|Branch|Rama|modificada|
|[aGyOlzElbjBAx2ru.htm](age-of-ashes-bestiary-items/aGyOlzElbjBAx2ru.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[AHGxpzkTgjwABgBa.htm](age-of-ashes-bestiary-items/AHGxpzkTgjwABgBa.htm)|Pseudopod|Pseudópodo|modificada|
|[AhUdp5npAh56MGtg.htm](age-of-ashes-bestiary-items/AhUdp5npAh56MGtg.htm)|Shell Spikes|Shell Spikes|modificada|
|[AjWArPc1kXvPmWvW.htm](age-of-ashes-bestiary-items/AjWArPc1kXvPmWvW.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[AKFVNPYKCZ4jZLKw.htm](age-of-ashes-bestiary-items/AKFVNPYKCZ4jZLKw.htm)|Necrotic Pulse|Pulso Necrótico|modificada|
|[AkuMvwXObEyEbsN4.htm](age-of-ashes-bestiary-items/AkuMvwXObEyEbsN4.htm)|Corrosive Whip|Látigo Corrosivo|modificada|
|[Am5pbCNVDkDBNz5Y.htm](age-of-ashes-bestiary-items/Am5pbCNVDkDBNz5Y.htm)|Stench|Hedor|modificada|
|[aM8ZIzzGKo3LwAaz.htm](age-of-ashes-bestiary-items/aM8ZIzzGKo3LwAaz.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[aMtBg34djNUQyLrP.htm](age-of-ashes-bestiary-items/aMtBg34djNUQyLrP.htm)|Telepathy 1 Mile|Telepatía 1 milla|modificada|
|[anEeBSbM8nCZzVZ4.htm](age-of-ashes-bestiary-items/anEeBSbM8nCZzVZ4.htm)|Scent (Imprecise) 120 feet|Olor (Impreciso) 120 pies|modificada|
|[aocJaGIdE31itQEp.htm](age-of-ashes-bestiary-items/aocJaGIdE31itQEp.htm)|Swift Slash|Celeridad Slash|modificada|
|[aod69CMkW6wAO7gp.htm](age-of-ashes-bestiary-items/aod69CMkW6wAO7gp.htm)|Spear|Lanza|modificada|
|[AP6aPJLtb3i0qgVG.htm](age-of-ashes-bestiary-items/AP6aPJLtb3i0qgVG.htm)|Dagger|Daga|modificada|
|[Ari4IpvWQTIYV64K.htm](age-of-ashes-bestiary-items/Ari4IpvWQTIYV64K.htm)|Yellow Eye Beam|Yellow Eye Beam|modificada|
|[aTXAOXeP9oRRcBfj.htm](age-of-ashes-bestiary-items/aTXAOXeP9oRRcBfj.htm)|Grasping Hands|Manos que agarran|modificada|
|[axG8Gqi3ZFCSb6l3.htm](age-of-ashes-bestiary-items/axG8Gqi3ZFCSb6l3.htm)|Staff|Báculo|modificada|
|[AXpLna1FjT83aiYJ.htm](age-of-ashes-bestiary-items/AXpLna1FjT83aiYJ.htm)|Tormenting Touch|Toque Atormentador|modificada|
|[AxvNayOm3jws9vMf.htm](age-of-ashes-bestiary-items/AxvNayOm3jws9vMf.htm)|Constrict|Restringir|modificada|
|[ayGFqy96KRzgh1Jx.htm](age-of-ashes-bestiary-items/ayGFqy96KRzgh1Jx.htm)|Magma Splash|Salpicadura de magma|modificada|
|[ayPIYrcz520FXkl7.htm](age-of-ashes-bestiary-items/ayPIYrcz520FXkl7.htm)|Sap|Sap|modificada|
|[AzkWSZ1llpwDtMLX.htm](age-of-ashes-bestiary-items/AzkWSZ1llpwDtMLX.htm)|Spin Silk|Spin Silk|modificada|
|[B2pDIbpVOV0J4SPQ.htm](age-of-ashes-bestiary-items/B2pDIbpVOV0J4SPQ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[B2pWjBsWDmisLrz0.htm](age-of-ashes-bestiary-items/B2pWjBsWDmisLrz0.htm)|Running Poison Strike|Golpe Venenoso Ejecutado|modificada|
|[b4gT3E9tko3GkAav.htm](age-of-ashes-bestiary-items/b4gT3E9tko3GkAav.htm)|Dagger|Daga|modificada|
|[B6yK16tyh4kEH9SU.htm](age-of-ashes-bestiary-items/B6yK16tyh4kEH9SU.htm)|Negative Healing|Curación negativa|modificada|
|[b8bCRfgf4RGqbIsZ.htm](age-of-ashes-bestiary-items/b8bCRfgf4RGqbIsZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[b9ku7PIEzSu9iFAq.htm](age-of-ashes-bestiary-items/b9ku7PIEzSu9iFAq.htm)|Breath Weapon|Breath Weapon|modificada|
|[bbD52Yw4aCU9xjzy.htm](age-of-ashes-bestiary-items/bbD52Yw4aCU9xjzy.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[BbG4VRHLFcOdy1kI.htm](age-of-ashes-bestiary-items/BbG4VRHLFcOdy1kI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bbL3QbriTw1sy7so.htm](age-of-ashes-bestiary-items/bbL3QbriTw1sy7so.htm)|Belch Smoke|Eructo de humo|modificada|
|[bc7DxJeQl2oX946w.htm](age-of-ashes-bestiary-items/bc7DxJeQl2oX946w.htm)|Woodland Stride|Paso forestal|modificada|
|[BCcVa7LcDbfMataD.htm](age-of-ashes-bestiary-items/BCcVa7LcDbfMataD.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[bD1gctOA2Ni82NSM.htm](age-of-ashes-bestiary-items/bD1gctOA2Ni82NSM.htm)|Breath-Seared Greatsword|Breath-Seared Greatsword|modificada|
|[BDUlQffqQeFK7d1K.htm](age-of-ashes-bestiary-items/BDUlQffqQeFK7d1K.htm)|Focused Breath Weapon|Focused Breath Weapon|modificada|
|[BEdK4O8CNtdvGUvX.htm](age-of-ashes-bestiary-items/BEdK4O8CNtdvGUvX.htm)|Shadow Strike|Golpe de Sombra|modificada|
|[bFgmfCe3rBGAWZK1.htm](age-of-ashes-bestiary-items/bFgmfCe3rBGAWZK1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bfmkqrbi3vcWnEQ8.htm](age-of-ashes-bestiary-items/bfmkqrbi3vcWnEQ8.htm)|Hunt Prey|Perseguir presa|modificada|
|[bFQ8s2UKCuHaBspm.htm](age-of-ashes-bestiary-items/bFQ8s2UKCuHaBspm.htm)|Noxious Breath|Aliento Nocivo|modificada|
|[BiJxTD0UCp5DwYqu.htm](age-of-ashes-bestiary-items/BiJxTD0UCp5DwYqu.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[BIvhzgu4Bi6t171L.htm](age-of-ashes-bestiary-items/BIvhzgu4Bi6t171L.htm)|Deny Advantage|Denegar ventaja|modificada|
|[Bizy0PzkQAYT8X7H.htm](age-of-ashes-bestiary-items/Bizy0PzkQAYT8X7H.htm)|Heat Rock|Calentar roca|modificada|
|[BJZe244H2HjWtcpq.htm](age-of-ashes-bestiary-items/BJZe244H2HjWtcpq.htm)|Lava Bomb|Bomba de lava|modificada|
|[bK1xdYaxhyB8kJli.htm](age-of-ashes-bestiary-items/bK1xdYaxhyB8kJli.htm)|Sudden Charge|Carga súbita|modificada|
|[BKBcAo4en90X1m11.htm](age-of-ashes-bestiary-items/BKBcAo4en90X1m11.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[BKJk7pGIcKhoCKA6.htm](age-of-ashes-bestiary-items/BKJk7pGIcKhoCKA6.htm)|Forge Breath|Aliento de fragua|modificada|
|[bLoaxjzxhJesDzcL.htm](age-of-ashes-bestiary-items/bLoaxjzxhJesDzcL.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[bnOBGha4PmmSNkZJ.htm](age-of-ashes-bestiary-items/bnOBGha4PmmSNkZJ.htm)|Stench|Hedor|modificada|
|[Brr7VPPaAfdPuEoy.htm](age-of-ashes-bestiary-items/Brr7VPPaAfdPuEoy.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[BSFOYE4jxrwLzw75.htm](age-of-ashes-bestiary-items/BSFOYE4jxrwLzw75.htm)|Regeneration 25|Regeneración 25|modificada|
|[bTTWJNcprDJ2oony.htm](age-of-ashes-bestiary-items/bTTWJNcprDJ2oony.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[BtXKfb6sra2e5xGW.htm](age-of-ashes-bestiary-items/BtXKfb6sra2e5xGW.htm)|Tail|Tail|modificada|
|[BuGYWvXzoxguBhaX.htm](age-of-ashes-bestiary-items/BuGYWvXzoxguBhaX.htm)|Quick Dervish Strike|Golpe de derviche rápido|modificada|
|[bUNqaCaQSkkB6se8.htm](age-of-ashes-bestiary-items/bUNqaCaQSkkB6se8.htm)|+1 Status to All Saves vs. Mental|+1 situación a todas las salvaciones contra mental.|modificada|
|[BvK09Yczybu8kLG6.htm](age-of-ashes-bestiary-items/BvK09Yczybu8kLG6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bvXcRvh1dBNmRcSg.htm](age-of-ashes-bestiary-items/bvXcRvh1dBNmRcSg.htm)|Live a Thousand Lives|Vive Mil Vidas|modificada|
|[BWXMuY5UwGtZJiaG.htm](age-of-ashes-bestiary-items/BWXMuY5UwGtZJiaG.htm)|Knockdown|Derribo|modificada|
|[C18MqFsSOmOKFyGk.htm](age-of-ashes-bestiary-items/C18MqFsSOmOKFyGk.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[c2BbfXmMmIl5wmhi.htm](age-of-ashes-bestiary-items/c2BbfXmMmIl5wmhi.htm)|Infrared Vision|Visión Infrarroja|modificada|
|[C5HAmEH4wICnDXXA.htm](age-of-ashes-bestiary-items/C5HAmEH4wICnDXXA.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[C9IfyXUudzvj3eop.htm](age-of-ashes-bestiary-items/C9IfyXUudzvj3eop.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[CadzQEjQFp41MtkP.htm](age-of-ashes-bestiary-items/CadzQEjQFp41MtkP.htm)|Blowgun|Blowgun|modificada|
|[cbNmcndriv0G3Pqu.htm](age-of-ashes-bestiary-items/cbNmcndriv0G3Pqu.htm)|Dagger|Daga|modificada|
|[cBPi7wQqwCZJ4GR7.htm](age-of-ashes-bestiary-items/cBPi7wQqwCZJ4GR7.htm)|Efficient Capture|Captura eficiente|modificada|
|[CCddbUlZDpZVHPi9.htm](age-of-ashes-bestiary-items/CCddbUlZDpZVHPi9.htm)|Arcane Spells Known|Hechizos Arcanos Conocidos|modificada|
|[ceaQ24wBGNxFLxDc.htm](age-of-ashes-bestiary-items/ceaQ24wBGNxFLxDc.htm)|Dagger|Daga|modificada|
|[CFIFmdujP4DOl3KL.htm](age-of-ashes-bestiary-items/CFIFmdujP4DOl3KL.htm)|Thrown Weapon Mastery|Maestría con las armas arrojadizas|modificada|
|[CgQzFuFeDdZQSFQL.htm](age-of-ashes-bestiary-items/CgQzFuFeDdZQSFQL.htm)|Jaws|Fauces|modificada|
|[CJe3a5GahNX20FL8.htm](age-of-ashes-bestiary-items/CJe3a5GahNX20FL8.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CKxM9QsObYnbO5fx.htm](age-of-ashes-bestiary-items/CKxM9QsObYnbO5fx.htm)|Dagger|Daga|modificada|
|[CLQtaRBZzEVhkEpc.htm](age-of-ashes-bestiary-items/CLQtaRBZzEVhkEpc.htm)|Staff Gems|Gemas de bastón|modificada|
|[CP15DfrCLkhY6tX0.htm](age-of-ashes-bestiary-items/CP15DfrCLkhY6tX0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cP2We7SRbI7YuRzt.htm](age-of-ashes-bestiary-items/cP2We7SRbI7YuRzt.htm)|Constrict|Restringir|modificada|
|[cpgcQN4JOvn67e7q.htm](age-of-ashes-bestiary-items/cpgcQN4JOvn67e7q.htm)|Jaws|Fauces|modificada|
|[cstX7ITEtTu7WN4L.htm](age-of-ashes-bestiary-items/cstX7ITEtTu7WN4L.htm)|Vine Lash|Vine Lash|modificada|
|[cSY3pNZ6AkV4mhbe.htm](age-of-ashes-bestiary-items/cSY3pNZ6AkV4mhbe.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[ct0XvSPn4vZOIhf3.htm](age-of-ashes-bestiary-items/ct0XvSPn4vZOIhf3.htm)|Punish Imperfection|Castigar la imperfección|modificada|
|[CU86HVtetBBtNPBx.htm](age-of-ashes-bestiary-items/CU86HVtetBBtNPBx.htm)|Dragonstorm Strike|Golpe Tormenta de Dragones|modificada|
|[cUPegejySeJGT7RK.htm](age-of-ashes-bestiary-items/cUPegejySeJGT7RK.htm)|Corpse Disguise|Disfraz de Cadáver|modificada|
|[CVnZaJ028qTQ4fIE.htm](age-of-ashes-bestiary-items/CVnZaJ028qTQ4fIE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D1apG8hCM6fFYauw.htm](age-of-ashes-bestiary-items/D1apG8hCM6fFYauw.htm)|Breath Weapon|Breath Weapon|modificada|
|[d5HPFb8SkWD4lJZS.htm](age-of-ashes-bestiary-items/d5HPFb8SkWD4lJZS.htm)|Dagger|Daga|modificada|
|[D72zZj9nlTG1c06S.htm](age-of-ashes-bestiary-items/D72zZj9nlTG1c06S.htm)|Horns|Cuernos|modificada|
|[d8AM5zYWUUZUAdDc.htm](age-of-ashes-bestiary-items/d8AM5zYWUUZUAdDc.htm)|Batter the Fallen|Apalear a los caídos|modificada|
|[D8iQ4Gr9GPDsrYgl.htm](age-of-ashes-bestiary-items/D8iQ4Gr9GPDsrYgl.htm)|Searing Heat|Searing Heat|modificada|
|[D9pjDoEUMn7yYSQT.htm](age-of-ashes-bestiary-items/D9pjDoEUMn7yYSQT.htm)|Mudwalking|Mudwalking|modificada|
|[Da7jV5DGeFRpV5qb.htm](age-of-ashes-bestiary-items/Da7jV5DGeFRpV5qb.htm)|Unleash Fragments|Desatar Fragmentos|modificada|
|[DCLGBKaqRvqYPFZZ.htm](age-of-ashes-bestiary-items/DCLGBKaqRvqYPFZZ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[DCw8NyXCQyGIT7dh.htm](age-of-ashes-bestiary-items/DCw8NyXCQyGIT7dh.htm)|Spear (Thrown)|Lanza (Arrojada)|modificada|
|[dDFsE7xMeGqFnlPS.htm](age-of-ashes-bestiary-items/dDFsE7xMeGqFnlPS.htm)|Firebleed|Firebleed|modificada|
|[DdM0HuOCPDC9g1gC.htm](age-of-ashes-bestiary-items/DdM0HuOCPDC9g1gC.htm)|Rend|Rasgadura|modificada|
|[DDzd3ocX0kkjYD9e.htm](age-of-ashes-bestiary-items/DDzd3ocX0kkjYD9e.htm)|Trap Dodger|Esquiva Trampas|modificada|
|[delWZwlGtrbwQofE.htm](age-of-ashes-bestiary-items/delWZwlGtrbwQofE.htm)|Shell Game|Shell Game|modificada|
|[DEoIu6kGyGbwbSn3.htm](age-of-ashes-bestiary-items/DEoIu6kGyGbwbSn3.htm)|Consume Flesh|Consumir carne|modificada|
|[DF8C1D50NHzFNPco.htm](age-of-ashes-bestiary-items/DF8C1D50NHzFNPco.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[DFdEgb4naWOYCPXX.htm](age-of-ashes-bestiary-items/DFdEgb4naWOYCPXX.htm)|Site Bound|Ligado a una ubicación|modificada|
|[dfNLOS9TpYGudTfO.htm](age-of-ashes-bestiary-items/dfNLOS9TpYGudTfO.htm)|Improved Grab|Agarrado mejorado|modificada|
|[DH9wyO9XNqPjz1gc.htm](age-of-ashes-bestiary-items/DH9wyO9XNqPjz1gc.htm)|Bonds of Iron|Ligaduras de hierro|modificada|
|[dI9RG9Y1v3nG3JkI.htm](age-of-ashes-bestiary-items/dI9RG9Y1v3nG3JkI.htm)|Longbow|Arco largo|modificada|
|[DjmGUYM8SGPzUgRQ.htm](age-of-ashes-bestiary-items/DjmGUYM8SGPzUgRQ.htm)|Telekinetic Assault|Asalto telecinético|modificada|
|[DJoAQhbwF6EFhlAL.htm](age-of-ashes-bestiary-items/DJoAQhbwF6EFhlAL.htm)|Swallow Whole|Engullir Todo|modificada|
|[dKp6yeEMkDRQTEq9.htm](age-of-ashes-bestiary-items/dKp6yeEMkDRQTEq9.htm)|Greatclub|Greatclub|modificada|
|[DlwhGb66IrdDlQFU.htm](age-of-ashes-bestiary-items/DlwhGb66IrdDlQFU.htm)|Hand|Mano|modificada|
|[Dmc8CB0p5bleq65p.htm](age-of-ashes-bestiary-items/Dmc8CB0p5bleq65p.htm)|Soul Shriek|Soul Shriek|modificada|
|[DMn2jw41YgSewTab.htm](age-of-ashes-bestiary-items/DMn2jw41YgSewTab.htm)|Leg|Pierna|modificada|
|[Do1UK3MaHGPk6Raa.htm](age-of-ashes-bestiary-items/Do1UK3MaHGPk6Raa.htm)|Shrieking Frenzy|Shrieking Frenzy|modificada|
|[doKYunVCGEcZDNXb.htm](age-of-ashes-bestiary-items/doKYunVCGEcZDNXb.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[DOuyoqgTXzVADMKJ.htm](age-of-ashes-bestiary-items/DOuyoqgTXzVADMKJ.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[DPwtg3OqirDYbsrI.htm](age-of-ashes-bestiary-items/DPwtg3OqirDYbsrI.htm)|Focus Gaze|Centrar mirada|modificada|
|[dQFY1Er30mYSlRnj.htm](age-of-ashes-bestiary-items/dQFY1Er30mYSlRnj.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[DqIf07lCKF6aemjq.htm](age-of-ashes-bestiary-items/DqIf07lCKF6aemjq.htm)|Jungle Stride|Zancada selvática|modificada|
|[dqiO3lCiS7jAYB3f.htm](age-of-ashes-bestiary-items/dqiO3lCiS7jAYB3f.htm)|Improved Evasion|Evasión mejorada.|modificada|
|[dU7uiLZral2AnGbC.htm](age-of-ashes-bestiary-items/dU7uiLZral2AnGbC.htm)|Confuse|Confusión|modificada|
|[DuTAy3tlbbhX6YSj.htm](age-of-ashes-bestiary-items/DuTAy3tlbbhX6YSj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DxojGnsJOG0ItLxo.htm](age-of-ashes-bestiary-items/DxojGnsJOG0ItLxo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DyilSr4hYYAgoPFF.htm](age-of-ashes-bestiary-items/DyilSr4hYYAgoPFF.htm)|Reverberating Revenge|Reverberating Revenge|modificada|
|[Dz4yG96ucUj1zePA.htm](age-of-ashes-bestiary-items/Dz4yG96ucUj1zePA.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[e0qpil2ll45f3L8R.htm](age-of-ashes-bestiary-items/e0qpil2ll45f3L8R.htm)|Greater Thunderstone|Piedra de trueno, mayor|modificada|
|[e2jWaDeB99NST1wo.htm](age-of-ashes-bestiary-items/e2jWaDeB99NST1wo.htm)|Negative Healing|Curación negativa|modificada|
|[e2Kf0nqsKQ0S7M4n.htm](age-of-ashes-bestiary-items/e2Kf0nqsKQ0S7M4n.htm)|Smoky Retreat|Smoky Retreat|modificada|
|[E6DkiwmjLTAKqArb.htm](age-of-ashes-bestiary-items/E6DkiwmjLTAKqArb.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[e73hhyixLdkOAGhD.htm](age-of-ashes-bestiary-items/e73hhyixLdkOAGhD.htm)|Javelin|Javelin|modificada|
|[e7C7CLUio6dT7UwM.htm](age-of-ashes-bestiary-items/e7C7CLUio6dT7UwM.htm)|Web|Telara|modificada|
|[ebQ7QkhGrSSsXuy0.htm](age-of-ashes-bestiary-items/ebQ7QkhGrSSsXuy0.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[EcbzD7TsltUXe4Ge.htm](age-of-ashes-bestiary-items/EcbzD7TsltUXe4Ge.htm)|Wing|Ala|modificada|
|[eCRAMtpF3k73LRFd.htm](age-of-ashes-bestiary-items/eCRAMtpF3k73LRFd.htm)|Paralysis|Parálisis|modificada|
|[EDnXO2OsX5YQGXQy.htm](age-of-ashes-bestiary-items/EDnXO2OsX5YQGXQy.htm)|Trample|Trample|modificada|
|[EDSriTe4fC1hsq33.htm](age-of-ashes-bestiary-items/EDSriTe4fC1hsq33.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[EferdcZmtLTCxvjj.htm](age-of-ashes-bestiary-items/EferdcZmtLTCxvjj.htm)|Staff Mastery|Dominio del Bastón|modificada|
|[efWcncs8XDaP6xQA.htm](age-of-ashes-bestiary-items/efWcncs8XDaP6xQA.htm)|Corrosive Dagger|Daga Corrosiva|modificada|
|[Egs11D8l9LMkS7uI.htm](age-of-ashes-bestiary-items/Egs11D8l9LMkS7uI.htm)|Needle Rain|Lluvia de Agujas|modificada|
|[ekJjxL6yjWuexk2l.htm](age-of-ashes-bestiary-items/ekJjxL6yjWuexk2l.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[Elbm7mEpf5b1tHsf.htm](age-of-ashes-bestiary-items/Elbm7mEpf5b1tHsf.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[EM1Qpnk3JXilo1Qb.htm](age-of-ashes-bestiary-items/EM1Qpnk3JXilo1Qb.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[EMNCDSnJYrVEceeF.htm](age-of-ashes-bestiary-items/EMNCDSnJYrVEceeF.htm)|Fangs|Colmillos|modificada|
|[eMRmzaCOQXRtZ69R.htm](age-of-ashes-bestiary-items/eMRmzaCOQXRtZ69R.htm)|Power Attack|Ataque poderoso|modificada|
|[EorcRyybCCQIkYR8.htm](age-of-ashes-bestiary-items/EorcRyybCCQIkYR8.htm)|Woodland Elf|Elfo silvano|modificada|
|[ePFuv9mcwpdpUPHc.htm](age-of-ashes-bestiary-items/ePFuv9mcwpdpUPHc.htm)|Claw|Garra|modificada|
|[EPvcNjklx5lHjwSK.htm](age-of-ashes-bestiary-items/EPvcNjklx5lHjwSK.htm)|Counterspell|Contraconjuro|modificada|
|[eQxLyu1oOuefKVbK.htm](age-of-ashes-bestiary-items/eQxLyu1oOuefKVbK.htm)|+2 Circumstance to All Saves to Disbelieve Illusions|+2 Circunstancia a todas las salvaciones para poner en duda ilusiones.|modificada|
|[eTaUxizC6y1b7l3T.htm](age-of-ashes-bestiary-items/eTaUxizC6y1b7l3T.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[ETzIyo5ZvX3NpC3J.htm](age-of-ashes-bestiary-items/ETzIyo5ZvX3NpC3J.htm)|Obliteration Beam|Obliteration Beam|modificada|
|[EuBw3avFZNz7BacT.htm](age-of-ashes-bestiary-items/EuBw3avFZNz7BacT.htm)|Dagger|Daga|modificada|
|[eV1zBAr9CgzQ2C5X.htm](age-of-ashes-bestiary-items/eV1zBAr9CgzQ2C5X.htm)|Spore Explosion|Explosión de Esporas.|modificada|
|[eVfKn6MTgzYtBFUg.htm](age-of-ashes-bestiary-items/eVfKn6MTgzYtBFUg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eWRYW3sMrzor03KL.htm](age-of-ashes-bestiary-items/eWRYW3sMrzor03KL.htm)|Frightful Presence|Frightful Presence|modificada|
|[eX2VLJ64ecfgdr3K.htm](age-of-ashes-bestiary-items/eX2VLJ64ecfgdr3K.htm)|Scimitar|Cimitarra|modificada|
|[EYE5NG1a4JuddG5h.htm](age-of-ashes-bestiary-items/EYE5NG1a4JuddG5h.htm)|Perception and Vision|Percepción y Visión|modificada|
|[F2trczQsM5ejlXEm.htm](age-of-ashes-bestiary-items/F2trczQsM5ejlXEm.htm)|Jaws|Fauces|modificada|
|[F3y7pLxdrd17fqdw.htm](age-of-ashes-bestiary-items/F3y7pLxdrd17fqdw.htm)|Death Throes|Estertores de muerte|modificada|
|[fBvRbrhTqjBsWGVa.htm](age-of-ashes-bestiary-items/fBvRbrhTqjBsWGVa.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[FDGrNkxmckSR32ou.htm](age-of-ashes-bestiary-items/FDGrNkxmckSR32ou.htm)|Forge Breath|Aliento de fragua|modificada|
|[fINveZMkSiZN9kMX.htm](age-of-ashes-bestiary-items/fINveZMkSiZN9kMX.htm)|Expunge|Expurgar|modificada|
|[Fj81D52uCvQB3Zp7.htm](age-of-ashes-bestiary-items/Fj81D52uCvQB3Zp7.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[fjgBPuKgD22yTRuQ.htm](age-of-ashes-bestiary-items/fjgBPuKgD22yTRuQ.htm)|Inject Unstable Mutagen|Inyectar mutágeno inestable|modificada|
|[fjxiygmwqMkFDeyM.htm](age-of-ashes-bestiary-items/fjxiygmwqMkFDeyM.htm)|Constant Spells|Constant Spells|modificada|
|[FP0lfB9biTbzRAwD.htm](age-of-ashes-bestiary-items/FP0lfB9biTbzRAwD.htm)|Tail|Tail|modificada|
|[FQumD5lxgm3RrE1g.htm](age-of-ashes-bestiary-items/FQumD5lxgm3RrE1g.htm)|Blindsight (Precise) 120 feet|Vista ciega (precisión) 120 pies.|modificada|
|[FrfAFUxOL2jRQG6y.htm](age-of-ashes-bestiary-items/FrfAFUxOL2jRQG6y.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fRTC42KFOaC3bfxs.htm](age-of-ashes-bestiary-items/fRTC42KFOaC3bfxs.htm)|Disrupting Cold Iron Dagger|Disruptora Daga de Hierro Frío|modificada|
|[FwcS5N0auHwePEe8.htm](age-of-ashes-bestiary-items/FwcS5N0auHwePEe8.htm)|Adroit Disarm|Desarme experto|modificada|
|[fWPGUdYIRBw22fnz.htm](age-of-ashes-bestiary-items/fWPGUdYIRBw22fnz.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[fXz6WRXGipzaUAhE.htm](age-of-ashes-bestiary-items/fXz6WRXGipzaUAhE.htm)|Radiant Explosion|Explosión Radiante|modificada|
|[fyWgrVIE0U0GZARU.htm](age-of-ashes-bestiary-items/fyWgrVIE0U0GZARU.htm)|Lifelike Scintillation|Scintillation|modificada|
|[Fzwm2tFZ9TfeKZxu.htm](age-of-ashes-bestiary-items/Fzwm2tFZ9TfeKZxu.htm)|Contingency|Contingencia|modificada|
|[G04XQpPTsP0scmx5.htm](age-of-ashes-bestiary-items/G04XQpPTsP0scmx5.htm)|Shipboard Grace|Gracia a bordo|modificada|
|[G0N1HOgAQ6sfApBK.htm](age-of-ashes-bestiary-items/G0N1HOgAQ6sfApBK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[G2rX0J2oGGfLdB0F.htm](age-of-ashes-bestiary-items/G2rX0J2oGGfLdB0F.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[g6yamqCVKN9pYELp.htm](age-of-ashes-bestiary-items/g6yamqCVKN9pYELp.htm)|Inspiring Presence|Presencia Inspiradora|modificada|
|[GAMbl4nEeYZTf7VD.htm](age-of-ashes-bestiary-items/GAMbl4nEeYZTf7VD.htm)|Constant Spells|Constant Spells|modificada|
|[gca4Y5kOq7GHUaJp.htm](age-of-ashes-bestiary-items/gca4Y5kOq7GHUaJp.htm)|Bones of Stone|Huesos de piedra|modificada|
|[gDANDUYKwxqtpEpB.htm](age-of-ashes-bestiary-items/gDANDUYKwxqtpEpB.htm)|Graceful Double Slice|Doble tajo con gracia|modificada|
|[GDrd8HWCNlKRmARz.htm](age-of-ashes-bestiary-items/GDrd8HWCNlKRmARz.htm)|Telekinetic Defense|Telekinetic Defense|modificada|
|[GE9EMUIIHP8yDVMg.htm](age-of-ashes-bestiary-items/GE9EMUIIHP8yDVMg.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[gEAiXMZU4XPY9yTM.htm](age-of-ashes-bestiary-items/gEAiXMZU4XPY9yTM.htm)|Engulf|Envolver|modificada|
|[GFSu7vcUlllwKJzY.htm](age-of-ashes-bestiary-items/GFSu7vcUlllwKJzY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gg05Cq52L76J5iG8.htm](age-of-ashes-bestiary-items/gg05Cq52L76J5iG8.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[ggGBjbtHJJq44MAq.htm](age-of-ashes-bestiary-items/ggGBjbtHJJq44MAq.htm)|Claw|Garra|modificada|
|[gGMQ3mV8xiQNHsV3.htm](age-of-ashes-bestiary-items/gGMQ3mV8xiQNHsV3.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[gJ5Si4cUxad6gAip.htm](age-of-ashes-bestiary-items/gJ5Si4cUxad6gAip.htm)|Eschew Materials|Abstención de materiales|modificada|
|[gKRDi4EHuz4qIfX0.htm](age-of-ashes-bestiary-items/gKRDi4EHuz4qIfX0.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[GLAGuOAJAsQFW841.htm](age-of-ashes-bestiary-items/GLAGuOAJAsQFW841.htm)|Major Alchemist's Fire|Fuego de alquimista superior|modificada|
|[GLOrUOMtmv1uHTph.htm](age-of-ashes-bestiary-items/GLOrUOMtmv1uHTph.htm)|Change Shape|Change Shape|modificada|
|[gncjMzeBqmNqD4vz.htm](age-of-ashes-bestiary-items/gncjMzeBqmNqD4vz.htm)|Mauler|Vapulear|modificada|
|[go1LGZS91Af3mtP5.htm](age-of-ashes-bestiary-items/go1LGZS91Af3mtP5.htm)|Reactive|Reactive|modificada|
|[gOhhyt5jY5m4BCII.htm](age-of-ashes-bestiary-items/gOhhyt5jY5m4BCII.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[GpY52PWVwnKAAp0M.htm](age-of-ashes-bestiary-items/GpY52PWVwnKAAp0M.htm)|Quick Bomber|Bombardero rápido|modificada|
|[gqBKUBOWGOGtavDi.htm](age-of-ashes-bestiary-items/gqBKUBOWGOGtavDi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GQHHw4yF36Vczq0u.htm](age-of-ashes-bestiary-items/GQHHw4yF36Vczq0u.htm)|Negative Healing|Curación negativa|modificada|
|[gQJ5aamwehy4Pgwd.htm](age-of-ashes-bestiary-items/gQJ5aamwehy4Pgwd.htm)|Master Smith|Master Smith|modificada|
|[grsWv0LAFcO3iohL.htm](age-of-ashes-bestiary-items/grsWv0LAFcO3iohL.htm)|Claw|Garra|modificada|
|[gTfs4nOLCXGSkmgU.htm](age-of-ashes-bestiary-items/gTfs4nOLCXGSkmgU.htm)|Silver Dagger|Daga de plata|modificada|
|[guGMBVBsqTK7okFR.htm](age-of-ashes-bestiary-items/guGMBVBsqTK7okFR.htm)|Slam Doors|Portazo|modificada|
|[gW2dzfKwJdsR6lTt.htm](age-of-ashes-bestiary-items/gW2dzfKwJdsR6lTt.htm)|Channel Smite|Canalizar castigo|modificada|
|[gw4Ka0K9vOSo5rbh.htm](age-of-ashes-bestiary-items/gw4Ka0K9vOSo5rbh.htm)|Soul Shriek|Soul Shriek|modificada|
|[gw66iZEKmcFi0JjW.htm](age-of-ashes-bestiary-items/gw66iZEKmcFi0JjW.htm)|Howl|Howl|modificada|
|[GynHyVx5cEZ0vb9p.htm](age-of-ashes-bestiary-items/GynHyVx5cEZ0vb9p.htm)|Chase Down|Chase Down|modificada|
|[gzeeloXOYQJ3ZCvu.htm](age-of-ashes-bestiary-items/gzeeloXOYQJ3ZCvu.htm)|Mental Erosion|Erosión Mental|modificada|
|[GZrlbqO60jo1adXW.htm](age-of-ashes-bestiary-items/GZrlbqO60jo1adXW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H1vAXye0WIg0VDxS.htm](age-of-ashes-bestiary-items/H1vAXye0WIg0VDxS.htm)|Pick|Pick|modificada|
|[H2vI4lzuGs1S1ljJ.htm](age-of-ashes-bestiary-items/H2vI4lzuGs1S1ljJ.htm)|True Debilitating Bomb|Bomba debilitante verdadera.|modificada|
|[H2zLCRS1UX8XpweP.htm](age-of-ashes-bestiary-items/H2zLCRS1UX8XpweP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[H4swqBH4RI4TQYpt.htm](age-of-ashes-bestiary-items/H4swqBH4RI4TQYpt.htm)|Cold Iron Dagger|Daga de Hierro Frío|modificada|
|[h4y4kf4q85cEAF9x.htm](age-of-ashes-bestiary-items/h4y4kf4q85cEAF9x.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[H6cbKbRUjKF71Gko.htm](age-of-ashes-bestiary-items/H6cbKbRUjKF71Gko.htm)|Dagger|Daga|modificada|
|[h7AGKtRekFbuZp1Z.htm](age-of-ashes-bestiary-items/h7AGKtRekFbuZp1Z.htm)|Heavy Book|Heavy Book|modificada|
|[h7oremneSeWvOijy.htm](age-of-ashes-bestiary-items/h7oremneSeWvOijy.htm)|Change Shape|Change Shape|modificada|
|[hCNYJ9tfRIOCl989.htm](age-of-ashes-bestiary-items/hCNYJ9tfRIOCl989.htm)|Greater Acid Flask|Mayor frasco de ácido|modificada|
|[hdN9cUP88jTa2Ykb.htm](age-of-ashes-bestiary-items/hdN9cUP88jTa2Ykb.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[hdRK1K0vud2hRJEt.htm](age-of-ashes-bestiary-items/hdRK1K0vud2hRJEt.htm)|Promise Guard Stance|Posición de guardia de la promesa.|modificada|
|[HEnPvo9LJho9qb75.htm](age-of-ashes-bestiary-items/HEnPvo9LJho9qb75.htm)|Manipulate Energy|Manipular la energía|modificada|
|[HezD1ffNIEmWNt12.htm](age-of-ashes-bestiary-items/HezD1ffNIEmWNt12.htm)|Tail|Tail|modificada|
|[Hfn2kPN8v4JuYF1l.htm](age-of-ashes-bestiary-items/Hfn2kPN8v4JuYF1l.htm)|Quick Bomber|Bombardero rápido|modificada|
|[hgONc5BRFBZc4j73.htm](age-of-ashes-bestiary-items/hgONc5BRFBZc4j73.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HH6SL5ftuMn3NSHu.htm](age-of-ashes-bestiary-items/HH6SL5ftuMn3NSHu.htm)|Echoing Cry|Echoing Cry|modificada|
|[Hh8TSJGN8yL5SyKZ.htm](age-of-ashes-bestiary-items/Hh8TSJGN8yL5SyKZ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[hhR7r91njgZuHH4x.htm](age-of-ashes-bestiary-items/hhR7r91njgZuHH4x.htm)|Brutal Cleave|Hendedura brutal|modificada|
|[hI3zcHdUBaFLeoZX.htm](age-of-ashes-bestiary-items/hI3zcHdUBaFLeoZX.htm)|Jaws|Fauces|modificada|
|[hJc7HGNvwjdmOdfq.htm](age-of-ashes-bestiary-items/hJc7HGNvwjdmOdfq.htm)|Edifice|Edifice|modificada|
|[HlgR9oLdO3ZGqCry.htm](age-of-ashes-bestiary-items/HlgR9oLdO3ZGqCry.htm)|Reactive Breath|Reactive Breath|modificada|
|[HoXBI3O9X2kkeeC1.htm](age-of-ashes-bestiary-items/HoXBI3O9X2kkeeC1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hPc0H3LlS5oAeiST.htm](age-of-ashes-bestiary-items/hPc0H3LlS5oAeiST.htm)|Frightful Presence|Frightful Presence|modificada|
|[HPLwJZH8RwLW9c13.htm](age-of-ashes-bestiary-items/HPLwJZH8RwLW9c13.htm)|Black Eye Beam|Rayo Ojo Negro|modificada|
|[hRrW3wtTTKj4Kv6s.htm](age-of-ashes-bestiary-items/hRrW3wtTTKj4Kv6s.htm)|Longbow|Arco largo|modificada|
|[hrt10usZTV3s3uKQ.htm](age-of-ashes-bestiary-items/hrt10usZTV3s3uKQ.htm)|Haunted Form|Forma Embrujada|modificada|
|[HSs8knjTJuMzsAQr.htm](age-of-ashes-bestiary-items/HSs8knjTJuMzsAQr.htm)|Recognize Ally|Reconocer a un aliado|modificada|
|[HtPykjM3gCE3oQiR.htm](age-of-ashes-bestiary-items/HtPykjM3gCE3oQiR.htm)|Torpor|Torpor|modificada|
|[hVAcTBzqhXEsus66.htm](age-of-ashes-bestiary-items/hVAcTBzqhXEsus66.htm)|Wendigo Torment|Tormento Wendigo|modificada|
|[HVINdiV8c3uYz4or.htm](age-of-ashes-bestiary-items/HVINdiV8c3uYz4or.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hWFsgyTptyHOBJtD.htm](age-of-ashes-bestiary-items/hWFsgyTptyHOBJtD.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[HXiycpYZMsyFqs9l.htm](age-of-ashes-bestiary-items/HXiycpYZMsyFqs9l.htm)|Shortbow|Arco corto|modificada|
|[hZNyCnxdujLVMKqb.htm](age-of-ashes-bestiary-items/hZNyCnxdujLVMKqb.htm)|Identify an Opening|Identificar una abertura|modificada|
|[HzVWIyzzAWGlhGDC.htm](age-of-ashes-bestiary-items/HzVWIyzzAWGlhGDC.htm)|Claw|Garra|modificada|
|[i2cs4dR5xnohX9E6.htm](age-of-ashes-bestiary-items/i2cs4dR5xnohX9E6.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[i7B8yhwnUu48ZFdz.htm](age-of-ashes-bestiary-items/i7B8yhwnUu48ZFdz.htm)|Dragon Pillar Glance|Mirada del Pilar del Dragón|modificada|
|[i7bZ6f6zTCdQwhiN.htm](age-of-ashes-bestiary-items/i7bZ6f6zTCdQwhiN.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[IBkN8sEe9mFN0Uf4.htm](age-of-ashes-bestiary-items/IBkN8sEe9mFN0Uf4.htm)|Pseudopod|Pseudópodo|modificada|
|[ICaSdqQPFGE4xmmc.htm](age-of-ashes-bestiary-items/ICaSdqQPFGE4xmmc.htm)|Dagger|Daga|modificada|
|[IcCigf17UR8tbG7v.htm](age-of-ashes-bestiary-items/IcCigf17UR8tbG7v.htm)|Reflect|Reflect|modificada|
|[ICOl05YuWjz7C6GN.htm](age-of-ashes-bestiary-items/ICOl05YuWjz7C6GN.htm)|Paragon's Guard Stance|Posición de guardia de Paragon.|modificada|
|[IcprrivigaRsacAR.htm](age-of-ashes-bestiary-items/IcprrivigaRsacAR.htm)|Swallow Whole|Engullir Todo|modificada|
|[IDbHyzOZv9b2OyKQ.htm](age-of-ashes-bestiary-items/IDbHyzOZv9b2OyKQ.htm)|Impede|Obstaculizar|modificada|
|[IDc6afCxHUwfqrkX.htm](age-of-ashes-bestiary-items/IDc6afCxHUwfqrkX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Idy0MtsKPxYUxKqC.htm](age-of-ashes-bestiary-items/Idy0MtsKPxYUxKqC.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[ieB3sq8pLvjePur7.htm](age-of-ashes-bestiary-items/ieB3sq8pLvjePur7.htm)|Ghast Fever|Fiebre del ghast|modificada|
|[IFl5ilnFtCXtwx8I.htm](age-of-ashes-bestiary-items/IFl5ilnFtCXtwx8I.htm)|Pack Attack|Ataque en manada|modificada|
|[IHKQdP1f65ZJ8Oqu.htm](age-of-ashes-bestiary-items/IHKQdP1f65ZJ8Oqu.htm)|Poison Weapon|Arma envenenada|modificada|
|[IHmGGY9udr3P8p7m.htm](age-of-ashes-bestiary-items/IHmGGY9udr3P8p7m.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[IJhw9ETWROv5Y9rt.htm](age-of-ashes-bestiary-items/IJhw9ETWROv5Y9rt.htm)|Spikes|Púas|modificada|
|[iLD58rCFSuSX1HF6.htm](age-of-ashes-bestiary-items/iLD58rCFSuSX1HF6.htm)|Thundering Kukri|Kukri Tronante|modificada|
|[IrffLfsr4F6AQ70K.htm](age-of-ashes-bestiary-items/IrffLfsr4F6AQ70K.htm)|Subduing Strikes|Golpes subyugadores|modificada|
|[iTmWT27s6I0W1gVm.htm](age-of-ashes-bestiary-items/iTmWT27s6I0W1gVm.htm)|Manifest Lesser Dragonstorm|Manifiesto Tormenta de Dragones Menor|modificada|
|[iVkBxitUPibUUdxV.htm](age-of-ashes-bestiary-items/iVkBxitUPibUUdxV.htm)|Shield Breaker|Rompeescudos|modificada|
|[iVzCadSFlB0zTY05.htm](age-of-ashes-bestiary-items/iVzCadSFlB0zTY05.htm)|Shortsword|Espada corta|modificada|
|[iVZygV9OkEH28o2Q.htm](age-of-ashes-bestiary-items/iVZygV9OkEH28o2Q.htm)|Black Eye Beam|Rayo Ojo Negro|modificada|
|[IwqiOEcQaE9YK1Sa.htm](age-of-ashes-bestiary-items/IwqiOEcQaE9YK1Sa.htm)|Spell Strike|Golpe de Hechizo|modificada|
|[j0tisNS7G4pqIgVz.htm](age-of-ashes-bestiary-items/j0tisNS7G4pqIgVz.htm)|Shield Block|Bloquear con escudo|modificada|
|[j3NCaHUgoOUiHpye.htm](age-of-ashes-bestiary-items/j3NCaHUgoOUiHpye.htm)|Ghast Fever|Fiebre del ghast|modificada|
|[j4MfbH7vW63xSuRI.htm](age-of-ashes-bestiary-items/j4MfbH7vW63xSuRI.htm)|Shield Boss|Jefe escudo con umbo|modificada|
|[j5LDyAGkf9FeoBAt.htm](age-of-ashes-bestiary-items/j5LDyAGkf9FeoBAt.htm)|Rapier|Estoque|modificada|
|[J7B6WzdiD0YTP0qQ.htm](age-of-ashes-bestiary-items/J7B6WzdiD0YTP0qQ.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[j7hDiUXLpry6mkqN.htm](age-of-ashes-bestiary-items/j7hDiUXLpry6mkqN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j95Y0cxi70Mb6UMQ.htm](age-of-ashes-bestiary-items/j95Y0cxi70Mb6UMQ.htm)|Rugged Travel|Viajero curtido|modificada|
|[Janx9PZwNEn44igV.htm](age-of-ashes-bestiary-items/Janx9PZwNEn44igV.htm)|Fleshroaster|Fleshroaster|modificada|
|[jBkyceYP8fzH2x9P.htm](age-of-ashes-bestiary-items/jBkyceYP8fzH2x9P.htm)|Rock|Roca|modificada|
|[jfBUSjcjhlewvG1J.htm](age-of-ashes-bestiary-items/jfBUSjcjhlewvG1J.htm)|Longsword|Longsword|modificada|
|[jFY95i89pS7nXrQl.htm](age-of-ashes-bestiary-items/jFY95i89pS7nXrQl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jHCCY0ZIpf7axn62.htm](age-of-ashes-bestiary-items/jHCCY0ZIpf7axn62.htm)|Channel Dragonstorm|Channel Dragonstorm|modificada|
|[jI9GCbJaB0b3F4Kw.htm](age-of-ashes-bestiary-items/jI9GCbJaB0b3F4Kw.htm)|Indigo Eye Beam|Indigo Eye Beam|modificada|
|[jL6Ovx8CLeSJ0K2S.htm](age-of-ashes-bestiary-items/jL6Ovx8CLeSJ0K2S.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[jNcUglLORusRcA4f.htm](age-of-ashes-bestiary-items/jNcUglLORusRcA4f.htm)|Red Eye Beam|Red Eye Beam|modificada|
|[jQUqBzYMtuQRpcmS.htm](age-of-ashes-bestiary-items/jQUqBzYMtuQRpcmS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JRd0vnYlb4G88YJ6.htm](age-of-ashes-bestiary-items/JRd0vnYlb4G88YJ6.htm)|Crossbow|Ballesta|modificada|
|[jVPlrYrdfUoKsCFZ.htm](age-of-ashes-bestiary-items/jVPlrYrdfUoKsCFZ.htm)|Tremorsense|Tremorsense|modificada|
|[jwRUzgElIiQUtmhR.htm](age-of-ashes-bestiary-items/jwRUzgElIiQUtmhR.htm)|Dimensional Tether|Dimensional Tether|modificada|
|[JZbnTWFBrM3l78qS.htm](age-of-ashes-bestiary-items/JZbnTWFBrM3l78qS.htm)|Volcanic Eruption|Erupción volcánica|modificada|
|[jZFyZg8r1I1EWoBY.htm](age-of-ashes-bestiary-items/jZFyZg8r1I1EWoBY.htm)|Timely Distraction|Distracción oportuna|modificada|
|[jZsVGuNQlvV5TkQ9.htm](age-of-ashes-bestiary-items/jZsVGuNQlvV5TkQ9.htm)|Perception and Vision|Percepción y Visión|modificada|
|[K28NAtboAXcpvklg.htm](age-of-ashes-bestiary-items/K28NAtboAXcpvklg.htm)|Dahak's Glance|Dahak's Glance|modificada|
|[K5dIa5gCC7aePzQf.htm](age-of-ashes-bestiary-items/K5dIa5gCC7aePzQf.htm)|Soul Siphon|Succionar almas|modificada|
|[k7a5PsosLu8FXsTk.htm](age-of-ashes-bestiary-items/k7a5PsosLu8FXsTk.htm)|Speed Scimitar|Cimitarra de Velocidad|modificada|
|[k8bgECKZjROUF5gA.htm](age-of-ashes-bestiary-items/k8bgECKZjROUF5gA.htm)|Staff of Fire|Bastón de fuego|modificada|
|[kedOBfOiNpFOHZa0.htm](age-of-ashes-bestiary-items/kedOBfOiNpFOHZa0.htm)|Jaws|Fauces|modificada|
|[kg205YMOcrEz6XQw.htm](age-of-ashes-bestiary-items/kg205YMOcrEz6XQw.htm)|Ranged Legerdemain|Legerdemain rango de distancia|modificada|
|[KhFyp3oQ1nmWpdA9.htm](age-of-ashes-bestiary-items/KhFyp3oQ1nmWpdA9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KHiVxpvVx4edKn9M.htm](age-of-ashes-bestiary-items/KHiVxpvVx4edKn9M.htm)|Horns|Cuernos|modificada|
|[KiltiTEGGXxRaLvr.htm](age-of-ashes-bestiary-items/KiltiTEGGXxRaLvr.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[kJlV6Vzu6KZumb6u.htm](age-of-ashes-bestiary-items/kJlV6Vzu6KZumb6u.htm)|Volcanic Heat|Calor volcánico|modificada|
|[KJxjnogCSiWsIs3n.htm](age-of-ashes-bestiary-items/KJxjnogCSiWsIs3n.htm)|Magma Swim|Nadar Magma|modificada|
|[kJz2LXDyAG4vASIf.htm](age-of-ashes-bestiary-items/kJz2LXDyAG4vASIf.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[KKaF35HWVDgg10gX.htm](age-of-ashes-bestiary-items/KKaF35HWVDgg10gX.htm)|Scimitar|Cimitarra|modificada|
|[KOEGii7EkPZsq9Iz.htm](age-of-ashes-bestiary-items/KOEGii7EkPZsq9Iz.htm)|Glare of Rage|Resplandor de Furia|modificada|
|[KoIGP6ravPoUypu2.htm](age-of-ashes-bestiary-items/KoIGP6ravPoUypu2.htm)|Constant Spells|Constant Spells|modificada|
|[koqHCbsGHHME7p6J.htm](age-of-ashes-bestiary-items/koqHCbsGHHME7p6J.htm)|Claw|Garra|modificada|
|[Kp4oqpyn0RuzJfDQ.htm](age-of-ashes-bestiary-items/Kp4oqpyn0RuzJfDQ.htm)|Staff|Báculo|modificada|
|[kPGNXeFYyzZHqKiv.htm](age-of-ashes-bestiary-items/kPGNXeFYyzZHqKiv.htm)|Return Fire|Retornar Fuego|modificada|
|[kqgeNf8SyOkbJ0qi.htm](age-of-ashes-bestiary-items/kqgeNf8SyOkbJ0qi.htm)|Greatsword|Greatsword|modificada|
|[KqSNLofqd5sNL9bp.htm](age-of-ashes-bestiary-items/KqSNLofqd5sNL9bp.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[KqV4dBWVzfi3DXyl.htm](age-of-ashes-bestiary-items/KqV4dBWVzfi3DXyl.htm)|Efficient Capture|Captura eficiente|modificada|
|[KrTgWTUW9uB20sKT.htm](age-of-ashes-bestiary-items/KrTgWTUW9uB20sKT.htm)|Frightful Presence|Frightful Presence|modificada|
|[kT5280SVobphMNJn.htm](age-of-ashes-bestiary-items/kT5280SVobphMNJn.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Kvh0b9Gxe3FzqXsg.htm](age-of-ashes-bestiary-items/Kvh0b9Gxe3FzqXsg.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kVU7cb7Ts2sMfPyu.htm](age-of-ashes-bestiary-items/kVU7cb7Ts2sMfPyu.htm)|Whip|Látigo|modificada|
|[kwP6P4BpQ8TfjZTO.htm](age-of-ashes-bestiary-items/kwP6P4BpQ8TfjZTO.htm)|Heavy Book|Heavy Book|modificada|
|[kx9XRnPu3GxZYqST.htm](age-of-ashes-bestiary-items/kx9XRnPu3GxZYqST.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kyJU0dp0AuLccYHV.htm](age-of-ashes-bestiary-items/kyJU0dp0AuLccYHV.htm)|Breath Weapon|Breath Weapon|modificada|
|[KYpQ8TgZcmUrKnjC.htm](age-of-ashes-bestiary-items/KYpQ8TgZcmUrKnjC.htm)|Independent Limbs|Extremidades Independientes|modificada|
|[Kz3r31N7mvhlPQc8.htm](age-of-ashes-bestiary-items/Kz3r31N7mvhlPQc8.htm)|Occult Innate Spellss|Hechizos Innatos de Ocultismo|modificada|
|[kZ6DWvbDyy1GyJ6D.htm](age-of-ashes-bestiary-items/kZ6DWvbDyy1GyJ6D.htm)|Personal Quake|Personal Quake|modificada|
|[kZcHhT2dlbVHF7AS.htm](age-of-ashes-bestiary-items/kZcHhT2dlbVHF7AS.htm)|Kobold Explosives|Explosión kobold|modificada|
|[L0ZHkQFYtz2JK3hH.htm](age-of-ashes-bestiary-items/L0ZHkQFYtz2JK3hH.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[L10IjFT5OxF7Vdtw.htm](age-of-ashes-bestiary-items/L10IjFT5OxF7Vdtw.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[L1rIkFNagYO7XkgM.htm](age-of-ashes-bestiary-items/L1rIkFNagYO7XkgM.htm)|Wing Deflection|Desvío con el ala|modificada|
|[l1U1HsRzfUA7agTu.htm](age-of-ashes-bestiary-items/l1U1HsRzfUA7agTu.htm)|Walk in Shadow|Caminar en las sombras|modificada|
|[l69fOTyMIBdJNWTY.htm](age-of-ashes-bestiary-items/l69fOTyMIBdJNWTY.htm)|Guardian Sense|Sentido Guardián|modificada|
|[L6oDlNintmXHhjAv.htm](age-of-ashes-bestiary-items/L6oDlNintmXHhjAv.htm)|Precision|Precisión|modificada|
|[l7LvpPXTQL4jhPb5.htm](age-of-ashes-bestiary-items/l7LvpPXTQL4jhPb5.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LB86IJopeELMnAze.htm](age-of-ashes-bestiary-items/LB86IJopeELMnAze.htm)|Frightful Moan|Frightful Moan|modificada|
|[lBp2RqOAK3CORaeJ.htm](age-of-ashes-bestiary-items/lBp2RqOAK3CORaeJ.htm)|Spine|Spine|modificada|
|[lbSqT3JsKYmywHiq.htm](age-of-ashes-bestiary-items/lbSqT3JsKYmywHiq.htm)|Jaws|Fauces|modificada|
|[LbVTFpHTRiZGGVE9.htm](age-of-ashes-bestiary-items/LbVTFpHTRiZGGVE9.htm)|Longsword|Longsword|modificada|
|[lcdFqkdanjf6Jdom.htm](age-of-ashes-bestiary-items/lcdFqkdanjf6Jdom.htm)|Caustic Nightmare Vapor|Vapor de pesadilla cáustica|modificada|
|[ld8xSt4F2q5m6vRe.htm](age-of-ashes-bestiary-items/ld8xSt4F2q5m6vRe.htm)|Paralyzing Force|Fuerza Paralizante|modificada|
|[LePEQxJOrm3uEIPP.htm](age-of-ashes-bestiary-items/LePEQxJOrm3uEIPP.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[lfmWRoj9CXxKLkCs.htm](age-of-ashes-bestiary-items/lfmWRoj9CXxKLkCs.htm)|Red Eye Beam|Red Eye Beam|modificada|
|[lJ6fQwHU2UpZ6h6F.htm](age-of-ashes-bestiary-items/lJ6fQwHU2UpZ6h6F.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[LLzxyBASOUnyUmZk.htm](age-of-ashes-bestiary-items/LLzxyBASOUnyUmZk.htm)|Green Empathy|Empatía vegetal|modificada|
|[LM8A2eiEIyuB9hk2.htm](age-of-ashes-bestiary-items/LM8A2eiEIyuB9hk2.htm)|Shortbow|Arco corto|modificada|
|[LNJkhJSFpg8gvkwp.htm](age-of-ashes-bestiary-items/LNJkhJSFpg8gvkwp.htm)|Heatsight|Heatsight|modificada|
|[LnwQpnNyyd6h5Xx0.htm](age-of-ashes-bestiary-items/LnwQpnNyyd6h5Xx0.htm)|Face of the Fatal Divine|Rostro de la fatalidad divina|modificada|
|[lpDLQW5YfXIYXiJS.htm](age-of-ashes-bestiary-items/lpDLQW5YfXIYXiJS.htm)|Claw|Garra|modificada|
|[lpORbE7ZJjtwviYL.htm](age-of-ashes-bestiary-items/lpORbE7ZJjtwviYL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lPxb0HqwCjMCigsv.htm](age-of-ashes-bestiary-items/lPxb0HqwCjMCigsv.htm)|Returning Light Hammer|Martillo de Luz Retornante|modificada|
|[LQ20lTeaoccMicH8.htm](age-of-ashes-bestiary-items/LQ20lTeaoccMicH8.htm)|Psychic Screech|Psychic Screech|modificada|
|[lRGKk5oP862ZMOI3.htm](age-of-ashes-bestiary-items/lRGKk5oP862ZMOI3.htm)|Change Shape|Change Shape|modificada|
|[LsLTSWuEmgszvIJo.htm](age-of-ashes-bestiary-items/LsLTSWuEmgszvIJo.htm)|Shortbow|Arco corto|modificada|
|[lSxI33XT0yIaAARS.htm](age-of-ashes-bestiary-items/lSxI33XT0yIaAARS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LT73ykCcsBulHrcy.htm](age-of-ashes-bestiary-items/LT73ykCcsBulHrcy.htm)|Jaws|Fauces|modificada|
|[lup461lEK9zazSYF.htm](age-of-ashes-bestiary-items/lup461lEK9zazSYF.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[lw6P5HJ72vY1Ef6K.htm](age-of-ashes-bestiary-items/lw6P5HJ72vY1Ef6K.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[lWDO2icn4khYwlgs.htm](age-of-ashes-bestiary-items/lWDO2icn4khYwlgs.htm)|Jaws|Fauces|modificada|
|[LwLNUmtRRcLH4yWo.htm](age-of-ashes-bestiary-items/LwLNUmtRRcLH4yWo.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[lxqSAaKp7jMekQke.htm](age-of-ashes-bestiary-items/lxqSAaKp7jMekQke.htm)|Inflate Bellows|Inflar fuelles|modificada|
|[M0hdta5U4NIGRZAD.htm](age-of-ashes-bestiary-items/M0hdta5U4NIGRZAD.htm)|Terrifying Visions|Visiones Aterradoras|modificada|
|[M4OptJYHoUkyQh2f.htm](age-of-ashes-bestiary-items/M4OptJYHoUkyQh2f.htm)|Trample|Trample|modificada|
|[M4S8LvGZQeUkoq88.htm](age-of-ashes-bestiary-items/M4S8LvGZQeUkoq88.htm)|Horns|Cuernos|modificada|
|[m8n5v2B0saXm9zp1.htm](age-of-ashes-bestiary-items/m8n5v2B0saXm9zp1.htm)|Major Bottled Lightning|Rayo embotellado, superior|modificada|
|[m8X8IWzzFl0HzmIE.htm](age-of-ashes-bestiary-items/m8X8IWzzFl0HzmIE.htm)|Golden Luck|Suerte de oro|modificada|
|[MaPDXpGVOauOwVc8.htm](age-of-ashes-bestiary-items/MaPDXpGVOauOwVc8.htm)|Rock|Roca|modificada|
|[mEFkqvuYvc9WzXcr.htm](age-of-ashes-bestiary-items/mEFkqvuYvc9WzXcr.htm)|Reactive Shield|Escudo reactivo|modificada|
|[mgUFs2wbhtnPQXLr.htm](age-of-ashes-bestiary-items/mgUFs2wbhtnPQXLr.htm)|Fist|Puño|modificada|
|[MhYvDcxQITUxb0tw.htm](age-of-ashes-bestiary-items/MhYvDcxQITUxb0tw.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[MJg2MnY8wKpQte4q.htm](age-of-ashes-bestiary-items/MJg2MnY8wKpQte4q.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[mjhchEu4tZZFuxhA.htm](age-of-ashes-bestiary-items/mjhchEu4tZZFuxhA.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[mJhd5iLsmLx74Qjo.htm](age-of-ashes-bestiary-items/mJhd5iLsmLx74Qjo.htm)|Spontaneous Divine Spells|Hechizos Divinos Espontáneos|modificada|
|[mkKQLOSUvNml05qi.htm](age-of-ashes-bestiary-items/mkKQLOSUvNml05qi.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[mKqF9CDVTOOPFVm6.htm](age-of-ashes-bestiary-items/mKqF9CDVTOOPFVm6.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[MLHgQjjeyBRDloTd.htm](age-of-ashes-bestiary-items/MLHgQjjeyBRDloTd.htm)|Entangling Residue|Residuo enmarado|modificada|
|[MMtAAek31GnnT3PK.htm](age-of-ashes-bestiary-items/MMtAAek31GnnT3PK.htm)|Warhammer|Warhammer|modificada|
|[mMwtoDsUUn0ZCGvL.htm](age-of-ashes-bestiary-items/mMwtoDsUUn0ZCGvL.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[MNIeEFHVbRZ8b5Ln.htm](age-of-ashes-bestiary-items/MNIeEFHVbRZ8b5Ln.htm)|Hostile Juxtaposition|Yuxtaposición hostil|modificada|
|[mnMWLmMnVuCl04IC.htm](age-of-ashes-bestiary-items/mnMWLmMnVuCl04IC.htm)|Fling Sand in the Eyes|Lanzar por los aires arena|modificada|
|[Mo8pDcRqUWKoQzPF.htm](age-of-ashes-bestiary-items/Mo8pDcRqUWKoQzPF.htm)|Jaws|Fauces|modificada|
|[moQZVLDgyoG4xYuR.htm](age-of-ashes-bestiary-items/moQZVLDgyoG4xYuR.htm)|Volcanic Purge|Purga volcánica|modificada|
|[MpCCxEfa5YHRcVVR.htm](age-of-ashes-bestiary-items/MpCCxEfa5YHRcVVR.htm)|Mace|Maza|modificada|
|[MQvALn3TuAb3iI1B.htm](age-of-ashes-bestiary-items/MQvALn3TuAb3iI1B.htm)|Innate Arcane Spells|Hechizos Arcanos Innatos|modificada|
|[Mrf2WIIKXTByxA81.htm](age-of-ashes-bestiary-items/Mrf2WIIKXTByxA81.htm)|Intimidating Attack of Opportunity|Ataque de oportunidad intimidante|modificada|
|[mrHQQjwSTfXCnZch.htm](age-of-ashes-bestiary-items/mrHQQjwSTfXCnZch.htm)|Chained Dagger|Chained Dagger|modificada|
|[mSAR7TLZJBzuU46P.htm](age-of-ashes-bestiary-items/mSAR7TLZJBzuU46P.htm)|Thrown Rock|Arrojar roca|modificada|
|[msk25zy4vClQBl9Q.htm](age-of-ashes-bestiary-items/msk25zy4vClQBl9Q.htm)|Inflate Bellows|Inflar fuelles|modificada|
|[MTe5oaN8qxo4KYFm.htm](age-of-ashes-bestiary-items/MTe5oaN8qxo4KYFm.htm)|Demiplane Lair|Guarida Demiplane|modificada|
|[mTgeFQ9F1VC2Jckr.htm](age-of-ashes-bestiary-items/mTgeFQ9F1VC2Jckr.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[MuK4kIpfXEUbojeB.htm](age-of-ashes-bestiary-items/MuK4kIpfXEUbojeB.htm)|HP is 72 for each 5 foot patch|HP es 72 por cada parche de 5 pies|modificada|
|[MW0LxdUJZd9ozWtO.htm](age-of-ashes-bestiary-items/MW0LxdUJZd9ozWtO.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[mwEc6YhRZUrr9Qkb.htm](age-of-ashes-bestiary-items/mwEc6YhRZUrr9Qkb.htm)|Kick|Kick|modificada|
|[MxiNEcQrKuXmCuWD.htm](age-of-ashes-bestiary-items/MxiNEcQrKuXmCuWD.htm)|Fangs|Colmillos|modificada|
|[mXRRlIf5fS8NO5I1.htm](age-of-ashes-bestiary-items/mXRRlIf5fS8NO5I1.htm)|Woodland Stride|Paso forestal|modificada|
|[MYgT9dmiyGDI48Hx.htm](age-of-ashes-bestiary-items/MYgT9dmiyGDI48Hx.htm)|Superlative Summoner|Convocador superlativo|modificada|
|[MyJ5pEba9K3unoCm.htm](age-of-ashes-bestiary-items/MyJ5pEba9K3unoCm.htm)|Archery Experience|Experiencia en tiro con arco|modificada|
|[MZs5uxec71OZQSzk.htm](age-of-ashes-bestiary-items/MZs5uxec71OZQSzk.htm)|Disrupting Cold Iron Dagger|Disruptora Daga de Hierro Frío|modificada|
|[N01NwkU6GvutbIp7.htm](age-of-ashes-bestiary-items/N01NwkU6GvutbIp7.htm)|Shell Block|Bloqueo con caparazón|modificada|
|[N096jcv77bKpi9xA.htm](age-of-ashes-bestiary-items/N096jcv77bKpi9xA.htm)|Backshot|Devolver el disparo|modificada|
|[n0JJ4L7jPOWqDejM.htm](age-of-ashes-bestiary-items/n0JJ4L7jPOWqDejM.htm)|Matriarch's Caress|Caricia de la matriarca|modificada|
|[n2I1TMJLTQ65exGf.htm](age-of-ashes-bestiary-items/n2I1TMJLTQ65exGf.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[n327lTMbR7EaIPxV.htm](age-of-ashes-bestiary-items/n327lTMbR7EaIPxV.htm)|Darting Shot|Darting Shot|modificada|
|[n32H8SDG3hQfZVNB.htm](age-of-ashes-bestiary-items/n32H8SDG3hQfZVNB.htm)|Instantaneous Movement|Movimiento Instantáneo|modificada|
|[n4acrtFiDu5TwYGe.htm](age-of-ashes-bestiary-items/n4acrtFiDu5TwYGe.htm)|Hammer the Chained|Martillar al encadenado|modificada|
|[N62QvxPwdPCrIlP1.htm](age-of-ashes-bestiary-items/N62QvxPwdPCrIlP1.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[n6SPErVyS2S3b3lv.htm](age-of-ashes-bestiary-items/n6SPErVyS2S3b3lv.htm)|Horn Snare|Horn Snare|modificada|
|[N9ItpLBOPJW5l9DI.htm](age-of-ashes-bestiary-items/N9ItpLBOPJW5l9DI.htm)|Extra Reaction|Reacción adicional|modificada|
|[nA6MKkodX1XzMSli.htm](age-of-ashes-bestiary-items/nA6MKkodX1XzMSli.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[naVs815EnCcnoaW1.htm](age-of-ashes-bestiary-items/naVs815EnCcnoaW1.htm)|Drowning Drone|Zumbido ensordecedor|modificada|
|[nf1iMpVleTsR2jEV.htm](age-of-ashes-bestiary-items/nf1iMpVleTsR2jEV.htm)|Internal Furnace|Horno interno|modificada|
|[NfPbxqhBdeyeKfyS.htm](age-of-ashes-bestiary-items/NfPbxqhBdeyeKfyS.htm)|Weapon Master|Maestro de armas|modificada|
|[NgedLFfO7OJlxvqT.htm](age-of-ashes-bestiary-items/NgedLFfO7OJlxvqT.htm)|Frightful Presence|Frightful Presence|modificada|
|[NgHUvhpolU7B4yNd.htm](age-of-ashes-bestiary-items/NgHUvhpolU7B4yNd.htm)|Dazzling Display|Dazzling Display|modificada|
|[NI8nqs0x537N5LvI.htm](age-of-ashes-bestiary-items/NI8nqs0x537N5LvI.htm)|Tail|Tail|modificada|
|[NioStPlx80WxmDvR.htm](age-of-ashes-bestiary-items/NioStPlx80WxmDvR.htm)|Searing Heat|Searing Heat|modificada|
|[nkAN9Dt2tljmS96M.htm](age-of-ashes-bestiary-items/nkAN9Dt2tljmS96M.htm)|Site Bound (Jewelgate Way Station)|Ligado a una ubicación (estación Jewelgate Way).|modificada|
|[nKfHeaA4JlYimGVv.htm](age-of-ashes-bestiary-items/nKfHeaA4JlYimGVv.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[NKkENtFd0bUL6yx7.htm](age-of-ashes-bestiary-items/NKkENtFd0bUL6yx7.htm)|Woodland Stride|Paso forestal|modificada|
|[nMOYUQ60Uw7g4k0q.htm](age-of-ashes-bestiary-items/nMOYUQ60Uw7g4k0q.htm)|Flurry|Ráfaga|modificada|
|[nnJ4kOGHM4Qct2PR.htm](age-of-ashes-bestiary-items/nnJ4kOGHM4Qct2PR.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[NNXU0f8NQLPkYMva.htm](age-of-ashes-bestiary-items/NNXU0f8NQLPkYMva.htm)|Painsight|Painsight|modificada|
|[nPeM50gZBykCKDxe.htm](age-of-ashes-bestiary-items/nPeM50gZBykCKDxe.htm)|Distant Ringing|Repiqueteo lejano|modificada|
|[nPeylmjWQ7slkCXr.htm](age-of-ashes-bestiary-items/nPeylmjWQ7slkCXr.htm)|Teleportation Attachment|Aferrarse teletransporte|modificada|
|[nTtoBqYHxSoaPWuC.htm](age-of-ashes-bestiary-items/nTtoBqYHxSoaPWuC.htm)|Site Bound|Ligado a una ubicación|modificada|
|[NttUPgbz0pGvaOnd.htm](age-of-ashes-bestiary-items/NttUPgbz0pGvaOnd.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Nu8j6VpdlorVM75S.htm](age-of-ashes-bestiary-items/Nu8j6VpdlorVM75S.htm)|Designate Bellflower Crop|Designar cultivo Bellflower|modificada|
|[nurvAKW6FmusUHRh.htm](age-of-ashes-bestiary-items/nurvAKW6FmusUHRh.htm)|Dagger|Daga|modificada|
|[nw4MbhXR5zCwPJBV.htm](age-of-ashes-bestiary-items/nw4MbhXR5zCwPJBV.htm)|Dagger|Daga|modificada|
|[nxnCWI2x0Zy4iWjR.htm](age-of-ashes-bestiary-items/nxnCWI2x0Zy4iWjR.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[nycjIkuU99HnHnf9.htm](age-of-ashes-bestiary-items/nycjIkuU99HnHnf9.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[NYttSUeq7rE5Lwlt.htm](age-of-ashes-bestiary-items/NYttSUeq7rE5Lwlt.htm)|Rapier|Estoque|modificada|
|[Nyz0P3ikAyq0ud3H.htm](age-of-ashes-bestiary-items/Nyz0P3ikAyq0ud3H.htm)|Thrown Weapon Mastery|Maestría con las armas arrojadizas|modificada|
|[NZ4xdM6bKvntdTEa.htm](age-of-ashes-bestiary-items/NZ4xdM6bKvntdTEa.htm)|Fearsome Brute|Bruto temible|modificada|
|[o0njxNAMbjPeC9AS.htm](age-of-ashes-bestiary-items/o0njxNAMbjPeC9AS.htm)|Cataclysmic Rain|Cataclysmic Rain|modificada|
|[o10Yz3qVKjeI4Fxz.htm](age-of-ashes-bestiary-items/o10Yz3qVKjeI4Fxz.htm)|Breath Weapon|Breath Weapon|modificada|
|[o1edk89NwTsVdvC1.htm](age-of-ashes-bestiary-items/o1edk89NwTsVdvC1.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[o4fl6Wqm7Pupv1R8.htm](age-of-ashes-bestiary-items/o4fl6Wqm7Pupv1R8.htm)|Tail|Tail|modificada|
|[O4Vu0lAef9YMzn23.htm](age-of-ashes-bestiary-items/O4Vu0lAef9YMzn23.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[O5uoqD3Lzqcdx708.htm](age-of-ashes-bestiary-items/O5uoqD3Lzqcdx708.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[O6DYo04qm4tluUeG.htm](age-of-ashes-bestiary-items/O6DYo04qm4tluUeG.htm)|Spine Volley|Volea Espinal|modificada|
|[o8XSgCFe5Tgc8VwW.htm](age-of-ashes-bestiary-items/o8XSgCFe5Tgc8VwW.htm)|Vulnerable to Stone to Flesh|Vulnerable a Piedra a la carne|modificada|
|[o9eWNluFGesUiB9v.htm](age-of-ashes-bestiary-items/o9eWNluFGesUiB9v.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[O9V5OSgRwAX3gEBH.htm](age-of-ashes-bestiary-items/O9V5OSgRwAX3gEBH.htm)|Grab|Agarrado|modificada|
|[OBpAWIEWPW9DSQMx.htm](age-of-ashes-bestiary-items/OBpAWIEWPW9DSQMx.htm)|Longbow|Arco largo|modificada|
|[oBtQcDrceotbFbrk.htm](age-of-ashes-bestiary-items/oBtQcDrceotbFbrk.htm)|Constant Spells|Constant Spells|modificada|
|[oBv9CmgE9lFftp0E.htm](age-of-ashes-bestiary-items/oBv9CmgE9lFftp0E.htm)|Swiftness|Celeridad|modificada|
|[obvyw8sjJyDFGLDy.htm](age-of-ashes-bestiary-items/obvyw8sjJyDFGLDy.htm)|Infused Items|Equipos infundidos|modificada|
|[OC3Cenu8w3vfySOp.htm](age-of-ashes-bestiary-items/OC3Cenu8w3vfySOp.htm)|Twisting Tail|Enredar con la cola|modificada|
|[Od7nphkGvQM8Iv5L.htm](age-of-ashes-bestiary-items/Od7nphkGvQM8Iv5L.htm)|Blue Eye Beam|Blue Eye Beam|modificada|
|[oDodI8lhFK3QYtHx.htm](age-of-ashes-bestiary-items/oDodI8lhFK3QYtHx.htm)|Indigo Eye Beam|Indigo Eye Beam|modificada|
|[ODRNG6vG9zjC3Etf.htm](age-of-ashes-bestiary-items/ODRNG6vG9zjC3Etf.htm)|Rend|Rasgadura|modificada|
|[OfIb5QtIPTTftNlq.htm](age-of-ashes-bestiary-items/OfIb5QtIPTTftNlq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[OHnvdksMNZmChe4c.htm](age-of-ashes-bestiary-items/OHnvdksMNZmChe4c.htm)|Light Hammer|Martillo ligero|modificada|
|[oIJ6ppXsMXZMBfSi.htm](age-of-ashes-bestiary-items/oIJ6ppXsMXZMBfSi.htm)|Liberation Vulnerability|Vulnerabilidad liberador|modificada|
|[Okk4imbIUOwkYs3E.htm](age-of-ashes-bestiary-items/Okk4imbIUOwkYs3E.htm)|Hunter's Aim|Puntería del cazador|modificada|
|[OLb44HyPmPQ1A0Sm.htm](age-of-ashes-bestiary-items/OLb44HyPmPQ1A0Sm.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[OlZeNmmphsIzWVz5.htm](age-of-ashes-bestiary-items/OlZeNmmphsIzWVz5.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[OM9iuNqyLKsh2ZjP.htm](age-of-ashes-bestiary-items/OM9iuNqyLKsh2ZjP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OmjmOPy2aa488z2d.htm](age-of-ashes-bestiary-items/OmjmOPy2aa488z2d.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Omu620MVyXotE5Nc.htm](age-of-ashes-bestiary-items/Omu620MVyXotE5Nc.htm)|Breath Weapon|Breath Weapon|modificada|
|[OOmkcTVG03cJ0cT3.htm](age-of-ashes-bestiary-items/OOmkcTVG03cJ0cT3.htm)|Shield Block|Bloquear con escudo|modificada|
|[oPh7RYiHA3OguL9Z.htm](age-of-ashes-bestiary-items/oPh7RYiHA3OguL9Z.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OqoxUu62vH7mcrPs.htm](age-of-ashes-bestiary-items/OqoxUu62vH7mcrPs.htm)|Terrifying Stare|Mirada Aterradora|modificada|
|[Oqy1IXCABemD4A9g.htm](age-of-ashes-bestiary-items/Oqy1IXCABemD4A9g.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[OSgjqd6Z5NewzIw4.htm](age-of-ashes-bestiary-items/OSgjqd6Z5NewzIw4.htm)|Focus Spells|Conjuros de foco|modificada|
|[oUbNiZbQMyGjNTIj.htm](age-of-ashes-bestiary-items/oUbNiZbQMyGjNTIj.htm)|Reactionary|Reaccionario|modificada|
|[OvKpoweI2x9CioBO.htm](age-of-ashes-bestiary-items/OvKpoweI2x9CioBO.htm)|Overwhelming Energy|Energía abrumadora|modificada|
|[OVyCsNi5aandXp9w.htm](age-of-ashes-bestiary-items/OVyCsNi5aandXp9w.htm)|Hunt Prey|Perseguir presa|modificada|
|[owejjJWuMlBx46tR.htm](age-of-ashes-bestiary-items/owejjJWuMlBx46tR.htm)|Whiplash|Latigazo|modificada|
|[OWhmwuZtv80QG0wy.htm](age-of-ashes-bestiary-items/OWhmwuZtv80QG0wy.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[OwoH6FLZ94ihNvEv.htm](age-of-ashes-bestiary-items/OwoH6FLZ94ihNvEv.htm)|Returning Light Hammer|Martillo de Luz Retornante|modificada|
|[OzVIHvIPV38rnIVv.htm](age-of-ashes-bestiary-items/OzVIHvIPV38rnIVv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ozZqtI8i4QAUvyoA.htm](age-of-ashes-bestiary-items/ozZqtI8i4QAUvyoA.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[p1bvrj2qMZFdHpsv.htm](age-of-ashes-bestiary-items/p1bvrj2qMZFdHpsv.htm)|Self-Repair|Autorreparación|modificada|
|[P54JJ58qynLo2URI.htm](age-of-ashes-bestiary-items/P54JJ58qynLo2URI.htm)|Breath Weapon|Breath Weapon|modificada|
|[p5rguZk07i9usCOe.htm](age-of-ashes-bestiary-items/p5rguZk07i9usCOe.htm)|Dagger|Daga|modificada|
|[p9DHIoccyeBea4Yd.htm](age-of-ashes-bestiary-items/p9DHIoccyeBea4Yd.htm)|Twin Shot|Twin Shot|modificada|
|[pb3yDpk6Rl0BuY7j.htm](age-of-ashes-bestiary-items/pb3yDpk6Rl0BuY7j.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[PcDH3sH6RU74yewe.htm](age-of-ashes-bestiary-items/PcDH3sH6RU74yewe.htm)|Liberating Trick|Truco liberador|modificada|
|[PcLLTu6aeVnY2Ogg.htm](age-of-ashes-bestiary-items/PcLLTu6aeVnY2Ogg.htm)|Claw|Garra|modificada|
|[pDkq3pcVeqspt9o5.htm](age-of-ashes-bestiary-items/pDkq3pcVeqspt9o5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PdMypFZGmGTQBUO7.htm](age-of-ashes-bestiary-items/PdMypFZGmGTQBUO7.htm)|Door|Puerta|modificada|
|[pdp1XYZ1mtcsTvM3.htm](age-of-ashes-bestiary-items/pdp1XYZ1mtcsTvM3.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[peAxpqZ3KlwaPcjL.htm](age-of-ashes-bestiary-items/peAxpqZ3KlwaPcjL.htm)|Paralyzing Force|Fuerza Paralizante|modificada|
|[PemrcXjJFtKQ8192.htm](age-of-ashes-bestiary-items/PemrcXjJFtKQ8192.htm)|Shortsword|Espada corta|modificada|
|[peSYmPiuqBy9OU2r.htm](age-of-ashes-bestiary-items/peSYmPiuqBy9OU2r.htm)|Change Shape|Change Shape|modificada|
|[pF8fcF4d3JB5eI7V.htm](age-of-ashes-bestiary-items/pF8fcF4d3JB5eI7V.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pfkbWfIp6yg9hS9F.htm](age-of-ashes-bestiary-items/pfkbWfIp6yg9hS9F.htm)|Shortsword|Espada corta|modificada|
|[pFweIlOkcH38fuxf.htm](age-of-ashes-bestiary-items/pFweIlOkcH38fuxf.htm)|Spikes|Púas|modificada|
|[pHsMHzt7Z1sM5ScM.htm](age-of-ashes-bestiary-items/pHsMHzt7Z1sM5ScM.htm)|Tongue|Lengua|modificada|
|[PhUe9NLaMWI1oIUJ.htm](age-of-ashes-bestiary-items/PhUe9NLaMWI1oIUJ.htm)|Halberd|Alabarda|modificada|
|[pIKbKs8nTXF2nUAm.htm](age-of-ashes-bestiary-items/pIKbKs8nTXF2nUAm.htm)|Fiend Summoner|Convocador de infernales|modificada|
|[pjPtT8ePki7wgMaO.htm](age-of-ashes-bestiary-items/pjPtT8ePki7wgMaO.htm)|Swift Leap|Salto veloz|modificada|
|[plcsZ25Y6jlruAMn.htm](age-of-ashes-bestiary-items/plcsZ25Y6jlruAMn.htm)|Twin Parry|Parada gemela|modificada|
|[pLURCvEu8rYktiZE.htm](age-of-ashes-bestiary-items/pLURCvEu8rYktiZE.htm)|Paralyzing Touch|Toque paralizante|modificada|
|[pmGTuXbF38zbcznu.htm](age-of-ashes-bestiary-items/pmGTuXbF38zbcznu.htm)|Claw|Garra|modificada|
|[PmP7kVHHTCVo9E2y.htm](age-of-ashes-bestiary-items/PmP7kVHHTCVo9E2y.htm)|Longsword|Longsword|modificada|
|[PMvxB0zhxiZNDxxW.htm](age-of-ashes-bestiary-items/PMvxB0zhxiZNDxxW.htm)|Necrotic Field|Campo Necrótico|modificada|
|[ppHf9s7R9QhMXyxE.htm](age-of-ashes-bestiary-items/ppHf9s7R9QhMXyxE.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[ppTxZD2ni4N12liM.htm](age-of-ashes-bestiary-items/ppTxZD2ni4N12liM.htm)|Fangs|Colmillos|modificada|
|[pqOaIKfZFro1QcD2.htm](age-of-ashes-bestiary-items/pqOaIKfZFro1QcD2.htm)|Shrieking Frenzy|Shrieking Frenzy|modificada|
|[PrMrxrT6XDC0RiYK.htm](age-of-ashes-bestiary-items/PrMrxrT6XDC0RiYK.htm)|Thrown Weapon Mastery|Maestría con las armas arrojadizas|modificada|
|[Ps0fTafFUadvtLAi.htm](age-of-ashes-bestiary-items/Ps0fTafFUadvtLAi.htm)|Efficient Capture|Captura eficiente|modificada|
|[PT2MUTRJtcohFtP9.htm](age-of-ashes-bestiary-items/PT2MUTRJtcohFtP9.htm)|Lava Bomb|Bomba de lava|modificada|
|[pTHZcKbaFZmIwmOo.htm](age-of-ashes-bestiary-items/pTHZcKbaFZmIwmOo.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[PTLfrf6aBtRzqBS1.htm](age-of-ashes-bestiary-items/PTLfrf6aBtRzqBS1.htm)|Nolly's Hoe|Azada de Nolly|modificada|
|[pvaF5cf0XkH3cCbK.htm](age-of-ashes-bestiary-items/pvaF5cf0XkH3cCbK.htm)|Claw|Garra|modificada|
|[pWNu3N2Ovu0m978Z.htm](age-of-ashes-bestiary-items/pWNu3N2Ovu0m978Z.htm)|Blood Quarry|Cantera de Sangre|modificada|
|[PWOiCJ1IBh9T12ba.htm](age-of-ashes-bestiary-items/PWOiCJ1IBh9T12ba.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Py6UVHQQNzpI6Lww.htm](age-of-ashes-bestiary-items/Py6UVHQQNzpI6Lww.htm)|Soul Chain|Cadena de almas|modificada|
|[py7MzheoXU7EEjNA.htm](age-of-ashes-bestiary-items/py7MzheoXU7EEjNA.htm)|Negative Healing|Curación negativa|modificada|
|[pYNPdghXNuSzUUUS.htm](age-of-ashes-bestiary-items/pYNPdghXNuSzUUUS.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[pzO0JwhFqVcRKzAN.htm](age-of-ashes-bestiary-items/pzO0JwhFqVcRKzAN.htm)|Giant Tarantula Venom|Veneno de Tarántula Gigante|modificada|
|[q1GTtW7ypFaAaLpl.htm](age-of-ashes-bestiary-items/q1GTtW7ypFaAaLpl.htm)|Shield Block|Bloquear con escudo|modificada|
|[Q2kYqdtBur9KmjY3.htm](age-of-ashes-bestiary-items/Q2kYqdtBur9KmjY3.htm)|Jaws|Fauces|modificada|
|[Q6x7Lrplu7fSsiYf.htm](age-of-ashes-bestiary-items/Q6x7Lrplu7fSsiYf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[q90PCZCTBqoxOX62.htm](age-of-ashes-bestiary-items/q90PCZCTBqoxOX62.htm)|Jaws|Fauces|modificada|
|[Qbd7q1jTcWs61OsF.htm](age-of-ashes-bestiary-items/Qbd7q1jTcWs61OsF.htm)|Hail of Arrows|Lluvia de flechas|modificada|
|[QD3U7kcL3GCOpa3y.htm](age-of-ashes-bestiary-items/QD3U7kcL3GCOpa3y.htm)|Smoke Vision|Visión de Humo|modificada|
|[qdRHgb9PmpqQ2jTG.htm](age-of-ashes-bestiary-items/qdRHgb9PmpqQ2jTG.htm)|Quick Draw|Desenvainado rápido|modificada|
|[qfAZ12H9DKd8pEad.htm](age-of-ashes-bestiary-items/qfAZ12H9DKd8pEad.htm)|Shell Defense|Defensa con caparazón|modificada|
|[QFVHYWU00lctwBwf.htm](age-of-ashes-bestiary-items/QFVHYWU00lctwBwf.htm)|Change Shape|Change Shape|modificada|
|[QhwSqyQ7YKPRITDw.htm](age-of-ashes-bestiary-items/QhwSqyQ7YKPRITDw.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[qi5FtOzBWTiiCStB.htm](age-of-ashes-bestiary-items/qi5FtOzBWTiiCStB.htm)|Explosion|Explosión|modificada|
|[QKmae867GS9N8IKK.htm](age-of-ashes-bestiary-items/QKmae867GS9N8IKK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qLKyo9iiDXWWqXvs.htm](age-of-ashes-bestiary-items/qLKyo9iiDXWWqXvs.htm)|Grab|Agarrado|modificada|
|[QmrJQN5wGkBpsqMo.htm](age-of-ashes-bestiary-items/QmrJQN5wGkBpsqMo.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[qnM0VhMof6zWC5w1.htm](age-of-ashes-bestiary-items/qnM0VhMof6zWC5w1.htm)|Claw|Garra|modificada|
|[QPPzshzEbERB3TmQ.htm](age-of-ashes-bestiary-items/QPPzshzEbERB3TmQ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[Qqwiu5T6ag0OjmLs.htm](age-of-ashes-bestiary-items/Qqwiu5T6ag0OjmLs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QStNE8d3elauT8lt.htm](age-of-ashes-bestiary-items/QStNE8d3elauT8lt.htm)|Silver Dagger|Daga de plata|modificada|
|[QsuzBxkK6bf7oS8S.htm](age-of-ashes-bestiary-items/QsuzBxkK6bf7oS8S.htm)|Captivate|Captivate|modificada|
|[QtbetvkizVrPlsdb.htm](age-of-ashes-bestiary-items/QtbetvkizVrPlsdb.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[qtiwr8Rp5Y4kIa7O.htm](age-of-ashes-bestiary-items/qtiwr8Rp5Y4kIa7O.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[QVKGD3jLsVMeTZzu.htm](age-of-ashes-bestiary-items/QVKGD3jLsVMeTZzu.htm)|Surprise Strike|Golpe Sorpresa|modificada|
|[Qw4pImBH1KPofsS1.htm](age-of-ashes-bestiary-items/Qw4pImBH1KPofsS1.htm)|Assemble Choir|Ensamblar Coro|modificada|
|[QWZ2jVcqi9CN4U6I.htm](age-of-ashes-bestiary-items/QWZ2jVcqi9CN4U6I.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[r0yOc789JUXP2zpN.htm](age-of-ashes-bestiary-items/r0yOc789JUXP2zpN.htm)|Aluum Antimagic|Aluum Antimagia|modificada|
|[R9ndNOt0KSVZBrCG.htm](age-of-ashes-bestiary-items/R9ndNOt0KSVZBrCG.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[RBix0BOrbHGTntW9.htm](age-of-ashes-bestiary-items/RBix0BOrbHGTntW9.htm)|Dragonstorm Aura|Aura Tormenta de Dragones|modificada|
|[RF1LobyvELbvZKdf.htm](age-of-ashes-bestiary-items/RF1LobyvELbvZKdf.htm)|Wings|Alas|modificada|
|[Rg3ygnHjRloP7FoD.htm](age-of-ashes-bestiary-items/Rg3ygnHjRloP7FoD.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[rGBruApiTZDc1HuB.htm](age-of-ashes-bestiary-items/rGBruApiTZDc1HuB.htm)|Redirect Energy|Redirigir Energía|modificada|
|[RgYma4kYCK90TSJD.htm](age-of-ashes-bestiary-items/RgYma4kYCK90TSJD.htm)|Imprisoned|Cautiverio|modificada|
|[RhOO7QphVzTN8Y2j.htm](age-of-ashes-bestiary-items/RhOO7QphVzTN8Y2j.htm)|Furious Claws|Garras furiosas|modificada|
|[rI6uxFg6LJxzLDta.htm](age-of-ashes-bestiary-items/rI6uxFg6LJxzLDta.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[riLQEjTb79Im6cTV.htm](age-of-ashes-bestiary-items/riLQEjTb79Im6cTV.htm)|Tongue Pull|Tirón de lengua|modificada|
|[RiXHSOTCKfKIONvP.htm](age-of-ashes-bestiary-items/RiXHSOTCKfKIONvP.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[RjIX3v1EU84c920i.htm](age-of-ashes-bestiary-items/RjIX3v1EU84c920i.htm)|Breath Snatcher|Breath Snatcher|modificada|
|[rJLMQhvHJTZBujHg.htm](age-of-ashes-bestiary-items/rJLMQhvHJTZBujHg.htm)|Frightful Presence|Frightful Presence|modificada|
|[RJtBHxR2U04jtD7I.htm](age-of-ashes-bestiary-items/RJtBHxR2U04jtD7I.htm)|Destructive Explosion|Explosión Destructiva|modificada|
|[RKUjuuHr7izDXZqd.htm](age-of-ashes-bestiary-items/RKUjuuHr7izDXZqd.htm)|Reach Spell|Conjuro de alcance|modificada|
|[RmNG2smIjmn2isvO.htm](age-of-ashes-bestiary-items/RmNG2smIjmn2isvO.htm)|Deny Advantage|Denegar ventaja|modificada|
|[rOBeT7cNsBIJSzT7.htm](age-of-ashes-bestiary-items/rOBeT7cNsBIJSzT7.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[rqq8NjCeDPoInPTq.htm](age-of-ashes-bestiary-items/rqq8NjCeDPoInPTq.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[rRrTo5REtGDduBlv.htm](age-of-ashes-bestiary-items/rRrTo5REtGDduBlv.htm)|Fangs|Colmillos|modificada|
|[rU9gIiMETSDDnipL.htm](age-of-ashes-bestiary-items/rU9gIiMETSDDnipL.htm)|Tail|Tail|modificada|
|[rUMDjgQAaue8rA2w.htm](age-of-ashes-bestiary-items/rUMDjgQAaue8rA2w.htm)|Regeneration 15 (Deactivated by Good)|Regeneración 15 (Desactivado por Bueno)|modificada|
|[RwCa647AJFm8BWJR.htm](age-of-ashes-bestiary-items/RwCa647AJFm8BWJR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rWg63VpAt6JzaV7z.htm](age-of-ashes-bestiary-items/rWg63VpAt6JzaV7z.htm)|Wing|Ala|modificada|
|[RwOAEo9R3fFzTJY7.htm](age-of-ashes-bestiary-items/RwOAEo9R3fFzTJY7.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[ryLB6NeDkc2PFBmb.htm](age-of-ashes-bestiary-items/ryLB6NeDkc2PFBmb.htm)|Carapace|Caparazón|modificada|
|[rZ3NKdEarhgXFXmc.htm](age-of-ashes-bestiary-items/rZ3NKdEarhgXFXmc.htm)|Catch Rock|Atrapar roca|modificada|
|[RzjCNg6HpRoCBHoz.htm](age-of-ashes-bestiary-items/RzjCNg6HpRoCBHoz.htm)|Jungle Stride|Zancada selvática|modificada|
|[s2CoTcNYzppH89ej.htm](age-of-ashes-bestiary-items/s2CoTcNYzppH89ej.htm)|Cinderclaw Gauntlet|Guantelete Cinderclaw|modificada|
|[S5Inn9EP3p8RwITB.htm](age-of-ashes-bestiary-items/S5Inn9EP3p8RwITB.htm)|Warded Casting|Warded Casting|modificada|
|[s5L6PEZRqvHlBdWg.htm](age-of-ashes-bestiary-items/s5L6PEZRqvHlBdWg.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[saqRnmZhtXKwqFed.htm](age-of-ashes-bestiary-items/saqRnmZhtXKwqFed.htm)|Demilich Eye Gems|Gemas oculares de semiliche|modificada|
|[sBHiaipaEIdoNG9q.htm](age-of-ashes-bestiary-items/sBHiaipaEIdoNG9q.htm)|Telekinetic Storm|Tormenta Telequinética|modificada|
|[SBQifbHhjnwomydG.htm](age-of-ashes-bestiary-items/SBQifbHhjnwomydG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sc3daf0IgUPTb2rM.htm](age-of-ashes-bestiary-items/sc3daf0IgUPTb2rM.htm)|Greater Frost Vial|Gélida de frasco de escarcha mayor.|modificada|
|[SdtmvPS7LAUlE41O.htm](age-of-ashes-bestiary-items/SdtmvPS7LAUlE41O.htm)|Swamp Stride|Swamp Stride|modificada|
|[seHBkvEFgGTlfAtt.htm](age-of-ashes-bestiary-items/seHBkvEFgGTlfAtt.htm)|Dagger|Daga|modificada|
|[sEn2RRTxyiFjpqfu.htm](age-of-ashes-bestiary-items/sEn2RRTxyiFjpqfu.htm)|Resonant Wail|Lamento resonante|modificada|
|[SH0nk4BfgI1ur3oJ.htm](age-of-ashes-bestiary-items/SH0nk4BfgI1ur3oJ.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[SH5975wJdafMupdy.htm](age-of-ashes-bestiary-items/SH5975wJdafMupdy.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[shtsWYCyenHOUURJ.htm](age-of-ashes-bestiary-items/shtsWYCyenHOUURJ.htm)|Primal Rituals|Primal Rituals|modificada|
|[SHZi101TqFOYWlIL.htm](age-of-ashes-bestiary-items/SHZi101TqFOYWlIL.htm)|Green Eye Beam|Rayo Ojo Verde|modificada|
|[SJg4gN7fByvyyHi4.htm](age-of-ashes-bestiary-items/SJg4gN7fByvyyHi4.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[sjs5I4Em4t7kFOwK.htm](age-of-ashes-bestiary-items/sjs5I4Em4t7kFOwK.htm)|Violet Eye Beam|Violet Eye Beam|modificada|
|[SJvjp5tv9lMwcTyw.htm](age-of-ashes-bestiary-items/SJvjp5tv9lMwcTyw.htm)|Weapon Trick|Truco con las armas|modificada|
|[sKH86qpUBKLIW8ls.htm](age-of-ashes-bestiary-items/sKH86qpUBKLIW8ls.htm)|Efficient Capture|Captura eficiente|modificada|
|[skQ2ydydslD2F8Br.htm](age-of-ashes-bestiary-items/skQ2ydydslD2F8Br.htm)|Fist|Puño|modificada|
|[SkzgfQVojElPeAh6.htm](age-of-ashes-bestiary-items/SkzgfQVojElPeAh6.htm)|Breath Weapon|Breath Weapon|modificada|
|[sLKipq0eV2QIdP0P.htm](age-of-ashes-bestiary-items/sLKipq0eV2QIdP0P.htm)|Consume Flesh|Consumir carne|modificada|
|[sLQqwa6hoaw69U93.htm](age-of-ashes-bestiary-items/sLQqwa6hoaw69U93.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[SlVqql8huNGs1yPM.htm](age-of-ashes-bestiary-items/SlVqql8huNGs1yPM.htm)|Horns|Cuernos|modificada|
|[SmEfn9FnR2kxlXyr.htm](age-of-ashes-bestiary-items/SmEfn9FnR2kxlXyr.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[SNejKUlOFhtJWGTZ.htm](age-of-ashes-bestiary-items/SNejKUlOFhtJWGTZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Sohj5I5HF2aiJ17e.htm](age-of-ashes-bestiary-items/Sohj5I5HF2aiJ17e.htm)|Yellow Eye Beam|Yellow Eye Beam|modificada|
|[sQsoukNeadSMEKWS.htm](age-of-ashes-bestiary-items/sQsoukNeadSMEKWS.htm)|Beak|Beak|modificada|
|[sRUKk7rcYO0y4CrH.htm](age-of-ashes-bestiary-items/sRUKk7rcYO0y4CrH.htm)|Razor Sharp|Navaja afilada|modificada|
|[Supu8qO5sZ5gfG4U.htm](age-of-ashes-bestiary-items/Supu8qO5sZ5gfG4U.htm)|Claw|Garra|modificada|
|[SV6WrRtaq5OJtKUq.htm](age-of-ashes-bestiary-items/SV6WrRtaq5OJtKUq.htm)|Hampering Shot|Hampering Shot|modificada|
|[SW6CqLFVtIpxlZcy.htm](age-of-ashes-bestiary-items/SW6CqLFVtIpxlZcy.htm)|Major Acid Flask|Frasco de ácido, superior.|modificada|
|[swIJOeM7FXkgQV1H.htm](age-of-ashes-bestiary-items/swIJOeM7FXkgQV1H.htm)|Jaws|Fauces|modificada|
|[SxjIbhY3Md3bS3dx.htm](age-of-ashes-bestiary-items/SxjIbhY3Md3bS3dx.htm)|Morningstar|Morningstar|modificada|
|[sXWZxvJlU7LZrpHC.htm](age-of-ashes-bestiary-items/sXWZxvJlU7LZrpHC.htm)|Orange Eye Beam|Rayo Ojo Naranja|modificada|
|[t00h5GlGA9LELf56.htm](age-of-ashes-bestiary-items/t00h5GlGA9LELf56.htm)|Soul Chain|Cadena de almas|modificada|
|[T1sHFc7JoaRRC0iK.htm](age-of-ashes-bestiary-items/T1sHFc7JoaRRC0iK.htm)|Claw|Garra|modificada|
|[T3t1bWcAoYCVuiiE.htm](age-of-ashes-bestiary-items/T3t1bWcAoYCVuiiE.htm)|Drain Soul Cage|Drenar Jaula de Almas|modificada|
|[t5AqMVMcvrvoUCcN.htm](age-of-ashes-bestiary-items/t5AqMVMcvrvoUCcN.htm)|Necrotic Affliction|Aflicción necrótica.|modificada|
|[T82SzULUe8LH0NIZ.htm](age-of-ashes-bestiary-items/T82SzULUe8LH0NIZ.htm)|Split|Split|modificada|
|[TBCoGqPy0fhFmpMq.htm](age-of-ashes-bestiary-items/TBCoGqPy0fhFmpMq.htm)|Twin Takedown|Derribo gemelo|modificada|
|[tBIBeHhWxRinsTO5.htm](age-of-ashes-bestiary-items/tBIBeHhWxRinsTO5.htm)|Soul Binder|Encuadernador de almas|modificada|
|[TbMZhJ1evBpaauR7.htm](age-of-ashes-bestiary-items/TbMZhJ1evBpaauR7.htm)|Create Shadow Sanctuary|Elaborar Sombra Santuario|modificada|
|[tCcBQS2TNgo7EIvB.htm](age-of-ashes-bestiary-items/tCcBQS2TNgo7EIvB.htm)|Efficient Capture|Captura eficiente|modificada|
|[tdbB9sBzDpFxgzUz.htm](age-of-ashes-bestiary-items/tdbB9sBzDpFxgzUz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TF94bqW7ak7QdxKX.htm](age-of-ashes-bestiary-items/TF94bqW7ak7QdxKX.htm)|Efficient Capture|Captura eficiente|modificada|
|[tfAQ1l4sV2c1psLD.htm](age-of-ashes-bestiary-items/tfAQ1l4sV2c1psLD.htm)|Precise Debilitations|Debilitaciones precisas|modificada|
|[Thxxg6IpvoOEOFZu.htm](age-of-ashes-bestiary-items/Thxxg6IpvoOEOFZu.htm)|Scoff at the Divine|Burlarse de lo divino.|modificada|
|[tHzRquAwQcwKEsgJ.htm](age-of-ashes-bestiary-items/tHzRquAwQcwKEsgJ.htm)|Superior Pack Attack|Ataque en manada superior|modificada|
|[TIhbTdeTcedCd2mp.htm](age-of-ashes-bestiary-items/TIhbTdeTcedCd2mp.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TJWwRY8dDZB5iZvQ.htm](age-of-ashes-bestiary-items/TJWwRY8dDZB5iZvQ.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[tKTardJEDrHVNygR.htm](age-of-ashes-bestiary-items/tKTardJEDrHVNygR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tl2R3rnbspKfWaNX.htm](age-of-ashes-bestiary-items/tl2R3rnbspKfWaNX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tmioSSIeBsfcu9eY.htm](age-of-ashes-bestiary-items/tmioSSIeBsfcu9eY.htm)|5-6 Bolts of Lightning|5-6 Relámpagos|modificada|
|[To3ttdceko61OtLl.htm](age-of-ashes-bestiary-items/To3ttdceko61OtLl.htm)|Breath Weapon|Breath Weapon|modificada|
|[TOo0Vq8jXzgZHSGq.htm](age-of-ashes-bestiary-items/TOo0Vq8jXzgZHSGq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[TpYZfRAOyWTWC5du.htm](age-of-ashes-bestiary-items/TpYZfRAOyWTWC5du.htm)|Quick Movements|Movimientos Rápidos|modificada|
|[TsPUS6z43vjlNNkp.htm](age-of-ashes-bestiary-items/TsPUS6z43vjlNNkp.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[TSSV8a0oKTioJrDT.htm](age-of-ashes-bestiary-items/TSSV8a0oKTioJrDT.htm)|Spectral Fist|Puño Espectral|modificada|
|[TTHC9vPRacUfSJco.htm](age-of-ashes-bestiary-items/TTHC9vPRacUfSJco.htm)|Crystallize|Cristalizar|modificada|
|[TU1DbG3ubOh1bDcw.htm](age-of-ashes-bestiary-items/TU1DbG3ubOh1bDcw.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[TupueOehWDuKS2G5.htm](age-of-ashes-bestiary-items/TupueOehWDuKS2G5.htm)|Shortbow|Arco corto|modificada|
|[tvo7UaALlpq5xWzP.htm](age-of-ashes-bestiary-items/tvo7UaALlpq5xWzP.htm)|Halfling Luck|Suerte del mediano|modificada|
|[TW4r3ip5ObjvYZMs.htm](age-of-ashes-bestiary-items/TW4r3ip5ObjvYZMs.htm)|Shield Spikes|Púas de escudo|modificada|
|[txRGabDLLfoByLm2.htm](age-of-ashes-bestiary-items/txRGabDLLfoByLm2.htm)|Thundering Maul|Zarpazo doble|modificada|
|[u3F1k0BPoP05Xfzn.htm](age-of-ashes-bestiary-items/u3F1k0BPoP05Xfzn.htm)|Scent (Imprecise) 120 feet|Olor (Impreciso) 120 pies|modificada|
|[U62hbZDWfFSaAj64.htm](age-of-ashes-bestiary-items/U62hbZDWfFSaAj64.htm)|Smoke Vision|Visión de Humo|modificada|
|[U6Wwg69lSYJAW46L.htm](age-of-ashes-bestiary-items/U6Wwg69lSYJAW46L.htm)|Constant Spells|Constant Spells|modificada|
|[uapfpdQSjUz13nBn.htm](age-of-ashes-bestiary-items/uapfpdQSjUz13nBn.htm)|Eye Beam|Eye Beam|modificada|
|[uAtJgY4Z2GeiZYOi.htm](age-of-ashes-bestiary-items/uAtJgY4Z2GeiZYOi.htm)|No MAP|No MAP|modificada|
|[ubsRZqqQfGfMpXdi.htm](age-of-ashes-bestiary-items/ubsRZqqQfGfMpXdi.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[UcUzuyj7o64QhvCg.htm](age-of-ashes-bestiary-items/UcUzuyj7o64QhvCg.htm)|Dagger|Daga|modificada|
|[UDRLkUU6I38waiLi.htm](age-of-ashes-bestiary-items/UDRLkUU6I38waiLi.htm)|Tail Swipe|Vaivén de Cola|modificada|
|[UDx53YecyYB7KIrJ.htm](age-of-ashes-bestiary-items/UDx53YecyYB7KIrJ.htm)|Anadi Venom|Anadi Veneno|modificada|
|[uf0REXRCA5Os0He4.htm](age-of-ashes-bestiary-items/uf0REXRCA5Os0He4.htm)|Claw|Garra|modificada|
|[Uf6zmBkqRIkAQTiY.htm](age-of-ashes-bestiary-items/Uf6zmBkqRIkAQTiY.htm)|Claw|Garra|modificada|
|[UfanUBQ8Zs0ulv4e.htm](age-of-ashes-bestiary-items/UfanUBQ8Zs0ulv4e.htm)|Frightful Presence|Frightful Presence|modificada|
|[UhafuYzXLYPCHxPD.htm](age-of-ashes-bestiary-items/UhafuYzXLYPCHxPD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[uIFB2aU5gYZC7XXL.htm](age-of-ashes-bestiary-items/uIFB2aU5gYZC7XXL.htm)|Innate Arcane Spells|Hechizos Arcanos Innatos|modificada|
|[uiKPE6MQz8FAQvOx.htm](age-of-ashes-bestiary-items/uiKPE6MQz8FAQvOx.htm)|Shield Warden|Guardián escudo|modificada|
|[UIvPyDXGpNjhmqBZ.htm](age-of-ashes-bestiary-items/UIvPyDXGpNjhmqBZ.htm)|Hatchet|Hacha|modificada|
|[ulyeBuv2PkM39FKm.htm](age-of-ashes-bestiary-items/ulyeBuv2PkM39FKm.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[umJV0GGKKxt5cyGw.htm](age-of-ashes-bestiary-items/umJV0GGKKxt5cyGw.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[UOfQIWygJYcqT1kQ.htm](age-of-ashes-bestiary-items/UOfQIWygJYcqT1kQ.htm)|Mobility|Movilidad|modificada|
|[UQaty56jSFbPq0pG.htm](age-of-ashes-bestiary-items/UQaty56jSFbPq0pG.htm)|Bite|Muerdemuerde|modificada|
|[UQInVEZyWsihDZJI.htm](age-of-ashes-bestiary-items/UQInVEZyWsihDZJI.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[UQnCKGq6QIQn8xKq.htm](age-of-ashes-bestiary-items/UQnCKGq6QIQn8xKq.htm)|Greatsword|Greatsword|modificada|
|[uR0sxFBmOFXxYqZi.htm](age-of-ashes-bestiary-items/uR0sxFBmOFXxYqZi.htm)|Claw|Garra|modificada|
|[ur5ordRthB6Bqyei.htm](age-of-ashes-bestiary-items/ur5ordRthB6Bqyei.htm)|Grab|Agarrado|modificada|
|[USQ16Z86qZYqJDkV.htm](age-of-ashes-bestiary-items/USQ16Z86qZYqJDkV.htm)|Persistent Bleed Critical|Persistente Sangrado Crítico|modificada|
|[Ut7DWly2y0Fghbxh.htm](age-of-ashes-bestiary-items/Ut7DWly2y0Fghbxh.htm)|Corrupt Ally|Aliado Corrupto|modificada|
|[uufsa9Y9z4IZhCZb.htm](age-of-ashes-bestiary-items/uufsa9Y9z4IZhCZb.htm)|Keen Eyes|Ojos Afilados|modificada|
|[UwdvzOviNTex0Xqb.htm](age-of-ashes-bestiary-items/UwdvzOviNTex0Xqb.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[UxtBtayqxXB5mZip.htm](age-of-ashes-bestiary-items/UxtBtayqxXB5mZip.htm)|Bully's Push|Bully's Push|modificada|
|[V02rKZLk4zOrx65r.htm](age-of-ashes-bestiary-items/V02rKZLk4zOrx65r.htm)|Thrown Debris|Arrojar Escombros|modificada|
|[v0Bn1s1wRHKj8MGA.htm](age-of-ashes-bestiary-items/v0Bn1s1wRHKj8MGA.htm)|Lazurite-Infused Flesh|Lazurite-Infused Flesh|modificada|
|[v1DFg4W547LdWp1p.htm](age-of-ashes-bestiary-items/v1DFg4W547LdWp1p.htm)|Pummeling Flurry|Aluvión de porrazos|modificada|
|[V2OOVzLbdPKRulIf.htm](age-of-ashes-bestiary-items/V2OOVzLbdPKRulIf.htm)|Adhesive Body|Cuerpo Adhesivo|modificada|
|[v2P0c4rmwI5BoEBT.htm](age-of-ashes-bestiary-items/v2P0c4rmwI5BoEBT.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[v5JDDS5HftErPvPh.htm](age-of-ashes-bestiary-items/v5JDDS5HftErPvPh.htm)|Opened Sluices|Compuertas Abiertas|modificada|
|[V6HMMbCm0RtvxhNC.htm](age-of-ashes-bestiary-items/V6HMMbCm0RtvxhNC.htm)|Claw|Garra|modificada|
|[V7VvhfarrGzmaDnf.htm](age-of-ashes-bestiary-items/V7VvhfarrGzmaDnf.htm)|Bite|Muerdemuerde|modificada|
|[v8QaZXPq7Yj9K2v1.htm](age-of-ashes-bestiary-items/v8QaZXPq7Yj9K2v1.htm)|Constant Spells|Constant Spells|modificada|
|[vCxz30plLPffDrlE.htm](age-of-ashes-bestiary-items/vCxz30plLPffDrlE.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[VDCmacyFJeeLLjxL.htm](age-of-ashes-bestiary-items/VDCmacyFJeeLLjxL.htm)|Incredible Initiative|Iniciativa sensacional|modificada|
|[veIehkJyX5KsiJzn.htm](age-of-ashes-bestiary-items/veIehkJyX5KsiJzn.htm)|Hunter's Flurry|Ráfaga de Cazadores|modificada|
|[veigzkgmikNe9iEL.htm](age-of-ashes-bestiary-items/veigzkgmikNe9iEL.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[vEOKWwRMGGUetHuk.htm](age-of-ashes-bestiary-items/vEOKWwRMGGUetHuk.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[vevNfOyFN6QRJtR2.htm](age-of-ashes-bestiary-items/vevNfOyFN6QRJtR2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[vfDhFBTrynIBoAmF.htm](age-of-ashes-bestiary-items/vfDhFBTrynIBoAmF.htm)|Prepared Divine Spells|Hechizos divinos preparados.|modificada|
|[VGLGaYgY8xICa8nV.htm](age-of-ashes-bestiary-items/VGLGaYgY8xICa8nV.htm)|Improved Grab|Agarrado mejorado|modificada|
|[VHTjtjDkxGqmFbdd.htm](age-of-ashes-bestiary-items/VHTjtjDkxGqmFbdd.htm)|Dagger|Daga|modificada|
|[vJ1d2ivm8JgAi3ET.htm](age-of-ashes-bestiary-items/vJ1d2ivm8JgAi3ET.htm)|Hatchet|Hacha|modificada|
|[VJbm5TGMwUCws6K1.htm](age-of-ashes-bestiary-items/VJbm5TGMwUCws6K1.htm)|Longsword|Longsword|modificada|
|[VkjtpvreAQ1v0h8Z.htm](age-of-ashes-bestiary-items/VkjtpvreAQ1v0h8Z.htm)|Hatchet|Hacha|modificada|
|[VlCgM24yW7fJGoEX.htm](age-of-ashes-bestiary-items/VlCgM24yW7fJGoEX.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[vlFbh7Px7CtFWTTe.htm](age-of-ashes-bestiary-items/vlFbh7Px7CtFWTTe.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[vM66vkQqjowc47rC.htm](age-of-ashes-bestiary-items/vM66vkQqjowc47rC.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[vmOhdTHRiDnoF4LK.htm](age-of-ashes-bestiary-items/vmOhdTHRiDnoF4LK.htm)|Eye Beam|Eye Beam|modificada|
|[VNd49RdCdrZvopZ3.htm](age-of-ashes-bestiary-items/VNd49RdCdrZvopZ3.htm)|Motion Sense|Sentido del movimiento|modificada|
|[vPx7Kv8CDnL5rd3e.htm](age-of-ashes-bestiary-items/vPx7Kv8CDnL5rd3e.htm)|Warhammer|Warhammer|modificada|
|[VqMcIUne6jfbwvel.htm](age-of-ashes-bestiary-items/VqMcIUne6jfbwvel.htm)|Blood Magic|Magia de sangre|modificada|
|[vreeTrUJqAchFZUM.htm](age-of-ashes-bestiary-items/vreeTrUJqAchFZUM.htm)|+2 Circumstance to All Saves vs. Shove, Trip, or Being Knocked Prone|+2 Circunstancia a todas las salvaciones contra empujón, Derribar o quedar tumbado/a.|modificada|
|[vt4zVYNHBbCrRw9A.htm](age-of-ashes-bestiary-items/vt4zVYNHBbCrRw9A.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VTvR3zWnwVcxzIwl.htm](age-of-ashes-bestiary-items/VTvR3zWnwVcxzIwl.htm)|Improved Grab|Agarrado mejorado|modificada|
|[vxAKcJqC8kzmxb4t.htm](age-of-ashes-bestiary-items/vxAKcJqC8kzmxb4t.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vYOEAucH84uYzG3z.htm](age-of-ashes-bestiary-items/vYOEAucH84uYzG3z.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VzUnnud6z8Zj4DTC.htm](age-of-ashes-bestiary-items/VzUnnud6z8Zj4DTC.htm)|Scimitar|Cimitarra|modificada|
|[W0905p3wQrOz7E2x.htm](age-of-ashes-bestiary-items/W0905p3wQrOz7E2x.htm)|Shield Block|Bloquear con escudo|modificada|
|[w40QDydlY3kEtlQE.htm](age-of-ashes-bestiary-items/w40QDydlY3kEtlQE.htm)|Rain of Arrows|Lluvia de flechas|modificada|
|[w481TRk2bURCXIxx.htm](age-of-ashes-bestiary-items/w481TRk2bURCXIxx.htm)|Coven|Coven|modificada|
|[w5Yk889pfRGklcLm.htm](age-of-ashes-bestiary-items/w5Yk889pfRGklcLm.htm)|Violet Eye Beam|Violet Eye Beam|modificada|
|[W6f4zhENM79wqemW.htm](age-of-ashes-bestiary-items/W6f4zhENM79wqemW.htm)|Tail|Tail|modificada|
|[wb5jOSkiZA5lYAX9.htm](age-of-ashes-bestiary-items/wb5jOSkiZA5lYAX9.htm)|Destructive Strikes|Golpes Destructivos|modificada|
|[wBZt1NPsZgzDeD2m.htm](age-of-ashes-bestiary-items/wBZt1NPsZgzDeD2m.htm)|Expanded Splash|Salpicadura extendida|modificada|
|[wDnuOjox5cjI817l.htm](age-of-ashes-bestiary-items/wDnuOjox5cjI817l.htm)|Immortality|Inmortalidad|modificada|
|[wHfHFuiq0qNw7Q0J.htm](age-of-ashes-bestiary-items/wHfHFuiq0qNw7Q0J.htm)|Vulnerability to Exorcism|Vulnerabilidad al exorcismo|modificada|
|[whQM5kvewGUXnnLe.htm](age-of-ashes-bestiary-items/whQM5kvewGUXnnLe.htm)|Anguished Shriek|Grito Angustiado|modificada|
|[WIq9iFyHE2YJoFwA.htm](age-of-ashes-bestiary-items/WIq9iFyHE2YJoFwA.htm)|Claw|Garra|modificada|
|[wkS0gm33Vs1iiRST.htm](age-of-ashes-bestiary-items/wkS0gm33Vs1iiRST.htm)|Dream Haunting|Acoso onírico del Sueño|modificada|
|[wmpfvEE1W9Sses2l.htm](age-of-ashes-bestiary-items/wmpfvEE1W9Sses2l.htm)|Bark Orders|Órdenes de Ladrido|modificada|
|[wnE8aQGkQtI87toJ.htm](age-of-ashes-bestiary-items/wnE8aQGkQtI87toJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Wo5Z8Jix5ohz1Urx.htm](age-of-ashes-bestiary-items/Wo5Z8Jix5ohz1Urx.htm)|Rock Dwarf|Enano de la roca|modificada|
|[wOCajMqUzhODb2NB.htm](age-of-ashes-bestiary-items/wOCajMqUzhODb2NB.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[WonIEfwUndYFXxyq.htm](age-of-ashes-bestiary-items/WonIEfwUndYFXxyq.htm)|Thrown Debris|Arrojar Escombros|modificada|
|[WpLmYPgmK0qG58K8.htm](age-of-ashes-bestiary-items/WpLmYPgmK0qG58K8.htm)|Pseudopod|Pseudópodo|modificada|
|[wqr4H9kTRu8PWkfc.htm](age-of-ashes-bestiary-items/wqr4H9kTRu8PWkfc.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[wSkREkhq1FagvrHD.htm](age-of-ashes-bestiary-items/wSkREkhq1FagvrHD.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[WsUJjMncNN9DWybk.htm](age-of-ashes-bestiary-items/WsUJjMncNN9DWybk.htm)|Frightful Presence|Frightful Presence|modificada|
|[wt7Ds9r5d3F2v34T.htm](age-of-ashes-bestiary-items/wt7Ds9r5d3F2v34T.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[wTrhi9PyRoPxh06i.htm](age-of-ashes-bestiary-items/wTrhi9PyRoPxh06i.htm)|Burning Mace|Maza Ardiente|modificada|
|[WvSXMmKbLjsbglaD.htm](age-of-ashes-bestiary-items/WvSXMmKbLjsbglaD.htm)|Glimpse of Redemption|Atisbo de redención|modificada|
|[wwajiyf5MX0aRcgm.htm](age-of-ashes-bestiary-items/wwajiyf5MX0aRcgm.htm)|Tail|Tail|modificada|
|[wXHpR0xMnyNfcooq.htm](age-of-ashes-bestiary-items/wXHpR0xMnyNfcooq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wXqVDCpWmbgHRsvv.htm](age-of-ashes-bestiary-items/wXqVDCpWmbgHRsvv.htm)|Prismatic Beam|Prismatic Beam|modificada|
|[x4yLU5eVHfGlI7wG.htm](age-of-ashes-bestiary-items/x4yLU5eVHfGlI7wG.htm)|Telekinetic Object|Objeto telequinético|modificada|
|[x6TyhZ97JVNfZqqb.htm](age-of-ashes-bestiary-items/x6TyhZ97JVNfZqqb.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[x7eL2M3jSBwA2oka.htm](age-of-ashes-bestiary-items/x7eL2M3jSBwA2oka.htm)|Frightful Presence|Frightful Presence|modificada|
|[x94Pe7R5UrLKdRbG.htm](age-of-ashes-bestiary-items/x94Pe7R5UrLKdRbG.htm)|Invoke Reckoning|Invocar reconciliación|modificada|
|[x95HHOmRVl9H3tZ3.htm](age-of-ashes-bestiary-items/x95HHOmRVl9H3tZ3.htm)|Catch Rock|Atrapar roca|modificada|
|[x9FRA1b0TaNaQQ1p.htm](age-of-ashes-bestiary-items/x9FRA1b0TaNaQQ1p.htm)|Pervasive Attacks|Ataques omnipresentes|modificada|
|[xCWXBbQXE3SoIRlC.htm](age-of-ashes-bestiary-items/xCWXBbQXE3SoIRlC.htm)|Spectral Hand|Mano espectral|modificada|
|[XD9BdYMcx2Thd1zB.htm](age-of-ashes-bestiary-items/XD9BdYMcx2Thd1zB.htm)|Shield Shove|Empujar escudoón.|modificada|
|[XDuhN8g49cd7hcxi.htm](age-of-ashes-bestiary-items/XDuhN8g49cd7hcxi.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[XfTtTfEahCIBTcwm.htm](age-of-ashes-bestiary-items/XfTtTfEahCIBTcwm.htm)|Flail|Mayal|modificada|
|[xfV7KvKayKVA2xs0.htm](age-of-ashes-bestiary-items/xfV7KvKayKVA2xs0.htm)|Explosive Breath|Aliento Explosión|modificada|
|[xGPdcgIZR0tAI9IY.htm](age-of-ashes-bestiary-items/xGPdcgIZR0tAI9IY.htm)|Hair Barrage|Hair Barrage|modificada|
|[xHg8hrfrTg35erxf.htm](age-of-ashes-bestiary-items/xHg8hrfrTg35erxf.htm)|Contingency|Contingencia|modificada|
|[XIHvMvJwIJxCpNA7.htm](age-of-ashes-bestiary-items/XIHvMvJwIJxCpNA7.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[XiZUzjxIJiMTuUq9.htm](age-of-ashes-bestiary-items/XiZUzjxIJiMTuUq9.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[XJmutb6gsFNsubD3.htm](age-of-ashes-bestiary-items/XJmutb6gsFNsubD3.htm)|Jaws|Fauces|modificada|
|[XLA0uMn7KxLm7iN7.htm](age-of-ashes-bestiary-items/XLA0uMn7KxLm7iN7.htm)|Tail Lash|Azote con la cola|modificada|
|[XLCaqjJ5OC0PqgJp.htm](age-of-ashes-bestiary-items/XLCaqjJ5OC0PqgJp.htm)|Dagger|Daga|modificada|
|[xMoISwQsrQGkhK7q.htm](age-of-ashes-bestiary-items/xMoISwQsrQGkhK7q.htm)|Dimensional Shunt|Dimensional Shunt|modificada|
|[xN7xuZd6V7GrLGii.htm](age-of-ashes-bestiary-items/xN7xuZd6V7GrLGii.htm)|7-8 Flaming Vortex|7-8 Flamígera Vortexígera|modificada|
|[Xnas07kWC3ZCJmY4.htm](age-of-ashes-bestiary-items/Xnas07kWC3ZCJmY4.htm)|Dragonstorm Breath|Dragonstorm Breath|modificada|
|[xnnq7Jjs1DHgYSUC.htm](age-of-ashes-bestiary-items/xnnq7Jjs1DHgYSUC.htm)|Web Sense (Imprecise) 60 feet|Sentido de la telara (Impreciso) 60 pies.|modificada|
|[XO1T3uBgfNZ2aJks.htm](age-of-ashes-bestiary-items/XO1T3uBgfNZ2aJks.htm)|Throw Rock|Arrojar roca|modificada|
|[XqfuKJG5gZuzeCDr.htm](age-of-ashes-bestiary-items/XqfuKJG5gZuzeCDr.htm)|Frightful Presence|Frightful Presence|modificada|
|[XrDITDvengsQOcjL.htm](age-of-ashes-bestiary-items/XrDITDvengsQOcjL.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[xSd36hFNsd1RqBw7.htm](age-of-ashes-bestiary-items/xSd36hFNsd1RqBw7.htm)|Ferocity|Ferocidad|modificada|
|[XsSwjx894G8tLV3p.htm](age-of-ashes-bestiary-items/XsSwjx894G8tLV3p.htm)|+2 Status to All Saves vs. Arcane Magic|+2 situación a todas las salvaciones contra magia arcana|modificada|
|[XteT6RS3CQMyiNDq.htm](age-of-ashes-bestiary-items/XteT6RS3CQMyiNDq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XTtyz95mOu6Xrre4.htm](age-of-ashes-bestiary-items/XTtyz95mOu6Xrre4.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[XUqvBZPxNzGJ4RIm.htm](age-of-ashes-bestiary-items/XUqvBZPxNzGJ4RIm.htm)|Vulnerable to Dispelling|Vulnerable a Disipar|modificada|
|[xvs14t3klUSEYCct.htm](age-of-ashes-bestiary-items/xvs14t3klUSEYCct.htm)|Prepared Arcane Spells|Hechizos Arcanos Preparados|modificada|
|[XXHOTNV0xQRjxtwV.htm](age-of-ashes-bestiary-items/XXHOTNV0xQRjxtwV.htm)|Stunning Retort|Stunning Retort|modificada|
|[XxjjAD0GTCEaspdx.htm](age-of-ashes-bestiary-items/XxjjAD0GTCEaspdx.htm)|Perfect Will|Voluntad Perfecta|modificada|
|[Xy8CYVbA7GcoN8SC.htm](age-of-ashes-bestiary-items/Xy8CYVbA7GcoN8SC.htm)|Whip Swing|Whip Swing|modificada|
|[xyPbW6KkYnYYyzU1.htm](age-of-ashes-bestiary-items/xyPbW6KkYnYYyzU1.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XzC7Tpe9v9HjnSor.htm](age-of-ashes-bestiary-items/XzC7Tpe9v9HjnSor.htm)|Localized Dragonstorm|Tormenta de Dragones Localizada|modificada|
|[XZIP4xIGRqQ4M88R.htm](age-of-ashes-bestiary-items/XZIP4xIGRqQ4M88R.htm)|Inexorable|Inexorable|modificada|
|[Y1AIIIjE3wSGI9AA.htm](age-of-ashes-bestiary-items/Y1AIIIjE3wSGI9AA.htm)|Engulf|Envolver|modificada|
|[y1Ce9dDq4YBhhTY5.htm](age-of-ashes-bestiary-items/y1Ce9dDq4YBhhTY5.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[y5Na5QhVKW187D0K.htm](age-of-ashes-bestiary-items/y5Na5QhVKW187D0K.htm)|Mental Magic|Magia mental|modificada|
|[y6J0cFsDLtTzzYwe.htm](age-of-ashes-bestiary-items/y6J0cFsDLtTzzYwe.htm)|Negative Healing|Curación negativa|modificada|
|[Y6pLT1gObD3hTlFj.htm](age-of-ashes-bestiary-items/Y6pLT1gObD3hTlFj.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[Y8FacHkJWvG6NwDu.htm](age-of-ashes-bestiary-items/Y8FacHkJWvG6NwDu.htm)|Rend|Rasgadura|modificada|
|[YC7WTdAOh21NEeNx.htm](age-of-ashes-bestiary-items/YC7WTdAOh21NEeNx.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[yCCEm7HEqedgAAhD.htm](age-of-ashes-bestiary-items/yCCEm7HEqedgAAhD.htm)|+1 Status to All Saves vs. Poison|+1 situación a todas las salvaciones contra veneno.|modificada|
|[yCOLPsjbm44QUBqX.htm](age-of-ashes-bestiary-items/yCOLPsjbm44QUBqX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[YeFEaw3Ja6zgrf9o.htm](age-of-ashes-bestiary-items/YeFEaw3Ja6zgrf9o.htm)|Double Slice|Doble tajo|modificada|
|[yEzDmYi6aYTiIA8Y.htm](age-of-ashes-bestiary-items/yEzDmYi6aYTiIA8Y.htm)|Ride the Wind|Montar el Viento|modificada|
|[yfiUAGMbYu5GpkVc.htm](age-of-ashes-bestiary-items/yfiUAGMbYu5GpkVc.htm)|Constant Spells|Constant Spells|modificada|
|[yggAbFpFmUKjj7Bz.htm](age-of-ashes-bestiary-items/yggAbFpFmUKjj7Bz.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[yh0jnKztqB0nQlYV.htm](age-of-ashes-bestiary-items/yh0jnKztqB0nQlYV.htm)|Nail|Uña|modificada|
|[YhBi7QJI7xdnQU10.htm](age-of-ashes-bestiary-items/YhBi7QJI7xdnQU10.htm)|Acidic Needle|Aguja Acida|modificada|
|[YhdTXGv6J7fkl4wd.htm](age-of-ashes-bestiary-items/YhdTXGv6J7fkl4wd.htm)|Inexorable March|Marcha inexorable|modificada|
|[Yimb91yYaAqRGR3x.htm](age-of-ashes-bestiary-items/Yimb91yYaAqRGR3x.htm)|Seismic Spear|Lanza Sísmica|modificada|
|[yJtHwSdZ0Gj2LGAH.htm](age-of-ashes-bestiary-items/yJtHwSdZ0Gj2LGAH.htm)|Shrieking Frenzy|Shrieking Frenzy|modificada|
|[ymEZhbkIeDn7RKGI.htm](age-of-ashes-bestiary-items/ymEZhbkIeDn7RKGI.htm)|Trap Soul|Atrapar alma|modificada|
|[YMJhZThbEkA9mBqf.htm](age-of-ashes-bestiary-items/YMJhZThbEkA9mBqf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yo9I3mhk7rIUPwWa.htm](age-of-ashes-bestiary-items/yo9I3mhk7rIUPwWa.htm)|Daemonic Trap Making|Fabricación de Trampas Daimónico|modificada|
|[YoSJ0Xce0DnEuH7Z.htm](age-of-ashes-bestiary-items/YoSJ0Xce0DnEuH7Z.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[yqO9SHE3AFcvwy0R.htm](age-of-ashes-bestiary-items/yqO9SHE3AFcvwy0R.htm)|Disturbing Vision|Disturbing Vision|modificada|
|[YQZTNPJv0qGnH7F6.htm](age-of-ashes-bestiary-items/YQZTNPJv0qGnH7F6.htm)|Focus Spells|Conjuros de foco|modificada|
|[yrnHfAodzInJxTRU.htm](age-of-ashes-bestiary-items/yrnHfAodzInJxTRU.htm)|Constant Spells|Constant Spells|modificada|
|[yRzCy4eBcJG8ydmJ.htm](age-of-ashes-bestiary-items/yRzCy4eBcJG8ydmJ.htm)|9-10 Poison Miasma|9-10 Poison Miasma|modificada|
|[YsvmoSYwOoP0KhQx.htm](age-of-ashes-bestiary-items/YsvmoSYwOoP0KhQx.htm)|Blue Eye Beam|Blue Eye Beam|modificada|
|[yt0OzxOSDhYO0Nle.htm](age-of-ashes-bestiary-items/yt0OzxOSDhYO0Nle.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[YT5Tkw6exXuHF2ph.htm](age-of-ashes-bestiary-items/YT5Tkw6exXuHF2ph.htm)|Soul Chain|Cadena de almas|modificada|
|[yTHVYVd7pjTLwWpu.htm](age-of-ashes-bestiary-items/yTHVYVd7pjTLwWpu.htm)|Syringe|Jeringa|modificada|
|[Yuh3IpCAUHQMRRoE.htm](age-of-ashes-bestiary-items/Yuh3IpCAUHQMRRoE.htm)|Deny Advantage|Denegar ventaja|modificada|
|[YW4QH2QsfjdJXFo8.htm](age-of-ashes-bestiary-items/YW4QH2QsfjdJXFo8.htm)|Aluum Antimagic|Aluum Antimagia|modificada|
|[yXL2GLDidWh51HPb.htm](age-of-ashes-bestiary-items/yXL2GLDidWh51HPb.htm)|Dagger|Daga|modificada|
|[yyARzCq3i2SQxTFI.htm](age-of-ashes-bestiary-items/yyARzCq3i2SQxTFI.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Z28y4lP04fvgUiy2.htm](age-of-ashes-bestiary-items/Z28y4lP04fvgUiy2.htm)|Mutagenic Adaptation|Adaptación Mutagénica|modificada|
|[z4fETZWZ1URqOEw5.htm](age-of-ashes-bestiary-items/z4fETZWZ1URqOEw5.htm)|Subsonic Hum|Zumbido subsónico|modificada|
|[Z6mjzInaKKCOyvbM.htm](age-of-ashes-bestiary-items/Z6mjzInaKKCOyvbM.htm)|Glowing Spores|Glowing Spores|modificada|
|[Z7DJdh6dNliHnh4s.htm](age-of-ashes-bestiary-items/Z7DJdh6dNliHnh4s.htm)|Crossbow|Ballesta|modificada|
|[Z875mlZme0MfKkqS.htm](age-of-ashes-bestiary-items/Z875mlZme0MfKkqS.htm)|Redirect Portal|Portal de Redirección|modificada|
|[z89EX9zy3v9pZxJc.htm](age-of-ashes-bestiary-items/z89EX9zy3v9pZxJc.htm)|Command Skeletons|Orden imperiosa Skeletons|modificada|
|[ZB91njeJdhCxI5DS.htm](age-of-ashes-bestiary-items/ZB91njeJdhCxI5DS.htm)|Rock|Roca|modificada|
|[zFmXqrFJBmQGs6z5.htm](age-of-ashes-bestiary-items/zFmXqrFJBmQGs6z5.htm)|Create Haunt|Elaborar Haunt|modificada|
|[zgALCNYNRMkV6Bhv.htm](age-of-ashes-bestiary-items/zgALCNYNRMkV6Bhv.htm)|Dagger|Daga|modificada|
|[ZgtDywSfiOFQD0r7.htm](age-of-ashes-bestiary-items/ZgtDywSfiOFQD0r7.htm)|Regeneration 50|Regeneración 50|modificada|
|[zhwIQhGc33778Khq.htm](age-of-ashes-bestiary-items/zhwIQhGc33778Khq.htm)|Improved Push|Empuje mejorado|modificada|
|[Zj2BDpm3BaAvGpbd.htm](age-of-ashes-bestiary-items/Zj2BDpm3BaAvGpbd.htm)|Arborean Arm|Arborean Arm|modificada|
|[ZkM3S4OabItFRUIq.htm](age-of-ashes-bestiary-items/ZkM3S4OabItFRUIq.htm)|Negative Healing|Curación negativa|modificada|
|[znA23DnIxfbHuPHh.htm](age-of-ashes-bestiary-items/znA23DnIxfbHuPHh.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[zRPB5eZcHteseFIt.htm](age-of-ashes-bestiary-items/zRPB5eZcHteseFIt.htm)|Nul-Acrumi Vazghul Ritual|Ritual Nul-Acrumi Vazghul|modificada|
|[ztAvaU3ULkuadpMc.htm](age-of-ashes-bestiary-items/ztAvaU3ULkuadpMc.htm)|Slash and Slam|Tajo y Golpe|modificada|
|[zuDdECTRYKz8JuqE.htm](age-of-ashes-bestiary-items/zuDdECTRYKz8JuqE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zVCalXk0BtUnOMfM.htm](age-of-ashes-bestiary-items/zVCalXk0BtUnOMfM.htm)|Sharpened Fragment|Fragmento Afilado|modificada|
|[ZWaPe5FQtP4LDiEM.htm](age-of-ashes-bestiary-items/ZWaPe5FQtP4LDiEM.htm)|Bleeding Nail|Uña sangrante|modificada|
|[zWMWo1FSBvdX4mhf.htm](age-of-ashes-bestiary-items/zWMWo1FSBvdX4mhf.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ZXC6yvdBWQpHBJIb.htm](age-of-ashes-bestiary-items/ZXC6yvdBWQpHBJIb.htm)|Constrict|Restringir|modificada|
|[ZXkVB1alDTTOm35s.htm](age-of-ashes-bestiary-items/ZXkVB1alDTTOm35s.htm)|Freeze|Freeze|modificada|
|[ZyKpCc2am8rFqQ93.htm](age-of-ashes-bestiary-items/ZyKpCc2am8rFqQ93.htm)|Manifest Dagger|Manifest Dagger|modificada|
|[ZYT8B7LGfrmxllL4.htm](age-of-ashes-bestiary-items/ZYT8B7LGfrmxllL4.htm)|Bloodsense|Bloodsense|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[31OJENvCqE3md7mz.htm](age-of-ashes-bestiary-items/31OJENvCqE3md7mz.htm)|Portal Lore|vacía|
|[3NMUDjuSV3mI03a1.htm](age-of-ashes-bestiary-items/3NMUDjuSV3mI03a1.htm)|Sack With 5 Rocks|vacía|
|[5qZDSHwIQw5algfw.htm](age-of-ashes-bestiary-items/5qZDSHwIQw5algfw.htm)|Library Lore|vacía|
|[60TKTeYwUMmrQq1Q.htm](age-of-ashes-bestiary-items/60TKTeYwUMmrQq1Q.htm)|Engineering Lore|vacía|
|[6gUaRoXq4cNuZm4W.htm](age-of-ashes-bestiary-items/6gUaRoXq4cNuZm4W.htm)|Athletics|vacía|
|[9h6YoWxYy7gK1ukK.htm](age-of-ashes-bestiary-items/9h6YoWxYy7gK1ukK.htm)|Stealth|vacía|
|[aTEZKPVNhE0QO16t.htm](age-of-ashes-bestiary-items/aTEZKPVNhE0QO16t.htm)|Underworld Lore|vacía|
|[BdxoNSpPKQa1FYSZ.htm](age-of-ashes-bestiary-items/BdxoNSpPKQa1FYSZ.htm)|Soul Chain|vacía|
|[CadRGJpqw2lsmgp4.htm](age-of-ashes-bestiary-items/CadRGJpqw2lsmgp4.htm)|Droskar Lore|vacía|
|[CClklHku7hq4O9Iy.htm](age-of-ashes-bestiary-items/CClklHku7hq4O9Iy.htm)|Dragon Lore|vacía|
|[DqY7L33t5uJuYvA3.htm](age-of-ashes-bestiary-items/DqY7L33t5uJuYvA3.htm)|Key to Manacles|vacía|
|[E9FrySzNqUR0B1Hm.htm](age-of-ashes-bestiary-items/E9FrySzNqUR0B1Hm.htm)|Athletics|vacía|
|[EwJM5XNHsNxMYpSu.htm](age-of-ashes-bestiary-items/EwJM5XNHsNxMYpSu.htm)|Crafting|vacía|
|[FdjRwOKhKnzO4NIv.htm](age-of-ashes-bestiary-items/FdjRwOKhKnzO4NIv.htm)|Hermea Lore|vacía|
|[fX2zoaLIfhwnjmIl.htm](age-of-ashes-bestiary-items/fX2zoaLIfhwnjmIl.htm)|Mercantile Lore|vacía|
|[g3GoUN2RoHvK9OHE.htm](age-of-ashes-bestiary-items/g3GoUN2RoHvK9OHE.htm)|Breachill Lore|vacía|
|[GWB9Y5J1UIUrQmlk.htm](age-of-ashes-bestiary-items/GWB9Y5J1UIUrQmlk.htm)|Infused Reagents|vacía|
|[hhRccIBA0AFYl4Su.htm](age-of-ashes-bestiary-items/hhRccIBA0AFYl4Su.htm)|Key Ring (Opens Locks in Summershade Quarry)|vacía|
|[IidItzJQhC8vx430.htm](age-of-ashes-bestiary-items/IidItzJQhC8vx430.htm)|Torture Lore|vacía|
|[ILyJiCshei4yjRby.htm](age-of-ashes-bestiary-items/ILyJiCshei4yjRby.htm)|Aiudara Lore|vacía|
|[iqsG9i0M9rfRDnFG.htm](age-of-ashes-bestiary-items/iqsG9i0M9rfRDnFG.htm)|Hermea Lore|vacía|
|[IZI87xvq3Ze64hGf.htm](age-of-ashes-bestiary-items/IZI87xvq3Ze64hGf.htm)|Sailing Lore|vacía|
|[kn7uZpzyYXn8aUO7.htm](age-of-ashes-bestiary-items/kn7uZpzyYXn8aUO7.htm)|Key to Manacles|vacía|
|[lSLgvPqHIBXWMrhx.htm](age-of-ashes-bestiary-items/lSLgvPqHIBXWMrhx.htm)|Plane of Fire Lore|vacía|
|[LTrdnkawIh2aRpXo.htm](age-of-ashes-bestiary-items/LTrdnkawIh2aRpXo.htm)|Katapesh Lore|vacía|
|[Mvo72kTvELk6g3ZD.htm](age-of-ashes-bestiary-items/Mvo72kTvELk6g3ZD.htm)|Shadow Plane Lore|vacía|
|[NMhI8XzKo8AYJRpE.htm](age-of-ashes-bestiary-items/NMhI8XzKo8AYJRpE.htm)|Keys to Manacles|vacía|
|[nSj9cVczzhejjp9x.htm](age-of-ashes-bestiary-items/nSj9cVczzhejjp9x.htm)|Saggorak Lore|vacía|
|[oh8XLsynAdQBhHEF.htm](age-of-ashes-bestiary-items/oh8XLsynAdQBhHEF.htm)|Azlanti Lore|vacía|
|[omPyHCBiVsSmjHtN.htm](age-of-ashes-bestiary-items/omPyHCBiVsSmjHtN.htm)|Keys to Each Dormitory Door and Slave Manacles|vacía|
|[oqVJRy53SS1ih6ly.htm](age-of-ashes-bestiary-items/oqVJRy53SS1ih6ly.htm)|Goblin Lore|vacía|
|[ouzInD92zLt5EjxO.htm](age-of-ashes-bestiary-items/ouzInD92zLt5EjxO.htm)|Underworld Lore|vacía|
|[P8LB7aKoS0qhsbne.htm](age-of-ashes-bestiary-items/P8LB7aKoS0qhsbne.htm)|Ancient History Lore|vacía|
|[PPVreyhaPRMpOiLh.htm](age-of-ashes-bestiary-items/PPVreyhaPRMpOiLh.htm)|Key to Chiselrock's Prison|vacía|
|[RNHnYYn5s0onufID.htm](age-of-ashes-bestiary-items/RNHnYYn5s0onufID.htm)|Crafting|vacía|
|[rzanS9lRHYe4hZQt.htm](age-of-ashes-bestiary-items/rzanS9lRHYe4hZQt.htm)|Sailing Lore|vacía|
|[S16umnQkXhv237qK.htm](age-of-ashes-bestiary-items/S16umnQkXhv237qK.htm)|Key to Manacles|vacía|
|[S7uX0GFBE6GEwURU.htm](age-of-ashes-bestiary-items/S7uX0GFBE6GEwURU.htm)|Dragon Lore|vacía|
|[s9Tsu9PDPlPpfeYQ.htm](age-of-ashes-bestiary-items/s9Tsu9PDPlPpfeYQ.htm)|Torture Lore|vacía|
|[SduZ57vCDQWA59rW.htm](age-of-ashes-bestiary-items/SduZ57vCDQWA59rW.htm)|Athletics|vacía|
|[sek6swVu6eFYssLv.htm](age-of-ashes-bestiary-items/sek6swVu6eFYssLv.htm)|Key to Chest|vacía|
|[T1MUgKrMJnC4CnSS.htm](age-of-ashes-bestiary-items/T1MUgKrMJnC4CnSS.htm)|Crafting|vacía|
|[tvWQ8FsujbpIgBJg.htm](age-of-ashes-bestiary-items/tvWQ8FsujbpIgBJg.htm)|Mining Lore|vacía|
|[UCFuIxxWMMjN84Rx.htm](age-of-ashes-bestiary-items/UCFuIxxWMMjN84Rx.htm)|Kovlar Lore|vacía|
|[UicWUJ68tR3WgGLD.htm](age-of-ashes-bestiary-items/UicWUJ68tR3WgGLD.htm)|Grapeshot|vacía|
|[uUTQ7Z3rao6lhQYz.htm](age-of-ashes-bestiary-items/uUTQ7Z3rao6lhQYz.htm)|History of the Bumblebrashers in Isger|vacía|
|[vaRLfCxd4TnmCYFX.htm](age-of-ashes-bestiary-items/vaRLfCxd4TnmCYFX.htm)|Droskar Lore|vacía|
|[VbtHhiKtz4qjoR6z.htm](age-of-ashes-bestiary-items/VbtHhiKtz4qjoR6z.htm)|Stealth|vacía|
|[vMtSSRTHSKwtSssM.htm](age-of-ashes-bestiary-items/vMtSSRTHSKwtSssM.htm)|Ekujae Lore|vacía|
|[WDdm6RT885xA9FVZ.htm](age-of-ashes-bestiary-items/WDdm6RT885xA9FVZ.htm)|Kintargo Lore|vacía|
|[wlSd81ltH4W9B3X7.htm](age-of-ashes-bestiary-items/wlSd81ltH4W9B3X7.htm)|Slaver Lore|vacía|
|[wrMmoUVK16gf58er.htm](age-of-ashes-bestiary-items/wrMmoUVK16gf58er.htm)|Acrobatics|vacía|
|[WslAyAlavrAXyhAt.htm](age-of-ashes-bestiary-items/WslAyAlavrAXyhAt.htm)|Syringe Filled with Unstable Mutagen|vacía|
|[WuZqaLIPaZyuIDMD.htm](age-of-ashes-bestiary-items/WuZqaLIPaZyuIDMD.htm)|Key to Manacles|vacía|
|[x15lr82vvdm9m35m.htm](age-of-ashes-bestiary-items/x15lr82vvdm9m35m.htm)|Warhammer|vacía|
|[x7Maw7sCvZ5bEGPz.htm](age-of-ashes-bestiary-items/x7Maw7sCvZ5bEGPz.htm)|Hermea Lore|vacía|
|[YCQ2DWy0a4bzK0Hb.htm](age-of-ashes-bestiary-items/YCQ2DWy0a4bzK0Hb.htm)|Crafting|vacía|
|[YdZT3Ed6Vo7vzA6W.htm](age-of-ashes-bestiary-items/YdZT3Ed6Vo7vzA6W.htm)|Gunpower|vacía|
|[YlxLNSbifo76lEgK.htm](age-of-ashes-bestiary-items/YlxLNSbifo76lEgK.htm)|Dragon Lore|vacía|
|[YW9ZoJ6fNOB1slcu.htm](age-of-ashes-bestiary-items/YW9ZoJ6fNOB1slcu.htm)|Dragon Lore|vacía|
|[ZJpZacKciS27DjVJ.htm](age-of-ashes-bestiary-items/ZJpZacKciS27DjVJ.htm)|Monocle|vacía|
|[zkT4pDnggpJK1FkR.htm](age-of-ashes-bestiary-items/zkT4pDnggpJK1FkR.htm)|Key to the Temple of All Gods|vacía|
|[ZRoLOOZe4nN8DGRf.htm](age-of-ashes-bestiary-items/ZRoLOOZe4nN8DGRf.htm)|Mercantile Lore|vacía|
|[ZvcUmXAhxPGfnsNc.htm](age-of-ashes-bestiary-items/ZvcUmXAhxPGfnsNc.htm)|Spider Lore|vacía|
|[zWqQgevLJLJ7zv0x.htm](age-of-ashes-bestiary-items/zWqQgevLJLJ7zv0x.htm)|Mercantile Lore|vacía|
|[zZnaIccm0OeZf8hn.htm](age-of-ashes-bestiary-items/zZnaIccm0OeZf8hn.htm)|Hermea Lore|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
