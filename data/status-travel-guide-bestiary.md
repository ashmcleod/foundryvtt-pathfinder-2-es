# Estado de la traducción (travel-guide-bestiary)

 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[yRhCsQsoz1Uqvkmk.htm](travel-guide-bestiary/yRhCsQsoz1Uqvkmk.htm)|Vulture Rat|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
