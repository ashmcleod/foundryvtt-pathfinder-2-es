# Estado de la traducción (pathfinder-bestiary-2)

 * **modificada**: 373


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0rhltfBkQtYkCwXd.htm](pathfinder-bestiary-2/0rhltfBkQtYkCwXd.htm)|Yellow Musk Thrall|Siervo almizcle amarillo|modificada|
|[10JbkieWjqKA5Hzc.htm](pathfinder-bestiary-2/10JbkieWjqKA5Hzc.htm)|Irlgaunt|Irlgaunt|modificada|
|[12vyw160K7p3M3Mt.htm](pathfinder-bestiary-2/12vyw160K7p3M3Mt.htm)|Quetz Couatl|Quetz Couatl|modificada|
|[18NInoyRLIrWE0nt.htm](pathfinder-bestiary-2/18NInoyRLIrWE0nt.htm)|Chernobue|Chernobue|modificada|
|[1QWZDaBoefL5rWkz.htm](pathfinder-bestiary-2/1QWZDaBoefL5rWkz.htm)|Hodag|Hodag|modificada|
|[27R4ntwgch7eIQM6.htm](pathfinder-bestiary-2/27R4ntwgch7eIQM6.htm)|Akizendri|Akizendri|modificada|
|[2Kw49I6EZbKKlTtK.htm](pathfinder-bestiary-2/2Kw49I6EZbKKlTtK.htm)|Blizzardborn|Nacido de la ventisca|modificada|
|[32HHvah2Ji6FjJnM.htm](pathfinder-bestiary-2/32HHvah2Ji6FjJnM.htm)|Giant Squid|Calamar gigante|modificada|
|[340AwQpRXGblw1kF.htm](pathfinder-bestiary-2/340AwQpRXGblw1kF.htm)|Sarglagon|Sarglagon|modificada|
|[37FAi1y5S8snofws.htm](pathfinder-bestiary-2/37FAi1y5S8snofws.htm)|Olethrodaemon|Olethrodaemon|modificada|
|[3dlupMwDA9qv1xWS.htm](pathfinder-bestiary-2/3dlupMwDA9qv1xWS.htm)|Suli Dune Dancer|Suli Dune Dancer|modificada|
|[3DSqfobadad3pA6O.htm](pathfinder-bestiary-2/3DSqfobadad3pA6O.htm)|Giant Fly|Mosca Gigante|modificada|
|[3MDlKPlPhHBo1DId.htm](pathfinder-bestiary-2/3MDlKPlPhHBo1DId.htm)|Vampire Squid|Calamar Vampiro|modificada|
|[3u4toki9EeSRvcs2.htm](pathfinder-bestiary-2/3u4toki9EeSRvcs2.htm)|Quoppopak|Quoppopak|modificada|
|[3VFHZyjSjNA1jbiO.htm](pathfinder-bestiary-2/3VFHZyjSjNA1jbiO.htm)|Sportlebore Swarm|Enjambre Sportlebore|modificada|
|[3YcsuATyahEMygNy.htm](pathfinder-bestiary-2/3YcsuATyahEMygNy.htm)|Revenant|Revenant|modificada|
|[48agy7vl9fQW1LLA.htm](pathfinder-bestiary-2/48agy7vl9fQW1LLA.htm)|Draugr|Draugr|modificada|
|[49y0g521xHESClzk.htm](pathfinder-bestiary-2/49y0g521xHESClzk.htm)|Young Magma Dragon|Joven Dragón de Magma|modificada|
|[4x9trDSABU5dqq9w.htm](pathfinder-bestiary-2/4x9trDSABU5dqq9w.htm)|Blue Dracolisk|Dracolisk azul|modificada|
|[4Y8UO8NloCoPxJ1E.htm](pathfinder-bestiary-2/4Y8UO8NloCoPxJ1E.htm)|Umonlee|Umonlee|modificada|
|[57pzXwM9qFiRTwq9.htm](pathfinder-bestiary-2/57pzXwM9qFiRTwq9.htm)|Dig-Widget|Artilugio excavador|modificada|
|[5CixyyBkCLj2VI8q.htm](pathfinder-bestiary-2/5CixyyBkCLj2VI8q.htm)|Hippopotamus|Hipopótamo|modificada|
|[5FcYbQxHehjO4w1d.htm](pathfinder-bestiary-2/5FcYbQxHehjO4w1d.htm)|Ooze Mephit|Ooze Mephit|modificada|
|[5FGuXfg6Ub2hCBG7.htm](pathfinder-bestiary-2/5FGuXfg6Ub2hCBG7.htm)|Yellow Musk Creeper|Enredadera Almizclera Amarilla|modificada|
|[5l0b80kz53Ie26gq.htm](pathfinder-bestiary-2/5l0b80kz53Ie26gq.htm)|Wood Giant|Gigante de Madera|modificada|
|[5l3surWoPHTgPoWY.htm](pathfinder-bestiary-2/5l3surWoPHTgPoWY.htm)|Petitioner (Abaddon)|Peticionario (Abaddon)|modificada|
|[5QmDXmgNGBmEhQd9.htm](pathfinder-bestiary-2/5QmDXmgNGBmEhQd9.htm)|Adult Crystal Dragon (Spellcaster)|Dragón de Cristal Adulto (Hechicero)|modificada|
|[64sdDzjLWu2W2R2e.htm](pathfinder-bestiary-2/64sdDzjLWu2W2R2e.htm)|Water Orm|Orm acuático|modificada|
|[6EX8nxH3zRotLRJG.htm](pathfinder-bestiary-2/6EX8nxH3zRotLRJG.htm)|Sea Snake|Culebra marina|modificada|
|[6FltuGxvUoNH9b17.htm](pathfinder-bestiary-2/6FltuGxvUoNH9b17.htm)|Petitioner (Plane of Air)|Peticionario (Plano del Aire)|modificada|
|[6ic6oNCpWjkVAsIN.htm](pathfinder-bestiary-2/6ic6oNCpWjkVAsIN.htm)|Ember Fox|Zorro de ascuas|modificada|
|[6jqnHzjmJ190G8Zj.htm](pathfinder-bestiary-2/6jqnHzjmJ190G8Zj.htm)|Yeth Hound|Yeth Hound|modificada|
|[6Me4Q1JDupyCCnGg.htm](pathfinder-bestiary-2/6Me4Q1JDupyCCnGg.htm)|Ice Mephit|Mephit de hielo|modificada|
|[6tKVMKDdvbp0VhZD.htm](pathfinder-bestiary-2/6tKVMKDdvbp0VhZD.htm)|Slime Mold|Moho de baba|modificada|
|[6wpHJXze0RLxl780.htm](pathfinder-bestiary-2/6wpHJXze0RLxl780.htm)|Behir|Behir|modificada|
|[6xioRweZ0T4o4X9e.htm](pathfinder-bestiary-2/6xioRweZ0T4o4X9e.htm)|Witchfire|Fuego de bruja|modificada|
|[6YOyTHRGqkYl2Prb.htm](pathfinder-bestiary-2/6YOyTHRGqkYl2Prb.htm)|Wolverine|Wolverine|modificada|
|[6ZLs4H4gjVyb3yV7.htm](pathfinder-bestiary-2/6ZLs4H4gjVyb3yV7.htm)|Petitioner (Elysium)|Peticionario (Elysium)|modificada|
|[73qJxJK8ES4Tbp2k.htm](pathfinder-bestiary-2/73qJxJK8ES4Tbp2k.htm)|Lurker In Light|Lurker En Luz|modificada|
|[7EppoGdIcmd0AV9B.htm](pathfinder-bestiary-2/7EppoGdIcmd0AV9B.htm)|Onidoshi|Onidoshi|modificada|
|[7lL7APeHQgkVxU1B.htm](pathfinder-bestiary-2/7lL7APeHQgkVxU1B.htm)|Fire Jellyfish Swarm|Enjambre de medusas de fuego|modificada|
|[7oiIDnLBDpC2r2AA.htm](pathfinder-bestiary-2/7oiIDnLBDpC2r2AA.htm)|Sacristan|Sacristán|modificada|
|[7SVhYtnBn967Hy8O.htm](pathfinder-bestiary-2/7SVhYtnBn967Hy8O.htm)|Bone Prophet|Profeta Hueso|modificada|
|[7UBCht1tQfeba2Ih.htm](pathfinder-bestiary-2/7UBCht1tQfeba2Ih.htm)|Wood Golem|Golem de madera|modificada|
|[89isDKkB1UCbsSvU.htm](pathfinder-bestiary-2/89isDKkB1UCbsSvU.htm)|Ostiarius|Ostiarius|modificada|
|[8a9N2iPA5HLpkWaF.htm](pathfinder-bestiary-2/8a9N2iPA5HLpkWaF.htm)|Army Ant Swarm|Army Ant Swarm|modificada|
|[8HJNH3aDTJyJi3pU.htm](pathfinder-bestiary-2/8HJNH3aDTJyJi3pU.htm)|Cockroach Swarm|Enjambre de cucarachas|modificada|
|[8IyfadYYpYXBvlXN.htm](pathfinder-bestiary-2/8IyfadYYpYXBvlXN.htm)|Vrykolakas Spawn|Vrykolakas Spawn|modificada|
|[8MQXM56Rm0rmzaxY.htm](pathfinder-bestiary-2/8MQXM56Rm0rmzaxY.htm)|Ancient Cloud Dragon (Spellcaster)|Ancient Cloud Dragon (Hechicero)|modificada|
|[8usfZlFqdD9cchPJ.htm](pathfinder-bestiary-2/8usfZlFqdD9cchPJ.htm)|Bottlenose Dolphin|Delfín mular|modificada|
|[8WAc7n2M4Ks00yvj.htm](pathfinder-bestiary-2/8WAc7n2M4Ks00yvj.htm)|Cavern Troll|Cavern Troll|modificada|
|[9sUx2sV4E7Sy3vJT.htm](pathfinder-bestiary-2/9sUx2sV4E7Sy3vJT.htm)|Zelekhut|Zelekhut|modificada|
|[a6TY4fgTz5MbpkRR.htm](pathfinder-bestiary-2/a6TY4fgTz5MbpkRR.htm)|Young Magma Dragon (Spellcaster)|Joven Dragón de Magma (Hechicero)|modificada|
|[a8Ih1RIr4UUV4QCL.htm](pathfinder-bestiary-2/a8Ih1RIr4UUV4QCL.htm)|Fire Yai|Fuego Yai|modificada|
|[Aa652cDdKTani3DY.htm](pathfinder-bestiary-2/Aa652cDdKTani3DY.htm)|Scythe Tree|Árbol de la guadaña|modificada|
|[aATQbGnSA0KOWG53.htm](pathfinder-bestiary-2/aATQbGnSA0KOWG53.htm)|Anancus|Anancus|modificada|
|[ABMn623izSLHA0sx.htm](pathfinder-bestiary-2/ABMn623izSLHA0sx.htm)|Glass Golem|Golem de cristal|modificada|
|[ABrzFoOqQohQqU6C.htm](pathfinder-bestiary-2/ABrzFoOqQohQqU6C.htm)|Yamaraj|Yamaraj|modificada|
|[ACIqdW6VsFA1TCpN.htm](pathfinder-bestiary-2/ACIqdW6VsFA1TCpN.htm)|Manta Ray|Manta Ray|modificada|
|[aFRGgQWWCDIiBlUl.htm](pathfinder-bestiary-2/aFRGgQWWCDIiBlUl.htm)|D'ziriak|D'ziriak|modificada|
|[aMIb3Xm64NzRj8YP.htm](pathfinder-bestiary-2/aMIb3Xm64NzRj8YP.htm)|Mandragora|Mandragora|modificada|
|[ARw4pyNmuQxoZDke.htm](pathfinder-bestiary-2/ARw4pyNmuQxoZDke.htm)|Steam Mephit|Steam Mephit|modificada|
|[aVf2IojxU9ZZ64M5.htm](pathfinder-bestiary-2/aVf2IojxU9ZZ64M5.htm)|Fetchling Scout|Explorador Fetchling|modificada|
|[b0jMGY65XVyuKUjf.htm](pathfinder-bestiary-2/b0jMGY65XVyuKUjf.htm)|Blink Dog|Perro intermitente|modificada|
|[B4TJHP0yX8SXHQ4x.htm](pathfinder-bestiary-2/B4TJHP0yX8SXHQ4x.htm)|Trumpet Archon|Trompeta Archon|modificada|
|[b5b6J3TqbcXnmKyy.htm](pathfinder-bestiary-2/b5b6J3TqbcXnmKyy.htm)|Giant Dragonfly|Libélula Gigante|modificada|
|[B8dOh27iarR7rZI3.htm](pathfinder-bestiary-2/B8dOh27iarR7rZI3.htm)|Blindheim|Blindheim|modificada|
|[bAaVfrdNLfFh7iHv.htm](pathfinder-bestiary-2/bAaVfrdNLfFh7iHv.htm)|Basidirond|Basidirond|modificada|
|[BbIyNJDPkGKLK0Rt.htm](pathfinder-bestiary-2/BbIyNJDPkGKLK0Rt.htm)|Vexgit|Vexgit|modificada|
|[BE5GH4dwZ76qV7rH.htm](pathfinder-bestiary-2/BE5GH4dwZ76qV7rH.htm)|Petitioner (Maelstrom)|Peticionario (Maelstrom)|modificada|
|[BJoM8LAVP8MA5o8v.htm](pathfinder-bestiary-2/BJoM8LAVP8MA5o8v.htm)|Catoblepas|Catoblepas|modificada|
|[BNizhfieKEditLGb.htm](pathfinder-bestiary-2/BNizhfieKEditLGb.htm)|Petitioner (Plane of Fire)|Peticionario (Plano del Fuego)|modificada|
|[bpwaqRoO1wYo15T5.htm](pathfinder-bestiary-2/bpwaqRoO1wYo15T5.htm)|Nuglub|Nuglub|modificada|
|[BSlvgDdLkTXU9hRF.htm](pathfinder-bestiary-2/BSlvgDdLkTXU9hRF.htm)|Ancient Umbral Dragon|Dragón Umbral Antiguo|modificada|
|[BTQ2ARKulg5Egueg.htm](pathfinder-bestiary-2/BTQ2ARKulg5Egueg.htm)|Augur|Augur|modificada|
|[Bz5wgKEVsHAyaYEx.htm](pathfinder-bestiary-2/Bz5wgKEVsHAyaYEx.htm)|Sunflower Leshy|Leshy de los girasoles|modificada|
|[c4uQKZaRWv9n9IHo.htm](pathfinder-bestiary-2/c4uQKZaRWv9n9IHo.htm)|Solar|Solar|modificada|
|[C7LvY2Xxzm2xRL1M.htm](pathfinder-bestiary-2/C7LvY2Xxzm2xRL1M.htm)|Giant Solifugid|Solífugo gigante|modificada|
|[cd2IaNGpdmHHZnrN.htm](pathfinder-bestiary-2/cd2IaNGpdmHHZnrN.htm)|Xill|Xill|modificada|
|[cijMuXBk8YUw2RoB.htm](pathfinder-bestiary-2/cijMuXBk8YUw2RoB.htm)|Caligni Slayer|Caligni Slayer (p.-s)|modificada|
|[cKu0rrZtEAFCosej.htm](pathfinder-bestiary-2/cKu0rrZtEAFCosej.htm)|Void Zombie|Void Zombie|modificada|
|[cmuCFkXdn1JuVTK4.htm](pathfinder-bestiary-2/cmuCFkXdn1JuVTK4.htm)|Gorgon|Gorgona|modificada|
|[coNTMrHerjBIf2ig.htm](pathfinder-bestiary-2/coNTMrHerjBIf2ig.htm)|Culdewen|Culdewen|modificada|
|[cPX0NmB242FqSRxa.htm](pathfinder-bestiary-2/cPX0NmB242FqSRxa.htm)|Grendel|Grendel|modificada|
|[CrAsEzLl3F2vejd3.htm](pathfinder-bestiary-2/CrAsEzLl3F2vejd3.htm)|Earthen Destrier|Destrero de tierra|modificada|
|[cu6Bl5pFByrpinxd.htm](pathfinder-bestiary-2/cu6Bl5pFByrpinxd.htm)|Planetar|Planetar|modificada|
|[CvO6MAoxDFO0J5qm.htm](pathfinder-bestiary-2/CvO6MAoxDFO0J5qm.htm)|Witchwyrd|Witchwyrd|modificada|
|[CwrVQsRAeqlr1Vh0.htm](pathfinder-bestiary-2/CwrVQsRAeqlr1Vh0.htm)|Athach|Athach|modificada|
|[dBUiB3Hyh0hN8Exc.htm](pathfinder-bestiary-2/dBUiB3Hyh0hN8Exc.htm)|Woolly Rhinoceros|Rinoceronte lanudo|modificada|
|[DC9b8mG1KdyXZkLp.htm](pathfinder-bestiary-2/DC9b8mG1KdyXZkLp.htm)|Calathgar|Calathgar|modificada|
|[DEo5AEY1i9s0kWYZ.htm](pathfinder-bestiary-2/DEo5AEY1i9s0kWYZ.htm)|Lunar Naga|Naga Lunar|modificada|
|[DM5js1XSiuiPAf3t.htm](pathfinder-bestiary-2/DM5js1XSiuiPAf3t.htm)|White Dracolisk|Dracolisk blanco|modificada|
|[drtCZDDoESPSUqH7.htm](pathfinder-bestiary-2/drtCZDDoESPSUqH7.htm)|Peluda|Peluda|modificada|
|[duaHanGHvRGhUtB5.htm](pathfinder-bestiary-2/duaHanGHvRGhUtB5.htm)|Viper Vine|Viper Vine|modificada|
|[dvZM7hGNDu8olV7A.htm](pathfinder-bestiary-2/dvZM7hGNDu8olV7A.htm)|Brood Leech Swarm|Plaga de crías de sanguijuela|modificada|
|[DWEk6i42qAhckgqF.htm](pathfinder-bestiary-2/DWEk6i42qAhckgqF.htm)|Ancient Crystal Dragon (Spellcaster)|Antiguo Dragón de Cristal (Hechicero)|modificada|
|[DWQzgPL1WjXfu4rV.htm](pathfinder-bestiary-2/DWQzgPL1WjXfu4rV.htm)|Assassin Vine|Enredadera Asesina|modificada|
|[dwyt7e1EZPjw9KBB.htm](pathfinder-bestiary-2/dwyt7e1EZPjw9KBB.htm)|Giant Hippocampus|Hipocampo gigante|modificada|
|[DxV2vH17H789bJ6y.htm](pathfinder-bestiary-2/DxV2vH17H789bJ6y.htm)|Ankou|Ankou|modificada|
|[DYBZniwR1Ip4inVr.htm](pathfinder-bestiary-2/DYBZniwR1Ip4inVr.htm)|Morlock|Morlock|modificada|
|[e0PXAtnDnJo4ggFX.htm](pathfinder-bestiary-2/e0PXAtnDnJo4ggFX.htm)|Vrykolakas Ancient|Antiguo brucolaco|modificada|
|[e0YvEWJ5zpU946G4.htm](pathfinder-bestiary-2/e0YvEWJ5zpU946G4.htm)|Cave Scorpion|Escorpión de las cavernas|modificada|
|[E4aJhHWyFDxgJcy9.htm](pathfinder-bestiary-2/E4aJhHWyFDxgJcy9.htm)|Peryton|Peryton|modificada|
|[e5yE4RkwLQojRYHm.htm](pathfinder-bestiary-2/e5yE4RkwLQojRYHm.htm)|Urdefhan Warrior|Guerrero Urdefhan|modificada|
|[eD1kydftMIp4CL2K.htm](pathfinder-bestiary-2/eD1kydftMIp4CL2K.htm)|Sard|Sard|modificada|
|[EKbnknpDZEMkZSF4.htm](pathfinder-bestiary-2/EKbnknpDZEMkZSF4.htm)|Petitioner (Positive Energy Plane)|Peticionario (Plano de energía positiva)|modificada|
|[elb0mL1cfknHxskr.htm](pathfinder-bestiary-2/elb0mL1cfknHxskr.htm)|Petitioner (Nirvana)|Peticionario (Nirvana)|modificada|
|[EOiOqs1vHCxZAj3T.htm](pathfinder-bestiary-2/EOiOqs1vHCxZAj3T.htm)|Scarecrow|Espantapájaros|modificada|
|[ePUTKSr45H7S5dQs.htm](pathfinder-bestiary-2/ePUTKSr45H7S5dQs.htm)|Veranallia|Veranallia|modificada|
|[Et2784TVZOXIbtM1.htm](pathfinder-bestiary-2/Et2784TVZOXIbtM1.htm)|Interlocutor|Interlocutor|modificada|
|[EWoJAp5LJeL1Qfye.htm](pathfinder-bestiary-2/EWoJAp5LJeL1Qfye.htm)|Giant Frog|Rana gigante|modificada|
|[EXeR2HC4xsvHcf6K.htm](pathfinder-bestiary-2/EXeR2HC4xsvHcf6K.htm)|Monadic Deva|Deva monádica|modificada|
|[F5QibbbxUeO8A3Kj.htm](pathfinder-bestiary-2/F5QibbbxUeO8A3Kj.htm)|Grimstalker|Grimstalker|modificada|
|[FERUCyMwvy4Nejou.htm](pathfinder-bestiary-2/FERUCyMwvy4Nejou.htm)|Hellcat|Hellcat|modificada|
|[FHnWzK7DsniqZWzG.htm](pathfinder-bestiary-2/FHnWzK7DsniqZWzG.htm)|Nereid|Nereid|modificada|
|[fizcFuAe2WPauA2U.htm](pathfinder-bestiary-2/fizcFuAe2WPauA2U.htm)|Petitioner (Boneyard)|Peticionario (Boneyard)|modificada|
|[FJr7Rc9MQEwxjAcz.htm](pathfinder-bestiary-2/FJr7Rc9MQEwxjAcz.htm)|Urdefhan Tormentor|Urdefhan Tormentor|modificada|
|[FMU6L5vbiUxa1ZF7.htm](pathfinder-bestiary-2/FMU6L5vbiUxa1ZF7.htm)|Kelpie|Kelpie|modificada|
|[FnLuqlzLl4zCE9pJ.htm](pathfinder-bestiary-2/FnLuqlzLl4zCE9pJ.htm)|Adult Magma Dragon|Dragón de Magma adulto|modificada|
|[fpBJiCOoHDlefgpO.htm](pathfinder-bestiary-2/fpBJiCOoHDlefgpO.htm)|Thunderbird|Thunderbird|modificada|
|[fRLrlY25qXkOMBNG.htm](pathfinder-bestiary-2/fRLrlY25qXkOMBNG.htm)|Nuckelavee|Nuckelavee|modificada|
|[FRTpT9rLxUBKGdin.htm](pathfinder-bestiary-2/FRTpT9rLxUBKGdin.htm)|Star Archon|Arconte estelar|modificada|
|[fv91xoQJlogVbruW.htm](pathfinder-bestiary-2/fv91xoQJlogVbruW.htm)|Aapoph Serpentfolk|Aapoph Serpentfolk|modificada|
|[fVvRASaKa6SkQkYG.htm](pathfinder-bestiary-2/fVvRASaKa6SkQkYG.htm)|Giant Snapping Turtle|Tortuga mordedora gigante|modificada|
|[fwjDIGzmuEJN81Qq.htm](pathfinder-bestiary-2/fwjDIGzmuEJN81Qq.htm)|Trollhound|Trollhound|modificada|
|[g1htYgARxNKxc7MV.htm](pathfinder-bestiary-2/g1htYgARxNKxc7MV.htm)|Giant Slug|Babosa gigante|modificada|
|[g41X4b3q2iDMLRt3.htm](pathfinder-bestiary-2/g41X4b3q2iDMLRt3.htm)|Ravener|Ravener|modificada|
|[GBBVBEAsznP8A5gv.htm](pathfinder-bestiary-2/GBBVBEAsznP8A5gv.htm)|Leydroth|Leydroth|modificada|
|[Ge5Q5I7TTksf7QyN.htm](pathfinder-bestiary-2/Ge5Q5I7TTksf7QyN.htm)|Zomok|Zomok|modificada|
|[GfJLotTcrZmn6tay.htm](pathfinder-bestiary-2/GfJLotTcrZmn6tay.htm)|Purrodaemon|Purrodaemon|modificada|
|[GnKay44MFMZkqXRi.htm](pathfinder-bestiary-2/GnKay44MFMZkqXRi.htm)|Hadrosaurid|Hadrosaurid|modificada|
|[gOjrie9YndQPomU8.htm](pathfinder-bestiary-2/gOjrie9YndQPomU8.htm)|Sand Sentry|Centinela de la arena|modificada|
|[GsrqCDf0Dc9PisEt.htm](pathfinder-bestiary-2/GsrqCDf0Dc9PisEt.htm)|Violet Fungus|Hongo violeta|modificada|
|[GvjCh1Q6NEXnBHcD.htm](pathfinder-bestiary-2/GvjCh1Q6NEXnBHcD.htm)|Green Dracolisk|Dracolisk verde|modificada|
|[gVNKimslRe1k2hdV.htm](pathfinder-bestiary-2/gVNKimslRe1k2hdV.htm)|Vaspercham|Vaspercham|modificada|
|[H37tfTiLiUhpYxaS.htm](pathfinder-bestiary-2/H37tfTiLiUhpYxaS.htm)|Bastion Archon|Arconte baluarconte|modificada|
|[h4SZ3U9at7aT0ASv.htm](pathfinder-bestiary-2/h4SZ3U9at7aT0ASv.htm)|Spirit Naga|Espíritu Naga|modificada|
|[H8KNSMb9uo2mJF3P.htm](pathfinder-bestiary-2/H8KNSMb9uo2mJF3P.htm)|Twigjack|Twigjack|modificada|
|[HBkk9MYLomjKDKr4.htm](pathfinder-bestiary-2/HBkk9MYLomjKDKr4.htm)|Azuretzi|Azuretzi|modificada|
|[hFbfXmjMkGEzMkQ1.htm](pathfinder-bestiary-2/hFbfXmjMkGEzMkQ1.htm)|Raven|Cuervo|modificada|
|[HhSoTr6mpwV09Y4k.htm](pathfinder-bestiary-2/HhSoTr6mpwV09Y4k.htm)|Irnakurse|Irnakurse|modificada|
|[HI7SecILsRTmOtky.htm](pathfinder-bestiary-2/HI7SecILsRTmOtky.htm)|Shocker Lizard|Lagarto de choque|modificada|
|[hIWzgjW2E2K6ImPA.htm](pathfinder-bestiary-2/hIWzgjW2E2K6ImPA.htm)|Bog Mummy|Momia del pantano|modificada|
|[HjmqeJuWdmY5imiz.htm](pathfinder-bestiary-2/HjmqeJuWdmY5imiz.htm)|Triton|Triton|modificada|
|[HmeEHIXL46rBQWAa.htm](pathfinder-bestiary-2/HmeEHIXL46rBQWAa.htm)|Frost Troll|Gélida Troll|modificada|
|[hmnNDCNV425MisdA.htm](pathfinder-bestiary-2/hmnNDCNV425MisdA.htm)|Eremite|Eremita|modificada|
|[HNEUSYnInffvGPHK.htm](pathfinder-bestiary-2/HNEUSYnInffvGPHK.htm)|Specter|Espectro|modificada|
|[Ho1Dx2dXlQZYLApP.htm](pathfinder-bestiary-2/Ho1Dx2dXlQZYLApP.htm)|Sylph Sneak|Sylph Movimiento furtivo|modificada|
|[hvKRgNiQtQJhHk9u.htm](pathfinder-bestiary-2/hvKRgNiQtQJhHk9u.htm)|Imentesh|Imentesh|modificada|
|[hvoIxruNUei1auQT.htm](pathfinder-bestiary-2/hvoIxruNUei1auQT.htm)|Melody On The Wind|Melodía arrastrada por el viento|modificada|
|[Hwy4YrNVVr7NnsMC.htm](pathfinder-bestiary-2/Hwy4YrNVVr7NnsMC.htm)|Mudwretch|Mudwretch|modificada|
|[HXWT5QxmOCn0poMt.htm](pathfinder-bestiary-2/HXWT5QxmOCn0poMt.htm)|Stingray|Stingray|modificada|
|[HzeOxVy3VXxaJmC5.htm](pathfinder-bestiary-2/HzeOxVy3VXxaJmC5.htm)|Adult Umbral Dragon|Dragón Umbral Adulto|modificada|
|[i8C4jqI3VdozylBL.htm](pathfinder-bestiary-2/i8C4jqI3VdozylBL.htm)|Cornugon|Cornugon|modificada|
|[iLZpGlyBWxS4Idbi.htm](pathfinder-bestiary-2/iLZpGlyBWxS4Idbi.htm)|Emperor Cobra|Emperador Cobra|modificada|
|[iW6WKMVV3Ug8sa2q.htm](pathfinder-bestiary-2/iW6WKMVV3Ug8sa2q.htm)|Thanadaemon|Thanadaemon|modificada|
|[iZ3B52CYBCfqAaOo.htm](pathfinder-bestiary-2/iZ3B52CYBCfqAaOo.htm)|Adult Magma Dragon (Spellcaster)|Dragón de Magma Adulto (Hechicero)|modificada|
|[j43O2vBu8eu2wC5z.htm](pathfinder-bestiary-2/j43O2vBu8eu2wC5z.htm)|Vrykolakas Master|Maestro brucolaco|modificada|
|[jCF9HjF42SPXTptP.htm](pathfinder-bestiary-2/jCF9HjF42SPXTptP.htm)|Marrmora|Marrmora|modificada|
|[jCJUvzs1FaL6PAkq.htm](pathfinder-bestiary-2/jCJUvzs1FaL6PAkq.htm)|Bodak|Bodak|modificada|
|[JCoILqEvwovT7tkj.htm](pathfinder-bestiary-2/JCoILqEvwovT7tkj.htm)|Chupacabra|Chupacabra|modificada|
|[JgePINrFINmlsvn3.htm](pathfinder-bestiary-2/JgePINrFINmlsvn3.htm)|Hound Of Tindalos|Sabueso De Tindalos|modificada|
|[jgMFqFDNUWsrnnBH.htm](pathfinder-bestiary-2/jgMFqFDNUWsrnnBH.htm)|Tick Swarm|Enjambre de garrapatas|modificada|
|[jhr6WpTeY8GXXwK1.htm](pathfinder-bestiary-2/jhr6WpTeY8GXXwK1.htm)|Ifrit Pyrochemist|Piroalquímico efrítido de Ifrit|modificada|
|[JJKxWVExhUEno9j0.htm](pathfinder-bestiary-2/JJKxWVExhUEno9j0.htm)|Giant Leech|Sanguijuela gigante|modificada|
|[JKBO2qqNrRezSb7d.htm](pathfinder-bestiary-2/JKBO2qqNrRezSb7d.htm)|Ancient Magma Dragon|Dragón de Magma Antiguo|modificada|
|[jpjhbGvaduK4iqwq.htm](pathfinder-bestiary-2/jpjhbGvaduK4iqwq.htm)|Spark Bat|Murciélago de chispas|modificada|
|[jQ7zBVDCEhuMOOTl.htm](pathfinder-bestiary-2/jQ7zBVDCEhuMOOTl.htm)|Nabasu|Nabasu|modificada|
|[JqnzS6RhUnOjnUdb.htm](pathfinder-bestiary-2/JqnzS6RhUnOjnUdb.htm)|Adult Cloud Dragon (Spellcaster)|Dragón de las Nubes Adulto (Hechicero)|modificada|
|[kapPWNOxXOLcd4VF.htm](pathfinder-bestiary-2/kapPWNOxXOLcd4VF.htm)|Amoeba Swarm|Enjambre de amebas|modificada|
|[kElZCC1zMQVftoi9.htm](pathfinder-bestiary-2/kElZCC1zMQVftoi9.htm)|Isqulug|Isqulug|modificada|
|[kIzBE6VaHQbJ3kdx.htm](pathfinder-bestiary-2/kIzBE6VaHQbJ3kdx.htm)|Red Dracolisk|Dracolisco rojo|modificada|
|[kKH3uMHbVZQmEtxj.htm](pathfinder-bestiary-2/kKH3uMHbVZQmEtxj.htm)|Jotund Troll|Jotund Troll|modificada|
|[Km07kb7vPbSXO7EL.htm](pathfinder-bestiary-2/Km07kb7vPbSXO7EL.htm)|Young Crystal Dragon (Spellcaster)|Joven Dragón de Cristal (Hechicero)|modificada|
|[Kr5LtF0bqZJAC2Wn.htm](pathfinder-bestiary-2/Kr5LtF0bqZJAC2Wn.htm)|Ogre Spider|Araña Ogro|modificada|
|[Kw4UUxt5rorsGjsK.htm](pathfinder-bestiary-2/Kw4UUxt5rorsGjsK.htm)|Yellow Musk Brute|Bruto de almizcle amarillo|modificada|
|[l3kFjH4n9Shet6bz.htm](pathfinder-bestiary-2/l3kFjH4n9Shet6bz.htm)|Fen Mosquito Swarm|Enjambre de mosquitos Fen|modificada|
|[l4UOJbphMT7jZ0xB.htm](pathfinder-bestiary-2/l4UOJbphMT7jZ0xB.htm)|Giant Crab|Cangrejo Gigante|modificada|
|[LcdQZ2NTG2eJswXA.htm](pathfinder-bestiary-2/LcdQZ2NTG2eJswXA.htm)|Sceaduinar|Sceaduinar|modificada|
|[le7VaOJyQQnl37Wa.htm](pathfinder-bestiary-2/le7VaOJyQQnl37Wa.htm)|Giant Tick|Garrapata gigante|modificada|
|[LQL8HU6tISXZwlOw.htm](pathfinder-bestiary-2/LQL8HU6tISXZwlOw.htm)|Skaveling|Skaveling|modificada|
|[LtaVzD7ADtWmBWap.htm](pathfinder-bestiary-2/LtaVzD7ADtWmBWap.htm)|Skrik Nettle|Ortiga Skrik|modificada|
|[luPebMfNmwtOZcWR.htm](pathfinder-bestiary-2/luPebMfNmwtOZcWR.htm)|Thrasfyr|Thrasfyr|modificada|
|[lUYlClIPITmhzepe.htm](pathfinder-bestiary-2/lUYlClIPITmhzepe.htm)|Two-Headed Troll|Troll de dos cabezas|modificada|
|[lvvLNyZpbb4Ydz3U.htm](pathfinder-bestiary-2/lvvLNyZpbb4Ydz3U.htm)|Petitioner (Axis)|Peticionario (Eje)|modificada|
|[LZp7LLcFzDBlrwod.htm](pathfinder-bestiary-2/LZp7LLcFzDBlrwod.htm)|Nixie|Nixie|modificada|
|[M2BkGD9YwEmCvD9D.htm](pathfinder-bestiary-2/M2BkGD9YwEmCvD9D.htm)|Invidiak|Invidiak|modificada|
|[m40M5R7wcm5DSVK2.htm](pathfinder-bestiary-2/m40M5R7wcm5DSVK2.htm)|Dust Mephit|Polvo Mephit|modificada|
|[M4mDroIJlrBYfHkM.htm](pathfinder-bestiary-2/M4mDroIJlrBYfHkM.htm)|Giant Crawling Hand|Mano reptante gigante|modificada|
|[MCZgXBC0I4Oovcjd.htm](pathfinder-bestiary-2/MCZgXBC0I4Oovcjd.htm)|Giant Wolverine|Lobezno Gigante|modificada|
|[MDdbC8XDUPFg4FCv.htm](pathfinder-bestiary-2/MDdbC8XDUPFg4FCv.htm)|Spriggan Warlord|Spriggan Warlord|modificada|
|[mpBOeFrbtx1wMrNn.htm](pathfinder-bestiary-2/mpBOeFrbtx1wMrNn.htm)|Frost Worm|Gusano de la escarcha|modificada|
|[MUVc2ZjwGtlpcuX1.htm](pathfinder-bestiary-2/MUVc2ZjwGtlpcuX1.htm)|Gosreg|Gosreg|modificada|
|[MYE3jqYh3CrdWolX.htm](pathfinder-bestiary-2/MYE3jqYh3CrdWolX.htm)|Petitioner (Heaven)|Peticionario (Cielo)|modificada|
|[mZLOTDHhChme6mfh.htm](pathfinder-bestiary-2/mZLOTDHhChme6mfh.htm)|Young Crystal Dragon|Joven Dragón de Cristal|modificada|
|[mZxVXRI1Zh3HUXxn.htm](pathfinder-bestiary-2/mZxVXRI1Zh3HUXxn.htm)|Living Thunderclap|Trueno viviente|modificada|
|[n1jTYFRCaqF1nQrf.htm](pathfinder-bestiary-2/n1jTYFRCaqF1nQrf.htm)|Titan Centipede|Titán Ciempiés|modificada|
|[N3XfcwDn1Nu7milG.htm](pathfinder-bestiary-2/N3XfcwDn1Nu7milG.htm)|Icewyrm|Icewyrm|modificada|
|[n6cv7eCzPSs4dbHn.htm](pathfinder-bestiary-2/n6cv7eCzPSs4dbHn.htm)|Dweomercat|Dweomercat|modificada|
|[N7Ej1AyPPolzboex.htm](pathfinder-bestiary-2/N7Ej1AyPPolzboex.htm)|Grindylow|Grindylow|modificada|
|[NAy7eUqFI8tGm3Ug.htm](pathfinder-bestiary-2/NAy7eUqFI8tGm3Ug.htm)|Taiga Linnorm|Linnorm de la taiga|modificada|
|[nipVBTEgG6itWJSW.htm](pathfinder-bestiary-2/nipVBTEgG6itWJSW.htm)|Shoal Linnorm|Linnorm del bajío|modificada|
|[Nk3Inn5jel1VQMLt.htm](pathfinder-bestiary-2/Nk3Inn5jel1VQMLt.htm)|Radiant Warden|Custodio radiante|modificada|
|[NlMqwcN2ZaALOVSY.htm](pathfinder-bestiary-2/NlMqwcN2ZaALOVSY.htm)|Carnivorous Blob|Carnivorous Blob|modificada|
|[nOAQehZL3045YMX1.htm](pathfinder-bestiary-2/nOAQehZL3045YMX1.htm)|Young Umbral Dragon|Dragón Umbral Joven|modificada|
|[NoCtPLRoVvloZe63.htm](pathfinder-bestiary-2/NoCtPLRoVvloZe63.htm)|Adult Cloud Dragon|Dragón Nube Adulto|modificada|
|[NQ6qRvpD9DIwVxt3.htm](pathfinder-bestiary-2/NQ6qRvpD9DIwVxt3.htm)|Vampiric Mist|Niebla Vampírica|modificada|
|[nRsH7unLIRWq0ZHC.htm](pathfinder-bestiary-2/nRsH7unLIRWq0ZHC.htm)|Akata|Akata|modificada|
|[nSDXAfFOF2gEKmkM.htm](pathfinder-bestiary-2/nSDXAfFOF2gEKmkM.htm)|Movanic Deva|Movanic Deva|modificada|
|[NtezcliwnH4R1tJw.htm](pathfinder-bestiary-2/NtezcliwnH4R1tJw.htm)|Thulgant|Thulgant|modificada|
|[nxR3nseHT01YmOQo.htm](pathfinder-bestiary-2/nxR3nseHT01YmOQo.htm)|Piscodaemon|Piscodaemon|modificada|
|[nzLGh9GKCk7imto3.htm](pathfinder-bestiary-2/nzLGh9GKCk7imto3.htm)|Blodeuwedd|Blodeuwedd|modificada|
|[o2IbtStBj11QCRvS.htm](pathfinder-bestiary-2/o2IbtStBj11QCRvS.htm)|Rhinoceros|Rinoceronte|modificada|
|[O69kXMo6shkA606k.htm](pathfinder-bestiary-2/O69kXMo6shkA606k.htm)|Choker|Gargantilla|modificada|
|[O9LzsedLTUVFtOix.htm](pathfinder-bestiary-2/O9LzsedLTUVFtOix.htm)|Black Scorpion|Escorpión negro|modificada|
|[OaKx1ZaU25i3cypS.htm](pathfinder-bestiary-2/OaKx1ZaU25i3cypS.htm)|Theletos|Theletos|modificada|
|[OaNtHZAraCrVLuSo.htm](pathfinder-bestiary-2/OaNtHZAraCrVLuSo.htm)|Mohrg|Mohrg|modificada|
|[OERK0GGnaEOthcdV.htm](pathfinder-bestiary-2/OERK0GGnaEOthcdV.htm)|Cave Fisher|Pescador de cuevas|modificada|
|[OFHr8UhsBOOgj0ef.htm](pathfinder-bestiary-2/OFHr8UhsBOOgj0ef.htm)|Intellect Devourer|Devorador de intelecto|modificada|
|[oKogG9AwUZ9iBsm3.htm](pathfinder-bestiary-2/oKogG9AwUZ9iBsm3.htm)|Derghodaemon|Derghodaemon|modificada|
|[oMZaSpBmWddKZ1KN.htm](pathfinder-bestiary-2/oMZaSpBmWddKZ1KN.htm)|Ancient Brine Dragon|Dragón de Salmuera Antiguo|modificada|
|[oSQ3Q9tzTohksAtf.htm](pathfinder-bestiary-2/oSQ3Q9tzTohksAtf.htm)|Crawling Hand|Mano rastrera|modificada|
|[OuimMwM3yFRy5tRK.htm](pathfinder-bestiary-2/OuimMwM3yFRy5tRK.htm)|Evangelist|Evangelista|modificada|
|[OVRvL7fKERLsM4KP.htm](pathfinder-bestiary-2/OVRvL7fKERLsM4KP.htm)|Grodair|Grodair|modificada|
|[p5ZGpvIfEiBgMG2f.htm](pathfinder-bestiary-2/p5ZGpvIfEiBgMG2f.htm)|Petitioner (Ethereal Plane)|Peticionario (Plano Etéreo)|modificada|
|[PAGytLw1QnAiNYC2.htm](pathfinder-bestiary-2/PAGytLw1QnAiNYC2.htm)|Coil Spy|Retorcer Espía|modificada|
|[PKAs8bMBMpnegmwq.htm](pathfinder-bestiary-2/PKAs8bMBMpnegmwq.htm)|Badger|Badger|modificada|
|[pqqe4IKAmUXdf7VM.htm](pathfinder-bestiary-2/pqqe4IKAmUXdf7VM.htm)|Adult Brine Dragon (Spellcaster)|Dragón de Salmuera Adulto (Hechicero)|modificada|
|[prNJZOU8tdRIZITT.htm](pathfinder-bestiary-2/prNJZOU8tdRIZITT.htm)|Doprillu|Doprillu|modificada|
|[pxlZ8QbxMK6b3l5w.htm](pathfinder-bestiary-2/pxlZ8QbxMK6b3l5w.htm)|Leng Spider|Araña Leng|modificada|
|[PXTQ8wTvnd5qF1hW.htm](pathfinder-bestiary-2/PXTQ8wTvnd5qF1hW.htm)|Adult Brine Dragon|Dragón de Salmuera Adulto|modificada|
|[q6FDEqLZQVjZfJ3x.htm](pathfinder-bestiary-2/q6FDEqLZQVjZfJ3x.htm)|Spiral Centurion|Centurión espiral|modificada|
|[qdlP3qTZtjzZXpmQ.htm](pathfinder-bestiary-2/qdlP3qTZtjzZXpmQ.htm)|Spriggan Bully|Spriggan Bully|modificada|
|[QHNXT5bZDqKRzwEF.htm](pathfinder-bestiary-2/QHNXT5bZDqKRzwEF.htm)|Spear Frog|Rana lanza|modificada|
|[qjAtPDIi7OyUGsNy.htm](pathfinder-bestiary-2/qjAtPDIi7OyUGsNy.htm)|Bythos|Bythos|modificada|
|[QLY246Z6hP41LnCF.htm](pathfinder-bestiary-2/QLY246Z6hP41LnCF.htm)|Belker|Belker|modificada|
|[QpTRWkbZ6k6xP4tR.htm](pathfinder-bestiary-2/QpTRWkbZ6k6xP4tR.htm)|Petitioner (Plane of Water)|Peticionario (Plano del Agua)|modificada|
|[QR3AC4mkpfz6KPSh.htm](pathfinder-bestiary-2/QR3AC4mkpfz6KPSh.htm)|Cythnigot|Cythnigot|modificada|
|[R0dyG06kbNYiCqRW.htm](pathfinder-bestiary-2/R0dyG06kbNYiCqRW.htm)|Water Yai|Agua Yai|modificada|
|[R0YrD0izubtQYbTU.htm](pathfinder-bestiary-2/R0YrD0izubtQYbTU.htm)|Leprechaun|Leprechaun|modificada|
|[r3yLNfqS00g8sdrT.htm](pathfinder-bestiary-2/r3yLNfqS00g8sdrT.htm)|Duneshaker Solifugid|Duneshaker Solifugid|modificada|
|[r5i3MkSNgNBYa5Cm.htm](pathfinder-bestiary-2/r5i3MkSNgNBYa5Cm.htm)|Filth Fire|Fuego inmundo|modificada|
|[r9L6tdT44kYeN1FV.htm](pathfinder-bestiary-2/r9L6tdT44kYeN1FV.htm)|Giant Chameleon|Camaleón gigante|modificada|
|[RFCxaIS4OkacCpxD.htm](pathfinder-bestiary-2/RFCxaIS4OkacCpxD.htm)|Froghemoth|Froghemoth|modificada|
|[ril4ohkYrf5mGwHO.htm](pathfinder-bestiary-2/ril4ohkYrf5mGwHO.htm)|Attic Whisperer|Susurrador del Ático|modificada|
|[RknMXlwJcbvGhk3n.htm](pathfinder-bestiary-2/RknMXlwJcbvGhk3n.htm)|Reef Octopus|Pulpo de arrecife|modificada|
|[rpq2rHlZjy5g31xo.htm](pathfinder-bestiary-2/rpq2rHlZjy5g31xo.htm)|Grippli Scout|Explorador Grippli|modificada|
|[RqABskvexmp5sT7X.htm](pathfinder-bestiary-2/RqABskvexmp5sT7X.htm)|Giant Cockroach|Cucaracha gigante|modificada|
|[RqcIiiNNEkF28ui2.htm](pathfinder-bestiary-2/RqcIiiNNEkF28ui2.htm)|Orca|Orca|modificada|
|[Rt3lbxJ4jcwGnpF3.htm](pathfinder-bestiary-2/Rt3lbxJ4jcwGnpF3.htm)|Petitioner (Plane of Earth)|Peticionario (Plano de la Tierra)|modificada|
|[RUdP7qWg6pnKrPnh.htm](pathfinder-bestiary-2/RUdP7qWg6pnKrPnh.htm)|Striding Fire|Zancada de Fuego|modificada|
|[rZdaxjM7CFVAEq2e.htm](pathfinder-bestiary-2/rZdaxjM7CFVAEq2e.htm)|Pachycephalosaurus|Pachycephalosaurus|modificada|
|[s0wn50S5mGxagSAa.htm](pathfinder-bestiary-2/s0wn50S5mGxagSAa.htm)|Zyss Serpentfolk|Zyss Serpentfolk|modificada|
|[s8EjxYqdN4gOvcBh.htm](pathfinder-bestiary-2/s8EjxYqdN4gOvcBh.htm)|Spinosaurus|Spinosaurus|modificada|
|[sbVdWfLZ2rdPh9Ec.htm](pathfinder-bestiary-2/sbVdWfLZ2rdPh9Ec.htm)|Carrion Golem|Carrion Golem|modificada|
|[scSsdEmG0jkEnyLV.htm](pathfinder-bestiary-2/scSsdEmG0jkEnyLV.htm)|Young Brine Dragon|Joven Dragón de Salmuera|modificada|
|[SFpMOxBwiS529dJk.htm](pathfinder-bestiary-2/SFpMOxBwiS529dJk.htm)|Brownie|Brownie|modificada|
|[SfPoGbcaQqPlgJ85.htm](pathfinder-bestiary-2/SfPoGbcaQqPlgJ85.htm)|Baobhan Sith|Baobhan Sith|modificada|
|[SG8fOxo4SS7bXpav.htm](pathfinder-bestiary-2/SG8fOxo4SS7bXpav.htm)|Bog Strider|Bog Strider|modificada|
|[sGI1gh7EPVSRadhY.htm](pathfinder-bestiary-2/sGI1gh7EPVSRadhY.htm)|Granite Glyptodont|Gliptodonte de granito|modificada|
|[sHKhlvs40YywkYw4.htm](pathfinder-bestiary-2/sHKhlvs40YywkYw4.htm)|Petitioner (Dimension of Dreams)|Peticionario (Dimensión de los Sueños)|modificada|
|[SKooDsYDMEloMG16.htm](pathfinder-bestiary-2/SKooDsYDMEloMG16.htm)|Giant Jellyfish|Medusa gigante|modificada|
|[sMNqIHHxcVBXG08h.htm](pathfinder-bestiary-2/sMNqIHHxcVBXG08h.htm)|Cairn Wight|Cairn Wight|modificada|
|[SpnJQIHLeiJOjEWR.htm](pathfinder-bestiary-2/SpnJQIHLeiJOjEWR.htm)|Shadow Giant|Gigante de las sombras|modificada|
|[SsR4wiSWA7xhlkH9.htm](pathfinder-bestiary-2/SsR4wiSWA7xhlkH9.htm)|Iguanodon|Iguanodon|modificada|
|[sY3VkazdyPiMgMj1.htm](pathfinder-bestiary-2/sY3VkazdyPiMgMj1.htm)|Fjord Linnorm|Linnorm del fiordo|modificada|
|[SY8pvLPqc2EgP9Xj.htm](pathfinder-bestiary-2/SY8pvLPqc2EgP9Xj.htm)|Soul Eater|Comealmas|modificada|
|[SYaC46yrqcyp16Dq.htm](pathfinder-bestiary-2/SYaC46yrqcyp16Dq.htm)|Weretiger|Weretiger|modificada|
|[T3low0wQNMO0SEtr.htm](pathfinder-bestiary-2/T3low0wQNMO0SEtr.htm)|Leucrotta|Leucrotta|modificada|
|[t8XwrLnvMnz3Z1al.htm](pathfinder-bestiary-2/t8XwrLnvMnz3Z1al.htm)|Catrina|Catrina|modificada|
|[T9vzmpvpRL5xjrhd.htm](pathfinder-bestiary-2/T9vzmpvpRL5xjrhd.htm)|Giant Badger|Tejón Gigante|modificada|
|[TCFurVn1aWhukqxb.htm](pathfinder-bestiary-2/TCFurVn1aWhukqxb.htm)|Petitioner (Dead Vault)|Peticionario (Bóveda muerta)|modificada|
|[tCJEjNDUxyV1GzE3.htm](pathfinder-bestiary-2/tCJEjNDUxyV1GzE3.htm)|Hamatula|Hamatula|modificada|
|[tdCyjq57rgfPezjT.htm](pathfinder-bestiary-2/tdCyjq57rgfPezjT.htm)|Carbuncle|Carbuncle|modificada|
|[TeUO57XO1Mqk1cTl.htm](pathfinder-bestiary-2/TeUO57XO1Mqk1cTl.htm)|Ancient Magma Dragon (Spellcaster)|Dragón de Magma Antiguo (Hechicero)|modificada|
|[tIDbOvltrFsgnwgf.htm](pathfinder-bestiary-2/tIDbOvltrFsgnwgf.htm)|Sea Drake|Sea Drake|modificada|
|[tieZOKf2qX7GsL9H.htm](pathfinder-bestiary-2/tieZOKf2qX7GsL9H.htm)|Ice Golem|Golem de hielo|modificada|
|[TKlnOKpYJkg1dNwb.htm](pathfinder-bestiary-2/TKlnOKpYJkg1dNwb.htm)|Totenmaske|Totenmaske|modificada|
|[tOVGYQueeJwLtn0v.htm](pathfinder-bestiary-2/tOVGYQueeJwLtn0v.htm)|Denizen of Leng|Denizen of Leng|modificada|
|[TRXxfSSk8oKHvMn5.htm](pathfinder-bestiary-2/TRXxfSSk8oKHvMn5.htm)|Adult Umbral Dragon (Spellcaster)|Dragón Umbral Adulto (Hechicero)|modificada|
|[tu31BdhnxNknt7kK.htm](pathfinder-bestiary-2/tu31BdhnxNknt7kK.htm)|Marut|Marut|modificada|
|[TzdLLC68qlgrn91m.htm](pathfinder-bestiary-2/TzdLLC68qlgrn91m.htm)|Nyogoth|Nyogoth|modificada|
|[tZkiE8hnN5e9KIzi.htm](pathfinder-bestiary-2/tZkiE8hnN5e9KIzi.htm)|Stygira|Stygira|modificada|
|[U2hAALepVC90CNhQ.htm](pathfinder-bestiary-2/U2hAALepVC90CNhQ.htm)|Rusalka|Rusalka|modificada|
|[uBG93M52pa84qIt5.htm](pathfinder-bestiary-2/uBG93M52pa84qIt5.htm)|Giant Mosquito|Mosquito gigante|modificada|
|[UcUlBj3brC80A8gq.htm](pathfinder-bestiary-2/UcUlBj3brC80A8gq.htm)|Snapping Turtle|Tortuga mordedora|modificada|
|[Ud6Iy1GolgtwrKQK.htm](pathfinder-bestiary-2/Ud6Iy1GolgtwrKQK.htm)|Babau|Babau|modificada|
|[UgEN8JYAT6tLbryy.htm](pathfinder-bestiary-2/UgEN8JYAT6tLbryy.htm)|Ahuizotl|Ahuizotl|modificada|
|[uGXTQYuRNtgPt1Pp.htm](pathfinder-bestiary-2/uGXTQYuRNtgPt1Pp.htm)|Skulk|Skulk|modificada|
|[uiTUtcXixZ6TdKYE.htm](pathfinder-bestiary-2/uiTUtcXixZ6TdKYE.htm)|Vanth|Vanth|modificada|
|[uIYl2DKLlcFLHNKo.htm](pathfinder-bestiary-2/uIYl2DKLlcFLHNKo.htm)|Young Brine Dragon (Spellcaster)|Joven Dragón de Salmuera (Hechicero)|modificada|
|[UJHJVPPvV0VbQqlW.htm](pathfinder-bestiary-2/UJHJVPPvV0VbQqlW.htm)|Adult Crystal Dragon|Dragón de Cristal Adulto|modificada|
|[UnWr8QSMq2VZUQhX.htm](pathfinder-bestiary-2/UnWr8QSMq2VZUQhX.htm)|Tatzlwyrm|Tatzlwyrm|modificada|
|[uQBfbNaIHdt0l2QB.htm](pathfinder-bestiary-2/uQBfbNaIHdt0l2QB.htm)|Quickling|Quickling|modificada|
|[UqFObUjgFAlWrriA.htm](pathfinder-bestiary-2/UqFObUjgFAlWrriA.htm)|Polar Bear|Polar Bear|modificada|
|[URYQ5goPCmrQvJf5.htm](pathfinder-bestiary-2/URYQ5goPCmrQvJf5.htm)|Giant Ant|Hormiga Gigante|modificada|
|[UTnrcGoCIXR95zKi.htm](pathfinder-bestiary-2/UTnrcGoCIXR95zKi.htm)|Giant Whiptail Centipede|Ciempiés gigante|modificada|
|[V6OBYa51VcehPj22.htm](pathfinder-bestiary-2/V6OBYa51VcehPj22.htm)|Gray Ooze|Ooze Gris|modificada|
|[Va2LUALLnnfWX3wq.htm](pathfinder-bestiary-2/Va2LUALLnnfWX3wq.htm)|Vrolikai|Vrolikai|modificada|
|[veyyE0brfjIoQK1q.htm](pathfinder-bestiary-2/veyyE0brfjIoQK1q.htm)|Icicle Snake|Serpiente carámbano|modificada|
|[vg3nE7p855SXkXbz.htm](pathfinder-bestiary-2/vg3nE7p855SXkXbz.htm)|Bebilith|Bebilith|modificada|
|[vHTvuteLd2kYX3ib.htm](pathfinder-bestiary-2/vHTvuteLd2kYX3ib.htm)|Flytrap Leshy|Leshy de las atrapamoscas|modificada|
|[VHXsmfnOBRBXB6TL.htm](pathfinder-bestiary-2/VHXsmfnOBRBXB6TL.htm)|Megalania|Megalania|modificada|
|[vjEHKCgMunMHKl7r.htm](pathfinder-bestiary-2/vjEHKCgMunMHKl7r.htm)|Wereboar|Wereboar|modificada|
|[VlxScTgNFzdc6KUY.htm](pathfinder-bestiary-2/VlxScTgNFzdc6KUY.htm)|Young Cloud Dragon (Spellcaster)|Joven Dragón de las Nubes (Hechicero)|modificada|
|[VPDKzeTY8O1XrKLk.htm](pathfinder-bestiary-2/VPDKzeTY8O1XrKLk.htm)|Giant Amoeba|Ameba gigante|modificada|
|[VrkmTQKB3Ln7wVTr.htm](pathfinder-bestiary-2/VrkmTQKB3Ln7wVTr.htm)|Behemoth Hippopotamus|Behemoth Hippopotamus|modificada|
|[vXXEdDy8ciGJxt3i.htm](pathfinder-bestiary-2/vXXEdDy8ciGJxt3i.htm)|Mist Stalker|Acechador de la Niebla|modificada|
|[VYEPG1G64OaCQmwf.htm](pathfinder-bestiary-2/VYEPG1G64OaCQmwf.htm)|Elasmosaurus|Elasmosaurus|modificada|
|[W1OXnXGF22uybovO.htm](pathfinder-bestiary-2/W1OXnXGF22uybovO.htm)|Taiga Giant|Gigante de Taiga|modificada|
|[W2gd0emEVbD8EGmf.htm](pathfinder-bestiary-2/W2gd0emEVbD8EGmf.htm)|Marsh Giant|Gigante de los pantanos|modificada|
|[wBGUIPJMm9Van9GQ.htm](pathfinder-bestiary-2/wBGUIPJMm9Van9GQ.htm)|Ice Yai|Hielo Yai|modificada|
|[wByXwxI5r7YRlkAK.htm](pathfinder-bestiary-2/wByXwxI5r7YRlkAK.htm)|Shoggti|Shoggti|modificada|
|[wCsfRovcZMZxSdGm.htm](pathfinder-bestiary-2/wCsfRovcZMZxSdGm.htm)|Zebub|Zebub|modificada|
|[wE9kb53ZHifUfdD8.htm](pathfinder-bestiary-2/wE9kb53ZHifUfdD8.htm)|Petitioner (Abyss)|Peticionario (Abyss)|modificada|
|[WfzhHOqGb3bR1e7e.htm](pathfinder-bestiary-2/WfzhHOqGb3bR1e7e.htm)|Ancient Umbral Dragon (Spellcaster)|Dragón Umbral Antiguo (Hechicero)|modificada|
|[wjcBMB7OvCHrnS39.htm](pathfinder-bestiary-2/wjcBMB7OvCHrnS39.htm)|Petitioner (Material Plane)|Peticionario (Plano Material)|modificada|
|[WjZCNuHNKfeeHCQS.htm](pathfinder-bestiary-2/WjZCNuHNKfeeHCQS.htm)|Worm That Walks Cultist|Gusano Que Camina Cultista|modificada|
|[WKsogmP2X2F68Zdm.htm](pathfinder-bestiary-2/WKsogmP2X2F68Zdm.htm)|Devourer|Devorador|modificada|
|[WMiPblOVq5w2Fa1x.htm](pathfinder-bestiary-2/WMiPblOVq5w2Fa1x.htm)|Aolaz|Aolaz|modificada|
|[WPTXi1WGKAvIIsrH.htm](pathfinder-bestiary-2/WPTXi1WGKAvIIsrH.htm)|Ancient Brine Dragon (Spellcaster)|Dragón de Salmuera Antiguo (Hechicero)|modificada|
|[wqiFcGPARy8zmQfd.htm](pathfinder-bestiary-2/wqiFcGPARy8zmQfd.htm)|Hezrou|Hezrou|modificada|
|[WqppjHF2Y8yC6veS.htm](pathfinder-bestiary-2/WqppjHF2Y8yC6veS.htm)|Shadow Drake|Sombra Drake|modificada|
|[wqsmnoby2n3BOvKu.htm](pathfinder-bestiary-2/wqsmnoby2n3BOvKu.htm)|Moonflower|Moonflower|modificada|
|[wrzlJufGEfMg5kVE.htm](pathfinder-bestiary-2/wrzlJufGEfMg5kVE.htm)|Raven Swarm|Enjambre de cuervos|modificada|
|[Wtok8DP7ybbEJ1fB.htm](pathfinder-bestiary-2/Wtok8DP7ybbEJ1fB.htm)|Cairn Linnorm|Cairn Linnorm del túmulo|modificada|
|[wtxNIFVSpVFuYfmI.htm](pathfinder-bestiary-2/wtxNIFVSpVFuYfmI.htm)|Ancient Crystal Dragon|Antiguo Dragón de Cristal|modificada|
|[wxBQsGLdti2xcGVT.htm](pathfinder-bestiary-2/wxBQsGLdti2xcGVT.htm)|Neothelid|Neothelid|modificada|
|[WxSpfUReYJb4StxG.htm](pathfinder-bestiary-2/WxSpfUReYJb4StxG.htm)|Blue-Ringed Octopus|Pulpo de anillos azules|modificada|
|[xarabMfYuo7yvD1N.htm](pathfinder-bestiary-2/xarabMfYuo7yvD1N.htm)|Giant Dragonfly Nymph|Ninfa libélula gigante|modificada|
|[Xb0uY2tg1Cd3kZd0.htm](pathfinder-bestiary-2/Xb0uY2tg1Cd3kZd0.htm)|Jyoti|Jyoti|modificada|
|[XCNRztCxn6EbJ0gr.htm](pathfinder-bestiary-2/XCNRztCxn6EbJ0gr.htm)|Norn|Norn|modificada|
|[xdIHvCBspeSXRL6A.htm](pathfinder-bestiary-2/xdIHvCBspeSXRL6A.htm)|Ghonhatine|Ghonhatino|modificada|
|[xfcFXLbadD3KdlHW.htm](pathfinder-bestiary-2/xfcFXLbadD3KdlHW.htm)|Lerritan|Lerritan|modificada|
|[xIvv8vKBa0YsTEzt.htm](pathfinder-bestiary-2/xIvv8vKBa0YsTEzt.htm)|Korred|Korred|modificada|
|[xjV0TmPNfKR7UcKu.htm](pathfinder-bestiary-2/xjV0TmPNfKR7UcKu.htm)|Osyluth|Osyluth|modificada|
|[xrAx0xj0UsM4DG5T.htm](pathfinder-bestiary-2/xrAx0xj0UsM4DG5T.htm)|Oread Guard|Guardia Oread|modificada|
|[xUtl7Nkb6iJpPUMK.htm](pathfinder-bestiary-2/xUtl7Nkb6iJpPUMK.htm)|Destrachan|Destrachan|modificada|
|[Xv74O1mFzzP06IXl.htm](pathfinder-bestiary-2/Xv74O1mFzzP06IXl.htm)|Hippogriff|Hippogriff|modificada|
|[xxP5FJotshmUQNtY.htm](pathfinder-bestiary-2/xxP5FJotshmUQNtY.htm)|Black Bear|Oso Negro|modificada|
|[XZZdG5RosoYsF237.htm](pathfinder-bestiary-2/XZZdG5RosoYsF237.htm)|Compsognathus|Compsognathus|modificada|
|[Y0qsrBFTFMbaiLzr.htm](pathfinder-bestiary-2/Y0qsrBFTFMbaiLzr.htm)|Jabberwock|Jabberwock|modificada|
|[Y1jsLqhsbqoXiGQe.htm](pathfinder-bestiary-2/Y1jsLqhsbqoXiGQe.htm)|Giant Toad|Sapo gigante|modificada|
|[Y1lwEf06O1ijOnwt.htm](pathfinder-bestiary-2/Y1lwEf06O1ijOnwt.htm)|Augnagar|Augnagar|modificada|
|[y2LqIPBHv2dA0pjI.htm](pathfinder-bestiary-2/y2LqIPBHv2dA0pjI.htm)|Necrophidius|Necrophidius|modificada|
|[Y5uPBnABTaAnpdqz.htm](pathfinder-bestiary-2/Y5uPBnABTaAnpdqz.htm)|Young Umbral Dragon (Spellcaster)|Joven Dragón Umbral (Hechicero)|modificada|
|[y9D2wh63pBzIyI7i.htm](pathfinder-bestiary-2/y9D2wh63pBzIyI7i.htm)|Giant Maggot|Gusano gigante|modificada|
|[Y9SDbANT1DWI1DKU.htm](pathfinder-bestiary-2/Y9SDbANT1DWI1DKU.htm)|Animate Dream|Sue animado|modificada|
|[yBglC9GBy6p1x87t.htm](pathfinder-bestiary-2/yBglC9GBy6p1x87t.htm)|Tendriculos|Tendriculos|modificada|
|[YFvur5htRwt9EfQs.htm](pathfinder-bestiary-2/YFvur5htRwt9EfQs.htm)|Sandpoint Devil|Sandpoint Devil|modificada|
|[yIpB2uLyeBWQjfsn.htm](pathfinder-bestiary-2/yIpB2uLyeBWQjfsn.htm)|Gylou|Gylou|modificada|
|[yr5xSfvSt7vA45wx.htm](pathfinder-bestiary-2/yr5xSfvSt7vA45wx.htm)|Young Cloud Dragon|Joven Dragón de las Nubes|modificada|
|[ytsHNgRwLzabsrZt.htm](pathfinder-bestiary-2/ytsHNgRwLzabsrZt.htm)|Meladaemon|Meladaemon|modificada|
|[YUZF9WhDmfzma3dE.htm](pathfinder-bestiary-2/YUZF9WhDmfzma3dE.htm)|Nalfeshnee|Nalfeshnee|modificada|
|[Yv0005Vym1peVKHq.htm](pathfinder-bestiary-2/Yv0005Vym1peVKHq.htm)|Ravener Husk|Cascarón de voraz|modificada|
|[yvmX4J21XE1wqDar.htm](pathfinder-bestiary-2/yvmX4J21XE1wqDar.htm)|Esobok|Esobok|modificada|
|[yXP9vS35tZ5iAUix.htm](pathfinder-bestiary-2/yXP9vS35tZ5iAUix.htm)|Petitioner (Astral Plane)|Peticionario (Plano Astral)|modificada|
|[YZeJRw4hvKyBrNFx.htm](pathfinder-bestiary-2/YZeJRw4hvKyBrNFx.htm)|Petitioner (Hell)|Peticionario (Infierno)|modificada|
|[Z2v611Np3J1MBkpv.htm](pathfinder-bestiary-2/Z2v611Np3J1MBkpv.htm)|Dream Spider|Araña del Sue|modificada|
|[z39LYFGYmOsoGVSh.htm](pathfinder-bestiary-2/z39LYFGYmOsoGVSh.htm)|Hippocampus|Hippocampus|modificada|
|[Z3J7uGaLmlFRfwPk.htm](pathfinder-bestiary-2/Z3J7uGaLmlFRfwPk.htm)|Magma Scorpion|Escorpión de magma|modificada|
|[Z4eI4EhXBWDs8bl8.htm](pathfinder-bestiary-2/Z4eI4EhXBWDs8bl8.htm)|Undine Hydromancer|Undine Hydromancer|modificada|
|[Z9rOUqCqAaMjvgBc.htm](pathfinder-bestiary-2/Z9rOUqCqAaMjvgBc.htm)|Drainberry Bush|Arbusto de bayas drenadoras|modificada|
|[zA9tCwjyViY6ZPfS.htm](pathfinder-bestiary-2/zA9tCwjyViY6ZPfS.htm)|Ancient Cloud Dragon|Ancient Cloud Dragon|modificada|
|[zAxCU99Jwky6QoBy.htm](pathfinder-bestiary-2/zAxCU99Jwky6QoBy.htm)|Living Boulder|Pesco vivo|modificada|
|[ZBTqN3DNjdreXCu7.htm](pathfinder-bestiary-2/ZBTqN3DNjdreXCu7.htm)|Mastodon|Mastodon|modificada|
|[ZGEf08j9q4fIPDFd.htm](pathfinder-bestiary-2/ZGEf08j9q4fIPDFd.htm)|Dread Wraith|Wraith aterrador|modificada|
|[zjPdqdQGNjO3Kto7.htm](pathfinder-bestiary-2/zjPdqdQGNjO3Kto7.htm)|Aurumvorax|Aurumvorax|modificada|
|[ZKTz1ABoPTtlgYIy.htm](pathfinder-bestiary-2/ZKTz1ABoPTtlgYIy.htm)|Hound Archon|Hound Archon|modificada|
|[ZrDGTNxlISyxNAKX.htm](pathfinder-bestiary-2/ZrDGTNxlISyxNAKX.htm)|Bralani|Bralani|modificada|
|[zXZrUTQSI0WxAsF5.htm](pathfinder-bestiary-2/zXZrUTQSI0WxAsF5.htm)|Verdurous Ooze|Cieno verduroso|modificada|
|[ZyZMJ19Ea0cfydK4.htm](pathfinder-bestiary-2/ZyZMJ19Ea0cfydK4.htm)|Petitioner (Shadow Plane)|Peticionario (Plano de Sombra)|modificada|
|[zzu3q9m4kJzgnNfl.htm](pathfinder-bestiary-2/zzu3q9m4kJzgnNfl.htm)|Black Dracolisk|Dracolisk negro|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
