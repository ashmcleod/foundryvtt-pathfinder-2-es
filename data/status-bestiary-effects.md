# Estado de la traducción (bestiary-effects)

 * **modificada**: 262
 * **ninguna**: 8


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[E0fYyli4aCVmevgJ.htm](bestiary-effects/E0fYyli4aCVmevgJ.htm)|Effect: Capture Magic|
|[lJGAJF9QiXCOPd2I.htm](bestiary-effects/lJGAJF9QiXCOPd2I.htm)|Effect: Draconic Breath Weapon Aura|
|[RbyXD99lybxPK6yS.htm](bestiary-effects/RbyXD99lybxPK6yS.htm)|Effect: Warning Hoot|
|[t8Yxrx3XgjS78Hxt.htm](bestiary-effects/t8Yxrx3XgjS78Hxt.htm)|Effect: Assimilate Lava|
|[WYJdjmvfm8J6sG6g.htm](bestiary-effects/WYJdjmvfm8J6sG6g.htm)|Effect: Jury-Rig|
|[XE2YhBMl7wc6nQAZ.htm](bestiary-effects/XE2YhBMl7wc6nQAZ.htm)|Effect: Replenishment of War|
|[xOD3ufpzA8H7W4sP.htm](bestiary-effects/xOD3ufpzA8H7W4sP.htm)|Effect: Aura of Good Cheer|
|[YKCsmlMgI0aS7joO.htm](bestiary-effects/YKCsmlMgI0aS7joO.htm)|Effect: Silent Aura|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0ifqiVhhKULXJyf9.htm](bestiary-effects/0ifqiVhhKULXJyf9.htm)|Effect: Rune of Flames (Longspear)|Efecto: Runa de las Flamígera.|modificada|
|[0jAT2TJoqC1z6NCf.htm](bestiary-effects/0jAT2TJoqC1z6NCf.htm)|Effect: Consume Memories|Efecto: Consumir recuerdos|modificada|
|[0jo8CUzw5lWehNg3.htm](bestiary-effects/0jo8CUzw5lWehNg3.htm)|Effect: Oceanic Armor|Efecto: Armadura Oceánica|modificada|
|[1bOSJ2LbEC28aI9f.htm](bestiary-effects/1bOSJ2LbEC28aI9f.htm)|Effect: Despair|Efecto: Desesperación|modificada|
|[1dwMVgBHfT4qO4OS.htm](bestiary-effects/1dwMVgBHfT4qO4OS.htm)|Effect: Resonance|Efecto: Resonancia|modificada|
|[1jrrnMwsfO97LXi4.htm](bestiary-effects/1jrrnMwsfO97LXi4.htm)|Effect: Absorb Memories|Efecto: Absorber Recuerdos|modificada|
|[1toVzNVJZx0RwG1v.htm](bestiary-effects/1toVzNVJZx0RwG1v.htm)|Effect: Darivan's Bloodline Magic|Efecto: Magia de linaje de Darivan|modificada|
|[2ccLQxmTlTPySnOR.htm](bestiary-effects/2ccLQxmTlTPySnOR.htm)|Effect: Technology Control|Efecto: Control de Tecnología|modificada|
|[2DlOfFoYLGSlfugH.htm](bestiary-effects/2DlOfFoYLGSlfugH.htm)|Effect: Manipulate Luck (Good)|Efecto: Manipular Suerte (Bueno)|modificada|
|[31nnjHZqiaqaWBUi.htm](bestiary-effects/31nnjHZqiaqaWBUi.htm)|Effect: Harmonizing Aura (Allies)|Efecto: Aura armonizante (aliados).|modificada|
|[361dIAAhiZE0wg8v.htm](bestiary-effects/361dIAAhiZE0wg8v.htm)|Effect: Adaptive Strike|Efecto: Golpe Adaptativo|modificada|
|[37wbBpPBi5eBtNqM.htm](bestiary-effects/37wbBpPBi5eBtNqM.htm)|Effect: Lantern of Hope|Efecto: Faro de esperanza|modificada|
|[3MIZf42EhhKbIwLQ.htm](bestiary-effects/3MIZf42EhhKbIwLQ.htm)|Effect: Aura of Corruption|Efecto: Aura de Corrupción|modificada|
|[3np7EyJ8pyrVlpie.htm](bestiary-effects/3np7EyJ8pyrVlpie.htm)|Effect: Claim Corpse - Skeletal|Efecto: Reclamar Cadáver - Esquelético|modificada|
|[3QUaxNvpHDmf9cTo.htm](bestiary-effects/3QUaxNvpHDmf9cTo.htm)|Effect: Rune of Flames (Rock)|Efecto: Runa de Flamígera (Roca).|modificada|
|[3Wtzyb0ZgkaC7vHY.htm](bestiary-effects/3Wtzyb0ZgkaC7vHY.htm)|Effect: Fanatical Frenzy|Efecto: Frenesí fanático|modificada|
|[4bR1i7qzmSJ5No6O.htm](bestiary-effects/4bR1i7qzmSJ5No6O.htm)|Effect: Bond in Light|Efecto: Lazo en Luz|modificada|
|[4FginnDcOt4wfedf.htm](bestiary-effects/4FginnDcOt4wfedf.htm)|Effect: Bittersweet Dreams|Efecto: Sueños agridulces.|modificada|
|[4FMFRlEC923CnLI7.htm](bestiary-effects/4FMFRlEC923CnLI7.htm)|Effect: Fed by Wood|Efecto: Alimentado por Madera|modificada|
|[4GAJHkurmZ1ttKDv.htm](bestiary-effects/4GAJHkurmZ1ttKDv.htm)|Effect: Blood Magic|Efecto: Magia de sangre|modificada|
|[4HJJdogS751Z04lD.htm](bestiary-effects/4HJJdogS751Z04lD.htm)|Effect: Tail Lash (Skill Check)|Efecto: Azote con la cola (Chequeo de Habilidad)|modificada|
|[4M2K16mH4gndHAKa.htm](bestiary-effects/4M2K16mH4gndHAKa.htm)|Effect: Undead Mastery|Efecto: Maestría de muerto viviente|modificada|
|[4q8Of8NM9DC8kWyK.htm](bestiary-effects/4q8Of8NM9DC8kWyK.htm)|Effect: Pry|Efecto: Pry|modificada|
|[4tGVIEqwH4TQoF0O.htm](bestiary-effects/4tGVIEqwH4TQoF0O.htm)|Effect: Bastion Aura|Efecto: Aura de Bastión|modificada|
|[5NSWRxAsJuvwyl0E.htm](bestiary-effects/5NSWRxAsJuvwyl0E.htm)|Effect: Commander's Aura|Efecto: Aura del Comandante|modificada|
|[5roCV7EbPx1G5xOd.htm](bestiary-effects/5roCV7EbPx1G5xOd.htm)|Effect: Mangling Rend|Efecto: Desgarro mutilador|modificada|
|[5syHrFGAE6lo0FUr.htm](bestiary-effects/5syHrFGAE6lo0FUr.htm)|Effect: Invigorating Passion|Efecto: Pasión vigorizante.|modificada|
|[5x0XpNftvx9uGbXt.htm](bestiary-effects/5x0XpNftvx9uGbXt.htm)|Effect: Death Gasp (Etioling)|Efecto: Jadeo Mortal (Etioling)|modificada|
|[5ZK22sNW7o26aST0.htm](bestiary-effects/5ZK22sNW7o26aST0.htm)|Effect: Dirty Bomb|Efecto: Bomba Sucia|modificada|
|[64wrP9IbfHbj1mrA.htm](bestiary-effects/64wrP9IbfHbj1mrA.htm)|Effect: War Leader|Efecto: Líder de Guerra|modificada|
|[6E8bOkwFzFuQ3ZAw.htm](bestiary-effects/6E8bOkwFzFuQ3ZAw.htm)|Effect: Lurker's Glow (Critical Failure)|Efecto: Lurker's Glow (Fallo crítico)|modificada|
|[6GC248hHu3LdOAtS.htm](bestiary-effects/6GC248hHu3LdOAtS.htm)|Effect: Sting of the Lash|Efecto: Aguijón del Latigazo|modificada|
|[6rnB7nK6J6zF4vea.htm](bestiary-effects/6rnB7nK6J6zF4vea.htm)|Effect: Graveknight's Curse|Efecto: Maldición del caballero sepulcral.|modificada|
|[75B7z49jfQbWcSy9.htm](bestiary-effects/75B7z49jfQbWcSy9.htm)|Effect: Spray Toxic Oil|Efecto: Rociar Aceite Tóxico|modificada|
|[7OJrBAEah6YFLzcK.htm](bestiary-effects/7OJrBAEah6YFLzcK.htm)|Effect: Crush Chitin|Efecto: Aplastar quitina|modificada|
|[7PjBr9LsVB0Jjzyu.htm](bestiary-effects/7PjBr9LsVB0Jjzyu.htm)|Effect: Interpose|Efecto: Interponer|modificada|
|[7PYhxmQxCD5kzMlw.htm](bestiary-effects/7PYhxmQxCD5kzMlw.htm)|Effect: Harrowing Misfortune|Efecto: Desgracia Desgarradora|modificada|
|[7qoZauizAKfPIXeu.htm](bestiary-effects/7qoZauizAKfPIXeu.htm)|Effect: Volcanic Purge|Efecto: Purga volcánica.|modificada|
|[7wDH2q0UcFdu2w58.htm](bestiary-effects/7wDH2q0UcFdu2w58.htm)|Effect: Focused Assault|Efecto: Asalto concentrado|modificada|
|[7x0O2GqWBJiAk5PF.htm](bestiary-effects/7x0O2GqWBJiAk5PF.htm)|Effect: Brutal Rally|Efecto: Reunión brutal|modificada|
|[81XYZZ3H0GL8thdQ.htm](bestiary-effects/81XYZZ3H0GL8thdQ.htm)|Effect: Countered by Water|Efecto: Contrarrestado por Agua|modificada|
|[86uYNsVXzYWxqLTV.htm](bestiary-effects/86uYNsVXzYWxqLTV.htm)|Effect: Splintered Ground|Efecto: Tierra Astillada|modificada|
|[8z4Q84UlS8cMYVWu.htm](bestiary-effects/8z4Q84UlS8cMYVWu.htm)|Effect: Black Cat Curse|Efecto: Maldición del Gato Negro|modificada|
|[9Qyu0HN5j6DO8Izc.htm](bestiary-effects/9Qyu0HN5j6DO8Izc.htm)|Effect: Sinful Bite|Efecto: Muerdemuerde Pecaminoso|modificada|
|[a0nJK2mKHwWYzpdG.htm](bestiary-effects/a0nJK2mKHwWYzpdG.htm)|Effect: Bully's Rage|Efecto: Furia de Matón|modificada|
|[a4bZrBNL17zPjDiH.htm](bestiary-effects/a4bZrBNL17zPjDiH.htm)|Effect: Countered by Fire|Efecto: Contrarrestado por Fuego|modificada|
|[AL7E03DYahfDhbcR.htm](bestiary-effects/AL7E03DYahfDhbcR.htm)|Effect: Guardian's Aegis|Efecto: Égida del Guardián|modificada|
|[aOpSI5tApXF5xHCM.htm](bestiary-effects/aOpSI5tApXF5xHCM.htm)|Effect: Unsettled Aura|Efecto: Aura Inquietante|modificada|
|[APJJCYdq4gNe0PF4.htm](bestiary-effects/APJJCYdq4gNe0PF4.htm)|Effect: Susceptible to Mockery (Critical Failure)|Efecto: Susceptible a la Burla (Fallo Crítico)|modificada|
|[AThOnEYzPO9ssYTB.htm](bestiary-effects/AThOnEYzPO9ssYTB.htm)|Effect: Disturbing Vision|Efecto: Visión Perturbadora|modificada|
|[auJ02rr1Jr88oD3Z.htm](bestiary-effects/auJ02rr1Jr88oD3Z.htm)|Effect: Claim Corpse - Fleshy|Efecto: Reclamar Cadáver - Carnoso|modificada|
|[BmAw66zEkifSvOtg.htm](bestiary-effects/BmAw66zEkifSvOtg.htm)|Effect: Brand of the Impenitent|Efecto: Marca del impenitente.|modificada|
|[BsuYuFxE20mnRxbs.htm](bestiary-effects/BsuYuFxE20mnRxbs.htm)|Effect: Defensive Slam|Efecto: Golpe Defensivo|modificada|
|[BTSstQAn4Xlm1PWV.htm](bestiary-effects/BTSstQAn4Xlm1PWV.htm)|Effect: Uncanny Tinker|Efecto: Uncanny Trastear|modificada|
|[bV70ZxnUAoYnfXaZ.htm](bestiary-effects/bV70ZxnUAoYnfXaZ.htm)|Effect: Rune of Smiting (Rock)|Efecto: Runa de Smiting (Roca)|modificada|
|[c6SiBB3mzV8lZUQr.htm](bestiary-effects/c6SiBB3mzV8lZUQr.htm)|Effect: Protean Anatomy|Efecto: Anatomía Proteica|modificada|
|[C9nb9XbnQgbnXpTq.htm](bestiary-effects/C9nb9XbnQgbnXpTq.htm)|Effect: Crystalline Dust Form|Efecto: Forma de Polvo Cristalino|modificada|
|[Cc5WsKkbOPOzopCY.htm](bestiary-effects/Cc5WsKkbOPOzopCY.htm)|Effect: Dire Warning|Efecto: Advertencia funesta|modificada|
|[ceOkHxhJNTcvZkCy.htm](bestiary-effects/ceOkHxhJNTcvZkCy.htm)|Effect: Blood Siphon (Critical Failure)|Efecto: Sifón de Sangre (Fallo Crítico)|modificada|
|[CiCG3r7SHYMJeUxz.htm](bestiary-effects/CiCG3r7SHYMJeUxz.htm)|Effect: Inspirational Presence|Efecto: Presencia Inspiradora|modificada|
|[ckL8iL7BKBPteEVL.htm](bestiary-effects/ckL8iL7BKBPteEVL.htm)|Effect: Goblin Song|Efecto: Canción de goblin.|modificada|
|[CoPPOCyfADvybcxV.htm](bestiary-effects/CoPPOCyfADvybcxV.htm)|Effect: Gleaming Armor|Efecto: Armadura reluciente|modificada|
|[CUgmb7g65vYNT2hn.htm](bestiary-effects/CUgmb7g65vYNT2hn.htm)|Effect: Curse of Boiling Blood|Efecto: Maldición de sangre hirviente.|modificada|
|[dNs30jfFOJqhjFW7.htm](bestiary-effects/dNs30jfFOJqhjFW7.htm)|Effect: Entangling Train|Efecto: Tren de enredos|modificada|
|[dRe8n1nBWMIXd8jh.htm](bestiary-effects/dRe8n1nBWMIXd8jh.htm)|Effect: Moon Frenzy|Efecto: Frenesí lunar.|modificada|
|[dVVc5MYx4G3QPHoa.htm](bestiary-effects/dVVc5MYx4G3QPHoa.htm)|Effect: Guildmaster's Lead|Efecto: Guildmaster's Lead|modificada|
|[E10qJgInN8Fx3yhW.htm](bestiary-effects/E10qJgInN8Fx3yhW.htm)|Effect: Distracting Frolic|Efecto: Cabriolas desconcertantes|modificada|
|[e4HoYakV7SvwvcrH.htm](bestiary-effects/e4HoYakV7SvwvcrH.htm)|Effect: Divine Aegis|Efecto: Égida Divina|modificada|
|[EM0VP31B8CyjFD15.htm](bestiary-effects/EM0VP31B8CyjFD15.htm)|Effect: Inspired Feast|Efecto: Festín inspirado.|modificada|
|[EqdXFH2l0TyeOTQz.htm](bestiary-effects/EqdXFH2l0TyeOTQz.htm)|Effect: Bloodline Magic (Stirvyn Banyan)|Efecto: Magia de linaje (Stirvyn Banyan)|modificada|
|[EwhWYPCNM8bIIYEI.htm](bestiary-effects/EwhWYPCNM8bIIYEI.htm)|Effect: Defensive Slice|Efecto: Rebanada Defensiva|modificada|
|[eZL4B1P4H8hr1CAn.htm](bestiary-effects/eZL4B1P4H8hr1CAn.htm)|Effect: Glowing Spores|Efecto: Esporas Brillantes|modificada|
|[f8rncEs06bqcn3LZ.htm](bestiary-effects/f8rncEs06bqcn3LZ.htm)|Effect: Sandstorm|Efecto: Tormenta de arena|modificada|
|[fGI77mVeCQKBAP0f.htm](bestiary-effects/fGI77mVeCQKBAP0f.htm)|Effect: Drain Luck|Efecto: Drenar Suerte|modificada|
|[FISKjI7bal26AnyL.htm](bestiary-effects/FISKjI7bal26AnyL.htm)|Effect: Timely Advice|Efecto: Consejo Oportuno|modificada|
|[FjcvJWPFwkyy5vEk.htm](bestiary-effects/FjcvJWPFwkyy5vEk.htm)|Effect: Artificer's Command|Efecto: Orden imperiosa del artífice.|modificada|
|[fPhbDSTPuT01f0Kn.htm](bestiary-effects/fPhbDSTPuT01f0Kn.htm)|Effect: Ensnare|Efecto: Enredar|modificada|
|[FQrNDisx6noJnFaQ.htm](bestiary-effects/FQrNDisx6noJnFaQ.htm)|Effect: Fettering Fedora|Efecto: Fettering Fedora|modificada|
|[g4CRjb2zS1pkVDbk.htm](bestiary-effects/g4CRjb2zS1pkVDbk.htm)|Effect: Sticky Spores|Efecto: Esporas Pegajosas|modificada|
|[g5Yen1FhEMCzCJkv.htm](bestiary-effects/g5Yen1FhEMCzCJkv.htm)|Effect: Profane Gift|Efecto: Don profano|modificada|
|[GC5441z8i7N8swk1.htm](bestiary-effects/GC5441z8i7N8swk1.htm)|Effect: Pulse of Rage|Efecto: Pulso de Furia|modificada|
|[gHczZWA34WQx4px0.htm](bestiary-effects/gHczZWA34WQx4px0.htm)|Effect: Bully the Departed|Efecto: Intimidar a los difuntos|modificada|
|[gORyINQLvplfthlm.htm](bestiary-effects/gORyINQLvplfthlm.htm)|Effect: Pollution Infusion|Efecto: Infusión de Contaminación|modificada|
|[GstmxSP75eKmHtCQ.htm](bestiary-effects/GstmxSP75eKmHtCQ.htm)|Effect: Hallucinogenic Pollen (Failure)|Efecto: Polen alucinógeno (Fallo)|modificada|
|[gxBjyPaYDdhjAImf.htm](bestiary-effects/gxBjyPaYDdhjAImf.htm)|Effect: Ancestral Response|Efecto: Respuesta Ancestral|modificada|
|[H99PJXU5cvJ4Oycm.htm](bestiary-effects/H99PJXU5cvJ4Oycm.htm)|Effect: Fiery Form|Efecto: Forma Ardiente|modificada|
|[HafP5vrdGdoqoFMo.htm](bestiary-effects/HafP5vrdGdoqoFMo.htm)|Effect: Barbed Net|Efecto: Red de Púas|modificada|
|[HArljmKc2IR7rtfc.htm](bestiary-effects/HArljmKc2IR7rtfc.htm)|Effect: Swig|Efecto: Trago|modificada|
|[hc7kR6quUE9ryRyj.htm](bestiary-effects/hc7kR6quUE9ryRyj.htm)|Effect: Splintered Ground (Critical Failure)|Efecto: Suelo astillado (Fallo crítico)|modificada|
|[hdsl0XexqEL2emE3.htm](bestiary-effects/hdsl0XexqEL2emE3.htm)|Effect: Lantern of Hope (Gestalt)|Efecto: Faro de esperanza (Gestalt)|modificada|
|[he0QiJSgJWNeIoxt.htm](bestiary-effects/he0QiJSgJWNeIoxt.htm)|Effect: Wrath of Spurned Hospitality|Efecto: Ira de la hospitalidad despreciada|modificada|
|[HF2kOaqrjCkaZnK9.htm](bestiary-effects/HF2kOaqrjCkaZnK9.htm)|Effect: Prepare Fangs|Efecto: Preparar Colmillos|modificada|
|[hftFxE8JGJGzXAtU.htm](bestiary-effects/hftFxE8JGJGzXAtU.htm)|Effect: Song of the Swamp|Efecto: Canción del Pantano|modificada|
|[HGP0c0tOK2WnC4p5.htm](bestiary-effects/HGP0c0tOK2WnC4p5.htm)|Effect: Breach the Abyss - Fire|Efecto: Emerger el Abismo - Fuego|modificada|
|[HKyMZobjOsd6WFuo.htm](bestiary-effects/HKyMZobjOsd6WFuo.htm)|Effect: Hurtful Critique|Efecto: Crítica hiriente|modificada|
|[HpX3bGlOkSp1PeWg.htm](bestiary-effects/HpX3bGlOkSp1PeWg.htm)|Effect: Vent Energy|Efecto: Expulsar gases|modificada|
|[hQsqr2sRPp9rNm4U.htm](bestiary-effects/hQsqr2sRPp9rNm4U.htm)|Effect: Putrid Blast|Efecto: Explosión pútrida|modificada|
|[Hv4NO0HADyAAdS4F.htm](bestiary-effects/Hv4NO0HADyAAdS4F.htm)|Effect: Bark Orders|Efecto: Órdenes de Ladrido|modificada|
|[i3942RucZD9OHbAE.htm](bestiary-effects/i3942RucZD9OHbAE.htm)|Effect: Void Shroud|Efecto: Mortaja del vacío|modificada|
|[i4S6h5c1KK4FLq6w.htm](bestiary-effects/i4S6h5c1KK4FLq6w.htm)|Effect: Feed|Efecto: Alimentarse|modificada|
|[I5Fd4TkIKRJT6WXf.htm](bestiary-effects/I5Fd4TkIKRJT6WXf.htm)|Effect: Aura of Righteousness (Planetar)|Efecto: Aura de rectitud (Planetario).|modificada|
|[iDLu83vhWoNIE7xt.htm](bestiary-effects/iDLu83vhWoNIE7xt.htm)|Effect: Commanding Aura (Quara)|Efecto: Aura imperiosa (Quara)|modificada|
|[IfUod2VxNmZMGGPq.htm](bestiary-effects/IfUod2VxNmZMGGPq.htm)|Effect: Bodyguard's Defense|Efecto: Defensa del Guardaespaldas|modificada|
|[irSKPLF5oLfnqUNW.htm](bestiary-effects/irSKPLF5oLfnqUNW.htm)|Effect: Spotlight|Efecto: Foco|modificada|
|[IrXb05caCTkP792e.htm](bestiary-effects/IrXb05caCTkP792e.htm)|Effect: Reminder of Doom|Efecto: Recordatorio de Perdición|modificada|
|[iSyyR4QVOaY6mxtg.htm](bestiary-effects/iSyyR4QVOaY6mxtg.htm)|Effect: Nymph Queen's Inspiration|Efecto: Inspiración de la Reina Ninfa|modificada|
|[itdoXvcDo8wwmTME.htm](bestiary-effects/itdoXvcDo8wwmTME.htm)|Effect: Despoiler|Efecto: Despojador|modificada|
|[itVZwwvaXYN2zOR8.htm](bestiary-effects/itVZwwvaXYN2zOR8.htm)|Effect: Parry Dance|Efecto: Parry Danzante|modificada|
|[iuy6AA6sQZUZkN9X.htm](bestiary-effects/iuy6AA6sQZUZkN9X.htm)|Effect: Rapid Evolution - Husk|Efecto: Evolución Rápida - Husk|modificada|
|[iWLAcND0iyW4NUZm.htm](bestiary-effects/iWLAcND0iyW4NUZm.htm)|Effect: Viscous Trap|Efecto: Trampa viscosa|modificada|
|[iXVsVqttUGlfdIHS.htm](bestiary-effects/iXVsVqttUGlfdIHS.htm)|Effect: Share Pain|Efecto: Compartir Dolor|modificada|
|[J6EwMsXtXg30eaiB.htm](bestiary-effects/J6EwMsXtXg30eaiB.htm)|Effect: Stoke the Fervent - High Tormentor|Efecto: Avivar a los fervientes - Alto Atormentador|modificada|
|[JlaO7fgm53Om1DxB.htm](bestiary-effects/JlaO7fgm53Om1DxB.htm)|Effect: Swell|Efecto: Hincharse|modificada|
|[jQmfZcPD6xVYx5nb.htm](bestiary-effects/jQmfZcPD6xVYx5nb.htm)|Effect: Entangling Slime|Efecto: Limo enmarado|modificada|
|[jrfuplUEq3PIo6j1.htm](bestiary-effects/jrfuplUEq3PIo6j1.htm)|Effect: Blasphemous Utterances|Efecto: Proclamaciones blasfemas|modificada|
|[K3r1lfbtCTf3dFhV.htm](bestiary-effects/K3r1lfbtCTf3dFhV.htm)|Effect: Stick a Fork in It|Efecto: Clavarle un tenedor|modificada|
|[KdV8UtN8Af5oCerj.htm](bestiary-effects/KdV8UtN8Af5oCerj.htm)|Effect: Cite Precedent|Efecto: Citar Precedente|modificada|
|[kdZEZ6iBYQPVGzYD.htm](bestiary-effects/kdZEZ6iBYQPVGzYD.htm)|Effect: Call to Action|Efecto: Llamada a la Acción|modificada|
|[kHRbOHKEedKaBFH5.htm](bestiary-effects/kHRbOHKEedKaBFH5.htm)|Effect: Transfer Protection|Efecto: Transferir protección.|modificada|
|[kjP0J97hhoIhbF3M.htm](bestiary-effects/kjP0J97hhoIhbF3M.htm)|Effect: Necrotic Field|Efecto: Campo Necrótico|modificada|
|[KjWPSJonzrZ8jv7X.htm](bestiary-effects/KjWPSJonzrZ8jv7X.htm)|Effect: Phalanx Fighter|Efecto: Luchador de Falange|modificada|
|[kOGRUuyK04MjLc3m.htm](bestiary-effects/kOGRUuyK04MjLc3m.htm)|Effect: Spiritual Warden|Efecto: Guardián Espiritual|modificada|
|[KpRRcLsAiIhJFE03.htm](bestiary-effects/KpRRcLsAiIhJFE03.htm)|Effect: Furious Possession|Efecto: Posesión furiosa.|modificada|
|[l62iAFL3EO7wSsLL.htm](bestiary-effects/l62iAFL3EO7wSsLL.htm)|Effect: Form a Phalanx|Efecto: Forma una Falange|modificada|
|[L7SiTshdimUPWfnB.htm](bestiary-effects/L7SiTshdimUPWfnB.htm)|Effect: Witchflame (Failure)|Efecto: Llama de Bruja (Fallo)|modificada|
|[lBpprC8VD4GRzTtg.htm](bestiary-effects/lBpprC8VD4GRzTtg.htm)|Effect: Wild Shape (Oaksteward)|Efecto: Forma salvaje (Oaksteward)|modificada|
|[LgZx5xotO08JzhVc.htm](bestiary-effects/LgZx5xotO08JzhVc.htm)|Effect: Mutilating Bite|Efecto: Muerdemuerde|modificada|
|[lIEc8Lx3ROm17Dqa.htm](bestiary-effects/lIEc8Lx3ROm17Dqa.htm)|Effect: Fetid Screech (Critical Failure)|Efecto: Grito fétido (Fallo crítico)|modificada|
|[LohrnRXCQ0yVt8MK.htm](bestiary-effects/LohrnRXCQ0yVt8MK.htm)|Effect: Raise Guard|Efecto: Levantar guardia|modificada|
|[LOLoQpaAp5cC0hbm.htm](bestiary-effects/LOLoQpaAp5cC0hbm.htm)|Effect: Blood Fury|Efecto: Furia de sangre|modificada|
|[lt2t24E4hiByHhi3.htm](bestiary-effects/lt2t24E4hiByHhi3.htm)|Effect: Lost Plates|Efecto: Placas Perdidas|modificada|
|[lyeRL7khixdlJsaf.htm](bestiary-effects/lyeRL7khixdlJsaf.htm)|Effect: Death Gasp|Efecto: Jadeo Mortal|modificada|
|[McawF8weCe7O81um.htm](bestiary-effects/McawF8weCe7O81um.htm)|Effect: Witchflame (Critical Failure)|Efecto: Llama de bruja (Fallo crítico)|modificada|
|[Me16QQGxpivWE2WW.htm](bestiary-effects/Me16QQGxpivWE2WW.htm)|Effect: Gloom Aura|Efecto: Aura Sombría|modificada|
|[mFnSXW6HTwS7Iu1k.htm](bestiary-effects/mFnSXW6HTwS7Iu1k.htm)|Effect: Hunted Fear|Efecto: Temor a ser cazado|modificada|
|[MKC2ne315YiCUiWd.htm](bestiary-effects/MKC2ne315YiCUiWd.htm)|Effect: Inspire Defense (Love Siktempora)|Efecto: Inspirar defensa (Siktempora amorosa).|modificada|
|[mo4IRyv7GGRBJihU.htm](bestiary-effects/mo4IRyv7GGRBJihU.htm)|Effect: Discerning Aura|Efecto: Aura de discernimiento|modificada|
|[MxArXuo8JRCm1hus.htm](bestiary-effects/MxArXuo8JRCm1hus.htm)|Effect: Unsettling Attention|Efecto: Atención Inquietante|modificada|
|[Mz1ihIHaHngdFhsA.htm](bestiary-effects/Mz1ihIHaHngdFhsA.htm)|Effect: Musk|Efecto: Almizcle|modificada|
|[n96DB8vpPr1Fu6GZ.htm](bestiary-effects/n96DB8vpPr1Fu6GZ.htm)|Effect: Smash Kneecaps|Efecto: Aplastar rótulas|modificada|
|[NHwmrSyF0e8HUE3m.htm](bestiary-effects/NHwmrSyF0e8HUE3m.htm)|Effect: Witchflame (Success)|Efecto: Llama de Bruja (Éxito)|modificada|
|[NLjzI7gt6djXSoXc.htm](bestiary-effects/NLjzI7gt6djXSoXc.htm)|Effect: Remove Face|Efecto: Quitar Cara|modificada|
|[No817gKxdliCVQ9K.htm](bestiary-effects/No817gKxdliCVQ9K.htm)|Effect: Energy Ward|Efecto: Energy Ward|modificada|
|[o0zqsH5kYopRanav.htm](bestiary-effects/o0zqsH5kYopRanav.htm)|Effect: Rope Snare|Efecto: Atrapar con cuerda|modificada|
|[o6ZI01zZBQDASuaw.htm](bestiary-effects/o6ZI01zZBQDASuaw.htm)|Effect: Wolf Shape|Efecto: Forma de Lobo|modificada|
|[ob00D61F0c4PIvj1.htm](bestiary-effects/ob00D61F0c4PIvj1.htm)|Effect: Utter Despair|Efecto: Desesperación total|modificada|
|[OMJnZspk5YHai8yn.htm](bestiary-effects/OMJnZspk5YHai8yn.htm)|Effect: Fed by Metal|Efecto: Alimentado por Metal|modificada|
|[oORIapm28xioxPDp.htm](bestiary-effects/oORIapm28xioxPDp.htm)|Effect: Blood Wake|Efecto: Blood Wake|modificada|
|[otUESSH8BDdPKHtb.htm](bestiary-effects/otUESSH8BDdPKHtb.htm)|Effect: Hamstring|Efecto: Tendón|modificada|
|[OxOMYmlPtjsEkRtY.htm](bestiary-effects/OxOMYmlPtjsEkRtY.htm)|Effect: Aura of Command|Efecto: Aura de orden imperiosa|modificada|
|[P4bNtDVyknQwjTPo.htm](bestiary-effects/P4bNtDVyknQwjTPo.htm)|Effect: Runic Resistance|Efecto: Resistencia Rúnica|modificada|
|[PFeGCDFOo2AjI6ib.htm](bestiary-effects/PFeGCDFOo2AjI6ib.htm)|Effect: Ganzi Resistance|Efecto: Resistencia Ganzi|modificada|
|[pfG1S7yShwkHT9e0.htm](bestiary-effects/pfG1S7yShwkHT9e0.htm)|Effect: Calming Bioluminescence|Efecto: Bioluminiscencia calmante.|modificada|
|[PJxUFFeAvQJ4l52f.htm](bestiary-effects/PJxUFFeAvQJ4l52f.htm)|Effect: Choose Weakness|Efecto: Elegir debilidad|modificada|
|[pMPe01GWvfwcYwUR.htm](bestiary-effects/pMPe01GWvfwcYwUR.htm)|Effect: Defensive Assault|Efecto: Asalto defensivo|modificada|
|[pvA97TXnq7wCbXp6.htm](bestiary-effects/pvA97TXnq7wCbXp6.htm)|Effect: Call to Halt (Success)|Efecto: Llamada a Alto (Éxito)|modificada|
|[PxLWRiCbgpqOTLO9.htm](bestiary-effects/PxLWRiCbgpqOTLO9.htm)|Effect: Towering Stance (Disbelieved)|Efecto: Posición de torre (No se puede creer).|modificada|
|[q2D1QBalqBQfKzTc.htm](bestiary-effects/q2D1QBalqBQfKzTc.htm)|Effect: Hurl Net|Efecto: Lanzar red.|modificada|
|[Qa0HP5uCuqh4vxBh.htm](bestiary-effects/Qa0HP5uCuqh4vxBh.htm)|Effect: Consume Organ|Efecto: Consumir Organo|modificada|
|[qddahkHks1iLZ4sM.htm](bestiary-effects/qddahkHks1iLZ4sM.htm)|Effect: Praise Adachros|Efecto: Alabar Adachros|modificada|
|[QDs8t1U1IepzYlyi.htm](bestiary-effects/QDs8t1U1IepzYlyi.htm)|Effect: Sylvan Wine|Efecto: Vino silvano|modificada|
|[qFDiWpzk8cuwQGyH.htm](bestiary-effects/qFDiWpzk8cuwQGyH.htm)|Effect: Hypnotic Stare|Efecto: Mirada hipnótica|modificada|
|[QjgLHIgRXIk3KAOY.htm](bestiary-effects/QjgLHIgRXIk3KAOY.htm)|Effect: Curl Up|Efecto: Enroscarse|modificada|
|[QMcmhifNQOPguv2t.htm](bestiary-effects/QMcmhifNQOPguv2t.htm)|Effect: Ectoplasmic Form (Physical)|Efecto: Forma Ectoplásmica (Física)|modificada|
|[Qomp2EujVCbzJb4X.htm](bestiary-effects/Qomp2EujVCbzJb4X.htm)|Effect: Filth Wave|Efecto: Ola de Suciedad|modificada|
|[QoneHsjZKtGHWlam.htm](bestiary-effects/QoneHsjZKtGHWlam.htm)|Effect: Aura of Misfortune|Efecto: Aura de infortunio|modificada|
|[QpG9AaYpXJzDEjZA.htm](bestiary-effects/QpG9AaYpXJzDEjZA.htm)|Effect: Devour Soul|Efecto: Devorar alma|modificada|
|[QqC9xymSXSVunDgl.htm](bestiary-effects/QqC9xymSXSVunDgl.htm)|Effect: Tail Lash (Attack Roll)|Efecto: Azote con la cola (tirada de ataque).|modificada|
|[QSy3KaLI1AYTR7rP.htm](bestiary-effects/QSy3KaLI1AYTR7rP.htm)|Effect: Wrath of Fate|Efecto: Ira del Destino|modificada|
|[R8clbf7gtIlU3fIj.htm](bestiary-effects/R8clbf7gtIlU3fIj.htm)|Effect: Crush of Hundreds|Efecto: Aplastamiento de Cientos|modificada|
|[RAKgTTFKc2YEbvrc.htm](bestiary-effects/RAKgTTFKc2YEbvrc.htm)|Effect: Hallucinogenic Pollen (Critical Failure)|Efecto: Polen alucinógeno (Fallo crítico)|modificada|
|[RBfd5qwFkuKxNb57.htm](bestiary-effects/RBfd5qwFkuKxNb57.htm)|Effect: Rapid Evolution - Energy Gland|Efecto: Evolución Rápida - Glándula de Energía|modificada|
|[rbgXQqXbpaQCcNNx.htm](bestiary-effects/rbgXQqXbpaQCcNNx.htm)|Effect: Read the Stars (Critical Success)|Efecto: Leer las estrellas (Éxito crítico)|modificada|
|[RjrsiEloXs7ze1TZ.htm](bestiary-effects/RjrsiEloXs7ze1TZ.htm)|Effect: Aura of Vitality|Efecto: Aura de Vitalidad|modificada|
|[Rn1aE4uvq30iiH0Y.htm](bestiary-effects/Rn1aE4uvq30iiH0Y.htm)|Effect: Regression|Efecto: Regresión|modificada|
|[rpxdrOlzY2SOtMWB.htm](bestiary-effects/rpxdrOlzY2SOtMWB.htm)|Effect: Whirlwind Form|Efecto: Forma de torbellino|modificada|
|[rRgVlTHiNEJ86T7N.htm](bestiary-effects/rRgVlTHiNEJ86T7N.htm)|Effect: Nanite Surge (Glow)|Efecto: Oleaje de Nanite (Resplandor)|modificada|
|[ryY6fdqpC5Ztnagd.htm](bestiary-effects/ryY6fdqpC5Ztnagd.htm)|Effect: Dirty Bomb (Critical Failure)|Efecto: Bomba Sucia (Fallo Crítico)|modificada|
|[s16XQpDz2HNzR9BB.htm](bestiary-effects/s16XQpDz2HNzR9BB.htm)|Effect: Aura of Protection (Solar)|Efecto: Aura de protección (Solar).|modificada|
|[S38CM3hyr7DnsmRR.htm](bestiary-effects/S38CM3hyr7DnsmRR.htm)|Effect: Devour Soul (Victim Level 15 or Higher)|Efecto: Devorar alma (nivel de la víctima 15 o superior).|modificada|
|[s5mS7CyE0oOYlecv.htm](bestiary-effects/s5mS7CyE0oOYlecv.htm)|Effect: Lost Red Cap|Efecto: Gorro rojo perdido|modificada|
|[SA5f54t1dVah5fJm.htm](bestiary-effects/SA5f54t1dVah5fJm.htm)|Effect: Revert Form|Efecto: Revertir forma|modificada|
|[ScZ8tlV5zS0Jwnqn.htm](bestiary-effects/ScZ8tlV5zS0Jwnqn.htm)|Effect: Blood Frenzy|Efecto: Frenesí de Sangre|modificada|
|[seWuw1GMSm3kzCWV.htm](bestiary-effects/seWuw1GMSm3kzCWV.htm)|Effect: Fan Bolt|Efecto: Rayo Abanico|modificada|
|[sI73mEP9wZRvMpWw.htm](bestiary-effects/sI73mEP9wZRvMpWw.htm)|Effect: Curse of Frost|Efecto: Maldición de Gélida|modificada|
|[SjrM6XVTLySToS12.htm](bestiary-effects/SjrM6XVTLySToS12.htm)|Effect: Soul Siphon|Efecto: Succionar almas|modificada|
|[St2QzqQvKN0IaXxG.htm](bestiary-effects/St2QzqQvKN0IaXxG.htm)|Effect: Kharna's Blessing|Efecto: Bendición de Kharna|modificada|
|[svXRrZvixRInmabj.htm](bestiary-effects/svXRrZvixRInmabj.htm)|Effect: Rune of Destruction (Greatsword)|Efecto: Runa de Destrucción (Espada Grande)|modificada|
|[T2tfacwoOozQfsIz.htm](bestiary-effects/T2tfacwoOozQfsIz.htm)|Effect: Infectious Aura|Efecto: Aura Infecciosa|modificada|
|[T9wQ1LvsvPWTefQR.htm](bestiary-effects/T9wQ1LvsvPWTefQR.htm)|Effect: Under Command|Efecto: Bajo orden imperiosa|modificada|
|[TbmkcfpKIs558skY.htm](bestiary-effects/TbmkcfpKIs558skY.htm)|Effect: Caustic Mucus|Efecto: Mucosa cáustica|modificada|
|[TcgvhgpUkJFjVqBx.htm](bestiary-effects/TcgvhgpUkJFjVqBx.htm)|Effect: Stink Sap|Efecto: Savia Apestosa|modificada|
|[TEzLEXBZzNCp6flg.htm](bestiary-effects/TEzLEXBZzNCp6flg.htm)|Effect: Instinctual Tinker (Critical Success)|Efecto: Trastear instintivo (Éxito crítico).|modificada|
|[TjRZbd52qWPjTbNT.htm](bestiary-effects/TjRZbd52qWPjTbNT.htm)|Effect: No Quarter!|Efecto: ¡Sin cuartel!|modificada|
|[tJx9B2e3AET6PbJD.htm](bestiary-effects/tJx9B2e3AET6PbJD.htm)|Effect: Pain Frenzy|Efecto: Enfurecido por el dolor|modificada|
|[Tkf33YglEZcZ6gOT.htm](bestiary-effects/Tkf33YglEZcZ6gOT.htm)|Effect: Curse of the Baneful Venom|Efecto: Maldición del veneno nefasto.|modificada|
|[TSdrv1FmLHJnk9AD.htm](bestiary-effects/TSdrv1FmLHJnk9AD.htm)|Effect: Shadow Rapier|Efecto: Estoque de Sombra|modificada|
|[tSF9z5VTeevxoww3.htm](bestiary-effects/tSF9z5VTeevxoww3.htm)|Effect: Harmonizing Aura (Enemies)|Efecto: Aura armonizante (enemigos).|modificada|
|[tvybMInVm415jA3p.htm](bestiary-effects/tvybMInVm415jA3p.htm)|Effect: Bloodline Magic|Efecto: Magia de linaje|modificada|
|[U05L8bPBcRHTtTB3.htm](bestiary-effects/U05L8bPBcRHTtTB3.htm)|Effect: Crushing Spirits|Efecto: Espíritus Aplastantes|modificada|
|[u1Z7i2RyL6Sd9OVU.htm](bestiary-effects/u1Z7i2RyL6Sd9OVU.htm)|Effect: Take Them Down!|Efecto: ¬°¡Abatidlos!|modificada|
|[u2z1BgpZsxl7Zc2z.htm](bestiary-effects/u2z1BgpZsxl7Zc2z.htm)|Effect: Poison Frenzy|Efecto: Frenesí de Veneno|modificada|
|[uB3CVn3momZO9h0L.htm](bestiary-effects/uB3CVn3momZO9h0L.htm)|Effect: Xulgath Stench|Efecto: Hedor de Xulgath|modificada|
|[ucAwSIZsGrpBLg6G.htm](bestiary-effects/ucAwSIZsGrpBLg6G.htm)|Effect: Alchemical Crossbow|Efecto: Ballesta alquímica|modificada|
|[uFi8GhPkYY3ti40N.htm](bestiary-effects/uFi8GhPkYY3ti40N.htm)|Effect: Inspire Courage (Love Siktempora)|Efecto: Inspirar valor (Siktempora amorosa).|modificada|
|[UgoM6FBiZfc7KqCV.htm](bestiary-effects/UgoM6FBiZfc7KqCV.htm)|Effect: Curse of Stone|Efecto: Maldición de Piedra|modificada|
|[UiISDbCjK6AUDsll.htm](bestiary-effects/UiISDbCjK6AUDsll.htm)|Effect: Guide's Warning|Efecto: Advertencia del Guía|modificada|
|[uqjx1bwYduHwwC7d.htm](bestiary-effects/uqjx1bwYduHwwC7d.htm)|Effect: Guiding Words|Efecto: Guiding Words|modificada|
|[us3KiMrcGM8e6ri3.htm](bestiary-effects/us3KiMrcGM8e6ri3.htm)|Effect: Sylvan Wine (Horned Hunter)|Efecto: Vino silvano (Cazador con cuernos)|modificada|
|[USMuxi8ACBJRUrdS.htm](bestiary-effects/USMuxi8ACBJRUrdS.htm)|Effect: Crocodile Form|Efecto: Forma de Cocodrilo|modificada|
|[UuEUZU1fSUb23yOk.htm](bestiary-effects/UuEUZU1fSUb23yOk.htm)|Effect: Totems of the Past|Efecto: Tótems del pasado|modificada|
|[UxeoSRmPbD5hjiEi.htm](bestiary-effects/UxeoSRmPbD5hjiEi.htm)|Effect: Inspiring Presence|Efecto: Presencia Inspiradora|modificada|
|[UxNq9uo1GpDYTi5Z.htm](bestiary-effects/UxNq9uo1GpDYTi5Z.htm)|Effect: Waterlogged|Efecto: Anegado|modificada|
|[uZJOdounIHaFDC1t.htm](bestiary-effects/uZJOdounIHaFDC1t.htm)|Effect: Hydra Heads|Efecto: Cabezas de Hidra|modificada|
|[v1oNhc9xa8nWxmFQ.htm](bestiary-effects/v1oNhc9xa8nWxmFQ.htm)|Effect: Breach the Abyss - Acid|Efecto: Emerger el Abismo - Ácido|modificada|
|[v54mj5jknAynlCM9.htm](bestiary-effects/v54mj5jknAynlCM9.htm)|Effect: Call to Blood|Efecto: Llamada a la Sangre|modificada|
|[V60TWjo1G1r5ZtPM.htm](bestiary-effects/V60TWjo1G1r5ZtPM.htm)|Effect: Vulnerable to Curved Space|Efecto: Vulnerable a Espacio Curvo|modificada|
|[VAy6IZ0ck7ALVL4b.htm](bestiary-effects/VAy6IZ0ck7ALVL4b.htm)|Effect: Rune of Smiting (Greatsword)|Efecto: Runa de Smiting (Greatsword)|modificada|
|[vbxcQIjLVWa43sdt.htm](bestiary-effects/vbxcQIjLVWa43sdt.htm)|Effect: Rune of Destruction (Rock)|Efecto: Runa de Destrucción (Roca)|modificada|
|[vc3AOrJpacJ7T1hO.htm](bestiary-effects/vc3AOrJpacJ7T1hO.htm)|Effect: Deceitful Feast (Failure)|Efecto: Festín Engañoso (Fallo)|modificada|
|[VFIVdYmS3XTdd3hj.htm](bestiary-effects/VFIVdYmS3XTdd3hj.htm)|Effect: Curse of Fire|Efecto: Maldición de Fuego|modificada|
|[vGL5LlFxVJqjy1gM.htm](bestiary-effects/vGL5LlFxVJqjy1gM.htm)|Effect: Breach the Abyss - Cold|Efecto: Emerger el Abismo - Frío|modificada|
|[vJAHjBoEuFKGQoGs.htm](bestiary-effects/vJAHjBoEuFKGQoGs.htm)|Effect: Rune of Destruction (Longspear)|Efecto: Runa de Destrucción (Longspear)|modificada|
|[vKTvlFD2OSgTPuvl.htm](bestiary-effects/vKTvlFD2OSgTPuvl.htm)|Effect: Breach the Abyss - Electricity|Efecto: Emerger el Abismo - Electricidad|modificada|
|[VLOvyQiZKeXppS2L.htm](bestiary-effects/VLOvyQiZKeXppS2L.htm)|Effect: Gird in Prayer|Efecto: Ceñir en Oración|modificada|
|[vuaungtl9VCBoHDK.htm](bestiary-effects/vuaungtl9VCBoHDK.htm)|Effect: Rockfall Failure|Efecto: Fallo en la Caída de Rocas|modificada|
|[vUFIbQ74jz22rOXw.htm](bestiary-effects/vUFIbQ74jz22rOXw.htm)|Effect: Mortal Reflection|Efecto: Reflejo Mortal|modificada|
|[Vz9Cb8FEzNwfH3mE.htm](bestiary-effects/Vz9Cb8FEzNwfH3mE.htm)|Effect: Breach the Abyss - Negative|Efecto: Emerger el Abismo - Negativo|modificada|
|[wcNGbhwn3AuamtsX.htm](bestiary-effects/wcNGbhwn3AuamtsX.htm)|Effect: Scraping Clamor|Efecto: Clamor Rascadura|modificada|
|[WfiaKdXNSxC3POcs.htm](bestiary-effects/WfiaKdXNSxC3POcs.htm)|Effect: Bosun's Command - Attack Bonus|Efecto: Orden imperiosa - Bonificación de ataque|modificada|
|[wHQZO70bSECtSUZn.htm](bestiary-effects/wHQZO70bSECtSUZn.htm)|Effect: Ancestral Journey|Efecto: Viaje Ancestral|modificada|
|[wJsOZsYI2ZUVcGxc.htm](bestiary-effects/wJsOZsYI2ZUVcGxc.htm)|Effect: Fiddle|Efecto: Violín|modificada|
|[wPIGBcpCqCWgrCiq.htm](bestiary-effects/wPIGBcpCqCWgrCiq.htm)|Effect: Blood Soak|Efecto: Blood Soak|modificada|
|[wPW6kVscJgBLPVKZ.htm](bestiary-effects/wPW6kVscJgBLPVKZ.htm)|Effect: Ghonhatine Feed|Efecto: Ghonhatino Alimentarse|modificada|
|[wX9L6fbqVMLP05hn.htm](bestiary-effects/wX9L6fbqVMLP05hn.htm)|Effect: Stench|Efecto: Hedor|modificada|
|[XaYM6Td0yhx2POau.htm](bestiary-effects/XaYM6Td0yhx2POau.htm)|Effect: Haywire|Efecto: Haywire|modificada|
|[XH49blfNDjkqnH5N.htm](bestiary-effects/XH49blfNDjkqnH5N.htm)|Effect: Motivating Assault|Efecto: Asalto Motivador|modificada|
|[xiOi7btey4AS6jhe.htm](bestiary-effects/xiOi7btey4AS6jhe.htm)|Effect: Overtake Soul|Efecto: Superar Alma|modificada|
|[xmES7HVxM7L3j1EN.htm](bestiary-effects/xmES7HVxM7L3j1EN.htm)|Effect: Putrid Stench|Efecto: Hedor pútrido|modificada|
|[XSAmMuMgMvdjdpHc.htm](bestiary-effects/XSAmMuMgMvdjdpHc.htm)|Effect: Slime Trap|Efecto: Trampa de limo|modificada|
|[XSXMGb5h4Um6YXiT.htm](bestiary-effects/XSXMGb5h4Um6YXiT.htm)|Effect: Air of Sickness|Efecto: Aire de Enfermedad|modificada|
|[xXfzzNaBOqDUbYBD.htm](bestiary-effects/xXfzzNaBOqDUbYBD.htm)|Effect: Rotting Stench|Efecto: Hedor putrefacto|modificada|
|[XyhPcnlkg3mGqEwT.htm](bestiary-effects/XyhPcnlkg3mGqEwT.htm)|Effect: Rockfall Critical Failure|Efecto: Rockfall Fallo Crítico|modificada|
|[Y5vsF5Kb8oNIccWo.htm](bestiary-effects/Y5vsF5Kb8oNIccWo.htm)|Effect: Rune of Smiting (Longspear)|Efecto: Runa de Herir (Lanza larga)|modificada|
|[ya8r3IdzQ7QGNH8N.htm](bestiary-effects/ya8r3IdzQ7QGNH8N.htm)|Effect: Activate Defenses|Efecto: Activar Defensas|modificada|
|[YDJnC3Bdc2GI6hX5.htm](bestiary-effects/YDJnC3Bdc2GI6hX5.htm)|Effect: Inspire Envoy|Efecto: Inspirar Enviado|modificada|
|[yDut6pczQVRJqt2L.htm](bestiary-effects/yDut6pczQVRJqt2L.htm)|Effect: PFS Level Bump|Efecto: Subida de Nivel PFS|modificada|
|[yG27Ixwdpxqbrhzf.htm](bestiary-effects/yG27Ixwdpxqbrhzf.htm)|Effect: Bosun's Command - Speed Bonus|Efecto: Orden imperiosa - Bonificación de Velocidad.|modificada|
|[YIXeQFdrt0vPvmsW.htm](bestiary-effects/YIXeQFdrt0vPvmsW.htm)|Effect: Snake Form|Efecto: Forma de Serpiente|modificada|
|[yQd2Yoht8libCMCv.htm](bestiary-effects/yQd2Yoht8libCMCv.htm)|Effect: Fortune's Favor|Efecto: Favor de la Fortuna|modificada|
|[Yqq4AkZ9lrm4CcID.htm](bestiary-effects/Yqq4AkZ9lrm4CcID.htm)|Effect: Battle Cry|Efecto: Grito de guerra|modificada|
|[yrenENpzVgBKsnNi.htm](bestiary-effects/yrenENpzVgBKsnNi.htm)|Effect: Alchemical Strike|Efecto: Impacto alquímico|modificada|
|[yxgHWK2rTZKKAzDN.htm](bestiary-effects/yxgHWK2rTZKKAzDN.htm)|Effect: Unluck Aura|Efecto: Aura de mala suerte|modificada|
|[zBbmuudbWY8FaOwZ.htm](bestiary-effects/zBbmuudbWY8FaOwZ.htm)|Effect: Chug|Efecto: Chug|modificada|
|[zBQM4rzLfkLzpET6.htm](bestiary-effects/zBQM4rzLfkLzpET6.htm)|Effect: Rune of Flames (Greatsword)|Efecto: Runa de Flamígera (Greatsword)|modificada|
|[zGWc4q0rXW9eMoYT.htm](bestiary-effects/zGWc4q0rXW9eMoYT.htm)|Effect: Phantasmagoric Fog|Efecto: Niebla fantasmagórica|modificada|
|[zNOomUXHZHcc1PWD.htm](bestiary-effects/zNOomUXHZHcc1PWD.htm)|Effect: Vengeful Fibers|Efecto: Fibras vengativas|modificada|
|[zqgntQXIEbulBgZX.htm](bestiary-effects/zqgntQXIEbulBgZX.htm)|Effect: Share Defenses|Efecto: Compartir Defensas|modificada|
|[zTrZozbWDd1A5L5S.htm](bestiary-effects/zTrZozbWDd1A5L5S.htm)|Effect: Cat Sith's Mark|Efecto: Marca del Gato Sith|modificada|
|[zXIpCkTtFRkhwI4x.htm](bestiary-effects/zXIpCkTtFRkhwI4x.htm)|Effect: Sixfold Flurry|Efecto: Ráfaga séxtuple|modificada|
|[zXzXDlh0HkyPJu8f.htm](bestiary-effects/zXzXDlh0HkyPJu8f.htm)|Effect: Ill Omen|Efecto: Mal agüero|modificada|
|[zzdOof9hHUf9s13H.htm](bestiary-effects/zzdOof9hHUf9s13H.htm)|Effect: Manipulate Luck (Bad)|Efecto: Manipular Suerte (Malo)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
