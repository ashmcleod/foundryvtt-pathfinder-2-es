# Estado de la traducción (adventure-specific-actions)

 * **modificada**: 92
 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[MFyKaQVMMphwpHut.htm](adventure-specific-actions/MFyKaQVMMphwpHut.htm)|Explore the Electric Castle|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0nyMrziUqqRcSxXD.htm](adventure-specific-actions/0nyMrziUqqRcSxXD.htm)|Fight Fires|Combatir Incendios|modificada|
|[0z53rJzCIGysXGTy.htm](adventure-specific-actions/0z53rJzCIGysXGTy.htm)|Fire the Cannons! (5-6)|¡Disparen los Cañones! (5-6)|modificada|
|[236E6kEEayy8h2CF.htm](adventure-specific-actions/236E6kEEayy8h2CF.htm)|Position the Hunters|Posicionar a los Cazadores|modificada|
|[2Kz5whXqnRsL4fEl.htm](adventure-specific-actions/2Kz5whXqnRsL4fEl.htm)|Practical Research|Investigación Práctica|modificada|
|[2Vrg2RkEdOteyp5O.htm](adventure-specific-actions/2Vrg2RkEdOteyp5O.htm)|Perform a Trick|Interpretar un truco|modificada|
|[3E5TMUDCSEI5x2bv.htm](adventure-specific-actions/3E5TMUDCSEI5x2bv.htm)|Smooth the Path|Suavizar el camino|modificada|
|[3P0rSASN69wXV6Fw.htm](adventure-specific-actions/3P0rSASN69wXV6Fw.htm)|Approach Duneshadow|Acercarse a Duneshadow|modificada|
|[3Pr6Jk6AobttQRqN.htm](adventure-specific-actions/3Pr6Jk6AobttQRqN.htm)|Forge Documents|Forja Documentos|modificada|
|[43vVOSRi8Lnk1Ril.htm](adventure-specific-actions/43vVOSRi8Lnk1Ril.htm)|Investigate Chamber|Investigar Cámara|modificada|
|[52zcawpATnCLh4J9.htm](adventure-specific-actions/52zcawpATnCLh4J9.htm)|Send in the Clowns|Recado de los payasos|modificada|
|[56ajIlNy3SEvP9Ud.htm](adventure-specific-actions/56ajIlNy3SEvP9Ud.htm)|Costar|Costar|modificada|
|[5DVnTrN2xt3NINGZ.htm](adventure-specific-actions/5DVnTrN2xt3NINGZ.htm)|Make General Repairs|Hacer Reparaciones Generales|modificada|
|[5QQqv1A3aG6G2Sui.htm](adventure-specific-actions/5QQqv1A3aG6G2Sui.htm)|Rebuild Collapsed Stairs|Rebuild Collapsed Stairs|modificada|
|[6hrDW8tF6CDIaZl7.htm](adventure-specific-actions/6hrDW8tF6CDIaZl7.htm)|Dream Research|Investigación del Sue|modificada|
|[764EVGVADrDSbdqu.htm](adventure-specific-actions/764EVGVADrDSbdqu.htm)|Forgive Foe|Forgive Foe|modificada|
|[799p70dXF3UYkTih.htm](adventure-specific-actions/799p70dXF3UYkTih.htm)|Communicate From Beyond|Comunicar Desde el Más Allá|modificada|
|[7WczIXwjOs9raMja.htm](adventure-specific-actions/7WczIXwjOs9raMja.htm)|Fey Luck|Suerte feérica|modificada|
|[9NWMchSkF940L0SW.htm](adventure-specific-actions/9NWMchSkF940L0SW.htm)|Invoke Eiseth|Invoke Eiseth|modificada|
|[aCFKA59YgGDNnSJ8.htm](adventure-specific-actions/aCFKA59YgGDNnSJ8.htm)|Exhale Poison|Exhala Veneno|modificada|
|[aUgAKW3SWhU9ATl8.htm](adventure-specific-actions/aUgAKW3SWhU9ATl8.htm)|Repair Crumbled Walls|Reparar Paredes Desmoronadas|modificada|
|[AvpKHblov4xjANaH.htm](adventure-specific-actions/AvpKHblov4xjANaH.htm)|Build Training Facility|Construir Instalación de Entrenamiento|modificada|
|[aw2JzLBAsmO8dD3r.htm](adventure-specific-actions/aw2JzLBAsmO8dD3r.htm)|Fire the Cannons! (3-4)|¡Disparen los cañones! (3-4)|modificada|
|[awJY2ylrsVEJNTFi.htm](adventure-specific-actions/awJY2ylrsVEJNTFi.htm)|Pander to the Crowd|Pander to the Crowd|modificada|
|[Bh1Qnh8JP4CMysRK.htm](adventure-specific-actions/Bh1Qnh8JP4CMysRK.htm)|Shootist's Draw|Shootist's Draw|modificada|
|[BnKNrkcM6F3v0p7s.htm](adventure-specific-actions/BnKNrkcM6F3v0p7s.htm)|Haul Supplies|Haul Supplies|modificada|
|[Bw2L4guiRFphTpF1.htm](adventure-specific-actions/Bw2L4guiRFphTpF1.htm)|Appeal to Shadowy Intruders|Appeal to Shadowy Intruders|modificada|
|[cGgeAlsnsSQPSPDQ.htm](adventure-specific-actions/cGgeAlsnsSQPSPDQ.htm)|Upgrade Defenses|Mejorar Defensas|modificada|
|[cp8eFBCr1n7xIaxq.htm](adventure-specific-actions/cp8eFBCr1n7xIaxq.htm)|Rebuild Battlements|Rebuild Battlements|modificada|
|[CRiItJtc8N9Hc0X0.htm](adventure-specific-actions/CRiItJtc8N9Hc0X0.htm)|Explore the Vault of Boundless Wonder|Explora la Bóveda de las Maravillas Infinitas|modificada|
|[Dir7OyCss7H1XQGX.htm](adventure-specific-actions/Dir7OyCss7H1XQGX.htm)|Organize Labor|Organizar mano de obra|modificada|
|[dqE9pP9Pv0JG9d8X.htm](adventure-specific-actions/dqE9pP9Pv0JG9d8X.htm)|Smuggled|Contrabando|modificada|
|[EUU6zUeCzYm9jIhT.htm](adventure-specific-actions/EUU6zUeCzYm9jIhT.htm)|Find the Cells|Encontrar las Células|modificada|
|[FLz8SEF0Y4UEavvD.htm](adventure-specific-actions/FLz8SEF0Y4UEavvD.htm)|Hunt the Animals|Caza a los animales|modificada|
|[fUNCyoyLgpIYFLe1.htm](adventure-specific-actions/fUNCyoyLgpIYFLe1.htm)|Influence Guild|Gremio de influencia|modificada|
|[FyT7VwMCJjjHDSgO.htm](adventure-specific-actions/FyT7VwMCJjjHDSgO.htm)|Contact Steel Falcons|Contacta con Halcones de Acero|modificada|
|[G8xZPhzoLF1SGyV9.htm](adventure-specific-actions/G8xZPhzoLF1SGyV9.htm)|Diviner on Duty|Adivinación de servicio|modificada|
|[GhahZSxNPSrabaA6.htm](adventure-specific-actions/GhahZSxNPSrabaA6.htm)|Scout the Facility|Explorar la Instalación|modificada|
|[HcASfeYcE8QXayfk.htm](adventure-specific-actions/HcASfeYcE8QXayfk.htm)|Check the Walls|Comprobar las Paredes|modificada|
|[hQ5BAIjAKpp2dYhR.htm](adventure-specific-actions/hQ5BAIjAKpp2dYhR.htm)|Build Connections|Crea Conexión|modificada|
|[hq5KwJRPbTVcoD3k.htm](adventure-specific-actions/hq5KwJRPbTVcoD3k.htm)|Dispel a Disguise|Disipar un Disfraz|modificada|
|[hvvzc86tW5MgElMB.htm](adventure-specific-actions/hvvzc86tW5MgElMB.htm)|Assessing Tatzlford's Defenses|Evaluando las defensas de Tatzlford|modificada|
|[I19VNyhXYaFCpsxl.htm](adventure-specific-actions/I19VNyhXYaFCpsxl.htm)|Scout Duneshadow|Explorar Duneshadow|modificada|
|[IQewgylxmkmBYcoY.htm](adventure-specific-actions/IQewgylxmkmBYcoY.htm)|Secure Invitation|Invitación Segura|modificada|
|[iQfHTjg9dNLbuzr8.htm](adventure-specific-actions/iQfHTjg9dNLbuzr8.htm)|Clear Courtyard|Despejar Patio|modificada|
|[IuD5u9tSTabZ0KD1.htm](adventure-specific-actions/IuD5u9tSTabZ0KD1.htm)|Build Library|Build Library|modificada|
|[jwo5CvftA5puYp7i.htm](adventure-specific-actions/jwo5CvftA5puYp7i.htm)|Mesmerizing Performance|Mesmerizing Interpretar|modificada|
|[KIU5eDZP9VyQIfas.htm](adventure-specific-actions/KIU5eDZP9VyQIfas.htm)|Protector's Interdiction|Protector's Interdiction|modificada|
|[KmpPAOjNP980NuCY.htm](adventure-specific-actions/KmpPAOjNP980NuCY.htm)|Repair Huntergate|Reparar Huntergate|modificada|
|[KsYvgnBNvdC23gnC.htm](adventure-specific-actions/KsYvgnBNvdC23gnC.htm)|Erect Barricades|Erigir Barricadas|modificada|
|[L8UbCtOYzgEutput.htm](adventure-specific-actions/L8UbCtOYzgEutput.htm)|Fight the Fire|Fight the Fire|modificada|
|[lD2RA75awEu4cG7e.htm](adventure-specific-actions/lD2RA75awEu4cG7e.htm)|Promote the Circus|Promover el Circo|modificada|
|[LRuwz61jNmIfQYby.htm](adventure-specific-actions/LRuwz61jNmIfQYby.htm)|Shed Time|Tiempo de cobertizo|modificada|
|[lySoX0VbIaEEEnDZ.htm](adventure-specific-actions/lySoX0VbIaEEEnDZ.htm)|Clean|Limpio|modificada|
|[m9Si1ygkv9ISjKVN.htm](adventure-specific-actions/m9Si1ygkv9ISjKVN.htm)|Navigate Steamgrotto|Navegar Steamgrotto|modificada|
|[MgSwLes5lp3TE1ZV.htm](adventure-specific-actions/MgSwLes5lp3TE1ZV.htm)|Mental Ward|Mental Ward|modificada|
|[mluc8JLd20HjGrqu.htm](adventure-specific-actions/mluc8JLd20HjGrqu.htm)|Convince Mengkare|Convencer a Mengkare|modificada|
|[nLLgAxo4IHebsyg1.htm](adventure-specific-actions/nLLgAxo4IHebsyg1.htm)|Deadly Traps|Trampas Letales|modificada|
|[nmgmPqExUZt5u5Wr.htm](adventure-specific-actions/nmgmPqExUZt5u5Wr.htm)|Rotate the Wheel|Gira la Rueda|modificada|
|[nVdoKUIWaJ47xMuB.htm](adventure-specific-actions/nVdoKUIWaJ47xMuB.htm)|Distract Guards|Distraer guardias|modificada|
|[o3u4snDwjBDcNlG3.htm](adventure-specific-actions/o3u4snDwjBDcNlG3.htm)|Topple Crates|Topple Crates|modificada|
|[O4MWAxAYxNkndVnt.htm](adventure-specific-actions/O4MWAxAYxNkndVnt.htm)|Shortcut Through the Wastes|Shortcut Through the Wastes|modificada|
|[o6hu1my40jfcLHqD.htm](adventure-specific-actions/o6hu1my40jfcLHqD.htm)|Host Event|Evento Anfitrión|modificada|
|[OSFi8oH5ndLgnksD.htm](adventure-specific-actions/OSFi8oH5ndLgnksD.htm)|Search the Laughing Jungle|Registrar la Selva de la Risa|modificada|
|[OV77buFW6zHl4Smo.htm](adventure-specific-actions/OV77buFW6zHl4Smo.htm)|Breaking and Entering|Allanamiento de morada|modificada|
|[OxRT8qhujKG9Rhb2.htm](adventure-specific-actions/OxRT8qhujKG9Rhb2.htm)|Build Infirmary|Construir Enfermería|modificada|
|[pQIgAdZucEEWCMfL.htm](adventure-specific-actions/pQIgAdZucEEWCMfL.htm)|Gather Information|Reunir información|modificada|
|[pWxRtCeAw4XnyzoM.htm](adventure-specific-actions/pWxRtCeAw4XnyzoM.htm)|Post Snipers|Post Francotiradores|modificada|
|[Qkm7jcKPA3elk9Nx.htm](adventure-specific-actions/Qkm7jcKPA3elk9Nx.htm)|Administer|Administrar|modificada|
|[QW2gYIKce3W31xXf.htm](adventure-specific-actions/QW2gYIKce3W31xXf.htm)|Prove Peace|Probar la paz|modificada|
|[R90HXdiRwl0Fa4wb.htm](adventure-specific-actions/R90HXdiRwl0Fa4wb.htm)|Loot the Vaults|Saquea las Bóvedas|modificada|
|[rI9qKyONeMPtajZ8.htm](adventure-specific-actions/rI9qKyONeMPtajZ8.htm)|Build Workshop (Crafting)|Construir Taller (Artesanía)|modificada|
|[rKWfjflS15KEB3Yt.htm](adventure-specific-actions/rKWfjflS15KEB3Yt.htm)|De-Animating Gestures (False)|De-Animating Gestures (Falso)|modificada|
|[rR8UYHHpo2Rffi1p.htm](adventure-specific-actions/rR8UYHHpo2Rffi1p.htm)|De-Animating Gestures (True)|Gestos de Desanimación (Verdadero)|modificada|
|[RX62MAyEUtuHMNBm.htm](adventure-specific-actions/RX62MAyEUtuHMNBm.htm)|Guild Investigation|Guild Investigar|modificada|
|[sT8EpCnySUSiBqBp.htm](adventure-specific-actions/sT8EpCnySUSiBqBp.htm)|Locked Doors|Puertas Cerradas|modificada|
|[sWTvJahHpdz4CC6E.htm](adventure-specific-actions/sWTvJahHpdz4CC6E.htm)|Recruit Wildlife|Reclutar fauna|modificada|
|[tay92zbn04IR40qv.htm](adventure-specific-actions/tay92zbn04IR40qv.htm)|Prepare Firepots|Prepare Firepots|modificada|
|[TM4pOPSM9r7XEM64.htm](adventure-specific-actions/TM4pOPSM9r7XEM64.htm)|Soul Ward|Soul Ward|modificada|
|[Vt6CuD83hPiyoiOZ.htm](adventure-specific-actions/Vt6CuD83hPiyoiOZ.htm)|Study|Estudia|modificada|
|[WKFZlmmuZGnucRen.htm](adventure-specific-actions/WKFZlmmuZGnucRen.htm)|Breaking the Chains|Romper las cadenas|modificada|
|[WlXmO2kwyWGgAuOv.htm](adventure-specific-actions/WlXmO2kwyWGgAuOv.htm)|Deduce Traditions|Deducir Tradiciones|modificada|
|[x5hIMfjmsDlpQWyt.htm](adventure-specific-actions/x5hIMfjmsDlpQWyt.htm)|Blend In|Blend In|modificada|
|[xklnt1hmdFnix64F.htm](adventure-specific-actions/xklnt1hmdFnix64F.htm)|Rescue Citizens|Rescatar Ciudadanos|modificada|
|[XTDd73QzhETeXY3g.htm](adventure-specific-actions/XTDd73QzhETeXY3g.htm)|Secure Disguises|Disfraz Seguro|modificada|
|[XUNM9eqfhnSaPVov.htm](adventure-specific-actions/XUNM9eqfhnSaPVov.htm)|Steal Keys|Sustraer Llaves|modificada|
|[XyfC1zPgSTP01ZUr.htm](adventure-specific-actions/XyfC1zPgSTP01ZUr.htm)|Cram|Cram|modificada|
|[Ys6rFLuyxocQy2hA.htm](adventure-specific-actions/Ys6rFLuyxocQy2hA.htm)|Seek the Hidden Forge|Buscar la Fragua Escondida|modificada|
|[YvSjpOAI9bCDQU5h.htm](adventure-specific-actions/YvSjpOAI9bCDQU5h.htm)|Seek the Animals|Buscar a los Animales|modificada|
|[yy5PeDyV7kIWlPOU.htm](adventure-specific-actions/yy5PeDyV7kIWlPOU.htm)|Influence Regent|Influencia Regente|modificada|
|[z1noqZiavhnqQL50.htm](adventure-specific-actions/z1noqZiavhnqQL50.htm)|Set Traps|Poner Trampas|modificada|
|[zF3q1nTANa5NYYJu.htm](adventure-specific-actions/zF3q1nTANa5NYYJu.htm)|Cunning Disguise|Astucia Disfraz|modificada|
|[zIwbbth7qyKraiWV.htm](adventure-specific-actions/zIwbbth7qyKraiWV.htm)|Issue Challenge|Emitir Desafío|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
