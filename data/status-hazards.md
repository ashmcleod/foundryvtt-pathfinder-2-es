# Estado de la traducción (hazards)

 * **modificada**: 53


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0dg9YrjsDi6Ap3jF.htm](hazards/0dg9YrjsDi6Ap3jF.htm)|Web Lurker Deadfall|Web Lurker Deadfall|modificada|
|[1egK7HFAFdHxMS9N.htm](hazards/1egK7HFAFdHxMS9N.htm)|Sportlebore|Sportlebore|modificada|
|[2GAOUxDfoA48uCWP.htm](hazards/2GAOUxDfoA48uCWP.htm)|Fireball Rune|Runa de bola de fuego|modificada|
|[491qhVbjsHnOuMZW.htm](hazards/491qhVbjsHnOuMZW.htm)|Electric Latch Rune|Runa de pestillo eléctrico|modificada|
|[4jAJh3cTLIgkqtou.htm](hazards/4jAJh3cTLIgkqtou.htm)|Malevolent Mannequins|Malevolent Mannequins|modificada|
|[4jJxLlimyQaIVXlt.htm](hazards/4jJxLlimyQaIVXlt.htm)|Darkside Mirror|Espejo al lado oscuro|modificada|
|[4O7wKZdeAemTEbvG.htm](hazards/4O7wKZdeAemTEbvG.htm)|Slamming Door|Portazo|modificada|
|[6In2S3lDnxNgZ2np.htm](hazards/6In2S3lDnxNgZ2np.htm)|Titanic Flytrap|Atrapamoscas titánico|modificada|
|[7VqibTAEXXX6PIhh.htm](hazards/7VqibTAEXXX6PIhh.htm)|Scythe Blades|Cuchillas de guada|modificada|
|[8ewUvJlvn6LVjoXJ.htm](hazards/8ewUvJlvn6LVjoXJ.htm)|Second Chance|Segunda oportunidad|modificada|
|[8gAoSgBJN8QqzP1R.htm](hazards/8gAoSgBJN8QqzP1R.htm)|Frozen Moment|Momento congelado|modificada|
|[98rS64gLzy1ReXoR.htm](hazards/98rS64gLzy1ReXoR.htm)|Hampering Web|Hampering Web|modificada|
|[A93flWUsot3FmC7t.htm](hazards/A93flWUsot3FmC7t.htm)|Yellow Mold|Moho amarillo|modificada|
|[AM3YY2Zfe2ChJHd7.htm](hazards/AM3YY2Zfe2ChJHd7.htm)|Telekinetic Swarm Trap|Trampa de plaga telecinética|modificada|
|[BHq5wpQU8hQEke8D.htm](hazards/BHq5wpQU8hQEke8D.htm)|Hidden Pit|Foso escondido|modificada|
|[BsZ6o2YrwVovKfNh.htm](hazards/BsZ6o2YrwVovKfNh.htm)|Armageddon Orb|Orbe del Armaggedon|modificada|
|[C6nFe8SCWJ8FmLOT.htm](hazards/C6nFe8SCWJ8FmLOT.htm)|Quicksand|Arenas movedizas|modificada|
|[d3YklujLpBFC5HfB.htm](hazards/d3YklujLpBFC5HfB.htm)|Ghostly Choir|Ghostly Choir|modificada|
|[g9HovYB4pfHgIML9.htm](hazards/g9HovYB4pfHgIML9.htm)|Flensing Blades|Flensing Blades|modificada|
|[gB9WkJtH88jJQa5Z.htm](hazards/gB9WkJtH88jJQa5Z.htm)|Plummeting Doom|La perdición en picado|modificada|
|[gFt2nzQrVgXM9tmJ.htm](hazards/gFt2nzQrVgXM9tmJ.htm)|Treacherous Scree|Treacherous Scree|modificada|
|[H2GX04CQXLPQHT8h.htm](hazards/H2GX04CQXLPQHT8h.htm)|Wheel Of Misery|Rueda de la miseria|modificada|
|[H8CPGJn81JSTCRNx.htm](hazards/H8CPGJn81JSTCRNx.htm)|Polymorph Trap|Trampa de polimorfismo|modificada|
|[HnPd9Vqh5NHKEdRq.htm](hazards/HnPd9Vqh5NHKEdRq.htm)|Spinning Blade Pillar|Pilar de cuchilla giratoria|modificada|
|[inUlZWE1isqnTRc5.htm](hazards/inUlZWE1isqnTRc5.htm)|Jealous Abjurer|Abjurador Celoso|modificada|
|[J4YChuob7MIPT5Mq.htm](hazards/J4YChuob7MIPT5Mq.htm)|Vorpal Executioner|Ejecutor vorpalino|modificada|
|[lgyGhpyPosriQUzE.htm](hazards/lgyGhpyPosriQUzE.htm)|Perilous Flash Flood|Perilous Flash Flood|modificada|
|[LLPsEKLoVmoPleJS.htm](hazards/LLPsEKLoVmoPleJS.htm)|Green Slime|Baba Verde|modificada|
|[lVqVDjXnHboMif7F.htm](hazards/lVqVDjXnHboMif7F.htm)|Poisoned Dart Gallery|Galería de dardos envenenados|modificada|
|[m4PRYxFq9ojcwesh.htm](hazards/m4PRYxFq9ojcwesh.htm)|Pharaoh's Ward|Pharaoh's Ward|modificada|
|[mMXHyWdmmAN0GPvG.htm](hazards/mMXHyWdmmAN0GPvG.htm)|Banshee's Symphony|Sinfonía de la banshee|modificada|
|[mWhmhYBHH9X1Ebb9.htm](hazards/mWhmhYBHH9X1Ebb9.htm)|Insistent Privacy Fence|Valla de privacidad insistente|modificada|
|[nO4osrBRnpWKFCMP.htm](hazards/nO4osrBRnpWKFCMP.htm)|Summoning Rune|Runa de convocación|modificada|
|[O0qA1ElCOgYGEBtL.htm](hazards/O0qA1ElCOgYGEBtL.htm)|Eternal Flame|Llama Eterna|modificada|
|[OekigjNLNp9XENjx.htm](hazards/OekigjNLNp9XENjx.htm)|Drowning Pit|Foso de ahogamiento|modificada|
|[oNLgR1iq6MVvNRWo.htm](hazards/oNLgR1iq6MVvNRWo.htm)|Lava Flume Tube|Tubo de canal de lava|modificada|
|[Or0jjL8xS3GyiMq0.htm](hazards/Or0jjL8xS3GyiMq0.htm)|Web Lurker Noose|Lazo acechador de telara|modificada|
|[OSPYSuckHhHl4Cr9.htm](hazards/OSPYSuckHhHl4Cr9.htm)|Steam Vents|Expulsar gases|modificada|
|[Q8bXKgDm8eguqThB.htm](hazards/Q8bXKgDm8eguqThB.htm)|Brown Mold|Moho Marrón|modificada|
|[sFaAWmy1szDRmFtk.htm](hazards/sFaAWmy1szDRmFtk.htm)|Confounding Betrayal|Confounding Betrayal|modificada|
|[siylw0zIh1g4VnCW.htm](hazards/siylw0zIh1g4VnCW.htm)|Grasp Of The Damned|Grasp Of The Damned|modificada|
|[su4TRBOoso4vjkoK.htm](hazards/su4TRBOoso4vjkoK.htm)|Bloodthirsty Urge|Ansia sanguinaria|modificada|
|[tbwGr6FIr5WpvQ6l.htm](hazards/tbwGr6FIr5WpvQ6l.htm)|Hammer Of Forbiddance|Martillo de interdicción|modificada|
|[uEZ4Jv2wNyukJTRL.htm](hazards/uEZ4Jv2wNyukJTRL.htm)|Hallucination Powder Trap|Trampa de polvo alucinciógeno|modificada|
|[Uw2iVgbbeyn3mOjt.htm](hazards/Uw2iVgbbeyn3mOjt.htm)|Spectral Reflection|Spectral Reflection|modificada|
|[v2xIxZ9ZZ6fJyATF.htm](hazards/v2xIxZ9ZZ6fJyATF.htm)|Poisoned Lock|Cerradura envenenada|modificada|
|[VA4VL3kVUxBYbwRf.htm](hazards/VA4VL3kVUxBYbwRf.htm)|Snowfall|Nevadas|modificada|
|[vlMuFskctUvjJe8X.htm](hazards/vlMuFskctUvjJe8X.htm)|Spear Launcher|Arrojador de lanzas|modificada|
|[vTdWEBzJzltMM6r4.htm](hazards/vTdWEBzJzltMM6r4.htm)|Shrieker|Shrieker|modificada|
|[wHUIZ5QhG37cRzSV.htm](hazards/wHUIZ5QhG37cRzSV.htm)|Gravehall Trap|Gravehall Trap|modificada|
|[xkqjwu1ox0pQLOnb.htm](hazards/xkqjwu1ox0pQLOnb.htm)|Bottomless Pit|Pozo sin fondo|modificada|
|[yM4G2LvMwvkIRx0G.htm](hazards/yM4G2LvMwvkIRx0G.htm)|Planar Rift|Grieta planaria|modificada|
|[ZUCGvc2dTJUlM9dC.htm](hazards/ZUCGvc2dTJUlM9dC.htm)|Dance Of Death|Danzante de la Muerte|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
