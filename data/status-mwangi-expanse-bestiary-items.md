# Estado de la traducción (mwangi-expanse-bestiary-items)

 * **vacía**: 2
 * **ninguna**: 7


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[boazYiEU4yxCfnQA.htm](mwangi-expanse-bestiary-items/boazYiEU4yxCfnQA.htm)|Grab|
|[CAUgTX44PLXhK1z6.htm](mwangi-expanse-bestiary-items/CAUgTX44PLXhK1z6.htm)|Darkvision|
|[IFr3MHYmCrmlI0NY.htm](mwangi-expanse-bestiary-items/IFr3MHYmCrmlI0NY.htm)|Scent (Imprecise) 60 feet|
|[qqYEHc1PVdtmltg3.htm](mwangi-expanse-bestiary-items/qqYEHc1PVdtmltg3.htm)|Gilded Fascination|
|[tFpAHyH73IXBqT5F.htm](mwangi-expanse-bestiary-items/tFpAHyH73IXBqT5F.htm)|Pounce|
|[tYIw3txzXdYOMvO4.htm](mwangi-expanse-bestiary-items/tYIw3txzXdYOMvO4.htm)|Half Steps|
|[uFV9awWQUUTLzpt8.htm](mwangi-expanse-bestiary-items/uFV9awWQUUTLzpt8.htm)|Half-Speaker|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[6C2GqTT5fDsCyi7g.htm](mwangi-expanse-bestiary-items/6C2GqTT5fDsCyi7g.htm)|Jaws|vacía|
|[TQfSl3IbFAv26t8Q.htm](mwangi-expanse-bestiary-items/TQfSl3IbFAv26t8Q.htm)|Fist|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
