# Estado de la traducción (highhelm-bestiary-items)

 * **ninguna**: 8


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[AOVmlDAyLWPLQ3Es.htm](highhelm-bestiary-items/AOVmlDAyLWPLQ3Es.htm)|Demon Shape|
|[fv7KJFbrBRTvqefI.htm](highhelm-bestiary-items/fv7KJFbrBRTvqefI.htm)|Pincer|
|[gQO4Q9FL0lmy1nEE.htm](highhelm-bestiary-items/gQO4Q9FL0lmy1nEE.htm)|Tremorsense 60 feet|
|[HJNwa0UJhR7KxLCJ.htm](highhelm-bestiary-items/HJNwa0UJhR7KxLCJ.htm)|Biting Swarm|
|[spDqiGTl7PbBDxUU.htm](highhelm-bestiary-items/spDqiGTl7PbBDxUU.htm)|Bloodsense 120 feet|
|[V2KUQn6veEirBu7B.htm](highhelm-bestiary-items/V2KUQn6veEirBu7B.htm)|Improved Grab|
|[xitZqvPQVHDfsIC7.htm](highhelm-bestiary-items/xitZqvPQVHDfsIC7.htm)|Biting Constrict|
|[xS0LL49WhRi5SrNm.htm](highhelm-bestiary-items/xS0LL49WhRi5SrNm.htm)|Feeding Frenzy|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
