# Estado de la traducción (pathfinder-bestiary-3-items)

 * **ninguna**: 347
 * **modificada**: 2925
 * **vacía**: 136


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[01e3LF3IBDnqhKXi.htm](pathfinder-bestiary-3-items/01e3LF3IBDnqhKXi.htm)|Magic Aura (Constant) (Shaukeen and its Items Only)|
|[025oERI6JXgOoPWL.htm](pathfinder-bestiary-3-items/025oERI6JXgOoPWL.htm)|Pass Without Trace (Constant)|
|[0CCLws9uCZ7vqR3l.htm](pathfinder-bestiary-3-items/0CCLws9uCZ7vqR3l.htm)|Air Walk (At Will)|
|[0FzHP2R1UYaIKxeF.htm](pathfinder-bestiary-3-items/0FzHP2R1UYaIKxeF.htm)|Clairvoyance (At Will)|
|[0IiLvdj1EuziMc4L.htm](pathfinder-bestiary-3-items/0IiLvdj1EuziMc4L.htm)|Tongues (Constant)|
|[0iZv2VZwkbyp6ufE.htm](pathfinder-bestiary-3-items/0iZv2VZwkbyp6ufE.htm)|Entangle|
|[0lXyiooxNtOuTiyT.htm](pathfinder-bestiary-3-items/0lXyiooxNtOuTiyT.htm)|Dimension Door (At Will)|
|[0zFvRc93Gl6eANug.htm](pathfinder-bestiary-3-items/0zFvRc93Gl6eANug.htm)|Halberd|+1|
|[1b748L6U33YBgL1V.htm](pathfinder-bestiary-3-items/1b748L6U33YBgL1V.htm)|Undetectable Alignment (Constant)|
|[1DAzbaSEKgnFKzqV.htm](pathfinder-bestiary-3-items/1DAzbaSEKgnFKzqV.htm)|True Seeing (Constant)|
|[1elKyf9JIeMq5jrZ.htm](pathfinder-bestiary-3-items/1elKyf9JIeMq5jrZ.htm)|Fan Bolts|
|[1jFj68yQe0moSwH1.htm](pathfinder-bestiary-3-items/1jFj68yQe0moSwH1.htm)|Circle of Protection (Constant)|
|[1mdGEjHdiIWq524G.htm](pathfinder-bestiary-3-items/1mdGEjHdiIWq524G.htm)|Air Walk (Constant)|
|[1rv8rr7V1SPtaWM4.htm](pathfinder-bestiary-3-items/1rv8rr7V1SPtaWM4.htm)|True Seeing (Constant)|
|[1UZfCsTCiCQdrzdC.htm](pathfinder-bestiary-3-items/1UZfCsTCiCQdrzdC.htm)|Dream Message (At Will)|
|[1VZg8BVuvg7BFe9c.htm](pathfinder-bestiary-3-items/1VZg8BVuvg7BFe9c.htm)|Earthbind (At Will)|
|[1WtEtbp6ITuVjGUO.htm](pathfinder-bestiary-3-items/1WtEtbp6ITuVjGUO.htm)|Water Walk (Constant)|
|[1YwW47mP6ygWjXlh.htm](pathfinder-bestiary-3-items/1YwW47mP6ygWjXlh.htm)|+2 Resilient Glamered Full Plate|
|[2dwXilE1hqrwdBdk.htm](pathfinder-bestiary-3-items/2dwXilE1hqrwdBdk.htm)|Spear|+1,striking|
|[2EhHblW8AgWdPhB8.htm](pathfinder-bestiary-3-items/2EhHblW8AgWdPhB8.htm)|Detect Alignment (At Will) (Evil Only)|
|[2etpUEez5sWOqlMb.htm](pathfinder-bestiary-3-items/2etpUEez5sWOqlMb.htm)|True Seeing (Constant)|
|[2if951NcJCXU3XXv.htm](pathfinder-bestiary-3-items/2if951NcJCXU3XXv.htm)|Resurrect (Doesn't Require Secondary Casters)|
|[2m8xZRm2hISAn5U0.htm](pathfinder-bestiary-3-items/2m8xZRm2hISAn5U0.htm)|Charm (Undead Only)|
|[2nQjq9UXbBGNp21e.htm](pathfinder-bestiary-3-items/2nQjq9UXbBGNp21e.htm)|Detect Alignment (At Will)|
|[2tww80yq8tBSw5KT.htm](pathfinder-bestiary-3-items/2tww80yq8tBSw5KT.htm)|Blowgun Darts with Harvester Poison|
|[3Lzyr6PVBTQSPZzD.htm](pathfinder-bestiary-3-items/3Lzyr6PVBTQSPZzD.htm)|Flail|+1,striking|
|[3YK02GIFVfg4gqYs.htm](pathfinder-bestiary-3-items/3YK02GIFVfg4gqYs.htm)|Plane Shift (At Will)|
|[41XCv8P4PnillpYy.htm](pathfinder-bestiary-3-items/41XCv8P4PnillpYy.htm)|Longsword|+1,striking|
|[4rONi7rqC9gJLQZT.htm](pathfinder-bestiary-3-items/4rONi7rqC9gJLQZT.htm)|+2 Greater Resilient Full Plate|
|[4v1IFAFpIxXq9Uzv.htm](pathfinder-bestiary-3-items/4v1IFAFpIxXq9Uzv.htm)|Remove Curse (At Will)|
|[57if0qj4jc4HcQGo.htm](pathfinder-bestiary-3-items/57if0qj4jc4HcQGo.htm)|Nondetection (Constant) (Self Only)|
|[5KlhepR2kBxgHDeS.htm](pathfinder-bestiary-3-items/5KlhepR2kBxgHDeS.htm)|Clairaudience (At Will)|
|[6frIjAjjbonxZDls.htm](pathfinder-bestiary-3-items/6frIjAjjbonxZDls.htm)|Endure Elements (Constant)|
|[6gxrdqOXjumJ5L6w.htm](pathfinder-bestiary-3-items/6gxrdqOXjumJ5L6w.htm)|Sound Burst (At Will)|
|[6ruimm3IigTOGGP3.htm](pathfinder-bestiary-3-items/6ruimm3IigTOGGP3.htm)|Tongues (Constant)|
|[6S6IDhHyRogz5bB7.htm](pathfinder-bestiary-3-items/6S6IDhHyRogz5bB7.htm)|Illusory Object (At Will)|
|[6utF0bwgFoQkxHUD.htm](pathfinder-bestiary-3-items/6utF0bwgFoQkxHUD.htm)|True Seeing (Constant)|
|[6Xmm3pc3idD2x94u.htm](pathfinder-bestiary-3-items/6Xmm3pc3idD2x94u.htm)|Create Water (At Will)|
|[76GyDdxf5paIWLH7.htm](pathfinder-bestiary-3-items/76GyDdxf5paIWLH7.htm)|Air Walk (Constant)|
|[76utl0ImcsGHY3qX.htm](pathfinder-bestiary-3-items/76utl0ImcsGHY3qX.htm)|Freedom of Movement (At Will)|
|[7FMwxdPwaQDAu4Bt.htm](pathfinder-bestiary-3-items/7FMwxdPwaQDAu4Bt.htm)|Invisibility (At Will) (Self Only)|
|[7iPW2qJv28sNIMlG.htm](pathfinder-bestiary-3-items/7iPW2qJv28sNIMlG.htm)|Tongues (Constant)|
|[7je9N9tvy1YfaYoe.htm](pathfinder-bestiary-3-items/7je9N9tvy1YfaYoe.htm)|Create Water (At Will)|
|[7kWnGY8jWtyZFMYQ.htm](pathfinder-bestiary-3-items/7kWnGY8jWtyZFMYQ.htm)|Detect Alignment (At Will) (Evil Only)|
|[7N5JvxeRQzcnB5rG.htm](pathfinder-bestiary-3-items/7N5JvxeRQzcnB5rG.htm)|Create Water (At Will)|
|[7qC64GqGP34ClPu4.htm](pathfinder-bestiary-3-items/7qC64GqGP34ClPu4.htm)|True Seeing (Constant)|
|[7ueu1lLVg5YdZ0pi.htm](pathfinder-bestiary-3-items/7ueu1lLVg5YdZ0pi.htm)|Create Undead (Doesn't Require Secondary Casters)|
|[82eOb078T13bwHGH.htm](pathfinder-bestiary-3-items/82eOb078T13bwHGH.htm)|Shape Stone (See Idols of Stone)|
|[82PUYYf4hPvhXKja.htm](pathfinder-bestiary-3-items/82PUYYf4hPvhXKja.htm)|Fear (At Will)|
|[8d5s5kWfxsTt1E7x.htm](pathfinder-bestiary-3-items/8d5s5kWfxsTt1E7x.htm)|Scythe|+1,striking|
|[8fuYbarODhWt1FXZ.htm](pathfinder-bestiary-3-items/8fuYbarODhWt1FXZ.htm)|Invisibility (At Will) (Self Only)|
|[8geadWeP6EV4w18U.htm](pathfinder-bestiary-3-items/8geadWeP6EV4w18U.htm)|Charm (At Will)|
|[8jSa46Rumwn8HF3e.htm](pathfinder-bestiary-3-items/8jSa46Rumwn8HF3e.htm)|Dominate (At Will) (See Doninate)|
|[8pagn6n2lyrn1sVZ.htm](pathfinder-bestiary-3-items/8pagn6n2lyrn1sVZ.htm)|Scroll of Confusion (Level 4)|
|[8pbf8Jlvt2FtaVXg.htm](pathfinder-bestiary-3-items/8pbf8Jlvt2FtaVXg.htm)|Spear|+1,striking,returning|
|[8YB1Ghz9CBsNGNNe.htm](pathfinder-bestiary-3-items/8YB1Ghz9CBsNGNNe.htm)|Invisibility (At Will) (Self Only)|
|[9azjuUmcCUGNaceC.htm](pathfinder-bestiary-3-items/9azjuUmcCUGNaceC.htm)|Ventriloquism (At Will)|
|[9bZvu1KELcz1AAxo.htm](pathfinder-bestiary-3-items/9bZvu1KELcz1AAxo.htm)|Dimension Door (At Will)|
|[9f0C9k9fO0tU9t1k.htm](pathfinder-bestiary-3-items/9f0C9k9fO0tU9t1k.htm)|Detect Alignment (At Will) (Evil Only)|
|[9jiKLswJEKTZnpYW.htm](pathfinder-bestiary-3-items/9jiKLswJEKTZnpYW.htm)|Charm (Animals Only)|
|[9RHcmhuS5shME3T2.htm](pathfinder-bestiary-3-items/9RHcmhuS5shME3T2.htm)|Greatclub|+3,majorStriking|
|[9utEXpSfzcSAzWEq.htm](pathfinder-bestiary-3-items/9utEXpSfzcSAzWEq.htm)|Paranoia (At Will)|
|[9xFBrkHYg5Q7QAn6.htm](pathfinder-bestiary-3-items/9xFBrkHYg5Q7QAn6.htm)|Earthbind (At Will)|
|[A1bXxv71EPG8m8nv.htm](pathfinder-bestiary-3-items/A1bXxv71EPG8m8nv.htm)|Tongues (Constant)|
|[aDe6hni7u78mFd28.htm](pathfinder-bestiary-3-items/aDe6hni7u78mFd28.htm)|Control Water|
|[Ae2lLfIjacCnLEfD.htm](pathfinder-bestiary-3-items/Ae2lLfIjacCnLEfD.htm)|Floating Disk (see Disk Rider)|
|[AelQuSvbNDOMD6xd.htm](pathfinder-bestiary-3-items/AelQuSvbNDOMD6xd.htm)|Humanoid Form (At Will)|
|[aHZC7D82nkogCiRc.htm](pathfinder-bestiary-3-items/aHZC7D82nkogCiRc.htm)|Air Walk (Constant) (Self Only)|
|[AiejWhSgj1tzGlOW.htm](pathfinder-bestiary-3-items/AiejWhSgj1tzGlOW.htm)|True Seeing (Constant)|
|[aLEUgg9lhbABSHFk.htm](pathfinder-bestiary-3-items/aLEUgg9lhbABSHFk.htm)|Fear (Animals, Fungi, And Plants Only)|
|[amsXIUz7b4eRuZRR.htm](pathfinder-bestiary-3-items/amsXIUz7b4eRuZRR.htm)|Dagger|+1,striking,returning|
|[anJNKDz3R8ApuLb2.htm](pathfinder-bestiary-3-items/anJNKDz3R8ApuLb2.htm)|Floating Disk (At Will)|
|[aox1nvPhhyKKaTi9.htm](pathfinder-bestiary-3-items/aox1nvPhhyKKaTi9.htm)|Scroll of Fly (Level 4)|
|[APy2XnP43Ct9hDjp.htm](pathfinder-bestiary-3-items/APy2XnP43Ct9hDjp.htm)|True Seeing (Constant)|
|[AR1CzPoZmsKpCqkD.htm](pathfinder-bestiary-3-items/AR1CzPoZmsKpCqkD.htm)|Plane Shift (To Or From The Shadow Plane Only)|
|[AW7uAsCknGCb4PDf.htm](pathfinder-bestiary-3-items/AW7uAsCknGCb4PDf.htm)|Read Omens (At Will)|
|[B5UmGa9iTcz4fPFG.htm](pathfinder-bestiary-3-items/B5UmGa9iTcz4fPFG.htm)|Clairaudience (At Will)|
|[B9hUiPWhzaBpL0E4.htm](pathfinder-bestiary-3-items/B9hUiPWhzaBpL0E4.htm)|Water Walk (Constant)|
|[bBRh5P56Wfgd2pT1.htm](pathfinder-bestiary-3-items/bBRh5P56Wfgd2pT1.htm)|Scroll of Fly (Level 4)|
|[BhejT6zPZ9fehFxC.htm](pathfinder-bestiary-3-items/BhejT6zPZ9fehFxC.htm)|Tree Shape (See Forest Shape)|
|[bnDqqEqGa8k1Und0.htm](pathfinder-bestiary-3-items/bnDqqEqGa8k1Und0.htm)|Plane Shift (Self Only)|
|[bpNYXau9AAUwSo12.htm](pathfinder-bestiary-3-items/bpNYXau9AAUwSo12.htm)|Invisibility (At Will) (Self Only)|
|[BrYFJCgLri32rKt1.htm](pathfinder-bestiary-3-items/BrYFJCgLri32rKt1.htm)|Tongues (Constant)|
|[BWPZgNFWXwjeS6rr.htm](pathfinder-bestiary-3-items/BWPZgNFWXwjeS6rr.htm)|Clairaudience (At Will)|
|[C1CsUuSohUDsve6I.htm](pathfinder-bestiary-3-items/C1CsUuSohUDsve6I.htm)|Air Walk (Constant)|
|[c2bv8VJPrEoXworT.htm](pathfinder-bestiary-3-items/c2bv8VJPrEoXworT.htm)|Ventriloquism (At Will)|
|[C2fTIwqzj871lbht.htm](pathfinder-bestiary-3-items/C2fTIwqzj871lbht.htm)|Fear (At Will)|
|[C8Ncqnw55IXOEyme.htm](pathfinder-bestiary-3-items/C8Ncqnw55IXOEyme.htm)|Pest Form (Monkey Only)|
|[cfPdfe58QE4eOHM0.htm](pathfinder-bestiary-3-items/cfPdfe58QE4eOHM0.htm)|Fear (At Will)|
|[cgNs8rXo0upiL2I7.htm](pathfinder-bestiary-3-items/cgNs8rXo0upiL2I7.htm)|Invisibility (Self Only) (At Will)|
|[cRVYRqrBJjmjPGU5.htm](pathfinder-bestiary-3-items/cRVYRqrBJjmjPGU5.htm)|Augury (At Will)|
|[ctbxOCAPmqkVJNSg.htm](pathfinder-bestiary-3-items/ctbxOCAPmqkVJNSg.htm)|Detect Alignment (At Will) (Evil Only)|
|[CTcOXdYuenSJsDQC.htm](pathfinder-bestiary-3-items/CTcOXdYuenSJsDQC.htm)|Suggestion (At Will)|
|[ctFijrOvAkTERCu9.htm](pathfinder-bestiary-3-items/ctFijrOvAkTERCu9.htm)|Water Breathing (Constant)|
|[CU8RCoSIPpjOMZoO.htm](pathfinder-bestiary-3-items/CU8RCoSIPpjOMZoO.htm)|Greataxe|+1,striking|
|[d5EqozxY8XEY3NIw.htm](pathfinder-bestiary-3-items/d5EqozxY8XEY3NIw.htm)|Telekinetic Maneuver (At Will)|
|[dCFKo5jQVRwny9tZ.htm](pathfinder-bestiary-3-items/dCFKo5jQVRwny9tZ.htm)|Control Water (At Will)|
|[dKid8PGYfgX6asC5.htm](pathfinder-bestiary-3-items/dKid8PGYfgX6asC5.htm)|Detect Alignment (Constant)|
|[DTNOBV0vRtCJQIK8.htm](pathfinder-bestiary-3-items/DTNOBV0vRtCJQIK8.htm)|+1 Resilient Hide Armor|
|[DVNHPkNXh0og7150.htm](pathfinder-bestiary-3-items/DVNHPkNXh0og7150.htm)|Dimension Door (At Will)|
|[EKw1H7sHPZrfrDL9.htm](pathfinder-bestiary-3-items/EKw1H7sHPZrfrDL9.htm)|Composite Longbow|+1,striking|
|[eLmaLQ51OWOQK6bj.htm](pathfinder-bestiary-3-items/eLmaLQ51OWOQK6bj.htm)|Divine Decree (Evil Only)|
|[ELpuKrFNyvwzCapj.htm](pathfinder-bestiary-3-items/ELpuKrFNyvwzCapj.htm)|True Strike (At Will)|
|[eno8CnD1EnlY1yIZ.htm](pathfinder-bestiary-3-items/eno8CnD1EnlY1yIZ.htm)|Zone of Truth (At Will)|
|[ETR9TOwQKjt3PBxR.htm](pathfinder-bestiary-3-items/ETR9TOwQKjt3PBxR.htm)|Sudden Blight (At Will)|
|[Eyuxywd5xyJvHMPE.htm](pathfinder-bestiary-3-items/Eyuxywd5xyJvHMPE.htm)|Mind Reading (At Will)|
|[f27aQS5iwwcDr82I.htm](pathfinder-bestiary-3-items/f27aQS5iwwcDr82I.htm)|Full Plate (see Death Blaze)|
|[f44ALHTlp3xIkYpz.htm](pathfinder-bestiary-3-items/f44ALHTlp3xIkYpz.htm)|Tree Shape (See Forest Shape)|
|[fCrkAswBNGcLQ2S5.htm](pathfinder-bestiary-3-items/fCrkAswBNGcLQ2S5.htm)|Detect Alignment (At Will) (Evil Only)|
|[fkJeFr0LCaDT6DYo.htm](pathfinder-bestiary-3-items/fkJeFr0LCaDT6DYo.htm)|Air Walk (Constant)|
|[FoWz6fNCdey5pLcj.htm](pathfinder-bestiary-3-items/FoWz6fNCdey5pLcj.htm)|Dispel Magic (At Will)|
|[fpKOIfEsphtEaCbR.htm](pathfinder-bestiary-3-items/fpKOIfEsphtEaCbR.htm)|Longsword|+1,striking,disrupting|
|[FpyBx0T91Ob8bRXz.htm](pathfinder-bestiary-3-items/FpyBx0T91Ob8bRXz.htm)|Mindlink (At Will)|
|[FWYR69TIoTEaL4Sc.htm](pathfinder-bestiary-3-items/FWYR69TIoTEaL4Sc.htm)|Speak with Animals (Constant)|
|[GeCGs1bKTNRXa8oP.htm](pathfinder-bestiary-3-items/GeCGs1bKTNRXa8oP.htm)|Comprehend Language (Self Only)|
|[gEPUwy8WYR35LnP8.htm](pathfinder-bestiary-3-items/gEPUwy8WYR35LnP8.htm)|Maul|+1|
|[gfVREKT4YiAXf0Sh.htm](pathfinder-bestiary-3-items/gfVREKT4YiAXf0Sh.htm)|Mind Reading (At Will)|
|[gg1skd2GL6lN70Op.htm](pathfinder-bestiary-3-items/gg1skd2GL6lN70Op.htm)|Foresight (Constant) (Self Only)|
|[gGq6qmIUe3LgDalo.htm](pathfinder-bestiary-3-items/gGq6qmIUe3LgDalo.htm)|Illusory Creature (At Will)|
|[GIMEaPqcCrWjsAx0.htm](pathfinder-bestiary-3-items/GIMEaPqcCrWjsAx0.htm)|Suggestion (At Will)|
|[gll6P1NY192pZXCX.htm](pathfinder-bestiary-3-items/gll6P1NY192pZXCX.htm)|Zealous Conviction (Self Only)|
|[GPFQRaqbTvsHzgLv.htm](pathfinder-bestiary-3-items/GPFQRaqbTvsHzgLv.htm)|Speak with Animals (Constant) (Spiders Only)|
|[GXP09LUUMpZjzZNK.htm](pathfinder-bestiary-3-items/GXP09LUUMpZjzZNK.htm)|Meld into Stone (At Will)|
|[h3rEsPLD8eRRgdqm.htm](pathfinder-bestiary-3-items/h3rEsPLD8eRRgdqm.htm)|Freedom of Movement (Constant)|
|[hagL5rqsb5F2Dib3.htm](pathfinder-bestiary-3-items/hagL5rqsb5F2Dib3.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[HdHa8fMuZx5zztbP.htm](pathfinder-bestiary-3-items/HdHa8fMuZx5zztbP.htm)|Voidglass Kukri|
|[HHcUzm4oVPHPOsqI.htm](pathfinder-bestiary-3-items/HHcUzm4oVPHPOsqI.htm)|Suggestion (At Will)|
|[HHzuVZDMyhnuUwVS.htm](pathfinder-bestiary-3-items/HHzuVZDMyhnuUwVS.htm)|Speak with Animals (Constant)|
|[HMAJFIpbicpFe1je.htm](pathfinder-bestiary-3-items/HMAJFIpbicpFe1je.htm)|Charm (Undead Only)|
|[HpKFDo446U6j9P11.htm](pathfinder-bestiary-3-items/HpKFDo446U6j9P11.htm)|Composite Longbow|+1|
|[hR5BG9TXTZY8EKQh.htm](pathfinder-bestiary-3-items/hR5BG9TXTZY8EKQh.htm)|Glyph of Warding (At Will)|
|[Hr6tT6rCEgL0bZVy.htm](pathfinder-bestiary-3-items/Hr6tT6rCEgL0bZVy.htm)|Speak with Animals (At Will)|
|[HRwyZKqvUxP3jUSQ.htm](pathfinder-bestiary-3-items/HRwyZKqvUxP3jUSQ.htm)|Plane Shift (Self Only) (Astral or Material Plane Only)|
|[HulNPVFqPe3KCKvw.htm](pathfinder-bestiary-3-items/HulNPVFqPe3KCKvw.htm)|+1 Resilient Breastplate|
|[hZBLQC0oOFlrs0Tt.htm](pathfinder-bestiary-3-items/hZBLQC0oOFlrs0Tt.htm)|Fear (At Will)|
|[I5elsFKNOWOCVOwx.htm](pathfinder-bestiary-3-items/I5elsFKNOWOCVOwx.htm)|Falchion|+2,striking|
|[I6HjzvdkLUHJLowS.htm](pathfinder-bestiary-3-items/I6HjzvdkLUHJLowS.htm)|Detect Alignment (At Will)|
|[Idr71bCK1gM6nuVc.htm](pathfinder-bestiary-3-items/Idr71bCK1gM6nuVc.htm)|Blight (Doesn't Require Secondary Casters)|
|[IegAA8jyEDik5QGH.htm](pathfinder-bestiary-3-items/IegAA8jyEDik5QGH.htm)|Darkness (At Will)|
|[IELKs5btgBCSQgGs.htm](pathfinder-bestiary-3-items/IELKs5btgBCSQgGs.htm)|Misdirection (At Will) (Self Only)|
|[iFLHtzCCLkMeOw2v.htm](pathfinder-bestiary-3-items/iFLHtzCCLkMeOw2v.htm)|Longsword|+1,striking|
|[iGgnvBVMpX59E7rh.htm](pathfinder-bestiary-3-items/iGgnvBVMpX59E7rh.htm)|Invisibility (Self Only)|
|[iinBWMkulBkpeUxH.htm](pathfinder-bestiary-3-items/iinBWMkulBkpeUxH.htm)|Dispel Magic (At Will)|
|[IKkbf0MnPy4EbhpO.htm](pathfinder-bestiary-3-items/IKkbf0MnPy4EbhpO.htm)|Read Omens (At Will)|
|[IKYih1OQrUQy2d83.htm](pathfinder-bestiary-3-items/IKYih1OQrUQy2d83.htm)|Fear (At Will)|
|[iN9SAyDZsYVDzYfN.htm](pathfinder-bestiary-3-items/iN9SAyDZsYVDzYfN.htm)|Mind Reading (At Will)|
|[ISNwBWNpl1RrSuwh.htm](pathfinder-bestiary-3-items/ISNwBWNpl1RrSuwh.htm)|Invisibility (At Will) (Self Only)|
|[it8V6trXRaEs8bg1.htm](pathfinder-bestiary-3-items/it8V6trXRaEs8bg1.htm)|Charm (At Will)|
|[IYB1MIKRfITclT4i.htm](pathfinder-bestiary-3-items/IYB1MIKRfITclT4i.htm)|Suggestion (At Will)|
|[iZEu9jYTf7nhB7z5.htm](pathfinder-bestiary-3-items/iZEu9jYTf7nhB7z5.htm)|Composite Shortbow|+1,striking|
|[J5Is1PpCXNmzaN0y.htm](pathfinder-bestiary-3-items/J5Is1PpCXNmzaN0y.htm)|Maze (Once per Week)|
|[jbVk6BJ2TRpBl0PN.htm](pathfinder-bestiary-3-items/jbVk6BJ2TRpBl0PN.htm)|Composite Longbow|+1,striking|
|[jcu8xSbi5cUAAu26.htm](pathfinder-bestiary-3-items/jcu8xSbi5cUAAu26.htm)|+2 Greater Resilient Breastplate|
|[Jd2DYD67ChktNYVN.htm](pathfinder-bestiary-3-items/Jd2DYD67ChktNYVN.htm)|Tongues (Constant)|
|[jDheLY8hqIt7kVGR.htm](pathfinder-bestiary-3-items/jDheLY8hqIt7kVGR.htm)|Dispel Magic (At Will)|
|[JGruUFHuCKGitKYf.htm](pathfinder-bestiary-3-items/JGruUFHuCKGitKYf.htm)|Magic Aura (At Will)|
|[jhyOsq6KuZehFYax.htm](pathfinder-bestiary-3-items/jhyOsq6KuZehFYax.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[jidSgetwzkY6k5WH.htm](pathfinder-bestiary-3-items/jidSgetwzkY6k5WH.htm)|See Invisibility (Constant)|
|[JxGDiOzvqKBoFWu5.htm](pathfinder-bestiary-3-items/JxGDiOzvqKBoFWu5.htm)|Plane Shift (Self Only)|
|[JZJkUq4BEg1IAg4W.htm](pathfinder-bestiary-3-items/JZJkUq4BEg1IAg4W.htm)|Tongues (Constant)|
|[K3dfOHkozmujfcG3.htm](pathfinder-bestiary-3-items/K3dfOHkozmujfcG3.htm)|Endure Elements (Self Only)|
|[K4gqGlmGETi7zB7J.htm](pathfinder-bestiary-3-items/K4gqGlmGETi7zB7J.htm)|Charm (Undead Only)|
|[k5ozmTQNA7IyzbmH.htm](pathfinder-bestiary-3-items/k5ozmTQNA7IyzbmH.htm)|Mind Reading (At Will)|
|[kCoPTtj3Cu57Xebn.htm](pathfinder-bestiary-3-items/kCoPTtj3Cu57Xebn.htm)|Halberd|+3,greaterStriking|
|[KdJvX2wvTeXTeu5i.htm](pathfinder-bestiary-3-items/KdJvX2wvTeXTeu5i.htm)|Dimension Door (At Will)|
|[KF8c1lT0YVj5lMG4.htm](pathfinder-bestiary-3-items/KF8c1lT0YVj5lMG4.htm)|Mind Reading (At Will)|
|[KgGTXxW5g0BAy1nV.htm](pathfinder-bestiary-3-items/KgGTXxW5g0BAy1nV.htm)|Spiked Chain|+1,striking,ghostTouch|
|[KHXOl15PR5DH02Gu.htm](pathfinder-bestiary-3-items/KHXOl15PR5DH02Gu.htm)|Dispel Magic (At Will)|
|[kIfdLdjbTbTKS03H.htm](pathfinder-bestiary-3-items/kIfdLdjbTbTKS03H.htm)|Comprehend Language (At Will) (Self Only)|
|[kVLMWbSRPeQyl3Oe.htm](pathfinder-bestiary-3-items/kVLMWbSRPeQyl3Oe.htm)|Spiritual Epidemic (At Will)|
|[l5mWQVGm4hAXrFYp.htm](pathfinder-bestiary-3-items/l5mWQVGm4hAXrFYp.htm)|Spiked Chain|+3,greaterStriking,cold-iron|
|[L86Ap8gRiTU3o6rf.htm](pathfinder-bestiary-3-items/L86Ap8gRiTU3o6rf.htm)|Dimension Door (At Will)|
|[lbhWyawC0OJPtNR4.htm](pathfinder-bestiary-3-items/lbhWyawC0OJPtNR4.htm)|Geas (Doesn't Require Secondary Casters And Can Target a Willing Creature of Any Level)|
|[lGFDmPhDxFNEVo95.htm](pathfinder-bestiary-3-items/lGFDmPhDxFNEVo95.htm)|Invisibility (At Will) (Self Only)|
|[Lni9y4yXW9RhxwLM.htm](pathfinder-bestiary-3-items/Lni9y4yXW9RhxwLM.htm)|Ventriloquism (At Will)|
|[LNRtNrzHIseTefIq.htm](pathfinder-bestiary-3-items/LNRtNrzHIseTefIq.htm)|Clockwork Wand|
|[LoE3dWpTJPk8g8Ei.htm](pathfinder-bestiary-3-items/LoE3dWpTJPk8g8Ei.htm)|Create Water (At Will)|
|[lpvW88Zc5DItTR6J.htm](pathfinder-bestiary-3-items/lpvW88Zc5DItTR6J.htm)|Water Walk (Constant)|
|[lRiN69RyGchL1AiD.htm](pathfinder-bestiary-3-items/lRiN69RyGchL1AiD.htm)|Speak with Animals (Constant)|
|[lTytlP2GFTlmWEkk.htm](pathfinder-bestiary-3-items/lTytlP2GFTlmWEkk.htm)|Haste (At Will) (Self Only)|
|[m0gDkd2xvDLRZxHw.htm](pathfinder-bestiary-3-items/m0gDkd2xvDLRZxHw.htm)|Invisibility (Self Only)|
|[M6L8pe2IvyC5P2i4.htm](pathfinder-bestiary-3-items/M6L8pe2IvyC5P2i4.htm)|Longspear|+1,striking|
|[M9CMfdS3cQy6u8eS.htm](pathfinder-bestiary-3-items/M9CMfdS3cQy6u8eS.htm)|Create Undead (No Secondary Caster Required)|
|[maMkCYAHEvq6tAqg.htm](pathfinder-bestiary-3-items/maMkCYAHEvq6tAqg.htm)|Invisibility (At Will)|
|[MGFv64yTRt7JOqf8.htm](pathfinder-bestiary-3-items/MGFv64yTRt7JOqf8.htm)|+1 Striking Felt Shears (as Dagger)|+1,striking|
|[MHmU7Zg92WbTJ7CF.htm](pathfinder-bestiary-3-items/MHmU7Zg92WbTJ7CF.htm)|Calm Emotions (At Will)|
|[mKGOHDAfBouobqfT.htm](pathfinder-bestiary-3-items/mKGOHDAfBouobqfT.htm)|Darkness (At Will)|
|[mOQO1nFke9ZyiXTD.htm](pathfinder-bestiary-3-items/mOQO1nFke9ZyiXTD.htm)|Mending (At Will)|
|[MtmIGyh1mEwfrLk8.htm](pathfinder-bestiary-3-items/MtmIGyh1mEwfrLk8.htm)|Ventriloquism (At Will)|
|[MvqE0WoDiotm6xOj.htm](pathfinder-bestiary-3-items/MvqE0WoDiotm6xOj.htm)|Gust of Wind (At Will)|
|[N6eFdrrSCUkH7776.htm](pathfinder-bestiary-3-items/N6eFdrrSCUkH7776.htm)|Misdirection (At Will) (Self Only)|
|[Nc80ofsnO8nL7SBk.htm](pathfinder-bestiary-3-items/Nc80ofsnO8nL7SBk.htm)|Nondetection (Constant) (Self Only)|
|[NCGFbhlnJHZ1rJp9.htm](pathfinder-bestiary-3-items/NCGFbhlnJHZ1rJp9.htm)|Clairvoyance (At Will)|
|[ner0C5vBZGTckOHF.htm](pathfinder-bestiary-3-items/ner0C5vBZGTckOHF.htm)|Obscuring Mist (At Will)|
|[nIqiNS5etJdv3J1R.htm](pathfinder-bestiary-3-items/nIqiNS5etJdv3J1R.htm)|Amulet|
|[nivZKXrJ6jbmI61w.htm](pathfinder-bestiary-3-items/nivZKXrJ6jbmI61w.htm)|Plane Shift (Self Only) (To Or From The Shadow Plane Only)|
|[NqfD7fDgNcg0SGu9.htm](pathfinder-bestiary-3-items/NqfD7fDgNcg0SGu9.htm)|True Seeing (Constant)|
|[nr0PMLn9MREOWXYD.htm](pathfinder-bestiary-3-items/nr0PMLn9MREOWXYD.htm)|Tongues (Constant)|
|[nROFFDEy5y0nKYS0.htm](pathfinder-bestiary-3-items/nROFFDEy5y0nKYS0.htm)|Suggestion (At Will)|
|[nrXR81qrvYlS1etB.htm](pathfinder-bestiary-3-items/nrXR81qrvYlS1etB.htm)|Chill Touch (Undead Only)|
|[O4OD37pDdJ7DDfXC.htm](pathfinder-bestiary-3-items/O4OD37pDdJ7DDfXC.htm)|Staff|+2,striking|
|[O4uO0WyOyJlypiSX.htm](pathfinder-bestiary-3-items/O4uO0WyOyJlypiSX.htm)|Charm (Undead Only)|
|[o6IceHXhizmbsddI.htm](pathfinder-bestiary-3-items/o6IceHXhizmbsddI.htm)|Invisibility (Self Only)|
|[O7lGGxeUoi47EMD0.htm](pathfinder-bestiary-3-items/O7lGGxeUoi47EMD0.htm)|Fear (At Will)|
|[O9kHIQhPaga6ZnIu.htm](pathfinder-bestiary-3-items/O9kHIQhPaga6ZnIu.htm)|Speak with Plants (At Will)|
|[ObwEq85BpNcwAeik.htm](pathfinder-bestiary-3-items/ObwEq85BpNcwAeik.htm)|Tongues (Constant)|
|[OcXCtZyWYIL0Reqo.htm](pathfinder-bestiary-3-items/OcXCtZyWYIL0Reqo.htm)|True Seeing (Constant)|
|[oeWCnT69F8n5CEeg.htm](pathfinder-bestiary-3-items/oeWCnT69F8n5CEeg.htm)|Plane Shift (Self Only)|
|[OeXYKvZIjZ3ny4IS.htm](pathfinder-bestiary-3-items/OeXYKvZIjZ3ny4IS.htm)|Glyph of Warding (At Will)|
|[OflSSMFtOIlEpn58.htm](pathfinder-bestiary-3-items/OflSSMFtOIlEpn58.htm)|Elegant Cane (As Mace)|
|[OHawqyo3r0zsutMI.htm](pathfinder-bestiary-3-items/OHawqyo3r0zsutMI.htm)|Shortsword|+1,striking|
|[OhLCOozHQSHlWn0N.htm](pathfinder-bestiary-3-items/OhLCOozHQSHlWn0N.htm)|True Seeing (Constant)|
|[oil97jTz67aGZk18.htm](pathfinder-bestiary-3-items/oil97jTz67aGZk18.htm)|Tongues (Constant)|
|[oqveVOVwv267LEYp.htm](pathfinder-bestiary-3-items/oqveVOVwv267LEYp.htm)|Speak with Animals (Constant)|
|[OsLma7Z7aL2Uy9WP.htm](pathfinder-bestiary-3-items/OsLma7Z7aL2Uy9WP.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[OTCTmOT1dbEFaHMD.htm](pathfinder-bestiary-3-items/OTCTmOT1dbEFaHMD.htm)|Detect Alignment (At Will)|
|[ou4TzsHhJ2YJ9Ub3.htm](pathfinder-bestiary-3-items/ou4TzsHhJ2YJ9Ub3.htm)|Fireball (At Will) (See Unstable Magic)|
|[oyLpBeZ7RvXbKxJA.htm](pathfinder-bestiary-3-items/oyLpBeZ7RvXbKxJA.htm)|Outcast's Curse (At Will)|
|[p2TaK9MrvykMKB0o.htm](pathfinder-bestiary-3-items/p2TaK9MrvykMKB0o.htm)|Dimension Door (At Will)|
|[p5VLQl4f56EqDMBY.htm](pathfinder-bestiary-3-items/p5VLQl4f56EqDMBY.htm)|Magic Aura (Constant) (Self Only)|
|[P9EmNj8ZddzoKHMT.htm](pathfinder-bestiary-3-items/P9EmNj8ZddzoKHMT.htm)|Possession (See Mind Swap)|
|[pbe04bBz3BMbUs0Q.htm](pathfinder-bestiary-3-items/pbe04bBz3BMbUs0Q.htm)|Suggestion (At Will)|
|[PCyOFsdSGo2P3emN.htm](pathfinder-bestiary-3-items/PCyOFsdSGo2P3emN.htm)|Glyph of Warding (At Will)|
|[pCYui6n9e9HBtE9b.htm](pathfinder-bestiary-3-items/pCYui6n9e9HBtE9b.htm)|Tree Stride (At Will)|
|[PDk5irePkj7swYlp.htm](pathfinder-bestiary-3-items/PDk5irePkj7swYlp.htm)|Hatchet|+1|
|[pEUpV6dWX1Vbavcs.htm](pathfinder-bestiary-3-items/pEUpV6dWX1Vbavcs.htm)|Glyph of Warding (At Will)|
|[pfRf6x3WGakhgEUH.htm](pathfinder-bestiary-3-items/pfRf6x3WGakhgEUH.htm)|Tongues (Constant)|
|[pHk7V2hLZrgzY4OE.htm](pathfinder-bestiary-3-items/pHk7V2hLZrgzY4OE.htm)|Charm (At Will)|
|[phln4muQIICQhLF5.htm](pathfinder-bestiary-3-items/phln4muQIICQhLF5.htm)|Illusory Disguise (At Will)|
|[pLvuVluNzMrdi4RY.htm](pathfinder-bestiary-3-items/pLvuVluNzMrdi4RY.htm)|Illusory Scene (At Will)|
|[pOCKvTWHNSvPbzYY.htm](pathfinder-bestiary-3-items/pOCKvTWHNSvPbzYY.htm)|See Invisibility (Constant)|
|[pQzYMrj90yKkCKzz.htm](pathfinder-bestiary-3-items/pQzYMrj90yKkCKzz.htm)|Mind Blank (Constant)|
|[PVVUt6V4luw3UnL9.htm](pathfinder-bestiary-3-items/PVVUt6V4luw3UnL9.htm)|Dimension Door (At Will)|
|[pWSuV2gScsPeanBP.htm](pathfinder-bestiary-3-items/pWSuV2gScsPeanBP.htm)|Divine Lance (Chaos or Evil)|
|[pwWN3Mp741VnQ6Lx.htm](pathfinder-bestiary-3-items/pwWN3Mp741VnQ6Lx.htm)|Command (Animals Only)|
|[q44VpDPavuRcix13.htm](pathfinder-bestiary-3-items/q44VpDPavuRcix13.htm)|Composite Shortbow|+1,striking|
|[q72VNUpEzEqiaYgo.htm](pathfinder-bestiary-3-items/q72VNUpEzEqiaYgo.htm)|Tongues (Constant)|
|[Q8UNdxr6PscskBIx.htm](pathfinder-bestiary-3-items/Q8UNdxr6PscskBIx.htm)|Flaming Sphere (At Will) (See Unstable Magic)|
|[qaQZJI6uMjww56l2.htm](pathfinder-bestiary-3-items/qaQZJI6uMjww56l2.htm)|Tongues (Constant)|
|[QBCimVCmKxfPzDh2.htm](pathfinder-bestiary-3-items/QBCimVCmKxfPzDh2.htm)|+1 Breastplate|
|[QC1IrArhxNW6ef3t.htm](pathfinder-bestiary-3-items/QC1IrArhxNW6ef3t.htm)|Longbow|+1|
|[qcMqwJBE8PblmJXd.htm](pathfinder-bestiary-3-items/qcMqwJBE8PblmJXd.htm)|Tongues (Constant)|
|[QIy5U5WATHHnpiex.htm](pathfinder-bestiary-3-items/QIy5U5WATHHnpiex.htm)|Touch of Idiocy (At Will)|
|[qRZz1ras2ekZ3Dnn.htm](pathfinder-bestiary-3-items/qRZz1ras2ekZ3Dnn.htm)|Crushing Despair (At Will)|
|[r1Ei390nYbTkJ2Ac.htm](pathfinder-bestiary-3-items/r1Ei390nYbTkJ2Ac.htm)|Air Walk (Constant)|
|[R8ZpJ5Qjyevn9M2I.htm](pathfinder-bestiary-3-items/R8ZpJ5Qjyevn9M2I.htm)|Control Weather (Doesn't Require Secondary Casters)|
|[RgzVnccNPsj40XQj.htm](pathfinder-bestiary-3-items/RgzVnccNPsj40XQj.htm)|Speak with Animals (Constant)|
|[RLIvbPC5j2g106DY.htm](pathfinder-bestiary-3-items/RLIvbPC5j2g106DY.htm)|Katana|+1|
|[RLNzraMr15AQohaZ.htm](pathfinder-bestiary-3-items/RLNzraMr15AQohaZ.htm)|Endure Elements (Constant)|
|[RqeQ9keiv0CzpNUe.htm](pathfinder-bestiary-3-items/RqeQ9keiv0CzpNUe.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[rREO3eXjpKJUOkJt.htm](pathfinder-bestiary-3-items/rREO3eXjpKJUOkJt.htm)|Dispel Magic (At Will)|
|[rRrYw3Yi8aOQcMIK.htm](pathfinder-bestiary-3-items/rRrYw3Yi8aOQcMIK.htm)|Detect Alignment (At Will)|
|[S1xpDPa446jLmH3J.htm](pathfinder-bestiary-3-items/S1xpDPa446jLmH3J.htm)|Mask of Terror (Self Only)|
|[s49RXHWrH4qqNcEe.htm](pathfinder-bestiary-3-items/s49RXHWrH4qqNcEe.htm)|Tree Stride (At Will) (Cherry Trees Only)|
|[s5b07GowVsMRP7ro.htm](pathfinder-bestiary-3-items/s5b07GowVsMRP7ro.htm)|Mask of Terror (At Will)|
|[S7iB7CDLRoVYT6Pp.htm](pathfinder-bestiary-3-items/S7iB7CDLRoVYT6Pp.htm)|Mind Reading (At Will)|
|[sieqVjLe7KzZuR5l.htm](pathfinder-bestiary-3-items/sieqVjLe7KzZuR5l.htm)|Clairvoyance (At Will)|
|[sLLDvETLI8wX6s36.htm](pathfinder-bestiary-3-items/sLLDvETLI8wX6s36.htm)|Heal (At Will)|
|[sLqam8gjwb8YnuXS.htm](pathfinder-bestiary-3-items/sLqam8gjwb8YnuXS.htm)|Voidglass Kukri|
|[Snsjj1UYj5mIU9UL.htm](pathfinder-bestiary-3-items/Snsjj1UYj5mIU9UL.htm)|Falchion|+2,greaterStriking|
|[SoMnIapfSLR7sCTB.htm](pathfinder-bestiary-3-items/SoMnIapfSLR7sCTB.htm)|Control Weather (No Secondary Caster Required)|
|[sTFtTWlMXrbtb4sx.htm](pathfinder-bestiary-3-items/sTFtTWlMXrbtb4sx.htm)|Detect Alignment (At Will) (Evil Only)|
|[STWxL9WtBfQg569j.htm](pathfinder-bestiary-3-items/STWxL9WtBfQg569j.htm)|Dimension Door (At Will)|
|[szK7m2cRz0pWuEUb.htm](pathfinder-bestiary-3-items/szK7m2cRz0pWuEUb.htm)|Mind Reading (At Will)|
|[TF0po3aZY65Jw1V4.htm](pathfinder-bestiary-3-items/TF0po3aZY65Jw1V4.htm)|Suggestion (At Will)|
|[TjfSgZMdZR3uPTrF.htm](pathfinder-bestiary-3-items/TjfSgZMdZR3uPTrF.htm)|Dimension Door (At Will)|
|[tpcdOVPJoLXH4D7L.htm](pathfinder-bestiary-3-items/tpcdOVPJoLXH4D7L.htm)|Planar Binding (Doesn't Require Secondary Casters)|
|[tqRZXY6ERk4h1lPa.htm](pathfinder-bestiary-3-items/tqRZXY6ERk4h1lPa.htm)|Nondetection (At Will) (Self Only)|
|[Ts3F6FIbPPOUlZZJ.htm](pathfinder-bestiary-3-items/Ts3F6FIbPPOUlZZJ.htm)|Pass Without Trace (Constant)|
|[TS6AnRbKZab05oJP.htm](pathfinder-bestiary-3-items/TS6AnRbKZab05oJP.htm)|Fly (At Will)|
|[TUW5DOM2gHCfHxkX.htm](pathfinder-bestiary-3-items/TUW5DOM2gHCfHxkX.htm)|Air Walk (Constant)|
|[tx21vhxchyBGHxOD.htm](pathfinder-bestiary-3-items/tx21vhxchyBGHxOD.htm)|Touch of Idiocy (At Will)|
|[U2e7s0f0SwOH3IDu.htm](pathfinder-bestiary-3-items/U2e7s0f0SwOH3IDu.htm)|Air Walk (Constant)|
|[U8FXRYDBOA6S2wc1.htm](pathfinder-bestiary-3-items/U8FXRYDBOA6S2wc1.htm)|Darkness (At Will)|
|[U9ViaHzmhkqzKJ3Q.htm](pathfinder-bestiary-3-items/U9ViaHzmhkqzKJ3Q.htm)|See Invisibility (Constant)|
|[uaFUxzIXa5N9QFrp.htm](pathfinder-bestiary-3-items/uaFUxzIXa5N9QFrp.htm)|Fly (Constant)|
|[Uc3ieomfmTp95mE5.htm](pathfinder-bestiary-3-items/Uc3ieomfmTp95mE5.htm)|Obscuring Mist|
|[uDHQXwHJ5JXGBLA0.htm](pathfinder-bestiary-3-items/uDHQXwHJ5JXGBLA0.htm)|Fire Shield (Constant)|
|[uM4bCzmiegIXPcJf.htm](pathfinder-bestiary-3-items/uM4bCzmiegIXPcJf.htm)|Meld into Stone (At Will)|
|[uN7b49VuSYz5eKNd.htm](pathfinder-bestiary-3-items/uN7b49VuSYz5eKNd.htm)|Water Walk (Constant)|
|[unMEeRrwlVMzjW7n.htm](pathfinder-bestiary-3-items/unMEeRrwlVMzjW7n.htm)|Mind Reading (At Will)|
|[uQOW7jTQlM2mFtVU.htm](pathfinder-bestiary-3-items/uQOW7jTQlM2mFtVU.htm)|Endure Elements (Self Only)|
|[usTqKqK0mzh2dkzZ.htm](pathfinder-bestiary-3-items/usTqKqK0mzh2dkzZ.htm)|Scroll of Dimensional Anchor (Level 4)|
|[uWPyQC3rQMHc7uh1.htm](pathfinder-bestiary-3-items/uWPyQC3rQMHc7uh1.htm)|Hypercognition (At Will)|
|[Uytd5UdwOMgJqTS5.htm](pathfinder-bestiary-3-items/Uytd5UdwOMgJqTS5.htm)|Tongues (Constant)|
|[V1DW1BLRqkZoZJFG.htm](pathfinder-bestiary-3-items/V1DW1BLRqkZoZJFG.htm)|Shape Stone (At Will)|
|[v43kRV9YPyaSeo23.htm](pathfinder-bestiary-3-items/v43kRV9YPyaSeo23.htm)|Air Walk (Constant)|
|[V4TijnwUl9hbxsnI.htm](pathfinder-bestiary-3-items/V4TijnwUl9hbxsnI.htm)|Greatpick|+3,greaterStriking|
|[V5i5HJfqqedgNK8E.htm](pathfinder-bestiary-3-items/V5i5HJfqqedgNK8E.htm)|Bind Undead (At Will)|
|[vFvsiqwCZ9mkVwHL.htm](pathfinder-bestiary-3-items/vFvsiqwCZ9mkVwHL.htm)|True Seeing (Constant)|
|[vg0HZLmdVPcQrpjQ.htm](pathfinder-bestiary-3-items/vg0HZLmdVPcQrpjQ.htm)|Speak with Animals (Constant)|
|[vjPEjP5swzJDQYdF.htm](pathfinder-bestiary-3-items/vjPEjP5swzJDQYdF.htm)|Plane Shift (Self and Mount Only)|
|[VksbgDGpgQBGjtRJ.htm](pathfinder-bestiary-3-items/VksbgDGpgQBGjtRJ.htm)|Plant Growth|
|[vOU0EcH2WqzHGtgl.htm](pathfinder-bestiary-3-items/vOU0EcH2WqzHGtgl.htm)|Air Walk (Constant)|
|[Vp57wsIQVmtG1jPa.htm](pathfinder-bestiary-3-items/Vp57wsIQVmtG1jPa.htm)|Suggestion (At Will)|
|[vQvd4d7TqB4xYYzG.htm](pathfinder-bestiary-3-items/vQvd4d7TqB4xYYzG.htm)|Silence (At Will)|
|[vQY8xEtGMI71ZFRC.htm](pathfinder-bestiary-3-items/vQY8xEtGMI71ZFRC.htm)|Outcast's Curse (At Will)|
|[VTqkprtwM4tCpoqy.htm](pathfinder-bestiary-3-items/VTqkprtwM4tCpoqy.htm)|True Seeing (Constant)|
|[VWEAgF7iG7hLA3uD.htm](pathfinder-bestiary-3-items/VWEAgF7iG7hLA3uD.htm)|Gaseous Form (At Will)|
|[w5870YWidWVrdHst.htm](pathfinder-bestiary-3-items/w5870YWidWVrdHst.htm)|Nondetection (Constant) (Self Only)|
|[W7hhuWr7UL3cytW8.htm](pathfinder-bestiary-3-items/W7hhuWr7UL3cytW8.htm)|Defiled Religious Symbol of Pharasma (Wooden)|
|[wA8VVomLPk1zjrds.htm](pathfinder-bestiary-3-items/wA8VVomLPk1zjrds.htm)|Handwraps of Mighty Blows|+1|
|[WAFwt6ZXf9QztSeJ.htm](pathfinder-bestiary-3-items/WAFwt6ZXf9QztSeJ.htm)|Create Water (At Will)|
|[wBBkm38TLkhTV2GZ.htm](pathfinder-bestiary-3-items/wBBkm38TLkhTV2GZ.htm)|Control Weather (No Secondary Caster Required)|
|[WBhfbegIvXs0rmDc.htm](pathfinder-bestiary-3-items/WBhfbegIvXs0rmDc.htm)|Air Walk (Constant)|
|[Wg3QK5gzo1szg9zD.htm](pathfinder-bestiary-3-items/Wg3QK5gzo1szg9zD.htm)|Mindlink (At Will)|
|[WjhRi0h58DIX9ATj.htm](pathfinder-bestiary-3-items/WjhRi0h58DIX9ATj.htm)|Suggestion (At Will)|
|[WOmi5gJbb9PPIAM8.htm](pathfinder-bestiary-3-items/WOmi5gJbb9PPIAM8.htm)|Ventriloquism (At Will)|
|[Wv5iLNU2m5Tyg4mv.htm](pathfinder-bestiary-3-items/Wv5iLNU2m5Tyg4mv.htm)|Dimension Door (At Will)|
|[wX6Fc9wZK3CK5WNe.htm](pathfinder-bestiary-3-items/wX6Fc9wZK3CK5WNe.htm)|Shadow Walk (See Shadow's Swiftness)|
|[Wzi3fuQeULw68X2T.htm](pathfinder-bestiary-3-items/Wzi3fuQeULw68X2T.htm)|Purify Food and Drink (At Will)|
|[x29TRtgx6HdtboNu.htm](pathfinder-bestiary-3-items/x29TRtgx6HdtboNu.htm)|Dimension Door (At Will)|
|[XAg8ikRjbnklrzff.htm](pathfinder-bestiary-3-items/XAg8ikRjbnklrzff.htm)|True Seeing (Constant)|
|[XAyHusrI0x96dmIN.htm](pathfinder-bestiary-3-items/XAyHusrI0x96dmIN.htm)|True Seeing (Constant)|
|[Xco2zlLptJvbWtBI.htm](pathfinder-bestiary-3-items/Xco2zlLptJvbWtBI.htm)|Dimension Door (At Will)|
|[xcujrw8UlSaahhu6.htm](pathfinder-bestiary-3-items/xcujrw8UlSaahhu6.htm)|Earthbind (At Will)|
|[xFikHberuOZ3zfc4.htm](pathfinder-bestiary-3-items/xFikHberuOZ3zfc4.htm)|Speak with Plants (Constant)|
|[xfMHekuTgDpCE7UY.htm](pathfinder-bestiary-3-items/xfMHekuTgDpCE7UY.htm)|See Invisibility (Constant)|
|[xhfzAaoPZ4coGOku.htm](pathfinder-bestiary-3-items/xhfzAaoPZ4coGOku.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[Xiw2DZbMz5qQ6sUm.htm](pathfinder-bestiary-3-items/Xiw2DZbMz5qQ6sUm.htm)|Tongues (Constant)|
|[xkZQodLXco1BCblV.htm](pathfinder-bestiary-3-items/xkZQodLXco1BCblV.htm)|Suggestion (At Will)|
|[XRASk2fmJyT1LAqe.htm](pathfinder-bestiary-3-items/XRASk2fmJyT1LAqe.htm)|Mind Reading (At Will)|
|[XrFiVMNybl9KRDD9.htm](pathfinder-bestiary-3-items/XrFiVMNybl9KRDD9.htm)|Speak with Animals (Constant)|
|[XrGSSYX0IvMaaBC0.htm](pathfinder-bestiary-3-items/XrGSSYX0IvMaaBC0.htm)|Stone Tell (Constant)|
|[xTlKFL3sVgzIZhmL.htm](pathfinder-bestiary-3-items/xTlKFL3sVgzIZhmL.htm)|Pass Without Trace (Constant)|
|[xWxtACkZ1XENgLax.htm](pathfinder-bestiary-3-items/xWxtACkZ1XENgLax.htm)|Greatclub|+1,striking|
|[XX6N64aoSGL9jWS6.htm](pathfinder-bestiary-3-items/XX6N64aoSGL9jWS6.htm)|Magic Weapon (At Will)|
|[xZlNbzJzZCNKvumU.htm](pathfinder-bestiary-3-items/xZlNbzJzZCNKvumU.htm)|Plane Shift (Self Only) (to the Material or Shadow Plane only)|
|[Y8b1W9ZdIMaUhI2I.htm](pathfinder-bestiary-3-items/Y8b1W9ZdIMaUhI2I.htm)|Magic Aura (Constant) (Self Only)|
|[Y9Hwv9W7YTv5qXrI.htm](pathfinder-bestiary-3-items/Y9Hwv9W7YTv5qXrI.htm)|Endure Elements (Self Only)|
|[YC9q2aIlbYj8sEUp.htm](pathfinder-bestiary-3-items/YC9q2aIlbYj8sEUp.htm)|Greatclub|+1,striking|
|[yfzdBwOPqpyuRl0x.htm](pathfinder-bestiary-3-items/yfzdBwOPqpyuRl0x.htm)|Flute|
|[YLG2dq3uetOa36Vb.htm](pathfinder-bestiary-3-items/YLG2dq3uetOa36Vb.htm)|Bind Undead (At Will)|
|[YLigc5sHGyr9ZE6A.htm](pathfinder-bestiary-3-items/YLigc5sHGyr9ZE6A.htm)|Detect Alignment (At Will) (Evil Only)|
|[YlQhHUez8TDobfxk.htm](pathfinder-bestiary-3-items/YlQhHUez8TDobfxk.htm)|Illusory Object (At Will)|
|[yMZjDluDwdyDmxeR.htm](pathfinder-bestiary-3-items/yMZjDluDwdyDmxeR.htm)|Dominate (At Will) (See Dominate)|
|[YNbqq2itxEP5DMSE.htm](pathfinder-bestiary-3-items/YNbqq2itxEP5DMSE.htm)|Wall of Fire (At Will) (See Unstable Magic)|
|[zANtgVUDX9St398r.htm](pathfinder-bestiary-3-items/zANtgVUDX9St398r.htm)|Tree Shape (Cherry Tree Only)|
|[ZarnbOm1D79SECpY.htm](pathfinder-bestiary-3-items/ZarnbOm1D79SECpY.htm)|Summon Animal (Spiders Only)|
|[zBG0dgN8cMdRn2tF.htm](pathfinder-bestiary-3-items/zBG0dgN8cMdRn2tF.htm)|Phantasmal Killer (Image Resembles the Brainchild)|
|[zChDwHyu2FWl3wIc.htm](pathfinder-bestiary-3-items/zChDwHyu2FWl3wIc.htm)|+1 Breastplate|
|[Zh5jidqsamP7xlFa.htm](pathfinder-bestiary-3-items/Zh5jidqsamP7xlFa.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[zr91h9LPlTzhqjrE.htm](pathfinder-bestiary-3-items/zr91h9LPlTzhqjrE.htm)|True Seeing (Constant)|
|[ZVB4BE6xf6rF4Gl7.htm](pathfinder-bestiary-3-items/ZVB4BE6xf6rF4Gl7.htm)|Tongues (Constant)|
|[zVHM5ms0PMEfCJcT.htm](pathfinder-bestiary-3-items/zVHM5ms0PMEfCJcT.htm)|Darkness (At Will)|
|[zvQC0uB9gzHndGBg.htm](pathfinder-bestiary-3-items/zvQC0uB9gzHndGBg.htm)|Air Walk (Constant)|
|[zWsIEzGEL4qx0Z0P.htm](pathfinder-bestiary-3-items/zWsIEzGEL4qx0Z0P.htm)|Scimitar|+1,striking|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[04WUnuVaMwggkQxr.htm](pathfinder-bestiary-3-items/04WUnuVaMwggkQxr.htm)|Plaque Burst|Ráfaga de placas|modificada|
|[06gks6jfRaUwycln.htm](pathfinder-bestiary-3-items/06gks6jfRaUwycln.htm)|Gift of Knowledge|Don del Conocimiento|modificada|
|[06mT1ZKXqT7oAL0x.htm](pathfinder-bestiary-3-items/06mT1ZKXqT7oAL0x.htm)|Talon|Talon|modificada|
|[06rO4SWWCzcZfK0D.htm](pathfinder-bestiary-3-items/06rO4SWWCzcZfK0D.htm)|Spiny Eurypterid Venom|Spiny Eurypterid Venom|modificada|
|[08t9rJT8yGrqWVpw.htm](pathfinder-bestiary-3-items/08t9rJT8yGrqWVpw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[08TcEyoA4GxnmNBv.htm](pathfinder-bestiary-3-items/08TcEyoA4GxnmNBv.htm)|Raise Grain Cloud|Levantar Nube de Grano|modificada|
|[0a3piFjb7YtsWrsN.htm](pathfinder-bestiary-3-items/0a3piFjb7YtsWrsN.htm)|Uncanny Pounce|Uncanny Abalanzarse|modificada|
|[0AymbzLegfs9ibC8.htm](pathfinder-bestiary-3-items/0AymbzLegfs9ibC8.htm)|Tied to the Land|Tied to the Land|modificada|
|[0BvqGo4aw3xH9SrP.htm](pathfinder-bestiary-3-items/0BvqGo4aw3xH9SrP.htm)|Fan the Flames|Aviva las Flamígeras|modificada|
|[0cGHRE1RTMbDwp5J.htm](pathfinder-bestiary-3-items/0cGHRE1RTMbDwp5J.htm)|Tremorsense (Imprecise) 15 feet|Sentido del Temblor (Impreciso) 15 pies|modificada|
|[0cUUUVCjDDYMeTyQ.htm](pathfinder-bestiary-3-items/0cUUUVCjDDYMeTyQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0eBQQnVqxlDicgcI.htm](pathfinder-bestiary-3-items/0eBQQnVqxlDicgcI.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[0eEbWDjEXskh1gvK.htm](pathfinder-bestiary-3-items/0eEbWDjEXskh1gvK.htm)|Cold Adaptation|Adaptación al frío|modificada|
|[0eyOInV05vjoOU3X.htm](pathfinder-bestiary-3-items/0eyOInV05vjoOU3X.htm)|Blinding Sulfur|Azufre cegador|modificada|
|[0fDCOqKLdjm45yXu.htm](pathfinder-bestiary-3-items/0fDCOqKLdjm45yXu.htm)|Resonance|Resonance|modificada|
|[0fEFXQi4HqLkc4EL.htm](pathfinder-bestiary-3-items/0fEFXQi4HqLkc4EL.htm)|Raise a Shield|Alzar un escudo|modificada|
|[0fGH8UJJm4ccTubw.htm](pathfinder-bestiary-3-items/0fGH8UJJm4ccTubw.htm)|Accord Essence|Esencia Accord|modificada|
|[0fyQTAFgkuG0R7JH.htm](pathfinder-bestiary-3-items/0fyQTAFgkuG0R7JH.htm)|Stinger|Aguijón|modificada|
|[0GjO0CZOnFyUsbIr.htm](pathfinder-bestiary-3-items/0GjO0CZOnFyUsbIr.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[0Himurdx3RL3D4gk.htm](pathfinder-bestiary-3-items/0Himurdx3RL3D4gk.htm)|Broken Thorns|Broken Thorns|modificada|
|[0hiycK0RHpWLyxZG.htm](pathfinder-bestiary-3-items/0hiycK0RHpWLyxZG.htm)|No Breath|No Breath|modificada|
|[0Hotofas3viOSSly.htm](pathfinder-bestiary-3-items/0Hotofas3viOSSly.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[0hSL8u2ImY5BTyJo.htm](pathfinder-bestiary-3-items/0hSL8u2ImY5BTyJo.htm)|Drain Life|Drenar Vida|modificada|
|[0Iag5l05kpRuaYEj.htm](pathfinder-bestiary-3-items/0Iag5l05kpRuaYEj.htm)|Philanthropic Bile|Bilis Filantrópica|modificada|
|[0IOuDjFm6iLT2q5x.htm](pathfinder-bestiary-3-items/0IOuDjFm6iLT2q5x.htm)|Vanish into the Wilds|Desaparecer en lo salvaje|modificada|
|[0jNl0jg5W1N5NrTS.htm](pathfinder-bestiary-3-items/0jNl0jg5W1N5NrTS.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[0k3Jmkc79tf6HAi0.htm](pathfinder-bestiary-3-items/0k3Jmkc79tf6HAi0.htm)|All-Around Vision|All-Around Vision|modificada|
|[0kBV6c4sgKTQp33O.htm](pathfinder-bestiary-3-items/0kBV6c4sgKTQp33O.htm)|Talon|Talon|modificada|
|[0LkFUNpvQRTipNKI.htm](pathfinder-bestiary-3-items/0LkFUNpvQRTipNKI.htm)|Constrict|Restringir|modificada|
|[0LnVcBoOwSUOZfMh.htm](pathfinder-bestiary-3-items/0LnVcBoOwSUOZfMh.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[0nLSNrzLY7to9xEI.htm](pathfinder-bestiary-3-items/0nLSNrzLY7to9xEI.htm)|Form Up|Form Up|modificada|
|[0NvY0SLrHGDIxxdQ.htm](pathfinder-bestiary-3-items/0NvY0SLrHGDIxxdQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0oYv3Dg6IHZomTdm.htm](pathfinder-bestiary-3-items/0oYv3Dg6IHZomTdm.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0PhWUNCIDMNJ1T2x.htm](pathfinder-bestiary-3-items/0PhWUNCIDMNJ1T2x.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[0PU6xyU0Ewf6vUVD.htm](pathfinder-bestiary-3-items/0PU6xyU0Ewf6vUVD.htm)|Huntblade Brutality|Huntblade Brutalidad|modificada|
|[0QWcjdtxCTYgd8Bm.htm](pathfinder-bestiary-3-items/0QWcjdtxCTYgd8Bm.htm)|Autonomous Spell|Hechizo autónomo|modificada|
|[0Rn8CE4JcrTxdKqN.htm](pathfinder-bestiary-3-items/0Rn8CE4JcrTxdKqN.htm)|Easy to Call|Fácil de Llamar|modificada|
|[0taZfsX99OZREL4H.htm](pathfinder-bestiary-3-items/0taZfsX99OZREL4H.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0wqFlm8L407f7apV.htm](pathfinder-bestiary-3-items/0wqFlm8L407f7apV.htm)|Grab|Agarrado|modificada|
|[0XhFSYYoNSo9Uab7.htm](pathfinder-bestiary-3-items/0XhFSYYoNSo9Uab7.htm)|Claw|Garra|modificada|
|[0yTcCw02hLLf5qMi.htm](pathfinder-bestiary-3-items/0yTcCw02hLLf5qMi.htm)|Change Shape|Change Shape|modificada|
|[0zMlEvkv5OFS1XI2.htm](pathfinder-bestiary-3-items/0zMlEvkv5OFS1XI2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[10hI8m7tJ9cKdeoE.htm](pathfinder-bestiary-3-items/10hI8m7tJ9cKdeoE.htm)|Hoof|Hoof|modificada|
|[10pooBpkF2WtrqUt.htm](pathfinder-bestiary-3-items/10pooBpkF2WtrqUt.htm)|Consume Organ|Consumir Órgano|modificada|
|[135ZjsQICC0n6hMK.htm](pathfinder-bestiary-3-items/135ZjsQICC0n6hMK.htm)|Hoof|Hoof|modificada|
|[13p4CNlMthrJMobp.htm](pathfinder-bestiary-3-items/13p4CNlMthrJMobp.htm)|Negative Healing|Curación negativa|modificada|
|[19AKAkXuEUEtoqkr.htm](pathfinder-bestiary-3-items/19AKAkXuEUEtoqkr.htm)|Recall the Fallen|Recall the Fallen|modificada|
|[1a2EGaGWMPDZQqkX.htm](pathfinder-bestiary-3-items/1a2EGaGWMPDZQqkX.htm)|Harvester Poison|Harvester Poison|modificada|
|[1AH3QAMA4BbIHNxh.htm](pathfinder-bestiary-3-items/1AH3QAMA4BbIHNxh.htm)|Stinger|Aguijón|modificada|
|[1bmJGFD6zAmHJap5.htm](pathfinder-bestiary-3-items/1bmJGFD6zAmHJap5.htm)|Leng Ghoul Fever|Leng Ghoul Fever|modificada|
|[1BVkuiVh37hRA4y8.htm](pathfinder-bestiary-3-items/1BVkuiVh37hRA4y8.htm)|Vent Energy|Expulsar gases|modificada|
|[1BzSQkOqOPqFvz2u.htm](pathfinder-bestiary-3-items/1BzSQkOqOPqFvz2u.htm)|Dominate|Dominar|modificada|
|[1CAm301u2swnFr8b.htm](pathfinder-bestiary-3-items/1CAm301u2swnFr8b.htm)|Horn|Cuerno|modificada|
|[1cGNt3AXx2z6VszS.htm](pathfinder-bestiary-3-items/1cGNt3AXx2z6VszS.htm)|Constant Spells|Constant Spells|modificada|
|[1EJsPFGk71lyNvRf.htm](pathfinder-bestiary-3-items/1EJsPFGk71lyNvRf.htm)|Mauler|Vapulear|modificada|
|[1emEy5ty5iwJ1bDG.htm](pathfinder-bestiary-3-items/1emEy5ty5iwJ1bDG.htm)|Constant Spells|Constant Spells|modificada|
|[1EYcnayZrJgCFXca.htm](pathfinder-bestiary-3-items/1EYcnayZrJgCFXca.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[1F1dP9JtIaewb9ib.htm](pathfinder-bestiary-3-items/1F1dP9JtIaewb9ib.htm)|Catch Rock|Atrapar roca|modificada|
|[1F2GCLISmzNTXa9u.htm](pathfinder-bestiary-3-items/1F2GCLISmzNTXa9u.htm)|Extend Vines|Extend Vines|modificada|
|[1GpBs3jPWwtTzZjf.htm](pathfinder-bestiary-3-items/1GpBs3jPWwtTzZjf.htm)|Impossible Stature|Impossible Stature|modificada|
|[1hdOnHb2baGiZGTs.htm](pathfinder-bestiary-3-items/1hdOnHb2baGiZGTs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1hmYrhxS6td9Cs3g.htm](pathfinder-bestiary-3-items/1hmYrhxS6td9Cs3g.htm)|Heartsong|Heartsong|modificada|
|[1i0yksL3nMzWjZLz.htm](pathfinder-bestiary-3-items/1i0yksL3nMzWjZLz.htm)|Tickle|Cosquillas|modificada|
|[1IBSMp5wMRvadNtl.htm](pathfinder-bestiary-3-items/1IBSMp5wMRvadNtl.htm)|Hyponatremia|Hiponatremia|modificada|
|[1iD6khc2Ev2LOLO7.htm](pathfinder-bestiary-3-items/1iD6khc2Ev2LOLO7.htm)|Perfect Camouflage|Camuflaje Perfecto|modificada|
|[1Ld61yL8VIWBvakX.htm](pathfinder-bestiary-3-items/1Ld61yL8VIWBvakX.htm)|Swarm Shape|Forma de Enjambre|modificada|
|[1LUodDO3qpr7CYqM.htm](pathfinder-bestiary-3-items/1LUodDO3qpr7CYqM.htm)|Violent Retort|Violent Retort|modificada|
|[1lYiwUfYhh9bsuiW.htm](pathfinder-bestiary-3-items/1lYiwUfYhh9bsuiW.htm)|Buck|Encabritarse|modificada|
|[1Nzt9YANcjiUjxZu.htm](pathfinder-bestiary-3-items/1Nzt9YANcjiUjxZu.htm)|Liquefy|Licuar|modificada|
|[1oCth6jpnhdrJCOk.htm](pathfinder-bestiary-3-items/1oCth6jpnhdrJCOk.htm)|Walk the Ethereal Line|Camina por la línea Etérea|modificada|
|[1pEzu9hLGS2XL7cf.htm](pathfinder-bestiary-3-items/1pEzu9hLGS2XL7cf.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[1qHeEusR10OOW67e.htm](pathfinder-bestiary-3-items/1qHeEusR10OOW67e.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[1qvnCAYxCtqLHNWB.htm](pathfinder-bestiary-3-items/1qvnCAYxCtqLHNWB.htm)|Extend Limbs|Extend Limbs|modificada|
|[1RfKpuWxU1cMc4p7.htm](pathfinder-bestiary-3-items/1RfKpuWxU1cMc4p7.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[1RYNKTCRimaI46EV.htm](pathfinder-bestiary-3-items/1RYNKTCRimaI46EV.htm)|Sand Stride|Zancada de Arena|modificada|
|[1Skzxnout1K9TVNP.htm](pathfinder-bestiary-3-items/1Skzxnout1K9TVNP.htm)|Tied to the Land|Tied to the Land|modificada|
|[1Ssl4YAndiCjS1sF.htm](pathfinder-bestiary-3-items/1Ssl4YAndiCjS1sF.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[1sZ0LFXxfqkedyyb.htm](pathfinder-bestiary-3-items/1sZ0LFXxfqkedyyb.htm)|Swallow Whole|Engullir Todo|modificada|
|[1TjWSXYs7BlJtfxX.htm](pathfinder-bestiary-3-items/1TjWSXYs7BlJtfxX.htm)|Opening Statement|Apertura|modificada|
|[1UaZoefxmocVHBRY.htm](pathfinder-bestiary-3-items/1UaZoefxmocVHBRY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1URQxVXYnS7mCVrp.htm](pathfinder-bestiary-3-items/1URQxVXYnS7mCVrp.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[1vK61e5tiUHGEF7i.htm](pathfinder-bestiary-3-items/1vK61e5tiUHGEF7i.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[1xKmwhrUAlCPlAwz.htm](pathfinder-bestiary-3-items/1xKmwhrUAlCPlAwz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1YPP0PyKjErYFPk7.htm](pathfinder-bestiary-3-items/1YPP0PyKjErYFPk7.htm)|Paralytic Fear|Paralytic Fear|modificada|
|[1zABoyA2x54PjRbW.htm](pathfinder-bestiary-3-items/1zABoyA2x54PjRbW.htm)|Staff|Báculo|modificada|
|[1zDo0dT2WRDJ2XqD.htm](pathfinder-bestiary-3-items/1zDo0dT2WRDJ2XqD.htm)|Mist Vision|Mist Vision|modificada|
|[20T7ujLS0ToWyxkl.htm](pathfinder-bestiary-3-items/20T7ujLS0ToWyxkl.htm)|Inhabit Object|Habitar objeto|modificada|
|[276uGZ3JI9bINWVh.htm](pathfinder-bestiary-3-items/276uGZ3JI9bINWVh.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[2815rJTOaYW8FTZW.htm](pathfinder-bestiary-3-items/2815rJTOaYW8FTZW.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2aPAtYqGlSZ40pDT.htm](pathfinder-bestiary-3-items/2aPAtYqGlSZ40pDT.htm)|Reflect Spell|Reflejar conjuros|modificada|
|[2bcr0iC80HhF1QZo.htm](pathfinder-bestiary-3-items/2bcr0iC80HhF1QZo.htm)|Inhabit Vessel|Habitar Nave|modificada|
|[2BRmODyR3yH6ev3Q.htm](pathfinder-bestiary-3-items/2BRmODyR3yH6ev3Q.htm)|Swamp Stride|Swamp Stride|modificada|
|[2CGRxGWozrsdSvIi.htm](pathfinder-bestiary-3-items/2CGRxGWozrsdSvIi.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[2cVkSactyUiKGzdD.htm](pathfinder-bestiary-3-items/2cVkSactyUiKGzdD.htm)|Backdrop|Telón de fondo|modificada|
|[2DJRrjRKoxQStF9N.htm](pathfinder-bestiary-3-items/2DJRrjRKoxQStF9N.htm)|Kukri|Kukri|modificada|
|[2epurBlrSyaqWetH.htm](pathfinder-bestiary-3-items/2epurBlrSyaqWetH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2ERFu4hiXliGhhKj.htm](pathfinder-bestiary-3-items/2ERFu4hiXliGhhKj.htm)|All-Around Vision|All-Around Vision|modificada|
|[2GzvaeDl0Dl1kjjw.htm](pathfinder-bestiary-3-items/2GzvaeDl0Dl1kjjw.htm)|Drink Blood|Beber Sangre|modificada|
|[2iPW0FxKyAPkbkDp.htm](pathfinder-bestiary-3-items/2iPW0FxKyAPkbkDp.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2iSSH3y2Kuv4MNqO.htm](pathfinder-bestiary-3-items/2iSSH3y2Kuv4MNqO.htm)|Emotional Bolt|Rayo Emocional|modificada|
|[2IybDR9dBLK8bfWV.htm](pathfinder-bestiary-3-items/2IybDR9dBLK8bfWV.htm)|Agent of Fate|Agente del destino|modificada|
|[2JP7ZbSsTYsLb6ux.htm](pathfinder-bestiary-3-items/2JP7ZbSsTYsLb6ux.htm)|Aquatic Drag|Arrastre Acuático|modificada|
|[2JQEQa6cUhcYoUM4.htm](pathfinder-bestiary-3-items/2JQEQa6cUhcYoUM4.htm)|Tentacle|Tentáculo|modificada|
|[2kcFz29KySrfv5cW.htm](pathfinder-bestiary-3-items/2kcFz29KySrfv5cW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[2kFQsJv3fMYJ1RND.htm](pathfinder-bestiary-3-items/2kFQsJv3fMYJ1RND.htm)|Beak|Beak|modificada|
|[2kSrspDPmUktGvBB.htm](pathfinder-bestiary-3-items/2kSrspDPmUktGvBB.htm)|Tooth Tug|Tirón de dientes|modificada|
|[2KYuE80DPoFkgFGT.htm](pathfinder-bestiary-3-items/2KYuE80DPoFkgFGT.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[2LhHLzXgOoyNmS7p.htm](pathfinder-bestiary-3-items/2LhHLzXgOoyNmS7p.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2lp8axHkLWnfErrb.htm](pathfinder-bestiary-3-items/2lp8axHkLWnfErrb.htm)|Lower Spears!|¡Baja Spears!|modificada|
|[2mSXXYPRnq3gIcpj.htm](pathfinder-bestiary-3-items/2mSXXYPRnq3gIcpj.htm)|Grab|Agarrado|modificada|
|[2nEKM6ZrGJImSDZh.htm](pathfinder-bestiary-3-items/2nEKM6ZrGJImSDZh.htm)|Grab|Agarrado|modificada|
|[2NUCE7ph3hld0mgZ.htm](pathfinder-bestiary-3-items/2NUCE7ph3hld0mgZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2oNUku2hW5pSquMS.htm](pathfinder-bestiary-3-items/2oNUku2hW5pSquMS.htm)|Sap Mind|Sap Mind|modificada|
|[2Ou5MlYuSq3KB2Rn.htm](pathfinder-bestiary-3-items/2Ou5MlYuSq3KB2Rn.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2OyUOHJ2HoOkk9ki.htm](pathfinder-bestiary-3-items/2OyUOHJ2HoOkk9ki.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[2PoHFE6FWRaWarJD.htm](pathfinder-bestiary-3-items/2PoHFE6FWRaWarJD.htm)|Breath Weapon|Breath Weapon|modificada|
|[2pZ54UBlAD1KlL3E.htm](pathfinder-bestiary-3-items/2pZ54UBlAD1KlL3E.htm)|Flame|Flamígera|modificada|
|[2Qt36u2pXWig3wKR.htm](pathfinder-bestiary-3-items/2Qt36u2pXWig3wKR.htm)|Shell Block|Bloqueo con caparazón|modificada|
|[2rRPRh9T9ed91v7l.htm](pathfinder-bestiary-3-items/2rRPRh9T9ed91v7l.htm)|Cacophony|Cacofonía|modificada|
|[2rT3ehZEN8ZxCrCM.htm](pathfinder-bestiary-3-items/2rT3ehZEN8ZxCrCM.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2Rzw5Pms3YBG0FNZ.htm](pathfinder-bestiary-3-items/2Rzw5Pms3YBG0FNZ.htm)|Viper Swarm Venom|Veneno de enjambre de víboras|modificada|
|[2tYrB09YxVQESLhH.htm](pathfinder-bestiary-3-items/2tYrB09YxVQESLhH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2vb8hf7ikquLEd9z.htm](pathfinder-bestiary-3-items/2vb8hf7ikquLEd9z.htm)|Aquatic Echolocation (Precise) 120 feet|Ecolocalización acuática (precisión) 120 pies.|modificada|
|[2Vj9sjJhfJuSzBn1.htm](pathfinder-bestiary-3-items/2Vj9sjJhfJuSzBn1.htm)|Grab|Agarrado|modificada|
|[2xn05F1PZDFa3Es4.htm](pathfinder-bestiary-3-items/2xn05F1PZDFa3Es4.htm)|Jaws|Fauces|modificada|
|[2yxDzeKlqEJcTu27.htm](pathfinder-bestiary-3-items/2yxDzeKlqEJcTu27.htm)|Uncanny Pounce|Uncanny Abalanzarse|modificada|
|[302qi3pb2HGvsM6E.htm](pathfinder-bestiary-3-items/302qi3pb2HGvsM6E.htm)|Shield Spikes|Púas de escudo|modificada|
|[333AoGB4COJFtWLx.htm](pathfinder-bestiary-3-items/333AoGB4COJFtWLx.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[34s5p7VALREkL8Sh.htm](pathfinder-bestiary-3-items/34s5p7VALREkL8Sh.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[34yAb411T3yPWYAp.htm](pathfinder-bestiary-3-items/34yAb411T3yPWYAp.htm)|Darting Legs|Darting Legs|modificada|
|[37C5tkSFoAf7HJd2.htm](pathfinder-bestiary-3-items/37C5tkSFoAf7HJd2.htm)|Fist|Puño|modificada|
|[3b4YyISk8knkUFuM.htm](pathfinder-bestiary-3-items/3b4YyISk8knkUFuM.htm)|Falchion|Falchion|modificada|
|[3BM49h9dJSP6UcHN.htm](pathfinder-bestiary-3-items/3BM49h9dJSP6UcHN.htm)|Enraged Home (Piercing)|Casa Enfurecida (Perforante)|modificada|
|[3BpK7a2mX5UCDLmB.htm](pathfinder-bestiary-3-items/3BpK7a2mX5UCDLmB.htm)|Stick a Fork in It|Stick a Fork in It|modificada|
|[3bqSjZ8OFBahcmGk.htm](pathfinder-bestiary-3-items/3bqSjZ8OFBahcmGk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3Epd1MoTcY2zIoJQ.htm](pathfinder-bestiary-3-items/3Epd1MoTcY2zIoJQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3eyaSs1hwilHo39q.htm](pathfinder-bestiary-3-items/3eyaSs1hwilHo39q.htm)|Frightful Presence|Frightful Presence|modificada|
|[3F0FPP20eSpY3u1Q.htm](pathfinder-bestiary-3-items/3F0FPP20eSpY3u1Q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3FHM8X9p0VGx9jpe.htm](pathfinder-bestiary-3-items/3FHM8X9p0VGx9jpe.htm)|Rock|Roca|modificada|
|[3FtOenHRAd3nreJ2.htm](pathfinder-bestiary-3-items/3FtOenHRAd3nreJ2.htm)|Jaws|Fauces|modificada|
|[3gpkwgTnI0kfqxxS.htm](pathfinder-bestiary-3-items/3gpkwgTnI0kfqxxS.htm)|Missile Volley|Volea de misiles|modificada|
|[3gYN9UrAQK8utoKH.htm](pathfinder-bestiary-3-items/3gYN9UrAQK8utoKH.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[3ISG9gfCmesBzeYt.htm](pathfinder-bestiary-3-items/3ISG9gfCmesBzeYt.htm)|Nosferatu Vulnerabilities|Vulnerabilidades Nosferatu|modificada|
|[3IZdVb2IEp23wALM.htm](pathfinder-bestiary-3-items/3IZdVb2IEp23wALM.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[3jrrGptpZSe7SNzt.htm](pathfinder-bestiary-3-items/3jrrGptpZSe7SNzt.htm)|Constant Spells|Constant Spells|modificada|
|[3keJLwwlQMlCUtDM.htm](pathfinder-bestiary-3-items/3keJLwwlQMlCUtDM.htm)|Frightful Presence|Frightful Presence|modificada|
|[3ltFeU7t0v6ZbQqL.htm](pathfinder-bestiary-3-items/3ltFeU7t0v6ZbQqL.htm)|Sudden Destruction|Sudden Destruction|modificada|
|[3MP8S4relPgvIRf2.htm](pathfinder-bestiary-3-items/3MP8S4relPgvIRf2.htm)|Hat Toss|Hat Toss|modificada|
|[3nAIvklO9ro6F88p.htm](pathfinder-bestiary-3-items/3nAIvklO9ro6F88p.htm)|Big Claw|Gran Garra|modificada|
|[3OQIsBgxhUgao5MD.htm](pathfinder-bestiary-3-items/3OQIsBgxhUgao5MD.htm)|Nourishing Feast|Festín Nutritivo|modificada|
|[3p5BUgo3SACdAkGj.htm](pathfinder-bestiary-3-items/3p5BUgo3SACdAkGj.htm)|Leaping Pounce|Salto sin carrerilla|modificada|
|[3r5kNijs14DNKRqh.htm](pathfinder-bestiary-3-items/3r5kNijs14DNKRqh.htm)|Ice Climb|Trepar por el hielo|modificada|
|[3SCP38lxFNM5HyPh.htm](pathfinder-bestiary-3-items/3SCP38lxFNM5HyPh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[3sFxSnysZjKDkccp.htm](pathfinder-bestiary-3-items/3sFxSnysZjKDkccp.htm)|Jaws|Fauces|modificada|
|[3SJ0K1VlKcITVMvi.htm](pathfinder-bestiary-3-items/3SJ0K1VlKcITVMvi.htm)|Twisting Thrash|Twisting Sacudirse|modificada|
|[3UNbqS2zBuQO6NaW.htm](pathfinder-bestiary-3-items/3UNbqS2zBuQO6NaW.htm)|Constant Spells|Constant Spells|modificada|
|[3vdP680iRFJVfvwV.htm](pathfinder-bestiary-3-items/3vdP680iRFJVfvwV.htm)|Sudden Charge|Carga súbita|modificada|
|[3VhaqjXhDYgbAlz2.htm](pathfinder-bestiary-3-items/3VhaqjXhDYgbAlz2.htm)|Smoke Vision|Visión de Humo|modificada|
|[3VjRp3T0kmoqaYH2.htm](pathfinder-bestiary-3-items/3VjRp3T0kmoqaYH2.htm)|Break Swell|Break Swell|modificada|
|[3VJSS90TtdqUONTz.htm](pathfinder-bestiary-3-items/3VJSS90TtdqUONTz.htm)|Constant Spells|Constant Spells|modificada|
|[3wLOdV2RXEY5pnF5.htm](pathfinder-bestiary-3-items/3wLOdV2RXEY5pnF5.htm)|Mutilating Bite|Muerdemuerde|modificada|
|[3Wnn8u8Vi6jbroLI.htm](pathfinder-bestiary-3-items/3Wnn8u8Vi6jbroLI.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[3xQc4jPhxkh2EbIF.htm](pathfinder-bestiary-3-items/3xQc4jPhxkh2EbIF.htm)|Claw|Garra|modificada|
|[3z3CSczhmYzfNCxB.htm](pathfinder-bestiary-3-items/3z3CSczhmYzfNCxB.htm)|Ward|Ward|modificada|
|[3ZfdDyLTXwOFpx2V.htm](pathfinder-bestiary-3-items/3ZfdDyLTXwOFpx2V.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[3zY8sXqA8CTtAm4D.htm](pathfinder-bestiary-3-items/3zY8sXqA8CTtAm4D.htm)|Lingering Enmity|Enemistad persistente|modificada|
|[3ZZyaQ6uTOW9lyxY.htm](pathfinder-bestiary-3-items/3ZZyaQ6uTOW9lyxY.htm)|Tail|Tail|modificada|
|[40daZI7TkA0gZGz3.htm](pathfinder-bestiary-3-items/40daZI7TkA0gZGz3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[40G9Tq0fb0vTHD9L.htm](pathfinder-bestiary-3-items/40G9Tq0fb0vTHD9L.htm)|Negative Healing|Curación negativa|modificada|
|[41sWFM6OF3ytKfyk.htm](pathfinder-bestiary-3-items/41sWFM6OF3ytKfyk.htm)|Drink Abundance|Beber Abundancia|modificada|
|[425lZ6UBU1Pd31FP.htm](pathfinder-bestiary-3-items/425lZ6UBU1Pd31FP.htm)|Change Shape|Change Shape|modificada|
|[42qxpi5EYfZ17E0X.htm](pathfinder-bestiary-3-items/42qxpi5EYfZ17E0X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[43AUXc5WoGyh1pKT.htm](pathfinder-bestiary-3-items/43AUXc5WoGyh1pKT.htm)|Drench|Sofocar|modificada|
|[43VfyQa8GnQbb1JR.htm](pathfinder-bestiary-3-items/43VfyQa8GnQbb1JR.htm)|Team Attack|Team Attack|modificada|
|[45zsYQgswZpSa14E.htm](pathfinder-bestiary-3-items/45zsYQgswZpSa14E.htm)|Resonance|Resonance|modificada|
|[46fsaAZvo5Pu9usU.htm](pathfinder-bestiary-3-items/46fsaAZvo5Pu9usU.htm)|Troop Movement|Movimiento de tropas|modificada|
|[46M4PewZUpmrWn09.htm](pathfinder-bestiary-3-items/46M4PewZUpmrWn09.htm)|Longsword|Longsword|modificada|
|[48uJqrNPzQyywSb2.htm](pathfinder-bestiary-3-items/48uJqrNPzQyywSb2.htm)|Claw|Garra|modificada|
|[4923v7qhGbPOnzkA.htm](pathfinder-bestiary-3-items/4923v7qhGbPOnzkA.htm)|Xiuh Couatl Venom|Xiuh Couatl Venom|modificada|
|[49AEhw1auYxOww4q.htm](pathfinder-bestiary-3-items/49AEhw1auYxOww4q.htm)|Feast|Festín|modificada|
|[4ajY6AeHzOgwoB3H.htm](pathfinder-bestiary-3-items/4ajY6AeHzOgwoB3H.htm)|Slip|Slip|modificada|
|[4bIGF4lDjFiCWkiL.htm](pathfinder-bestiary-3-items/4bIGF4lDjFiCWkiL.htm)|Strafing Chomp|Strafing Chomp|modificada|
|[4csEUCYiZOJmhWRo.htm](pathfinder-bestiary-3-items/4csEUCYiZOJmhWRo.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[4cx1d9efogHuWe26.htm](pathfinder-bestiary-3-items/4cx1d9efogHuWe26.htm)|Wreck|Wreck|modificada|
|[4DUpOE1nhjDUloIt.htm](pathfinder-bestiary-3-items/4DUpOE1nhjDUloIt.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[4DxK55znXhBy05Je.htm](pathfinder-bestiary-3-items/4DxK55znXhBy05Je.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[4FIPIf24n9DntnSj.htm](pathfinder-bestiary-3-items/4FIPIf24n9DntnSj.htm)|Rend|Rasgadura|modificada|
|[4FtT2fJlh9Q0McYv.htm](pathfinder-bestiary-3-items/4FtT2fJlh9Q0McYv.htm)|Tackle|Tackle|modificada|
|[4gjSVNhiTon9n0Xd.htm](pathfinder-bestiary-3-items/4gjSVNhiTon9n0Xd.htm)|Swarming Strikes|Golpes de Enjambre|modificada|
|[4j8tGzWuHcBuRSsZ.htm](pathfinder-bestiary-3-items/4j8tGzWuHcBuRSsZ.htm)|Frightful Presence|Frightful Presence|modificada|
|[4JqTaWMRh2PWQLJX.htm](pathfinder-bestiary-3-items/4JqTaWMRh2PWQLJX.htm)|Foot|Pie|modificada|
|[4JSskrFw05x7ggm1.htm](pathfinder-bestiary-3-items/4JSskrFw05x7ggm1.htm)|Kick Back|Kick Back|modificada|
|[4k6Uvj17HBcW7ylM.htm](pathfinder-bestiary-3-items/4k6Uvj17HBcW7ylM.htm)|Energy Conversion|Conversión de Energía|modificada|
|[4KGlzeHqmtsJnLv9.htm](pathfinder-bestiary-3-items/4KGlzeHqmtsJnLv9.htm)|Spectral Hand|Mano espectral|modificada|
|[4nSekhH06OdsuVpK.htm](pathfinder-bestiary-3-items/4nSekhH06OdsuVpK.htm)|Dominate|Dominar|modificada|
|[4q3DAdCBSdwFd8Ge.htm](pathfinder-bestiary-3-items/4q3DAdCBSdwFd8Ge.htm)|Claw|Garra|modificada|
|[4Q80XDMMBCj2Mihv.htm](pathfinder-bestiary-3-items/4Q80XDMMBCj2Mihv.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4QajQWtHxBUlKsgo.htm](pathfinder-bestiary-3-items/4QajQWtHxBUlKsgo.htm)|Reconstitution|Reconstitución|modificada|
|[4QFJBrjb8mR8VE6I.htm](pathfinder-bestiary-3-items/4QFJBrjb8mR8VE6I.htm)|Stormcalling|Stormcalling|modificada|
|[4QS3vuiF6NBVHzYN.htm](pathfinder-bestiary-3-items/4QS3vuiF6NBVHzYN.htm)|Blood Wake|Estela de Sangre|modificada|
|[4r3fZEvFKQpzQMJ0.htm](pathfinder-bestiary-3-items/4r3fZEvFKQpzQMJ0.htm)|Dagger|Daga|modificada|
|[4rjTham5fRzKLGA2.htm](pathfinder-bestiary-3-items/4rjTham5fRzKLGA2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4SUjs1Fnnil2m2Qt.htm](pathfinder-bestiary-3-items/4SUjs1Fnnil2m2Qt.htm)|Cursed Gaze|Mirada Maldita|modificada|
|[4t55o9zeccODBur0.htm](pathfinder-bestiary-3-items/4t55o9zeccODBur0.htm)|Stitch Skin|Stitch Skin|modificada|
|[4t6lOiLhCYAlFpnf.htm](pathfinder-bestiary-3-items/4t6lOiLhCYAlFpnf.htm)|Greatsword|Greatsword|modificada|
|[4TqIsk44LcZm61Ws.htm](pathfinder-bestiary-3-items/4TqIsk44LcZm61Ws.htm)|Rend|Rasgadura|modificada|
|[4vjdVbvQ2cPvkxA1.htm](pathfinder-bestiary-3-items/4vjdVbvQ2cPvkxA1.htm)|Seek Quarry|Buscar Cantera|modificada|
|[4W60RWwqP4LPF4Ve.htm](pathfinder-bestiary-3-items/4W60RWwqP4LPF4Ve.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[4w6cni5pDSonPMZW.htm](pathfinder-bestiary-3-items/4w6cni5pDSonPMZW.htm)|Shortsword|Espada corta|modificada|
|[4W98qLPXYtwytrBj.htm](pathfinder-bestiary-3-items/4W98qLPXYtwytrBj.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4wEJM0uffDwyN4QS.htm](pathfinder-bestiary-3-items/4wEJM0uffDwyN4QS.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[4x1dSgcNIbL7YtTa.htm](pathfinder-bestiary-3-items/4x1dSgcNIbL7YtTa.htm)|Frightening Howl|Frightening Howl|modificada|
|[4XdLVNceTaTeWXxs.htm](pathfinder-bestiary-3-items/4XdLVNceTaTeWXxs.htm)|Weak Acid|Ácido débil|modificada|
|[4y0Im0eAbj4YFOII.htm](pathfinder-bestiary-3-items/4y0Im0eAbj4YFOII.htm)|Troop Movement|Movimiento de tropas|modificada|
|[4yxB753affbJoFUo.htm](pathfinder-bestiary-3-items/4yxB753affbJoFUo.htm)|Constant Spells|Constant Spells|modificada|
|[4ZK6UkKCERwU9jVd.htm](pathfinder-bestiary-3-items/4ZK6UkKCERwU9jVd.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[50TREly1W5yrRm7R.htm](pathfinder-bestiary-3-items/50TREly1W5yrRm7R.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[51mhuepTTAtKvDL3.htm](pathfinder-bestiary-3-items/51mhuepTTAtKvDL3.htm)|Diseased Pustules|Pústulas enfermas|modificada|
|[53ZHZQrxzfyMUm3s.htm](pathfinder-bestiary-3-items/53ZHZQrxzfyMUm3s.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[54ARy8tIUzIr76GH.htm](pathfinder-bestiary-3-items/54ARy8tIUzIr76GH.htm)|Fed by Metal|Alimentado por Metal|modificada|
|[55pDhEb6m82JCtfR.htm](pathfinder-bestiary-3-items/55pDhEb6m82JCtfR.htm)|Abrogation of Consequences|Abrogación de Consecuencias|modificada|
|[56iLpxxqpM1gYATc.htm](pathfinder-bestiary-3-items/56iLpxxqpM1gYATc.htm)|Death Gaze|Mirada de la Muerte|modificada|
|[56KGSD8qAWdFbvdP.htm](pathfinder-bestiary-3-items/56KGSD8qAWdFbvdP.htm)|Divine Innate Spells (See Ganzi Spells)|Hechizos innatos divinos (Ver Hechizos de Ganz).|modificada|
|[58QNGdCWXAUWVAti.htm](pathfinder-bestiary-3-items/58QNGdCWXAUWVAti.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5AumaQTWt3WVkDej.htm](pathfinder-bestiary-3-items/5AumaQTWt3WVkDej.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[5auWJrdx79CUpWa2.htm](pathfinder-bestiary-3-items/5auWJrdx79CUpWa2.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[5B3uRzeJSlXUyJPR.htm](pathfinder-bestiary-3-items/5B3uRzeJSlXUyJPR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5d0jyVv6vjBbVxdc.htm](pathfinder-bestiary-3-items/5d0jyVv6vjBbVxdc.htm)|Sure Stride|Sure Zancada|modificada|
|[5e9nAAnEPwtzAoiR.htm](pathfinder-bestiary-3-items/5e9nAAnEPwtzAoiR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5eqtzIqWpDscyCIh.htm](pathfinder-bestiary-3-items/5eqtzIqWpDscyCIh.htm)|Breath Weapon|Breath Weapon|modificada|
|[5FoNMqcRkDgmHGWx.htm](pathfinder-bestiary-3-items/5FoNMqcRkDgmHGWx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5FqnH65aSIz1yYu4.htm](pathfinder-bestiary-3-items/5FqnH65aSIz1yYu4.htm)|Share Pain|Share Pain|modificada|
|[5gfJon4GXyqVHTz0.htm](pathfinder-bestiary-3-items/5gfJon4GXyqVHTz0.htm)|Negative Healing|Curación negativa|modificada|
|[5gY8gk28J2srHl1t.htm](pathfinder-bestiary-3-items/5gY8gk28J2srHl1t.htm)|Skip Between|Saltar entre|modificada|
|[5h2xMj0CQYOauzxy.htm](pathfinder-bestiary-3-items/5h2xMj0CQYOauzxy.htm)|Monk Ki Spells|Conjuros de ki del monje|modificada|
|[5hGIIhdbBibMGMJ3.htm](pathfinder-bestiary-3-items/5hGIIhdbBibMGMJ3.htm)|Cat's Grace|Cat's Grace|modificada|
|[5hNT46xUSUSqA2x1.htm](pathfinder-bestiary-3-items/5hNT46xUSUSqA2x1.htm)|Musk|Almizcle|modificada|
|[5I1UyAH3ppy0Pi6l.htm](pathfinder-bestiary-3-items/5I1UyAH3ppy0Pi6l.htm)|Rib Skewer|Ensartar Costilla|modificada|
|[5iBIN1PlI1tshlnI.htm](pathfinder-bestiary-3-items/5iBIN1PlI1tshlnI.htm)|Knockdown|Derribo|modificada|
|[5iH51KTA8CfAOS08.htm](pathfinder-bestiary-3-items/5iH51KTA8CfAOS08.htm)|Light Wisp|Luz Wisp|modificada|
|[5IHuVnedjxI2lxzF.htm](pathfinder-bestiary-3-items/5IHuVnedjxI2lxzF.htm)|Distracting Gaze|Mirada Distractora|modificada|
|[5iw21DmwOYFPkVbC.htm](pathfinder-bestiary-3-items/5iw21DmwOYFPkVbC.htm)|Prickly Burst|Ráfaga espinosa|modificada|
|[5j0da1f0NNKtJ9uB.htm](pathfinder-bestiary-3-items/5j0da1f0NNKtJ9uB.htm)|Pulse of Rage|Pulso de Furia|modificada|
|[5JFwqj1O27XBnI7T.htm](pathfinder-bestiary-3-items/5JFwqj1O27XBnI7T.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[5JGaGCjUurVrjoyX.htm](pathfinder-bestiary-3-items/5JGaGCjUurVrjoyX.htm)|Unfathomable Infinity|Infinito Insondable|modificada|
|[5JjMeb6t3MgQy8HQ.htm](pathfinder-bestiary-3-items/5JjMeb6t3MgQy8HQ.htm)|Rock|Roca|modificada|
|[5mZBnyI8jN8hRDpC.htm](pathfinder-bestiary-3-items/5mZBnyI8jN8hRDpC.htm)|Barbed Net|Red de púas|modificada|
|[5NPHDSBtMnU44c52.htm](pathfinder-bestiary-3-items/5NPHDSBtMnU44c52.htm)|Rally|Rally|modificada|
|[5OeW9Mm1qaPRCaYt.htm](pathfinder-bestiary-3-items/5OeW9Mm1qaPRCaYt.htm)|+2 to All Saves vs. Poison|+2 a todas las salvaciones contra veneno|modificada|
|[5OmSIpE30Qir6OKb.htm](pathfinder-bestiary-3-items/5OmSIpE30Qir6OKb.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[5OPP2h29MKUEtig6.htm](pathfinder-bestiary-3-items/5OPP2h29MKUEtig6.htm)|Ectoplasmic Secretions|Ectoplasmic Secretions|modificada|
|[5OTunhc4CEJ5XrAr.htm](pathfinder-bestiary-3-items/5OTunhc4CEJ5XrAr.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[5pRHBAOx7kPRJDBR.htm](pathfinder-bestiary-3-items/5pRHBAOx7kPRJDBR.htm)|Negative Healing|Curación negativa|modificada|
|[5PRYxLiEoiOScKcc.htm](pathfinder-bestiary-3-items/5PRYxLiEoiOScKcc.htm)|Swift Steps|Pasos Rápidos|modificada|
|[5qFwYj9ry9CVIKY7.htm](pathfinder-bestiary-3-items/5qFwYj9ry9CVIKY7.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[5rmiy80ibEEvlvIL.htm](pathfinder-bestiary-3-items/5rmiy80ibEEvlvIL.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[5RtC2ASqDYa1fPfS.htm](pathfinder-bestiary-3-items/5RtC2ASqDYa1fPfS.htm)|Constant Spells|Constant Spells|modificada|
|[5SsaserwdJ4my9cj.htm](pathfinder-bestiary-3-items/5SsaserwdJ4my9cj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5SZG9rr8f11whg0x.htm](pathfinder-bestiary-3-items/5SZG9rr8f11whg0x.htm)|Speak with Bats|Habla con los murciélagos|modificada|
|[5TY3CK7BZ3ZrKA1C.htm](pathfinder-bestiary-3-items/5TY3CK7BZ3ZrKA1C.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[5u8Fh5gvFK0DreBo.htm](pathfinder-bestiary-3-items/5u8Fh5gvFK0DreBo.htm)|Handspring Kick|Handspring Kick|modificada|
|[5VVtnGIh8V9iyhaS.htm](pathfinder-bestiary-3-items/5VVtnGIh8V9iyhaS.htm)|Sense Apostate 500 feet|Sense Apostate 500 feet|modificada|
|[5WhjDHH8kb9e6f8l.htm](pathfinder-bestiary-3-items/5WhjDHH8kb9e6f8l.htm)|Shadowplay|Juego de sombras|modificada|
|[5wlVfCVGmDq3fRKw.htm](pathfinder-bestiary-3-items/5wlVfCVGmDq3fRKw.htm)|Malodorous Smoke|Humo Maloliente|modificada|
|[5XfUKnzADDIB9IN0.htm](pathfinder-bestiary-3-items/5XfUKnzADDIB9IN0.htm)|Create Spawn|Elaborar Spawn|modificada|
|[5YGPeCSsTq71DxAb.htm](pathfinder-bestiary-3-items/5YGPeCSsTq71DxAb.htm)|Souleater|Souleater|modificada|
|[5YK8hJa0XyWYTPW1.htm](pathfinder-bestiary-3-items/5YK8hJa0XyWYTPW1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5zASRad3bigyGHMo.htm](pathfinder-bestiary-3-items/5zASRad3bigyGHMo.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[5ZYqE0wvrRymU2Bg.htm](pathfinder-bestiary-3-items/5ZYqE0wvrRymU2Bg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[60CCB5Edf3n6TRxV.htm](pathfinder-bestiary-3-items/60CCB5Edf3n6TRxV.htm)|Tongue|Lengua|modificada|
|[60deOo2mPqEopbvy.htm](pathfinder-bestiary-3-items/60deOo2mPqEopbvy.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[61RVU0BzmvnLofFj.htm](pathfinder-bestiary-3-items/61RVU0BzmvnLofFj.htm)|Absorb Evocation|Absorber Evocación|modificada|
|[62PNJhmFdbtT0TrS.htm](pathfinder-bestiary-3-items/62PNJhmFdbtT0TrS.htm)|Voice Imitation|Imitación de voz|modificada|
|[62tSPn9Q3DkS4DA9.htm](pathfinder-bestiary-3-items/62tSPn9Q3DkS4DA9.htm)|Sandwalking|Sandwalking|modificada|
|[64CXDHwnDpHKKZpp.htm](pathfinder-bestiary-3-items/64CXDHwnDpHKKZpp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[64fFy6rN4KqZRePB.htm](pathfinder-bestiary-3-items/64fFy6rN4KqZRePB.htm)|Feign Death|Fingir Muerte|modificada|
|[66fDZvUWClHrgbKL.htm](pathfinder-bestiary-3-items/66fDZvUWClHrgbKL.htm)|Breath Weapon|Breath Weapon|modificada|
|[66GzinTfrKKU8xfg.htm](pathfinder-bestiary-3-items/66GzinTfrKKU8xfg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[66q0s4rjQzYyXMxq.htm](pathfinder-bestiary-3-items/66q0s4rjQzYyXMxq.htm)|Snake Search|Registrar Serpiente|modificada|
|[69BcgWueS6EljWeK.htm](pathfinder-bestiary-3-items/69BcgWueS6EljWeK.htm)|Benthic Wave|Benthic Wave|modificada|
|[6cqbtL8P73BAKLlb.htm](pathfinder-bestiary-3-items/6cqbtL8P73BAKLlb.htm)|Void Weapon|Void Weapon|modificada|
|[6CxEvaX1CFDQ7rOR.htm](pathfinder-bestiary-3-items/6CxEvaX1CFDQ7rOR.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[6d6cIadAucEYZpne.htm](pathfinder-bestiary-3-items/6d6cIadAucEYZpne.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[6D8hv3P04IeQWepE.htm](pathfinder-bestiary-3-items/6D8hv3P04IeQWepE.htm)|Alien Vestment|Alien Vestment|modificada|
|[6DJ5UGod5sVQIgQ8.htm](pathfinder-bestiary-3-items/6DJ5UGod5sVQIgQ8.htm)|Sunset Ribbon|Sunset Ribbon|modificada|
|[6fc7mDScSYrAh4aK.htm](pathfinder-bestiary-3-items/6fc7mDScSYrAh4aK.htm)|Broom|Escoba|modificada|
|[6FLKit13w08Xyhqy.htm](pathfinder-bestiary-3-items/6FLKit13w08Xyhqy.htm)|Bond with Ward|Lazo con Ward|modificada|
|[6hgXjCxuZTwCxmFa.htm](pathfinder-bestiary-3-items/6hgXjCxuZTwCxmFa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6HIVko7t9eQdURP8.htm](pathfinder-bestiary-3-items/6HIVko7t9eQdURP8.htm)|Tendril|Tendril|modificada|
|[6jf1IhNb2PBN2aXV.htm](pathfinder-bestiary-3-items/6jf1IhNb2PBN2aXV.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[6Jhrih75LDb7Tovl.htm](pathfinder-bestiary-3-items/6Jhrih75LDb7Tovl.htm)|Warping Ray|Warping Ray|modificada|
|[6jmiViG6aADCKgpM.htm](pathfinder-bestiary-3-items/6jmiViG6aADCKgpM.htm)|Wing|Ala|modificada|
|[6jXWp7OM7AukkRT3.htm](pathfinder-bestiary-3-items/6jXWp7OM7AukkRT3.htm)|Bite|Muerdemuerde|modificada|
|[6kZm8XnYELAQFppU.htm](pathfinder-bestiary-3-items/6kZm8XnYELAQFppU.htm)|Bat Empathy|Bat Empathy|modificada|
|[6LlX67R81S5J80Ws.htm](pathfinder-bestiary-3-items/6LlX67R81S5J80Ws.htm)|All-Around Vision|All-Around Vision|modificada|
|[6LN4eVSW1LF6YGqK.htm](pathfinder-bestiary-3-items/6LN4eVSW1LF6YGqK.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[6ludWZkd4jUcg8tv.htm](pathfinder-bestiary-3-items/6ludWZkd4jUcg8tv.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[6LVQDNRocR70kvVD.htm](pathfinder-bestiary-3-items/6LVQDNRocR70kvVD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6m81W89YrbQ1gHjG.htm](pathfinder-bestiary-3-items/6m81W89YrbQ1gHjG.htm)|Elongate Tongue|Lengua alargada|modificada|
|[6meS6jDvt73ZAOZV.htm](pathfinder-bestiary-3-items/6meS6jDvt73ZAOZV.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[6mHmfXwMk0MX37HC.htm](pathfinder-bestiary-3-items/6mHmfXwMk0MX37HC.htm)|Greater Constrict|Mayor Restricción|modificada|
|[6OrGVyunvCJATyUD.htm](pathfinder-bestiary-3-items/6OrGVyunvCJATyUD.htm)|Self-Absorbed|Autoabsorbida|modificada|
|[6OT1Cj34QvB0zv3Q.htm](pathfinder-bestiary-3-items/6OT1Cj34QvB0zv3Q.htm)|Tail|Tail|modificada|
|[6PLzHEkZC8lh9I6a.htm](pathfinder-bestiary-3-items/6PLzHEkZC8lh9I6a.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6PZisICkQg9iEoQs.htm](pathfinder-bestiary-3-items/6PZisICkQg9iEoQs.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[6qxKmQ1NJ2m3caTx.htm](pathfinder-bestiary-3-items/6qxKmQ1NJ2m3caTx.htm)|No Breath|No Breath|modificada|
|[6QzeJqibXMpIAXDj.htm](pathfinder-bestiary-3-items/6QzeJqibXMpIAXDj.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[6r3WJI32FjeNNZlp.htm](pathfinder-bestiary-3-items/6r3WJI32FjeNNZlp.htm)|Echolocation 20 feet|Ecolocalización 20 pies|modificada|
|[6SDloGsttpr7ZAJq.htm](pathfinder-bestiary-3-items/6SDloGsttpr7ZAJq.htm)|Hatred of Beauty|El odio a la belleza|modificada|
|[6ThLzzOnr19ZeVm4.htm](pathfinder-bestiary-3-items/6ThLzzOnr19ZeVm4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6tZvLirOPTqzo6of.htm](pathfinder-bestiary-3-items/6tZvLirOPTqzo6of.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[6u4WiGSgJcSBMIVQ.htm](pathfinder-bestiary-3-items/6u4WiGSgJcSBMIVQ.htm)|Universal Language|Lenguaje Universal|modificada|
|[6U6A8ryVDLXX3rmv.htm](pathfinder-bestiary-3-items/6U6A8ryVDLXX3rmv.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[6UlwNI4X3CPg9JEG.htm](pathfinder-bestiary-3-items/6UlwNI4X3CPg9JEG.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[6wmw4xZcc4sGgnNR.htm](pathfinder-bestiary-3-items/6wmw4xZcc4sGgnNR.htm)|Carrion Fever|Carrion Fever|modificada|
|[6WZJcN2ygA1eIjQE.htm](pathfinder-bestiary-3-items/6WZJcN2ygA1eIjQE.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[6XahuYsNr6LHrkAs.htm](pathfinder-bestiary-3-items/6XahuYsNr6LHrkAs.htm)|Sudden Dive|Inmersión Súbita|modificada|
|[6xVdg4wmRT0c4mXW.htm](pathfinder-bestiary-3-items/6xVdg4wmRT0c4mXW.htm)|Sugar Rush|Sugar Embestida|modificada|
|[6yq9O6WHEsvMhrT8.htm](pathfinder-bestiary-3-items/6yq9O6WHEsvMhrT8.htm)|Change Shape|Change Shape|modificada|
|[6ZdU1MvTTYu6fH0E.htm](pathfinder-bestiary-3-items/6ZdU1MvTTYu6fH0E.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[6zHIzorHFZDIXyyR.htm](pathfinder-bestiary-3-items/6zHIzorHFZDIXyyR.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[6ZOExAAHG9cE1G7V.htm](pathfinder-bestiary-3-items/6ZOExAAHG9cE1G7V.htm)|Jaws|Fauces|modificada|
|[7101NEoU5qf7eU5R.htm](pathfinder-bestiary-3-items/7101NEoU5qf7eU5R.htm)|Greater Constrict|Mayor Restricción|modificada|
|[71GJwFaE95Ae7SXJ.htm](pathfinder-bestiary-3-items/71GJwFaE95Ae7SXJ.htm)|Mage Bond|Mage Bond|modificada|
|[71vyIq4WtorjpuSI.htm](pathfinder-bestiary-3-items/71vyIq4WtorjpuSI.htm)|Pseudopod|Pseudópodo|modificada|
|[71ZISy5sK6KLCTyg.htm](pathfinder-bestiary-3-items/71ZISy5sK6KLCTyg.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[72K68WtjLjkNXxWr.htm](pathfinder-bestiary-3-items/72K68WtjLjkNXxWr.htm)|Invigorating Feast|Festín vigorizante|modificada|
|[73Q1gfTlvsbrJIOV.htm](pathfinder-bestiary-3-items/73Q1gfTlvsbrJIOV.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[74cvbuceLeJciQVI.htm](pathfinder-bestiary-3-items/74cvbuceLeJciQVI.htm)|Flesh Grafting|Injerto de carne|modificada|
|[74QNWqV1T1TsPKzT.htm](pathfinder-bestiary-3-items/74QNWqV1T1TsPKzT.htm)|Rend|Rasgadura|modificada|
|[75r7SZRmZYqQiKfH.htm](pathfinder-bestiary-3-items/75r7SZRmZYqQiKfH.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[77tPlIEwFc6nptCm.htm](pathfinder-bestiary-3-items/77tPlIEwFc6nptCm.htm)|Wildfire Storm|Tormenta de Fuego Salvaje|modificada|
|[78eiTtsU1ddUvLm1.htm](pathfinder-bestiary-3-items/78eiTtsU1ddUvLm1.htm)|Dromophobia|Dromofobia|modificada|
|[7ASsgG1v1611ihJD.htm](pathfinder-bestiary-3-items/7ASsgG1v1611ihJD.htm)|Bile|Bilis|modificada|
|[7bHEWVYytDW7lLet.htm](pathfinder-bestiary-3-items/7bHEWVYytDW7lLet.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7bvs9Qjgwf9G0s4w.htm](pathfinder-bestiary-3-items/7bvs9Qjgwf9G0s4w.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7c1InSV9VXV4cB8Z.htm](pathfinder-bestiary-3-items/7c1InSV9VXV4cB8Z.htm)|Grasping Tail|Cola de Agarre|modificada|
|[7cb3300PlOTebHcM.htm](pathfinder-bestiary-3-items/7cb3300PlOTebHcM.htm)|Troop Spellcasting|Hechizo de Tropa|modificada|
|[7cU2Ki9ZUte4XeOY.htm](pathfinder-bestiary-3-items/7cU2Ki9ZUte4XeOY.htm)|Claw|Garra|modificada|
|[7F01r7qfkds4bfEo.htm](pathfinder-bestiary-3-items/7F01r7qfkds4bfEo.htm)|Claw|Garra|modificada|
|[7fBfykPx1yG8CYGz.htm](pathfinder-bestiary-3-items/7fBfykPx1yG8CYGz.htm)|Atrophic Plague|Plaga Atrófica|modificada|
|[7FCmN4UL4psU0tDh.htm](pathfinder-bestiary-3-items/7FCmN4UL4psU0tDh.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[7FPtqs0896vW7FwI.htm](pathfinder-bestiary-3-items/7FPtqs0896vW7FwI.htm)|Titanic Charge|Titanic Charge|modificada|
|[7GA62FWHgOMnOoir.htm](pathfinder-bestiary-3-items/7GA62FWHgOMnOoir.htm)|Pounce|Abalanzarse|modificada|
|[7H5EOw27GyoL0zDR.htm](pathfinder-bestiary-3-items/7H5EOw27GyoL0zDR.htm)|Nauseating Slap|Bofetada Nauseabunda|modificada|
|[7hmsuLy1J8DJxM6F.htm](pathfinder-bestiary-3-items/7hmsuLy1J8DJxM6F.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[7i9LTQqlhq2Hhy7j.htm](pathfinder-bestiary-3-items/7i9LTQqlhq2Hhy7j.htm)|Constant Spells|Constant Spells|modificada|
|[7Ja558AxHimR0DNe.htm](pathfinder-bestiary-3-items/7Ja558AxHimR0DNe.htm)|No Breath|No Breath|modificada|
|[7JznoiaHsY4zQn0z.htm](pathfinder-bestiary-3-items/7JznoiaHsY4zQn0z.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[7KrbqcGEJ1GaWuQc.htm](pathfinder-bestiary-3-items/7KrbqcGEJ1GaWuQc.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[7lG8jBDPOKE6KGt2.htm](pathfinder-bestiary-3-items/7lG8jBDPOKE6KGt2.htm)|Green Caress|Green Caress|modificada|
|[7M6TofwZ6Yvd0iw4.htm](pathfinder-bestiary-3-items/7M6TofwZ6Yvd0iw4.htm)|Change Shape|Change Shape|modificada|
|[7NvFdNeBt7AssuX2.htm](pathfinder-bestiary-3-items/7NvFdNeBt7AssuX2.htm)|Faithful Weapon|Arma Fiel|modificada|
|[7O5f2VrYGu8KYH37.htm](pathfinder-bestiary-3-items/7O5f2VrYGu8KYH37.htm)|Emit Spores|Emit Spores|modificada|
|[7OHhyz8S9jaICp6G.htm](pathfinder-bestiary-3-items/7OHhyz8S9jaICp6G.htm)|Root In Place|Enraizarse en el lugar|modificada|
|[7oiohGkxeOfZceVR.htm](pathfinder-bestiary-3-items/7oiohGkxeOfZceVR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[7osAd2va2Xd7Jqmw.htm](pathfinder-bestiary-3-items/7osAd2va2Xd7Jqmw.htm)|Ectoplasmic Form|Forma Ectoplasmica|modificada|
|[7q0sH9B8Zn1uCrNj.htm](pathfinder-bestiary-3-items/7q0sH9B8Zn1uCrNj.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[7qBjkooMOFnRW7Uy.htm](pathfinder-bestiary-3-items/7qBjkooMOFnRW7Uy.htm)|Jaws|Fauces|modificada|
|[7SJO477OusJy7wpB.htm](pathfinder-bestiary-3-items/7SJO477OusJy7wpB.htm)|Jaws|Fauces|modificada|
|[7SyelFSQ8Pix3kVJ.htm](pathfinder-bestiary-3-items/7SyelFSQ8Pix3kVJ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[7u9nInfiCammnrKi.htm](pathfinder-bestiary-3-items/7u9nInfiCammnrKi.htm)|Jaws|Fauces|modificada|
|[7UK0F7yfKDuLMdqq.htm](pathfinder-bestiary-3-items/7UK0F7yfKDuLMdqq.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[7ulY5fkbW89xf55O.htm](pathfinder-bestiary-3-items/7ulY5fkbW89xf55O.htm)|Pest Haven|Pest Haven|modificada|
|[7V9CRphWnj5Wbphk.htm](pathfinder-bestiary-3-items/7V9CRphWnj5Wbphk.htm)|Constant Spells|Constant Spells|modificada|
|[7VXwGJWTdkaP6Ch0.htm](pathfinder-bestiary-3-items/7VXwGJWTdkaP6Ch0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7wr42RamnztEcqpc.htm](pathfinder-bestiary-3-items/7wr42RamnztEcqpc.htm)|Send Beyond|Recado Más Allá|modificada|
|[7WVvb1W4IfVQZzSv.htm](pathfinder-bestiary-3-items/7WVvb1W4IfVQZzSv.htm)|Malevolent Possession|Posesión maléfica.|modificada|
|[7wxTRall7vLvrVdI.htm](pathfinder-bestiary-3-items/7wxTRall7vLvrVdI.htm)|Sharp Riposte|Riposte afilado|modificada|
|[7xqB2gVMxUHI1QMC.htm](pathfinder-bestiary-3-items/7xqB2gVMxUHI1QMC.htm)|Skeleton Crew|Skeleton Crew|modificada|
|[847XuFlQNdsrsK3G.htm](pathfinder-bestiary-3-items/847XuFlQNdsrsK3G.htm)|Roll Up|Roll Up|modificada|
|[85a0vfvP7BEWaC7o.htm](pathfinder-bestiary-3-items/85a0vfvP7BEWaC7o.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[85CNz1N6H7AEa4kU.htm](pathfinder-bestiary-3-items/85CNz1N6H7AEa4kU.htm)|Scimitar|Cimitarra|modificada|
|[87If4txGT9U4ihPf.htm](pathfinder-bestiary-3-items/87If4txGT9U4ihPf.htm)|Scrabbling Swarm|Scrabbling Swarm|modificada|
|[88a6l4wOhZKuqey0.htm](pathfinder-bestiary-3-items/88a6l4wOhZKuqey0.htm)|Flight Commander of Dis|Comandante de Vuelo de Dis|modificada|
|[88wfIYYh2caypx0G.htm](pathfinder-bestiary-3-items/88wfIYYh2caypx0G.htm)|Wavesense (Imprecise) 100 feet|Sentir ondulaciones (Impreciso) 100 pies.|modificada|
|[89pRruq95QsvuZYX.htm](pathfinder-bestiary-3-items/89pRruq95QsvuZYX.htm)|Sensory Fever|Sensory Fever|modificada|
|[89ujLImiUz00Zomc.htm](pathfinder-bestiary-3-items/89ujLImiUz00Zomc.htm)|Lance Charge|Lance Charge|modificada|
|[8AK5AzyX03GXrikY.htm](pathfinder-bestiary-3-items/8AK5AzyX03GXrikY.htm)|Fist|Puño|modificada|
|[8Amc9UddnS7QBYOL.htm](pathfinder-bestiary-3-items/8Amc9UddnS7QBYOL.htm)|Cryptomnesia|Criptomnesia|modificada|
|[8B31oTS6yoGOkPve.htm](pathfinder-bestiary-3-items/8B31oTS6yoGOkPve.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[8BmcD1iEjqhQRthD.htm](pathfinder-bestiary-3-items/8BmcD1iEjqhQRthD.htm)|Tail|Tail|modificada|
|[8cr5UQhOU7Fs4x6B.htm](pathfinder-bestiary-3-items/8cr5UQhOU7Fs4x6B.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[8cXuUewtlaWaE5MT.htm](pathfinder-bestiary-3-items/8cXuUewtlaWaE5MT.htm)|Tendril|Tendril|modificada|
|[8e4jbz96UsZDTcka.htm](pathfinder-bestiary-3-items/8e4jbz96UsZDTcka.htm)|Retaliate|Retaliate|modificada|
|[8EzoTBKzzz2kcPsk.htm](pathfinder-bestiary-3-items/8EzoTBKzzz2kcPsk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8FgEIFUzEgLTHjHc.htm](pathfinder-bestiary-3-items/8FgEIFUzEgLTHjHc.htm)|Fearsense (Precise) 60 feet|Fearsense (Preciso) 60 pies.|modificada|
|[8GbsrAKrtoDroA9Y.htm](pathfinder-bestiary-3-items/8GbsrAKrtoDroA9Y.htm)|Snout|Hocico|modificada|
|[8GmTiza6VxQxjlFA.htm](pathfinder-bestiary-3-items/8GmTiza6VxQxjlFA.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[8GuVI0hREIK9sjQW.htm](pathfinder-bestiary-3-items/8GuVI0hREIK9sjQW.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[8hoPxayirM6ubTyC.htm](pathfinder-bestiary-3-items/8hoPxayirM6ubTyC.htm)|Longspear|Longspear|modificada|
|[8HtO6GuiGguIFsmV.htm](pathfinder-bestiary-3-items/8HtO6GuiGguIFsmV.htm)|Spine|Spine|modificada|
|[8IAg57ZRzV51urlh.htm](pathfinder-bestiary-3-items/8IAg57ZRzV51urlh.htm)|Warhammer|Warhammer|modificada|
|[8IQ2N3VKsykBfWvw.htm](pathfinder-bestiary-3-items/8IQ2N3VKsykBfWvw.htm)|Pincer|Pinza|modificada|
|[8isKGe0HwKWmQDEg.htm](pathfinder-bestiary-3-items/8isKGe0HwKWmQDEg.htm)|Focus Beauty|Focus Belleza|modificada|
|[8jAtUvdgKdyaNzHN.htm](pathfinder-bestiary-3-items/8jAtUvdgKdyaNzHN.htm)|Fiery Wake|Estela Ardiente|modificada|
|[8JO7dr59w8qgOjKx.htm](pathfinder-bestiary-3-items/8JO7dr59w8qgOjKx.htm)|Sling|Sling|modificada|
|[8k94OQXOqEJ2FUbE.htm](pathfinder-bestiary-3-items/8k94OQXOqEJ2FUbE.htm)|Grab|Agarrado|modificada|
|[8KGUlBcPtdSEm805.htm](pathfinder-bestiary-3-items/8KGUlBcPtdSEm805.htm)|Fist|Puño|modificada|
|[8kIjzzH6mYzHjhor.htm](pathfinder-bestiary-3-items/8kIjzzH6mYzHjhor.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8kKowoHm6gXmxhb1.htm](pathfinder-bestiary-3-items/8kKowoHm6gXmxhb1.htm)|-1 to All Saves vs. Emotion Effects|-1 a Todas las Salvaciones contra Efectos de Emoción|modificada|
|[8LHwhivJZ4n6E2Gd.htm](pathfinder-bestiary-3-items/8LHwhivJZ4n6E2Gd.htm)|Mass Wriggle|Retorcimiento Masivo|modificada|
|[8lKDQyCeoSuhotl9.htm](pathfinder-bestiary-3-items/8lKDQyCeoSuhotl9.htm)|Regeneration 50 (Deactivated by Evil, Mental, or Orichalcum)|Regeneración 50 (Desactivado por Maligno, Mental u Orichalcum)|modificada|
|[8N2A9BvddYZu0qKO.htm](pathfinder-bestiary-3-items/8N2A9BvddYZu0qKO.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[8n2FSC3RM16gF3r7.htm](pathfinder-bestiary-3-items/8n2FSC3RM16gF3r7.htm)|Falchion|Falchion|modificada|
|[8N8GpSFs6h26Jfs5.htm](pathfinder-bestiary-3-items/8N8GpSFs6h26Jfs5.htm)|Prehensile Tail|Cola prensil|modificada|
|[8NQPS5MLiZHxcRCO.htm](pathfinder-bestiary-3-items/8NQPS5MLiZHxcRCO.htm)|Ward|Ward|modificada|
|[8NXS92A64sYRn2Rv.htm](pathfinder-bestiary-3-items/8NXS92A64sYRn2Rv.htm)|Bully the Departed|Bully the Departed|modificada|
|[8oZMFFQs6aQ2T29D.htm](pathfinder-bestiary-3-items/8oZMFFQs6aQ2T29D.htm)|Knockdown|Derribo|modificada|
|[8rkyOZr5JbW7dVMr.htm](pathfinder-bestiary-3-items/8rkyOZr5JbW7dVMr.htm)|Nanite Surge|Oleaje de Nanite|modificada|
|[8So4CPwp508RfAuQ.htm](pathfinder-bestiary-3-items/8So4CPwp508RfAuQ.htm)|Hoof|Hoof|modificada|
|[8SPxajKwnuThr56n.htm](pathfinder-bestiary-3-items/8SPxajKwnuThr56n.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8tOUu5ISAj7PMRRU.htm](pathfinder-bestiary-3-items/8tOUu5ISAj7PMRRU.htm)|Absorb Blood|Absorber Sangre|modificada|
|[8Ukd831yvoAA6YEz.htm](pathfinder-bestiary-3-items/8Ukd831yvoAA6YEz.htm)|Hull|Casco|modificada|
|[8UV5F66KioAedVVP.htm](pathfinder-bestiary-3-items/8UV5F66KioAedVVP.htm)|Challenge Foe|Challenge Foe|modificada|
|[8VqNl9kqJUEBBRW1.htm](pathfinder-bestiary-3-items/8VqNl9kqJUEBBRW1.htm)|Ferocious Will|Voluntad feroz|modificada|
|[8wjQ8NlAQiJtzns1.htm](pathfinder-bestiary-3-items/8wjQ8NlAQiJtzns1.htm)|Mindbound|Mindbound|modificada|
|[8XnKrSfP40tWixA8.htm](pathfinder-bestiary-3-items/8XnKrSfP40tWixA8.htm)|Knockdown|Derribo|modificada|
|[8YPBBSxz9NwSDdqE.htm](pathfinder-bestiary-3-items/8YPBBSxz9NwSDdqE.htm)|Expel Wave|Expel Wave|modificada|
|[8znZn6HZQJY5Ryah.htm](pathfinder-bestiary-3-items/8znZn6HZQJY5Ryah.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[8zVt6SNpE1sJ6ntx.htm](pathfinder-bestiary-3-items/8zVt6SNpE1sJ6ntx.htm)|Hydraulic Deflection|Desviación Hidráulica|modificada|
|[90h9CZlrzmq29yPp.htm](pathfinder-bestiary-3-items/90h9CZlrzmq29yPp.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[926QjL85SoOXZHaD.htm](pathfinder-bestiary-3-items/926QjL85SoOXZHaD.htm)|Clever Disguises|Clever Disguises|modificada|
|[92aGlrWvxAQQzu5H.htm](pathfinder-bestiary-3-items/92aGlrWvxAQQzu5H.htm)|Cheaters Never Prosper|Los tramposos nunca prosperan|modificada|
|[983mBG3qwmzQKGS2.htm](pathfinder-bestiary-3-items/983mBG3qwmzQKGS2.htm)|Hatred of Mirrors|Odio a los espejos|modificada|
|[98oLnVVl0tvsGdHl.htm](pathfinder-bestiary-3-items/98oLnVVl0tvsGdHl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9aVqnZyByrnNoi0O.htm](pathfinder-bestiary-3-items/9aVqnZyByrnNoi0O.htm)|Plagued Coffin Restoration|Restablecimiento de ataúd plagado|modificada|
|[9bnT3kvF7tmt4OdX.htm](pathfinder-bestiary-3-items/9bnT3kvF7tmt4OdX.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[9Cf5HlYqqu2sA7jZ.htm](pathfinder-bestiary-3-items/9Cf5HlYqqu2sA7jZ.htm)|Vortex|Vortex|modificada|
|[9DknzWoTx4dGllLO.htm](pathfinder-bestiary-3-items/9DknzWoTx4dGllLO.htm)|Pressgang Soul|Pressgang Soul|modificada|
|[9eI0RiQpFdTMAoty.htm](pathfinder-bestiary-3-items/9eI0RiQpFdTMAoty.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9FHqdE5S2AJGokrP.htm](pathfinder-bestiary-3-items/9FHqdE5S2AJGokrP.htm)|Am I Pretty?|¿Soy Guapa?|modificada|
|[9FrKdx9Hv7Nqbxqh.htm](pathfinder-bestiary-3-items/9FrKdx9Hv7Nqbxqh.htm)|Snow Vision|Snow Vision|modificada|
|[9FS7AxQLqZNLvTdp.htm](pathfinder-bestiary-3-items/9FS7AxQLqZNLvTdp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9h6KJeGxzm8rEPaD.htm](pathfinder-bestiary-3-items/9h6KJeGxzm8rEPaD.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[9hTzL7fC49Gu7r0d.htm](pathfinder-bestiary-3-items/9hTzL7fC49Gu7r0d.htm)|Jaws|Fauces|modificada|
|[9Io2XFFvCWC7aHPp.htm](pathfinder-bestiary-3-items/9Io2XFFvCWC7aHPp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9iPQQuORbP2KlVOO.htm](pathfinder-bestiary-3-items/9iPQQuORbP2KlVOO.htm)|Ultrasonic Thrust|Empuje ultrasónico|modificada|
|[9jBzC5INoea9yhwL.htm](pathfinder-bestiary-3-items/9jBzC5INoea9yhwL.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[9Jcz4Yr2wdXvPymK.htm](pathfinder-bestiary-3-items/9Jcz4Yr2wdXvPymK.htm)|Believe the Lie|Creer la Mentira|modificada|
|[9kiCgRcrTLqrvzAj.htm](pathfinder-bestiary-3-items/9kiCgRcrTLqrvzAj.htm)|Negative Healing|Curación negativa|modificada|
|[9ksWonUvUDnlpZJ9.htm](pathfinder-bestiary-3-items/9ksWonUvUDnlpZJ9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9LhBHOk8wBt80kaJ.htm](pathfinder-bestiary-3-items/9LhBHOk8wBt80kaJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9MHMx6sPv3PI8rHT.htm](pathfinder-bestiary-3-items/9MHMx6sPv3PI8rHT.htm)|Rock|Roca|modificada|
|[9nNxDGgy23r1ETVB.htm](pathfinder-bestiary-3-items/9nNxDGgy23r1ETVB.htm)|Fed by Water|Alimentado por el agua|modificada|
|[9NuzaAkDhBBdhrxI.htm](pathfinder-bestiary-3-items/9NuzaAkDhBBdhrxI.htm)|Constrict|Restringir|modificada|
|[9om3JGlrd1NB7cuF.htm](pathfinder-bestiary-3-items/9om3JGlrd1NB7cuF.htm)|Burning Cold Fusillade|Burning Cold Fusillade|modificada|
|[9Pg0o9SqvEh925xr.htm](pathfinder-bestiary-3-items/9Pg0o9SqvEh925xr.htm)|In Concert|En Concierto|modificada|
|[9RNFc8HCHuW9XJgm.htm](pathfinder-bestiary-3-items/9RNFc8HCHuW9XJgm.htm)|Lithe|Lithe|modificada|
|[9RSHzC11L764mlf4.htm](pathfinder-bestiary-3-items/9RSHzC11L764mlf4.htm)|Liquefy|Licuar|modificada|
|[9RW0VcibxF9cA8uA.htm](pathfinder-bestiary-3-items/9RW0VcibxF9cA8uA.htm)|All This Will Happen Again|Todo Esto Volverá a Pasar|modificada|
|[9rWukglRKNo6c7Jk.htm](pathfinder-bestiary-3-items/9rWukglRKNo6c7Jk.htm)|Claw|Garra|modificada|
|[9sOhZcHSPREufoxA.htm](pathfinder-bestiary-3-items/9sOhZcHSPREufoxA.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[9sVRsQeqp2NqPZKy.htm](pathfinder-bestiary-3-items/9sVRsQeqp2NqPZKy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9uc04LSVh0aJFRbE.htm](pathfinder-bestiary-3-items/9uc04LSVh0aJFRbE.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9uczAsAAuGtsnZP2.htm](pathfinder-bestiary-3-items/9uczAsAAuGtsnZP2.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[9uDPkhvDHQvs18xG.htm](pathfinder-bestiary-3-items/9uDPkhvDHQvs18xG.htm)|Viviparous Birth|Parto Vivíparo|modificada|
|[9uhb8pW1o8nRmwPD.htm](pathfinder-bestiary-3-items/9uhb8pW1o8nRmwPD.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[9uoAAHOpjHhrvzWl.htm](pathfinder-bestiary-3-items/9uoAAHOpjHhrvzWl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9UrYZXifKee6nnBC.htm](pathfinder-bestiary-3-items/9UrYZXifKee6nnBC.htm)|Retributive Suplex|Suplex Retributivo|modificada|
|[9wegz19U5LREiL9x.htm](pathfinder-bestiary-3-items/9wegz19U5LREiL9x.htm)|Strix Vengeance|Strix Vengeance|modificada|
|[9Wm3gZTBJ0G9sk1w.htm](pathfinder-bestiary-3-items/9Wm3gZTBJ0G9sk1w.htm)|Glimpse of Redemption|Atisbo de redención|modificada|
|[9WqbsuRExPiFqTnh.htm](pathfinder-bestiary-3-items/9WqbsuRExPiFqTnh.htm)|Flameheart Weapon|Arma Corazón de Llama|modificada|
|[9WYmk5gm1kpKey4j.htm](pathfinder-bestiary-3-items/9WYmk5gm1kpKey4j.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[9x6UoyWK1LPpP2M1.htm](pathfinder-bestiary-3-items/9x6UoyWK1LPpP2M1.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[9X9mEo92lODvZP6d.htm](pathfinder-bestiary-3-items/9X9mEo92lODvZP6d.htm)|Troop Defenses|Troop Defenses|modificada|
|[9xVKiyYkmLvWLsPN.htm](pathfinder-bestiary-3-items/9xVKiyYkmLvWLsPN.htm)|Countered by Earth|Contrarrestado por la Tierra|modificada|
|[a0XndmlcRchhe4Sl.htm](pathfinder-bestiary-3-items/a0XndmlcRchhe4Sl.htm)|Horn|Cuerno|modificada|
|[a2FJlFGOGuTMTLOI.htm](pathfinder-bestiary-3-items/a2FJlFGOGuTMTLOI.htm)|Fist|Puño|modificada|
|[A2z7JUAPZ4320zP5.htm](pathfinder-bestiary-3-items/A2z7JUAPZ4320zP5.htm)|Negative Healing|Curación negativa|modificada|
|[A3POhpGYovQJDozd.htm](pathfinder-bestiary-3-items/A3POhpGYovQJDozd.htm)|Bonded Vessel|Bonded Vessel|modificada|
|[a4OTeLXz850yN3NZ.htm](pathfinder-bestiary-3-items/a4OTeLXz850yN3NZ.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[a6e5k1in4L7W0ieF.htm](pathfinder-bestiary-3-items/a6e5k1in4L7W0ieF.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[a77can8I0pBhmKe0.htm](pathfinder-bestiary-3-items/a77can8I0pBhmKe0.htm)|+2 Circumstance to All Saves vs. Shove and Trip|+2 Circunstancia a todas las salvaciones contra Empujar y Derribar.|modificada|
|[A7emqw5XPEaVatWu.htm](pathfinder-bestiary-3-items/A7emqw5XPEaVatWu.htm)|Constant Spells|Constant Spells|modificada|
|[a7ntWl311o1OfxCR.htm](pathfinder-bestiary-3-items/a7ntWl311o1OfxCR.htm)|Vulnerable to Sunlight|Vulnerabilidad a la luz del sol|modificada|
|[a83gShMg9yb9LGhv.htm](pathfinder-bestiary-3-items/a83gShMg9yb9LGhv.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[a8Oke93EjcLb3CyD.htm](pathfinder-bestiary-3-items/a8Oke93EjcLb3CyD.htm)|Trident|Trident|modificada|
|[a9aLzghe9uCANIDN.htm](pathfinder-bestiary-3-items/a9aLzghe9uCANIDN.htm)|Jaws|Fauces|modificada|
|[aA0Li6f8IO68qqXw.htm](pathfinder-bestiary-3-items/aA0Li6f8IO68qqXw.htm)|Draining Blight|Tizón Drenante|modificada|
|[ab11N1FmMkMVAP9Q.htm](pathfinder-bestiary-3-items/ab11N1FmMkMVAP9Q.htm)|Greater Constrict|Mayor Restricción|modificada|
|[aBGHzzN9TzDAz9kf.htm](pathfinder-bestiary-3-items/aBGHzzN9TzDAz9kf.htm)|Divine Lightning|Relámpago Divino|modificada|
|[AbzlxSNMB7g4nTJc.htm](pathfinder-bestiary-3-items/AbzlxSNMB7g4nTJc.htm)|Passive Points|Puntos pasivos|modificada|
|[aCOs4ab0FqX7Nup4.htm](pathfinder-bestiary-3-items/aCOs4ab0FqX7Nup4.htm)|Champion Focus Spells|Conjuros de foco de campeón|modificada|
|[acq6pZafGJRNM5Um.htm](pathfinder-bestiary-3-items/acq6pZafGJRNM5Um.htm)|+2 Status Bonus on Saves vs. Linguistic Effects|Bonificador +2 a la situación en las salvaciones contra efectos lingüísticos.|modificada|
|[Ad7v2ldIjRYpmP6V.htm](pathfinder-bestiary-3-items/Ad7v2ldIjRYpmP6V.htm)|Swallow Whole|Engullir Todo|modificada|
|[ADj1D97ZuGgEgAMr.htm](pathfinder-bestiary-3-items/ADj1D97ZuGgEgAMr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[adMwYHLlDzh2bKaz.htm](pathfinder-bestiary-3-items/adMwYHLlDzh2bKaz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[adUdgeL10QH11zJu.htm](pathfinder-bestiary-3-items/adUdgeL10QH11zJu.htm)|Mortal Shield|Escudo Mortal|modificada|
|[Ae1LTM5lLFv32i3l.htm](pathfinder-bestiary-3-items/Ae1LTM5lLFv32i3l.htm)|Change Shape|Change Shape|modificada|
|[AELQ9eZZYSwimA7P.htm](pathfinder-bestiary-3-items/AELQ9eZZYSwimA7P.htm)|Bide|Bide|modificada|
|[aEnTHY8PwlCJWqzG.htm](pathfinder-bestiary-3-items/aEnTHY8PwlCJWqzG.htm)|Foot|Pie|modificada|
|[AErcLCe3qBLb8HU0.htm](pathfinder-bestiary-3-items/AErcLCe3qBLb8HU0.htm)|No Breath|No Breath|modificada|
|[AEz7KLpjP8KnKGjx.htm](pathfinder-bestiary-3-items/AEz7KLpjP8KnKGjx.htm)|Liquid Leap|Salto sin carrerilla líquido|modificada|
|[AF48h2X3hAjthWZX.htm](pathfinder-bestiary-3-items/AF48h2X3hAjthWZX.htm)|Jaws|Fauces|modificada|
|[af7dgcRuc5gWm1ej.htm](pathfinder-bestiary-3-items/af7dgcRuc5gWm1ej.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[AG5uVa2pIKuc9bfQ.htm](pathfinder-bestiary-3-items/AG5uVa2pIKuc9bfQ.htm)|Flailing Thrash|Sacudirse|modificada|
|[AGsJjbwv43AZYfPx.htm](pathfinder-bestiary-3-items/AGsJjbwv43AZYfPx.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[AHL4n7RnueMnqqXS.htm](pathfinder-bestiary-3-items/AHL4n7RnueMnqqXS.htm)|Form a Phalanx|Formar una Falange|modificada|
|[aHOLCHiRhu5ipc7L.htm](pathfinder-bestiary-3-items/aHOLCHiRhu5ipc7L.htm)|Ectoplasmic Shield|Escudo ectoplasmático|modificada|
|[Ai9DSqNliIuPOhxP.htm](pathfinder-bestiary-3-items/Ai9DSqNliIuPOhxP.htm)|Interplanar Lifesense|Interplanar Lifesense|modificada|
|[AijMBZl9gTqlCcb0.htm](pathfinder-bestiary-3-items/AijMBZl9gTqlCcb0.htm)|Tormented Snarl|Gruñido Atormentado|modificada|
|[AirzEgJQdbzrmlyJ.htm](pathfinder-bestiary-3-items/AirzEgJQdbzrmlyJ.htm)|Light Wisp|Luz Wisp|modificada|
|[aJ9nob7nAtJIDXwG.htm](pathfinder-bestiary-3-items/aJ9nob7nAtJIDXwG.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[ajC29c9gmHsNUgs1.htm](pathfinder-bestiary-3-items/ajC29c9gmHsNUgs1.htm)|Nymph's Beauty|Nymph's Beauty|modificada|
|[AJK2FzoU6wIUxE43.htm](pathfinder-bestiary-3-items/AJK2FzoU6wIUxE43.htm)|Flaming Shroud|Mortaja Flamígera|modificada|
|[AJLCWM50AXEWFDYT.htm](pathfinder-bestiary-3-items/AJLCWM50AXEWFDYT.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[aK7xyvWTJe4GyQ42.htm](pathfinder-bestiary-3-items/aK7xyvWTJe4GyQ42.htm)|Darkvision|Visión en la oscuridad|modificada|
|[aK9UmOvlCoa0e4uF.htm](pathfinder-bestiary-3-items/aK9UmOvlCoa0e4uF.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[AKn4Oke8VMoU85yk.htm](pathfinder-bestiary-3-items/AKn4Oke8VMoU85yk.htm)|Indomitable Oration|Oración Indomable|modificada|
|[AKUv4ErgbncGAS63.htm](pathfinder-bestiary-3-items/AKUv4ErgbncGAS63.htm)|On All Fours|On All Fours|modificada|
|[AkvUF9WUqlp4ytHG.htm](pathfinder-bestiary-3-items/AkvUF9WUqlp4ytHG.htm)|Toxic Body|Cuerpo Tóxico|modificada|
|[aKXQLekTbiAqj1Id.htm](pathfinder-bestiary-3-items/aKXQLekTbiAqj1Id.htm)|Troop Movement|Movimiento de tropas|modificada|
|[alAwSazDkJZWiv6r.htm](pathfinder-bestiary-3-items/alAwSazDkJZWiv6r.htm)|Cooperative Scrying|Escudrimiento Cooperativo|modificada|
|[AlDSrVGyweTmAY0N.htm](pathfinder-bestiary-3-items/AlDSrVGyweTmAY0N.htm)|Hoof|Hoof|modificada|
|[alp4ZDrGM2zotNDt.htm](pathfinder-bestiary-3-items/alp4ZDrGM2zotNDt.htm)|Slash the Suffering|Rajar el sufrimiento|modificada|
|[aLsS2G1pU06ABZHI.htm](pathfinder-bestiary-3-items/aLsS2G1pU06ABZHI.htm)|Powerful Scimitars|Cimitarras Poderosas|modificada|
|[aMgKVhdJKyEfpoK9.htm](pathfinder-bestiary-3-items/aMgKVhdJKyEfpoK9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[AmUgwUNSujKkZSco.htm](pathfinder-bestiary-3-items/AmUgwUNSujKkZSco.htm)|Forehead|Frente|modificada|
|[aMxPxph18d6Elee9.htm](pathfinder-bestiary-3-items/aMxPxph18d6Elee9.htm)|Echoblade Flurry|Echoblade Flurry|modificada|
|[An7ZmOxkCPkGUuBN.htm](pathfinder-bestiary-3-items/An7ZmOxkCPkGUuBN.htm)|Claw|Garra|modificada|
|[aNoJsFxQdiX8ZDqx.htm](pathfinder-bestiary-3-items/aNoJsFxQdiX8ZDqx.htm)|Halberd|Alabarda|modificada|
|[anyFEiqXXoGS63Gs.htm](pathfinder-bestiary-3-items/anyFEiqXXoGS63Gs.htm)|Grasping Tail|Cola de Agarre|modificada|
|[aO0RDgMo7pF6pa0h.htm](pathfinder-bestiary-3-items/aO0RDgMo7pF6pa0h.htm)|Snake Fangs|Colmillos de Serpiente|modificada|
|[aOaPtLmoTvz46pJw.htm](pathfinder-bestiary-3-items/aOaPtLmoTvz46pJw.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[aOeCz4c09QMbgoTG.htm](pathfinder-bestiary-3-items/aOeCz4c09QMbgoTG.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[apuiQK82CCi4s2wU.htm](pathfinder-bestiary-3-items/apuiQK82CCi4s2wU.htm)|Echoblade|Echoblade|modificada|
|[AQ7Uf21qvFcnJL1X.htm](pathfinder-bestiary-3-items/AQ7Uf21qvFcnJL1X.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[aqpkkRpk5KmwHfhG.htm](pathfinder-bestiary-3-items/aqpkkRpk5KmwHfhG.htm)|Knockdown|Derribo|modificada|
|[AQxSKJnCPtcDDqGB.htm](pathfinder-bestiary-3-items/AQxSKJnCPtcDDqGB.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[arx5pjwY2S5WLBAA.htm](pathfinder-bestiary-3-items/arx5pjwY2S5WLBAA.htm)|Mounted Troop|Tropa Montada|modificada|
|[AsbIVQDcJZfJhU4N.htm](pathfinder-bestiary-3-items/AsbIVQDcJZfJhU4N.htm)|Troop Defenses|Troop Defenses|modificada|
|[ASdIdSgTDc6bN48E.htm](pathfinder-bestiary-3-items/ASdIdSgTDc6bN48E.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[AsRS8u2z4hfaeLCO.htm](pathfinder-bestiary-3-items/AsRS8u2z4hfaeLCO.htm)|Telepathy 50 feet|Telepatía de 15 metros.|modificada|
|[At4UFNUZDqPnIhl7.htm](pathfinder-bestiary-3-items/At4UFNUZDqPnIhl7.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[aT8anQIUKNtDMO8Y.htm](pathfinder-bestiary-3-items/aT8anQIUKNtDMO8Y.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[aT8INKRo18qw8cTl.htm](pathfinder-bestiary-3-items/aT8INKRo18qw8cTl.htm)|Breath Weapon|Breath Weapon|modificada|
|[atf7XFGfAWXeI1TN.htm](pathfinder-bestiary-3-items/atf7XFGfAWXeI1TN.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[atgtDLkpO7KEpGOE.htm](pathfinder-bestiary-3-items/atgtDLkpO7KEpGOE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[atierys9eTCKzPyz.htm](pathfinder-bestiary-3-items/atierys9eTCKzPyz.htm)|Greater Constrict|Mayor Restricción|modificada|
|[ATj9bVkYk7iDTGyM.htm](pathfinder-bestiary-3-items/ATj9bVkYk7iDTGyM.htm)|Dagger|Daga|modificada|
|[AtSxBEaHS4ljnAyv.htm](pathfinder-bestiary-3-items/AtSxBEaHS4ljnAyv.htm)|Coven|Coven|modificada|
|[AuA52N8pGsULYcjw.htm](pathfinder-bestiary-3-items/AuA52N8pGsULYcjw.htm)|Tendril|Tendril|modificada|
|[AuPR3E2MyY05z9N2.htm](pathfinder-bestiary-3-items/AuPR3E2MyY05z9N2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[AUSkGV3ZjHHnyIrW.htm](pathfinder-bestiary-3-items/AUSkGV3ZjHHnyIrW.htm)|Skitter Away|Zafarse lejos|modificada|
|[aVCE3zDK0cQohDQM.htm](pathfinder-bestiary-3-items/aVCE3zDK0cQohDQM.htm)|Monk Ki Spells|Conjuros de ki del monje|modificada|
|[AvNNZdhRs6SKQiy3.htm](pathfinder-bestiary-3-items/AvNNZdhRs6SKQiy3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[aw8UyWcvmu5VncsT.htm](pathfinder-bestiary-3-items/aw8UyWcvmu5VncsT.htm)|Mist Vision|Mist Vision|modificada|
|[AWOczN1NAKx6tjD8.htm](pathfinder-bestiary-3-items/AWOczN1NAKx6tjD8.htm)|Kukri|Kukri|modificada|
|[AwRCPMRzo9dIymmM.htm](pathfinder-bestiary-3-items/AwRCPMRzo9dIymmM.htm)|Susceptible to Death|Susceptible a la Muerte|modificada|
|[AXuWafDpkZy5SX8V.htm](pathfinder-bestiary-3-items/AXuWafDpkZy5SX8V.htm)|Incite Violence|Incitar a la violencia|modificada|
|[aYd4v0mDwmgHckIH.htm](pathfinder-bestiary-3-items/aYd4v0mDwmgHckIH.htm)|Bonded Strike|Golpe Bonded|modificada|
|[AyDallashGfDWvMX.htm](pathfinder-bestiary-3-items/AyDallashGfDWvMX.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ayEWmTQnDQXk54zc.htm](pathfinder-bestiary-3-items/ayEWmTQnDQXk54zc.htm)|Immortal Flesh|Carne Inmortal|modificada|
|[aYv6mdSXvzzYWgRA.htm](pathfinder-bestiary-3-items/aYv6mdSXvzzYWgRA.htm)|Capuchin's Curse|Maldición del Capuchino|modificada|
|[aZTKSdmtywAvr7TJ.htm](pathfinder-bestiary-3-items/aZTKSdmtywAvr7TJ.htm)|Claw|Garra|modificada|
|[AzWuaOagwffOzhLP.htm](pathfinder-bestiary-3-items/AzWuaOagwffOzhLP.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[aZYbkerDQjojnR8G.htm](pathfinder-bestiary-3-items/aZYbkerDQjojnR8G.htm)|Shield Block|Bloquear con escudo|modificada|
|[B0CIGI7Er3AYCaem.htm](pathfinder-bestiary-3-items/B0CIGI7Er3AYCaem.htm)|Darkvision|Visión en la oscuridad|modificada|
|[B0U6YmeB8UGglyGa.htm](pathfinder-bestiary-3-items/B0U6YmeB8UGglyGa.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[B2rF45WAMrowNHio.htm](pathfinder-bestiary-3-items/B2rF45WAMrowNHio.htm)|Darkvision|Visión en la oscuridad|modificada|
|[b2yf3FTVhb4cL3mI.htm](pathfinder-bestiary-3-items/b2yf3FTVhb4cL3mI.htm)|Pose a Riddle|Pose a Riddle|modificada|
|[b3L5EEZVfcjwleqc.htm](pathfinder-bestiary-3-items/b3L5EEZVfcjwleqc.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[B3NBi21pYXb87QM2.htm](pathfinder-bestiary-3-items/B3NBi21pYXb87QM2.htm)|Nosferatu Vulnerabilities|Vulnerabilidades Nosferatu|modificada|
|[b4LBZAjcRm8KzaWR.htm](pathfinder-bestiary-3-items/b4LBZAjcRm8KzaWR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[B5cFXl5CIucCzoy0.htm](pathfinder-bestiary-3-items/B5cFXl5CIucCzoy0.htm)|Launched Blade|Cuchilla Lanzada|modificada|
|[b5tNOyXUMi9maaal.htm](pathfinder-bestiary-3-items/b5tNOyXUMi9maaal.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[B6M6fTBtJwAQDYCs.htm](pathfinder-bestiary-3-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|Swarm Mind|modificada|
|[b70iU5hV08AtqcDw.htm](pathfinder-bestiary-3-items/b70iU5hV08AtqcDw.htm)|Coven Spells|Coven Spells|modificada|
|[b7j8M7xzLStzldNC.htm](pathfinder-bestiary-3-items/b7j8M7xzLStzldNC.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[b8889HDEEpdiqYFf.htm](pathfinder-bestiary-3-items/b8889HDEEpdiqYFf.htm)|Constant Spells|Constant Spells|modificada|
|[b8MhSEnrYG0hWLqw.htm](pathfinder-bestiary-3-items/b8MhSEnrYG0hWLqw.htm)|+2 Status to All Saves vs. Divine Magic|+2 situación a todas las salvaciones contra magia divina.|modificada|
|[B8Uf7QHdJ2D0Fs74.htm](pathfinder-bestiary-3-items/B8Uf7QHdJ2D0Fs74.htm)|Capture|Captura|modificada|
|[B9EouOshvYsQf0Td.htm](pathfinder-bestiary-3-items/B9EouOshvYsQf0Td.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[baOID7dh8Ikettqn.htm](pathfinder-bestiary-3-items/baOID7dh8Ikettqn.htm)|Magical Broth|Magical Broth|modificada|
|[BaTacjtQvnaLoVc5.htm](pathfinder-bestiary-3-items/BaTacjtQvnaLoVc5.htm)|Wind-Up|Wind-Up|modificada|
|[bAYPijLnxouZs1Ea.htm](pathfinder-bestiary-3-items/bAYPijLnxouZs1Ea.htm)|Grab|Agarrado|modificada|
|[bcflm5XlNEDnnGZ3.htm](pathfinder-bestiary-3-items/bcflm5XlNEDnnGZ3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[BCIdFtWYKRXP0vLE.htm](pathfinder-bestiary-3-items/BCIdFtWYKRXP0vLE.htm)|Typhoon Dive|Typhoon Dive|modificada|
|[BCsGqHil1Dziu3FX.htm](pathfinder-bestiary-3-items/BCsGqHil1Dziu3FX.htm)|Lower Halberds!|¡Alabarderos inferiores!|modificada|
|[BcWStlMljvf4ap7v.htm](pathfinder-bestiary-3-items/BcWStlMljvf4ap7v.htm)|Wide Cleave|Hendedura Amplia|modificada|
|[BD35N4Wa7tdNg5y4.htm](pathfinder-bestiary-3-items/BD35N4Wa7tdNg5y4.htm)|Jaws|Fauces|modificada|
|[BD81Ab7NPFAacuhc.htm](pathfinder-bestiary-3-items/BD81Ab7NPFAacuhc.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[BDbSH109hF8RleEa.htm](pathfinder-bestiary-3-items/BDbSH109hF8RleEa.htm)|Hatred of Red|Odio al rojo|modificada|
|[bDZ6kq6HqfFLK2PU.htm](pathfinder-bestiary-3-items/bDZ6kq6HqfFLK2PU.htm)|Embed|Embed|modificada|
|[BE8uoWMEnODt0Xw7.htm](pathfinder-bestiary-3-items/BE8uoWMEnODt0Xw7.htm)|Swallow Whole|Engullir Todo|modificada|
|[BEcAt0SdsvIa6qp9.htm](pathfinder-bestiary-3-items/BEcAt0SdsvIa6qp9.htm)|Befuddling Lash|Befuddling Lash|modificada|
|[befWMYmJQKeMMpzD.htm](pathfinder-bestiary-3-items/befWMYmJQKeMMpzD.htm)|Club|Club|modificada|
|[BFhitQjxtXx7cQKS.htm](pathfinder-bestiary-3-items/BFhitQjxtXx7cQKS.htm)|Sting Shot|Sting Shot|modificada|
|[BFS5mQ3ITub7ZdJL.htm](pathfinder-bestiary-3-items/BFS5mQ3ITub7ZdJL.htm)|Claw|Garra|modificada|
|[BG9o62cI16MBV3Qy.htm](pathfinder-bestiary-3-items/BG9o62cI16MBV3Qy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BgiZcqILhEbxEVG7.htm](pathfinder-bestiary-3-items/BgiZcqILhEbxEVG7.htm)|In Concert|En Concierto|modificada|
|[BgqCS9ExYivBMHU2.htm](pathfinder-bestiary-3-items/BgqCS9ExYivBMHU2.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[bgr9Qyvp6pX1W3KQ.htm](pathfinder-bestiary-3-items/bgr9Qyvp6pX1W3KQ.htm)|Raktavarna Venom|Raktavarna Venom|modificada|
|[bgwyucC0EpyEmxl7.htm](pathfinder-bestiary-3-items/bgwyucC0EpyEmxl7.htm)|Change Shape|Change Shape|modificada|
|[Bh4tXIFDTCdh3Vzs.htm](pathfinder-bestiary-3-items/Bh4tXIFDTCdh3Vzs.htm)|Breath Weapon|Breath Weapon|modificada|
|[BHN2n3G77QRNLHhM.htm](pathfinder-bestiary-3-items/BHN2n3G77QRNLHhM.htm)|Stinger|Aguijón|modificada|
|[BHNm9PtnZ9Hrj6fQ.htm](pathfinder-bestiary-3-items/BHNm9PtnZ9Hrj6fQ.htm)|Unsettled Mind|Unsettled Mind|modificada|
|[bhqneDVB7PGBIMMh.htm](pathfinder-bestiary-3-items/bhqneDVB7PGBIMMh.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[BI9PPwdfEfP8sJv5.htm](pathfinder-bestiary-3-items/BI9PPwdfEfP8sJv5.htm)|Constant Spells|Constant Spells|modificada|
|[BIKoqqYwjx9IZ1th.htm](pathfinder-bestiary-3-items/BIKoqqYwjx9IZ1th.htm)|Fox's Cunning|Astucia del Zorro|modificada|
|[bIOojNVEElRivlmd.htm](pathfinder-bestiary-3-items/bIOojNVEElRivlmd.htm)|Claw|Garra|modificada|
|[BISNEn6kmVke1CIR.htm](pathfinder-bestiary-3-items/BISNEn6kmVke1CIR.htm)|Forest Shape|Forest Shape|modificada|
|[bjqB60qqpfDFqKNU.htm](pathfinder-bestiary-3-items/bjqB60qqpfDFqKNU.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[bJUdfd279JTmWONI.htm](pathfinder-bestiary-3-items/bJUdfd279JTmWONI.htm)|Door|Puerta|modificada|
|[bkajUVm6XDKbIO89.htm](pathfinder-bestiary-3-items/bkajUVm6XDKbIO89.htm)|Spike|Spike|modificada|
|[BKsoNNVMI5d9mEhL.htm](pathfinder-bestiary-3-items/BKsoNNVMI5d9mEhL.htm)|All-Around Vision|All-Around Vision|modificada|
|[BlH8W4GE6xpZoZM2.htm](pathfinder-bestiary-3-items/BlH8W4GE6xpZoZM2.htm)|Troop Defenses|Troop Defenses|modificada|
|[BLkAKMlADR3rQxEw.htm](pathfinder-bestiary-3-items/BLkAKMlADR3rQxEw.htm)|Troop Defenses|Troop Defenses|modificada|
|[bLo1G95EZjpM4Zf6.htm](pathfinder-bestiary-3-items/bLo1G95EZjpM4Zf6.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[blSV9HiaoSCWQT9w.htm](pathfinder-bestiary-3-items/blSV9HiaoSCWQT9w.htm)|Grab|Agarrado|modificada|
|[bLxMh4Pt4sU0x8Pp.htm](pathfinder-bestiary-3-items/bLxMh4Pt4sU0x8Pp.htm)|Telepathic Singer|Telepathic Singer|modificada|
|[bMrJDUmLpocEes2w.htm](pathfinder-bestiary-3-items/bMrJDUmLpocEes2w.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[bMrmRT314fkcPeuk.htm](pathfinder-bestiary-3-items/bMrmRT314fkcPeuk.htm)|Jaws|Fauces|modificada|
|[bnBa5XcA2wHzP5KM.htm](pathfinder-bestiary-3-items/bnBa5XcA2wHzP5KM.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[bnU4poD2v5S9mLDT.htm](pathfinder-bestiary-3-items/bnU4poD2v5S9mLDT.htm)|Eternal Fear|Miedo Eterno|modificada|
|[Bo9liMM0Qn69gKdi.htm](pathfinder-bestiary-3-items/Bo9liMM0Qn69gKdi.htm)|Grab|Agarrado|modificada|
|[Bpok96twh3ZtuzEH.htm](pathfinder-bestiary-3-items/Bpok96twh3ZtuzEH.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[bpvnWZi8sukcfy9H.htm](pathfinder-bestiary-3-items/bpvnWZi8sukcfy9H.htm)|Displace|Desplazamiento|modificada|
|[bqDyC6IKtXktariv.htm](pathfinder-bestiary-3-items/bqDyC6IKtXktariv.htm)|Needle|Aguja|modificada|
|[BqST74R7dRaivdDQ.htm](pathfinder-bestiary-3-items/BqST74R7dRaivdDQ.htm)|Beak|Beak|modificada|
|[bQxZThiBIxPwNuDH.htm](pathfinder-bestiary-3-items/bQxZThiBIxPwNuDH.htm)|Sea Spray|Sea Spray|modificada|
|[bQZIEB7cmxSGgulV.htm](pathfinder-bestiary-3-items/bQZIEB7cmxSGgulV.htm)|Jaws|Fauces|modificada|
|[br8FCwfVCLAXisnU.htm](pathfinder-bestiary-3-items/br8FCwfVCLAXisnU.htm)|Dragon's Wisdom|Sabiduría del Dragón|modificada|
|[brAwbDIfmKeRHjjo.htm](pathfinder-bestiary-3-items/brAwbDIfmKeRHjjo.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[bRWkzuZ2ByRvUEXx.htm](pathfinder-bestiary-3-items/bRWkzuZ2ByRvUEXx.htm)|Negative Healing|Curación negativa|modificada|
|[BRY6lAS6ePaNJsIO.htm](pathfinder-bestiary-3-items/BRY6lAS6ePaNJsIO.htm)|Disk Rider|Disk Rider|modificada|
|[bScgZ7J4SQWXIPfp.htm](pathfinder-bestiary-3-items/bScgZ7J4SQWXIPfp.htm)|Void Transmission|Void Transmission|modificada|
|[bsKbGLCEij4kqAPY.htm](pathfinder-bestiary-3-items/bsKbGLCEij4kqAPY.htm)|Ice Staff|Ice Staff|modificada|
|[BskkanKxSINfDBr9.htm](pathfinder-bestiary-3-items/BskkanKxSINfDBr9.htm)|Bittersweet Dreams|Sueños agridulces|modificada|
|[BsVgKelZGZWfZaYs.htm](pathfinder-bestiary-3-items/BsVgKelZGZWfZaYs.htm)|Greatclub|Greatclub|modificada|
|[bTNygOkcj07sIda5.htm](pathfinder-bestiary-3-items/bTNygOkcj07sIda5.htm)|Improved Grab|Agarrado mejorado|modificada|
|[btVPiH1rJHK8iziH.htm](pathfinder-bestiary-3-items/btVPiH1rJHK8iziH.htm)|Elven Curve Blade|Hoja curva élfica|modificada|
|[BTxgwCFuROqASlPz.htm](pathfinder-bestiary-3-items/BTxgwCFuROqASlPz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bUFMLfwRNZ1zBmqa.htm](pathfinder-bestiary-3-items/bUFMLfwRNZ1zBmqa.htm)|Swarm Mind|Swarm Mind|modificada|
|[BurQSnXk9enXhVZH.htm](pathfinder-bestiary-3-items/BurQSnXk9enXhVZH.htm)|Steal Breath|Sustraer Aliento|modificada|
|[BVccjoWLx8QgvIDh.htm](pathfinder-bestiary-3-items/BVccjoWLx8QgvIDh.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[BVGtxqRvaiXmxNDG.htm](pathfinder-bestiary-3-items/BVGtxqRvaiXmxNDG.htm)|Rage of Spirits|Furia de los espíritus|modificada|
|[BVZS9T28bimG6NBU.htm](pathfinder-bestiary-3-items/BVZS9T28bimG6NBU.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[BW9Qv2EOWjmgeEgL.htm](pathfinder-bestiary-3-items/BW9Qv2EOWjmgeEgL.htm)|Inspiration|Inspiración|modificada|
|[BwimiLeXJKeEigfN.htm](pathfinder-bestiary-3-items/BwimiLeXJKeEigfN.htm)|Tail|Tail|modificada|
|[bwScFju6YTBM7feQ.htm](pathfinder-bestiary-3-items/bwScFju6YTBM7feQ.htm)|Horn|Cuerno|modificada|
|[BwU8clOdeUsnJX4w.htm](pathfinder-bestiary-3-items/BwU8clOdeUsnJX4w.htm)|Push|Push|modificada|
|[BX7sT7k5GNZuI9hc.htm](pathfinder-bestiary-3-items/BX7sT7k5GNZuI9hc.htm)|Silver Scissors|Tijeras de Plata|modificada|
|[bxnrpUx8pN5QerLc.htm](pathfinder-bestiary-3-items/bxnrpUx8pN5QerLc.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[bYiotnUBfPu81i3V.htm](pathfinder-bestiary-3-items/bYiotnUBfPu81i3V.htm)|Swarm Mind|Swarm Mind|modificada|
|[BySVNQG4P1lbNbyJ.htm](pathfinder-bestiary-3-items/BySVNQG4P1lbNbyJ.htm)|Mortic Ferocity|Ferocidad Mortal|modificada|
|[BZPtJSauua8fLt2G.htm](pathfinder-bestiary-3-items/BZPtJSauua8fLt2G.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[C0NEkV1zwoV5vogY.htm](pathfinder-bestiary-3-items/C0NEkV1zwoV5vogY.htm)|Reconstitution|Reconstitución|modificada|
|[C10wjHeJK4PRbQiQ.htm](pathfinder-bestiary-3-items/C10wjHeJK4PRbQiQ.htm)|Jaws|Fauces|modificada|
|[C22ZfqwOAI1G9r1O.htm](pathfinder-bestiary-3-items/C22ZfqwOAI1G9r1O.htm)|Maul|Zarpazo doble|modificada|
|[c2jgSAE0FH6AmDJ2.htm](pathfinder-bestiary-3-items/c2jgSAE0FH6AmDJ2.htm)|Jaws|Fauces|modificada|
|[c2zqPCnz9E8UlKRw.htm](pathfinder-bestiary-3-items/c2zqPCnz9E8UlKRw.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[c3sGdZtwuCDBGlig.htm](pathfinder-bestiary-3-items/c3sGdZtwuCDBGlig.htm)|Shortbow|Arco corto|modificada|
|[c85RY0nsV1xyDA3t.htm](pathfinder-bestiary-3-items/c85RY0nsV1xyDA3t.htm)|Improved Grab|Agarrado mejorado|modificada|
|[C8pZwyrtELK6lVYw.htm](pathfinder-bestiary-3-items/C8pZwyrtELK6lVYw.htm)|Arrow of Despair|Flecha de la Desesperación|modificada|
|[c95y554eFZ79003o.htm](pathfinder-bestiary-3-items/c95y554eFZ79003o.htm)|Grab|Agarrado|modificada|
|[caAfIPQy8oLw6CGa.htm](pathfinder-bestiary-3-items/caAfIPQy8oLw6CGa.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[CAgNpEik2Ky3XXu4.htm](pathfinder-bestiary-3-items/CAgNpEik2Ky3XXu4.htm)|Retract|Retract|modificada|
|[caybfvEpmrHbdtN6.htm](pathfinder-bestiary-3-items/caybfvEpmrHbdtN6.htm)|Return Arrow|Flecha Retornante|modificada|
|[cb5dMTXksovLZMVP.htm](pathfinder-bestiary-3-items/cb5dMTXksovLZMVP.htm)|Claw|Garra|modificada|
|[cBLNcI8XKhBuOFqE.htm](pathfinder-bestiary-3-items/cBLNcI8XKhBuOFqE.htm)|Claw|Garra|modificada|
|[CC7MztEx8rEoNgaL.htm](pathfinder-bestiary-3-items/CC7MztEx8rEoNgaL.htm)|Longsword|Longsword|modificada|
|[CChXCrOyQxz6h0X9.htm](pathfinder-bestiary-3-items/CChXCrOyQxz6h0X9.htm)|Abeyance Rift|Grieta de Abeyance|modificada|
|[CcOaWq1ZikuzfZ88.htm](pathfinder-bestiary-3-items/CcOaWq1ZikuzfZ88.htm)|Tenebral Form|Forma Tenebral|modificada|
|[CCUY44whvoSSEz3I.htm](pathfinder-bestiary-3-items/CCUY44whvoSSEz3I.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Cdh7brQkfnpWbZDJ.htm](pathfinder-bestiary-3-items/Cdh7brQkfnpWbZDJ.htm)|Mandibles|Mandíbulas|modificada|
|[cdmwwMzpk0Bsc4Jg.htm](pathfinder-bestiary-3-items/cdmwwMzpk0Bsc4Jg.htm)|Jaws|Fauces|modificada|
|[cdrifaen6W903klB.htm](pathfinder-bestiary-3-items/cdrifaen6W903klB.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ce63eYi0AotvHaz2.htm](pathfinder-bestiary-3-items/ce63eYi0AotvHaz2.htm)|Constant Spells|Constant Spells|modificada|
|[cE6juVvy0O4iJMgY.htm](pathfinder-bestiary-3-items/cE6juVvy0O4iJMgY.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CEHvST4zuzVjG0oO.htm](pathfinder-bestiary-3-items/CEHvST4zuzVjG0oO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[ceUAQwTriMohyVjk.htm](pathfinder-bestiary-3-items/ceUAQwTriMohyVjk.htm)|Swooping Dive|Swooping Dive|modificada|
|[CFh42XjjfBF8zZRE.htm](pathfinder-bestiary-3-items/CFh42XjjfBF8zZRE.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[CFiD5D2XvZ2N67x1.htm](pathfinder-bestiary-3-items/CFiD5D2XvZ2N67x1.htm)|Swarming Gnaw|Swarming Gnaw|modificada|
|[cFSlH4qPRNdzcxY6.htm](pathfinder-bestiary-3-items/cFSlH4qPRNdzcxY6.htm)|Tide of Creation|Marea de creación|modificada|
|[cg8fgSm6sGAJE6bt.htm](pathfinder-bestiary-3-items/cg8fgSm6sGAJE6bt.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[CGIXYmLYYlMz2iRx.htm](pathfinder-bestiary-3-items/CGIXYmLYYlMz2iRx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[CgQW6ZFOrGsrgkHZ.htm](pathfinder-bestiary-3-items/CgQW6ZFOrGsrgkHZ.htm)|Throw Rock|Arrojar roca|modificada|
|[CGyzuG0hwimkA0im.htm](pathfinder-bestiary-3-items/CGyzuG0hwimkA0im.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[CHKrjbjG2iuM6dGa.htm](pathfinder-bestiary-3-items/CHKrjbjG2iuM6dGa.htm)|Black Apoxia|Apoxia Negra|modificada|
|[cIdzhTylzbVeNykJ.htm](pathfinder-bestiary-3-items/cIdzhTylzbVeNykJ.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[CjNF1hyzoK7u7txT.htm](pathfinder-bestiary-3-items/CjNF1hyzoK7u7txT.htm)|Wind Blast|Ráfaga de Viento|modificada|
|[ck37LN0pcQmPLf0a.htm](pathfinder-bestiary-3-items/ck37LN0pcQmPLf0a.htm)|Troop Movement|Movimiento de tropas|modificada|
|[CK5NsrPLZx91r8av.htm](pathfinder-bestiary-3-items/CK5NsrPLZx91r8av.htm)|Constant Spells|Constant Spells|modificada|
|[CKhA0wknMwjoGOGJ.htm](pathfinder-bestiary-3-items/CKhA0wknMwjoGOGJ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[cLFteTTZ1b4O3AoI.htm](pathfinder-bestiary-3-items/cLFteTTZ1b4O3AoI.htm)|Fire Crossbows!|¡Ballestas de Fuego!|modificada|
|[cm8kh6khpDzrtqm7.htm](pathfinder-bestiary-3-items/cm8kh6khpDzrtqm7.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CMbpDKN5GasPKdEy.htm](pathfinder-bestiary-3-items/CMbpDKN5GasPKdEy.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[cp5UE666Pg5Pz1gR.htm](pathfinder-bestiary-3-items/cp5UE666Pg5Pz1gR.htm)|Shadow Step|Paso de Sombra|modificada|
|[cPRyXJDPZFJ7bHKq.htm](pathfinder-bestiary-3-items/cPRyXJDPZFJ7bHKq.htm)|Sweltering Heat|Sweltering Heat|modificada|
|[cpVCCJ4aUToJ2wyU.htm](pathfinder-bestiary-3-items/cpVCCJ4aUToJ2wyU.htm)|Beak|Beak|modificada|
|[cpZes8po87QoRYMi.htm](pathfinder-bestiary-3-items/cpZes8po87QoRYMi.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[cQCs1PH16u35vCzT.htm](pathfinder-bestiary-3-items/cQCs1PH16u35vCzT.htm)|Countered by Fire|Contrarrestado por Fuego|modificada|
|[cQMEu0nbq6cPn2sD.htm](pathfinder-bestiary-3-items/cQMEu0nbq6cPn2sD.htm)|Hellstrider|Hellstrider|modificada|
|[CqrOCl7qaEx1GVuB.htm](pathfinder-bestiary-3-items/CqrOCl7qaEx1GVuB.htm)|Powerful Charge|Carga Poderosa|modificada|
|[CR2hQQQ9hlXe7miP.htm](pathfinder-bestiary-3-items/CR2hQQQ9hlXe7miP.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CrApsTeWiiQiyew0.htm](pathfinder-bestiary-3-items/CrApsTeWiiQiyew0.htm)|Living Machine|Máquina Viviente|modificada|
|[CRN9VNs8XxxFho4z.htm](pathfinder-bestiary-3-items/CRN9VNs8XxxFho4z.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[CsRpTNQIfspNNH9f.htm](pathfinder-bestiary-3-items/CsRpTNQIfspNNH9f.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ct5v7Ozu8ADoqRMV.htm](pathfinder-bestiary-3-items/ct5v7Ozu8ADoqRMV.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[CTfA2dy8dT55Ygfa.htm](pathfinder-bestiary-3-items/CTfA2dy8dT55Ygfa.htm)|Knockdown|Derribo|modificada|
|[cTfE4TeDdxDyNmZt.htm](pathfinder-bestiary-3-items/cTfE4TeDdxDyNmZt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cUAlKL5D2aom7SjG.htm](pathfinder-bestiary-3-items/cUAlKL5D2aom7SjG.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[cuBDxCCJtk4iMr0A.htm](pathfinder-bestiary-3-items/cuBDxCCJtk4iMr0A.htm)|Trailblazing Stride|Zancada pionera|modificada|
|[CucaKzBXWexDGgV3.htm](pathfinder-bestiary-3-items/CucaKzBXWexDGgV3.htm)|Swing Back|Swing Back|modificada|
|[cUQLbIojBehGDQn1.htm](pathfinder-bestiary-3-items/cUQLbIojBehGDQn1.htm)|Frightful Presence|Frightful Presence|modificada|
|[cur7nFf5cRCA5ccx.htm](pathfinder-bestiary-3-items/cur7nFf5cRCA5ccx.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[CuvZkG5iAgBSIV6w.htm](pathfinder-bestiary-3-items/CuvZkG5iAgBSIV6w.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cUvzo0ln0c4WgCh2.htm](pathfinder-bestiary-3-items/cUvzo0ln0c4WgCh2.htm)|Mind Swap|Mind Swap|modificada|
|[Cv7lau7bBNhJQwti.htm](pathfinder-bestiary-3-items/Cv7lau7bBNhJQwti.htm)|Fist|Puño|modificada|
|[CvF5ABLW3yZL4Pya.htm](pathfinder-bestiary-3-items/CvF5ABLW3yZL4Pya.htm)|Dagger|Daga|modificada|
|[CVVLZgcOZxdjSrnH.htm](pathfinder-bestiary-3-items/CVVLZgcOZxdjSrnH.htm)|Wasting Curse|Wasting Curse|modificada|
|[Cw4UJYMkN1OxnJo9.htm](pathfinder-bestiary-3-items/Cw4UJYMkN1OxnJo9.htm)|Grab|Agarrado|modificada|
|[cwfUBFIbuyqydNEj.htm](pathfinder-bestiary-3-items/cwfUBFIbuyqydNEj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CWqtP7800C9YheCY.htm](pathfinder-bestiary-3-items/CWqtP7800C9YheCY.htm)|Constant Spells|Constant Spells|modificada|
|[cwvyDjJgUkCmj3aI.htm](pathfinder-bestiary-3-items/cwvyDjJgUkCmj3aI.htm)|Adamantine Claws|Garras Adamantinas|modificada|
|[cwzm6hgGDNWyBDjU.htm](pathfinder-bestiary-3-items/cwzm6hgGDNWyBDjU.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[cXcE5HUwD2KD7fgp.htm](pathfinder-bestiary-3-items/cXcE5HUwD2KD7fgp.htm)|Regeneration 30 (Deactivated by Acid, Cold, or Fire)|Regeneración 30 (Desactivado por ácido, frío o fuego)|modificada|
|[cXE4SYOLUHsl6waW.htm](pathfinder-bestiary-3-items/cXE4SYOLUHsl6waW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cxN4u9cT3UrjnDry.htm](pathfinder-bestiary-3-items/cxN4u9cT3UrjnDry.htm)|Water Jet|Chorro de Agua|modificada|
|[CybU0X1fwtJZX5W4.htm](pathfinder-bestiary-3-items/CybU0X1fwtJZX5W4.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[CYFB6ARFi1FTJRUe.htm](pathfinder-bestiary-3-items/CYFB6ARFi1FTJRUe.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[cyqkRqwp35f8CtAJ.htm](pathfinder-bestiary-3-items/cyqkRqwp35f8CtAJ.htm)|Whispers of Discord|Susurros de la Discordia|modificada|
|[Cz9TDLxidHRU8bVI.htm](pathfinder-bestiary-3-items/Cz9TDLxidHRU8bVI.htm)|Psychic Shard|Psychic Shard|modificada|
|[CzKSMmeRg2rmeWlU.htm](pathfinder-bestiary-3-items/CzKSMmeRg2rmeWlU.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[CZmwCcrjPndjeWl3.htm](pathfinder-bestiary-3-items/CZmwCcrjPndjeWl3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[CZrjwV0LG9oK1IpV.htm](pathfinder-bestiary-3-items/CZrjwV0LG9oK1IpV.htm)|Countered by Fire|Contrarrestado por Fuego|modificada|
|[CZTZt5TtBNcJtjOp.htm](pathfinder-bestiary-3-items/CZTZt5TtBNcJtjOp.htm)|Uncanny Pounce|Uncanny Abalanzarse|modificada|
|[czupLRtK5lXMuEaI.htm](pathfinder-bestiary-3-items/czupLRtK5lXMuEaI.htm)|Telepathy 80 feet|Telepatía 80 pies.|modificada|
|[D0ZfsdnVuEVXFyqB.htm](pathfinder-bestiary-3-items/D0ZfsdnVuEVXFyqB.htm)|Focused Slam|Focused Slam|modificada|
|[D1C3re9l8xuzleoq.htm](pathfinder-bestiary-3-items/D1C3re9l8xuzleoq.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[d1cyC0vptbTtXFE2.htm](pathfinder-bestiary-3-items/d1cyC0vptbTtXFE2.htm)|Dagger|Daga|modificada|
|[D1Nl8ZLF8I2wRwxv.htm](pathfinder-bestiary-3-items/D1Nl8ZLF8I2wRwxv.htm)|Hoof|Hoof|modificada|
|[d26LgNmNMpMAAoX3.htm](pathfinder-bestiary-3-items/d26LgNmNMpMAAoX3.htm)|Calculated Reload|Calculated Reload|modificada|
|[d2g5vbtRBlZsAPu4.htm](pathfinder-bestiary-3-items/d2g5vbtRBlZsAPu4.htm)|Wrathful Misfortune|Wrathful Misfortune|modificada|
|[d3CPZluPoEOBmUDr.htm](pathfinder-bestiary-3-items/d3CPZluPoEOBmUDr.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[D4ExwJPbq9vP2fZU.htm](pathfinder-bestiary-3-items/D4ExwJPbq9vP2fZU.htm)|Shy|Shy|modificada|
|[D4noCa8oJeJbAEEn.htm](pathfinder-bestiary-3-items/D4noCa8oJeJbAEEn.htm)|Change Shape|Change Shape|modificada|
|[D5k9m1BTcXvhogT6.htm](pathfinder-bestiary-3-items/D5k9m1BTcXvhogT6.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[d5pg9c64JL7EqHVV.htm](pathfinder-bestiary-3-items/d5pg9c64JL7EqHVV.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[D6GffrGRbXblVQ49.htm](pathfinder-bestiary-3-items/D6GffrGRbXblVQ49.htm)|Transparent|Transparente|modificada|
|[D6XunANSlrJ1WOJS.htm](pathfinder-bestiary-3-items/D6XunANSlrJ1WOJS.htm)|Tilt Scales|Escalas de inclinación|modificada|
|[d7MLmQG9ZkBIstUb.htm](pathfinder-bestiary-3-items/d7MLmQG9ZkBIstUb.htm)|Compulsive Counting|Conteo Compulsivo|modificada|
|[d85usCDnVlCRnJzP.htm](pathfinder-bestiary-3-items/d85usCDnVlCRnJzP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[d8kaihBUHhOfdpSO.htm](pathfinder-bestiary-3-items/d8kaihBUHhOfdpSO.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[D8VyX86E4dNG3pvk.htm](pathfinder-bestiary-3-items/D8VyX86E4dNG3pvk.htm)|Katana|Katana|modificada|
|[d8xFQfk53fDdc7Ge.htm](pathfinder-bestiary-3-items/d8xFQfk53fDdc7Ge.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[d9VRUtPpmcA0zUfF.htm](pathfinder-bestiary-3-items/d9VRUtPpmcA0zUfF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DA63Y6dqt1pn07ex.htm](pathfinder-bestiary-3-items/DA63Y6dqt1pn07ex.htm)|Sludge Tendril|Sludge Tendril|modificada|
|[DADYHBK8ittlLsj9.htm](pathfinder-bestiary-3-items/DADYHBK8ittlLsj9.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[dahd3jKunJZarMSO.htm](pathfinder-bestiary-3-items/dahd3jKunJZarMSO.htm)|Frightful Presence|Frightful Presence|modificada|
|[DAwhqp01jQeK2AM2.htm](pathfinder-bestiary-3-items/DAwhqp01jQeK2AM2.htm)|Wolfstorm|Wolfstorm|modificada|
|[dbA4U6OCx4Nn4dOe.htm](pathfinder-bestiary-3-items/dbA4U6OCx4Nn4dOe.htm)|Seaweed Strand|Seaweed Strand|modificada|
|[dBTUlLnCE86ceUMS.htm](pathfinder-bestiary-3-items/dBTUlLnCE86ceUMS.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[DbyMn4FBjCER4ikN.htm](pathfinder-bestiary-3-items/DbyMn4FBjCER4ikN.htm)|Dance of Destruction|Danza de la Destrucción|modificada|
|[DCeikz6Fl0ryJW3v.htm](pathfinder-bestiary-3-items/DCeikz6Fl0ryJW3v.htm)|No Escape|Sin huida posible|modificada|
|[DCeV72u09INkg49k.htm](pathfinder-bestiary-3-items/DCeV72u09INkg49k.htm)|Trident|Trident|modificada|
|[DCLIjWlO5fWrsiw7.htm](pathfinder-bestiary-3-items/DCLIjWlO5fWrsiw7.htm)|Shining Blaze|Shining Blaze|modificada|
|[dd90VYwacTBPrupI.htm](pathfinder-bestiary-3-items/dd90VYwacTBPrupI.htm)|Grioth Venom|Grioth Venom|modificada|
|[DDcodZwZoVe2IQw6.htm](pathfinder-bestiary-3-items/DDcodZwZoVe2IQw6.htm)|Constant Spells|Constant Spells|modificada|
|[dDtA4Al0gFgC9jck.htm](pathfinder-bestiary-3-items/dDtA4Al0gFgC9jck.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dEG5crSMOZ8MheG9.htm](pathfinder-bestiary-3-items/dEG5crSMOZ8MheG9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DElua88KAjGxlJiI.htm](pathfinder-bestiary-3-items/DElua88KAjGxlJiI.htm)|Cold Adaptation|Adaptación al frío|modificada|
|[deXYiHJyBjDSNFF5.htm](pathfinder-bestiary-3-items/deXYiHJyBjDSNFF5.htm)|Grasping Bites|Muerdemuerde|modificada|
|[df6x7z2JqZ325lY2.htm](pathfinder-bestiary-3-items/df6x7z2JqZ325lY2.htm)|Bite|Muerdemuerde|modificada|
|[DF8EHlGsG4hDnHIf.htm](pathfinder-bestiary-3-items/DF8EHlGsG4hDnHIf.htm)|Countered by Metal|Contrarrestado por Metal|modificada|
|[dGghVG22K6EF1IOo.htm](pathfinder-bestiary-3-items/dGghVG22K6EF1IOo.htm)|Twin Bites|Muerdemuerde Gemelos|modificada|
|[DHbKIfHAeaazMHgH.htm](pathfinder-bestiary-3-items/DHbKIfHAeaazMHgH.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[dHCnMadBsh7HsABT.htm](pathfinder-bestiary-3-items/dHCnMadBsh7HsABT.htm)|Polymorphic Appendage|Apéndice polimorfismo|modificada|
|[dhMEgZcYzpqH15gY.htm](pathfinder-bestiary-3-items/dhMEgZcYzpqH15gY.htm)|Chattering Teeth|Chattering Teeth|modificada|
|[DhZVlrSd475Ljs1k.htm](pathfinder-bestiary-3-items/DhZVlrSd475Ljs1k.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[DJ5RgiOgDCBnlgH3.htm](pathfinder-bestiary-3-items/DJ5RgiOgDCBnlgH3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[DjcXxE3rldN09uxu.htm](pathfinder-bestiary-3-items/DjcXxE3rldN09uxu.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[DjRAEGwWJTNcjr6z.htm](pathfinder-bestiary-3-items/DjRAEGwWJTNcjr6z.htm)|Draining Blight|Tizón Drenante|modificada|
|[dJT018LZV3FEJvJO.htm](pathfinder-bestiary-3-items/dJT018LZV3FEJvJO.htm)|Knockdown|Derribo|modificada|
|[DkdUj7zo7tI7BCpF.htm](pathfinder-bestiary-3-items/DkdUj7zo7tI7BCpF.htm)|Jaws|Fauces|modificada|
|[DKeP0NHGZX49S9dE.htm](pathfinder-bestiary-3-items/DKeP0NHGZX49S9dE.htm)|Grab|Agarrado|modificada|
|[DkRuMYAHXbZdXuqZ.htm](pathfinder-bestiary-3-items/DkRuMYAHXbZdXuqZ.htm)|Threatening Visage|Visaje Amenazador|modificada|
|[Dl0UbAiU402HXgL9.htm](pathfinder-bestiary-3-items/Dl0UbAiU402HXgL9.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[DL9RtsdSy9Gulbqd.htm](pathfinder-bestiary-3-items/DL9RtsdSy9Gulbqd.htm)|Greatsword|Greatsword|modificada|
|[DLKqB3QVep4jRNGS.htm](pathfinder-bestiary-3-items/DLKqB3QVep4jRNGS.htm)|Truescript|Truescript|modificada|
|[dLMAFMqNUxlmTN4X.htm](pathfinder-bestiary-3-items/dLMAFMqNUxlmTN4X.htm)|Dagger|Daga|modificada|
|[DNA5DHqZmcle3dXg.htm](pathfinder-bestiary-3-items/DNA5DHqZmcle3dXg.htm)|Shriek|Shriek|modificada|
|[DNDY9jZtwm6GdoBP.htm](pathfinder-bestiary-3-items/DNDY9jZtwm6GdoBP.htm)|Improved Grab|Agarrado mejorado|modificada|
|[DNkqMNI50CBgRZbS.htm](pathfinder-bestiary-3-items/DNkqMNI50CBgRZbS.htm)|Ice Stride|Zancada de hielo|modificada|
|[DNMBuy0G69bflxTr.htm](pathfinder-bestiary-3-items/DNMBuy0G69bflxTr.htm)|Tendril|Tendril|modificada|
|[doTDspe65aJOvJFL.htm](pathfinder-bestiary-3-items/doTDspe65aJOvJFL.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[DotZCkt036LONE1t.htm](pathfinder-bestiary-3-items/DotZCkt036LONE1t.htm)|Claw|Garra|modificada|
|[DP0QpHqfJo5budhm.htm](pathfinder-bestiary-3-items/DP0QpHqfJo5budhm.htm)|Swallow Whole|Engullir Todo|modificada|
|[dp14tTXhxb0SZSZq.htm](pathfinder-bestiary-3-items/dp14tTXhxb0SZSZq.htm)|Project Echoblade|Proyecto Echoblade|modificada|
|[DPf0kYMfi2748Y1L.htm](pathfinder-bestiary-3-items/DPf0kYMfi2748Y1L.htm)|Improved Grab|Agarrado mejorado|modificada|
|[Dpji6q4R6gQMLALe.htm](pathfinder-bestiary-3-items/Dpji6q4R6gQMLALe.htm)|Earthen Fist|Puño de Tierra|modificada|
|[dpMfwwbltlZ55vSZ.htm](pathfinder-bestiary-3-items/dpMfwwbltlZ55vSZ.htm)|Final Blasphemy|Blasfemia final|modificada|
|[DpQ8esg6GOdnxHkt.htm](pathfinder-bestiary-3-items/DpQ8esg6GOdnxHkt.htm)|Claw|Garra|modificada|
|[DqllMZa6Ja60FhmJ.htm](pathfinder-bestiary-3-items/DqllMZa6Ja60FhmJ.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[dQW2DvGO2PSM5XBY.htm](pathfinder-bestiary-3-items/dQW2DvGO2PSM5XBY.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Dr1ESmEvHchNQHYd.htm](pathfinder-bestiary-3-items/Dr1ESmEvHchNQHYd.htm)|Grab|Agarrado|modificada|
|[DRfoCntho3tTjU8x.htm](pathfinder-bestiary-3-items/DRfoCntho3tTjU8x.htm)|Constrict|Restringir|modificada|
|[DrR0GXOBDUWyiyEd.htm](pathfinder-bestiary-3-items/DrR0GXOBDUWyiyEd.htm)|Frightful Presence|Frightful Presence|modificada|
|[dRWLZGZySawLWgLz.htm](pathfinder-bestiary-3-items/dRWLZGZySawLWgLz.htm)|Winning Smile|Winning Smile|modificada|
|[dS2bG5NB6KkMXMXH.htm](pathfinder-bestiary-3-items/dS2bG5NB6KkMXMXH.htm)|Jaws|Fauces|modificada|
|[ds6T4axImpnQYao4.htm](pathfinder-bestiary-3-items/ds6T4axImpnQYao4.htm)|Break Ground|Break Ground|modificada|
|[DSDbtNz99LPKAHnV.htm](pathfinder-bestiary-3-items/DSDbtNz99LPKAHnV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DSdWvRkowqLA8hig.htm](pathfinder-bestiary-3-items/DSdWvRkowqLA8hig.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[dSIKXmZhpwoDtoh3.htm](pathfinder-bestiary-3-items/dSIKXmZhpwoDtoh3.htm)|Beak|Beak|modificada|
|[dStg3v3luFQKIonn.htm](pathfinder-bestiary-3-items/dStg3v3luFQKIonn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DToLcWr6RKiExkcA.htm](pathfinder-bestiary-3-items/DToLcWr6RKiExkcA.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[dUtAAFoA9eQnE0J5.htm](pathfinder-bestiary-3-items/dUtAAFoA9eQnE0J5.htm)|Disrupting Staff|Personal Disruptora|modificada|
|[dvHLzTpiciD6p1Yb.htm](pathfinder-bestiary-3-items/dvHLzTpiciD6p1Yb.htm)|Astral Recoil|Astral Recoil|modificada|
|[dW7bkWhXvNcwfUCh.htm](pathfinder-bestiary-3-items/dW7bkWhXvNcwfUCh.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[DW8Cqi1XpUWPpQyd.htm](pathfinder-bestiary-3-items/DW8Cqi1XpUWPpQyd.htm)|Stinger|Aguijón|modificada|
|[dWa7XGI4buL9uDdZ.htm](pathfinder-bestiary-3-items/dWa7XGI4buL9uDdZ.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[DwbOG3M8gGZfp6CI.htm](pathfinder-bestiary-3-items/DwbOG3M8gGZfp6CI.htm)|Pry|Pry|modificada|
|[dwH6nJY0Yb3I4jsA.htm](pathfinder-bestiary-3-items/dwH6nJY0Yb3I4jsA.htm)|Reposition|Reposicionar|modificada|
|[dwi92HM5kiYhFz8W.htm](pathfinder-bestiary-3-items/dwi92HM5kiYhFz8W.htm)|Fist|Puño|modificada|
|[dwMummZWtYk7b2LJ.htm](pathfinder-bestiary-3-items/dwMummZWtYk7b2LJ.htm)|Final Jape|Final Jape|modificada|
|[dWphYFgVjZP3oKOy.htm](pathfinder-bestiary-3-items/dWphYFgVjZP3oKOy.htm)|Negative Healing|Curación negativa|modificada|
|[dWtpFgBDu68MZFgx.htm](pathfinder-bestiary-3-items/dWtpFgBDu68MZFgx.htm)|Fist|Puño|modificada|
|[DWxhZZHfxKIg0dak.htm](pathfinder-bestiary-3-items/DWxhZZHfxKIg0dak.htm)|Fist|Puño|modificada|
|[dX486NKkzEeXKpLt.htm](pathfinder-bestiary-3-items/dX486NKkzEeXKpLt.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[dXeWudwXB4aN3eTx.htm](pathfinder-bestiary-3-items/dXeWudwXB4aN3eTx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[dXTDQTPpz6jKxLYH.htm](pathfinder-bestiary-3-items/dXTDQTPpz6jKxLYH.htm)|Improved Grab|Agarrado mejorado|modificada|
|[Dy4ItmxMvWZNtvIJ.htm](pathfinder-bestiary-3-items/Dy4ItmxMvWZNtvIJ.htm)|Surface-Bound|Surface-Bound|modificada|
|[DY5764x6bU3jfnwu.htm](pathfinder-bestiary-3-items/DY5764x6bU3jfnwu.htm)|Slow|Lentificado/a|modificada|
|[dyHPOgwE2Bso99pQ.htm](pathfinder-bestiary-3-items/dyHPOgwE2Bso99pQ.htm)|Antler|Antler|modificada|
|[DYPC5uZCfiJo895S.htm](pathfinder-bestiary-3-items/DYPC5uZCfiJo895S.htm)|Manifest|Manifiesto|modificada|
|[dyqAZBHswn1148hK.htm](pathfinder-bestiary-3-items/dyqAZBHswn1148hK.htm)|Knockdown|Derribo|modificada|
|[dyrhkvCX2H3pZBp9.htm](pathfinder-bestiary-3-items/dyrhkvCX2H3pZBp9.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[dYuJ14oybPHNyAOW.htm](pathfinder-bestiary-3-items/dYuJ14oybPHNyAOW.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[dyWftDFNWQBORQLX.htm](pathfinder-bestiary-3-items/dyWftDFNWQBORQLX.htm)|Grab Weapon|Arma agarrada|modificada|
|[dZb5yXk5MS02kU1G.htm](pathfinder-bestiary-3-items/dZb5yXk5MS02kU1G.htm)|Brutal Blows|Golpes Brutales|modificada|
|[DzdpbPovIBcXuiRi.htm](pathfinder-bestiary-3-items/DzdpbPovIBcXuiRi.htm)|Claw|Garra|modificada|
|[dzHyKmRIrVao4ke0.htm](pathfinder-bestiary-3-items/dzHyKmRIrVao4ke0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[E06rANySjyXiIjDH.htm](pathfinder-bestiary-3-items/E06rANySjyXiIjDH.htm)|Boiling Blood|Sangre hirviendo|modificada|
|[E1tUKsZc3ldzya9y.htm](pathfinder-bestiary-3-items/E1tUKsZc3ldzya9y.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[e2fOY59UxAgnqdHF.htm](pathfinder-bestiary-3-items/e2fOY59UxAgnqdHF.htm)|Command Thrall|Orden imperiosa Thrall|modificada|
|[e3kt6jZa3bspNiSF.htm](pathfinder-bestiary-3-items/e3kt6jZa3bspNiSF.htm)|Anchored Soul|Alma Anclada|modificada|
|[e3ozogxqOi4jcWiU.htm](pathfinder-bestiary-3-items/e3ozogxqOi4jcWiU.htm)|Pungent Aura|Aura Picante|modificada|
|[E3RAvTSsOhNonHz1.htm](pathfinder-bestiary-3-items/E3RAvTSsOhNonHz1.htm)|Constant Spells|Constant Spells|modificada|
|[E3tmyHUT7v0BSgAP.htm](pathfinder-bestiary-3-items/E3tmyHUT7v0BSgAP.htm)|Greater Constrict|Mayor Restricción|modificada|
|[e4gcYFnxnq6FmPws.htm](pathfinder-bestiary-3-items/e4gcYFnxnq6FmPws.htm)|Deadly Spark|Chispa letal|modificada|
|[E4Nclckgv1bSz78W.htm](pathfinder-bestiary-3-items/E4Nclckgv1bSz78W.htm)|Blood Scent|Blood Scent|modificada|
|[e4q0ijGMv88voluX.htm](pathfinder-bestiary-3-items/e4q0ijGMv88voluX.htm)|Grab|Agarrado|modificada|
|[E5jGyO2W780NMvYV.htm](pathfinder-bestiary-3-items/E5jGyO2W780NMvYV.htm)|Antler|Antler|modificada|
|[E6dBhZRBf4SXzGl7.htm](pathfinder-bestiary-3-items/E6dBhZRBf4SXzGl7.htm)|Resize Plant|Redimensionar Planta|modificada|
|[e81mjeZGp9mKBDKv.htm](pathfinder-bestiary-3-items/e81mjeZGp9mKBDKv.htm)|Change Shape|Change Shape|modificada|
|[EAC12Ob2QRDH7V3R.htm](pathfinder-bestiary-3-items/EAC12Ob2QRDH7V3R.htm)|Breath Weapon|Breath Weapon|modificada|
|[EaFbQFfNsYFJLrSK.htm](pathfinder-bestiary-3-items/EaFbQFfNsYFJLrSK.htm)|Fangs|Colmillos|modificada|
|[eaYQr6jbgAf1fd2l.htm](pathfinder-bestiary-3-items/eaYQr6jbgAf1fd2l.htm)|Jaws|Fauces|modificada|
|[eb3vh43tEB1z2vtJ.htm](pathfinder-bestiary-3-items/eb3vh43tEB1z2vtJ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ebO4IFQGFnQQfnSq.htm](pathfinder-bestiary-3-items/ebO4IFQGFnQQfnSq.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[EbsFN4gbz3zpDwft.htm](pathfinder-bestiary-3-items/EbsFN4gbz3zpDwft.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ebuEFfG89GEGr5RZ.htm](pathfinder-bestiary-3-items/ebuEFfG89GEGr5RZ.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[eBzYlEBFycddJLAk.htm](pathfinder-bestiary-3-items/eBzYlEBFycddJLAk.htm)|Hunt Prey|Perseguir presa|modificada|
|[ECl6ybDr5qUy0KF6.htm](pathfinder-bestiary-3-items/ECl6ybDr5qUy0KF6.htm)|Fed by Wood|Alimentado por Madera|modificada|
|[EcU0YXprTtXbRRmY.htm](pathfinder-bestiary-3-items/EcU0YXprTtXbRRmY.htm)|Bloodbird|Bloodbird|modificada|
|[ECVcMDSSzJXSESDi.htm](pathfinder-bestiary-3-items/ECVcMDSSzJXSESDi.htm)|Claw|Garra|modificada|
|[eD9aNvigXSOpv46o.htm](pathfinder-bestiary-3-items/eD9aNvigXSOpv46o.htm)|Ultrasonic Pulse|Pulso Ultrasónico|modificada|
|[EDn6PkdwmLETtSlD.htm](pathfinder-bestiary-3-items/EDn6PkdwmLETtSlD.htm)|Phase Lurch|Phase Lurch|modificada|
|[edVyMro5viX5rgD9.htm](pathfinder-bestiary-3-items/edVyMro5viX5rgD9.htm)|Wind-Up|Wind-Up|modificada|
|[EdX3IAMq8CXGNxOu.htm](pathfinder-bestiary-3-items/EdX3IAMq8CXGNxOu.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[ee90NjJ1Ze4Ghwsk.htm](pathfinder-bestiary-3-items/ee90NjJ1Ze4Ghwsk.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[EFidHtj7QBgjYsOb.htm](pathfinder-bestiary-3-items/EFidHtj7QBgjYsOb.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EFkfk6hbU9iqt1ja.htm](pathfinder-bestiary-3-items/EFkfk6hbU9iqt1ja.htm)|Frightful Presence|Frightful Presence|modificada|
|[EGyTn2mFKrYKKJvX.htm](pathfinder-bestiary-3-items/EGyTn2mFKrYKKJvX.htm)|Nature's Chosen|Nature's Chosen|modificada|
|[EhA1NamwzAZrJMj2.htm](pathfinder-bestiary-3-items/EhA1NamwzAZrJMj2.htm)|Bite|Muerdemuerde|modificada|
|[EHAajCEP6flvt7fx.htm](pathfinder-bestiary-3-items/EHAajCEP6flvt7fx.htm)|Lignifying Bite|Muerdemuerde Lignificante|modificada|
|[EhcLe86zdmbslOZG.htm](pathfinder-bestiary-3-items/EhcLe86zdmbslOZG.htm)|Wave|Wave|modificada|
|[ehlp9AWnS6V5OAuB.htm](pathfinder-bestiary-3-items/ehlp9AWnS6V5OAuB.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[EHsKUjzbFonBGt9N.htm](pathfinder-bestiary-3-items/EHsKUjzbFonBGt9N.htm)|Envenom|Envenom|modificada|
|[eI8RLbyITDSsCGtu.htm](pathfinder-bestiary-3-items/eI8RLbyITDSsCGtu.htm)|Bound Spirits|Bound Spirits|modificada|
|[eiBZvzNac6ZHZQDi.htm](pathfinder-bestiary-3-items/eiBZvzNac6ZHZQDi.htm)|Hatchet|Hacha|modificada|
|[EIopSecYGMv2HxGV.htm](pathfinder-bestiary-3-items/EIopSecYGMv2HxGV.htm)|Embed Quill|Embed Quill|modificada|
|[eiuG3n39AiXSGDav.htm](pathfinder-bestiary-3-items/eiuG3n39AiXSGDav.htm)|Dimensional Pit|Dimensional Pit|modificada|
|[EJ2Xgd9VGb4Rby3n.htm](pathfinder-bestiary-3-items/EJ2Xgd9VGb4Rby3n.htm)|Isolating Lash|Latigazo Aislante|modificada|
|[eJPyB6k2nbixpAxn.htm](pathfinder-bestiary-3-items/eJPyB6k2nbixpAxn.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[EknlSoHchVPvAGPA.htm](pathfinder-bestiary-3-items/EknlSoHchVPvAGPA.htm)|Grasping Bites|Muerdemuerde|modificada|
|[eKQzWhQFHFwAwUYf.htm](pathfinder-bestiary-3-items/eKQzWhQFHFwAwUYf.htm)|Perfected Flight|Perfected Flight|modificada|
|[EKRq00ExnMsBTNP4.htm](pathfinder-bestiary-3-items/EKRq00ExnMsBTNP4.htm)|Hurtful Critique|Crítica hiriente|modificada|
|[EkUXODIcewWOk6qH.htm](pathfinder-bestiary-3-items/EkUXODIcewWOk6qH.htm)|Freeze Shell|Congelar Shell|modificada|
|[elcu7DQCtC0dX7Q4.htm](pathfinder-bestiary-3-items/elcu7DQCtC0dX7Q4.htm)|Negative Healing|Curación negativa|modificada|
|[eLKeINPUnuoZeD9D.htm](pathfinder-bestiary-3-items/eLKeINPUnuoZeD9D.htm)|Swarm Mind|Swarm Mind|modificada|
|[ELmguzDIW19zBvG2.htm](pathfinder-bestiary-3-items/ELmguzDIW19zBvG2.htm)|Constant Spells|Constant Spells|modificada|
|[emdMVqKxpviFTtN7.htm](pathfinder-bestiary-3-items/emdMVqKxpviFTtN7.htm)|Regeneration 40 (Deactivated by Chaotic, Mental or Orichalcum)|Regeneración 40 (Desactivado por Caótico, Mental u Orichalcum)|modificada|
|[eMH8aLOsO2dW2SPL.htm](pathfinder-bestiary-3-items/eMH8aLOsO2dW2SPL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[eNkQHj53Zq1tG2jD.htm](pathfinder-bestiary-3-items/eNkQHj53Zq1tG2jD.htm)|Eclipse|Eclipse|modificada|
|[ENpP2o2H3ia9c6NJ.htm](pathfinder-bestiary-3-items/ENpP2o2H3ia9c6NJ.htm)|Surreal Anatomy|Anatomía Surrealista|modificada|
|[ENxpJknQm2f0aGri.htm](pathfinder-bestiary-3-items/ENxpJknQm2f0aGri.htm)|Speak with Arthropods|Habla con Artrópodos|modificada|
|[eoGlb3oqWZjUaNDi.htm](pathfinder-bestiary-3-items/eoGlb3oqWZjUaNDi.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[eogxSxYx5HC0X9Tc.htm](pathfinder-bestiary-3-items/eogxSxYx5HC0X9Tc.htm)|Jaws|Fauces|modificada|
|[EoRz5vyqTCn8pIIk.htm](pathfinder-bestiary-3-items/EoRz5vyqTCn8pIIk.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[EPLy8QbtiSRbEEme.htm](pathfinder-bestiary-3-items/EPLy8QbtiSRbEEme.htm)|Swarm Mind|Swarm Mind|modificada|
|[eQjA75mkF3WdkNS8.htm](pathfinder-bestiary-3-items/eQjA75mkF3WdkNS8.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[eqx6f68rVSfojXpJ.htm](pathfinder-bestiary-3-items/eqx6f68rVSfojXpJ.htm)|Hyponatremia|Hiponatremia|modificada|
|[erIKrXEzR8ADuFq8.htm](pathfinder-bestiary-3-items/erIKrXEzR8ADuFq8.htm)|Tail|Tail|modificada|
|[ERS9rhuIGUTjxi8H.htm](pathfinder-bestiary-3-items/ERS9rhuIGUTjxi8H.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ET0tToniC9uHpsbM.htm](pathfinder-bestiary-3-items/ET0tToniC9uHpsbM.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[eT3PtWf4uiMjHI7j.htm](pathfinder-bestiary-3-items/eT3PtWf4uiMjHI7j.htm)|Perfected Flight|Perfected Flight|modificada|
|[EtHDwrRNzkfZ50vn.htm](pathfinder-bestiary-3-items/EtHDwrRNzkfZ50vn.htm)|Forsaken Patron|Patrón de los Repudiados|modificada|
|[eTWugVatqxe8rLdZ.htm](pathfinder-bestiary-3-items/eTWugVatqxe8rLdZ.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[ETZ0a2GwlpGrMxWM.htm](pathfinder-bestiary-3-items/ETZ0a2GwlpGrMxWM.htm)|Rock|Roca|modificada|
|[eU85Cgnz1ezRDCdF.htm](pathfinder-bestiary-3-items/eU85Cgnz1ezRDCdF.htm)|Hatchet|Hacha|modificada|
|[EuQQ73V8hJuBn5EU.htm](pathfinder-bestiary-3-items/EuQQ73V8hJuBn5EU.htm)|Grab Item|Objeto agarrado|modificada|
|[EViJHho1D7U93moQ.htm](pathfinder-bestiary-3-items/EViJHho1D7U93moQ.htm)|Smoke Vision|Visión de Humo|modificada|
|[eVqb0828602ykLqp.htm](pathfinder-bestiary-3-items/eVqb0828602ykLqp.htm)|Head Bowl|Tazón de cabeza|modificada|
|[Ew8oLYahqyGEluxo.htm](pathfinder-bestiary-3-items/Ew8oLYahqyGEluxo.htm)|Greater Constrict|Mayor Restricción|modificada|
|[eWeqt47GwgkuwIQ8.htm](pathfinder-bestiary-3-items/eWeqt47GwgkuwIQ8.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[eWV9eGMG3Ss6ZE78.htm](pathfinder-bestiary-3-items/eWV9eGMG3Ss6ZE78.htm)|+4 Status to All Saves vs. Mental or Divine|+4 situación a todas las salvaciones contra mental o divino.|modificada|
|[EWX813DLZAePlSAY.htm](pathfinder-bestiary-3-items/EWX813DLZAePlSAY.htm)|Jaws|Fauces|modificada|
|[EXdI3guudVr8XFFO.htm](pathfinder-bestiary-3-items/EXdI3guudVr8XFFO.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[eY479IPS7BQvkOTF.htm](pathfinder-bestiary-3-items/eY479IPS7BQvkOTF.htm)|Love's Impunity|La impunidad del amor|modificada|
|[EybKkcXQpisZym7b.htm](pathfinder-bestiary-3-items/EybKkcXQpisZym7b.htm)|Claim Trophy|Reclamar Trofeo|modificada|
|[eYkYSZX390lDUFFK.htm](pathfinder-bestiary-3-items/eYkYSZX390lDUFFK.htm)|Pounce|Abalanzarse|modificada|
|[EYLtQQcjxfjr6MNe.htm](pathfinder-bestiary-3-items/EYLtQQcjxfjr6MNe.htm)|Regeneration (50) (Deactivated by Good, Mental, Orichalcum)|Regeneración (50) (Desactivado por Bueno, Mental, Orichalcum)|modificada|
|[eZ5EIf8w0pc83e2W.htm](pathfinder-bestiary-3-items/eZ5EIf8w0pc83e2W.htm)|Shameful Loathing|Aborrecimiento vergonzoso|modificada|
|[EZESCdF1dzv8z0rc.htm](pathfinder-bestiary-3-items/EZESCdF1dzv8z0rc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EzfpvwnxObWcM5rn.htm](pathfinder-bestiary-3-items/EzfpvwnxObWcM5rn.htm)|Carver's Curse|Carver's Curse|modificada|
|[eZiRahkdCT1SRMx1.htm](pathfinder-bestiary-3-items/eZiRahkdCT1SRMx1.htm)|Rock|Roca|modificada|
|[EzVhFWQhs6axH99B.htm](pathfinder-bestiary-3-items/EzVhFWQhs6axH99B.htm)|Spear|Lanza|modificada|
|[F0RaqdAbEfogkX2c.htm](pathfinder-bestiary-3-items/F0RaqdAbEfogkX2c.htm)|Fan Bolt|Fan Bolt|modificada|
|[F0su6gl7ieMAuCn8.htm](pathfinder-bestiary-3-items/F0su6gl7ieMAuCn8.htm)|Raise Guard|Levantar guardia|modificada|
|[f0YkO5SMtEfB3r39.htm](pathfinder-bestiary-3-items/f0YkO5SMtEfB3r39.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[F1BUx1q2oTY8sFpj.htm](pathfinder-bestiary-3-items/F1BUx1q2oTY8sFpj.htm)|Clutching Cold|Clutching Cold|modificada|
|[f4MFrfJdBTJTN23S.htm](pathfinder-bestiary-3-items/f4MFrfJdBTJTN23S.htm)|Command Thrall|Orden imperiosa Thrall|modificada|
|[f4Mkjn4uAW6nabJR.htm](pathfinder-bestiary-3-items/f4Mkjn4uAW6nabJR.htm)|Rearrange Possessions|Reorganizar posesión|modificada|
|[F5nrBH7jkTIQsJqp.htm](pathfinder-bestiary-3-items/F5nrBH7jkTIQsJqp.htm)|Spirit Body|Cuerpo espiritual|modificada|
|[f661MaVKOhxZZMUI.htm](pathfinder-bestiary-3-items/f661MaVKOhxZZMUI.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[f6S96FxE504I9tBq.htm](pathfinder-bestiary-3-items/f6S96FxE504I9tBq.htm)|Lifesense 120 feet|Lifesense 120 pies|modificada|
|[f6v0xX9B6TCjk14f.htm](pathfinder-bestiary-3-items/f6v0xX9B6TCjk14f.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[f714vqweH1u5q1ZG.htm](pathfinder-bestiary-3-items/f714vqweH1u5q1ZG.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[F7XOjtJspPrYwCzy.htm](pathfinder-bestiary-3-items/F7XOjtJspPrYwCzy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[f8ucclfLXdX89h8k.htm](pathfinder-bestiary-3-items/f8ucclfLXdX89h8k.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[F98RBByZ9IcrIL7x.htm](pathfinder-bestiary-3-items/F98RBByZ9IcrIL7x.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[f9gqhbFaPyj77i0e.htm](pathfinder-bestiary-3-items/f9gqhbFaPyj77i0e.htm)|Light Hammer|Martillo ligero|modificada|
|[F9hcxqTd9pv3rmpZ.htm](pathfinder-bestiary-3-items/F9hcxqTd9pv3rmpZ.htm)|Skip Between|Saltar entre|modificada|
|[F9OvUrIH2XqsqnGm.htm](pathfinder-bestiary-3-items/F9OvUrIH2XqsqnGm.htm)|Root|Enraizarse|modificada|
|[f9xJOejUBSfPwnLt.htm](pathfinder-bestiary-3-items/f9xJOejUBSfPwnLt.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[F9z5eqsEuY9xP7v7.htm](pathfinder-bestiary-3-items/F9z5eqsEuY9xP7v7.htm)|Frightful Presence|Frightful Presence|modificada|
|[FAGZiwfUhhTFHxxn.htm](pathfinder-bestiary-3-items/FAGZiwfUhhTFHxxn.htm)|Claw|Garra|modificada|
|[faqkx0QvzDNLMR1M.htm](pathfinder-bestiary-3-items/faqkx0QvzDNLMR1M.htm)|Constant Spells|Constant Spells|modificada|
|[fAtIew8TlThwaKzS.htm](pathfinder-bestiary-3-items/fAtIew8TlThwaKzS.htm)|Dagger|Daga|modificada|
|[fbcWIR3VTHaRmvLw.htm](pathfinder-bestiary-3-items/fbcWIR3VTHaRmvLw.htm)|Swarming Slither|Deslizarse de plaga|modificada|
|[fcF7yPdFg6xTRRlu.htm](pathfinder-bestiary-3-items/fcF7yPdFg6xTRRlu.htm)|Hunter's Triumph|Triunfo del Cazador|modificada|
|[fCSVGUKsmrepwWjP.htm](pathfinder-bestiary-3-items/fCSVGUKsmrepwWjP.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[fcx7u3nEY1i0o8JF.htm](pathfinder-bestiary-3-items/fcx7u3nEY1i0o8JF.htm)|Pungent|Picante|modificada|
|[fDBEEgdrhbSzqYYM.htm](pathfinder-bestiary-3-items/fDBEEgdrhbSzqYYM.htm)|Delicious|Delicioso|modificada|
|[fDOGJqfBeWIT7IOl.htm](pathfinder-bestiary-3-items/fDOGJqfBeWIT7IOl.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[fE3RaY5ixs8crh6p.htm](pathfinder-bestiary-3-items/fE3RaY5ixs8crh6p.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[FeIulhzHCcKLFyNH.htm](pathfinder-bestiary-3-items/FeIulhzHCcKLFyNH.htm)|Claw|Garra|modificada|
|[FEmYP0SV4WLWDH9k.htm](pathfinder-bestiary-3-items/FEmYP0SV4WLWDH9k.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[fFOSRHY0ACjbHmk0.htm](pathfinder-bestiary-3-items/fFOSRHY0ACjbHmk0.htm)|Strix Camaraderie|Camaradería Strix|modificada|
|[fg1os9FHkiVGPkuh.htm](pathfinder-bestiary-3-items/fg1os9FHkiVGPkuh.htm)|Limited Immortality|Inmortalidad Limitada|modificada|
|[FGcmZG66TcZc2r8o.htm](pathfinder-bestiary-3-items/FGcmZG66TcZc2r8o.htm)|Constrict|Restringir|modificada|
|[fgDDQdQN0mgFgxVm.htm](pathfinder-bestiary-3-items/fgDDQdQN0mgFgxVm.htm)|Raccoon's Whimsy|Capricho de mapache|modificada|
|[Fgyso5whxxzTMpVE.htm](pathfinder-bestiary-3-items/Fgyso5whxxzTMpVE.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[fH4zN6EufktPqbW4.htm](pathfinder-bestiary-3-items/fH4zN6EufktPqbW4.htm)|Clockwork Wand|Clockwork Wand|modificada|
|[FH98KC2pUcRXdN35.htm](pathfinder-bestiary-3-items/FH98KC2pUcRXdN35.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[FHCDHU9Avy8uYlDu.htm](pathfinder-bestiary-3-items/FHCDHU9Avy8uYlDu.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[fhIh70SWeQm18cfv.htm](pathfinder-bestiary-3-items/fhIh70SWeQm18cfv.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[fhK47KtCqw1hqre0.htm](pathfinder-bestiary-3-items/fhK47KtCqw1hqre0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[FHVxImDGF7MB4tzs.htm](pathfinder-bestiary-3-items/FHVxImDGF7MB4tzs.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[FHWDKbm2r5ltPmmK.htm](pathfinder-bestiary-3-items/FHWDKbm2r5ltPmmK.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Fil9NBsfO3iADreb.htm](pathfinder-bestiary-3-items/Fil9NBsfO3iADreb.htm)|Sandstorm|Tormenta de Arena|modificada|
|[FJEaQqHttC6Z3MP3.htm](pathfinder-bestiary-3-items/FJEaQqHttC6Z3MP3.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[FJGG1xzdcXWDtz4x.htm](pathfinder-bestiary-3-items/FJGG1xzdcXWDtz4x.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[fJlI13pn1Y8ntWFP.htm](pathfinder-bestiary-3-items/fJlI13pn1Y8ntWFP.htm)|Heartless Furnace|Horno sin corazón|modificada|
|[Fk0okw5jh318HfC0.htm](pathfinder-bestiary-3-items/Fk0okw5jh318HfC0.htm)|Boundless Reach|Alcance sin Límites|modificada|
|[Fk6KpPG5r84Cxf3v.htm](pathfinder-bestiary-3-items/Fk6KpPG5r84Cxf3v.htm)|Constant Spells|Constant Spells|modificada|
|[fKdYArB4XGbeZO0e.htm](pathfinder-bestiary-3-items/fKdYArB4XGbeZO0e.htm)|Staff|Báculo|modificada|
|[FkGm83vCSFYej5yy.htm](pathfinder-bestiary-3-items/FkGm83vCSFYej5yy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FkwYpWuwVTFS02Jn.htm](pathfinder-bestiary-3-items/FkwYpWuwVTFS02Jn.htm)|All-Around Vision|All-Around Vision|modificada|
|[fLCQ4ieE5VzQA9NP.htm](pathfinder-bestiary-3-items/fLCQ4ieE5VzQA9NP.htm)|Borrowed Skin|Piel prestada|modificada|
|[flo5oC8zWtFBUsvt.htm](pathfinder-bestiary-3-items/flo5oC8zWtFBUsvt.htm)|Jaws|Fauces|modificada|
|[fLXcaCWjl1dAjO33.htm](pathfinder-bestiary-3-items/fLXcaCWjl1dAjO33.htm)|Bite|Muerdemuerde|modificada|
|[FLY1h37SM1s6HQ3K.htm](pathfinder-bestiary-3-items/FLY1h37SM1s6HQ3K.htm)|Web Trap|Trampa telara|modificada|
|[FmabDHqtzgl9JIl4.htm](pathfinder-bestiary-3-items/FmabDHqtzgl9JIl4.htm)|Jaws|Fauces|modificada|
|[fmxx7smV9KEkfRN8.htm](pathfinder-bestiary-3-items/fmxx7smV9KEkfRN8.htm)|Isolate Foes|Aislar enemigos|modificada|
|[fN2P498vq870hq1W.htm](pathfinder-bestiary-3-items/fN2P498vq870hq1W.htm)|All-Around Vision|All-Around Vision|modificada|
|[FnIxsS3RE81lmBvi.htm](pathfinder-bestiary-3-items/FnIxsS3RE81lmBvi.htm)|Kukri|Kukri|modificada|
|[fNOL0XY0wCqc5rUq.htm](pathfinder-bestiary-3-items/fNOL0XY0wCqc5rUq.htm)|+3 Status to All Saves vs. Divine Magic|+3 situación a todas las salvaciones contra magia divina.|modificada|
|[fOaemwLydAbI9Tdb.htm](pathfinder-bestiary-3-items/fOaemwLydAbI9Tdb.htm)|Grab|Agarrado|modificada|
|[FodKNeEvBm6FqwvE.htm](pathfinder-bestiary-3-items/FodKNeEvBm6FqwvE.htm)|Impaling Charge|Carga empaladora|modificada|
|[foFqU5DE5kDifheg.htm](pathfinder-bestiary-3-items/foFqU5DE5kDifheg.htm)|Smoke Vision|Visión de Humo|modificada|
|[foR7mgXEL0XMHJ6J.htm](pathfinder-bestiary-3-items/foR7mgXEL0XMHJ6J.htm)|Flurry of Pods|Ráfaga de vainas|modificada|
|[fp6MDEvJKxcE7hE3.htm](pathfinder-bestiary-3-items/fp6MDEvJKxcE7hE3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FPfOOgSVW55hbKb0.htm](pathfinder-bestiary-3-items/FPfOOgSVW55hbKb0.htm)|Splatter|Splatter|modificada|
|[fPZelv8NqLRyuQ1p.htm](pathfinder-bestiary-3-items/fPZelv8NqLRyuQ1p.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[fQAp6FTjVozJr8XU.htm](pathfinder-bestiary-3-items/fQAp6FTjVozJr8XU.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[fQuNqFErQHkBJLE8.htm](pathfinder-bestiary-3-items/fQuNqFErQHkBJLE8.htm)|Claw|Garra|modificada|
|[FqY5LFjb311dDEv1.htm](pathfinder-bestiary-3-items/FqY5LFjb311dDEv1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FR0NE1tkGwNuwmOZ.htm](pathfinder-bestiary-3-items/FR0NE1tkGwNuwmOZ.htm)|Improved Grab|Agarrado mejorado|modificada|
|[fr7zLtKzGQVPqqKL.htm](pathfinder-bestiary-3-items/fr7zLtKzGQVPqqKL.htm)|Spit|Escupe|modificada|
|[fRSODlNG63LciyB1.htm](pathfinder-bestiary-3-items/fRSODlNG63LciyB1.htm)|Constant Spells|Constant Spells|modificada|
|[fRYKciZWxC0TiEcw.htm](pathfinder-bestiary-3-items/fRYKciZWxC0TiEcw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fSDTMDmYkmPrgbRM.htm](pathfinder-bestiary-3-items/fSDTMDmYkmPrgbRM.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[fSFM5tsKzCAUyEVt.htm](pathfinder-bestiary-3-items/fSFM5tsKzCAUyEVt.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[FsndhUfvxvaK2vMV.htm](pathfinder-bestiary-3-items/FsndhUfvxvaK2vMV.htm)|Grab|Agarrado|modificada|
|[FTFBPXIVH7AJW2HL.htm](pathfinder-bestiary-3-items/FTFBPXIVH7AJW2HL.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[FtnpYBZenPEqBKZT.htm](pathfinder-bestiary-3-items/FtnpYBZenPEqBKZT.htm)|Vishkanyan Venom|Vishkanyan Venom|modificada|
|[FV9xFHPHkv8KNSub.htm](pathfinder-bestiary-3-items/FV9xFHPHkv8KNSub.htm)|Inspire Envoy|Inspire Envoy|modificada|
|[fv9ZIBX07mdzmfwt.htm](pathfinder-bestiary-3-items/fv9ZIBX07mdzmfwt.htm)|Change Shape|Change Shape|modificada|
|[fvbnbBzVNf3FpASj.htm](pathfinder-bestiary-3-items/fvbnbBzVNf3FpASj.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[FVNiKshCldd6k244.htm](pathfinder-bestiary-3-items/FVNiKshCldd6k244.htm)|Hand of Despair|Hand of Despair|modificada|
|[fvVvVEF0pK4ATbmC.htm](pathfinder-bestiary-3-items/fvVvVEF0pK4ATbmC.htm)|Luck Osmosis|Luck Osmosis|modificada|
|[FWr6Ex98OyC3DSds.htm](pathfinder-bestiary-3-items/FWr6Ex98OyC3DSds.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[fXjJYcC6HHWIClZe.htm](pathfinder-bestiary-3-items/fXjJYcC6HHWIClZe.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[fXKjy8wUG1Jgkyga.htm](pathfinder-bestiary-3-items/fXKjy8wUG1Jgkyga.htm)|Inspiration|Inspiración|modificada|
|[FYOsSxnsDAEzO8SZ.htm](pathfinder-bestiary-3-items/FYOsSxnsDAEzO8SZ.htm)|Claw|Garra|modificada|
|[fYpLZdqHoH2EpmhI.htm](pathfinder-bestiary-3-items/fYpLZdqHoH2EpmhI.htm)|Swift Steps|Pasos Rápidos|modificada|
|[FYQBhz6UyHtF4h3Q.htm](pathfinder-bestiary-3-items/FYQBhz6UyHtF4h3Q.htm)|Claws|Garras|modificada|
|[FyrrgVjaMyZCtRfk.htm](pathfinder-bestiary-3-items/FyrrgVjaMyZCtRfk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fysOeh3zCgDpX1IC.htm](pathfinder-bestiary-3-items/fysOeh3zCgDpX1IC.htm)|Antler|Antler|modificada|
|[Fz5eA42qzHRa526p.htm](pathfinder-bestiary-3-items/Fz5eA42qzHRa526p.htm)|Death Gasp|Death Gasp|modificada|
|[fzjCg0wFBhRYlKEh.htm](pathfinder-bestiary-3-items/fzjCg0wFBhRYlKEh.htm)|Dragon's Salvation|Dragon's Salvation|modificada|
|[fzkFHUIw06YZhQU3.htm](pathfinder-bestiary-3-items/fzkFHUIw06YZhQU3.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[fZUt506WBOAg0Mrm.htm](pathfinder-bestiary-3-items/fZUt506WBOAg0Mrm.htm)|Spearing Tail|Spearing Tail|modificada|
|[fzv99kNrktzJJd55.htm](pathfinder-bestiary-3-items/fzv99kNrktzJJd55.htm)|Overpowering Healing|Overpowering Healing|modificada|
|[g078MCPSCGCFoMNJ.htm](pathfinder-bestiary-3-items/g078MCPSCGCFoMNJ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[g2ws0uSei8FpMGsN.htm](pathfinder-bestiary-3-items/g2ws0uSei8FpMGsN.htm)|Negative Healing|Curación negativa|modificada|
|[g4EAOBnH3VEiBpbj.htm](pathfinder-bestiary-3-items/g4EAOBnH3VEiBpbj.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[G4lup29lJteGQEx7.htm](pathfinder-bestiary-3-items/G4lup29lJteGQEx7.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[g4OWJGRw7uHmBmrk.htm](pathfinder-bestiary-3-items/g4OWJGRw7uHmBmrk.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[g5jByzHTDqydOMis.htm](pathfinder-bestiary-3-items/g5jByzHTDqydOMis.htm)|Relentless|Relentless|modificada|
|[G5wnFqnf2owuuqKY.htm](pathfinder-bestiary-3-items/G5wnFqnf2owuuqKY.htm)|Bonded Vessel|Bonded Vessel|modificada|
|[g5zUhW3HyLCDaXR1.htm](pathfinder-bestiary-3-items/g5zUhW3HyLCDaXR1.htm)|Fist|Puño|modificada|
|[g6t9K95If99AfQfM.htm](pathfinder-bestiary-3-items/g6t9K95If99AfQfM.htm)|Change Shape|Change Shape|modificada|
|[g7u85rySCuCvovJ1.htm](pathfinder-bestiary-3-items/g7u85rySCuCvovJ1.htm)|Tail|Tail|modificada|
|[G9pbNjrUbv6UwBPm.htm](pathfinder-bestiary-3-items/G9pbNjrUbv6UwBPm.htm)|Feed on Quintessence|Alimentarse de Quintaesencia|modificada|
|[ga5JXiw8tZ2VnkLB.htm](pathfinder-bestiary-3-items/ga5JXiw8tZ2VnkLB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[gaAm3rT0oo2DoWFh.htm](pathfinder-bestiary-3-items/gaAm3rT0oo2DoWFh.htm)|Nymph's Beauty|Nymph's Beauty|modificada|
|[gacO7J9Bw3Sc7Hnf.htm](pathfinder-bestiary-3-items/gacO7J9Bw3Sc7Hnf.htm)|Hop On|Hop On|modificada|
|[gbeM9uQuXvgenm6R.htm](pathfinder-bestiary-3-items/gbeM9uQuXvgenm6R.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gbGFrdjx00klWwUk.htm](pathfinder-bestiary-3-items/gbGFrdjx00klWwUk.htm)|Coven|Coven|modificada|
|[GBOycFh1m1Y5Jb9h.htm](pathfinder-bestiary-3-items/GBOycFh1m1Y5Jb9h.htm)|Change Shape|Change Shape|modificada|
|[gbX1GThYkUe0eSAF.htm](pathfinder-bestiary-3-items/gbX1GThYkUe0eSAF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GC4dnaKqE0PPSLwc.htm](pathfinder-bestiary-3-items/GC4dnaKqE0PPSLwc.htm)|Blinding Spittle|Saliva cegadora|modificada|
|[gcmEV9uoqo5w9B3u.htm](pathfinder-bestiary-3-items/gcmEV9uoqo5w9B3u.htm)|Throw Rock|Arrojar roca|modificada|
|[gdbnGMdSRW0Nzev8.htm](pathfinder-bestiary-3-items/gdbnGMdSRW0Nzev8.htm)|Spit|Escupe|modificada|
|[GdnX1i87yKmvFsjB.htm](pathfinder-bestiary-3-items/GdnX1i87yKmvFsjB.htm)|Fume|Fume|modificada|
|[GDsko6VNw9V3q3Fn.htm](pathfinder-bestiary-3-items/GDsko6VNw9V3q3Fn.htm)|Nature's Chosen|Nature's Chosen|modificada|
|[GeAdyeLnGrb8jf9c.htm](pathfinder-bestiary-3-items/GeAdyeLnGrb8jf9c.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[geu1krmMeuI55Tx7.htm](pathfinder-bestiary-3-items/geu1krmMeuI55Tx7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[GFFvPm7EbrqTqDNh.htm](pathfinder-bestiary-3-items/GFFvPm7EbrqTqDNh.htm)|Borer|Borer|modificada|
|[GfHEx2NMpfuQjhkS.htm](pathfinder-bestiary-3-items/GfHEx2NMpfuQjhkS.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[GFv0unGRcJq78oa1.htm](pathfinder-bestiary-3-items/GFv0unGRcJq78oa1.htm)|Negative Healing|Curación negativa|modificada|
|[gGHq0sBBHPVuZhDJ.htm](pathfinder-bestiary-3-items/gGHq0sBBHPVuZhDJ.htm)|Constrict|Restringir|modificada|
|[gGpB7QLHdHm3LW5f.htm](pathfinder-bestiary-3-items/gGpB7QLHdHm3LW5f.htm)|Pufferfish Venom|Pufferfish Venom|modificada|
|[ggqzAen8OXMFadmR.htm](pathfinder-bestiary-3-items/ggqzAen8OXMFadmR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Gh301Rw6jmBr5P8s.htm](pathfinder-bestiary-3-items/Gh301Rw6jmBr5P8s.htm)|Putrid Vomit|Vómito Pútrido|modificada|
|[ghavKYYcFvQxKQ3n.htm](pathfinder-bestiary-3-items/ghavKYYcFvQxKQ3n.htm)|Font of Death|Fuente de la Muerte|modificada|
|[gHkt9ZPmla4aDS6c.htm](pathfinder-bestiary-3-items/gHkt9ZPmla4aDS6c.htm)|Telepathy|Telepatía|modificada|
|[GhM8SkeNK34K4KuM.htm](pathfinder-bestiary-3-items/GhM8SkeNK34K4KuM.htm)|Extend Neck|Extender Cuello|modificada|
|[gi7A2YM0065rtCEt.htm](pathfinder-bestiary-3-items/gi7A2YM0065rtCEt.htm)|Horn|Cuerno|modificada|
|[gIPLLT7gHYLZoTe5.htm](pathfinder-bestiary-3-items/gIPLLT7gHYLZoTe5.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[Gj7A4LTikw9f1cDC.htm](pathfinder-bestiary-3-items/Gj7A4LTikw9f1cDC.htm)|Jaws|Fauces|modificada|
|[gJ7hdDvMBTlevMyx.htm](pathfinder-bestiary-3-items/gJ7hdDvMBTlevMyx.htm)|Maw|Maw|modificada|
|[gk6ccJsfW8SnQ1XT.htm](pathfinder-bestiary-3-items/gk6ccJsfW8SnQ1XT.htm)|Limited Flight|Vuelo limitado|modificada|
|[gKE1dU3eNHT5VhmQ.htm](pathfinder-bestiary-3-items/gKE1dU3eNHT5VhmQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GkEh5ET2hVXw3FKO.htm](pathfinder-bestiary-3-items/GkEh5ET2hVXw3FKO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gKhFLhWvvDGY1Xbf.htm](pathfinder-bestiary-3-items/gKhFLhWvvDGY1Xbf.htm)|Trident|Trident|modificada|
|[GKPDyYkNMI6xU6fY.htm](pathfinder-bestiary-3-items/GKPDyYkNMI6xU6fY.htm)|Trample|Trample|modificada|
|[gKuOYzPHRi2J8prN.htm](pathfinder-bestiary-3-items/gKuOYzPHRi2J8prN.htm)|Woodland Stride|Paso forestal|modificada|
|[gKxdmvPJZYi5W7YT.htm](pathfinder-bestiary-3-items/gKxdmvPJZYi5W7YT.htm)|Vicious Strafe|Vicious Strafe|modificada|
|[gLEu7XWn7pQSVc6w.htm](pathfinder-bestiary-3-items/gLEu7XWn7pQSVc6w.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[gLK9FYslyozpAyWH.htm](pathfinder-bestiary-3-items/gLK9FYslyozpAyWH.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[gMnuejIYSlD0wrxg.htm](pathfinder-bestiary-3-items/gMnuejIYSlD0wrxg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gmOQkMoumZBsdfpn.htm](pathfinder-bestiary-3-items/gmOQkMoumZBsdfpn.htm)|Jaws|Fauces|modificada|
|[GNmpOlkmA2KqPmhK.htm](pathfinder-bestiary-3-items/GNmpOlkmA2KqPmhK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gNwlqfCtYRIjrH6e.htm](pathfinder-bestiary-3-items/gNwlqfCtYRIjrH6e.htm)|Fed by Metal|Alimentado por Metal|modificada|
|[go13nXjPXGhj1YCQ.htm](pathfinder-bestiary-3-items/go13nXjPXGhj1YCQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[GOF0Ud2jC782jqa5.htm](pathfinder-bestiary-3-items/GOF0Ud2jC782jqa5.htm)|Lore Master|Lore Master|modificada|
|[gOiaDjo5WK8ESJHN.htm](pathfinder-bestiary-3-items/gOiaDjo5WK8ESJHN.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[gokPql4rMjZsUmFa.htm](pathfinder-bestiary-3-items/gokPql4rMjZsUmFa.htm)|Claw|Garra|modificada|
|[GOr0VDbwyouZE0as.htm](pathfinder-bestiary-3-items/GOr0VDbwyouZE0as.htm)|Guardian's Aegis|Guardian's Aegis|modificada|
|[GosHB6QvTUOTDIVs.htm](pathfinder-bestiary-3-items/GosHB6QvTUOTDIVs.htm)|Hurl Corpse|Hurl Corpse|modificada|
|[gPaWa8Zkt9jBAg2K.htm](pathfinder-bestiary-3-items/gPaWa8Zkt9jBAg2K.htm)|Forge Jewelry|Forjar Joyas|modificada|
|[GPeTASJke20Pg69M.htm](pathfinder-bestiary-3-items/GPeTASJke20Pg69M.htm)|Negative Healing|Curación negativa|modificada|
|[GpjM5EevUR9J0dQv.htm](pathfinder-bestiary-3-items/GpjM5EevUR9J0dQv.htm)|Viscous Choke|Viscous Choke|modificada|
|[gpsZOiwpAEi2TKTL.htm](pathfinder-bestiary-3-items/gpsZOiwpAEi2TKTL.htm)|Despoiler|Despoiler|modificada|
|[GPxeevqqNhveOB8u.htm](pathfinder-bestiary-3-items/GPxeevqqNhveOB8u.htm)|Snowstep|Snowstep|modificada|
|[GpYSF4Qy9tOtd9Z4.htm](pathfinder-bestiary-3-items/GpYSF4Qy9tOtd9Z4.htm)|Slice and Dice|Slice and Dice|modificada|
|[GPZIIPDmUwYRcaYF.htm](pathfinder-bestiary-3-items/GPZIIPDmUwYRcaYF.htm)|Grab|Agarrado|modificada|
|[gQmDkG34wRgRZDty.htm](pathfinder-bestiary-3-items/gQmDkG34wRgRZDty.htm)|Taste Anger (Imprecise) 1 mile|Sabor Ira (Impreciso) 1 milla|modificada|
|[GqtkVnERy0SmBtKA.htm](pathfinder-bestiary-3-items/GqtkVnERy0SmBtKA.htm)|Dart|Dardo|modificada|
|[gr15jMdsKQApNTr1.htm](pathfinder-bestiary-3-items/gr15jMdsKQApNTr1.htm)|Hatchet|Hacha|modificada|
|[grdS9mKHqWcDMpuH.htm](pathfinder-bestiary-3-items/grdS9mKHqWcDMpuH.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[GrP9ijoNzD5cbT4X.htm](pathfinder-bestiary-3-items/GrP9ijoNzD5cbT4X.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[GRQsdgoPIcgExLRe.htm](pathfinder-bestiary-3-items/GRQsdgoPIcgExLRe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GS25DV8nC7hEzR8I.htm](pathfinder-bestiary-3-items/GS25DV8nC7hEzR8I.htm)|Easy to Call|Fácil de Llamar|modificada|
|[gsORJTMYFRzNjUAn.htm](pathfinder-bestiary-3-items/gsORJTMYFRzNjUAn.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Gt37cWDUiwwAkjyI.htm](pathfinder-bestiary-3-items/Gt37cWDUiwwAkjyI.htm)|Troop Movement|Movimiento de tropas|modificada|
|[gTGE8w5NItEtmFuG.htm](pathfinder-bestiary-3-items/gTGE8w5NItEtmFuG.htm)|Constant Spells|Constant Spells|modificada|
|[gtLlO4bF2NESErTl.htm](pathfinder-bestiary-3-items/gtLlO4bF2NESErTl.htm)|Boat Breaker|Boat Breaker|modificada|
|[gU4MP6UZdedQtspk.htm](pathfinder-bestiary-3-items/gU4MP6UZdedQtspk.htm)|Frightful Presence|Frightful Presence|modificada|
|[Gu762rdO8uTT9ixQ.htm](pathfinder-bestiary-3-items/Gu762rdO8uTT9ixQ.htm)|Attack of Opportunity (Stinger Only)|Ataque de oportunidad (sólo aguijón).|modificada|
|[GugfZAMFY5fRgjN3.htm](pathfinder-bestiary-3-items/GugfZAMFY5fRgjN3.htm)|Longbow|Arco largo|modificada|
|[guqcEGLb6OHHFXDW.htm](pathfinder-bestiary-3-items/guqcEGLb6OHHFXDW.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[GUTBHqMY0oGnnUx0.htm](pathfinder-bestiary-3-items/GUTBHqMY0oGnnUx0.htm)|Plagued Coffin Restoration|Restablecimiento de ataúd plagado|modificada|
|[GVOl6lIUVxSW5gZK.htm](pathfinder-bestiary-3-items/GVOl6lIUVxSW5gZK.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[GWdF6u5sHvS26nIZ.htm](pathfinder-bestiary-3-items/GWdF6u5sHvS26nIZ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[gWdFg8nl9k3xaERt.htm](pathfinder-bestiary-3-items/gWdFg8nl9k3xaERt.htm)|Shove into Stone|Empujar dentro de la piedra|modificada|
|[GWKCP70pS87RXrFP.htm](pathfinder-bestiary-3-items/GWKCP70pS87RXrFP.htm)|Aura of Disquietude|Aura de Inquietud|modificada|
|[Gwusjl0kDQRYPaID.htm](pathfinder-bestiary-3-items/Gwusjl0kDQRYPaID.htm)|Astral Shock|Electrizante Astral|modificada|
|[GWx7ppzGBq5TVLbL.htm](pathfinder-bestiary-3-items/GWx7ppzGBq5TVLbL.htm)|Constant Spells|Constant Spells|modificada|
|[GWXJlKCvo60X8rp8.htm](pathfinder-bestiary-3-items/GWXJlKCvo60X8rp8.htm)|Hundred-Handed Whirlwind|Hundred-Handed Whirlwind|modificada|
|[gxBpGZrMdteLCa6c.htm](pathfinder-bestiary-3-items/gxBpGZrMdteLCa6c.htm)|Hand|Mano|modificada|
|[Gxd48LW0au3UyXRn.htm](pathfinder-bestiary-3-items/Gxd48LW0au3UyXRn.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[gxkIdHdPIrnUNavf.htm](pathfinder-bestiary-3-items/gxkIdHdPIrnUNavf.htm)|Transcribe|Transcribe|modificada|
|[gXT2WwNt19Jqn8Lo.htm](pathfinder-bestiary-3-items/gXT2WwNt19Jqn8Lo.htm)|Proficient Poisoner|Proficient Poisoner|modificada|
|[GyUxfyjtgA7EdiPY.htm](pathfinder-bestiary-3-items/GyUxfyjtgA7EdiPY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gZ11aiTmjuFfS0D9.htm](pathfinder-bestiary-3-items/gZ11aiTmjuFfS0D9.htm)|Swarm Mind|Swarm Mind|modificada|
|[gzbNuojJ54XXTTe3.htm](pathfinder-bestiary-3-items/gzbNuojJ54XXTTe3.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[gzGzxCPvcdZu8f4O.htm](pathfinder-bestiary-3-items/gzGzxCPvcdZu8f4O.htm)|Constrict|Restringir|modificada|
|[GZIzUrSeCXXcCvhZ.htm](pathfinder-bestiary-3-items/GZIzUrSeCXXcCvhZ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gZVSvNNNodOSP8Js.htm](pathfinder-bestiary-3-items/gZVSvNNNodOSP8Js.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gzZaUElRwVXoDLly.htm](pathfinder-bestiary-3-items/gzZaUElRwVXoDLly.htm)|Sagebane|Sagebane|modificada|
|[H00CJvnkXb56atN1.htm](pathfinder-bestiary-3-items/H00CJvnkXb56atN1.htm)|Jaws|Fauces|modificada|
|[h1ZAFdNFnrnrUzal.htm](pathfinder-bestiary-3-items/h1ZAFdNFnrnrUzal.htm)|Wavesense (Imprecise) 120 feet|Sentir ondulaciones (Impreciso) 120 pies|modificada|
|[H2G6L0SJ45uHxDiE.htm](pathfinder-bestiary-3-items/H2G6L0SJ45uHxDiE.htm)|Jaws|Fauces|modificada|
|[h2l4ytlaIH7Elzxp.htm](pathfinder-bestiary-3-items/h2l4ytlaIH7Elzxp.htm)|Fist|Puño|modificada|
|[h2v7VpyXTPjTzVX1.htm](pathfinder-bestiary-3-items/h2v7VpyXTPjTzVX1.htm)|Smoke Vision|Visión de Humo|modificada|
|[h3GtcNC8KADgxKzm.htm](pathfinder-bestiary-3-items/h3GtcNC8KADgxKzm.htm)|Tearing Clutch|Tearing Clutch|modificada|
|[H3pa17KySBkdeqib.htm](pathfinder-bestiary-3-items/H3pa17KySBkdeqib.htm)|Moonlight's Kiss|Moonlight's Kiss|modificada|
|[H4TL62mthkiNmZPt.htm](pathfinder-bestiary-3-items/H4TL62mthkiNmZPt.htm)|Jaws|Fauces|modificada|
|[h4X51BYbgu9tzFnJ.htm](pathfinder-bestiary-3-items/h4X51BYbgu9tzFnJ.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[h4zO0kK45DmIdXa1.htm](pathfinder-bestiary-3-items/h4zO0kK45DmIdXa1.htm)|Grab|Agarrado|modificada|
|[H6bcCPSTnAt5zBu8.htm](pathfinder-bestiary-3-items/H6bcCPSTnAt5zBu8.htm)|Felling Assault|Asalto de tala|modificada|
|[H6E4xmp7lHmRZXO2.htm](pathfinder-bestiary-3-items/H6E4xmp7lHmRZXO2.htm)|Hadalic Presence|Presencia Hadalic|modificada|
|[h6z492dnOVTr6NoI.htm](pathfinder-bestiary-3-items/h6z492dnOVTr6NoI.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[h7VjKWk9XbmBEL2M.htm](pathfinder-bestiary-3-items/h7VjKWk9XbmBEL2M.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[h8caQJMnKj4F22zZ.htm](pathfinder-bestiary-3-items/h8caQJMnKj4F22zZ.htm)|Frenzied Slashes|Frenzied Slashes|modificada|
|[h8iiN0oJfHoKkkw6.htm](pathfinder-bestiary-3-items/h8iiN0oJfHoKkkw6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[h8Phu8ErJOaWCuUU.htm](pathfinder-bestiary-3-items/h8Phu8ErJOaWCuUU.htm)|Pincer|Pinza|modificada|
|[h8rEFtxwQFlx0MIz.htm](pathfinder-bestiary-3-items/h8rEFtxwQFlx0MIz.htm)|Venomous Fangs|Venomous Fangs|modificada|
|[h8UYIVYJvZxviPCt.htm](pathfinder-bestiary-3-items/h8UYIVYJvZxviPCt.htm)|Web|Telara|modificada|
|[H8WZz3DKIs1SCcFr.htm](pathfinder-bestiary-3-items/H8WZz3DKIs1SCcFr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H9YGbRcmWQV6mrYy.htm](pathfinder-bestiary-3-items/H9YGbRcmWQV6mrYy.htm)|Grab|Agarrado|modificada|
|[HAbopgX5BTCYokYc.htm](pathfinder-bestiary-3-items/HAbopgX5BTCYokYc.htm)|Regenerative Blood|Regenerative Blood|modificada|
|[HayFvnvEzF4yeL8e.htm](pathfinder-bestiary-3-items/HayFvnvEzF4yeL8e.htm)|Cleanly Vulnerability|Vulnerabilidad Cleanly|modificada|
|[hbJLdmKm20p84vtq.htm](pathfinder-bestiary-3-items/hbJLdmKm20p84vtq.htm)|Hurled Debris|Hurled Debris|modificada|
|[HbNu5OQ0rlq7Yc5T.htm](pathfinder-bestiary-3-items/HbNu5OQ0rlq7Yc5T.htm)|Surface Skimmer|Skimmer de superficie|modificada|
|[HBWRwsQLwNl5P9GZ.htm](pathfinder-bestiary-3-items/HBWRwsQLwNl5P9GZ.htm)|Painful Bite|Muerdemuerde Doloroso|modificada|
|[hCBIDqLoaomtVumJ.htm](pathfinder-bestiary-3-items/hCBIDqLoaomtVumJ.htm)|Tremorsense (Imprecise) within their entire bound yard|Sentido del Temblor (Impreciso) dentro de todo su patio delimitado.|modificada|
|[hcf7RGBrhHKjGcG4.htm](pathfinder-bestiary-3-items/hcf7RGBrhHKjGcG4.htm)|Camel Spit|Camel Spit|modificada|
|[hCqIQRbX94na2LJI.htm](pathfinder-bestiary-3-items/hCqIQRbX94na2LJI.htm)|Spit|Escupe|modificada|
|[hcWQWEF9ku17m4ue.htm](pathfinder-bestiary-3-items/hcWQWEF9ku17m4ue.htm)|Claw|Garra|modificada|
|[HD1JVNGcHKCVtQRP.htm](pathfinder-bestiary-3-items/HD1JVNGcHKCVtQRP.htm)|Fed by Earth|Alimentado por la Tierra|modificada|
|[hD5zZmaH7szmKu7h.htm](pathfinder-bestiary-3-items/hD5zZmaH7szmKu7h.htm)|Claw|Garra|modificada|
|[hD8oXqV8Wz5DBv2c.htm](pathfinder-bestiary-3-items/hD8oXqV8Wz5DBv2c.htm)|Claw|Garra|modificada|
|[HDgHNFoKqVXHIcGb.htm](pathfinder-bestiary-3-items/HDgHNFoKqVXHIcGb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hDrYmsPyJApRYUgu.htm](pathfinder-bestiary-3-items/hDrYmsPyJApRYUgu.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[He0zYHgzyYLryoo4.htm](pathfinder-bestiary-3-items/He0zYHgzyYLryoo4.htm)|Claw|Garra|modificada|
|[He1e2JVLL4LLFCXX.htm](pathfinder-bestiary-3-items/He1e2JVLL4LLFCXX.htm)|Protective Pinch|Pinza de protección|modificada|
|[HeSYt5jeXQwluP8x.htm](pathfinder-bestiary-3-items/HeSYt5jeXQwluP8x.htm)|Fangs|Colmillos|modificada|
|[hfaAY55AoAQW59YR.htm](pathfinder-bestiary-3-items/hfaAY55AoAQW59YR.htm)|Inflict Misery|Infligir Miseria|modificada|
|[hfbHiFnMOX2vWG3b.htm](pathfinder-bestiary-3-items/hfbHiFnMOX2vWG3b.htm)|Engulf|Envolver|modificada|
|[hfMabMzyolY9p2Gw.htm](pathfinder-bestiary-3-items/hfMabMzyolY9p2Gw.htm)|Euphoric Spark|Chispa Eufórica|modificada|
|[hFX9SvNVh26s4No7.htm](pathfinder-bestiary-3-items/hFX9SvNVh26s4No7.htm)|Fade Away|Fade Away|modificada|
|[HHDECazmKhMUWZZF.htm](pathfinder-bestiary-3-items/HHDECazmKhMUWZZF.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hHLSrHMm2augW6G7.htm](pathfinder-bestiary-3-items/hHLSrHMm2augW6G7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[HHS4CPEzSJeRK2TJ.htm](pathfinder-bestiary-3-items/HHS4CPEzSJeRK2TJ.htm)|Drain Life|Drenar Vida|modificada|
|[Hi0AFeSVBBw7vuPF.htm](pathfinder-bestiary-3-items/Hi0AFeSVBBw7vuPF.htm)|Suction|Succión|modificada|
|[HiELCWTyhJvQaGUl.htm](pathfinder-bestiary-3-items/HiELCWTyhJvQaGUl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[HiTMvvga2hoAWC9Y.htm](pathfinder-bestiary-3-items/HiTMvvga2hoAWC9Y.htm)|Snatch Between|Arrebatar Entre|modificada|
|[HItnjZpl8JZa8gGc.htm](pathfinder-bestiary-3-items/HItnjZpl8JZa8gGc.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Hj7vX0JdYOvuiOk2.htm](pathfinder-bestiary-3-items/Hj7vX0JdYOvuiOk2.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[hJMhsrU4b1Xsyslz.htm](pathfinder-bestiary-3-items/hJMhsrU4b1Xsyslz.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[HJT5NvgiIrqOYp97.htm](pathfinder-bestiary-3-items/HJT5NvgiIrqOYp97.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[hJYdbtet0tqbLviV.htm](pathfinder-bestiary-3-items/hJYdbtet0tqbLviV.htm)|Fed by Metal|Alimentado por Metal|modificada|
|[HKJDIUhv4yxNRcOz.htm](pathfinder-bestiary-3-items/HKJDIUhv4yxNRcOz.htm)|Grab|Agarrado|modificada|
|[hKMqAgh48onTAhpH.htm](pathfinder-bestiary-3-items/hKMqAgh48onTAhpH.htm)|Clinch Victory|Clinch Victory|modificada|
|[Hl6TVTFqmE0SOKoE.htm](pathfinder-bestiary-3-items/Hl6TVTFqmE0SOKoE.htm)|Storm of Battle|Tormenta de Batalla|modificada|
|[hljpYgDUm3hDZfDm.htm](pathfinder-bestiary-3-items/hljpYgDUm3hDZfDm.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[hlLjyEJFBxuPfIsH.htm](pathfinder-bestiary-3-items/hlLjyEJFBxuPfIsH.htm)|Spectral Claw|Garra espectral|modificada|
|[hNp02EzjCutL6cMN.htm](pathfinder-bestiary-3-items/hNp02EzjCutL6cMN.htm)|Jaws|Fauces|modificada|
|[hoeCSNdGULGrdSYX.htm](pathfinder-bestiary-3-items/hoeCSNdGULGrdSYX.htm)|Shield Block|Bloquear con escudo|modificada|
|[HOEKikyuy4nwWSmn.htm](pathfinder-bestiary-3-items/HOEKikyuy4nwWSmn.htm)|Inkstain|Inkstain|modificada|
|[hozqm7eiBZchD7T3.htm](pathfinder-bestiary-3-items/hozqm7eiBZchD7T3.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[hP7NqLHuZTlhjPtO.htm](pathfinder-bestiary-3-items/hP7NqLHuZTlhjPtO.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hpbnFFNrcbDIDwkP.htm](pathfinder-bestiary-3-items/hpbnFFNrcbDIDwkP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[hPBPkTGmnJVgZ22X.htm](pathfinder-bestiary-3-items/hPBPkTGmnJVgZ22X.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[HPHMTMivSSxm6A9g.htm](pathfinder-bestiary-3-items/HPHMTMivSSxm6A9g.htm)|Firebolt|Firebolt|modificada|
|[hT2TFquy1W3hmWXW.htm](pathfinder-bestiary-3-items/hT2TFquy1W3hmWXW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[HTapFz8e3dL6LNJc.htm](pathfinder-bestiary-3-items/HTapFz8e3dL6LNJc.htm)|Smoke Vision|Visión de Humo|modificada|
|[hteKQpvnrGLuVGSS.htm](pathfinder-bestiary-3-items/hteKQpvnrGLuVGSS.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[HTL4UczGmmOt7HUk.htm](pathfinder-bestiary-3-items/HTL4UczGmmOt7HUk.htm)|Persuasive Rebuttal|Persuasive Rebuttal|modificada|
|[HTp6N4B1ohShQdxQ.htm](pathfinder-bestiary-3-items/HTp6N4B1ohShQdxQ.htm)|Spray Musk|Spray Musk|modificada|
|[hUmX5vToV3dXSRRK.htm](pathfinder-bestiary-3-items/hUmX5vToV3dXSRRK.htm)|Change Shape|Change Shape|modificada|
|[Hv7Blt8YRg7FuNu6.htm](pathfinder-bestiary-3-items/Hv7Blt8YRg7FuNu6.htm)|Deflect Aggression|Desviar la agresión|modificada|
|[HvbA2JrPJX04mXf4.htm](pathfinder-bestiary-3-items/HvbA2JrPJX04mXf4.htm)|Smoke Vision|Visión de Humo|modificada|
|[HVELxWKZ2of1Q0F1.htm](pathfinder-bestiary-3-items/HVELxWKZ2of1Q0F1.htm)|Suspend Soul|Suspender Alma|modificada|
|[HvrOYSndJdFfyuSN.htm](pathfinder-bestiary-3-items/HvrOYSndJdFfyuSN.htm)|Towering Stance|Posición de torre.|modificada|
|[hVTC4rrt8IWJq51F.htm](pathfinder-bestiary-3-items/hVTC4rrt8IWJq51F.htm)|Green Tongue|Lengua verde|modificada|
|[HvYM5Z1ubUt120XB.htm](pathfinder-bestiary-3-items/HvYM5Z1ubUt120XB.htm)|Claw|Garra|modificada|
|[HwqKHt0yeIDL3Jjm.htm](pathfinder-bestiary-3-items/HwqKHt0yeIDL3Jjm.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[hww0iSmzvgv1CuVE.htm](pathfinder-bestiary-3-items/hww0iSmzvgv1CuVE.htm)|Sunset Ray|Sunset Ray|modificada|
|[HX5VU8Xvb5yPZ2aH.htm](pathfinder-bestiary-3-items/HX5VU8Xvb5yPZ2aH.htm)|Discorporate|Discorporate|modificada|
|[HX6LFkk4karuzyse.htm](pathfinder-bestiary-3-items/HX6LFkk4karuzyse.htm)|Constrict|Restringir|modificada|
|[hXhEkQgvn1PZadRa.htm](pathfinder-bestiary-3-items/hXhEkQgvn1PZadRa.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hyj6N5o5oSyNgbJd.htm](pathfinder-bestiary-3-items/hyj6N5o5oSyNgbJd.htm)|Vine|Vid|modificada|
|[hykuv0gwwDzF4GZW.htm](pathfinder-bestiary-3-items/hykuv0gwwDzF4GZW.htm)|Bonded Strike|Golpe Bonded|modificada|
|[HYkzxImoz15eP5c0.htm](pathfinder-bestiary-3-items/HYkzxImoz15eP5c0.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[HYlZSPGidgo056wQ.htm](pathfinder-bestiary-3-items/HYlZSPGidgo056wQ.htm)|Constant Spells|Constant Spells|modificada|
|[hYXNxWZ2lgrY2oNX.htm](pathfinder-bestiary-3-items/hYXNxWZ2lgrY2oNX.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[hzAD0q71nN1lkiVO.htm](pathfinder-bestiary-3-items/hzAD0q71nN1lkiVO.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[hZqDsZrM19xB27pn.htm](pathfinder-bestiary-3-items/hZqDsZrM19xB27pn.htm)|Blessed Strikes|Golpes Bendecidos|modificada|
|[i0CgRjDNx28m8d7r.htm](pathfinder-bestiary-3-items/i0CgRjDNx28m8d7r.htm)|Hundred-Dimension Grasp|Hundred-Dimension Grasp|modificada|
|[i0GVDRjE4Mf3fPfW.htm](pathfinder-bestiary-3-items/i0GVDRjE4Mf3fPfW.htm)|Spear|Lanza|modificada|
|[i1KyfP7F6bj0FZS5.htm](pathfinder-bestiary-3-items/i1KyfP7F6bj0FZS5.htm)|Constrict|Restringir|modificada|
|[I1xWawDf3yw69pnw.htm](pathfinder-bestiary-3-items/I1xWawDf3yw69pnw.htm)|Desert Stride|Zancada del Desierto|modificada|
|[I239EJXsU4ME0R50.htm](pathfinder-bestiary-3-items/I239EJXsU4ME0R50.htm)|Shortsword|Espada corta|modificada|
|[i2o1AoEa6G6aYbNZ.htm](pathfinder-bestiary-3-items/i2o1AoEa6G6aYbNZ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[i2xCkOeImkc5M5zz.htm](pathfinder-bestiary-3-items/i2xCkOeImkc5M5zz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[i2YbHX9ZBEKMZMOv.htm](pathfinder-bestiary-3-items/i2YbHX9ZBEKMZMOv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[I3LHE7CKMGHBy5aq.htm](pathfinder-bestiary-3-items/I3LHE7CKMGHBy5aq.htm)|Call to Arms|Llamada a las Armas|modificada|
|[i49FXrRnhIZz63Ee.htm](pathfinder-bestiary-3-items/i49FXrRnhIZz63Ee.htm)|Recognize Hero|Recognize Hero|modificada|
|[i4jbUrRlPXHHy15s.htm](pathfinder-bestiary-3-items/i4jbUrRlPXHHy15s.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[I5FrBDYXau3Iuild.htm](pathfinder-bestiary-3-items/I5FrBDYXau3Iuild.htm)|Telepathy 120 feet|Telepatía 120 pies.|modificada|
|[I6ATQo2zBIuQQinX.htm](pathfinder-bestiary-3-items/I6ATQo2zBIuQQinX.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[I7lMAhjCqMyxPu6y.htm](pathfinder-bestiary-3-items/I7lMAhjCqMyxPu6y.htm)|Inexorable|Inexorable|modificada|
|[i7YE4zH1GBMET2Uu.htm](pathfinder-bestiary-3-items/i7YE4zH1GBMET2Uu.htm)|Change Shape|Change Shape|modificada|
|[i8I92PVcpFQf4HBz.htm](pathfinder-bestiary-3-items/i8I92PVcpFQf4HBz.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[I9tVwoP0Z0dV0r5j.htm](pathfinder-bestiary-3-items/I9tVwoP0Z0dV0r5j.htm)|Amulet Relic|Reliquia Amuleto|modificada|
|[ia7nfgIs3ERJytZy.htm](pathfinder-bestiary-3-items/ia7nfgIs3ERJytZy.htm)|Persistence of Memory|Persistencia de la Memoria|modificada|
|[IaFXrGv80HlNVl19.htm](pathfinder-bestiary-3-items/IaFXrGv80HlNVl19.htm)|Telepathy 150 feet|Telepatía 150 pies.|modificada|
|[IAO39ERjCHDfcZlr.htm](pathfinder-bestiary-3-items/IAO39ERjCHDfcZlr.htm)|Hoof|Hoof|modificada|
|[IBxaJ1PsBPxvnN70.htm](pathfinder-bestiary-3-items/IBxaJ1PsBPxvnN70.htm)|Droning Distraction|Droning Distracción|modificada|
|[iC8kTbasCU08DFOc.htm](pathfinder-bestiary-3-items/iC8kTbasCU08DFOc.htm)|Loathing Garotte|Loathing Garotte|modificada|
|[ICHmpsSeRPd2QliC.htm](pathfinder-bestiary-3-items/ICHmpsSeRPd2QliC.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[iCM4V1WXNcTslhi7.htm](pathfinder-bestiary-3-items/iCM4V1WXNcTslhi7.htm)|Coven|Coven|modificada|
|[ICqXmQ1I3WhSNBF7.htm](pathfinder-bestiary-3-items/ICqXmQ1I3WhSNBF7.htm)|Green Rituals|Rituales Verdes|modificada|
|[icR13DUCw64iVgQ2.htm](pathfinder-bestiary-3-items/icR13DUCw64iVgQ2.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[icUrUnsfgvWuac89.htm](pathfinder-bestiary-3-items/icUrUnsfgvWuac89.htm)|Tail|Tail|modificada|
|[IDk5Kln9p90yl0SF.htm](pathfinder-bestiary-3-items/IDk5Kln9p90yl0SF.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[Idu5EyPugITPjmRq.htm](pathfinder-bestiary-3-items/Idu5EyPugITPjmRq.htm)|Paralysis|Parálisis|modificada|
|[IE75LW4gCgc5TOPJ.htm](pathfinder-bestiary-3-items/IE75LW4gCgc5TOPJ.htm)|Designate Master|Designar Maestro|modificada|
|[iewmwrowaGzoIyTr.htm](pathfinder-bestiary-3-items/iewmwrowaGzoIyTr.htm)|Woodland Stride|Paso forestal|modificada|
|[if2CB9IchjxMbiab.htm](pathfinder-bestiary-3-items/if2CB9IchjxMbiab.htm)|Coven Spells|Coven Spells|modificada|
|[IFBwgiXBfRM7tF8Q.htm](pathfinder-bestiary-3-items/IFBwgiXBfRM7tF8Q.htm)|Eel Dart|Dardo de anguila|modificada|
|[IfgRYpjy1kMxYZ7N.htm](pathfinder-bestiary-3-items/IfgRYpjy1kMxYZ7N.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[iFhR8LsLMNJ2v6O0.htm](pathfinder-bestiary-3-items/iFhR8LsLMNJ2v6O0.htm)|Absorb Magic|Absorber Magia|modificada|
|[iFKv7PFNKtSVy9XA.htm](pathfinder-bestiary-3-items/iFKv7PFNKtSVy9XA.htm)|Slow|Lentificado/a|modificada|
|[IfQ2Ql0FDoKs4CXh.htm](pathfinder-bestiary-3-items/IfQ2Ql0FDoKs4CXh.htm)|Fist|Puño|modificada|
|[ifv7iRcnxapPCh5S.htm](pathfinder-bestiary-3-items/ifv7iRcnxapPCh5S.htm)|Grab|Agarrado|modificada|
|[Ig0zFPa6ddSOqkXY.htm](pathfinder-bestiary-3-items/Ig0zFPa6ddSOqkXY.htm)|Javelin|Javelin|modificada|
|[iGBJDGQHVXz9hpgE.htm](pathfinder-bestiary-3-items/iGBJDGQHVXz9hpgE.htm)|Constant Spells|Constant Spells|modificada|
|[igF8SYccIFVckGta.htm](pathfinder-bestiary-3-items/igF8SYccIFVckGta.htm)|Master of the Granary|Maestro del Granero|modificada|
|[IgNXM2vcFy0D1uCF.htm](pathfinder-bestiary-3-items/IgNXM2vcFy0D1uCF.htm)|Cudgel|Garrote|modificada|
|[Igt62T1pB0clDkfb.htm](pathfinder-bestiary-3-items/Igt62T1pB0clDkfb.htm)|All-Around Vision|All-Around Vision|modificada|
|[iGx7eQKe0LJXzUAo.htm](pathfinder-bestiary-3-items/iGx7eQKe0LJXzUAo.htm)|Breath Weapon|Breath Weapon|modificada|
|[igxHjmUJWlr6ULG7.htm](pathfinder-bestiary-3-items/igxHjmUJWlr6ULG7.htm)|Head Spin|Giro de cabeza|modificada|
|[IH1oaRD65ApAyt5k.htm](pathfinder-bestiary-3-items/IH1oaRD65ApAyt5k.htm)|Longsword|Longsword|modificada|
|[iHJPeHHphrdXidbV.htm](pathfinder-bestiary-3-items/iHJPeHHphrdXidbV.htm)|Enraged Home (Slashing)|Casa Enfurecida (Slashing)|modificada|
|[iHPmzxNLB3Yqy1nG.htm](pathfinder-bestiary-3-items/iHPmzxNLB3Yqy1nG.htm)|Frightful Presence|Frightful Presence|modificada|
|[IhW67AKfQIjSFjgx.htm](pathfinder-bestiary-3-items/IhW67AKfQIjSFjgx.htm)|Elegy of the Faithless|Elegy of the Faithless|modificada|
|[iIIyNY3zJBmroqzM.htm](pathfinder-bestiary-3-items/iIIyNY3zJBmroqzM.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[iIkQe60YaPrjs5Wa.htm](pathfinder-bestiary-3-items/iIkQe60YaPrjs5Wa.htm)|Claw|Garra|modificada|
|[iJaSMrCi07AzcK4L.htm](pathfinder-bestiary-3-items/iJaSMrCi07AzcK4L.htm)|Share Defenses|Compartir Defensas|modificada|
|[Ijep1LR2jAGG5Xqz.htm](pathfinder-bestiary-3-items/Ijep1LR2jAGG5Xqz.htm)|Negative Healing|Curación negativa|modificada|
|[IjHEwnv7xjIRPpYZ.htm](pathfinder-bestiary-3-items/IjHEwnv7xjIRPpYZ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[IjkPvQ204dD2FWaQ.htm](pathfinder-bestiary-3-items/IjkPvQ204dD2FWaQ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ijtDf4mbj5lb5bvM.htm](pathfinder-bestiary-3-items/ijtDf4mbj5lb5bvM.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[iJuVRVeuCbiPmn0M.htm](pathfinder-bestiary-3-items/iJuVRVeuCbiPmn0M.htm)|Grab|Agarrado|modificada|
|[iK81tvrAddbR20xS.htm](pathfinder-bestiary-3-items/iK81tvrAddbR20xS.htm)|Pincer|Pinza|modificada|
|[IKBRGKTG4o0biJz9.htm](pathfinder-bestiary-3-items/IKBRGKTG4o0biJz9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[iKR8M3h9wCtVtDTe.htm](pathfinder-bestiary-3-items/iKR8M3h9wCtVtDTe.htm)|Swing from the Saddle|Columpio de la silla de montar|modificada|
|[ikvqvxMtrtqOwbWu.htm](pathfinder-bestiary-3-items/ikvqvxMtrtqOwbWu.htm)|Seize Prayer|Seize Prayer|modificada|
|[Il0NCYZxE3i4U1ud.htm](pathfinder-bestiary-3-items/Il0NCYZxE3i4U1ud.htm)|Echoblade|Echoblade|modificada|
|[iLC1r5LnzDRvTshg.htm](pathfinder-bestiary-3-items/iLC1r5LnzDRvTshg.htm)|Banished from the Ground|Destierro de la tierra|modificada|
|[ILDPyfeR9ijXko3B.htm](pathfinder-bestiary-3-items/ILDPyfeR9ijXko3B.htm)|Jaws|Fauces|modificada|
|[IlJzVorvcYKLmyZM.htm](pathfinder-bestiary-3-items/IlJzVorvcYKLmyZM.htm)|Backdrop|Telón de fondo|modificada|
|[ILOl1dY50IaR6WPN.htm](pathfinder-bestiary-3-items/ILOl1dY50IaR6WPN.htm)|Liquefy|Licuar|modificada|
|[IO9iVIVpEfBjrcmw.htm](pathfinder-bestiary-3-items/IO9iVIVpEfBjrcmw.htm)|Divine Dispelling|Disipación divina|modificada|
|[IOBHBhN2gfM5QQHO.htm](pathfinder-bestiary-3-items/IOBHBhN2gfM5QQHO.htm)|Greater Constrict|Mayor Restricción|modificada|
|[iommO8SU4yNamwC1.htm](pathfinder-bestiary-3-items/iommO8SU4yNamwC1.htm)|Focus Gaze|Centrar mirada|modificada|
|[iov5t4k8TcxeShEp.htm](pathfinder-bestiary-3-items/iov5t4k8TcxeShEp.htm)|Shortbow|Arco corto|modificada|
|[IP7OFv62ex3QvHzS.htm](pathfinder-bestiary-3-items/IP7OFv62ex3QvHzS.htm)|Knockdown|Derribo|modificada|
|[IpaUvWrHaznmdOGb.htm](pathfinder-bestiary-3-items/IpaUvWrHaznmdOGb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iPdoOjHPIU1Zhkvd.htm](pathfinder-bestiary-3-items/iPdoOjHPIU1Zhkvd.htm)|Stepping Decoy|Señuelo de Paso|modificada|
|[IPI8W2nuWEsuk2ti.htm](pathfinder-bestiary-3-items/IPI8W2nuWEsuk2ti.htm)|Despairing Weep|Despairing Weep|modificada|
|[iq5hF08qzTQTeTBD.htm](pathfinder-bestiary-3-items/iq5hF08qzTQTeTBD.htm)|Trample|Trample|modificada|
|[iQBwvNjlgULNrpOy.htm](pathfinder-bestiary-3-items/iQBwvNjlgULNrpOy.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[iQNybW2FjtzVHCOS.htm](pathfinder-bestiary-3-items/iQNybW2FjtzVHCOS.htm)|Self-Resurrection|Autorresurrección|modificada|
|[iQvaTMNbSTahgvnU.htm](pathfinder-bestiary-3-items/iQvaTMNbSTahgvnU.htm)|Foot|Pie|modificada|
|[iraYrEvANESYqSYz.htm](pathfinder-bestiary-3-items/iraYrEvANESYqSYz.htm)|Antler|Antler|modificada|
|[IRdpsVF3pePDKcEF.htm](pathfinder-bestiary-3-items/IRdpsVF3pePDKcEF.htm)|Enraged Home (Bludgeoning)|Casa Enfurecida (Bludgeoning)|modificada|
|[iRJk9tacSkOQcb2p.htm](pathfinder-bestiary-3-items/iRJk9tacSkOQcb2p.htm)|Create Golden Apple|Elaborar manzana de oro|modificada|
|[IRV6fBqa828pUJvZ.htm](pathfinder-bestiary-3-items/IRV6fBqa828pUJvZ.htm)|Unnatural Leap|Salto sin carrerilla|modificada|
|[IsKgw1GkBDUH68wl.htm](pathfinder-bestiary-3-items/IsKgw1GkBDUH68wl.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[ISW9dU1e7h1uA2Vu.htm](pathfinder-bestiary-3-items/ISW9dU1e7h1uA2Vu.htm)|In Concert|En Concierto|modificada|
|[IT0DYOyhbNdwhEsc.htm](pathfinder-bestiary-3-items/IT0DYOyhbNdwhEsc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[It88LMNyaOfw3HMG.htm](pathfinder-bestiary-3-items/It88LMNyaOfw3HMG.htm)|Shadow's Swiftness|Celeridad de Sombra|modificada|
|[iuA961D6giH9Ci1G.htm](pathfinder-bestiary-3-items/iuA961D6giH9Ci1G.htm)|Construct Armor (Hardness 10)|Construir Armadura (Dureza 10)|modificada|
|[iUcqbwAXsz9BBt5U.htm](pathfinder-bestiary-3-items/iUcqbwAXsz9BBt5U.htm)|Change Shape|Change Shape|modificada|
|[IUp0cmk3iouIb4mc.htm](pathfinder-bestiary-3-items/IUp0cmk3iouIb4mc.htm)|Mundane Appearance|Mundane Appearance|modificada|
|[IvCrd9B34OQupCPq.htm](pathfinder-bestiary-3-items/IvCrd9B34OQupCPq.htm)|Slime Ball|Bola de Baba|modificada|
|[IVnGWQzHYIx6BwIf.htm](pathfinder-bestiary-3-items/IVnGWQzHYIx6BwIf.htm)|Ride Corpse|Montar Cadáver|modificada|
|[ivTJbPtmjGlcioNL.htm](pathfinder-bestiary-3-items/ivTJbPtmjGlcioNL.htm)|Halberd|Alabarda|modificada|
|[IvUr6AnOtG8K2BSD.htm](pathfinder-bestiary-3-items/IvUr6AnOtG8K2BSD.htm)|Energize Clockwork Wand|Energize Clockwork Wand|modificada|
|[iVuxv6GFqvanqgOO.htm](pathfinder-bestiary-3-items/iVuxv6GFqvanqgOO.htm)|Rend|Rasgadura|modificada|
|[iWhzjrN6V3SMvbjf.htm](pathfinder-bestiary-3-items/iWhzjrN6V3SMvbjf.htm)|Claw|Garra|modificada|
|[IwRzWoZPoZN3p2w4.htm](pathfinder-bestiary-3-items/IwRzWoZPoZN3p2w4.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[iXj6VKYDT9ao0fDy.htm](pathfinder-bestiary-3-items/iXj6VKYDT9ao0fDy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iXMM9LrLHQONVyO1.htm](pathfinder-bestiary-3-items/iXMM9LrLHQONVyO1.htm)|Throw Spirits|Lanzar Espíritus|modificada|
|[IyoiZQ9XhzNia9nz.htm](pathfinder-bestiary-3-items/IyoiZQ9XhzNia9nz.htm)|Shortsword|Espada corta|modificada|
|[IyQeFgzzdZuwWSrh.htm](pathfinder-bestiary-3-items/IyQeFgzzdZuwWSrh.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[iYWcxAxjnGp9gag8.htm](pathfinder-bestiary-3-items/iYWcxAxjnGp9gag8.htm)|Hatchet|Hacha|modificada|
|[iZ48ihJeJ9DbfW5I.htm](pathfinder-bestiary-3-items/iZ48ihJeJ9DbfW5I.htm)|Jaws|Fauces|modificada|
|[IzAYt0TOGDn03Iuj.htm](pathfinder-bestiary-3-items/IzAYt0TOGDn03Iuj.htm)|Fiery Explosion|Explosión Ardiente|modificada|
|[iZqo211s6bnAgJto.htm](pathfinder-bestiary-3-items/iZqo211s6bnAgJto.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[j1Bwf8sEmKvjZNHK.htm](pathfinder-bestiary-3-items/j1Bwf8sEmKvjZNHK.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[j1yTlYWclYVbVuCO.htm](pathfinder-bestiary-3-items/j1yTlYWclYVbVuCO.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[j2EwWhmaQSWT0Ktw.htm](pathfinder-bestiary-3-items/j2EwWhmaQSWT0Ktw.htm)|Wrath of Spurned Hospitality|Ira de la Hospitalidad Despreciada|modificada|
|[J2Wcbnb4u8polTU4.htm](pathfinder-bestiary-3-items/J2Wcbnb4u8polTU4.htm)|Grab|Agarrado|modificada|
|[J3Ldbu2TKyrPVZBY.htm](pathfinder-bestiary-3-items/J3Ldbu2TKyrPVZBY.htm)|Instrument of Faith|Instrumento de Fe|modificada|
|[j3teD40q6U6024pW.htm](pathfinder-bestiary-3-items/j3teD40q6U6024pW.htm)|Foot|Pie|modificada|
|[j3YK3rYte4HUEXXX.htm](pathfinder-bestiary-3-items/j3YK3rYte4HUEXXX.htm)|Troop Defenses|Troop Defenses|modificada|
|[J5VPK19qAf7MabPt.htm](pathfinder-bestiary-3-items/J5VPK19qAf7MabPt.htm)|Emerge From Undergrowth|Emerge de la maleza|modificada|
|[j67aVe855G3FeDEO.htm](pathfinder-bestiary-3-items/j67aVe855G3FeDEO.htm)|Improved Grab|Agarrado mejorado|modificada|
|[j68N9UkahC1d6lhq.htm](pathfinder-bestiary-3-items/j68N9UkahC1d6lhq.htm)|-1 Status to All Saves vs. Death Effects|-1 situación a todas las salvaciones contra efectos de muerte.|modificada|
|[J6FVU37oPVKikGeY.htm](pathfinder-bestiary-3-items/J6FVU37oPVKikGeY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j6jdXFHyW1BBy2iT.htm](pathfinder-bestiary-3-items/j6jdXFHyW1BBy2iT.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[j6z5lXtJI1JXtP6N.htm](pathfinder-bestiary-3-items/j6z5lXtJI1JXtP6N.htm)|Constant Spells|Constant Spells|modificada|
|[j71uBba5yrrEslXS.htm](pathfinder-bestiary-3-items/j71uBba5yrrEslXS.htm)|Flexible|Flexible|modificada|
|[J7axLPnjNBn2CYP9.htm](pathfinder-bestiary-3-items/J7axLPnjNBn2CYP9.htm)|Negative Healing|Curación negativa|modificada|
|[J8ElHZzPwIKugFCK.htm](pathfinder-bestiary-3-items/J8ElHZzPwIKugFCK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[J9DbeNCFrbq08rQy.htm](pathfinder-bestiary-3-items/J9DbeNCFrbq08rQy.htm)|Felt Shears|Tijeras de fieltro|modificada|
|[JA8W1EoSKmMH74zg.htm](pathfinder-bestiary-3-items/JA8W1EoSKmMH74zg.htm)|Illusory Weapon|Illusory Weapon|modificada|
|[JAaRcZ0xgXIgQi8D.htm](pathfinder-bestiary-3-items/JAaRcZ0xgXIgQi8D.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[jAcMhmLTK9Y34bhZ.htm](pathfinder-bestiary-3-items/jAcMhmLTK9Y34bhZ.htm)|Coven Spells|Coven Spells|modificada|
|[jaJAvtkcuLs6oqUz.htm](pathfinder-bestiary-3-items/jaJAvtkcuLs6oqUz.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[jB6DPG2ZqtuA1Z3r.htm](pathfinder-bestiary-3-items/jB6DPG2ZqtuA1Z3r.htm)|Regeneration 15 (Deactivated by Acid)|Regeneración 15 (Desactivado por Ácido)|modificada|
|[jBnvJizQVNlbpByJ.htm](pathfinder-bestiary-3-items/jBnvJizQVNlbpByJ.htm)|Jaws|Fauces|modificada|
|[jbOS76Gp3MzY4W9x.htm](pathfinder-bestiary-3-items/jbOS76Gp3MzY4W9x.htm)|Torturous Buzz|Torturous Buzz|modificada|
|[jBu4ahdLBrGQ0cxJ.htm](pathfinder-bestiary-3-items/jBu4ahdLBrGQ0cxJ.htm)|Shuriken|Shuriken|modificada|
|[JCVCYsMSue62ekID.htm](pathfinder-bestiary-3-items/JCVCYsMSue62ekID.htm)|Project Terror|Proyecto Terror|modificada|
|[JcXJdmKDoEq5nqTp.htm](pathfinder-bestiary-3-items/JcXJdmKDoEq5nqTp.htm)|Covetous of Secrets|Codicioso de secretos|modificada|
|[jd3HtosBbbWGDYMZ.htm](pathfinder-bestiary-3-items/jd3HtosBbbWGDYMZ.htm)|Spider Legs|Spider Legs|modificada|
|[JD7dIGSb1yYwZOil.htm](pathfinder-bestiary-3-items/JD7dIGSb1yYwZOil.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jdhRSwRmm4AuXQt1.htm](pathfinder-bestiary-3-items/jdhRSwRmm4AuXQt1.htm)|Foot|Pie|modificada|
|[JDNUanP8QQ5riRTG.htm](pathfinder-bestiary-3-items/JDNUanP8QQ5riRTG.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[jEfZwLub2SXZ9ApN.htm](pathfinder-bestiary-3-items/jEfZwLub2SXZ9ApN.htm)|Ominous Footsteps|Ominous Footsteps|modificada|
|[jETzhtuReRYnPbPE.htm](pathfinder-bestiary-3-items/jETzhtuReRYnPbPE.htm)|False Foe|False Foe|modificada|
|[jeUZBWMJc0ynT78X.htm](pathfinder-bestiary-3-items/jeUZBWMJc0ynT78X.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JG2FJBq2rmJM7TqB.htm](pathfinder-bestiary-3-items/JG2FJBq2rmJM7TqB.htm)|+1 Status to All Saves vs. Darkness or Shadow|+1 situación a todas las salvaciones contra oscuridad o Sombra.|modificada|
|[jgB5xsEwGDphGPM7.htm](pathfinder-bestiary-3-items/jgB5xsEwGDphGPM7.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[JgIL2Chquwv95iZZ.htm](pathfinder-bestiary-3-items/JgIL2Chquwv95iZZ.htm)|Hyponatremia|Hiponatremia|modificada|
|[JGSUP9GpkQinyXNh.htm](pathfinder-bestiary-3-items/JGSUP9GpkQinyXNh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jGVjxRiLDypUaQCi.htm](pathfinder-bestiary-3-items/jGVjxRiLDypUaQCi.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[JHEhs3yuQkcfulWY.htm](pathfinder-bestiary-3-items/JHEhs3yuQkcfulWY.htm)|Anguished Cry|Anguished Cry|modificada|
|[jhi0sreme2yjolfI.htm](pathfinder-bestiary-3-items/jhi0sreme2yjolfI.htm)|Spear|Lanza|modificada|
|[jHVQHOSLTThAA8th.htm](pathfinder-bestiary-3-items/jHVQHOSLTThAA8th.htm)|Antler|Antler|modificada|
|[ji4mZPdeTTXd5DD2.htm](pathfinder-bestiary-3-items/ji4mZPdeTTXd5DD2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[JiieW7FHU7H9vlCH.htm](pathfinder-bestiary-3-items/JiieW7FHU7H9vlCH.htm)|Greater Constrict|Mayor Restricción|modificada|
|[jJdhYFljJjyWjYuO.htm](pathfinder-bestiary-3-items/jJdhYFljJjyWjYuO.htm)|Light to Dark|De la luz a la oscuridad|modificada|
|[JjTVprpY8GeQPnTT.htm](pathfinder-bestiary-3-items/JjTVprpY8GeQPnTT.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[jkOvDfJsGXDaYicW.htm](pathfinder-bestiary-3-items/jkOvDfJsGXDaYicW.htm)|Crocodile Empathy|Crocodile Empathy|modificada|
|[jKqS45yr5dqPtPAf.htm](pathfinder-bestiary-3-items/jKqS45yr5dqPtPAf.htm)|Claw|Garra|modificada|
|[jlBFW0dhslsLTtR1.htm](pathfinder-bestiary-3-items/jlBFW0dhslsLTtR1.htm)|Collective Sense|Sentido Colectivo|modificada|
|[jlphhH3RnH4L3Qup.htm](pathfinder-bestiary-3-items/jlphhH3RnH4L3Qup.htm)|Grab|Agarrado|modificada|
|[jLWpzWTqyo83F7Uy.htm](pathfinder-bestiary-3-items/jLWpzWTqyo83F7Uy.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[JmeFujNsZiXeU7qA.htm](pathfinder-bestiary-3-items/JmeFujNsZiXeU7qA.htm)|Breath Weapon|Breath Weapon|modificada|
|[JmiVjM3xyF1WaZae.htm](pathfinder-bestiary-3-items/JmiVjM3xyF1WaZae.htm)|Uncanny Pounce|Uncanny Abalanzarse|modificada|
|[jMJ3VKTn3P4G9pne.htm](pathfinder-bestiary-3-items/jMJ3VKTn3P4G9pne.htm)|Instant Suspension|Suspensión instantánea|modificada|
|[jMQ5yo71IDbzIoiV.htm](pathfinder-bestiary-3-items/jMQ5yo71IDbzIoiV.htm)|Jaws|Fauces|modificada|
|[JNAbSt0GireCV1x1.htm](pathfinder-bestiary-3-items/JNAbSt0GireCV1x1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JNflvBLtzzPBlMKZ.htm](pathfinder-bestiary-3-items/JNflvBLtzzPBlMKZ.htm)|Heat of the Forge|Calor de la fragua|modificada|
|[JNHO9c4kCv8dEqkU.htm](pathfinder-bestiary-3-items/JNHO9c4kCv8dEqkU.htm)|Tongue|Lengua|modificada|
|[jnPwDZEoWvaBHihS.htm](pathfinder-bestiary-3-items/jnPwDZEoWvaBHihS.htm)|Jorogumo Venom|Jorogumo Venom|modificada|
|[JOnNHpqNRRfoZym8.htm](pathfinder-bestiary-3-items/JOnNHpqNRRfoZym8.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[JOPv6XlxMrnEuMiK.htm](pathfinder-bestiary-3-items/JOPv6XlxMrnEuMiK.htm)|Negative Healing|Curación negativa|modificada|
|[jpLpYja2kQc3ywu3.htm](pathfinder-bestiary-3-items/jpLpYja2kQc3ywu3.htm)|Telepathy 30 feet (Munavris Only)|Telepatía 30 pies (sólo Munavris).|modificada|
|[JpsnonKEA4ScV5n5.htm](pathfinder-bestiary-3-items/JpsnonKEA4ScV5n5.htm)|Tail|Tail|modificada|
|[JQcN49zNIu4wTuSg.htm](pathfinder-bestiary-3-items/JQcN49zNIu4wTuSg.htm)|Defensive Quills|Defensive Quills|modificada|
|[jQCQmFdnjvvlQOeA.htm](pathfinder-bestiary-3-items/jQCQmFdnjvvlQOeA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jQDDocLXx8D5KEwx.htm](pathfinder-bestiary-3-items/jQDDocLXx8D5KEwx.htm)|Catch Rock|Atrapar roca|modificada|
|[jQpWelCVysebUl0R.htm](pathfinder-bestiary-3-items/jQpWelCVysebUl0R.htm)|Tail|Tail|modificada|
|[jqQAuZsUGFs0m1uI.htm](pathfinder-bestiary-3-items/jqQAuZsUGFs0m1uI.htm)|Lifesense 120 feet|Lifesense 120 pies|modificada|
|[jrAbmB8HIKhZ8PFd.htm](pathfinder-bestiary-3-items/jrAbmB8HIKhZ8PFd.htm)|Jaws|Fauces|modificada|
|[jRNANkCIgSnmDUWP.htm](pathfinder-bestiary-3-items/jRNANkCIgSnmDUWP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Js5HPgVs02JYjPwr.htm](pathfinder-bestiary-3-items/Js5HPgVs02JYjPwr.htm)|Spray Pus|Spray Pus|modificada|
|[JScL1xh1oN0V3BSZ.htm](pathfinder-bestiary-3-items/JScL1xh1oN0V3BSZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JSGbWepzGabICqtd.htm](pathfinder-bestiary-3-items/JSGbWepzGabICqtd.htm)|Spray Blinding Musk|Rociar Almizcle Cegador|modificada|
|[jspQIwxqoVmeF37U.htm](pathfinder-bestiary-3-items/jspQIwxqoVmeF37U.htm)|Invoke Haunter of the Dark|Invoke Haunter of the Dark|modificada|
|[JSRXi7FpsI5stZOw.htm](pathfinder-bestiary-3-items/JSRXi7FpsI5stZOw.htm)|Fangs|Colmillos|modificada|
|[Jt5uw27Ef9hVZ8D5.htm](pathfinder-bestiary-3-items/Jt5uw27Ef9hVZ8D5.htm)|Thoughtsense (Precise) 60 feet|Percibir pensamientos (precisión) 60 pies.|modificada|
|[jTT3N3sXixsPE4b1.htm](pathfinder-bestiary-3-items/jTT3N3sXixsPE4b1.htm)|Fist|Puño|modificada|
|[jTvAXL1UqsQrbQhO.htm](pathfinder-bestiary-3-items/jTvAXL1UqsQrbQhO.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[jUd95l5nba0bNy3I.htm](pathfinder-bestiary-3-items/jUd95l5nba0bNy3I.htm)|Gleaming Armor|Armadura reluciente|modificada|
|[jveVL0lDMa0EIIx6.htm](pathfinder-bestiary-3-items/jveVL0lDMa0EIIx6.htm)|Constant Spells|Constant Spells|modificada|
|[jvJQ52E2kygewU8J.htm](pathfinder-bestiary-3-items/jvJQ52E2kygewU8J.htm)|Spear|Lanza|modificada|
|[jw9ImZmmbzTSmeX5.htm](pathfinder-bestiary-3-items/jw9ImZmmbzTSmeX5.htm)|Threatening Lunge|Acometer Amenazante|modificada|
|[JwmDTNMiGDZ6Kis3.htm](pathfinder-bestiary-3-items/JwmDTNMiGDZ6Kis3.htm)|Dagger|Daga|modificada|
|[jwmn7WQty7oJo2L7.htm](pathfinder-bestiary-3-items/jwmn7WQty7oJo2L7.htm)|Construct Armor (Hardness 15)|Construir Armadura (Dureza 15)|modificada|
|[JwRLhgIxYJX1wLQx.htm](pathfinder-bestiary-3-items/JwRLhgIxYJX1wLQx.htm)|Thundering Charge|Carga Tronante|modificada|
|[JxdXfCP6ayaEEZND.htm](pathfinder-bestiary-3-items/JxdXfCP6ayaEEZND.htm)|Jaws|Fauces|modificada|
|[jXHs8IwojZHlZUSA.htm](pathfinder-bestiary-3-items/jXHs8IwojZHlZUSA.htm)|Wind Mastery|Dominio del Viento|modificada|
|[JxnXxf6brDnOqBXG.htm](pathfinder-bestiary-3-items/JxnXxf6brDnOqBXG.htm)|Sweltering Heat|Sweltering Heat|modificada|
|[JXy1hnyoxsc0xnCO.htm](pathfinder-bestiary-3-items/JXy1hnyoxsc0xnCO.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[JYnZKoZ4Immi2yaz.htm](pathfinder-bestiary-3-items/JYnZKoZ4Immi2yaz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Jyzm7OFRyidSiJk0.htm](pathfinder-bestiary-3-items/Jyzm7OFRyidSiJk0.htm)|Spore Domination|Dominación Espora|modificada|
|[Jzdce7Z64aM5gu70.htm](pathfinder-bestiary-3-items/Jzdce7Z64aM5gu70.htm)|Phantom Sword|Phantom Sword|modificada|
|[JZeE5BqCANDTf6hq.htm](pathfinder-bestiary-3-items/JZeE5BqCANDTf6hq.htm)|Change Shape|Change Shape|modificada|
|[jZf3j2xK5YKD3uC0.htm](pathfinder-bestiary-3-items/jZf3j2xK5YKD3uC0.htm)|Activate Defenses|Activar Defensas|modificada|
|[jzI14zm2om9KJeOM.htm](pathfinder-bestiary-3-items/jzI14zm2om9KJeOM.htm)|Devourer of Swarms|Devorador de Enjambres|modificada|
|[K0pcy2s3gTJZZK7a.htm](pathfinder-bestiary-3-items/K0pcy2s3gTJZZK7a.htm)|Stormsight|Stormsight|modificada|
|[k1fBI6krs8NLi3AR.htm](pathfinder-bestiary-3-items/k1fBI6krs8NLi3AR.htm)|Pitchfork|Horca|modificada|
|[k1FCB3wNkyv3rnly.htm](pathfinder-bestiary-3-items/k1FCB3wNkyv3rnly.htm)|Vortex|Vortex|modificada|
|[k2B8t1pzRzSkO2qC.htm](pathfinder-bestiary-3-items/k2B8t1pzRzSkO2qC.htm)|Talon|Talon|modificada|
|[k2HBbsayvnQzyEIo.htm](pathfinder-bestiary-3-items/k2HBbsayvnQzyEIo.htm)|Death Umbra|Umbra de la Muerte|modificada|
|[k2Num39uDHGiZwTm.htm](pathfinder-bestiary-3-items/k2Num39uDHGiZwTm.htm)|Breath Weapon|Breath Weapon|modificada|
|[k2oujPtkWYkCjV8t.htm](pathfinder-bestiary-3-items/k2oujPtkWYkCjV8t.htm)|Jaws|Fauces|modificada|
|[K32Ag7GpssjobCOz.htm](pathfinder-bestiary-3-items/K32Ag7GpssjobCOz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[k3a4ohAlfjdpm7Ck.htm](pathfinder-bestiary-3-items/k3a4ohAlfjdpm7Ck.htm)|Grab|Agarrado|modificada|
|[k3dgXH9CegWMvwQi.htm](pathfinder-bestiary-3-items/k3dgXH9CegWMvwQi.htm)|Consume Souls|Consumir almas|modificada|
|[k4kguAJD9K2mg4zY.htm](pathfinder-bestiary-3-items/k4kguAJD9K2mg4zY.htm)|Impossible Stature|Impossible Stature|modificada|
|[k5a2kb9JPuGSAe7J.htm](pathfinder-bestiary-3-items/k5a2kb9JPuGSAe7J.htm)|Darkvision|Visión en la oscuridad|modificada|
|[K5ozGM2drmKhGIoI.htm](pathfinder-bestiary-3-items/K5ozGM2drmKhGIoI.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[k6bhkrULfZctxZZa.htm](pathfinder-bestiary-3-items/k6bhkrULfZctxZZa.htm)|Landslide|Landslide|modificada|
|[k6KXYRt0pYf0ds2B.htm](pathfinder-bestiary-3-items/k6KXYRt0pYf0ds2B.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[K6p90DP5TOgu1uHl.htm](pathfinder-bestiary-3-items/K6p90DP5TOgu1uHl.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[K8HY6MgNzIvDLuDT.htm](pathfinder-bestiary-3-items/K8HY6MgNzIvDLuDT.htm)|Bone Chariot|Bone Chariot|modificada|
|[K9b86nLjHgbbzFnm.htm](pathfinder-bestiary-3-items/K9b86nLjHgbbzFnm.htm)|Fist|Puño|modificada|
|[ka8ggPLmvKPNC19r.htm](pathfinder-bestiary-3-items/ka8ggPLmvKPNC19r.htm)|Wind Blast|Ráfaga de Viento|modificada|
|[kacehtkxmzv3j5Zn.htm](pathfinder-bestiary-3-items/kacehtkxmzv3j5Zn.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[kB4Y7lsPktJzaUtm.htm](pathfinder-bestiary-3-items/kB4Y7lsPktJzaUtm.htm)|Countered by Metal|Contrarrestado por Metal|modificada|
|[kBmOKMaUjcEjTeia.htm](pathfinder-bestiary-3-items/kBmOKMaUjcEjTeia.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[KbRk42Ska9TyPynC.htm](pathfinder-bestiary-3-items/KbRk42Ska9TyPynC.htm)|Dab|Dab|modificada|
|[KD9WyZyMHgs07nLa.htm](pathfinder-bestiary-3-items/KD9WyZyMHgs07nLa.htm)|Claw|Garra|modificada|
|[KdePPltFxHzCPJrm.htm](pathfinder-bestiary-3-items/KdePPltFxHzCPJrm.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KDhzo4KVYsJvSpXo.htm](pathfinder-bestiary-3-items/KDhzo4KVYsJvSpXo.htm)|Keelhaul|Keelhaul|modificada|
|[KdYM2NV3yWc57PNc.htm](pathfinder-bestiary-3-items/KdYM2NV3yWc57PNc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ke8jPPnQzzzeKXWw.htm](pathfinder-bestiary-3-items/Ke8jPPnQzzzeKXWw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KEJNkeF17Hd5RgpW.htm](pathfinder-bestiary-3-items/KEJNkeF17Hd5RgpW.htm)|Feral Trackers|Feral Trackers|modificada|
|[KewwLz9N8mWqhTpA.htm](pathfinder-bestiary-3-items/KewwLz9N8mWqhTpA.htm)|Frightful Presence|Frightful Presence|modificada|
|[kfBNKa5ltxDIa0qG.htm](pathfinder-bestiary-3-items/kfBNKa5ltxDIa0qG.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KftIJAHBIGp8Mnad.htm](pathfinder-bestiary-3-items/KftIJAHBIGp8Mnad.htm)|Camouflage|Camuflaje|modificada|
|[KGCeRo6qPcjltHnA.htm](pathfinder-bestiary-3-items/KGCeRo6qPcjltHnA.htm)|Wavesense (Imprecise) 10 feet|Sentir ondulaciones (Impreciso) 10 pies|modificada|
|[KgidUZZqAF6dfZdR.htm](pathfinder-bestiary-3-items/KgidUZZqAF6dfZdR.htm)|Bo Staff|Báculo Bo|modificada|
|[KGyAqL8XCgBPKJwN.htm](pathfinder-bestiary-3-items/KGyAqL8XCgBPKJwN.htm)|Divine Lightning|Relámpago Divino|modificada|
|[khSydk2fZucaWpwr.htm](pathfinder-bestiary-3-items/khSydk2fZucaWpwr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KIEOH7Hzo0bgoAnC.htm](pathfinder-bestiary-3-items/KIEOH7Hzo0bgoAnC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kimzS4sVtC1JuBK6.htm](pathfinder-bestiary-3-items/kimzS4sVtC1JuBK6.htm)|Tail|Tail|modificada|
|[KjameIcuKwYkyTzJ.htm](pathfinder-bestiary-3-items/KjameIcuKwYkyTzJ.htm)|Cooperative Hunting|Caza cooperativa|modificada|
|[kjCwpEZhQpTXIyZj.htm](pathfinder-bestiary-3-items/kjCwpEZhQpTXIyZj.htm)|Defensive Disarm|Desarmar defensivo|modificada|
|[KjLaF8NJBaPjsRAz.htm](pathfinder-bestiary-3-items/KjLaF8NJBaPjsRAz.htm)|Gnash|Gnash|modificada|
|[kjoPJjVHl7tsoAet.htm](pathfinder-bestiary-3-items/kjoPJjVHl7tsoAet.htm)|Breath Weapon|Breath Weapon|modificada|
|[KkkIZP8XxpPJcECa.htm](pathfinder-bestiary-3-items/KkkIZP8XxpPJcECa.htm)|Flaming Greataxe|Flamígera Greataxe|modificada|
|[KkSN8aLpddZGTcZq.htm](pathfinder-bestiary-3-items/KkSN8aLpddZGTcZq.htm)|Greater Constrict|Mayor Restricción|modificada|
|[klPNjA3PDnepkkn6.htm](pathfinder-bestiary-3-items/klPNjA3PDnepkkn6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KluTBobaNmcJwryy.htm](pathfinder-bestiary-3-items/KluTBobaNmcJwryy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KMdZUSARMPSywMsn.htm](pathfinder-bestiary-3-items/KMdZUSARMPSywMsn.htm)|Divine Aegis|Égida divina|modificada|
|[kMfzeWG6rv2XM68m.htm](pathfinder-bestiary-3-items/kMfzeWG6rv2XM68m.htm)|Blade-Leg|Blade-Leg|modificada|
|[KmgwRHZExpyqDBij.htm](pathfinder-bestiary-3-items/KmgwRHZExpyqDBij.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[knIdAZZkuVAcH33t.htm](pathfinder-bestiary-3-items/knIdAZZkuVAcH33t.htm)|Claw|Garra|modificada|
|[kNl1E67lBZGpSC5U.htm](pathfinder-bestiary-3-items/kNl1E67lBZGpSC5U.htm)|Gory Tendril|Gory Tendril|modificada|
|[KnlMKaajUHNW24mj.htm](pathfinder-bestiary-3-items/KnlMKaajUHNW24mj.htm)|Crossbow|Ballesta|modificada|
|[KOan7i2UeawwyO2v.htm](pathfinder-bestiary-3-items/KOan7i2UeawwyO2v.htm)|Push|Push|modificada|
|[koOLkiJRcE1XYMxS.htm](pathfinder-bestiary-3-items/koOLkiJRcE1XYMxS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kpB9dRBV5LWoW0uG.htm](pathfinder-bestiary-3-items/kpB9dRBV5LWoW0uG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kpvnHl1XmqHSX6rP.htm](pathfinder-bestiary-3-items/kpvnHl1XmqHSX6rP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KPwsQn8bO8tcLOgw.htm](pathfinder-bestiary-3-items/KPwsQn8bO8tcLOgw.htm)|Frightful Presence|Frightful Presence|modificada|
|[KpWuD5FV2RQBjAeo.htm](pathfinder-bestiary-3-items/KpWuD5FV2RQBjAeo.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[kpx9ES2ZOnjt0XMh.htm](pathfinder-bestiary-3-items/kpx9ES2ZOnjt0XMh.htm)|Troop Defenses|Troop Defenses|modificada|
|[kpySQfuxcXr6ztFA.htm](pathfinder-bestiary-3-items/kpySQfuxcXr6ztFA.htm)|Halberd|Alabarda|modificada|
|[kQaDhJaLxNJw4l8l.htm](pathfinder-bestiary-3-items/kQaDhJaLxNJw4l8l.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[kqcQU0n1PhfM4fgW.htm](pathfinder-bestiary-3-items/kqcQU0n1PhfM4fgW.htm)|Swarm Mind|Swarm Mind|modificada|
|[kQNlQmU6whlWK7M9.htm](pathfinder-bestiary-3-items/kQNlQmU6whlWK7M9.htm)|Telepathic Wail|Lamento Telepático|modificada|
|[KqQvNC6PQXFL9bre.htm](pathfinder-bestiary-3-items/KqQvNC6PQXFL9bre.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[KRjbKjp9iT8REk4M.htm](pathfinder-bestiary-3-items/KRjbKjp9iT8REk4M.htm)|Ecstatic Hunger|Ecstatic Hunger|modificada|
|[krWu51yBcWRllWxP.htm](pathfinder-bestiary-3-items/krWu51yBcWRllWxP.htm)|Sand Spin|Sand Spin|modificada|
|[ks2bGQlMCszTgN6c.htm](pathfinder-bestiary-3-items/ks2bGQlMCszTgN6c.htm)|Bardic Lore|Saber bárdico|modificada|
|[ks76ZS8gWSFgrdl9.htm](pathfinder-bestiary-3-items/ks76ZS8gWSFgrdl9.htm)|Catch Rock|Atrapar roca|modificada|
|[kSMYzNRzwjJcLeCe.htm](pathfinder-bestiary-3-items/kSMYzNRzwjJcLeCe.htm)|Tail|Tail|modificada|
|[kt9Kmearp1t75oDh.htm](pathfinder-bestiary-3-items/kt9Kmearp1t75oDh.htm)|Grab|Agarrado|modificada|
|[KUErEL0JPCYaIeu0.htm](pathfinder-bestiary-3-items/KUErEL0JPCYaIeu0.htm)|Electrical Field|Campo Eléctrico|modificada|
|[KvEbx6NMqRfeeaD2.htm](pathfinder-bestiary-3-items/KvEbx6NMqRfeeaD2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kWARaiGoyPB0KHgj.htm](pathfinder-bestiary-3-items/kWARaiGoyPB0KHgj.htm)|Spirit Body|Cuerpo espiritual|modificada|
|[kWiBTKt3h01RReit.htm](pathfinder-bestiary-3-items/kWiBTKt3h01RReit.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kX3JkP0HZPIQ7vWj.htm](pathfinder-bestiary-3-items/kX3JkP0HZPIQ7vWj.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[kXXLa1y6PCq4db69.htm](pathfinder-bestiary-3-items/kXXLa1y6PCq4db69.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[KXYQjUBjJFbx6t3Z.htm](pathfinder-bestiary-3-items/KXYQjUBjJFbx6t3Z.htm)|Claw|Garra|modificada|
|[kY9XyX74vUAFhali.htm](pathfinder-bestiary-3-items/kY9XyX74vUAFhali.htm)|Flay|Flay|modificada|
|[kYmteRQa3LahCveN.htm](pathfinder-bestiary-3-items/kYmteRQa3LahCveN.htm)|Touch of Ages|Touch of Ages|modificada|
|[kZCjL41fVSpE8vkC.htm](pathfinder-bestiary-3-items/kZCjL41fVSpE8vkC.htm)|Skip Between|Saltar entre|modificada|
|[KZhlld4EKQcCTBjw.htm](pathfinder-bestiary-3-items/KZhlld4EKQcCTBjw.htm)|Swarm Mind|Swarm Mind|modificada|
|[kziqHkR0lOsYBKJz.htm](pathfinder-bestiary-3-items/kziqHkR0lOsYBKJz.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[kzOsIUUeSNgTP9xK.htm](pathfinder-bestiary-3-items/kzOsIUUeSNgTP9xK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kzwDYGXsWl0wyTwN.htm](pathfinder-bestiary-3-items/kzwDYGXsWl0wyTwN.htm)|Light Hammer|Martillo ligero|modificada|
|[L0AaJkB1CVD61yGv.htm](pathfinder-bestiary-3-items/L0AaJkB1CVD61yGv.htm)|Master's Eyes|Master's Eyes|modificada|
|[L2LnR1T2MiBlznWr.htm](pathfinder-bestiary-3-items/L2LnR1T2MiBlznWr.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[l2ymfoz6uzs9Q7vM.htm](pathfinder-bestiary-3-items/l2ymfoz6uzs9Q7vM.htm)|Constant Spells|Constant Spells|modificada|
|[l2ZTQQTqrVwPvUY1.htm](pathfinder-bestiary-3-items/l2ZTQQTqrVwPvUY1.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[L3k41cRxvKs5ZH4L.htm](pathfinder-bestiary-3-items/L3k41cRxvKs5ZH4L.htm)|Morpheme Glyph|Glifo Morfema|modificada|
|[l3QKE8kkrmNe5aZO.htm](pathfinder-bestiary-3-items/l3QKE8kkrmNe5aZO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[L4eZWrxPCscqkfv1.htm](pathfinder-bestiary-3-items/L4eZWrxPCscqkfv1.htm)|Hurl Javelins!|¡Lanza Jabalinas!|modificada|
|[l6dezchxG4CL8dny.htm](pathfinder-bestiary-3-items/l6dezchxG4CL8dny.htm)|Burning Wings|Burning Wings|modificada|
|[L87Xx2Cd9z06I697.htm](pathfinder-bestiary-3-items/L87Xx2Cd9z06I697.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[l8Vwgjba06YNgLZg.htm](pathfinder-bestiary-3-items/l8Vwgjba06YNgLZg.htm)|Regeneration 20 (Deactivated by Evil)|Regeneración 20 (Desactivado por maligno)|modificada|
|[L9XsoNHRPeOdO6cL.htm](pathfinder-bestiary-3-items/L9XsoNHRPeOdO6cL.htm)|Striking Koa|Golpear a Koa|modificada|
|[LaB8UoWpNQqZKM30.htm](pathfinder-bestiary-3-items/LaB8UoWpNQqZKM30.htm)|Rolling Thunder|Tronante Rodante|modificada|
|[LAhnCuHlBH3zJBoP.htm](pathfinder-bestiary-3-items/LAhnCuHlBH3zJBoP.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[Lb8baU3vfeAtn1Nw.htm](pathfinder-bestiary-3-items/Lb8baU3vfeAtn1Nw.htm)|Cower|Cower|modificada|
|[lbG0r8knl6cp3RdW.htm](pathfinder-bestiary-3-items/lbG0r8knl6cp3RdW.htm)|Claw|Garra|modificada|
|[lBss0k9IK6iCcQEo.htm](pathfinder-bestiary-3-items/lBss0k9IK6iCcQEo.htm)|Excruciating Enzyme|Excruciating Enzyme|modificada|
|[LcChlrMndc88kp30.htm](pathfinder-bestiary-3-items/LcChlrMndc88kp30.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[LcdaZzKwze9fmC6D.htm](pathfinder-bestiary-3-items/LcdaZzKwze9fmC6D.htm)|Defensive Slam|Slam defensivo|modificada|
|[lcGPZQiY2GFPXOVQ.htm](pathfinder-bestiary-3-items/lcGPZQiY2GFPXOVQ.htm)|Stunning Flurry|Ráfaga de golpes|modificada|
|[lciayGn2ma9dSbcM.htm](pathfinder-bestiary-3-items/lciayGn2ma9dSbcM.htm)|Curse of Darkness|Maldición de la oscuridad|modificada|
|[lcYKbUtg1W8w9cul.htm](pathfinder-bestiary-3-items/lcYKbUtg1W8w9cul.htm)|Wing|Ala|modificada|
|[lcYudXUlkKY7E3iT.htm](pathfinder-bestiary-3-items/lcYudXUlkKY7E3iT.htm)|Trample|Trample|modificada|
|[LCYVlQZcfJT8AY75.htm](pathfinder-bestiary-3-items/LCYVlQZcfJT8AY75.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[lD1oJbNSEUiWgWzZ.htm](pathfinder-bestiary-3-items/lD1oJbNSEUiWgWzZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[lD2k5BU1mLju5ewn.htm](pathfinder-bestiary-3-items/lD2k5BU1mLju5ewn.htm)|Blade|Blade|modificada|
|[LD67mqtsXztjCTmJ.htm](pathfinder-bestiary-3-items/LD67mqtsXztjCTmJ.htm)|Regeneration 20 (Deactivated by Fire)|Regeneración 20 (Desactivado por Fuego)|modificada|
|[LdhQrUO09um9htY6.htm](pathfinder-bestiary-3-items/LdhQrUO09um9htY6.htm)|Drain Blood|Drenar sangre|modificada|
|[lDtCFaHXdvj9JdET.htm](pathfinder-bestiary-3-items/lDtCFaHXdvj9JdET.htm)|Shortbow|Arco corto|modificada|
|[LDTFrdNHyvizP1mX.htm](pathfinder-bestiary-3-items/LDTFrdNHyvizP1mX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[le8d6e5N7Wjb7Rgo.htm](pathfinder-bestiary-3-items/le8d6e5N7Wjb7Rgo.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[LehQoRloBWVwpTHh.htm](pathfinder-bestiary-3-items/LehQoRloBWVwpTHh.htm)|Mangle|Mangle|modificada|
|[leLKKGPbDbUPHstg.htm](pathfinder-bestiary-3-items/leLKKGPbDbUPHstg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lFFJEvkkHiFKVLEu.htm](pathfinder-bestiary-3-items/lFFJEvkkHiFKVLEu.htm)|Pinch|Pinch|modificada|
|[LFIyH1YUMUoGUmPX.htm](pathfinder-bestiary-3-items/LFIyH1YUMUoGUmPX.htm)|Countered by Metal|Contrarrestado por Metal|modificada|
|[LfpK0azkldAisbCY.htm](pathfinder-bestiary-3-items/LfpK0azkldAisbCY.htm)|Extend Limb|Extend Limb|modificada|
|[lgdEGoF8XPhWshor.htm](pathfinder-bestiary-3-items/lgdEGoF8XPhWshor.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Lgf1CXruVEasnLms.htm](pathfinder-bestiary-3-items/Lgf1CXruVEasnLms.htm)|Tremorsense (Imprecise) within their entire bound home|Sentido del temblor (Impreciso) dentro de toda su casa ligada.|modificada|
|[LH24pBPDHDAEH6TV.htm](pathfinder-bestiary-3-items/LH24pBPDHDAEH6TV.htm)|Create Golden Apple|Elaborar manzana de oro|modificada|
|[LhA2xtluaWsZrc1T.htm](pathfinder-bestiary-3-items/LhA2xtluaWsZrc1T.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[lHGpzeVpbkAh9PNL.htm](pathfinder-bestiary-3-items/lHGpzeVpbkAh9PNL.htm)|Bastard Sword|Espada Bastarda|modificada|
|[liA5VGcqAKl36Vqh.htm](pathfinder-bestiary-3-items/liA5VGcqAKl36Vqh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[liadzf6LKysC2rK8.htm](pathfinder-bestiary-3-items/liadzf6LKysC2rK8.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[liMrTZS1ECyEEh6I.htm](pathfinder-bestiary-3-items/liMrTZS1ECyEEh6I.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lIN8nRgTYzPOm90G.htm](pathfinder-bestiary-3-items/lIN8nRgTYzPOm90G.htm)|Incalculable Fangs|Colmillos Incalculables|modificada|
|[LIQcObuGu1wqBnEw.htm](pathfinder-bestiary-3-items/LIQcObuGu1wqBnEw.htm)|Grab|Agarrado|modificada|
|[lj6vRNNZwXQmeqG7.htm](pathfinder-bestiary-3-items/lj6vRNNZwXQmeqG7.htm)|Claw|Garra|modificada|
|[lJDxq1w6WvzVEghk.htm](pathfinder-bestiary-3-items/lJDxq1w6WvzVEghk.htm)|Sprint|Sprint|modificada|
|[Ljg3kgAdR1sVGBeL.htm](pathfinder-bestiary-3-items/Ljg3kgAdR1sVGBeL.htm)|Echolocation 60 feet|Ecolocalización 60 pies|modificada|
|[ljmD4eOFLneTqsb9.htm](pathfinder-bestiary-3-items/ljmD4eOFLneTqsb9.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lK75Yi29CDBjFNYu.htm](pathfinder-bestiary-3-items/lK75Yi29CDBjFNYu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LKCha1KO0muUvUr0.htm](pathfinder-bestiary-3-items/LKCha1KO0muUvUr0.htm)|Hellwasp Venom|Hellwasp Venom|modificada|
|[lkfuADlIxVnyt6EH.htm](pathfinder-bestiary-3-items/lkfuADlIxVnyt6EH.htm)|Scimitar|Cimitarra|modificada|
|[lL49wJa4ig4V0ag1.htm](pathfinder-bestiary-3-items/lL49wJa4ig4V0ag1.htm)|Record Audio|Record Audio|modificada|
|[Ll9EKS0upQ66GUIJ.htm](pathfinder-bestiary-3-items/Ll9EKS0upQ66GUIJ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LLArpiTAsI8pUto2.htm](pathfinder-bestiary-3-items/LLArpiTAsI8pUto2.htm)|Master of the Home|Maestro del Hogar|modificada|
|[llRkvAv40JIHPwXk.htm](pathfinder-bestiary-3-items/llRkvAv40JIHPwXk.htm)|Horn|Cuerno|modificada|
|[LMRK013cxClnSMlQ.htm](pathfinder-bestiary-3-items/LMRK013cxClnSMlQ.htm)|Tail|Tail|modificada|
|[lmUd66np5PVIvPFr.htm](pathfinder-bestiary-3-items/lmUd66np5PVIvPFr.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[Ln0qmde8wRjzuIos.htm](pathfinder-bestiary-3-items/Ln0qmde8wRjzuIos.htm)|Moisture Dependency|Dependencia de la humedad|modificada|
|[lnftcCa5nWLimB4r.htm](pathfinder-bestiary-3-items/lnftcCa5nWLimB4r.htm)|Wing Thrash|Golpeteo con alas|modificada|
|[lNuMFP2gGs9IFNk7.htm](pathfinder-bestiary-3-items/lNuMFP2gGs9IFNk7.htm)|Telepathy 30 feet|Telepatía 30 piesía.|modificada|
|[lO3tFaQKyvdDCf09.htm](pathfinder-bestiary-3-items/lO3tFaQKyvdDCf09.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LoLS2Gw8HzHFPWld.htm](pathfinder-bestiary-3-items/LoLS2Gw8HzHFPWld.htm)|Languages|Idiomas|modificada|
|[LorLNjRUhElv6hVC.htm](pathfinder-bestiary-3-items/LorLNjRUhElv6hVC.htm)|Tail|Tail|modificada|
|[LP9djZsLKMnkBo3M.htm](pathfinder-bestiary-3-items/LP9djZsLKMnkBo3M.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[lp9zpRImtn8gAWuk.htm](pathfinder-bestiary-3-items/lp9zpRImtn8gAWuk.htm)|Bloodsense (Imprecise) 90 feet|Sentido de la Sangre (Impreciso) 90 pies|modificada|
|[lpbIToo47rzzxQRz.htm](pathfinder-bestiary-3-items/lpbIToo47rzzxQRz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lPHcKkQiBqbtQQ8z.htm](pathfinder-bestiary-3-items/lPHcKkQiBqbtQQ8z.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[lPUDxg0sCW4m6DZX.htm](pathfinder-bestiary-3-items/lPUDxg0sCW4m6DZX.htm)|Dagger|Daga|modificada|
|[lq5shiJkFR0pdE6O.htm](pathfinder-bestiary-3-items/lq5shiJkFR0pdE6O.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[lQ9VDly9F5WovfxG.htm](pathfinder-bestiary-3-items/lQ9VDly9F5WovfxG.htm)|Tail|Tail|modificada|
|[lqiFGbLGpuC1xc0l.htm](pathfinder-bestiary-3-items/lqiFGbLGpuC1xc0l.htm)|Grab|Agarrado|modificada|
|[lQKNHnyXpn3nHnzE.htm](pathfinder-bestiary-3-items/lQKNHnyXpn3nHnzE.htm)|Swarm Mind|Swarm Mind|modificada|
|[LRFB4rNU7O66JVlQ.htm](pathfinder-bestiary-3-items/LRFB4rNU7O66JVlQ.htm)|Grab|Agarrado|modificada|
|[LrGPFAVrLLeOkRJj.htm](pathfinder-bestiary-3-items/LrGPFAVrLLeOkRJj.htm)|Whisper Earworm|Whisper Earworm|modificada|
|[LrtLY1p0wVwq0afd.htm](pathfinder-bestiary-3-items/LrtLY1p0wVwq0afd.htm)|Jaws|Fauces|modificada|
|[Lscs4sD2vIZEzZIX.htm](pathfinder-bestiary-3-items/Lscs4sD2vIZEzZIX.htm)|Tail|Tail|modificada|
|[lsECqEvllEKtOv5g.htm](pathfinder-bestiary-3-items/lsECqEvllEKtOv5g.htm)|Chain Shot|Chain Shot|modificada|
|[Lt0Vd3S5Hji79WEW.htm](pathfinder-bestiary-3-items/Lt0Vd3S5Hji79WEW.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[LtMnRFGdEZ9u0QFh.htm](pathfinder-bestiary-3-items/LtMnRFGdEZ9u0QFh.htm)|Pluck Dream|Desplumar Sue|modificada|
|[lTNuGoh4u8QMy0pk.htm](pathfinder-bestiary-3-items/lTNuGoh4u8QMy0pk.htm)|Tendril|Tendril|modificada|
|[lufhjv4m2WdjSTZs.htm](pathfinder-bestiary-3-items/lufhjv4m2WdjSTZs.htm)|Countered by Water|Contrarrestado por Agua|modificada|
|[lUKDtZBh5MBovcUd.htm](pathfinder-bestiary-3-items/lUKDtZBh5MBovcUd.htm)|Negative Healing|Curación negativa|modificada|
|[LupnDs04lfZkJ52r.htm](pathfinder-bestiary-3-items/LupnDs04lfZkJ52r.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[LUWAzCWdvMJPK5Am.htm](pathfinder-bestiary-3-items/LUWAzCWdvMJPK5Am.htm)|Hide and Seek|Esconderse y Buscar|modificada|
|[lV8lCapoUOFDMPz8.htm](pathfinder-bestiary-3-items/lV8lCapoUOFDMPz8.htm)|Pukwudgie Poison|Pukwudgie Poison|modificada|
|[LVNb1MU1A4uFDYSo.htm](pathfinder-bestiary-3-items/LVNb1MU1A4uFDYSo.htm)|Push|Push|modificada|
|[LVzeZlyXzpgBSv5s.htm](pathfinder-bestiary-3-items/LVzeZlyXzpgBSv5s.htm)|Jaws|Fauces|modificada|
|[lwLwA2EvXhOPYIuA.htm](pathfinder-bestiary-3-items/lwLwA2EvXhOPYIuA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lwULACd9cg2TzXz5.htm](pathfinder-bestiary-3-items/lwULACd9cg2TzXz5.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[Lx5Q366mof1O0MPp.htm](pathfinder-bestiary-3-items/Lx5Q366mof1O0MPp.htm)|Form Up|Form Up|modificada|
|[lxcQL19jOkUey7nF.htm](pathfinder-bestiary-3-items/lxcQL19jOkUey7nF.htm)|Blowgun|Blowgun|modificada|
|[lXlQ3g7mZu8LDrvB.htm](pathfinder-bestiary-3-items/lXlQ3g7mZu8LDrvB.htm)|Woodland Stride|Paso forestal|modificada|
|[lxWOOSDzdS3gKtgq.htm](pathfinder-bestiary-3-items/lxWOOSDzdS3gKtgq.htm)|Swarm Mind|Swarm Mind|modificada|
|[LY9m5BMsAb8g3SIx.htm](pathfinder-bestiary-3-items/LY9m5BMsAb8g3SIx.htm)|Negative Healing|Curación negativa|modificada|
|[lYRZM8I1flblujiU.htm](pathfinder-bestiary-3-items/lYRZM8I1flblujiU.htm)|Negative Healing|Curación negativa|modificada|
|[LzvPd4UTa6iofTzD.htm](pathfinder-bestiary-3-items/LzvPd4UTa6iofTzD.htm)|Hoof|Hoof|modificada|
|[lZYIDbiemHOhYhm5.htm](pathfinder-bestiary-3-items/lZYIDbiemHOhYhm5.htm)|Abandon Corpse|Abandonar Cadáver|modificada|
|[m03bX0sD03E32bCM.htm](pathfinder-bestiary-3-items/m03bX0sD03E32bCM.htm)|Constant Spells|Constant Spells|modificada|
|[m0pBEDByK2E42m1L.htm](pathfinder-bestiary-3-items/m0pBEDByK2E42m1L.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[m1kfe61fI2Y0NgZd.htm](pathfinder-bestiary-3-items/m1kfe61fI2Y0NgZd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M1p2Iq57MOBT9II9.htm](pathfinder-bestiary-3-items/M1p2Iq57MOBT9II9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M1rzEefuLXlWlYN8.htm](pathfinder-bestiary-3-items/M1rzEefuLXlWlYN8.htm)|Colossus's Grasp|Colossus's Grasp|modificada|
|[m2GqanJ6fVKX39SQ.htm](pathfinder-bestiary-3-items/m2GqanJ6fVKX39SQ.htm)|Grab|Agarrado|modificada|
|[m2xn36z0bizgQRSl.htm](pathfinder-bestiary-3-items/m2xn36z0bizgQRSl.htm)|Polyglot|Políglota|modificada|
|[M3azf7Pp6YDlxgN3.htm](pathfinder-bestiary-3-items/M3azf7Pp6YDlxgN3.htm)|Quick Escape|Huir rápido|modificada|
|[M3e5MWLAt9COPusW.htm](pathfinder-bestiary-3-items/M3e5MWLAt9COPusW.htm)|Tendril|Tendril|modificada|
|[m3ZfC7sl35Uue2KZ.htm](pathfinder-bestiary-3-items/m3ZfC7sl35Uue2KZ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[M4gK880IlR5fkws0.htm](pathfinder-bestiary-3-items/M4gK880IlR5fkws0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M4yZRTxw1DkV1j2m.htm](pathfinder-bestiary-3-items/M4yZRTxw1DkV1j2m.htm)|Grab|Agarrado|modificada|
|[M68tcWaPYgJDf7pp.htm](pathfinder-bestiary-3-items/M68tcWaPYgJDf7pp.htm)|Launch|Lanzamiento|modificada|
|[M6bPLFIXKoFFgM2u.htm](pathfinder-bestiary-3-items/M6bPLFIXKoFFgM2u.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M6HiBc42J8BI5rZo.htm](pathfinder-bestiary-3-items/M6HiBc42J8BI5rZo.htm)|Unstable Magic|Magia Inestable|modificada|
|[M6VhLwSsBbyN4CsJ.htm](pathfinder-bestiary-3-items/M6VhLwSsBbyN4CsJ.htm)|Greatpick|Greatpick|modificada|
|[M7bSwayItCZu1H43.htm](pathfinder-bestiary-3-items/M7bSwayItCZu1H43.htm)|Crush Chitin|Triturar quitina|modificada|
|[m8Ycgo6ZGdfthZSw.htm](pathfinder-bestiary-3-items/m8Ycgo6ZGdfthZSw.htm)|Sanguine Spray|Sanguine Spray|modificada|
|[M9Gq6cRX2w40Q3Yb.htm](pathfinder-bestiary-3-items/M9Gq6cRX2w40Q3Yb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mAl92vhU2YdKG3bV.htm](pathfinder-bestiary-3-items/mAl92vhU2YdKG3bV.htm)|Breath Weapon|Breath Weapon|modificada|
|[maPEoGc6nv9lMP5R.htm](pathfinder-bestiary-3-items/maPEoGc6nv9lMP5R.htm)|Fist|Puño|modificada|
|[MAu9spTENeddU6Zv.htm](pathfinder-bestiary-3-items/MAu9spTENeddU6Zv.htm)|Foot|Pie|modificada|
|[maVqItGv6VD6w8M3.htm](pathfinder-bestiary-3-items/maVqItGv6VD6w8M3.htm)|Roiling Rebuke|Roiling Rebuke|modificada|
|[MB3A4i2cdRbTv3vg.htm](pathfinder-bestiary-3-items/MB3A4i2cdRbTv3vg.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[MbX8MuX8mUeGVwXv.htm](pathfinder-bestiary-3-items/MbX8MuX8mUeGVwXv.htm)|Stunning Electricity|Electricidad Aturdidora|modificada|
|[MCvCX567cQVCilQv.htm](pathfinder-bestiary-3-items/MCvCX567cQVCilQv.htm)|Claw|Garra|modificada|
|[MdAaSnMbM5KOAlWl.htm](pathfinder-bestiary-3-items/MdAaSnMbM5KOAlWl.htm)|Pummeling Assault|Pummeling Assault|modificada|
|[MDEHgLAgesfgrGrt.htm](pathfinder-bestiary-3-items/MDEHgLAgesfgrGrt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[MDqLlKyAsgRUfism.htm](pathfinder-bestiary-3-items/MDqLlKyAsgRUfism.htm)|Constant Spells|Constant Spells|modificada|
|[mEcsR2Hq0xwLS5m1.htm](pathfinder-bestiary-3-items/mEcsR2Hq0xwLS5m1.htm)|Shadow Whip|Látigo de Sombra|modificada|
|[MEdTOOe1fbIYRy1N.htm](pathfinder-bestiary-3-items/MEdTOOe1fbIYRy1N.htm)|Jaws|Fauces|modificada|
|[mep6FodvFU5OWGuk.htm](pathfinder-bestiary-3-items/mep6FodvFU5OWGuk.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[Mf6HjC4fVjp3RLPm.htm](pathfinder-bestiary-3-items/Mf6HjC4fVjp3RLPm.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[mfooCk9alzoismP0.htm](pathfinder-bestiary-3-items/mfooCk9alzoismP0.htm)|Grab|Agarrado|modificada|
|[mfQZq4ONY5lVM7IX.htm](pathfinder-bestiary-3-items/mfQZq4ONY5lVM7IX.htm)|Constant Spells|Constant Spells|modificada|
|[mFrFiI59EQmtyVk8.htm](pathfinder-bestiary-3-items/mFrFiI59EQmtyVk8.htm)|Scorch Earth|Tierra abrasadora|modificada|
|[MfS2aGB9sp6OgznC.htm](pathfinder-bestiary-3-items/MfS2aGB9sp6OgznC.htm)|Claw|Garra|modificada|
|[Mfyiq7MrkDXVAE1n.htm](pathfinder-bestiary-3-items/Mfyiq7MrkDXVAE1n.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[MGarveOJoEZEZRDb.htm](pathfinder-bestiary-3-items/MGarveOJoEZEZRDb.htm)|Vine Forest|Bosque de viñas|modificada|
|[mGFhVUVLfuBvJdR9.htm](pathfinder-bestiary-3-items/mGFhVUVLfuBvJdR9.htm)|Fist|Puño|modificada|
|[MgZCPERomWYyl1Nh.htm](pathfinder-bestiary-3-items/MgZCPERomWYyl1Nh.htm)|Inspire Envoy|Inspire Envoy|modificada|
|[MhafIYjoiiUIlHV9.htm](pathfinder-bestiary-3-items/MhafIYjoiiUIlHV9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MHEumHsZqQqW917y.htm](pathfinder-bestiary-3-items/MHEumHsZqQqW917y.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[MhmAMaJ3NhpSRreS.htm](pathfinder-bestiary-3-items/MhmAMaJ3NhpSRreS.htm)|Girtablilu Venom|Girtablilu Venom|modificada|
|[MHWhouO72rbCG3vP.htm](pathfinder-bestiary-3-items/MHWhouO72rbCG3vP.htm)|Whip Tail|Cola de Látigo|modificada|
|[MIgppLmIY2AofKTC.htm](pathfinder-bestiary-3-items/MIgppLmIY2AofKTC.htm)|Swarming Chimes|Swarming Chimes|modificada|
|[Mj3sDpLmMbVTOv59.htm](pathfinder-bestiary-3-items/Mj3sDpLmMbVTOv59.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[MjldvRpasuQZnNAa.htm](pathfinder-bestiary-3-items/MjldvRpasuQZnNAa.htm)|Absorbed Language|Absorbed Language|modificada|
|[mjNu7tTMIDpyD02w.htm](pathfinder-bestiary-3-items/mjNu7tTMIDpyD02w.htm)|Foot|Pie|modificada|
|[mjVHIWJqPBBMeBhy.htm](pathfinder-bestiary-3-items/mjVHIWJqPBBMeBhy.htm)|Talon|Talon|modificada|
|[mk57CVzb2MTvBoCX.htm](pathfinder-bestiary-3-items/mk57CVzb2MTvBoCX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[MKArQIqeRcZmRwMf.htm](pathfinder-bestiary-3-items/MKArQIqeRcZmRwMf.htm)|Faith Bound|Faith Bound|modificada|
|[mkOmRandYut3CQzA.htm](pathfinder-bestiary-3-items/mkOmRandYut3CQzA.htm)|Spade|Spade|modificada|
|[mksu92xcObxdYZwg.htm](pathfinder-bestiary-3-items/mksu92xcObxdYZwg.htm)|Rend Faith|Rasgadura|modificada|
|[MLb6AX5Sc9kHDdik.htm](pathfinder-bestiary-3-items/MLb6AX5Sc9kHDdik.htm)|Ranseur|Ranseur|modificada|
|[mLIM2js0SKePsRgt.htm](pathfinder-bestiary-3-items/mLIM2js0SKePsRgt.htm)|Shortsword|Espada corta|modificada|
|[mLT5MDiW1VnsaeKk.htm](pathfinder-bestiary-3-items/mLT5MDiW1VnsaeKk.htm)|Breath Weapon|Breath Weapon|modificada|
|[mM2VhVOnYEHZ9vNy.htm](pathfinder-bestiary-3-items/mM2VhVOnYEHZ9vNy.htm)|Ooze Globule|Ooze Globule|modificada|
|[MN3PR10lW2gEDJPP.htm](pathfinder-bestiary-3-items/MN3PR10lW2gEDJPP.htm)|Swallow Whole|Engullir Todo|modificada|
|[Mn6J9sGKxgEPsSFM.htm](pathfinder-bestiary-3-items/Mn6J9sGKxgEPsSFM.htm)|Breath Weapon|Breath Weapon|modificada|
|[MnCWApQWsNq3LZGf.htm](pathfinder-bestiary-3-items/MnCWApQWsNq3LZGf.htm)|Rapid Evolution|Rapid Evolution|modificada|
|[mnF1BUlA5VyNQsJJ.htm](pathfinder-bestiary-3-items/mnF1BUlA5VyNQsJJ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Mnk4wTPNqIwrijFP.htm](pathfinder-bestiary-3-items/Mnk4wTPNqIwrijFP.htm)|Curse of Darkness|Maldición de la oscuridad|modificada|
|[mnmEm4rMTXMCnOlp.htm](pathfinder-bestiary-3-items/mnmEm4rMTXMCnOlp.htm)|Shuriken|Shuriken|modificada|
|[mNMGvHwMrDdtnVoO.htm](pathfinder-bestiary-3-items/mNMGvHwMrDdtnVoO.htm)|Fast Swallow|Tragar de golpe|modificada|
|[mODjcKhuMNY9CZD6.htm](pathfinder-bestiary-3-items/mODjcKhuMNY9CZD6.htm)|Trample|Trample|modificada|
|[MODzmDCzn3MDbT9X.htm](pathfinder-bestiary-3-items/MODzmDCzn3MDbT9X.htm)|Negative Healing|Curación negativa|modificada|
|[moI2lATWRRsnrF0Z.htm](pathfinder-bestiary-3-items/moI2lATWRRsnrF0Z.htm)|Raise Shields|Alzar escudos|modificada|
|[MoM2zK3F63zlLyW3.htm](pathfinder-bestiary-3-items/MoM2zK3F63zlLyW3.htm)|Venom Spritz|Venom Spritz|modificada|
|[MOMA8yyykZNLQhay.htm](pathfinder-bestiary-3-items/MOMA8yyykZNLQhay.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[MoqxDoRDqZhJhZwN.htm](pathfinder-bestiary-3-items/MoqxDoRDqZhJhZwN.htm)|Troop Movement|Movimiento de tropas|modificada|
|[MOxpvxjnhMCu9QIM.htm](pathfinder-bestiary-3-items/MOxpvxjnhMCu9QIM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mpDohzU1L0FfV4Tr.htm](pathfinder-bestiary-3-items/mpDohzU1L0FfV4Tr.htm)|Toxic Blood|Sangre Tóxica|modificada|
|[MpmCmnbV6vrc9yPG.htm](pathfinder-bestiary-3-items/MpmCmnbV6vrc9yPG.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[MPnS8zod0eSdIWLP.htm](pathfinder-bestiary-3-items/MPnS8zod0eSdIWLP.htm)|Mist Vision|Mist Vision|modificada|
|[Mq1buiTxEF2rhIx8.htm](pathfinder-bestiary-3-items/Mq1buiTxEF2rhIx8.htm)|Weep|Weep|modificada|
|[MqBEcHEyj2aKh7uO.htm](pathfinder-bestiary-3-items/MqBEcHEyj2aKh7uO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mqJmVUehvZOB9E80.htm](pathfinder-bestiary-3-items/mqJmVUehvZOB9E80.htm)|Null Spirit|Espíritu Nulo|modificada|
|[mqyNB4TXgzDtKJ3Q.htm](pathfinder-bestiary-3-items/mqyNB4TXgzDtKJ3Q.htm)|Fed by Earth|Alimentado por la Tierra|modificada|
|[mqzpWjry4YKks5Hz.htm](pathfinder-bestiary-3-items/mqzpWjry4YKks5Hz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mR1hECOdG4ihzUpu.htm](pathfinder-bestiary-3-items/mR1hECOdG4ihzUpu.htm)|Drink Blood|Beber Sangre|modificada|
|[mr3JOEATvs2okn6i.htm](pathfinder-bestiary-3-items/mr3JOEATvs2okn6i.htm)|Slippery Grease|Grasa resbaladiza|modificada|
|[MRHUn0OVB4n1HA2L.htm](pathfinder-bestiary-3-items/MRHUn0OVB4n1HA2L.htm)|Sudden Charge|Carga súbita|modificada|
|[MS8VRrrPU2Us5eB9.htm](pathfinder-bestiary-3-items/MS8VRrrPU2Us5eB9.htm)|Shield Block|Bloquear con escudo|modificada|
|[MtxKrGdtCxm0wczN.htm](pathfinder-bestiary-3-items/MtxKrGdtCxm0wczN.htm)|Phase Lurch|Phase Lurch|modificada|
|[MUIAw06V4IdebBMA.htm](pathfinder-bestiary-3-items/MUIAw06V4IdebBMA.htm)|Construct Armor (Hardness 8)|Construir Armadura (Dureza 8)|modificada|
|[MUxEI1u92ZQGjdB8.htm](pathfinder-bestiary-3-items/MUxEI1u92ZQGjdB8.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[MvzDVPMtmss6IZMj.htm](pathfinder-bestiary-3-items/MvzDVPMtmss6IZMj.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[mw396KKLKWJKNSqQ.htm](pathfinder-bestiary-3-items/mw396KKLKWJKNSqQ.htm)|Jaws|Fauces|modificada|
|[MWJz2HDAK5IGRX8W.htm](pathfinder-bestiary-3-items/MWJz2HDAK5IGRX8W.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MWlueD3tLQnzSbjz.htm](pathfinder-bestiary-3-items/MWlueD3tLQnzSbjz.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[mx7M2JUGpqZUL4LJ.htm](pathfinder-bestiary-3-items/mx7M2JUGpqZUL4LJ.htm)|Projectile Vomit|Projectile Vomit|modificada|
|[mX7yAH39yBUykxli.htm](pathfinder-bestiary-3-items/mX7yAH39yBUykxli.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MYdtzKzZtoPS3j70.htm](pathfinder-bestiary-3-items/MYdtzKzZtoPS3j70.htm)|Tail|Tail|modificada|
|[mYKiwGxlqb75Vo6G.htm](pathfinder-bestiary-3-items/mYKiwGxlqb75Vo6G.htm)|Corrupting Touch|Corrupting Touch|modificada|
|[Mynf5Z4xbH7kVwpi.htm](pathfinder-bestiary-3-items/Mynf5Z4xbH7kVwpi.htm)|Spit|Escupe|modificada|
|[MYOFzFbpGCWFCD85.htm](pathfinder-bestiary-3-items/MYOFzFbpGCWFCD85.htm)|Constrict|Restringir|modificada|
|[MzHIjOVqhOXnluws.htm](pathfinder-bestiary-3-items/MzHIjOVqhOXnluws.htm)|Extinguishing Aversion|Extinguiendo la aversión|modificada|
|[MzIjuOCLnKBWgj5w.htm](pathfinder-bestiary-3-items/MzIjuOCLnKBWgj5w.htm)|Claw|Garra|modificada|
|[MZrswDwhii9LR4mL.htm](pathfinder-bestiary-3-items/MZrswDwhii9LR4mL.htm)|-2 to All Saves vs. Emotion Effects|-2 a Todas las Salvaciones contra Efectos de Emoción.|modificada|
|[n0IdU8aHDRaDIoOA.htm](pathfinder-bestiary-3-items/n0IdU8aHDRaDIoOA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[N0j4xFqLJ9FtXRAP.htm](pathfinder-bestiary-3-items/N0j4xFqLJ9FtXRAP.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[N0L60ChJeeVdQogn.htm](pathfinder-bestiary-3-items/N0L60ChJeeVdQogn.htm)|All-Around Vision|All-Around Vision|modificada|
|[N0n5SsuqfJHWLi1E.htm](pathfinder-bestiary-3-items/N0n5SsuqfJHWLi1E.htm)|Claw|Garra|modificada|
|[N0pERoSKEmaLOJSB.htm](pathfinder-bestiary-3-items/N0pERoSKEmaLOJSB.htm)|Leech Essence|Esencia de sanguijuela|modificada|
|[n249UcrNORZw28Hg.htm](pathfinder-bestiary-3-items/n249UcrNORZw28Hg.htm)|Swarm Mind|Swarm Mind|modificada|
|[N2oBeSLsFrTuwOTn.htm](pathfinder-bestiary-3-items/N2oBeSLsFrTuwOTn.htm)|Frightening Flurry|Ráfaga Asustadora|modificada|
|[N2Qsn1VaKlDp82Ga.htm](pathfinder-bestiary-3-items/N2Qsn1VaKlDp82Ga.htm)|Scalding Oil|Aceite Escaldante|modificada|
|[N4NhBjgFg8WdpS84.htm](pathfinder-bestiary-3-items/N4NhBjgFg8WdpS84.htm)|Negative Healing|Curación negativa|modificada|
|[N6z6E7Ly3t7eFBoU.htm](pathfinder-bestiary-3-items/N6z6E7Ly3t7eFBoU.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[N7bXlw2VkUQPBHUy.htm](pathfinder-bestiary-3-items/N7bXlw2VkUQPBHUy.htm)|Scorch|Scorch|modificada|
|[n7LgURaW8hihxzYn.htm](pathfinder-bestiary-3-items/n7LgURaW8hihxzYn.htm)|Phalanx Charge|Phalanx Charge|modificada|
|[n7ZxA7v0OJE04AGq.htm](pathfinder-bestiary-3-items/n7ZxA7v0OJE04AGq.htm)|Frightful Presence|Frightful Presence|modificada|
|[n8MOp28YVOpx7Eoq.htm](pathfinder-bestiary-3-items/n8MOp28YVOpx7Eoq.htm)|Flit Back|Flit Back|modificada|
|[N8Z7KBCRIUBXL2Gu.htm](pathfinder-bestiary-3-items/N8Z7KBCRIUBXL2Gu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[n96wAvXJdhT8a2uD.htm](pathfinder-bestiary-3-items/n96wAvXJdhT8a2uD.htm)|Claw|Garra|modificada|
|[N9fBVLGGH7iHDjIs.htm](pathfinder-bestiary-3-items/N9fBVLGGH7iHDjIs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[N9uynDS2jAyjfbwR.htm](pathfinder-bestiary-3-items/N9uynDS2jAyjfbwR.htm)|Breath Weapon|Breath Weapon|modificada|
|[Na4ZinpZPOodP1WY.htm](pathfinder-bestiary-3-items/Na4ZinpZPOodP1WY.htm)|Blighted Footfalls|Blighted Footfalls|modificada|
|[NAngdrs8j255ZSod.htm](pathfinder-bestiary-3-items/NAngdrs8j255ZSod.htm)|Steal Memories|Sustraer Recuerdos|modificada|
|[NaTbdbqoxFlKljoH.htm](pathfinder-bestiary-3-items/NaTbdbqoxFlKljoH.htm)|Tremorsense (Precise) 40 feet, (Imprecise) 80 feet|Sentido del Temblor (Precisión) 40 pies, (Impreciso) 80 pies.|modificada|
|[NbGq64tR4svPBmpM.htm](pathfinder-bestiary-3-items/NbGq64tR4svPBmpM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Nc31MydJOBHcqpyb.htm](pathfinder-bestiary-3-items/Nc31MydJOBHcqpyb.htm)|Divine Dispelling|Disipación divina|modificada|
|[NCFt2ltSfGTesan4.htm](pathfinder-bestiary-3-items/NCFt2ltSfGTesan4.htm)|-2 to All Saves vs. Emotion Effects|-2 a Todas las Salvaciones contra Efectos de Emoción.|modificada|
|[ncklibzh6Dfp2LTc.htm](pathfinder-bestiary-3-items/ncklibzh6Dfp2LTc.htm)|Shield Block|Bloquear con escudo|modificada|
|[nCkzzJiycKKevhSG.htm](pathfinder-bestiary-3-items/nCkzzJiycKKevhSG.htm)|Countered by Water|Contrarrestado por Agua|modificada|
|[ncL6q9VyNef5Kfov.htm](pathfinder-bestiary-3-items/ncL6q9VyNef5Kfov.htm)|Interpose|Interponer|modificada|
|[NCoWkgKyY8nUdbrO.htm](pathfinder-bestiary-3-items/NCoWkgKyY8nUdbrO.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[NcpXTQwFwbyulXQM.htm](pathfinder-bestiary-3-items/NcpXTQwFwbyulXQM.htm)|Claw|Garra|modificada|
|[NDkFem8B3XVJXQkP.htm](pathfinder-bestiary-3-items/NDkFem8B3XVJXQkP.htm)|Cecaelia Jet|Cecaelia Jet|modificada|
|[ndQeCjr03ZZsI5o1.htm](pathfinder-bestiary-3-items/ndQeCjr03ZZsI5o1.htm)|Memory Maelstrom|Memory Maelstrom|modificada|
|[nEE49kEL2IsSaPcv.htm](pathfinder-bestiary-3-items/nEE49kEL2IsSaPcv.htm)|Green Grab|Agarre Verde|modificada|
|[nEHCDw3LRtrCnVMz.htm](pathfinder-bestiary-3-items/nEHCDw3LRtrCnVMz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[neSR5XCiVlUBQSvT.htm](pathfinder-bestiary-3-items/neSR5XCiVlUBQSvT.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[NewHXtJk8yORbKND.htm](pathfinder-bestiary-3-items/NewHXtJk8yORbKND.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[NFIsLlfq5D02gNol.htm](pathfinder-bestiary-3-items/NFIsLlfq5D02gNol.htm)|Grab|Agarrado|modificada|
|[Ng4z9p0PvzMzN8Di.htm](pathfinder-bestiary-3-items/Ng4z9p0PvzMzN8Di.htm)|Kinsense|Kinsense|modificada|
|[NgeIHogL6m58BKDp.htm](pathfinder-bestiary-3-items/NgeIHogL6m58BKDp.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[ngkPL0GL8aCtZQ2t.htm](pathfinder-bestiary-3-items/ngkPL0GL8aCtZQ2t.htm)|Flaming Sword|Espada Flamígera|modificada|
|[nH4VBesV9iFOfCnq.htm](pathfinder-bestiary-3-items/nH4VBesV9iFOfCnq.htm)|Ink Cloud|Nube de Tinta|modificada|
|[nHC6WGYqix3arWBf.htm](pathfinder-bestiary-3-items/nHC6WGYqix3arWBf.htm)|Feed on Sorrow|Alimentarse de Pena|modificada|
|[nhlXnY0WEq9xwOO7.htm](pathfinder-bestiary-3-items/nhlXnY0WEq9xwOO7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NHqBLwHkFRahYskc.htm](pathfinder-bestiary-3-items/NHqBLwHkFRahYskc.htm)|Telepathy 30 feet|Telepatía 30 piesía.|modificada|
|[NHrIQILaWSM6emU0.htm](pathfinder-bestiary-3-items/NHrIQILaWSM6emU0.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[nIiLVQf3htOx2fUg.htm](pathfinder-bestiary-3-items/nIiLVQf3htOx2fUg.htm)|Grab|Agarrado|modificada|
|[niyS4ePBUKlm1nbJ.htm](pathfinder-bestiary-3-items/niyS4ePBUKlm1nbJ.htm)|Frightful Presence|Frightful Presence|modificada|
|[NizhFg0yQ725ETUd.htm](pathfinder-bestiary-3-items/NizhFg0yQ725ETUd.htm)|Titanic Grasp|Titanic Grasp|modificada|
|[njHt74BnuaBXWlru.htm](pathfinder-bestiary-3-items/njHt74BnuaBXWlru.htm)|Claw|Garra|modificada|
|[nJj3OATPJmrNMM3y.htm](pathfinder-bestiary-3-items/nJj3OATPJmrNMM3y.htm)|Inflating Rush|Inflating Embestida|modificada|
|[NJm2VdVQrwDn112D.htm](pathfinder-bestiary-3-items/NJm2VdVQrwDn112D.htm)|Shadow Shift|Sombra Cambiante|modificada|
|[NKqgBeuuqEtFMoGN.htm](pathfinder-bestiary-3-items/NKqgBeuuqEtFMoGN.htm)|Dual Mind|Mente Dual|modificada|
|[NlBTftnU3oQdbjtw.htm](pathfinder-bestiary-3-items/NlBTftnU3oQdbjtw.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[nllsy0skrldw3leB.htm](pathfinder-bestiary-3-items/nllsy0skrldw3leB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NLwKm20SkugAa88n.htm](pathfinder-bestiary-3-items/NLwKm20SkugAa88n.htm)|Feign Death|Fingir Muerte|modificada|
|[NmGeqt2BxwfGod5Z.htm](pathfinder-bestiary-3-items/NmGeqt2BxwfGod5Z.htm)|Form Up|Form Up|modificada|
|[nmzcowdCvUQCrcqI.htm](pathfinder-bestiary-3-items/nmzcowdCvUQCrcqI.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[Nn5E53ud3JgH2R3T.htm](pathfinder-bestiary-3-items/Nn5E53ud3JgH2R3T.htm)|Champion Focus Spells|Conjuros de foco de campeón|modificada|
|[nNhyC2WwCF4L4Lhd.htm](pathfinder-bestiary-3-items/nNhyC2WwCF4L4Lhd.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[nNJH2B7s8hfn3Tci.htm](pathfinder-bestiary-3-items/nNJH2B7s8hfn3Tci.htm)|Negative Healing|Curación negativa|modificada|
|[NO4hZxcJ6X16f4Og.htm](pathfinder-bestiary-3-items/NO4hZxcJ6X16f4Og.htm)|Change Shape|Change Shape|modificada|
|[NO59VkQbOQIqZLrI.htm](pathfinder-bestiary-3-items/NO59VkQbOQIqZLrI.htm)|Saturated|Saturated|modificada|
|[np15xTTViDna2Ngj.htm](pathfinder-bestiary-3-items/np15xTTViDna2Ngj.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Np1YXLEjDe9xa1Qh.htm](pathfinder-bestiary-3-items/Np1YXLEjDe9xa1Qh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NqfZq31gduTX8PMN.htm](pathfinder-bestiary-3-items/NqfZq31gduTX8PMN.htm)|Thorn|Espina|modificada|
|[nr5RYRA0lNXTCCfR.htm](pathfinder-bestiary-3-items/nr5RYRA0lNXTCCfR.htm)|Ram Charge|Ram Charge|modificada|
|[nRcZT1xJaPAlURaU.htm](pathfinder-bestiary-3-items/nRcZT1xJaPAlURaU.htm)|Even Now?|¿Incluso ahora?|modificada|
|[NrIJRYd3U38RpP6w.htm](pathfinder-bestiary-3-items/NrIJRYd3U38RpP6w.htm)|Fool's Gold|Fool's Gold|modificada|
|[Nsjcg03W4x2ElnCc.htm](pathfinder-bestiary-3-items/Nsjcg03W4x2ElnCc.htm)|Jaws|Fauces|modificada|
|[Nskz1Z4716xdGyZy.htm](pathfinder-bestiary-3-items/Nskz1Z4716xdGyZy.htm)|Rock|Roca|modificada|
|[NsmFyn1MUaCrkX52.htm](pathfinder-bestiary-3-items/NsmFyn1MUaCrkX52.htm)|Frozen Weapons|Armas Congeladas|modificada|
|[NsMzxbtDP9JAooC6.htm](pathfinder-bestiary-3-items/NsMzxbtDP9JAooC6.htm)|Bond in Light|Lazo en Luz|modificada|
|[NsrDJM8cD8NU8iJt.htm](pathfinder-bestiary-3-items/NsrDJM8cD8NU8iJt.htm)|Grioth Venom|Grioth Venom|modificada|
|[nTIiE8RjvvPcUYEK.htm](pathfinder-bestiary-3-items/nTIiE8RjvvPcUYEK.htm)|Rend|Rasgadura|modificada|
|[NtoApZINNb0XOSCK.htm](pathfinder-bestiary-3-items/NtoApZINNb0XOSCK.htm)|Positive Energy Transfer|Transferencia de energía positiva|modificada|
|[nul98z8Tuin8RNul.htm](pathfinder-bestiary-3-items/nul98z8Tuin8RNul.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NunhObJDovYnRhq5.htm](pathfinder-bestiary-3-items/NunhObJDovYnRhq5.htm)|Claw|Garra|modificada|
|[nVF38zJQgYheOlaQ.htm](pathfinder-bestiary-3-items/nVF38zJQgYheOlaQ.htm)|Snatch Skull|Arrebatar Cráneo|modificada|
|[Nvg6G4rwFK7be0Td.htm](pathfinder-bestiary-3-items/Nvg6G4rwFK7be0Td.htm)|Claw|Garra|modificada|
|[nVOb5B6Sn8JM7NDn.htm](pathfinder-bestiary-3-items/nVOb5B6Sn8JM7NDn.htm)|Constant Spells|Constant Spells|modificada|
|[NvRpRxRlhF2Xz2bB.htm](pathfinder-bestiary-3-items/NvRpRxRlhF2Xz2bB.htm)|Touch|Toque|modificada|
|[Nwuz4zQLB3u2ujwO.htm](pathfinder-bestiary-3-items/Nwuz4zQLB3u2ujwO.htm)|Tail|Tail|modificada|
|[NWXRpTU3jhjVFz93.htm](pathfinder-bestiary-3-items/NWXRpTU3jhjVFz93.htm)|Death Gasp|Death Gasp|modificada|
|[NX0pJgKM21qtQACy.htm](pathfinder-bestiary-3-items/NX0pJgKM21qtQACy.htm)|Stench|Hedor|modificada|
|[Nxg7iUxwWhxMhySE.htm](pathfinder-bestiary-3-items/Nxg7iUxwWhxMhySE.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[nXjrQb5q4kAXIS61.htm](pathfinder-bestiary-3-items/nXjrQb5q4kAXIS61.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[nXUfkwitBNWhIe8d.htm](pathfinder-bestiary-3-items/nXUfkwitBNWhIe8d.htm)|Fossilization|Fosilización|modificada|
|[NxYSa8z9vgTVhuCT.htm](pathfinder-bestiary-3-items/NxYSa8z9vgTVhuCT.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[NY9oPlAKUHSpnJ52.htm](pathfinder-bestiary-3-items/NY9oPlAKUHSpnJ52.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NYACujLILqXuexgZ.htm](pathfinder-bestiary-3-items/NYACujLILqXuexgZ.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[NZUOo1WmtomF3Pyf.htm](pathfinder-bestiary-3-items/NZUOo1WmtomF3Pyf.htm)|Swarming Gnaw|Swarming Gnaw|modificada|
|[o11nVdIh4C9R8NHS.htm](pathfinder-bestiary-3-items/o11nVdIh4C9R8NHS.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[O1nBRrEFZ3T6AQYn.htm](pathfinder-bestiary-3-items/O1nBRrEFZ3T6AQYn.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[o1xm0AkoMLLKUiqH.htm](pathfinder-bestiary-3-items/o1xm0AkoMLLKUiqH.htm)|Claw|Garra|modificada|
|[O36hrH8CJRLhvwrI.htm](pathfinder-bestiary-3-items/O36hrH8CJRLhvwrI.htm)|Temporal Sense|Sentido temporal|modificada|
|[o3fl8ed8HrHnV97F.htm](pathfinder-bestiary-3-items/o3fl8ed8HrHnV97F.htm)|Longbow|Arco largo|modificada|
|[O5TqNDUtiJmWEbpZ.htm](pathfinder-bestiary-3-items/O5TqNDUtiJmWEbpZ.htm)|Desert Stride|Zancada del Desierto|modificada|
|[O5Ub4Pr4nhdrG6Xy.htm](pathfinder-bestiary-3-items/O5Ub4Pr4nhdrG6Xy.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[o5UbC8XEchUb9DrO.htm](pathfinder-bestiary-3-items/o5UbC8XEchUb9DrO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[O5YGoCAO4nuXA6Pb.htm](pathfinder-bestiary-3-items/O5YGoCAO4nuXA6Pb.htm)|Ganzi Resistance|Resistencia Ganzi|modificada|
|[o681r0ZsSIWthmiQ.htm](pathfinder-bestiary-3-items/o681r0ZsSIWthmiQ.htm)|Nimble Stride|Zancada ágil|modificada|
|[o6pEQo5KbjxyW7Bj.htm](pathfinder-bestiary-3-items/o6pEQo5KbjxyW7Bj.htm)|Trample|Trample|modificada|
|[o754wI61VUPIBAZy.htm](pathfinder-bestiary-3-items/o754wI61VUPIBAZy.htm)|Grab|Agarrado|modificada|
|[O7GCOmC1ewn8Hglr.htm](pathfinder-bestiary-3-items/O7GCOmC1ewn8Hglr.htm)|Claw|Garra|modificada|
|[o7YwycWP8YFceUCM.htm](pathfinder-bestiary-3-items/o7YwycWP8YFceUCM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[o9bdzGDtUTzmMtAv.htm](pathfinder-bestiary-3-items/o9bdzGDtUTzmMtAv.htm)|Tendril|Tendril|modificada|
|[o9gWtjSqFLRnOVk2.htm](pathfinder-bestiary-3-items/o9gWtjSqFLRnOVk2.htm)|Claw|Garra|modificada|
|[O9KVtNjS6f57XAHS.htm](pathfinder-bestiary-3-items/O9KVtNjS6f57XAHS.htm)|Construct Armor (Hardness 14)|Construir Armadura (Dureza 14)|modificada|
|[o9sNXlscXUyhKlOK.htm](pathfinder-bestiary-3-items/o9sNXlscXUyhKlOK.htm)|Blatant Liar|Mentiroso descarado|modificada|
|[oaeEm5dS1vLarcaG.htm](pathfinder-bestiary-3-items/oaeEm5dS1vLarcaG.htm)|Whip Drain|Drenaje del látigo|modificada|
|[oAwLdSvl1lwzYmYb.htm](pathfinder-bestiary-3-items/oAwLdSvl1lwzYmYb.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[OazlOifRnQFSPc2g.htm](pathfinder-bestiary-3-items/OazlOifRnQFSPc2g.htm)|Fire Healing|Curación por fuego|modificada|
|[oB4SNmUD0LdtVMO3.htm](pathfinder-bestiary-3-items/oB4SNmUD0LdtVMO3.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[oB6zbIt1d6nbCIXg.htm](pathfinder-bestiary-3-items/oB6zbIt1d6nbCIXg.htm)|Breath Weapon|Breath Weapon|modificada|
|[obpFkh9f0LWUmFG2.htm](pathfinder-bestiary-3-items/obpFkh9f0LWUmFG2.htm)|Frightful Presence|Frightful Presence|modificada|
|[oBzXzmE3ZsGSx8r0.htm](pathfinder-bestiary-3-items/oBzXzmE3ZsGSx8r0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OC5iW5RWtphm9Mby.htm](pathfinder-bestiary-3-items/OC5iW5RWtphm9Mby.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ocvnX6iZ7cCsqHIf.htm](pathfinder-bestiary-3-items/ocvnX6iZ7cCsqHIf.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[OCyAo1qV9dwYv7Ul.htm](pathfinder-bestiary-3-items/OCyAo1qV9dwYv7Ul.htm)|No Breath|No Breath|modificada|
|[oD3DnNMV4dQa3Gss.htm](pathfinder-bestiary-3-items/oD3DnNMV4dQa3Gss.htm)|Constant Spells|Constant Spells|modificada|
|[ODB8H9ordvDj9MK8.htm](pathfinder-bestiary-3-items/ODB8H9ordvDj9MK8.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[oeXGUL3qzPHQgzrW.htm](pathfinder-bestiary-3-items/oeXGUL3qzPHQgzrW.htm)|Slime Trap|Slime Trap|modificada|
|[OEzKykNunY5znzf7.htm](pathfinder-bestiary-3-items/OEzKykNunY5znzf7.htm)|Claw|Garra|modificada|
|[ofXdILv2twbnZo5Z.htm](pathfinder-bestiary-3-items/ofXdILv2twbnZo5Z.htm)|Claw|Garra|modificada|
|[Og5zTgT13s8QIGgb.htm](pathfinder-bestiary-3-items/Og5zTgT13s8QIGgb.htm)|Ram Charge|Ram Charge|modificada|
|[OgFZ82Cgk6qKkD9l.htm](pathfinder-bestiary-3-items/OgFZ82Cgk6qKkD9l.htm)|Aim as One|Apunta como Uno|modificada|
|[OGIF0iSMoUfXZTlW.htm](pathfinder-bestiary-3-items/OGIF0iSMoUfXZTlW.htm)|Breath Weapon|Breath Weapon|modificada|
|[oGImpwgKAU80AN98.htm](pathfinder-bestiary-3-items/oGImpwgKAU80AN98.htm)|Faceless|Faceless|modificada|
|[OH7aLLnIFKP4SaLG.htm](pathfinder-bestiary-3-items/OH7aLLnIFKP4SaLG.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[ohHcoAMXMCHN6Scv.htm](pathfinder-bestiary-3-items/ohHcoAMXMCHN6Scv.htm)|Beak|Beak|modificada|
|[ohqrq2d7d3If0s76.htm](pathfinder-bestiary-3-items/ohqrq2d7d3If0s76.htm)|Club|Club|modificada|
|[oHw8udhGCS0SJgQT.htm](pathfinder-bestiary-3-items/oHw8udhGCS0SJgQT.htm)|Hold Still|Hold Still|modificada|
|[OhwcTonDsZ7QOZTR.htm](pathfinder-bestiary-3-items/OhwcTonDsZ7QOZTR.htm)|Smoke Vision|Visión de Humo|modificada|
|[OIExxzLzZHeopXKg.htm](pathfinder-bestiary-3-items/OIExxzLzZHeopXKg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OiznKRNhVSCejM5r.htm](pathfinder-bestiary-3-items/OiznKRNhVSCejM5r.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Oj448zcLgfmVIFy1.htm](pathfinder-bestiary-3-items/Oj448zcLgfmVIFy1.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[oj5hseYYTFuq4OYh.htm](pathfinder-bestiary-3-items/oj5hseYYTFuq4OYh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ojGq55t3EY5AHaLi.htm](pathfinder-bestiary-3-items/ojGq55t3EY5AHaLi.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[OjOzwh1AVQ8baTZt.htm](pathfinder-bestiary-3-items/OjOzwh1AVQ8baTZt.htm)|Gleaming Armor|Armadura reluciente|modificada|
|[ojRftryhvZxryakY.htm](pathfinder-bestiary-3-items/ojRftryhvZxryakY.htm)|Godslayer|Godslayer|modificada|
|[oJXCaHxdH67r6d1I.htm](pathfinder-bestiary-3-items/oJXCaHxdH67r6d1I.htm)|Inhabit Vessel|Habitar Nave|modificada|
|[OK5mcs3GjpZRifx9.htm](pathfinder-bestiary-3-items/OK5mcs3GjpZRifx9.htm)|Jaws|Fauces|modificada|
|[okbEN7zWQ3xJAzkg.htm](pathfinder-bestiary-3-items/okbEN7zWQ3xJAzkg.htm)|Jaws|Fauces|modificada|
|[Ol5nO6bgZS5cJIkA.htm](pathfinder-bestiary-3-items/Ol5nO6bgZS5cJIkA.htm)|Constant Spells|Constant Spells|modificada|
|[OLXpGW8u6XmpJXAp.htm](pathfinder-bestiary-3-items/OLXpGW8u6XmpJXAp.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[oLyWvtqHGoL3rQb1.htm](pathfinder-bestiary-3-items/oLyWvtqHGoL3rQb1.htm)|Constant Spells|Constant Spells|modificada|
|[oM4VdHuJNfDqEoHR.htm](pathfinder-bestiary-3-items/oM4VdHuJNfDqEoHR.htm)|Anchor|Ancla|modificada|
|[oMZGYxkqoirlCaQm.htm](pathfinder-bestiary-3-items/oMZGYxkqoirlCaQm.htm)|Shortsword|Espada corta|modificada|
|[ON9shjsp6vVBXsO0.htm](pathfinder-bestiary-3-items/ON9shjsp6vVBXsO0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OnmLcXNYU1Jxp5ji.htm](pathfinder-bestiary-3-items/OnmLcXNYU1Jxp5ji.htm)|Bubonic Plague|Bubonic Plague|modificada|
|[oNP92kbO07u3ivGK.htm](pathfinder-bestiary-3-items/oNP92kbO07u3ivGK.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Oo1ff4ERhvPS7Bhw.htm](pathfinder-bestiary-3-items/Oo1ff4ERhvPS7Bhw.htm)|Frightful Presence|Frightful Presence|modificada|
|[ooPhbtvdtMFxysJe.htm](pathfinder-bestiary-3-items/ooPhbtvdtMFxysJe.htm)|Evasion|Evasión|modificada|
|[OoyUDyw74uN0JNYI.htm](pathfinder-bestiary-3-items/OoyUDyw74uN0JNYI.htm)|Regeneration 40 (Deactivated by Positive, Mental, or Orichalcum)|Regeneración 40 (Desactivado por Positivo, Mental u Orichalcum)|modificada|
|[oPdEp3kAGptn9Bkh.htm](pathfinder-bestiary-3-items/oPdEp3kAGptn9Bkh.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[oPF7KkI27o6FCE0l.htm](pathfinder-bestiary-3-items/oPF7KkI27o6FCE0l.htm)|Improved Grab|Agarrado mejorado|modificada|
|[Ophbfa4MbDPldjGG.htm](pathfinder-bestiary-3-items/Ophbfa4MbDPldjGG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OPww9QAdYNjhEqoV.htm](pathfinder-bestiary-3-items/OPww9QAdYNjhEqoV.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[oQ1wQPUo6g2iysB1.htm](pathfinder-bestiary-3-items/oQ1wQPUo6g2iysB1.htm)|Jaws|Fauces|modificada|
|[Or20Bb0UzTLxRqoj.htm](pathfinder-bestiary-3-items/Or20Bb0UzTLxRqoj.htm)|Shortbow|Arco corto|modificada|
|[ORnkr2b5BKCvtKXe.htm](pathfinder-bestiary-3-items/ORnkr2b5BKCvtKXe.htm)|Grab|Agarrado|modificada|
|[oRzzljhsCixNqf5j.htm](pathfinder-bestiary-3-items/oRzzljhsCixNqf5j.htm)|Elegant Cane|Bastón Elegante|modificada|
|[osh2zvGEnGifdNKA.htm](pathfinder-bestiary-3-items/osh2zvGEnGifdNKA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OshjLmAWIWTiJZoR.htm](pathfinder-bestiary-3-items/OshjLmAWIWTiJZoR.htm)|Adamantine Claws|Garras Adamantinas|modificada|
|[OSib6ckkzfe3tvbE.htm](pathfinder-bestiary-3-items/OSib6ckkzfe3tvbE.htm)|Enormous|Gigantesca|modificada|
|[OsJRsnBwCYSFdA4G.htm](pathfinder-bestiary-3-items/OsJRsnBwCYSFdA4G.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[OTkxsBzxKFuPWRCX.htm](pathfinder-bestiary-3-items/OTkxsBzxKFuPWRCX.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[oTrOmw914xPkqVd5.htm](pathfinder-bestiary-3-items/oTrOmw914xPkqVd5.htm)|Fist|Puño|modificada|
|[OU34B5fiyPMmjnXO.htm](pathfinder-bestiary-3-items/OU34B5fiyPMmjnXO.htm)|Easy to Call|Fácil de Llamar|modificada|
|[oU5HOhztZ5crg339.htm](pathfinder-bestiary-3-items/oU5HOhztZ5crg339.htm)|Kukri|Kukri|modificada|
|[ou7GQNYw2dnoJPZW.htm](pathfinder-bestiary-3-items/ou7GQNYw2dnoJPZW.htm)|Greatsword|Greatsword|modificada|
|[oUqggV8Z8uzpYdNA.htm](pathfinder-bestiary-3-items/oUqggV8Z8uzpYdNA.htm)|Instrument of Retribution|Instrumento de Retribución|modificada|
|[oveFRPbhF3sXi9RJ.htm](pathfinder-bestiary-3-items/oveFRPbhF3sXi9RJ.htm)|Ephemeral Claw|Garra efímera|modificada|
|[ovf1EYDzh0K1JNl2.htm](pathfinder-bestiary-3-items/ovf1EYDzh0K1JNl2.htm)|Ward|Ward|modificada|
|[oVjrC7X48FBIjCug.htm](pathfinder-bestiary-3-items/oVjrC7X48FBIjCug.htm)|Breath Weapon|Breath Weapon|modificada|
|[oVMhwSHesHUR8PLW.htm](pathfinder-bestiary-3-items/oVMhwSHesHUR8PLW.htm)|Unluck Aura|Aura de mala suerte|modificada|
|[OvoJR0fdN07ZLFYR.htm](pathfinder-bestiary-3-items/OvoJR0fdN07ZLFYR.htm)|Attack of Opportunity (Stinger Only)|Ataque de oportunidad (sólo aguijón).|modificada|
|[Ovtg8L0Jkr5t6b4n.htm](pathfinder-bestiary-3-items/Ovtg8L0Jkr5t6b4n.htm)|Blazing Admonition|Blazing Admonition|modificada|
|[OVXjqkURq28dcMFO.htm](pathfinder-bestiary-3-items/OVXjqkURq28dcMFO.htm)|Negative Healing|Curación negativa|modificada|
|[OW9oEttpg9WFvgOt.htm](pathfinder-bestiary-3-items/OW9oEttpg9WFvgOt.htm)|Constant Spells|Constant Spells|modificada|
|[OWa41XZ0GByISijn.htm](pathfinder-bestiary-3-items/OWa41XZ0GByISijn.htm)|Spear|Lanza|modificada|
|[oWdMjEIfQss7DdS1.htm](pathfinder-bestiary-3-items/oWdMjEIfQss7DdS1.htm)|Jaws|Fauces|modificada|
|[oWFVODhnr16hmGjN.htm](pathfinder-bestiary-3-items/oWFVODhnr16hmGjN.htm)|Constant Spells|Constant Spells|modificada|
|[OwP8GUdLEkFwmgjL.htm](pathfinder-bestiary-3-items/OwP8GUdLEkFwmgjL.htm)|Champion Focus Spells|Conjuros de foco de campeón|modificada|
|[OwpDVJif5SRqFrSS.htm](pathfinder-bestiary-3-items/OwpDVJif5SRqFrSS.htm)|Tail|Tail|modificada|
|[OwZj80arbVgN8YC9.htm](pathfinder-bestiary-3-items/OwZj80arbVgN8YC9.htm)|-1 to All Saves vs. Emotion Effects|-1 a Todas las Salvaciones contra Efectos de Emoción|modificada|
|[oX5mS0nonoT9j23k.htm](pathfinder-bestiary-3-items/oX5mS0nonoT9j23k.htm)|Paint|Paint|modificada|
|[oX8m7VQu9DO6VFnp.htm](pathfinder-bestiary-3-items/oX8m7VQu9DO6VFnp.htm)|Boneshard Burst|Boneshard Burst|modificada|
|[oxJLEKIfZkDETCv8.htm](pathfinder-bestiary-3-items/oxJLEKIfZkDETCv8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oXM5aJ0mHfq2fGWh.htm](pathfinder-bestiary-3-items/oXM5aJ0mHfq2fGWh.htm)|Javelin|Javelin|modificada|
|[OYZDy9EFgKjBC4ZW.htm](pathfinder-bestiary-3-items/OYZDy9EFgKjBC4ZW.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[ozof8R0i6Gx0GTeA.htm](pathfinder-bestiary-3-items/ozof8R0i6Gx0GTeA.htm)|Fist|Puño|modificada|
|[oZq1LEjJ1S0dXNc2.htm](pathfinder-bestiary-3-items/oZq1LEjJ1S0dXNc2.htm)|Change Shape|Change Shape|modificada|
|[ozywltVfeeRvEgor.htm](pathfinder-bestiary-3-items/ozywltVfeeRvEgor.htm)|Eel Jaws|Fauces de Anguila|modificada|
|[p0P37OZT7ovYmUkB.htm](pathfinder-bestiary-3-items/p0P37OZT7ovYmUkB.htm)|Canine Vulnerability|Vulnerabilidad Canina|modificada|
|[p3DOduEPOUUeLaxK.htm](pathfinder-bestiary-3-items/p3DOduEPOUUeLaxK.htm)|Grab|Agarrado|modificada|
|[P3e58ycpovGthuDe.htm](pathfinder-bestiary-3-items/P3e58ycpovGthuDe.htm)|Scent (Imprecise) 40 feet|Olor (Impreciso) 40 pies|modificada|
|[p3v8D49u0adS76qw.htm](pathfinder-bestiary-3-items/p3v8D49u0adS76qw.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[p4F4jzZzQp6LGCex.htm](pathfinder-bestiary-3-items/p4F4jzZzQp6LGCex.htm)|Smother|Asfixiar|modificada|
|[p4RYgQW9q1rXPgz5.htm](pathfinder-bestiary-3-items/p4RYgQW9q1rXPgz5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[p58AYJyyu8cSum0T.htm](pathfinder-bestiary-3-items/p58AYJyyu8cSum0T.htm)|Grab|Agarrado|modificada|
|[p5i4xSC4BGMzlqUF.htm](pathfinder-bestiary-3-items/p5i4xSC4BGMzlqUF.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[p5iUKuZYPVDb0i3z.htm](pathfinder-bestiary-3-items/p5iUKuZYPVDb0i3z.htm)|Sumbreiva Huntblade|Sumbreiva Huntblade|modificada|
|[P6lMDdeaBGEoO8lA.htm](pathfinder-bestiary-3-items/P6lMDdeaBGEoO8lA.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[p6oPHL2T4fZfMivY.htm](pathfinder-bestiary-3-items/p6oPHL2T4fZfMivY.htm)|Jaws|Fauces|modificada|
|[p6uToNU7wgFDgDDH.htm](pathfinder-bestiary-3-items/p6uToNU7wgFDgDDH.htm)|Sloth|Pereza|modificada|
|[P6yyZcwSDswPX7xS.htm](pathfinder-bestiary-3-items/P6yyZcwSDswPX7xS.htm)|Immortal|Inmortal|modificada|
|[P7PEpIp5LPhybC2C.htm](pathfinder-bestiary-3-items/P7PEpIp5LPhybC2C.htm)|Engulf|Envolver|modificada|
|[P7rE9vi1P70NJ5iD.htm](pathfinder-bestiary-3-items/P7rE9vi1P70NJ5iD.htm)|Plague of Ancients|Plague of Ancients|modificada|
|[p7SnyvGd17jjb0aF.htm](pathfinder-bestiary-3-items/p7SnyvGd17jjb0aF.htm)|Tidal Wave|Maremoto|modificada|
|[p8hPnF6s5wU3KJk3.htm](pathfinder-bestiary-3-items/p8hPnF6s5wU3KJk3.htm)|Claw|Garra|modificada|
|[P8K5a9sUjb1J99U0.htm](pathfinder-bestiary-3-items/P8K5a9sUjb1J99U0.htm)|Talon|Talon|modificada|
|[p931SlqMJE64SPG4.htm](pathfinder-bestiary-3-items/p931SlqMJE64SPG4.htm)|Spirit Tendril|Spirit Tendril|modificada|
|[PA233M8LHOomjH0v.htm](pathfinder-bestiary-3-items/PA233M8LHOomjH0v.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[pA64qfMCuUfGqBOF.htm](pathfinder-bestiary-3-items/pA64qfMCuUfGqBOF.htm)|Amphisbaena Venom|Veneno de Amphisbaena|modificada|
|[paacuYr4AiURwRWW.htm](pathfinder-bestiary-3-items/paacuYr4AiURwRWW.htm)|Tail|Tail|modificada|
|[PB6GFQeZfrHdNRmj.htm](pathfinder-bestiary-3-items/PB6GFQeZfrHdNRmj.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[pBgScXHzKgG3o8iv.htm](pathfinder-bestiary-3-items/pBgScXHzKgG3o8iv.htm)|Oceanic Armor|Armadura Oceánica|modificada|
|[pbSfPZuBX9faeDui.htm](pathfinder-bestiary-3-items/pbSfPZuBX9faeDui.htm)|Horn|Cuerno|modificada|
|[pbwIXTUIrcHbVCd3.htm](pathfinder-bestiary-3-items/pbwIXTUIrcHbVCd3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pCERhTThClxu07sY.htm](pathfinder-bestiary-3-items/pCERhTThClxu07sY.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[pcMLR5ngGmapeJYS.htm](pathfinder-bestiary-3-items/pcMLR5ngGmapeJYS.htm)|Tail|Tail|modificada|
|[pDKSuwaiL7A6j7D7.htm](pathfinder-bestiary-3-items/pDKSuwaiL7A6j7D7.htm)|Jaws|Fauces|modificada|
|[pDpwUF5lNCdUWFQT.htm](pathfinder-bestiary-3-items/pDpwUF5lNCdUWFQT.htm)|Claw|Garra|modificada|
|[PDYDZKTsK9cr0dq7.htm](pathfinder-bestiary-3-items/PDYDZKTsK9cr0dq7.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[pedL6wHN0TYSu6Fd.htm](pathfinder-bestiary-3-items/pedL6wHN0TYSu6Fd.htm)|Spear|Lanza|modificada|
|[pePRN91wJ0Dpe7pM.htm](pathfinder-bestiary-3-items/pePRN91wJ0Dpe7pM.htm)|Urban Chasers|Urban Chasers|modificada|
|[pFDqDZzKZDMzZZk1.htm](pathfinder-bestiary-3-items/pFDqDZzKZDMzZZk1.htm)|Claw|Garra|modificada|
|[PfPbwkrM06fK0tGR.htm](pathfinder-bestiary-3-items/PfPbwkrM06fK0tGR.htm)|Hand|Mano|modificada|
|[PFvXq6fIzpNGeRPv.htm](pathfinder-bestiary-3-items/PFvXq6fIzpNGeRPv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PG2qs05bMZmS8OIt.htm](pathfinder-bestiary-3-items/PG2qs05bMZmS8OIt.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[pgjyNm2rRbnBKZmb.htm](pathfinder-bestiary-3-items/pgjyNm2rRbnBKZmb.htm)|Sudden Manifestation|Manifestación Súbita|modificada|
|[pGmtgv4CyBqJ9cJH.htm](pathfinder-bestiary-3-items/pGmtgv4CyBqJ9cJH.htm)|Stunning Screech|Chillido aturdidor|modificada|
|[pgNaQm9L35oslwOp.htm](pathfinder-bestiary-3-items/pgNaQm9L35oslwOp.htm)|All-Around Vision|All-Around Vision|modificada|
|[PgnWSQrfu9kmcYy1.htm](pathfinder-bestiary-3-items/PgnWSQrfu9kmcYy1.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[pGQDgZ3Sme2JsoNm.htm](pathfinder-bestiary-3-items/pGQDgZ3Sme2JsoNm.htm)|Clutches|Embragues|modificada|
|[pgVVTGf6DwqRarDp.htm](pathfinder-bestiary-3-items/pgVVTGf6DwqRarDp.htm)|Wolfrime|Wolfrime|modificada|
|[phH8RreUZhjjwiuo.htm](pathfinder-bestiary-3-items/phH8RreUZhjjwiuo.htm)|Body|Cuerpo|modificada|
|[Phi18mvpdKJpQmql.htm](pathfinder-bestiary-3-items/Phi18mvpdKJpQmql.htm)|Phantom Horn|Phantom Horn|modificada|
|[pHOf2i3tNwpIeAHs.htm](pathfinder-bestiary-3-items/pHOf2i3tNwpIeAHs.htm)|Frightful Presence|Frightful Presence|modificada|
|[phtWMfKuIsMCi5cE.htm](pathfinder-bestiary-3-items/phtWMfKuIsMCi5cE.htm)|Tongue Reposition|Reposición de la lengua|modificada|
|[PhxCt9NDQTWqfCU5.htm](pathfinder-bestiary-3-items/PhxCt9NDQTWqfCU5.htm)|Eviscerate|Eviscerar|modificada|
|[PHZ0zixmQt2ljkJm.htm](pathfinder-bestiary-3-items/PHZ0zixmQt2ljkJm.htm)|Entrails|Entrails|modificada|
|[pIoRWbV6ahXe5lvR.htm](pathfinder-bestiary-3-items/pIoRWbV6ahXe5lvR.htm)|Change Shape|Change Shape|modificada|
|[pj0FI4XEhBPiuOmy.htm](pathfinder-bestiary-3-items/pj0FI4XEhBPiuOmy.htm)|Change Shape|Change Shape|modificada|
|[pJKtjGeoTW0LzTPG.htm](pathfinder-bestiary-3-items/pJKtjGeoTW0LzTPG.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[pjtcZhkm20HjFEmn.htm](pathfinder-bestiary-3-items/pjtcZhkm20HjFEmn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pJUrlkv2i5xEz2WO.htm](pathfinder-bestiary-3-items/pJUrlkv2i5xEz2WO.htm)|Star Child|Niño Estrella|modificada|
|[pKaeBB4bJYXTzs69.htm](pathfinder-bestiary-3-items/pKaeBB4bJYXTzs69.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[PKh7s5uQyxm0yPQg.htm](pathfinder-bestiary-3-items/PKh7s5uQyxm0yPQg.htm)|Pliers|Alicates|modificada|
|[pKpXpiLPYdr4VX46.htm](pathfinder-bestiary-3-items/pKpXpiLPYdr4VX46.htm)|Skip Between|Saltar entre|modificada|
|[PLTi8diRg1rhDOR9.htm](pathfinder-bestiary-3-items/PLTi8diRg1rhDOR9.htm)|Trackless Step|Pisada sin rastro|modificada|
|[PlTKFXVlBOQ1pLtX.htm](pathfinder-bestiary-3-items/PlTKFXVlBOQ1pLtX.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[pmhHWb3PBr7rQpE5.htm](pathfinder-bestiary-3-items/pmhHWb3PBr7rQpE5.htm)|Spitfire|Spitfire|modificada|
|[PmLhjqOQdj9Bkm2j.htm](pathfinder-bestiary-3-items/PmLhjqOQdj9Bkm2j.htm)|Drink Oil|Drink Oil|modificada|
|[pnaFRppT92BaCHjj.htm](pathfinder-bestiary-3-items/pnaFRppT92BaCHjj.htm)|Claw|Garra|modificada|
|[pnC70Lg68qGCzyl2.htm](pathfinder-bestiary-3-items/pnC70Lg68qGCzyl2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pnip6OCCdZn9i0pD.htm](pathfinder-bestiary-3-items/pnip6OCCdZn9i0pD.htm)|Countered by Fire|Contrarrestado por Fuego|modificada|
|[PO5OyKjxeT0rYaYl.htm](pathfinder-bestiary-3-items/PO5OyKjxeT0rYaYl.htm)|Jaws|Fauces|modificada|
|[POpEyXnpPzh143yS.htm](pathfinder-bestiary-3-items/POpEyXnpPzh143yS.htm)|Improved Grab|Agarrado mejorado|modificada|
|[POZqpY2nqdOUZBf7.htm](pathfinder-bestiary-3-items/POZqpY2nqdOUZBf7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[pp693fsJOt23L3X9.htm](pathfinder-bestiary-3-items/pp693fsJOt23L3X9.htm)|Shock Mind|Mente Electrizante|modificada|
|[pPPA3LPdsuQ55Gp0.htm](pathfinder-bestiary-3-items/pPPA3LPdsuQ55Gp0.htm)|Counterattack|Contraataque|modificada|
|[PqKDvCgTiNBUGAhF.htm](pathfinder-bestiary-3-items/PqKDvCgTiNBUGAhF.htm)|Coven Spells|Coven Spells|modificada|
|[PrETYXvcaucelz4m.htm](pathfinder-bestiary-3-items/PrETYXvcaucelz4m.htm)|Powerful Leaper|Poderoso Leaper|modificada|
|[pRfIkGoEX8AyxeSw.htm](pathfinder-bestiary-3-items/pRfIkGoEX8AyxeSw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[prTBGYHOcT9eQskj.htm](pathfinder-bestiary-3-items/prTBGYHOcT9eQskj.htm)|Eye Probe|Eye Probe|modificada|
|[pRvVMMMJPM8IsEkp.htm](pathfinder-bestiary-3-items/pRvVMMMJPM8IsEkp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PrWWui76kCsgmSBk.htm](pathfinder-bestiary-3-items/PrWWui76kCsgmSBk.htm)|Flying Wheel|Rueda Voladora|modificada|
|[pRXjlEM0YDK7HrCS.htm](pathfinder-bestiary-3-items/pRXjlEM0YDK7HrCS.htm)|Squirming Embrace|Abrazo Retorcido|modificada|
|[PS82ZXV5QXRxiBXf.htm](pathfinder-bestiary-3-items/PS82ZXV5QXRxiBXf.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ptpsl6OafcvwkgOx.htm](pathfinder-bestiary-3-items/ptpsl6OafcvwkgOx.htm)|Accord Essence|Esencia Accord|modificada|
|[PtvDEVABQH9iJs6d.htm](pathfinder-bestiary-3-items/PtvDEVABQH9iJs6d.htm)|Siphon Magic|Siphon Magic|modificada|
|[PuvbyIbJk14Uho2a.htm](pathfinder-bestiary-3-items/PuvbyIbJk14Uho2a.htm)|Tremorsense 60 feet|Tremorsense 60 pies|modificada|
|[Pv0epZ0IMHJFIRG2.htm](pathfinder-bestiary-3-items/Pv0epZ0IMHJFIRG2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pV1OZsdl0kyBME6Q.htm](pathfinder-bestiary-3-items/pV1OZsdl0kyBME6Q.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[pVxNWlSICm5ij03c.htm](pathfinder-bestiary-3-items/pVxNWlSICm5ij03c.htm)|Grab|Agarrado|modificada|
|[pW9sevYZuAwpWAr3.htm](pathfinder-bestiary-3-items/pW9sevYZuAwpWAr3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PWE9sinIQ5L6H4v2.htm](pathfinder-bestiary-3-items/PWE9sinIQ5L6H4v2.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[pwmnQMrCnI7oQIoE.htm](pathfinder-bestiary-3-items/pwmnQMrCnI7oQIoE.htm)|Jaws|Fauces|modificada|
|[pWvysguPWMKlVIzr.htm](pathfinder-bestiary-3-items/pWvysguPWMKlVIzr.htm)|Raptor Dive|Raptor Dive|modificada|
|[Px9k1rac0p4O9b4J.htm](pathfinder-bestiary-3-items/Px9k1rac0p4O9b4J.htm)|Fist|Puño|modificada|
|[PxEpWsl4UxCVOJxy.htm](pathfinder-bestiary-3-items/PxEpWsl4UxCVOJxy.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[PxHTXBiqvpnvByT4.htm](pathfinder-bestiary-3-items/PxHTXBiqvpnvByT4.htm)|Calculated Blow|Golpe Calculado|modificada|
|[pXlz78EGb6lw6Aij.htm](pathfinder-bestiary-3-items/pXlz78EGb6lw6Aij.htm)|Fist|Puño|modificada|
|[pxvM1CdSpuWlWrQ3.htm](pathfinder-bestiary-3-items/pxvM1CdSpuWlWrQ3.htm)|-1 Status to All Saves vs. Death Effects|-1 situación a todas las salvaciones contra efectos de muerte.|modificada|
|[pYnHQN69rHC0bPm0.htm](pathfinder-bestiary-3-items/pYnHQN69rHC0bPm0.htm)|Longspear|Longspear|modificada|
|[PyxiGz5dlEpJ0SE7.htm](pathfinder-bestiary-3-items/PyxiGz5dlEpJ0SE7.htm)|Deluge|Diluvio|modificada|
|[pZYPf41244FTorQE.htm](pathfinder-bestiary-3-items/pZYPf41244FTorQE.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Q0IYQhi4qttZkUKn.htm](pathfinder-bestiary-3-items/Q0IYQhi4qttZkUKn.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[Q2J6B9C1q7HPAVHY.htm](pathfinder-bestiary-3-items/Q2J6B9C1q7HPAVHY.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[Q345ui1zxYcyOoLX.htm](pathfinder-bestiary-3-items/Q345ui1zxYcyOoLX.htm)|Constant Spells|Constant Spells|modificada|
|[q3uWzNeCqQuLwiIA.htm](pathfinder-bestiary-3-items/q3uWzNeCqQuLwiIA.htm)|Aura of Courage|Aura de valor|modificada|
|[Q49aLd8cKOkjpk9J.htm](pathfinder-bestiary-3-items/Q49aLd8cKOkjpk9J.htm)|Darkvision|Visión en la oscuridad|modificada|
|[q5JXKKVYw0NywPhT.htm](pathfinder-bestiary-3-items/q5JXKKVYw0NywPhT.htm)|Pummeling Charge|Carga demoledora|modificada|
|[Q6AA6pHAaFSGJA1w.htm](pathfinder-bestiary-3-items/Q6AA6pHAaFSGJA1w.htm)|Little Oasis|Pequeño Oasis|modificada|
|[q6bluZPiKPFzD79n.htm](pathfinder-bestiary-3-items/q6bluZPiKPFzD79n.htm)|Frightful Presence|Frightful Presence|modificada|
|[Q6ieY38ZPpXhQz2w.htm](pathfinder-bestiary-3-items/Q6ieY38ZPpXhQz2w.htm)|Telepathy 500 feet|Telepatía 500 pies.|modificada|
|[Q77O3dFWbNc8OyJh.htm](pathfinder-bestiary-3-items/Q77O3dFWbNc8OyJh.htm)|Grab|Agarrado|modificada|
|[Q7Qfg4mcXnQcUmCd.htm](pathfinder-bestiary-3-items/Q7Qfg4mcXnQcUmCd.htm)|Fist|Puño|modificada|
|[Q9b50evzwuGvfkAB.htm](pathfinder-bestiary-3-items/Q9b50evzwuGvfkAB.htm)|Self-Destruct|Autodestrucción|modificada|
|[q9cxVnhnmsCNsS4G.htm](pathfinder-bestiary-3-items/q9cxVnhnmsCNsS4G.htm)|Earthen Fist|Puño de Tierra|modificada|
|[Q9YkvCkVgM00oIy8.htm](pathfinder-bestiary-3-items/Q9YkvCkVgM00oIy8.htm)|Thorn|Espina|modificada|
|[qAXVCcqDcG1w6bek.htm](pathfinder-bestiary-3-items/qAXVCcqDcG1w6bek.htm)|Claw|Garra|modificada|
|[QB0FNEPET64VPGnO.htm](pathfinder-bestiary-3-items/QB0FNEPET64VPGnO.htm)|Greatclub|Greatclub|modificada|
|[QBHI3WhGKrjBNZMq.htm](pathfinder-bestiary-3-items/QBHI3WhGKrjBNZMq.htm)|Hydration|Hidratación|modificada|
|[QbUWrst99xyuP7X9.htm](pathfinder-bestiary-3-items/QbUWrst99xyuP7X9.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[QCpgvjWcAi1GGE3l.htm](pathfinder-bestiary-3-items/QCpgvjWcAi1GGE3l.htm)|Foot|Pie|modificada|
|[QCRkv3Pld8YuyUlE.htm](pathfinder-bestiary-3-items/QCRkv3Pld8YuyUlE.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[QE9gzkXJFMTGYg19.htm](pathfinder-bestiary-3-items/QE9gzkXJFMTGYg19.htm)|Out You Go|Out You Go|modificada|
|[QeDdL4PRsKD6EnrC.htm](pathfinder-bestiary-3-items/QeDdL4PRsKD6EnrC.htm)|Rock|Roca|modificada|
|[qedIdKEDmmqyp8F5.htm](pathfinder-bestiary-3-items/qedIdKEDmmqyp8F5.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[QeGks9jOjqzdzWci.htm](pathfinder-bestiary-3-items/QeGks9jOjqzdzWci.htm)|Bloodsense (Imprecise) 60 feet|Sentido de la Sangre (Impreciso) 60 pies|modificada|
|[qEjTHqZLDRx2o0dJ.htm](pathfinder-bestiary-3-items/qEjTHqZLDRx2o0dJ.htm)|Claw|Garra|modificada|
|[qen9rYoBFDQD84n7.htm](pathfinder-bestiary-3-items/qen9rYoBFDQD84n7.htm)|Cannonade|Cannonade|modificada|
|[QEObXX17C19vzj8L.htm](pathfinder-bestiary-3-items/QEObXX17C19vzj8L.htm)|Tail|Tail|modificada|
|[Qew18sYA4NJJ2n7y.htm](pathfinder-bestiary-3-items/Qew18sYA4NJJ2n7y.htm)|Fed by Water|Alimentado por el agua|modificada|
|[QfgdLYrvrXT358Kh.htm](pathfinder-bestiary-3-items/QfgdLYrvrXT358Kh.htm)|Body|Cuerpo|modificada|
|[QfsNS0isn7JsRZIX.htm](pathfinder-bestiary-3-items/QfsNS0isn7JsRZIX.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[qgbvTpc5c7E9GDGd.htm](pathfinder-bestiary-3-items/qgbvTpc5c7E9GDGd.htm)|Jaws|Fauces|modificada|
|[Qgr1ThnIaqkHRArD.htm](pathfinder-bestiary-3-items/Qgr1ThnIaqkHRArD.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qH5DHTYxShVT5a5y.htm](pathfinder-bestiary-3-items/qH5DHTYxShVT5a5y.htm)|Curse of the Werecrocodile|Maldición del Werecrocodile|modificada|
|[qhMGpfia6l2CmKf0.htm](pathfinder-bestiary-3-items/qhMGpfia6l2CmKf0.htm)|Leech Thought|Pensamiento Sanguijuela|modificada|
|[qhoxtRXXWKvmotj5.htm](pathfinder-bestiary-3-items/qhoxtRXXWKvmotj5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qHZIM3SkMqqfQWWG.htm](pathfinder-bestiary-3-items/qHZIM3SkMqqfQWWG.htm)|Beak|Beak|modificada|
|[qi2lvWIXN1GWDfm1.htm](pathfinder-bestiary-3-items/qi2lvWIXN1GWDfm1.htm)|Entangling Train|Tren de Enredar|modificada|
|[qI7na98jgBlQpNui.htm](pathfinder-bestiary-3-items/qI7na98jgBlQpNui.htm)|Blade Ally|Blade Ally|modificada|
|[qiEzVS822d89t62v.htm](pathfinder-bestiary-3-items/qiEzVS822d89t62v.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[qJi07N1qiZ8im9Ed.htm](pathfinder-bestiary-3-items/qJi07N1qiZ8im9Ed.htm)|Healing Arrow|Flecha curadora|modificada|
|[qjiQvNcg8Xp6U5g6.htm](pathfinder-bestiary-3-items/qjiQvNcg8Xp6U5g6.htm)|Jaws|Fauces|modificada|
|[QJZgoLZp0BfuvxvJ.htm](pathfinder-bestiary-3-items/QJZgoLZp0BfuvxvJ.htm)|Echolocation (Precise) 120 feet|Ecolocalización (precisión) 120 pies.|modificada|
|[qkbQpoQc1isiqlSL.htm](pathfinder-bestiary-3-items/qkbQpoQc1isiqlSL.htm)|Spiritual Rope|Cuerda Espiritual|modificada|
|[qki0wO5XwHHhxpMl.htm](pathfinder-bestiary-3-items/qki0wO5XwHHhxpMl.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qKR5KyDCt5y2Z7A4.htm](pathfinder-bestiary-3-items/qKR5KyDCt5y2Z7A4.htm)|Liquefy|Licuar|modificada|
|[qkU2EydXIrYI5Hxx.htm](pathfinder-bestiary-3-items/qkU2EydXIrYI5Hxx.htm)|Tusk|Tusk|modificada|
|[qLKTHssJqXd4ynva.htm](pathfinder-bestiary-3-items/qLKTHssJqXd4ynva.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qm3E8aZeyjHSz3i8.htm](pathfinder-bestiary-3-items/qm3E8aZeyjHSz3i8.htm)|Rend|Rasgadura|modificada|
|[QNcsHGzGsxzU10k0.htm](pathfinder-bestiary-3-items/QNcsHGzGsxzU10k0.htm)|Consume Spell|Consume Hechizo|modificada|
|[qNUdm1wGl9coLrPP.htm](pathfinder-bestiary-3-items/qNUdm1wGl9coLrPP.htm)|Dagger|Daga|modificada|
|[QOpnCjoiZN5QnY8X.htm](pathfinder-bestiary-3-items/QOpnCjoiZN5QnY8X.htm)|Jaws|Fauces|modificada|
|[qP4bmFVnqKAeVCn1.htm](pathfinder-bestiary-3-items/qP4bmFVnqKAeVCn1.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[QP9beVsoQsi5P8A5.htm](pathfinder-bestiary-3-items/QP9beVsoQsi5P8A5.htm)|Improved Grab|Agarrado mejorado|modificada|
|[qpCzKmCc7RVqEcVK.htm](pathfinder-bestiary-3-items/qpCzKmCc7RVqEcVK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qppBAh5FNr8f4f2X.htm](pathfinder-bestiary-3-items/qppBAh5FNr8f4f2X.htm)|Divine Destruction|Destrucción Divina|modificada|
|[qQZbVChzEGjI5sIk.htm](pathfinder-bestiary-3-items/qQZbVChzEGjI5sIk.htm)|Claw|Garra|modificada|
|[qRW2k1VmeIkTIOB9.htm](pathfinder-bestiary-3-items/qRW2k1VmeIkTIOB9.htm)|Throw Rock|Arrojar roca|modificada|
|[qSCF9pmEsCQejxxw.htm](pathfinder-bestiary-3-items/qSCF9pmEsCQejxxw.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[qSEQHwgppWcWoGDK.htm](pathfinder-bestiary-3-items/qSEQHwgppWcWoGDK.htm)|Tail|Tail|modificada|
|[QSp7Nee2yP67M9zp.htm](pathfinder-bestiary-3-items/QSp7Nee2yP67M9zp.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[qT7gzi3MA0fVfY8a.htm](pathfinder-bestiary-3-items/qT7gzi3MA0fVfY8a.htm)|Grab|Agarrado|modificada|
|[QTLnr9MYtcbjoq6C.htm](pathfinder-bestiary-3-items/QTLnr9MYtcbjoq6C.htm)|Negative Healing|Curación negativa|modificada|
|[QtPVup2bHHIHGyCe.htm](pathfinder-bestiary-3-items/QtPVup2bHHIHGyCe.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[qTVf39ECIPyC5jMa.htm](pathfinder-bestiary-3-items/qTVf39ECIPyC5jMa.htm)|Negative Healing|Curación negativa|modificada|
|[Qu61Gf9rt5ANopIq.htm](pathfinder-bestiary-3-items/Qu61Gf9rt5ANopIq.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[quetxJ36kYJJorBD.htm](pathfinder-bestiary-3-items/quetxJ36kYJJorBD.htm)|Feathered Charge|Feathered Charge|modificada|
|[qUO24kmJdTM3pw3H.htm](pathfinder-bestiary-3-items/qUO24kmJdTM3pw3H.htm)|Jaws|Fauces|modificada|
|[QUp7RYwQLvBoTGp7.htm](pathfinder-bestiary-3-items/QUp7RYwQLvBoTGp7.htm)|Grab|Agarrado|modificada|
|[QuSzkFT66YGAGqLE.htm](pathfinder-bestiary-3-items/QuSzkFT66YGAGqLE.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[qVbkGmZA4J4JEc5c.htm](pathfinder-bestiary-3-items/qVbkGmZA4J4JEc5c.htm)|Temporal Sense|Sentido temporal|modificada|
|[qvJ5SX7zo7TKIP2h.htm](pathfinder-bestiary-3-items/qvJ5SX7zo7TKIP2h.htm)|Toenail Cutter|Cortauñas|modificada|
|[qVqG3qPrNw7cF8D4.htm](pathfinder-bestiary-3-items/qVqG3qPrNw7cF8D4.htm)|Breath Weapon|Breath Weapon|modificada|
|[QvzzSlemey10PLLP.htm](pathfinder-bestiary-3-items/QvzzSlemey10PLLP.htm)|Claw|Garra|modificada|
|[QwKsTc2ps0cjzmnW.htm](pathfinder-bestiary-3-items/QwKsTc2ps0cjzmnW.htm)|Jaws|Fauces|modificada|
|[QxQT0BLNv0boSPAf.htm](pathfinder-bestiary-3-items/QxQT0BLNv0boSPAf.htm)|Abandon Puppet|Abandonar Marioneta|modificada|
|[QXV1JtzUVWMyWGFW.htm](pathfinder-bestiary-3-items/QXV1JtzUVWMyWGFW.htm)|Thoughtsense (Imprecise) 120 feet|Percibir pensamientos (Impreciso) 120 pies|modificada|
|[QyBhco1VMbstqt4y.htm](pathfinder-bestiary-3-items/QyBhco1VMbstqt4y.htm)|Branch|Rama|modificada|
|[qYJ6jUOSJMEwOOuC.htm](pathfinder-bestiary-3-items/qYJ6jUOSJMEwOOuC.htm)|Nibble|Nibble|modificada|
|[Qz5ELLWHhm9W0RD3.htm](pathfinder-bestiary-3-items/Qz5ELLWHhm9W0RD3.htm)|Gnathobase|Gnathobase|modificada|
|[qzIjJHwF4eyLDmOh.htm](pathfinder-bestiary-3-items/qzIjJHwF4eyLDmOh.htm)|Fountain Pen|Pluma estilográfica|modificada|
|[QZPUv0k24Dx8d0JZ.htm](pathfinder-bestiary-3-items/QZPUv0k24Dx8d0JZ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[QZx7vPULEK8wzVbx.htm](pathfinder-bestiary-3-items/QZx7vPULEK8wzVbx.htm)|Void Weapon|Void Weapon|modificada|
|[R0bN0QbVsJDFnHId.htm](pathfinder-bestiary-3-items/R0bN0QbVsJDFnHId.htm)|Dagger|Daga|modificada|
|[r0CnvIkvKsI5FMgI.htm](pathfinder-bestiary-3-items/r0CnvIkvKsI5FMgI.htm)|Thoughtsense (Precise) 120 feet|Percibir pensamientos (precisión) 120 pies.|modificada|
|[R0HLiBnfzTgXGH4w.htm](pathfinder-bestiary-3-items/R0HLiBnfzTgXGH4w.htm)|Staff|Báculo|modificada|
|[R0vde3bCSBW2a8br.htm](pathfinder-bestiary-3-items/R0vde3bCSBW2a8br.htm)|Clinging Bites|Muerdemuerde aferrado|modificada|
|[R1JY8Uw7yAyXK7JQ.htm](pathfinder-bestiary-3-items/R1JY8Uw7yAyXK7JQ.htm)|Sunset Ribbon|Sunset Ribbon|modificada|
|[R2Ck3XzpQHa6qLKl.htm](pathfinder-bestiary-3-items/R2Ck3XzpQHa6qLKl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r3ldx2ZybEAqaKTp.htm](pathfinder-bestiary-3-items/r3ldx2ZybEAqaKTp.htm)|+2 Perception to Sense Motive|+2 Percepción a Sentido Motivo|modificada|
|[r3zwcLhwIxuPvofI.htm](pathfinder-bestiary-3-items/r3zwcLhwIxuPvofI.htm)|Fist|Puño|modificada|
|[r4jLytkNtr1uMewT.htm](pathfinder-bestiary-3-items/r4jLytkNtr1uMewT.htm)|Gleaming Armor|Armadura reluciente|modificada|
|[R4srCUwoLVvI4h6T.htm](pathfinder-bestiary-3-items/R4srCUwoLVvI4h6T.htm)|Negative Healing|Curación negativa|modificada|
|[R4UxTvMjd8XGo0H1.htm](pathfinder-bestiary-3-items/R4UxTvMjd8XGo0H1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[R4Vzee8KxsqTF0Wf.htm](pathfinder-bestiary-3-items/R4Vzee8KxsqTF0Wf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r5rP9a4eNdlmkjiO.htm](pathfinder-bestiary-3-items/r5rP9a4eNdlmkjiO.htm)|Claw|Garra|modificada|
|[r5va25MjWQsL5Nut.htm](pathfinder-bestiary-3-items/r5va25MjWQsL5Nut.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[R6LPamKnpozwfOnm.htm](pathfinder-bestiary-3-items/R6LPamKnpozwfOnm.htm)|Telepathic Ballad|Telepathic Ballad|modificada|
|[R6XHv4H8ZshCgHND.htm](pathfinder-bestiary-3-items/R6XHv4H8ZshCgHND.htm)|Grab|Agarrado|modificada|
|[r9r9sl3qGETAhlH9.htm](pathfinder-bestiary-3-items/r9r9sl3qGETAhlH9.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[r9s9fT1PAiICOzs0.htm](pathfinder-bestiary-3-items/r9s9fT1PAiICOzs0.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[raEWo6pY0qJvkkyn.htm](pathfinder-bestiary-3-items/raEWo6pY0qJvkkyn.htm)|+2 Status to All Saves vs. Poison|+2 situación a todas las salvaciones contra veneno.|modificada|
|[Ram97nW6kaJ9IBc0.htm](pathfinder-bestiary-3-items/Ram97nW6kaJ9IBc0.htm)|Constant Spells|Constant Spells|modificada|
|[rb3UkMRburjTZJpj.htm](pathfinder-bestiary-3-items/rb3UkMRburjTZJpj.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RbIcnu5Fx9eAX5Uq.htm](pathfinder-bestiary-3-items/RbIcnu5Fx9eAX5Uq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RbKBM2RsozHFLA9U.htm](pathfinder-bestiary-3-items/RbKBM2RsozHFLA9U.htm)|Bond with Mortal|Bond with Mortal|modificada|
|[rbM0VDuWa5kOfsbQ.htm](pathfinder-bestiary-3-items/rbM0VDuWa5kOfsbQ.htm)|Tentacle|Tentáculo|modificada|
|[RBrP1ThmfTW2EUCG.htm](pathfinder-bestiary-3-items/RBrP1ThmfTW2EUCG.htm)|Whistling Bones|Whistling Bones|modificada|
|[RcFq3iNSRnikQSug.htm](pathfinder-bestiary-3-items/RcFq3iNSRnikQSug.htm)|Tusk|Tusk|modificada|
|[RcHSgAM2xaq0Lve5.htm](pathfinder-bestiary-3-items/RcHSgAM2xaq0Lve5.htm)|Easily Fascinated|Fácilmente Fascinable|modificada|
|[RcQhtjlnYqR8A8J9.htm](pathfinder-bestiary-3-items/RcQhtjlnYqR8A8J9.htm)|Spray Perfume|Perfume en spray|modificada|
|[RdbeYdVwFLgopJjk.htm](pathfinder-bestiary-3-items/RdbeYdVwFLgopJjk.htm)|Talon|Talon|modificada|
|[RDF08DgCVW9LDvLd.htm](pathfinder-bestiary-3-items/RDF08DgCVW9LDvLd.htm)|Change Shape|Change Shape|modificada|
|[RdGjfkq57iIV43T3.htm](pathfinder-bestiary-3-items/RdGjfkq57iIV43T3.htm)|Telepathy 30 feet|Telepatía 30 piesía.|modificada|
|[RdiP9Lesck92HsdO.htm](pathfinder-bestiary-3-items/RdiP9Lesck92HsdO.htm)|Forest Shape|Forest Shape|modificada|
|[Rdy2GbIHuRoKy7bU.htm](pathfinder-bestiary-3-items/Rdy2GbIHuRoKy7bU.htm)|Fed by Wood|Alimentado por Madera|modificada|
|[Re0uYYA9c4nnaRne.htm](pathfinder-bestiary-3-items/Re0uYYA9c4nnaRne.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Re3laGuS2iEiMxt0.htm](pathfinder-bestiary-3-items/Re3laGuS2iEiMxt0.htm)|Burning Touch|Toque Ardiente|modificada|
|[Re8xzF2naY7y8ZBf.htm](pathfinder-bestiary-3-items/Re8xzF2naY7y8ZBf.htm)|Mauling Rush|Embestida Zarpazo doble|modificada|
|[RE9BxxRzi4ENZrXg.htm](pathfinder-bestiary-3-items/RE9BxxRzi4ENZrXg.htm)|Proboscis Tongue|Lengua Proboscis|modificada|
|[REbcJzGp6sT7jOuZ.htm](pathfinder-bestiary-3-items/REbcJzGp6sT7jOuZ.htm)|Limb|Limb|modificada|
|[rFakD0KrlUr9DJiQ.htm](pathfinder-bestiary-3-items/rFakD0KrlUr9DJiQ.htm)|Claw|Garra|modificada|
|[RFaRYNVP3GPdcMkY.htm](pathfinder-bestiary-3-items/RFaRYNVP3GPdcMkY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RfjRZPiLUrrypXhA.htm](pathfinder-bestiary-3-items/RfjRZPiLUrrypXhA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RfXLcZpuY88IxZTD.htm](pathfinder-bestiary-3-items/RfXLcZpuY88IxZTD.htm)|Catch Rock|Atrapar roca|modificada|
|[RGJbS9RTIb1LL5uF.htm](pathfinder-bestiary-3-items/RGJbS9RTIb1LL5uF.htm)|Swarming Snips|Swarming Snips|modificada|
|[rgq47BGgm0jbIXb6.htm](pathfinder-bestiary-3-items/rgq47BGgm0jbIXb6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rGx3BT8mbRKRUpA2.htm](pathfinder-bestiary-3-items/rGx3BT8mbRKRUpA2.htm)|Destabilizing Field|Campo Desestabilizador|modificada|
|[RIF4K2wDuZCHWToa.htm](pathfinder-bestiary-3-items/RIF4K2wDuZCHWToa.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RIfmoCoHRDlhRlU1.htm](pathfinder-bestiary-3-items/RIfmoCoHRDlhRlU1.htm)|Claw|Garra|modificada|
|[riFooTptrWPMV3tk.htm](pathfinder-bestiary-3-items/riFooTptrWPMV3tk.htm)|+1 Status to All Saves vs. Evil|+1 situación a todas las salvaciones contra el maligno.|modificada|
|[riqBNngN6YkDWYqv.htm](pathfinder-bestiary-3-items/riqBNngN6YkDWYqv.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[rIrMS5z48NzYuh9b.htm](pathfinder-bestiary-3-items/rIrMS5z48NzYuh9b.htm)|Impending Dread|Pavor Inminente|modificada|
|[rjh44a42EWaBBBP1.htm](pathfinder-bestiary-3-items/rjh44a42EWaBBBP1.htm)|Fed by Earth|Alimentado por la Tierra|modificada|
|[rjVHVaa9BN3ZAiKL.htm](pathfinder-bestiary-3-items/rjVHVaa9BN3ZAiKL.htm)|Spike Storm|Tormenta de pinchos|modificada|
|[RKz5iqiz10TiodA6.htm](pathfinder-bestiary-3-items/RKz5iqiz10TiodA6.htm)|Breath Weapon|Breath Weapon|modificada|
|[RKZuKmTaOGPFwnzJ.htm](pathfinder-bestiary-3-items/RKZuKmTaOGPFwnzJ.htm)|Impossible Stature|Impossible Stature|modificada|
|[rL2cjpuNRY9BBahF.htm](pathfinder-bestiary-3-items/rL2cjpuNRY9BBahF.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[rlXD928xXNnyihPK.htm](pathfinder-bestiary-3-items/rlXD928xXNnyihPK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RM8HImoxZfJtj8F9.htm](pathfinder-bestiary-3-items/RM8HImoxZfJtj8F9.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[rmezKNjwT2IakFHV.htm](pathfinder-bestiary-3-items/rmezKNjwT2IakFHV.htm)|Spine|Spine|modificada|
|[rmrUx8mVzDx7W96E.htm](pathfinder-bestiary-3-items/rmrUx8mVzDx7W96E.htm)|Flames of Fury|Llamas de la furia|modificada|
|[Rmtq9X0tzbZPgELk.htm](pathfinder-bestiary-3-items/Rmtq9X0tzbZPgELk.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Rmxe7dutW3AoDW4m.htm](pathfinder-bestiary-3-items/Rmxe7dutW3AoDW4m.htm)|Echolocation (Precise) 40 feet|Ecolocalización (precisión) 40 pies.|modificada|
|[rNBF8QplzOETa39b.htm](pathfinder-bestiary-3-items/rNBF8QplzOETa39b.htm)|Branch|Rama|modificada|
|[RP2SeHrbTJkiKCCL.htm](pathfinder-bestiary-3-items/RP2SeHrbTJkiKCCL.htm)|Constrict|Restringir|modificada|
|[rpBeKBUmHNZG8uQA.htm](pathfinder-bestiary-3-items/rpBeKBUmHNZG8uQA.htm)|Hoof|Hoof|modificada|
|[rPhxztMzLxuNALLo.htm](pathfinder-bestiary-3-items/rPhxztMzLxuNALLo.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[RpO4Le4eD3npO4n3.htm](pathfinder-bestiary-3-items/RpO4Le4eD3npO4n3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[rPTqCxoOB0B5ON1m.htm](pathfinder-bestiary-3-items/rPTqCxoOB0B5ON1m.htm)|Weapon Arm|Arma Brazo|modificada|
|[rQhc6SkjKUBdbb2C.htm](pathfinder-bestiary-3-items/rQhc6SkjKUBdbb2C.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rtGARtAYnK1Pseab.htm](pathfinder-bestiary-3-items/rtGARtAYnK1Pseab.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RTMzhRViOSVhuoXI.htm](pathfinder-bestiary-3-items/RTMzhRViOSVhuoXI.htm)|Electric Projectiles|Electric Projectiles|modificada|
|[rU17t3NM7TJ3u03o.htm](pathfinder-bestiary-3-items/rU17t3NM7TJ3u03o.htm)|Regeneration 10 (Deactivated by Piercing)|Regeneración 10 (Desactivado por perforación)|modificada|
|[rUKEfvwyoUMhaFzX.htm](pathfinder-bestiary-3-items/rUKEfvwyoUMhaFzX.htm)|Vicelike Jaws|Vicelike Jauces|modificada|
|[RUqAGMQwHrGpfJPs.htm](pathfinder-bestiary-3-items/RUqAGMQwHrGpfJPs.htm)|Arcane Tendril|Arcane Tendril|modificada|
|[rVdhm2xzR2AUjY4g.htm](pathfinder-bestiary-3-items/rVdhm2xzR2AUjY4g.htm)|Beak|Beak|modificada|
|[RvhHvUE7nZmwkyMD.htm](pathfinder-bestiary-3-items/RvhHvUE7nZmwkyMD.htm)|Accord Essence|Esencia Accord|modificada|
|[RVk9mcBqt1YRswGY.htm](pathfinder-bestiary-3-items/RVk9mcBqt1YRswGY.htm)|Desert-Adapted|Desert-Adapted|modificada|
|[rvv40f4QmFMFlF5L.htm](pathfinder-bestiary-3-items/rvv40f4QmFMFlF5L.htm)|Jaws|Fauces|modificada|
|[RwrYgxqil6oSI14J.htm](pathfinder-bestiary-3-items/RwrYgxqil6oSI14J.htm)|Hurl Net|Lanzar red|modificada|
|[rXKR8tn1tmh9DdJ3.htm](pathfinder-bestiary-3-items/rXKR8tn1tmh9DdJ3.htm)|Jaws|Fauces|modificada|
|[rXrJgVzgOwMP7Cbr.htm](pathfinder-bestiary-3-items/rXrJgVzgOwMP7Cbr.htm)|Stable Stance|Posición Estable|modificada|
|[rxsLa6trqZBzZIOL.htm](pathfinder-bestiary-3-items/rxsLa6trqZBzZIOL.htm)|Impossible Stature|Impossible Stature|modificada|
|[rxu5E3okZFA5hwkO.htm](pathfinder-bestiary-3-items/rxu5E3okZFA5hwkO.htm)|Plaque Burst|Ráfaga de placas|modificada|
|[RxVHmIYJh9eOX9c3.htm](pathfinder-bestiary-3-items/RxVHmIYJh9eOX9c3.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[rYBfhejHUk9mGJyA.htm](pathfinder-bestiary-3-items/rYBfhejHUk9mGJyA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[RyfXnY4RnP8Gsh8n.htm](pathfinder-bestiary-3-items/RyfXnY4RnP8Gsh8n.htm)|Vulnerable to Calm Emotions|Vulnerable a calmar emociones|modificada|
|[rYN4yg46PUJy1sS9.htm](pathfinder-bestiary-3-items/rYN4yg46PUJy1sS9.htm)|Tail|Tail|modificada|
|[rYubtl6wVH1V3yQM.htm](pathfinder-bestiary-3-items/rYubtl6wVH1V3yQM.htm)|Surface-Bound|Surface-Bound|modificada|
|[rZeB3sV7jSAvG69a.htm](pathfinder-bestiary-3-items/rZeB3sV7jSAvG69a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S0fNGUpgdHgR1UVg.htm](pathfinder-bestiary-3-items/S0fNGUpgdHgR1UVg.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[s2oghprwMzFfjM11.htm](pathfinder-bestiary-3-items/s2oghprwMzFfjM11.htm)|Shy|Shy|modificada|
|[S2oLvvECz7beXP1O.htm](pathfinder-bestiary-3-items/S2oLvvECz7beXP1O.htm)|Jaws|Fauces|modificada|
|[S3r2AvwbHmmhBwB3.htm](pathfinder-bestiary-3-items/S3r2AvwbHmmhBwB3.htm)|Regression|Regresión|modificada|
|[S3S752aZGm6sxlqG.htm](pathfinder-bestiary-3-items/S3S752aZGm6sxlqG.htm)|Knockdown|Derribo|modificada|
|[S5fuvAOuYoMnXjOB.htm](pathfinder-bestiary-3-items/S5fuvAOuYoMnXjOB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s65OrlZwX6JQgM40.htm](pathfinder-bestiary-3-items/s65OrlZwX6JQgM40.htm)|Radiant Mantle|Manto Radiante|modificada|
|[s6BLghmvkOpXc6TW.htm](pathfinder-bestiary-3-items/s6BLghmvkOpXc6TW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s6k7oyMyiZKIqAar.htm](pathfinder-bestiary-3-items/s6k7oyMyiZKIqAar.htm)|Claw|Garra|modificada|
|[s6UD93sTVBhNgF8P.htm](pathfinder-bestiary-3-items/s6UD93sTVBhNgF8P.htm)|Assume Fiery Form|Adoptar forma de fuego|modificada|
|[s7ODA9MMbyTJSlyF.htm](pathfinder-bestiary-3-items/s7ODA9MMbyTJSlyF.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[S7uNAygkE7sTVfcL.htm](pathfinder-bestiary-3-items/S7uNAygkE7sTVfcL.htm)|Vindictive Crush|Vindictive Crush|modificada|
|[s87z24wrwStP1Ouk.htm](pathfinder-bestiary-3-items/s87z24wrwStP1Ouk.htm)|Positive Energy Transfer|Transferencia de energía positiva|modificada|
|[S8A8KdLa7fpa3GXF.htm](pathfinder-bestiary-3-items/S8A8KdLa7fpa3GXF.htm)|Ectoplasmic Form|Forma Ectoplasmica|modificada|
|[S8UsqHDRsdN6XtV5.htm](pathfinder-bestiary-3-items/S8UsqHDRsdN6XtV5.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[s9EYmFKDJ3ygchjQ.htm](pathfinder-bestiary-3-items/s9EYmFKDJ3ygchjQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S9RBBqUiQCE9JKB1.htm](pathfinder-bestiary-3-items/S9RBBqUiQCE9JKB1.htm)|Emotionally Unaware|Emotionally Unaware|modificada|
|[s9Y9sorFMjlmE7OS.htm](pathfinder-bestiary-3-items/s9Y9sorFMjlmE7OS.htm)|Anathematic Aversion|Anathematic Aversion|modificada|
|[Sa5PkHx5wTuKIBts.htm](pathfinder-bestiary-3-items/Sa5PkHx5wTuKIBts.htm)|Constrict|Restringir|modificada|
|[SA7P07cUNUdSC4Sp.htm](pathfinder-bestiary-3-items/SA7P07cUNUdSC4Sp.htm)|Betrayal Toxin|Betrayal Toxin|modificada|
|[sAINNUJA1VNOBd31.htm](pathfinder-bestiary-3-items/sAINNUJA1VNOBd31.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[SamwGEbNZfw3hWWZ.htm](pathfinder-bestiary-3-items/SamwGEbNZfw3hWWZ.htm)|Rapier|Estoque|modificada|
|[SaZttWSFJOlcV8gF.htm](pathfinder-bestiary-3-items/SaZttWSFJOlcV8gF.htm)|Violent Retort|Violent Retort|modificada|
|[SBSCHoUK3Je0TZWJ.htm](pathfinder-bestiary-3-items/SBSCHoUK3Je0TZWJ.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[sBY73yijWI9qogpX.htm](pathfinder-bestiary-3-items/sBY73yijWI9qogpX.htm)|All-Around Vision|All-Around Vision|modificada|
|[SbZX27efEktYhABv.htm](pathfinder-bestiary-3-items/SbZX27efEktYhABv.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[Scblqwycs4aZk1u1.htm](pathfinder-bestiary-3-items/Scblqwycs4aZk1u1.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[sCnH4uFy8fBv6bnP.htm](pathfinder-bestiary-3-items/sCnH4uFy8fBv6bnP.htm)|No Breath|No Breath|modificada|
|[SCRF3318aESeiNRz.htm](pathfinder-bestiary-3-items/SCRF3318aESeiNRz.htm)|Idols of Stone|Ídolos de Piedra|modificada|
|[ScsosKeNG0BlbPN5.htm](pathfinder-bestiary-3-items/ScsosKeNG0BlbPN5.htm)|Hallucinatory Haunting|Hallucinatory Haunting|modificada|
|[sCv6nwuflODCdMaO.htm](pathfinder-bestiary-3-items/sCv6nwuflODCdMaO.htm)|Portentous Gaze|Mirada Portentosa|modificada|
|[SCWOEv6X8RtegOjU.htm](pathfinder-bestiary-3-items/SCWOEv6X8RtegOjU.htm)|Scythe|Guadaña|modificada|
|[SE2Oc5VGiAxYtDb3.htm](pathfinder-bestiary-3-items/SE2Oc5VGiAxYtDb3.htm)|Crossbow|Ballesta|modificada|
|[se2uQ9cHOTmmlxON.htm](pathfinder-bestiary-3-items/se2uQ9cHOTmmlxON.htm)|Spectral Claw|Garra espectral|modificada|
|[sEbF3y3ttLqHmwtS.htm](pathfinder-bestiary-3-items/sEbF3y3ttLqHmwtS.htm)|Flame Ray|Rayo de Flamígera|modificada|
|[SEw7wjeoKGHYcloR.htm](pathfinder-bestiary-3-items/SEw7wjeoKGHYcloR.htm)|Accord Essence|Esencia Accord|modificada|
|[SF8eX3XCiOBA6wh4.htm](pathfinder-bestiary-3-items/SF8eX3XCiOBA6wh4.htm)|Intuit Object|Objeto Intuit|modificada|
|[SfFcwGTaNbZ6zFNW.htm](pathfinder-bestiary-3-items/SfFcwGTaNbZ6zFNW.htm)|Spear|Lanza|modificada|
|[sfp7Tgb12PISMHBT.htm](pathfinder-bestiary-3-items/sfp7Tgb12PISMHBT.htm)|Fist|Puño|modificada|
|[SfQfbmrgZ4vr3nFt.htm](pathfinder-bestiary-3-items/SfQfbmrgZ4vr3nFt.htm)|Spear|Lanza|modificada|
|[sfynZ9FF1MvgyIsO.htm](pathfinder-bestiary-3-items/sfynZ9FF1MvgyIsO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[sGASuiT2DzsMvSIa.htm](pathfinder-bestiary-3-items/sGASuiT2DzsMvSIa.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[sGEoB9eohFcV4gEe.htm](pathfinder-bestiary-3-items/sGEoB9eohFcV4gEe.htm)|Regeneration 50 (Deactivated by Fire)|Regeneración 50 (Desactivado por Fuego)|modificada|
|[SGw3tiHnJxU3HeUz.htm](pathfinder-bestiary-3-items/SGw3tiHnJxU3HeUz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SgXWjRi0fOvCndJM.htm](pathfinder-bestiary-3-items/SgXWjRi0fOvCndJM.htm)|Tongue|Lengua|modificada|
|[Sh368mCxkzNFInV2.htm](pathfinder-bestiary-3-items/Sh368mCxkzNFInV2.htm)|Greataxe|Greataxe|modificada|
|[shT4gXjrfuduYtZr.htm](pathfinder-bestiary-3-items/shT4gXjrfuduYtZr.htm)|Hellwasp Stings|Hellwasp Stings|modificada|
|[sIbPX7HRtLZsZC7R.htm](pathfinder-bestiary-3-items/sIbPX7HRtLZsZC7R.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[SIJ9zzLLshuohFwx.htm](pathfinder-bestiary-3-items/SIJ9zzLLshuohFwx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sIJOxufLnRAxLd8H.htm](pathfinder-bestiary-3-items/sIJOxufLnRAxLd8H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SIJXP2XGtiBASmHE.htm](pathfinder-bestiary-3-items/SIJXP2XGtiBASmHE.htm)|Constant Spells|Constant Spells|modificada|
|[sIlgLiMP9ruto6w9.htm](pathfinder-bestiary-3-items/sIlgLiMP9ruto6w9.htm)|Scent (Imprecise) 40 feet|Olor (Impreciso) 40 pies|modificada|
|[sIWC4aZPyNtODaFY.htm](pathfinder-bestiary-3-items/sIWC4aZPyNtODaFY.htm)|Negative Healing|Curación negativa|modificada|
|[sIZrd0MxYGEnR6YP.htm](pathfinder-bestiary-3-items/sIZrd0MxYGEnR6YP.htm)|Dual Opportunity|Dual Opportunity|modificada|
|[sj9VbYqqHsDlLaRb.htm](pathfinder-bestiary-3-items/sj9VbYqqHsDlLaRb.htm)|Fire Jackal Saliva|Saliva de chacal de fuego|modificada|
|[sjeNzkkJM6xfdPo8.htm](pathfinder-bestiary-3-items/sjeNzkkJM6xfdPo8.htm)|Eye Beam|Eye Beam|modificada|
|[SJtGnpq0cBxkmxWc.htm](pathfinder-bestiary-3-items/SJtGnpq0cBxkmxWc.htm)|Punish Flight|Punish Flight|modificada|
|[sK09hTiJx42HpDxj.htm](pathfinder-bestiary-3-items/sK09hTiJx42HpDxj.htm)|Nature's Rebirth|Nature's Rebirth|modificada|
|[sk5aFaaf3tRIgb0p.htm](pathfinder-bestiary-3-items/sk5aFaaf3tRIgb0p.htm)|Spear|Lanza|modificada|
|[SKfXJh58tHRMQxNa.htm](pathfinder-bestiary-3-items/SKfXJh58tHRMQxNa.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[SKICe54HSoZHNbYB.htm](pathfinder-bestiary-3-items/SKICe54HSoZHNbYB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sKizGnItFf8dn6kf.htm](pathfinder-bestiary-3-items/sKizGnItFf8dn6kf.htm)|Dooming Touch|Toque condenado/a|modificada|
|[sl07Not81gCCYcg7.htm](pathfinder-bestiary-3-items/sl07Not81gCCYcg7.htm)|Cavern Empathy|Caverna Empatía|modificada|
|[sLdsVr8WCWegLUjP.htm](pathfinder-bestiary-3-items/sLdsVr8WCWegLUjP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SlKd6WmjF0C4lXGp.htm](pathfinder-bestiary-3-items/SlKd6WmjF0C4lXGp.htm)|Form Up|Form Up|modificada|
|[sM1MA3hKVxt7ZvXp.htm](pathfinder-bestiary-3-items/sM1MA3hKVxt7ZvXp.htm)|+2 Circumstance to All Saves vs. Disease|+2 Circunstancia a todas las salvaciones contra enfermedad|modificada|
|[sm8dNJYFVglgLjGA.htm](pathfinder-bestiary-3-items/sm8dNJYFVglgLjGA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[sMOW7L2I7HwP3CtJ.htm](pathfinder-bestiary-3-items/sMOW7L2I7HwP3CtJ.htm)|Reassemble|Reassemble|modificada|
|[Sn9QsKQ5fSuHrD9F.htm](pathfinder-bestiary-3-items/Sn9QsKQ5fSuHrD9F.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[sNchZQFp9J0mXyVA.htm](pathfinder-bestiary-3-items/sNchZQFp9J0mXyVA.htm)|Throw Rock|Arrojar roca|modificada|
|[SnFwuUdykz9voojZ.htm](pathfinder-bestiary-3-items/SnFwuUdykz9voojZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sNGs8FpXUh0x309i.htm](pathfinder-bestiary-3-items/sNGs8FpXUh0x309i.htm)|Telepathy 30 feet|Telepatía 30 piesía.|modificada|
|[snPpaYVXqbrRM8Ne.htm](pathfinder-bestiary-3-items/snPpaYVXqbrRM8Ne.htm)|Breath Weapon|Breath Weapon|modificada|
|[sNth9LOE6nRfhE3n.htm](pathfinder-bestiary-3-items/sNth9LOE6nRfhE3n.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[sOt6KDS7Y34upG7G.htm](pathfinder-bestiary-3-items/sOt6KDS7Y34upG7G.htm)|Breath Weapon|Breath Weapon|modificada|
|[SP4I3NKUoM1EaTaK.htm](pathfinder-bestiary-3-items/SP4I3NKUoM1EaTaK.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[SPB7FRg5vvaLT9yY.htm](pathfinder-bestiary-3-items/SPB7FRg5vvaLT9yY.htm)|Pummeling Assault|Pummeling Assault|modificada|
|[sq2f8Tcx4gzFa7t5.htm](pathfinder-bestiary-3-items/sq2f8Tcx4gzFa7t5.htm)|Tusk|Tusk|modificada|
|[SQ3H5jQFY4vWcYDC.htm](pathfinder-bestiary-3-items/SQ3H5jQFY4vWcYDC.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[SQvpxbj1u4t8orbJ.htm](pathfinder-bestiary-3-items/SQvpxbj1u4t8orbJ.htm)|Ferocious Roar|Rugido Feroz|modificada|
|[SrTNivRRl7pbrWZN.htm](pathfinder-bestiary-3-items/SrTNivRRl7pbrWZN.htm)|Ghostly Longbow|Ghostly Longbow|modificada|
|[sRU8pYLkFEMCRECp.htm](pathfinder-bestiary-3-items/sRU8pYLkFEMCRECp.htm)|Death Blaze|Death Blaze|modificada|
|[SRv1ATMYjrhtxE5O.htm](pathfinder-bestiary-3-items/SRv1ATMYjrhtxE5O.htm)|Gloom Aura|Gloom Aura|modificada|
|[Sryn4awFAiEFPWJi.htm](pathfinder-bestiary-3-items/Sryn4awFAiEFPWJi.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[sT02s8CpPBlLaICN.htm](pathfinder-bestiary-3-items/sT02s8CpPBlLaICN.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[st3MDTSpYydB4U32.htm](pathfinder-bestiary-3-items/st3MDTSpYydB4U32.htm)|Material Leap|Material Salto sin carrerilla|modificada|
|[sT9rrvUVQPaZDRMI.htm](pathfinder-bestiary-3-items/sT9rrvUVQPaZDRMI.htm)|Drain Life|Drenar Vida|modificada|
|[stALstSmHYXTOJRU.htm](pathfinder-bestiary-3-items/stALstSmHYXTOJRU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[STN5ZaMm14HJIg38.htm](pathfinder-bestiary-3-items/STN5ZaMm14HJIg38.htm)|Jaws|Fauces|modificada|
|[stXqE9CezJeM4Whd.htm](pathfinder-bestiary-3-items/stXqE9CezJeM4Whd.htm)|Greatclub|Greatclub|modificada|
|[SU3ySHGYIBWN3dgx.htm](pathfinder-bestiary-3-items/SU3ySHGYIBWN3dgx.htm)|Adamantine Claw|Adamantine Claw|modificada|
|[SUBRW3AOWawVs2rU.htm](pathfinder-bestiary-3-items/SUBRW3AOWawVs2rU.htm)|Trample|Trample|modificada|
|[SuGCwf8YVqJ0KZNG.htm](pathfinder-bestiary-3-items/SuGCwf8YVqJ0KZNG.htm)|Energy Ray|Energy Ray|modificada|
|[SUKtpi6VGVyh7Jip.htm](pathfinder-bestiary-3-items/SUKtpi6VGVyh7Jip.htm)|Retaliatory Scratch|Rascarse en represalia|modificada|
|[SUQKMz3YCGazvNeH.htm](pathfinder-bestiary-3-items/SUQKMz3YCGazvNeH.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Svc6cpZQ69ghEA6T.htm](pathfinder-bestiary-3-items/Svc6cpZQ69ghEA6T.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[SVF9VI2ywkitioon.htm](pathfinder-bestiary-3-items/SVF9VI2ywkitioon.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[svqKqAYsrMhgU25p.htm](pathfinder-bestiary-3-items/svqKqAYsrMhgU25p.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[sVS5x7UzSVU5jjX2.htm](pathfinder-bestiary-3-items/sVS5x7UzSVU5jjX2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SVucgTqH9TEpdPX4.htm](pathfinder-bestiary-3-items/SVucgTqH9TEpdPX4.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[sW0kQIjx6HrXLDiV.htm](pathfinder-bestiary-3-items/sW0kQIjx6HrXLDiV.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[sX0MwC23Mvp1YeD2.htm](pathfinder-bestiary-3-items/sX0MwC23Mvp1YeD2.htm)|Merciless Thrust|Empuje Despiadado|modificada|
|[SYiq4FiF50sGlZtS.htm](pathfinder-bestiary-3-items/SYiq4FiF50sGlZtS.htm)|In Concert|En Concierto|modificada|
|[SZ0KpuFOSI1WY5Fo.htm](pathfinder-bestiary-3-items/SZ0KpuFOSI1WY5Fo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[sZ10byXqZzWm4yp5.htm](pathfinder-bestiary-3-items/sZ10byXqZzWm4yp5.htm)|Throw Rock|Arrojar roca|modificada|
|[sZ2qJ9Ced7EAt6pU.htm](pathfinder-bestiary-3-items/sZ2qJ9Ced7EAt6pU.htm)|Fist|Puño|modificada|
|[SZBQldnYP5zx1PyP.htm](pathfinder-bestiary-3-items/SZBQldnYP5zx1PyP.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[SzbZwTdWXUHNi17Y.htm](pathfinder-bestiary-3-items/SzbZwTdWXUHNi17Y.htm)|Claw|Garra|modificada|
|[SZjcOjfoi55nSlVu.htm](pathfinder-bestiary-3-items/SZjcOjfoi55nSlVu.htm)|Breath Weapon|Breath Weapon|modificada|
|[SzriRFiHHVYO1GYI.htm](pathfinder-bestiary-3-items/SzriRFiHHVYO1GYI.htm)|Gremlin Lice|Gremlin Piojos|modificada|
|[t0I4vgS1D9by3wX6.htm](pathfinder-bestiary-3-items/t0I4vgS1D9by3wX6.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[t0jxVnsS6HbKkoTf.htm](pathfinder-bestiary-3-items/t0jxVnsS6HbKkoTf.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[T1AhLjoX6p5mr4gQ.htm](pathfinder-bestiary-3-items/T1AhLjoX6p5mr4gQ.htm)|Grab|Agarrado|modificada|
|[T27ladpNIH9pULrU.htm](pathfinder-bestiary-3-items/T27ladpNIH9pULrU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[t2HIuBRLJiGYJZXE.htm](pathfinder-bestiary-3-items/t2HIuBRLJiGYJZXE.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[t4NelvHMno5EsQBb.htm](pathfinder-bestiary-3-items/t4NelvHMno5EsQBb.htm)|Penanggalan Bile|Penanggalan Bile|modificada|
|[T4S3miQ63r06a6Bu.htm](pathfinder-bestiary-3-items/T4S3miQ63r06a6Bu.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[T52SfDBkv0NA6Q8o.htm](pathfinder-bestiary-3-items/T52SfDBkv0NA6Q8o.htm)|Pincer|Pinza|modificada|
|[t6e0LBuCBj0cSlzG.htm](pathfinder-bestiary-3-items/t6e0LBuCBj0cSlzG.htm)|Ward|Ward|modificada|
|[T6ZAqc1mMzmpUCtm.htm](pathfinder-bestiary-3-items/T6ZAqc1mMzmpUCtm.htm)|Barbed Net|Red de púas|modificada|
|[t73fUopYHfJiDPvy.htm](pathfinder-bestiary-3-items/t73fUopYHfJiDPvy.htm)|Grab|Agarrado|modificada|
|[T7cl6r9Q9cdH7q2Q.htm](pathfinder-bestiary-3-items/T7cl6r9Q9cdH7q2Q.htm)|Anchored Soul|Alma Anclada|modificada|
|[t9cow1t47Ns4t0H0.htm](pathfinder-bestiary-3-items/t9cow1t47Ns4t0H0.htm)|Ravenspeaker|Ravenspeaker|modificada|
|[TAMn8gMkkXRH5ctl.htm](pathfinder-bestiary-3-items/TAMn8gMkkXRH5ctl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tAmUn1fmHBzAZrMX.htm](pathfinder-bestiary-3-items/tAmUn1fmHBzAZrMX.htm)|Arrow Volley|Volea de Flechas|modificada|
|[tAtaZY5U93gi2tUw.htm](pathfinder-bestiary-3-items/tAtaZY5U93gi2tUw.htm)|Leech Moisture|Leech Moisture|modificada|
|[tauTat1vId2N5H4u.htm](pathfinder-bestiary-3-items/tauTat1vId2N5H4u.htm)|Bite|Muerdemuerde|modificada|
|[Taw4K2MqiOVhIddu.htm](pathfinder-bestiary-3-items/Taw4K2MqiOVhIddu.htm)|Pain Touch|Toque de Dolor|modificada|
|[Tb6i1oczFczW6Kyf.htm](pathfinder-bestiary-3-items/Tb6i1oczFczW6Kyf.htm)|Jaws|Fauces|modificada|
|[TBMl5Ts5TOwuhfd4.htm](pathfinder-bestiary-3-items/TBMl5Ts5TOwuhfd4.htm)|+2 Circumstance to All Saves vs. Disease|+2 Circunstancia a todas las salvaciones contra enfermedad|modificada|
|[tBtFLroAVGw32fIG.htm](pathfinder-bestiary-3-items/tBtFLroAVGw32fIG.htm)|Negative Healing|Curación negativa|modificada|
|[tC5SlnihlaxwiNPE.htm](pathfinder-bestiary-3-items/tC5SlnihlaxwiNPE.htm)|Horn|Cuerno|modificada|
|[TcyFcfEZxlVqc3lO.htm](pathfinder-bestiary-3-items/TcyFcfEZxlVqc3lO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[td3gay0LzawYp5Ff.htm](pathfinder-bestiary-3-items/td3gay0LzawYp5Ff.htm)|Death Throes|Estertores de muerte|modificada|
|[td7f48qGt23BZNFA.htm](pathfinder-bestiary-3-items/td7f48qGt23BZNFA.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[Tdbu943P6LKUO8bz.htm](pathfinder-bestiary-3-items/Tdbu943P6LKUO8bz.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[Te1JpsOS99z9IDyD.htm](pathfinder-bestiary-3-items/Te1JpsOS99z9IDyD.htm)|Cone of Chalk|Cono de Tiza|modificada|
|[tetpWKxbpwLYXdRP.htm](pathfinder-bestiary-3-items/tetpWKxbpwLYXdRP.htm)|Improved Grab|Agarrado mejorado|modificada|
|[TFCVPbBolzO2jAdu.htm](pathfinder-bestiary-3-items/TFCVPbBolzO2jAdu.htm)|Knockdown|Derribo|modificada|
|[TFFEZk6wOV2NQ6BA.htm](pathfinder-bestiary-3-items/TFFEZk6wOV2NQ6BA.htm)|Hoof|Hoof|modificada|
|[TFIM3DwopeaNPL4N.htm](pathfinder-bestiary-3-items/TFIM3DwopeaNPL4N.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TgCafFeowTgKBPzr.htm](pathfinder-bestiary-3-items/TgCafFeowTgKBPzr.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[TgpF2GlBi4HH9xoC.htm](pathfinder-bestiary-3-items/TgpF2GlBi4HH9xoC.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tHyoisfllt6q3L0n.htm](pathfinder-bestiary-3-items/tHyoisfllt6q3L0n.htm)|Fed by Water|Alimentado por el agua|modificada|
|[Ti3e0CkEDJSOuOJy.htm](pathfinder-bestiary-3-items/Ti3e0CkEDJSOuOJy.htm)|Ferocity|Ferocidad|modificada|
|[ti9wuWwyYalyQsgW.htm](pathfinder-bestiary-3-items/ti9wuWwyYalyQsgW.htm)|Tongue|Lengua|modificada|
|[Tiw7WqKClIOO7liK.htm](pathfinder-bestiary-3-items/Tiw7WqKClIOO7liK.htm)|Rearward Rush|Rearward Embestida|modificada|
|[TjBokB0HyB1Xj2dM.htm](pathfinder-bestiary-3-items/TjBokB0HyB1Xj2dM.htm)|Puppetmaster|Puppetmaster|modificada|
|[tJCrBl4wNM7ey8KT.htm](pathfinder-bestiary-3-items/tJCrBl4wNM7ey8KT.htm)|Paralysis|Parálisis|modificada|
|[TjHnriTSE9y88Oaj.htm](pathfinder-bestiary-3-items/TjHnriTSE9y88Oaj.htm)|Purple Pox|Purple Pox|modificada|
|[TJq1OPO5CFe7Ww6A.htm](pathfinder-bestiary-3-items/TJq1OPO5CFe7Ww6A.htm)|Fist|Puño|modificada|
|[tLkLt02YQsb0Ahfo.htm](pathfinder-bestiary-3-items/tLkLt02YQsb0Ahfo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TlwNO1rmOqaNw0W7.htm](pathfinder-bestiary-3-items/TlwNO1rmOqaNw0W7.htm)|Bond with Mortal|Bond with Mortal|modificada|
|[TM9l7vqFbUf1UCLT.htm](pathfinder-bestiary-3-items/TM9l7vqFbUf1UCLT.htm)|Negative Healing|Curación negativa|modificada|
|[TmaCh6JDsKX35pc2.htm](pathfinder-bestiary-3-items/TmaCh6JDsKX35pc2.htm)|Upper Jaw|Fauces Superiores|modificada|
|[TnfM5nNHrq8AMX9L.htm](pathfinder-bestiary-3-items/TnfM5nNHrq8AMX9L.htm)|Tremorsense (Imprecise) within their entire bound granary or storeroom|Sentido del Temblor (Impreciso) dentro de todo su granero o almacén delimitado.|modificada|
|[tNH3T8aLwLgqsb4C.htm](pathfinder-bestiary-3-items/tNH3T8aLwLgqsb4C.htm)|Scintillating Claw|Scintillating Claw|modificada|
|[TnmeD4Xchfk7n9oy.htm](pathfinder-bestiary-3-items/TnmeD4Xchfk7n9oy.htm)|Veil of Lies|Velo de Mentiras|modificada|
|[tnQ4cIcOMWuGbaPY.htm](pathfinder-bestiary-3-items/tnQ4cIcOMWuGbaPY.htm)|Swift Staff Strike|Golpe de Vara Veloz|modificada|
|[TnYQrIo30Kk8l4BV.htm](pathfinder-bestiary-3-items/TnYQrIo30Kk8l4BV.htm)|Temporal Sense|Sentido temporal|modificada|
|[To0rnGkFKHIi4i3T.htm](pathfinder-bestiary-3-items/To0rnGkFKHIi4i3T.htm)|Echolocation (Precise) 20 feet|Ecolocalización (Precisión) 20 pies.|modificada|
|[tODukcahVjECiTcK.htm](pathfinder-bestiary-3-items/tODukcahVjECiTcK.htm)|Ghostly Light Mace|Maza de luz fantasmal|modificada|
|[Tp53J4QE9RBvdlCd.htm](pathfinder-bestiary-3-items/Tp53J4QE9RBvdlCd.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Tp8OXXguuWM7iKK6.htm](pathfinder-bestiary-3-items/Tp8OXXguuWM7iKK6.htm)|Electrical Blast|Ráfaga Eléctrica|modificada|
|[tpcpAmqxc53jhCQ7.htm](pathfinder-bestiary-3-items/tpcpAmqxc53jhCQ7.htm)|Talon|Talon|modificada|
|[tqdfXRytHABtgkJ4.htm](pathfinder-bestiary-3-items/tqdfXRytHABtgkJ4.htm)|Devour Tail|Devorar Cola|modificada|
|[TQhgfFteEWb6zzlT.htm](pathfinder-bestiary-3-items/TQhgfFteEWb6zzlT.htm)|Curl Up|Curl Up|modificada|
|[tqxAfXEcZbDZLdxn.htm](pathfinder-bestiary-3-items/tqxAfXEcZbDZLdxn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tR481pq7xrt35Xrw.htm](pathfinder-bestiary-3-items/tR481pq7xrt35Xrw.htm)|Frightful Presence|Frightful Presence|modificada|
|[TRofD56vepZwhI9C.htm](pathfinder-bestiary-3-items/TRofD56vepZwhI9C.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[TROzn3iLJ2A6fcpj.htm](pathfinder-bestiary-3-items/TROzn3iLJ2A6fcpj.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[TRPmSholsdyNcMiC.htm](pathfinder-bestiary-3-items/TRPmSholsdyNcMiC.htm)|Guardian Monolith|Guardian Monolith|modificada|
|[tsZ8GnCQamaRzMs3.htm](pathfinder-bestiary-3-items/tsZ8GnCQamaRzMs3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tT4vLmSN3IsZ2yur.htm](pathfinder-bestiary-3-items/tT4vLmSN3IsZ2yur.htm)|Four-Fanged Assault|Four-Fanged Assault|modificada|
|[ttBTEeYxisLmK0oL.htm](pathfinder-bestiary-3-items/ttBTEeYxisLmK0oL.htm)|Claw|Garra|modificada|
|[TTmqj0RzBKjYpDRC.htm](pathfinder-bestiary-3-items/TTmqj0RzBKjYpDRC.htm)|Telepathy 300 feet|Telepatía 300 pies.|modificada|
|[TTotdT50Y6eFIsHY.htm](pathfinder-bestiary-3-items/TTotdT50Y6eFIsHY.htm)|Jaws|Fauces|modificada|
|[tTRbrpeMS0tQXfb1.htm](pathfinder-bestiary-3-items/tTRbrpeMS0tQXfb1.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[tuqKOrUDPUmRlE6Q.htm](pathfinder-bestiary-3-items/tuqKOrUDPUmRlE6Q.htm)|Primal Purpose|Primal Purpose|modificada|
|[tvG7g3H6RsfVKvsw.htm](pathfinder-bestiary-3-items/tvG7g3H6RsfVKvsw.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[TwivoTQWKF6KVynb.htm](pathfinder-bestiary-3-items/TwivoTQWKF6KVynb.htm)|Ransack|Ransack|modificada|
|[tWOZ4o9V5cviyUJD.htm](pathfinder-bestiary-3-items/tWOZ4o9V5cviyUJD.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Twp10eJAJOK8lgbO.htm](pathfinder-bestiary-3-items/Twp10eJAJOK8lgbO.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tX7g78Vle2NcjulW.htm](pathfinder-bestiary-3-items/tX7g78Vle2NcjulW.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[TxmC3aaWzECZldPm.htm](pathfinder-bestiary-3-items/TxmC3aaWzECZldPm.htm)|Bone Spike|Pincho de Hueso|modificada|
|[tXq7aF0UVeeMJx53.htm](pathfinder-bestiary-3-items/tXq7aF0UVeeMJx53.htm)|Grab|Agarrado|modificada|
|[txykeoHQFVm7Kix0.htm](pathfinder-bestiary-3-items/txykeoHQFVm7Kix0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ty02a1CedQJfgYkL.htm](pathfinder-bestiary-3-items/ty02a1CedQJfgYkL.htm)|Wild Swing|Wild Swing|modificada|
|[tZtHn0wl4msKd5S2.htm](pathfinder-bestiary-3-items/tZtHn0wl4msKd5S2.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[u0EOvM37SL9GUjJu.htm](pathfinder-bestiary-3-items/u0EOvM37SL9GUjJu.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[u191bxAWSFhNXMic.htm](pathfinder-bestiary-3-items/u191bxAWSFhNXMic.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[U1W2qEPWeEon0DtN.htm](pathfinder-bestiary-3-items/U1W2qEPWeEon0DtN.htm)|Recall Reflection|Recall Reflection|modificada|
|[u3ndMdQRfiAH7Z7J.htm](pathfinder-bestiary-3-items/u3ndMdQRfiAH7Z7J.htm)|Phalanx Fighter|Phalanx Fighter|modificada|
|[u4b35ICyQPSv3OKF.htm](pathfinder-bestiary-3-items/u4b35ICyQPSv3OKF.htm)|Scimitar|Cimitarra|modificada|
|[u4eux5FgWu5TDOdj.htm](pathfinder-bestiary-3-items/u4eux5FgWu5TDOdj.htm)|Positive Nature|Naturaleza Positiva|modificada|
|[U4IAJfb86qQA800m.htm](pathfinder-bestiary-3-items/U4IAJfb86qQA800m.htm)|Pincer|Pinza|modificada|
|[u4uyw8rTejQnjDhT.htm](pathfinder-bestiary-3-items/u4uyw8rTejQnjDhT.htm)|Tail|Tail|modificada|
|[u5cLd0qEPkNofLzr.htm](pathfinder-bestiary-3-items/u5cLd0qEPkNofLzr.htm)|Clutching Cobbles|Clutching Cobbles|modificada|
|[u5VmraPdTduQVeUa.htm](pathfinder-bestiary-3-items/u5VmraPdTduQVeUa.htm)|Breach Vulnerability|Vulnerabilidad Emerger|modificada|
|[U615W2wUWCOrTcC6.htm](pathfinder-bestiary-3-items/U615W2wUWCOrTcC6.htm)|Splinter Sycophant|Splinter Sycophant|modificada|
|[U74B3Cx6dSGCsSHP.htm](pathfinder-bestiary-3-items/U74B3Cx6dSGCsSHP.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[u8fKUkjNFuyL7a5x.htm](pathfinder-bestiary-3-items/u8fKUkjNFuyL7a5x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uara7mD6sRfBQr3k.htm](pathfinder-bestiary-3-items/uara7mD6sRfBQr3k.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[UBAt1dB8VrLmKvtW.htm](pathfinder-bestiary-3-items/UBAt1dB8VrLmKvtW.htm)|Holy Weaponry|Armas Sagradas|modificada|
|[uBnRvlWD0zCwJWoX.htm](pathfinder-bestiary-3-items/uBnRvlWD0zCwJWoX.htm)|Grab|Agarrado|modificada|
|[uC8vf4vEv2zFOWgo.htm](pathfinder-bestiary-3-items/uC8vf4vEv2zFOWgo.htm)|Jaws|Fauces|modificada|
|[uCxQUNy75OANDWsm.htm](pathfinder-bestiary-3-items/uCxQUNy75OANDWsm.htm)|Adamantine Jaws|Fauces Adamantinas|modificada|
|[uD92m7U1xCZSE1U8.htm](pathfinder-bestiary-3-items/uD92m7U1xCZSE1U8.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[udfFYOsTo90aVSzS.htm](pathfinder-bestiary-3-items/udfFYOsTo90aVSzS.htm)|Throw Rock|Arrojar roca|modificada|
|[UDgM4FtV95bM8Jo2.htm](pathfinder-bestiary-3-items/UDgM4FtV95bM8Jo2.htm)|Sprint|Sprint|modificada|
|[uDO2KFHi9xxyZa8l.htm](pathfinder-bestiary-3-items/uDO2KFHi9xxyZa8l.htm)|Trample|Trample|modificada|
|[uDYAOTcNICSCyK3S.htm](pathfinder-bestiary-3-items/uDYAOTcNICSCyK3S.htm)|Shadow Invisibility|Sombra Invisibilidad|modificada|
|[uEckCwzDaV3V8orK.htm](pathfinder-bestiary-3-items/uEckCwzDaV3V8orK.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[UeMKUFMnWNbhRbEj.htm](pathfinder-bestiary-3-items/UeMKUFMnWNbhRbEj.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[UEtsgHyvVOtngUcs.htm](pathfinder-bestiary-3-items/UEtsgHyvVOtngUcs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[uf75VmCg2lXJXIS6.htm](pathfinder-bestiary-3-items/uf75VmCg2lXJXIS6.htm)|Spore Pop|Spore Pop|modificada|
|[UFxr3UoWoyUD8Oh4.htm](pathfinder-bestiary-3-items/UFxr3UoWoyUD8Oh4.htm)|Feral Gnaw|Feral Gnaw|modificada|
|[UGDbrE4flLByNgPe.htm](pathfinder-bestiary-3-items/UGDbrE4flLByNgPe.htm)|Tusk|Tusk|modificada|
|[UGRYo2y98N0QxtVh.htm](pathfinder-bestiary-3-items/UGRYo2y98N0QxtVh.htm)|Shadowbind|Shadowbind|modificada|
|[UH1P5Ajkj0bfwuUa.htm](pathfinder-bestiary-3-items/UH1P5Ajkj0bfwuUa.htm)|+2 Status to All Saves vs. Curses|+2 situación a todas las salvaciones contra maldiciones.|modificada|
|[UHVntHDP7laXevSA.htm](pathfinder-bestiary-3-items/UHVntHDP7laXevSA.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[UjrUewvwpknHeqGz.htm](pathfinder-bestiary-3-items/UjrUewvwpknHeqGz.htm)|Flaming Composite Longbow|Arco largo compuesto Flamógera|modificada|
|[UJX7CIiJGskPMXPv.htm](pathfinder-bestiary-3-items/UJX7CIiJGskPMXPv.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[UK9gsjbHYgmKZze9.htm](pathfinder-bestiary-3-items/UK9gsjbHYgmKZze9.htm)|Self-Destruct|Autodestrucción|modificada|
|[UKAw8CcCGNk7VFkB.htm](pathfinder-bestiary-3-items/UKAw8CcCGNk7VFkB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[UkgUfv2gU8Cnz7Dk.htm](pathfinder-bestiary-3-items/UkgUfv2gU8Cnz7Dk.htm)|Central Weapon|Arma central|modificada|
|[uLElA6gc5JAsMKBq.htm](pathfinder-bestiary-3-items/uLElA6gc5JAsMKBq.htm)|Swarm Mind|Swarm Mind|modificada|
|[ulnI58GokIE09FJW.htm](pathfinder-bestiary-3-items/ulnI58GokIE09FJW.htm)|Fist|Puño|modificada|
|[ULp1f7aziCabSbdr.htm](pathfinder-bestiary-3-items/ULp1f7aziCabSbdr.htm)|Claw|Garra|modificada|
|[uLwnIHpENAqfjCwz.htm](pathfinder-bestiary-3-items/uLwnIHpENAqfjCwz.htm)|Jaws|Fauces|modificada|
|[uLZvd57CejMavFox.htm](pathfinder-bestiary-3-items/uLZvd57CejMavFox.htm)|Eurypterid Venom|Eurypterid Venom|modificada|
|[uM7SvgL7DwtCruSv.htm](pathfinder-bestiary-3-items/uM7SvgL7DwtCruSv.htm)|Leg|Pierna|modificada|
|[Umi4PGsADEgpFaTb.htm](pathfinder-bestiary-3-items/Umi4PGsADEgpFaTb.htm)|Skip Between|Saltar entre|modificada|
|[UmmKeGJIw44WAK7i.htm](pathfinder-bestiary-3-items/UmmKeGJIw44WAK7i.htm)|Fell Shriek|Fell Shriek|modificada|
|[uMNXrK8Is6PAzNEB.htm](pathfinder-bestiary-3-items/uMNXrK8Is6PAzNEB.htm)|Scuttle Away|Scuttle Away|modificada|
|[UmrTl2VeCsF3GsBm.htm](pathfinder-bestiary-3-items/UmrTl2VeCsF3GsBm.htm)|Plantsense 60 feet|Plantsense 60 pies|modificada|
|[uN385UW65mQl9tQb.htm](pathfinder-bestiary-3-items/uN385UW65mQl9tQb.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[unEjKFEEILdk8x7m.htm](pathfinder-bestiary-3-items/unEjKFEEILdk8x7m.htm)|Grab|Agarrado|modificada|
|[Unq1mYTZ7Jk2AHCF.htm](pathfinder-bestiary-3-items/Unq1mYTZ7Jk2AHCF.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[UnTBAVC6J8CCjIdl.htm](pathfinder-bestiary-3-items/UnTBAVC6J8CCjIdl.htm)|Rotting Flesh|Carne podrida|modificada|
|[UOOf6xLzb7FqQtsU.htm](pathfinder-bestiary-3-items/UOOf6xLzb7FqQtsU.htm)|Offal|Despojos|modificada|
|[upknbVcHKdFGHUwR.htm](pathfinder-bestiary-3-items/upknbVcHKdFGHUwR.htm)|Glorious Fist|Glorious Fist|modificada|
|[UpN12Ls7Ii9P965x.htm](pathfinder-bestiary-3-items/UpN12Ls7Ii9P965x.htm)|Troop Defenses|Troop Defenses|modificada|
|[UPSTethShZCl8vHY.htm](pathfinder-bestiary-3-items/UPSTethShZCl8vHY.htm)|Fist|Puño|modificada|
|[Upzw2dn4HSqTYIma.htm](pathfinder-bestiary-3-items/Upzw2dn4HSqTYIma.htm)|Home Guardian|Home Guardian|modificada|
|[UQ5bqrNhcoxqd2TA.htm](pathfinder-bestiary-3-items/UQ5bqrNhcoxqd2TA.htm)|Change Shape|Change Shape|modificada|
|[uQaUx0jJSihzX8ob.htm](pathfinder-bestiary-3-items/uQaUx0jJSihzX8ob.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[UQgqIDpkTWprpPzL.htm](pathfinder-bestiary-3-items/UQgqIDpkTWprpPzL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[UQmOfNcYdQJoHq8o.htm](pathfinder-bestiary-3-items/UQmOfNcYdQJoHq8o.htm)|Avenging Bite|Muerdemuerde Vengador|modificada|
|[uQmt7JbKVVMAsd5R.htm](pathfinder-bestiary-3-items/uQmt7JbKVVMAsd5R.htm)|Grab|Agarrado|modificada|
|[URIeoHiQX0CLnpUA.htm](pathfinder-bestiary-3-items/URIeoHiQX0CLnpUA.htm)|Jaws|Fauces|modificada|
|[UrsYHXoFzbr0aW0A.htm](pathfinder-bestiary-3-items/UrsYHXoFzbr0aW0A.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Urwp7vLcfy6avc98.htm](pathfinder-bestiary-3-items/Urwp7vLcfy6avc98.htm)|Grab|Agarrado|modificada|
|[uS98HfKqTgAefSsN.htm](pathfinder-bestiary-3-items/uS98HfKqTgAefSsN.htm)|Devour Life|Devorar la vida|modificada|
|[USVxY3qpSEozrQU8.htm](pathfinder-bestiary-3-items/USVxY3qpSEozrQU8.htm)|Fist|Puño|modificada|
|[uubvbSioie58su3V.htm](pathfinder-bestiary-3-items/uubvbSioie58su3V.htm)|Feeding Frenzy|Frenesí alimenticio|modificada|
|[uuZFA4waTTmjGuf7.htm](pathfinder-bestiary-3-items/uuZFA4waTTmjGuf7.htm)|Claw|Garra|modificada|
|[UuZpaLnXCCuS5ijA.htm](pathfinder-bestiary-3-items/UuZpaLnXCCuS5ijA.htm)|Concussive Blow|Concussive Blow|modificada|
|[uVHi6RIyqbXq9V9N.htm](pathfinder-bestiary-3-items/uVHi6RIyqbXq9V9N.htm)|Regurgitated Wrath|Ira Regurgitada|modificada|
|[Uw2yhPghf93MQk8d.htm](pathfinder-bestiary-3-items/Uw2yhPghf93MQk8d.htm)|Constrict|Restringir|modificada|
|[UwDGN0t984W2aQuV.htm](pathfinder-bestiary-3-items/UwDGN0t984W2aQuV.htm)|Paired Strike|Golpe Emparejado|modificada|
|[UWTdQ7IdEAOO2D4Q.htm](pathfinder-bestiary-3-items/UWTdQ7IdEAOO2D4Q.htm)|Champion Focus Spells|Conjuros de foco de campeón|modificada|
|[UXcwYHarnmB2msA1.htm](pathfinder-bestiary-3-items/UXcwYHarnmB2msA1.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[UXEJVTlMCdSK3nis.htm](pathfinder-bestiary-3-items/UXEJVTlMCdSK3nis.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[UxfNIf8feaMNNjuy.htm](pathfinder-bestiary-3-items/UxfNIf8feaMNNjuy.htm)|Jaws|Fauces|modificada|
|[uxGxWm5nbZ7Oxq7U.htm](pathfinder-bestiary-3-items/uxGxWm5nbZ7Oxq7U.htm)|Telepathy 200 feet|Telepatía 200 pies.|modificada|
|[UxlcIKPaetxhxrB9.htm](pathfinder-bestiary-3-items/UxlcIKPaetxhxrB9.htm)|Stench|Hedor|modificada|
|[uybrNwUFDqMCKZ3R.htm](pathfinder-bestiary-3-items/uybrNwUFDqMCKZ3R.htm)|Tail|Tail|modificada|
|[UYKJzdzZCg9KRzcD.htm](pathfinder-bestiary-3-items/UYKJzdzZCg9KRzcD.htm)|Easy to Call|Fácil de Llamar|modificada|
|[uyLFQBi1NYtMlqCF.htm](pathfinder-bestiary-3-items/uyLFQBi1NYtMlqCF.htm)|Tremorsense 60 feet|Tremorsense 60 pies|modificada|
|[uYmFPiCSJytUnLkG.htm](pathfinder-bestiary-3-items/uYmFPiCSJytUnLkG.htm)|Fatal Fantasia|Fantasía fatal|modificada|
|[UYwd2bv5bCYGaPAI.htm](pathfinder-bestiary-3-items/UYwd2bv5bCYGaPAI.htm)|Silver Strike|Golpe de Plata|modificada|
|[uZG6teo4GWKlxaiD.htm](pathfinder-bestiary-3-items/uZG6teo4GWKlxaiD.htm)|Punishing Strike|Golpe de castigo|modificada|
|[UZivbag22tDFAKdd.htm](pathfinder-bestiary-3-items/UZivbag22tDFAKdd.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[UzKSlqvopdc1rGlh.htm](pathfinder-bestiary-3-items/UzKSlqvopdc1rGlh.htm)|Spit|Escupe|modificada|
|[uzLf2ezYP1JPXfrQ.htm](pathfinder-bestiary-3-items/uzLf2ezYP1JPXfrQ.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[v0UApfvf8TYOWcOQ.htm](pathfinder-bestiary-3-items/v0UApfvf8TYOWcOQ.htm)|Constant Spells|Constant Spells|modificada|
|[V1IL4PehVXrysvbs.htm](pathfinder-bestiary-3-items/V1IL4PehVXrysvbs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[V2ghyimSN4jxOywM.htm](pathfinder-bestiary-3-items/V2ghyimSN4jxOywM.htm)|Talon|Talon|modificada|
|[V2NsuzlWxUrYA0mE.htm](pathfinder-bestiary-3-items/V2NsuzlWxUrYA0mE.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[v2RqXgBeNgkZ0DtA.htm](pathfinder-bestiary-3-items/v2RqXgBeNgkZ0DtA.htm)|Wind-Up|Wind-Up|modificada|
|[V6QvQyNmHdNppYYJ.htm](pathfinder-bestiary-3-items/V6QvQyNmHdNppYYJ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[v79Z6h5Gc46hkIBk.htm](pathfinder-bestiary-3-items/v79Z6h5Gc46hkIBk.htm)|Form Up|Form Up|modificada|
|[v88O0NAy45VWmtTO.htm](pathfinder-bestiary-3-items/v88O0NAy45VWmtTO.htm)|Curse of the Werebat|Curse of the Werebat|modificada|
|[v9Q0Mz13HzloiDTp.htm](pathfinder-bestiary-3-items/v9Q0Mz13HzloiDTp.htm)|Dagger|Daga|modificada|
|[VAggE1QOFwbQrV0m.htm](pathfinder-bestiary-3-items/VAggE1QOFwbQrV0m.htm)|Grab|Agarrado|modificada|
|[vagKbfsnprcn97gn.htm](pathfinder-bestiary-3-items/vagKbfsnprcn97gn.htm)|Flurry of Kicks|Ráfaga de patadas|modificada|
|[VAj42GSNyP2at5VR.htm](pathfinder-bestiary-3-items/VAj42GSNyP2at5VR.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[vAYa9X9DeqTHfehJ.htm](pathfinder-bestiary-3-items/vAYa9X9DeqTHfehJ.htm)|Shrieking Scream|Shrieking Scream|modificada|
|[VB7Ak1txQ1RPwHwJ.htm](pathfinder-bestiary-3-items/VB7Ak1txQ1RPwHwJ.htm)|Construct Armor (Hardness 3)|Construir Armadura (Dureza 3)|modificada|
|[VBA2WIH3wy9R2XsO.htm](pathfinder-bestiary-3-items/VBA2WIH3wy9R2XsO.htm)|Kukri|Kukri|modificada|
|[vBiVfLo0mr2jIdhC.htm](pathfinder-bestiary-3-items/vBiVfLo0mr2jIdhC.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[vBOtCKqI5yNghuqQ.htm](pathfinder-bestiary-3-items/vBOtCKqI5yNghuqQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vbSi698j0JxF2fvv.htm](pathfinder-bestiary-3-items/vbSi698j0JxF2fvv.htm)|Negative Healing|Curación negativa|modificada|
|[VBxySuyNQGXdtmjL.htm](pathfinder-bestiary-3-items/VBxySuyNQGXdtmjL.htm)|Claw|Garra|modificada|
|[VbzlMz5d1BV2siTI.htm](pathfinder-bestiary-3-items/VbzlMz5d1BV2siTI.htm)|Knockdown|Derribo|modificada|
|[vcbazPs6B9bMeBpm.htm](pathfinder-bestiary-3-items/vcbazPs6B9bMeBpm.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[vct7Th4oUyktL8WO.htm](pathfinder-bestiary-3-items/vct7Th4oUyktL8WO.htm)|Constant Spells|Constant Spells|modificada|
|[vdcv4s3CzGpAj5KB.htm](pathfinder-bestiary-3-items/vdcv4s3CzGpAj5KB.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[VdgqOmcdTkgggnVY.htm](pathfinder-bestiary-3-items/VdgqOmcdTkgggnVY.htm)|Dagger|Daga|modificada|
|[VdKAGQI4ZZFDhZeL.htm](pathfinder-bestiary-3-items/VdKAGQI4ZZFDhZeL.htm)|Claw|Garra|modificada|
|[VdozlfKy0DHfmOKJ.htm](pathfinder-bestiary-3-items/VdozlfKy0DHfmOKJ.htm)|Flail|Mayal|modificada|
|[vdpzi8I0mzpB3lHw.htm](pathfinder-bestiary-3-items/vdpzi8I0mzpB3lHw.htm)|Constant Spells|Constant Spells|modificada|
|[vDs8pDlAo0An3Z5L.htm](pathfinder-bestiary-3-items/vDs8pDlAo0An3Z5L.htm)|Negative Healing|Curación negativa|modificada|
|[vDVTFesUl1YqnWqr.htm](pathfinder-bestiary-3-items/vDVTFesUl1YqnWqr.htm)|Claw|Garra|modificada|
|[VE0UqyozDof75tsl.htm](pathfinder-bestiary-3-items/VE0UqyozDof75tsl.htm)|Form Up|Form Up|modificada|
|[Ve2k20bE8qSEdyvY.htm](pathfinder-bestiary-3-items/Ve2k20bE8qSEdyvY.htm)|Drain Life|Drenar Vida|modificada|
|[VE76RnTQLL1Y1ttf.htm](pathfinder-bestiary-3-items/VE76RnTQLL1Y1ttf.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[VECsjpvEAaMDK0IJ.htm](pathfinder-bestiary-3-items/VECsjpvEAaMDK0IJ.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[veXylj85GdkPPrim.htm](pathfinder-bestiary-3-items/veXylj85GdkPPrim.htm)|Swallow Whole|Engullir Todo|modificada|
|[veYnaiiLoAVleTjO.htm](pathfinder-bestiary-3-items/veYnaiiLoAVleTjO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vFdekjhomHHOwhBO.htm](pathfinder-bestiary-3-items/vFdekjhomHHOwhBO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VFkAq790PgfMtp1j.htm](pathfinder-bestiary-3-items/VFkAq790PgfMtp1j.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vFTbscR1hFTWc1f3.htm](pathfinder-bestiary-3-items/vFTbscR1hFTWc1f3.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[vfThatLsAEQPDKBL.htm](pathfinder-bestiary-3-items/vfThatLsAEQPDKBL.htm)|Claw|Garra|modificada|
|[VgbKhZurZd89uLHJ.htm](pathfinder-bestiary-3-items/VgbKhZurZd89uLHJ.htm)|Frightening Rant|Frightening Rant|modificada|
|[Vge4HZcUTyLH8jKo.htm](pathfinder-bestiary-3-items/Vge4HZcUTyLH8jKo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vGPorxL6swyKt73y.htm](pathfinder-bestiary-3-items/vGPorxL6swyKt73y.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[VgsFe1dtKkzBHRVz.htm](pathfinder-bestiary-3-items/VgsFe1dtKkzBHRVz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vGTPPE8IbwvNQkbd.htm](pathfinder-bestiary-3-items/vGTPPE8IbwvNQkbd.htm)|Shock Mind|Mente Electrizante|modificada|
|[VgzHbzcl0K2wE4qk.htm](pathfinder-bestiary-3-items/VgzHbzcl0K2wE4qk.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[Vh1hkSBb3FKa0QjG.htm](pathfinder-bestiary-3-items/Vh1hkSBb3FKa0QjG.htm)|Sumbreiva Huntblade|Sumbreiva Huntblade|modificada|
|[VHBHFrYc6mpE6sPl.htm](pathfinder-bestiary-3-items/VHBHFrYc6mpE6sPl.htm)|Trawl for Bones|Trawl for Bones|modificada|
|[vhL7f1KV4YaXn5FV.htm](pathfinder-bestiary-3-items/vhL7f1KV4YaXn5FV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vhPEduQMUJTBlxzC.htm](pathfinder-bestiary-3-items/vhPEduQMUJTBlxzC.htm)|Shape Void|Shape Void|modificada|
|[VHt437pso612ShZb.htm](pathfinder-bestiary-3-items/VHt437pso612ShZb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VHwtj3724SEQAAlz.htm](pathfinder-bestiary-3-items/VHwtj3724SEQAAlz.htm)|Constant Spells|Constant Spells|modificada|
|[VHx2gNusdgS21WWh.htm](pathfinder-bestiary-3-items/VHx2gNusdgS21WWh.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[VhXGyXRgvJQTjUxr.htm](pathfinder-bestiary-3-items/VhXGyXRgvJQTjUxr.htm)|Claw|Garra|modificada|
|[vHyMiWUvIDKSzTO4.htm](pathfinder-bestiary-3-items/vHyMiWUvIDKSzTO4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vHZ8RpuMQkQsDTHW.htm](pathfinder-bestiary-3-items/vHZ8RpuMQkQsDTHW.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[VI8C7X1hf2SOT0jS.htm](pathfinder-bestiary-3-items/VI8C7X1hf2SOT0jS.htm)|Violent Retort|Violent Retort|modificada|
|[vICaR1ONcB3ugGfH.htm](pathfinder-bestiary-3-items/vICaR1ONcB3ugGfH.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[VIDJVH2nBIkzXg2L.htm](pathfinder-bestiary-3-items/VIDJVH2nBIkzXg2L.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[vihivNwS1MIyMHBB.htm](pathfinder-bestiary-3-items/vihivNwS1MIyMHBB.htm)|Rootbound|Rootbound|modificada|
|[vIMdth3wX5RnQu2i.htm](pathfinder-bestiary-3-items/vIMdth3wX5RnQu2i.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[viOxzbPu5sPdBqSc.htm](pathfinder-bestiary-3-items/viOxzbPu5sPdBqSc.htm)|Constrict|Restringir|modificada|
|[vJ4WPwSeQegH8KMs.htm](pathfinder-bestiary-3-items/vJ4WPwSeQegH8KMs.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[VjdE8difbmDAhk1V.htm](pathfinder-bestiary-3-items/VjdE8difbmDAhk1V.htm)|Tail|Tail|modificada|
|[VjJS4dQI0QyHntez.htm](pathfinder-bestiary-3-items/VjJS4dQI0QyHntez.htm)|Soul Feast|Soul Feast|modificada|
|[vjjTR5hrPf5pDmGk.htm](pathfinder-bestiary-3-items/vjjTR5hrPf5pDmGk.htm)|Change Shape|Change Shape|modificada|
|[VjpfyKmnhYR8Bus6.htm](pathfinder-bestiary-3-items/VjpfyKmnhYR8Bus6.htm)|Regeneration 5 (Deactivated by Acid or Fire)|Regeneración 5 (Desactivado por ácido o fuego)|modificada|
|[Vk1vUceA5furX6Ez.htm](pathfinder-bestiary-3-items/Vk1vUceA5furX6Ez.htm)|Wide Cleave|Hendedura Amplia|modificada|
|[vkAopqi1PCG6CvnD.htm](pathfinder-bestiary-3-items/vkAopqi1PCG6CvnD.htm)|Flame Jump|Salto de Flamígera|modificada|
|[vkQCMGp1H43fc8Jo.htm](pathfinder-bestiary-3-items/vkQCMGp1H43fc8Jo.htm)|Manipulate Luck|Manipular Suerte|modificada|
|[vKSl7FDTI6tQZCio.htm](pathfinder-bestiary-3-items/vKSl7FDTI6tQZCio.htm)|Negative Healing|Curación negativa|modificada|
|[vlGXeDzuvihHbasj.htm](pathfinder-bestiary-3-items/vlGXeDzuvihHbasj.htm)|Claw|Garra|modificada|
|[vlieQSRegKDFCdLu.htm](pathfinder-bestiary-3-items/vlieQSRegKDFCdLu.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[vlJE8WHtB9SH2Ca0.htm](pathfinder-bestiary-3-items/vlJE8WHtB9SH2Ca0.htm)|Telepathy 60 feet|Telepatía 60 pies.|modificada|
|[vLM5mXAWGq7oMjMv.htm](pathfinder-bestiary-3-items/vLM5mXAWGq7oMjMv.htm)|Grab|Agarrado|modificada|
|[VMapQLoJPfFmzdaS.htm](pathfinder-bestiary-3-items/VMapQLoJPfFmzdaS.htm)|Susceptible to Death|Susceptible a la Muerte|modificada|
|[VMiBWybV4muz0sCm.htm](pathfinder-bestiary-3-items/VMiBWybV4muz0sCm.htm)|Contingent Glyph|Glifo de contingencia|modificada|
|[VmpcHMhX036wRdpf.htm](pathfinder-bestiary-3-items/VmpcHMhX036wRdpf.htm)|Thorns|Espinas|modificada|
|[vMRN1ON8B5Jk9K9h.htm](pathfinder-bestiary-3-items/vMRN1ON8B5Jk9K9h.htm)|Easy to Call|Fácil de Llamar|modificada|
|[vMTJr0Wv9CyKUmqi.htm](pathfinder-bestiary-3-items/vMTJr0Wv9CyKUmqi.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[vNKRTp7YasyHRjqH.htm](pathfinder-bestiary-3-items/vNKRTp7YasyHRjqH.htm)|Claw|Garra|modificada|
|[VnUdpwyEF14owCWc.htm](pathfinder-bestiary-3-items/VnUdpwyEF14owCWc.htm)|Grab|Agarrado|modificada|
|[vOKfAcHQJQuAlDmX.htm](pathfinder-bestiary-3-items/vOKfAcHQJQuAlDmX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vPFPkp810JifvYk3.htm](pathfinder-bestiary-3-items/vPFPkp810JifvYk3.htm)|Pack Attack|Ataque en manada|modificada|
|[vPiD83jHe3pfnCGb.htm](pathfinder-bestiary-3-items/vPiD83jHe3pfnCGb.htm)|Vine|Vid|modificada|
|[vpKKf7MXtmLs5zCF.htm](pathfinder-bestiary-3-items/vpKKf7MXtmLs5zCF.htm)|Coven|Coven|modificada|
|[vpPweRPkFLYzku6Q.htm](pathfinder-bestiary-3-items/vpPweRPkFLYzku6Q.htm)|Resonance|Resonance|modificada|
|[VPTJLGDVtuMcWdkQ.htm](pathfinder-bestiary-3-items/VPTJLGDVtuMcWdkQ.htm)|Constrict|Restringir|modificada|
|[VPwJArBSwEFYQLmI.htm](pathfinder-bestiary-3-items/VPwJArBSwEFYQLmI.htm)|Viscous Trap|Viscous Trap|modificada|
|[VR2zhinm3bRfVONP.htm](pathfinder-bestiary-3-items/VR2zhinm3bRfVONP.htm)|Breath Weapon|Breath Weapon|modificada|
|[vRBgSJWn6sIDkLoJ.htm](pathfinder-bestiary-3-items/vRBgSJWn6sIDkLoJ.htm)|Boneshard Burst|Boneshard Burst|modificada|
|[VRhuQIGcrHKCOyL4.htm](pathfinder-bestiary-3-items/VRhuQIGcrHKCOyL4.htm)|Flaming Weapon|Arma Flamígera|modificada|
|[vRX7fHLnwfVnMQMN.htm](pathfinder-bestiary-3-items/vRX7fHLnwfVnMQMN.htm)|Trackless Step|Pisada sin rastro|modificada|
|[vrYCafAUKL8c1AqG.htm](pathfinder-bestiary-3-items/vrYCafAUKL8c1AqG.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[VS3BUS7yb5FWxaZH.htm](pathfinder-bestiary-3-items/VS3BUS7yb5FWxaZH.htm)|Appetizing Aroma|Aroma apetitoso|modificada|
|[VSblAk10Who8I3wv.htm](pathfinder-bestiary-3-items/VSblAk10Who8I3wv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VSLTIQMCHCfLxq1H.htm](pathfinder-bestiary-3-items/VSLTIQMCHCfLxq1H.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Vsv36iP3BjY1rHBr.htm](pathfinder-bestiary-3-items/Vsv36iP3BjY1rHBr.htm)|Claw|Garra|modificada|
|[VT3MCbqQoHOeEO6f.htm](pathfinder-bestiary-3-items/VT3MCbqQoHOeEO6f.htm)|Shadowed Blade|Sombra Blade|modificada|
|[VtDOTpa01IbIaeYP.htm](pathfinder-bestiary-3-items/VtDOTpa01IbIaeYP.htm)|Pack Attack|Ataque en manada|modificada|
|[vTV8lwwNuVs4eL7F.htm](pathfinder-bestiary-3-items/vTV8lwwNuVs4eL7F.htm)|Focus Vines|Focus Vines|modificada|
|[Vu7yHDGMbHRCK2KR.htm](pathfinder-bestiary-3-items/Vu7yHDGMbHRCK2KR.htm)|All-Around Vision|All-Around Vision|modificada|
|[vumGr1e1Y2sLOMlM.htm](pathfinder-bestiary-3-items/vumGr1e1Y2sLOMlM.htm)|Negative Healing|Curación negativa|modificada|
|[Vvhg9eA8gPTjQD32.htm](pathfinder-bestiary-3-items/Vvhg9eA8gPTjQD32.htm)|Grab|Agarrado|modificada|
|[VvNRwoYk3JBUgyor.htm](pathfinder-bestiary-3-items/VvNRwoYk3JBUgyor.htm)|Infest Corpse|Infest Corpse|modificada|
|[VvTUZ7JyzVpWPNRj.htm](pathfinder-bestiary-3-items/VvTUZ7JyzVpWPNRj.htm)|Sunset Ray|Sunset Ray|modificada|
|[VW8DMx7itpBNELJa.htm](pathfinder-bestiary-3-items/VW8DMx7itpBNELJa.htm)|Tail|Tail|modificada|
|[Vw9udtwUZC4fiY9x.htm](pathfinder-bestiary-3-items/Vw9udtwUZC4fiY9x.htm)|Trample|Trample|modificada|
|[vx5wkGlnDaE4xpPz.htm](pathfinder-bestiary-3-items/vx5wkGlnDaE4xpPz.htm)|Tail|Tail|modificada|
|[Vx9iqVf4ismUN743.htm](pathfinder-bestiary-3-items/Vx9iqVf4ismUN743.htm)|Strike as One|Golpear como uno solo|modificada|
|[vxPqbqu0444ekUR7.htm](pathfinder-bestiary-3-items/vxPqbqu0444ekUR7.htm)|Ghostly Longsword|Ghostly Longsword|modificada|
|[VXQuJ8bq1mVA48Xh.htm](pathfinder-bestiary-3-items/VXQuJ8bq1mVA48Xh.htm)|Heretic's Smite|Heretic's Smite|modificada|
|[VxrL8PMJq1Vfet9z.htm](pathfinder-bestiary-3-items/VxrL8PMJq1Vfet9z.htm)|Countered by Earth|Contrarrestado por la Tierra|modificada|
|[vxu3Y6OKaA1HJSb6.htm](pathfinder-bestiary-3-items/vxu3Y6OKaA1HJSb6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VY2kHKsJk6ZDMTkn.htm](pathfinder-bestiary-3-items/VY2kHKsJk6ZDMTkn.htm)|Demolish Veil|Demoler Velo|modificada|
|[vYEI2Ae6r4vFEWMb.htm](pathfinder-bestiary-3-items/vYEI2Ae6r4vFEWMb.htm)|Negative Healing|Curación negativa|modificada|
|[vyrmCD1p0HDQDVzt.htm](pathfinder-bestiary-3-items/vyrmCD1p0HDQDVzt.htm)|Wailing Dive|Lamento Dive|modificada|
|[vYyLO2LUYvXaIzFj.htm](pathfinder-bestiary-3-items/vYyLO2LUYvXaIzFj.htm)|Warding Glyph|Glifo custodio|modificada|
|[VzlO3rF9ml7NQJRF.htm](pathfinder-bestiary-3-items/VzlO3rF9ml7NQJRF.htm)|Fleeting Blossoms|Flores fugaces|modificada|
|[VZlTraWqYIrTlPGW.htm](pathfinder-bestiary-3-items/VZlTraWqYIrTlPGW.htm)|Tormenting Dreams|Sueños Atormentadores|modificada|
|[VZVdci43sd9eSYaO.htm](pathfinder-bestiary-3-items/VZVdci43sd9eSYaO.htm)|Absorb Spell|Absorber Hechizo|modificada|
|[VzzWLmieSojZVoLh.htm](pathfinder-bestiary-3-items/VzzWLmieSojZVoLh.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[W3i2bV0xohyukCqg.htm](pathfinder-bestiary-3-items/W3i2bV0xohyukCqg.htm)|Foxfire|Foxfire|modificada|
|[w43ksSNtCpLwtJqW.htm](pathfinder-bestiary-3-items/w43ksSNtCpLwtJqW.htm)|Ice Shard|Esquirla de Hielo|modificada|
|[w4lnLML460ytZr1D.htm](pathfinder-bestiary-3-items/w4lnLML460ytZr1D.htm)|Form Up|Form Up|modificada|
|[w6hxoqW1tHgrDy7z.htm](pathfinder-bestiary-3-items/w6hxoqW1tHgrDy7z.htm)|Constant Spells|Constant Spells|modificada|
|[w7zrM0Z9BtHG4iCl.htm](pathfinder-bestiary-3-items/w7zrM0Z9BtHG4iCl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W87SHAgheuR4yFTV.htm](pathfinder-bestiary-3-items/W87SHAgheuR4yFTV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WaigNlDjE6reTIhN.htm](pathfinder-bestiary-3-items/WaigNlDjE6reTIhN.htm)|Mist Vision|Mist Vision|modificada|
|[WC0xg40Qwic6NwdR.htm](pathfinder-bestiary-3-items/WC0xg40Qwic6NwdR.htm)|Radiant Ray|Radiant Ray|modificada|
|[Wc4yL4hHzoo1254s.htm](pathfinder-bestiary-3-items/Wc4yL4hHzoo1254s.htm)|Pained Muttering|Pained Muttering|modificada|
|[wCK0LYZXOOURpX1D.htm](pathfinder-bestiary-3-items/wCK0LYZXOOURpX1D.htm)|Constant Spells|Constant Spells|modificada|
|[WCzpBUZV9MrBfZQq.htm](pathfinder-bestiary-3-items/WCzpBUZV9MrBfZQq.htm)|+4 Status to All Saves vs. Mental or Divine|+4 situación a todas las salvaciones contra mental o divino.|modificada|
|[WD4ykPiVOX7xRtgI.htm](pathfinder-bestiary-3-items/WD4ykPiVOX7xRtgI.htm)|Perfected Flight|Perfected Flight|modificada|
|[wDaKwffZUuh51WSU.htm](pathfinder-bestiary-3-items/wDaKwffZUuh51WSU.htm)|Emit Musk|Emitir Almizcle|modificada|
|[WDDv1gdGqwkBHqVr.htm](pathfinder-bestiary-3-items/WDDv1gdGqwkBHqVr.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[wdoiMwQp71XmTDBR.htm](pathfinder-bestiary-3-items/wdoiMwQp71XmTDBR.htm)|Spikes|Púas|modificada|
|[wdp8tW3xpAHzFNVP.htm](pathfinder-bestiary-3-items/wdp8tW3xpAHzFNVP.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[wDXudHrGRboTsL0B.htm](pathfinder-bestiary-3-items/wDXudHrGRboTsL0B.htm)|Grab and Go|Agarrar y llevar|modificada|
|[WdzKzT9cZ51TPqV6.htm](pathfinder-bestiary-3-items/WdzKzT9cZ51TPqV6.htm)|Jotun Slayer|Jotun Slayer|modificada|
|[WE0EIWm5dsgBMGU3.htm](pathfinder-bestiary-3-items/WE0EIWm5dsgBMGU3.htm)|Throw Rock|Arrojar roca|modificada|
|[wEJxEzOsrNPsy1G2.htm](pathfinder-bestiary-3-items/wEJxEzOsrNPsy1G2.htm)|Absorb Memories|Absorber Recuerdos|modificada|
|[wETifc6MCAQ7g53r.htm](pathfinder-bestiary-3-items/wETifc6MCAQ7g53r.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[WEZI1PxyY7ic04Cy.htm](pathfinder-bestiary-3-items/WEZI1PxyY7ic04Cy.htm)|Grab|Agarrado|modificada|
|[Wf76V3o8gstoUE6k.htm](pathfinder-bestiary-3-items/Wf76V3o8gstoUE6k.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[wFnxNcpjZQq97vyn.htm](pathfinder-bestiary-3-items/wFnxNcpjZQq97vyn.htm)|Mountain Stride|Zancada de Montaña|modificada|
|[WFRxpdnf4FFb3z7i.htm](pathfinder-bestiary-3-items/WFRxpdnf4FFb3z7i.htm)|Telepathy 30 feet|Telepatía 30 piesía.|modificada|
|[WFS8UUOKxm78jwiH.htm](pathfinder-bestiary-3-items/WFS8UUOKxm78jwiH.htm)|Grab|Agarrado|modificada|
|[WgefMvFrdC4a4wjm.htm](pathfinder-bestiary-3-items/WgefMvFrdC4a4wjm.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[WgimcieBkqdGPzKv.htm](pathfinder-bestiary-3-items/WgimcieBkqdGPzKv.htm)|Change Shape|Change Shape|modificada|
|[wGOw52Kk1d6zz1Wr.htm](pathfinder-bestiary-3-items/wGOw52Kk1d6zz1Wr.htm)|Avenging Claws|Garras Vengadoras|modificada|
|[whcEPoZw0dVdy1U1.htm](pathfinder-bestiary-3-items/whcEPoZw0dVdy1U1.htm)|Telepathy 100 feet (Myceloids and Those Afflicted by Purple Pox Only)|Telepatía de 100 pies (solo miceloides y los afectados por la viruela púrpura)|modificada|
|[wHFnTcSpBzMvrkSD.htm](pathfinder-bestiary-3-items/wHFnTcSpBzMvrkSD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WIkVLqYDYSPO2pUz.htm](pathfinder-bestiary-3-items/WIkVLqYDYSPO2pUz.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[WIxfvddFY1utvHlp.htm](pathfinder-bestiary-3-items/WIxfvddFY1utvHlp.htm)|Air of Sickness|Air of Sickness|modificada|
|[WIXzhLmn2W2ARVKv.htm](pathfinder-bestiary-3-items/WIXzhLmn2W2ARVKv.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[wJ1vHRd41BTDplWu.htm](pathfinder-bestiary-3-items/wJ1vHRd41BTDplWu.htm)|Longspear|Longspear|modificada|
|[wjAhC8gFPZh72a5L.htm](pathfinder-bestiary-3-items/wjAhC8gFPZh72a5L.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wJFd0snSm32sHDWx.htm](pathfinder-bestiary-3-items/wJFd0snSm32sHDWx.htm)|Punish the Naughty|Castigar al travieso|modificada|
|[WJSuFXX25ngL6SXe.htm](pathfinder-bestiary-3-items/WJSuFXX25ngL6SXe.htm)|Speak With Snakes|Speak With Snakes|modificada|
|[WjVRDLqgP2pwz4W9.htm](pathfinder-bestiary-3-items/WjVRDLqgP2pwz4W9.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[wjyglR2qfZUfzQhE.htm](pathfinder-bestiary-3-items/wjyglR2qfZUfzQhE.htm)|Glide|Glide|modificada|
|[wK3dnqQbpz2HUZw5.htm](pathfinder-bestiary-3-items/wK3dnqQbpz2HUZw5.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[wKM9T9u0owr9Pynk.htm](pathfinder-bestiary-3-items/wKM9T9u0owr9Pynk.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[WKOA5jV7935qIaIo.htm](pathfinder-bestiary-3-items/WKOA5jV7935qIaIo.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[wkqPL5ekfW5yOlp9.htm](pathfinder-bestiary-3-items/wkqPL5ekfW5yOlp9.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[WKzCKcnmUlEawWY8.htm](pathfinder-bestiary-3-items/WKzCKcnmUlEawWY8.htm)|Fangs|Colmillos|modificada|
|[WLELPQnngNQdOSNv.htm](pathfinder-bestiary-3-items/WLELPQnngNQdOSNv.htm)|Contorted Clutch|Contorted Clutch|modificada|
|[WLzqAFoShoUcDjON.htm](pathfinder-bestiary-3-items/WLzqAFoShoUcDjON.htm)|Death Gasp|Death Gasp|modificada|
|[wMD2GLX6aqo2KE1s.htm](pathfinder-bestiary-3-items/wMD2GLX6aqo2KE1s.htm)|+1 Circumstance to All Saves vs. Disease, Poison, and Radiation|+1 Circunstancia a Todas las Salvaciones contra Enfermedad, Veneno y Radiación|modificada|
|[WMgMO9G3wUtuSBs8.htm](pathfinder-bestiary-3-items/WMgMO9G3wUtuSBs8.htm)|Fangs|Colmillos|modificada|
|[WMiSz943Mp34MPxS.htm](pathfinder-bestiary-3-items/WMiSz943Mp34MPxS.htm)|Countered by Water|Contrarrestado por Agua|modificada|
|[wncZzBRw5mBr1I3b.htm](pathfinder-bestiary-3-items/wncZzBRw5mBr1I3b.htm)|Claw|Garra|modificada|
|[wndNBL1gS8EX4LkQ.htm](pathfinder-bestiary-3-items/wndNBL1gS8EX4LkQ.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[WnLAb54M532yXeAK.htm](pathfinder-bestiary-3-items/WnLAb54M532yXeAK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Wo73VHAjF7M37ttK.htm](pathfinder-bestiary-3-items/Wo73VHAjF7M37ttK.htm)|Dagger|Daga|modificada|
|[WOLOhrv8TOFesFur.htm](pathfinder-bestiary-3-items/WOLOhrv8TOFesFur.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[wP25CSpHH1XfhQZV.htm](pathfinder-bestiary-3-items/wP25CSpHH1XfhQZV.htm)|Wing|Ala|modificada|
|[wPEh1COqJAlq37ce.htm](pathfinder-bestiary-3-items/wPEh1COqJAlq37ce.htm)|Shock Composite Longbow|Arco largo de Composición Electrizante|modificada|
|[WpHT325mhd62bmHx.htm](pathfinder-bestiary-3-items/WpHT325mhd62bmHx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[wPsE3OnHZcFptWrh.htm](pathfinder-bestiary-3-items/wPsE3OnHZcFptWrh.htm)|Claimer of the Slain|Reclamador de los muertos|modificada|
|[wq97ytCKPYfmWhuM.htm](pathfinder-bestiary-3-items/wq97ytCKPYfmWhuM.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[wqfNw7k5yrmfOl9I.htm](pathfinder-bestiary-3-items/wqfNw7k5yrmfOl9I.htm)|Cat's Curiosity|Curiosidad de gato|modificada|
|[wQOPxpduTlBDFOg3.htm](pathfinder-bestiary-3-items/wQOPxpduTlBDFOg3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[WrlWLg3CTbIvRroV.htm](pathfinder-bestiary-3-items/WrlWLg3CTbIvRroV.htm)|Mace|Maza|modificada|
|[wRO8DImltQqpTp5v.htm](pathfinder-bestiary-3-items/wRO8DImltQqpTp5v.htm)|Felt Shears|Tijeras de fieltro|modificada|
|[wRvyhBs5zmAJtpe8.htm](pathfinder-bestiary-3-items/wRvyhBs5zmAJtpe8.htm)|Temporal Sense|Sentido temporal|modificada|
|[WsuMP3feuXNP1eeE.htm](pathfinder-bestiary-3-items/WsuMP3feuXNP1eeE.htm)|Sling|Sling|modificada|
|[WtYldqbo5WHxuBHU.htm](pathfinder-bestiary-3-items/WtYldqbo5WHxuBHU.htm)|Swarm Mind|Swarm Mind|modificada|
|[wuJYkji7dBLYrLAK.htm](pathfinder-bestiary-3-items/wuJYkji7dBLYrLAK.htm)|Constant Spells|Constant Spells|modificada|
|[WUkTk0F7LErjOXbO.htm](pathfinder-bestiary-3-items/WUkTk0F7LErjOXbO.htm)|Site Bound|Ligado a una ubicación|modificada|
|[WUlRpT25Akwrbx9n.htm](pathfinder-bestiary-3-items/WUlRpT25Akwrbx9n.htm)|Hatchet|Hacha|modificada|
|[WUrrk4eny3GFngud.htm](pathfinder-bestiary-3-items/WUrrk4eny3GFngud.htm)|Burning Cold|Burning Cold|modificada|
|[wuWal4gyvorVZotg.htm](pathfinder-bestiary-3-items/wuWal4gyvorVZotg.htm)|Deflecting Lie|Mentir desviando|modificada|
|[WvLlxDJajjcl40yg.htm](pathfinder-bestiary-3-items/WvLlxDJajjcl40yg.htm)|Pull Arm|Pull Arm|modificada|
|[wVvVI3Jfnk8L5F3L.htm](pathfinder-bestiary-3-items/wVvVI3Jfnk8L5F3L.htm)|Pustulant Flail|Pustulant Flail|modificada|
|[wVY7ijG6fY0kdgBv.htm](pathfinder-bestiary-3-items/wVY7ijG6fY0kdgBv.htm)|Spear|Lanza|modificada|
|[wWvqRgRb0u1JwCr6.htm](pathfinder-bestiary-3-items/wWvqRgRb0u1JwCr6.htm)|Troop Movement|Movimiento de tropas|modificada|
|[wY1dx0xYE5qgM9aA.htm](pathfinder-bestiary-3-items/wY1dx0xYE5qgM9aA.htm)|Sonic Pulse|Pulso Sónico|modificada|
|[WY6HZHUNFdCpdD8m.htm](pathfinder-bestiary-3-items/WY6HZHUNFdCpdD8m.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[wy6isKCHF1syJmZf.htm](pathfinder-bestiary-3-items/wy6isKCHF1syJmZf.htm)|Fist|Puño|modificada|
|[wycfQOBOlmzkFJ7X.htm](pathfinder-bestiary-3-items/wycfQOBOlmzkFJ7X.htm)|Slough Toxins|Toxinas de muda|modificada|
|[WypFVNf6qa1B0aVW.htm](pathfinder-bestiary-3-items/WypFVNf6qa1B0aVW.htm)|Voice of the Storm|Voz de la tormenta|modificada|
|[WYWDDEYazjVp60qA.htm](pathfinder-bestiary-3-items/WYWDDEYazjVp60qA.htm)|Rain of Debris|Lluvia de Escombros|modificada|
|[WZ8Y5R6xt0YzzTUf.htm](pathfinder-bestiary-3-items/WZ8Y5R6xt0YzzTUf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wzhDzF6I1V67YTEq.htm](pathfinder-bestiary-3-items/wzhDzF6I1V67YTEq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[X0s0MfJlPDdh23yP.htm](pathfinder-bestiary-3-items/X0s0MfJlPDdh23yP.htm)|Peaceful Aura|Aura pacífica|modificada|
|[X0u8yRaoumH1Qm27.htm](pathfinder-bestiary-3-items/X0u8yRaoumH1Qm27.htm)|Lithe|Lithe|modificada|
|[X2Wh8jC2XB0vLQWK.htm](pathfinder-bestiary-3-items/X2Wh8jC2XB0vLQWK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[x3F1PlXQKZuCBCGg.htm](pathfinder-bestiary-3-items/x3F1PlXQKZuCBCGg.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[x40IMqy9XVt1sVgV.htm](pathfinder-bestiary-3-items/x40IMqy9XVt1sVgV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[X47wX3wcEYVktTy9.htm](pathfinder-bestiary-3-items/X47wX3wcEYVktTy9.htm)|Bound|Bound|modificada|
|[X4dbGwvffyOz8AAl.htm](pathfinder-bestiary-3-items/X4dbGwvffyOz8AAl.htm)|Fist|Puño|modificada|
|[X4oCGHtZLhDPu84J.htm](pathfinder-bestiary-3-items/X4oCGHtZLhDPu84J.htm)|Shortbow|Arco corto|modificada|
|[X4QNdoSZgj2ri5we.htm](pathfinder-bestiary-3-items/X4QNdoSZgj2ri5we.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[X5bjxwBhWpEDkFUA.htm](pathfinder-bestiary-3-items/X5bjxwBhWpEDkFUA.htm)|Forge Weapon|Arma de forja|modificada|
|[x5dNPm9EV0pS1WDJ.htm](pathfinder-bestiary-3-items/x5dNPm9EV0pS1WDJ.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[X5iRzpgvcVsGqbsr.htm](pathfinder-bestiary-3-items/X5iRzpgvcVsGqbsr.htm)|Thorn|Espina|modificada|
|[x5ke7WXv7p23V2Mc.htm](pathfinder-bestiary-3-items/x5ke7WXv7p23V2Mc.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[x68dPIsgXM3s4jCZ.htm](pathfinder-bestiary-3-items/x68dPIsgXM3s4jCZ.htm)|Claw|Garra|modificada|
|[X6SY2vedf1w4NFyM.htm](pathfinder-bestiary-3-items/X6SY2vedf1w4NFyM.htm)|Fangs|Colmillos|modificada|
|[x7CJTFxW4u5WZvs5.htm](pathfinder-bestiary-3-items/x7CJTFxW4u5WZvs5.htm)|Fade Away|Fade Away|modificada|
|[X7keRhNv8rJSPJUM.htm](pathfinder-bestiary-3-items/X7keRhNv8rJSPJUM.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[X7My4JpekzJFoAlA.htm](pathfinder-bestiary-3-items/X7My4JpekzJFoAlA.htm)|Jaws|Fauces|modificada|
|[X7tUpkvJ2IFENOEb.htm](pathfinder-bestiary-3-items/X7tUpkvJ2IFENOEb.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[X8q0VOcCAdQbazmw.htm](pathfinder-bestiary-3-items/X8q0VOcCAdQbazmw.htm)|Urban Legend|Leyenda Urbana|modificada|
|[x8x4bMdmpqcgY6vi.htm](pathfinder-bestiary-3-items/x8x4bMdmpqcgY6vi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xA0xMv9xbqMW0QIQ.htm](pathfinder-bestiary-3-items/xA0xMv9xbqMW0QIQ.htm)|Tearing Clutch|Tearing Clutch|modificada|
|[XAD9XYroOQ2xoyQF.htm](pathfinder-bestiary-3-items/XAD9XYroOQ2xoyQF.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[xb36ZcB5nu2McDrU.htm](pathfinder-bestiary-3-items/xb36ZcB5nu2McDrU.htm)|Grave Tide|Grave Tide|modificada|
|[XbI8HI1jqqylY3ZB.htm](pathfinder-bestiary-3-items/XbI8HI1jqqylY3ZB.htm)|Fly Free|Fly Free|modificada|
|[xBKXa275uqX5FmD9.htm](pathfinder-bestiary-3-items/xBKXa275uqX5FmD9.htm)|Beak|Beak|modificada|
|[XBz2toYQ5LlpBAkp.htm](pathfinder-bestiary-3-items/XBz2toYQ5LlpBAkp.htm)|Transpose|Transponer|modificada|
|[xc23c2YKrqqX2Dqy.htm](pathfinder-bestiary-3-items/xc23c2YKrqqX2Dqy.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XCtNEYFEjjOTsfCx.htm](pathfinder-bestiary-3-items/XCtNEYFEjjOTsfCx.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[XdiuvRsoT9Lh40Vm.htm](pathfinder-bestiary-3-items/XdiuvRsoT9Lh40Vm.htm)|Spike|Spike|modificada|
|[xDnUeGZfUafSNkYD.htm](pathfinder-bestiary-3-items/xDnUeGZfUafSNkYD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XdPq6vZ9PifML9Fy.htm](pathfinder-bestiary-3-items/XdPq6vZ9PifML9Fy.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[xdQJFj57QglkQXeG.htm](pathfinder-bestiary-3-items/xdQJFj57QglkQXeG.htm)|Vicious Criticals|Críticos despiadados|modificada|
|[xDxTLZhpgmfspHHB.htm](pathfinder-bestiary-3-items/xDxTLZhpgmfspHHB.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[xEaJ3PQ5662GbdCV.htm](pathfinder-bestiary-3-items/xEaJ3PQ5662GbdCV.htm)|Jaws|Fauces|modificada|
|[xeAn1C6HdB8ksSEc.htm](pathfinder-bestiary-3-items/xeAn1C6HdB8ksSEc.htm)|Instant Repair|Reparar al instante|modificada|
|[xEEbQADYYULp07pu.htm](pathfinder-bestiary-3-items/xEEbQADYYULp07pu.htm)|Stinger|Aguijón|modificada|
|[XFaAhmVWAH18gZJm.htm](pathfinder-bestiary-3-items/XFaAhmVWAH18gZJm.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[XFheFXfH7AWzxt0S.htm](pathfinder-bestiary-3-items/XFheFXfH7AWzxt0S.htm)|Bone Cannon|Bone Cannon|modificada|
|[xFhOtowzFmFYfYz2.htm](pathfinder-bestiary-3-items/xFhOtowzFmFYfYz2.htm)|Constant Spells|Constant Spells|modificada|
|[XfqrwEKpnBBJXrSD.htm](pathfinder-bestiary-3-items/XfqrwEKpnBBJXrSD.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[XGigEFgiBGNmtOkF.htm](pathfinder-bestiary-3-items/XGigEFgiBGNmtOkF.htm)|Change Shape|Change Shape|modificada|
|[xglgBFL5H1uvbArD.htm](pathfinder-bestiary-3-items/xglgBFL5H1uvbArD.htm)|Adamantine Claws|Garras Adamantinas|modificada|
|[xglqaHZLA1NfPGQN.htm](pathfinder-bestiary-3-items/xglqaHZLA1NfPGQN.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[XGpAwWvwSpDI36HG.htm](pathfinder-bestiary-3-items/XGpAwWvwSpDI36HG.htm)|Skeleton Crew|Skeleton Crew|modificada|
|[xGpHtG4FKoXUAvFz.htm](pathfinder-bestiary-3-items/xGpHtG4FKoXUAvFz.htm)|Trident|Trident|modificada|
|[XH6YaBHVaUMGRWkN.htm](pathfinder-bestiary-3-items/XH6YaBHVaUMGRWkN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XhqGsa6pKMextMJ1.htm](pathfinder-bestiary-3-items/XhqGsa6pKMextMJ1.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[xIt4uW4tEMRY5Y7p.htm](pathfinder-bestiary-3-items/xIt4uW4tEMRY5Y7p.htm)|Fist|Puño|modificada|
|[XjarabsV3GAFA01K.htm](pathfinder-bestiary-3-items/XjarabsV3GAFA01K.htm)|Claw|Garra|modificada|
|[xJGiCaAnnERzWXes.htm](pathfinder-bestiary-3-items/xJGiCaAnnERzWXes.htm)|Change Shape|Change Shape|modificada|
|[xji68Q4mnqfdWVnq.htm](pathfinder-bestiary-3-items/xji68Q4mnqfdWVnq.htm)|Shield Block|Bloquear con escudo|modificada|
|[XkAvPPqqBigAbyb2.htm](pathfinder-bestiary-3-items/XkAvPPqqBigAbyb2.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[XlCJ1ZHQ8EGtKrFn.htm](pathfinder-bestiary-3-items/XlCJ1ZHQ8EGtKrFn.htm)|Bite|Muerdemuerde|modificada|
|[Xlnd5wqyTUYU18hD.htm](pathfinder-bestiary-3-items/Xlnd5wqyTUYU18hD.htm)|Fangs|Colmillos|modificada|
|[xly8aKaWKIrEtqCD.htm](pathfinder-bestiary-3-items/xly8aKaWKIrEtqCD.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[xMaFAzpC6rkfoyYs.htm](pathfinder-bestiary-3-items/xMaFAzpC6rkfoyYs.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[xMNtSzQGqtHwsi9I.htm](pathfinder-bestiary-3-items/xMNtSzQGqtHwsi9I.htm)|Rock|Roca|modificada|
|[xmvNKENAwK9gdUuB.htm](pathfinder-bestiary-3-items/xmvNKENAwK9gdUuB.htm)|Grab|Agarrado|modificada|
|[Xn5fxlgPbRTrvMi3.htm](pathfinder-bestiary-3-items/Xn5fxlgPbRTrvMi3.htm)|Crew's Call|Llamada, la tripulación|modificada|
|[Xn6fpT76Eq3gNDgg.htm](pathfinder-bestiary-3-items/Xn6fpT76Eq3gNDgg.htm)|+4 to Will Saves vs. Fear|+4 a Salvaciones de Voluntad contra Miedo.|modificada|
|[XN9PqGr1Gft89U0v.htm](pathfinder-bestiary-3-items/XN9PqGr1Gft89U0v.htm)|Pervert Miracle|Pervertido Milagro|modificada|
|[xNumFo6zFMddKGAf.htm](pathfinder-bestiary-3-items/xNumFo6zFMddKGAf.htm)|Master of the Yard|Maestro del Patio|modificada|
|[xOPKChTgrsXHKQAv.htm](pathfinder-bestiary-3-items/xOPKChTgrsXHKQAv.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[XPaMcuntcVkLXwLT.htm](pathfinder-bestiary-3-items/XPaMcuntcVkLXwLT.htm)|Jaws|Fauces|modificada|
|[xPc5xxHX62bKWgkk.htm](pathfinder-bestiary-3-items/xPc5xxHX62bKWgkk.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[xpiNy47A0TciwBNQ.htm](pathfinder-bestiary-3-items/xpiNy47A0TciwBNQ.htm)|Jaws|Fauces|modificada|
|[XpkRG2gKRWLJkJEI.htm](pathfinder-bestiary-3-items/XpkRG2gKRWLJkJEI.htm)|Constant Spells|Constant Spells|modificada|
|[XPllxNneASW7mX3o.htm](pathfinder-bestiary-3-items/XPllxNneASW7mX3o.htm)|Blizzard Sight|Blizzard Sight|modificada|
|[XpzY4UQrwOlgO4km.htm](pathfinder-bestiary-3-items/XpzY4UQrwOlgO4km.htm)|Jaws|Fauces|modificada|
|[Xq6IkurlmqAmuchc.htm](pathfinder-bestiary-3-items/Xq6IkurlmqAmuchc.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[XqjbxnxA89sOEKAK.htm](pathfinder-bestiary-3-items/XqjbxnxA89sOEKAK.htm)|Claw|Garra|modificada|
|[xQqEt4PLNOCTutWG.htm](pathfinder-bestiary-3-items/xQqEt4PLNOCTutWG.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[xRGfPORY2OUX19Yt.htm](pathfinder-bestiary-3-items/xRGfPORY2OUX19Yt.htm)|Spirit Dart|Dardo Espiritual|modificada|
|[xsnc3cQ3J9m1QSHk.htm](pathfinder-bestiary-3-items/xsnc3cQ3J9m1QSHk.htm)|Vie for Victory|Vie for Victory|modificada|
|[xSSsTbYujfY4mN5H.htm](pathfinder-bestiary-3-items/xSSsTbYujfY4mN5H.htm)|Spade|Spade|modificada|
|[xt4bQy0cnsmUHdm8.htm](pathfinder-bestiary-3-items/xt4bQy0cnsmUHdm8.htm)|Spherical Body|Cuerpo Esférico|modificada|
|[XTe4xCgJVw7ZYYiN.htm](pathfinder-bestiary-3-items/XTe4xCgJVw7ZYYiN.htm)|Upside Down|Upside Down|modificada|
|[XTgwZSQK0BbVTSBC.htm](pathfinder-bestiary-3-items/XTgwZSQK0BbVTSBC.htm)|+1 Status to All Saves vs. Evil|+1 situación a todas las salvaciones contra el maligno.|modificada|
|[XTh9EmC4LPMteKEg.htm](pathfinder-bestiary-3-items/XTh9EmC4LPMteKEg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XuhLACe64duPSQuG.htm](pathfinder-bestiary-3-items/XuhLACe64duPSQuG.htm)|Phantom Bow|Phantom Bow|modificada|
|[xuNy9YUXbjhDbBPJ.htm](pathfinder-bestiary-3-items/xuNy9YUXbjhDbBPJ.htm)|Leaping Pounce|Salto sin carrerilla|modificada|
|[xUovRSHL0ImgkXwW.htm](pathfinder-bestiary-3-items/xUovRSHL0ImgkXwW.htm)|Claw|Garra|modificada|
|[xvKXt3hrhkReIqhL.htm](pathfinder-bestiary-3-items/xvKXt3hrhkReIqhL.htm)|Corrupt Speech|Discurso Corrupto|modificada|
|[xVneolKzqFvV56qV.htm](pathfinder-bestiary-3-items/xVneolKzqFvV56qV.htm)|Jaws|Fauces|modificada|
|[xWKUQcMCVRZCnWMH.htm](pathfinder-bestiary-3-items/xWKUQcMCVRZCnWMH.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[xWTrZfFzg3jvqkuu.htm](pathfinder-bestiary-3-items/xWTrZfFzg3jvqkuu.htm)|Greater Constrict|Mayor Restricción|modificada|
|[xXgKRQDkgjLOs0Pw.htm](pathfinder-bestiary-3-items/xXgKRQDkgjLOs0Pw.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[xxSdDW74Jjn0m4TC.htm](pathfinder-bestiary-3-items/xxSdDW74Jjn0m4TC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[xyVLktOlqlJ6Ce4h.htm](pathfinder-bestiary-3-items/xyVLktOlqlJ6Ce4h.htm)|Resonance|Resonance|modificada|
|[Xyy6EkOtHfPObm42.htm](pathfinder-bestiary-3-items/Xyy6EkOtHfPObm42.htm)|Constant Spells|Constant Spells|modificada|
|[xz2zppQgQtgwnNtp.htm](pathfinder-bestiary-3-items/xz2zppQgQtgwnNtp.htm)|Shriek|Shriek|modificada|
|[xzjn66Avz2JyCDZb.htm](pathfinder-bestiary-3-items/xzjn66Avz2JyCDZb.htm)|Chitinous Spines|Espinas quitinosas|modificada|
|[Xzku75HXAlTNH0q3.htm](pathfinder-bestiary-3-items/Xzku75HXAlTNH0q3.htm)|Constrict|Restringir|modificada|
|[y0GOJlbF7uqf9iXm.htm](pathfinder-bestiary-3-items/y0GOJlbF7uqf9iXm.htm)|Jaws|Fauces|modificada|
|[Y1qTkVB6Ss3pauQB.htm](pathfinder-bestiary-3-items/Y1qTkVB6Ss3pauQB.htm)|Disorienting Faces|Caras desorientadoras|modificada|
|[Y2DLiZGrL1Rc9uJZ.htm](pathfinder-bestiary-3-items/Y2DLiZGrL1Rc9uJZ.htm)|Vulnerable to Slow|Vulnerable a la Lentificación|modificada|
|[Y2rKb2rKiZlOJBow.htm](pathfinder-bestiary-3-items/Y2rKb2rKiZlOJBow.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Y2Zs23iG5XQJYKVX.htm](pathfinder-bestiary-3-items/Y2Zs23iG5XQJYKVX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Y4jOGsg9U0NdL52x.htm](pathfinder-bestiary-3-items/Y4jOGsg9U0NdL52x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[y4KzwwQKc4qz6rj7.htm](pathfinder-bestiary-3-items/y4KzwwQKc4qz6rj7.htm)|Colossal Echo|Eco Colosal|modificada|
|[Y4pImWAwa2NK5CDI.htm](pathfinder-bestiary-3-items/Y4pImWAwa2NK5CDI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Y7BT5iq85aFgz9ND.htm](pathfinder-bestiary-3-items/Y7BT5iq85aFgz9ND.htm)|Rearing Thrust|Rearing Thrust|modificada|
|[y7oztbBrqRdXk2kF.htm](pathfinder-bestiary-3-items/y7oztbBrqRdXk2kF.htm)|Knockdown|Derribo|modificada|
|[y7quO1sHtNujvw9P.htm](pathfinder-bestiary-3-items/y7quO1sHtNujvw9P.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[y8arNw5bZrQloRjm.htm](pathfinder-bestiary-3-items/y8arNw5bZrQloRjm.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[Y9n8hgpI4feL9erM.htm](pathfinder-bestiary-3-items/Y9n8hgpI4feL9erM.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[y9v6b1HnMTeyR6Aa.htm](pathfinder-bestiary-3-items/y9v6b1HnMTeyR6Aa.htm)|Jaws|Fauces|modificada|
|[YAt3ohXujxgfi1dY.htm](pathfinder-bestiary-3-items/YAt3ohXujxgfi1dY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YAtOaRD1Fb2RuzBs.htm](pathfinder-bestiary-3-items/YAtOaRD1Fb2RuzBs.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[YbkGuRsw8NVVsIfB.htm](pathfinder-bestiary-3-items/YbkGuRsw8NVVsIfB.htm)|Spectral Jaws|Fauces Espectrales|modificada|
|[YBLgLADsWKQWYwXJ.htm](pathfinder-bestiary-3-items/YBLgLADsWKQWYwXJ.htm)|Breath Weapon|Breath Weapon|modificada|
|[yBpXyahw41IRnTwr.htm](pathfinder-bestiary-3-items/yBpXyahw41IRnTwr.htm)|Dreadful Prediction|Dreadful Prediction|modificada|
|[ybXtrOFeg06hi75j.htm](pathfinder-bestiary-3-items/ybXtrOFeg06hi75j.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[yC55qBRcZsAS9EaR.htm](pathfinder-bestiary-3-items/yC55qBRcZsAS9EaR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ycIkiccSXhjaJ6am.htm](pathfinder-bestiary-3-items/ycIkiccSXhjaJ6am.htm)|Deflecting Gale|Vendaval protector|modificada|
|[yCLa7vNTrFdsMYUz.htm](pathfinder-bestiary-3-items/yCLa7vNTrFdsMYUz.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Ycopi6QWkxh3DeYu.htm](pathfinder-bestiary-3-items/Ycopi6QWkxh3DeYu.htm)|Easy to Call|Fácil de Llamar|modificada|
|[yd3ZruXaEyefPpGs.htm](pathfinder-bestiary-3-items/yd3ZruXaEyefPpGs.htm)|Talon|Talon|modificada|
|[yDBgrswetRTvDh9j.htm](pathfinder-bestiary-3-items/yDBgrswetRTvDh9j.htm)|Revived Retaliation|Retaliation reanimación|modificada|
|[YDmIyZCs3c67oBWR.htm](pathfinder-bestiary-3-items/YDmIyZCs3c67oBWR.htm)|Jaws|Fauces|modificada|
|[yds3BWASeIlBPuQ3.htm](pathfinder-bestiary-3-items/yds3BWASeIlBPuQ3.htm)|Telepathy 500 feet|Telepatía 500 pies.|modificada|
|[YE7BBnb0szfbgyZp.htm](pathfinder-bestiary-3-items/YE7BBnb0szfbgyZp.htm)|Change Shape|Change Shape|modificada|
|[YEEOqFUadOvlmJnt.htm](pathfinder-bestiary-3-items/YEEOqFUadOvlmJnt.htm)|Bounding Sprint|Bounding Sprint|modificada|
|[YF0cM4fWT0fvGHqC.htm](pathfinder-bestiary-3-items/YF0cM4fWT0fvGHqC.htm)|Revived Retaliation|Retaliation reanimación|modificada|
|[yF5x6s4ZYEZtto6J.htm](pathfinder-bestiary-3-items/yF5x6s4ZYEZtto6J.htm)|Claw|Garra|modificada|
|[YFR9EAebk1pSnIl3.htm](pathfinder-bestiary-3-items/YFR9EAebk1pSnIl3.htm)|Tremorsense (Precise) 30 feet|Sentido del temblor (precisión) 30 pies.|modificada|
|[Yft4ncVPX04wtjTC.htm](pathfinder-bestiary-3-items/Yft4ncVPX04wtjTC.htm)|Echolocation (Precise) 20 feet|Ecolocalización (Precisión) 20 pies.|modificada|
|[YfW3hmqsouC0M5an.htm](pathfinder-bestiary-3-items/YfW3hmqsouC0M5an.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[YGMkhOIOrpQ8AZbt.htm](pathfinder-bestiary-3-items/YGMkhOIOrpQ8AZbt.htm)|Erudite|Erudito|modificada|
|[yHPiBI51EEqH1jct.htm](pathfinder-bestiary-3-items/yHPiBI51EEqH1jct.htm)|Rituals|Rituales|modificada|
|[yipkwbXsFDd1t15w.htm](pathfinder-bestiary-3-items/yipkwbXsFDd1t15w.htm)|Consume Thoughts|Consume Pensamientos|modificada|
|[yiRwJtMI4udvjxuF.htm](pathfinder-bestiary-3-items/yiRwJtMI4udvjxuF.htm)|Frightful Presence|Frightful Presence|modificada|
|[YJ4VNOPXDLADhc0v.htm](pathfinder-bestiary-3-items/YJ4VNOPXDLADhc0v.htm)|Jaws|Fauces|modificada|
|[yJ8dDwM4sX2nS4UR.htm](pathfinder-bestiary-3-items/yJ8dDwM4sX2nS4UR.htm)|Aquatic Drag|Arrastre Acuático|modificada|
|[yJoFSnPHI2YuGrUT.htm](pathfinder-bestiary-3-items/yJoFSnPHI2YuGrUT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ykB9VqfwXdIegLS2.htm](pathfinder-bestiary-3-items/ykB9VqfwXdIegLS2.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[YKcp0LZjzLqu4KfR.htm](pathfinder-bestiary-3-items/YKcp0LZjzLqu4KfR.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[YKo3Am15Wz2kJyKI.htm](pathfinder-bestiary-3-items/YKo3Am15Wz2kJyKI.htm)|Illusory Weapon|Illusory Weapon|modificada|
|[Ykw6ewrTUkGjjiKl.htm](pathfinder-bestiary-3-items/Ykw6ewrTUkGjjiKl.htm)|Defensive Shove|Empujón defensivo|modificada|
|[ylEVi6mr1P1f1Nwz.htm](pathfinder-bestiary-3-items/ylEVi6mr1P1f1Nwz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YLX02foABJxLh21p.htm](pathfinder-bestiary-3-items/YLX02foABJxLh21p.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Ym03EsvdVPymdBSB.htm](pathfinder-bestiary-3-items/Ym03EsvdVPymdBSB.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[ymApeJr8ZiUS6OhX.htm](pathfinder-bestiary-3-items/ymApeJr8ZiUS6OhX.htm)|Spewing Bile|Spewing Bile|modificada|
|[YmLWpVwgT7G41DLf.htm](pathfinder-bestiary-3-items/YmLWpVwgT7G41DLf.htm)|Tattered Soul|Tattered Soul|modificada|
|[yMNZmEZoX7NmpxxP.htm](pathfinder-bestiary-3-items/yMNZmEZoX7NmpxxP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[yn9p5OZFEVAJgqpJ.htm](pathfinder-bestiary-3-items/yn9p5OZFEVAJgqpJ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[yNJghW8czppyVYi1.htm](pathfinder-bestiary-3-items/yNJghW8czppyVYi1.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[YNpN5FwvZ27Zfviz.htm](pathfinder-bestiary-3-items/YNpN5FwvZ27Zfviz.htm)|Shambling Onslaught|Shambling Onslaught|modificada|
|[YnX58rhrT4hsl8zl.htm](pathfinder-bestiary-3-items/YnX58rhrT4hsl8zl.htm)|Arcane Bolt|Rayo Arcano|modificada|
|[YnXG494wllYVpiYZ.htm](pathfinder-bestiary-3-items/YnXG494wllYVpiYZ.htm)|Negative Healing|Curación negativa|modificada|
|[yo565WZx3G5mcub3.htm](pathfinder-bestiary-3-items/yo565WZx3G5mcub3.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[yO6YNvoUDLZPlNwE.htm](pathfinder-bestiary-3-items/yO6YNvoUDLZPlNwE.htm)|Focus Beauty|Focus Belleza|modificada|
|[yodQpRTpnwuSLWgE.htm](pathfinder-bestiary-3-items/yodQpRTpnwuSLWgE.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yoFbIDWoysXLt9p4.htm](pathfinder-bestiary-3-items/yoFbIDWoysXLt9p4.htm)|Foot|Pie|modificada|
|[yox41dJM9YFdHSRU.htm](pathfinder-bestiary-3-items/yox41dJM9YFdHSRU.htm)|Constant Spells|Constant Spells|modificada|
|[YPcOC7ZuzY8aASqU.htm](pathfinder-bestiary-3-items/YPcOC7ZuzY8aASqU.htm)|Ghost Hunter|Cazador de fantasmas|modificada|
|[YPqH3eXv1Ux2rBDb.htm](pathfinder-bestiary-3-items/YPqH3eXv1Ux2rBDb.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[ypyEw36BPA1kfq0T.htm](pathfinder-bestiary-3-items/ypyEw36BPA1kfq0T.htm)|Claw|Garra|modificada|
|[yQ6h3MLUgYAJcomj.htm](pathfinder-bestiary-3-items/yQ6h3MLUgYAJcomj.htm)|Furious Possession|Posesión Furiosa|modificada|
|[YQs3tTvZeC9rNSZB.htm](pathfinder-bestiary-3-items/YQs3tTvZeC9rNSZB.htm)|Song of the Swamp|Song of the Swamp|modificada|
|[yQzN4wExqO58Ssdr.htm](pathfinder-bestiary-3-items/yQzN4wExqO58Ssdr.htm)|Slow|Lentificado/a|modificada|
|[YRKU8F7zH6qq03Om.htm](pathfinder-bestiary-3-items/YRKU8F7zH6qq03Om.htm)|Constant Spells|Constant Spells|modificada|
|[YrKVoYsRIkzzPSEH.htm](pathfinder-bestiary-3-items/YrKVoYsRIkzzPSEH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yRRl0UTbRwQx83WC.htm](pathfinder-bestiary-3-items/yRRl0UTbRwQx83WC.htm)|Smear|Smear|modificada|
|[yrUTciOjsrqkiFEh.htm](pathfinder-bestiary-3-items/yrUTciOjsrqkiFEh.htm)|Jaws|Fauces|modificada|
|[YS29qvFcBB3p68fh.htm](pathfinder-bestiary-3-items/YS29qvFcBB3p68fh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YsbObHTSL33vRGBA.htm](pathfinder-bestiary-3-items/YsbObHTSL33vRGBA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ySPa5Sa7wY7Oyugj.htm](pathfinder-bestiary-3-items/ySPa5Sa7wY7Oyugj.htm)|Claw|Garra|modificada|
|[ySTvjgpwqUPCVoUE.htm](pathfinder-bestiary-3-items/ySTvjgpwqUPCVoUE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YSVbN2hohx9XI8OH.htm](pathfinder-bestiary-3-items/YSVbN2hohx9XI8OH.htm)|Tail|Tail|modificada|
|[ytbxTl9miHNeB5iO.htm](pathfinder-bestiary-3-items/ytbxTl9miHNeB5iO.htm)|Spring Up|Spring Up|modificada|
|[yTqrSB5RzCRPTbYd.htm](pathfinder-bestiary-3-items/yTqrSB5RzCRPTbYd.htm)|Countered by Earth|Contrarrestado por la Tierra|modificada|
|[YvbJG9mY33OhkVRp.htm](pathfinder-bestiary-3-items/YvbJG9mY33OhkVRp.htm)|Negative Healing|Curación negativa|modificada|
|[yVpUeHZRYHGkXayx.htm](pathfinder-bestiary-3-items/yVpUeHZRYHGkXayx.htm)|Suspended Animation|Animación Suspendida|modificada|
|[YwO8ViIiTT0pBxLd.htm](pathfinder-bestiary-3-items/YwO8ViIiTT0pBxLd.htm)|Splatter|Splatter|modificada|
|[YWWsxw1woFyoUa9a.htm](pathfinder-bestiary-3-items/YWWsxw1woFyoUa9a.htm)|Negative Healing|Curación negativa|modificada|
|[Yx7C6ebcU2WQckqn.htm](pathfinder-bestiary-3-items/Yx7C6ebcU2WQckqn.htm)|Rift Sense|Rift Sense|modificada|
|[YXFicpAJz9WZdrnS.htm](pathfinder-bestiary-3-items/YXFicpAJz9WZdrnS.htm)|Boneshard Burst|Boneshard Burst|modificada|
|[yxOzb3edpHIxCIh9.htm](pathfinder-bestiary-3-items/yxOzb3edpHIxCIh9.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[YxUwb1otEos5UMSS.htm](pathfinder-bestiary-3-items/YxUwb1otEos5UMSS.htm)|All This Has Happened Before|Todo Esto Ha Pasado Antes|modificada|
|[YyJ8ioOaohHGksR2.htm](pathfinder-bestiary-3-items/YyJ8ioOaohHGksR2.htm)|Negative Healing|Curación negativa|modificada|
|[yySGyBmXQKMzLIZ0.htm](pathfinder-bestiary-3-items/yySGyBmXQKMzLIZ0.htm)|+1 Status to all Saves vs. Divine and Positive|+1 situación a todas las salvaciones contra divinos y positivos.|modificada|
|[yyvo4Lyc2M4RQhHy.htm](pathfinder-bestiary-3-items/yyvo4Lyc2M4RQhHy.htm)|Grab|Agarrado|modificada|
|[YZALPaj0pw2QQhWF.htm](pathfinder-bestiary-3-items/YZALPaj0pw2QQhWF.htm)|Grab|Agarrado|modificada|
|[YZgastPDC3pUUwDr.htm](pathfinder-bestiary-3-items/YZgastPDC3pUUwDr.htm)|Ink Blade|Ink Blade|modificada|
|[yzSpI7KmbcXVQQEF.htm](pathfinder-bestiary-3-items/yzSpI7KmbcXVQQEF.htm)|Mentalist Counterspell|Mentalista Contraconjuro|modificada|
|[YzVNI5zauuNyLldf.htm](pathfinder-bestiary-3-items/YzVNI5zauuNyLldf.htm)|Sunset Dependent|Sunset Dependent|modificada|
|[z19VI7TL2KhjANdt.htm](pathfinder-bestiary-3-items/z19VI7TL2KhjANdt.htm)|Constrict|Restringir|modificada|
|[z23Nlek8h4x4L0lq.htm](pathfinder-bestiary-3-items/z23Nlek8h4x4L0lq.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Z2AdAroZg5MIB8bp.htm](pathfinder-bestiary-3-items/Z2AdAroZg5MIB8bp.htm)|Wide Cleave|Hendedura Amplia|modificada|
|[z2iLBR1l32mSdxZg.htm](pathfinder-bestiary-3-items/z2iLBR1l32mSdxZg.htm)|Cynic's Curse|Maldición del Cínico|modificada|
|[Z2P5qc4Zo39S1t6w.htm](pathfinder-bestiary-3-items/Z2P5qc4Zo39S1t6w.htm)|Fist|Puño|modificada|
|[Z3lZR1EPDU26XJlG.htm](pathfinder-bestiary-3-items/Z3lZR1EPDU26XJlG.htm)|Jaws|Fauces|modificada|
|[Z4KMWDa1g8Ai7MCE.htm](pathfinder-bestiary-3-items/Z4KMWDa1g8Ai7MCE.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Z4wolFb7CMno5VO4.htm](pathfinder-bestiary-3-items/Z4wolFb7CMno5VO4.htm)|Forest Stride|Forest Zancada|modificada|
|[Z6FPbe7j4A4d8s6s.htm](pathfinder-bestiary-3-items/Z6FPbe7j4A4d8s6s.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[Z79hB9KYIozan9AY.htm](pathfinder-bestiary-3-items/Z79hB9KYIozan9AY.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[z8GylSGuIeDFrE0Z.htm](pathfinder-bestiary-3-items/z8GylSGuIeDFrE0Z.htm)|Shy|Shy|modificada|
|[z8QOqIIzB4F2TaBY.htm](pathfinder-bestiary-3-items/z8QOqIIzB4F2TaBY.htm)|Claw|Garra|modificada|
|[zaD6IUn59EF4mqzi.htm](pathfinder-bestiary-3-items/zaD6IUn59EF4mqzi.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[zAfIfdV5UBJtB564.htm](pathfinder-bestiary-3-items/zAfIfdV5UBJtB564.htm)|Smooth Swimmer|Nadador Suave|modificada|
|[ZaqhlgxYla5yl1jh.htm](pathfinder-bestiary-3-items/ZaqhlgxYla5yl1jh.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[zBdDYujCaw7HK02d.htm](pathfinder-bestiary-3-items/zBdDYujCaw7HK02d.htm)|Anticoagulant|Anticoagulante|modificada|
|[zbmQRPZHDEeRga68.htm](pathfinder-bestiary-3-items/zbmQRPZHDEeRga68.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[zbt0txkLR1uhzPjL.htm](pathfinder-bestiary-3-items/zbt0txkLR1uhzPjL.htm)|+1 Status to All Saves vs. Controlled Condition|+1 estado a todas las salvaciones frente a la situación controlada.|modificada|
|[zc0tBM6QCWY3pRQR.htm](pathfinder-bestiary-3-items/zc0tBM6QCWY3pRQR.htm)|Arm|Brazo|modificada|
|[zCqM5htGK65bK9Uh.htm](pathfinder-bestiary-3-items/zCqM5htGK65bK9Uh.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[zdb8RR0jcIIol6on.htm](pathfinder-bestiary-3-items/zdb8RR0jcIIol6on.htm)|Wind-Up|Wind-Up|modificada|
|[zDFtneAmSUjx6qnK.htm](pathfinder-bestiary-3-items/zDFtneAmSUjx6qnK.htm)|Pointed Charge|Carga Puntiaguda|modificada|
|[ZEFMlTiTYApQQKuS.htm](pathfinder-bestiary-3-items/ZEFMlTiTYApQQKuS.htm)|Grab|Agarrado|modificada|
|[ZEGxGxVAaZSmhNp7.htm](pathfinder-bestiary-3-items/ZEGxGxVAaZSmhNp7.htm)|Foot|Pie|modificada|
|[zEouFbaKNoTFZeHy.htm](pathfinder-bestiary-3-items/zEouFbaKNoTFZeHy.htm)|Mix Couatl Venom|Mezcla Veneno Couatl|modificada|
|[zeWAnIFSjhteCiiY.htm](pathfinder-bestiary-3-items/zeWAnIFSjhteCiiY.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[zfcj9MZNHpEvkerg.htm](pathfinder-bestiary-3-items/zfcj9MZNHpEvkerg.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ZffdskKO51EjTFvx.htm](pathfinder-bestiary-3-items/ZffdskKO51EjTFvx.htm)|Constrict|Restringir|modificada|
|[ZFhHkksQnaNOI9Rk.htm](pathfinder-bestiary-3-items/ZFhHkksQnaNOI9Rk.htm)|Fed by Wood|Alimentado por Madera|modificada|
|[ZFMGP6RS8c651Pki.htm](pathfinder-bestiary-3-items/ZFMGP6RS8c651Pki.htm)|Claw|Garra|modificada|
|[zFOxLexJnjqhpWy0.htm](pathfinder-bestiary-3-items/zFOxLexJnjqhpWy0.htm)|Needle|Aguja|modificada|
|[zFuVmNNi7HexpunZ.htm](pathfinder-bestiary-3-items/zFuVmNNi7HexpunZ.htm)|Sixfold Flurry|Ráfaga séxtuple|modificada|
|[zg034Oefe1w3sHwh.htm](pathfinder-bestiary-3-items/zg034Oefe1w3sHwh.htm)|+2 Status to All Saves vs. Primal Magic|+2 situación a todas las salvaciones contra magia primigenia.|modificada|
|[zGreN5MDFtAqJh8U.htm](pathfinder-bestiary-3-items/zGreN5MDFtAqJh8U.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zGXRWPh3UMA5q46D.htm](pathfinder-bestiary-3-items/zGXRWPh3UMA5q46D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZgZ9FcS7hIAO9FxC.htm](pathfinder-bestiary-3-items/ZgZ9FcS7hIAO9FxC.htm)|Girtablilu Venom|Girtablilu Venom|modificada|
|[zHIoJ0oELrIHabJu.htm](pathfinder-bestiary-3-items/zHIoJ0oELrIHabJu.htm)|Slippery|Slippery|modificada|
|[ZHJXY7uHZTuD4bL6.htm](pathfinder-bestiary-3-items/ZHJXY7uHZTuD4bL6.htm)|Scatterbrain Palm|Scatterbrain Palm|modificada|
|[zhUapm5Z8hOglOwU.htm](pathfinder-bestiary-3-items/zhUapm5Z8hOglOwU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zILlYq3L0c0BJfWG.htm](pathfinder-bestiary-3-items/zILlYq3L0c0BJfWG.htm)|Runic Resistance|Resistencia Rúnica|modificada|
|[ZiNy2010T2g4saPw.htm](pathfinder-bestiary-3-items/ZiNy2010T2g4saPw.htm)|Spiny Body|Cuerpo espinoso|modificada|
|[zj5lykgkozQXgCYv.htm](pathfinder-bestiary-3-items/zj5lykgkozQXgCYv.htm)|Skip Between|Saltar entre|modificada|
|[zJKO70Z0e4PdkPUS.htm](pathfinder-bestiary-3-items/zJKO70Z0e4PdkPUS.htm)|Quills|Quills|modificada|
|[zJprqN3Lkl03vYP7.htm](pathfinder-bestiary-3-items/zJprqN3Lkl03vYP7.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[zjRf8vLHR2qRhuCx.htm](pathfinder-bestiary-3-items/zjRf8vLHR2qRhuCx.htm)|Kukri|Kukri|modificada|
|[zJSR7LD72PzGqnDl.htm](pathfinder-bestiary-3-items/zJSR7LD72PzGqnDl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zJVmAmV1kg1wV6T3.htm](pathfinder-bestiary-3-items/zJVmAmV1kg1wV6T3.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[ZkPq0d1hLdzoA3Mf.htm](pathfinder-bestiary-3-items/ZkPq0d1hLdzoA3Mf.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zLh5jRcxUaafQvod.htm](pathfinder-bestiary-3-items/zLh5jRcxUaafQvod.htm)|Greater Constrict|Mayor Restricción|modificada|
|[ZlMkrt960OMod6zh.htm](pathfinder-bestiary-3-items/ZlMkrt960OMod6zh.htm)|Jaws|Fauces|modificada|
|[ZLvVuoT7wsCVMtYC.htm](pathfinder-bestiary-3-items/ZLvVuoT7wsCVMtYC.htm)|Frightful Presence|Frightful Presence|modificada|
|[ZmPA9nWFtMXPCArA.htm](pathfinder-bestiary-3-items/ZmPA9nWFtMXPCArA.htm)|Ganzi Spells|Hechizos Ganzi|modificada|
|[zMQJh0NjokM3IneE.htm](pathfinder-bestiary-3-items/zMQJh0NjokM3IneE.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[ZmVqNTMjqte6Xf1k.htm](pathfinder-bestiary-3-items/ZmVqNTMjqte6Xf1k.htm)|Fist|Puño|modificada|
|[zMy5ikhAFWW94nIF.htm](pathfinder-bestiary-3-items/zMy5ikhAFWW94nIF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zmZ1L8pspuQQGh1m.htm](pathfinder-bestiary-3-items/zmZ1L8pspuQQGh1m.htm)|Drain Blood|Drenar sangre|modificada|
|[znaX5hWmLXH23ywJ.htm](pathfinder-bestiary-3-items/znaX5hWmLXH23ywJ.htm)|Subsonic Pulse|Subsonic Pulse|modificada|
|[ZneHwzrBFdJkdzsG.htm](pathfinder-bestiary-3-items/ZneHwzrBFdJkdzsG.htm)|Change Shape|Change Shape|modificada|
|[znuFqFCIX1YQTcWx.htm](pathfinder-bestiary-3-items/znuFqFCIX1YQTcWx.htm)|Resonant Chimes|Resonant Chimes|modificada|
|[znWQpCRgJXaYEk5Q.htm](pathfinder-bestiary-3-items/znWQpCRgJXaYEk5Q.htm)|Grab|Agarrado|modificada|
|[zNYXcSmgZ8w8pTTE.htm](pathfinder-bestiary-3-items/zNYXcSmgZ8w8pTTE.htm)|Tremorsense (Imprecise) 15 feet|Sentido del Temblor (Impreciso) 15 pies|modificada|
|[Zo7w17AAhYEJgyRJ.htm](pathfinder-bestiary-3-items/Zo7w17AAhYEJgyRJ.htm)|Catch Rock|Atrapar roca|modificada|
|[zobdYiwWZOnM8PAI.htm](pathfinder-bestiary-3-items/zobdYiwWZOnM8PAI.htm)|Lifewick Candle|Lifewick Candle|modificada|
|[zoT7uILQYCXy5Dtz.htm](pathfinder-bestiary-3-items/zoT7uILQYCXy5Dtz.htm)|Grab|Agarrado|modificada|
|[zOtMfpFqx7YaAJxc.htm](pathfinder-bestiary-3-items/zOtMfpFqx7YaAJxc.htm)|Crystalline Dust|Polvo Cristalino|modificada|
|[Zp3zBQ50NNxsfjVx.htm](pathfinder-bestiary-3-items/Zp3zBQ50NNxsfjVx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZPn8LuEY8HTRPp38.htm](pathfinder-bestiary-3-items/ZPn8LuEY8HTRPp38.htm)|Withering Aura|Aura de marchitamiento|modificada|
|[zPSlJozXBmZIdCsT.htm](pathfinder-bestiary-3-items/zPSlJozXBmZIdCsT.htm)|Nervous Consumption|Consumo Nervioso|modificada|
|[zPU1mG2FJpTt19Z0.htm](pathfinder-bestiary-3-items/zPU1mG2FJpTt19Z0.htm)|Change Shape|Change Shape|modificada|
|[zpYoiV9dZOr8GH5F.htm](pathfinder-bestiary-3-items/zpYoiV9dZOr8GH5F.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[zQ1jyG0TlCxqXyuU.htm](pathfinder-bestiary-3-items/zQ1jyG0TlCxqXyuU.htm)|Jaws|Fauces|modificada|
|[zqnwkdM7RpErrayk.htm](pathfinder-bestiary-3-items/zqnwkdM7RpErrayk.htm)|Death Gasp|Death Gasp|modificada|
|[zR7WoyREJtjNrtuv.htm](pathfinder-bestiary-3-items/zR7WoyREJtjNrtuv.htm)|Rock|Roca|modificada|
|[zsHciQgmfpHT7Rdu.htm](pathfinder-bestiary-3-items/zsHciQgmfpHT7Rdu.htm)|Jaws|Fauces|modificada|
|[ZSiz1ntZMGGH8ef9.htm](pathfinder-bestiary-3-items/ZSiz1ntZMGGH8ef9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zSnETnJBgsrn1A04.htm](pathfinder-bestiary-3-items/zSnETnJBgsrn1A04.htm)|Earthmound Dweller|Earthmound Dweller|modificada|
|[ZsO3Se3p63hjEFm6.htm](pathfinder-bestiary-3-items/ZsO3Se3p63hjEFm6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZSyd5tDHWnYAj8zD.htm](pathfinder-bestiary-3-items/ZSyd5tDHWnYAj8zD.htm)|Spectral Jaws|Fauces Espectrales|modificada|
|[Zta2Kf1JwyI8OprE.htm](pathfinder-bestiary-3-items/Zta2Kf1JwyI8OprE.htm)|Burning Cold|Burning Cold|modificada|
|[ztbAEwewoiM3nyRj.htm](pathfinder-bestiary-3-items/ztbAEwewoiM3nyRj.htm)|Dagger|Daga|modificada|
|[zTdgUNL0sTJoxpwC.htm](pathfinder-bestiary-3-items/zTdgUNL0sTJoxpwC.htm)|Clutching Stones|Clutching Stones|modificada|
|[zTFuXpu6BuPdbg6N.htm](pathfinder-bestiary-3-items/zTFuXpu6BuPdbg6N.htm)|Brawling Critical|Brawling Critical|modificada|
|[Zu7OCWAY7oZL41Am.htm](pathfinder-bestiary-3-items/Zu7OCWAY7oZL41Am.htm)|Walk the Ethereal Line|Camina por la línea Etérea|modificada|
|[ZuL0FdvovBnvOlaq.htm](pathfinder-bestiary-3-items/ZuL0FdvovBnvOlaq.htm)|Negative Healing|Curación negativa|modificada|
|[zuM9eNga9GMtun7x.htm](pathfinder-bestiary-3-items/zuM9eNga9GMtun7x.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZUqkHDdNfKyhgkOs.htm](pathfinder-bestiary-3-items/ZUqkHDdNfKyhgkOs.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[zvCWR1v2of2jkxSl.htm](pathfinder-bestiary-3-items/zvCWR1v2of2jkxSl.htm)|Plague of Ancients|Plague of Ancients|modificada|
|[zVDg72KpqmTWTtgS.htm](pathfinder-bestiary-3-items/zVDg72KpqmTWTtgS.htm)|Doru Venom|Doru Venom|modificada|
|[ZVncjXiJwtqSbdbS.htm](pathfinder-bestiary-3-items/ZVncjXiJwtqSbdbS.htm)|+4 Status to All Saves vs. Mental or Divine|+4 situación a todas las salvaciones contra mental o divino.|modificada|
|[ZVnCqIGwZwPukDhh.htm](pathfinder-bestiary-3-items/ZVnCqIGwZwPukDhh.htm)|Spear|Lanza|modificada|
|[zwfNSb5Ft4v1QTW3.htm](pathfinder-bestiary-3-items/zwfNSb5Ft4v1QTW3.htm)|Rock|Roca|modificada|
|[zwNUc8bsuHFTyEqi.htm](pathfinder-bestiary-3-items/zwNUc8bsuHFTyEqi.htm)|Coiling Frenzy|Retorcer Frenesí|modificada|
|[zwPydFnSps4ZXlka.htm](pathfinder-bestiary-3-items/zwPydFnSps4ZXlka.htm)|Swarmwalker|Swarmwalker|modificada|
|[zXCRAQ4vboei7ilQ.htm](pathfinder-bestiary-3-items/zXCRAQ4vboei7ilQ.htm)|Carrion Scent (Imprecise) 30 feet|Olor a carroña (Impreciso) 30 pies|modificada|
|[ZXDEnUnH0Uz4H2J7.htm](pathfinder-bestiary-3-items/ZXDEnUnH0Uz4H2J7.htm)|Tremorsense (Precise) 120 feet, (Imprecise) 240 feet|Sentido del Temblor (Precisión) 120 pies, (Impreciso) 240 pies|modificada|
|[ZxPGNj1JDdTfH2vH.htm](pathfinder-bestiary-3-items/ZxPGNj1JDdTfH2vH.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[zxRmkFpH5qKW2WJe.htm](pathfinder-bestiary-3-items/zxRmkFpH5qKW2WJe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZY0LmvdvxDzWYzTp.htm](pathfinder-bestiary-3-items/ZY0LmvdvxDzWYzTp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[zY3L0jOu3AcqWwDa.htm](pathfinder-bestiary-3-items/zY3L0jOu3AcqWwDa.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[zY6HRNQ28oqHNnmV.htm](pathfinder-bestiary-3-items/zY6HRNQ28oqHNnmV.htm)|Unsettling Mind|Unsettling Mind|modificada|
|[ZYC0qWDiNlFEQBZ3.htm](pathfinder-bestiary-3-items/ZYC0qWDiNlFEQBZ3.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Zylzzu3xfzA6axxc.htm](pathfinder-bestiary-3-items/Zylzzu3xfzA6axxc.htm)|Craft Ice Staff|Elaborar bastón de hielo.|modificada|
|[zZ9i2cbCfyyfJXBh.htm](pathfinder-bestiary-3-items/zZ9i2cbCfyyfJXBh.htm)|Breath Weapon|Breath Weapon|modificada|
|[zZADJ8AZtILSidCx.htm](pathfinder-bestiary-3-items/zZADJ8AZtILSidCx.htm)|Horn|Cuerno|modificada|
|[zZdlKoZukVTRu2lC.htm](pathfinder-bestiary-3-items/zZdlKoZukVTRu2lC.htm)|Cavern Dependent|Cavern Dependent|modificada|
|[zZdUA3JVNbgUwFrw.htm](pathfinder-bestiary-3-items/zZdUA3JVNbgUwFrw.htm)|Jump Scare|Saltar Susto|modificada|
|[ZZpXzxmtbX8o1MxN.htm](pathfinder-bestiary-3-items/ZZpXzxmtbX8o1MxN.htm)|Vulnerable to Stone to Flesh|Vulnerable a Piedra a la carne|modificada|
|[zzRvWHp5hTbs93tu.htm](pathfinder-bestiary-3-items/zzRvWHp5hTbs93tu.htm)|Breath Weapon|Breath Weapon|modificada|
|[ZZwg2ODhydDeHkEy.htm](pathfinder-bestiary-3-items/ZZwg2ODhydDeHkEy.htm)|Scimitar Blitz|Scimitar Blitz|modificada|
|[zZzMJYEYbjykXd3S.htm](pathfinder-bestiary-3-items/zZzMJYEYbjykXd3S.htm)|Flamewing Buffet|Flamewing Buffet|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0F9P2pX66pZEF0GL.htm](pathfinder-bestiary-3-items/0F9P2pX66pZEF0GL.htm)|Ruins Lore (Applies Only to Their Home Ruins)|vacía|
|[0R9aK9TliKd2z99X.htm](pathfinder-bestiary-3-items/0R9aK9TliKd2z99X.htm)|Bardic Lore|vacía|
|[18UAUscmHJhznFH9.htm](pathfinder-bestiary-3-items/18UAUscmHJhznFH9.htm)|Society|vacía|
|[1CvWXq8Ii6oimfQW.htm](pathfinder-bestiary-3-items/1CvWXq8Ii6oimfQW.htm)|Stealth|vacía|
|[2Cym04NuN3V52Lkm.htm](pathfinder-bestiary-3-items/2Cym04NuN3V52Lkm.htm)|Performance|vacía|
|[2LqNyl4yrY4R0XPU.htm](pathfinder-bestiary-3-items/2LqNyl4yrY4R0XPU.htm)|Forest Lore (applies to the arboreal archive's territory)|vacía|
|[49TebdoZrVR7bJei.htm](pathfinder-bestiary-3-items/49TebdoZrVR7bJei.htm)|Art Lore|vacía|
|[4o2YdJckFmrO6XVf.htm](pathfinder-bestiary-3-items/4o2YdJckFmrO6XVf.htm)|Plane of Fire Lore|vacía|
|[4uqNBCMxlug85vdT.htm](pathfinder-bestiary-3-items/4uqNBCMxlug85vdT.htm)|Stealth|vacía|
|[5h9QlGG2MnMtfSRm.htm](pathfinder-bestiary-3-items/5h9QlGG2MnMtfSRm.htm)|Boneyard Lore|vacía|
|[5xKUaQZJHSM8chyv.htm](pathfinder-bestiary-3-items/5xKUaQZJHSM8chyv.htm)|Intimidation|vacía|
|[6kF88w6XCeKwvBaF.htm](pathfinder-bestiary-3-items/6kF88w6XCeKwvBaF.htm)|Stealth|vacía|
|[6twRlVuFcy8KOm6s.htm](pathfinder-bestiary-3-items/6twRlVuFcy8KOm6s.htm)|Farming Lore|vacía|
|[7Ik8WbV7r4dXszxh.htm](pathfinder-bestiary-3-items/7Ik8WbV7r4dXszxh.htm)|Acrobatics|vacía|
|[81AZ7vz3WmQESZ0c.htm](pathfinder-bestiary-3-items/81AZ7vz3WmQESZ0c.htm)|Rock|vacía|
|[831DXsppKlNomlSq.htm](pathfinder-bestiary-3-items/831DXsppKlNomlSq.htm)|Thievery|vacía|
|[8ZmjmY8SXUuq9HVi.htm](pathfinder-bestiary-3-items/8ZmjmY8SXUuq9HVi.htm)|Dreamlands Lore|vacía|
|[9RLSccx0GSuMZYk2.htm](pathfinder-bestiary-3-items/9RLSccx0GSuMZYk2.htm)|Plane of Air Lore|vacía|
|[9u2lwbCkserddtx7.htm](pathfinder-bestiary-3-items/9u2lwbCkserddtx7.htm)|Lore (any three)|vacía|
|[9xiLnsl12cVesxyN.htm](pathfinder-bestiary-3-items/9xiLnsl12cVesxyN.htm)|Crafting|vacía|
|[a2IB9CvFYzHYpXPK.htm](pathfinder-bestiary-3-items/a2IB9CvFYzHYpXPK.htm)|Athletics|vacía|
|[a9pJTuCtvZCUpnCc.htm](pathfinder-bestiary-3-items/a9pJTuCtvZCUpnCc.htm)|Deception|vacía|
|[Ahyotq9GfFcfraAt.htm](pathfinder-bestiary-3-items/Ahyotq9GfFcfraAt.htm)|Signet Ring|vacía|
|[bJtDTWk5J19kyrIU.htm](pathfinder-bestiary-3-items/bJtDTWk5J19kyrIU.htm)|Dimension of Time Lore|vacía|
|[BlSwWRr9dRFacb9z.htm](pathfinder-bestiary-3-items/BlSwWRr9dRFacb9z.htm)|Warfare Lore|vacía|
|[BvmTCIQKaeHE9MSd.htm](pathfinder-bestiary-3-items/BvmTCIQKaeHE9MSd.htm)|Shadow Plane Lore|vacía|
|[bXIGmBzDzq3zSjQh.htm](pathfinder-bestiary-3-items/bXIGmBzDzq3zSjQh.htm)|Sailing Lore|vacía|
|[BXslGLJsOIB3mptW.htm](pathfinder-bestiary-3-items/BXslGLJsOIB3mptW.htm)|Diplomacy|vacía|
|[C6Fl3KvNV5ml8FIL.htm](pathfinder-bestiary-3-items/C6Fl3KvNV5ml8FIL.htm)|Stealth|vacía|
|[CG0ROboEmdpQKFaJ.htm](pathfinder-bestiary-3-items/CG0ROboEmdpQKFaJ.htm)|Stealth|vacía|
|[cNgD6mqVpe7o2GOL.htm](pathfinder-bestiary-3-items/cNgD6mqVpe7o2GOL.htm)|Dungeon Lore|vacía|
|[Cs1rBxjb2dOVjN5V.htm](pathfinder-bestiary-3-items/Cs1rBxjb2dOVjN5V.htm)|Rock|vacía|
|[cVJFpIMkobfBe9gI.htm](pathfinder-bestiary-3-items/cVJFpIMkobfBe9gI.htm)|Sailing Lore|vacía|
|[daFMLxLznj0FiDbZ.htm](pathfinder-bestiary-3-items/daFMLxLznj0FiDbZ.htm)|Nirvana Lore|vacía|
|[dAiQDFnLDhubSBkJ.htm](pathfinder-bestiary-3-items/dAiQDFnLDhubSBkJ.htm)|Stealth|vacía|
|[dcxy9KeoPzEN2Fpd.htm](pathfinder-bestiary-3-items/dcxy9KeoPzEN2Fpd.htm)|Lore (any one)|vacía|
|[dEqNcyLplxjymRI4.htm](pathfinder-bestiary-3-items/dEqNcyLplxjymRI4.htm)|Athletics|vacía|
|[dJBsPpWCcZqoQuz1.htm](pathfinder-bestiary-3-items/dJBsPpWCcZqoQuz1.htm)|Bardic Lore|vacía|
|[dYaztoeE08mMvpAK.htm](pathfinder-bestiary-3-items/dYaztoeE08mMvpAK.htm)|Dimension of Time Lore|vacía|
|[DyOzh90VV1d6GTKS.htm](pathfinder-bestiary-3-items/DyOzh90VV1d6GTKS.htm)|Dimension of Time Lore|vacía|
|[e6Ag02OjJBtEzB5f.htm](pathfinder-bestiary-3-items/e6Ag02OjJBtEzB5f.htm)|Acrobatics|vacía|
|[e6ZkEbkFGX8LfATR.htm](pathfinder-bestiary-3-items/e6ZkEbkFGX8LfATR.htm)|Rock|vacía|
|[eIrU8U3KMyv0flIO.htm](pathfinder-bestiary-3-items/eIrU8U3KMyv0flIO.htm)|Deception|vacía|
|[eMLzHgxK6gQ67wx6.htm](pathfinder-bestiary-3-items/eMLzHgxK6gQ67wx6.htm)|Household Lore|vacía|
|[erx9pGmKEEP0jCHW.htm](pathfinder-bestiary-3-items/erx9pGmKEEP0jCHW.htm)|Festival Lore|vacía|
|[fCe9kl0ZVqmf44Qi.htm](pathfinder-bestiary-3-items/fCe9kl0ZVqmf44Qi.htm)|Midwifery Lore|vacía|
|[fj2rLzo717u5F5Ya.htm](pathfinder-bestiary-3-items/fj2rLzo717u5F5Ya.htm)|Positive Energy Plane Lore|vacía|
|[fS0jOjwX16xkNJ2N.htm](pathfinder-bestiary-3-items/fS0jOjwX16xkNJ2N.htm)|Pitchfork|vacía|
|[Fx2DEwhdjtpLUXBj.htm](pathfinder-bestiary-3-items/Fx2DEwhdjtpLUXBj.htm)|Sack for Holding Rocks|vacía|
|[gemxvf3zXMNV7oqP.htm](pathfinder-bestiary-3-items/gemxvf3zXMNV7oqP.htm)|Sack for Holding Rocks|vacía|
|[GeuKn3hCssJl4FAm.htm](pathfinder-bestiary-3-items/GeuKn3hCssJl4FAm.htm)|Plane of Earth Lore|vacía|
|[gjjjiArVeBThq31t.htm](pathfinder-bestiary-3-items/gjjjiArVeBThq31t.htm)|Library Lore|vacía|
|[gWmjAFdZNF6c51z1.htm](pathfinder-bestiary-3-items/gWmjAFdZNF6c51z1.htm)|Plane of Fire Lore|vacía|
|[hGcK7acpYirlNAXQ.htm](pathfinder-bestiary-3-items/hGcK7acpYirlNAXQ.htm)|Jewelry Lore|vacía|
|[hjpDAszGRHhxvxyq.htm](pathfinder-bestiary-3-items/hjpDAszGRHhxvxyq.htm)|Legal Lore|vacía|
|[HofMxPk6h7wuflyA.htm](pathfinder-bestiary-3-items/HofMxPk6h7wuflyA.htm)|Stealth|vacía|
|[hR97QpYRCioTid90.htm](pathfinder-bestiary-3-items/hR97QpYRCioTid90.htm)|Lore (Any one Celestial Plane)|vacía|
|[Hu6MV5KT0NEuooVU.htm](pathfinder-bestiary-3-items/Hu6MV5KT0NEuooVU.htm)|Acrobatics|vacía|
|[HZoHUgBTn23rMgPJ.htm](pathfinder-bestiary-3-items/HZoHUgBTn23rMgPJ.htm)|Meteorology Lore|vacía|
|[IbrrhH58fU066gLI.htm](pathfinder-bestiary-3-items/IbrrhH58fU066gLI.htm)|Nirvana Lore|vacía|
|[inTqnHZGNmB3Nv4k.htm](pathfinder-bestiary-3-items/inTqnHZGNmB3Nv4k.htm)|Crafting|vacía|
|[iqOncPekvnZawgEq.htm](pathfinder-bestiary-3-items/iqOncPekvnZawgEq.htm)|Meteorology Lore|vacía|
|[iwCc7o3EXz0xMRTD.htm](pathfinder-bestiary-3-items/iwCc7o3EXz0xMRTD.htm)|Legal Ledgers|vacía|
|[IXlGwryzhB2V9aPV.htm](pathfinder-bestiary-3-items/IXlGwryzhB2V9aPV.htm)|Stealth|vacía|
|[JK8lH0FZWQnphVLZ.htm](pathfinder-bestiary-3-items/JK8lH0FZWQnphVLZ.htm)|Farming Lore|vacía|
|[JVzWcWD2qbxHGbUb.htm](pathfinder-bestiary-3-items/JVzWcWD2qbxHGbUb.htm)|Athletics|vacía|
|[K1MeCVjxSahZAhHQ.htm](pathfinder-bestiary-3-items/K1MeCVjxSahZAhHQ.htm)|Sack for Holding Rocks|vacía|
|[kALPdWJYfV0Cg7Iu.htm](pathfinder-bestiary-3-items/kALPdWJYfV0Cg7Iu.htm)|Toenail Cutter|vacía|
|[Kf0yffdxraBrMmiJ.htm](pathfinder-bestiary-3-items/Kf0yffdxraBrMmiJ.htm)|Acrobatics|vacía|
|[KJRA38LAe9tJK0hJ.htm](pathfinder-bestiary-3-items/KJRA38LAe9tJK0hJ.htm)|Heraldry Lore|vacía|
|[KJxwCEgKTrckplr8.htm](pathfinder-bestiary-3-items/KJxwCEgKTrckplr8.htm)|Hell Lore|vacía|
|[kry7QWW5PONsNRlw.htm](pathfinder-bestiary-3-items/kry7QWW5PONsNRlw.htm)|Stealth|vacía|
|[KZzCxzwU4zROZ7As.htm](pathfinder-bestiary-3-items/KZzCxzwU4zROZ7As.htm)|Engineering Lore|vacía|
|[l7a80DGdIvJp8Pp7.htm](pathfinder-bestiary-3-items/l7a80DGdIvJp8Pp7.htm)|Dwelling Lore|vacía|
|[LK5MP99wRhzbl0Py.htm](pathfinder-bestiary-3-items/LK5MP99wRhzbl0Py.htm)|Settlement Lore|vacía|
|[lo1xAOTNNBx9CRHH.htm](pathfinder-bestiary-3-items/lo1xAOTNNBx9CRHH.htm)|Harp|vacía|
|[LOMkNEBVrbQNC9tu.htm](pathfinder-bestiary-3-items/LOMkNEBVrbQNC9tu.htm)|Acrobatics|vacía|
|[lxK1cONnIVJsnFVA.htm](pathfinder-bestiary-3-items/lxK1cONnIVJsnFVA.htm)|Sandals|vacía|
|[mo4QicMXhbgaEdej.htm](pathfinder-bestiary-3-items/mo4QicMXhbgaEdej.htm)|Stealth|vacía|
|[MPHAiW7bYkK3AAtF.htm](pathfinder-bestiary-3-items/MPHAiW7bYkK3AAtF.htm)|Millinery Lore|vacía|
|[MTzVT8iMpCahyDa9.htm](pathfinder-bestiary-3-items/MTzVT8iMpCahyDa9.htm)|Stealth|vacía|
|[nkZ7Ek2VIoobJ59S.htm](pathfinder-bestiary-3-items/nkZ7Ek2VIoobJ59S.htm)|Cooking Lore|vacía|
|[nLrqfymDMpj0e5mW.htm](pathfinder-bestiary-3-items/nLrqfymDMpj0e5mW.htm)|Stealth|vacía|
|[nOiQxEVLn3tVCv9I.htm](pathfinder-bestiary-3-items/nOiQxEVLn3tVCv9I.htm)|Monastic Lore|vacía|
|[NweZyZVjfaAj87xq.htm](pathfinder-bestiary-3-items/NweZyZVjfaAj87xq.htm)|Nirvana Lore|vacía|
|[OrjyA3TToykWjeyF.htm](pathfinder-bestiary-3-items/OrjyA3TToykWjeyF.htm)|Boneyard Lore|vacía|
|[OW53VBMI2Iw52vm3.htm](pathfinder-bestiary-3-items/OW53VBMI2Iw52vm3.htm)|Nature|vacía|
|[PbrxNFl95AGC5nKu.htm](pathfinder-bestiary-3-items/PbrxNFl95AGC5nKu.htm)|Diplomacy|vacía|
|[pFF77x8PA51YY8TF.htm](pathfinder-bestiary-3-items/pFF77x8PA51YY8TF.htm)|Household Lore|vacía|
|[poFV0K8IzaMCquPb.htm](pathfinder-bestiary-3-items/poFV0K8IzaMCquPb.htm)|Nirvana Lore|vacía|
|[PSRTnwfMOQYpIg4X.htm](pathfinder-bestiary-3-items/PSRTnwfMOQYpIg4X.htm)|Scribing Lore|vacía|
|[qbjGaacQh4FUm7lS.htm](pathfinder-bestiary-3-items/qbjGaacQh4FUm7lS.htm)|Athletics|vacía|
|[qmwnlcLuNKPhcNEX.htm](pathfinder-bestiary-3-items/qmwnlcLuNKPhcNEX.htm)|Undead Lore|vacía|
|[Qq0M2j9jLs4ePYWY.htm](pathfinder-bestiary-3-items/Qq0M2j9jLs4ePYWY.htm)|Athletics|vacía|
|[qqLORgTIh6GxitQS.htm](pathfinder-bestiary-3-items/qqLORgTIh6GxitQS.htm)|Lore (Any One)|vacía|
|[qsBmWCL2GK9QfAiP.htm](pathfinder-bestiary-3-items/qsBmWCL2GK9QfAiP.htm)|Crafting|vacía|
|[Qv1U20ksT1elULUW.htm](pathfinder-bestiary-3-items/Qv1U20ksT1elULUW.htm)|Rock|vacía|
|[Ran96W84fw5Bh3Y3.htm](pathfinder-bestiary-3-items/Ran96W84fw5Bh3Y3.htm)|Manticore Lore|vacía|
|[RscN8Wq0CouXZD3j.htm](pathfinder-bestiary-3-items/RscN8Wq0CouXZD3j.htm)|Plane of Water Lore|vacía|
|[RzulMB9xO8oiQIcf.htm](pathfinder-bestiary-3-items/RzulMB9xO8oiQIcf.htm)|Forest Lore|vacía|
|[sqYKSxNkOZ37Fmz6.htm](pathfinder-bestiary-3-items/sqYKSxNkOZ37Fmz6.htm)|Warfare Lore|vacía|
|[tasMLbX195fSmOL7.htm](pathfinder-bestiary-3-items/tasMLbX195fSmOL7.htm)|Engineering Lore|vacía|
|[tF1tkro4vKNfKEDE.htm](pathfinder-bestiary-3-items/tF1tkro4vKNfKEDE.htm)|Spiritual Rope|vacía|
|[TGEeTa0OpK8ohmMq.htm](pathfinder-bestiary-3-items/TGEeTa0OpK8ohmMq.htm)|Athletics|vacía|
|[tgfeTgJO1gvajbtU.htm](pathfinder-bestiary-3-items/tgfeTgJO1gvajbtU.htm)|Dimension of Time Lore|vacía|
|[tkG1nJtu7gnW9eby.htm](pathfinder-bestiary-3-items/tkG1nJtu7gnW9eby.htm)|Outrageous Hat|vacía|
|[tYt3A9hKDpzLb05E.htm](pathfinder-bestiary-3-items/tYt3A9hKDpzLb05E.htm)|Deception|vacía|
|[u1oQzKxAI1vhe2bM.htm](pathfinder-bestiary-3-items/u1oQzKxAI1vhe2bM.htm)|Confectionery Lore|vacía|
|[uCO7vGNGI7hr4h24.htm](pathfinder-bestiary-3-items/uCO7vGNGI7hr4h24.htm)|Stealth|vacía|
|[UNgk0PBDduIdcbGj.htm](pathfinder-bestiary-3-items/UNgk0PBDduIdcbGj.htm)|Ocean Lore|vacía|
|[UR0KFHIUkZ47JTL0.htm](pathfinder-bestiary-3-items/UR0KFHIUkZ47JTL0.htm)|Athletics|vacía|
|[uUe3OiXyQgpCeszE.htm](pathfinder-bestiary-3-items/uUe3OiXyQgpCeszE.htm)|Lore (Its Home Settlement or Country)|vacía|
|[uxgEQgxtbzCI4zVo.htm](pathfinder-bestiary-3-items/uxgEQgxtbzCI4zVo.htm)|Athletics|vacía|
|[Uy3YAPSoQX8UsHK3.htm](pathfinder-bestiary-3-items/Uy3YAPSoQX8UsHK3.htm)|Athletics|vacía|
|[V3rzyrOOf8B6y3I2.htm](pathfinder-bestiary-3-items/V3rzyrOOf8B6y3I2.htm)|Silver Scissors|vacía|
|[vewX6T3r92AKkxif.htm](pathfinder-bestiary-3-items/vewX6T3r92AKkxif.htm)|Dark Tapestry Lore|vacía|
|[vgGl8uaNNQbavIkC.htm](pathfinder-bestiary-3-items/vgGl8uaNNQbavIkC.htm)|Stealth|vacía|
|[VZit0bc3TQKyAAbe.htm](pathfinder-bestiary-3-items/VZit0bc3TQKyAAbe.htm)|Shadow Plane Lore|vacía|
|[WavB32uYsyFzI2uA.htm](pathfinder-bestiary-3-items/WavB32uYsyFzI2uA.htm)|Performance|vacía|
|[WDHTK76R7cwm5ZSc.htm](pathfinder-bestiary-3-items/WDHTK76R7cwm5ZSc.htm)|Intimidation|vacía|
|[WdP7r88rTRk6Y9Lu.htm](pathfinder-bestiary-3-items/WdP7r88rTRk6Y9Lu.htm)|Positive Energy Plane Lore|vacía|
|[wwKhJGtC2Ah8Opom.htm](pathfinder-bestiary-3-items/wwKhJGtC2Ah8Opom.htm)|Satchel for Holding Rocks|vacía|
|[xdDVdaosFR2v4kpi.htm](pathfinder-bestiary-3-items/xdDVdaosFR2v4kpi.htm)|Black Onyx Gems|vacía|
|[xFzHJGL72aPpRLgB.htm](pathfinder-bestiary-3-items/xFzHJGL72aPpRLgB.htm)|Lore (Any One Subcategory)|vacía|
|[xGCY0XLJysaFyIyL.htm](pathfinder-bestiary-3-items/xGCY0XLJysaFyIyL.htm)|Labor Lore|vacía|
|[xKAa1bclabyOkPCJ.htm](pathfinder-bestiary-3-items/xKAa1bclabyOkPCJ.htm)|Pliers|vacía|
|[xLpHFUCeWtRMaSOY.htm](pathfinder-bestiary-3-items/xLpHFUCeWtRMaSOY.htm)|Crafting|vacía|
|[XzaWmxiliLGXDQy7.htm](pathfinder-bestiary-3-items/XzaWmxiliLGXDQy7.htm)|Lore (Associated with the Guardian's Need)|vacía|
|[y5xPl9xFyELDGekP.htm](pathfinder-bestiary-3-items/y5xPl9xFyELDGekP.htm)|Desert Lore|vacía|
|[ycvxnNDxb7l6WNHB.htm](pathfinder-bestiary-3-items/ycvxnNDxb7l6WNHB.htm)|Athletics|vacía|
|[YHH8WnAJViHJAQnW.htm](pathfinder-bestiary-3-items/YHH8WnAJViHJAQnW.htm)|Household Lore|vacía|
|[yUtAKhKRZtc1R8LE.htm](pathfinder-bestiary-3-items/yUtAKhKRZtc1R8LE.htm)|Athletics|vacía|
|[YwaUnI2GQySpg6fO.htm](pathfinder-bestiary-3-items/YwaUnI2GQySpg6fO.htm)|Stealth|vacía|
|[Z67Fh0402PQRc5fp.htm](pathfinder-bestiary-3-items/Z67Fh0402PQRc5fp.htm)|Weather Lore|vacía|
|[Zh8VyOJVStjNfjec.htm](pathfinder-bestiary-3-items/Zh8VyOJVStjNfjec.htm)|Acrobatics|vacía|
|[zTJl5YEwlSQQwXu1.htm](pathfinder-bestiary-3-items/zTJl5YEwlSQQwXu1.htm)|Stealth|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
