# Estado de la traducción (menace-under-otari-bestiary)

 * **modificada**: 62
 * **oficial**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0oWKApY5FR8IO7GG.htm](menace-under-otari-bestiary/0oWKApY5FR8IO7GG.htm)|Orc Scrapper (BB)|Orc Scrapper (BB)|modificada|
|[0plBflWwrCWQO2RO.htm](menace-under-otari-bestiary/0plBflWwrCWQO2RO.htm)|Zombie Shambler (BB)|Zombi tambaleante (BB)|modificada|
|[2H2AEwQnfKJC0nrd.htm](menace-under-otari-bestiary/2H2AEwQnfKJC0nrd.htm)|Ghost Commoner (BB)|Común fantasma de plebeyo (BB)|modificada|
|[4Axci50gPQArPg7d.htm](menace-under-otari-bestiary/4Axci50gPQArPg7d.htm)|Kobold Trapmaster (BB)|Kobold Trapmaster (BB)|modificada|
|[4MwjCsa5O9aAjxSm.htm](menace-under-otari-bestiary/4MwjCsa5O9aAjxSm.htm)|Boar (BB)|Jabalí (BB)|modificada|
|[4O7wKZdeAemTEbvG.htm](menace-under-otari-bestiary/4O7wKZdeAemTEbvG.htm)|Slamming Door (BB)|Portazo (BB)|modificada|
|[5H8ZX7y5IkUBhvhF.htm](menace-under-otari-bestiary/5H8ZX7y5IkUBhvhF.htm)|Skeleton Guard (BB)|Guardia Esqueleto (BB)|modificada|
|[5MVBU86ZRw2ANMQn.htm](menace-under-otari-bestiary/5MVBU86ZRw2ANMQn.htm)|Skeletal Giant (BB)|Gigante esquelético (BB)|modificada|
|[5xjmJoJvBhASkEKS.htm](menace-under-otari-bestiary/5xjmJoJvBhASkEKS.htm)|Orc Commander (BB)|Comandante orco (BB)|modificada|
|[7VqibTAEXXX6PIhh.htm](menace-under-otari-bestiary/7VqibTAEXXX6PIhh.htm)|Scythe Blades (BB)|Cuchillas de guada (BB)|modificada|
|[93hZtLl9pRRfqI05.htm](menace-under-otari-bestiary/93hZtLl9pRRfqI05.htm)|Blue Kobold Dragon Mage (BB)|Mago dracónico kobold azul (BB)|modificada|
|[9sa2KE4Fbh3OPH7M.htm](menace-under-otari-bestiary/9sa2KE4Fbh3OPH7M.htm)|Brine Shark (BB)|Tiburón de salmuera (BB)|modificada|
|[AdQVjlOWB6rmBRVp.htm](menace-under-otari-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger (BB)|Doppelganger (BB)|modificada|
|[aeCoh4u6c5kt1iCs.htm](menace-under-otari-bestiary/aeCoh4u6c5kt1iCs.htm)|Gargoyle (BB)|Gargoyle (BB)|modificada|
|[AleeS0IRqT4tUphB.htm](menace-under-otari-bestiary/AleeS0IRqT4tUphB.htm)|Kobold Boss Zolgran (BB)|Kobold Jefe Zolgran (BB)|modificada|
|[AuCC04X2AO8oFN75.htm](menace-under-otari-bestiary/AuCC04X2AO8oFN75.htm)|Harpy (BB)|Arpía (BB)|modificada|
|[AYwdybUfm4meGUTJ.htm](menace-under-otari-bestiary/AYwdybUfm4meGUTJ.htm)|Giant Rat (BB)|Rata gigante (BB)|modificada|
|[BHq5wpQU8hQEke8D.htm](menace-under-otari-bestiary/BHq5wpQU8hQEke8D.htm)|Hidden Pit (BB)|Foso escondido (BB)|modificada|
|[BKPRkJgq7ehsW7uX.htm](menace-under-otari-bestiary/BKPRkJgq7ehsW7uX.htm)|Giant Centipede (BB)|Ciempiés Gigante (BB)|modificada|
|[Br1AtKUHe3nbzjnY.htm](menace-under-otari-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimic (BB)|Mimic (BB)|modificada|
|[cBHpMcVaLRPZu9po.htm](menace-under-otari-bestiary/cBHpMcVaLRPZu9po.htm)|Zephyr Hawk (BB)|Halcón de céfiro (BB)|modificada|
|[CJuHwIRCAgTB1SEl.htm](menace-under-otari-bestiary/CJuHwIRCAgTB1SEl.htm)|Red Kobold Dragon Mage (BB)|Mago dracónico kobold rojo (BB)|modificada|
|[cZDiyluplFqRxmGy.htm](menace-under-otari-bestiary/cZDiyluplFqRxmGy.htm)|Animated Armor (BB)|Armadura animada (BB)|modificada|
|[EtRqBsWh1Hv1toqh.htm](menace-under-otari-bestiary/EtRqBsWh1Hv1toqh.htm)|Orc Trooper (BB)|Orc Trooper (BB)|modificada|
|[FaBHkmFGuEIqIYM1.htm](menace-under-otari-bestiary/FaBHkmFGuEIqIYM1.htm)|Drow Priestess (BB)|Sacerdotisa Drow (BB)|modificada|
|[gdXok08bITkhowDJ.htm](menace-under-otari-bestiary/gdXok08bITkhowDJ.htm)|Ogre Warrior (BB)|Ogro combatiente (BB)|modificada|
|[gvCCATlH9mPGWbsp.htm](menace-under-otari-bestiary/gvCCATlH9mPGWbsp.htm)|Troll (BB)|Troll (BB)|modificada|
|[hiGwRWdxAsoCII4f.htm](menace-under-otari-bestiary/hiGwRWdxAsoCII4f.htm)|Cinder Rat (BB)|Rata de ceniza (BB)|modificada|
|[Hkq9ZS2J2iKnT7vT.htm](menace-under-otari-bestiary/Hkq9ZS2J2iKnT7vT.htm)|Sewer Ooze (BB)|Exudado de alcantarilla (BB)|modificada|
|[j8qD2LVDSP2lhLUO.htm](menace-under-otari-bestiary/j8qD2LVDSP2lhLUO.htm)|Central Spears (BB)|Lanzas centrales (BB)|modificada|
|[jeAGl6OAVrrIPgu3.htm](menace-under-otari-bestiary/jeAGl6OAVrrIPgu3.htm)|Hell Hound (BB)|Sabueso infernal (BB)|modificada|
|[jGzVwekcRX5aQpbT.htm](menace-under-otari-bestiary/jGzVwekcRX5aQpbT.htm)|Goblin Commando (BB)|Goblin Commando (BB)|modificada|
|[jnmUcTs4hn1c5bz9.htm](menace-under-otari-bestiary/jnmUcTs4hn1c5bz9.htm)|Pugwampi (BB)|Pugwampi (BB)|modificada|
|[jVZRROs0GzDjVrgi.htm](menace-under-otari-bestiary/jVZRROs0GzDjVrgi.htm)|Goblin Warrior (BB)|Guerrero Goblin (BB)|modificada|
|[kCRfBZqCugMQmdpd.htm](menace-under-otari-bestiary/kCRfBZqCugMQmdpd.htm)|White Kobold Dragon Mage (BB)|Mago dracónico kobold blanco (BB)|modificada|
|[KsWAIXTTh3mfNWOY.htm](menace-under-otari-bestiary/KsWAIXTTh3mfNWOY.htm)|Giant Viper (BB)|Víbora gigante (BB)|modificada|
|[lFlXmieuHTBIonhj.htm](menace-under-otari-bestiary/lFlXmieuHTBIonhj.htm)|Viper (BB)|Viper (BB)|modificada|
|[LHHgGSs0ELCR4CYK.htm](menace-under-otari-bestiary/LHHgGSs0ELCR4CYK.htm)|Ghoul (BB)|Ghoul (BB)|modificada|
|[M8oJOKJ4AgrLZcJQ.htm](menace-under-otari-bestiary/M8oJOKJ4AgrLZcJQ.htm)|Hobgoblin Warrior (BB)|Guerrero Hobgoblin (BB)|modificada|
|[NVWaLagWOu5tCCZu.htm](menace-under-otari-bestiary/NVWaLagWOu5tCCZu.htm)|Sod Hound (BB)|Sabueso terroso (BB)|modificada|
|[Oilfs8Atv2LjAsUS.htm](menace-under-otari-bestiary/Oilfs8Atv2LjAsUS.htm)|Wolf (BB)|Lobo (BB)|modificada|
|[pw2NFqvkDm54lsbt.htm](menace-under-otari-bestiary/pw2NFqvkDm54lsbt.htm)|Envenomed Lock (BB)|Cerradura envenenada (BB)|modificada|
|[QaldZV2p9RpMXzzn.htm](menace-under-otari-bestiary/QaldZV2p9RpMXzzn.htm)|Green Kobold Dragon Mage (BB)|Mago dracónico kobold verde (BB)|modificada|
|[R9eoGwQ2tudxUKxS.htm](menace-under-otari-bestiary/R9eoGwQ2tudxUKxS.htm)|Black Kobold Dragon Mage (BB)|Mago dracónico kobold negro (BB)|modificada|
|[r9w1n85mp9Ip4QiS.htm](menace-under-otari-bestiary/r9w1n85mp9Ip4QiS.htm)|Kobold Warrior (BB)|Kóbold combatiente (BB)|modificada|
|[rPaHIh0ICnTLnRO6.htm](menace-under-otari-bestiary/rPaHIh0ICnTLnRO6.htm)|Kobold Scout (BB)|Kóbold explorador (BB)|modificada|
|[rPHxXClTnoPYHYuZ.htm](menace-under-otari-bestiary/rPHxXClTnoPYHYuZ.htm)|Basilisk (BB)|Basilisk (BB)|modificada|
|[RTzFvmdSCf5yhguy.htm](menace-under-otari-bestiary/RTzFvmdSCf5yhguy.htm)|Xulgath Warrior (BB)|Xulgath combatiente (BB)|modificada|
|[shT19KaQjWRVrHLI.htm](menace-under-otari-bestiary/shT19KaQjWRVrHLI.htm)|Goblin Igniter (BB)|Goblin incendiario (BB)|modificada|
|[sW8koPLrSgHalAnq.htm](menace-under-otari-bestiary/sW8koPLrSgHalAnq.htm)|Drow Warrior (BB)|Drow combatiente (BB)|modificada|
|[UjREHs2JQoO85Glt.htm](menace-under-otari-bestiary/UjREHs2JQoO85Glt.htm)|Bugbear Marauder (BB)|Bugbear Marauder (BB)|modificada|
|[v51J7K27abdDyLgJ.htm](menace-under-otari-bestiary/v51J7K27abdDyLgJ.htm)|Mermaid Fountain (BB)|Fuente de la Sirena (BB)|modificada|
|[vlMuFskctUvjJe8X.htm](menace-under-otari-bestiary/vlMuFskctUvjJe8X.htm)|Spear Launcher (BB)|Arrojador de lanzas (BB)|modificada|
|[WPsgrCUSFCqgDvJi.htm](menace-under-otari-bestiary/WPsgrCUSFCqgDvJi.htm)|Green Dragon Wyrmling (BB)|Green Dragon Wyrmling (BB)|modificada|
|[wqPYzMNgYvrO6oEP.htm](menace-under-otari-bestiary/wqPYzMNgYvrO6oEP.htm)|Leopard (BB)|Leopard (BB)|modificada|
|[X03vq2RWi2jiA6Ri.htm](menace-under-otari-bestiary/X03vq2RWi2jiA6Ri.htm)|Owlbear (BB)|Owlbear (BB)|modificada|
|[xKYIN88ULPFgSZmw.htm](menace-under-otari-bestiary/xKYIN88ULPFgSZmw.htm)|Drow Sneak (BB)|Movimiento furtivo drow (BB)|modificada|
|[XrmHgbKgcHDi4OnK.htm](menace-under-otari-bestiary/XrmHgbKgcHDi4OnK.htm)|Shadow (BB)|Sombra (BB)|modificada|
|[YdBCG0vzOA5BgoIi.htm](menace-under-otari-bestiary/YdBCG0vzOA5BgoIi.htm)|Xulgath Boss (BB)|Xulgath Jefe (BB)|modificada|
|[Z9ggO7spfHwr8up1.htm](menace-under-otari-bestiary/Z9ggO7spfHwr8up1.htm)|Falling Ceiling (BB)|Falling Ceiling (BB)|modificada|
|[ZMr28tFTA5NUcBTi.htm](menace-under-otari-bestiary/ZMr28tFTA5NUcBTi.htm)|Web Lurker (BB)|Acechador telara (BB)|modificada|
|[ZPjQkKVMi3xoPcU0.htm](menace-under-otari-bestiary/ZPjQkKVMi3xoPcU0.htm)|Wight (BB)|Wight (BB)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[6NdqvKIlxo4cGhf8.htm](menace-under-otari-bestiary/6NdqvKIlxo4cGhf8.htm)|Giant Spider (BB)|Araña gigante (BB)|oficial|
