# Estado de la traducción (extinction-curse-bestiary-items)

 * **modificada**: 1293
 * **ninguna**: 221
 * **vacía**: 73


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0uWgmxpffv5a1h3J.htm](extinction-curse-bestiary-items/0uWgmxpffv5a1h3J.htm)|Detect Alignment (Constant) (Good Only)|
|[14Udi3de3xejTJ37.htm](extinction-curse-bestiary-items/14Udi3de3xejTJ37.htm)|Calm Emotions (At Will)|
|[1CtLUNvKIjzCyJIK.htm](extinction-curse-bestiary-items/1CtLUNvKIjzCyJIK.htm)|Religious Symbol of Zevgavizeb (Bone)|
|[1IjRwDjNaN8Y7Fm0.htm](extinction-curse-bestiary-items/1IjRwDjNaN8Y7Fm0.htm)|Religious Symbol of Zevgavizeb (Silver)|
|[2La0q0KxbfIUFD3k.htm](extinction-curse-bestiary-items/2La0q0KxbfIUFD3k.htm)|Faerie Fire (At Will)|
|[2SeK0haJzlBzrLGe.htm](extinction-curse-bestiary-items/2SeK0haJzlBzrLGe.htm)|Concentrated Xulgath Bile (Infused)|
|[2y98GhAzbFbtTv88.htm](extinction-curse-bestiary-items/2y98GhAzbFbtTv88.htm)|Bastard Sword|+2,greaterStriking|
|[34564bR2UkVUdEBz.htm](extinction-curse-bestiary-items/34564bR2UkVUdEBz.htm)|Maul|+1,striking|
|[3BzPIWSalYh96dFx.htm](extinction-curse-bestiary-items/3BzPIWSalYh96dFx.htm)|Wand of Magic Mouth (Level 2)|
|[3pFc3GsLQcSTVkLk.htm](extinction-curse-bestiary-items/3pFc3GsLQcSTVkLk.htm)|Summon Animal (Dinosaurs Only)|
|[3rsC0fxsq7KltGj5.htm](extinction-curse-bestiary-items/3rsC0fxsq7KltGj5.htm)|+2 Resilient Leather Armor|
|[3yrncDBmozmswIwg.htm](extinction-curse-bestiary-items/3yrncDBmozmswIwg.htm)|True Seeing (Constant)|
|[4cj7pDegwTLGP1gx.htm](extinction-curse-bestiary-items/4cj7pDegwTLGP1gx.htm)|Invisibility (Self Only)|
|[4Ct2SmjptIVcbVSy.htm](extinction-curse-bestiary-items/4Ct2SmjptIVcbVSy.htm)|Darkness (At Will)|
|[4nBcbBTCqRjCvM7x.htm](extinction-curse-bestiary-items/4nBcbBTCqRjCvM7x.htm)|Invisibility (At-Will) (Self Only)|
|[5dfhS831WAAuyoAD.htm](extinction-curse-bestiary-items/5dfhS831WAAuyoAD.htm)|Composite Longbow|+1,striking|
|[5fE5X1TnaedkAQkG.htm](extinction-curse-bestiary-items/5fE5X1TnaedkAQkG.htm)|Fear (At-Will)|
|[5hkkuahwgVNIk8o8.htm](extinction-curse-bestiary-items/5hkkuahwgVNIk8o8.htm)|Wand of Heal (Level 2)|
|[5JvYtzfHEmAqqWPv.htm](extinction-curse-bestiary-items/5JvYtzfHEmAqqWPv.htm)|Illusory Scene (At Will)|
|[5NVqmIO25meno1io.htm](extinction-curse-bestiary-items/5NVqmIO25meno1io.htm)|Hand Crossbow|+2,greaterStriking|
|[5WvF0LR7Xr4lkeWV.htm](extinction-curse-bestiary-items/5WvF0LR7Xr4lkeWV.htm)|Composite Longbow|+2,greaterStriking|
|[6H5o1EKmMK9GLW0X.htm](extinction-curse-bestiary-items/6H5o1EKmMK9GLW0X.htm)|Scroll of Unrelenting Observation (Level 8)|
|[6j7TXX4foQ6J6jT6.htm](extinction-curse-bestiary-items/6j7TXX4foQ6J6jT6.htm)|+1 Striking Mancatcher|+1,striking|
|[6Q3lgvUWzPKUKxeH.htm](extinction-curse-bestiary-items/6Q3lgvUWzPKUKxeH.htm)|Expert Alchemist's Tools|
|[6qV8HNnpHA2TjjBA.htm](extinction-curse-bestiary-items/6qV8HNnpHA2TjjBA.htm)|Acid Flask (Greater) (Infused)|
|[6tlcY6vJ6xRLoy23.htm](extinction-curse-bestiary-items/6tlcY6vJ6xRLoy23.htm)|Longsword|+3,majorStriking|
|[6yZT0Oua0CQQmx9r.htm](extinction-curse-bestiary-items/6yZT0Oua0CQQmx9r.htm)|Alchemist's Fire (Greater) (Infused)|
|[7e908l7OkfXJHtHN.htm](extinction-curse-bestiary-items/7e908l7OkfXJHtHN.htm)|Animal Vision (At Will) (Dinosaurs Only)|
|[7PZLjnzWIYsyU8k9.htm](extinction-curse-bestiary-items/7PZLjnzWIYsyU8k9.htm)|Greatclub|+1,striking|
|[8uTem9WAYzYRXxjq.htm](extinction-curse-bestiary-items/8uTem9WAYzYRXxjq.htm)|Tongues (Constant)|
|[954oCjihi0rQxaMN.htm](extinction-curse-bestiary-items/954oCjihi0rQxaMN.htm)|Paranoia (Animals Only)|
|[98XSigNoV6MVwImS.htm](extinction-curse-bestiary-items/98XSigNoV6MVwImS.htm)|Acid Flask (Greater) [Infused]|
|[9r5OSSnhL1XGFAvn.htm](extinction-curse-bestiary-items/9r5OSSnhL1XGFAvn.htm)|Gray Robes|
|[9rFk9LIKScKcgEaC.htm](extinction-curse-bestiary-items/9rFk9LIKScKcgEaC.htm)|Air Walk (Constant)|
|[9siPff9BhSHUe2TK.htm](extinction-curse-bestiary-items/9siPff9BhSHUe2TK.htm)|Scroll of False Life (Level 2)|
|[9ysQBDvKbsfYCZAe.htm](extinction-curse-bestiary-items/9ysQBDvKbsfYCZAe.htm)|Rapier|+1|
|[a06YRZqcMZjOHFeJ.htm](extinction-curse-bestiary-items/a06YRZqcMZjOHFeJ.htm)|Spellbook|
|[aBlnDa69uYoI9jdA.htm](extinction-curse-bestiary-items/aBlnDa69uYoI9jdA.htm)|Sleep (At Will)|
|[AHRPf1UGPJOGMM9l.htm](extinction-curse-bestiary-items/AHRPf1UGPJOGMM9l.htm)|Dispel Magic (At Will)|
|[AYJVxqIbMbM6Hom4.htm](extinction-curse-bestiary-items/AYJVxqIbMbM6Hom4.htm)|Rhoka Sword|+2,striking|
|[beKDaFwTUAjBlXuY.htm](extinction-curse-bestiary-items/beKDaFwTUAjBlXuY.htm)|Phantom Pain (At Will)|
|[bGV9QKdEo62zbwAD.htm](extinction-curse-bestiary-items/bGV9QKdEo62zbwAD.htm)|+1 Resilient Bone Armor|
|[BoGD0m0tUXjkgQax.htm](extinction-curse-bestiary-items/BoGD0m0tUXjkgQax.htm)|Detect Alignment (Constant)(Evil Only)|
|[btfD7bWkD0rzi4JA.htm](extinction-curse-bestiary-items/btfD7bWkD0rzi4JA.htm)|Shortsword|+1,striking|
|[c9o8wtXKr3BgyUJa.htm](extinction-curse-bestiary-items/c9o8wtXKr3BgyUJa.htm)|Spear|+2,striking|
|[Cc90LRPBjtIS0WLk.htm](extinction-curse-bestiary-items/Cc90LRPBjtIS0WLk.htm)|Invisibility (At Will) (Self Only)|
|[ClFUTI0Jfb1az69J.htm](extinction-curse-bestiary-items/ClFUTI0Jfb1az69J.htm)|Invisibility (At Will)|
|[cM3DMiodWWRFm8ve.htm](extinction-curse-bestiary-items/cM3DMiodWWRFm8ve.htm)|+2 Greater Resilient Chain Mail|
|[CNoZswC3ObHsm2Jd.htm](extinction-curse-bestiary-items/CNoZswC3ObHsm2Jd.htm)|Knock (At-Will)|
|[Cp4os9TY3OTwnTf2.htm](extinction-curse-bestiary-items/Cp4os9TY3OTwnTf2.htm)|Composite Shortbow|+2,greaterStriking|
|[cs9D6zSPlntBJcWq.htm](extinction-curse-bestiary-items/cs9D6zSPlntBJcWq.htm)|Detect Magic (Constant)|
|[CucvL4Psm96kCofz.htm](extinction-curse-bestiary-items/CucvL4Psm96kCofz.htm)|Fear (At Will)|
|[cWgbf0nThpGkUwma.htm](extinction-curse-bestiary-items/cWgbf0nThpGkUwma.htm)|Tongues (Constant)|
|[CwrLtsP8ondPDdfF.htm](extinction-curse-bestiary-items/CwrLtsP8ondPDdfF.htm)|Telekinetic Haul (At Will)|
|[D6Xun7rk5Brir0IF.htm](extinction-curse-bestiary-items/D6Xun7rk5Brir0IF.htm)|Speak with Animals (Constant)|
|[dfYmwPtqFedXkfE7.htm](extinction-curse-bestiary-items/dfYmwPtqFedXkfE7.htm)|True Seeing (Constant)|
|[dnBXS9fUYPg74ls2.htm](extinction-curse-bestiary-items/dnBXS9fUYPg74ls2.htm)|True Seeing (Constant)|
|[DPKdvyRQD79RjlTV.htm](extinction-curse-bestiary-items/DPKdvyRQD79RjlTV.htm)|Summon Fiend (Daemons Only)|
|[dweCpMS4m3DyGNS6.htm](extinction-curse-bestiary-items/dweCpMS4m3DyGNS6.htm)|Tanglefoot Bag (Greater) (Infused)|
|[dWpvSluY2OjLNlab.htm](extinction-curse-bestiary-items/dWpvSluY2OjLNlab.htm)|Detect Magic (Constant)|
|[dxePtwfdF42mXjHb.htm](extinction-curse-bestiary-items/dxePtwfdF42mXjHb.htm)|Religious Symbol (Silver) of Aroden|
|[E2nQYNcliQTmFXuG.htm](extinction-curse-bestiary-items/E2nQYNcliQTmFXuG.htm)|Darkness (At Will)|
|[EAbi0YGQcDdrK1rK.htm](extinction-curse-bestiary-items/EAbi0YGQcDdrK1rK.htm)|+2 Resilient Leather Armor|
|[EC6gyIBni8OO9swx.htm](extinction-curse-bestiary-items/EC6gyIBni8OO9swx.htm)|Ethereal Jaunt (At Will) (From Heartstone)|
|[ef5iLBGiYGg2sKRf.htm](extinction-curse-bestiary-items/ef5iLBGiYGg2sKRf.htm)|Chief Constable's Badge|
|[EK9FzoYNZUXr3Mk4.htm](extinction-curse-bestiary-items/EK9FzoYNZUXr3Mk4.htm)|Dimension Door (At Will)|
|[En5XyqbwPFuM1ya6.htm](extinction-curse-bestiary-items/En5XyqbwPFuM1ya6.htm)|Ranseur|+1,striking|
|[eQesIwS05im7it7H.htm](extinction-curse-bestiary-items/eQesIwS05im7it7H.htm)|Rhoka Sword|+2,greaterStriking|
|[EroB5CcwaWtvURqP.htm](extinction-curse-bestiary-items/EroB5CcwaWtvURqP.htm)|Fear (At Will)|
|[eZpEJwLSEEfDXS1R.htm](extinction-curse-bestiary-items/eZpEJwLSEEfDXS1R.htm)|Bastard Sword|+1,striking|
|[ezZxuEc2ENbqDDnV.htm](extinction-curse-bestiary-items/ezZxuEc2ENbqDDnV.htm)|Hand Crossbow|+1,striking|
|[f6pIZeCMzq997PVq.htm](extinction-curse-bestiary-items/f6pIZeCMzq997PVq.htm)|Maul|+2,greaterStriking|
|[FaneRZfLNKwW9Fj3.htm](extinction-curse-bestiary-items/FaneRZfLNKwW9Fj3.htm)|Alchemist's Fire (Greater) [Infused]|
|[FaPP84ihUiXrLLm8.htm](extinction-curse-bestiary-items/FaPP84ihUiXrLLm8.htm)|True Seeing (Constant)|
|[FEN9wcbD8tt6FXsm.htm](extinction-curse-bestiary-items/FEN9wcbD8tt6FXsm.htm)|Gaseous Form (Self Only)|
|[FFhpizMPIxIyhvzO.htm](extinction-curse-bestiary-items/FFhpizMPIxIyhvzO.htm)|Harrow Deck (Metal)|
|[fIVKEuRBev6cm8EW.htm](extinction-curse-bestiary-items/fIVKEuRBev6cm8EW.htm)|Status (At Will)|
|[FJ5ZIeD2msGVDFXU.htm](extinction-curse-bestiary-items/FJ5ZIeD2msGVDFXU.htm)|Shortsword|+2,greaterStriking|
|[fLgTnGYjeLODWrZ5.htm](extinction-curse-bestiary-items/fLgTnGYjeLODWrZ5.htm)|Illusory Disguise (At Will)|
|[fNT0RdVIPBpQZgpm.htm](extinction-curse-bestiary-items/fNT0RdVIPBpQZgpm.htm)|Whip of Compliance|+1,striking|
|[ftecIzpBFfnsQX7L.htm](extinction-curse-bestiary-items/ftecIzpBFfnsQX7L.htm)|Blind Ambition (At Will) (Emotional Focus)|
|[ftRL4ZW49NsWC8tu.htm](extinction-curse-bestiary-items/ftRL4ZW49NsWC8tu.htm)|Spear|+2,greaterStriking|
|[Fu8tkGZao9c5IYgV.htm](extinction-curse-bestiary-items/Fu8tkGZao9c5IYgV.htm)|Telekinetic Maneuver (At Will)|
|[fwSryrzGPdQfxaBv.htm](extinction-curse-bestiary-items/fwSryrzGPdQfxaBv.htm)|Animal Vision (At Will) (Target Must be a Rodent, Serpent, or Similar Animal Considered Troublesome by Humans)|
|[FzdAA0olz3ivmV27.htm](extinction-curse-bestiary-items/FzdAA0olz3ivmV27.htm)|Bravo's Brew (Moderate) (Infused)|
|[G4RxtZW5Dd8XP74g.htm](extinction-curse-bestiary-items/G4RxtZW5Dd8XP74g.htm)|Wand of Ray of Enfeeblement (Level 2)|
|[GBZBg2dACMgs5Uz2.htm](extinction-curse-bestiary-items/GBZBg2dACMgs5Uz2.htm)|Disappearance (At Will) (Self Only)|
|[gCcUGrKxC8wq622r.htm](extinction-curse-bestiary-items/gCcUGrKxC8wq622r.htm)|+3 Major Resilient Breastplate|
|[GDrdlzlbCnTetn1d.htm](extinction-curse-bestiary-items/GDrdlzlbCnTetn1d.htm)|Confusion (At Will)|
|[gfMG9GR7q2Nmo3c2.htm](extinction-curse-bestiary-items/gfMG9GR7q2Nmo3c2.htm)|Dagger|+1,striking|
|[gkZZB6VEmnazsd8w.htm](extinction-curse-bestiary-items/gkZZB6VEmnazsd8w.htm)|+2 Greater Resilient Studded Leather Armor|
|[gmbDVefxioirNHej.htm](extinction-curse-bestiary-items/gmbDVefxioirNHej.htm)|Feather Fall (At Will) (Self Only)|
|[gRXkTbTggZI3GBGZ.htm](extinction-curse-bestiary-items/gRXkTbTggZI3GBGZ.htm)|Longspear|+1|
|[hHMgZMry8AKd125Z.htm](extinction-curse-bestiary-items/hHMgZMry8AKd125Z.htm)|Elixir of Life (Greater) [Infused]|
|[HSdzmGpD9SX9gMti.htm](extinction-curse-bestiary-items/HSdzmGpD9SX9gMti.htm)|Charm (Animals Only)|
|[HTSkrC7WJF2LvhAY.htm](extinction-curse-bestiary-items/HTSkrC7WJF2LvhAY.htm)|Quicksilver Mutagen (Greater) (Infused)|
|[I0l5wzkOqX0rEsjo.htm](extinction-curse-bestiary-items/I0l5wzkOqX0rEsjo.htm)|Fear (At Will)|
|[idbSmKz24v98oQhN.htm](extinction-curse-bestiary-items/idbSmKz24v98oQhN.htm)|Sheriff Banyan's Map of the Sea Caves|
|[ifTXKPVR6xsgBX4o.htm](extinction-curse-bestiary-items/ifTXKPVR6xsgBX4o.htm)|Antidote (Major) [Infused]|
|[ix9mbv1z3onh9Lsi.htm](extinction-curse-bestiary-items/ix9mbv1z3onh9Lsi.htm)|Longsword|+2,greaterStriking|
|[iYbZ0U36eZpGPrvb.htm](extinction-curse-bestiary-items/iYbZ0U36eZpGPrvb.htm)|Rhoka Sword|+1,striking|
|[J9r3EGdiW0V79wgo.htm](extinction-curse-bestiary-items/J9r3EGdiW0V79wgo.htm)|Dimension Door (At Will)|
|[JAdbfBOcQMFxEEUU.htm](extinction-curse-bestiary-items/JAdbfBOcQMFxEEUU.htm)|Staff of the Magi|+3,majorStriking|
|[jG3Y1PfZ9WG6fVyv.htm](extinction-curse-bestiary-items/jG3Y1PfZ9WG6fVyv.htm)|Fly (Constant)|
|[jlvhbKSEAZkBHBIm.htm](extinction-curse-bestiary-items/jlvhbKSEAZkBHBIm.htm)|Longbow|+1,striking|
|[JNfFOPN58ILPiAjU.htm](extinction-curse-bestiary-items/JNfFOPN58ILPiAjU.htm)|Harm (At-Will)|
|[JOyY3xr5i8Bxot4t.htm](extinction-curse-bestiary-items/JOyY3xr5i8Bxot4t.htm)|Feather Fall (Self Only)|
|[JyWVziAy6MCtn3Nb.htm](extinction-curse-bestiary-items/JyWVziAy6MCtn3Nb.htm)|Formula Book|
|[k3I5F189xsmhpHsC.htm](extinction-curse-bestiary-items/k3I5F189xsmhpHsC.htm)|Composite Longbow|+1,striking|
|[K7VOxwpZ7Na4TBFK.htm](extinction-curse-bestiary-items/K7VOxwpZ7Na4TBFK.htm)|+1 Resilient Breastplate|
|[K8HQpzjGee43PFeB.htm](extinction-curse-bestiary-items/K8HQpzjGee43PFeB.htm)|Light Mace|+1|
|[kANFuSENY1afffH8.htm](extinction-curse-bestiary-items/kANFuSENY1afffH8.htm)|Spiked Gauntlet|+1,striking|
|[KDeAmqNZ8NGjjpMZ.htm](extinction-curse-bestiary-items/KDeAmqNZ8NGjjpMZ.htm)|Levitate (At Will)|
|[kdKmscxdhMu94gTS.htm](extinction-curse-bestiary-items/kdKmscxdhMu94gTS.htm)|Frost Vial (Greater) (Infused)|
|[KGXwqGgOZpDp0Ck3.htm](extinction-curse-bestiary-items/KGXwqGgOZpDp0Ck3.htm)|+1 Hide Armor|
|[KkjtCnHpVjDcnDIb.htm](extinction-curse-bestiary-items/KkjtCnHpVjDcnDIb.htm)|+2 Greater Resilient Full Plate|
|[kNNjjTC0b0ZPTnII.htm](extinction-curse-bestiary-items/kNNjjTC0b0ZPTnII.htm)|Speak with Animals (Constant)|
|[KOREkPHjaugDT5fn.htm](extinction-curse-bestiary-items/KOREkPHjaugDT5fn.htm)|Religious Symbol of Urgathoa (Silver)|
|[KQUnhrJmsbAJPZik.htm](extinction-curse-bestiary-items/KQUnhrJmsbAJPZik.htm)|Scroll of Maze (Level 8)|
|[Kqva1BHv5UXhAeIW.htm](extinction-curse-bestiary-items/Kqva1BHv5UXhAeIW.htm)|Telekinetic Maneuver (At Will)|
|[kV8gViNdEEYh26gD.htm](extinction-curse-bestiary-items/kV8gViNdEEYh26gD.htm)|+2 Greater Resilient Mithral Breastplate (Standard-Grade)|
|[KvKBQk0h1QCw5ZC8.htm](extinction-curse-bestiary-items/KvKBQk0h1QCw5ZC8.htm)|Telekinetic Haul (At Will)|
|[l3yA4WK8vdnQnJJb.htm](extinction-curse-bestiary-items/l3yA4WK8vdnQnJJb.htm)|+1 Studded Leather Armor|
|[lcfERNye3ZrCAlsu.htm](extinction-curse-bestiary-items/lcfERNye3ZrCAlsu.htm)|Shape Stone (At Will)|
|[lGFh9Y08etrUTHz5.htm](extinction-curse-bestiary-items/lGFh9Y08etrUTHz5.htm)|Fly (Constant)|
|[ljula66WZiHTsMFe.htm](extinction-curse-bestiary-items/ljula66WZiHTsMFe.htm)|See Invisibility (Constant)|
|[lLSRWP7Om7Huzd19.htm](extinction-curse-bestiary-items/lLSRWP7Om7Huzd19.htm)|Veil (Self Only)|
|[Ly6nitvAQ2kCd7K0.htm](extinction-curse-bestiary-items/Ly6nitvAQ2kCd7K0.htm)|Sickle|+1|
|[mAwxYKlNfq1Lgofj.htm](extinction-curse-bestiary-items/mAwxYKlNfq1Lgofj.htm)|Spear|+1,striking|
|[mH9WDo5EI4ZZJuTb.htm](extinction-curse-bestiary-items/mH9WDo5EI4ZZJuTb.htm)|Teleport (Self Only)|
|[MtbB1z53GXFLggFe.htm](extinction-curse-bestiary-items/MtbB1z53GXFLggFe.htm)|Alchemist's Fire (Moderate) (Infused)|
|[mWnwn5pBpT6VjGMB.htm](extinction-curse-bestiary-items/mWnwn5pBpT6VjGMB.htm)|Dimension Door (At Will)|
|[n86sZNE8O0jn1kkF.htm](extinction-curse-bestiary-items/n86sZNE8O0jn1kkF.htm)|Humanoid Form (At Will)|
|[nkY4eQPWGxaslKMa.htm](extinction-curse-bestiary-items/nkY4eQPWGxaslKMa.htm)|Augury (At Will)|
|[Nnkb3sPqsfGFOhtv.htm](extinction-curse-bestiary-items/Nnkb3sPqsfGFOhtv.htm)|Falchion|+1,striking|
|[NqAs4SvCETnYSYOK.htm](extinction-curse-bestiary-items/NqAs4SvCETnYSYOK.htm)|Bolts coated with shadow essence|
|[NSj8GP5Uyy1BW0JE.htm](extinction-curse-bestiary-items/NSj8GP5Uyy1BW0JE.htm)|Air Walk (Constant)|
|[nycgOFC3jC4CfnmG.htm](extinction-curse-bestiary-items/nycgOFC3jC4CfnmG.htm)|Feather Fall (At Will) (Self Only)|
|[nYZMYEzR32X1P92B.htm](extinction-curse-bestiary-items/nYZMYEzR32X1P92B.htm)|Composite Shortbow|+1,striking|
|[o7B3d9PhWctaj0P8.htm](extinction-curse-bestiary-items/o7B3d9PhWctaj0P8.htm)|Scimitar|+2,greaterStriking|
|[oFEYbMQiLvzwPAtG.htm](extinction-curse-bestiary-items/oFEYbMQiLvzwPAtG.htm)|Religious Symbol of Bokrug (Platinum)|
|[OjAuvyGPfAG9j4Dv.htm](extinction-curse-bestiary-items/OjAuvyGPfAG9j4Dv.htm)|Dimension Door (At Will)|
|[Oq8qCN1Ot7PQUhWE.htm](extinction-curse-bestiary-items/Oq8qCN1Ot7PQUhWE.htm)|Paralyze (At Will)|
|[orAEjgUO2zLIc2Di.htm](extinction-curse-bestiary-items/orAEjgUO2zLIc2Di.htm)|+1 Resilient Leather Armor|
|[oRBm7hBPk5gfvefV.htm](extinction-curse-bestiary-items/oRBm7hBPk5gfvefV.htm)|Enhance Victuals (At Will)|
|[OrumAw48Pwq9fDTZ.htm](extinction-curse-bestiary-items/OrumAw48Pwq9fDTZ.htm)|Ankylostar|+2,greaterStriking|
|[oSoePQdNadEUnATM.htm](extinction-curse-bestiary-items/oSoePQdNadEUnATM.htm)|Faerie Fire (At Will)|
|[P1prRlTlxSzsnhNi.htm](extinction-curse-bestiary-items/P1prRlTlxSzsnhNi.htm)|Stoneraiser Javelin|+2,striking,returning|
|[PP7sGT67f4IAwfaU.htm](extinction-curse-bestiary-items/PP7sGT67f4IAwfaU.htm)|Faerie Fire (At Will)|
|[PqvaZ3DvfcX3DqQD.htm](extinction-curse-bestiary-items/PqvaZ3DvfcX3DqQD.htm)|Blink (At Will)|
|[PVPqGw7nPCIvqbHf.htm](extinction-curse-bestiary-items/PVPqGw7nPCIvqbHf.htm)|Captivating Adoration (At Will) (Emotional Focus)|
|[pwOWBEPVPsrfwS5D.htm](extinction-curse-bestiary-items/pwOWBEPVPsrfwS5D.htm)|Telekinetic Maneuver (At Will)|
|[q2hMXszV7WfZt7lw.htm](extinction-curse-bestiary-items/q2hMXszV7WfZt7lw.htm)|Charm (At-Will)|
|[q2ksBBTuj9IItWjI.htm](extinction-curse-bestiary-items/q2ksBBTuj9IItWjI.htm)|Invisibility (At Will)|
|[qddyx06c0lhO2bqA.htm](extinction-curse-bestiary-items/qddyx06c0lhO2bqA.htm)|Enlarge (Self Only)|
|[QF1sbXL0ly14wIOr.htm](extinction-curse-bestiary-items/QF1sbXL0ly14wIOr.htm)|Invisibility (At-Will) (Self-Only)|
|[Qfpb2l5lZtzBzwYX.htm](extinction-curse-bestiary-items/Qfpb2l5lZtzBzwYX.htm)|Burning Hands (At Will)|
|[QKMTY0vHcMBDExoq.htm](extinction-curse-bestiary-items/QKMTY0vHcMBDExoq.htm)|Lion Tamer's Outfit|
|[qPFlP0EUcnxETgQw.htm](extinction-curse-bestiary-items/qPFlP0EUcnxETgQw.htm)|+2 Greater Resilient Elven Chain (Standard-Grade)|
|[QwnhlBEgmmLWoeSk.htm](extinction-curse-bestiary-items/QwnhlBEgmmLWoeSk.htm)|Bind Soul (At Will) (From Heartstone)|
|[QyNDhFzjNVZdcfcL.htm](extinction-curse-bestiary-items/QyNDhFzjNVZdcfcL.htm)|Handwraps of Mighty Blows|+3,majorStriking|
|[R1VM8NI2d9gcyPk0.htm](extinction-curse-bestiary-items/R1VM8NI2d9gcyPk0.htm)|Telekinetic Maneuver (At Will)|
|[RFVHeM61eFQI1Y9j.htm](extinction-curse-bestiary-items/RFVHeM61eFQI1Y9j.htm)|Halberd|+1,striking|
|[RjbNYYv9HvyrTFlA.htm](extinction-curse-bestiary-items/RjbNYYv9HvyrTFlA.htm)|Dream Message (At Will)|
|[ROfK1SKRZK5bHSNe.htm](extinction-curse-bestiary-items/ROfK1SKRZK5bHSNe.htm)|Religious Symbol of Gozreh (Wooden, Defaced with Demonic Runes)|
|[RSvyyBPox2VzEhkh.htm](extinction-curse-bestiary-items/RSvyyBPox2VzEhkh.htm)|Ray of Enfeeblement (At Will)|
|[rWtKnUa62qWW2Knk.htm](extinction-curse-bestiary-items/rWtKnUa62qWW2Knk.htm)|Pass Without Trace (Constant)|
|[S3xTVbBZjdKXi28O.htm](extinction-curse-bestiary-items/S3xTVbBZjdKXi28O.htm)|Heavy Crossbow|+2,striking|
|[SBbVvzgTVrVDQLcd.htm](extinction-curse-bestiary-items/SBbVvzgTVrVDQLcd.htm)|Religious Symbol of Bokrug|
|[siGtm9FgzQmusNTV.htm](extinction-curse-bestiary-items/siGtm9FgzQmusNTV.htm)|Rapier|+1|
|[sLxIE1kiKD1TAX9B.htm](extinction-curse-bestiary-items/sLxIE1kiKD1TAX9B.htm)|Telekinetic Maneuver (At Will)|
|[sn7vd9r7ywSeeqkm.htm](extinction-curse-bestiary-items/sn7vd9r7ywSeeqkm.htm)|Spirit Blast (From Heartstone)|
|[SnC7t8ZXBipTfwbi.htm](extinction-curse-bestiary-items/SnC7t8ZXBipTfwbi.htm)|War Flail|+2,greaterStriking,greaterFlaming|
|[sPi3ZAYG2vpg9HAv.htm](extinction-curse-bestiary-items/sPi3ZAYG2vpg9HAv.htm)|Naginata|+2,greaterStriking|
|[sPp6K8MLjVeA5Dok.htm](extinction-curse-bestiary-items/sPp6K8MLjVeA5Dok.htm)|Teleport (At Will, Self Only)|
|[t8wpGhneIB81fXLO.htm](extinction-curse-bestiary-items/t8wpGhneIB81fXLO.htm)|Magic Missile (At Will)|
|[t8XNlOoaUrFDisdv.htm](extinction-curse-bestiary-items/t8XNlOoaUrFDisdv.htm)|Freedom of Movement (Constant)|
|[TCAdoYNBMwayH1bw.htm](extinction-curse-bestiary-items/TCAdoYNBMwayH1bw.htm)|+1 Resilient Chain Shirt|
|[teoYBsXeF4ePoNS9.htm](extinction-curse-bestiary-items/teoYBsXeF4ePoNS9.htm)|Spiked Gauntlet|+1,striking|
|[tgKfIAeJLnZoj24L.htm](extinction-curse-bestiary-items/tgKfIAeJLnZoj24L.htm)|Signal Arrows|
|[TKCUudDUNrXv1Z8q.htm](extinction-curse-bestiary-items/TKCUudDUNrXv1Z8q.htm)|Acid Flask (Moderate) (Infused)|
|[tOUoA3MBZL0bbGQn.htm](extinction-curse-bestiary-items/tOUoA3MBZL0bbGQn.htm)|Delusional Pride (At Will) (Emotional Focus)|
|[trpqjYarZmAIE6JR.htm](extinction-curse-bestiary-items/trpqjYarZmAIE6JR.htm)|Detect Magic (Constant)|
|[tWCEPnK5ZMOw1rIS.htm](extinction-curse-bestiary-items/tWCEPnK5ZMOw1rIS.htm)|Spear|+2,striking|
|[TzHZRpvvWRrSuk59.htm](extinction-curse-bestiary-items/TzHZRpvvWRrSuk59.htm)|Darkness (At Will)|
|[u6kenk4mksE3KEYJ.htm](extinction-curse-bestiary-items/u6kenk4mksE3KEYJ.htm)|Religious Symbol (Wooden) of Zevgavizeb|
|[UBibmfZaH20tWub7.htm](extinction-curse-bestiary-items/UBibmfZaH20tWub7.htm)|Detect Alignment (Constant) (All Alignments Simultaneously)|
|[udZhqwVQP0tOUeZZ.htm](extinction-curse-bestiary-items/udZhqwVQP0tOUeZZ.htm)|True Seeing (Constant)|
|[UeNHmJELEMJ1Bs7N.htm](extinction-curse-bestiary-items/UeNHmJELEMJ1Bs7N.htm)|Scroll of Heroism (Level 9)|
|[UnpSG1NEamzBnZEZ.htm](extinction-curse-bestiary-items/UnpSG1NEamzBnZEZ.htm)|True Seeing (Constant)|
|[UOtcq8C7cvf4ZBlr.htm](extinction-curse-bestiary-items/UOtcq8C7cvf4ZBlr.htm)|Animal Vision (At Will) (Dinosaurs Only)|
|[UX2gj24gyBJCM4LU.htm](extinction-curse-bestiary-items/UX2gj24gyBJCM4LU.htm)|Fly (Constant)|
|[V1W3sYkzk0bs9ztJ.htm](extinction-curse-bestiary-items/V1W3sYkzk0bs9ztJ.htm)|Darkness (At Will)|
|[vbtSXntRG1Pu1oO6.htm](extinction-curse-bestiary-items/vbtSXntRG1Pu1oO6.htm)|Fly (Constant)|
|[vebtRV0UMG9x1p84.htm](extinction-curse-bestiary-items/vebtRV0UMG9x1p84.htm)|Divine Wrath (Lawful Only)|
|[VEujF17wRSyBWQXO.htm](extinction-curse-bestiary-items/VEujF17wRSyBWQXO.htm)|Speak with Animals (Constant)|
|[vFlEsdlagsFZzRgT.htm](extinction-curse-bestiary-items/vFlEsdlagsFZzRgT.htm)|Warhammer|+1,striking|
|[VKl3rUBu2nLA2szl.htm](extinction-curse-bestiary-items/VKl3rUBu2nLA2szl.htm)|Sylvan Wineskin|
|[vMw7KfTKAEzlocp3.htm](extinction-curse-bestiary-items/vMw7KfTKAEzlocp3.htm)|Mirror Image (At Will)|
|[VoNCwqZ2SF42txoX.htm](extinction-curse-bestiary-items/VoNCwqZ2SF42txoX.htm)|True Seeing (Constant)|
|[vOSiesS62Grm2PL7.htm](extinction-curse-bestiary-items/vOSiesS62Grm2PL7.htm)|Composite Longbow|+1,striking|
|[Vz8k5bYltczBVQhv.htm](extinction-curse-bestiary-items/Vz8k5bYltczBVQhv.htm)|+2 Resilient Leather Armor|
|[WdgZS5lTwdxmxcS7.htm](extinction-curse-bestiary-items/WdgZS5lTwdxmxcS7.htm)|Spiked Gauntlet|+3,majorStriking,unholy|
|[wg9Dbifr3nxpoe9D.htm](extinction-curse-bestiary-items/wg9Dbifr3nxpoe9D.htm)|True Seeing (Constant)|
|[wKHSYNpLLDbCM74h.htm](extinction-curse-bestiary-items/wKHSYNpLLDbCM74h.htm)|Religious Symbol (Silver) of Zevgavizeb|
|[wQkNuWQMiqVySwgb.htm](extinction-curse-bestiary-items/wQkNuWQMiqVySwgb.htm)|Ranseur|+2,greaterStriking|
|[wwBDrFSCf5XFZioZ.htm](extinction-curse-bestiary-items/wwBDrFSCf5XFZioZ.htm)|Divine Wrath (At Will)|
|[WXL6oZ1rorjYNmxa.htm](extinction-curse-bestiary-items/WXL6oZ1rorjYNmxa.htm)|Spiked Gauntlet|+1,striking|
|[XDQPyhPgdA1Luya1.htm](extinction-curse-bestiary-items/XDQPyhPgdA1Luya1.htm)|Telekinetic Haul (At Will)|
|[XiYeDNfpnbFWC8bp.htm](extinction-curse-bestiary-items/XiYeDNfpnbFWC8bp.htm)|Invisibility (Self Only)|
|[xNeFCRc6vVzyQDT0.htm](extinction-curse-bestiary-items/xNeFCRc6vVzyQDT0.htm)|Bokrug's Lashing Tail|+2,greaterStriking,keen|
|[xs380ivZqjl5WCwP.htm](extinction-curse-bestiary-items/xs380ivZqjl5WCwP.htm)|Charm (At Will)|
|[Ym5lHTSIM5818EUZ.htm](extinction-curse-bestiary-items/Ym5lHTSIM5818EUZ.htm)|+1 Striking Warhammer|+1,striking|
|[Yvw6yKyrv3WnvdAA.htm](extinction-curse-bestiary-items/Yvw6yKyrv3WnvdAA.htm)|Musical Instrument (Panpipes)|
|[YYHOVKuankVTjlpC.htm](extinction-curse-bestiary-items/YYHOVKuankVTjlpC.htm)|Dimension Door (At Will)|
|[z0kRnyPJ1GIDPXB0.htm](extinction-curse-bestiary-items/z0kRnyPJ1GIDPXB0.htm)|Detect Magic (Constant)|
|[zbFv2GQKidMFTpld.htm](extinction-curse-bestiary-items/zbFv2GQKidMFTpld.htm)|Bastard Sword|+1,striking|
|[ZCYqySTC44S2U1ig.htm](extinction-curse-bestiary-items/ZCYqySTC44S2U1ig.htm)|Composite Shortbow|+1,striking|
|[ZiFwoApOZ8V32uzv.htm](extinction-curse-bestiary-items/ZiFwoApOZ8V32uzv.htm)|Gray Robes|
|[zltQfojhYPzk7OpP.htm](extinction-curse-bestiary-items/zltQfojhYPzk7OpP.htm)|Feather Fall (Self Only)|
|[zrv4X9CqDhIpkMkJ.htm](extinction-curse-bestiary-items/zrv4X9CqDhIpkMkJ.htm)|Shadow Blast (From Heartstone)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[03fiR3RK6Jw9Wv9p.htm](extinction-curse-bestiary-items/03fiR3RK6Jw9Wv9p.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[05Ch36nkyu7Nm0e7.htm](extinction-curse-bestiary-items/05Ch36nkyu7Nm0e7.htm)|Spiked Gauntlet|Spiked Gauntlet|modificada|
|[0ADnKFiKMszSKZaF.htm](extinction-curse-bestiary-items/0ADnKFiKMszSKZaF.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[0CIDFYX7xU5OaPcw.htm](extinction-curse-bestiary-items/0CIDFYX7xU5OaPcw.htm)|Convergent Tactics|Tácticas Convergentes|modificada|
|[0CogrGS0ASUBnsef.htm](extinction-curse-bestiary-items/0CogrGS0ASUBnsef.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[0eCHhLgu7JC3do8q.htm](extinction-curse-bestiary-items/0eCHhLgu7JC3do8q.htm)|Abandon Body|Abandonar Cuerpo|modificada|
|[0hTWvwnoEFyZWBzR.htm](extinction-curse-bestiary-items/0hTWvwnoEFyZWBzR.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[0inCgv8zE1zM339E.htm](extinction-curse-bestiary-items/0inCgv8zE1zM339E.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[0MA7GYvcLU7sgYtn.htm](extinction-curse-bestiary-items/0MA7GYvcLU7sgYtn.htm)|Whip|Látigo|modificada|
|[0OUF0y5EmFm2s0oD.htm](extinction-curse-bestiary-items/0OUF0y5EmFm2s0oD.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[0oyBktgze70q4n6D.htm](extinction-curse-bestiary-items/0oyBktgze70q4n6D.htm)|Raking Claws|Garras Rastrilladoras|modificada|
|[0PmWWWaGPmE3r4bq.htm](extinction-curse-bestiary-items/0PmWWWaGPmE3r4bq.htm)|Longbow|Arco largo|modificada|
|[0QAulbffblLQuA21.htm](extinction-curse-bestiary-items/0QAulbffblLQuA21.htm)|Constant Spells|Constant Spells|modificada|
|[0UbSYN1Y6Sk2mfjQ.htm](extinction-curse-bestiary-items/0UbSYN1Y6Sk2mfjQ.htm)|Beak Crunch|Mascar de Pico|modificada|
|[1329nZKSKvV6X654.htm](extinction-curse-bestiary-items/1329nZKSKvV6X654.htm)|Consume Knowledge|Consume Conocimiento|modificada|
|[13WOSIjbbyXFLN47.htm](extinction-curse-bestiary-items/13WOSIjbbyXFLN47.htm)|Uncanny Bombs|Bombas asombrosas|modificada|
|[14NkcqddU66Ux2ro.htm](extinction-curse-bestiary-items/14NkcqddU66Ux2ro.htm)|Jaws|Fauces|modificada|
|[15cOjThlAU6xLAzc.htm](extinction-curse-bestiary-items/15cOjThlAU6xLAzc.htm)|Convergent Calm|Calma convergente|modificada|
|[16HnvP6u858vpixj.htm](extinction-curse-bestiary-items/16HnvP6u858vpixj.htm)|Tentacle|Tentáculo|modificada|
|[1Aoz2hUdk0inH018.htm](extinction-curse-bestiary-items/1Aoz2hUdk0inH018.htm)|Familiar|Familiar|modificada|
|[1BwIo7mgXKcuS2Vg.htm](extinction-curse-bestiary-items/1BwIo7mgXKcuS2Vg.htm)|Weakening Strike|Golpe debilitador|modificada|
|[1cplu9lvCddcGPOx.htm](extinction-curse-bestiary-items/1cplu9lvCddcGPOx.htm)|Demonic Condemnation|Condena demoníaca|modificada|
|[1d1wn0NQpxxTHN1s.htm](extinction-curse-bestiary-items/1d1wn0NQpxxTHN1s.htm)|Incredible Reload|Recarga sensacional|modificada|
|[1fJFJOb1u8PBT1qs.htm](extinction-curse-bestiary-items/1fJFJOb1u8PBT1qs.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[1FQhQGiVuDUtLiNn.htm](extinction-curse-bestiary-items/1FQhQGiVuDUtLiNn.htm)|Javelin|Javelin|modificada|
|[1GNw5dkroNexAH5F.htm](extinction-curse-bestiary-items/1GNw5dkroNexAH5F.htm)|Nimble Grazer|Nimble Grazer|modificada|
|[1K62WMcAnmRMi30L.htm](extinction-curse-bestiary-items/1K62WMcAnmRMi30L.htm)|Hammer Mastery|Dominio del martillo|modificada|
|[1LKBXuy7BK2mG9vh.htm](extinction-curse-bestiary-items/1LKBXuy7BK2mG9vh.htm)|Negative Healing|Curación negativa|modificada|
|[1PpMA6kgFuFqrS88.htm](extinction-curse-bestiary-items/1PpMA6kgFuFqrS88.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[1Rnw0tm3ktKN04Sv.htm](extinction-curse-bestiary-items/1Rnw0tm3ktKN04Sv.htm)|Primordial Balance|Mantener el equilibrio primordial|modificada|
|[1sDrhVs1HdgK5SIw.htm](extinction-curse-bestiary-items/1sDrhVs1HdgK5SIw.htm)|Jaws|Fauces|modificada|
|[1SlSXQvnIyNY6mQu.htm](extinction-curse-bestiary-items/1SlSXQvnIyNY6mQu.htm)|Grab|Agarrado|modificada|
|[1uReN3TQSYYikfWz.htm](extinction-curse-bestiary-items/1uReN3TQSYYikfWz.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[1W3SqzQpvwlpRf5W.htm](extinction-curse-bestiary-items/1W3SqzQpvwlpRf5W.htm)|Double Shot|Disparo doble|modificada|
|[1x1JjlN10VDIIVb3.htm](extinction-curse-bestiary-items/1x1JjlN10VDIIVb3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1xcDVCY3sx0uDDys.htm](extinction-curse-bestiary-items/1xcDVCY3sx0uDDys.htm)|Greater Constrict|Mayor Restricción|modificada|
|[20pfR59zCrylpO9C.htm](extinction-curse-bestiary-items/20pfR59zCrylpO9C.htm)|Mobility|Movilidad|modificada|
|[22saQWACDGfXp2ry.htm](extinction-curse-bestiary-items/22saQWACDGfXp2ry.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[2AI929eLb9JZQxCU.htm](extinction-curse-bestiary-items/2AI929eLb9JZQxCU.htm)|Swift Sneak|Movimiento furtivo rápido|modificada|
|[2aiIUhx9Z3yMKGyS.htm](extinction-curse-bestiary-items/2aiIUhx9Z3yMKGyS.htm)|Rage|Furia|modificada|
|[2arHY3THmPR4sHUA.htm](extinction-curse-bestiary-items/2arHY3THmPR4sHUA.htm)|Talons|Talons|modificada|
|[2bDfYtubnMMlCagj.htm](extinction-curse-bestiary-items/2bDfYtubnMMlCagj.htm)|Turn to Mist|Turn to Mist|modificada|
|[2CJo5tauCDhGyp1O.htm](extinction-curse-bestiary-items/2CJo5tauCDhGyp1O.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[2CTbAYtWJgwlBDPL.htm](extinction-curse-bestiary-items/2CTbAYtWJgwlBDPL.htm)|Grab|Agarrado|modificada|
|[2dOTpPLMKccRRnjc.htm](extinction-curse-bestiary-items/2dOTpPLMKccRRnjc.htm)|Devourer's Dictum|Devourer's Dictum|modificada|
|[2dsQhpuTqRGQXQLq.htm](extinction-curse-bestiary-items/2dsQhpuTqRGQXQLq.htm)|Quick Draw|Desenvainado rápido|modificada|
|[2f1qGs2WLtiERtrf.htm](extinction-curse-bestiary-items/2f1qGs2WLtiERtrf.htm)|Shadow Essence|Esencia de sombra|modificada|
|[2GPHZdvZjNrQX4S7.htm](extinction-curse-bestiary-items/2GPHZdvZjNrQX4S7.htm)|Vanished Alignment|Vanished Alignment|modificada|
|[2GPOucqeB9jZndUB.htm](extinction-curse-bestiary-items/2GPOucqeB9jZndUB.htm)|Jaws|Fauces|modificada|
|[2HXsP310zFFnwcpi.htm](extinction-curse-bestiary-items/2HXsP310zFFnwcpi.htm)|Claw|Garra|modificada|
|[2LY21nrCeQ74PTkR.htm](extinction-curse-bestiary-items/2LY21nrCeQ74PTkR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[2mOCKRrDP2pm8VSK.htm](extinction-curse-bestiary-items/2mOCKRrDP2pm8VSK.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[2oeiSTDdWeqdi1Ew.htm](extinction-curse-bestiary-items/2oeiSTDdWeqdi1Ew.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[2oftzl4JLLJIvRRz.htm](extinction-curse-bestiary-items/2oftzl4JLLJIvRRz.htm)|Elaborate Feint|Elaborar Fintar|modificada|
|[2Om4RqsiR8arN4hn.htm](extinction-curse-bestiary-items/2Om4RqsiR8arN4hn.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[2sSVjsfJJNodprfg.htm](extinction-curse-bestiary-items/2sSVjsfJJNodprfg.htm)|Change of Luck|Cambio de Suerte|modificada|
|[2tVmuiOOVxmIhIT4.htm](extinction-curse-bestiary-items/2tVmuiOOVxmIhIT4.htm)|Furious Swing|Furious Swing|modificada|
|[2UrfKw5PocQe3AE6.htm](extinction-curse-bestiary-items/2UrfKw5PocQe3AE6.htm)|Constant Spells|Constant Spells|modificada|
|[2WRsdtZcLSWodZG3.htm](extinction-curse-bestiary-items/2WRsdtZcLSWodZG3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[2yAxIhu0gToaXv6n.htm](extinction-curse-bestiary-items/2yAxIhu0gToaXv6n.htm)|Evasion|Evasión|modificada|
|[2zQIifa5JMldE13S.htm](extinction-curse-bestiary-items/2zQIifa5JMldE13S.htm)|+26 when using Perception for Initiative|+26 al usar Percepción para Iniciativa|modificada|
|[30u18PLPEhoWfUTp.htm](extinction-curse-bestiary-items/30u18PLPEhoWfUTp.htm)|Claw|Garra|modificada|
|[33EELhSSsT1qGqIx.htm](extinction-curse-bestiary-items/33EELhSSsT1qGqIx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[33Vq76XNGvlE9MHH.htm](extinction-curse-bestiary-items/33Vq76XNGvlE9MHH.htm)|Powerful Arm|Powerful Arm|modificada|
|[34SnogKLGJ3qYmNi.htm](extinction-curse-bestiary-items/34SnogKLGJ3qYmNi.htm)|Echoes of Aeons|Ecos de los Eones|modificada|
|[38UzCSLYydGU7NfZ.htm](extinction-curse-bestiary-items/38UzCSLYydGU7NfZ.htm)|Recoil from Wasted Opportunities|Retroceso de Oportunidades Desperdiciadas|modificada|
|[39iXJ3VDAsZ4Ij0P.htm](extinction-curse-bestiary-items/39iXJ3VDAsZ4Ij0P.htm)|Utter Despair|Utter Despair|modificada|
|[3AW2a8B425pkI8WL.htm](extinction-curse-bestiary-items/3AW2a8B425pkI8WL.htm)|Flea Fever|Fiebre de Pulgas|modificada|
|[3cmVTpBQzoejUsbQ.htm](extinction-curse-bestiary-items/3cmVTpBQzoejUsbQ.htm)|Rock|Roca|modificada|
|[3eMwI2ZGla9CVsIz.htm](extinction-curse-bestiary-items/3eMwI2ZGla9CVsIz.htm)|Constant Spells|Constant Spells|modificada|
|[3FngXmc5sXcStz5D.htm](extinction-curse-bestiary-items/3FngXmc5sXcStz5D.htm)|+1 Status Bonus to Saves vs. Magical Effects|Bonificador de situación +1 a las salvaciones contra efectos mágicos.|modificada|
|[3fuKucSTNEuRZh4b.htm](extinction-curse-bestiary-items/3fuKucSTNEuRZh4b.htm)|Blood Squirt|Blood Squirt|modificada|
|[3gJM7nNe6xASowZz.htm](extinction-curse-bestiary-items/3gJM7nNe6xASowZz.htm)|Swallow Whole|Engullir Todo|modificada|
|[3h4TV4uROy8yVnxx.htm](extinction-curse-bestiary-items/3h4TV4uROy8yVnxx.htm)|Constant Spells|Constant Spells|modificada|
|[3IYBBjbLG10Rsmme.htm](extinction-curse-bestiary-items/3IYBBjbLG10Rsmme.htm)|Terrifying Sneer|Desprecio Aterrador|modificada|
|[3khAQSOvCGGFmARp.htm](extinction-curse-bestiary-items/3khAQSOvCGGFmARp.htm)|Blightburn Radiation|Blightburn Radiation|modificada|
|[3kILoZ5MVAOnGohC.htm](extinction-curse-bestiary-items/3kILoZ5MVAOnGohC.htm)|Scimitar|Cimitarra|modificada|
|[3LzPeJJxCoxD2Roq.htm](extinction-curse-bestiary-items/3LzPeJJxCoxD2Roq.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[3O0SbByZ5mishfy0.htm](extinction-curse-bestiary-items/3O0SbByZ5mishfy0.htm)|Quick Bomber|Bombardero rápido|modificada|
|[3pL0yHt4nvMJI7eK.htm](extinction-curse-bestiary-items/3pL0yHt4nvMJI7eK.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[3QVgbicaKeJdgy6l.htm](extinction-curse-bestiary-items/3QVgbicaKeJdgy6l.htm)|Tremorsense (imprecise) 30 feet|Sentido del Temblor (impreciso) 30 pies|modificada|
|[3RchvgPIdNc2KUu9.htm](extinction-curse-bestiary-items/3RchvgPIdNc2KUu9.htm)|Fist|Puño|modificada|
|[3sXjjaMPyL0UFfVY.htm](extinction-curse-bestiary-items/3sXjjaMPyL0UFfVY.htm)|Expeditious Evolution|Evolución Expeditiva|modificada|
|[3VYFcfVRP5S7MswK.htm](extinction-curse-bestiary-items/3VYFcfVRP5S7MswK.htm)|Jaws|Fauces|modificada|
|[3zIULjjGCvaxoCpI.htm](extinction-curse-bestiary-items/3zIULjjGCvaxoCpI.htm)|Staff|Báculo|modificada|
|[415YyiRgxhBj4QVH.htm](extinction-curse-bestiary-items/415YyiRgxhBj4QVH.htm)|Sulfurous Plume|Sulfurous Plume|modificada|
|[41uD7oGTc3XEcgP8.htm](extinction-curse-bestiary-items/41uD7oGTc3XEcgP8.htm)|Greater Frost Rapier|Estoque Gélida, mayor|modificada|
|[46lzlqxwhzjHJYhD.htm](extinction-curse-bestiary-items/46lzlqxwhzjHJYhD.htm)|Disrupted Link|Enlace Disruptora|modificada|
|[49kZdXcigtGfmc19.htm](extinction-curse-bestiary-items/49kZdXcigtGfmc19.htm)|Constant Spells|Constant Spells|modificada|
|[4BgQ2Qd9NVWa3Hw1.htm](extinction-curse-bestiary-items/4BgQ2Qd9NVWa3Hw1.htm)|Gnashing Bite|Muerdemuerde|modificada|
|[4hmSKla48vJAAJG6.htm](extinction-curse-bestiary-items/4hmSKla48vJAAJG6.htm)|Tainted Guts|Tainted Guts|modificada|
|[4hQQHFYYXkIr2wtS.htm](extinction-curse-bestiary-items/4hQQHFYYXkIr2wtS.htm)|Pseudopod|Pseudópodo|modificada|
|[4mA8PLTN8ZFWmcr6.htm](extinction-curse-bestiary-items/4mA8PLTN8ZFWmcr6.htm)|Furious Claws|Garras furiosas|modificada|
|[4ooKJCagcx7gbeiK.htm](extinction-curse-bestiary-items/4ooKJCagcx7gbeiK.htm)|Compact|Compacto|modificada|
|[4qK7Ch0rYRLpn8rm.htm](extinction-curse-bestiary-items/4qK7Ch0rYRLpn8rm.htm)|All-Around Vision|All-Around Vision|modificada|
|[4QuTQ1CVNw72E020.htm](extinction-curse-bestiary-items/4QuTQ1CVNw72E020.htm)|Jaws|Fauces|modificada|
|[4v1QwNqequfwzIu9.htm](extinction-curse-bestiary-items/4v1QwNqequfwzIu9.htm)|Unholy Spiked Gauntlet|Profana Spiked Gauntlet|modificada|
|[4xdwD1vWSCPYxjVQ.htm](extinction-curse-bestiary-items/4xdwD1vWSCPYxjVQ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[53xmmigXl7qYER3Y.htm](extinction-curse-bestiary-items/53xmmigXl7qYER3Y.htm)|Infuse Weapons|Infundir armas|modificada|
|[55Ue9KoElZXuS8Dc.htm](extinction-curse-bestiary-items/55Ue9KoElZXuS8Dc.htm)|Quick Bomber|Bombardero rápido|modificada|
|[57yZt0sTPCTIwvyc.htm](extinction-curse-bestiary-items/57yZt0sTPCTIwvyc.htm)|Claw|Garra|modificada|
|[5AIvyudQYSy8JIW9.htm](extinction-curse-bestiary-items/5AIvyudQYSy8JIW9.htm)|Claw|Garra|modificada|
|[5ARBJjRyKKr0s71H.htm](extinction-curse-bestiary-items/5ARBJjRyKKr0s71H.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[5dzqMDEkCOAmRjCF.htm](extinction-curse-bestiary-items/5dzqMDEkCOAmRjCF.htm)|Pack Attack|Ataque en manada|modificada|
|[5gMfuCDBI4h3IyAu.htm](extinction-curse-bestiary-items/5gMfuCDBI4h3IyAu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5h3L7HCWGZnNodvy.htm](extinction-curse-bestiary-items/5h3L7HCWGZnNodvy.htm)|Bounce|Bounce|modificada|
|[5hIqoXM1Pti9OonO.htm](extinction-curse-bestiary-items/5hIqoXM1Pti9OonO.htm)|Ill Omen|Mal agüero|modificada|
|[5IFayxoGlhzAgcEL.htm](extinction-curse-bestiary-items/5IFayxoGlhzAgcEL.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[5JNkXeCqgTEkUhoN.htm](extinction-curse-bestiary-items/5JNkXeCqgTEkUhoN.htm)|Deny Advantage|Denegar ventaja|modificada|
|[5OiYfhHPstcj412O.htm](extinction-curse-bestiary-items/5OiYfhHPstcj412O.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[5pgk77sKtK8gCgSb.htm](extinction-curse-bestiary-items/5pgk77sKtK8gCgSb.htm)|Jaws|Fauces|modificada|
|[5pSRrtmYXSwMkm8e.htm](extinction-curse-bestiary-items/5pSRrtmYXSwMkm8e.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5pTxW5pvTdGLWj2C.htm](extinction-curse-bestiary-items/5pTxW5pvTdGLWj2C.htm)|Longspear|Longspear|modificada|
|[5QcLOLT0zFzjb2Fg.htm](extinction-curse-bestiary-items/5QcLOLT0zFzjb2Fg.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[5ShmRpRiadEdwNuC.htm](extinction-curse-bestiary-items/5ShmRpRiadEdwNuC.htm)|+2 Status to All Saves vs. Magical Effects|+2 situación a todas las salvaciones contra efectos mágicos.|modificada|
|[5TKcGIGIKGyjkpnm.htm](extinction-curse-bestiary-items/5TKcGIGIKGyjkpnm.htm)|Drink Blood|Beber Sangre|modificada|
|[5Uoof6HFaymWjuTE.htm](extinction-curse-bestiary-items/5Uoof6HFaymWjuTE.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[5utWutqAL3Pfpee3.htm](extinction-curse-bestiary-items/5utWutqAL3Pfpee3.htm)|Stunning Blow|Golpe Impresionante|modificada|
|[5VgxQkr436I8Ty46.htm](extinction-curse-bestiary-items/5VgxQkr436I8Ty46.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[5WzAbTjNTM4YElhM.htm](extinction-curse-bestiary-items/5WzAbTjNTM4YElhM.htm)|Channel Smite|Canalizar castigo|modificada|
|[5XALtpIuUMlUiRTS.htm](extinction-curse-bestiary-items/5XALtpIuUMlUiRTS.htm)|Jaws|Fauces|modificada|
|[5yJ7XmHWutKmWc2L.htm](extinction-curse-bestiary-items/5yJ7XmHWutKmWc2L.htm)|Swallow Whole|Engullir Todo|modificada|
|[612zPFqgcul7ohBP.htm](extinction-curse-bestiary-items/612zPFqgcul7ohBP.htm)|Scalescribe|Scalescribe|modificada|
|[62XRuG8BxfIDEFOw.htm](extinction-curse-bestiary-items/62XRuG8BxfIDEFOw.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[65H1lnX2GYJaLpHs.htm](extinction-curse-bestiary-items/65H1lnX2GYJaLpHs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[68ILx13H6RgkuEJO.htm](extinction-curse-bestiary-items/68ILx13H6RgkuEJO.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[69tw4cowol64x682.htm](extinction-curse-bestiary-items/69tw4cowol64x682.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6aID4H750Zqy0EOH.htm](extinction-curse-bestiary-items/6aID4H750Zqy0EOH.htm)|Cold Iron Silver Longsword|Cold Iron Silver Longsword|modificada|
|[6coOtGICrhXGQ5MW.htm](extinction-curse-bestiary-items/6coOtGICrhXGQ5MW.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[6CRzO3g1vhWEqvny.htm](extinction-curse-bestiary-items/6CRzO3g1vhWEqvny.htm)|Sickle|Hoz|modificada|
|[6CvKZvykGC9DRN7Z.htm](extinction-curse-bestiary-items/6CvKZvykGC9DRN7Z.htm)|Deny Advantage|Denegar ventaja|modificada|
|[6DipIrHqdCiJN01W.htm](extinction-curse-bestiary-items/6DipIrHqdCiJN01W.htm)|Dagger|Daga|modificada|
|[6fCsQA7Mbw0rYh0L.htm](extinction-curse-bestiary-items/6fCsQA7Mbw0rYh0L.htm)|Tanglefoot Bag (Greater) (Infused)|Bolsa de mara, mayor (infundida).|modificada|
|[6Iz60sgKIQyvGdXg.htm](extinction-curse-bestiary-items/6Iz60sgKIQyvGdXg.htm)|Cowardly|Cobarde|modificada|
|[6JEzupbVB1uGFoq4.htm](extinction-curse-bestiary-items/6JEzupbVB1uGFoq4.htm)|Disrupted Link|Enlace Disruptora|modificada|
|[6jthkAwvS3DHh7jF.htm](extinction-curse-bestiary-items/6jthkAwvS3DHh7jF.htm)|Summon Monster|Invocar Monstruo|modificada|
|[6lzGDph9WZ9msyJq.htm](extinction-curse-bestiary-items/6lzGDph9WZ9msyJq.htm)|Arrow Vulnerability|Vulnerabilidad de Flecha|modificada|
|[6nV4lSGXCgZTjigM.htm](extinction-curse-bestiary-items/6nV4lSGXCgZTjigM.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6pnFCJxQ1BPSdZfN.htm](extinction-curse-bestiary-items/6pnFCJxQ1BPSdZfN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6SbGSgbpCYxPYfsS.htm](extinction-curse-bestiary-items/6SbGSgbpCYxPYfsS.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[6swZIa9vyXdVDpwQ.htm](extinction-curse-bestiary-items/6swZIa9vyXdVDpwQ.htm)|Obsidian Sliver|Obsidian Sliver|modificada|
|[6T41uBvYSdsd3etb.htm](extinction-curse-bestiary-items/6T41uBvYSdsd3etb.htm)|Congealed|Congealed|modificada|
|[6Ue9mTJcWViQYqOR.htm](extinction-curse-bestiary-items/6Ue9mTJcWViQYqOR.htm)|Jaws|Fauces|modificada|
|[6ufwCMehbagfms2J.htm](extinction-curse-bestiary-items/6ufwCMehbagfms2J.htm)|Set on Fire|Set on Fire|modificada|
|[6uHq8MgU7xzJ6vGW.htm](extinction-curse-bestiary-items/6uHq8MgU7xzJ6vGW.htm)|Sprinkle Pixie Dust|Sprinkle Pixie Dust|modificada|
|[6Ww4Qkldv49LbeZh.htm](extinction-curse-bestiary-items/6Ww4Qkldv49LbeZh.htm)|Crystalline Construction|Construcción Cristalina|modificada|
|[6x5ndW51ukQRdEkd.htm](extinction-curse-bestiary-items/6x5ndW51ukQRdEkd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6ZhIbuBKgvn1KAfr.htm](extinction-curse-bestiary-items/6ZhIbuBKgvn1KAfr.htm)|Constrict|Restringir|modificada|
|[70GgLhQiyd0TPrVN.htm](extinction-curse-bestiary-items/70GgLhQiyd0TPrVN.htm)|Hoof|Hoof|modificada|
|[74wDKknM5LCOcK73.htm](extinction-curse-bestiary-items/74wDKknM5LCOcK73.htm)|Hears Heartbeats|Escucha latidos|modificada|
|[75B144VmeAAs88N3.htm](extinction-curse-bestiary-items/75B144VmeAAs88N3.htm)|Grab|Agarrado|modificada|
|[7601xuNSze8YXgHA.htm](extinction-curse-bestiary-items/7601xuNSze8YXgHA.htm)|Shortsword|Espada corta|modificada|
|[76GZfG5yXvb8EAyU.htm](extinction-curse-bestiary-items/76GZfG5yXvb8EAyU.htm)|Jaws|Fauces|modificada|
|[7b3GNADyuxWoQu4e.htm](extinction-curse-bestiary-items/7b3GNADyuxWoQu4e.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[7C6yRhAx3CPR1NFc.htm](extinction-curse-bestiary-items/7C6yRhAx3CPR1NFc.htm)|Whip Vulnerability|Látigo Vulnerabilidad|modificada|
|[7D7K3q9IS7T2K11z.htm](extinction-curse-bestiary-items/7D7K3q9IS7T2K11z.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[7HIDyV0Dl364Jch8.htm](extinction-curse-bestiary-items/7HIDyV0Dl364Jch8.htm)|Foot|Pie|modificada|
|[7HpYZ4qlfqXrnAeX.htm](extinction-curse-bestiary-items/7HpYZ4qlfqXrnAeX.htm)|Bastard Sword|Espada Bastarda|modificada|
|[7I2FYqAcQ3SORicy.htm](extinction-curse-bestiary-items/7I2FYqAcQ3SORicy.htm)|Grab|Agarrado|modificada|
|[7jb5XLgHQFBJzvra.htm](extinction-curse-bestiary-items/7jb5XLgHQFBJzvra.htm)|Constant Spells|Constant Spells|modificada|
|[7jGgiwswo0feOhqx.htm](extinction-curse-bestiary-items/7jGgiwswo0feOhqx.htm)|Claw|Garra|modificada|
|[7JQkP6acnn4UB5Dt.htm](extinction-curse-bestiary-items/7JQkP6acnn4UB5Dt.htm)|Malevolent Possession|Posesión maléfica.|modificada|
|[7rfez4zvg21jamKH.htm](extinction-curse-bestiary-items/7rfez4zvg21jamKH.htm)|Ankylostar|Ankylostar|modificada|
|[7wtKLLAt30PdWUEX.htm](extinction-curse-bestiary-items/7wtKLLAt30PdWUEX.htm)|Ghostly Bite|Muerdemuerde|modificada|
|[7XUkm64tCN25G6VB.htm](extinction-curse-bestiary-items/7XUkm64tCN25G6VB.htm)|Candle Fingers|Dedos de Vela|modificada|
|[7XZ5d79YD3f0pWUk.htm](extinction-curse-bestiary-items/7XZ5d79YD3f0pWUk.htm)|Reactive Slime|Baba Reactiva|modificada|
|[7yOLigTAGYofVRkH.htm](extinction-curse-bestiary-items/7yOLigTAGYofVRkH.htm)|Channel Rot|Canalizar putridez|modificada|
|[7YxbvFT8BdjHaBTM.htm](extinction-curse-bestiary-items/7YxbvFT8BdjHaBTM.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[7YxHt2qByZ37NDj5.htm](extinction-curse-bestiary-items/7YxHt2qByZ37NDj5.htm)|Unfathomable Aspect|Aspecto Insondable|modificada|
|[7Z7WDMHFDrNJcL7D.htm](extinction-curse-bestiary-items/7Z7WDMHFDrNJcL7D.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7zPJ2oI0YtpNMQiH.htm](extinction-curse-bestiary-items/7zPJ2oI0YtpNMQiH.htm)|Thorny Branch|Thorny Branch|modificada|
|[85ZFVNdum6WrNAnT.htm](extinction-curse-bestiary-items/85ZFVNdum6WrNAnT.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[860MOhDdvHc8KanP.htm](extinction-curse-bestiary-items/860MOhDdvHc8KanP.htm)|Hand|Mano|modificada|
|[88vgxSTx2c1kRrzY.htm](extinction-curse-bestiary-items/88vgxSTx2c1kRrzY.htm)|Claw|Garra|modificada|
|[89iNsonBI863Pvyb.htm](extinction-curse-bestiary-items/89iNsonBI863Pvyb.htm)|Sack of Spiders|Saco de Arañas|modificada|
|[8aMnXRh8CULYzKV8.htm](extinction-curse-bestiary-items/8aMnXRh8CULYzKV8.htm)|Ignite Baton|Ignite Baton|modificada|
|[8arXWMYazzSvR5im.htm](extinction-curse-bestiary-items/8arXWMYazzSvR5im.htm)|Constant Spells|Constant Spells|modificada|
|[8JSEG6z9Sf6PnMGY.htm](extinction-curse-bestiary-items/8JSEG6z9Sf6PnMGY.htm)|Frightful Presence|Frightful Presence|modificada|
|[8kQ7IgMQlvRAyicP.htm](extinction-curse-bestiary-items/8kQ7IgMQlvRAyicP.htm)|Earthen Torrent|Torrente de Tierra|modificada|
|[8LWkgJLzQH5BuUe3.htm](extinction-curse-bestiary-items/8LWkgJLzQH5BuUe3.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[8NbpXctXpdSLejVt.htm](extinction-curse-bestiary-items/8NbpXctXpdSLejVt.htm)|Power Attack|Ataque poderoso|modificada|
|[8oiunFF5LcYpynHM.htm](extinction-curse-bestiary-items/8oiunFF5LcYpynHM.htm)|Grab|Agarrado|modificada|
|[8pEUtQIaEuhNMeNq.htm](extinction-curse-bestiary-items/8pEUtQIaEuhNMeNq.htm)|Falling Stone Unarmed Attack|Falling Stone Ataque sin armas|modificada|
|[8rZVMVE5e5IobbkP.htm](extinction-curse-bestiary-items/8rZVMVE5e5IobbkP.htm)|Javelin|Javelin|modificada|
|[8T1lphW5agh8iPDk.htm](extinction-curse-bestiary-items/8T1lphW5agh8iPDk.htm)|Sudden Charge|Carga súbita|modificada|
|[8UWmcpWRaJbBvrgi.htm](extinction-curse-bestiary-items/8UWmcpWRaJbBvrgi.htm)|Worry Prey|Presa Preocupante|modificada|
|[8vEzM2MigepekMyc.htm](extinction-curse-bestiary-items/8vEzM2MigepekMyc.htm)|Alchemist's Fire (Moderate)|Fuego de Alquimista (Moderada)|modificada|
|[8z8uIcobom68tmyS.htm](extinction-curse-bestiary-items/8z8uIcobom68tmyS.htm)|Deepest Fear|Miedo Profundo|modificada|
|[92xnULMmOwNxGCpN.htm](extinction-curse-bestiary-items/92xnULMmOwNxGCpN.htm)|Death Stench|Hedor de Muerte|modificada|
|[938PC6CKpMMlv4b7.htm](extinction-curse-bestiary-items/938PC6CKpMMlv4b7.htm)|Earthen Blow|Golpe de Tierra|modificada|
|[9ClztbAmAcGhe6zm.htm](extinction-curse-bestiary-items/9ClztbAmAcGhe6zm.htm)|Constant Spells|Constant Spells|modificada|
|[9GD9cU7HYaHGStvx.htm](extinction-curse-bestiary-items/9GD9cU7HYaHGStvx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9gQhAfAUHKZwPfXQ.htm](extinction-curse-bestiary-items/9gQhAfAUHKZwPfXQ.htm)|Jaws|Fauces|modificada|
|[9JJx0yI602CpXiOj.htm](extinction-curse-bestiary-items/9JJx0yI602CpXiOj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9KjQBibdPnAX1HTL.htm](extinction-curse-bestiary-items/9KjQBibdPnAX1HTL.htm)|Stone Throes|Garganta de Piedra|modificada|
|[9LeI99p7iFnhng1g.htm](extinction-curse-bestiary-items/9LeI99p7iFnhng1g.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[9MkG2PZhLavYiFIf.htm](extinction-curse-bestiary-items/9MkG2PZhLavYiFIf.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[9mMqegSFLDuJ14KD.htm](extinction-curse-bestiary-items/9mMqegSFLDuJ14KD.htm)|Claw|Garra|modificada|
|[9OdQ3Zp6ZN6b2hx5.htm](extinction-curse-bestiary-items/9OdQ3Zp6ZN6b2hx5.htm)|Constant Spells|Constant Spells|modificada|
|[9P7DE9pnYbvjzjog.htm](extinction-curse-bestiary-items/9P7DE9pnYbvjzjog.htm)|Ranseur|Ranseur|modificada|
|[9TRBVjnPJbPXtMlt.htm](extinction-curse-bestiary-items/9TRBVjnPJbPXtMlt.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[9tVCotEnUfgqibXT.htm](extinction-curse-bestiary-items/9tVCotEnUfgqibXT.htm)|Stone Throes|Garganta de Piedra|modificada|
|[9UJyODZuM14ccZBZ.htm](extinction-curse-bestiary-items/9UJyODZuM14ccZBZ.htm)|Fist|Puño|modificada|
|[9vk0rdEFsd8p86wX.htm](extinction-curse-bestiary-items/9vk0rdEFsd8p86wX.htm)|Major Mentalist's Staff|Báculo del Mentalista Superior|modificada|
|[9vz4EyPOnZrxMkmn.htm](extinction-curse-bestiary-items/9vz4EyPOnZrxMkmn.htm)|Jaws|Fauces|modificada|
|[9WtQUWj0NxeFpth4.htm](extinction-curse-bestiary-items/9WtQUWj0NxeFpth4.htm)|Quick Catch|Quick Catch|modificada|
|[9yZ9mazhUqzLcQR7.htm](extinction-curse-bestiary-items/9yZ9mazhUqzLcQR7.htm)|Mobility|Movilidad|modificada|
|[a0KvieVyT5Xohz5m.htm](extinction-curse-bestiary-items/a0KvieVyT5Xohz5m.htm)|Shield Warden|Guardián escudo|modificada|
|[a0TSS06q0DBjSQ3n.htm](extinction-curse-bestiary-items/a0TSS06q0DBjSQ3n.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[a4SSMVjAQgcwes1C.htm](extinction-curse-bestiary-items/a4SSMVjAQgcwes1C.htm)|Muscle Striker|Muscle Striker|modificada|
|[A4XBLOuiaS91sKg0.htm](extinction-curse-bestiary-items/A4XBLOuiaS91sKg0.htm)|Digging Bar|Barra de excavación|modificada|
|[a5hbLlCGgZWkEv4t.htm](extinction-curse-bestiary-items/a5hbLlCGgZWkEv4t.htm)|Smoke Exhalation|Exhalación de humo|modificada|
|[aamOVZjUNF7bmH68.htm](extinction-curse-bestiary-items/aamOVZjUNF7bmH68.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ab7DMqaYPHB6wIhC.htm](extinction-curse-bestiary-items/ab7DMqaYPHB6wIhC.htm)|Foot|Pie|modificada|
|[AbffdpOVV9q9QwtQ.htm](extinction-curse-bestiary-items/AbffdpOVV9q9QwtQ.htm)|Aggressive Rush|Embestida agresiva|modificada|
|[aBnc125GEiD84WGM.htm](extinction-curse-bestiary-items/aBnc125GEiD84WGM.htm)|Ruinous Weapons|Armas de la ruina|modificada|
|[aBNPRaWuVV6WJ41a.htm](extinction-curse-bestiary-items/aBNPRaWuVV6WJ41a.htm)|Spiked Gauntlet|Spiked Gauntlet|modificada|
|[AcW2qid0dvfeGVR0.htm](extinction-curse-bestiary-items/AcW2qid0dvfeGVR0.htm)|Fist|Puño|modificada|
|[aCZG1swHw2Dy22DT.htm](extinction-curse-bestiary-items/aCZG1swHw2Dy22DT.htm)|Blunt Snout|Hocico romo|modificada|
|[af6LBcRSbi6EeWEW.htm](extinction-curse-bestiary-items/af6LBcRSbi6EeWEW.htm)|Falling Rocks|Caída de rocas|modificada|
|[afriJ8KUeqfph4Ra.htm](extinction-curse-bestiary-items/afriJ8KUeqfph4Ra.htm)|Rhoka Sword|Espada Rhoka|modificada|
|[aGg1kw2fRdZFdeew.htm](extinction-curse-bestiary-items/aGg1kw2fRdZFdeew.htm)|Stone Fist|Puño de Piedra|modificada|
|[aGULlUDAVR1BMKC7.htm](extinction-curse-bestiary-items/aGULlUDAVR1BMKC7.htm)|Smoke Vision|Visión de Humo|modificada|
|[aGUovHznkIyYG8m6.htm](extinction-curse-bestiary-items/aGUovHznkIyYG8m6.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[aJky1rLHdCQPoA58.htm](extinction-curse-bestiary-items/aJky1rLHdCQPoA58.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[AKIZnRoLY7lsf9VG.htm](extinction-curse-bestiary-items/AKIZnRoLY7lsf9VG.htm)|Wicked Bite|Muerdemuerde|modificada|
|[aKtsu17liUh7DC0t.htm](extinction-curse-bestiary-items/aKtsu17liUh7DC0t.htm)|Grab|Agarrado|modificada|
|[An5Bh0snG4EiSy6u.htm](extinction-curse-bestiary-items/An5Bh0snG4EiSy6u.htm)|Trunk Beam|Trunk Beam|modificada|
|[AnbtNKsICChPCBHe.htm](extinction-curse-bestiary-items/AnbtNKsICChPCBHe.htm)|Fling Wax|Lanzar por los aires Cera|modificada|
|[aNQI6UJBZemWiBCa.htm](extinction-curse-bestiary-items/aNQI6UJBZemWiBCa.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[aoirwF9wgYMRSHdg.htm](extinction-curse-bestiary-items/aoirwF9wgYMRSHdg.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[aoZIcdrhcrNGcXgG.htm](extinction-curse-bestiary-items/aoZIcdrhcrNGcXgG.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[apbGXdheHVHXOiQZ.htm](extinction-curse-bestiary-items/apbGXdheHVHXOiQZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[AR8S1f2bVJPU2t0I.htm](extinction-curse-bestiary-items/AR8S1f2bVJPU2t0I.htm)|Coffin Restoration|Restauración de ataúdes|modificada|
|[aT0ulfinBIPpvzuM.htm](extinction-curse-bestiary-items/aT0ulfinBIPpvzuM.htm)|Hampering Blow|Hampering Blow|modificada|
|[AUdWGOamBXZXat3B.htm](extinction-curse-bestiary-items/AUdWGOamBXZXat3B.htm)|Consume Flesh|Consumir carne|modificada|
|[aVumT4gAl6v4gbG0.htm](extinction-curse-bestiary-items/aVumT4gAl6v4gbG0.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[aWpXug8seWiT6d81.htm](extinction-curse-bestiary-items/aWpXug8seWiT6d81.htm)|Dagger|Daga|modificada|
|[AwQS25ihKMRo7aXf.htm](extinction-curse-bestiary-items/AwQS25ihKMRo7aXf.htm)|Tail|Tail|modificada|
|[aWZBvHQ5pBO8L1iS.htm](extinction-curse-bestiary-items/aWZBvHQ5pBO8L1iS.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[ax83ccfFLMazkzxs.htm](extinction-curse-bestiary-items/ax83ccfFLMazkzxs.htm)|Powerful Stench|Hedor intenso|modificada|
|[AYL0s7HCCgpgbmtO.htm](extinction-curse-bestiary-items/AYL0s7HCCgpgbmtO.htm)|Alluring Aspect|Alluring Aspect|modificada|
|[AYucJ5sk37mn8r87.htm](extinction-curse-bestiary-items/AYucJ5sk37mn8r87.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[AYvQsWHdPY8qJXTz.htm](extinction-curse-bestiary-items/AYvQsWHdPY8qJXTz.htm)|Wasting Wound|Herida Hiriente|modificada|
|[B0RufxivwZqPbB6R.htm](extinction-curse-bestiary-items/B0RufxivwZqPbB6R.htm)|Trample|Trample|modificada|
|[b0yCrKSgbslsUe5W.htm](extinction-curse-bestiary-items/b0yCrKSgbslsUe5W.htm)|Dagger|Daga|modificada|
|[b1DiEoXUNFfYIgRG.htm](extinction-curse-bestiary-items/b1DiEoXUNFfYIgRG.htm)|Dagger|Daga|modificada|
|[B1S8agx9nAGw4Bxt.htm](extinction-curse-bestiary-items/B1S8agx9nAGw4Bxt.htm)|Negative Healing|Curación negativa|modificada|
|[Ba3OUInVSq7YCdrO.htm](extinction-curse-bestiary-items/Ba3OUInVSq7YCdrO.htm)|Mounted Bow Expert|Experto en arco montado|modificada|
|[bAw4bSovV5qrCsAx.htm](extinction-curse-bestiary-items/bAw4bSovV5qrCsAx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bB3qQlo5VdUbfeQh.htm](extinction-curse-bestiary-items/bB3qQlo5VdUbfeQh.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[bc9ZTQWLulMjdnJ5.htm](extinction-curse-bestiary-items/bc9ZTQWLulMjdnJ5.htm)|Haywire|Haywire|modificada|
|[bClPOSeIXlOZbjEV.htm](extinction-curse-bestiary-items/bClPOSeIXlOZbjEV.htm)|Iron Mind|Mente férrea|modificada|
|[bcmrCRt6L0KXk4iE.htm](extinction-curse-bestiary-items/bcmrCRt6L0KXk4iE.htm)|Claw|Garra|modificada|
|[bcrG00wkCK5L7uZE.htm](extinction-curse-bestiary-items/bcrG00wkCK5L7uZE.htm)|Negative Healing|Curación negativa|modificada|
|[BcrVzHc6oLVuEmXs.htm](extinction-curse-bestiary-items/BcrVzHc6oLVuEmXs.htm)|Grab|Agarrado|modificada|
|[BD0WtU1mZFhFSCn4.htm](extinction-curse-bestiary-items/BD0WtU1mZFhFSCn4.htm)|Hardscale Shield Stance|Posición de escudo Hardscale.|modificada|
|[bd8O12QXeS6Usuz1.htm](extinction-curse-bestiary-items/bd8O12QXeS6Usuz1.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[BEMhZJteQTj5sUGM.htm](extinction-curse-bestiary-items/BEMhZJteQTj5sUGM.htm)|Engulf|Envolver|modificada|
|[BeWl7Ot39HAvDQoK.htm](extinction-curse-bestiary-items/BeWl7Ot39HAvDQoK.htm)|Status Bonus to Saves|Bonificación de situación a las salvaciones.|modificada|
|[BFPXxBgFh6cenPgG.htm](extinction-curse-bestiary-items/BFPXxBgFh6cenPgG.htm)|Vulnerable to Rust|Vulnerable a Rust|modificada|
|[bgLBaYc65Aq8OxQ2.htm](extinction-curse-bestiary-items/bgLBaYc65Aq8OxQ2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[bHjyGlr68JjtV7RP.htm](extinction-curse-bestiary-items/bHjyGlr68JjtV7RP.htm)|Shred Armor|Triturar Armadura|modificada|
|[bHxBiwRHApJXOQTs.htm](extinction-curse-bestiary-items/bHxBiwRHApJXOQTs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Bi8RZTKlzNcrlZXd.htm](extinction-curse-bestiary-items/Bi8RZTKlzNcrlZXd.htm)|Maul|Zarpazo doble|modificada|
|[bipIvdk7SAVNAKJV.htm](extinction-curse-bestiary-items/bipIvdk7SAVNAKJV.htm)|Quick Draw|Desenvainado rápido|modificada|
|[bj60CVwHmHYJF9pA.htm](extinction-curse-bestiary-items/bj60CVwHmHYJF9pA.htm)|Fangs|Colmillos|modificada|
|[BJhx0ORRonO7nFzG.htm](extinction-curse-bestiary-items/BJhx0ORRonO7nFzG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Bjuv7ra9W5xu1wJi.htm](extinction-curse-bestiary-items/Bjuv7ra9W5xu1wJi.htm)|Shrewd Eye|Ojo astuto|modificada|
|[bKUoSbiAMRYkv1ol.htm](extinction-curse-bestiary-items/bKUoSbiAMRYkv1ol.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[BKVl8zZlq09HHet1.htm](extinction-curse-bestiary-items/BKVl8zZlq09HHet1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bKziwa1hNAYNhHtz.htm](extinction-curse-bestiary-items/bKziwa1hNAYNhHtz.htm)|Charged Earth|Tierra Cargada|modificada|
|[BLfd48IDsZFHeeCm.htm](extinction-curse-bestiary-items/BLfd48IDsZFHeeCm.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[bn0Z3k2gK4VsKjQ4.htm](extinction-curse-bestiary-items/bn0Z3k2gK4VsKjQ4.htm)|Hatchet|Hacha|modificada|
|[bNJHQZLJcksGVWsc.htm](extinction-curse-bestiary-items/bNJHQZLJcksGVWsc.htm)|Boneshaking Roar|Boneshaking Roar|modificada|
|[bOKG9bGFFIf6vSxZ.htm](extinction-curse-bestiary-items/bOKG9bGFFIf6vSxZ.htm)|Cleaver|Cleaver|modificada|
|[boMBR55PMuMTWR4T.htm](extinction-curse-bestiary-items/boMBR55PMuMTWR4T.htm)|Touch of Zevgavizeb|Toque de Zevgavizeb|modificada|
|[BPDL9YkRvinFnlEi.htm](extinction-curse-bestiary-items/BPDL9YkRvinFnlEi.htm)|Selective Scent|Selective Scent|modificada|
|[BPZQ1dC6ouU0B3Ud.htm](extinction-curse-bestiary-items/BPZQ1dC6ouU0B3Ud.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[bQB1uLPL0KXt6C4L.htm](extinction-curse-bestiary-items/bQB1uLPL0KXt6C4L.htm)|Longsword|Longsword|modificada|
|[Bqkj3XCvzqqQZur2.htm](extinction-curse-bestiary-items/Bqkj3XCvzqqQZur2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Br7RNde4i4eoRkTL.htm](extinction-curse-bestiary-items/Br7RNde4i4eoRkTL.htm)|Spear|Lanza|modificada|
|[Brr6EgY2XOH5SCbH.htm](extinction-curse-bestiary-items/Brr6EgY2XOH5SCbH.htm)|Cat Sith's Mark|Cat Sith's Mark|modificada|
|[BsPkvlLcpyGM76pg.htm](extinction-curse-bestiary-items/BsPkvlLcpyGM76pg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[BSqgVF2Zcf7Vv8im.htm](extinction-curse-bestiary-items/BSqgVF2Zcf7Vv8im.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Btfq47RDcKImj5q3.htm](extinction-curse-bestiary-items/Btfq47RDcKImj5q3.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[BTkdHTwWzCskxz7S.htm](extinction-curse-bestiary-items/BTkdHTwWzCskxz7S.htm)|Spectral Gore|Spectral Gore|modificada|
|[buuATh1VmZvUaYAM.htm](extinction-curse-bestiary-items/buuATh1VmZvUaYAM.htm)|Trunk|Tronco|modificada|
|[Bwk1au1bwoRPov0x.htm](extinction-curse-bestiary-items/Bwk1au1bwoRPov0x.htm)|Jaws|Fauces|modificada|
|[bXlG2k7ws1b8Wffl.htm](extinction-curse-bestiary-items/bXlG2k7ws1b8Wffl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[c1nV69h5eKoUvmDM.htm](extinction-curse-bestiary-items/c1nV69h5eKoUvmDM.htm)|Mounted Superiority|Superioridad montada|modificada|
|[c2lrhpLmasLXBnIC.htm](extinction-curse-bestiary-items/c2lrhpLmasLXBnIC.htm)|Constant Spells|Constant Spells|modificada|
|[c3A2YAnkRX3JPc0B.htm](extinction-curse-bestiary-items/c3A2YAnkRX3JPc0B.htm)|Darkvision|Visión en la oscuridad|modificada|
|[C4dcn6EdPVp0Ec5y.htm](extinction-curse-bestiary-items/C4dcn6EdPVp0Ec5y.htm)|Vulnerability to Extinguishing|Vulnerabilidad a la extinción|modificada|
|[C5CbW2c2gPmnN8il.htm](extinction-curse-bestiary-items/C5CbW2c2gPmnN8il.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[C5gcMBexKui4KaYg.htm](extinction-curse-bestiary-items/C5gcMBexKui4KaYg.htm)|Paralytic Saliva|Paralytic Saliva|modificada|
|[c716Uv2dXVDhuZaX.htm](extinction-curse-bestiary-items/c716Uv2dXVDhuZaX.htm)|Treacherous Veil|Velo engaso|modificada|
|[C8fFcklKWgsWS7pR.htm](extinction-curse-bestiary-items/C8fFcklKWgsWS7pR.htm)|Forced Transfusion|Transfusión Forzada|modificada|
|[c9zfJMOOSFj5Gqcb.htm](extinction-curse-bestiary-items/c9zfJMOOSFj5Gqcb.htm)|Mounted Superiority|Superioridad montada|modificada|
|[CARAuWZoSe8i4SSF.htm](extinction-curse-bestiary-items/CARAuWZoSe8i4SSF.htm)|Jaws|Fauces|modificada|
|[Cb2C8LIPyMVI1r9J.htm](extinction-curse-bestiary-items/Cb2C8LIPyMVI1r9J.htm)|Vent|Expulsar gases|modificada|
|[cca2HxwcLz1pklgq.htm](extinction-curse-bestiary-items/cca2HxwcLz1pklgq.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[cceloNgYjNLlTIG8.htm](extinction-curse-bestiary-items/cceloNgYjNLlTIG8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CCoxtcbk850mBaMi.htm](extinction-curse-bestiary-items/CCoxtcbk850mBaMi.htm)|Jaws|Fauces|modificada|
|[cDjgzvQmTMWhvvaO.htm](extinction-curse-bestiary-items/cDjgzvQmTMWhvvaO.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[cdxLqOHRZzka3F9n.htm](extinction-curse-bestiary-items/cdxLqOHRZzka3F9n.htm)|Tentacular Burst|Tentacular Burst|modificada|
|[ce0ME84C2cbPH0JA.htm](extinction-curse-bestiary-items/ce0ME84C2cbPH0JA.htm)|Paralysis|Parálisis|modificada|
|[Ce2SsOT9AnFtxFb9.htm](extinction-curse-bestiary-items/Ce2SsOT9AnFtxFb9.htm)|Ranseur|Ranseur|modificada|
|[CeucQqSmssZqnYDf.htm](extinction-curse-bestiary-items/CeucQqSmssZqnYDf.htm)|Flippable|Flippable|modificada|
|[CfGyCIUgQSgrIVyE.htm](extinction-curse-bestiary-items/CfGyCIUgQSgrIVyE.htm)|Powerful Stench|Hedor intenso|modificada|
|[Cgn2V1u3dIaaSjCc.htm](extinction-curse-bestiary-items/Cgn2V1u3dIaaSjCc.htm)|Successful Fortitude saves are critical successes instead|Las salvaciones de Fortaleza exitosas son éxitos críticos en su lugar|modificada|
|[CHFfq14eNMMH7UGa.htm](extinction-curse-bestiary-items/CHFfq14eNMMH7UGa.htm)|Demonic Strength|Fuerza demoníaca|modificada|
|[CHkvWxLYAe1SOzaW.htm](extinction-curse-bestiary-items/CHkvWxLYAe1SOzaW.htm)|Greatpick|Greatpick|modificada|
|[cI3F9pBXykpzpGpP.htm](extinction-curse-bestiary-items/cI3F9pBXykpzpGpP.htm)|Tail|Tail|modificada|
|[Ci7biTFOwXd5SyQV.htm](extinction-curse-bestiary-items/Ci7biTFOwXd5SyQV.htm)|+23 when using Perception for Initiative|+23 al usar Percepción para Iniciativa|modificada|
|[cI80h0WLWTix6fdX.htm](extinction-curse-bestiary-items/cI80h0WLWTix6fdX.htm)|Dagger|Daga|modificada|
|[cJAwzP7ugkINDPvs.htm](extinction-curse-bestiary-items/cJAwzP7ugkINDPvs.htm)|Trample|Trample|modificada|
|[cKPrl4Em8PlcrlED.htm](extinction-curse-bestiary-items/cKPrl4Em8PlcrlED.htm)|Bowling Pin|Bowling Pin|modificada|
|[cKUzL3t8iW7J5EWm.htm](extinction-curse-bestiary-items/cKUzL3t8iW7J5EWm.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[CLbLXBJNSCtMb0bH.htm](extinction-curse-bestiary-items/CLbLXBJNSCtMb0bH.htm)|Jaws|Fauces|modificada|
|[CmEN8uWFDeuYp1rP.htm](extinction-curse-bestiary-items/CmEN8uWFDeuYp1rP.htm)|Improvised Surprise|Sorpresa improvisada|modificada|
|[cmJ4It6KgqjEgmOi.htm](extinction-curse-bestiary-items/cmJ4It6KgqjEgmOi.htm)|Powerful Stench|Hedor intenso|modificada|
|[coep2DirH6k49uz3.htm](extinction-curse-bestiary-items/coep2DirH6k49uz3.htm)|Flea Fever|Fiebre de Pulgas|modificada|
|[cpcIX2KKJCMLAkwJ.htm](extinction-curse-bestiary-items/cpcIX2KKJCMLAkwJ.htm)|Quick Draw|Desenvainado rápido|modificada|
|[cQcOaBFYo9hz8JUB.htm](extinction-curse-bestiary-items/cQcOaBFYo9hz8JUB.htm)|Fast Healing 30|Curación rápida 30|modificada|
|[cr8qKlbGGGNeDAHl.htm](extinction-curse-bestiary-items/cr8qKlbGGGNeDAHl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Csc3FpAiT4LS7zI7.htm](extinction-curse-bestiary-items/Csc3FpAiT4LS7zI7.htm)|Jaws|Fauces|modificada|
|[cSPBpYXKmUBKXwQ3.htm](extinction-curse-bestiary-items/cSPBpYXKmUBKXwQ3.htm)|Horns|Cuernos|modificada|
|[Csqes18z0CZnmC2a.htm](extinction-curse-bestiary-items/Csqes18z0CZnmC2a.htm)|Begin Convergence|Begin Convergence|modificada|
|[cSvshId5YCLIYZCG.htm](extinction-curse-bestiary-items/cSvshId5YCLIYZCG.htm)|Eye Stalk|Tallo del ojo|modificada|
|[CVT6zaghpuo7F5Qs.htm](extinction-curse-bestiary-items/CVT6zaghpuo7F5Qs.htm)|Cleaver|Cleaver|modificada|
|[CwMPxG8qELCyPvc1.htm](extinction-curse-bestiary-items/CwMPxG8qELCyPvc1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cXwerHf1acuccvb8.htm](extinction-curse-bestiary-items/cXwerHf1acuccvb8.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[cYcpQzRaSNxmtRwk.htm](extinction-curse-bestiary-items/cYcpQzRaSNxmtRwk.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[CyWELRcAGeuO8Xdv.htm](extinction-curse-bestiary-items/CyWELRcAGeuO8Xdv.htm)|Claw|Garra|modificada|
|[cZEvbEpDTZmmwmdY.htm](extinction-curse-bestiary-items/cZEvbEpDTZmmwmdY.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[D1U3wK3uyoOs5sL1.htm](extinction-curse-bestiary-items/D1U3wK3uyoOs5sL1.htm)|Tainted Rage|Furia manchada|modificada|
|[D2bX7v0RmrFFnVye.htm](extinction-curse-bestiary-items/D2bX7v0RmrFFnVye.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[D8wFcvl1kfrS8ylG.htm](extinction-curse-bestiary-items/D8wFcvl1kfrS8ylG.htm)|Reckless Abandon|Abandono temerario|modificada|
|[daRZgdF46eHobOw0.htm](extinction-curse-bestiary-items/daRZgdF46eHobOw0.htm)|Stench|Hedor|modificada|
|[DBng7QU65qzXSOrS.htm](extinction-curse-bestiary-items/DBng7QU65qzXSOrS.htm)|Resin Spray|Spray de Resina|modificada|
|[dCh3xhDcX6ef0gtU.htm](extinction-curse-bestiary-items/dCh3xhDcX6ef0gtU.htm)|Greasy Seepage|Greasy Seepage|modificada|
|[dcsCqElp022elQ9N.htm](extinction-curse-bestiary-items/dcsCqElp022elQ9N.htm)|Jaws|Fauces|modificada|
|[deSS0MPuciBz6LLK.htm](extinction-curse-bestiary-items/deSS0MPuciBz6LLK.htm)|Syringe|Jeringa|modificada|
|[dFflNs9ymwMB3ky8.htm](extinction-curse-bestiary-items/dFflNs9ymwMB3ky8.htm)|Stench|Hedor|modificada|
|[dFgEVCoqXZx2LxVQ.htm](extinction-curse-bestiary-items/dFgEVCoqXZx2LxVQ.htm)|No Escape|Sin huida posible|modificada|
|[DfLYFCPstsIKpqse.htm](extinction-curse-bestiary-items/DfLYFCPstsIKpqse.htm)|Knockdown|Derribo|modificada|
|[dFwMJyxs9OvKd6Hk.htm](extinction-curse-bestiary-items/dFwMJyxs9OvKd6Hk.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[DgPlSSqql6BVhbUO.htm](extinction-curse-bestiary-items/DgPlSSqql6BVhbUO.htm)|Quick Draw|Desenvainado rápido|modificada|
|[dIqtE84EVpEREjSZ.htm](extinction-curse-bestiary-items/dIqtE84EVpEREjSZ.htm)|Quick Bomber|Bombardero rápido|modificada|
|[diUbIYXAIUT4w27K.htm](extinction-curse-bestiary-items/diUbIYXAIUT4w27K.htm)|Pain Touch|Toque de Dolor|modificada|
|[DJwAey2Keob2rJ2y.htm](extinction-curse-bestiary-items/DJwAey2Keob2rJ2y.htm)|Dangerous Sorcery|Hechicería peligrosa|modificada|
|[DLLKE5ky8d8zWKa4.htm](extinction-curse-bestiary-items/DLLKE5ky8d8zWKa4.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[Dm321ezqoyu7Pz2L.htm](extinction-curse-bestiary-items/Dm321ezqoyu7Pz2L.htm)|Truth Vulnerability|Vulnerabilidad a la verdad|modificada|
|[dM9C8lwWHWEBUxDR.htm](extinction-curse-bestiary-items/dM9C8lwWHWEBUxDR.htm)|Wild Empathy|Empatía salvaje|modificada|
|[DmFD5HCxaT3wtIMk.htm](extinction-curse-bestiary-items/DmFD5HCxaT3wtIMk.htm)|Devour Soul|Devorar alma|modificada|
|[DNVtXPFDKGXBnkvT.htm](extinction-curse-bestiary-items/DNVtXPFDKGXBnkvT.htm)|Claw|Garra|modificada|
|[doOqdxPdlJYa0GsX.htm](extinction-curse-bestiary-items/doOqdxPdlJYa0GsX.htm)|Mobile Shot Stance|Posición de disparo múltiple.|modificada|
|[dPn7Q8LR4nsQ4Aar.htm](extinction-curse-bestiary-items/dPn7Q8LR4nsQ4Aar.htm)|Jaws|Fauces|modificada|
|[dq6M4Jej5LRa8pai.htm](extinction-curse-bestiary-items/dq6M4Jej5LRa8pai.htm)|Restoration Vulnerability|Vulnerabilidad de restablecimiento|modificada|
|[DQLALym9BRxjL6Rc.htm](extinction-curse-bestiary-items/DQLALym9BRxjL6Rc.htm)|Fist|Puño|modificada|
|[DqSmbwh963Ftosau.htm](extinction-curse-bestiary-items/DqSmbwh963Ftosau.htm)|Improvised Weapon|Arma Improvisada|modificada|
|[dQxzoa0Vlfr0DKua.htm](extinction-curse-bestiary-items/dQxzoa0Vlfr0DKua.htm)|Halberd|Alabarda|modificada|
|[DqZJwi3AmpInsFhs.htm](extinction-curse-bestiary-items/DqZJwi3AmpInsFhs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DrJdeATQTR9zK1Rn.htm](extinction-curse-bestiary-items/DrJdeATQTR9zK1Rn.htm)|Pummel the Fallen|Pummel the Fallen|modificada|
|[DsnYPhbnCJOnQiIV.htm](extinction-curse-bestiary-items/DsnYPhbnCJOnQiIV.htm)|Negative Healing|Curación negativa|modificada|
|[DtEZmNobgsVWzikz.htm](extinction-curse-bestiary-items/DtEZmNobgsVWzikz.htm)|Dooming Bark|Ladrido condenado/a|modificada|
|[DtYm8jdItEs6dlmW.htm](extinction-curse-bestiary-items/DtYm8jdItEs6dlmW.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[DVb8rll2Xx3wj0Zx.htm](extinction-curse-bestiary-items/DVb8rll2Xx3wj0Zx.htm)|Claw|Garra|modificada|
|[DveD5UtW57U98ZZr.htm](extinction-curse-bestiary-items/DveD5UtW57U98ZZr.htm)|Weapon Master|Maestro de armas|modificada|
|[dvKGUrS6UQXX4qts.htm](extinction-curse-bestiary-items/dvKGUrS6UQXX4qts.htm)|Tentacle|Tentáculo|modificada|
|[DvUIErNvZwDWm1xw.htm](extinction-curse-bestiary-items/DvUIErNvZwDWm1xw.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[DwsRNc9qk55FLo8f.htm](extinction-curse-bestiary-items/DwsRNc9qk55FLo8f.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[dWxzw3v6njHLRsgV.htm](extinction-curse-bestiary-items/dWxzw3v6njHLRsgV.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[DXu0yLebZZ8ZPgay.htm](extinction-curse-bestiary-items/DXu0yLebZZ8ZPgay.htm)|Convergent Link|Enlace Convergente|modificada|
|[DzcmUlbG0ynUzWSX.htm](extinction-curse-bestiary-items/DzcmUlbG0ynUzWSX.htm)|Alchemist's Fire|Alchemist's Fire|modificada|
|[e09QNAXtgAev98xw.htm](extinction-curse-bestiary-items/e09QNAXtgAev98xw.htm)|Shortsword|Espada corta|modificada|
|[e1DGkcB2QL0mRcLw.htm](extinction-curse-bestiary-items/e1DGkcB2QL0mRcLw.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[E2UQQSW7N3ZLX51N.htm](extinction-curse-bestiary-items/E2UQQSW7N3ZLX51N.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[E4kWYfMtWsLrq662.htm](extinction-curse-bestiary-items/E4kWYfMtWsLrq662.htm)|Claw|Garra|modificada|
|[e4w04mrVyi9Okgu8.htm](extinction-curse-bestiary-items/e4w04mrVyi9Okgu8.htm)|Twisted Desires|Deseos corrompidos|modificada|
|[e6hdWb90a4uxuhor.htm](extinction-curse-bestiary-items/e6hdWb90a4uxuhor.htm)|Bomber|Bombardero|modificada|
|[E7ey0z1cm7nKTIH7.htm](extinction-curse-bestiary-items/E7ey0z1cm7nKTIH7.htm)|Vampire Drider Venom|Veneno de dra vampiro|modificada|
|[ead7pwbvr3oideQg.htm](extinction-curse-bestiary-items/ead7pwbvr3oideQg.htm)|Pollen Burst|Ráfaga de Polen|modificada|
|[eb5QTbp37dxuOa2K.htm](extinction-curse-bestiary-items/eb5QTbp37dxuOa2K.htm)|Constant Spells|Constant Spells|modificada|
|[EbJV7OigGb2BRRbY.htm](extinction-curse-bestiary-items/EbJV7OigGb2BRRbY.htm)|Rabies|Rabia|modificada|
|[EbKHyd2DVj0sOh0r.htm](extinction-curse-bestiary-items/EbKHyd2DVj0sOh0r.htm)|Dagger|Daga|modificada|
|[ee9wYp6ZD1nOsbJs.htm](extinction-curse-bestiary-items/ee9wYp6ZD1nOsbJs.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[EEbVQQPyiltRr8dm.htm](extinction-curse-bestiary-items/EEbVQQPyiltRr8dm.htm)|Counterspell|Contraconjuro|modificada|
|[eF1NPrH42zK6nWtF.htm](extinction-curse-bestiary-items/eF1NPrH42zK6nWtF.htm)|Bloodline Magic|Magia de linaje|modificada|
|[EHoVL9UXHlhPng6e.htm](extinction-curse-bestiary-items/EHoVL9UXHlhPng6e.htm)|Fist|Puño|modificada|
|[eJ1kLbGFLHyRoqnW.htm](extinction-curse-bestiary-items/eJ1kLbGFLHyRoqnW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Ekh1nBGghfmd4C6b.htm](extinction-curse-bestiary-items/Ekh1nBGghfmd4C6b.htm)|Blunt Snout|Hocico romo|modificada|
|[eKTLgw1q7VJ64kFf.htm](extinction-curse-bestiary-items/eKTLgw1q7VJ64kFf.htm)|Claw|Garra|modificada|
|[ElO9xLxjB4wblX7d.htm](extinction-curse-bestiary-items/ElO9xLxjB4wblX7d.htm)|Flail|Mayal|modificada|
|[eNCqlXhdNVfLn6zx.htm](extinction-curse-bestiary-items/eNCqlXhdNVfLn6zx.htm)|Powerful Stench|Hedor intenso|modificada|
|[EOAJdvYpfMEyRoEY.htm](extinction-curse-bestiary-items/EOAJdvYpfMEyRoEY.htm)|Claw|Garra|modificada|
|[EoEZjFDg31YP4hUr.htm](extinction-curse-bestiary-items/EoEZjFDg31YP4hUr.htm)|Hatchet|Hacha|modificada|
|[EoheVjsQC39ZT9Ya.htm](extinction-curse-bestiary-items/EoheVjsQC39ZT9Ya.htm)|Quick Draw|Desenvainado rápido|modificada|
|[EOlqIML78FxmOKnN.htm](extinction-curse-bestiary-items/EOlqIML78FxmOKnN.htm)|Grab|Agarrado|modificada|
|[Eoo8nFMnqyYVA8S9.htm](extinction-curse-bestiary-items/Eoo8nFMnqyYVA8S9.htm)|Unsprung Wires|Unsprung Wires|modificada|
|[Epd15gSJ08XAuMS2.htm](extinction-curse-bestiary-items/Epd15gSJ08XAuMS2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[EPOz45AVHD1zOj5z.htm](extinction-curse-bestiary-items/EPOz45AVHD1zOj5z.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[ePRrl3MsWtcXXzD7.htm](extinction-curse-bestiary-items/ePRrl3MsWtcXXzD7.htm)|Terrifying Touch|Toque terrorífico|modificada|
|[epTn8izUucNDtBXJ.htm](extinction-curse-bestiary-items/epTn8izUucNDtBXJ.htm)|Jaws|Fauces|modificada|
|[EQPvoy0awap74MNj.htm](extinction-curse-bestiary-items/EQPvoy0awap74MNj.htm)|Shortsword|Espada corta|modificada|
|[eQwOg2taLdr6yqIb.htm](extinction-curse-bestiary-items/eQwOg2taLdr6yqIb.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[erJjvQ0PRlmb1VLz.htm](extinction-curse-bestiary-items/erJjvQ0PRlmb1VLz.htm)|Existential Agony|Agonía Existencial|modificada|
|[ESJs8ZFuYfcIo7WE.htm](extinction-curse-bestiary-items/ESJs8ZFuYfcIo7WE.htm)|Muse Possession|Posesión de musa|modificada|
|[eTns0wXwB8nrvDuF.htm](extinction-curse-bestiary-items/eTns0wXwB8nrvDuF.htm)|Push|Push|modificada|
|[etXQCdL5Q2AdyjxP.htm](extinction-curse-bestiary-items/etXQCdL5Q2AdyjxP.htm)|Force Communication|Comunicación vigorosa|modificada|
|[EVi2gsT6wLToy6mR.htm](extinction-curse-bestiary-items/EVi2gsT6wLToy6mR.htm)|Light in the Darkness|Luz en la oscuridad|modificada|
|[eWqNIy4ejpF685wS.htm](extinction-curse-bestiary-items/eWqNIy4ejpF685wS.htm)|Lurching Charge|Cargar dando bandazos|modificada|
|[eWuV67fIWoPIYMfp.htm](extinction-curse-bestiary-items/eWuV67fIWoPIYMfp.htm)|Grab|Agarrado|modificada|
|[eX9GmQ5kPOaJk2yM.htm](extinction-curse-bestiary-items/eX9GmQ5kPOaJk2yM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[exnEDT15hRasjXZk.htm](extinction-curse-bestiary-items/exnEDT15hRasjXZk.htm)|Powerful Stench|Hedor intenso|modificada|
|[eYBDQwviDeBD3lXK.htm](extinction-curse-bestiary-items/eYBDQwviDeBD3lXK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EyUD2ROErzsB3uXQ.htm](extinction-curse-bestiary-items/EyUD2ROErzsB3uXQ.htm)|Jaws|Fauces|modificada|
|[F4djhkzZDXpAotWi.htm](extinction-curse-bestiary-items/F4djhkzZDXpAotWi.htm)|Rend|Rasgadura|modificada|
|[f6rRwvnYIpGpqCcN.htm](extinction-curse-bestiary-items/f6rRwvnYIpGpqCcN.htm)|Negative Healing|Curación negativa|modificada|
|[F9xnDBHxVUqqbUBb.htm](extinction-curse-bestiary-items/F9xnDBHxVUqqbUBb.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FADVo5mG2frt3d8x.htm](extinction-curse-bestiary-items/FADVo5mG2frt3d8x.htm)|Soulfire Inhalation|Soulfire Inhalation|modificada|
|[faLRVKzt3bOyIaTB.htm](extinction-curse-bestiary-items/faLRVKzt3bOyIaTB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[FbbJSkeiW59zMcZe.htm](extinction-curse-bestiary-items/FbbJSkeiW59zMcZe.htm)|Grab|Agarrado|modificada|
|[FBUfRtHdX6pDQqmM.htm](extinction-curse-bestiary-items/FBUfRtHdX6pDQqmM.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[FbV73v3RSYlWpc56.htm](extinction-curse-bestiary-items/FbV73v3RSYlWpc56.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FD1j6iyu3TIbvvW9.htm](extinction-curse-bestiary-items/FD1j6iyu3TIbvvW9.htm)|Web|Telara|modificada|
|[fD1wLMV2uktFhB2U.htm](extinction-curse-bestiary-items/fD1wLMV2uktFhB2U.htm)|Devour Soul|Devorar alma|modificada|
|[Fdjdb2OGv08d4sL7.htm](extinction-curse-bestiary-items/Fdjdb2OGv08d4sL7.htm)|Jaws|Fauces|modificada|
|[fekB8XfrAypEycMA.htm](extinction-curse-bestiary-items/fekB8XfrAypEycMA.htm)|Whip Of Compliance|Whip Of Compliance|modificada|
|[fgb3vhQw8zBEaCrc.htm](extinction-curse-bestiary-items/fgb3vhQw8zBEaCrc.htm)|Spear|Lanza|modificada|
|[fhBXlEiQsR5ijwfv.htm](extinction-curse-bestiary-items/fhBXlEiQsR5ijwfv.htm)|Grab|Agarrado|modificada|
|[FHc5gHaZ8ngFzhdy.htm](extinction-curse-bestiary-items/FHc5gHaZ8ngFzhdy.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[fHtpNhpRCcTQHzVO.htm](extinction-curse-bestiary-items/fHtpNhpRCcTQHzVO.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[FHYZijWKqxyPWq4b.htm](extinction-curse-bestiary-items/FHYZijWKqxyPWq4b.htm)|Construct Armor (Hardness 10)|Construir Armadura (Dureza 10)|modificada|
|[FJAtShazGIV67CZM.htm](extinction-curse-bestiary-items/FJAtShazGIV67CZM.htm)|Longbow|Arco largo|modificada|
|[Fkch2WMm3nRHukfN.htm](extinction-curse-bestiary-items/Fkch2WMm3nRHukfN.htm)|Cleric Focus Spells|Cleric Conjuros de foco|modificada|
|[FKJSbQwQyoWxOJAj.htm](extinction-curse-bestiary-items/FKJSbQwQyoWxOJAj.htm)|Fling Shards|Lanzar por los aires|modificada|
|[FMub7YCrl8phAqPa.htm](extinction-curse-bestiary-items/FMub7YCrl8phAqPa.htm)|Change Shape|Change Shape|modificada|
|[FnXVIj505rfZjlbt.htm](extinction-curse-bestiary-items/FnXVIj505rfZjlbt.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[FOFEvFiF3HyHeD3C.htm](extinction-curse-bestiary-items/FOFEvFiF3HyHeD3C.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[fofjczD57mSL1xA2.htm](extinction-curse-bestiary-items/fofjczD57mSL1xA2.htm)|Motion Sense|Sentido del movimiento|modificada|
|[fQBkHuE4KEzg86Bk.htm](extinction-curse-bestiary-items/fQBkHuE4KEzg86Bk.htm)|Wicked Bite|Muerdemuerde|modificada|
|[FQIgX2HCNkiPsSUB.htm](extinction-curse-bestiary-items/FQIgX2HCNkiPsSUB.htm)|Death Drider Venom|Veneno de dra de la muerte|modificada|
|[FrMmR6TGEZYNt4Fc.htm](extinction-curse-bestiary-items/FrMmR6TGEZYNt4Fc.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[FS7b7xFxtknejiPh.htm](extinction-curse-bestiary-items/FS7b7xFxtknejiPh.htm)|Psychic Howl|Aullido Psíquico|modificada|
|[Fu9Yec7x12lmehJE.htm](extinction-curse-bestiary-items/Fu9Yec7x12lmehJE.htm)|Snatch|Arrebatar|modificada|
|[fUeuCzXg5rcHjjeb.htm](extinction-curse-bestiary-items/fUeuCzXg5rcHjjeb.htm)|Rage|Furia|modificada|
|[fVDzActy99X50n5n.htm](extinction-curse-bestiary-items/fVDzActy99X50n5n.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[FWCaUjBmQQkZbFfs.htm](extinction-curse-bestiary-items/FWCaUjBmQQkZbFfs.htm)|Grievous Strike|Golpe lacerante|modificada|
|[fWI5g5k9p5EtqWoH.htm](extinction-curse-bestiary-items/fWI5g5k9p5EtqWoH.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[FwQ6hfFQbFbTSEWf.htm](extinction-curse-bestiary-items/FwQ6hfFQbFbTSEWf.htm)|Juggernaut|Juggernaut|modificada|
|[Fz4qoI14He66tmpf.htm](extinction-curse-bestiary-items/Fz4qoI14He66tmpf.htm)|Commander's Aura|Aura de comandante|modificada|
|[Fza6vai8Wr7r8P6I.htm](extinction-curse-bestiary-items/Fza6vai8Wr7r8P6I.htm)|Buck|Encabritarse|modificada|
|[fzEhis5mum8aBN0a.htm](extinction-curse-bestiary-items/fzEhis5mum8aBN0a.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[FZSw4o3rZCgCEFcW.htm](extinction-curse-bestiary-items/FZSw4o3rZCgCEFcW.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[g1Oa6Im9D6DICdqU.htm](extinction-curse-bestiary-items/g1Oa6Im9D6DICdqU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[g2NeO9TpE9vzveoP.htm](extinction-curse-bestiary-items/g2NeO9TpE9vzveoP.htm)|Swallow Whole|Engullir Todo|modificada|
|[g2OkSiICf1gw8aes.htm](extinction-curse-bestiary-items/g2OkSiICf1gw8aes.htm)|Quicken|Quicken|modificada|
|[G2WFzkTuSXgoD6cE.htm](extinction-curse-bestiary-items/G2WFzkTuSXgoD6cE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[g4ebpNlSIRHftDFy.htm](extinction-curse-bestiary-items/g4ebpNlSIRHftDFy.htm)|Spring-loaded Fist|Puño con resorte|modificada|
|[g65rOk12pYhMLjoe.htm](extinction-curse-bestiary-items/g65rOk12pYhMLjoe.htm)|Bob and Weave|Bob y Weave|modificada|
|[g6JXzCEcGkpvkTOb.htm](extinction-curse-bestiary-items/g6JXzCEcGkpvkTOb.htm)|Stalker|Stalker|modificada|
|[gb1r1Z5H5PiFdqyV.htm](extinction-curse-bestiary-items/gb1r1Z5H5PiFdqyV.htm)|Sudden Slices|Sudden Slices|modificada|
|[GCpCG2QeVwHXzzqM.htm](extinction-curse-bestiary-items/GCpCG2QeVwHXzzqM.htm)|Ghast Fever|Fiebre del ghast|modificada|
|[gen8uxMPnuBegIbr.htm](extinction-curse-bestiary-items/gen8uxMPnuBegIbr.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[gFpOboFky2k5eBIf.htm](extinction-curse-bestiary-items/gFpOboFky2k5eBIf.htm)|Sudden Betrayal|Traición inesperada|modificada|
|[Gg4EGIEKIMWM6Lln.htm](extinction-curse-bestiary-items/Gg4EGIEKIMWM6Lln.htm)|Color Splash|Color Splash|modificada|
|[GHc8hcw19z08DhNU.htm](extinction-curse-bestiary-items/GHc8hcw19z08DhNU.htm)|Soulfire Breath|Soulfire Breath|modificada|
|[gHELFZYck8aJljEX.htm](extinction-curse-bestiary-items/gHELFZYck8aJljEX.htm)|Negative Healing|Curación negativa|modificada|
|[gHi2vKCOAok2Xdj9.htm](extinction-curse-bestiary-items/gHi2vKCOAok2Xdj9.htm)|Frost Susceptibility|Susceptibilidad a las Gélidas|modificada|
|[gi1EHeu40BAEjrvS.htm](extinction-curse-bestiary-items/gi1EHeu40BAEjrvS.htm)|Negative Healing|Curación negativa|modificada|
|[GiqRd4nRXj6C8YKM.htm](extinction-curse-bestiary-items/GiqRd4nRXj6C8YKM.htm)|Opportunistic Brawler|Oportunista Brawler|modificada|
|[GJ3bbiF7rgm9jQY8.htm](extinction-curse-bestiary-items/GJ3bbiF7rgm9jQY8.htm)|Claw|Garra|modificada|
|[GJq1pfArAB262GZd.htm](extinction-curse-bestiary-items/GJq1pfArAB262GZd.htm)|Raptor Jaw Disarm|Desarmar Fauces de Raptor|modificada|
|[glENAMfpx6hiXDjN.htm](extinction-curse-bestiary-items/glENAMfpx6hiXDjN.htm)|Tongue|Lengua|modificada|
|[GliiYsgCIg3rdZ5a.htm](extinction-curse-bestiary-items/GliiYsgCIg3rdZ5a.htm)|Vulnerable to Dispelling|Vulnerable a Disipar|modificada|
|[glkLv9FBF94aocjz.htm](extinction-curse-bestiary-items/glkLv9FBF94aocjz.htm)|Flame Spit|Escupir Flamígera|modificada|
|[Gm2RUsy31ZQjt84W.htm](extinction-curse-bestiary-items/Gm2RUsy31ZQjt84W.htm)|Jaws|Fauces|modificada|
|[gnbyS4J4UvV5J0pq.htm](extinction-curse-bestiary-items/gnbyS4J4UvV5J0pq.htm)|Deadly Aim|Puntería mortal|modificada|
|[GoczydT7BBRPFRMa.htm](extinction-curse-bestiary-items/GoczydT7BBRPFRMa.htm)|Trample|Trample|modificada|
|[GoIeG0DnvMbYVA6T.htm](extinction-curse-bestiary-items/GoIeG0DnvMbYVA6T.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[GP3KcxrUOzub9EVd.htm](extinction-curse-bestiary-items/GP3KcxrUOzub9EVd.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[gQ7hVeDi4X11cJlc.htm](extinction-curse-bestiary-items/gQ7hVeDi4X11cJlc.htm)|Cursed Wound|Herida maldita|modificada|
|[GrkAR3xwWPZIyTAt.htm](extinction-curse-bestiary-items/GrkAR3xwWPZIyTAt.htm)|Fangs|Colmillos|modificada|
|[grrGcmbt2tnL36tN.htm](extinction-curse-bestiary-items/grrGcmbt2tnL36tN.htm)|Destructive Strike|Golpe Destructivo|modificada|
|[GrscH4cYbnoD8K5i.htm](extinction-curse-bestiary-items/GrscH4cYbnoD8K5i.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[gSUoDPZCnPwyLrzQ.htm](extinction-curse-bestiary-items/gSUoDPZCnPwyLrzQ.htm)|Horn|Cuerno|modificada|
|[GTru3mYViGeiuDV3.htm](extinction-curse-bestiary-items/GTru3mYViGeiuDV3.htm)|Fist|Puño|modificada|
|[guwEcneKYJeKnJ6x.htm](extinction-curse-bestiary-items/guwEcneKYJeKnJ6x.htm)|Jaws|Fauces|modificada|
|[gV8plzH46OeVvlNG.htm](extinction-curse-bestiary-items/gV8plzH46OeVvlNG.htm)|Stench|Hedor|modificada|
|[gWGzUCdK9CnnBc97.htm](extinction-curse-bestiary-items/gWGzUCdK9CnnBc97.htm)|Spiked Gauntlet|Spiked Gauntlet|modificada|
|[GWziNffDvPJKsyJE.htm](extinction-curse-bestiary-items/GWziNffDvPJKsyJE.htm)|Wicked Bite|Muerdemuerde|modificada|
|[GXC9PnE9TL5hNcx5.htm](extinction-curse-bestiary-items/GXC9PnE9TL5hNcx5.htm)|Falling Stones|Falling Stones|modificada|
|[gYLcvHg47lmk8WVF.htm](extinction-curse-bestiary-items/gYLcvHg47lmk8WVF.htm)|Negative Healing|Curación negativa|modificada|
|[h0hJAD32OkrXK3lx.htm](extinction-curse-bestiary-items/h0hJAD32OkrXK3lx.htm)|Wolf Empathy|Wolf Empathy|modificada|
|[H1uqJEQyZBDutvmh.htm](extinction-curse-bestiary-items/H1uqJEQyZBDutvmh.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[H3lSdbz8S3BWprZ6.htm](extinction-curse-bestiary-items/H3lSdbz8S3BWprZ6.htm)|Striking Fear|Golpe de Miedo|modificada|
|[H5lFEWzr0REUektR.htm](extinction-curse-bestiary-items/H5lFEWzr0REUektR.htm)|Constant Spells|Constant Spells|modificada|
|[H8wjoy6dgcpleGxb.htm](extinction-curse-bestiary-items/H8wjoy6dgcpleGxb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[H925aaHFfAyXeLkl.htm](extinction-curse-bestiary-items/H925aaHFfAyXeLkl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H9E215npIwweVezi.htm](extinction-curse-bestiary-items/H9E215npIwweVezi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[h9vBtsjNnNHixcDh.htm](extinction-curse-bestiary-items/h9vBtsjNnNHixcDh.htm)|Constant Spells|Constant Spells|modificada|
|[h9wGVkU8tCqqBBXU.htm](extinction-curse-bestiary-items/h9wGVkU8tCqqBBXU.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[HAq5bDyta4z3yz98.htm](extinction-curse-bestiary-items/HAq5bDyta4z3yz98.htm)|Perverse Prayer|Oración Perversa|modificada|
|[hcpegSxpNx41ckc9.htm](extinction-curse-bestiary-items/hcpegSxpNx41ckc9.htm)|Brutal Blow|Golpe Brutal|modificada|
|[hcqt8oyd2ReY1Sbz.htm](extinction-curse-bestiary-items/hcqt8oyd2ReY1Sbz.htm)|Cleaver|Cleaver|modificada|
|[hd8pBhwAE36GIvt7.htm](extinction-curse-bestiary-items/hd8pBhwAE36GIvt7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[heTem3QEjhzZKzbj.htm](extinction-curse-bestiary-items/heTem3QEjhzZKzbj.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Heyof9Emy1vFTnqq.htm](extinction-curse-bestiary-items/Heyof9Emy1vFTnqq.htm)|Frightful Presence|Frightful Presence|modificada|
|[HGadzUCJdCezQNEJ.htm](extinction-curse-bestiary-items/HGadzUCJdCezQNEJ.htm)|Mauler|Vapulear|modificada|
|[hgAe4LDV9pOetFnG.htm](extinction-curse-bestiary-items/hgAe4LDV9pOetFnG.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[hhgKxBXjAKv8GaOe.htm](extinction-curse-bestiary-items/hhgKxBXjAKv8GaOe.htm)|Generate Bomb|Generar bomba|modificada|
|[hKCyP0kuF8Zoh3ey.htm](extinction-curse-bestiary-items/hKCyP0kuF8Zoh3ey.htm)|Bedazzling|Bedazzling|modificada|
|[hmpuTKDBrivG8uJz.htm](extinction-curse-bestiary-items/hmpuTKDBrivG8uJz.htm)|Sylvan Wine|Sylvan Wine|modificada|
|[HnvK1KfAAcXoF3Gq.htm](extinction-curse-bestiary-items/HnvK1KfAAcXoF3Gq.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[hoeJkPRGT9A1gfra.htm](extinction-curse-bestiary-items/hoeJkPRGT9A1gfra.htm)|Claw|Garra|modificada|
|[hoHQP1kFuvAU3pGi.htm](extinction-curse-bestiary-items/hoHQP1kFuvAU3pGi.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[hOlYawxI9XqrexmA.htm](extinction-curse-bestiary-items/hOlYawxI9XqrexmA.htm)|Fleet Performer|Pies ligeros Interpretar|modificada|
|[HorpW2NucpQNU3sV.htm](extinction-curse-bestiary-items/HorpW2NucpQNU3sV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HRNkLkrR4YcOavPi.htm](extinction-curse-bestiary-items/HRNkLkrR4YcOavPi.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[hrq73xXhTyT182d2.htm](extinction-curse-bestiary-items/hrq73xXhTyT182d2.htm)|Suppressed Alignment|Alineación Suprimida|modificada|
|[HtbPnLHYwn4NLjKm.htm](extinction-curse-bestiary-items/HtbPnLHYwn4NLjKm.htm)|Purple Worm Venom|Veneno de gusano púrpura|modificada|
|[HthRagLsoKlSBX0L.htm](extinction-curse-bestiary-items/HthRagLsoKlSBX0L.htm)|Sand Spear|Lanza de Arena|modificada|
|[HuhJDJ9P5pLj5Asb.htm](extinction-curse-bestiary-items/HuhJDJ9P5pLj5Asb.htm)|Claw|Garra|modificada|
|[hXMssBGUnnIgsqkV.htm](extinction-curse-bestiary-items/hXMssBGUnnIgsqkV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hz1IPPAcQC4UoSnA.htm](extinction-curse-bestiary-items/hz1IPPAcQC4UoSnA.htm)|Grab|Agarrado|modificada|
|[HZ3H7Z76Xxjqqoqw.htm](extinction-curse-bestiary-items/HZ3H7Z76Xxjqqoqw.htm)|Rapid Strikes|Golpes Rápidos|modificada|
|[I07i3Rw3iXrxS2A9.htm](extinction-curse-bestiary-items/I07i3Rw3iXrxS2A9.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[i0jem1LORXY0csEi.htm](extinction-curse-bestiary-items/i0jem1LORXY0csEi.htm)|Change Shape|Change Shape|modificada|
|[I3SJEMtzxGoUPoc7.htm](extinction-curse-bestiary-items/I3SJEMtzxGoUPoc7.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[i621hyfk7W2Zq4X5.htm](extinction-curse-bestiary-items/i621hyfk7W2Zq4X5.htm)|Demonic Strength|Fuerza demoníaca|modificada|
|[I7TcllhLdsgw39xY.htm](extinction-curse-bestiary-items/I7TcllhLdsgw39xY.htm)|Mancatcher|Mancatcher|modificada|
|[I8280Iq9dO7AtDc4.htm](extinction-curse-bestiary-items/I8280Iq9dO7AtDc4.htm)|Abyssal Plague|Plaga abisal|modificada|
|[I8blzGPi6zi9IEQ5.htm](extinction-curse-bestiary-items/I8blzGPi6zi9IEQ5.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[i9Y89elHLIhcIh43.htm](extinction-curse-bestiary-items/i9Y89elHLIhcIh43.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[iADRxNCeaT5m51vW.htm](extinction-curse-bestiary-items/iADRxNCeaT5m51vW.htm)|Children of the Night|Niños de la Noche|modificada|
|[iBalLMYNzfQfwcHn.htm](extinction-curse-bestiary-items/iBalLMYNzfQfwcHn.htm)|Herecite Deity|Herecite Deity|modificada|
|[IbgOSKVasX4WGvhx.htm](extinction-curse-bestiary-items/IbgOSKVasX4WGvhx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[iCJLRUtX82AUP977.htm](extinction-curse-bestiary-items/iCJLRUtX82AUP977.htm)|Roll Up|Roll Up|modificada|
|[iCMgd2Z2HKq0Ewg3.htm](extinction-curse-bestiary-items/iCMgd2Z2HKq0Ewg3.htm)|Claw|Garra|modificada|
|[ICQ6LdZTOXIh61jG.htm](extinction-curse-bestiary-items/ICQ6LdZTOXIh61jG.htm)|Negative Healing|Curación negativa|modificada|
|[ICsz8ij2XAYIvcoc.htm](extinction-curse-bestiary-items/ICsz8ij2XAYIvcoc.htm)|Warhammer|Warhammer|modificada|
|[iCvFkYwUNj1fDYM9.htm](extinction-curse-bestiary-items/iCvFkYwUNj1fDYM9.htm)|Convergent Link|Enlace Convergente|modificada|
|[iCxGTcQkrWZdAtUe.htm](extinction-curse-bestiary-items/iCxGTcQkrWZdAtUe.htm)|Ravenous Tracker|Rastreador Voraz|modificada|
|[IdhAmzNMMDraoy7q.htm](extinction-curse-bestiary-items/IdhAmzNMMDraoy7q.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[IeoEKCfC28dShDCr.htm](extinction-curse-bestiary-items/IeoEKCfC28dShDCr.htm)|Choke Slam|Choke Slam|modificada|
|[iEPnPUyNUxfYs63B.htm](extinction-curse-bestiary-items/iEPnPUyNUxfYs63B.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[ieZcxj746XMjV8LQ.htm](extinction-curse-bestiary-items/ieZcxj746XMjV8LQ.htm)|Jaws|Fauces|modificada|
|[iFh8asouVDS4zj2y.htm](extinction-curse-bestiary-items/iFh8asouVDS4zj2y.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[IFPnmQpUJLW9TJa9.htm](extinction-curse-bestiary-items/IFPnmQpUJLW9TJa9.htm)|Wail|Lamento|modificada|
|[iH7yVlvaFJF7bjNQ.htm](extinction-curse-bestiary-items/iH7yVlvaFJF7bjNQ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Ii6Er4G5VD71YDMV.htm](extinction-curse-bestiary-items/Ii6Er4G5VD71YDMV.htm)|Create Spawn|Elaborar Spawn|modificada|
|[IiFWeofaMoWPJb8k.htm](extinction-curse-bestiary-items/IiFWeofaMoWPJb8k.htm)|Opening Threat|Amenaza de Apertura|modificada|
|[iKOLPcoDilY7HLdL.htm](extinction-curse-bestiary-items/iKOLPcoDilY7HLdL.htm)|Mercy Vulnerability|Vulnerabilidad de Merced|modificada|
|[iKTJM59aw3XI1kNW.htm](extinction-curse-bestiary-items/iKTJM59aw3XI1kNW.htm)|Fist|Puño|modificada|
|[IKv1F31tfUJko4Kl.htm](extinction-curse-bestiary-items/IKv1F31tfUJko4Kl.htm)|Hoof|Hoof|modificada|
|[iMfJOLJk4i8ywGZb.htm](extinction-curse-bestiary-items/iMfJOLJk4i8ywGZb.htm)|Frightful Presence|Frightful Presence|modificada|
|[iO7f9ASpYCNIBid3.htm](extinction-curse-bestiary-items/iO7f9ASpYCNIBid3.htm)|Sanguine Rain|Sanguine Rain|modificada|
|[IOV4sVgze4V4qLcw.htm](extinction-curse-bestiary-items/IOV4sVgze4V4qLcw.htm)|Fist|Puño|modificada|
|[IQ3sd03wmGJr3aIw.htm](extinction-curse-bestiary-items/IQ3sd03wmGJr3aIw.htm)|Fist|Puño|modificada|
|[IryGUwPWgCMPmKR1.htm](extinction-curse-bestiary-items/IryGUwPWgCMPmKR1.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[issxBVRAEt4rjlKI.htm](extinction-curse-bestiary-items/issxBVRAEt4rjlKI.htm)|Flaming Baton|Flamígera|modificada|
|[iur53JebaExSunmH.htm](extinction-curse-bestiary-items/iur53JebaExSunmH.htm)|Iron Sword|Espada de Hierro|modificada|
|[IuSKKdURFPmkHFi8.htm](extinction-curse-bestiary-items/IuSKKdURFPmkHFi8.htm)|Grab|Agarrado|modificada|
|[IUZN9K8piSmzzwJE.htm](extinction-curse-bestiary-items/IUZN9K8piSmzzwJE.htm)|Skirmishing Movement|Movimiento de Escaramuza|modificada|
|[IvDxsg9WiiyQoCtb.htm](extinction-curse-bestiary-items/IvDxsg9WiiyQoCtb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[iWDeWv63BnPfzCqn.htm](extinction-curse-bestiary-items/iWDeWv63BnPfzCqn.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[IXhK0WImaHn472zp.htm](extinction-curse-bestiary-items/IXhK0WImaHn472zp.htm)|Ram|Ram|modificada|
|[iyWevLBiGLmYMOP5.htm](extinction-curse-bestiary-items/iyWevLBiGLmYMOP5.htm)|Frightful Presence|Frightful Presence|modificada|
|[IZ8Omnth78RPYDTU.htm](extinction-curse-bestiary-items/IZ8Omnth78RPYDTU.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[izc4ztOBcRsH7EKB.htm](extinction-curse-bestiary-items/izc4ztOBcRsH7EKB.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[iZhufeHVk6VtD2Mw.htm](extinction-curse-bestiary-items/iZhufeHVk6VtD2Mw.htm)|Jaws|Fauces|modificada|
|[j0wzmEVge78zbfkb.htm](extinction-curse-bestiary-items/j0wzmEVge78zbfkb.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[j1ofFhuO1mqyLeZV.htm](extinction-curse-bestiary-items/j1ofFhuO1mqyLeZV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j2hIRiY8Zn7Egfco.htm](extinction-curse-bestiary-items/j2hIRiY8Zn7Egfco.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[j7QVtuN08hfCGEUq.htm](extinction-curse-bestiary-items/j7QVtuN08hfCGEUq.htm)|Keen Eyes|Ojos Afilados|modificada|
|[J7YUxP9ma7sbY5rl.htm](extinction-curse-bestiary-items/J7YUxP9ma7sbY5rl.htm)|Claw|Garra|modificada|
|[jajMdZuaov0LtkBl.htm](extinction-curse-bestiary-items/jajMdZuaov0LtkBl.htm)|Call to Halt|Llamada a Alto|modificada|
|[jAotzs7DanRzRCVW.htm](extinction-curse-bestiary-items/jAotzs7DanRzRCVW.htm)|Claw|Garra|modificada|
|[jaYwPmKt8kxpFg9N.htm](extinction-curse-bestiary-items/jaYwPmKt8kxpFg9N.htm)|Feral Directive|Feral Directive|modificada|
|[jBd2Z17zgzpxyvDF.htm](extinction-curse-bestiary-items/jBd2Z17zgzpxyvDF.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[JbKAKCipLhCSyGfm.htm](extinction-curse-bestiary-items/JbKAKCipLhCSyGfm.htm)|Swallow Whole|Engullir Todo|modificada|
|[jBPbNNVD1lCQ5GZC.htm](extinction-curse-bestiary-items/jBPbNNVD1lCQ5GZC.htm)|Spittle|Escupitajo|modificada|
|[JBZGDHmgcSKsrjtc.htm](extinction-curse-bestiary-items/JBZGDHmgcSKsrjtc.htm)|Stand Still|Ponerse de pie|modificada|
|[jcxnj2Ps2GYGCILS.htm](extinction-curse-bestiary-items/jcxnj2Ps2GYGCILS.htm)|Sand Fist|Puño de Arena|modificada|
|[jD5e8vnSLE6rJyBA.htm](extinction-curse-bestiary-items/jD5e8vnSLE6rJyBA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jgEA3ptMljnGxy1j.htm](extinction-curse-bestiary-items/jgEA3ptMljnGxy1j.htm)|Forest Jaunt|Bosque Jaunt|modificada|
|[jGq0rfEKsdYFGYmW.htm](extinction-curse-bestiary-items/jGq0rfEKsdYFGYmW.htm)|Bounce-Bounce|Bounce-Bounce|modificada|
|[jiZ3qpQbCw5jBgSz.htm](extinction-curse-bestiary-items/jiZ3qpQbCw5jBgSz.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jk7KLiC4TT506huA.htm](extinction-curse-bestiary-items/jk7KLiC4TT506huA.htm)|Fist|Puño|modificada|
|[JlbjzKNAD6l6yzqs.htm](extinction-curse-bestiary-items/JlbjzKNAD6l6yzqs.htm)|Eat Anything|Eat Anything|modificada|
|[JLMrybXzf35bFGZ5.htm](extinction-curse-bestiary-items/JLMrybXzf35bFGZ5.htm)|Bloodsense (Precise) 30 feet|Bloodsense (Preciso) 30 pies|modificada|
|[jMeQhmA9WidXyyaB.htm](extinction-curse-bestiary-items/jMeQhmA9WidXyyaB.htm)|Churning Frenzy|Frenesí Agitador|modificada|
|[joVEQgf7vPuTLuTt.htm](extinction-curse-bestiary-items/joVEQgf7vPuTLuTt.htm)|+37 Will Save vs. Emotion Effects|+37 Salvación de Voluntad vs. Efectos de Emoción|modificada|
|[jovXFUyi4oe3ZNFR.htm](extinction-curse-bestiary-items/jovXFUyi4oe3ZNFR.htm)|Alchemist's Fire|Alchemist's Fire|modificada|
|[jp2sVLnIKEVPq8o6.htm](extinction-curse-bestiary-items/jp2sVLnIKEVPq8o6.htm)|Staff|Báculo|modificada|
|[jpECNxPropkVu94M.htm](extinction-curse-bestiary-items/jpECNxPropkVu94M.htm)|Mobility|Movilidad|modificada|
|[JPP8jkjRT4UlELJV.htm](extinction-curse-bestiary-items/JPP8jkjRT4UlELJV.htm)|Claw|Garra|modificada|
|[jQn2KwqwhEwFFaZA.htm](extinction-curse-bestiary-items/jQn2KwqwhEwFFaZA.htm)|Fist|Puño|modificada|
|[jrcPXASGsebozgzh.htm](extinction-curse-bestiary-items/jrcPXASGsebozgzh.htm)|Jaws|Fauces|modificada|
|[JrRQ3eTuGR50kL6B.htm](extinction-curse-bestiary-items/JrRQ3eTuGR50kL6B.htm)|Roll the Bones|Roll the Bones|modificada|
|[JsETPxZAqYOAGagW.htm](extinction-curse-bestiary-items/JsETPxZAqYOAGagW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[jSG9ZWWLJseQGg4X.htm](extinction-curse-bestiary-items/jSG9ZWWLJseQGg4X.htm)|Fist|Puño|modificada|
|[jsL5jhGaz2pctkeU.htm](extinction-curse-bestiary-items/jsL5jhGaz2pctkeU.htm)|Restrain|Neutralizado/a|modificada|
|[JTK8CW0TwOok1X9Z.htm](extinction-curse-bestiary-items/JTK8CW0TwOok1X9Z.htm)|Thunderstone Explosion|Explosión de Piedra Trueno|modificada|
|[JtY4GmPSaE1qxfgU.htm](extinction-curse-bestiary-items/JtY4GmPSaE1qxfgU.htm)|No Vision|No Vision|modificada|
|[JUAv4A165EW9vjcX.htm](extinction-curse-bestiary-items/JUAv4A165EW9vjcX.htm)|Constant Spells|Constant Spells|modificada|
|[jUDRlSsIxqf6G6h3.htm](extinction-curse-bestiary-items/jUDRlSsIxqf6G6h3.htm)|Drain Life|Drenar Vida|modificada|
|[JW7pj6pjhMG4D95l.htm](extinction-curse-bestiary-items/JW7pj6pjhMG4D95l.htm)|Residual Grease|Grasa residual|modificada|
|[jwBiXSOEkYsL5V2p.htm](extinction-curse-bestiary-items/jwBiXSOEkYsL5V2p.htm)|Sidestep|Paso lateral|modificada|
|[JWlYLXxlI4IRYbeF.htm](extinction-curse-bestiary-items/JWlYLXxlI4IRYbeF.htm)|Greatpick|Greatpick|modificada|
|[jwtvc92eNGbKi3wr.htm](extinction-curse-bestiary-items/jwtvc92eNGbKi3wr.htm)|Rhoka Sword|Espada Rhoka|modificada|
|[Jxth7CtaG4IyFCWC.htm](extinction-curse-bestiary-items/Jxth7CtaG4IyFCWC.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[JXwGkvpYAsrmdfj9.htm](extinction-curse-bestiary-items/JXwGkvpYAsrmdfj9.htm)|Beguile the Addled|Beguile the Addled|modificada|
|[Jz2nWIs5hfh5nWak.htm](extinction-curse-bestiary-items/Jz2nWIs5hfh5nWak.htm)|Powerful Stench|Hedor intenso|modificada|
|[JzIDoWltig01Ezsi.htm](extinction-curse-bestiary-items/JzIDoWltig01Ezsi.htm)|Fist|Puño|modificada|
|[k0JyHOPZZzOu7DCM.htm](extinction-curse-bestiary-items/k0JyHOPZZzOu7DCM.htm)|Fanatical Juggernaut|Juggernaut Fanático|modificada|
|[k0Pbu3xoSfQc9iEi.htm](extinction-curse-bestiary-items/k0Pbu3xoSfQc9iEi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[k1bxsxdo1I2iTOLW.htm](extinction-curse-bestiary-items/k1bxsxdo1I2iTOLW.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[k1Ehx0dobB1JSYjA.htm](extinction-curse-bestiary-items/k1Ehx0dobB1JSYjA.htm)|Swift Leap|Salto veloz|modificada|
|[k1jWhjh5eXKkavLa.htm](extinction-curse-bestiary-items/k1jWhjh5eXKkavLa.htm)|Frightful Presence|Frightful Presence|modificada|
|[K6KEcVCjAgpkCZuv.htm](extinction-curse-bestiary-items/K6KEcVCjAgpkCZuv.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[k9lNa3pfAPN99oJd.htm](extinction-curse-bestiary-items/k9lNa3pfAPN99oJd.htm)|Convergent Tactics|Tácticas Convergentes|modificada|
|[kBizs5Rm1STeq41l.htm](extinction-curse-bestiary-items/kBizs5Rm1STeq41l.htm)|Aura of Smoke|Aura de Humo|modificada|
|[kCl7ix3UfVgsJxQ8.htm](extinction-curse-bestiary-items/kCl7ix3UfVgsJxQ8.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[KczcQ2zt8BqH8GXB.htm](extinction-curse-bestiary-items/KczcQ2zt8BqH8GXB.htm)|Enraging Stench|Hedor Enfurecedor|modificada|
|[KdGsDBgPGKlQjwFc.htm](extinction-curse-bestiary-items/KdGsDBgPGKlQjwFc.htm)|Trample|Trample|modificada|
|[KeuZBLboNz2Rx0ou.htm](extinction-curse-bestiary-items/KeuZBLboNz2Rx0ou.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[kGDDuORhStpjLtdf.htm](extinction-curse-bestiary-items/kGDDuORhStpjLtdf.htm)|Jaws|Fauces|modificada|
|[kgOx9c22i9ZSZt40.htm](extinction-curse-bestiary-items/kgOx9c22i9ZSZt40.htm)|Negative Healing|Curación negativa|modificada|
|[kHgE08pNOtxoY0UO.htm](extinction-curse-bestiary-items/kHgE08pNOtxoY0UO.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[kHpIOS55WOLGDdil.htm](extinction-curse-bestiary-items/kHpIOS55WOLGDdil.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[kiapf7DQYgbxC5NC.htm](extinction-curse-bestiary-items/kiapf7DQYgbxC5NC.htm)|Frightful Presence|Frightful Presence|modificada|
|[kifvfBeEd0qFnfEN.htm](extinction-curse-bestiary-items/kifvfBeEd0qFnfEN.htm)|Constant Spells|Constant Spells|modificada|
|[Kke4izO3OxCZuJm6.htm](extinction-curse-bestiary-items/Kke4izO3OxCZuJm6.htm)|Grab|Agarrado|modificada|
|[KkvACSDVhU76AEdA.htm](extinction-curse-bestiary-items/KkvACSDVhU76AEdA.htm)|Negative Healing|Curación negativa|modificada|
|[KLJhKMXJejeXccOV.htm](extinction-curse-bestiary-items/KLJhKMXJejeXccOV.htm)|Jaws|Fauces|modificada|
|[km1pCQZC65kAx4WY.htm](extinction-curse-bestiary-items/km1pCQZC65kAx4WY.htm)|Bastard Sword|Espada Bastarda|modificada|
|[kmD7smfQheBtKJZb.htm](extinction-curse-bestiary-items/kmD7smfQheBtKJZb.htm)|Fist|Puño|modificada|
|[kNi8uFBuD1YHDFE8.htm](extinction-curse-bestiary-items/kNi8uFBuD1YHDFE8.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Kp2VQoJVTR9vUiFM.htm](extinction-curse-bestiary-items/Kp2VQoJVTR9vUiFM.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[kqKZCfaYLS8DSSjM.htm](extinction-curse-bestiary-items/kqKZCfaYLS8DSSjM.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[KqUSudmO5lFvURLO.htm](extinction-curse-bestiary-items/KqUSudmO5lFvURLO.htm)|Sting of the Lash|Sting of the Lash|modificada|
|[krSzk0G6TRACGHQK.htm](extinction-curse-bestiary-items/krSzk0G6TRACGHQK.htm)|Stench|Hedor|modificada|
|[KsOqiokf6euChMn3.htm](extinction-curse-bestiary-items/KsOqiokf6euChMn3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[KTuygUzynAd3MJFg.htm](extinction-curse-bestiary-items/KTuygUzynAd3MJFg.htm)|Jaws|Fauces|modificada|
|[KUODkAXkx2zLQ2SC.htm](extinction-curse-bestiary-items/KUODkAXkx2zLQ2SC.htm)|Jaws|Fauces|modificada|
|[KVrMIzW0i6AHJQvS.htm](extinction-curse-bestiary-items/KVrMIzW0i6AHJQvS.htm)|The Maze Awakens|El laberinto despierta|modificada|
|[Kw8WsCyXZ9AnshFU.htm](extinction-curse-bestiary-items/Kw8WsCyXZ9AnshFU.htm)|Coward Sense (Imprecise) 60 feet|Sentido Cobarde (Impreciso) 60 pies|modificada|
|[KwKdT0U7saZ9u8qc.htm](extinction-curse-bestiary-items/KwKdT0U7saZ9u8qc.htm)|Roiling Mind|Roiling Mind|modificada|
|[kwNOg7TY9zGl3ju9.htm](extinction-curse-bestiary-items/kwNOg7TY9zGl3ju9.htm)|Constant Spells|Constant Spells|modificada|
|[kWxHjxdCg01V9cxk.htm](extinction-curse-bestiary-items/kWxHjxdCg01V9cxk.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kXtQUn28pNXI8ZAF.htm](extinction-curse-bestiary-items/kXtQUn28pNXI8ZAF.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[KYxBpk7xiZXkkbFH.htm](extinction-curse-bestiary-items/KYxBpk7xiZXkkbFH.htm)|Spear|Lanza|modificada|
|[L0Lzy1bN4NABxIgx.htm](extinction-curse-bestiary-items/L0Lzy1bN4NABxIgx.htm)|Evasion|Evasión|modificada|
|[L7PvaYLhErs0eOxg.htm](extinction-curse-bestiary-items/L7PvaYLhErs0eOxg.htm)|Wicked Bite|Muerdemuerde|modificada|
|[L8YJHX8v6mfDBrMC.htm](extinction-curse-bestiary-items/L8YJHX8v6mfDBrMC.htm)|Spear|Lanza|modificada|
|[l94suSMqUEuJilRY.htm](extinction-curse-bestiary-items/l94suSMqUEuJilRY.htm)|Vengeful Presence|Presencia vengativa|modificada|
|[L9otISXKarqdQVqY.htm](extinction-curse-bestiary-items/L9otISXKarqdQVqY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lajGLW4hoWGw1JCP.htm](extinction-curse-bestiary-items/lajGLW4hoWGw1JCP.htm)|Grab|Agarrado|modificada|
|[lB6Yf4JEFM6O42nb.htm](extinction-curse-bestiary-items/lB6Yf4JEFM6O42nb.htm)|Awakened|Awakened|modificada|
|[Ld3LrV6FM6WRbeYH.htm](extinction-curse-bestiary-items/Ld3LrV6FM6WRbeYH.htm)|Jaws|Fauces|modificada|
|[lDjbS8x4P6y41QbW.htm](extinction-curse-bestiary-items/lDjbS8x4P6y41QbW.htm)|Gout of Acid|Gota de Ácido|modificada|
|[LDnG8zvyqote8tTp.htm](extinction-curse-bestiary-items/LDnG8zvyqote8tTp.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[LE0JjeyWLVVvqtCV.htm](extinction-curse-bestiary-items/LE0JjeyWLVVvqtCV.htm)|Naginata|Naginata|modificada|
|[lFbZsm0qVAkyKv1O.htm](extinction-curse-bestiary-items/lFbZsm0qVAkyKv1O.htm)|Vulnerable to Sunlight|Vulnerabilidad a la luz del sol|modificada|
|[lFWZDfozTLjw9c8x.htm](extinction-curse-bestiary-items/lFWZDfozTLjw9c8x.htm)|Heart-Seeking Strike|Golpe que Busca el Corazón|modificada|
|[lGlw9snRuf39t1H3.htm](extinction-curse-bestiary-items/lGlw9snRuf39t1H3.htm)|Grab|Agarrado|modificada|
|[lhfpnOLNlbRjtgBB.htm](extinction-curse-bestiary-items/lhfpnOLNlbRjtgBB.htm)|Claw|Garra|modificada|
|[lHtukJ46NfLrTp0w.htm](extinction-curse-bestiary-items/lHtukJ46NfLrTp0w.htm)|Grab|Agarrado|modificada|
|[LHyFqz22wgaucwGA.htm](extinction-curse-bestiary-items/LHyFqz22wgaucwGA.htm)|Jaws|Fauces|modificada|
|[LI5f4Jm5rjlsXN34.htm](extinction-curse-bestiary-items/LI5f4Jm5rjlsXN34.htm)|Petrification Overcharge|Sobrecarga de petrificación|modificada|
|[Ll1mko5i4mBq60YH.htm](extinction-curse-bestiary-items/Ll1mko5i4mBq60YH.htm)|Embrace of Death|Abrazo de la Muerte|modificada|
|[lMFrsrHGqi1hl36L.htm](extinction-curse-bestiary-items/lMFrsrHGqi1hl36L.htm)|Rattling Blow|Rattling Blow|modificada|
|[lmrBUipZFBXl2NgJ.htm](extinction-curse-bestiary-items/lmrBUipZFBXl2NgJ.htm)|Claw|Garra|modificada|
|[ln65krKkJthepPkH.htm](extinction-curse-bestiary-items/ln65krKkJthepPkH.htm)|Tail|Tail|modificada|
|[lnChTLzpk1PEBJ0E.htm](extinction-curse-bestiary-items/lnChTLzpk1PEBJ0E.htm)|Caustic Dart|Dardo cáustico|modificada|
|[LnqJSEHKMH6xbkRx.htm](extinction-curse-bestiary-items/LnqJSEHKMH6xbkRx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[lO6FqcK8sAscEaQN.htm](extinction-curse-bestiary-items/lO6FqcK8sAscEaQN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lp862sClvRojxe2C.htm](extinction-curse-bestiary-items/lp862sClvRojxe2C.htm)|Phantom Sermon|Sermón Fantasma|modificada|
|[LqrQ3Kv7BGLa9Qyo.htm](extinction-curse-bestiary-items/LqrQ3Kv7BGLa9Qyo.htm)|Negative Healing|Curación negativa|modificada|
|[LRKpdpQoppWvEcLr.htm](extinction-curse-bestiary-items/LRKpdpQoppWvEcLr.htm)|Site Bound|Ligado a una ubicación|modificada|
|[ltmniMbuhHmyNIeh.htm](extinction-curse-bestiary-items/ltmniMbuhHmyNIeh.htm)|Spear|Lanza|modificada|
|[ltyVtPkBGAreDacY.htm](extinction-curse-bestiary-items/ltyVtPkBGAreDacY.htm)|Suck Blood|Chupar Sangre|modificada|
|[lu7LiJ9o5N69xKMd.htm](extinction-curse-bestiary-items/lu7LiJ9o5N69xKMd.htm)|Spear|Lanza|modificada|
|[LURnplPbJOwNAozj.htm](extinction-curse-bestiary-items/LURnplPbJOwNAozj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[luzb0P0GAIgFEHxS.htm](extinction-curse-bestiary-items/luzb0P0GAIgFEHxS.htm)|Curse of Defiled Idols|Maldición de los ídolos profanados|modificada|
|[LVECyCtAv2DesAxX.htm](extinction-curse-bestiary-items/LVECyCtAv2DesAxX.htm)|Headbutt|Headbutt|modificada|
|[LvMFhf8LPHSHd6Jd.htm](extinction-curse-bestiary-items/LvMFhf8LPHSHd6Jd.htm)|Claw|Garra|modificada|
|[lwnO6Iuybc8JncAu.htm](extinction-curse-bestiary-items/lwnO6Iuybc8JncAu.htm)|Negative Healing|Curación negativa|modificada|
|[lWRG2NAbjrUze1M2.htm](extinction-curse-bestiary-items/lWRG2NAbjrUze1M2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LxPQprD4lkZ2Wjc9.htm](extinction-curse-bestiary-items/LxPQprD4lkZ2Wjc9.htm)|Grand Finale|Grand Finale|modificada|
|[LyJY1n9lgwAKOFk7.htm](extinction-curse-bestiary-items/LyJY1n9lgwAKOFk7.htm)|Xulgath Bile|Xulgath Bile|modificada|
|[lyKcDYNprlsFkk9e.htm](extinction-curse-bestiary-items/lyKcDYNprlsFkk9e.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[lYkQae8WS94MiKJn.htm](extinction-curse-bestiary-items/lYkQae8WS94MiKJn.htm)|Wight Spawn|Wight Spawn|modificada|
|[LzStFZ3TQx9YYYe4.htm](extinction-curse-bestiary-items/LzStFZ3TQx9YYYe4.htm)|Spear|Lanza|modificada|
|[M07XMDmE5qHjyRuU.htm](extinction-curse-bestiary-items/M07XMDmE5qHjyRuU.htm)|Minion|Esbirro|modificada|
|[m0wrjjZLBjDz3Ona.htm](extinction-curse-bestiary-items/m0wrjjZLBjDz3Ona.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[m1KBPeMqrNwEMxby.htm](extinction-curse-bestiary-items/m1KBPeMqrNwEMxby.htm)|Aggressive Block|Bloqueo agresivo|modificada|
|[M203mlcBNFvAqyIJ.htm](extinction-curse-bestiary-items/M203mlcBNFvAqyIJ.htm)|Burn Alive|Burn Alive|modificada|
|[m716szn1fHz5unyk.htm](extinction-curse-bestiary-items/m716szn1fHz5unyk.htm)|Tentacle Assault|Tentacle Assault|modificada|
|[mB66uu8XN9drrr9S.htm](extinction-curse-bestiary-items/mB66uu8XN9drrr9S.htm)|Cleaver|Cleaver|modificada|
|[mC8ZXIDyGQCCGvee.htm](extinction-curse-bestiary-items/mC8ZXIDyGQCCGvee.htm)|Jaws|Fauces|modificada|
|[MCrT1usrhDxwMWCc.htm](extinction-curse-bestiary-items/MCrT1usrhDxwMWCc.htm)|Engulf|Envolver|modificada|
|[mDgoSp5xBF4354YD.htm](extinction-curse-bestiary-items/mDgoSp5xBF4354YD.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[mdOM5OxBjVA6iPqa.htm](extinction-curse-bestiary-items/mdOM5OxBjVA6iPqa.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[mDxntBa1Fvx5511p.htm](extinction-curse-bestiary-items/mDxntBa1Fvx5511p.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[MEvSBSRMvbzou7sj.htm](extinction-curse-bestiary-items/MEvSBSRMvbzou7sj.htm)|Dart Barrage|Dart Barrage|modificada|
|[mEzBPI4zxWLerwvt.htm](extinction-curse-bestiary-items/mEzBPI4zxWLerwvt.htm)|Tail|Tail|modificada|
|[mezWuI2AxifmZTwP.htm](extinction-curse-bestiary-items/mezWuI2AxifmZTwP.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[MFsAqLtYbroqBTeA.htm](extinction-curse-bestiary-items/MFsAqLtYbroqBTeA.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[MGk6Sbt5QtkGL8k3.htm](extinction-curse-bestiary-items/MGk6Sbt5QtkGL8k3.htm)|Claw|Garra|modificada|
|[MHRF7WqhFfgOn6ID.htm](extinction-curse-bestiary-items/MHRF7WqhFfgOn6ID.htm)|Beak|Beak|modificada|
|[miu3o0Zxz1ZBSRci.htm](extinction-curse-bestiary-items/miu3o0Zxz1ZBSRci.htm)|Sneak Savant|Sabio de la furtividad|modificada|
|[mJ6Xt01ZdOkIrWw2.htm](extinction-curse-bestiary-items/mJ6Xt01ZdOkIrWw2.htm)|Negative Healing|Curación negativa|modificada|
|[MJeOGWYLpgDMSTo4.htm](extinction-curse-bestiary-items/MJeOGWYLpgDMSTo4.htm)|Frost Vial (Greater) (Infused)|Vial de escarcha (mayor) (infundido).|modificada|
|[mK3VIhZzqlLlHNtN.htm](extinction-curse-bestiary-items/mK3VIhZzqlLlHNtN.htm)|Writhe Independently|Writhe Independently|modificada|
|[mnxX8zV7Y41pDoeS.htm](extinction-curse-bestiary-items/mnxX8zV7Y41pDoeS.htm)|Hasty Sacrifice|Hasty Sacrifice|modificada|
|[mOgh0bLWwjU5JzRo.htm](extinction-curse-bestiary-items/mOgh0bLWwjU5JzRo.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[MoGzK6NqeB1NQ13G.htm](extinction-curse-bestiary-items/MoGzK6NqeB1NQ13G.htm)|Hungering Web|Telara hambrienta|modificada|
|[Mq5ICRhJZYQxQMyX.htm](extinction-curse-bestiary-items/Mq5ICRhJZYQxQMyX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[mQ8k4znou6fvkiXP.htm](extinction-curse-bestiary-items/mQ8k4znou6fvkiXP.htm)|Guiding Finish|Remate orientador|modificada|
|[MQLisfH68V3Wk7Bg.htm](extinction-curse-bestiary-items/MQLisfH68V3Wk7Bg.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[MSrCpe3iS4MlI9Or.htm](extinction-curse-bestiary-items/MSrCpe3iS4MlI9Or.htm)|Enervating Tug|Remolcador Enervación|modificada|
|[MTJ6P2pU1320fO7f.htm](extinction-curse-bestiary-items/MTJ6P2pU1320fO7f.htm)|Berserk|Bersérker|modificada|
|[mtxgvU7yVCk7qxpV.htm](extinction-curse-bestiary-items/mtxgvU7yVCk7qxpV.htm)|Command an Animal|Comandar a un animal|modificada|
|[MvMDJ7fMPk4NO6bP.htm](extinction-curse-bestiary-items/MvMDJ7fMPk4NO6bP.htm)|Pounce|Abalanzarse|modificada|
|[MWMoiiaCQn1JlcdB.htm](extinction-curse-bestiary-items/MWMoiiaCQn1JlcdB.htm)|Change Shape|Change Shape|modificada|
|[mXs0X782H3FV7qJD.htm](extinction-curse-bestiary-items/mXs0X782H3FV7qJD.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[mxYaxKXMJuFXTuBN.htm](extinction-curse-bestiary-items/mxYaxKXMJuFXTuBN.htm)|Claw|Garra|modificada|
|[mY4dvHb9fAotqjkc.htm](extinction-curse-bestiary-items/mY4dvHb9fAotqjkc.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[MyPNYt7AI7qWY6wW.htm](extinction-curse-bestiary-items/MyPNYt7AI7qWY6wW.htm)|Alchemical Chambers|Compartimentos alquímicos|modificada|
|[MzlHAoedA1ZLzlQi.htm](extinction-curse-bestiary-items/MzlHAoedA1ZLzlQi.htm)|Tail|Tail|modificada|
|[N0g5xdIF3LC3JDxP.htm](extinction-curse-bestiary-items/N0g5xdIF3LC3JDxP.htm)|Alchemical Rupture|Fractura alquímica|modificada|
|[n0pHuo2H6DjU4MBo.htm](extinction-curse-bestiary-items/n0pHuo2H6DjU4MBo.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[n0wujzukuAjn0imB.htm](extinction-curse-bestiary-items/n0wujzukuAjn0imB.htm)|Dig Quickly|Excava Rápidamente|modificada|
|[n178y9YC0VdwlUrF.htm](extinction-curse-bestiary-items/n178y9YC0VdwlUrF.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[N1mvyhPLJjZvHzqX.htm](extinction-curse-bestiary-items/N1mvyhPLJjZvHzqX.htm)|Rumbling Rocks|Rumbling Rocks|modificada|
|[NAIEy57GDQ8NKq2y.htm](extinction-curse-bestiary-items/NAIEy57GDQ8NKq2y.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NBPD4xc1GPzZK1eR.htm](extinction-curse-bestiary-items/NBPD4xc1GPzZK1eR.htm)|Rhoka Sword|Espada Rhoka|modificada|
|[nCMY5ROefFFtABDw.htm](extinction-curse-bestiary-items/nCMY5ROefFFtABDw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nDFUQPOK3ZLFR63i.htm](extinction-curse-bestiary-items/nDFUQPOK3ZLFR63i.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[nDQsWnkhOjMNedYF.htm](extinction-curse-bestiary-items/nDQsWnkhOjMNedYF.htm)|Change Shape|Change Shape|modificada|
|[neAhXUkNh8UEeJcf.htm](extinction-curse-bestiary-items/neAhXUkNh8UEeJcf.htm)|Claw|Garra|modificada|
|[Neq70NbSJ4xaMcQZ.htm](extinction-curse-bestiary-items/Neq70NbSJ4xaMcQZ.htm)|Axiomatic Polymorph|Axiomática Polimorfismo|modificada|
|[NHf4kQKVaYzotrz9.htm](extinction-curse-bestiary-items/NHf4kQKVaYzotrz9.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[nhKkjCmqXzikihGp.htm](extinction-curse-bestiary-items/nhKkjCmqXzikihGp.htm)|Grab|Agarrado|modificada|
|[nIf5o2VCB8hURo6S.htm](extinction-curse-bestiary-items/nIf5o2VCB8hURo6S.htm)|Trident|Trident|modificada|
|[NjJyrUWQep7Xf1ap.htm](extinction-curse-bestiary-items/NjJyrUWQep7Xf1ap.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[nKJryW0RpvxmB4hH.htm](extinction-curse-bestiary-items/nKJryW0RpvxmB4hH.htm)|Striking Fear|Golpe de Miedo|modificada|
|[NKs8hoGb4IyfFWCw.htm](extinction-curse-bestiary-items/NKs8hoGb4IyfFWCw.htm)|Vengeful Spite|Rencor vengativo|modificada|
|[nlC81yf92sWZGQNO.htm](extinction-curse-bestiary-items/nlC81yf92sWZGQNO.htm)|Sharp Eyes|Sharp Eyes|modificada|
|[nlKm3K7VC3GevqjC.htm](extinction-curse-bestiary-items/nlKm3K7VC3GevqjC.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[NlM6fsKFpKz5nVMX.htm](extinction-curse-bestiary-items/NlM6fsKFpKz5nVMX.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[nN899egloBXvGHGV.htm](extinction-curse-bestiary-items/nN899egloBXvGHGV.htm)|Assault the Soul|Asalto al Alma|modificada|
|[nNzZIeP9wu8KlZFp.htm](extinction-curse-bestiary-items/nNzZIeP9wu8KlZFp.htm)|Seawater Retch|Retorcimiento de agua de mar|modificada|
|[NoasXhnYfse9eg51.htm](extinction-curse-bestiary-items/NoasXhnYfse9eg51.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[NrFxf3psvDqHyWQv.htm](extinction-curse-bestiary-items/NrFxf3psvDqHyWQv.htm)|Ghost Crystal Cloud|Nube de Cristal Fantasma|modificada|
|[nrhu2A276xievQyN.htm](extinction-curse-bestiary-items/nrhu2A276xievQyN.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[NSJZXBcVLdrnmMBh.htm](extinction-curse-bestiary-items/NSJZXBcVLdrnmMBh.htm)|Focused Assault|Asalto concentrado|modificada|
|[nSWM6vrnzV4iaYnK.htm](extinction-curse-bestiary-items/nSWM6vrnzV4iaYnK.htm)|Tusk|Tusk|modificada|
|[nTRfMlXAqXK1i2VK.htm](extinction-curse-bestiary-items/nTRfMlXAqXK1i2VK.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nWG8oxYnQkgZ85MI.htm](extinction-curse-bestiary-items/nWG8oxYnQkgZ85MI.htm)|Emotional Frenzy|Emotional Frenzy|modificada|
|[nxmZVcQPHk28sOnW.htm](extinction-curse-bestiary-items/nxmZVcQPHk28sOnW.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[NXWA3YnWW3T52ycS.htm](extinction-curse-bestiary-items/NXWA3YnWW3T52ycS.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nyPjPPwEThfAJoow.htm](extinction-curse-bestiary-items/nyPjPPwEThfAJoow.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[NZ4q9cv0qu2zI3NC.htm](extinction-curse-bestiary-items/NZ4q9cv0qu2zI3NC.htm)|Dueling Riposte|Réplica de duelo|modificada|
|[o1Dwspcc5Yryf7By.htm](extinction-curse-bestiary-items/o1Dwspcc5Yryf7By.htm)|Devour Soul|Devorar alma|modificada|
|[o204NA9EFmplkmbF.htm](extinction-curse-bestiary-items/o204NA9EFmplkmbF.htm)|Motion Sense (60 feet)|Sentido del movimiento (60 pies)|modificada|
|[O2vFMtCMKiP3cEqw.htm](extinction-curse-bestiary-items/O2vFMtCMKiP3cEqw.htm)|Claw|Garra|modificada|
|[O3GuY1OWhHhmq9FW.htm](extinction-curse-bestiary-items/O3GuY1OWhHhmq9FW.htm)|Suck Blood|Chupar Sangre|modificada|
|[o59thQeI8g01OAvC.htm](extinction-curse-bestiary-items/o59thQeI8g01OAvC.htm)|Hungry Spear|Lanza hambrienta|modificada|
|[O5By0tAUVvL66vcN.htm](extinction-curse-bestiary-items/O5By0tAUVvL66vcN.htm)|Evasion|Evasión|modificada|
|[o5oVycGSBlCtwNU9.htm](extinction-curse-bestiary-items/o5oVycGSBlCtwNU9.htm)|+2 Circumstance to Fortitude and Reflex vs. Shove or Trip|+2 Circunstancia a Fortaleza y Reflejos contra Empujar o Derribar.|modificada|
|[O6ibunfTGnutfemd.htm](extinction-curse-bestiary-items/O6ibunfTGnutfemd.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[o7xu8uge6T0IT23h.htm](extinction-curse-bestiary-items/o7xu8uge6T0IT23h.htm)|Hypnotic Stench|Hedor Hipnótico|modificada|
|[O8uuBNiuKfPW8eyF.htm](extinction-curse-bestiary-items/O8uuBNiuKfPW8eyF.htm)|Whip|Látigo|modificada|
|[oAogJcqTK6rECpGp.htm](extinction-curse-bestiary-items/oAogJcqTK6rECpGp.htm)|Dire Warning|Dire Warning|modificada|
|[oByC6W1h6qcY4eXg.htm](extinction-curse-bestiary-items/oByC6W1h6qcY4eXg.htm)|Deny Advantage|Denegar ventaja|modificada|
|[OcYo8NrM7fSjcymh.htm](extinction-curse-bestiary-items/OcYo8NrM7fSjcymh.htm)|Claw|Garra|modificada|
|[Od9QuV4TEmA9vlH5.htm](extinction-curse-bestiary-items/Od9QuV4TEmA9vlH5.htm)|Black Seed Cloud|Nube de semillas negras|modificada|
|[odG6wvcX6jYyHa7t.htm](extinction-curse-bestiary-items/odG6wvcX6jYyHa7t.htm)|Punishing Tail|Coletazo castigador|modificada|
|[oDVz4ipascXAYmri.htm](extinction-curse-bestiary-items/oDVz4ipascXAYmri.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[oe15nn62zh9y60Wn.htm](extinction-curse-bestiary-items/oe15nn62zh9y60Wn.htm)|Smoking Wound|Herida Hiriente|modificada|
|[OE99dZLqJ6NlpBqw.htm](extinction-curse-bestiary-items/OE99dZLqJ6NlpBqw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oeenNOAvMw2BAQWV.htm](extinction-curse-bestiary-items/oeenNOAvMw2BAQWV.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[OeYHa0iu7IPcFWFH.htm](extinction-curse-bestiary-items/OeYHa0iu7IPcFWFH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OfDoBC89FG7NjSPu.htm](extinction-curse-bestiary-items/OfDoBC89FG7NjSPu.htm)|Poisoned Thorns|Poisoned Thorns|modificada|
|[OFM7aCfqlQVWMVNo.htm](extinction-curse-bestiary-items/OFM7aCfqlQVWMVNo.htm)|Claw|Garra|modificada|
|[OGUXwEjvyda2Jgfs.htm](extinction-curse-bestiary-items/OGUXwEjvyda2Jgfs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ohdTtVsfQICponGx.htm](extinction-curse-bestiary-items/ohdTtVsfQICponGx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oKMZi1P2n9vQ9SvK.htm](extinction-curse-bestiary-items/oKMZi1P2n9vQ9SvK.htm)|Psychic Sip|Psychic Sip|modificada|
|[OkSGq1Wy2ceTrxFS.htm](extinction-curse-bestiary-items/OkSGq1Wy2ceTrxFS.htm)|Pseudopod|Pseudópodo|modificada|
|[oKvWxPQ1VdadXlOL.htm](extinction-curse-bestiary-items/oKvWxPQ1VdadXlOL.htm)|Cabal|Cabal|modificada|
|[oLLRMvLu25gNhBdu.htm](extinction-curse-bestiary-items/oLLRMvLu25gNhBdu.htm)|Dagger|Daga|modificada|
|[oLMdVdJKbDXXxlbp.htm](extinction-curse-bestiary-items/oLMdVdJKbDXXxlbp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OLmfUY8tRGHs6zpa.htm](extinction-curse-bestiary-items/OLmfUY8tRGHs6zpa.htm)|Divert Thoughts|Desviar pensamientos|modificada|
|[oM6jqTPLCdRYP1WX.htm](extinction-curse-bestiary-items/oM6jqTPLCdRYP1WX.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[om85FhVgReT8plPu.htm](extinction-curse-bestiary-items/om85FhVgReT8plPu.htm)|Feral Directive|Feral Directive|modificada|
|[omGN70P6v81nFafa.htm](extinction-curse-bestiary-items/omGN70P6v81nFafa.htm)|Emotional Focus|Enfoque Emocional|modificada|
|[ONJaqLmNDjXmPpES.htm](extinction-curse-bestiary-items/ONJaqLmNDjXmPpES.htm)|Sickle|Hoz|modificada|
|[OoEHiY3SG02Ny7kr.htm](extinction-curse-bestiary-items/OoEHiY3SG02Ny7kr.htm)|Antler|Antler|modificada|
|[OpIksuLcYybF2H6o.htm](extinction-curse-bestiary-items/OpIksuLcYybF2H6o.htm)|Putrid Blast|Putrid Blast|modificada|
|[OpULqY48Og5zS0iV.htm](extinction-curse-bestiary-items/OpULqY48Og5zS0iV.htm)|Mounted Defense|Defensa Montada|modificada|
|[oQVUQYLZSbOzdbRx.htm](extinction-curse-bestiary-items/oQVUQYLZSbOzdbRx.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[OQX9XEhfUYIiPSNz.htm](extinction-curse-bestiary-items/OQX9XEhfUYIiPSNz.htm)|Hex of the Bloody Thief|Maleficio del ladrón sangriento|modificada|
|[ordT6deii6Tl80qq.htm](extinction-curse-bestiary-items/ordT6deii6Tl80qq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ORzFTDNAhTrNzkq6.htm](extinction-curse-bestiary-items/ORzFTDNAhTrNzkq6.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[OS3Ccca6DLGDr67x.htm](extinction-curse-bestiary-items/OS3Ccca6DLGDr67x.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[oSaKLB4trA23IqwP.htm](extinction-curse-bestiary-items/oSaKLB4trA23IqwP.htm)|Shield Block|Bloquear con escudo|modificada|
|[oTDnAl5JS81qlDYh.htm](extinction-curse-bestiary-items/oTDnAl5JS81qlDYh.htm)|Shortsword|Espada corta|modificada|
|[OthdN7VZZ2OhjTnY.htm](extinction-curse-bestiary-items/OthdN7VZZ2OhjTnY.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OtJ0BMN8Z5o0eMdr.htm](extinction-curse-bestiary-items/OtJ0BMN8Z5o0eMdr.htm)|Rapier|Estoque|modificada|
|[oTqwsUwuBzfzn5t4.htm](extinction-curse-bestiary-items/oTqwsUwuBzfzn5t4.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Ou7j9iRSTfEmOPJq.htm](extinction-curse-bestiary-items/Ou7j9iRSTfEmOPJq.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[oUCSylGhNS9ioPWO.htm](extinction-curse-bestiary-items/oUCSylGhNS9ioPWO.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[OukSsGFoINYIbHD9.htm](extinction-curse-bestiary-items/OukSsGFoINYIbHD9.htm)|Trample|Trample|modificada|
|[ouztmCgjgV65kMJ4.htm](extinction-curse-bestiary-items/ouztmCgjgV65kMJ4.htm)|Antler Toss|Antler Toss|modificada|
|[oV6jmBptcFGgbNXA.htm](extinction-curse-bestiary-items/oV6jmBptcFGgbNXA.htm)|Reach Spell|Conjuro de alcance|modificada|
|[OwAhtu9K9pfUPHun.htm](extinction-curse-bestiary-items/OwAhtu9K9pfUPHun.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[OWuc4hzQaHKQ4cO0.htm](extinction-curse-bestiary-items/OWuc4hzQaHKQ4cO0.htm)|Constant Spells|Constant Spells|modificada|
|[oxKLVBMpW1qrYgZH.htm](extinction-curse-bestiary-items/oxKLVBMpW1qrYgZH.htm)|Hallucinogenic Pollen|Polen alucinógeno|modificada|
|[OYivzyUv9rlanxFD.htm](extinction-curse-bestiary-items/OYivzyUv9rlanxFD.htm)|Blade|Blade|modificada|
|[oZtIJY1LCleBd0XM.htm](extinction-curse-bestiary-items/oZtIJY1LCleBd0XM.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[oZzczpHFxrH5IUJq.htm](extinction-curse-bestiary-items/oZzczpHFxrH5IUJq.htm)|Roar|Roar|modificada|
|[p1OayYNJswICCP7g.htm](extinction-curse-bestiary-items/p1OayYNJswICCP7g.htm)|Jaws|Fauces|modificada|
|[P55ThVkZipK0fIVo.htm](extinction-curse-bestiary-items/P55ThVkZipK0fIVo.htm)|Uncanny Divination|Adivinación misteriosa|modificada|
|[P6IbJGGscBwb0Kuy.htm](extinction-curse-bestiary-items/P6IbJGGscBwb0Kuy.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[p7TxyyKhv9w7s5Xp.htm](extinction-curse-bestiary-items/p7TxyyKhv9w7s5Xp.htm)|Beguiling Gaze|Beguiling Gaze|modificada|
|[P85ToDPnYlaZWUq4.htm](extinction-curse-bestiary-items/P85ToDPnYlaZWUq4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PA1pNOAVXRh7cuhC.htm](extinction-curse-bestiary-items/PA1pNOAVXRh7cuhC.htm)|Negative Healing|Curación negativa|modificada|
|[pAMOEkL6TdnNLhqQ.htm](extinction-curse-bestiary-items/pAMOEkL6TdnNLhqQ.htm)|Rend|Rasgadura|modificada|
|[pBAPuoNaSyHoRG4V.htm](extinction-curse-bestiary-items/pBAPuoNaSyHoRG4V.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[pCcRbDIkKmjmDyfT.htm](extinction-curse-bestiary-items/pCcRbDIkKmjmDyfT.htm)|Constant Spells|Constant Spells|modificada|
|[PCdyZXJEAyvmCnYy.htm](extinction-curse-bestiary-items/PCdyZXJEAyvmCnYy.htm)|Constant Spells|Constant Spells|modificada|
|[PED3IBsGf0atbCwI.htm](extinction-curse-bestiary-items/PED3IBsGf0atbCwI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PGIZYkUT2mwkytuL.htm](extinction-curse-bestiary-items/PGIZYkUT2mwkytuL.htm)|Scent (Precise) 100 feet|Aroma (Precisión) 100 pies.|modificada|
|[phoQ0yOkbf9saI2l.htm](extinction-curse-bestiary-items/phoQ0yOkbf9saI2l.htm)|Jaws|Fauces|modificada|
|[pHZZDAT6KIGrViZE.htm](extinction-curse-bestiary-items/pHZZDAT6KIGrViZE.htm)|Assimilate Lava|Asimilar Lava|modificada|
|[PmoZHqEM39IJVPZt.htm](extinction-curse-bestiary-items/PmoZHqEM39IJVPZt.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[PMvKwqjv8zSTg6HR.htm](extinction-curse-bestiary-items/PMvKwqjv8zSTg6HR.htm)|Burrowing Agony|Burrowing Agony|modificada|
|[pn15gaicH5zNmSAV.htm](extinction-curse-bestiary-items/pn15gaicH5zNmSAV.htm)|Maul|Zarpazo doble|modificada|
|[pnI5OGCrwAz8IPJ1.htm](extinction-curse-bestiary-items/pnI5OGCrwAz8IPJ1.htm)|Adhesive Body|Cuerpo Adhesivo|modificada|
|[PO0gF2xQ8vL9kRKY.htm](extinction-curse-bestiary-items/PO0gF2xQ8vL9kRKY.htm)|Firedamp Winds|Vientos Firedamp|modificada|
|[po14dNEO7G6AIdqA.htm](extinction-curse-bestiary-items/po14dNEO7G6AIdqA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PoE00pbKu92pJcmI.htm](extinction-curse-bestiary-items/PoE00pbKu92pJcmI.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Pp1p9fNkgDG02dcL.htm](extinction-curse-bestiary-items/Pp1p9fNkgDG02dcL.htm)|Inhabit Object|Habitar objeto|modificada|
|[PP6cWEeflOzf6dln.htm](extinction-curse-bestiary-items/PP6cWEeflOzf6dln.htm)|Counteflora Toxin|Counteflora Toxin|modificada|
|[pPAfuiRx3hQmyGuJ.htm](extinction-curse-bestiary-items/pPAfuiRx3hQmyGuJ.htm)|Boneshaking Roar|Boneshaking Roar|modificada|
|[pPirfuGfRrPRUV8Y.htm](extinction-curse-bestiary-items/pPirfuGfRrPRUV8Y.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[PpLXQRLE9C2ygCvU.htm](extinction-curse-bestiary-items/PpLXQRLE9C2ygCvU.htm)|Tail|Tail|modificada|
|[PQ4Thk0ntFSWU7Gz.htm](extinction-curse-bestiary-items/PQ4Thk0ntFSWU7Gz.htm)|Cabal Communion|Cabal Communion|modificada|
|[PQCJPgjYoVwBLXyY.htm](extinction-curse-bestiary-items/PQCJPgjYoVwBLXyY.htm)|Harrowing Misfortune|Harrowing Misfortune|modificada|
|[PQprcJ7E0RDiHXfR.htm](extinction-curse-bestiary-items/PQprcJ7E0RDiHXfR.htm)|Grab|Agarrado|modificada|
|[pquHeaKZPdfFQK6c.htm](extinction-curse-bestiary-items/pquHeaKZPdfFQK6c.htm)|Dagger|Daga|modificada|
|[PqYLOfqbS9HKpXwR.htm](extinction-curse-bestiary-items/PqYLOfqbS9HKpXwR.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[pr00tZKsLorUCDcD.htm](extinction-curse-bestiary-items/pr00tZKsLorUCDcD.htm)|Hunt Prey|Perseguir presa|modificada|
|[PTFpKWM9gdLACL3N.htm](extinction-curse-bestiary-items/PTFpKWM9gdLACL3N.htm)|Coven|Coven|modificada|
|[ptMSulmBrypLe9hN.htm](extinction-curse-bestiary-items/ptMSulmBrypLe9hN.htm)|Stench|Hedor|modificada|
|[pTrcnnDT8kWdlqJU.htm](extinction-curse-bestiary-items/pTrcnnDT8kWdlqJU.htm)|Fast Swallow|Tragar de golpe|modificada|
|[pulNjOGaDMXFhrC4.htm](extinction-curse-bestiary-items/pulNjOGaDMXFhrC4.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[PVSV062yywdBQCMh.htm](extinction-curse-bestiary-items/PVSV062yywdBQCMh.htm)|Corrosive Kiss|Beso Corrosivo|modificada|
|[pvYJQt0PVzYMhbJY.htm](extinction-curse-bestiary-items/pvYJQt0PVzYMhbJY.htm)|Jaws|Fauces|modificada|
|[pwQ9ySONmvrBDAn7.htm](extinction-curse-bestiary-items/pwQ9ySONmvrBDAn7.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[pxM83ECTkuL1t1yb.htm](extinction-curse-bestiary-items/pxM83ECTkuL1t1yb.htm)|Possessed|posesión|modificada|
|[q0Go6gYxsqhxoIiT.htm](extinction-curse-bestiary-items/q0Go6gYxsqhxoIiT.htm)|Mountain Stronghold|Baluarte de la monta|modificada|
|[q34JoxlJlf1D0nZT.htm](extinction-curse-bestiary-items/q34JoxlJlf1D0nZT.htm)|Claw|Garra|modificada|
|[Q3hmK1bo6Sh5wQEk.htm](extinction-curse-bestiary-items/Q3hmK1bo6Sh5wQEk.htm)|Tail|Tail|modificada|
|[q4nI7tSE0zcxhH3I.htm](extinction-curse-bestiary-items/q4nI7tSE0zcxhH3I.htm)|Pierced Tendon|Pierced Tendon|modificada|
|[q4Nwd5vpZrD2cK4B.htm](extinction-curse-bestiary-items/q4Nwd5vpZrD2cK4B.htm)|Haymaker|Haymaker|modificada|
|[Q7lByDcjQdmv3be5.htm](extinction-curse-bestiary-items/Q7lByDcjQdmv3be5.htm)|Acid Flask|Frasco de ácido|modificada|
|[q8ASqdiBd9SflYVU.htm](extinction-curse-bestiary-items/q8ASqdiBd9SflYVU.htm)|Spectral Ripple|Ondulación espectral|modificada|
|[qAcFJ6ENZ6hIxnoG.htm](extinction-curse-bestiary-items/qAcFJ6ENZ6hIxnoG.htm)|Show-Off|Show-Off|modificada|
|[qCyjY8GOlu7YozEX.htm](extinction-curse-bestiary-items/qCyjY8GOlu7YozEX.htm)|Web|Telara|modificada|
|[QDO52xyGxgu4Sa4l.htm](extinction-curse-bestiary-items/QDO52xyGxgu4Sa4l.htm)|Negative Healing|Curación negativa|modificada|
|[qEzLJPCCAZLBYVV4.htm](extinction-curse-bestiary-items/qEzLJPCCAZLBYVV4.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[QFL7zXFbbEXPg5V9.htm](extinction-curse-bestiary-items/QFL7zXFbbEXPg5V9.htm)|Claw|Garra|modificada|
|[qFVgtcLTXV4LIYQu.htm](extinction-curse-bestiary-items/qFVgtcLTXV4LIYQu.htm)|Glowing Bones|Glowing Bones|modificada|
|[QHfqtuROQMmVf7uq.htm](extinction-curse-bestiary-items/QHfqtuROQMmVf7uq.htm)|Unkillable|Unkillable|modificada|
|[QhKOuFQANuodhk8x.htm](extinction-curse-bestiary-items/QhKOuFQANuodhk8x.htm)|Mountain Stance|Posición de la monta|modificada|
|[QIPvgEw3eXLxKT8d.htm](extinction-curse-bestiary-items/QIPvgEw3eXLxKT8d.htm)|Self-Repair|Autorreparación|modificada|
|[QkXcthXHJxRMKAFf.htm](extinction-curse-bestiary-items/QkXcthXHJxRMKAFf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qLAXvZ1JTs2cxRUR.htm](extinction-curse-bestiary-items/qLAXvZ1JTs2cxRUR.htm)|Expanded Splash|Salpicadura extendida|modificada|
|[QlMKYLdaF5xd0vap.htm](extinction-curse-bestiary-items/QlMKYLdaF5xd0vap.htm)|Divine Restoration|Restablecimiento divino|modificada|
|[QNih5VevNZaGEYIs.htm](extinction-curse-bestiary-items/QNih5VevNZaGEYIs.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[QnkaPblW1eS7DJQR.htm](extinction-curse-bestiary-items/QnkaPblW1eS7DJQR.htm)|Spear|Lanza|modificada|
|[qnz5lB0E15Y5lYDX.htm](extinction-curse-bestiary-items/qnz5lB0E15Y5lYDX.htm)|Terror Master|Maestro del Terror|modificada|
|[qpmRSL3kdhharvvt.htm](extinction-curse-bestiary-items/qpmRSL3kdhharvvt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qpXDUheFeUsWxKho.htm](extinction-curse-bestiary-items/qpXDUheFeUsWxKho.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QqLiKxNQA0ywz1hJ.htm](extinction-curse-bestiary-items/QqLiKxNQA0ywz1hJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qQOkGagQPycQw5YC.htm](extinction-curse-bestiary-items/qQOkGagQPycQw5YC.htm)|+1 Status to All Saves vs. Magical Effects and Positive Effects|+1 situación a todas las salvaciones contra efectos mágicos y efectos positivos.|modificada|
|[QR5bODSKMiojeESn.htm](extinction-curse-bestiary-items/QR5bODSKMiojeESn.htm)|Orc Ferocity|Ferocidad de orco|modificada|
|[QReEjgBjoIXn5NnC.htm](extinction-curse-bestiary-items/QReEjgBjoIXn5NnC.htm)|Bladestorm|Tormenta de cuchillas|modificada|
|[Qrev8Nd4NZoiDnJ7.htm](extinction-curse-bestiary-items/Qrev8Nd4NZoiDnJ7.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[qRTwH6KdmvOMv6pr.htm](extinction-curse-bestiary-items/qRTwH6KdmvOMv6pr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qs4zOgvQbmRifUez.htm](extinction-curse-bestiary-items/qs4zOgvQbmRifUez.htm)|Raving Diatribe|Raving Diatribe|modificada|
|[qsR9Zyfw0HbieQM9.htm](extinction-curse-bestiary-items/qsR9Zyfw0HbieQM9.htm)|Earthen Torrent|Torrente de Tierra|modificada|
|[qsz7qYjE6S3Fc1ut.htm](extinction-curse-bestiary-items/qsz7qYjE6S3Fc1ut.htm)|Bowling Pin|Bowling Pin|modificada|
|[QT4UShVoztc5IRSM.htm](extinction-curse-bestiary-items/QT4UShVoztc5IRSM.htm)|Eyes of the Enthralled|Ojos del cautivado|modificada|
|[QT8BrycRuuUhs2V7.htm](extinction-curse-bestiary-items/QT8BrycRuuUhs2V7.htm)|Fetid Winds|Fetid Winds|modificada|
|[QXq9Nn7zGrJqJnuV.htm](extinction-curse-bestiary-items/QXq9Nn7zGrJqJnuV.htm)|Experienced Ambusher|Emboscador experimentado|modificada|
|[qYCOhM3CGVIsOcqt.htm](extinction-curse-bestiary-items/qYCOhM3CGVIsOcqt.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[QYVQuOt7vXM39lkd.htm](extinction-curse-bestiary-items/QYVQuOt7vXM39lkd.htm)|Harrow Card|Harrow Card|modificada|
|[qZ6MdZpGfCcoPSt7.htm](extinction-curse-bestiary-items/qZ6MdZpGfCcoPSt7.htm)|Longbow|Arco largo|modificada|
|[r0SfPNZ2ODvH5Jn3.htm](extinction-curse-bestiary-items/r0SfPNZ2ODvH5Jn3.htm)|Tentacle|Tentáculo|modificada|
|[r3gz4OYV1y7jbwTj.htm](extinction-curse-bestiary-items/r3gz4OYV1y7jbwTj.htm)|Constant Spells|Constant Spells|modificada|
|[r4uxKTSX8eoYMesb.htm](extinction-curse-bestiary-items/r4uxKTSX8eoYMesb.htm)|Dagger|Daga|modificada|
|[r8JQHbOW0puwaFWz.htm](extinction-curse-bestiary-items/r8JQHbOW0puwaFWz.htm)|Stool Bash|Stool Bash|modificada|
|[R8Kt7N2ubdHBIdow.htm](extinction-curse-bestiary-items/R8Kt7N2ubdHBIdow.htm)|Disgorge Bile|Disgorge Bile|modificada|
|[R9WycjP9krAdeKQw.htm](extinction-curse-bestiary-items/R9WycjP9krAdeKQw.htm)|Cleric Domain Spell|Cleric Domain Spell|modificada|
|[R9xjpMLRExdAfpdw.htm](extinction-curse-bestiary-items/R9xjpMLRExdAfpdw.htm)|Duck Away|Duck Away|modificada|
|[Ra5CZtxKiCGaTCKR.htm](extinction-curse-bestiary-items/Ra5CZtxKiCGaTCKR.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RaAxUg8NsJWovO3S.htm](extinction-curse-bestiary-items/RaAxUg8NsJWovO3S.htm)|Drain Life|Drenar Vida|modificada|
|[RAzDul5aLDAcZI57.htm](extinction-curse-bestiary-items/RAzDul5aLDAcZI57.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[RBTICSCn9OcPSsfb.htm](extinction-curse-bestiary-items/RBTICSCn9OcPSsfb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rCVpQ7gsbhPzhRsi.htm](extinction-curse-bestiary-items/rCVpQ7gsbhPzhRsi.htm)|Thrown Bottle|Botella Arrojada|modificada|
|[rdFZCjqKcogZyMg7.htm](extinction-curse-bestiary-items/rdFZCjqKcogZyMg7.htm)|Dreadful Spite|Dreadful Spite|modificada|
|[RdSrem9D6XcqzAuu.htm](extinction-curse-bestiary-items/RdSrem9D6XcqzAuu.htm)|Boar Charge|Carga de jabalí|modificada|
|[rH50o3UwWDkWRLLK.htm](extinction-curse-bestiary-items/rH50o3UwWDkWRLLK.htm)|Wight Spawn|Wight Spawn|modificada|
|[rhC1IVvlV0IPjPcB.htm](extinction-curse-bestiary-items/rhC1IVvlV0IPjPcB.htm)|Rend|Rasgadura|modificada|
|[rHEKhmV0XO0CxR0y.htm](extinction-curse-bestiary-items/rHEKhmV0XO0CxR0y.htm)|Maul|Zarpazo doble|modificada|
|[rHEZ6pZ9PZWW3kwC.htm](extinction-curse-bestiary-items/rHEZ6pZ9PZWW3kwC.htm)|Claw|Garra|modificada|
|[RhulgNsATpCM6nip.htm](extinction-curse-bestiary-items/RhulgNsATpCM6nip.htm)|Claw|Garra|modificada|
|[RHWWGr2v6M4d2qBz.htm](extinction-curse-bestiary-items/RHWWGr2v6M4d2qBz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RJ26mcn4UnryXIsN.htm](extinction-curse-bestiary-items/RJ26mcn4UnryXIsN.htm)|Haunting Wail|Haunting Lamento|modificada|
|[rKjDLURibGU1uFiB.htm](extinction-curse-bestiary-items/rKjDLURibGU1uFiB.htm)|Jaws|Fauces|modificada|
|[RmnzUOOeZZH9SVi3.htm](extinction-curse-bestiary-items/RmnzUOOeZZH9SVi3.htm)|Alchemical Injection|Inyección alquímica|modificada|
|[RMwLgobPTr1kBImK.htm](extinction-curse-bestiary-items/RMwLgobPTr1kBImK.htm)|Resonating Note|Resonating Note|modificada|
|[rNe7hr19TVEKyp1e.htm](extinction-curse-bestiary-items/rNe7hr19TVEKyp1e.htm)|Starvation Aura|Starvation Aura|modificada|
|[rPxLM6aWMkXPfN46.htm](extinction-curse-bestiary-items/rPxLM6aWMkXPfN46.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RqGgTNak1Cjjsdm0.htm](extinction-curse-bestiary-items/RqGgTNak1Cjjsdm0.htm)|Greater Frost Main-Gauche|Gélida mayor Main-Gauche|modificada|
|[RqsjlbJfjDXjttuD.htm](extinction-curse-bestiary-items/RqsjlbJfjDXjttuD.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[rQT7KUvA4yzEm4PO.htm](extinction-curse-bestiary-items/rQT7KUvA4yzEm4PO.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[RtG3Dfe9kEKJnPYX.htm](extinction-curse-bestiary-items/RtG3Dfe9kEKJnPYX.htm)|Aura of Annihilation|Aura de Aniquilación|modificada|
|[RThixt2DZUbyrFWM.htm](extinction-curse-bestiary-items/RThixt2DZUbyrFWM.htm)|Pincer|Pinza|modificada|
|[RUD5WUI6tdR6jtHc.htm](extinction-curse-bestiary-items/RUD5WUI6tdR6jtHc.htm)|Mobility|Movilidad|modificada|
|[rulKmak2Pk0vfAnF.htm](extinction-curse-bestiary-items/rulKmak2Pk0vfAnF.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[rvBCgpjjGfH5PcRh.htm](extinction-curse-bestiary-items/rvBCgpjjGfH5PcRh.htm)|Negative Healing|Curación negativa|modificada|
|[RvNmrpkeCr0FxRBy.htm](extinction-curse-bestiary-items/RvNmrpkeCr0FxRBy.htm)|Gate Collapse|Colapso umbral|modificada|
|[RvPP5jeliNA9rwoy.htm](extinction-curse-bestiary-items/RvPP5jeliNA9rwoy.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[RVQyJE6Qk4ngMwfH.htm](extinction-curse-bestiary-items/RVQyJE6Qk4ngMwfH.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[rvtMyyj7UBsQraKA.htm](extinction-curse-bestiary-items/rvtMyyj7UBsQraKA.htm)|Negative Healing|Curación negativa|modificada|
|[rVwshWQjLtgO0x1Z.htm](extinction-curse-bestiary-items/rVwshWQjLtgO0x1Z.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ryVPqtMIfWvzGo2o.htm](extinction-curse-bestiary-items/ryVPqtMIfWvzGo2o.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s1YAaHM6yGhPRyhY.htm](extinction-curse-bestiary-items/s1YAaHM6yGhPRyhY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s2ZCoyccvRlVnT8V.htm](extinction-curse-bestiary-items/s2ZCoyccvRlVnT8V.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S9kIEucfakAniYkE.htm](extinction-curse-bestiary-items/S9kIEucfakAniYkE.htm)|Claw|Garra|modificada|
|[S9rsAqZ6i0J68L8D.htm](extinction-curse-bestiary-items/S9rsAqZ6i0J68L8D.htm)|Alchemical Formulas|Alchemical Formulas|modificada|
|[sa6rqRzXU6Y37Y0P.htm](extinction-curse-bestiary-items/sa6rqRzXU6Y37Y0P.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[sAcBKkpmfacpuDgO.htm](extinction-curse-bestiary-items/sAcBKkpmfacpuDgO.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[saDsnNdD6KrLH2zA.htm](extinction-curse-bestiary-items/saDsnNdD6KrLH2zA.htm)|Captive Rake|Desgarrar cautivo|modificada|
|[saeYOvVw7Zbc6J1y.htm](extinction-curse-bestiary-items/saeYOvVw7Zbc6J1y.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[SAILABWdBSyuuEzf.htm](extinction-curse-bestiary-items/SAILABWdBSyuuEzf.htm)|Fist|Puño|modificada|
|[SaNPZ048iYwMkyBR.htm](extinction-curse-bestiary-items/SaNPZ048iYwMkyBR.htm)|Remove Face|Quitar Cara|modificada|
|[SBj6G1VOAt4Bk46Z.htm](extinction-curse-bestiary-items/SBj6G1VOAt4Bk46Z.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SbnljAkVkA39hCW0.htm](extinction-curse-bestiary-items/SbnljAkVkA39hCW0.htm)|Javelin|Javelin|modificada|
|[sbOuf0oUW8o7LInv.htm](extinction-curse-bestiary-items/sbOuf0oUW8o7LInv.htm)|Devastating Strikes|Golpes devastadores|modificada|
|[SDcOSLxZFCLOVEvk.htm](extinction-curse-bestiary-items/SDcOSLxZFCLOVEvk.htm)|Etheric Tug|Remolcador Etéreo|modificada|
|[sdS6ZYyIEWr1joRs.htm](extinction-curse-bestiary-items/sdS6ZYyIEWr1joRs.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[sDwP6nXN8KQIiRuR.htm](extinction-curse-bestiary-items/sDwP6nXN8KQIiRuR.htm)|Spike Jab|Spike Jab|modificada|
|[SeJz5BH1ioJC1OVG.htm](extinction-curse-bestiary-items/SeJz5BH1ioJC1OVG.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[SFb9j3FbIppSf34W.htm](extinction-curse-bestiary-items/SFb9j3FbIppSf34W.htm)|Jaws|Fauces|modificada|
|[SFKdrfAhk0O6ItXS.htm](extinction-curse-bestiary-items/SFKdrfAhk0O6ItXS.htm)|Talon|Talon|modificada|
|[sgoOmgJ1Ff1lrKfa.htm](extinction-curse-bestiary-items/sgoOmgJ1Ff1lrKfa.htm)|Constant Spells|Constant Spells|modificada|
|[SgXreqFoQH1zwIKM.htm](extinction-curse-bestiary-items/SgXreqFoQH1zwIKM.htm)|Grab|Agarrado|modificada|
|[sHOiqltQCZRdyxTa.htm](extinction-curse-bestiary-items/sHOiqltQCZRdyxTa.htm)|Claw|Garra|modificada|
|[SHp8QZBD4sEwfuHc.htm](extinction-curse-bestiary-items/SHp8QZBD4sEwfuHc.htm)|Psychogenic Secretions|Secreciones Psicógenas|modificada|
|[ShRk5Gxx1Zm4oN09.htm](extinction-curse-bestiary-items/ShRk5Gxx1Zm4oN09.htm)|Grab|Agarrado|modificada|
|[SIOm7a8pyY2r7KU4.htm](extinction-curse-bestiary-items/SIOm7a8pyY2r7KU4.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[SizhdwVCwNc2BO62.htm](extinction-curse-bestiary-items/SizhdwVCwNc2BO62.htm)|Claw|Garra|modificada|
|[SjV0H9VKBoqzK0Bc.htm](extinction-curse-bestiary-items/SjV0H9VKBoqzK0Bc.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[skC80CG18plDdch1.htm](extinction-curse-bestiary-items/skC80CG18plDdch1.htm)|Vein|Vein|modificada|
|[SLW8aLZrbrMqAniN.htm](extinction-curse-bestiary-items/SLW8aLZrbrMqAniN.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[sM4ejYbqYSJxrVSd.htm](extinction-curse-bestiary-items/sM4ejYbqYSJxrVSd.htm)|Inexorable March|Marcha inexorable|modificada|
|[sMH395MKnCnUaAGg.htm](extinction-curse-bestiary-items/sMH395MKnCnUaAGg.htm)|Fortune's Favor|El Favor de la Fortuna|modificada|
|[SNhhLpkB6xBZW7tt.htm](extinction-curse-bestiary-items/SNhhLpkB6xBZW7tt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[sNTGYTNdoPw797fO.htm](extinction-curse-bestiary-items/sNTGYTNdoPw797fO.htm)|Claw|Garra|modificada|
|[SOJYFj8N8muE40NO.htm](extinction-curse-bestiary-items/SOJYFj8N8muE40NO.htm)|Ferocity|Ferocidad|modificada|
|[SOZB5DP0hEjlVrnz.htm](extinction-curse-bestiary-items/SOZB5DP0hEjlVrnz.htm)|Dagger|Daga|modificada|
|[srgnKyGLbnQtqfOC.htm](extinction-curse-bestiary-items/srgnKyGLbnQtqfOC.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ssPBbznwUNXKYBrK.htm](extinction-curse-bestiary-items/ssPBbznwUNXKYBrK.htm)|Warhammer|Warhammer|modificada|
|[st4i4dmwri16Jaw4.htm](extinction-curse-bestiary-items/st4i4dmwri16Jaw4.htm)|Claw|Garra|modificada|
|[STPKBwIDUxnzyqnQ.htm](extinction-curse-bestiary-items/STPKBwIDUxnzyqnQ.htm)|Claw|Garra|modificada|
|[Stv3Oe7QU577Gke1.htm](extinction-curse-bestiary-items/Stv3Oe7QU577Gke1.htm)|Wheel Spin|Giro de la rueda|modificada|
|[suHARe69G02OdMHO.htm](extinction-curse-bestiary-items/suHARe69G02OdMHO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[SWKWArGZdBHMKGGt.htm](extinction-curse-bestiary-items/SWKWArGZdBHMKGGt.htm)|Protect|Protección|modificada|
|[SWl7AjDMWH11G4ER.htm](extinction-curse-bestiary-items/SWl7AjDMWH11G4ER.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[swSzLoXHSeUzg4jT.htm](extinction-curse-bestiary-items/swSzLoXHSeUzg4jT.htm)|Dagger|Daga|modificada|
|[SX1FS88WJhELdBeJ.htm](extinction-curse-bestiary-items/SX1FS88WJhELdBeJ.htm)|Jaws|Fauces|modificada|
|[sxbPWmr9UrqQlMcl.htm](extinction-curse-bestiary-items/sxbPWmr9UrqQlMcl.htm)|Jaws|Fauces|modificada|
|[SZ8U6jGsKMqKBXxd.htm](extinction-curse-bestiary-items/SZ8U6jGsKMqKBXxd.htm)|Tighten Bracts|Apretar Brácteas|modificada|
|[SZJLgj6RbPu5Q3a7.htm](extinction-curse-bestiary-items/SZJLgj6RbPu5Q3a7.htm)|Alchemist's Fire (Greater) (Infused)|Fuego de alquimista (mayor) (infundido)|modificada|
|[T1x41KttaKGcSc4U.htm](extinction-curse-bestiary-items/T1x41KttaKGcSc4U.htm)|Vines|Vides|modificada|
|[T2LmG0xgs6TOZdMJ.htm](extinction-curse-bestiary-items/T2LmG0xgs6TOZdMJ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[t3bOnN3uPrJ4Ornk.htm](extinction-curse-bestiary-items/t3bOnN3uPrJ4Ornk.htm)|Stunning Tail|Cola Impresionante|modificada|
|[t4LSexvOzvp0ZgSM.htm](extinction-curse-bestiary-items/t4LSexvOzvp0ZgSM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[t5VxrfWqKM7zBVtb.htm](extinction-curse-bestiary-items/t5VxrfWqKM7zBVtb.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[tAJk9Rlb2IK6RZDK.htm](extinction-curse-bestiary-items/tAJk9Rlb2IK6RZDK.htm)|Claw|Garra|modificada|
|[taymsycnWc4CSJIZ.htm](extinction-curse-bestiary-items/taymsycnWc4CSJIZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tBRAlDxZSt1mK0F8.htm](extinction-curse-bestiary-items/tBRAlDxZSt1mK0F8.htm)|Jaws|Fauces|modificada|
|[tdl0fojnWCseyME0.htm](extinction-curse-bestiary-items/tdl0fojnWCseyME0.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[TDTA1aGACXzFFKJ1.htm](extinction-curse-bestiary-items/TDTA1aGACXzFFKJ1.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TdYMUhqe9meOFVEe.htm](extinction-curse-bestiary-items/TdYMUhqe9meOFVEe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[texToCbGMNj42gJm.htm](extinction-curse-bestiary-items/texToCbGMNj42gJm.htm)|Jaws|Fauces|modificada|
|[tF6fTR0l1CiErgQt.htm](extinction-curse-bestiary-items/tF6fTR0l1CiErgQt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tfSYqUkDGCRtI9KG.htm](extinction-curse-bestiary-items/tfSYqUkDGCRtI9KG.htm)|+2 Status to All Saves vs. Mental Effects|+2 situación a todas las salvaciones contra efectos mentales.|modificada|
|[TFv6rcyouMcUWGYM.htm](extinction-curse-bestiary-items/TFv6rcyouMcUWGYM.htm)|Jaws|Fauces|modificada|
|[th7bXgkU6h5f3edz.htm](extinction-curse-bestiary-items/th7bXgkU6h5f3edz.htm)|Sticky Poison|Veneno pegajoso|modificada|
|[ThFBVTrezGFEC7SU.htm](extinction-curse-bestiary-items/ThFBVTrezGFEC7SU.htm)|Vulnerable to Neutralize Poison|Vulnerable a neutralizar veneno|modificada|
|[thIGTmJJq0pQ2UGS.htm](extinction-curse-bestiary-items/thIGTmJJq0pQ2UGS.htm)|Ranseur|Ranseur|modificada|
|[TiC9nLfhB47bWwSQ.htm](extinction-curse-bestiary-items/TiC9nLfhB47bWwSQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TJ2DjlYVnOpP8O0a.htm](extinction-curse-bestiary-items/TJ2DjlYVnOpP8O0a.htm)|Fist|Puño|modificada|
|[tjSb9xUg6kUCt5Jc.htm](extinction-curse-bestiary-items/tjSb9xUg6kUCt5Jc.htm)|Improved Grab|Agarrado mejorado|modificada|
|[tk1qMQuv7snMkpnc.htm](extinction-curse-bestiary-items/tk1qMQuv7snMkpnc.htm)|Breath Weapon|Breath Weapon|modificada|
|[tkLePiTdXXB3VbWz.htm](extinction-curse-bestiary-items/tkLePiTdXXB3VbWz.htm)|Scorching Maul|Zarpazo doble|modificada|
|[Tlt7eeLvAwfloiuI.htm](extinction-curse-bestiary-items/Tlt7eeLvAwfloiuI.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[to3YPHwxklrUbd6F.htm](extinction-curse-bestiary-items/to3YPHwxklrUbd6F.htm)|Darkvision|Visión en la oscuridad|modificada|
|[togeLXdDUkCenYQ2.htm](extinction-curse-bestiary-items/togeLXdDUkCenYQ2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tQg1mD39bXYAzcyF.htm](extinction-curse-bestiary-items/tQg1mD39bXYAzcyF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TQoNfhkr54rzdEGA.htm](extinction-curse-bestiary-items/TQoNfhkr54rzdEGA.htm)|Etheric Fibers|Fibras Etéricas|modificada|
|[TqypIhZnwhIuh2eR.htm](extinction-curse-bestiary-items/TqypIhZnwhIuh2eR.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[TRACG7yjgPCzShSH.htm](extinction-curse-bestiary-items/TRACG7yjgPCzShSH.htm)|Furious Sprint|Carrera furiosa|modificada|
|[tRUwzQREvBkvCsOz.htm](extinction-curse-bestiary-items/tRUwzQREvBkvCsOz.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[tsSj7T94dDPZAsP0.htm](extinction-curse-bestiary-items/tsSj7T94dDPZAsP0.htm)|Wildwood Halfling|Mediano de las florestas salvajes|modificada|
|[tuA9DbCZRvsPkJ25.htm](extinction-curse-bestiary-items/tuA9DbCZRvsPkJ25.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[tW1DUMQeCYYiJFWi.htm](extinction-curse-bestiary-items/tW1DUMQeCYYiJFWi.htm)|Dream Haunting|Acoso onírico del Sueño|modificada|
|[TXbhRlWu7SAx8Ctw.htm](extinction-curse-bestiary-items/TXbhRlWu7SAx8Ctw.htm)|Powerful Stench|Hedor intenso|modificada|
|[TXT2Ytirxdb5hIhI.htm](extinction-curse-bestiary-items/TXT2Ytirxdb5hIhI.htm)|Blinding Flare|Llamarada Cegadora|modificada|
|[tZRyEtuS7OQFZRDu.htm](extinction-curse-bestiary-items/tZRyEtuS7OQFZRDu.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[tZyhBnoDUGVEDL7x.htm](extinction-curse-bestiary-items/tZyhBnoDUGVEDL7x.htm)|Lightning Catcher|Lightning Catcher|modificada|
|[U3GaqjscOV9MmG6W.htm](extinction-curse-bestiary-items/U3GaqjscOV9MmG6W.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[u3ZNtTIdDIwBtNav.htm](extinction-curse-bestiary-items/u3ZNtTIdDIwBtNav.htm)|Concentrated Xulgath Bile|Concentrado de bilis Xulgath|modificada|
|[U4mx6nAal2UDRgr5.htm](extinction-curse-bestiary-items/U4mx6nAal2UDRgr5.htm)|Shortbow|Arco corto|modificada|
|[u6AGWAaIUZZF6DmM.htm](extinction-curse-bestiary-items/u6AGWAaIUZZF6DmM.htm)|Psionic Scent (Imprecise) 120 feet|Aroma Psiónico (Impreciso) 120 pies|modificada|
|[u7gy4Uzdf1tEa3xv.htm](extinction-curse-bestiary-items/u7gy4Uzdf1tEa3xv.htm)|Spike|Spike|modificada|
|[U8nRgKXL8LdnfuMo.htm](extinction-curse-bestiary-items/U8nRgKXL8LdnfuMo.htm)|Lava Ball|Bola de Lava|modificada|
|[uaFGybzZAhGxxd3L.htm](extinction-curse-bestiary-items/uaFGybzZAhGxxd3L.htm)|Psychogenic Secretions|Secreciones Psicógenas|modificada|
|[ubs94wAy7XupF4V3.htm](extinction-curse-bestiary-items/ubs94wAy7XupF4V3.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[UBZHa183gehiCiti.htm](extinction-curse-bestiary-items/UBZHa183gehiCiti.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ucPPtZXjJlD0iDBT.htm](extinction-curse-bestiary-items/ucPPtZXjJlD0iDBT.htm)|Hoe|Hoe|modificada|
|[UE6Gaj3IfXSpVJue.htm](extinction-curse-bestiary-items/UE6Gaj3IfXSpVJue.htm)|Signal Arrow|Signal Arrow|modificada|
|[UEuaQyxEGdYTyPJh.htm](extinction-curse-bestiary-items/UEuaQyxEGdYTyPJh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ug2gQJqmRhm1KiCo.htm](extinction-curse-bestiary-items/Ug2gQJqmRhm1KiCo.htm)|Jaws|Fauces|modificada|
|[uH9ary8OcexwY8Hf.htm](extinction-curse-bestiary-items/uH9ary8OcexwY8Hf.htm)|Cat's Luck|Suerte felina|modificada|
|[UhrlbhvCVWik38KZ.htm](extinction-curse-bestiary-items/UhrlbhvCVWik38KZ.htm)|Mirror Vulnerability|Vulnerabilidad de espejo|modificada|
|[Ui0JxPyP262iI9kF.htm](extinction-curse-bestiary-items/Ui0JxPyP262iI9kF.htm)|Xulgath Bile|Xulgath Bile|modificada|
|[uI0lVkJrLHDNJ0a3.htm](extinction-curse-bestiary-items/uI0lVkJrLHDNJ0a3.htm)|Grab|Agarrado|modificada|
|[uIFV7L1y3JpQbP7J.htm](extinction-curse-bestiary-items/uIFV7L1y3JpQbP7J.htm)|Profane Gift|Don profano|modificada|
|[Uj6U5QEXhGHuGrJ0.htm](extinction-curse-bestiary-items/Uj6U5QEXhGHuGrJ0.htm)|Proboscis|Proboscis|modificada|
|[ujQNaJiDY5ja5QlI.htm](extinction-curse-bestiary-items/ujQNaJiDY5ja5QlI.htm)|Magical Tongue|Lengua mágica|modificada|
|[UK1kJIuJwvm59qRG.htm](extinction-curse-bestiary-items/UK1kJIuJwvm59qRG.htm)|Jaws|Fauces|modificada|
|[Ulxj8gjRkhtCZWc1.htm](extinction-curse-bestiary-items/Ulxj8gjRkhtCZWc1.htm)|Punishing Tail|Coletazo castigador|modificada|
|[un5jhCkWDwKr2WAC.htm](extinction-curse-bestiary-items/un5jhCkWDwKr2WAC.htm)|Stoneraiser Javelin|Stoneraiser Javelin|modificada|
|[Unihf1X2OVrBjHLa.htm](extinction-curse-bestiary-items/Unihf1X2OVrBjHLa.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[UNNVhJIyikJIMTEt.htm](extinction-curse-bestiary-items/UNNVhJIyikJIMTEt.htm)|Constrict|Restringir|modificada|
|[UOVIQWXoyaOc2IBj.htm](extinction-curse-bestiary-items/UOVIQWXoyaOc2IBj.htm)|Nightmare Rider|Jinete de pesadilla|modificada|
|[UpFgpADvEZmNKChA.htm](extinction-curse-bestiary-items/UpFgpADvEZmNKChA.htm)|Negative Healing|Curación negativa|modificada|
|[upwihrPQGTpiBwCk.htm](extinction-curse-bestiary-items/upwihrPQGTpiBwCk.htm)|Blightburn Sickness|Blightburn Sickness|modificada|
|[uqAGOAWqfLDXtxZj.htm](extinction-curse-bestiary-items/uqAGOAWqfLDXtxZj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UquLgAOYtyCaklg8.htm](extinction-curse-bestiary-items/UquLgAOYtyCaklg8.htm)|Bastard Sword|Espada Bastarda|modificada|
|[URItNOOpBctKvf7C.htm](extinction-curse-bestiary-items/URItNOOpBctKvf7C.htm)|Pounce|Abalanzarse|modificada|
|[URt0ZSSRegSabF7O.htm](extinction-curse-bestiary-items/URt0ZSSRegSabF7O.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Us6h9h1kYfyx8LCO.htm](extinction-curse-bestiary-items/Us6h9h1kYfyx8LCO.htm)|Powerful Stench|Hedor intenso|modificada|
|[usMIDvxtqfXusKnf.htm](extinction-curse-bestiary-items/usMIDvxtqfXusKnf.htm)|Phantom Hand|Phantom Hand|modificada|
|[ut1GcOdnxUjt4DaV.htm](extinction-curse-bestiary-items/ut1GcOdnxUjt4DaV.htm)|Jaws|Fauces|modificada|
|[UuJaCgIvEAS1EyoP.htm](extinction-curse-bestiary-items/UuJaCgIvEAS1EyoP.htm)|Longsword|Longsword|modificada|
|[UwmfxnwjIDn1MR7C.htm](extinction-curse-bestiary-items/UwmfxnwjIDn1MR7C.htm)|+2 status to all saves vs. magic|+2 situación a todas las salvaciones contra magia|modificada|
|[UxbAHJiUJAsFm4cq.htm](extinction-curse-bestiary-items/UxbAHJiUJAsFm4cq.htm)|Breath Weapon|Breath Weapon|modificada|
|[UxkpitCYml2lDYR5.htm](extinction-curse-bestiary-items/UxkpitCYml2lDYR5.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[uy0V4RLpmPZUTFQ9.htm](extinction-curse-bestiary-items/uy0V4RLpmPZUTFQ9.htm)|Pollen Spray|Polen Spray|modificada|
|[uyZlbHUmh7S7hZjt.htm](extinction-curse-bestiary-items/uyZlbHUmh7S7hZjt.htm)|Change Shape|Change Shape|modificada|
|[uZNc6ZOmT97s4O7v.htm](extinction-curse-bestiary-items/uZNc6ZOmT97s4O7v.htm)|Steal Soul|Sustraer Alma|modificada|
|[v0n7ntTptPAp7TPz.htm](extinction-curse-bestiary-items/v0n7ntTptPAp7TPz.htm)|Brazier|Brasero|modificada|
|[v3xcacl25td5gbaY.htm](extinction-curse-bestiary-items/v3xcacl25td5gbaY.htm)|Convergent Link|Enlace Convergente|modificada|
|[V4kdJTgqzGT8pdXd.htm](extinction-curse-bestiary-items/V4kdJTgqzGT8pdXd.htm)|Spiral of Despair|Espiral de Desesperación|modificada|
|[V6jm45qr9Nbcz49c.htm](extinction-curse-bestiary-items/V6jm45qr9Nbcz49c.htm)|Jaws|Fauces|modificada|
|[vbLxcEZSddIEfLOW.htm](extinction-curse-bestiary-items/vbLxcEZSddIEfLOW.htm)|Web Trap|Trampa telara|modificada|
|[VcMwjaxqor5ucwyE.htm](extinction-curse-bestiary-items/VcMwjaxqor5ucwyE.htm)|Javelin|Javelin|modificada|
|[VCPQniFfaKaaW5dO.htm](extinction-curse-bestiary-items/VCPQniFfaKaaW5dO.htm)|Dogged Persistence|Dogged Persistence|modificada|
|[vCZP7eeVSKKUB4y7.htm](extinction-curse-bestiary-items/vCZP7eeVSKKUB4y7.htm)|Caustic Fog|Niebla Cáustica|modificada|
|[vd0T6m7E9RIQTVJH.htm](extinction-curse-bestiary-items/vd0T6m7E9RIQTVJH.htm)|Resin Crust|Resin Crust|modificada|
|[vdGhEdioWGsSOE8A.htm](extinction-curse-bestiary-items/vdGhEdioWGsSOE8A.htm)|Grab|Agarrado|modificada|
|[vdR9EBKzeEkLlmzv.htm](extinction-curse-bestiary-items/vdR9EBKzeEkLlmzv.htm)|Infused Items|Equipos infundidos|modificada|
|[Ve7Oe6nX2LPhgzr3.htm](extinction-curse-bestiary-items/Ve7Oe6nX2LPhgzr3.htm)|Constant Spells|Constant Spells|modificada|
|[ves4VuZndUbyx170.htm](extinction-curse-bestiary-items/ves4VuZndUbyx170.htm)|Bile Jet|Chorro de bilis|modificada|
|[vGPRZAcP9sO1oSWb.htm](extinction-curse-bestiary-items/vGPRZAcP9sO1oSWb.htm)|Explosion|Explosión|modificada|
|[vhSzb4rXivmMx69j.htm](extinction-curse-bestiary-items/vhSzb4rXivmMx69j.htm)|Corpse Consumption|Consumo de cadáveres|modificada|
|[vHvfuUwHIpuo9u3k.htm](extinction-curse-bestiary-items/vHvfuUwHIpuo9u3k.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VIc5tlcbWy6TfS7W.htm](extinction-curse-bestiary-items/VIc5tlcbWy6TfS7W.htm)|Vampire Weaknesses|Vampire Weaknesses|modificada|
|[VKEoaAVU8jhX7DV0.htm](extinction-curse-bestiary-items/VKEoaAVU8jhX7DV0.htm)|Claw|Garra|modificada|
|[vkLS4esGKnpycmG3.htm](extinction-curse-bestiary-items/vkLS4esGKnpycmG3.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[vLi3Zdc0TOzo5Ns5.htm](extinction-curse-bestiary-items/vLi3Zdc0TOzo5Ns5.htm)|Stench|Hedor|modificada|
|[vmm346ihGo1JwNqk.htm](extinction-curse-bestiary-items/vmm346ihGo1JwNqk.htm)|Grab|Agarrado|modificada|
|[VMTjcCiLutAHY3LX.htm](extinction-curse-bestiary-items/VMTjcCiLutAHY3LX.htm)|Sudden Charge|Carga súbita|modificada|
|[Vn8Ud2h9UUkYBpqn.htm](extinction-curse-bestiary-items/Vn8Ud2h9UUkYBpqn.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[vNO1fTV51lJ1XgEx.htm](extinction-curse-bestiary-items/vNO1fTV51lJ1XgEx.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[voHVxry1TMmVuJ05.htm](extinction-curse-bestiary-items/voHVxry1TMmVuJ05.htm)|Dirt Clod|Dirt Clod|modificada|
|[VojkR50T97lhGAno.htm](extinction-curse-bestiary-items/VojkR50T97lhGAno.htm)|Blunt Snout|Hocico romo|modificada|
|[VPLYApahsyX5VFMb.htm](extinction-curse-bestiary-items/VPLYApahsyX5VFMb.htm)|Claw|Garra|modificada|
|[VQ95h7HFFMZ0YpGA.htm](extinction-curse-bestiary-items/VQ95h7HFFMZ0YpGA.htm)|Jaws|Fauces|modificada|
|[VsbGRflYaQb24YM7.htm](extinction-curse-bestiary-items/VsbGRflYaQb24YM7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VsSnUcSJag9gY3y8.htm](extinction-curse-bestiary-items/VsSnUcSJag9gY3y8.htm)|Mobility|Movilidad|modificada|
|[Vt51sZhJfLZaVw6i.htm](extinction-curse-bestiary-items/Vt51sZhJfLZaVw6i.htm)|Ghostly Grip|Ghostly Grip|modificada|
|[VTneIxzjqMPtSAZp.htm](extinction-curse-bestiary-items/VTneIxzjqMPtSAZp.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[VTpDnvKqgWyKG8Ow.htm](extinction-curse-bestiary-items/VTpDnvKqgWyKG8Ow.htm)|Claw|Garra|modificada|
|[vtYn8bg0Liql8rWa.htm](extinction-curse-bestiary-items/vtYn8bg0Liql8rWa.htm)|Improved Grab|Agarrado mejorado|modificada|
|[vU5wizpU0tuQR4N7.htm](extinction-curse-bestiary-items/vU5wizpU0tuQR4N7.htm)|Whip Vulnerability|Látigo Vulnerabilidad|modificada|
|[vUEEF3wzoLnBwUKQ.htm](extinction-curse-bestiary-items/vUEEF3wzoLnBwUKQ.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[vW9bna7OUkxyfZxx.htm](extinction-curse-bestiary-items/vW9bna7OUkxyfZxx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vwi28Ix3z8efoGcw.htm](extinction-curse-bestiary-items/vwi28Ix3z8efoGcw.htm)|Grab|Agarrado|modificada|
|[vWOXvMUv1MGUsjKM.htm](extinction-curse-bestiary-items/vWOXvMUv1MGUsjKM.htm)|Jaws|Fauces|modificada|
|[vwr9j20rI5uA7Vs7.htm](extinction-curse-bestiary-items/vwr9j20rI5uA7Vs7.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[vYBcrx4xIUPetI0S.htm](extinction-curse-bestiary-items/vYBcrx4xIUPetI0S.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[VyBZbrON0TldOYvl.htm](extinction-curse-bestiary-items/VyBZbrON0TldOYvl.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[vYd3rbKnbc32kox2.htm](extinction-curse-bestiary-items/vYd3rbKnbc32kox2.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[VyjU0grk7ThTTa4U.htm](extinction-curse-bestiary-items/VyjU0grk7ThTTa4U.htm)|Claw|Garra|modificada|
|[vZ2fFYnZKoSgkkhI.htm](extinction-curse-bestiary-items/vZ2fFYnZKoSgkkhI.htm)|Breath Weapon|Breath Weapon|modificada|
|[VZsSOXRGbGwz6dqc.htm](extinction-curse-bestiary-items/VZsSOXRGbGwz6dqc.htm)|Animated Attack|Ataque Animado|modificada|
|[W0iJibS7qX5Av9oY.htm](extinction-curse-bestiary-items/W0iJibS7qX5Av9oY.htm)|Knockdown|Derribo|modificada|
|[W1NWJyvyZIHarWZi.htm](extinction-curse-bestiary-items/W1NWJyvyZIHarWZi.htm)|Jaws|Fauces|modificada|
|[w285iz3GQ53ORMER.htm](extinction-curse-bestiary-items/w285iz3GQ53ORMER.htm)|Blood Eruption|Blood Eruption|modificada|
|[W4dTvjsUpOMjS9ip.htm](extinction-curse-bestiary-items/W4dTvjsUpOMjS9ip.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[W6tZElfSPD000KNK.htm](extinction-curse-bestiary-items/W6tZElfSPD000KNK.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[w8UngXIC47fcMU5n.htm](extinction-curse-bestiary-items/w8UngXIC47fcMU5n.htm)|+2 Circumstance to All Saves vs. Disarm|+2 Circunstancia a todas las salvaciones contra Desarmar.|modificada|
|[wAyWp8e9RHnS2chv.htm](extinction-curse-bestiary-items/wAyWp8e9RHnS2chv.htm)|Javelin|Javelin|modificada|
|[WC2Jedgc1MI3i61x.htm](extinction-curse-bestiary-items/WC2Jedgc1MI3i61x.htm)|Paddler Shoony|Remador Shoony|modificada|
|[wcALGN4qxbG1E6A4.htm](extinction-curse-bestiary-items/wcALGN4qxbG1E6A4.htm)|Unmasked Statues|Desenmascarado Estatuas|modificada|
|[WcSu6LxPKmis0S40.htm](extinction-curse-bestiary-items/WcSu6LxPKmis0S40.htm)|Disrupted Link|Enlace Disruptora|modificada|
|[wEVWlNgGdVwq51Y9.htm](extinction-curse-bestiary-items/wEVWlNgGdVwq51Y9.htm)|Claw|Garra|modificada|
|[wFol3Gz0VjCNM4zQ.htm](extinction-curse-bestiary-items/wFol3Gz0VjCNM4zQ.htm)|Bloodline Spells|Conjuros de linaje|modificada|
|[WggmVdmCE4CvyNXL.htm](extinction-curse-bestiary-items/WggmVdmCE4CvyNXL.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[WgQCSrHux9sfsvj3.htm](extinction-curse-bestiary-items/WgQCSrHux9sfsvj3.htm)|Trident|Trident|modificada|
|[wKH8WPFicTjEMDpB.htm](extinction-curse-bestiary-items/wKH8WPFicTjEMDpB.htm)|Draining Touch|Toque Drenante|modificada|
|[WL6FySYpcxtf9ULD.htm](extinction-curse-bestiary-items/WL6FySYpcxtf9ULD.htm)|Wing|Ala|modificada|
|[WLQETe40LtIHyPGa.htm](extinction-curse-bestiary-items/WLQETe40LtIHyPGa.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[wM1opIZyPoYqYrGh.htm](extinction-curse-bestiary-items/wM1opIZyPoYqYrGh.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[wmFjSXvEH8SqQPNG.htm](extinction-curse-bestiary-items/wmFjSXvEH8SqQPNG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wMph0ESDRk2zz1Ff.htm](extinction-curse-bestiary-items/wMph0ESDRk2zz1Ff.htm)|Juggernaut|Juggernaut|modificada|
|[WmQ1U0HMX7ayTqMC.htm](extinction-curse-bestiary-items/WmQ1U0HMX7ayTqMC.htm)|Unsettling Movement|Movimiento Inquietante|modificada|
|[Wn8lDlqkYOulrZju.htm](extinction-curse-bestiary-items/Wn8lDlqkYOulrZju.htm)|Negative Healing|Curación negativa|modificada|
|[wnbjJLnvmvIEGia9.htm](extinction-curse-bestiary-items/wnbjJLnvmvIEGia9.htm)|Choke Slam|Choke Slam|modificada|
|[wPnZZSRGb9t9kP6p.htm](extinction-curse-bestiary-items/wPnZZSRGb9t9kP6p.htm)|Dagger|Daga|modificada|
|[Wq1u7bRZlmf8iWH5.htm](extinction-curse-bestiary-items/Wq1u7bRZlmf8iWH5.htm)|Powerful Stench|Hedor intenso|modificada|
|[wrAEK8zz0CszI6AJ.htm](extinction-curse-bestiary-items/wrAEK8zz0CszI6AJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ws10H9jL2IqUsARn.htm](extinction-curse-bestiary-items/ws10H9jL2IqUsARn.htm)|Sudden Slices|Sudden Slices|modificada|
|[wSygxDqpllEezbzw.htm](extinction-curse-bestiary-items/wSygxDqpllEezbzw.htm)|Staff|Báculo|modificada|
|[WtzTULcqCIDli7Sh.htm](extinction-curse-bestiary-items/WtzTULcqCIDli7Sh.htm)|Kukri|Kukri|modificada|
|[wUOW3RgVxG2HQmH0.htm](extinction-curse-bestiary-items/wUOW3RgVxG2HQmH0.htm)|Pseudopod|Pseudópodo|modificada|
|[wv4csg9YxPFOP1Pm.htm](extinction-curse-bestiary-items/wv4csg9YxPFOP1Pm.htm)|Infused Items|Equipos infundidos|modificada|
|[WV6I29RGQdzGq17J.htm](extinction-curse-bestiary-items/WV6I29RGQdzGq17J.htm)|Claw|Garra|modificada|
|[wW41T2Rqe1HsYWiK.htm](extinction-curse-bestiary-items/wW41T2Rqe1HsYWiK.htm)|Repair Mode|Modo de reparación|modificada|
|[Ww4pKd6IZaOBRAbN.htm](extinction-curse-bestiary-items/Ww4pKd6IZaOBRAbN.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[wYMjLcGkMx9LTbKi.htm](extinction-curse-bestiary-items/wYMjLcGkMx9LTbKi.htm)|+1 Striking Falchion|+1 Golpe Falchion|modificada|
|[X4jmRoph9IEyOVDv.htm](extinction-curse-bestiary-items/X4jmRoph9IEyOVDv.htm)|Light Step|Pisada ligera|modificada|
|[X5jrkgJhRyd0x78g.htm](extinction-curse-bestiary-items/X5jrkgJhRyd0x78g.htm)|Earthen Blow|Golpe de Tierra|modificada|
|[x8bFDzkhGGqdhVb2.htm](extinction-curse-bestiary-items/x8bFDzkhGGqdhVb2.htm)|Inexorable March|Marcha inexorable|modificada|
|[X8zCQwR4m0Kqzz0U.htm](extinction-curse-bestiary-items/X8zCQwR4m0Kqzz0U.htm)|Coven Spells|Coven Spells|modificada|
|[x8zvyv0LEKpl4zb1.htm](extinction-curse-bestiary-items/x8zvyv0LEKpl4zb1.htm)|Pack Attack|Ataque en manada|modificada|
|[xAhm4AbqiD9yy2Or.htm](extinction-curse-bestiary-items/xAhm4AbqiD9yy2Or.htm)|Cleaver|Cleaver|modificada|
|[XB84vRtRawFpT6yg.htm](extinction-curse-bestiary-items/XB84vRtRawFpT6yg.htm)|Insidious Mummy Rot|Putridez de momia agravada|modificada|
|[xc31WWNLp2WBfKhJ.htm](extinction-curse-bestiary-items/xc31WWNLp2WBfKhJ.htm)|Greatclub|Greatclub|modificada|
|[xcCjk9YBXJF1CPRT.htm](extinction-curse-bestiary-items/xcCjk9YBXJF1CPRT.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[Xck3GduL0mgbVURO.htm](extinction-curse-bestiary-items/Xck3GduL0mgbVURO.htm)|Early Collapse|Early Collapse|modificada|
|[xD2fbKZSefTlgsmQ.htm](extinction-curse-bestiary-items/xD2fbKZSefTlgsmQ.htm)|Failure Vulnerability|Vulnerabilidad al fracaso|modificada|
|[xDvNX3A7fi4Fr7Rj.htm](extinction-curse-bestiary-items/xDvNX3A7fi4Fr7Rj.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[xdZGT2FQbkaAnnVv.htm](extinction-curse-bestiary-items/xdZGT2FQbkaAnnVv.htm)|Stench|Hedor|modificada|
|[Xe68wniGERn685DL.htm](extinction-curse-bestiary-items/Xe68wniGERn685DL.htm)|Longsword|Longsword|modificada|
|[Xg6Mx6eG3Oigbdmv.htm](extinction-curse-bestiary-items/Xg6Mx6eG3Oigbdmv.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[XGl6e6PBFVB4qmjB.htm](extinction-curse-bestiary-items/XGl6e6PBFVB4qmjB.htm)|Play the Pipes|Tocar la gaita|modificada|
|[Xh9l2gZgUXuevWZW.htm](extinction-curse-bestiary-items/Xh9l2gZgUXuevWZW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[xhdtj1UGrpdoFfnG.htm](extinction-curse-bestiary-items/xhdtj1UGrpdoFfnG.htm)|Rapier|Estoque|modificada|
|[XhQyqq54ULAcj0tz.htm](extinction-curse-bestiary-items/XhQyqq54ULAcj0tz.htm)|Dagger|Daga|modificada|
|[xHsqXGMqu6bMLRTB.htm](extinction-curse-bestiary-items/xHsqXGMqu6bMLRTB.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[xINgpEoG6H55cFZu.htm](extinction-curse-bestiary-items/xINgpEoG6H55cFZu.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[XKYMPRy9Hv28Tbkk.htm](extinction-curse-bestiary-items/XKYMPRy9Hv28Tbkk.htm)|Three-Limbed Lunge|Three-Limbed Acometer|modificada|
|[Xmcr4PwRCrFQgjVC.htm](extinction-curse-bestiary-items/Xmcr4PwRCrFQgjVC.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[XMwEKtn4GT15Vkbz.htm](extinction-curse-bestiary-items/XMwEKtn4GT15Vkbz.htm)|Grab|Agarrado|modificada|
|[xnDIzYzOSJgR2VHr.htm](extinction-curse-bestiary-items/xnDIzYzOSJgR2VHr.htm)|Tusk|Tusk|modificada|
|[xNolyNRgNNCS7SrY.htm](extinction-curse-bestiary-items/xNolyNRgNNCS7SrY.htm)|Feasting Tentacles|Festín de Tentáculos|modificada|
|[XNTq0RqLFZcAzxUn.htm](extinction-curse-bestiary-items/XNTq0RqLFZcAzxUn.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[xnyGzApt0aAZnZYJ.htm](extinction-curse-bestiary-items/xnyGzApt0aAZnZYJ.htm)|Mist Escape|Niebla Huir|modificada|
|[Xo4wSqGnQ754izAW.htm](extinction-curse-bestiary-items/Xo4wSqGnQ754izAW.htm)|Devour Soul|Devorar alma|modificada|
|[xOGJ7uK7SF5xcrH2.htm](extinction-curse-bestiary-items/xOGJ7uK7SF5xcrH2.htm)|Vulnerable to Disintegrate|Vulnerable a desintegrar|modificada|
|[xP1DZ7qExdlGxTkQ.htm](extinction-curse-bestiary-items/xP1DZ7qExdlGxTkQ.htm)|Acidic Effluence|Acidic Effluence|modificada|
|[xQhi5H6icTLenWSG.htm](extinction-curse-bestiary-items/xQhi5H6icTLenWSG.htm)|Needles|Agujas|modificada|
|[XrGLOuWPI7PbY3JT.htm](extinction-curse-bestiary-items/XrGLOuWPI7PbY3JT.htm)|Stench|Hedor|modificada|
|[xrgMIiBqzQXebseK.htm](extinction-curse-bestiary-items/xrgMIiBqzQXebseK.htm)|Bloom|Bloom|modificada|
|[xSiITNruineLQZJU.htm](extinction-curse-bestiary-items/xSiITNruineLQZJU.htm)|Constant Spells|Constant Spells|modificada|
|[XSNIitnbXnIcTzSZ.htm](extinction-curse-bestiary-items/XSNIitnbXnIcTzSZ.htm)|Light Mace|Maza de luz|modificada|
|[XtcOTRGK7GJm59i6.htm](extinction-curse-bestiary-items/XtcOTRGK7GJm59i6.htm)|Shield Block|Bloquear con escudo|modificada|
|[xUBmTzpgqhcLUJDd.htm](extinction-curse-bestiary-items/xUBmTzpgqhcLUJDd.htm)|Javelin|Javelin|modificada|
|[XUhIXiVMEs1bgrP7.htm](extinction-curse-bestiary-items/XUhIXiVMEs1bgrP7.htm)|Bite|Muerdemuerde|modificada|
|[XUO5Lf0Nls7cYGwN.htm](extinction-curse-bestiary-items/XUO5Lf0Nls7cYGwN.htm)|Eagle Dive|Picado de águila|modificada|
|[xVc28OEoqZGKHa2g.htm](extinction-curse-bestiary-items/xVc28OEoqZGKHa2g.htm)|Convergent Tactics|Tácticas Convergentes|modificada|
|[xX5Ycq93HKRAsmvI.htm](extinction-curse-bestiary-items/xX5Ycq93HKRAsmvI.htm)|Light Up|Luz|modificada|
|[xyO3M6BDrtYA6yRv.htm](extinction-curse-bestiary-items/xyO3M6BDrtYA6yRv.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[y0FMfElX3eNgSQWh.htm](extinction-curse-bestiary-items/y0FMfElX3eNgSQWh.htm)|Dagger|Daga|modificada|
|[y0mBs7a3QdDfJbUQ.htm](extinction-curse-bestiary-items/y0mBs7a3QdDfJbUQ.htm)|Predator's Leap|Salto sin carrerilla del depredador|modificada|
|[Y0ySCcnmHvN9oQCd.htm](extinction-curse-bestiary-items/Y0ySCcnmHvN9oQCd.htm)|Iron Golem Poison|Veneno de gólem de hierro|modificada|
|[Y16FL2sBfoT0zbNY.htm](extinction-curse-bestiary-items/Y16FL2sBfoT0zbNY.htm)|Attack of Opportunity (Tentacle Only)|Ataque de oportunidad (sólo tentáculo)|modificada|
|[Y4Ox9DseRSG2Epun.htm](extinction-curse-bestiary-items/Y4Ox9DseRSG2Epun.htm)|Wendigo Remnants|Wendigo Remanentes|modificada|
|[Y7eL4NGth0AzOHCF.htm](extinction-curse-bestiary-items/Y7eL4NGth0AzOHCF.htm)|Change Shape|Change Shape|modificada|
|[Y7lD8hRHSgTJuVoF.htm](extinction-curse-bestiary-items/Y7lD8hRHSgTJuVoF.htm)|Grab|Agarrado|modificada|
|[Y8Ki9xMtjEbm4nIF.htm](extinction-curse-bestiary-items/Y8Ki9xMtjEbm4nIF.htm)|Horrid Visage|Horrid Visage|modificada|
|[yaj6h10oMEPUzkTo.htm](extinction-curse-bestiary-items/yaj6h10oMEPUzkTo.htm)|Jaws|Fauces|modificada|
|[yAYC82wKWwT3Yr5O.htm](extinction-curse-bestiary-items/yAYC82wKWwT3Yr5O.htm)|Fist|Puño|modificada|
|[yaYZxfRFuqY5ZKUv.htm](extinction-curse-bestiary-items/yaYZxfRFuqY5ZKUv.htm)|Trapped in the Maze|Atrapado en el Laberinto|modificada|
|[YC6l2GTlJST6HmzF.htm](extinction-curse-bestiary-items/YC6l2GTlJST6HmzF.htm)|Stoke the Fervent|Avivar a los fervientes|modificada|
|[YDEvyjWRnqtD024w.htm](extinction-curse-bestiary-items/YDEvyjWRnqtD024w.htm)|Jaws|Fauces|modificada|
|[YeXnVtwRynKtKDVy.htm](extinction-curse-bestiary-items/YeXnVtwRynKtKDVy.htm)|Defensive Assault|Asalto defensivo|modificada|
|[YFBnzaEl7UBKGHLo.htm](extinction-curse-bestiary-items/YFBnzaEl7UBKGHLo.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[yfi5yLVQJBHQQfgr.htm](extinction-curse-bestiary-items/yfi5yLVQJBHQQfgr.htm)|Whirlwind Strike|Golpe de torbellino|modificada|
|[YHBBkkagd6BLfiQo.htm](extinction-curse-bestiary-items/YHBBkkagd6BLfiQo.htm)|Deceitful Feast|Festín Engañoso|modificada|
|[yhUioD8pgXFVQiro.htm](extinction-curse-bestiary-items/yhUioD8pgXFVQiro.htm)|Feed on Emotion|Alimentarse de Emoción|modificada|
|[YIb66Svrt8CZOKjj.htm](extinction-curse-bestiary-items/YIb66Svrt8CZOKjj.htm)|Renewed Vigor|Vigor renovado|modificada|
|[yihwJb9tcVGBGiIv.htm](extinction-curse-bestiary-items/yihwJb9tcVGBGiIv.htm)|Thespian Aura|Aura de Tespian|modificada|
|[yItwou9rDi09XB5s.htm](extinction-curse-bestiary-items/yItwou9rDi09XB5s.htm)|Reflection of Evil|Reflejo del maligno|modificada|
|[Yj0Sq5ehsOc1b6S1.htm](extinction-curse-bestiary-items/Yj0Sq5ehsOc1b6S1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yJXuk1WCt5HI5tjm.htm](extinction-curse-bestiary-items/yJXuk1WCt5HI5tjm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Yk8dZyRSg1tLKIy8.htm](extinction-curse-bestiary-items/Yk8dZyRSg1tLKIy8.htm)|Swallow Whole|Engullir Todo|modificada|
|[yKLg0f7z9z5rM6iA.htm](extinction-curse-bestiary-items/yKLg0f7z9z5rM6iA.htm)|Self-Detonate|Self-Detonate|modificada|
|[yM2uUCjL6GUDVlF0.htm](extinction-curse-bestiary-items/yM2uUCjL6GUDVlF0.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[YM5j7CwSD2jKdyee.htm](extinction-curse-bestiary-items/YM5j7CwSD2jKdyee.htm)|Light Step|Pisada ligera|modificada|
|[yMe7KsfLbsSFSdYK.htm](extinction-curse-bestiary-items/yMe7KsfLbsSFSdYK.htm)|Remove Face|Quitar Cara|modificada|
|[YMzo8b4ZWBJo3wFO.htm](extinction-curse-bestiary-items/YMzo8b4ZWBJo3wFO.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Yo23Art466hO6dEO.htm](extinction-curse-bestiary-items/Yo23Art466hO6dEO.htm)|Bomb|Bomba|modificada|
|[YOtRTnyprnEC769l.htm](extinction-curse-bestiary-items/YOtRTnyprnEC769l.htm)|Spell Ambush|Emboscada sortílega.|modificada|
|[YPaGpuTkNn4M3jlK.htm](extinction-curse-bestiary-items/YPaGpuTkNn4M3jlK.htm)|Shortbow|Arco corto|modificada|
|[YphbnRiR9XKU2vJ0.htm](extinction-curse-bestiary-items/YphbnRiR9XKU2vJ0.htm)|Javelin|Javelin|modificada|
|[YPo8ttJxdRwBB28Q.htm](extinction-curse-bestiary-items/YPo8ttJxdRwBB28Q.htm)|Greater Frost Hand Crossbow|Ballesta de mano Gélida, mayor|modificada|
|[YPpyu3mkV5IecFy4.htm](extinction-curse-bestiary-items/YPpyu3mkV5IecFy4.htm)|Change Shape|Change Shape|modificada|
|[YRcfAf2JSSaoxUCv.htm](extinction-curse-bestiary-items/YRcfAf2JSSaoxUCv.htm)|Furious Claws|Garras furiosas|modificada|
|[Yrpk6vP1DrVPPPhK.htm](extinction-curse-bestiary-items/Yrpk6vP1DrVPPPhK.htm)|Foil the Hunt|Frustrar la caza|modificada|
|[YRWhIzukE4zO9fUH.htm](extinction-curse-bestiary-items/YRWhIzukE4zO9fUH.htm)|-2 to All Saves (If Hearstone is Lost)|-2 a todas las salvaciones (si se pierde Hearstone)|modificada|
|[yTuD5OYd94nyf8YO.htm](extinction-curse-bestiary-items/yTuD5OYd94nyf8YO.htm)|Consume Knowledge|Consume Conocimiento|modificada|
|[yU4tKXoO17Gqu0i5.htm](extinction-curse-bestiary-items/yU4tKXoO17Gqu0i5.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[yVBUPO8bNkZEb4gC.htm](extinction-curse-bestiary-items/yVBUPO8bNkZEb4gC.htm)|Eat Away|Eat Away|modificada|
|[yVbYX4gSvL0pm4Py.htm](extinction-curse-bestiary-items/yVbYX4gSvL0pm4Py.htm)|Inhabit Body|Habitar Cuerpo|modificada|
|[YWC5X9RpwpHTDgGR.htm](extinction-curse-bestiary-items/YWC5X9RpwpHTDgGR.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[YXTmEPsQrFyF3kDl.htm](extinction-curse-bestiary-items/YXTmEPsQrFyF3kDl.htm)|Juggler|Malabarista|modificada|
|[yyIFnrfdqZb4Oqeh.htm](extinction-curse-bestiary-items/yyIFnrfdqZb4Oqeh.htm)|Devastating Blast|Explosión devastadora|modificada|
|[YzXOUwdHiS1qUgkO.htm](extinction-curse-bestiary-items/YzXOUwdHiS1qUgkO.htm)|Constrict|Restringir|modificada|
|[Z0JKKHnRuGwdq3gP.htm](extinction-curse-bestiary-items/Z0JKKHnRuGwdq3gP.htm)|Minion|Esbirro|modificada|
|[z346znqdZJAlbClQ.htm](extinction-curse-bestiary-items/z346znqdZJAlbClQ.htm)|Jolting Buzz|Jolting Buzz|modificada|
|[z3tK3Qo5wCvyDf6a.htm](extinction-curse-bestiary-items/z3tK3Qo5wCvyDf6a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[z49XhuhTdNery690.htm](extinction-curse-bestiary-items/z49XhuhTdNery690.htm)|Trample|Trample|modificada|
|[z7LlEkjfkvJpEt7l.htm](extinction-curse-bestiary-items/z7LlEkjfkvJpEt7l.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[z7ONYn1k9pLAkFgo.htm](extinction-curse-bestiary-items/z7ONYn1k9pLAkFgo.htm)|War Flail|War Flail|modificada|
|[z8WlMl1wNjG6WMAp.htm](extinction-curse-bestiary-items/z8WlMl1wNjG6WMAp.htm)|Jaws|Fauces|modificada|
|[ZA4AaC9hSaCxwUee.htm](extinction-curse-bestiary-items/ZA4AaC9hSaCxwUee.htm)|Mirror Rejuventation|Rejuvenecimiento Espejo|modificada|
|[ZAxGEizKqPqUvP2N.htm](extinction-curse-bestiary-items/ZAxGEizKqPqUvP2N.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[zBa9WIu0MHIhcZTg.htm](extinction-curse-bestiary-items/zBa9WIu0MHIhcZTg.htm)|Summon Monster|Invocar Monstruo|modificada|
|[zbf9bW9X1MyCYPvF.htm](extinction-curse-bestiary-items/zbf9bW9X1MyCYPvF.htm)|Club|Club|modificada|
|[ZBkyhFcereMKMx5a.htm](extinction-curse-bestiary-items/ZBkyhFcereMKMx5a.htm)|Stench|Hedor|modificada|
|[ZBRB00Un9P2Q4aVv.htm](extinction-curse-bestiary-items/ZBRB00Un9P2Q4aVv.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[zdB58TAmkBBPd6q1.htm](extinction-curse-bestiary-items/zdB58TAmkBBPd6q1.htm)|Scimitar|Cimitarra|modificada|
|[zDG10Ne7cluh9cw0.htm](extinction-curse-bestiary-items/zDG10Ne7cluh9cw0.htm)|Black Cat Curse|Maldición del Gato Negro|modificada|
|[zDJDO6RiYek1vtDZ.htm](extinction-curse-bestiary-items/zDJDO6RiYek1vtDZ.htm)|Ectoplasmic Explosion|Explosión Ectoplásmica|modificada|
|[zDrqxe9jDHILx6v0.htm](extinction-curse-bestiary-items/zDrqxe9jDHILx6v0.htm)|Slow|Lentificado/a|modificada|
|[ZEVtz1rOhoQ04e8r.htm](extinction-curse-bestiary-items/ZEVtz1rOhoQ04e8r.htm)|Swipe|Vaivén|modificada|
|[zGEOz9aVguJujbSi.htm](extinction-curse-bestiary-items/zGEOz9aVguJujbSi.htm)|Mutations|Mutaciones|modificada|
|[ZGfedCFrmAmnidZZ.htm](extinction-curse-bestiary-items/ZGfedCFrmAmnidZZ.htm)|Jaws|Fauces|modificada|
|[ZgYetoNRvvpabxnK.htm](extinction-curse-bestiary-items/ZgYetoNRvvpabxnK.htm)|Foot|Pie|modificada|
|[ZhPo31BzeCCR8NiY.htm](extinction-curse-bestiary-items/ZhPo31BzeCCR8NiY.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zIOafMbE3v7YoQZc.htm](extinction-curse-bestiary-items/zIOafMbE3v7YoQZc.htm)|Claw|Garra|modificada|
|[ZJ9xQpaRJlhEZ2NV.htm](extinction-curse-bestiary-items/ZJ9xQpaRJlhEZ2NV.htm)|Breathe Fire|Breathe Fire|modificada|
|[zJLCfFihvadAJAdk.htm](extinction-curse-bestiary-items/zJLCfFihvadAJAdk.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zjQ2H1SrpbuuW3al.htm](extinction-curse-bestiary-items/zjQ2H1SrpbuuW3al.htm)|Negative Healing|Curación negativa|modificada|
|[zkM8zASAbYmKxFT4.htm](extinction-curse-bestiary-items/zkM8zASAbYmKxFT4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZmHokNmS9yqQ5FIy.htm](extinction-curse-bestiary-items/ZmHokNmS9yqQ5FIy.htm)|Horn|Cuerno|modificada|
|[ZmpXD5Zs0PQBXPU3.htm](extinction-curse-bestiary-items/ZmpXD5Zs0PQBXPU3.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[zNcTKoBJlpFDMTec.htm](extinction-curse-bestiary-items/zNcTKoBJlpFDMTec.htm)|Berserk Slam|Porrazo bersérker|modificada|
|[zNdfAHhn7D5xSrzh.htm](extinction-curse-bestiary-items/zNdfAHhn7D5xSrzh.htm)|Acid Flask|Frasco de ácido|modificada|
|[ZnvHJDLrVZdsku1Y.htm](extinction-curse-bestiary-items/ZnvHJDLrVZdsku1Y.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[zP8eq2K9H89vQwFw.htm](extinction-curse-bestiary-items/zP8eq2K9H89vQwFw.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[ZQrY1mv55wut2A9o.htm](extinction-curse-bestiary-items/ZQrY1mv55wut2A9o.htm)|Spring|Primavera|modificada|
|[Zr3FwVvEurlu8nhb.htm](extinction-curse-bestiary-items/Zr3FwVvEurlu8nhb.htm)|Acid Flask (Greater) (Infused)|Frasco de ácido (Mayor) (Infundido)|modificada|
|[ztjUvBPpUPyRPb5P.htm](extinction-curse-bestiary-items/ztjUvBPpUPyRPb5P.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[ztlZ2YqDidf7D2DN.htm](extinction-curse-bestiary-items/ztlZ2YqDidf7D2DN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZtVK4wU2SnZ0H3zl.htm](extinction-curse-bestiary-items/ZtVK4wU2SnZ0H3zl.htm)|Flaming Coal|Carbón Flamígera|modificada|
|[ZUghtcO2JEOGQOAq.htm](extinction-curse-bestiary-items/ZUghtcO2JEOGQOAq.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[zUL7K0vd1msnHEUE.htm](extinction-curse-bestiary-items/zUL7K0vd1msnHEUE.htm)|Fist|Puño|modificada|
|[ZV6wTo1kAYTI9DlU.htm](extinction-curse-bestiary-items/ZV6wTo1kAYTI9DlU.htm)|Drunken Rage|Furia de Borrachos|modificada|
|[zw5dzRRxnfMr1IBO.htm](extinction-curse-bestiary-items/zw5dzRRxnfMr1IBO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZwdP3qwfYd4W0BZ7.htm](extinction-curse-bestiary-items/ZwdP3qwfYd4W0BZ7.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[zwkgV9sWG9MJ5GIJ.htm](extinction-curse-bestiary-items/zwkgV9sWG9MJ5GIJ.htm)|Foot|Pie|modificada|
|[ZwS0gU5ghEmq2cOB.htm](extinction-curse-bestiary-items/ZwS0gU5ghEmq2cOB.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[ZwZxW282cbsJVc4E.htm](extinction-curse-bestiary-items/ZwZxW282cbsJVc4E.htm)|Dueling Parry|Parada de duelo|modificada|
|[zXdU4CPFolugmDo2.htm](extinction-curse-bestiary-items/zXdU4CPFolugmDo2.htm)|Infused Items|Equipos infundidos|modificada|
|[zY6dnugGXHyrRplD.htm](extinction-curse-bestiary-items/zY6dnugGXHyrRplD.htm)|Nimble Opportunist|Oportunista ágil|modificada|
|[ZyHfjNED06IY0URq.htm](extinction-curse-bestiary-items/ZyHfjNED06IY0URq.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[ZZdTJTrqMUiQj1UM.htm](extinction-curse-bestiary-items/ZZdTJTrqMUiQj1UM.htm)|Staff|Báculo|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0XWnW8LKuBLAUWDn.htm](extinction-curse-bestiary-items/0XWnW8LKuBLAUWDn.htm)|Face Mask|vacía|
|[1FdVVIvFCyUYO42c.htm](extinction-curse-bestiary-items/1FdVVIvFCyUYO42c.htm)|Athletics|vacía|
|[1WyeUsZoxRGbP2Ad.htm](extinction-curse-bestiary-items/1WyeUsZoxRGbP2Ad.htm)|Gozreh Lore|vacía|
|[47vQ5EzH5mCWzmvL.htm](extinction-curse-bestiary-items/47vQ5EzH5mCWzmvL.htm)|Baton Doused in Flammable Oil|vacía|
|[4TqbrL1nWjV7igKG.htm](extinction-curse-bestiary-items/4TqbrL1nWjV7igKG.htm)|Black Desert Lore|vacía|
|[5uceub19IdfqJ0Ze.htm](extinction-curse-bestiary-items/5uceub19IdfqJ0Ze.htm)|Warfare Lore|vacía|
|[6ULxQITrncDRTf0g.htm](extinction-curse-bestiary-items/6ULxQITrncDRTf0g.htm)|Clown Costume|vacía|
|[8aV587HlWQ9O8qCx.htm](extinction-curse-bestiary-items/8aV587HlWQ9O8qCx.htm)|Necklace of Bone and Gems|vacía|
|[8GwhfcNZYW7M1YqR.htm](extinction-curse-bestiary-items/8GwhfcNZYW7M1YqR.htm)|Anatomy Lore|vacía|
|[8lXF29Na3oJh3PrL.htm](extinction-curse-bestiary-items/8lXF29Na3oJh3PrL.htm)|Forest Lore|vacía|
|[AJPHV58xJ71hC3xz.htm](extinction-curse-bestiary-items/AJPHV58xJ71hC3xz.htm)|Keys to All the Cages in the Glen of Uncommon Wonders|vacía|
|[ANQsTNA3uzOJTguT.htm](extinction-curse-bestiary-items/ANQsTNA3uzOJTguT.htm)|Gray Robes|vacía|
|[asg1peHnq8Kfjber.htm](extinction-curse-bestiary-items/asg1peHnq8Kfjber.htm)|Eagle Garrison Badge|vacía|
|[AvIzHasAT7nWYhzG.htm](extinction-curse-bestiary-items/AvIzHasAT7nWYhzG.htm)|Athletics|vacía|
|[Axi77dW9fWlKe5ct.htm](extinction-curse-bestiary-items/Axi77dW9fWlKe5ct.htm)|Pot of Grease Paint|vacía|
|[C1rEWsXR5VEckDEo.htm](extinction-curse-bestiary-items/C1rEWsXR5VEckDEo.htm)|Criminal Lore|vacía|
|[C5gIfV30l6HcWkwS.htm](extinction-curse-bestiary-items/C5gIfV30l6HcWkwS.htm)|Aroden Lore|vacía|
|[cA7JHHD6mDUi7NQE.htm](extinction-curse-bestiary-items/cA7JHHD6mDUi7NQE.htm)|Infused Reagents|vacía|
|[COxiLfpFDrDT8lEG.htm](extinction-curse-bestiary-items/COxiLfpFDrDT8lEG.htm)|Bowling Pin|vacía|
|[dSGJjwStpSRNtqOu.htm](extinction-curse-bestiary-items/dSGJjwStpSRNtqOu.htm)|Gambling Lore|vacía|
|[Dw6gmSPBaL248eKB.htm](extinction-curse-bestiary-items/Dw6gmSPBaL248eKB.htm)|Shraen Lore|vacía|
|[ghqho4OQkcRArLP9.htm](extinction-curse-bestiary-items/ghqho4OQkcRArLP9.htm)|Hoe|vacía|
|[GkBHOD9XctIvvbNf.htm](extinction-curse-bestiary-items/GkBHOD9XctIvvbNf.htm)|Cleaver|vacía|
|[GkznkrnInAnqtWGs.htm](extinction-curse-bestiary-items/GkznkrnInAnqtWGs.htm)|Abberton Lore|vacía|
|[hCTilN2T9gjd8N5c.htm](extinction-curse-bestiary-items/hCTilN2T9gjd8N5c.htm)|Willowside Lore|vacía|
|[hRyOqsE8Eje4tw06.htm](extinction-curse-bestiary-items/hRyOqsE8Eje4tw06.htm)|Survival|vacía|
|[imLjlss8L8M0heAz.htm](extinction-curse-bestiary-items/imLjlss8L8M0heAz.htm)|Circus Lore|vacía|
|[jBwBgMthqpbCxl50.htm](extinction-curse-bestiary-items/jBwBgMthqpbCxl50.htm)|Circus Lore|vacía|
|[JlpJ89IL7ZyHUWgp.htm](extinction-curse-bestiary-items/JlpJ89IL7ZyHUWgp.htm)|Athletics|vacía|
|[jtlZEA7cK11MXoOX.htm](extinction-curse-bestiary-items/jtlZEA7cK11MXoOX.htm)|Criminal Lore|vacía|
|[jU8gscCDgYLL4jRa.htm](extinction-curse-bestiary-items/jU8gscCDgYLL4jRa.htm)|Acrobatics|vacía|
|[k5PXZtu7pMNd0Hx4.htm](extinction-curse-bestiary-items/k5PXZtu7pMNd0Hx4.htm)|Criminal Lore|vacía|
|[kdH6teRXPVQOdmdp.htm](extinction-curse-bestiary-items/kdH6teRXPVQOdmdp.htm)|Shoony Lore|vacía|
|[KnZpH9lLGq7oF6GH.htm](extinction-curse-bestiary-items/KnZpH9lLGq7oF6GH.htm)|Acrobatics|vacía|
|[kQzJhTzViDZxfegS.htm](extinction-curse-bestiary-items/kQzJhTzViDZxfegS.htm)|Bottle|vacía|
|[kV6UuNK9ctIBBWvX.htm](extinction-curse-bestiary-items/kV6UuNK9ctIBBWvX.htm)|Farming Lore|vacía|
|[MBzAQ6mXHGE5nAla.htm](extinction-curse-bestiary-items/MBzAQ6mXHGE5nAla.htm)|Key to the Ringmaster's Wagon|vacía|
|[n08h7X8lJXldUrxY.htm](extinction-curse-bestiary-items/n08h7X8lJXldUrxY.htm)|Gozreh Lore|vacía|
|[NlljUPjO3NTY37sa.htm](extinction-curse-bestiary-items/NlljUPjO3NTY37sa.htm)|Stealth|vacía|
|[nLZYrihD0JFqZ1pJ.htm](extinction-curse-bestiary-items/nLZYrihD0JFqZ1pJ.htm)|Circus Lore|vacía|
|[NTyx5yZkxCxGwJBJ.htm](extinction-curse-bestiary-items/NTyx5yZkxCxGwJBJ.htm)|Gambling Lore|vacía|
|[O6DDDWmQMfgrvOUU.htm](extinction-curse-bestiary-items/O6DDDWmQMfgrvOUU.htm)|Black Desert Lore|vacía|
|[OhLq6xthHdhj2mzP.htm](extinction-curse-bestiary-items/OhLq6xthHdhj2mzP.htm)|Bag of Faces|vacía|
|[OLaJWLiujGgz8HCl.htm](extinction-curse-bestiary-items/OLaJWLiujGgz8HCl.htm)|Stealth|vacía|
|[OUAUqe08Aktnirvq.htm](extinction-curse-bestiary-items/OUAUqe08Aktnirvq.htm)|Deception|vacía|
|[peIPD9LBmPiab3Co.htm](extinction-curse-bestiary-items/peIPD9LBmPiab3Co.htm)|Zevgavizeb Lore|vacía|
|[pkJg8aphLGRyaSdk.htm](extinction-curse-bestiary-items/pkJg8aphLGRyaSdk.htm)|Rat Hood|vacía|
|[PnbdGiGnDuyflxJa.htm](extinction-curse-bestiary-items/PnbdGiGnDuyflxJa.htm)|Crafting|vacía|
|[qgRGLReGps2La4FL.htm](extinction-curse-bestiary-items/qgRGLReGps2La4FL.htm)|Shraen Lore|vacía|
|[RDYHiUOFIIaDKt8y.htm](extinction-curse-bestiary-items/RDYHiUOFIIaDKt8y.htm)|Infused Reagents|vacía|
|[rjSNiLNccg17t8pc.htm](extinction-curse-bestiary-items/rjSNiLNccg17t8pc.htm)|Key to Strongbox in A10|vacía|
|[sgfHGUowMefpHneQ.htm](extinction-curse-bestiary-items/sgfHGUowMefpHneQ.htm)|Xulgath Lore|vacía|
|[sKd0ZJXJ5gco5WqC.htm](extinction-curse-bestiary-items/sKd0ZJXJ5gco5WqC.htm)|Underworld Lore|vacía|
|[TFVafGVljhShJjij.htm](extinction-curse-bestiary-items/TFVafGVljhShJjij.htm)|Intimidation|vacía|
|[TG7StlfXO7qcbaFy.htm](extinction-curse-bestiary-items/TG7StlfXO7qcbaFy.htm)|Cave Lore|vacía|
|[tp9ORpYsDj2Yd3XE.htm](extinction-curse-bestiary-items/tp9ORpYsDj2Yd3XE.htm)|Zevgavizeb Lore|vacía|
|[tzGttrmK2YxnEDXH.htm](extinction-curse-bestiary-items/tzGttrmK2YxnEDXH.htm)|Stealth|vacía|
|[U2pZ9Ib09IyARwgZ.htm](extinction-curse-bestiary-items/U2pZ9Ib09IyARwgZ.htm)|Nature|vacía|
|[uTDwnPruhh463nll.htm](extinction-curse-bestiary-items/uTDwnPruhh463nll.htm)|Geology Lore|vacía|
|[VTbk56rOHlsO8Mcf.htm](extinction-curse-bestiary-items/VTbk56rOHlsO8Mcf.htm)|Circus Lore|vacía|
|[wgAgCBHThMqRAxPI.htm](extinction-curse-bestiary-items/wgAgCBHThMqRAxPI.htm)|A Brush and Pot of Red Paint|vacía|
|[wPt51hV4BCFg08eR.htm](extinction-curse-bestiary-items/wPt51hV4BCFg08eR.htm)|Short Stool|vacía|
|[WqAoB01jXaEfzA7R.htm](extinction-curse-bestiary-items/WqAoB01jXaEfzA7R.htm)|Athletics|vacía|
|[WqZw6sQ6UuR8XHlz.htm](extinction-curse-bestiary-items/WqZw6sQ6UuR8XHlz.htm)|Lore|vacía|
|[WWVER4ZYSmZhye2k.htm](extinction-curse-bestiary-items/WWVER4ZYSmZhye2k.htm)|Theatre Lore|vacía|
|[XMV37fJggFZoHRuj.htm](extinction-curse-bestiary-items/XMV37fJggFZoHRuj.htm)|Daemon Lore|vacía|
|[XYsfQIAGwEG7CEHy.htm](extinction-curse-bestiary-items/XYsfQIAGwEG7CEHy.htm)|Cleaver|vacía|
|[ycvmtIzMkmfLy5hY.htm](extinction-curse-bestiary-items/ycvmtIzMkmfLy5hY.htm)|Dinosaur Lore|vacía|
|[yhk9Uo7Z0sSIDvpj.htm](extinction-curse-bestiary-items/yhk9Uo7Z0sSIDvpj.htm)|Bag of Faces|vacía|
|[yjqHfRdCFN6ZTk5o.htm](extinction-curse-bestiary-items/yjqHfRdCFN6ZTk5o.htm)|Constable's Badge|vacía|
|[Z1FtloAoiSz9VnEc.htm](extinction-curse-bestiary-items/Z1FtloAoiSz9VnEc.htm)|Athletics|vacía|
|[Zlt4tx3zlotDVMMd.htm](extinction-curse-bestiary-items/Zlt4tx3zlotDVMMd.htm)|Dinosaur Lore|vacía|
|[ZXW62ilDNKxEYwX1.htm](extinction-curse-bestiary-items/ZXW62ilDNKxEYwX1.htm)|Athletics|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
