# Estado de la traducción (outlaws-of-alkenstar-bestiary)

 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[zUHMrJZKcKIv6IpM.htm](outlaws-of-alkenstar-bestiary/zUHMrJZKcKIv6IpM.htm)|Daelum|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
