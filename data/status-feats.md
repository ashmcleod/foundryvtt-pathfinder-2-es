# Estado de la traducción (feats)

 * **modificada**: 3173
 * **oficial**: 799
 * **ninguna**: 48


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[class-02-0i4zOM1p0rFxnc5m.htm](feats/class-02-0i4zOM1p0rFxnc5m.htm)|Harrower Dedication|
|[class-02-uBr7wHtOS6KIhHnd.htm](feats/class-02-uBr7wHtOS6KIhHnd.htm)|Twilight Speaker Dedication|
|[class-04-0IVnIC6gQUdQyM8b.htm](feats/class-04-0IVnIC6gQUdQyM8b.htm)|Rebel's Map|
|[class-04-0ptK0blZehF3ABha.htm](feats/class-04-0ptK0blZehF3ABha.htm)|Tut-Tut|
|[class-04-13bMIkqXpKt9pxAm.htm](feats/class-04-13bMIkqXpKt9pxAm.htm)|You're an Embarrassment|
|[class-04-ATsP9JCBnzkwVQbl.htm](feats/class-04-ATsP9JCBnzkwVQbl.htm)|Harrow Casting|
|[class-04-dbGocwZV9vJslbgC.htm](feats/class-04-dbGocwZV9vJslbgC.htm)|Broadside Buckos|
|[class-04-KU6nyh8DyQ7NoRQj.htm](feats/class-04-KU6nyh8DyQ7NoRQj.htm)|Ostentatious Reload|
|[class-04-mnHUemd21MtmV9FV.htm](feats/class-04-mnHUemd21MtmV9FV.htm)|Empathic Envoy|
|[class-04-nqEqvZviKYuGVxLV.htm](feats/class-04-nqEqvZviKYuGVxLV.htm)|Restore Omen|
|[class-04-OqqytsF2RIjB0EPR.htm](feats/class-04-OqqytsF2RIjB0EPR.htm)|Devrin's Cunning Stance|
|[class-04-Q0cTWUptV3uRIAIr.htm](feats/class-04-Q0cTWUptV3uRIAIr.htm)|Experienced Harrower|
|[class-04-woQhda2ZoO1GMYNz.htm](feats/class-04-woQhda2ZoO1GMYNz.htm)|Devrin's Dazzling Diversion|
|[class-06-7SwpVfxSkPMPMazJ.htm](feats/class-06-7SwpVfxSkPMPMazJ.htm)|Feeling Your Oats|
|[class-06-9xcvJn3lWXwbkU52.htm](feats/class-06-9xcvJn3lWXwbkU52.htm)|Watch This!|
|[class-06-cXuycCR29sFLBjbo.htm](feats/class-06-cXuycCR29sFLBjbo.htm)|Betraying Shank|
|[class-06-JgM1DMPcceeHYhQ8.htm](feats/class-06-JgM1DMPcceeHYhQ8.htm)|Harrow Ritualist|
|[class-06-JoZRbbexF3epeREe.htm](feats/class-06-JoZRbbexF3epeREe.htm)|Benevolent Spirit Deck|
|[class-06-PMvwXlZHHFuPGrPD.htm](feats/class-06-PMvwXlZHHFuPGrPD.htm)|Nightwave Springing Reload|
|[class-06-z8NUh2FayDa7Ctnp.htm](feats/class-06-z8NUh2FayDa7Ctnp.htm)|Vengeful Spirit Deck|
|[class-06-ZGgFrQAQWk0keaFW.htm](feats/class-06-ZGgFrQAQWk0keaFW.htm)|Disarming Smile|
|[class-06-zZZmRwvtwQazhU0b.htm](feats/class-06-zZZmRwvtwQazhU0b.htm)|Upset Balance|
|[class-08-6VWLyjurMGi33XAT.htm](feats/class-08-6VWLyjurMGi33XAT.htm)|Reading the Signs|
|[class-08-9tXGk4h9Bw3Wcnra.htm](feats/class-08-9tXGk4h9Bw3Wcnra.htm)|Helt's Spelldance|
|[class-08-CL43gGiErw5FUtFQ.htm](feats/class-08-CL43gGiErw5FUtFQ.htm)|Living for the Applause|
|[class-08-D7g9z4MuIeS4iRwN.htm](feats/class-08-D7g9z4MuIeS4iRwN.htm)|Public Execution|
|[class-08-DMjfZvWD20SGuIO4.htm](feats/class-08-DMjfZvWD20SGuIO4.htm)|Show-Off|
|[class-08-P092yzkGN4UYYVXB.htm](feats/class-08-P092yzkGN4UYYVXB.htm)|Statement Strut|
|[class-08-thNurw2OnN9jpBGV.htm](feats/class-08-thNurw2OnN9jpBGV.htm)|World-Wise Vigilance|
|[class-10-BV9k3nmVrWDLv8z6.htm](feats/class-10-BV9k3nmVrWDLv8z6.htm)|Emphatic Emissary|
|[class-10-lk80TBn933RECRD7.htm](feats/class-10-lk80TBn933RECRD7.htm)|Dazzling Display|
|[class-10-PfnlKlLuqKOOhLyK.htm](feats/class-10-PfnlKlLuqKOOhLyK.htm)|Big Debut|
|[class-10-WlgaSpTSGQQrHKlx.htm](feats/class-10-WlgaSpTSGQQrHKlx.htm)|Mockingbird's Disarm|
|[class-10-WSndnu3scUYWmbki.htm](feats/class-10-WSndnu3scUYWmbki.htm)|Hook 'Em|
|[class-12-arnSyCl9XfN1qviJ.htm](feats/class-12-arnSyCl9XfN1qviJ.htm)|Acknowledge Fan|
|[class-12-X62yfg4vky1JMnf1.htm](feats/class-12-X62yfg4vky1JMnf1.htm)|Illusory Identity|
|[class-14-hPfqQpiq6W8RPCxz.htm](feats/class-14-hPfqQpiq6W8RPCxz.htm)|It Was Me All Along!|
|[skill-02-5YlHrBCZJmb6Q4Lz.htm](feats/skill-02-5YlHrBCZJmb6Q4Lz.htm)|Slippery Prey|
|[skill-02-bJ4tF95Byy7wCS65.htm](feats/skill-02-bJ4tF95Byy7wCS65.htm)|Rolling Landing|
|[skill-02-bmtf49jRKBCfdC5W.htm](feats/skill-02-bmtf49jRKBCfdC5W.htm)|Half-Truths|
|[skill-02-IyJyZ1VQxS58Nk6V.htm](feats/skill-02-IyJyZ1VQxS58Nk6V.htm)|Eyes of the City|
|[skill-02-Z7M3ImHWqUyGu6cN.htm](feats/skill-02-Z7M3ImHWqUyGu6cN.htm)|Fleeing Diversion|
|[skill-03-xduV7Kyoi5OWvMEK.htm](feats/skill-03-xduV7Kyoi5OWvMEK.htm)|We're on the List|
|[skill-05-1MZ4WNoNoJ4jj5Z0.htm](feats/skill-05-1MZ4WNoNoJ4jj5Z0.htm)|Cutting Flattery|
|[skill-07-80AC6Q1m0eQiE7Et.htm](feats/skill-07-80AC6Q1m0eQiE7Et.htm)|Tumbling Theft|
|[skill-07-AHchBBO8lXCCuVxT.htm](feats/skill-07-AHchBBO8lXCCuVxT.htm)|Grudging Compliment|
|[skill-08-tjnL4wsriZugnHLn.htm](feats/skill-08-tjnL4wsriZugnHLn.htm)|Ilverani Purist|
|[skill-13-g0fyway0FkdSo7ZE.htm](feats/skill-13-g0fyway0FkdSo7ZE.htm)|Quick Spring|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[ancestry-01-0198lXWmDdrVWolN.htm](feats/ancestry-01-0198lXWmDdrVWolN.htm)|Wind Pillow|Wind Pillow|modificada|
|[ancestry-01-0DSCucjk9WZAw4xT.htm](feats/ancestry-01-0DSCucjk9WZAw4xT.htm)|Automaton Armament|Armamento Autómata|modificada|
|[ancestry-01-0fTHqIqXCwGKiykR.htm](feats/ancestry-01-0fTHqIqXCwGKiykR.htm)|Sprite's Spark|Sprite's Spark|modificada|
|[ancestry-01-0qeYP84FfQueggkx.htm](feats/ancestry-01-0qeYP84FfQueggkx.htm)|Hard Tail|Cola Dura|modificada|
|[ancestry-01-0qshtd4mBcjFAxA8.htm](feats/ancestry-01-0qshtd4mBcjFAxA8.htm)|Cantorian Reinforcement|Refuerzo cantoriano|modificada|
|[ancestry-01-13zaW8ZHRWnRe2pj.htm](feats/ancestry-01-13zaW8ZHRWnRe2pj.htm)|Hobgoblin Lore|Hobgoblin Lore|modificada|
|[ancestry-01-1Doigqr1vBzg0tWU.htm](feats/ancestry-01-1Doigqr1vBzg0tWU.htm)|Silent Stone|Piedra Silenciosa|modificada|
|[ancestry-01-1HsH8hE79MDsi8kK.htm](feats/ancestry-01-1HsH8hE79MDsi8kK.htm)|Orc Warmask|Orc Warmask|modificada|
|[ancestry-01-1MqvE1FL2mZRCnzo.htm](feats/ancestry-01-1MqvE1FL2mZRCnzo.htm)|Vestigial Wings|Vestigial Wings|modificada|
|[ancestry-01-1newzNV5nkLvZ9KE.htm](feats/ancestry-01-1newzNV5nkLvZ9KE.htm)|Handy with your Paws|Handy with your Paws|modificada|
|[ancestry-01-1r514EjD9YrZJ5rk.htm](feats/ancestry-01-1r514EjD9YrZJ5rk.htm)|Android Lore|Android Lore|modificada|
|[ancestry-01-1RygexXEjCKuR3Ps.htm](feats/ancestry-01-1RygexXEjCKuR3Ps.htm)|Undaunted|Intrépido|modificada|
|[ancestry-01-1tVC0mcxl8sTCg4U.htm](feats/ancestry-01-1tVC0mcxl8sTCg4U.htm)|Orc Weapon Familiarity|Familiaridad con las armas de los orcos|modificada|
|[ancestry-01-1xDMyQ8IHuhDHSXy.htm](feats/ancestry-01-1xDMyQ8IHuhDHSXy.htm)|Conrasu Weapon Familiarity|Familiaridad con las armas de los conrasu|modificada|
|[ancestry-01-1XuytsxHDUMgyVH1.htm](feats/ancestry-01-1XuytsxHDUMgyVH1.htm)|Climbing Tail|Cola Trepadora|modificada|
|[ancestry-01-23F2oqjL7SAMW3Ud.htm](feats/ancestry-01-23F2oqjL7SAMW3Ud.htm)|Ru-shi|Ru-shi|modificada|
|[ancestry-01-2abNd2shBjF2kz2S.htm](feats/ancestry-01-2abNd2shBjF2kz2S.htm)|Undead Empathy|Empatía de muerto viviente|modificada|
|[ancestry-01-2ebcYbg68pCZfAFQ.htm](feats/ancestry-01-2ebcYbg68pCZfAFQ.htm)|Halfling Weapon Familiarity|Halfling Arma Familiaridad|modificada|
|[ancestry-01-2XmdYW8OsAvjGDG3.htm](feats/ancestry-01-2XmdYW8OsAvjGDG3.htm)|Radiant Circuitry|Circuito Radiante|modificada|
|[ancestry-01-30e9SdbmI3t5OJQE.htm](feats/ancestry-01-30e9SdbmI3t5OJQE.htm)|Deliberate Death|Muerte Deliberada|modificada|
|[ancestry-01-3J0NxeHcA1h9eToK.htm](feats/ancestry-01-3J0NxeHcA1h9eToK.htm)|Creative Prodigy|Prodigio Creativo|modificada|
|[ancestry-01-3zr5Gt5LgHsMNpSO.htm](feats/ancestry-01-3zr5Gt5LgHsMNpSO.htm)|Gnome Weapon Familiarity|Familiaridad con las armas de los gnomos|modificada|
|[ancestry-01-48X3xSWxI20RPOr9.htm](feats/ancestry-01-48X3xSWxI20RPOr9.htm)|Burrow Elocutionist|Profesor de de elocución de excavadores|modificada|
|[ancestry-01-49mBVNF4SK6iYdJm.htm](feats/ancestry-01-49mBVNF4SK6iYdJm.htm)|Unconventional Weaponry|Armas poco convencionales|modificada|
|[ancestry-01-4NKyZVkmWjDyyIYZ.htm](feats/ancestry-01-4NKyZVkmWjDyyIYZ.htm)|Elven Weapon Familiarity|Familiaridad con las armas de los elfos|modificada|
|[ancestry-01-50CRpoP5XS1MVbu8.htm](feats/ancestry-01-50CRpoP5XS1MVbu8.htm)|Titan Slinger|Titán de la honda|modificada|
|[ancestry-01-5lZBacKOZOgIx4Pi.htm](feats/ancestry-01-5lZBacKOZOgIx4Pi.htm)|Eidetic Ear|Oído eidético|modificada|
|[ancestry-01-5nPtGnXii1WTM5Wt.htm](feats/ancestry-01-5nPtGnXii1WTM5Wt.htm)|Faultspawn|Faultspawn|modificada|
|[ancestry-01-5qJBjjitpHjkX5Wh.htm](feats/ancestry-01-5qJBjjitpHjkX5Wh.htm)|Embodied Legionary Subjectivity|La subjetividad legionaria encarnada|modificada|
|[ancestry-01-5vipJDU5hGs0SejD.htm](feats/ancestry-01-5vipJDU5hGs0SejD.htm)|Hyena Familiar|Hyena Familiar|modificada|
|[ancestry-01-5VWItcyt3Mx6mbSK.htm](feats/ancestry-01-5VWItcyt3Mx6mbSK.htm)|Grippli Lore|Saber grippli|modificada|
|[ancestry-01-6dymMIqs3dAFqnyR.htm](feats/ancestry-01-6dymMIqs3dAFqnyR.htm)|Plumekith|Plumekith|modificada|
|[ancestry-01-6EgHimhCuS8L0xDE.htm](feats/ancestry-01-6EgHimhCuS8L0xDE.htm)|Explosive Savant|Explosión Savant|modificada|
|[ancestry-01-6ENHSzqg88J6dri6.htm](feats/ancestry-01-6ENHSzqg88J6dri6.htm)|Elemental Eyes|Ojos Elementales|modificada|
|[ancestry-01-6oJ1EdANHgYCfCeF.htm](feats/ancestry-01-6oJ1EdANHgYCfCeF.htm)|Sharp Fangs|Colmillos Afilados|modificada|
|[ancestry-01-6ZNSMR0lRSLwBJBe.htm](feats/ancestry-01-6ZNSMR0lRSLwBJBe.htm)|Spelunker|Espeleólogo|modificada|
|[ancestry-01-7BVl9lFf0wuTjBgM.htm](feats/ancestry-01-7BVl9lFf0wuTjBgM.htm)|Elf Atavism|Atavismo elfo|modificada|
|[ancestry-01-7lXCPeGMB3RrDVdS.htm](feats/ancestry-01-7lXCPeGMB3RrDVdS.htm)|Hard To Fool|Hard To Fool|modificada|
|[ancestry-01-8DDwqrYHie33cf6d.htm](feats/ancestry-01-8DDwqrYHie33cf6d.htm)|Genie Weapon Familiarity|Genie Arma Familiaridad|modificada|
|[ancestry-01-8nF3r3NHW2uSRgwb.htm](feats/ancestry-01-8nF3r3NHW2uSRgwb.htm)|Devil's Advocate|Abogado del Diablo|modificada|
|[ancestry-01-8nz6gvymeTvEfdo0.htm](feats/ancestry-01-8nz6gvymeTvEfdo0.htm)|Smashing Tail|Cola aplastante|modificada|
|[ancestry-01-8rKlptZ7ArtJ6cI8.htm](feats/ancestry-01-8rKlptZ7ArtJ6cI8.htm)|Vanth's Weapon Familiarity|Vanth's Weapon Familiarity (Familiaridad con las armas de Vanth)|modificada|
|[ancestry-01-95PmHIS041KiYIks.htm](feats/ancestry-01-95PmHIS041KiYIks.htm)|Reassuring Presence|Presencia reconfortante|modificada|
|[ancestry-01-9bnkx6VgcO5mOk5b.htm](feats/ancestry-01-9bnkx6VgcO5mOk5b.htm)|Dwarven Lore|Saber de los enanos|modificada|
|[ancestry-01-9eDA2RLnrBBlCvce.htm](feats/ancestry-01-9eDA2RLnrBBlCvce.htm)|Catch the Details|Captar los detalles|modificada|
|[ancestry-01-9OdLdxvH5M9FyYSm.htm](feats/ancestry-01-9OdLdxvH5M9FyYSm.htm)|Corgi Mount|Corgi Mount|modificada|
|[ancestry-01-A1RzkLCj7mXJI1IY.htm](feats/ancestry-01-A1RzkLCj7mXJI1IY.htm)|Unassuming Dedication|Dedicación sin pretensiones|modificada|
|[ancestry-01-AAYN41WtyMZaLV1N.htm](feats/ancestry-01-AAYN41WtyMZaLV1N.htm)|Native Waters|Aguas Nativas|modificada|
|[ancestry-01-af2fSePLvqMNNp0r.htm](feats/ancestry-01-af2fSePLvqMNNp0r.htm)|Wildborn Magic|Magia de la selva|modificada|
|[ancestry-01-AFXY5eM4IQuo5Ygj.htm](feats/ancestry-01-AFXY5eM4IQuo5Ygj.htm)|Speak With Bats|Habla con los murciélagos|modificada|
|[ancestry-01-aGpH394T1xr2ey88.htm](feats/ancestry-01-aGpH394T1xr2ey88.htm)|Pack Hunter|Cazador de manadas|modificada|
|[ancestry-01-AgR1OPBHDvwV5wKD.htm](feats/ancestry-01-AgR1OPBHDvwV5wKD.htm)|Unexpected Shift|Cambiante inesperado|modificada|
|[ancestry-01-AjdmoGFoSIyx1mxd.htm](feats/ancestry-01-AjdmoGFoSIyx1mxd.htm)|Intuitive Cooperation|Cooperación intuitiva|modificada|
|[ancestry-01-akrj9t0lTrndmf0q.htm](feats/ancestry-01-akrj9t0lTrndmf0q.htm)|Canopy Sight|Vista de dosel|modificada|
|[ancestry-01-AKrxQ2JuDObM8coY.htm](feats/ancestry-01-AKrxQ2JuDObM8coY.htm)|Animal Senses|Animal Senses|modificada|
|[ancestry-01-AL2KD88ddEl5AetZ.htm](feats/ancestry-01-AL2KD88ddEl5AetZ.htm)|Saber Teeth|Dientes de sable|modificada|
|[ancestry-01-AlhyF8LCW011w9kq.htm](feats/ancestry-01-AlhyF8LCW011w9kq.htm)|Bouncy Goblin|Goblin saltarín|modificada|
|[ancestry-01-AMbYEv9rUt2fcR7F.htm](feats/ancestry-01-AMbYEv9rUt2fcR7F.htm)|Energy Beam|Rayo de energía|modificada|
|[ancestry-01-an8sq0RaJ8PW81EW.htm](feats/ancestry-01-an8sq0RaJ8PW81EW.htm)|Puncturing Horn|Cuerno Punzante|modificada|
|[ancestry-01-AogIo1gHLdz7DyHx.htm](feats/ancestry-01-AogIo1gHLdz7DyHx.htm)|Forlorn|Abandonado|modificada|
|[ancestry-01-aOhZUMCn0o2ZMkdW.htm](feats/ancestry-01-aOhZUMCn0o2ZMkdW.htm)|Crunch|Mascar|modificada|
|[ancestry-01-AoibfCoLi2FaTvcH.htm](feats/ancestry-01-AoibfCoLi2FaTvcH.htm)|Hidden Thorn|Espina Escondida|modificada|
|[ancestry-01-ARBIhYAREcytOIaL.htm](feats/ancestry-01-ARBIhYAREcytOIaL.htm)|Helpful Poppet|Poppet útil|modificada|
|[ancestry-01-arR15QoJH3xokw12.htm](feats/ancestry-01-arR15QoJH3xokw12.htm)|Wind Tempered|Templado por el viento|modificada|
|[ancestry-01-augNhQ51eSlZyead.htm](feats/ancestry-01-augNhQ51eSlZyead.htm)|Sneaky|Sneaky|modificada|
|[ancestry-01-b60ZCgKoaVDgMhBk.htm](feats/ancestry-01-b60ZCgKoaVDgMhBk.htm)|Kitsune Lore|Kitsune Lore|modificada|
|[ancestry-01-BF5B2kDrSruQpqgS.htm](feats/ancestry-01-BF5B2kDrSruQpqgS.htm)|Pyrophilic Recovery|Recuperación pirofílica|modificada|
|[ancestry-01-bLSd0y06QRjDYYMw.htm](feats/ancestry-01-bLSd0y06QRjDYYMw.htm)|Mental Sustenance|Sustento Mental|modificada|
|[ancestry-01-Bn9yK3FBndHkbVsu.htm](feats/ancestry-01-Bn9yK3FBndHkbVsu.htm)|Spine Stabber|Acuchillador con púas de columna|modificada|
|[ancestry-01-BNIAfaJPNCWKs3FN.htm](feats/ancestry-01-BNIAfaJPNCWKs3FN.htm)|Nocturnal Grippli|Grippli nocturno|modificada|
|[ancestry-01-BoqMvGy1jXpsaBbo.htm](feats/ancestry-01-BoqMvGy1jXpsaBbo.htm)|Viking Shieldbearer|Viking Shieldbearer|modificada|
|[ancestry-01-BRGdj5leyVFRHEUM.htm](feats/ancestry-01-BRGdj5leyVFRHEUM.htm)|Waxed Feathers|Plumas Enceradas|modificada|
|[ancestry-01-C1R4wd6G46CAVIn7.htm](feats/ancestry-01-C1R4wd6G46CAVIn7.htm)|Unburdened Iron|No impedido por el hierro|modificada|
|[ancestry-01-Cb44J1g1nO43DEBd.htm](feats/ancestry-01-Cb44J1g1nO43DEBd.htm)|Ceremony of the Evened Hand|Ceremonia de la mano lisa|modificada|
|[ancestry-01-ccWf2F5DqiqFwiQ1.htm](feats/ancestry-01-ccWf2F5DqiqFwiQ1.htm)|Gnome Polyglot|Gnomo Políglota|modificada|
|[ancestry-01-CF43oiymCFVVEkVS.htm](feats/ancestry-01-CF43oiymCFVVEkVS.htm)|Automaton Lore|Automaton Lore|modificada|
|[ancestry-01-ch3BWG3Z4kDEmuZW.htm](feats/ancestry-01-ch3BWG3Z4kDEmuZW.htm)|Alghollthu Bound|Alghollthu Bound|modificada|
|[ancestry-01-cilZUszwjSGB4p1W.htm](feats/ancestry-01-cilZUszwjSGB4p1W.htm)|Reinforced Chassis|Chasis reforzado|modificada|
|[ancestry-01-cixf1uAQF2Y3w1Qx.htm](feats/ancestry-01-cixf1uAQF2Y3w1Qx.htm)|Nightvision Adaptation|Nightvision Adaptation|modificada|
|[ancestry-01-ClVSyZxWk5L5KVLd.htm](feats/ancestry-01-ClVSyZxWk5L5KVLd.htm)|Aberration Kinship|Parentesco Aberración|modificada|
|[ancestry-01-csoGdGuWasEw3boD.htm](feats/ancestry-01-csoGdGuWasEw3boD.htm)|Saoc Astrology|Saoc Astrology|modificada|
|[ancestry-01-CUrxG9CzT1hSfuhP.htm](feats/ancestry-01-CUrxG9CzT1hSfuhP.htm)|Web Weaver|Tejedora de telaras|modificada|
|[ancestry-01-CvtJ98EZvBGSCLOX.htm](feats/ancestry-01-CvtJ98EZvBGSCLOX.htm)|Grippli Weapon Familiarity|Familiaridad con las armas de los grippli|modificada|
|[ancestry-01-CXS0ryG2SODSobm9.htm](feats/ancestry-01-CXS0ryG2SODSobm9.htm)|Dwarven Weapon Familiarity|Familiaridad con las armas de los enanos|modificada|
|[ancestry-01-d1FTY5ai9KjpkX59.htm](feats/ancestry-01-d1FTY5ai9KjpkX59.htm)|Star Orb|Star Orb|modificada|
|[ancestry-01-D2tyUKQiDSq3JO1Z.htm](feats/ancestry-01-D2tyUKQiDSq3JO1Z.htm)|Vigorous Health|Salud Vigorosa|modificada|
|[ancestry-01-D3SuA5MaKucO1flE.htm](feats/ancestry-01-D3SuA5MaKucO1flE.htm)|General Training|Entrenamiento general|modificada|
|[ancestry-01-d8SK0BQmTZiJ0VT7.htm](feats/ancestry-01-d8SK0BQmTZiJ0VT7.htm)|Community Knowledge|Conocimiento de la comunidad|modificada|
|[ancestry-01-db0RHtFGhCfMx8vT.htm](feats/ancestry-01-db0RHtFGhCfMx8vT.htm)|Internal Cohesion|Cohesión interna|modificada|
|[ancestry-01-DmcJtpMBSh3R5MHI.htm](feats/ancestry-01-DmcJtpMBSh3R5MHI.htm)|Quick Shape|Quick Shape|modificada|
|[ancestry-01-dmEMftRe8P5iPDKo.htm](feats/ancestry-01-dmEMftRe8P5iPDKo.htm)|Brinesoul|Brinesoul|modificada|
|[ancestry-01-Dq4JSejEdCzGNeTc.htm](feats/ancestry-01-Dq4JSejEdCzGNeTc.htm)|Arcane Tattoos|Tatuajes Arcanos|modificada|
|[ancestry-01-Ds1waj86N4Z2gCMN.htm](feats/ancestry-01-Ds1waj86N4Z2gCMN.htm)|Winter Cat Senses|Sentidos de Gato de Invierno|modificada|
|[ancestry-01-dWGa6cFSVrASTEfd.htm](feats/ancestry-01-dWGa6cFSVrASTEfd.htm)|Seedpod|Vaina de semillas|modificada|
|[ancestry-01-DwnnmTNOvpLbp7jJ.htm](feats/ancestry-01-DwnnmTNOvpLbp7jJ.htm)|Courteous Comeback|Regreso Cortés|modificada|
|[ancestry-01-e0Gz3tjd55A5ggYK.htm](feats/ancestry-01-e0Gz3tjd55A5ggYK.htm)|Alabaster Eyes|Ojos de Alabastro|modificada|
|[ancestry-01-E1sjYRb4zsZVrzrN.htm](feats/ancestry-01-E1sjYRb4zsZVrzrN.htm)|Innocuous|Inocuo|modificada|
|[ancestry-01-E5TRKeB63rC910PC.htm](feats/ancestry-01-E5TRKeB63rC910PC.htm)|Nagaji Lore|Nagaji Lore|modificada|
|[ancestry-01-ecJms2jtd6cZ5rQK.htm](feats/ancestry-01-ecJms2jtd6cZ5rQK.htm)|Round Ears|Orejas Redondas|modificada|
|[ancestry-01-eD3KZJV8ACLt2xns.htm](feats/ancestry-01-eD3KZJV8ACLt2xns.htm)|Elven Verve|Brío élfico|modificada|
|[ancestry-01-ED58GzldWb82yc2q.htm](feats/ancestry-01-ED58GzldWb82yc2q.htm)|Animal Accomplice|Cómplice animal|modificada|
|[ancestry-01-EEJKztPOpy5utha9.htm](feats/ancestry-01-EEJKztPOpy5utha9.htm)|Cleansing Subroutine|Subrutina de limpieza|modificada|
|[ancestry-01-eEVZ19G2BAnHQZpa.htm](feats/ancestry-01-eEVZ19G2BAnHQZpa.htm)|Emotionless|Emotionless|modificada|
|[ancestry-01-EFhh3AenX7wtAmrs.htm](feats/ancestry-01-EFhh3AenX7wtAmrs.htm)|Orc Superstition|Superstición de orco|modificada|
|[ancestry-01-ehvHedIwPcxq9cRt.htm](feats/ancestry-01-ehvHedIwPcxq9cRt.htm)|Sure Feet|Pies seguros|modificada|
|[ancestry-01-enRKfPyCLU5FMUOX.htm](feats/ancestry-01-enRKfPyCLU5FMUOX.htm)|Proximity Alert|Alerta de proximidad|modificada|
|[ancestry-01-eSP2938INGUG9b3w.htm](feats/ancestry-01-eSP2938INGUG9b3w.htm)|Musetouched|Musetouched|modificada|
|[ancestry-01-eWUTE7Ln3MwX6uer.htm](feats/ancestry-01-eWUTE7Ln3MwX6uer.htm)|Axiomatic Lore|Axiomática Lore|modificada|
|[ancestry-01-eZsMl6rx8Bv6Ccnp.htm](feats/ancestry-01-eZsMl6rx8Bv6Ccnp.htm)|Shiny Button Eyes|Ojos de Botón Brillante|modificada|
|[ancestry-01-F6C25qZ9UNYPw7pj.htm](feats/ancestry-01-F6C25qZ9UNYPw7pj.htm)|Critter Shape|Critter Shape|modificada|
|[ancestry-01-f6VPqhOPW9XfBKDr.htm](feats/ancestry-01-f6VPqhOPW9XfBKDr.htm)|Virga May|Virga May|modificada|
|[ancestry-01-FhCsnHjdIUyKteCM.htm](feats/ancestry-01-FhCsnHjdIUyKteCM.htm)|Lizardfolk Lore|Lizardfolk Lore|modificada|
|[ancestry-01-FHK4ZEl8SkGOpKdF.htm](feats/ancestry-01-FHK4ZEl8SkGOpKdF.htm)|Mirror-Risen|Mirror-Risen|modificada|
|[ancestry-01-fLDhS0e6fBDjiCUA.htm](feats/ancestry-01-fLDhS0e6fBDjiCUA.htm)|Bone Magic|Magia de Hueso|modificada|
|[ancestry-01-FnGOkNyLF4w3FyqZ.htm](feats/ancestry-01-FnGOkNyLF4w3FyqZ.htm)|Fang Sharpener|Afilador de Colmillos|modificada|
|[ancestry-01-FWCULKnVXhSPL0ST.htm](feats/ancestry-01-FWCULKnVXhSPL0ST.htm)|Fey Fellowship|Camaradería feérica|modificada|
|[ancestry-01-FwMTsYc87uU2q4Ox.htm](feats/ancestry-01-FwMTsYc87uU2q4Ox.htm)|Beastbrood|Beastbrood|modificada|
|[ancestry-01-fYVFBnv9aVHv1UNg.htm](feats/ancestry-01-fYVFBnv9aVHv1UNg.htm)|Leshy Superstition|Superstición leshy (s), leshys (pl.)|modificada|
|[ancestry-01-FZuQPFnQ5UkBWLU9.htm](feats/ancestry-01-FZuQPFnQ5UkBWLU9.htm)|Steady On Stone|Steady On Stone|modificada|
|[ancestry-01-g388ImzpenYBoiEF.htm](feats/ancestry-01-g388ImzpenYBoiEF.htm)|City Scavenger|Carroro urbano|modificada|
|[ancestry-01-G8WgbujrrnMQUQ8E.htm](feats/ancestry-01-G8WgbujrrnMQUQ8E.htm)|Share Thoughts|Compartir pensamientos|modificada|
|[ancestry-01-gC5EnlP38t1vTlWt.htm](feats/ancestry-01-gC5EnlP38t1vTlWt.htm)|Esteemed Visitor|Estimado Visitante|modificada|
|[ancestry-01-GEvaoKgQteMrd4ub.htm](feats/ancestry-01-GEvaoKgQteMrd4ub.htm)|Reptile Speaker|Reptile Speaker|modificada|
|[ancestry-01-GF9kkULUYowgjEWM.htm](feats/ancestry-01-GF9kkULUYowgjEWM.htm)|Rough Rider|Jinete experto|modificada|
|[ancestry-01-gJDTS7eeIxZws5Lr.htm](feats/ancestry-01-gJDTS7eeIxZws5Lr.htm)|Adhyabhau|Adhyabhau|modificada|
|[ancestry-01-gKDRnsBPBdhJB0FI.htm](feats/ancestry-01-gKDRnsBPBdhJB0FI.htm)|Avenge in Glory|Vengarse con gloria|modificada|
|[ancestry-01-gozOYxLVx7PQvOSj.htm](feats/ancestry-01-gozOYxLVx7PQvOSj.htm)|Fey Cantrips|Fey Cantrips|modificada|
|[ancestry-01-GPsYtPdkAkk710F3.htm](feats/ancestry-01-GPsYtPdkAkk710F3.htm)|Suli-jann|Suli-jann|modificada|
|[ancestry-01-gsDDw5KAb7qlPkms.htm](feats/ancestry-01-gsDDw5KAb7qlPkms.htm)|Shrouded Magic|Shrouded Magic|modificada|
|[ancestry-01-GYdLf1LhvSUeu95Y.htm](feats/ancestry-01-GYdLf1LhvSUeu95Y.htm)|Harmlessly Cute|Inofensivamente bello|modificada|
|[ancestry-01-H95Gh2nKUp9NKFuR.htm](feats/ancestry-01-H95Gh2nKUp9NKFuR.htm)|Shadow Blending|Mezcla de Sombra|modificada|
|[ancestry-01-HAMy8UiUqCGFdhrf.htm](feats/ancestry-01-HAMy8UiUqCGFdhrf.htm)|Lemma Of Vision|Lemma De Vision|modificada|
|[ancestry-01-HauCZuLvIthFe2we.htm](feats/ancestry-01-HauCZuLvIthFe2we.htm)|Draconic Sycophant|Sicofante dracónico|modificada|
|[ancestry-01-Hey5rucsel7apOOi.htm](feats/ancestry-01-Hey5rucsel7apOOi.htm)|Goloma Courage|Coraje de goloma|modificada|
|[ancestry-01-hkMQGiTJCLVAWHy0.htm](feats/ancestry-01-hkMQGiTJCLVAWHy0.htm)|Surface Culture|Surface Culture|modificada|
|[ancestry-01-Hmgy0GJKIawAiqHE.htm](feats/ancestry-01-Hmgy0GJKIawAiqHE.htm)|Maiden's Mending|Remendar de la Doncella|modificada|
|[ancestry-01-HOUHs5rahwIsQoBf.htm](feats/ancestry-01-HOUHs5rahwIsQoBf.htm)|Stonecunning|Astucia de la roca|modificada|
|[ancestry-01-HOXxEa7sAeAxpKHb.htm](feats/ancestry-01-HOXxEa7sAeAxpKHb.htm)|Strix Lore|Strix Lore|modificada|
|[ancestry-01-hSzNtRNwrma81Eeq.htm](feats/ancestry-01-hSzNtRNwrma81Eeq.htm)|Life-Giving Magic|Magia que da la vida|modificada|
|[ancestry-01-HTy9bQsVkKnS8bLT.htm](feats/ancestry-01-HTy9bQsVkKnS8bLT.htm)|As in Life, So in Death|Como en la vida, así en la muerte|modificada|
|[ancestry-01-Hy8SPadSjukKq078.htm](feats/ancestry-01-Hy8SPadSjukKq078.htm)|Sinister Appearance|Apariencia Siniestra|modificada|
|[ancestry-01-HZJqMESaEHTfefz3.htm](feats/ancestry-01-HZJqMESaEHTfefz3.htm)|Cindersoul|Cindersoul|modificada|
|[ancestry-01-i5W5aGWEiyo1vt2d.htm](feats/ancestry-01-i5W5aGWEiyo1vt2d.htm)|Leshy Lore|Leshy Lore (s), leshys (pl.)|modificada|
|[ancestry-01-IAXWdFXwjFChojeb.htm](feats/ancestry-01-IAXWdFXwjFChojeb.htm)|Harmless Doll|Muñeca Inofensiva|modificada|
|[ancestry-01-ICLUKJc9P0LgwVyt.htm](feats/ancestry-01-ICLUKJc9P0LgwVyt.htm)|Brightsoul|Brightsoul|modificada|
|[ancestry-01-Iqa96LHn3Bs2xEJA.htm](feats/ancestry-01-Iqa96LHn3Bs2xEJA.htm)|Startling Appearance (Fleshwarp)|Aspecto sorprendente (modelado)|modificada|
|[ancestry-01-Iqv8qj7ym63YjexN.htm](feats/ancestry-01-Iqv8qj7ym63YjexN.htm)|Catfolk Dance|Catfolk Danza|modificada|
|[ancestry-01-iRztLGEK5OfjZTPg.htm](feats/ancestry-01-iRztLGEK5OfjZTPg.htm)|Remorseless Lash|Remorseless Lash|modificada|
|[ancestry-01-itjTPh76mZfJCBxQ.htm](feats/ancestry-01-itjTPh76mZfJCBxQ.htm)|Marsh Runner|Corredor del pantano|modificada|
|[ancestry-01-izuErgHfh90KctAL.htm](feats/ancestry-01-izuErgHfh90KctAL.htm)|Fetchling Lore|Fetchling Lore|modificada|
|[ancestry-01-j54VJmwwAQZBlS6J.htm](feats/ancestry-01-j54VJmwwAQZBlS6J.htm)|Anadi Lore|Saber de los anadi|modificada|
|[ancestry-01-J7BtszszVxpETMD7.htm](feats/ancestry-01-J7BtszszVxpETMD7.htm)|Retractable Claws|Garras retráctiles|modificada|
|[ancestry-01-jmW8aZ5JGH3m6dL6.htm](feats/ancestry-01-jmW8aZ5JGH3m6dL6.htm)|Warren Friend|Amigo Warren|modificada|
|[ancestry-01-jNDjLsqpq13RqzD4.htm](feats/ancestry-01-jNDjLsqpq13RqzD4.htm)|Studious Magic|Magia estudiosa|modificada|
|[ancestry-01-JoeepCWheQChcQ9s.htm](feats/ancestry-01-JoeepCWheQChcQ9s.htm)|Lavasoul|Lavasoul|modificada|
|[ancestry-01-jOwZMv5jtwdKSfLS.htm](feats/ancestry-01-jOwZMv5jtwdKSfLS.htm)|Sensitive Nose|Hocico sensible|modificada|
|[ancestry-01-JQTP0XdI1XVAvBIn.htm](feats/ancestry-01-JQTP0XdI1XVAvBIn.htm)|Ghoran Lore|Ghoran Lore|modificada|
|[ancestry-01-JS24EjgLYcHB9E3T.htm](feats/ancestry-01-JS24EjgLYcHB9E3T.htm)|Nanite Surge|Oleaje de Nanite|modificada|
|[ancestry-01-jxUPlkB2kFuZKXef.htm](feats/ancestry-01-jxUPlkB2kFuZKXef.htm)|Lesser Enhance Venom|Menor Enhance Venom|modificada|
|[ancestry-01-k31sg0xBIwvkfWyg.htm](feats/ancestry-01-k31sg0xBIwvkfWyg.htm)|Reptile Rider|Reptile Rider|modificada|
|[ancestry-01-kBxgo589ctJsBwJj.htm](feats/ancestry-01-kBxgo589ctJsBwJj.htm)|Conrasu Lore|Saber de los conrasus|modificada|
|[ancestry-01-kCO4r8NOm8E2T3eH.htm](feats/ancestry-01-kCO4r8NOm8E2T3eH.htm)|Nimble Elf|Elfo ágil|modificada|
|[ancestry-01-kDklfrprKTuTpcEE.htm](feats/ancestry-01-kDklfrprKTuTpcEE.htm)|Shadow of the Wilds|Sombra de los Salvajes|modificada|
|[ancestry-01-keMP6xVg4fMloczj.htm](feats/ancestry-01-keMP6xVg4fMloczj.htm)|Cel Rau|Cel Rau|modificada|
|[ancestry-01-kJfLBPtiVi5LQu0v.htm](feats/ancestry-01-kJfLBPtiVi5LQu0v.htm)|Nocturnal Charm|Hechizo Nocturno|modificada|
|[ancestry-01-KKUDZUbX3nDdME4K.htm](feats/ancestry-01-KKUDZUbX3nDdME4K.htm)|Watchful Gaze|Mirada vigilante|modificada|
|[ancestry-01-kqRFoXfErUFEndIs.htm](feats/ancestry-01-kqRFoXfErUFEndIs.htm)|Hydraulic Deflection|Desviación Hidráulica|modificada|
|[ancestry-01-KQVE4FsDd9RFpWRz.htm](feats/ancestry-01-KQVE4FsDd9RFpWRz.htm)|Improvisational Defender|Defensor improvisado|modificada|
|[ancestry-01-KRMdfPcNCE7AVsEo.htm](feats/ancestry-01-KRMdfPcNCE7AVsEo.htm)|Scholar's Inheritance|Herencia de erudito|modificada|
|[ancestry-01-KuD4Yplwcdolhjsu.htm](feats/ancestry-01-KuD4Yplwcdolhjsu.htm)|Overlooked Mastermind|Overlooked Mastermind|modificada|
|[ancestry-01-KXUVAI6SDtxwjO7t.htm](feats/ancestry-01-KXUVAI6SDtxwjO7t.htm)|Web Walker|Caminante de telara|modificada|
|[ancestry-01-KYTSvAEqK7KAyVwi.htm](feats/ancestry-01-KYTSvAEqK7KAyVwi.htm)|Hunter's Defense|Defensa del cazador|modificada|
|[ancestry-01-Kz4MLcrfXFrdKhyS.htm](feats/ancestry-01-Kz4MLcrfXFrdKhyS.htm)|Aquatic Eyes|Ojos Acuáticos|modificada|
|[ancestry-01-l0VBbymCVT1Qz9t9.htm](feats/ancestry-01-l0VBbymCVT1Qz9t9.htm)|Swift|Celeridad|modificada|
|[ancestry-01-L7fhh2RTCq4FlVSN.htm](feats/ancestry-01-L7fhh2RTCq4FlVSN.htm)|Strix Defender|Strix Defender|modificada|
|[ancestry-01-Lb8mrOF3W2VGSOpA.htm](feats/ancestry-01-Lb8mrOF3W2VGSOpA.htm)|Arcane Communication|Comunicación arcana|modificada|
|[ancestry-01-LbyNDCxFEkjp0iG7.htm](feats/ancestry-01-LbyNDCxFEkjp0iG7.htm)|Slink|Slink|modificada|
|[ancestry-01-liqDAd5xk2b3xzeE.htm](feats/ancestry-01-liqDAd5xk2b3xzeE.htm)|Quah Bond|Quah Bond|modificada|
|[ancestry-01-lLO0WSxE3tO3Ovsq.htm](feats/ancestry-01-lLO0WSxE3tO3Ovsq.htm)|Whitecape|Whitecape|modificada|
|[ancestry-01-LLQ5SO2c44uXDFJk.htm](feats/ancestry-01-LLQ5SO2c44uXDFJk.htm)|Tail Whip|Coletazo|modificada|
|[ancestry-01-lqhhvDJAjPw6LZy5.htm](feats/ancestry-01-lqhhvDJAjPw6LZy5.htm)|Smokesoul|Smokesoul|modificada|
|[ancestry-01-Lug2p9E065L05Rhi.htm](feats/ancestry-01-Lug2p9E065L05Rhi.htm)|Story Crooner|Story Crooner|modificada|
|[ancestry-01-LvVg83ZDj8mabcWF.htm](feats/ancestry-01-LvVg83ZDj8mabcWF.htm)|Clan Pistol|Pistola del Clan|modificada|
|[ancestry-01-lwLcUHQMOqfaNND4.htm](feats/ancestry-01-lwLcUHQMOqfaNND4.htm)|Cooperative Nature|Naturaleza cooperativa|modificada|
|[ancestry-01-lwzspz4dktZLgqlY.htm](feats/ancestry-01-lwzspz4dktZLgqlY.htm)|Wash Out|Lavar|modificada|
|[ancestry-01-M41PGiFlLE2tByUL.htm](feats/ancestry-01-M41PGiFlLE2tByUL.htm)|Jungle Strider|Zancada de la jungla|modificada|
|[ancestry-01-m9tKNsZQQjHdsmEN.htm](feats/ancestry-01-m9tKNsZQQjHdsmEN.htm)|Tupilaq Carver|Tupilaq Carver|modificada|
|[ancestry-01-mAAlean6DuWd3wDT.htm](feats/ancestry-01-mAAlean6DuWd3wDT.htm)|Open Mind|Mente Abierta|modificada|
|[ancestry-01-MbvPuMVy2VhftJgd.htm](feats/ancestry-01-MbvPuMVy2VhftJgd.htm)|Perfect Dive|Inmersión Perfecta|modificada|
|[ancestry-01-mmYAiK3x0UMcgiNv.htm](feats/ancestry-01-mmYAiK3x0UMcgiNv.htm)|Dualborn|Dualborn|modificada|
|[ancestry-01-MqDLaBypDyp1VQKg.htm](feats/ancestry-01-MqDLaBypDyp1VQKg.htm)|Shisk Lore|Saber de los shisks|modificada|
|[ancestry-01-mW9uhe3RorEJg6Mn.htm](feats/ancestry-01-mW9uhe3RorEJg6Mn.htm)|Nagaji Spell Familiarity|Nagaji Hechizo Familiaridad|modificada|
|[ancestry-01-mxhGH4FVYXJwb0BC.htm](feats/ancestry-01-mxhGH4FVYXJwb0BC.htm)|Gnome Obsession|Obsesión de gnomo|modificada|
|[ancestry-01-MXkklchuimVSHZyd.htm](feats/ancestry-01-MXkklchuimVSHZyd.htm)|Theoretical Acumen|Perspicacia teórica|modificada|
|[ancestry-01-N3xyK9keDm00oUY6.htm](feats/ancestry-01-N3xyK9keDm00oUY6.htm)|Inner Fire (Ifrit)|Inner Fire (Ifrit)|modificada|
|[ancestry-01-n7JXquVGxQOfrCsf.htm](feats/ancestry-01-n7JXquVGxQOfrCsf.htm)|Ancestral Insight|Perspicacia Ancestral|modificada|
|[ancestry-01-n9CBjyiB17srkyr4.htm](feats/ancestry-01-n9CBjyiB17srkyr4.htm)|Hobgoblin Weapon Familiarity|Hobgoblin Weapon Familiarity|modificada|
|[ancestry-01-nbWkPLiUxagX8dCw.htm](feats/ancestry-01-nbWkPLiUxagX8dCw.htm)|Shapechanger's Intuition|Intuición de Shapechanger|modificada|
|[ancestry-01-ng9H9flz4H6agiiV.htm](feats/ancestry-01-ng9H9flz4H6agiiV.htm)|Demonbane Warrior|Guerrero Demonbane|modificada|
|[ancestry-01-ngEd8PgsyAARuTQ9.htm](feats/ancestry-01-ngEd8PgsyAARuTQ9.htm)|Vengeful Hatred|Odio vengativo|modificada|
|[ancestry-01-NIUSBGMmdqhkYtmo.htm](feats/ancestry-01-NIUSBGMmdqhkYtmo.htm)|Rock Runner|Corredor de las rocas|modificada|
|[ancestry-01-NN1U8ifiURc0h4Fx.htm](feats/ancestry-01-NN1U8ifiURc0h4Fx.htm)|Pierce the Darkness|Traspasar la oscuridad|modificada|
|[ancestry-01-Nn80wBZYJZxcuKsJ.htm](feats/ancestry-01-Nn80wBZYJZxcuKsJ.htm)|Stone Face|Cara de Piedra|modificada|
|[ancestry-01-nnlzI9LSvhl94U16.htm](feats/ancestry-01-nnlzI9LSvhl94U16.htm)|Scuttle Up|Scuttle Up|modificada|
|[ancestry-01-nqtO5dNnxT4nZDbB.htm](feats/ancestry-01-nqtO5dNnxT4nZDbB.htm)|Surface Skimmer|Skimmer de superficie|modificada|
|[ancestry-01-NYcgVwO4xLerJ9lO.htm](feats/ancestry-01-NYcgVwO4xLerJ9lO.htm)|Progenitor Lore|Progenitor Lore|modificada|
|[ancestry-01-nyNsIePvpovlDAws.htm](feats/ancestry-01-nyNsIePvpovlDAws.htm)|Alchemical Scholar|Erudito alquímico|modificada|
|[ancestry-01-NySPRgD0FjZY2QGs.htm](feats/ancestry-01-NySPRgD0FjZY2QGs.htm)|Consult The Stars|Consult The Stars|modificada|
|[ancestry-01-o6MXyjSgavTzU5AS.htm](feats/ancestry-01-o6MXyjSgavTzU5AS.htm)|Gemsoul|Gemsoul|modificada|
|[ancestry-01-oeGowXO2P6rHbZfY.htm](feats/ancestry-01-oeGowXO2P6rHbZfY.htm)|Very Sneaky|Muy escurridizo|modificada|
|[ancestry-01-oeVRwtBlQjsSVtXV.htm](feats/ancestry-01-oeVRwtBlQjsSVtXV.htm)|Ghoran Weapon Familiarity|Ghoran Arma Familiaridad|modificada|
|[ancestry-01-ojp39fVYqFBGAw38.htm](feats/ancestry-01-ojp39fVYqFBGAw38.htm)|Natural Performer|Interprete Natural|modificada|
|[ancestry-01-ojykhaQkT8IU7ouc.htm](feats/ancestry-01-ojykhaQkT8IU7ouc.htm)|Emotional Partitions|Particiones Emocionales|modificada|
|[ancestry-01-OKSsFlHY5UKc4dKu.htm](feats/ancestry-01-OKSsFlHY5UKc4dKu.htm)|Elemental Assault|Asalto elemental|modificada|
|[ancestry-01-OqGNcUTqaZTp2YND.htm](feats/ancestry-01-OqGNcUTqaZTp2YND.htm)|Molten Wit|Molten Wit|modificada|
|[ancestry-01-oQzyeSUlKx6eHcZp.htm](feats/ancestry-01-oQzyeSUlKx6eHcZp.htm)|Vanara Weapon Familiarity|Vanara Weapon Familiarity|modificada|
|[ancestry-01-OTjRxyCtwPoqNFP2.htm](feats/ancestry-01-OTjRxyCtwPoqNFP2.htm)|Kobold Weapon Familiarity|Familiaridad con armas kobold|modificada|
|[ancestry-01-OYjzfTeWU7RJBT7v.htm](feats/ancestry-01-OYjzfTeWU7RJBT7v.htm)|Goblin Weapon Familiarity|Familiaridad con las armas de los goblin|modificada|
|[ancestry-01-OZtoTusMmCJymObT.htm](feats/ancestry-01-OZtoTusMmCJymObT.htm)|Leech-Clipper|Leech-Clipper|modificada|
|[ancestry-01-Pat4H0VbmApblZxc.htm](feats/ancestry-01-Pat4H0VbmApblZxc.htm)|Otherworldly Magic|Magia de otro mundo|modificada|
|[ancestry-01-pCck5GgKIiIPGGdy.htm](feats/ancestry-01-pCck5GgKIiIPGGdy.htm)|Veil May|Velo Mayo|modificada|
|[ancestry-01-pgsSUJehsKZXlRp7.htm](feats/ancestry-01-pgsSUJehsKZXlRp7.htm)|Vishkanya Lore|Vishkanya Lore|modificada|
|[ancestry-01-PlhPpdwIV0rIAJ8K.htm](feats/ancestry-01-PlhPpdwIV0rIAJ8K.htm)|Orc Ferocity|Ferocidad de orco|modificada|
|[ancestry-01-PMRfunXzC9YizHNZ.htm](feats/ancestry-01-PMRfunXzC9YizHNZ.htm)|Riftmarked|Riftmarked|modificada|
|[ancestry-01-PodajLVxqYSAqVox.htm](feats/ancestry-01-PodajLVxqYSAqVox.htm)|Natural Ambition|Ambición natural|modificada|
|[ancestry-01-pP4pbFBg5GAgcOE9.htm](feats/ancestry-01-pP4pbFBg5GAgcOE9.htm)|Snow May|Mayo de Nieve|modificada|
|[ancestry-01-pQQhrcj1u6hUUc8L.htm](feats/ancestry-01-pQQhrcj1u6hUUc8L.htm)|Keep Up Appearances|Mantener las apariencias|modificada|
|[ancestry-01-PsLne80WUsD4IFa6.htm](feats/ancestry-01-PsLne80WUsD4IFa6.htm)|Dig Quickly|Excava Rápidamente|modificada|
|[ancestry-01-ptEOt3lqjxUnAW62.htm](feats/ancestry-01-ptEOt3lqjxUnAW62.htm)|Ancient Memories|Recuerdos Antiguos|modificada|
|[ancestry-01-qCV04rZMty2TJBrX.htm](feats/ancestry-01-qCV04rZMty2TJBrX.htm)|Foxfire|Foxfire|modificada|
|[ancestry-01-qm1lIMLVdsQtVFT0.htm](feats/ancestry-01-qm1lIMLVdsQtVFT0.htm)|Stormsoul|Stormsoul|modificada|
|[ancestry-01-qpVB9F9DURW4Lti1.htm](feats/ancestry-01-qpVB9F9DURW4Lti1.htm)|Uncanny Agility|Agilidad asombrosa|modificada|
|[ancestry-01-QsPvKvt4S1PR7kr7.htm](feats/ancestry-01-QsPvKvt4S1PR7kr7.htm)|Elemental Trade|Comercio elemental|modificada|
|[ancestry-01-qT68NpouO667sUMA.htm](feats/ancestry-01-qT68NpouO667sUMA.htm)|Sudden Mindfulness|Mente Repentina|modificada|
|[ancestry-01-qVk9htKv47niPmXS.htm](feats/ancestry-01-qVk9htKv47niPmXS.htm)|Deep Vision|Visión Profunda|modificada|
|[ancestry-01-qvxHGcOUYVG7Tqt4.htm](feats/ancestry-01-qvxHGcOUYVG7Tqt4.htm)|Dongun Education|Dongun Education|modificada|
|[ancestry-01-qxQf0HiYBh26mCMT.htm](feats/ancestry-01-qxQf0HiYBh26mCMT.htm)|Shrouded Mien|Shrouded Mien|modificada|
|[ancestry-01-QygsOzswFQEYONO3.htm](feats/ancestry-01-QygsOzswFQEYONO3.htm)|Shackleborn|Shackleborn|modificada|
|[ancestry-01-qzalwa2Ze3dIqIrA.htm](feats/ancestry-01-qzalwa2Ze3dIqIrA.htm)|Lightning Tongue|Lengua de relámpago|modificada|
|[ancestry-01-QZb0Utg0WFPf2Qg0.htm](feats/ancestry-01-QZb0Utg0WFPf2Qg0.htm)|Goblin Song|Canción de goblin|modificada|
|[ancestry-01-qZStZXyAj6kSAKQo.htm](feats/ancestry-01-qZStZXyAj6kSAKQo.htm)|Vanara Lore|Vanara Lore|modificada|
|[ancestry-01-Rfyhlyql1GSoDkI2.htm](feats/ancestry-01-Rfyhlyql1GSoDkI2.htm)|Natural Skill|Habilidad natural|modificada|
|[ancestry-01-RJClD7YYsAFutxNs.htm](feats/ancestry-01-RJClD7YYsAFutxNs.htm)|Elemental Lore|Elemental Lore|modificada|
|[ancestry-01-RmiMUZlae6yGUyXY.htm](feats/ancestry-01-RmiMUZlae6yGUyXY.htm)|Haughty Obstinacy|Obstinación altanera|modificada|
|[ancestry-01-roC6o0xJ8hDzHIWY.htm](feats/ancestry-01-roC6o0xJ8hDzHIWY.htm)|Serpent's Tongue|Lengua de Serpiente|modificada|
|[ancestry-01-rswkPvZvEBXISH96.htm](feats/ancestry-01-rswkPvZvEBXISH96.htm)|Rimesoul|Rimesoul|modificada|
|[ancestry-01-rWiddU8fHl0TraoN.htm](feats/ancestry-01-rWiddU8fHl0TraoN.htm)|Gnoll Lore|Gnoll Lore|modificada|
|[ancestry-01-ryRpqixE7cN8svwB.htm](feats/ancestry-01-ryRpqixE7cN8svwB.htm)|Evanescent Wings|Evanescent Wings|modificada|
|[ancestry-01-SCgqIo8VQZZKZ1Ws.htm](feats/ancestry-01-SCgqIo8VQZZKZ1Ws.htm)|Orc Sight|Vista de orco|modificada|
|[ancestry-01-shp63QZvw9xvkVvC.htm](feats/ancestry-01-shp63QZvw9xvkVvC.htm)|Know Oneself|Conócete a ti mismo|modificada|
|[ancestry-01-SJbokJKkFnA6rKkJ.htm](feats/ancestry-01-SJbokJKkFnA6rKkJ.htm)|Nestling Fall|Caída del nido|modificada|
|[ancestry-01-sL2GmYve5NXJ0wc1.htm](feats/ancestry-01-sL2GmYve5NXJ0wc1.htm)|Gloomseer|Gloomseer|modificada|
|[ancestry-01-sm6Y3fTcltxE8N0p.htm](feats/ancestry-01-sm6Y3fTcltxE8N0p.htm)|Goblin Lore|Saber goblin|modificada|
|[ancestry-01-sMZBH6ROL44EDpXB.htm](feats/ancestry-01-sMZBH6ROL44EDpXB.htm)|Vishkanya Weapon Familiarity|Vishkanya Arma Familiaridad|modificada|
|[ancestry-01-SrSYEHqOLXWuj65e.htm](feats/ancestry-01-SrSYEHqOLXWuj65e.htm)|Inventive Offensive|Ofensiva Inventiva|modificada|
|[ancestry-01-StsFnks3lQU9YYpB.htm](feats/ancestry-01-StsFnks3lQU9YYpB.htm)|Clone-Risen|Clonar-Risen|modificada|
|[ancestry-01-T4ulOYkFh8gq2kY9.htm](feats/ancestry-01-T4ulOYkFh8gq2kY9.htm)|Tide-hardened|Endurecido por la marea|modificada|
|[ancestry-01-TcUpt0KaDnoYheX8.htm](feats/ancestry-01-TcUpt0KaDnoYheX8.htm)|Tough Skin|Piel de Dureza|modificada|
|[ancestry-01-tFgsBRsEo9ZEA5fU.htm](feats/ancestry-01-tFgsBRsEo9ZEA5fU.htm)|Illusion Sense|Sentido de la ilusión|modificada|
|[ancestry-01-TGlb3gmSKkJBZt5q.htm](feats/ancestry-01-TGlb3gmSKkJBZt5q.htm)|Striking Retribution|Golpe de Retribución|modificada|
|[ancestry-01-TLuFqQwvnlJNeEsv.htm](feats/ancestry-01-TLuFqQwvnlJNeEsv.htm)|Crystal Luminescence|Cristal Luminiscencia|modificada|
|[ancestry-01-TmaxLWraJrvSQOkY.htm](feats/ancestry-01-TmaxLWraJrvSQOkY.htm)|Mariner's Fire|Mariner's Fire|modificada|
|[ancestry-01-tn7K5lbnF87rZ659.htm](feats/ancestry-01-tn7K5lbnF87rZ659.htm)|Sociable|Sociable|modificada|
|[ancestry-01-tP8pws78OOmobWjB.htm](feats/ancestry-01-tP8pws78OOmobWjB.htm)|Willing Death|Muerte Voluntaria|modificada|
|[ancestry-01-tQ0FWNYJEXbmPCt7.htm](feats/ancestry-01-tQ0FWNYJEXbmPCt7.htm)|Emberkin|Emberkin|modificada|
|[ancestry-01-ttgv3PPCJLeI5pUL.htm](feats/ancestry-01-ttgv3PPCJLeI5pUL.htm)|Social Camouflage|Camuflaje Social|modificada|
|[ancestry-01-TtlbpGchHOoWc4HN.htm](feats/ancestry-01-TtlbpGchHOoWc4HN.htm)|Collapse|Colapso|modificada|
|[ancestry-01-tVPxq2qYCcOqvIYZ.htm](feats/ancestry-01-tVPxq2qYCcOqvIYZ.htm)|Ganzi Gaze|Ganzi Gaze|modificada|
|[ancestry-01-TW5TY7kSf50uaX71.htm](feats/ancestry-01-TW5TY7kSf50uaX71.htm)|Slither|Slither|modificada|
|[ancestry-01-txgCXcNMDe9kO7N8.htm](feats/ancestry-01-txgCXcNMDe9kO7N8.htm)|Vibrant Display|Pantalla vibrante|modificada|
|[ancestry-01-TYoE0GDF0URwQqZI.htm](feats/ancestry-01-TYoE0GDF0URwQqZI.htm)|Mistsoul|Mistsoul|modificada|
|[ancestry-01-U09VfgU7alL0acWv.htm](feats/ancestry-01-U09VfgU7alL0acWv.htm)|Water Nagaji|Nagaji acuático|modificada|
|[ancestry-01-U12Sh43QuY85Prdm.htm](feats/ancestry-01-U12Sh43QuY85Prdm.htm)|Parthenogenic Hatchling|Parthenogenic Hatchling|modificada|
|[ancestry-01-u7FPgOjbSOmCTSD9.htm](feats/ancestry-01-u7FPgOjbSOmCTSD9.htm)|Flexible Form|Forma flexible|modificada|
|[ancestry-01-u8gmBNHgb880vN3S.htm](feats/ancestry-01-u8gmBNHgb880vN3S.htm)|Witch Warden|Witch Warden|modificada|
|[ancestry-01-Ugw1zZStQhg6iz8h.htm](feats/ancestry-01-Ugw1zZStQhg6iz8h.htm)|Adroit Manipulation|Adroit Manipulation|modificada|
|[ancestry-01-uiNRrdnJe0GOzy6Q.htm](feats/ancestry-01-uiNRrdnJe0GOzy6Q.htm)|Chance Death|Chance Death|modificada|
|[ancestry-01-upMcjxPDgNOLuu7N.htm](feats/ancestry-01-upMcjxPDgNOLuu7N.htm)|Internal Compartment|Compartimiento Interno|modificada|
|[ancestry-01-UsJtnrqWOs7puRZa.htm](feats/ancestry-01-UsJtnrqWOs7puRZa.htm)|Dragon Spit|Escupe Dragón|modificada|
|[ancestry-01-uuD8Z9jUG61GmenX.htm](feats/ancestry-01-uuD8Z9jUG61GmenX.htm)|Unfettered Halfling|Mediano sin restricciones|modificada|
|[ancestry-01-uW0VSyy75YrsvtWz.htm](feats/ancestry-01-uW0VSyy75YrsvtWz.htm)|Unwavering Mien|Semblante decidido|modificada|
|[ancestry-01-UwH0sGIthv8kiPUt.htm](feats/ancestry-01-UwH0sGIthv8kiPUt.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[ancestry-01-VatkAzfBYjA6z6OP.htm](feats/ancestry-01-VatkAzfBYjA6z6OP.htm)|Razor Claws|Garras de Navaja|modificada|
|[ancestry-01-vcfeHDoaWEZtEcfz.htm](feats/ancestry-01-vcfeHDoaWEZtEcfz.htm)|Distracting Shadows|Sombras distractoras|modificada|
|[ancestry-01-viFTJfZusRPx0G2q.htm](feats/ancestry-01-viFTJfZusRPx0G2q.htm)|Scamper Underfoot|Scamper Underfoot|modificada|
|[ancestry-01-vNDCXpumBONpk5JO.htm](feats/ancestry-01-vNDCXpumBONpk5JO.htm)|Goloma Lore|Saber de los golomas|modificada|
|[ancestry-01-VSINzKESKAw2zA20.htm](feats/ancestry-01-VSINzKESKAw2zA20.htm)|Ceremony of Protection|Ceremonia de protección|modificada|
|[ancestry-01-VU07hybqzUXIJ6l2.htm](feats/ancestry-01-VU07hybqzUXIJ6l2.htm)|Halfling Lore|Saber mediano|modificada|
|[ancestry-01-VV7vbDzcO8vdD3OO.htm](feats/ancestry-01-VV7vbDzcO8vdD3OO.htm)|Skull Creeper|Skull Creeper|modificada|
|[ancestry-01-vWf6uykXkQp1Li0r.htm](feats/ancestry-01-vWf6uykXkQp1Li0r.htm)|Fumesoul|Fumesoul|modificada|
|[ancestry-01-Vxs9btOk61C7KpP2.htm](feats/ancestry-01-Vxs9btOk61C7KpP2.htm)|Cynical|Cynical|modificada|
|[ancestry-01-VZDJgGm5LGb1Pdck.htm](feats/ancestry-01-VZDJgGm5LGb1Pdck.htm)|Elemental Embellish|Embellecer elemental|modificada|
|[ancestry-01-vzKvihYNModB7sJ7.htm](feats/ancestry-01-vzKvihYNModB7sJ7.htm)|Play Dead|Play Dead|modificada|
|[ancestry-01-w1UrOqEYVuJDgRZZ.htm](feats/ancestry-01-w1UrOqEYVuJDgRZZ.htm)|Cold Minded|Cold Minded|modificada|
|[ancestry-01-W5hgaJvaCrTKfYbC.htm](feats/ancestry-01-W5hgaJvaCrTKfYbC.htm)|Fire Savvy|Fire Savvy|modificada|
|[ancestry-01-wAeI18wyWVwfUrIP.htm](feats/ancestry-01-wAeI18wyWVwfUrIP.htm)|Tusks (Half-Orc)|Colmillos (semiorco)|modificada|
|[ancestry-01-wEtN2NkqW4z4GUON.htm](feats/ancestry-01-wEtN2NkqW4z4GUON.htm)|Nosoi's Mask|Nosoi's Mask|modificada|
|[ancestry-01-weYZzyMmlCIC2TZt.htm](feats/ancestry-01-weYZzyMmlCIC2TZt.htm)|Grasping Reach|Alcance prensil|modificada|
|[ancestry-01-WoLh16gyDp8y9WOZ.htm](feats/ancestry-01-WoLh16gyDp8y9WOZ.htm)|Ancestral Longevity|Longevidad ancestral|modificada|
|[ancestry-01-WUMnFrehmFbj2PIm.htm](feats/ancestry-01-WUMnFrehmFbj2PIm.htm)|Azarketi Weapon Familiarity|Azarketi Weapon Familiarity|modificada|
|[ancestry-01-WYEjiUZQJP3uMk4Z.htm](feats/ancestry-01-WYEjiUZQJP3uMk4Z.htm)|Irrepressible (Ganzi)|Irreprimible (Ganzi)|modificada|
|[ancestry-01-wylnETwIz32Au46y.htm](feats/ancestry-01-wylnETwIz32Au46y.htm)|Ageless Spirit|Ageless Spirit|modificada|
|[ancestry-01-WzJaArukCUf9hpeP.htm](feats/ancestry-01-WzJaArukCUf9hpeP.htm)|Hag's Sight|Vista de maléfico|modificada|
|[ancestry-01-X7wFUWjYjYhzpejU.htm](feats/ancestry-01-X7wFUWjYjYhzpejU.htm)|Clan's Edge|Clan's Edge|modificada|
|[ancestry-01-X9tKWtQrAcmn26Nv.htm](feats/ancestry-01-X9tKWtQrAcmn26Nv.htm)|Adapted Cantrip|Truco adaptado|modificada|
|[ancestry-01-xjwI1DlJvb7Rg6TG.htm](feats/ancestry-01-xjwI1DlJvb7Rg6TG.htm)|Azarketi Lore|Azarketi Lore|modificada|
|[ancestry-01-XKDFgBEtFdEzCz8X.htm](feats/ancestry-01-XKDFgBEtFdEzCz8X.htm)|Intuitive Crafting|Artesanía intuitiva|modificada|
|[ancestry-01-xOCmeskMkd8nCmba.htm](feats/ancestry-01-xOCmeskMkd8nCmba.htm)|Moon May|Moon May|modificada|
|[ancestry-01-xruboxaTF6nw8l7M.htm](feats/ancestry-01-xruboxaTF6nw8l7M.htm)|Miresoul|Miresoul|modificada|
|[ancestry-01-XXAqMjml33jnQiDO.htm](feats/ancestry-01-XXAqMjml33jnQiDO.htm)|Old Soul|Alma vieja|modificada|
|[ancestry-01-Y1bEisU8jVCsIYk3.htm](feats/ancestry-01-Y1bEisU8jVCsIYk3.htm)|Woodcraft|Woodcraft|modificada|
|[ancestry-01-Y8c8hAyjNAiqgxAO.htm](feats/ancestry-01-Y8c8hAyjNAiqgxAO.htm)|Pelagic Aptitude|Aptitud Pelágica|modificada|
|[ancestry-01-yaacOmfmBuGDcNOs.htm](feats/ancestry-01-yaacOmfmBuGDcNOs.htm)|Skittertalk|Hablar errático|modificada|
|[ancestry-01-yaoekizcgPIlqVcC.htm](feats/ancestry-01-yaoekizcgPIlqVcC.htm)|Warped Reflection|Warped Reflection|modificada|
|[ancestry-01-yCaWcKlpbAfebqlO.htm](feats/ancestry-01-yCaWcKlpbAfebqlO.htm)|Shoony Lore|Shoony Lore|modificada|
|[ancestry-01-yEbXxbD317IZgtsN.htm](feats/ancestry-01-yEbXxbD317IZgtsN.htm)|Dustsoul|Dustsoul|modificada|
|[ancestry-01-YG6OCTbbqZwqRTr3.htm](feats/ancestry-01-YG6OCTbbqZwqRTr3.htm)|Arcane Eye|Ojo Arcano|modificada|
|[ancestry-01-yJ8Ez5dEscIk1xr5.htm](feats/ancestry-01-yJ8Ez5dEscIk1xr5.htm)|First World Magic|Magia del Primer Mundo|modificada|
|[ancestry-01-yMfZulJcoSomQ6dO.htm](feats/ancestry-01-yMfZulJcoSomQ6dO.htm)|Elemental Wrath|Ira elemental|modificada|
|[ancestry-01-YMKtEoNwHKA713Cx.htm](feats/ancestry-01-YMKtEoNwHKA713Cx.htm)|Junk Tinker|Reciclador de chatarra Trastear|modificada|
|[ancestry-01-yQZ6naE8AP6JYTSH.htm](feats/ancestry-01-yQZ6naE8AP6JYTSH.htm)|Ember's Eyes|Ember's Eyes|modificada|
|[ancestry-01-z92LsdpE98QdwILa.htm](feats/ancestry-01-z92LsdpE98QdwILa.htm)|Adaptive Vision|Visión Adaptativa|modificada|
|[ancestry-01-zAZJpgeEf5TWvdq4.htm](feats/ancestry-01-zAZJpgeEf5TWvdq4.htm)|Watchful Halfling|Mediano vigilante|modificada|
|[ancestry-01-zb7F0M3H8PN3XsdX.htm](feats/ancestry-01-zb7F0M3H8PN3XsdX.htm)|Gnoll Weapon Familiarity|Gnoll Weapon Familiarity|modificada|
|[ancestry-01-ZbRVqf14RTJJIZXG.htm](feats/ancestry-01-ZbRVqf14RTJJIZXG.htm)|Halfling Luck|Suerte del mediano|modificada|
|[ancestry-01-ZDO7foRCMd9niGsK.htm](feats/ancestry-01-ZDO7foRCMd9niGsK.htm)|Quadruped|Cuadrúpedo|modificada|
|[ancestry-01-zi84Xt4dTsLeJ3uD.htm](feats/ancestry-01-zi84Xt4dTsLeJ3uD.htm)|Living Weapon|Arma viviente|modificada|
|[ancestry-01-zki5qdM5IQcAiscM.htm](feats/ancestry-01-zki5qdM5IQcAiscM.htm)|Elven Lore|Saber de los elfos|modificada|
|[ancestry-01-znX4u20IFE7TPi9Y.htm](feats/ancestry-01-znX4u20IFE7TPi9Y.htm)|Morph-Risen|Morfismo-Risen|modificada|
|[ancestry-01-Zr1sspa9Q16V8uZV.htm](feats/ancestry-01-Zr1sspa9Q16V8uZV.htm)|Dig Up Secrets|Desenterrar secretos|modificada|
|[ancestry-01-zs2FFGI88zB7EaBT.htm](feats/ancestry-01-zs2FFGI88zB7EaBT.htm)|Grim Insight|Grim Insight|modificada|
|[ancestry-01-ZUjjUj9lVGOKrJbp.htm](feats/ancestry-01-ZUjjUj9lVGOKrJbp.htm)|Idyllkin|Idyllkin|modificada|
|[ancestry-01-zvtYsz9jk8tPvlpS.htm](feats/ancestry-01-zvtYsz9jk8tPvlpS.htm)|Cat Nap|Cat Nap|modificada|
|[ancestry-01-ZxiAMposVPDNPwxI.htm](feats/ancestry-01-ZxiAMposVPDNPwxI.htm)|Forge-Day's Rest|Descanso del Día de la Forja|modificada|
|[ancestry-01-Zz7isE8Td2xDWqR8.htm](feats/ancestry-01-Zz7isE8Td2xDWqR8.htm)|Kitsune Spell Familiarity|Kitsune Hechizo Familiaridad|modificada|
|[ancestry-01-ZZzgqFTOtUgnzSLZ.htm](feats/ancestry-01-ZZzgqFTOtUgnzSLZ.htm)|Monstrous Peacemaker|Monstrous Peacemaker|modificada|
|[ancestry-05-0FqbyC5tR2DC0DOk.htm](feats/ancestry-05-0FqbyC5tR2DC0DOk.htm)|Pack Stalker|Acechador en manada|modificada|
|[ancestry-05-0ihK3qYItmi8eVZs.htm](feats/ancestry-05-0ihK3qYItmi8eVZs.htm)|Eclectic Obsession|Obsesión Ecléctica|modificada|
|[ancestry-05-0Iv3VbR1DMPbZIjD.htm](feats/ancestry-05-0Iv3VbR1DMPbZIjD.htm)|Drag Down|Drag Down|modificada|
|[ancestry-05-0u1SZ1c6gDo6l0hS.htm](feats/ancestry-05-0u1SZ1c6gDo6l0hS.htm)|Climate Adaptation|Adaptación al clima|modificada|
|[ancestry-05-1gAehVvstGY885kJ.htm](feats/ancestry-05-1gAehVvstGY885kJ.htm)|Bristle|Erizarse|modificada|
|[ancestry-05-1LFbIRhb3Fgk4203.htm](feats/ancestry-05-1LFbIRhb3Fgk4203.htm)|Sealed Poppet|Sealed Poppet|modificada|
|[ancestry-05-1miLVRtvsnZU6TTk.htm](feats/ancestry-05-1miLVRtvsnZU6TTk.htm)|Anchoring Roots|Enraizamiento de Anclaje|modificada|
|[ancestry-05-2IO2W09IvwGHvatH.htm](feats/ancestry-05-2IO2W09IvwGHvatH.htm)|Graceful Guidance|Graciosa orientación divina|modificada|
|[ancestry-05-2kwLzw618QaIHOap.htm](feats/ancestry-05-2kwLzw618QaIHOap.htm)|Genie Weapon Flourish|Floritura de Arma de Genio|modificada|
|[ancestry-05-3fLfHrJbMy2ayLMQ.htm](feats/ancestry-05-3fLfHrJbMy2ayLMQ.htm)|Blast Resistance|Blast Resistance|modificada|
|[ancestry-05-3NZSRyoulnzsi3sn.htm](feats/ancestry-05-3NZSRyoulnzsi3sn.htm)|Wildborn Adept|Wildborn Adept|modificada|
|[ancestry-05-47ZB8mYBtBt1C7zh.htm](feats/ancestry-05-47ZB8mYBtBt1C7zh.htm)|Histrionic Injury|Herida Histriónica|modificada|
|[ancestry-05-48crF8lpg78fRdhJ.htm](feats/ancestry-05-48crF8lpg78fRdhJ.htm)|Feed On Pain|Alimentarse De Dolor|modificada|
|[ancestry-05-4M36jGbeSDfZFM38.htm](feats/ancestry-05-4M36jGbeSDfZFM38.htm)|Elven Instincts|Instintos élficos|modificada|
|[ancestry-05-4RUTJC42ZaENYh9T.htm](feats/ancestry-05-4RUTJC42ZaENYh9T.htm)|Embodied Dreadnought Subjectivity|Subjetividad Acorazada Encarnada|modificada|
|[ancestry-05-4tZbjb1ote8Nij8I.htm](feats/ancestry-05-4tZbjb1ote8Nij8I.htm)|Darting Monkey|Darting Monkey|modificada|
|[ancestry-05-5AnJTAZnuWrkL8fa.htm](feats/ancestry-05-5AnJTAZnuWrkL8fa.htm)|Defiance Unto Death|Defiance Unto Death|modificada|
|[ancestry-05-5iULRBRDOryuWX6t.htm](feats/ancestry-05-5iULRBRDOryuWX6t.htm)|Iruxi Unarmed Cunning|Iruxi Astucia sin armas|modificada|
|[ancestry-05-6kC0OuuWHyaqR3UQ.htm](feats/ancestry-05-6kC0OuuWHyaqR3UQ.htm)|Venom Spit|Venom Spit|modificada|
|[ancestry-05-73JyUrJnH3nOQJM5.htm](feats/ancestry-05-73JyUrJnH3nOQJM5.htm)|Ceremony of Knowledge|Ceremonia del conocimiento|modificada|
|[ancestry-05-7hwTeZNq6Jmzmtz4.htm](feats/ancestry-05-7hwTeZNq6Jmzmtz4.htm)|Agonizing Rebuke|Reprimenda Agonizante|modificada|
|[ancestry-05-7tNyXrMXclURtvUY.htm](feats/ancestry-05-7tNyXrMXclURtvUY.htm)|Skillful Tail (Ganzi)|Cola Hábil (Ganzi)|modificada|
|[ancestry-05-7w8O3g3KM1HCDBSL.htm](feats/ancestry-05-7w8O3g3KM1HCDBSL.htm)|Ambush Awareness|Consciencia de las emboscadas|modificada|
|[ancestry-05-80TAzC8XeFKRl7t5.htm](feats/ancestry-05-80TAzC8XeFKRl7t5.htm)|Warp Likeness|Warp Likeness|modificada|
|[ancestry-05-8NnYg8RUY4DQ8Wkf.htm](feats/ancestry-05-8NnYg8RUY4DQ8Wkf.htm)|Ghoran Weapon Practice|Ghoran Weapon Practice|modificada|
|[ancestry-05-8RZ4VKXJgtl1aN27.htm](feats/ancestry-05-8RZ4VKXJgtl1aN27.htm)|Ornate Tattoo|Ornate Tattoo|modificada|
|[ancestry-05-969mPoiYANzV2261.htm](feats/ancestry-05-969mPoiYANzV2261.htm)|Ratfolk Roll|Ratfolk Roll|modificada|
|[ancestry-05-9CIy4cJJTcRM30Vz.htm](feats/ancestry-05-9CIy4cJJTcRM30Vz.htm)|Tough Tumbler|Dureza Tumbler|modificada|
|[ancestry-05-9WsYVbe47aSADs1Q.htm](feats/ancestry-05-9WsYVbe47aSADs1Q.htm)|Halfling Weapon Trickster|Truquista con las armas de los enanos|modificada|
|[ancestry-05-a0DM4VV48EFrga30.htm](feats/ancestry-05-a0DM4VV48EFrga30.htm)|Reveal Hidden Self|Revelar Yo Escondido|modificada|
|[ancestry-05-a3eKpbAQnxGrlnGq.htm](feats/ancestry-05-a3eKpbAQnxGrlnGq.htm)|Restoring Blood|Restablecimiento de la sangre|modificada|
|[ancestry-05-a5H6dJSQgWfviUHU.htm](feats/ancestry-05-a5H6dJSQgWfviUHU.htm)|Nothing But Fluff|Nothing But Fluff|modificada|
|[ancestry-05-ab7xlmUzPUOFnAl2.htm](feats/ancestry-05-ab7xlmUzPUOFnAl2.htm)|Water Conjuration|Conjuración de agua|modificada|
|[ancestry-05-AcgxKcA5I3dNTWLr.htm](feats/ancestry-05-AcgxKcA5I3dNTWLr.htm)|Wavetouched Paragon|Wavetouched Paragon|modificada|
|[ancestry-05-ACXWB7a38ETc32Qj.htm](feats/ancestry-05-ACXWB7a38ETc32Qj.htm)|Catchy Tune|Melodía pegadiza|modificada|
|[ancestry-05-aLgb1x3azE4IKz0o.htm](feats/ancestry-05-aLgb1x3azE4IKz0o.htm)|Gnoll Weapon Practicality|Practica con las armas de los gnoll|modificada|
|[ancestry-05-ALR9knJdktuMWzr4.htm](feats/ancestry-05-ALR9knJdktuMWzr4.htm)|Azarketi Weapon Aptitude|Azarketi Weapon Aptitude|modificada|
|[ancestry-05-aoz9dCq2NynyUYEf.htm](feats/ancestry-05-aoz9dCq2NynyUYEf.htm)|Formation Training|Formación de Entrenamiento|modificada|
|[ancestry-05-aVRuchEAJITim82y.htm](feats/ancestry-05-aVRuchEAJITim82y.htm)|Skillful Tail (Geniekin)|Cola hábil (Geniekin)|modificada|
|[ancestry-05-aZIdjtIYlLtJJP3g.htm](feats/ancestry-05-aZIdjtIYlLtJJP3g.htm)|Amorphous Aspect|Aspecto Amorfo|modificada|
|[ancestry-05-B4e8V5nExlScjojY.htm](feats/ancestry-05-B4e8V5nExlScjojY.htm)|Shoki's Argument|Argumento de Shoki|modificada|
|[ancestry-05-B6eoSmowSFNlhj9h.htm](feats/ancestry-05-B6eoSmowSFNlhj9h.htm)|Hobgoblin Weapon Discipline|Disciplina de Armas Hobgoblin|modificada|
|[ancestry-05-b8BZl7wbm83ObEtO.htm](feats/ancestry-05-b8BZl7wbm83ObEtO.htm)|Integrated Armament|Armamento integrado|modificada|
|[ancestry-05-B9IytVeJ1SMSJawB.htm](feats/ancestry-05-B9IytVeJ1SMSJawB.htm)|Defensive Needles|Agujas defensivas|modificada|
|[ancestry-05-BaSl8PmfQwESIiY6.htm](feats/ancestry-05-BaSl8PmfQwESIiY6.htm)|Hypnotic Lure|Señuelo hipnótico|modificada|
|[ancestry-05-BiPQAcvaEO0P2snr.htm](feats/ancestry-05-BiPQAcvaEO0P2snr.htm)|Healer's Halo|Healer's Halo|modificada|
|[ancestry-05-bJzANqEGTkho1bv6.htm](feats/ancestry-05-bJzANqEGTkho1bv6.htm)|Arcane Safeguards|Arcane Safeguards|modificada|
|[ancestry-05-bKc8MMFEOpOJJihb.htm](feats/ancestry-05-bKc8MMFEOpOJJihb.htm)|Heat Wave|Ola de Calor|modificada|
|[ancestry-05-BL2nLeClO30QoQGs.htm](feats/ancestry-05-BL2nLeClO30QoQGs.htm)|Spark Fist|Spark Fist|modificada|
|[ancestry-05-bZGCOKLtCzMnrVwk.htm](feats/ancestry-05-bZGCOKLtCzMnrVwk.htm)|Replicate|Replicate|modificada|
|[ancestry-05-CdSWMqPOVWIEzyUA.htm](feats/ancestry-05-CdSWMqPOVWIEzyUA.htm)|Fluid Contortionist|Contorsionista de fluidos|modificada|
|[ancestry-05-CnKc1kBejFV5gWtN.htm](feats/ancestry-05-CnKc1kBejFV5gWtN.htm)|Animalistic Resistance|Resistencia Animal|modificada|
|[ancestry-05-Crcz9cW0To2pkfSy.htm](feats/ancestry-05-Crcz9cW0To2pkfSy.htm)|Murderous Thorns|Espinas asesinas|modificada|
|[ancestry-05-DBWfPOYZaupwo3rz.htm](feats/ancestry-05-DBWfPOYZaupwo3rz.htm)|Adaptive Adept|Adepto adeptativo|modificada|
|[ancestry-05-DidzozerKLZ2UYLx.htm](feats/ancestry-05-DidzozerKLZ2UYLx.htm)|Fortify Shield|Fortificar Escudo|modificada|
|[ancestry-05-dIIqejy4JAVuF0I8.htm](feats/ancestry-05-dIIqejy4JAVuF0I8.htm)|Fey Influence|Influencia feérica|modificada|
|[ancestry-05-DmYbGBC2ukp8tYD4.htm](feats/ancestry-05-DmYbGBC2ukp8tYD4.htm)|Firesight|Firesight|modificada|
|[ancestry-05-DP5VVZHERQlYuYTa.htm](feats/ancestry-05-DP5VVZHERQlYuYTa.htm)|Protective Subroutine|Subrutina de protección|modificada|
|[ancestry-05-DSyLFBi2LcUxeORh.htm](feats/ancestry-05-DSyLFBi2LcUxeORh.htm)|Devil In Plain Sight|Devil In Plain Sight|modificada|
|[ancestry-05-DtazfQJaRRe46Sej.htm](feats/ancestry-05-DtazfQJaRRe46Sej.htm)|Tranquil Sanctuary|Santuario Tranquilo|modificada|
|[ancestry-05-e3oaaoMZPzyP26QV.htm](feats/ancestry-05-e3oaaoMZPzyP26QV.htm)|Noble Resolve|Noble Resolución|modificada|
|[ancestry-05-ehPnY1PuPK7EXkYc.htm](feats/ancestry-05-ehPnY1PuPK7EXkYc.htm)|Demonblood Frenzy|Frenesí de sangre demoníaca|modificada|
|[ancestry-05-EIbppwlEu11ltC7n.htm](feats/ancestry-05-EIbppwlEu11ltC7n.htm)|Reflective Pocket|Bolsillo reflectante|modificada|
|[ancestry-05-espST21gwaZQFxpw.htm](feats/ancestry-05-espST21gwaZQFxpw.htm)|Undead Companion|Compañero muerto viviente|modificada|
|[ancestry-05-eVn6hBjNjTB4liKw.htm](feats/ancestry-05-eVn6hBjNjTB4liKw.htm)|Chosen of Lamashtu|Elegido de Lamashtu|modificada|
|[ancestry-05-evwCimenReYvcruj.htm](feats/ancestry-05-evwCimenReYvcruj.htm)|Animal Elocutionist|Profesor de de elocución de animales|modificada|
|[ancestry-05-Ewk7h9aQpKvy1RJo.htm](feats/ancestry-05-Ewk7h9aQpKvy1RJo.htm)|Crystalline Dust|Polvo Cristalino|modificada|
|[ancestry-05-exJmNR2XH1i6PGw3.htm](feats/ancestry-05-exJmNR2XH1i6PGw3.htm)|Skilled Climber|Escalador experto|modificada|
|[ancestry-05-F4W5a2vkhUP7Lr4j.htm](feats/ancestry-05-F4W5a2vkhUP7Lr4j.htm)|Powerful Guts|Agallas Poderosas|modificada|
|[ancestry-05-f5Vkk2rM6tCe2zQn.htm](feats/ancestry-05-f5Vkk2rM6tCe2zQn.htm)|Shared Luck (Halfling)|Suerte compartida (Halfling)|modificada|
|[ancestry-05-f9YlMYWMjd0zoyy0.htm](feats/ancestry-05-f9YlMYWMjd0zoyy0.htm)|Protective Claws|Garras protectoras|modificada|
|[ancestry-05-FAUqvzfZDpRG44q0.htm](feats/ancestry-05-FAUqvzfZDpRG44q0.htm)|Renewing Quills|Renovar plumas|modificada|
|[ancestry-05-FayzcoNaiIdyPS2j.htm](feats/ancestry-05-FayzcoNaiIdyPS2j.htm)|Greater Animal Senses|Mayores Sentidos Animales|modificada|
|[ancestry-05-FBtm9rZzk0tCQu9H.htm](feats/ancestry-05-FBtm9rZzk0tCQu9H.htm)|Spark of Independence|Spark of Independence|modificada|
|[ancestry-05-Fj3ufCawOM6fZB24.htm](feats/ancestry-05-Fj3ufCawOM6fZB24.htm)|Expert Drill Sergeant|Experto Sargento Instructor|modificada|
|[ancestry-05-FLuv8uI1KoodNgY4.htm](feats/ancestry-05-FLuv8uI1KoodNgY4.htm)|Goblin Weapon Frenzy|Frenesí con las armas de los goblins|modificada|
|[ancestry-05-g0hHetVM9UmmIDKU.htm](feats/ancestry-05-g0hHetVM9UmmIDKU.htm)|Fighting Horn|Cuerno de lucha|modificada|
|[ancestry-05-GAh4CwCMS3Gsl8WH.htm](feats/ancestry-05-GAh4CwCMS3Gsl8WH.htm)|Tengu Feather Fan|Abanico de plumas Tengu|modificada|
|[ancestry-05-GTA6QM7cH40L8H5Q.htm](feats/ancestry-05-GTA6QM7cH40L8H5Q.htm)|Fey Disguise|Disfraz de feérico|modificada|
|[ancestry-05-GZrvQo5FcoP5qocX.htm](feats/ancestry-05-GZrvQo5FcoP5qocX.htm)|Gaping Flesh|Gaping Flesh|modificada|
|[ancestry-05-HCxmS8QGHL5O7LUf.htm](feats/ancestry-05-HCxmS8QGHL5O7LUf.htm)|Offensive Analysis|Análisis Ofensivo|modificada|
|[ancestry-05-HEvk4ja8nJ3RVEqi.htm](feats/ancestry-05-HEvk4ja8nJ3RVEqi.htm)|Skin Split|Escisión de piel|modificada|
|[ancestry-05-HKGyFj2w5dzkf3SW.htm](feats/ancestry-05-HKGyFj2w5dzkf3SW.htm)|Natural Illusionist|Ilusionista Natural|modificada|
|[ancestry-05-hrITlxkBqHvaiiRS.htm](feats/ancestry-05-hrITlxkBqHvaiiRS.htm)|Right-Hand Blood|Sangre de la mano derecha|modificada|
|[ancestry-05-I3EMC4pqkEWrodpq.htm](feats/ancestry-05-I3EMC4pqkEWrodpq.htm)|Practiced Paddler|Remador Practicado|modificada|
|[ancestry-05-iCsHR5tdSXDHGjCv.htm](feats/ancestry-05-iCsHR5tdSXDHGjCv.htm)|Plague Sniffer|Plague Sniffer|modificada|
|[ancestry-05-iCzJRywykSFwLto0.htm](feats/ancestry-05-iCzJRywykSFwLto0.htm)|Vanth's Weapon Execution|Ejecución de arma de Vanth|modificada|
|[ancestry-05-iliy0ONIb8Hw6muA.htm](feats/ancestry-05-iliy0ONIb8Hw6muA.htm)|Swift Swimmer|Nadador rápido|modificada|
|[ancestry-05-iS7MvHewCeQRT78d.htm](feats/ancestry-05-iS7MvHewCeQRT78d.htm)|Skillful Climber|Hábil Escalador|modificada|
|[ancestry-05-IsNs6hXQm9pJcXB1.htm](feats/ancestry-05-IsNs6hXQm9pJcXB1.htm)|Clever Shadow|Sombra Inteligente|modificada|
|[ancestry-05-IUVzB39pRVyBFOEx.htm](feats/ancestry-05-IUVzB39pRVyBFOEx.htm)|Elemental Bulwark|Baluarte elemental|modificada|
|[ancestry-05-IY86Kvopp4ACuQsw.htm](feats/ancestry-05-IY86Kvopp4ACuQsw.htm)|Envenom Fangs|Colmillos Envenom|modificada|
|[ancestry-05-j1620BT5t0RLVB0C.htm](feats/ancestry-05-j1620BT5t0RLVB0C.htm)|Grippli Weapon Innovator|Innovador con las armas de los grippli|modificada|
|[ancestry-05-jNrpvEqfncdGZPak.htm](feats/ancestry-05-jNrpvEqfncdGZPak.htm)|Halfling Ingenuity|Ingenio Halfling|modificada|
|[ancestry-05-juLtvliSzYPCmMa3.htm](feats/ancestry-05-juLtvliSzYPCmMa3.htm)|Winglets|Winglets|modificada|
|[ancestry-05-k8nWKHLYvAKMuwLd.htm](feats/ancestry-05-k8nWKHLYvAKMuwLd.htm)|Ageless Patience|Paciencia intemporal|modificada|
|[ancestry-05-KcbSxOPYC5CUqbZQ.htm](feats/ancestry-05-KcbSxOPYC5CUqbZQ.htm)|Clever Improviser|Improvisador astuto|modificada|
|[ancestry-05-KeE8Ky38dCX8XaTg.htm](feats/ancestry-05-KeE8Ky38dCX8XaTg.htm)|Tree's Ward|Sala del Árbol|modificada|
|[ancestry-05-kGed3uQbZ7x5SBB8.htm](feats/ancestry-05-kGed3uQbZ7x5SBB8.htm)|Gecko's Grip|Gecko's Grip|modificada|
|[ancestry-05-ko399VJY2VsKE7iM.htm](feats/ancestry-05-ko399VJY2VsKE7iM.htm)|Unlock Secret|Desbloquear Secreto|modificada|
|[ancestry-05-kpn4R65YlD38iAIS.htm](feats/ancestry-05-kpn4R65YlD38iAIS.htm)|Ritual Reversion|Reversión Ritual|modificada|
|[ancestry-05-krgoR0Ykqw59MDbg.htm](feats/ancestry-05-krgoR0Ykqw59MDbg.htm)|Uncanny Awareness|Uncanny Awareness|modificada|
|[ancestry-05-KU488rIt9bOdTNMA.htm](feats/ancestry-05-KU488rIt9bOdTNMA.htm)|Well of Potential|Pozo de Potencial|modificada|
|[ancestry-05-KYKK1vLqGIxXH5Tu.htm](feats/ancestry-05-KYKK1vLqGIxXH5Tu.htm)|Speak with Kindred|Hablar con Kindred|modificada|
|[ancestry-05-l2JaSC3NA9S6Qq46.htm](feats/ancestry-05-l2JaSC3NA9S6Qq46.htm)|Myriad Forms|Myriad Forms|modificada|
|[ancestry-05-LC2EnXQ4MkhDLViM.htm](feats/ancestry-05-LC2EnXQ4MkhDLViM.htm)|Finned Ridges|Crestas con aletas|modificada|
|[ancestry-05-lF5B49zbDG61sxXa.htm](feats/ancestry-05-lF5B49zbDG61sxXa.htm)|Lucky Break|Buena racha|modificada|
|[ancestry-05-llWKddRiyUHouaZx.htm](feats/ancestry-05-llWKddRiyUHouaZx.htm)|Empathic Calm|Calma Empática|modificada|
|[ancestry-05-LNyTKhIZlM17026W.htm](feats/ancestry-05-LNyTKhIZlM17026W.htm)|Gnaw|Gnaw|modificada|
|[ancestry-05-M0B0rt6rk5MkHiBN.htm](feats/ancestry-05-M0B0rt6rk5MkHiBN.htm)|Hybrid Shape|Forma híbrida|modificada|
|[ancestry-05-m64PtRsaiOkylVVk.htm](feats/ancestry-05-m64PtRsaiOkylVVk.htm)|Cunning Tinker|Astucia Trastear|modificada|
|[ancestry-05-mfy0nasMIiMLUm3f.htm](feats/ancestry-05-mfy0nasMIiMLUm3f.htm)|Slip With The Breeze|Slip With The Breeze|modificada|
|[ancestry-05-MwjnRpVn3br88Caj.htm](feats/ancestry-05-MwjnRpVn3br88Caj.htm)|Intuitive Illusions|Ilusión intuitiva|modificada|
|[ancestry-05-NBDwiz1NDioc2eMP.htm](feats/ancestry-05-NBDwiz1NDioc2eMP.htm)|Energized Font|Fuente energizada|modificada|
|[ancestry-05-NBwH9wEeUfKfOg8R.htm](feats/ancestry-05-NBwH9wEeUfKfOg8R.htm)|Magpie Snatch|Arrebatar Urraca|modificada|
|[ancestry-05-NHlCta7B1Lmt3S7w.htm](feats/ancestry-05-NHlCta7B1Lmt3S7w.htm)|Jungle Runner|Jungle Runner|modificada|
|[ancestry-05-NPATYCDg2cryH0Ya.htm](feats/ancestry-05-NPATYCDg2cryH0Ya.htm)|Conrasu Weapon Understanding|Entendimiento de las armas de los conrasu|modificada|
|[ancestry-05-Nv9hNKVSioDw5DHC.htm](feats/ancestry-05-Nv9hNKVSioDw5DHC.htm)|Ragdya's Revelry|Ragdya's Revelry|modificada|
|[ancestry-05-O89mwKPRg0up0J0I.htm](feats/ancestry-05-O89mwKPRg0up0J0I.htm)|Focused Cat Nap|Siesta de gato enfocada|modificada|
|[ancestry-05-OCANjuCQ1wcCRBsn.htm](feats/ancestry-05-OCANjuCQ1wcCRBsn.htm)|Vishkanya Weapon Arts|Vishkanya Weapon Arts|modificada|
|[ancestry-05-oEvcWFvBnsZ03OGW.htm](feats/ancestry-05-oEvcWFvBnsZ03OGW.htm)|Thrown Voice|Voz Lanzada|modificada|
|[ancestry-05-OhyN9pBHdMpC126F.htm](feats/ancestry-05-OhyN9pBHdMpC126F.htm)|Hybrid Form|Forma híbrida|modificada|
|[ancestry-05-OjUfwxMcM91CHLHP.htm](feats/ancestry-05-OjUfwxMcM91CHLHP.htm)|Well-Armed|Well-Armed|modificada|
|[ancestry-05-OK8Fm7EG8wjoYZBX.htm](feats/ancestry-05-OK8Fm7EG8wjoYZBX.htm)|Favorable Winds|Vientos Favorables|modificada|
|[ancestry-05-OK8X6QFRuRxpBMcZ.htm](feats/ancestry-05-OK8X6QFRuRxpBMcZ.htm)|Long Tongue|Lengua larga|modificada|
|[ancestry-05-oL6f86dy4yyx5N54.htm](feats/ancestry-05-oL6f86dy4yyx5N54.htm)|Tomb-Watcher's Glare|Tomb-Watcher's Glare|modificada|
|[ancestry-05-OOBSMpdAfYuiiQqo.htm](feats/ancestry-05-OOBSMpdAfYuiiQqo.htm)|Recognize Ambush|Reconocer Emboscar|modificada|
|[ancestry-05-OqJMjuK61uST7AlM.htm](feats/ancestry-05-OqJMjuK61uST7AlM.htm)|Gnome Weapon Innovator|Innovador con las armas de los gnomos|modificada|
|[ancestry-05-oqoftwLB6tvoLjnL.htm](feats/ancestry-05-oqoftwLB6tvoLjnL.htm)|Lightless Litheness|Lucidez sin luz|modificada|
|[ancestry-05-oSA1Ii6gTRGobeSO.htm](feats/ancestry-05-oSA1Ii6gTRGobeSO.htm)|Shadowy Disguise|Disfraz sombrío|modificada|
|[ancestry-05-otr60veuPNygNDFY.htm](feats/ancestry-05-otr60veuPNygNDFY.htm)|Cunning Hair|Pelo Astucia|modificada|
|[ancestry-05-OXhrwQgnZdOi81Yi.htm](feats/ancestry-05-OXhrwQgnZdOi81Yi.htm)|Defy Death|Desafía a la Muerte|modificada|
|[ancestry-05-P19AnciwNcSqxU7z.htm](feats/ancestry-05-P19AnciwNcSqxU7z.htm)|Distant Cackle|Cacareo Distante|modificada|
|[ancestry-05-pc00hoz4ILmqUwSC.htm](feats/ancestry-05-pc00hoz4ILmqUwSC.htm)|Devilish Wiles|Devilish Wiles|modificada|
|[ancestry-05-PdIN91xKsZ4z7p17.htm](feats/ancestry-05-PdIN91xKsZ4z7p17.htm)|Transposable Compliance|Cumplimiento transponible|modificada|
|[ancestry-05-PncXj46fgSwTWRl6.htm](feats/ancestry-05-PncXj46fgSwTWRl6.htm)|Ankle Bite|Muerdemuerde el Tobillo|modificada|
|[ancestry-05-pqezW5dqha1I32Ld.htm](feats/ancestry-05-pqezW5dqha1I32Ld.htm)|Inspire Imitation|Inspirar imitación|modificada|
|[ancestry-05-PRlN77xL6Bm2gUIp.htm](feats/ancestry-05-PRlN77xL6Bm2gUIp.htm)|Grippli Glide|Planeo de los grippli|modificada|
|[ancestry-05-psgPbKsbqfz6Qt4P.htm](feats/ancestry-05-psgPbKsbqfz6Qt4P.htm)|Speak With Flowers|Hablar con flores|modificada|
|[ancestry-05-PWKFdaNghx1YMKlA.htm](feats/ancestry-05-PWKFdaNghx1YMKlA.htm)|Hunter's Fangs|Colmillos del cazador|modificada|
|[ancestry-05-qKNo7Sr1dtVqhhAa.htm](feats/ancestry-05-qKNo7Sr1dtVqhhAa.htm)|Inured to the Heat|Habituado al calor|modificada|
|[ancestry-05-qkXRucQCLLS3VoMa.htm](feats/ancestry-05-qkXRucQCLLS3VoMa.htm)|Friendform|Forma de amigo|modificada|
|[ancestry-05-qUZnrueC2a0zf95N.htm](feats/ancestry-05-qUZnrueC2a0zf95N.htm)|Runtsage|Runtsage|modificada|
|[ancestry-05-qZUdGd2khS9cq4hJ.htm](feats/ancestry-05-qZUdGd2khS9cq4hJ.htm)|Shifting Faces|Cambiante Caras|modificada|
|[ancestry-05-RkEvEqwc8pCBcusz.htm](feats/ancestry-05-RkEvEqwc8pCBcusz.htm)|Hopping Stride|Zancada saltarina|modificada|
|[ancestry-05-rnEfO5eyRw7Fywzb.htm](feats/ancestry-05-rnEfO5eyRw7Fywzb.htm)|Tail Spin|Tail Spin|modificada|
|[ancestry-05-rp3mjgFXBVZYCleU.htm](feats/ancestry-05-rp3mjgFXBVZYCleU.htm)|Inoculation Subroutine|Subrutina de Inoculación|modificada|
|[ancestry-05-RP7TlsqNuge0dltp.htm](feats/ancestry-05-RP7TlsqNuge0dltp.htm)|Kitsune Spell Mysteries|Misterios de Hechizos Kitsune|modificada|
|[ancestry-05-Rr7GdUjXhGhV04Pe.htm](feats/ancestry-05-Rr7GdUjXhGhV04Pe.htm)|Supernatural Charm|Encanto sobrenatural|modificada|
|[ancestry-05-RrtqD8WmoUumauJD.htm](feats/ancestry-05-RrtqD8WmoUumauJD.htm)|Mask Of Power|Mascara De Poder|modificada|
|[ancestry-05-rTjGshtUMHnlBUfH.htm](feats/ancestry-05-rTjGshtUMHnlBUfH.htm)|Marine Ally|Aliado Marino|modificada|
|[ancestry-05-s1swBWSqtfrXTJHK.htm](feats/ancestry-05-s1swBWSqtfrXTJHK.htm)|Guided by the Stars|Guiado por las estrellas|modificada|
|[ancestry-05-SBhzem6n3buoxlG5.htm](feats/ancestry-05-SBhzem6n3buoxlG5.htm)|Extinguish Light|Extinguir la luz|modificada|
|[ancestry-05-scA2b141swxUPw8M.htm](feats/ancestry-05-scA2b141swxUPw8M.htm)|Garuda's Squall|Garuda's Squall|modificada|
|[ancestry-05-ShH7Wl7xfJL07DZC.htm](feats/ancestry-05-ShH7Wl7xfJL07DZC.htm)|Leshy Glide|Deslizamiento leshy (s), leshys (pl.)|modificada|
|[ancestry-05-sQLgEcqoQ0SYtbTg.htm](feats/ancestry-05-sQLgEcqoQ0SYtbTg.htm)|Tail Snatch|Arrebatar Cola|modificada|
|[ancestry-05-sTqdFqWVL9yxi5wt.htm](feats/ancestry-05-sTqdFqWVL9yxi5wt.htm)|Protective Sheath|Vaina de protección|modificada|
|[ancestry-05-SWNmYaj0OSPhhIqO.htm](feats/ancestry-05-SWNmYaj0OSPhhIqO.htm)|Mutate Weapon|Arma Mutante|modificada|
|[ancestry-05-SYcTallEKEaJeNGw.htm](feats/ancestry-05-SYcTallEKEaJeNGw.htm)|Swimming Poppet|Nadar Poppet|modificada|
|[ancestry-05-SzZFmcfLoHMAS0gt.htm](feats/ancestry-05-SzZFmcfLoHMAS0gt.htm)|Cloud Gazer|Cloud Gazer|modificada|
|[ancestry-05-t3IzY8uSyFj3aGmh.htm](feats/ancestry-05-t3IzY8uSyFj3aGmh.htm)|Shed Tail|Shed Tail|modificada|
|[ancestry-05-tB6V6rWv8vAFsKsX.htm](feats/ancestry-05-tB6V6rWv8vAFsKsX.htm)|Nanite Shroud|Nanite Shroud|modificada|
|[ancestry-05-ThqMksZRxNB18ivs.htm](feats/ancestry-05-ThqMksZRxNB18ivs.htm)|Advanced Targeting System|Sistema avanzado de designación de objetivos|modificada|
|[ancestry-05-TvVqZHp7qvkPakKf.htm](feats/ancestry-05-TvVqZHp7qvkPakKf.htm)|Mistaken Identity|Identidad Confundida|modificada|
|[ancestry-05-U5FcfRvveTKtgebq.htm](feats/ancestry-05-U5FcfRvveTKtgebq.htm)|Torch Goblin|Goblin Antorcha|modificada|
|[ancestry-05-ulrGnvF0KAgEaifX.htm](feats/ancestry-05-ulrGnvF0KAgEaifX.htm)|Cornered Fury|Furia Acorralada|modificada|
|[ancestry-05-uQGo33E4haaFNg6u.htm](feats/ancestry-05-uQGo33E4haaFNg6u.htm)|Pride Hunter|Cazador de orgullo|modificada|
|[ancestry-05-UuVz1QY7QXD5cnLu.htm](feats/ancestry-05-UuVz1QY7QXD5cnLu.htm)|Fledgling Flight|Fledgling Flight|modificada|
|[ancestry-05-v0ovKn4ZKmtXfXnu.htm](feats/ancestry-05-v0ovKn4ZKmtXfXnu.htm)|Vanara Weapon Trickery|Vanara Weapon Trickery|modificada|
|[ancestry-05-V4W2hTr5lm5vS8Dq.htm](feats/ancestry-05-V4W2hTr5lm5vS8Dq.htm)|Nagaji Spell Mysteries|Misterios de hechizos Nagaji|modificada|
|[ancestry-05-vdowlFFknihiz5pm.htm](feats/ancestry-05-vdowlFFknihiz5pm.htm)|Intercorporate|Intercorporate|modificada|
|[ancestry-05-VFBQ1MskOascFDNf.htm](feats/ancestry-05-VFBQ1MskOascFDNf.htm)|Steam Spell|Hechizo de vapor|modificada|
|[ancestry-05-VK2vAl1SfI4Qrtkt.htm](feats/ancestry-05-VK2vAl1SfI4Qrtkt.htm)|Loyal Empath|Empático Leal|modificada|
|[ancestry-05-VLCAFIXzUtiv1VuE.htm](feats/ancestry-05-VLCAFIXzUtiv1VuE.htm)|Flexible Tail|Cola flexible|modificada|
|[ancestry-05-VNmmHrMAVk3Tiw1d.htm](feats/ancestry-05-VNmmHrMAVk3Tiw1d.htm)|Kobold Weapon Innovator|Innovador de Armas Kobold|modificada|
|[ancestry-05-VQmKNh0QPHzYMmee.htm](feats/ancestry-05-VQmKNh0QPHzYMmee.htm)|Treacherous Earth|Tierra Traicionera|modificada|
|[ancestry-05-vW7tWi3jK7z2Clen.htm](feats/ancestry-05-vW7tWi3jK7z2Clen.htm)|Dwarven Weapon Cunning|Astucia con las armas de los enanos|modificada|
|[ancestry-05-vxE9hBKB6F2ctOX3.htm](feats/ancestry-05-vxE9hBKB6F2ctOX3.htm)|Mask Of Rejection|Máscara De Rechazo|modificada|
|[ancestry-05-wigOSPSxXnapFxeh.htm](feats/ancestry-05-wigOSPSxXnapFxeh.htm)|Dogfang Bite|Muerdemuerdeperro|modificada|
|[ancestry-05-wjhhlh82MABhfxCO.htm](feats/ancestry-05-wjhhlh82MABhfxCO.htm)|Strix Vengeance|Strix Vengeance|modificada|
|[ancestry-05-WWSwcvIjGUQOKKuD.htm](feats/ancestry-05-WWSwcvIjGUQOKKuD.htm)|Taste Blood|Taste Blood|modificada|
|[ancestry-05-XM4zR0q9rBxjR3lG.htm](feats/ancestry-05-XM4zR0q9rBxjR3lG.htm)|Darkseer|Darkseer|modificada|
|[ancestry-05-xoB4RDYkdAALt0U4.htm](feats/ancestry-05-xoB4RDYkdAALt0U4.htm)|Magical Resistance|Resistencia Mágica|modificada|
|[ancestry-05-XRLtXJXGswe20QKY.htm](feats/ancestry-05-XRLtXJXGswe20QKY.htm)|Towering Presence|Towering Presence|modificada|
|[ancestry-05-xruqgywumtExg8me.htm](feats/ancestry-05-xruqgywumtExg8me.htm)|Debilitating Venom|Debilitación Venenosa|modificada|
|[ancestry-05-XYtnVNKt6uPcRrdH.htm](feats/ancestry-05-XYtnVNKt6uPcRrdH.htm)|Tree Climber (Goblin)|Tree Climber (Goblin)|modificada|
|[ancestry-05-y0Ru2hRmoRwkRsxZ.htm](feats/ancestry-05-y0Ru2hRmoRwkRsxZ.htm)|Tongue Disarm|Desarmar lengua|modificada|
|[ancestry-05-Y3IUIsopvN13vEvZ.htm](feats/ancestry-05-Y3IUIsopvN13vEvZ.htm)|Orc Weapon Carnage|Carnicería con las armas de los orcos|modificada|
|[ancestry-05-ya9YlhUA4WspUlHB.htm](feats/ancestry-05-ya9YlhUA4WspUlHB.htm)|Clan Protector|Protector del Clan|modificada|
|[ancestry-05-YeVDITJplindA27l.htm](feats/ancestry-05-YeVDITJplindA27l.htm)|Victorious Vigor|Vigor victorioso|modificada|
|[ancestry-05-YndH82FX7KLawNBW.htm](feats/ancestry-05-YndH82FX7KLawNBW.htm)|Tenacious Net|Red tenaz|modificada|
|[ancestry-05-yqtaAZR9jfen6gEW.htm](feats/ancestry-05-yqtaAZR9jfen6gEW.htm)|Iruxi Glide|Iruxi Glide|modificada|
|[ancestry-05-z0Bwy7lxU3DIugpo.htm](feats/ancestry-05-z0Bwy7lxU3DIugpo.htm)|Animal Speaker|Animal Speaker|modificada|
|[ancestry-05-z2z3UTayfUhcGazv.htm](feats/ancestry-05-z2z3UTayfUhcGazv.htm)|Scar-Thick Skin|Piel gruesa como una cicatriz|modificada|
|[ancestry-05-Z30nSkai5UmZCyKu.htm](feats/ancestry-05-Z30nSkai5UmZCyKu.htm)|Past Life|Vida Pasada|modificada|
|[ancestry-05-Z56DtGBd3AcZOCeG.htm](feats/ancestry-05-Z56DtGBd3AcZOCeG.htm)|Cultural Adaptability|Adaptabilidad cultural|modificada|
|[ancestry-05-zc32MIxa1E6357D6.htm](feats/ancestry-05-zc32MIxa1E6357D6.htm)|Elven Weapon Elegance|Elegancia con las armas de los elfos|modificada|
|[ancestry-05-zgriBCYR4TmBoDqO.htm](feats/ancestry-05-zgriBCYR4TmBoDqO.htm)|Forest Stealth|Sigilo forestal|modificada|
|[ancestry-05-ZOOP2RzRJnpVqnCr.htm](feats/ancestry-05-ZOOP2RzRJnpVqnCr.htm)|Aquatic Conversationalist|Conversador Acuático|modificada|
|[ancestry-05-ZRI1OXaaa4ZC6EK1.htm](feats/ancestry-05-ZRI1OXaaa4ZC6EK1.htm)|Easily Dismissed|Fácilmente Desechable|modificada|
|[ancestry-05-zwqawaXccARDV0jL.htm](feats/ancestry-05-zwqawaXccARDV0jL.htm)|Feathered Cloak|Capa de plumas|modificada|
|[ancestry-05-ZwvSiuFFsyNGJiB3.htm](feats/ancestry-05-ZwvSiuFFsyNGJiB3.htm)|Ceremony of Sunlight|Ceremonia de la luz solar|modificada|
|[ancestry-05-ZZxePfQkBPuTHkt1.htm](feats/ancestry-05-ZZxePfQkBPuTHkt1.htm)|Boulder Roll|Rodar la roca|modificada|
|[ancestry-09-0BUSnsCKOeFCJKEp.htm](feats/ancestry-09-0BUSnsCKOeFCJKEp.htm)|Moderate Enhance Venom|Moderada Mejorar Veneno|modificada|
|[ancestry-09-0R15gtdXoXbrD8As.htm](feats/ancestry-09-0R15gtdXoXbrD8As.htm)|Fortified Mind|Mente Fortificada|modificada|
|[ancestry-09-0X4HZk036A5meZbo.htm](feats/ancestry-09-0X4HZk036A5meZbo.htm)|Serpentine Swimmer|Nadador Serpentino|modificada|
|[ancestry-09-2GrlSP1xhKIz4G8B.htm](feats/ancestry-09-2GrlSP1xhKIz4G8B.htm)|Glory And Valor!|¡Gloria y Valor!|modificada|
|[ancestry-09-2i82PgD1BEtAVeZt.htm](feats/ancestry-09-2i82PgD1BEtAVeZt.htm)|Call of Elysium|Llamada de Elysium|modificada|
|[ancestry-09-2kAyZ3LB28BDhXKa.htm](feats/ancestry-09-2kAyZ3LB28BDhXKa.htm)|Bark and Tendril|Corteza y Zarcillo|modificada|
|[ancestry-09-3TilQ1l97NV0okph.htm](feats/ancestry-09-3TilQ1l97NV0okph.htm)|Invoke The Elements (Veil May)|Invocar Los Elementos (Velo Mayo)|modificada|
|[ancestry-09-3yLdRzh16sT8RgFV.htm](feats/ancestry-09-3yLdRzh16sT8RgFV.htm)|Wheedle and Jig|Wheedle and Jig|modificada|
|[ancestry-09-4Ai8aNHYw7oEj7eE.htm](feats/ancestry-09-4Ai8aNHYw7oEj7eE.htm)|Gripping Limbs|Gripping Limbs|modificada|
|[ancestry-09-4cH2RoWsgXYaWIPa.htm](feats/ancestry-09-4cH2RoWsgXYaWIPa.htm)|Eerie Compression|Eerie Compression|modificada|
|[ancestry-09-4EgueMqUm0ebKV4J.htm](feats/ancestry-09-4EgueMqUm0ebKV4J.htm)|Dragonblood Paragon|Dragonblood Paragon|modificada|
|[ancestry-09-4Jwtl2FvxskruHQv.htm](feats/ancestry-09-4Jwtl2FvxskruHQv.htm)|Invoke The Elements|Invocar Los Elementos|modificada|
|[ancestry-09-56HvICglqH7uR3AY.htm](feats/ancestry-09-56HvICglqH7uR3AY.htm)|Cantorian Rejuvenation|Cantorian Rejuvenation|modificada|
|[ancestry-09-5j3nLvicUVmPqzA7.htm](feats/ancestry-09-5j3nLvicUVmPqzA7.htm)|Tongue Tether|Lengua pegajosa Tether|modificada|
|[ancestry-09-5S1nPoxHTgu9MGGV.htm](feats/ancestry-09-5S1nPoxHTgu9MGGV.htm)|Ricocheting Leap|Salto sin carrerilla|modificada|
|[ancestry-09-67UXSHUUH0K36xyB.htm](feats/ancestry-09-67UXSHUUH0K36xyB.htm)|Smoke Sight|Smoke Sight|modificada|
|[ancestry-09-6eXffyvqxpIzig2O.htm](feats/ancestry-09-6eXffyvqxpIzig2O.htm)|Offensive Subroutine|Subrutina Ofensiva|modificada|
|[ancestry-09-6px0s2nE8fHVWswz.htm](feats/ancestry-09-6px0s2nE8fHVWswz.htm)|Envenom Strike|Golpe Envenenado|modificada|
|[ancestry-09-6WwxTbX8KvU3Xxak.htm](feats/ancestry-09-6WwxTbX8KvU3Xxak.htm)|Janni Hospitality|Janni Hospitalidad|modificada|
|[ancestry-09-7cM7uKRKQDWz5eeu.htm](feats/ancestry-09-7cM7uKRKQDWz5eeu.htm)|Heroes' Call|Llamada, la de los héroes|modificada|
|[ancestry-09-7eyy63BkMv3enk5r.htm](feats/ancestry-09-7eyy63BkMv3enk5r.htm)|Internal Respirator|Respirador Interno|modificada|
|[ancestry-09-7RFu7EwwvjHMP0dq.htm](feats/ancestry-09-7RFu7EwwvjHMP0dq.htm)|Rakshasa Magic|Magia Rakshasa|modificada|
|[ancestry-09-7vUOlVqjheZV0Nmc.htm](feats/ancestry-09-7vUOlVqjheZV0Nmc.htm)|Vivacious Conduit|Conducto vivaz|modificada|
|[ancestry-09-8aRa9VHoDKl9B1Z1.htm](feats/ancestry-09-8aRa9VHoDKl9B1Z1.htm)|Brightness Seeker|Buscador de la iluminación|modificada|
|[ancestry-09-8buK32r3i3aGyyOR.htm](feats/ancestry-09-8buK32r3i3aGyyOR.htm)|Shaitan Magic|Magia Shaitan|modificada|
|[ancestry-09-8znlUoKgr8mmLPe1.htm](feats/ancestry-09-8znlUoKgr8mmLPe1.htm)|Wing Step|Paso de Ala|modificada|
|[ancestry-09-9LTBRvuQgXKZFiZc.htm](feats/ancestry-09-9LTBRvuQgXKZFiZc.htm)|Pack Tactics|Pack Tactics|modificada|
|[ancestry-09-a32r2n9j36khV0Cp.htm](feats/ancestry-09-a32r2n9j36khV0Cp.htm)|Spore Cloud|Nube de esporas|modificada|
|[ancestry-09-AGznaQpn2cE6jz9H.htm](feats/ancestry-09-AGznaQpn2cE6jz9H.htm)|Dangle|Dangle|modificada|
|[ancestry-09-aIm2qi4JZerthZmF.htm](feats/ancestry-09-aIm2qi4JZerthZmF.htm)|Elf Step|Paso elfo|modificada|
|[ancestry-09-AJVx3sHm6f6i8ZQW.htm](feats/ancestry-09-AJVx3sHm6f6i8ZQW.htm)|Efreeti Magic|Magia Efreeti|modificada|
|[ancestry-09-AKMx0GYKLjy7jTXl.htm](feats/ancestry-09-AKMx0GYKLjy7jTXl.htm)|No Evidence|No Evidence|modificada|
|[ancestry-09-AQvtqj2h2n5n8YYg.htm](feats/ancestry-09-AQvtqj2h2n5n8YYg.htm)|Lesser Augmentation|Aumento menor|modificada|
|[ancestry-09-aSgLhr8mM53DWbFc.htm](feats/ancestry-09-aSgLhr8mM53DWbFc.htm)|Endless Memories|Endless Memories|modificada|
|[ancestry-09-ATiQDz27aiBTAt17.htm](feats/ancestry-09-ATiQDz27aiBTAt17.htm)|Roll with It|Roll with It|modificada|
|[ancestry-09-AYXqqeLSqCYhgUY7.htm](feats/ancestry-09-AYXqqeLSqCYhgUY7.htm)|Core Attunement|Core Attunement|modificada|
|[ancestry-09-B53sWzd5irAoLn2U.htm](feats/ancestry-09-B53sWzd5irAoLn2U.htm)|Bone Caller|Llamador de huesos|modificada|
|[ancestry-09-B5HiNholWMwYdHTC.htm](feats/ancestry-09-B5HiNholWMwYdHTC.htm)|Energy Blessed|Bendito por la energía|modificada|
|[ancestry-09-bJTcHDqHOI6xD4AT.htm](feats/ancestry-09-bJTcHDqHOI6xD4AT.htm)|Unyielding Disguise|Disfraz inflexible|modificada|
|[ancestry-09-bo4JG09pkoS7ywSZ.htm](feats/ancestry-09-bo4JG09pkoS7ywSZ.htm)|Cave Climber|Trepador de cueva|modificada|
|[ancestry-09-bpNpqxtsBJIPYSX9.htm](feats/ancestry-09-bpNpqxtsBJIPYSX9.htm)|Ceremony of Strengthened Hand|Ceremonia de la mano reforzada|modificada|
|[ancestry-09-ByqxxJkJiNFtjghh.htm](feats/ancestry-09-ByqxxJkJiNFtjghh.htm)|Mask Of Pain|Máscara de dolor|modificada|
|[ancestry-09-C2PmqTxQgWGTHluf.htm](feats/ancestry-09-C2PmqTxQgWGTHluf.htm)|Studious Adept|Adepto Estudioso|modificada|
|[ancestry-09-C80vQCKQBGRaqcmq.htm](feats/ancestry-09-C80vQCKQBGRaqcmq.htm)|Multitalented|Polifacético|modificada|
|[ancestry-09-CCY6VsGjp5fdmM6K.htm](feats/ancestry-09-CCY6VsGjp5fdmM6K.htm)|Pride in Arms|Orgullo en Armas|modificada|
|[ancestry-09-chM3Pya6H8QGnEGo.htm](feats/ancestry-09-chM3Pya6H8QGnEGo.htm)|Water Dancer|Water Dancer|modificada|
|[ancestry-09-cmhfYMEM4uzrNIiV.htm](feats/ancestry-09-cmhfYMEM4uzrNIiV.htm)|Energize Wings|Energizar Alas|modificada|
|[ancestry-09-COP89tjrNhEucuRW.htm](feats/ancestry-09-COP89tjrNhEucuRW.htm)|Mountain's Stoutness|Corpulencia de la monta|modificada|
|[ancestry-09-cRMUYCnkkE9lSEhh.htm](feats/ancestry-09-cRMUYCnkkE9lSEhh.htm)|Irrepressible (Halfling)|Irreprimible (Halfling)|modificada|
|[ancestry-09-DhqsKns2SaGcOKO9.htm](feats/ancestry-09-DhqsKns2SaGcOKO9.htm)|Read The Stars|Leer Las Estrellas|modificada|
|[ancestry-09-DLE2rr5I1TBAk0I3.htm](feats/ancestry-09-DLE2rr5I1TBAk0I3.htm)|Disorienting Venom|Veneno desorientador|modificada|
|[ancestry-09-DnfVQGLk3PAl8UWh.htm](feats/ancestry-09-DnfVQGLk3PAl8UWh.htm)|Hungry Goblin|Goblin hambriento|modificada|
|[ancestry-09-dtmLxbSy2H8h8e4N.htm](feats/ancestry-09-dtmLxbSy2H8h8e4N.htm)|Skittering Scuttle|Huída a la carrera|modificada|
|[ancestry-09-DTU4jrz9YNr7e62e.htm](feats/ancestry-09-DTU4jrz9YNr7e62e.htm)|Larcenous Tail|Larcenous Tail|modificada|
|[ancestry-09-Eoy0zhpf8tYrTHN4.htm](feats/ancestry-09-Eoy0zhpf8tYrTHN4.htm)|Shory Aeromancer|Aeromante shory|modificada|
|[ancestry-09-Ezk3OgfPaRlEEyAD.htm](feats/ancestry-09-Ezk3OgfPaRlEEyAD.htm)|Virtue-Forged Tattoos|Tatuajes forjados por la virtud|modificada|
|[ancestry-09-fcSzCdQ9y800BdOv.htm](feats/ancestry-09-fcSzCdQ9y800BdOv.htm)|Velstrac Magic|Magia Velstrac|modificada|
|[ancestry-09-fEzxwMCNyvooYqdn.htm](feats/ancestry-09-fEzxwMCNyvooYqdn.htm)|Miraculous Repair|Reparar Milagroso|modificada|
|[ancestry-09-FGbrxFmNeOfPHFOG.htm](feats/ancestry-09-FGbrxFmNeOfPHFOG.htm)|Bone Missile|Misil de hueso|modificada|
|[ancestry-09-fiA3rfqPcCKFCI83.htm](feats/ancestry-09-fiA3rfqPcCKFCI83.htm)|Towering Growth|Towering Growth|modificada|
|[ancestry-09-FrzskqwNWexKY5BA.htm](feats/ancestry-09-FrzskqwNWexKY5BA.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[ancestry-09-ftntYtiKGoPBpvcv.htm](feats/ancestry-09-ftntYtiKGoPBpvcv.htm)|Qlippoth Magic|Magia Qlippoth|modificada|
|[ancestry-09-FUOUyhHufNH4ri7H.htm](feats/ancestry-09-FUOUyhHufNH4ri7H.htm)|Wings Of Air|Wings Of Air|modificada|
|[ancestry-09-FZZXgmVMAmvKHWIo.htm](feats/ancestry-09-FZZXgmVMAmvKHWIo.htm)|Breath Like Honey|Aliento de miel|modificada|
|[ancestry-09-g9AOSz4QDAlUee9M.htm](feats/ancestry-09-g9AOSz4QDAlUee9M.htm)|Fox Trick|Fox Trick|modificada|
|[ancestry-09-gWyCNTWUhxneOBne.htm](feats/ancestry-09-gWyCNTWUhxneOBne.htm)|Helpful Halfling|Halfling servicial|modificada|
|[ancestry-09-hgfRHSS5BsoyQ9Fj.htm](feats/ancestry-09-hgfRHSS5BsoyQ9Fj.htm)|Marid Magic|Marid Magic|modificada|
|[ancestry-09-HPW7Qi02lkVprW6V.htm](feats/ancestry-09-HPW7Qi02lkVprW6V.htm)|Silent Step|Paso silencioso|modificada|
|[ancestry-09-Ht21JJ95wiHOgZoT.htm](feats/ancestry-09-Ht21JJ95wiHOgZoT.htm)|Telekinetic Slip|Desliz Telequinético|modificada|
|[ancestry-09-ieFjiZSlT9J4boqP.htm](feats/ancestry-09-ieFjiZSlT9J4boqP.htm)|Dangle (Vanara)|Dangle (Vanara)|modificada|
|[ancestry-09-IHFqUHxNChCPZPml.htm](feats/ancestry-09-IHFqUHxNChCPZPml.htm)|Garuda Magic|Garuda Magic|modificada|
|[ancestry-09-IlzUi1viWgeRolU8.htm](feats/ancestry-09-IlzUi1viWgeRolU8.htm)|Legendary Size|Tamaño legendario|modificada|
|[ancestry-09-InTchkd50pzQok3f.htm](feats/ancestry-09-InTchkd50pzQok3f.htm)|Web Hunter|Cazador de telara|modificada|
|[ancestry-09-iXhMlT5rlLngybxX.htm](feats/ancestry-09-iXhMlT5rlLngybxX.htm)|Slip Into Shadow|Deslizarse en la Sombra|modificada|
|[ancestry-09-IzNyByUBNH94MDOr.htm](feats/ancestry-09-IzNyByUBNH94MDOr.htm)|Fey Ascension|Ascensión feérica|modificada|
|[ancestry-09-JeMGDAD5s9AXCZ2G.htm](feats/ancestry-09-JeMGDAD5s9AXCZ2G.htm)|Animal Magic|Magia Animal|modificada|
|[ancestry-09-jpRVp1INDAlYWvlI.htm](feats/ancestry-09-jpRVp1INDAlYWvlI.htm)|Rakshasa Ravaged|Rakshasa Arruinado|modificada|
|[ancestry-09-jzfV4lJ0721Hfzq1.htm](feats/ancestry-09-jzfV4lJ0721Hfzq1.htm)|Squad Tactics|Tácticas de escuadrón|modificada|
|[ancestry-09-k0sXAn4PPq5nW9al.htm](feats/ancestry-09-k0sXAn4PPq5nW9al.htm)|Guiding Luck|Suerte que guía|modificada|
|[ancestry-09-K9ixUl7PrNbBHGdA.htm](feats/ancestry-09-K9ixUl7PrNbBHGdA.htm)|Sodbuster|Sodbuster|modificada|
|[ancestry-09-kIIcgcc5SWFkyiBj.htm](feats/ancestry-09-kIIcgcc5SWFkyiBj.htm)|Transcendent Realization|Realización Trascendente|modificada|
|[ancestry-09-kjRMXN95lQmIi2hP.htm](feats/ancestry-09-kjRMXN95lQmIi2hP.htm)|Earthsense|Earthsense|modificada|
|[ancestry-09-kPyyZGD5L6b2Kl8C.htm](feats/ancestry-09-kPyyZGD5L6b2Kl8C.htm)|Constant Gaze|Mirada constante|modificada|
|[ancestry-09-KTLdk65OOAVixqtY.htm](feats/ancestry-09-KTLdk65OOAVixqtY.htm)|Stonewalker|Caminante de la piedra|modificada|
|[ancestry-09-Kuv9P0NdtpE8yPJL.htm](feats/ancestry-09-Kuv9P0NdtpE8yPJL.htm)|Solar Rejuvenation (Ghoran)|Rejuvenecimiento Solar (Ghoran)|modificada|
|[ancestry-09-l4ux6Mn2fklB2cXM.htm](feats/ancestry-09-l4ux6Mn2fklB2cXM.htm)|Evade Doom|Evadir Perdición|modificada|
|[ancestry-09-lBVzIet6IpufLXZg.htm](feats/ancestry-09-lBVzIet6IpufLXZg.htm)|Swift Application|Aplicación Celeridad|modificada|
|[ancestry-09-lHcDb9oXUdFupRdi.htm](feats/ancestry-09-lHcDb9oXUdFupRdi.htm)|Incredible Improvisation|Improvisación sensacional|modificada|
|[ancestry-09-LhpE0NsfNwYP6MOz.htm](feats/ancestry-09-LhpE0NsfNwYP6MOz.htm)|Freeze It!|¡Congélalo!|modificada|
|[ancestry-09-llWnSLYALh88iRGQ.htm](feats/ancestry-09-llWnSLYALh88iRGQ.htm)|Rain of Bolts|Rain of Bolts|modificada|
|[ancestry-09-LRBzEzpS19z3Eghd.htm](feats/ancestry-09-LRBzEzpS19z3Eghd.htm)|Sense Thoughts|Sense Thoughts|modificada|
|[ancestry-09-lT2sOwC6Pi5P4Yq0.htm](feats/ancestry-09-lT2sOwC6Pi5P4Yq0.htm)|Ancillary Motes|Motas Auxiliares|modificada|
|[ancestry-09-m25EfSMmEjJPSyJj.htm](feats/ancestry-09-m25EfSMmEjJPSyJj.htm)|Perfume Cloud|Nube de Perfume|modificada|
|[ancestry-09-m7aW56wExv2ieMFL.htm](feats/ancestry-09-m7aW56wExv2ieMFL.htm)|Invoke The Elements (Virga May)|Invocar Los Elementos (Virga May)|modificada|
|[ancestry-09-mnH68QcFRtkMbNE0.htm](feats/ancestry-09-mnH68QcFRtkMbNE0.htm)|Battleforger|Battleforger|modificada|
|[ancestry-09-MTp2j4N4H4wj07pH.htm](feats/ancestry-09-MTp2j4N4H4wj07pH.htm)|Heir of the Saoc|Heredero de los Saoc|modificada|
|[ancestry-09-mU8vTzrWX9fIlG0d.htm](feats/ancestry-09-mU8vTzrWX9fIlG0d.htm)|Grandmother's Wisdom|Sabiduría de la abuela|modificada|
|[ancestry-09-mXNwdRSM9kZrT2Um.htm](feats/ancestry-09-mXNwdRSM9kZrT2Um.htm)|Fell Rider|Fell Rider|modificada|
|[ancestry-09-N8Ci3w5gQ68rj6a6.htm](feats/ancestry-09-N8Ci3w5gQ68rj6a6.htm)|Arcane Camouflage|Camuflaje Arcano|modificada|
|[ancestry-09-Nb8iLgQeuHU73hQM.htm](feats/ancestry-09-Nb8iLgQeuHU73hQM.htm)|Mirror Refuge|Refugio Espejo|modificada|
|[ancestry-09-NEl8G3SlwxOR9Zx1.htm](feats/ancestry-09-NEl8G3SlwxOR9Zx1.htm)|Preemptive Reconfiguration|Preemptive Reconfiguration|modificada|
|[ancestry-09-ngNzsvIpnj1iLfSC.htm](feats/ancestry-09-ngNzsvIpnj1iLfSC.htm)|Anarchic Arcana|Arcanos Anárquicos|modificada|
|[ancestry-09-nHoRM1gLL7MtIiCS.htm](feats/ancestry-09-nHoRM1gLL7MtIiCS.htm)|Djinni Magic|Magia Djinni|modificada|
|[ancestry-09-NjPZbQjJJIygS1ru.htm](feats/ancestry-09-NjPZbQjJJIygS1ru.htm)|Shadow Sight|Sombra Sight|modificada|
|[ancestry-09-NnpUhj7d4RmfOKTE.htm](feats/ancestry-09-NnpUhj7d4RmfOKTE.htm)|Scorching Disarm|Desarmar abrasador|modificada|
|[ancestry-09-nqDhQdkgsnvebUMr.htm](feats/ancestry-09-nqDhQdkgsnvebUMr.htm)|Snare Commando|Snare Commando|modificada|
|[ancestry-09-nYSSdczKdRj7pdW6.htm](feats/ancestry-09-nYSSdczKdRj7pdW6.htm)|Ceremony of Aeon's Guidance|Ceremonia de la orientación divina|modificada|
|[ancestry-09-o1LhDmwymrpEy1u2.htm](feats/ancestry-09-o1LhDmwymrpEy1u2.htm)|Riptide|Riptide|modificada|
|[ancestry-09-O2iFrAt7hyELJlIR.htm](feats/ancestry-09-O2iFrAt7hyELJlIR.htm)|Slip The Grasp|Slip The Grasp|modificada|
|[ancestry-09-O80QCYMqz4VZsfxT.htm](feats/ancestry-09-O80QCYMqz4VZsfxT.htm)|Hefting Shadow|Hefting Sombra|modificada|
|[ancestry-09-oGPX29AOsyHj18mK.htm](feats/ancestry-09-oGPX29AOsyHj18mK.htm)|Flower Magic|Magia floral|modificada|
|[ancestry-09-p5dLNQ5HtM9Fq8SN.htm](feats/ancestry-09-p5dLNQ5HtM9Fq8SN.htm)|Invoke The Elements (Brine May)|Invocar los elementos (maia salitre)|modificada|
|[ancestry-09-pe6cHJzzMm5Tr25G.htm](feats/ancestry-09-pe6cHJzzMm5Tr25G.htm)|Juvenile Flight|Vuelo Juvenil|modificada|
|[ancestry-09-pr7e8xzrl8OLp6U9.htm](feats/ancestry-09-pr7e8xzrl8OLp6U9.htm)|Coating Of Slime|Capa De Baba|modificada|
|[ancestry-09-pt4oMtjAVGFQHtVw.htm](feats/ancestry-09-pt4oMtjAVGFQHtVw.htm)|Catrina's Presence|Presencia de Catrina|modificada|
|[ancestry-09-Q1lGguNI4SqPwgVn.htm](feats/ancestry-09-Q1lGguNI4SqPwgVn.htm)|Dragon Prince|Príncipe Dragón|modificada|
|[ancestry-09-Q8fdMjZ2Wv3FawOI.htm](feats/ancestry-09-Q8fdMjZ2Wv3FawOI.htm)|Water Strider|Water Strider|modificada|
|[ancestry-09-Q8XZYvatnYo4VlAQ.htm](feats/ancestry-09-Q8XZYvatnYo4VlAQ.htm)|Agathion Magic|Magia Agathion|modificada|
|[ancestry-09-qAdlJvAHkWBisDJ0.htm](feats/ancestry-09-qAdlJvAHkWBisDJ0.htm)|Ceremony of Aeon's Shield|Ceremonia del escudo del eón|modificada|
|[ancestry-09-Qe9IUDVo7gsowMHq.htm](feats/ancestry-09-Qe9IUDVo7gsowMHq.htm)|Fey Magic|Magia feérica|modificada|
|[ancestry-09-QNgnwkKZmJR5jT7K.htm](feats/ancestry-09-QNgnwkKZmJR5jT7K.htm)|Laughing Gnoll|Gnoll risue|modificada|
|[ancestry-09-QpvmQTvYaiT6iDJR.htm](feats/ancestry-09-QpvmQTvYaiT6iDJR.htm)|Guarded Thoughts|Pensamientos Guardados|modificada|
|[ancestry-09-qr1E37Tla555tvIO.htm](feats/ancestry-09-qr1E37Tla555tvIO.htm)|Fade Away|Fade Away|modificada|
|[ancestry-09-QUaSGUmRnlMbzw1P.htm](feats/ancestry-09-QUaSGUmRnlMbzw1P.htm)|Wind God's Fan|Abanico del dios del viento|modificada|
|[ancestry-09-RdnzuBhkEv7TKsNi.htm](feats/ancestry-09-RdnzuBhkEv7TKsNi.htm)|Asura Magic|Magia Asura|modificada|
|[ancestry-09-RN5aZCzEnmlYmBf5.htm](feats/ancestry-09-RN5aZCzEnmlYmBf5.htm)|Crystalline Cloud|Nube Cristalina|modificada|
|[ancestry-09-RTxPL1reRcJhhYeG.htm](feats/ancestry-09-RTxPL1reRcJhhYeG.htm)|Analyze Information|Analizar información|modificada|
|[ancestry-09-rU9Aw05FFLVq0MTV.htm](feats/ancestry-09-rU9Aw05FFLVq0MTV.htm)|Close Quarters|Close Quarters|modificada|
|[ancestry-09-rw4qq4qmCbO0hRfH.htm](feats/ancestry-09-rw4qq4qmCbO0hRfH.htm)|Peri Magic|Peri Magic|modificada|
|[ancestry-09-RxA1PdgGbijkieJD.htm](feats/ancestry-09-RxA1PdgGbijkieJD.htm)|Defensive Instincts|Instintos defensivos|modificada|
|[ancestry-09-rzBC5bHAWWpjHMEw.htm](feats/ancestry-09-rzBC5bHAWWpjHMEw.htm)|Ceremony of Fortification|Ceremonia de fortificación|modificada|
|[ancestry-09-Sav50NxWdLnbaDWQ.htm](feats/ancestry-09-Sav50NxWdLnbaDWQ.htm)|Ferocious Gust|Ráfaga feroz|modificada|
|[ancestry-09-sbAb2a6BzZpcYv8y.htm](feats/ancestry-09-sbAb2a6BzZpcYv8y.htm)|Mother's Mindfulness|La atención de la madre|modificada|
|[ancestry-09-SjwISllgvlKEcjSv.htm](feats/ancestry-09-SjwISllgvlKEcjSv.htm)|Two Truths|Two Truths|modificada|
|[ancestry-09-sTaeqBJCH1HQy96W.htm](feats/ancestry-09-sTaeqBJCH1HQy96W.htm)|Invoke The Elements (Snow May)|Invocar los elementos (Nieve de mayo)|modificada|
|[ancestry-09-swsMURQBMXZpjWl8.htm](feats/ancestry-09-swsMURQBMXZpjWl8.htm)|Cunning Climber|Astucia escaladora|modificada|
|[ancestry-09-SxRmlDYhYEkq10Ak.htm](feats/ancestry-09-SxRmlDYhYEkq10Ak.htm)|Serpentcoil Slam|Serpentcoil Slam|modificada|
|[ancestry-09-T4OSMMEvbymMzlIJ.htm](feats/ancestry-09-T4OSMMEvbymMzlIJ.htm)|Rejuvenation Token|Ficha de rejuvenecimiento|modificada|
|[ancestry-09-T8cBEhuHWkh3MqgO.htm](feats/ancestry-09-T8cBEhuHWkh3MqgO.htm)|Sense For Trouble|Sense For Trouble|modificada|
|[ancestry-09-tEzwhF3uFIi825xj.htm](feats/ancestry-09-tEzwhF3uFIi825xj.htm)|Scaling Poppet|Scaling Poppet|modificada|
|[ancestry-09-Thhcli0PR7HBBcPX.htm](feats/ancestry-09-Thhcli0PR7HBBcPX.htm)|Aboleth Transmutation|Transmutación de Aboleth|modificada|
|[ancestry-09-tkY5jXELipWjC8k2.htm](feats/ancestry-09-tkY5jXELipWjC8k2.htm)|Viper Strike|Golpe de víbora|modificada|
|[ancestry-09-TPX0fm9wxndpIqpk.htm](feats/ancestry-09-TPX0fm9wxndpIqpk.htm)|Morrigna's Spider Affinity|Afinidad con la araña de Morrigna|modificada|
|[ancestry-09-TRC4DgVq07cZO65B.htm](feats/ancestry-09-TRC4DgVq07cZO65B.htm)|Thorned Seedpod|Vaina de semillas espinosa|modificada|
|[ancestry-09-TwAps3ewk7KFHKDv.htm](feats/ancestry-09-TwAps3ewk7KFHKDv.htm)|Sculpt Shadows|Esculpir Sombras|modificada|
|[ancestry-09-Twhkz2FfzaZezVnG.htm](feats/ancestry-09-Twhkz2FfzaZezVnG.htm)|Fortuitous Shift|Cambio Fortuito|modificada|
|[ancestry-09-tyae2vpOiAMxXvQH.htm](feats/ancestry-09-tyae2vpOiAMxXvQH.htm)|Inner Breath|Inner Breath|modificada|
|[ancestry-09-u94fcPT5Oukqzql5.htm](feats/ancestry-09-u94fcPT5Oukqzql5.htm)|Azarketi Purification|Purificación Azarketi|modificada|
|[ancestry-09-UdwXT24zrLzg2ZIV.htm](feats/ancestry-09-UdwXT24zrLzg2ZIV.htm)|Strong Swimmer|Strong Swimmer|modificada|
|[ancestry-09-ulQzdBOnZH9LQu8M.htm](feats/ancestry-09-ulQzdBOnZH9LQu8M.htm)|Scalding Spit|Scalding Spit|modificada|
|[ancestry-09-uOFzs058hy144rzm.htm](feats/ancestry-09-uOFzs058hy144rzm.htm)|Captivating Curiosity|Curiosidad Cautivadora|modificada|
|[ancestry-09-UojXcKf98oAFJUE0.htm](feats/ancestry-09-UojXcKf98oAFJUE0.htm)|Skeletal Resistance|Resistencia esquelética|modificada|
|[ancestry-09-ux6kbsqRMsu9VHtn.htm](feats/ancestry-09-ux6kbsqRMsu9VHtn.htm)|Quill Spray|Rociada de púas|modificada|
|[ancestry-09-VazbV1s93eZqUZIu.htm](feats/ancestry-09-VazbV1s93eZqUZIu.htm)|Improvisational Warrior|Guerrero improvisador|modificada|
|[ancestry-09-vB7xdAJiZ1gWW2Yj.htm](feats/ancestry-09-vB7xdAJiZ1gWW2Yj.htm)|Jalmeri Rakshasa Magic|Jalmeri Rakshasa Magia|modificada|
|[ancestry-09-velPTcpjLXPnaYrm.htm](feats/ancestry-09-velPTcpjLXPnaYrm.htm)|Solar Rejuvenation|Rejuvenecimiento Solar|modificada|
|[ancestry-09-vfuHVSuExvtyajkW.htm](feats/ancestry-09-vfuHVSuExvtyajkW.htm)|Expert Longevity|Longevidad experta|modificada|
|[ancestry-09-VphmK5JRR35SyEhV.htm](feats/ancestry-09-VphmK5JRR35SyEhV.htm)|Pervasive Superstition|Superstición omnipresente|modificada|
|[ancestry-09-VtFuhfIIw8OoLF6v.htm](feats/ancestry-09-VtFuhfIIw8OoLF6v.htm)|Alluring Performance|Interpretación Seductora|modificada|
|[ancestry-09-vyb3BYDiIl2MiZp4.htm](feats/ancestry-09-vyb3BYDiIl2MiZp4.htm)|Absorb Toxin|Absorber toxinas|modificada|
|[ancestry-09-wGzKq2cx1b2Ycl4u.htm](feats/ancestry-09-wGzKq2cx1b2Ycl4u.htm)|Kneel for No God|Arrodíllate ante ningún dios|modificada|
|[ancestry-09-WldISqAE5Rw3Ewzn.htm](feats/ancestry-09-WldISqAE5Rw3Ewzn.htm)|Demolitionist|Demoledor|modificada|
|[ancestry-09-WQa6PxkOgyvRpaaM.htm](feats/ancestry-09-WQa6PxkOgyvRpaaM.htm)|Predator's Growl|Gruñido de depredador|modificada|
|[ancestry-09-WSxHCapuTn8uRdLI.htm](feats/ancestry-09-WSxHCapuTn8uRdLI.htm)|Dragon Grip|Dragon Grip|modificada|
|[ancestry-09-WUVNZoIZvr9XFv2x.htm](feats/ancestry-09-WUVNZoIZvr9XFv2x.htm)|First World Adept|Adepto del Primer Mundo|modificada|
|[ancestry-09-xDCtfNtbnaG166cy.htm](feats/ancestry-09-xDCtfNtbnaG166cy.htm)|Janni Magic|Magia Janni|modificada|
|[ancestry-09-xfbPSP9tl1N95xDF.htm](feats/ancestry-09-xfbPSP9tl1N95xDF.htm)|Arcane Slam|Arcane Slam|modificada|
|[ancestry-09-xM8IWff5yfbQGk4s.htm](feats/ancestry-09-xM8IWff5yfbQGk4s.htm)|Briar Battler|Batallador de los Zarzas|modificada|
|[ancestry-09-Xwk41o4fERfM07NR.htm](feats/ancestry-09-Xwk41o4fERfM07NR.htm)|Rokoan Arts|Rokoan Arts|modificada|
|[ancestry-09-YfjuRHzqLFhLLgCc.htm](feats/ancestry-09-YfjuRHzqLFhLLgCc.htm)|Piercing Quills|Púas perforantes|modificada|
|[ancestry-09-YgytD4HGjWNFwiev.htm](feats/ancestry-09-YgytD4HGjWNFwiev.htm)|Arcane Propulsion|Propulsión Arcana|modificada|
|[ancestry-09-YSEqqNx3McbS7k4n.htm](feats/ancestry-09-YSEqqNx3McbS7k4n.htm)|Embodied Dragoon Subjectivity|Dragoon subjetividad encarnada|modificada|
|[ancestry-09-yYIyAbwpdkySiCMU.htm](feats/ancestry-09-yYIyAbwpdkySiCMU.htm)|Uncanny Cheeks|Uncanny Cheeks|modificada|
|[ancestry-09-Z2EiYMtGoNnwW6Tk.htm](feats/ancestry-09-Z2EiYMtGoNnwW6Tk.htm)|Cooperative Soul|Alma cooperativa|modificada|
|[ancestry-09-z54Jl3KRZpA2UaZV.htm](feats/ancestry-09-z54Jl3KRZpA2UaZV.htm)|Tetraelemental Assault|Asalto Tetraelemental|modificada|
|[ancestry-09-ZGnmky2B3v1pbPDA.htm](feats/ancestry-09-ZGnmky2B3v1pbPDA.htm)|Strand Strider|Strand Strider|modificada|
|[ancestry-09-ZGT7NLpcvREkJsMd.htm](feats/ancestry-09-ZGT7NLpcvREkJsMd.htm)|Repair Module|Módulo Reparar|modificada|
|[ancestry-09-ZPJbjH5XCp39TVu7.htm](feats/ancestry-09-ZPJbjH5XCp39TVu7.htm)|Ragdya's Dance|Danza de Ragdya|modificada|
|[ancestry-09-zpQEOqMDoujexNfA.htm](feats/ancestry-09-zpQEOqMDoujexNfA.htm)|Replenishing Hydration|Hidratación Reponedora|modificada|
|[ancestry-09-zsubK6PaY58fOYCb.htm](feats/ancestry-09-zsubK6PaY58fOYCb.htm)|Lucky Keepsake|Custodiar la Suerte|modificada|
|[ancestry-09-zsWIZQeVhIihNw6M.htm](feats/ancestry-09-zsWIZQeVhIihNw6M.htm)|Charred Remains|Restos carbonizados|modificada|
|[ancestry-09-ZvrzK8NM390k139E.htm](feats/ancestry-09-ZvrzK8NM390k139E.htm)|Drain Emotion|Drenar Emoción|modificada|
|[ancestry-13-0jJ5FG72lydY3HHR.htm](feats/ancestry-13-0jJ5FG72lydY3HHR.htm)|Hydraulic Maneuvers|Maniobras hidráulicas|modificada|
|[ancestry-13-14dFcInubWcPnFzR.htm](feats/ancestry-13-14dFcInubWcPnFzR.htm)|Eternal Memories|Recuerdos Eternos|modificada|
|[ancestry-13-1jZ7f4TJqiFH8Ied.htm](feats/ancestry-13-1jZ7f4TJqiFH8Ied.htm)|Thunder God's Fan|Abanico del dios Tronante|modificada|
|[ancestry-13-1lpygRsa487Jto4L.htm](feats/ancestry-13-1lpygRsa487Jto4L.htm)|Revivification Protocol|Protocolo de Revivificación|modificada|
|[ancestry-13-1WPKV2sInXtiUaeG.htm](feats/ancestry-13-1WPKV2sInXtiUaeG.htm)|Flame Jump|Salto de Flamígera|modificada|
|[ancestry-13-2jy4uh04yz0ezz6Q.htm](feats/ancestry-13-2jy4uh04yz0ezz6Q.htm)|Reanimating Spark|Chispa Reanimadora|modificada|
|[ancestry-13-3X8BkDZEDWp90U4u.htm](feats/ancestry-13-3X8BkDZEDWp90U4u.htm)|Ancestor's Transformation|Transformación del Ancestro|modificada|
|[ancestry-13-3YByhZJi93ie5F45.htm](feats/ancestry-13-3YByhZJi93ie5F45.htm)|Shaitan Skin|Piel de Shaitan|modificada|
|[ancestry-13-48z7BPYIZJIgj3x5.htm](feats/ancestry-13-48z7BPYIZJIgj3x5.htm)|We March On|We March On|modificada|
|[ancestry-13-569SxRBvTCDiHlbW.htm](feats/ancestry-13-569SxRBvTCDiHlbW.htm)|Summon Air Elemental|Convocar elemental de aire|modificada|
|[ancestry-13-5BEY7VzDVuiAg4PX.htm](feats/ancestry-13-5BEY7VzDVuiAg4PX.htm)|Impossible Gossip|Cotilleo Imposible|modificada|
|[ancestry-13-5H2KmhiIGuPxKwBK.htm](feats/ancestry-13-5H2KmhiIGuPxKwBK.htm)|Nagaji Spell expertise|Experiencia en hechizos Nagaji|modificada|
|[ancestry-13-5iHB5ZFJ25XrZHye.htm](feats/ancestry-13-5iHB5ZFJ25XrZHye.htm)|Inspirit Hazard|Inspirit Hazard|modificada|
|[ancestry-13-5q8a36QyYAslgnsk.htm](feats/ancestry-13-5q8a36QyYAslgnsk.htm)|Eldritch Calm|Eldritch Calm|modificada|
|[ancestry-13-607fIUg4JjG0aIRx.htm](feats/ancestry-13-607fIUg4JjG0aIRx.htm)|Goblin Weapon Expertise|Experiencia con las armas de los goblins.|modificada|
|[ancestry-13-6GGPTYwljGeGL2B3.htm](feats/ancestry-13-6GGPTYwljGeGL2B3.htm)|Grippli Weapon Expertise|Experiencia con las armas de los grippli|modificada|
|[ancestry-13-7bVJEZt2vwAAnILV.htm](feats/ancestry-13-7bVJEZt2vwAAnILV.htm)|Suli Amir|Suli Amir|modificada|
|[ancestry-13-7Lx8rsEbBJkg6C17.htm](feats/ancestry-13-7Lx8rsEbBJkg6C17.htm)|Gift Of The Moon|Regalo De La Luna|modificada|
|[ancestry-13-7NsqI50oBIJ4bFwb.htm](feats/ancestry-13-7NsqI50oBIJ4bFwb.htm)|Monkey Spirits|Monkey Spirits|modificada|
|[ancestry-13-8Qn80RunXaChOM5p.htm](feats/ancestry-13-8Qn80RunXaChOM5p.htm)|Shadow's Assault|Asalto de Sombra|modificada|
|[ancestry-13-8VXYwHE5LqAGRGTB.htm](feats/ancestry-13-8VXYwHE5LqAGRGTB.htm)|Kashrishi Revivification|Revivificación Kashrishi|modificada|
|[ancestry-13-93vQcuKBESXUKoH5.htm](feats/ancestry-13-93vQcuKBESXUKoH5.htm)|Killing Stone|Piedra Asesina|modificada|
|[ancestry-13-99WRahrMC91D6MMe.htm](feats/ancestry-13-99WRahrMC91D6MMe.htm)|Unbreakable-er Goblin|Goblin irrompible-er|modificada|
|[ancestry-13-9eL7W4rvs4sjhWFT.htm](feats/ancestry-13-9eL7W4rvs4sjhWFT.htm)|Radiant Burst|Ráfaga radiante|modificada|
|[ancestry-13-ACeqRSWr4CEwLZgO.htm](feats/ancestry-13-ACeqRSWr4CEwLZgO.htm)|Shadow Pact|Pacto de Sombra|modificada|
|[ancestry-13-aEhUX2c1xj57CMw5.htm](feats/ancestry-13-aEhUX2c1xj57CMw5.htm)|Vishkanya Weapon Expertise|Experiencia con las armas Vishkanya|modificada|
|[ancestry-13-AP9PXXaP4a2sMdt2.htm](feats/ancestry-13-AP9PXXaP4a2sMdt2.htm)|Enforced Order|Orden Forzada|modificada|
|[ancestry-13-aQHB5LhxLaFRkzSt.htm](feats/ancestry-13-aQHB5LhxLaFRkzSt.htm)|Finest Trick|Finest Trick|modificada|
|[ancestry-13-aqTlEPtd5gzs1Lxg.htm](feats/ancestry-13-aqTlEPtd5gzs1Lxg.htm)|Unconventional Expertise|Experiencia poco convencional|modificada|
|[ancestry-13-arlZTqfppOAXBhdw.htm](feats/ancestry-13-arlZTqfppOAXBhdw.htm)|Irriseni Ice-Witch|Bruja de Hielo Irriseni|modificada|
|[ancestry-13-aRqXpCBWic6a3DQH.htm](feats/ancestry-13-aRqXpCBWic6a3DQH.htm)|Venom Purge|Venom Purge|modificada|
|[ancestry-13-AxqnIMh5WbSah5OS.htm](feats/ancestry-13-AxqnIMh5WbSah5OS.htm)|Mischievous Tail|Mischievous Tail|modificada|
|[ancestry-13-b5j3boj1iEcXSB9f.htm](feats/ancestry-13-b5j3boj1iEcXSB9f.htm)|Reimagine|Reimagine|modificada|
|[ancestry-13-bi77bT9uyAmJXVed.htm](feats/ancestry-13-bi77bT9uyAmJXVed.htm)|Truespeech|Truespeech|modificada|
|[ancestry-13-bj0y7JnIboNfCuC8.htm](feats/ancestry-13-bj0y7JnIboNfCuC8.htm)|Gnoll Weapon Expertise|Experiencia con las armas Gnoll|modificada|
|[ancestry-13-BTQj2N5erpJDWNFA.htm](feats/ancestry-13-BTQj2N5erpJDWNFA.htm)|Astral Blink|Intermitencia Astral|modificada|
|[ancestry-13-c5xHL6CDFwBqXx2a.htm](feats/ancestry-13-c5xHL6CDFwBqXx2a.htm)|Spiteful Rake|Rastrillo Rencoroso|modificada|
|[ancestry-13-CAfrSLaDM0OBaNtp.htm](feats/ancestry-13-CAfrSLaDM0OBaNtp.htm)|See the Unseen|Ver lo invisible|modificada|
|[ancestry-13-Cf0CDTZvGaYDAXUN.htm](feats/ancestry-13-Cf0CDTZvGaYDAXUN.htm)|Calaca's Showstopper|Calaca's Showstopper|modificada|
|[ancestry-13-cIJfKlbfKezdDbwK.htm](feats/ancestry-13-cIJfKlbfKezdDbwK.htm)|Summon Fire Elemental|Convocar elemental de fuego|modificada|
|[ancestry-13-cpsyKarYRHiOF0Nd.htm](feats/ancestry-13-cpsyKarYRHiOF0Nd.htm)|None Shall Know|Nadie debe saberlo|modificada|
|[ancestry-13-d3J74jmGOrPBWUm9.htm](feats/ancestry-13-d3J74jmGOrPBWUm9.htm)|Disruptive Stare|Mirada Disruptora|modificada|
|[ancestry-13-DIjpbE2dh5MRGiYO.htm](feats/ancestry-13-DIjpbE2dh5MRGiYO.htm)|Impose Order|Imponer Orden|modificada|
|[ancestry-13-DNZlWe2V28KoajoN.htm](feats/ancestry-13-DNZlWe2V28KoajoN.htm)|Eclectic Sword Mastery|Eclectic Sword Mastery|modificada|
|[ancestry-13-dor2F3g8gL2KVwX6.htm](feats/ancestry-13-dor2F3g8gL2KVwX6.htm)|Skirt The Light|Falda La Luz|modificada|
|[ancestry-13-E0EARGd5iryHJGJD.htm](feats/ancestry-13-E0EARGd5iryHJGJD.htm)|Unbound Freedom|Libertad sin límites|modificada|
|[ancestry-13-ECcFHAeAmh5F3mxg.htm](feats/ancestry-13-ECcFHAeAmh5F3mxg.htm)|Azarketi Weapon Expertise|Azarketi experiencia con las armas|modificada|
|[ancestry-13-eGfYZoWC6cDa2XWd.htm](feats/ancestry-13-eGfYZoWC6cDa2XWd.htm)|Jinx Glutton|Jinx Glutton|modificada|
|[ancestry-13-EiiCCJqWnN5RYMV4.htm](feats/ancestry-13-EiiCCJqWnN5RYMV4.htm)|Universal Longevity|Longevidad universal|modificada|
|[ancestry-13-EIyazsXwM7Zc2XGO.htm](feats/ancestry-13-EIyazsXwM7Zc2XGO.htm)|Arcane Sight|Vista Arcana|modificada|
|[ancestry-13-ewwhgHZbxHcpMdLn.htm](feats/ancestry-13-ewwhgHZbxHcpMdLn.htm)|Glamour|Glamour|modificada|
|[ancestry-13-FI4MnH0KQfIKJRNT.htm](feats/ancestry-13-FI4MnH0KQfIKJRNT.htm)|Arcane Locomotion|Locomoción Arcana|modificada|
|[ancestry-13-Fn8dEIcUZVxuWJgN.htm](feats/ancestry-13-Fn8dEIcUZVxuWJgN.htm)|Summon Water Elemental|Convocar elemental de agua|modificada|
|[ancestry-13-fnMT0AsZXFW9Ppyp.htm](feats/ancestry-13-fnMT0AsZXFW9Ppyp.htm)|Cloak Of Poison|Capa De Veneno|modificada|
|[ancestry-13-g6M5mapOXVf0g9BG.htm](feats/ancestry-13-g6M5mapOXVf0g9BG.htm)|Orc Weapon Expertise|Experiencia con las armas de los orcos|modificada|
|[ancestry-13-GELhS1k0vHEi1PK3.htm](feats/ancestry-13-GELhS1k0vHEi1PK3.htm)|Incredible Ferocity|Ferocidad sensacional|modificada|
|[ancestry-13-GfpHBiOX02PQz0Pm.htm](feats/ancestry-13-GfpHBiOX02PQz0Pm.htm)|Spew Tentacles|Spew Tentacles|modificada|
|[ancestry-13-Gh0rJNsdxBacK9b8.htm](feats/ancestry-13-Gh0rJNsdxBacK9b8.htm)|Conrasu Weapon Expertise|Experiencia con las armas de conrasu.|modificada|
|[ancestry-13-GndRoEYD3uWjJji3.htm](feats/ancestry-13-GndRoEYD3uWjJji3.htm)|Purge Sins|Purgar Pecados|modificada|
|[ancestry-13-gsW6mCMBWquLM3bj.htm](feats/ancestry-13-gsW6mCMBWquLM3bj.htm)|Halfling Weapon Expertise|Experiencia con las armas halfling|modificada|
|[ancestry-13-gwzjvKQQ6zmgVVmS.htm](feats/ancestry-13-gwzjvKQQ6zmgVVmS.htm)|Daywalker|Caminante diurno|modificada|
|[ancestry-13-H965m1koFvY4FQkF.htm](feats/ancestry-13-H965m1koFvY4FQkF.htm)|Form Of The Bat|Forma Del Murciélago|modificada|
|[ancestry-13-hOD9de1ftfYRSEKn.htm](feats/ancestry-13-hOD9de1ftfYRSEKn.htm)|Airy Step|Airy Paso|modificada|
|[ancestry-13-HpAUTAzEAK7mGsJ8.htm](feats/ancestry-13-HpAUTAzEAK7mGsJ8.htm)|Scrutinizing Gaze|Mirada escrutadora|modificada|
|[ancestry-13-hSHteYr1g1gKHcwM.htm](feats/ancestry-13-hSHteYr1g1gKHcwM.htm)|Fey Skin|Piel Feérica|modificada|
|[ancestry-13-HWLD3zGFBIjCDATo.htm](feats/ancestry-13-HWLD3zGFBIjCDATo.htm)|Planar Sidestep|Paso lateral planar|modificada|
|[ancestry-13-hXfyIWMtvB2kAEvA.htm](feats/ancestry-13-hXfyIWMtvB2kAEvA.htm)|Aquatic Camouflage|Camuflaje Acuático|modificada|
|[ancestry-13-I3Md8XYhuMVSJqKI.htm](feats/ancestry-13-I3Md8XYhuMVSJqKI.htm)|Continuous Assault|Asalto Continuo|modificada|
|[ancestry-13-i3wSkeU7CSyHEi4Y.htm](feats/ancestry-13-i3wSkeU7CSyHEi4Y.htm)|Genie Weapon Expertise|Genie experiencia con las armas|modificada|
|[ancestry-13-Ih85PWZSVTwU0xkI.htm](feats/ancestry-13-Ih85PWZSVTwU0xkI.htm)|Lifeblood's Call|Llamada de Lifeblood|modificada|
|[ancestry-13-iTsLr3zEaGZ45zez.htm](feats/ancestry-13-iTsLr3zEaGZ45zez.htm)|Incredible Luck (Halfling)|Suerte sensacional (Halfling)|modificada|
|[ancestry-13-j0fgquODHTyyekyO.htm](feats/ancestry-13-j0fgquODHTyyekyO.htm)|Kobold Weapon Expertise|Experiencia con las armas kobold|modificada|
|[ancestry-13-J2CPfHKPvu6RGfY6.htm](feats/ancestry-13-J2CPfHKPvu6RGfY6.htm)|Fully Flighted|Vuelo Completo|modificada|
|[ancestry-13-j49fEU2TWJUaxD30.htm](feats/ancestry-13-j49fEU2TWJUaxD30.htm)|Gnome Weapon Expertise|Experiencia con las armas de los gnomos|modificada|
|[ancestry-13-j8tukQK5FT1Vfx2G.htm](feats/ancestry-13-j8tukQK5FT1Vfx2G.htm)|Crafter's Instinct|Instinto de artesano|modificada|
|[ancestry-13-JAl4t2KINE8nI8KY.htm](feats/ancestry-13-JAl4t2KINE8nI8KY.htm)|Ceremony of Growth|Ceremonia del crecimiento|modificada|
|[ancestry-13-JlkkYedPyWLShuka.htm](feats/ancestry-13-JlkkYedPyWLShuka.htm)|Kitsune Spell Expertise|Hechizo Experto Kitsune|modificada|
|[ancestry-13-JmVuumcmqSrAmxdm.htm](feats/ancestry-13-JmVuumcmqSrAmxdm.htm)|Ghoran Weapon Expertise|Experiencia con las armas Ghoran|modificada|
|[ancestry-13-JQxFvMHu0ffo56RT.htm](feats/ancestry-13-JQxFvMHu0ffo56RT.htm)|Shinstabber|Shinstabber|modificada|
|[ancestry-13-k2L9p4cc8RrHufut.htm](feats/ancestry-13-k2L9p4cc8RrHufut.htm)|Look But Don't Touch|Mira pero no toques|modificada|
|[ancestry-13-kBnsLuh3eBLjwGpN.htm](feats/ancestry-13-kBnsLuh3eBLjwGpN.htm)|Hatchling Flight|Vuelo de Cría|modificada|
|[ancestry-13-KboQgBhYXI2WdCt8.htm](feats/ancestry-13-KboQgBhYXI2WdCt8.htm)|Mask Of Fear|Máscara De Miedo|modificada|
|[ancestry-13-KQ5xOAqZ1KPN3FgA.htm](feats/ancestry-13-KQ5xOAqZ1KPN3FgA.htm)|Stronger Debilitating Venom|Veneno Debilitación más fuerte|modificada|
|[ancestry-13-KRfZcToCc5nvcQRa.htm](feats/ancestry-13-KRfZcToCc5nvcQRa.htm)|Harbinger's Caw|Graznido del heraldo|modificada|
|[ancestry-13-l11OWMFUixCcRYGm.htm](feats/ancestry-13-l11OWMFUixCcRYGm.htm)|Aquatic Adaptation|Adaptación acuática|modificada|
|[ancestry-13-lIzsj8XlcL0tqQcm.htm](feats/ancestry-13-lIzsj8XlcL0tqQcm.htm)|Cannibalize Magic|Canibalizar la magia|modificada|
|[ancestry-13-lrTRELs8uDpVpsk0.htm](feats/ancestry-13-lrTRELs8uDpVpsk0.htm)|Summon Earth Elemental|Convocar elemental de tierra|modificada|
|[ancestry-13-LxAEF9VPwUVttA0F.htm](feats/ancestry-13-LxAEF9VPwUVttA0F.htm)|Delver|Cavador|modificada|
|[ancestry-13-M8pDuRo6tO0Kok9z.htm](feats/ancestry-13-M8pDuRo6tO0Kok9z.htm)|Secret Eyes|Ojos secretos|modificada|
|[ancestry-13-MII0ybm76DkDxsId.htm](feats/ancestry-13-MII0ybm76DkDxsId.htm)|Cobble Dancer|Cobble Dancer|modificada|
|[ancestry-13-n82gTpAENgr6XjCM.htm](feats/ancestry-13-n82gTpAENgr6XjCM.htm)|Translucent Skin|Piel Translúcida|modificada|
|[ancestry-13-NaY9HXeQ0yhoIorO.htm](feats/ancestry-13-NaY9HXeQ0yhoIorO.htm)|Skittering Sneak|Movimiento furtivo Zafarse|modificada|
|[ancestry-13-ne7nVluvvVXMvuB1.htm](feats/ancestry-13-ne7nVluvvVXMvuB1.htm)|Wandering Heart|Corazón Errante|modificada|
|[ancestry-13-NN9sKez7Z9R8q1i9.htm](feats/ancestry-13-NN9sKez7Z9R8q1i9.htm)|Vanara Battle Clarity|Vanara Battle Clarity|modificada|
|[ancestry-13-O65q744snXtC5Uc4.htm](feats/ancestry-13-O65q744snXtC5Uc4.htm)|Consistent Surge|Oleaje Constante|modificada|
|[ancestry-13-oaqkhZ6c0Dbk78wi.htm](feats/ancestry-13-oaqkhZ6c0Dbk78wi.htm)|Envenomed Edge|Filo envenenado|modificada|
|[ancestry-13-ODy9tq7NPlD5fxzZ.htm](feats/ancestry-13-ODy9tq7NPlD5fxzZ.htm)|Pit of Snakes|Pozo de Serpientes|modificada|
|[ancestry-13-okJjCvQ6hcCk8FOC.htm](feats/ancestry-13-okJjCvQ6hcCk8FOC.htm)|Malleable Form|Forma maleable|modificada|
|[ancestry-13-OwJRuy4EW8vW09AI.htm](feats/ancestry-13-OwJRuy4EW8vW09AI.htm)|Metal-veined Strikes|Golpes con venas de metal.|modificada|
|[ancestry-13-PfHvTR0L9ShjhWiP.htm](feats/ancestry-13-PfHvTR0L9ShjhWiP.htm)|Ceaseless Shadows|Sombras incesantes|modificada|
|[ancestry-13-pivX2SxPQjKDyvHU.htm](feats/ancestry-13-pivX2SxPQjKDyvHU.htm)|Iruxi Spirit Strike|Golpe del Espíritu Iruxi|modificada|
|[ancestry-13-plhQDES7yb6xDAXL.htm](feats/ancestry-13-plhQDES7yb6xDAXL.htm)|Shory Aerialist|Equilibrista shory|modificada|
|[ancestry-13-PvODcTsmFtXffaZn.htm](feats/ancestry-13-PvODcTsmFtXffaZn.htm)|Idol Threat|Amenaza de ídolo|modificada|
|[ancestry-13-QHPtlFvTpgx1GE7S.htm](feats/ancestry-13-QHPtlFvTpgx1GE7S.htm)|Formation Master|Maestro de Formación|modificada|
|[ancestry-13-QjHZN1uAxl2hSbej.htm](feats/ancestry-13-QjHZN1uAxl2hSbej.htm)|Vicious Snares|Vicious Snares|modificada|
|[ancestry-13-rHBmZi95n0pcQqxs.htm](feats/ancestry-13-rHBmZi95n0pcQqxs.htm)|Skeleton Commander|Comandante Esqueleto|modificada|
|[ancestry-13-s0OqtQOeYOGkBMYG.htm](feats/ancestry-13-s0OqtQOeYOGkBMYG.htm)|Steadfast Ally|Aliado Firme|modificada|
|[ancestry-13-S1Z5dFAkMKkFSofk.htm](feats/ancestry-13-S1Z5dFAkMKkFSofk.htm)|War Conditioning|Condicionamiento de guerra|modificada|
|[ancestry-13-SzkpazSZ0pi1WZsH.htm](feats/ancestry-13-SzkpazSZ0pi1WZsH.htm)|Squirm Free|Squirm Free|modificada|
|[ancestry-13-T3cwVrRT0VMZIwpT.htm](feats/ancestry-13-T3cwVrRT0VMZIwpT.htm)|One With Earth|One With Earth|modificada|
|[ancestry-13-TGDy2R6Dq8hwZir4.htm](feats/ancestry-13-TGDy2R6Dq8hwZir4.htm)|Redirect Attention|Redirigir la Atención|modificada|
|[ancestry-13-tpkih32Ch2wCA9R5.htm](feats/ancestry-13-tpkih32Ch2wCA9R5.htm)|Dwarven Weapon Expertise|Experiencia con las armas de los enanos|modificada|
|[ancestry-13-TtSLIMiNw1hqMui5.htm](feats/ancestry-13-TtSLIMiNw1hqMui5.htm)|Skeletal Transformation|Transformación Esquelética|modificada|
|[ancestry-13-TYe3dsNlks91YAks.htm](feats/ancestry-13-TYe3dsNlks91YAks.htm)|Explosive Expert|Experto en Explosión|modificada|
|[ancestry-13-u0DA0gkrZxXb0Hle.htm](feats/ancestry-13-u0DA0gkrZxXb0Hle.htm)|Very, Very Sneaky|Muy, muy escurridizo|modificada|
|[ancestry-13-uclbKFsrqCW6tQmB.htm](feats/ancestry-13-uclbKFsrqCW6tQmB.htm)|Core Rejuvenation|Rejuvenecimiento del núcleo|modificada|
|[ancestry-13-uDFgL6uCwJsaTHi3.htm](feats/ancestry-13-uDFgL6uCwJsaTHi3.htm)|Call Of The Green Man|Llamada del hombre verde|modificada|
|[ancestry-13-uQpAPkJ4ygzFZcIt.htm](feats/ancestry-13-uQpAPkJ4ygzFZcIt.htm)|Vanth's Weapon Expertise|Experiencia con las armas de Vanth.|modificada|
|[ancestry-13-vWUPSvEBHr5AAPUU.htm](feats/ancestry-13-vWUPSvEBHr5AAPUU.htm)|Violent Vines|Vides Violentas|modificada|
|[ancestry-13-VXAIElMlMnVvz3x5.htm](feats/ancestry-13-VXAIElMlMnVvz3x5.htm)|Webslinger|Webslinger|modificada|
|[ancestry-13-wHxXVknciaD4X8Ch.htm](feats/ancestry-13-wHxXVknciaD4X8Ch.htm)|Unrivaled Builder|Constructor sin rival|modificada|
|[ancestry-13-wruf4sMh3P7o8k1P.htm](feats/ancestry-13-wruf4sMh3P7o8k1P.htm)|Elven Weapon Expertise|Experiencia con las armas de los elfos|modificada|
|[ancestry-13-WWCZef1PBliiot9Q.htm](feats/ancestry-13-WWCZef1PBliiot9Q.htm)|Enlarged Chassis|Chasis agrandado|modificada|
|[ancestry-13-X1SPiYJqWzqwzPTs.htm](feats/ancestry-13-X1SPiYJqWzqwzPTs.htm)|Primal Rampage|Primal Rampage|modificada|
|[ancestry-13-X5NRdQ1W0Y5t6MFi.htm](feats/ancestry-13-X5NRdQ1W0Y5t6MFi.htm)|Dire Form|Dire Form|modificada|
|[ancestry-13-XJfUj2o4HdL5waZL.htm](feats/ancestry-13-XJfUj2o4HdL5waZL.htm)|Can't Fall Here|Can't Fall Here|modificada|
|[ancestry-13-XOudHBESJ192FBwF.htm](feats/ancestry-13-XOudHBESJ192FBwF.htm)|Rehydration|Rehidratación|modificada|
|[ancestry-13-XPVqOtHLN6jduHYs.htm](feats/ancestry-13-XPVqOtHLN6jduHYs.htm)|Hobgoblin Weapon Expertise|Experiencia con las armas Hobgoblin.|modificada|
|[ancestry-13-xTLy2AyVW90TrQk6.htm](feats/ancestry-13-xTLy2AyVW90TrQk6.htm)|Improved Elemental Bulwark|Baluarte elemental mejorado|modificada|
|[ancestry-13-XuL3g2ExgadFtWbb.htm](feats/ancestry-13-XuL3g2ExgadFtWbb.htm)|Bone Investiture|Investidura Ósea|modificada|
|[ancestry-13-XzTklOArNQle7rQP.htm](feats/ancestry-13-XzTklOArNQle7rQP.htm)|Invisible Trickster|Arlequín Invisible|modificada|
|[ancestry-13-Y5ompgSd1JGSHt0C.htm](feats/ancestry-13-Y5ompgSd1JGSHt0C.htm)|Augment Senses|Aumentar Sentidos|modificada|
|[ancestry-13-YDfcwp0NVbmpgxej.htm](feats/ancestry-13-YDfcwp0NVbmpgxej.htm)|Vanara Weapoin Expertise|Vanara Weapoin Expertise|modificada|
|[ancestry-13-YryDlMt3zARg6j7T.htm](feats/ancestry-13-YryDlMt3zARg6j7T.htm)|Arise, Ye Worthy!|¡Levantaos, dignos!|modificada|
|[ancestry-13-ytgmam8COVq8B7Do.htm](feats/ancestry-13-ytgmam8COVq8B7Do.htm)|Ancestor's Rage|Furia del Ancestro|modificada|
|[ancestry-13-Z15QFC45psi0txWg.htm](feats/ancestry-13-Z15QFC45psi0txWg.htm)|Alter Resistance|Alterar Resistencia|modificada|
|[ancestry-13-ZlPCQ3Qh1cgS9r87.htm](feats/ancestry-13-ZlPCQ3Qh1cgS9r87.htm)|Mist Strider|Mist Strider|modificada|
|[ancestry-17-4TndDyCjnF7pc1GC.htm](feats/ancestry-17-4TndDyCjnF7pc1GC.htm)|Ghoran's Wrath|Ira de Ghoran|modificada|
|[ancestry-17-5FHLom2tpC0X3nbf.htm](feats/ancestry-17-5FHLom2tpC0X3nbf.htm)|Greater Augmentation|Aumento mayor|modificada|
|[ancestry-17-5qVCl5MwPcx0sS7T.htm](feats/ancestry-17-5qVCl5MwPcx0sS7T.htm)|Elude Trouble|Eludir Problemas|modificada|
|[ancestry-17-5Unc9AhGAAw1klFN.htm](feats/ancestry-17-5Unc9AhGAAw1klFN.htm)|Animal Swiftness|Celeridad Animal|modificada|
|[ancestry-17-6NXlg11EeCqjOmSg.htm](feats/ancestry-17-6NXlg11EeCqjOmSg.htm)|Twist Healing|Giro Curación|modificada|
|[ancestry-17-715iDDXFPguSO9Or.htm](feats/ancestry-17-715iDDXFPguSO9Or.htm)|Bend Space|Doblar espacio|modificada|
|[ancestry-17-7iB1yacjF9fG6Rvn.htm](feats/ancestry-17-7iB1yacjF9fG6Rvn.htm)|Yamaraj's Grandeur|Grandeza de Yamaraj|modificada|
|[ancestry-17-8l3qDZrfhxUGijjB.htm](feats/ancestry-17-8l3qDZrfhxUGijjB.htm)|Rallying Cry|Rallying Cry|modificada|
|[ancestry-17-8ZWMJYNgyH5zh1yH.htm](feats/ancestry-17-8ZWMJYNgyH5zh1yH.htm)|Eternal Wings (Sylph)|Eternal Wings (Sylph)|modificada|
|[ancestry-17-9cq4j15EbVyrHKFX.htm](feats/ancestry-17-9cq4j15EbVyrHKFX.htm)|Forge-Blessed Shot|Tiro de forja bendecido|modificada|
|[ancestry-17-ABx8keV4c43gEmeN.htm](feats/ancestry-17-ABx8keV4c43gEmeN.htm)|Pierce The Light|Pierce The Light|modificada|
|[ancestry-17-bPPcPUZMlJ7m5lYq.htm](feats/ancestry-17-bPPcPUZMlJ7m5lYq.htm)|Tidal Shield|Escudo de marea|modificada|
|[ancestry-17-BS6UoGLyJ12xVD9P.htm](feats/ancestry-17-BS6UoGLyJ12xVD9P.htm)|Ratfolk Growth|Crecimiento Ratfolk|modificada|
|[ancestry-17-cayKGa2yK7Gwvk6m.htm](feats/ancestry-17-cayKGa2yK7Gwvk6m.htm)|Olethros's Decree|Olethros's Decree|modificada|
|[ancestry-17-CuyuwFht0rVbGMca.htm](feats/ancestry-17-CuyuwFht0rVbGMca.htm)|Relentless Wings|Alas Implacables|modificada|
|[ancestry-17-dftzUTOrj9dQyN3q.htm](feats/ancestry-17-dftzUTOrj9dQyN3q.htm)|Restitch|Restitch|modificada|
|[ancestry-17-Dxac0mgye7EFFVou.htm](feats/ancestry-17-Dxac0mgye7EFFVou.htm)|Radiate Glory|Irradiar Gloria|modificada|
|[ancestry-17-DYFOlJlMoDuHjrZx.htm](feats/ancestry-17-DYFOlJlMoDuHjrZx.htm)|Channel The Godmind|Channel The Godmind|modificada|
|[ancestry-17-E4MS2wnRQJfyldrT.htm](feats/ancestry-17-E4MS2wnRQJfyldrT.htm)|Bone Swarm|Bone Swarm|modificada|
|[ancestry-17-EEnL4zyCDo4HD6Rn.htm](feats/ancestry-17-EEnL4zyCDo4HD6Rn.htm)|Fey Transcendence|Trascendencia Feérica|modificada|
|[ancestry-17-ERCd44zQMYI4rgqM.htm](feats/ancestry-17-ERCd44zQMYI4rgqM.htm)|Animal Swiftness (Land)|Celeridad animal (Tierra)|modificada|
|[ancestry-17-FBhBnyGX0Uje0tbJ.htm](feats/ancestry-17-FBhBnyGX0Uje0tbJ.htm)|Storm Form|Forma de tormenta|modificada|
|[ancestry-17-ffjoPXtAoIR2Drwe.htm](feats/ancestry-17-ffjoPXtAoIR2Drwe.htm)|Regrowth|Regrowth|modificada|
|[ancestry-17-FWfKbAxTct6q3AMp.htm](feats/ancestry-17-FWfKbAxTct6q3AMp.htm)|Vicious Venom|Vicious Venom|modificada|
|[ancestry-17-gXAAJCnjfCDK7YV2.htm](feats/ancestry-17-gXAAJCnjfCDK7YV2.htm)|Cleansing Light|Luz Limpiadora|modificada|
|[ancestry-17-GYvaR6ZD8ZKdQWrF.htm](feats/ancestry-17-GYvaR6ZD8ZKdQWrF.htm)|Stone Form|Forma de Piedra|modificada|
|[ancestry-17-h9b5CId7S7gV7j2t.htm](feats/ancestry-17-h9b5CId7S7gV7j2t.htm)|Axial Recall|Axial Recall|modificada|
|[ancestry-17-hkU92nqUYBQLQSMt.htm](feats/ancestry-17-hkU92nqUYBQLQSMt.htm)|Blazing Aura|Blazing Aura|modificada|
|[ancestry-17-hNr4OrMdCMhQLbtB.htm](feats/ancestry-17-hNr4OrMdCMhQLbtB.htm)|Wyrmling Flight|Vuelo Wyrmling|modificada|
|[ancestry-17-hZUFP1NPwdGMzs1y.htm](feats/ancestry-17-hZUFP1NPwdGMzs1y.htm)|Ceremony of Sun's Gift|Ceremonia del don del sol|modificada|
|[ancestry-17-I7S8Snq8FlrBHmbf.htm](feats/ancestry-17-I7S8Snq8FlrBHmbf.htm)|Ten Lives|Ten Lives|modificada|
|[ancestry-17-iJrjzLnLJkvQgrbS.htm](feats/ancestry-17-iJrjzLnLJkvQgrbS.htm)|Hurricane Swing|Huracán Swing|modificada|
|[ancestry-17-IOlvpmeIwlk0IACr.htm](feats/ancestry-17-IOlvpmeIwlk0IACr.htm)|Bone Rider|Bone Rider|modificada|
|[ancestry-17-IyFz7kVF8wzPdrfJ.htm](feats/ancestry-17-IyFz7kVF8wzPdrfJ.htm)|Unfettering Prankster|Unfettering Prankster|modificada|
|[ancestry-17-IzJ76OXe1gl2hfbd.htm](feats/ancestry-17-IzJ76OXe1gl2hfbd.htm)|Stormy Heart|Corazón Tormentoso|modificada|
|[ancestry-17-JgGsWppVh4q4HtSy.htm](feats/ancestry-17-JgGsWppVh4q4HtSy.htm)|Dominion Aura|Aura de Dominio|modificada|
|[ancestry-17-JrBqOxHZX20b8gTT.htm](feats/ancestry-17-JrBqOxHZX20b8gTT.htm)|Soaring Poppet|Soaring Poppet|modificada|
|[ancestry-17-kr537ZL3f8tCSgDK.htm](feats/ancestry-17-kr537ZL3f8tCSgDK.htm)|True Gaze|Mirada verdadera|modificada|
|[ancestry-17-kt5UJEfTzO3LiInN.htm](feats/ancestry-17-kt5UJEfTzO3LiInN.htm)|Crone's Cruelty|Crueldad de Crone|modificada|
|[ancestry-17-LIEbgqgSXkGIli8u.htm](feats/ancestry-17-LIEbgqgSXkGIli8u.htm)|Animal Swiftness (Fly)|Celeridad Animal (Volar)|modificada|
|[ancestry-17-LoZY48txEgHZxewj.htm](feats/ancestry-17-LoZY48txEgHZxewj.htm)|Azaersi's Roads|Caminos de Azaersi|modificada|
|[ancestry-17-LTIARSoQP8WYE33A.htm](feats/ancestry-17-LTIARSoQP8WYE33A.htm)|Necromantic Heir|Heredero Nigromántico|modificada|
|[ancestry-17-MzdoJWpqyMmMDI1F.htm](feats/ancestry-17-MzdoJWpqyMmMDI1F.htm)|Fountain of Secrets|Fuente de secretos|modificada|
|[ancestry-17-Nd7C7X9LYyp0TaET.htm](feats/ancestry-17-Nd7C7X9LYyp0TaET.htm)|Animal Swiftness (Swim)|Celeridad Animal (Nadar)|modificada|
|[ancestry-17-oItG5dqlPEi8wNnR.htm](feats/ancestry-17-oItG5dqlPEi8wNnR.htm)|Trickster Tengu|Trickster Tengu|modificada|
|[ancestry-17-OpSHTPkoZDrqR0Mq.htm](feats/ancestry-17-OpSHTPkoZDrqR0Mq.htm)|Animal Shape|Forma animal|modificada|
|[ancestry-17-Qc9MH7wT182qasSV.htm](feats/ancestry-17-Qc9MH7wT182qasSV.htm)|Cantorian Restoration|Restablecimiento Cantoriano|modificada|
|[ancestry-17-rq0lnwZ51AtJ0aux.htm](feats/ancestry-17-rq0lnwZ51AtJ0aux.htm)|Breath of Calamity|Aliento de Calamidad|modificada|
|[ancestry-17-SFnKHPrd65RORTgN.htm](feats/ancestry-17-SFnKHPrd65RORTgN.htm)|Prismatic Scales|Prismatic Scales|modificada|
|[ancestry-17-U9nJy5suyYDE0b20.htm](feats/ancestry-17-U9nJy5suyYDE0b20.htm)|Flourish And Ruin|Floritura Y Ruina|modificada|
|[ancestry-17-UOL44Qm1rOVD5TFg.htm](feats/ancestry-17-UOL44Qm1rOVD5TFg.htm)|Underwater Volcano|Volcán Submarino|modificada|
|[ancestry-17-VbHuOvF1MM9ls6Tg.htm](feats/ancestry-17-VbHuOvF1MM9ls6Tg.htm)|Favor Of Heaven|Favor del Cielo|modificada|
|[ancestry-17-VPKBjEy8frqDMxyO.htm](feats/ancestry-17-VPKBjEy8frqDMxyO.htm)|Greater Enhance Venom|Mayor Mejora Veneno|modificada|
|[ancestry-17-wFtJlamwRc6rSQmj.htm](feats/ancestry-17-wFtJlamwRc6rSQmj.htm)|Scion Transformation|Scion Transformation|modificada|
|[ancestry-17-WZC2XXQo6Pu2GQeo.htm](feats/ancestry-17-WZC2XXQo6Pu2GQeo.htm)|Soaring Form|Soaring Form|modificada|
|[ancestry-17-WZLw9UrIyCz6Eiqo.htm](feats/ancestry-17-WZLw9UrIyCz6Eiqo.htm)|Animal Swiftness (Climb)|Celeridad animal (Trepar)|modificada|
|[ancestry-17-xXaKiJbKhClF5eJx.htm](feats/ancestry-17-xXaKiJbKhClF5eJx.htm)|Final Form|Forma final|modificada|
|[ancestry-17-zeyrLJr6b7hPdx4w.htm](feats/ancestry-17-zeyrLJr6b7hPdx4w.htm)|Core Cannon|Core Cannon|modificada|
|[ancestry-17-Zl6vDXJEM7Lbp8JQ.htm](feats/ancestry-17-Zl6vDXJEM7Lbp8JQ.htm)|Rampaging Form|Forma arrasadora|modificada|
|[ancestry-17-ZOlwVqWa4hfv44xX.htm](feats/ancestry-17-ZOlwVqWa4hfv44xX.htm)|Hero's Wings|Hero's Wings|modificada|
|[ancestry-17-zSJ0Ke8HWzG1krlq.htm](feats/ancestry-17-zSJ0Ke8HWzG1krlq.htm)|Reflect Foe|Reflejar Enemigo|modificada|
|[bonus-00-1xpqOpguTUAeFsdO.htm](feats/bonus-00-1xpqOpguTUAeFsdO.htm)|Seek Injustice|Buscar Injusticia|modificada|
|[bonus-00-AGiIjgLOFuahmwiT.htm](feats/bonus-00-AGiIjgLOFuahmwiT.htm)|Shackles of Law|Grilletes de la Ley|modificada|
|[bonus-00-AxjjAoU1IZaNUGGS.htm](feats/bonus-00-AxjjAoU1IZaNUGGS.htm)|Fear No Law, Fear No One|No temas a la ley, no temas a nadie|modificada|
|[bonus-00-cMkpLgadlLCzDOv0.htm](feats/bonus-00-cMkpLgadlLCzDOv0.htm)|Blessings of the Five|Bendiciones de los Cinco|modificada|
|[bonus-00-FlsAYAGEiZg1gg7D.htm](feats/bonus-00-FlsAYAGEiZg1gg7D.htm)|Spiritual Disruption|Disruptora Espiritual|modificada|
|[bonus-00-jrDCPvkruz8y740Z.htm](feats/bonus-00-jrDCPvkruz8y740Z.htm)|Locate Lawbreakers|Localizar a los infractores de la ley|modificada|
|[bonus-00-Kz4E8heU12IGcYoi.htm](feats/bonus-00-Kz4E8heU12IGcYoi.htm)|Righteous Resistance|Resistencia Justa|modificada|
|[bonus-00-PpJURSJFEHzhutdp.htm](feats/bonus-00-PpJURSJFEHzhutdp.htm)|Trailblazing Stride|Zancada pionera|modificada|
|[bonus-00-PPSH5vdf90KC95jJ.htm](feats/bonus-00-PPSH5vdf90KC95jJ.htm)|Reveal Beasts|Revelar Bestias|modificada|
|[bonus-00-RkvAuikI0kIZlQTU.htm](feats/bonus-00-RkvAuikI0kIZlQTU.htm)|Disillusionment|Desilusión|modificada|
|[bonus-00-tPhhaCbaQqwenkzx.htm](feats/bonus-00-tPhhaCbaQqwenkzx.htm)|Silence Heresy|Herejía del Silencio|modificada|
|[bonus-00-UHejQS4uCNGRI45t.htm](feats/bonus-00-UHejQS4uCNGRI45t.htm)|Dedication to the Five|Dedicación a los Cinco|modificada|
|[bonus-00-w7dOgJvqAqyqSeSQ.htm](feats/bonus-00-w7dOgJvqAqyqSeSQ.htm)|Devil Allies|Aliados del Diablo|modificada|
|[bonus-00-XOAtP64xiSRIofsY.htm](feats/bonus-00-XOAtP64xiSRIofsY.htm)|Sturdy Bindings|Fijaciones Robustas|modificada|
|[bonus-10-q8c0LINVNcJrdK91.htm](feats/bonus-10-q8c0LINVNcJrdK91.htm)|Propulsive Leap|Salto sin carrerilla propulsión|modificada|
|[class-01-0PCDkVnRxVqxsp9j.htm](feats/class-01-0PCDkVnRxVqxsp9j.htm)|Energy Heart|Energy Heart|modificada|
|[class-01-0yPbPVEESwB6Bdfw.htm](feats/class-01-0yPbPVEESwB6Bdfw.htm)|Magus's Analysis|Análisis de Magus|modificada|
|[class-01-142QRyK2aPIrJu48.htm](feats/class-01-142QRyK2aPIrJu48.htm)|Holy Castigation|Castigo sagrado|modificada|
|[class-01-1JnERVwnPtX620f2.htm](feats/class-01-1JnERVwnPtX620f2.htm)|Animal Companion (Ranger)|Compañero animal (Ranger)|modificada|
|[class-01-1W0a6YCyoYv8dm4e.htm](feats/class-01-1W0a6YCyoYv8dm4e.htm)|Harming Hands|Manos danas|modificada|
|[class-01-2bCbfQ0KCSSYpg2q.htm](feats/class-01-2bCbfQ0KCSSYpg2q.htm)|Unfetter Eidolon|Unfetter Eidolon|modificada|
|[class-01-2c9awqDem5OLK47S.htm](feats/class-01-2c9awqDem5OLK47S.htm)|Weight of Guilt|El peso de la culpa|modificada|
|[class-01-2Lcwqwq8CF40TYHd.htm](feats/class-01-2Lcwqwq8CF40TYHd.htm)|Dual Studies|Estudios duales|modificada|
|[class-01-2xk4jdwcCfmasYfT.htm](feats/class-01-2xk4jdwcCfmasYfT.htm)|Power Attack|Ataque poderoso|modificada|
|[class-01-3a07jkWezSuORSpS.htm](feats/class-01-3a07jkWezSuORSpS.htm)|Ki Strike|Golpe de ki|modificada|
|[class-01-4HgKmqTrXYZJ4fyj.htm](feats/class-01-4HgKmqTrXYZJ4fyj.htm)|Animal Companion (Druid)|Animal Companion (Druida)|modificada|
|[class-01-4KkzwOu2OKYLKSXQ.htm](feats/class-01-4KkzwOu2OKYLKSXQ.htm)|Variable Core|Variable Core|modificada|
|[class-01-5pGMffGKkBTZKvjw.htm](feats/class-01-5pGMffGKkBTZKvjw.htm)|Raging Thrower|Lanzador furioso|modificada|
|[class-01-6GN1zh3RcnZhrzxP.htm](feats/class-01-6GN1zh3RcnZhrzxP.htm)|Everstand Stance|Posición de Everstand.|modificada|
|[class-01-6LFBPpPPJjDq07fg.htm](feats/class-01-6LFBPpPPJjDq07fg.htm)|Hit the Dirt!|¡Golpea la tierra!|modificada|
|[class-01-6thEFvU7aMPmLrly.htm](feats/class-01-6thEFvU7aMPmLrly.htm)|False Faith|Falsa Fe|modificada|
|[class-01-7y1BCJLrdk9mKXlc.htm](feats/class-01-7y1BCJLrdk9mKXlc.htm)|Leshy Familiar|Familiar leshy (s), leshys (pl.)|modificada|
|[class-01-8sy3sHwOHS4ImwvJ.htm](feats/class-01-8sy3sHwOHS4ImwvJ.htm)|Dragon Stance|Posición del dragón|modificada|
|[class-01-8tS5NzytLmgbq5hF.htm](feats/class-01-8tS5NzytLmgbq5hF.htm)|Exacting Strike|Golpe riguroso|modificada|
|[class-01-8zHsIubGREIrGfAA.htm](feats/class-01-8zHsIubGREIrGfAA.htm)|Prototype Companion|Compañero Prototipo|modificada|
|[class-01-9oKtrUzXixj58hOg.htm](feats/class-01-9oKtrUzXixj58hOg.htm)|Psychic Rapport|Psychic Rapport|modificada|
|[class-01-9XALeVNcmlIxf3tJ.htm](feats/class-01-9XALeVNcmlIxf3tJ.htm)|Twin Feint|Doble finta gemela|modificada|
|[class-01-AD2eQu6SjLhUGD6Z.htm](feats/class-01-AD2eQu6SjLhUGD6Z.htm)|Eschew Materials|Abstención de materiales|modificada|
|[class-01-AN9jY1JVcU20Qdw6.htm](feats/class-01-AN9jY1JVcU20Qdw6.htm)|Wolf Stance|Posición del lobo|modificada|
|[class-01-b09B0XLNAiFP3gFT.htm](feats/class-01-b09B0XLNAiFP3gFT.htm)|Counterspell (Sorcerer)|Contraconjuro (Hechicero)|modificada|
|[class-01-bcxIg7wi8ZAhvhOD.htm](feats/class-01-bcxIg7wi8ZAhvhOD.htm)|Familiar|Familiar|modificada|
|[class-01-bf7NCeKqDClaqhTR.htm](feats/class-01-bf7NCeKqDClaqhTR.htm)|Crane Stance|Posición de la grulla|modificada|
|[class-01-Bj0GvPgyPiC2kDH1.htm](feats/class-01-Bj0GvPgyPiC2kDH1.htm)|Glider Form|Forma Planeadora|modificada|
|[class-01-BJHsCiBLdjgJo6zM.htm](feats/class-01-BJHsCiBLdjgJo6zM.htm)|Far Lobber|Lanzar a distancia|modificada|
|[class-01-c3b7DhnDBC7YEgRG.htm](feats/class-01-c3b7DhnDBC7YEgRG.htm)|Hunted Shot|Tiro cazado|modificada|
|[class-01-CpjN7v1QN8TQFcvI.htm](feats/class-01-CpjN7v1QN8TQFcvI.htm)|Crossbow Ace|As de la ballesta|modificada|
|[class-01-deoHKUzpzT7iwWhL.htm](feats/class-01-deoHKUzpzT7iwWhL.htm)|Counterspell (Spontaneous)|Contraconjuro (Espontáneo)|modificada|
|[class-01-DfLkIIg2reyYW3a8.htm](feats/class-01-DfLkIIg2reyYW3a8.htm)|Deadly Simplicity|Simplicidad mortal|modificada|
|[class-01-dFQj6gLrbQi7APiA.htm](feats/class-01-dFQj6gLrbQi7APiA.htm)|Agile Shield Grip|Agarre Ágil con Escudo|modificada|
|[class-01-dGFQvkDRmyvvf4IQ.htm](feats/class-01-dGFQvkDRmyvvf4IQ.htm)|Splinter Faith|Splinter Faith|modificada|
|[class-01-dWbISC0di0r4oPCi.htm](feats/class-01-dWbISC0di0r4oPCi.htm)|Sword and Pistol|Espada y Pistola|modificada|
|[class-01-emRfSVvU8ZAH9UdK.htm](feats/class-01-emRfSVvU8ZAH9UdK.htm)|Dangerous Sorcery|Hechicería peligrosa|modificada|
|[class-01-epfQ2PwNDLwyY31t.htm](feats/class-01-epfQ2PwNDLwyY31t.htm)|Unimpeded Step|Paso sin trabas|modificada|
|[class-01-f2Pl5dWEL9ZvEyI1.htm](feats/class-01-f2Pl5dWEL9ZvEyI1.htm)|Animal Companion|Animal Companion|modificada|
|[class-01-fUR72e3t7p2IcqqG.htm](feats/class-01-fUR72e3t7p2IcqqG.htm)|Divine Disharmony|Desarmonía divina|modificada|
|[class-01-FXKIALDXAzEBfj5A.htm](feats/class-01-FXKIALDXAzEBfj5A.htm)|Deity's Domain|Dominio de tu dios|modificada|
|[class-01-gFQFgREm3HaFx1mf.htm](feats/class-01-gFQFgREm3HaFx1mf.htm)|Built-In Tools|Herramientas incorporadas|modificada|
|[class-01-GuEdTz1VMEptQnOd.htm](feats/class-01-GuEdTz1VMEptQnOd.htm)|Stoked Flame Stance|Posiciígera de Llama Avivada|modificada|
|[class-01-Gw0wGXikhAhiGoud.htm](feats/class-01-Gw0wGXikhAhiGoud.htm)|Twin Takedown|Derribo gemelo|modificada|
|[class-01-hT4INKGtly4QY8KN.htm](feats/class-01-hT4INKGtly4QY8KN.htm)|Domain Initiate|Iniciado de un dominio|modificada|
|[class-01-iP01b6eyUm4m6KQp.htm](feats/class-01-iP01b6eyUm4m6KQp.htm)|Haunt Ingenuity|Haunt Ingenio|modificada|
|[class-01-iTdbkP07UFMOo1rI.htm](feats/class-01-iTdbkP07UFMOo1rI.htm)|Ranged Reprisal|Represalia a distancia|modificada|
|[class-01-IzkL60LlKzbKIhY1.htm](feats/class-01-IzkL60LlKzbKIhY1.htm)|Cover Fire|Fuego de Cobertura|modificada|
|[class-01-jBp91q4uzwd4FeSX.htm](feats/class-01-jBp91q4uzwd4FeSX.htm)|Versatile Performance|Interpretación versátil|modificada|
|[class-01-JoxEspM0kbCop7og.htm](feats/class-01-JoxEspM0kbCop7og.htm)|Raise a Tome|Levantar un Tomo|modificada|
|[class-01-kFpVgcqREAfDmjXp.htm](feats/class-01-kFpVgcqREAfDmjXp.htm)|Advanced Weaponry|Armamento Avanzado|modificada|
|[class-01-KlqKpeq5OmTRxVHb.htm](feats/class-01-KlqKpeq5OmTRxVHb.htm)|Diverse Lore|Diverse Lore|modificada|
|[class-01-knZUN4sYExIyRC4F.htm](feats/class-01-knZUN4sYExIyRC4F.htm)|Reflective Ripple Stance|Posición de Ondulación Reflectante|modificada|
|[class-01-kS4wxSKrZxcOvSNK.htm](feats/class-01-kS4wxSKrZxcOvSNK.htm)|Shore Step|Shore Paso|modificada|
|[class-01-L5nSP8CkHWSLp2vV.htm](feats/class-01-L5nSP8CkHWSLp2vV.htm)|Arcane Fists|Puños Arcanos|modificada|
|[class-01-lFVqejlf52cdYrZy.htm](feats/class-01-lFVqejlf52cdYrZy.htm)|Munitions Crafter|Munitions Crafter|modificada|
|[class-01-loC0wIyIrsG43Zrd.htm](feats/class-01-loC0wIyIrsG43Zrd.htm)|Alchemical Familiar|Familiar alquímico|modificada|
|[class-01-mWCiu9Hl1WxajSLa.htm](feats/class-01-mWCiu9Hl1WxajSLa.htm)|Snagging Strike|Golpe imprevisto|modificada|
|[class-01-onde0SxLoxLBTnvm.htm](feats/class-01-onde0SxLoxLBTnvm.htm)|Double Slice|Doble tajo|modificada|
|[class-01-oQVp2UhXVBcELma5.htm](feats/class-01-oQVp2UhXVBcELma5.htm)|Root to Life|Enraizarse a la Vida|modificada|
|[class-01-pD1oDbUDkNtHadGY.htm](feats/class-01-pD1oDbUDkNtHadGY.htm)|Hand of the Apprentice|Mano del aprendiz|modificada|
|[class-01-PGi1EtcZMolnNA1M.htm](feats/class-01-PGi1EtcZMolnNA1M.htm)|Ancestral Mind|Mente ancestral|modificada|
|[class-01-QEOe9AhQzdqIx1ei.htm](feats/class-01-QEOe9AhQzdqIx1ei.htm)|Steadying Stone|Steadying Stone|modificada|
|[class-01-QKC9iiR4Epj1Lyc7.htm](feats/class-01-QKC9iiR4Epj1Lyc7.htm)|Counter Thought|Contrapensamiento|modificada|
|[class-01-qQt3CMrhLkUV1wCv.htm](feats/class-01-qQt3CMrhLkUV1wCv.htm)|Sudden Charge|Carga súbita|modificada|
|[class-01-rbiMK71SvGZGRLJ1.htm](feats/class-01-rbiMK71SvGZGRLJ1.htm)|Rain of Embers Stance|Posición de lluvia de ascuas.|modificada|
|[class-01-rPzEnABFjkbOARiB.htm](feats/class-01-rPzEnABFjkbOARiB.htm)|Expanded Senses|Sentidos Expandidos|modificada|
|[class-01-s6h0xkdKf3gecLk6.htm](feats/class-01-s6h0xkdKf3gecLk6.htm)|Crossbow Crack Shot|Crossbow Crack Shot|modificada|
|[class-01-s97nKj9Uye1KXr7A.htm](feats/class-01-s97nKj9Uye1KXr7A.htm)|Meld into Eidolon|Fundirse en Eidolon|modificada|
|[class-01-sf73K6f8xlfhHS1n.htm](feats/class-01-sf73K6f8xlfhHS1n.htm)|Scroll Thaumaturgy|Pergamino Taumaturgia|modificada|
|[class-01-SheifYobjKqyK3Fv.htm](feats/class-01-SheifYobjKqyK3Fv.htm)|Tamper|Tamper|modificada|
|[class-01-sjChYEuEWPqndCSK.htm](feats/class-01-sjChYEuEWPqndCSK.htm)|Dual-Weapon Reload|Dual-Weapon Reload|modificada|
|[class-01-sVjATEo8eqkAosNp.htm](feats/class-01-sVjATEo8eqkAosNp.htm)|Lingering Composition|Composición persistente|modificada|
|[class-01-tn1PajS2DrHeBlGg.htm](feats/class-01-tn1PajS2DrHeBlGg.htm)|Ammunition Thaumaturgy|Taumaturgia de munición|modificada|
|[class-01-u5cPwhuCd3xTAlWl.htm](feats/class-01-u5cPwhuCd3xTAlWl.htm)|Extend Boost|Extend Boost|modificada|
|[class-01-UpEjRfQkCJCruAfb.htm](feats/class-01-UpEjRfQkCJCruAfb.htm)|Storm Born|Nacido de la tormenta|modificada|
|[class-01-UtRW3hYBTFG8HQLh.htm](feats/class-01-UtRW3hYBTFG8HQLh.htm)|Mental Buffer|Buffer mental|modificada|
|[class-01-UVsMJwHpQHjVZLTK.htm](feats/class-01-UVsMJwHpQHjVZLTK.htm)|Explosive Leap|Salto sin carrerilla Explosión|modificada|
|[class-01-uVXEZblPRuCyPRua.htm](feats/class-01-uVXEZblPRuCyPRua.htm)|Bardic Lore|Saber bárdico|modificada|
|[class-01-VaIHQzOE5ibmbtqU.htm](feats/class-01-VaIHQzOE5ibmbtqU.htm)|Wild Shape|Forma salvaje|modificada|
|[class-01-VCjAlOvjNvFJOsG5.htm](feats/class-01-VCjAlOvjNvFJOsG5.htm)|Tiger Stance|Posición del tigre|modificada|
|[class-01-vRXPsMbyuvoPvbAQ.htm](feats/class-01-vRXPsMbyuvoPvbAQ.htm)|Moment of Clarity|Momento de claridad|modificada|
|[class-01-w8Ycgeq2zfyshtoS.htm](feats/class-01-w8Ycgeq2zfyshtoS.htm)|Reactive Shield|Escudo reactivo|modificada|
|[class-01-WcY7H7mRiK2VDquV.htm](feats/class-01-WcY7H7mRiK2VDquV.htm)|Dueling Parry (Swashbuckler)|Parada de duelo (Swashbuckler)|modificada|
|[class-01-xbg1scOIT7RI9Fij.htm](feats/class-01-xbg1scOIT7RI9Fij.htm)|Quick Bomber|Bombardero rápido|modificada|
|[class-01-XHy9inDi3oGZNd43.htm](feats/class-01-XHy9inDi3oGZNd43.htm)|No! No! I Created You!|¡No! ¡No! ¡Yo te Elaboré!|modificada|
|[class-01-XLoRtMn48f2KKvsu.htm](feats/class-01-XLoRtMn48f2KKvsu.htm)|Blast Lock|Blast Lock|modificada|
|[class-01-XseJI9XhKNtZN8pv.htm](feats/class-01-XseJI9XhKNtZN8pv.htm)|Raging Intimidation|Intimidación furiosa.|modificada|
|[class-01-yA9tsWT9uzL2REnw.htm](feats/class-01-yA9tsWT9uzL2REnw.htm)|Ki Rush|Embestida de ki|modificada|
|[class-01-YdpGPLN0QFTZIhbk.htm](feats/class-01-YdpGPLN0QFTZIhbk.htm)|Acute Vision|Visión aguda|modificada|
|[class-01-Yl2wYv24v5kw95aS.htm](feats/class-01-Yl2wYv24v5kw95aS.htm)|Point-Blank Shot|Disparo a bocajarro|modificada|
|[class-01-yOloZIGkulrrPYG4.htm](feats/class-01-yOloZIGkulrrPYG4.htm)|Fire Lung|Pulmón de Fuego|modificada|
|[class-01-ypir96PNY6LBSIWT.htm](feats/class-01-ypir96PNY6LBSIWT.htm)|Alchemical Savant|Sabio alquímico|modificada|
|[class-01-YTTJqRKH8QZl6al2.htm](feats/class-01-YTTJqRKH8QZl6al2.htm)|Monster Hunter|Cazador de monstruos|modificada|
|[class-01-YW4FdWgzTwRFneFF.htm](feats/class-01-YW4FdWgzTwRFneFF.htm)|Monastic Weaponry|Armamento monástico|modificada|
|[class-01-ZL5UU9quCTvcWzfY.htm](feats/class-01-ZL5UU9quCTvcWzfY.htm)|Mountain Stance|Posición de la monta|modificada|
|[class-01-zxaE7C1NCGqZR8aU.htm](feats/class-01-zxaE7C1NCGqZR8aU.htm)|Haphazard Repair|Reparar al azar|modificada|
|[class-01-zYhcEX4JnrZ08HfV.htm](feats/class-01-zYhcEX4JnrZ08HfV.htm)|Healing Hands|Manos curativas|modificada|
|[class-02-0Bm4WMi9u0EOHbkp.htm](feats/class-02-0Bm4WMi9u0EOHbkp.htm)|Resilient Mind|Mente Resistente|modificada|
|[class-02-0icxuITlxUBfeomX.htm](feats/class-02-0icxuITlxUBfeomX.htm)|Turpin Rowe Lumberjack Dedication|Dedicación de leñador Turpin Rowe|modificada|
|[class-02-0lieFKdwgNVXep7u.htm](feats/class-02-0lieFKdwgNVXep7u.htm)|Poison Resistance|Resistencia al veneno|modificada|
|[class-02-0NRIP8cDv9Opl5Ls.htm](feats/class-02-0NRIP8cDv9Opl5Ls.htm)|Fiendsbane Oath (Paladin)|Juramento del azote de infernales (Paladín)|modificada|
|[class-02-0ogbBzSD7fmiWp9d.htm](feats/class-02-0ogbBzSD7fmiWp9d.htm)|Living Vessel Dedication|Living Vessel Dedication|modificada|
|[class-02-0YjetnLVIUz9zQt5.htm](feats/class-02-0YjetnLVIUz9zQt5.htm)|Champion Dedication|Dedicación de campeón|modificada|
|[class-02-17jjQPpnBkHRkzPE.htm](feats/class-02-17jjQPpnBkHRkzPE.htm)|Turn Away Misfortune|Turn Away Misfortune|modificada|
|[class-02-19OLlTrxvEtDAOHc.htm](feats/class-02-19OLlTrxvEtDAOHc.htm)|Aggressive Block|Bloqueo agresivo|modificada|
|[class-02-1hyC4aiLMBSwM9Z1.htm](feats/class-02-1hyC4aiLMBSwM9Z1.htm)|Trick Driver Dedication|Dedicación al conductor de trucos|modificada|
|[class-02-20jNBiIIxaiOVyi0.htm](feats/class-02-20jNBiIIxaiOVyi0.htm)|Oozemorph Dedication|Dedicación cienomorfo|modificada|
|[class-02-2AeGnjk4EAqFDvK8.htm](feats/class-02-2AeGnjk4EAqFDvK8.htm)|Spirit Sheath|Spirit Sheath|modificada|
|[class-02-2rJP5TqvfazNeNmY.htm](feats/class-02-2rJP5TqvfazNeNmY.htm)|Revivifying Mutagen|Mutágeno revivificador|modificada|
|[class-02-2yq3l8e2Vzz34yRV.htm](feats/class-02-2yq3l8e2Vzz34yRV.htm)|Monster Warden|Guardián contra los monstruos|modificada|
|[class-02-3r4v6W4abJnLg7dv.htm](feats/class-02-3r4v6W4abJnLg7dv.htm)|Alter Ego Dedication|Dedicación Alter Ego|modificada|
|[class-02-3SGS0chf3SKosG5H.htm](feats/class-02-3SGS0chf3SKosG5H.htm)|Shieldmarshal Dedication|Shieldmarshal Dedicación|modificada|
|[class-02-4bvoePh1p3ZGgqhP.htm](feats/class-02-4bvoePh1p3ZGgqhP.htm)|Second Wind|Nuevas energías|modificada|
|[class-02-4f65nrWVzTG8Njj8.htm](feats/class-02-4f65nrWVzTG8Njj8.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[class-02-4HZTLPKPteEFsa7n.htm](feats/class-02-4HZTLPKPteEFsa7n.htm)|Esoteric Polymath|Polifacético esotérico|modificada|
|[class-02-4M5zIlJ8EvjQDtg9.htm](feats/class-02-4M5zIlJ8EvjQDtg9.htm)|Dandy Dedication|Dedicatoria Dandy|modificada|
|[class-02-4MUbwilvb9dI0X59.htm](feats/class-02-4MUbwilvb9dI0X59.htm)|Snarecrafter Dedication|Snarecrafter Dedication|modificada|
|[class-02-4zaU3GlTGMNqLFS8.htm](feats/class-02-4zaU3GlTGMNqLFS8.htm)|Familiar Master Dedication|Dedicación Maestro Familiar|modificada|
|[class-02-52nzskpmUt9AjUf8.htm](feats/class-02-52nzskpmUt9AjUf8.htm)|Ghost Dedication|Dedicación Fantasma|modificada|
|[class-02-54BhwBHyp2jp3e26.htm](feats/class-02-54BhwBHyp2jp3e26.htm)|Wild Empathy|Empatía salvaje|modificada|
|[class-02-57K9il2Jxs8PmsYL.htm](feats/class-02-57K9il2Jxs8PmsYL.htm)|Nantambu Chime-Ringer Dedication|Nantambu Chime-Ringer Dedicación|modificada|
|[class-02-5Cm2TYHzoO2DbQNM.htm](feats/class-02-5Cm2TYHzoO2DbQNM.htm)|Reanimator Dedication|Dedicación de Reanimador|modificada|
|[class-02-5CRt5Dy9eLv5LpRF.htm](feats/class-02-5CRt5Dy9eLv5LpRF.htm)|Herbalist Dedication|Dedicación Herbolario|modificada|
|[class-02-5FOqVC9Q0eEKEf3w.htm](feats/class-02-5FOqVC9Q0eEKEf3w.htm)|Magic Warrior Dedication|Dedicación de Guerrero Mágico|modificada|
|[class-02-5gnBhockV7O32jTR.htm](feats/class-02-5gnBhockV7O32jTR.htm)|Furious Finish|Remate furioso|modificada|
|[class-02-5npovgLMUlvtot2J.htm](feats/class-02-5npovgLMUlvtot2J.htm)|Wrestler Dedication|Dedicación del luchador|modificada|
|[class-02-5UQagWB13Z8xR5Z6.htm](feats/class-02-5UQagWB13Z8xR5Z6.htm)|Sorcerer Dedication|Dedicación de hechicero|modificada|
|[class-02-63L2iSAxkHyq6qEt.htm](feats/class-02-63L2iSAxkHyq6qEt.htm)|Hallowed Necromancer Dedication|Hallowed Necromancer Dedication|modificada|
|[class-02-6b8DiNx4c1zPZ7RP.htm](feats/class-02-6b8DiNx4c1zPZ7RP.htm)|Edgewatch Detective Dedication|Dedicación detective Edgewatch|modificada|
|[class-02-6PP57Aa5HmSr0qIZ.htm](feats/class-02-6PP57Aa5HmSr0qIZ.htm)|Sniping Duo Dedication|Dedicación Dúo Sniping|modificada|
|[class-02-7RFjTTzznsdPcaYB.htm](feats/class-02-7RFjTTzznsdPcaYB.htm)|Emblazon Armament|Estampar armamento|modificada|
|[class-02-80CEAB05TP5ki9iW.htm](feats/class-02-80CEAB05TP5ki9iW.htm)|Fane's Fourberie|Fourberie de Fane|modificada|
|[class-02-84n92oBsRVCxjifM.htm](feats/class-02-84n92oBsRVCxjifM.htm)|Expansive Spellstrike|Hechizo Expansivo|modificada|
|[class-02-85eUthbjE9TnF4fo.htm](feats/class-02-85eUthbjE9TnF4fo.htm)|Dragonslayer Oath (Redeemer)|Juramento del matadragones (redentor)|modificada|
|[class-02-8bMmGBO6gsMfWZk3.htm](feats/class-02-8bMmGBO6gsMfWZk3.htm)|Stunning Fist|Putazo aturdidor|modificada|
|[class-02-8foxmfC6FFT3oYpV.htm](feats/class-02-8foxmfC6FFT3oYpV.htm)|Sentinel Dedication|Dedicación Centinela|modificada|
|[class-02-8qawcaQAZMD7pC6Y.htm](feats/class-02-8qawcaQAZMD7pC6Y.htm)|Alkenstar Agent Dedication|Dedicación Agente Alkenstar|modificada|
|[class-02-9Jbl71C5C6MnOqxV.htm](feats/class-02-9Jbl71C5C6MnOqxV.htm)|Consume Energy|Consumir Energía|modificada|
|[class-02-9onf3uGvZRnNLPR6.htm](feats/class-02-9onf3uGvZRnNLPR6.htm)|Order Explorer|Explorador de la orden|modificada|
|[class-02-A2EBy2L4acrehiAA.htm](feats/class-02-A2EBy2L4acrehiAA.htm)|Pistol Twirl|Pistol Twirl|modificada|
|[class-02-A7ofsoPva0UtjqrX.htm](feats/class-02-A7ofsoPva0UtjqrX.htm)|Cantrip Expansion (Prepared Caster)|Expansión de truco (Lanzador preparado).|modificada|
|[class-02-a898miJnjgD93ZsX.htm](feats/class-02-a898miJnjgD93ZsX.htm)|Multifarious Muse|Musa variopinta|modificada|
|[class-02-AimSmPyiMhJZVZRq.htm](feats/class-02-AimSmPyiMhJZVZRq.htm)|Mauler Dedication|Dedicación Vapulear|modificada|
|[class-02-auv1lss6LxM0q3gz.htm](feats/class-02-auv1lss6LxM0q3gz.htm)|Shake it Off|Recuperación|modificada|
|[class-02-bCWieNDC1CD35tin.htm](feats/class-02-bCWieNDC1CD35tin.htm)|Rogue Dedication|Dedicación de pícaro|modificada|
|[class-02-BmAk6o14CutgnIOG.htm](feats/class-02-BmAk6o14CutgnIOG.htm)|Risky Reload|Risky Reload|modificada|
|[class-02-bPMYOiiqhrb098s6.htm](feats/class-02-bPMYOiiqhrb098s6.htm)|Call Implement|Llamada Implementar|modificada|
|[class-02-BU4NBIBkVZxdWLLH.htm](feats/class-02-BU4NBIBkVZxdWLLH.htm)|Elemental Fist|Pu elemental|modificada|
|[class-02-BVxcuJ5TWfN1px4L.htm](feats/class-02-BVxcuJ5TWfN1px4L.htm)|Pactbound Dedication|Pactbound Dedication|modificada|
|[class-02-BwDIwjHasZwcd61Z.htm](feats/class-02-BwDIwjHasZwcd61Z.htm)|Spellshot Dedication|Dedicación Spellshot|modificada|
|[class-02-bWYnjNa2RknMoibg.htm](feats/class-02-bWYnjNa2RknMoibg.htm)|Lastwall Sentry Dedication|Lastwall Centinela Dedicación|modificada|
|[class-02-C3ukUk4Js10o5IBA.htm](feats/class-02-C3ukUk4Js10o5IBA.htm)|Ghostly Grasp (Deviant)|Ghostly Grasp (Desviado)|modificada|
|[class-02-c4ao7b2T92fiM8jR.htm](feats/class-02-c4ao7b2T92fiM8jR.htm)|Living Monolith Dedication|Dedicación Monolito Viviente|modificada|
|[class-02-CaeCSWFWytWv8Fgu.htm](feats/class-02-CaeCSWFWytWv8Fgu.htm)|Scrounger Dedication|Dedicación Scrounger|modificada|
|[class-02-CbqaiI0TKsyDywDr.htm](feats/class-02-CbqaiI0TKsyDywDr.htm)|Spell Trickster Dedication|Dedicación de Hechizo Arlequín|modificada|
|[class-02-CcQISNjNdD0Fsj9n.htm](feats/class-02-CcQISNjNdD0Fsj9n.htm)|Shining Oath (Paladin)|Juramento brillante (paladín)|modificada|
|[class-02-CJMkxlxHiHZQYDCz.htm](feats/class-02-CJMkxlxHiHZQYDCz.htm)|Alchemist Dedication|Dedicación de alquimista|modificada|
|[class-02-corjPSTUKB02gkqN.htm](feats/class-02-corjPSTUKB02gkqN.htm)|Defensive Armaments|Armamento Defensivo|modificada|
|[class-02-CowQy8gXP9POjuxq.htm](feats/class-02-CowQy8gXP9POjuxq.htm)|Game Hunter Dedication|Cazador Dedicación|modificada|
|[class-02-CrS0iDwHmjKqvKti.htm](feats/class-02-CrS0iDwHmjKqvKti.htm)|Flexible Spellcaster Dedication|Dedicación Hechicero Flexible|modificada|
|[class-02-cT18roxM5P5MWDRT.htm](feats/class-02-cT18roxM5P5MWDRT.htm)|Reliable Squire|Escudero fiable|modificada|
|[class-02-cy9jqlHz75GTjg7l.htm](feats/class-02-cy9jqlHz75GTjg7l.htm)|Bone Spikes|Bone Spikes|modificada|
|[class-02-dF1HR6TqyMz03o6F.htm](feats/class-02-dF1HR6TqyMz03o6F.htm)|Combat Grab|Agarrar en combate|modificada|
|[class-02-DFusBl7CyNkuDTRa.htm](feats/class-02-DFusBl7CyNkuDTRa.htm)|Collapse Armor|Armadura de colapso|modificada|
|[class-02-dgmjiToPHC3Yf5I3.htm](feats/class-02-dgmjiToPHC3Yf5I3.htm)|Acute Scent|Olfato agudo|modificada|
|[class-02-dIH771mt4PcVTyAs.htm](feats/class-02-dIH771mt4PcVTyAs.htm)|Bard Dedication|Dedicación de bardo|modificada|
|[class-02-dkuY22d3yLUBcqhq.htm](feats/class-02-dkuY22d3yLUBcqhq.htm)|Cathartic Mage Dedication|Cathartic Mage Dedication|modificada|
|[class-02-Dm0YMEvSY0qg0jA0.htm](feats/class-02-Dm0YMEvSY0qg0jA0.htm)|Gladiator Dedication|Dedicación Gladiador|modificada|
|[class-02-DxcjEcxTbjlOA2C0.htm](feats/class-02-DxcjEcxTbjlOA2C0.htm)|Reinforce Eidolon|Reforzar Eidolon|modificada|
|[class-02-DYUlMxh7TDgEh9xB.htm](feats/class-02-DYUlMxh7TDgEh9xB.htm)|Brutal Beating|Paliza brutal|modificada|
|[class-02-E8SDesgMD6Zbye2j.htm](feats/class-02-E8SDesgMD6Zbye2j.htm)|Geomancer Dedication|Geomancer Dedicación|modificada|
|[class-02-e9NcDk6ds5YebGvb.htm](feats/class-02-e9NcDk6ds5YebGvb.htm)|Relentless Stalker|Acechador implacable|modificada|
|[class-02-eBdajOzs8kiJDic2.htm](feats/class-02-eBdajOzs8kiJDic2.htm)|Blessed One Dedication|Dedicación Bendecida|modificada|
|[class-02-eCzIiTjI4mQFYe9D.htm](feats/class-02-eCzIiTjI4mQFYe9D.htm)|Steed Form|Steed Form|modificada|
|[class-02-eHjqNXgylSuvA887.htm](feats/class-02-eHjqNXgylSuvA887.htm)|Student of Perfection Dedication|Estudiante de Perfección Dedicación|modificada|
|[class-02-EHJRMCBRL4YeBN6l.htm](feats/class-02-EHJRMCBRL4YeBN6l.htm)|Ghoul Dedication|Ghoul Dedication|modificada|
|[class-02-eJPu2Sj7XYCM0h0R.htm](feats/class-02-eJPu2Sj7XYCM0h0R.htm)|Warp Space|Espacio Warp|modificada|
|[class-02-eUXgY8W3fShlW7pd.htm](feats/class-02-eUXgY8W3fShlW7pd.htm)|Inspire Competence|Inspirar gran aptitud|modificada|
|[class-02-fdSbB0EQVxFgLoEd.htm](feats/class-02-fdSbB0EQVxFgLoEd.htm)|Sterling Dynamo Dedication|Dedicación Sterling Dynamo|modificada|
|[class-02-fL5lO5Odp7iJPkan.htm](feats/class-02-fL5lO5Odp7iJPkan.htm)|Vehicle Mechanic Dedication|Dedicación Mecánico de Vehículos|modificada|
|[class-02-FrlhErsjR6fEn6kX.htm](feats/class-02-FrlhErsjR6fEn6kX.htm)|Minor Magic|Magia menor|modificada|
|[class-02-Fs9ZMHZNCBo7B5Zc.htm](feats/class-02-Fs9ZMHZNCBo7B5Zc.htm)|Vengeful Oath|Juramento vengativo|modificada|
|[class-02-GFtNQvpzuqtsdOTG.htm](feats/class-02-GFtNQvpzuqtsdOTG.htm)|Hunter's Aim|Puntería del cazador|modificada|
|[class-02-gQAQRHxpFKEkNQFs.htm](feats/class-02-gQAQRHxpFKEkNQFs.htm)|Thaumaturge Dedication|Thaumaturge Dedication|modificada|
|[class-02-gt9pvIicabSOe6pB.htm](feats/class-02-gt9pvIicabSOe6pB.htm)|Brutish Shove|Empujón brutal|modificada|
|[class-02-hGjmbTNBUiJqYvsE.htm](feats/class-02-hGjmbTNBUiJqYvsE.htm)|Crushing Grab|Agarrón aplastante|modificada|
|[class-02-hLYoIjH8gPEVXyWG.htm](feats/class-02-hLYoIjH8gPEVXyWG.htm)|Distracting Feint|Finta de distracción|modificada|
|[class-02-I2DGiMcof703Cnjc.htm](feats/class-02-I2DGiMcof703Cnjc.htm)|Cantrip Casting|Lanzamiento de truco|modificada|
|[class-02-I5hzE0XT2BsjatS3.htm](feats/class-02-I5hzE0XT2BsjatS3.htm)|Vampire Dedication|Dedicación Vampiro|modificada|
|[class-02-iBmqKjsq4iTtoqvl.htm](feats/class-02-iBmqKjsq4iTtoqvl.htm)|Assassin Dedication|Dedicación de Asesino|modificada|
|[class-02-IUmN2WC55LxPNSBB.htm](feats/class-02-IUmN2WC55LxPNSBB.htm)|Mummy Dedication|Dedicación de momia|modificada|
|[class-02-j1hhTLOq7o80XCyV.htm](feats/class-02-j1hhTLOq7o80XCyV.htm)|Bullet Dancer Dedication|Dedicación Bailarín de balas|modificada|
|[class-02-j2XKVhDuhdcI15kD.htm](feats/class-02-j2XKVhDuhdcI15kD.htm)|First Frost|Primera Escarcha|modificada|
|[class-02-J3R0vx1lszU3CLp5.htm](feats/class-02-J3R0vx1lszU3CLp5.htm)|Fiendsbane Oath (Redeemer)|Juramento del azote de infernales (redentor)|modificada|
|[class-02-JIpjpZ8VneQomkZw.htm](feats/class-02-JIpjpZ8VneQomkZw.htm)|Inventor Dedication|Dedicación Inventor|modificada|
|[class-02-jm8kchZRznjxRy0C.htm](feats/class-02-jm8kchZRznjxRy0C.htm)|Clockwork Reanimator Dedication|Dedicación Reanimador Mecánico|modificada|
|[class-02-jPWF8Xe0UqVvbeyv.htm](feats/class-02-jPWF8Xe0UqVvbeyv.htm)|Eerie Flicker|Eerie Flicker|modificada|
|[class-02-JuEUNrvsxOYdXeYL.htm](feats/class-02-JuEUNrvsxOYdXeYL.htm)|Viking Dedication|Dedicación Vikinga|modificada|
|[class-02-juikoiIA0Jy8PboY.htm](feats/class-02-juikoiIA0Jy8PboY.htm)|Mind Smith Dedication|Dedicación de Mind Smith|modificada|
|[class-02-K3QkcNWY8qpNEJrk.htm](feats/class-02-K3QkcNWY8qpNEJrk.htm)|Dragonslayer Oath (Liberator)|Juramento del matadragones (liberador)|modificada|
|[class-02-l1eCHNahsBb7rUkT.htm](feats/class-02-l1eCHNahsBb7rUkT.htm)|Pirate Dedication|Dedicación Pirata|modificada|
|[class-02-lDfOzhKJoTCkLPtn.htm](feats/class-02-lDfOzhKJoTCkLPtn.htm)|Shining Oath (Liberator)|Juramento brillante (liberador)|modificada|
|[class-02-LJzk4Xxxs9IlbWz6.htm](feats/class-02-LJzk4Xxxs9IlbWz6.htm)|Curse Maelstrom Dedication|Maldición Dedicación Maelstrom|modificada|
|[class-02-lkcM4V3VDAtjlR9P.htm](feats/class-02-lkcM4V3VDAtjlR9P.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[class-02-LoeoiYOpxSaEkWKv.htm](feats/class-02-LoeoiYOpxSaEkWKv.htm)|Staff Acrobat Dedication|Dedicación acróbata del personal|modificada|
|[class-02-lotpVdFhxNsNP0Ru.htm](feats/class-02-lotpVdFhxNsNP0Ru.htm)|Unexpected Sharpshooter Dedication|Dedicación inesperada de Sharpshooter|modificada|
|[class-02-lslR2UfP3ze7TFYu.htm](feats/class-02-lslR2UfP3ze7TFYu.htm)|Eldritch Researcher Dedication|Dedicación de investigador de lo sobrenatural|modificada|
|[class-02-lT8XlX1Ig900BblS.htm](feats/class-02-lT8XlX1Ig900BblS.htm)|No Escape|Sin huida posible|modificada|
|[class-02-luKAFJAvdbAgEwV7.htm](feats/class-02-luKAFJAvdbAgEwV7.htm)|Runescarred Dedication|Dedicación Runescarred|modificada|
|[class-02-lyD6eXl4wc7Pq61q.htm](feats/class-02-lyD6eXl4wc7Pq61q.htm)|Loremaster Dedication|Dedicación Loremaster|modificada|
|[class-02-LZsTt6zPZfNyjIZl.htm](feats/class-02-LZsTt6zPZfNyjIZl.htm)|Dragonslayer Oath (Paladin)|Juramento del matadragones (Paladín)|modificada|
|[class-02-M0D2CQgASNfdZBrl.htm](feats/class-02-M0D2CQgASNfdZBrl.htm)|Demolitionist Dedication|Dedicación Demoledora|modificada|
|[class-02-m3ANSHYfBrFyFUvo.htm](feats/class-02-m3ANSHYfBrFyFUvo.htm)|Animal Trainer Dedication|Dedicación Entrenador de Animales|modificada|
|[class-02-M6cWLywN0aWnX5Gl.htm](feats/class-02-M6cWLywN0aWnX5Gl.htm)|Undead Master Dedication|Dedicación de maestro muerto viviente|modificada|
|[class-02-MJg24e9fJd7OASvF.htm](feats/class-02-MJg24e9fJd7OASvF.htm)|Medic Dedication|Medic Dedication|modificada|
|[class-02-mkp6lhBbTASEmKwY.htm](feats/class-02-mkp6lhBbTASEmKwY.htm)|Ghost Hunter Dedication|Cazador de fantasmas Dedicación|modificada|
|[class-02-MNArjo5Z5LUYITQm.htm](feats/class-02-MNArjo5Z5LUYITQm.htm)|Celebrity Dedication|Dedicación de famosos|modificada|
|[class-02-MndcBMz6I7e6SRqx.htm](feats/class-02-MndcBMz6I7e6SRqx.htm)|Weapon Improviser Dedication|Dedicación Mejorador de Armas|modificada|
|[class-02-mNsNl6w3l5rXx8dL.htm](feats/class-02-mNsNl6w3l5rXx8dL.htm)|Juggler Dedication|Dedicación Malabarista|modificada|
|[class-02-mrxAX7h5ya9Z7QV9.htm](feats/class-02-mrxAX7h5ya9Z7QV9.htm)|Oatia Skysage Dedication|Dedicación Oatia Skysage|modificada|
|[class-02-MVbNnjqQOK9d8Ki3.htm](feats/class-02-MVbNnjqQOK9d8Ki3.htm)|Firework Technician Dedication|Dedicación de técnico de fuegos artificiales|modificada|
|[class-02-mvNa9KfQooHYEXoA.htm](feats/class-02-mvNa9KfQooHYEXoA.htm)|Fighter Dedication|Dedicación de guerrero|modificada|
|[class-02-MYzRfNExDYp25rro.htm](feats/class-02-MYzRfNExDYp25rro.htm)|Marshal Dedication|Dedicación de Mariscal|modificada|
|[class-02-mz2x4HFrWT8usbEL.htm](feats/class-02-mz2x4HFrWT8usbEL.htm)|Runelord Dedication|Runelord Dedication|modificada|
|[class-02-mzpLIuRQ81DCYdKU.htm](feats/class-02-mzpLIuRQ81DCYdKU.htm)|Force Fang|Colmillo de Fuerza|modificada|
|[class-02-n7nQQR940OvFbw7T.htm](feats/class-02-n7nQQR940OvFbw7T.htm)|Dueling Parry (Fighter)|Parada de duelo (Luchador)|modificada|
|[class-02-Na1qfHd6AFAXoN1A.htm](feats/class-02-Na1qfHd6AFAXoN1A.htm)|Reverse Engineer|Ingeniero inverso|modificada|
|[class-02-ncrNQcwm4gOQRAA3.htm](feats/class-02-ncrNQcwm4gOQRAA3.htm)|Shadowcaster Dedication|Shadowcaster Dedicación|modificada|
|[class-02-NwMiszlcqNZWtezq.htm](feats/class-02-NwMiszlcqNZWtezq.htm)|Drow Shootist Dedication|Dedicación Tirador Drow|modificada|
|[class-02-obGGzuPcgO5Xiz6P.htm](feats/class-02-obGGzuPcgO5Xiz6P.htm)|Searing Restoration|Restablecimiento abrasador|modificada|
|[class-02-ODnXQHvFoK7tIVpu.htm](feats/class-02-ODnXQHvFoK7tIVpu.htm)|Smoke Bomb|Bomba de humo|modificada|
|[class-02-oo34CloLefFRi72w.htm](feats/class-02-oo34CloLefFRi72w.htm)|Dragon Disciple Dedication|Dedicación discípulo dragón|modificada|
|[class-02-oQ6EZRFXbHWCx08C.htm](feats/class-02-oQ6EZRFXbHWCx08C.htm)|Wizard Dedication|Dedicación de mago|modificada|
|[class-02-oVSlTmdmho8ZQo2k.htm](feats/class-02-oVSlTmdmho8ZQo2k.htm)|Ranged Combatant|Combatiente a distancia|modificada|
|[class-02-OW3W2vMARiojda7e.htm](feats/class-02-OW3W2vMARiojda7e.htm)|Psychic Dedication|Dedicación Psíquica|modificada|
|[class-02-OWhTAowdMvBZnCrT.htm](feats/class-02-OWhTAowdMvBZnCrT.htm)|Dancing Leaf|Hoja danzante|modificada|
|[class-02-P6cTyDMTXcC8HBDr.htm](feats/class-02-P6cTyDMTXcC8HBDr.htm)|Esoteric Warden|Esotérico Warden|modificada|
|[class-02-pEFcWRYiWLSjxvkW.htm](feats/class-02-pEFcWRYiWLSjxvkW.htm)|Mental Balm|Mental Balm|modificada|
|[class-02-PFMsj4LtdAIuGn4R.htm](feats/class-02-PFMsj4LtdAIuGn4R.htm)|Pistol Phenom Dedication|Dedicación Pistol Phenom|modificada|
|[class-02-PGyzFBwuTgypU8cD.htm](feats/class-02-PGyzFBwuTgypU8cD.htm)|Turn Undead|Expulsar muertos vivientes|modificada|
|[class-02-pI97a5xSg4LbBY1g.htm](feats/class-02-pI97a5xSg4LbBY1g.htm)|Aldori Duelist Dedication|Aldori Duelista Dedicación|modificada|
|[class-02-pPA2YF6Dal0PClWA.htm](feats/class-02-pPA2YF6Dal0PClWA.htm)|Undead Slayer Dedication|Dedicación de cazador de muertos vivientes|modificada|
|[class-02-pZMBq7gn66SEEA0n.htm](feats/class-02-pZMBq7gn66SEEA0n.htm)|Horizon Walker Dedication|Horizon Walker Dedication|modificada|
|[class-02-qFt6zyWVX1njJf1l.htm](feats/class-02-qFt6zyWVX1njJf1l.htm)|Quick Draw|Desenvainado rápido|modificada|
|[class-02-qhs0mWbaLKsdckZX.htm](feats/class-02-qhs0mWbaLKsdckZX.htm)|Hellknight Armiger Dedication|Dedicación de Caballero Infernal Armiger|modificada|
|[class-02-QkP007ESTWtw7UQG.htm](feats/class-02-QkP007ESTWtw7UQG.htm)|Cantrip Expansion (Spontaneous Caster)|Expansión de truco (Lanzador espontáneo)|modificada|
|[class-02-qMa2fIP2nqrFzHrq.htm](feats/class-02-qMa2fIP2nqrFzHrq.htm)|Scout Dedication|Dedicación Explorador|modificada|
|[class-02-QN7y2CQeiJ2iPioM.htm](feats/class-02-QN7y2CQeiJ2iPioM.htm)|Titan Swing|Titan Swing|modificada|
|[class-02-QnB5ArlO4wlRXQ2E.htm](feats/class-02-QnB5ArlO4wlRXQ2E.htm)|Pathfinder Agent Dedication|Dedicación de Agente Pathfinder|modificada|
|[class-02-R00qiDE5pBctgtyU.htm](feats/class-02-R00qiDE5pBctgtyU.htm)|Ranger Dedication|Dedicación de explorador|modificada|
|[class-02-r1um0cn7MRIMLqcq.htm](feats/class-02-r1um0cn7MRIMLqcq.htm)|Ghost Eater Dedication|Dedicación del Devorador de Fantasmas|modificada|
|[class-02-r72qcTupGzyRDiGe.htm](feats/class-02-r72qcTupGzyRDiGe.htm)|Duelist Dedication|Dedicación de Duelista|modificada|
|[class-02-RAymYRO2SLNNzKVk.htm](feats/class-02-RAymYRO2SLNNzKVk.htm)|Mobility|Movilidad|modificada|
|[class-02-renUpSO6MJXPSXow.htm](feats/class-02-renUpSO6MJXPSXow.htm)|Monk Dedication|Dedicación de monje|modificada|
|[class-02-rJ1XxUEACaQA9Lyo.htm](feats/class-02-rJ1XxUEACaQA9Lyo.htm)|Bright Lion Dedication|Dedicación Leones brillantes|modificada|
|[class-02-rTkr1EqpAN6YtnAh.htm](feats/class-02-rTkr1EqpAN6YtnAh.htm)|Lunge|Acometer|modificada|
|[class-02-ryBj7phZqASQSEjV.htm](feats/class-02-ryBj7phZqASQSEjV.htm)|Psi Burst|Ráfaga Psi|modificada|
|[class-02-s6wXcQYmHbew14gC.htm](feats/class-02-s6wXcQYmHbew14gC.htm)|Bastion Dedication|Dedicación de Bastión|modificada|
|[class-02-sG9fPQk4w9y6MmnY.htm](feats/class-02-sG9fPQk4w9y6MmnY.htm)|Folklorist Dedication|Dedicación Folclorista|modificada|
|[class-02-sgo7J9BVofBqwlsF.htm](feats/class-02-sgo7J9BVofBqwlsF.htm)|Favored Terrain|Terreno predilecto|modificada|
|[class-02-smCDaPlpRDA47xjK.htm](feats/class-02-smCDaPlpRDA47xjK.htm)|Cleric Dedication|Dedicación de clérigo|modificada|
|[class-02-SNhhx0hPWlERpQRr.htm](feats/class-02-SNhhx0hPWlERpQRr.htm)|Corpse Tender Dedication|Dedicación de prestamista de cadáveres|modificada|
|[class-02-SoocjFrWNOpchTVb.htm](feats/class-02-SoocjFrWNOpchTVb.htm)|Soulforger Dedication|Dedicación Soulforger|modificada|
|[class-02-Stydu9VtrhQZFZxt.htm](feats/class-02-Stydu9VtrhQZFZxt.htm)|Fake Out|Fake Out|modificada|
|[class-02-SwzPqEsLzZpNufvm.htm](feats/class-02-SwzPqEsLzZpNufvm.htm)|Summoner Dedication|Dedicación de Invocador|modificada|
|[class-02-t03Tc5I4B8RHsyqs.htm](feats/class-02-t03Tc5I4B8RHsyqs.htm)|Overwatch Dedication|Dedicación Overwatch|modificada|
|[class-02-t2Uvf6W8Z116DYvo.htm](feats/class-02-t2Uvf6W8Z116DYvo.htm)|Cantrip Expansion (Magus)|Expansión de truco (Magus).|modificada|
|[class-02-tdtCwmYoBMKzfhEp.htm](feats/class-02-tdtCwmYoBMKzfhEp.htm)|Shining Oath (Redeemer)|Juramento brillante (redentor)|modificada|
|[class-02-TiNlehXIDEnIl95M.htm](feats/class-02-TiNlehXIDEnIl95M.htm)|Sap Life|Minar la vida|modificada|
|[class-02-tJduF6N83l5khRow.htm](feats/class-02-tJduF6N83l5khRow.htm)|Divine Grace|Gracia divina|modificada|
|[class-02-tKaesXO5nlZ2sspx.htm](feats/class-02-tKaesXO5nlZ2sspx.htm)|Bounty Hunter Dedication|Cazarrecompensas Dedicación|modificada|
|[class-02-TtAvM02UvfNaXeXd.htm](feats/class-02-TtAvM02UvfNaXeXd.htm)|Blasting Beams|Blasting Beams|modificada|
|[class-02-tx9pkrpmtqe4FnvS.htm](feats/class-02-tx9pkrpmtqe4FnvS.htm)|Elementalist Dedication|Dedicación Elementalista|modificada|
|[class-02-TxKWgxor49xntDlU.htm](feats/class-02-TxKWgxor49xntDlU.htm)|Magus Dedication|Dedicación Magus|modificada|
|[class-02-txWKzKuJus7UBebX.htm](feats/class-02-txWKzKuJus7UBebX.htm)|Artillerist Dedication|Dedicación Artillero|modificada|
|[class-02-U6lS758rtYGR6aw9.htm](feats/class-02-U6lS758rtYGR6aw9.htm)|Amphibious Form|Forma anfibia|modificada|
|[class-02-u6SDVWbzHnBBPNMo.htm](feats/class-02-u6SDVWbzHnBBPNMo.htm)|Linguist Dedication|Dedicación Lingüista|modificada|
|[class-02-uD8J9wAE3KB2w0Cf.htm](feats/class-02-uD8J9wAE3KB2w0Cf.htm)|Pactbinder Dedication|Dedicación Pactbinder|modificada|
|[class-02-UiQbjeqBUFjUtgUR.htm](feats/class-02-UiQbjeqBUFjUtgUR.htm)|Assisting Shot|Disparo de ayuda|modificada|
|[class-02-uqIiSDbv80TbRwTQ.htm](feats/class-02-uqIiSDbv80TbRwTQ.htm)|Vigilante Dedication|Dedicación Vigilante|modificada|
|[class-02-USt0jwK8XI5loG4E.htm](feats/class-02-USt0jwK8XI5loG4E.htm)|Gunslinger Dedication|Dedicación de Pistolero|modificada|
|[class-02-UuPZ7drPBnSmI8Eo.htm](feats/class-02-UuPZ7drPBnSmI8Eo.htm)|Red Mantis Assassin Dedication|Dedicación Asesino Mantis Roja|modificada|
|[class-02-uv235v6hXCITAFej.htm](feats/class-02-uv235v6hXCITAFej.htm)|Fiendsbane Oath (Liberator)|Juramento del azote de infernales (liberador)|modificada|
|[class-02-uxHWqFbYD0ZvkeF8.htm](feats/class-02-uxHWqFbYD0ZvkeF8.htm)|Scroll Trickster Dedication|Pergamino Trickster Dedicación|modificada|
|[class-02-UyyrFtPWOo0qvXOv.htm](feats/class-02-UyyrFtPWOo0qvXOv.htm)|Zephyr Guard Dedication|Dedicación de la guardia del Céfiro|modificada|
|[class-02-Veaf8vm2M9w8bcBI.htm](feats/class-02-Veaf8vm2M9w8bcBI.htm)|Alacritous Action|Acción Alacritous|modificada|
|[class-02-VeVHWTrzE3aDm3rx.htm](feats/class-02-VeVHWTrzE3aDm3rx.htm)|Brawling Focus|Foco pendenciero|modificada|
|[class-02-vhw3n86TEs6laopA.htm](feats/class-02-vhw3n86TEs6laopA.htm)|Druid Dedication|Dedicación de druida|modificada|
|[class-02-VToVEOxiyy53AcEp.htm](feats/class-02-VToVEOxiyy53AcEp.htm)|Loremaster's Etude|Estudio del maestro del saber|modificada|
|[class-02-wdmUH6hdat7jpEWt.htm](feats/class-02-wdmUH6hdat7jpEWt.htm)|Beastmaster Dedication|Dedicación Beastmaster|modificada|
|[class-02-WfbmFsRxbVyzMmCz.htm](feats/class-02-WfbmFsRxbVyzMmCz.htm)|Magical Understudy|Magical Understudy|modificada|
|[class-02-wKFQreilUASJkKzV.htm](feats/class-02-wKFQreilUASJkKzV.htm)|Collapse Construct|Collapse Construct|modificada|
|[class-02-WVU0c8rgcpGSRqSi.htm](feats/class-02-WVU0c8rgcpGSRqSi.htm)|Barbarian Dedication|Dedicación de bárbaro|modificada|
|[class-02-X9AkydrMdFwg7qIn.htm](feats/class-02-X9AkydrMdFwg7qIn.htm)|Spell Parry|Parada de Hechizo|modificada|
|[class-02-XQEoKoFtq8n3wgA3.htm](feats/class-02-XQEoKoFtq8n3wgA3.htm)|Warning Shot|Tiro de advertencia|modificada|
|[class-02-y7DDs03GtDnmhxFp.htm](feats/class-02-y7DDs03GtDnmhxFp.htm)|Poisoner Dedication|Dedicación Envenenador|modificada|
|[class-02-yExxOkHN1PN37hUa.htm](feats/class-02-yExxOkHN1PN37hUa.htm)|Communal Healing|Curación en común|modificada|
|[class-02-ygCLN0brunmBYtJR.htm](feats/class-02-ygCLN0brunmBYtJR.htm)|Talisman Esoterica|Talismán Esotérico|modificada|
|[class-02-ygdbkfPPgSoWxaBa.htm](feats/class-02-ygdbkfPPgSoWxaBa.htm)|Chronoskimmer Dedication|Dedicación Chronoskimmer|modificada|
|[class-02-YhnCjiHNlS3nCeoC.htm](feats/class-02-YhnCjiHNlS3nCeoC.htm)|Dual-Weapon Warrior Dedication|Dedicación del combatiente de doble arma|modificada|
|[class-02-yWawboNWFoJMVl0g.htm](feats/class-02-yWawboNWFoJMVl0g.htm)|Soul Warden Dedication|Soul Warden Dedication|modificada|
|[class-02-YwVdaszwpDJd6kf9.htm](feats/class-02-YwVdaszwpDJd6kf9.htm)|Devoted Guardian|Guardián Devoto|modificada|
|[class-02-yYLVGhedXD7lFQMn.htm](feats/class-02-yYLVGhedXD7lFQMn.htm)|United Assault|Asalto Unido|modificada|
|[class-02-yZ7EcM9CLddZz8Hl.htm](feats/class-02-yZ7EcM9CLddZz8Hl.htm)|Versatile Font|Fuente versátil|modificada|
|[class-02-ZhToff996PnTESwb.htm](feats/class-02-ZhToff996PnTESwb.htm)|Magaambyan Attendant Dedication|Dedicación Asistente Magaambyan|modificada|
|[class-02-Ziky4XVV7syXVbUg.htm](feats/class-02-Ziky4XVV7syXVbUg.htm)|Powder Punch Stance|Posición de Puñetazo de Polvo|modificada|
|[class-02-zn7arorE3VJLNYsb.htm](feats/class-02-zn7arorE3VJLNYsb.htm)|Martial Artist Dedication|Dedicación de artista marcial|modificada|
|[class-02-zNxbsYgQgPVxdTLV.htm](feats/class-02-zNxbsYgQgPVxdTLV.htm)|Lion Blade Dedication|Dedicación de la Hoja del León|modificada|
|[class-02-ZR5Buon23cDQ1ryB.htm](feats/class-02-ZR5Buon23cDQ1ryB.htm)|Zombie Dedication|Dedicación Zombie|modificada|
|[class-02-zV4vzcl7eoVewz5p.htm](feats/class-02-zV4vzcl7eoVewz5p.htm)|Ursine Avenger Form|Ursine Avenger Form|modificada|
|[class-02-ZvPgibovxwiN8Wse.htm](feats/class-02-ZvPgibovxwiN8Wse.htm)|Call of the Wild|Llamada salvaje|modificada|
|[class-02-Zyb3RMGyhsKfTjEG.htm](feats/class-02-Zyb3RMGyhsKfTjEG.htm)|Wellspring Mage Dedication|Wellspring Mage Dedication|modificada|
|[class-04-00OnDt8UEMwfoYWH.htm](feats/class-04-00OnDt8UEMwfoYWH.htm)|Ghost Wrangler|Ghost Wrangler|modificada|
|[class-04-011wnYpjIwCEzFtl.htm](feats/class-04-011wnYpjIwCEzFtl.htm)|Elemental Summons|Convocar elemental|modificada|
|[class-04-0evTnx67DYsxWtg3.htm](feats/class-04-0evTnx67DYsxWtg3.htm)|Ice Crafter|Ice Crafter|modificada|
|[class-04-0haS0qXR9xTYKoTG.htm](feats/class-04-0haS0qXR9xTYKoTG.htm)|Snare Specialist|Especialista en trampas de lazo|modificada|
|[class-04-0VsrXyQYdluGRfsY.htm](feats/class-04-0VsrXyQYdluGRfsY.htm)|Pact of Fey Glamour|Pacto de enga feérico.|modificada|
|[class-04-16QBDinolgG2M66d.htm](feats/class-04-16QBDinolgG2M66d.htm)|Spellstriker|Spellstriker|modificada|
|[class-04-1fBHZpM3Z3MQtzvi.htm](feats/class-04-1fBHZpM3Z3MQtzvi.htm)|Doctor's Visitation|Doctor's Visitation|modificada|
|[class-04-1FnZhf5UwSb7Lo3t.htm](feats/class-04-1FnZhf5UwSb7Lo3t.htm)|Entity's Strike|Golpe de entidad|modificada|
|[class-04-1HvSkbUjqrIMXLiY.htm](feats/class-04-1HvSkbUjqrIMXLiY.htm)|Hijack Undead|Secuestrar muertos vivientes|modificada|
|[class-04-1j9QCzHHxi5MbIY3.htm](feats/class-04-1j9QCzHHxi5MbIY3.htm)|Big Game Trapper|Trampero de caza mayor|modificada|
|[class-04-1VrV24hhPSEvuKgT.htm](feats/class-04-1VrV24hhPSEvuKgT.htm)|Basic Shooting|Tiro básico|modificada|
|[class-04-1wTXeBrYU6BVEEOn.htm](feats/class-04-1wTXeBrYU6BVEEOn.htm)|Stage Fighting|Lucha Escénica|modificada|
|[class-04-2pglnWX8q5p3XcqR.htm](feats/class-04-2pglnWX8q5p3XcqR.htm)|Syu Tak-nwa's Skillful Tresses|Mechones Hábiles de Syu Tak-nwa|modificada|
|[class-04-2qR4QAgJVArv63Z2.htm](feats/class-04-2qR4QAgJVArv63Z2.htm)|Prayer-Touched Weapon|Prayer-Touched Weapon|modificada|
|[class-04-2UKf5IiUbpUbOC9a.htm](feats/class-04-2UKf5IiUbpUbOC9a.htm)|Draconic Scent|Aroma dracónico|modificada|
|[class-04-37uOb0iaWCfTCvBZ.htm](feats/class-04-37uOb0iaWCfTCvBZ.htm)|Harsh Judgement|Juicio severo|modificada|
|[class-04-39CqlOzlHjEhh0E4.htm](feats/class-04-39CqlOzlHjEhh0E4.htm)|Knockdown|Derribo|modificada|
|[class-04-3aG0gkHulBIHqqGE.htm](feats/class-04-3aG0gkHulBIHqqGE.htm)|Turn Back the Clock|Turn Back the Clock|modificada|
|[class-04-3EtlXNK4vc44R3Gm.htm](feats/class-04-3EtlXNK4vc44R3Gm.htm)|Reactive Dismissal|Despido reactivo|modificada|
|[class-04-3QLWe5oS9jGJ0Oq4.htm](feats/class-04-3QLWe5oS9jGJ0Oq4.htm)|Observant Explorer|Explorador Observador|modificada|
|[class-04-3R9l2t1ycN8iwmdU.htm](feats/class-04-3R9l2t1ycN8iwmdU.htm)|Student of the Staff|Estudiante del Bastón|modificada|
|[class-04-3y459uK2qfWtS9q4.htm](feats/class-04-3y459uK2qfWtS9q4.htm)|Everstand Strike|Golpe de Everstand|modificada|
|[class-04-4BbfHsGPRHPHfIGY.htm](feats/class-04-4BbfHsGPRHPHfIGY.htm)|Poultice Preparation|Preparación Cataplasma|modificada|
|[class-04-4HHw2DjTxdv1jBZd.htm](feats/class-04-4HHw2DjTxdv1jBZd.htm)|Decry Thief|Decry Ladrón|modificada|
|[class-04-4L5pj2W7Zyf8B3kg.htm](feats/class-04-4L5pj2W7Zyf8B3kg.htm)|Diving Armor|Armadura de Buceo|modificada|
|[class-04-4PvmSnyp3URIqJUM.htm](feats/class-04-4PvmSnyp3URIqJUM.htm)|Parting Shot|Parting Shot|modificada|
|[class-04-4Y7wKFogGB0LZ5ZA.htm](feats/class-04-4Y7wKFogGB0LZ5ZA.htm)|Steel Yourself!|¡Acércate!|modificada|
|[class-04-5EzJVhiHQvr3v72n.htm](feats/class-04-5EzJVhiHQvr3v72n.htm)|Breached Defenses|Defensas Emergidas|modificada|
|[class-04-5KXJkEs39y1gaPEm.htm](feats/class-04-5KXJkEs39y1gaPEm.htm)|Stargazer's Eyes|Stargazer's Eyes|modificada|
|[class-04-5op3m0gwZjL4udit.htm](feats/class-04-5op3m0gwZjL4udit.htm)|Duelist's Challenge|Duelist's Challenge|modificada|
|[class-04-6CE1nVGxt192AUGk.htm](feats/class-04-6CE1nVGxt192AUGk.htm)|Scout's Charge|Carga del Explorador|modificada|
|[class-04-6erzXaxxvcXYnL9H.htm](feats/class-04-6erzXaxxvcXYnL9H.htm)|Necrotic Infusion|Infusión necrótica.|modificada|
|[class-04-6oObLoUn3MjmwbaW.htm](feats/class-04-6oObLoUn3MjmwbaW.htm)|Cryptic Spell|Hechizo críptico|modificada|
|[class-04-6RjilN93bgy34y3H.htm](feats/class-04-6RjilN93bgy34y3H.htm)|Basic Red Mantis Magic|Magia básica de la mantis roja|modificada|
|[class-04-6TlBZSPr18Y8WiNk.htm](feats/class-04-6TlBZSPr18Y8WiNk.htm)|Instructive Strike|Golpe Instructivo|modificada|
|[class-04-7Hx0i4VchvwrIOV5.htm](feats/class-04-7Hx0i4VchvwrIOV5.htm)|Gear Gnash|Gear Gnash|modificada|
|[class-04-7jqBwXq9jVsghCva.htm](feats/class-04-7jqBwXq9jVsghCva.htm)|Gossip Lore|Gossip Lore|modificada|
|[class-04-7nnEtLdUKmDljdhm.htm](feats/class-04-7nnEtLdUKmDljdhm.htm)|Wellspring Control|Wellspring Control|modificada|
|[class-04-7Pb1WL8abrPBTPrH.htm](feats/class-04-7Pb1WL8abrPBTPrH.htm)|Basic Dogma|Dogma básico|modificada|
|[class-04-81pD03pQup9sPzXv.htm](feats/class-04-81pD03pQup9sPzXv.htm)|Snowcaster|Snowcaster|modificada|
|[class-04-8bDwJieEVCjrceM7.htm](feats/class-04-8bDwJieEVCjrceM7.htm)|Nocturnal Kindred|Nocturnal Kindred|modificada|
|[class-04-8pmd9gMl3TamFx3u.htm](feats/class-04-8pmd9gMl3TamFx3u.htm)|Deft Cooperation|Cooperación hábil|modificada|
|[class-04-9KvsO72JJ3pfkG4U.htm](feats/class-04-9KvsO72JJ3pfkG4U.htm)|Hurling Charge|Hurling Charge|modificada|
|[class-04-9SYnbjFgyucjhN5e.htm](feats/class-04-9SYnbjFgyucjhN5e.htm)|Dread Striker|Hostigador temible|modificada|
|[class-04-9yko78REsaw7i2gr.htm](feats/class-04-9yko78REsaw7i2gr.htm)|Suplex|Suplex|modificada|
|[class-04-AbsqV1P8OAhChcRl.htm](feats/class-04-AbsqV1P8OAhChcRl.htm)|Inspiring Resilience|Inspirando Resistente|modificada|
|[class-04-ALbosSUygdq4T1Yk.htm](feats/class-04-ALbosSUygdq4T1Yk.htm)|Poison Weapon|Arma envenenada|modificada|
|[class-04-aLJsBBZzlUK3G8MW.htm](feats/class-04-aLJsBBZzlUK3G8MW.htm)|Quick Study|Estudio rápido|modificada|
|[class-04-amPQHO9O86G6AC4P.htm](feats/class-04-amPQHO9O86G6AC4P.htm)|Devastating Spellstrike|Devastador Spellstrike|modificada|
|[class-04-asPQEfYCcsbYxx6K.htm](feats/class-04-asPQEfYCcsbYxx6K.htm)|Basic Breakthrough|Avance Básico|modificada|
|[class-04-AvfswILmh3BwNbyR.htm](feats/class-04-AvfswILmh3BwNbyR.htm)|Basic Sorcerer Spellcasting|Lanzamiento de conjuros básico de hechicero|modificada|
|[class-04-B0T6p3kcrOfSvLqQ.htm](feats/class-04-B0T6p3kcrOfSvLqQ.htm)|Triple Time|Triplicar la marcha|modificada|
|[class-04-B5f7eCAC3ZEmlR9h.htm](feats/class-04-B5f7eCAC3ZEmlR9h.htm)|Basic Spellcasting|Hechizo Básico|modificada|
|[class-04-B6jXVgfPuPWWLx2K.htm](feats/class-04-B6jXVgfPuPWWLx2K.htm)|Battle Assessment|Evaluación de batalla|modificada|
|[class-04-baD02AcIpU7xUBlD.htm](feats/class-04-baD02AcIpU7xUBlD.htm)|Basic Maneuver|Maniobra básica|modificada|
|[class-04-bCAvo59b5RyW12iI.htm](feats/class-04-bCAvo59b5RyW12iI.htm)|Sneak Attacker|Atacante furtivo|modificada|
|[class-04-BEqXsP6UqARzpEFD.htm](feats/class-04-BEqXsP6UqARzpEFD.htm)|Megaton Strike|Golpe Megatón|modificada|
|[class-04-bgnf7USFkqsNh8j1.htm](feats/class-04-bgnf7USFkqsNh8j1.htm)|Investigate Haunting|Investigar Haunting|modificada|
|[class-04-bj5QmGyrt7OYORjo.htm](feats/class-04-bj5QmGyrt7OYORjo.htm)|Senses of the Bear|Sentidos del Oso|modificada|
|[class-04-bJbWM6zcOeDvCOiZ.htm](feats/class-04-bJbWM6zcOeDvCOiZ.htm)|Sacred Spells|Hechizos Sagrados|modificada|
|[class-04-Bk07joho2dUG3lVw.htm](feats/class-04-Bk07joho2dUG3lVw.htm)|Duelist's Edge|Filo de duelista|modificada|
|[class-04-bLj5ufeOdVWDx8Aw.htm](feats/class-04-bLj5ufeOdVWDx8Aw.htm)|Starlit Eyes|Starlit Eyes|modificada|
|[class-04-BQkqUrlUVNFp8BEq.htm](feats/class-04-BQkqUrlUVNFp8BEq.htm)|Wild Lights|Luces Salvajes|modificada|
|[class-04-BridkNEysTuSOOLM.htm](feats/class-04-BridkNEysTuSOOLM.htm)|Basic Kata|Kata básica|modificada|
|[class-04-Btu0tA6SEBS6K1hE.htm](feats/class-04-Btu0tA6SEBS6K1hE.htm)|Never Tire|Nunca te canses|modificada|
|[class-04-BtZJJClWCpc31Ven.htm](feats/class-04-BtZJJClWCpc31Ven.htm)|Push Back the Dead!|¡Empuja a los muertos!|modificada|
|[class-04-bvOsJNeI0ewvQsFa.htm](feats/class-04-bvOsJNeI0ewvQsFa.htm)|Inspiring Marshal Stance|Posición de Mariscal Inspirador|modificada|
|[class-04-c7jNms3ZQ8eaMUqv.htm](feats/class-04-c7jNms3ZQ8eaMUqv.htm)|Efficient Alchemy (Alchemist)|Alquimia eficaz (Alquimista)|modificada|
|[class-04-cB6K0wkiDhduAjtL.htm](feats/class-04-cB6K0wkiDhduAjtL.htm)|Just the Tool|Sólo la herramienta|modificada|
|[class-04-chmQgMamyaZX90Tc.htm](feats/class-04-chmQgMamyaZX90Tc.htm)|Magical Edification|Magical Edification|modificada|
|[class-04-CLKlavik0540j5bl.htm](feats/class-04-CLKlavik0540j5bl.htm)|Wounded Rage|Furia herida|modificada|
|[class-04-cTR67ZKDD1EKntXw.htm](feats/class-04-cTR67ZKDD1EKntXw.htm)|Advanced Construct Companion|Compañero constructo avanzado|modificada|
|[class-04-CX4ISbBwndRWhP55.htm](feats/class-04-CX4ISbBwndRWhP55.htm)|Summon Ensemble|Summon Ensemble|modificada|
|[class-04-cxwDj2gZ7kJdP4hs.htm](feats/class-04-cxwDj2gZ7kJdP4hs.htm)|Thousand Faces|Millar de rostros|modificada|
|[class-04-CZQgH17ZxoBiVXLa.htm](feats/class-04-CZQgH17ZxoBiVXLa.htm)|Ka Stone Ritual|Ka Stone Ritual|modificada|
|[class-04-d8RfatiK9UOQANLz.htm](feats/class-04-d8RfatiK9UOQANLz.htm)|Lost in the Crowd|Perdido entre la multitud|modificada|
|[class-04-Daap4ugeDZQWoPCx.htm](feats/class-04-Daap4ugeDZQWoPCx.htm)|Spirit Spells|Conjuros espirituales|modificada|
|[class-04-DBsqWivnSaEo8jz5.htm](feats/class-04-DBsqWivnSaEo8jz5.htm)|Basic Arcana|Arcanos básicos|modificada|
|[class-04-dC14a0DZqDBA9B8g.htm](feats/class-04-dC14a0DZqDBA9B8g.htm)|Instant Backup|Copia de seguridad instantánea|modificada|
|[class-04-douVMHDuQQv8U8aq.htm](feats/class-04-douVMHDuQQv8U8aq.htm)|Lingering Chill|Escalofrío persistente|modificada|
|[class-04-DQN7YC7s7T0pL6Aa.htm](feats/class-04-DQN7YC7s7T0pL6Aa.htm)|Gadget Specialist|Especialista en gadgets|modificada|
|[class-04-dSSwRyuhKTq1VubX.htm](feats/class-04-dSSwRyuhKTq1VubX.htm)|Disarming Block|Desarmar Bloque|modificada|
|[class-04-dtt6xTOSF8PuoStg.htm](feats/class-04-dtt6xTOSF8PuoStg.htm)|Tracing Sigil|Rastrear sello|modificada|
|[class-04-ELdUj5ihdivlgb3H.htm](feats/class-04-ELdUj5ihdivlgb3H.htm)|Crystal Keeper Dedication|Dedicación Guardián de Cristal|modificada|
|[class-04-epBOnfEDOd4V9mQ0.htm](feats/class-04-epBOnfEDOd4V9mQ0.htm)|Natural Swimmer|Nadador Natural|modificada|
|[class-04-ePKe13FzKWaQqLo6.htm](feats/class-04-ePKe13FzKWaQqLo6.htm)|Basic Psychic Spellcasting|Hechizo Psíquico Básico|modificada|
|[class-04-EsA2F4R3UwUdI8Px.htm](feats/class-04-EsA2F4R3UwUdI8Px.htm)|Cycle Spell|Hechizo de Ciclo|modificada|
|[class-04-f69C05QokaBrDFjn.htm](feats/class-04-f69C05QokaBrDFjn.htm)|Shadow Spells|Hechizos de Sombra|modificada|
|[class-04-FbzbZc4LGUTcz9tA.htm](feats/class-04-FbzbZc4LGUTcz9tA.htm)|Gunpowder Gauntlet|Guantelete de Pólvora|modificada|
|[class-04-fGBb3VsJwf7osKaL.htm](feats/class-04-fGBb3VsJwf7osKaL.htm)|One With the Land|Uno con la tierra|modificada|
|[class-04-FItD6HmjasjbLdgS.htm](feats/class-04-FItD6HmjasjbLdgS.htm)|Spell Runes|Runas de Hechizo|modificada|
|[class-04-fO1vRDEfl9pysfLU.htm](feats/class-04-fO1vRDEfl9pysfLU.htm)|Guarded Movement|Movimiento protegido|modificada|
|[class-04-FoJd0oiEQ9mF5KS9.htm](feats/class-04-FoJd0oiEQ9mF5KS9.htm)|Steady Spellcasting (Magus)|Lanzamiento de conjuros firme (Magus)|modificada|
|[class-04-FSxugo3zTgRhW7Og.htm](feats/class-04-FSxugo3zTgRhW7Og.htm)|Improvised Pummel|Pummel Improvisado|modificada|
|[class-04-fT37dtsByEIc3glC.htm](feats/class-04-fT37dtsByEIc3glC.htm)|Scales Of The Dragon|Scales Of The Dragon|modificada|
|[class-04-FVDozRTuCQQzD97D.htm](feats/class-04-FVDozRTuCQQzD97D.htm)|Eye of Ozem|Ojo de Ozem|modificada|
|[class-04-fVP8jX7yRUpyrcVO.htm](feats/class-04-fVP8jX7yRUpyrcVO.htm)|Basic Cathartic Spellcasting|Hechizo catártico básico|modificada|
|[class-04-FWQSyjjYg8h0KpHq.htm](feats/class-04-FWQSyjjYg8h0KpHq.htm)|Initial Eidolon Ability|Habilidad inicial de Eidolon|modificada|
|[class-04-G43fiFtxFR24CWRs.htm](feats/class-04-G43fiFtxFR24CWRs.htm)|Magic Warrior Aspect|Aspecto Guerrero Mágico|modificada|
|[class-04-GByIIkeHN3JWkZIZ.htm](feats/class-04-GByIIkeHN3JWkZIZ.htm)|Mask Familiar|Máscara Familiar|modificada|
|[class-04-gc24C5CyWgqn1Lbl.htm](feats/class-04-gc24C5CyWgqn1Lbl.htm)|Basic Cleric Spellcasting|Lanzamiento de conjuros básico de clérigo|modificada|
|[class-04-GkPbsX5RZWyI0qFj.htm](feats/class-04-GkPbsX5RZWyI0qFj.htm)|Psychopomp Familiar|Psicopompo Familiar|modificada|
|[class-04-gL7QZsSMldjwE6te.htm](feats/class-04-gL7QZsSMldjwE6te.htm)|Inured to Alchemy|Curado a la alquimia|modificada|
|[class-04-gMPnLWLlHoNU9Lqv.htm](feats/class-04-gMPnLWLlHoNU9Lqv.htm)|Dragon Arcana|Arcanos del Dragón|modificada|
|[class-04-GNMy7NYfF3AQwHpN.htm](feats/class-04-GNMy7NYfF3AQwHpN.htm)|Divine Health|Salud divinaón|modificada|
|[class-04-gqQK7dDrGPkQDfQQ.htm](feats/class-04-gqQK7dDrGPkQDfQQ.htm)|Mortification|Mortificación|modificada|
|[class-04-GV0lOcVgcetsUlLO.htm](feats/class-04-GV0lOcVgcetsUlLO.htm)|Brilliant Crafter|Brilliant Crafter|modificada|
|[class-04-gVXefUaMzg7S1vpm.htm](feats/class-04-gVXefUaMzg7S1vpm.htm)|Knock Sense|Sentido de la Apertura|modificada|
|[class-04-Gz9NQBjSDRQP8YTY.htm](feats/class-04-Gz9NQBjSDRQP8YTY.htm)|Striker's Scroll|Striker's Scroll|modificada|
|[class-04-gZrlze9HlRYEQNBG.htm](feats/class-04-gZrlze9HlRYEQNBG.htm)|Soul Flare|Soul Flare|modificada|
|[class-04-H2GlPobLyqgBYbd9.htm](feats/class-04-H2GlPobLyqgBYbd9.htm)|Hybrid Study Spell|Hechizo de estudio híbrido|modificada|
|[class-04-h5ZT9i79BFVJ0VfE.htm](feats/class-04-h5ZT9i79BFVJ0VfE.htm)|Basic Concoction|Brebaje básico|modificada|
|[class-04-hENWnoZNljeJnZBR.htm](feats/class-04-hENWnoZNljeJnZBR.htm)|Elbow Breaker|Rompedor de codos|modificada|
|[class-04-HHAGiBYVv8nyUEsd.htm](feats/class-04-HHAGiBYVv8nyUEsd.htm)|Dual-Handed Assault|Asalto a dos manos|modificada|
|[class-04-HpT1GlcnkCBnDnVF.htm](feats/class-04-HpT1GlcnkCBnDnVF.htm)|Barbarian Resiliency|Resiliencia de bárbaro|modificada|
|[class-04-HVP3ZGIOlxlFy0ni.htm](feats/class-04-HVP3ZGIOlxlFy0ni.htm)|Hunter's Sanctum|Santuario del Cazador|modificada|
|[class-04-HvPvyeXM2iMK4OYf.htm](feats/class-04-HvPvyeXM2iMK4OYf.htm)|Repeating Hand Crossbow Training|Entrenamiento con ballesta de mano repetidora|modificada|
|[class-04-Hwvrive2vBIqZUcE.htm](feats/class-04-Hwvrive2vBIqZUcE.htm)|Familiar Mascot|Mascota Familiar|modificada|
|[class-04-Hzq8FOtaYWpur2BL.htm](feats/class-04-Hzq8FOtaYWpur2BL.htm)|Safety Measures|Medidas de Seguridad|modificada|
|[class-04-i86JRWsFRpfEJnZP.htm](feats/class-04-i86JRWsFRpfEJnZP.htm)|Pirate Weapon Training|Entrenamiento con armas piratas|modificada|
|[class-04-iAziUKdxgy4k4ypY.htm](feats/class-04-iAziUKdxgy4k4ypY.htm)|Shining Arms|Brazos Brillantes|modificada|
|[class-04-IeRX0tGbeOF0ev08.htm](feats/class-04-IeRX0tGbeOF0ev08.htm)|Shorthanded|Shorthanded|modificada|
|[class-04-ifiaewasPbc091BQ.htm](feats/class-04-ifiaewasPbc091BQ.htm)|Ankle Biter|Muerde Tobillos|modificada|
|[class-04-iKOcwFCbNX1a2OFT.htm](feats/class-04-iKOcwFCbNX1a2OFT.htm)|Ardent Armiger|Armero ardiente|modificada|
|[class-04-iMh9Ve8Kt8ZcdKU0.htm](feats/class-04-iMh9Ve8Kt8ZcdKU0.htm)|Bullying Staff|Bullying Staff|modificada|
|[class-04-io6eJGrw701WbmYe.htm](feats/class-04-io6eJGrw701WbmYe.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[class-04-IOdk7bOJ4dgYVh9I.htm](feats/class-04-IOdk7bOJ4dgYVh9I.htm)|Improved Communal Healing|Curación en común mejorada|modificada|
|[class-04-iojlXjVdbzi1fZGt.htm](feats/class-04-iojlXjVdbzi1fZGt.htm)|Directed Channel|Canalización dirigida|modificada|
|[class-04-IqTtpxZ48rApy4BN.htm](feats/class-04-IqTtpxZ48rApy4BN.htm)|Executioner Weapon Training|Entrenamiento con armas de verdugo|modificada|
|[class-04-irDFSzKCaF4ux3bx.htm](feats/class-04-irDFSzKCaF4ux3bx.htm)|Reflexive Catch|Reflexive Catch|modificada|
|[class-04-IrxtH1BFlUOS0DnQ.htm](feats/class-04-IrxtH1BFlUOS0DnQ.htm)|Captivator Dedication|Dedicación del Cautivador|modificada|
|[class-04-IYt6pMqiTocTrixA.htm](feats/class-04-IYt6pMqiTocTrixA.htm)|Familiar Conduit|Conducto Familiar|modificada|
|[class-04-iyXw5PnevT2jT8kU.htm](feats/class-04-iyXw5PnevT2jT8kU.htm)|Sense Alignment|Sense Alignment|modificada|
|[class-04-J1MURfqf0kbxrKG9.htm](feats/class-04-J1MURfqf0kbxrKG9.htm)|Warding Light|Warding Luz|modificada|
|[class-04-J8BSHRkmP3QLknwF.htm](feats/class-04-J8BSHRkmP3QLknwF.htm)|Beast Speaker|Altavoz Bestia|modificada|
|[class-04-j9Rp4fOZIxizyvYy.htm](feats/class-04-j9Rp4fOZIxizyvYy.htm)|Viking Weapon Familiarity|Familiaridad con armas vikingas|modificada|
|[class-04-jBeuyq0Aged45YAc.htm](feats/class-04-jBeuyq0Aged45YAc.htm)|Enduring Alchemy|Alquimia duradera|modificada|
|[class-04-JbrVcOf82oFXk3mY.htm](feats/class-04-JbrVcOf82oFXk3mY.htm)|Swipe|Vaivén|modificada|
|[class-04-JGfJPx6xkx11zHlW.htm](feats/class-04-JGfJPx6xkx11zHlW.htm)|Linked Focus|Foco enlazado|modificada|
|[class-04-JgfZOEOgnbKNYth4.htm](feats/class-04-JgfZOEOgnbKNYth4.htm)|Irezoko Tattoo|Irezoko Tattoo|modificada|
|[class-04-Jk6gZzXEABiX5A0S.htm](feats/class-04-Jk6gZzXEABiX5A0S.htm)|Loose Cannon|Loose Cannon|modificada|
|[class-04-JKvP4pFHzwWsHu2n.htm](feats/class-04-JKvP4pFHzwWsHu2n.htm)|Ranger Resiliency|Resiliencia de explorador|modificada|
|[class-04-JlGZFW3mCNxWPKvX.htm](feats/class-04-JlGZFW3mCNxWPKvX.htm)|Silent Spell|Conjurar en silencio|modificada|
|[class-04-jlLWz8e7PpLFt0Ed.htm](feats/class-04-jlLWz8e7PpLFt0Ed.htm)|Healing Touch|Toque curativo|modificada|
|[class-04-JM0umAUx30mAkuTz.htm](feats/class-04-JM0umAUx30mAkuTz.htm)|Basic Muse's Whispers|Susurros de la musa básicos|modificada|
|[class-04-jQ8JntOdBFHPw5S4.htm](feats/class-04-jQ8JntOdBFHPw5S4.htm)|Light of Revelation|Luz del Apocalipsis|modificada|
|[class-04-jr2LYiMvjnTNhfMM.htm](feats/class-04-jr2LYiMvjnTNhfMM.htm)|Slayer's Strike|Golpe del Cazador|modificada|
|[class-04-JStTmD3W8R41WvPg.htm](feats/class-04-JStTmD3W8R41WvPg.htm)|Sleepwalker Dedication|Sleepwalker Dedication|modificada|
|[class-04-jsXDbQAAH3yMchPU.htm](feats/class-04-jsXDbQAAH3yMchPU.htm)|Spot Translate|Spot Translate|modificada|
|[class-04-juTBSRs2jzJzLoth.htm](feats/class-04-juTBSRs2jzJzLoth.htm)|Fake It Till You Make It|Fake It Till You Make It|modificada|
|[class-04-jZbhRBR2yawntSmd.htm](feats/class-04-jZbhRBR2yawntSmd.htm)|Agile Hand|Mano Ágil|modificada|
|[class-04-JZurhROfi2JfmLfb.htm](feats/class-04-JZurhROfi2JfmLfb.htm)|Unnerving Expansion|Expansión enervante|modificada|
|[class-04-K3TasgeZLJQ84qtZ.htm](feats/class-04-K3TasgeZLJQ84qtZ.htm)|Primal Evolution|Evolución primigenia|modificada|
|[class-04-k5C1WNuYQ4u7nSHt.htm](feats/class-04-k5C1WNuYQ4u7nSHt.htm)|Nonlethal Takedown|No letal Takedown|modificada|
|[class-04-kh4bTBgi3C9CjwHK.htm](feats/class-04-kh4bTBgi3C9CjwHK.htm)|Melodious Spell|Conjuro melodioso|modificada|
|[class-04-kmIgbWLbaarqmMaY.htm](feats/class-04-kmIgbWLbaarqmMaY.htm)|Basic Captivator Spellcasting|Lanzamiento de hechizos de cautivador básico|modificada|
|[class-04-KMVXUgFArcftg1jQ.htm](feats/class-04-KMVXUgFArcftg1jQ.htm)|Masquerade of Seasons Stance|Posición Mascarada de Conocedores|modificada|
|[class-04-KOf6kmwAZaUJSDW9.htm](feats/class-04-KOf6kmwAZaUJSDW9.htm)|Terrain Scout|Explorador del terreno|modificada|
|[class-04-kPhym38UCLJpjnJD.htm](feats/class-04-kPhym38UCLJpjnJD.htm)|Angel of Vindication|Ángel de la reivindicación|modificada|
|[class-04-KrYvJ5n06yHCipCZ.htm](feats/class-04-KrYvJ5n06yHCipCZ.htm)|Play To The Crowd|Play To The Crowd|modificada|
|[class-04-kU4K9jj9qnktoAaQ.htm](feats/class-04-kU4K9jj9qnktoAaQ.htm)|Ritualist Dedication|Dedicación Ritualista|modificada|
|[class-04-kUa8PBjKmBk04zmc.htm](feats/class-04-kUa8PBjKmBk04zmc.htm)|Basic Wilding|Salvajismo básico|modificada|
|[class-04-Kxckxf4G9URXNc07.htm](feats/class-04-Kxckxf4G9URXNc07.htm)|Wolf in Sheep's Clothing|Lobo con piel de cordero|modificada|
|[class-04-KZJJ0WxcRd4RZKJR.htm](feats/class-04-KZJJ0WxcRd4RZKJR.htm)|Elemental Familiar|Elemental Familiar|modificada|
|[class-04-lKgpe2JKaeLjgnYF.htm](feats/class-04-lKgpe2JKaeLjgnYF.htm)|Woodland Stride|Paso forestal|modificada|
|[class-04-lkLwEfc3ZhLJSkVr.htm](feats/class-04-lkLwEfc3ZhLJSkVr.htm)|Manipulative Charm|Hechizar Manipulador|modificada|
|[class-04-lL2fQJ2oRyBgga8Q.htm](feats/class-04-lL2fQJ2oRyBgga8Q.htm)|Occult Evolution|Evolución ocultista|modificada|
|[class-04-lpG7ZXFDZygmkbH4.htm](feats/class-04-lpG7ZXFDZygmkbH4.htm)|Bloodletting Claws|Garras Sanguinarias|modificada|
|[class-04-LQ5YW01UD9hGKk0l.htm](feats/class-04-LQ5YW01UD9hGKk0l.htm)|Corpse Tender's Font|Corpse Tender's Font|modificada|
|[class-04-lyJ3pPvZAr9nRco6.htm](feats/class-04-lyJ3pPvZAr9nRco6.htm)|Hallowed Initiate|Iniciado Santificado|modificada|
|[class-04-lYVAGuHU47Ixyuxy.htm](feats/class-04-lYVAGuHU47Ixyuxy.htm)|Focused Juggler|Malabarista Centrado|modificada|
|[class-04-M9WIUEPY6IoRtgWN.htm](feats/class-04-M9WIUEPY6IoRtgWN.htm)|Gravelands Herbalist|Herbolario de las Tierras de las Sepulturas|modificada|
|[class-04-meQJfsKVar9tm6c9.htm](feats/class-04-meQJfsKVar9tm6c9.htm)|Running Reload|Recarga a la carrera|modificada|
|[class-04-Mj1KTiAwwovm7K9f.htm](feats/class-04-Mj1KTiAwwovm7K9f.htm)|Stand Still|Ponerse de pie|modificada|
|[class-04-MkLujXEplJUt6Arv.htm](feats/class-04-MkLujXEplJUt6Arv.htm)|Necromantic Resistance (Undead Slayer)|Resistencia nigromántica (Asesino de muertos vivientes)|modificada|
|[class-04-MnqDDUekqd40HTZc.htm](feats/class-04-MnqDDUekqd40HTZc.htm)|Necromantic Resistance|Resistencia Nigromántica|modificada|
|[class-04-MprbkBZUouqvbKGo.htm](feats/class-04-MprbkBZUouqvbKGo.htm)|Folktales Lore|Folktales Lore|modificada|
|[class-04-mRHkGMLecd5aaj2R.htm](feats/class-04-mRHkGMLecd5aaj2R.htm)|Tools Of The Trade|Tools Of The Trade|modificada|
|[class-04-MU9qS0QuBdcyLkza.htm](feats/class-04-MU9qS0QuBdcyLkza.htm)|Homing Beacon|Homing Beacon|modificada|
|[class-04-mV911W6MTGMvHPbE.htm](feats/class-04-mV911W6MTGMvHPbE.htm)|Basic Hunter's Trick|Truco del cazador básico|modificada|
|[class-04-mxO7u59ms58q7zyj.htm](feats/class-04-mxO7u59ms58q7zyj.htm)|Magical Trickster|Embaucador mágico|modificada|
|[class-04-mXp6G4YWCXGvp7Qd.htm](feats/class-04-mXp6G4YWCXGvp7Qd.htm)|Awakened Power|Poder Despertado|modificada|
|[class-04-N0YcU8mIJnQ4C2N6.htm](feats/class-04-N0YcU8mIJnQ4C2N6.htm)|Posse|Posse|modificada|
|[class-04-n5T4ChWJqDUblYfR.htm](feats/class-04-n5T4ChWJqDUblYfR.htm)|Bless Shield|Bendecir Escudo|modificada|
|[class-04-N8mhYbr5xBI8jydb.htm](feats/class-04-N8mhYbr5xBI8jydb.htm)|Violent Unleash|Violent Unleash|modificada|
|[class-04-nAgAICjPd4BSQlAj.htm](feats/class-04-nAgAICjPd4BSQlAj.htm)|Axe Climber|Escalador de hachas|modificada|
|[class-04-NagTqSLK8bAlo2nY.htm](feats/class-04-NagTqSLK8bAlo2nY.htm)|Firebrand Braggart Dedication|Instigador de la Dedicación Braggart|modificada|
|[class-04-nc5G99d20PI9JKCK.htm](feats/class-04-nc5G99d20PI9JKCK.htm)|Monk Resiliency|Resiliencia de monje|modificada|
|[class-04-ncXK0Cc8dZ9TilSC.htm](feats/class-04-ncXK0Cc8dZ9TilSC.htm)|Devil's Eye|Ojo del Diablo|modificada|
|[class-04-NlEZ0piDxg9buXCL.htm](feats/class-04-NlEZ0piDxg9buXCL.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[class-04-NQtIhIowH1tVywZI.htm](feats/class-04-NQtIhIowH1tVywZI.htm)|Replenishing Consumption|Reposición de Consumo|modificada|
|[class-04-NrCloSukzSUCprsM.htm](feats/class-04-NrCloSukzSUCprsM.htm)|Blessed Sacrifice|Sacrificio Bendecido|modificada|
|[class-04-nroOy0PBeEUGdUXD.htm](feats/class-04-nroOy0PBeEUGdUXD.htm)|Dousing Spell|Hechizo Dousing|modificada|
|[class-04-NrZt98r47sPSQ06j.htm](feats/class-04-NrZt98r47sPSQ06j.htm)|Crystal Ward Spells|Cristalizar Hechizos Ward|modificada|
|[class-04-NSyzjkDdQU2A75mX.htm](feats/class-04-NSyzjkDdQU2A75mX.htm)|Basic Bard Spellcasting|Lanzamiento de conjuros básico de bardo|modificada|
|[class-04-nT9Z4OPDTOg2AGYc.htm](feats/class-04-nT9Z4OPDTOg2AGYc.htm)|Basic Blood Potency|Potencia sanguínea básica.|modificada|
|[class-04-nU8W1QoA9hl3h6nR.htm](feats/class-04-nU8W1QoA9hl3h6nR.htm)|Watch and Learn|Mira y aprende|modificada|
|[class-04-NZgjqVV2HYzLCvvA.htm](feats/class-04-NZgjqVV2HYzLCvvA.htm)|Aldori Parry|Aldori Parry|modificada|
|[class-04-o5q9FBrPsAYqEl5w.htm](feats/class-04-o5q9FBrPsAYqEl5w.htm)|Channel Smite|Canalizar castigo|modificada|
|[class-04-oGu9AtUAx0SpRXy8.htm](feats/class-04-oGu9AtUAx0SpRXy8.htm)|Favored Enemy|Enemigo predilecto|modificada|
|[class-04-oHdUwzUUblg3neCT.htm](feats/class-04-oHdUwzUUblg3neCT.htm)|Order Magic|Magia de la orden|modificada|
|[class-04-oIE88rIDEFNm83Mr.htm](feats/class-04-oIE88rIDEFNm83Mr.htm)|Powerful Shove|Empujón poderoso|modificada|
|[class-04-OJxEF5FONTtEdbpP.htm](feats/class-04-OJxEF5FONTtEdbpP.htm)|Heal Animal|Curar animal|modificada|
|[class-04-OQAo3Us0ODGYdNNn.htm](feats/class-04-OQAo3Us0ODGYdNNn.htm)|Expert Backstabber|Pulada trapera experta|modificada|
|[class-04-OqU6QXkMrZqToEEi.htm](feats/class-04-OqU6QXkMrZqToEEi.htm)|Opportunist|Oportunista|modificada|
|[class-04-otLs7XzMXR1cZKGe.htm](feats/class-04-otLs7XzMXR1cZKGe.htm)|Quick Juggler|Malabarista Rápido|modificada|
|[class-04-OxvZSFGOjGfcSCv8.htm](feats/class-04-OxvZSFGOjGfcSCv8.htm)|Aeon Resonance|Aeon Resonance|modificada|
|[class-04-p4OllLWJ2rV4sjxe.htm](feats/class-04-p4OllLWJ2rV4sjxe.htm)|Semblance of Life|Semblanza de la vida|modificada|
|[class-04-pfBVx5xBfwKd1iVL.htm](feats/class-04-pfBVx5xBfwKd1iVL.htm)|Spiritual Explorer|Explorador espiritual|modificada|
|[class-04-pfF5UzT1MLW3KwHd.htm](feats/class-04-pfF5UzT1MLW3KwHd.htm)|Shared Attunement|Shared Attunement|modificada|
|[class-04-PH5b61x3iJSKP3Xi.htm](feats/class-04-PH5b61x3iJSKP3Xi.htm)|Farabellus Flip|Farabellus Flip|modificada|
|[class-04-pKoW1X95LjmWn5Jq.htm](feats/class-04-pKoW1X95LjmWn5Jq.htm)|Poisoner's Twist|Poisoner's Twist|modificada|
|[class-04-PLz1oIEGJojVUBsW.htm](feats/class-04-PLz1oIEGJojVUBsW.htm)|Fighter Resiliency|Resiliencia de guerrero|modificada|
|[class-04-PNG7e39mEhq1MorG.htm](feats/class-04-PNG7e39mEhq1MorG.htm)|Basic Druid Spellcasting|Lanzamiento de conjuros básico de druida|modificada|
|[class-04-pqVG9mRKcXg5Rsjc.htm](feats/class-04-pqVG9mRKcXg5Rsjc.htm)|Thoughtform Summoning|Thoughtform Summoning|modificada|
|[class-04-pvFRMbIazwAO0fjH.htm](feats/class-04-pvFRMbIazwAO0fjH.htm)|Echo of the Fallen|Eco de los Caídos|modificada|
|[class-04-PwcmmJOLY8C9JHau.htm](feats/class-04-PwcmmJOLY8C9JHau.htm)|Double Shot|Disparo doble|modificada|
|[class-04-pwM4RGwCTLiVSic0.htm](feats/class-04-pwM4RGwCTLiVSic0.htm)|Ghostly Resistance|Resistencia fantasmal|modificada|
|[class-04-Q1O4P1YIkCfeedHH.htm](feats/class-04-Q1O4P1YIkCfeedHH.htm)|Alchemical Shot|Tiro Alquímico|modificada|
|[class-04-Q6EkzXkbMuuk8f7c.htm](feats/class-04-Q6EkzXkbMuuk8f7c.htm)|Lion's Might|Poder del León|modificada|
|[class-04-qav9ec9cR4lFcz3C.htm](feats/class-04-qav9ec9cR4lFcz3C.htm)|Disrupt Prey|Perturbar presa|modificada|
|[class-04-qAVM65grmny3f8DP.htm](feats/class-04-qAVM65grmny3f8DP.htm)|Coughing Dragon Display|Exhibición del Dragón Tosedor|modificada|
|[class-04-qbH9ns3HMYBxIvEQ.htm](feats/class-04-qbH9ns3HMYBxIvEQ.htm)|Fire Resistance|Resistencia al Fuego|modificada|
|[class-04-qFR5OddDBmhZe6nl.htm](feats/class-04-qFR5OddDBmhZe6nl.htm)|Chemical Purification|Purificación química|modificada|
|[class-04-qmx8QwVepqI9FqiD.htm](feats/class-04-qmx8QwVepqI9FqiD.htm)|Tandem Movement|Movimiento en tándem|modificada|
|[class-04-QMycbf2StuPcUbzO.htm](feats/class-04-QMycbf2StuPcUbzO.htm)|Reach Beyond|Alcance Más Allá|modificada|
|[class-04-qpoE2KhsPbF1ZDsx.htm](feats/class-04-qpoE2KhsPbF1ZDsx.htm)|Ravenous Charge|Carga Voraz|modificada|
|[class-04-qQh8wnslOagixxD1.htm](feats/class-04-qQh8wnslOagixxD1.htm)|Careful Explorer|Explorador cuidadoso|modificada|
|[class-04-QrShJGrvmWPBj4oN.htm](feats/class-04-QrShJGrvmWPBj4oN.htm)|Arcane School Spell|Conjuro de escuela arcana|modificada|
|[class-04-qV6EuOI3UJYIL6xa.htm](feats/class-04-qV6EuOI3UJYIL6xa.htm)|Consolidated Overlay Panopticon|Consolidated Overlay Panopticon|modificada|
|[class-04-qWb5IxkBUpJWKSLf.htm](feats/class-04-qWb5IxkBUpJWKSLf.htm)|Champion Resiliency|Resiliencia de campeón|modificada|
|[class-04-qxh4evekG28Gt1vj.htm](feats/class-04-qxh4evekG28Gt1vj.htm)|Arcane Evolution|Evolución arcana|modificada|
|[class-04-R56DLSqoBG8dX4Zv.htm](feats/class-04-R56DLSqoBG8dX4Zv.htm)|Scion of Domora Dedication|Dedicación Scion of Domora|modificada|
|[class-04-r6dvGxru3FWNLVE2.htm](feats/class-04-r6dvGxru3FWNLVE2.htm)|Far Shot|Disparo a larga distancia|modificada|
|[class-04-R7c4PyTNkZb0yvoT.htm](feats/class-04-R7c4PyTNkZb0yvoT.htm)|Dread Marshal Stance|Posición de Mariscal Temible|modificada|
|[class-04-RAaUb9MPSDv1CGmF.htm](feats/class-04-RAaUb9MPSDv1CGmF.htm)|Perfect Weaponry|Armamento perfecto|modificada|
|[class-04-RL4GrJ2vTrdJuzW1.htm](feats/class-04-RL4GrJ2vTrdJuzW1.htm)|Clinging Climber|Escalador|modificada|
|[class-04-rNPeOwFZE5Ma18JJ.htm](feats/class-04-rNPeOwFZE5Ma18JJ.htm)|Social Purview|Social Purview|modificada|
|[class-04-RonS3ZJs4poFTckH.htm](feats/class-04-RonS3ZJs4poFTckH.htm)|Shrink Down|Encoger|modificada|
|[class-04-rPbh7sOhhL7i3j1z.htm](feats/class-04-rPbh7sOhhL7i3j1z.htm)|Greenwatch Initiate|Greenwatch Iniciar|modificada|
|[class-04-RsluSLtSWq1vN8Hc.htm](feats/class-04-RsluSLtSWq1vN8Hc.htm)|Form Control|Control de forma|modificada|
|[class-04-RzElsoBGTjKWjPgY.htm](feats/class-04-RzElsoBGTjKWjPgY.htm)|Sheltering Cave|Cueva Refugio|modificada|
|[class-04-S5vOQ7J8DKR8sEj0.htm](feats/class-04-S5vOQ7J8DKR8sEj0.htm)|Paired Link|Enlace emparejado|modificada|
|[class-04-sahJHnojXO9eEXVE.htm](feats/class-04-sahJHnojXO9eEXVE.htm)|Inspire Defense|Inspirar defensa|modificada|
|[class-04-SASOvOG2Nqs2ekdA.htm](feats/class-04-SASOvOG2Nqs2ekdA.htm)|Forceful Push|Empujón vigoroso|modificada|
|[class-04-SCDSgeJU7vULvHmo.htm](feats/class-04-SCDSgeJU7vULvHmo.htm)|Fast Movement|Movimiento rápido|modificada|
|[class-04-sgaqlDFTVC7Ryurt.htm](feats/class-04-sgaqlDFTVC7Ryurt.htm)|Deflect Arrow|Desviar flecha|modificada|
|[class-04-shDyS87L0eiabyHw.htm](feats/class-04-shDyS87L0eiabyHw.htm)|Grave's Voice|Voz de la tumba|modificada|
|[class-04-sHS5LQfBHdCsv6vZ.htm](feats/class-04-sHS5LQfBHdCsv6vZ.htm)|Basic Thoughtform|Forma de pensamiento básica|modificada|
|[class-04-SHUfHzElHZPXJFiP.htm](feats/class-04-SHUfHzElHZPXJFiP.htm)|Basic Wizard Spellcasting|Lanzamiento de conjuros básico de mago|modificada|
|[class-04-SjJ8BOy5sc8p2H5E.htm](feats/class-04-SjJ8BOy5sc8p2H5E.htm)|Log Roll|Log Roll|modificada|
|[class-04-sM3nmDi3PHWI64SH.htm](feats/class-04-sM3nmDi3PHWI64SH.htm)|Astral Tether|Amarre astral|modificada|
|[class-04-SnIx3FhBuuq6AZD0.htm](feats/class-04-SnIx3FhBuuq6AZD0.htm)|Quick Reversal|Redirección rápida|modificada|
|[class-04-Sr6CcCXceV8ALAmB.htm](feats/class-04-Sr6CcCXceV8ALAmB.htm)|Basic Fury|Furia básica|modificada|
|[class-04-SVdYJW5JsOMhAYd0.htm](feats/class-04-SVdYJW5JsOMhAYd0.htm)|Fleet Tempo|Pies ligeros|modificada|
|[class-04-sVEF7j9Wh1KNGPUm.htm](feats/class-04-sVEF7j9Wh1KNGPUm.htm)|Modular Dynamo|Modular Dynamo|modificada|
|[class-04-SVhyfYNAphQPxFjd.htm](feats/class-04-SVhyfYNAphQPxFjd.htm)|Mammoth Lord Dedication|Dedicación Señor Mamut|modificada|
|[class-04-TaoV7ArAuZjxpeQB.htm](feats/class-04-TaoV7ArAuZjxpeQB.htm)|Order Spell|Conjuro de orden|modificada|
|[class-04-TC6zELq2BOqVfgMh.htm](feats/class-04-TC6zELq2BOqVfgMh.htm)|Basic Synergy|Sinergia Básica|modificada|
|[class-04-TgYs2m9scSyEJwdr.htm](feats/class-04-TgYs2m9scSyEJwdr.htm)|Vibration Sense|Sentido de la Vibración|modificada|
|[class-04-tHqlcgcxHXzqLHPZ.htm](feats/class-04-tHqlcgcxHXzqLHPZ.htm)|Shielded Stride|Zancada protegida|modificada|
|[class-04-tIeVe9jOmxW7NgCK.htm](feats/class-04-tIeVe9jOmxW7NgCK.htm)|Distracting Spellstrike|Distraer Spellstrike|modificada|
|[class-04-TIwk07T0OxSbcOpJ.htm](feats/class-04-TIwk07T0OxSbcOpJ.htm)|Aura of Courage|Aura de valor|modificada|
|[class-04-Tln47zk8F8nswrEI.htm](feats/class-04-Tln47zk8F8nswrEI.htm)|Feast|Festín|modificada|
|[class-04-TltRTR1e5KGly64k.htm](feats/class-04-TltRTR1e5KGly64k.htm)|Basic Martial Magic|Magia Marcial Básica|modificada|
|[class-04-TNpoEG0cUEAuSju7.htm](feats/class-04-TNpoEG0cUEAuSju7.htm)|Sun Blade|Sun Blade|modificada|
|[class-04-toFhkS9QbObxg6cp.htm](feats/class-04-toFhkS9QbObxg6cp.htm)|Versatile Signature|Signatura versátil|modificada|
|[class-04-tor4lzY0wpNcJd2U.htm](feats/class-04-tor4lzY0wpNcJd2U.htm)|Ghost Blade|Ghost Blade|modificada|
|[class-04-TORYSZMLMAGgsSEW.htm](feats/class-04-TORYSZMLMAGgsSEW.htm)|Companion's Cry|Grito del comparo|modificada|
|[class-04-Tpcq3Lk7qEOZ3LDP.htm](feats/class-04-Tpcq3Lk7qEOZ3LDP.htm)|Scout's Warning|Aviso del batidor|modificada|
|[class-04-TUIQBw9miDowhezw.htm](feats/class-04-TUIQBw9miDowhezw.htm)|Exorcist Dedication|Exorcist Dedication|modificada|
|[class-04-txo0xFYUfAjdmyjt.htm](feats/class-04-txo0xFYUfAjdmyjt.htm)|Butterfly Blade Dedication|Dedicación Hoja de Mariposa|modificada|
|[class-04-U1I87PGViGpGaP7D.htm](feats/class-04-U1I87PGViGpGaP7D.htm)|Attunement Shift|Cambio de Sintonía|modificada|
|[class-04-U7dntZ2dAklzsqw8.htm](feats/class-04-U7dntZ2dAklzsqw8.htm)|Blessing of the Sun Gods|Bendición de los Dioses del Sol|modificada|
|[class-04-uCElsebJ45ltmZMT.htm](feats/class-04-uCElsebJ45ltmZMT.htm)|Mature Beastmaster Companion|Compañero Beastmaster Maduro|modificada|
|[class-04-uhU0KajD09h5bw4e.htm](feats/class-04-uhU0KajD09h5bw4e.htm)|Acclimatization|Aclimatación|modificada|
|[class-04-uisI6b7Ua5zSHDwj.htm](feats/class-04-uisI6b7Ua5zSHDwj.htm)|Psi Strikes|Golpes Psi|modificada|
|[class-04-UjWLK86BgHxz3Itd.htm](feats/class-04-UjWLK86BgHxz3Itd.htm)|Calculated Splash|Salpicadura calculada|modificada|
|[class-04-uKBT0D9gxdwMcwNl.htm](feats/class-04-uKBT0D9gxdwMcwNl.htm)|Basic Devotion|Devoción básica|modificada|
|[class-04-uMjczqcXuteoP7lf.htm](feats/class-04-uMjczqcXuteoP7lf.htm)|Claws Of The Dragon|Garras de dragón|modificada|
|[class-04-UsEGem9s9ElaTS0d.htm](feats/class-04-UsEGem9s9ElaTS0d.htm)|Voice of Authority|Voz de autoridad|modificada|
|[class-04-uT9RnHLgIIcm7Hhs.htm](feats/class-04-uT9RnHLgIIcm7Hhs.htm)|Trapsmith Dedication|Dedicación Trapsmith|modificada|
|[class-04-UWG1USE0L2ZxEPiO.htm](feats/class-04-UWG1USE0L2ZxEPiO.htm)|Wholeness of Body|Plenitud corporal|modificada|
|[class-04-uY03kVQBA81gbTj9.htm](feats/class-04-uY03kVQBA81gbTj9.htm)|Emergency Targe|Targe de emergencia|modificada|
|[class-04-UyMQ1X8KLSZvm7AT.htm](feats/class-04-UyMQ1X8KLSZvm7AT.htm)|Wayfinder Resonance Tinkerer|Orientador de Resonancia Pathfinder Trastear|modificada|
|[class-04-v6wYNnkoVDquzRpw.htm](feats/class-04-v6wYNnkoVDquzRpw.htm)|Spyglass Modification|Modificación Spyglass|modificada|
|[class-04-vbHF6HEC9jQorFGl.htm](feats/class-04-vbHF6HEC9jQorFGl.htm)|Environmental Explorer|Explorador Ambiental|modificada|
|[class-04-vCsvT7xqBIolF7zH.htm](feats/class-04-vCsvT7xqBIolF7zH.htm)|Command Corpse|Cadáver de orden imperiosa|modificada|
|[class-04-vDeG0N4kzTBDTC2b.htm](feats/class-04-vDeG0N4kzTBDTC2b.htm)|Sabotage|Sabotaje|modificada|
|[class-04-vKzAIJuyr9SU2JzU.htm](feats/class-04-vKzAIJuyr9SU2JzU.htm)|Safe House|Safe House|modificada|
|[class-04-VP2CWUTZ9Edg82uz.htm](feats/class-04-VP2CWUTZ9Edg82uz.htm)|Swift Leap|Salto veloz|modificada|
|[class-04-vPZxFpq7XkRmE3Uc.htm](feats/class-04-vPZxFpq7XkRmE3Uc.htm)|Black Powder Boost|Black Powder Boost|modificada|
|[class-04-vQ4DNfpktmaqdgdM.htm](feats/class-04-vQ4DNfpktmaqdgdM.htm)|Expanded Domain Initiate|Iniciado de un dominio expandido|modificada|
|[class-04-vUQ3XwCT0i3ydX1U.htm](feats/class-04-vUQ3XwCT0i3ydX1U.htm)|Always Ready|Siempre listo|modificada|
|[class-04-vVqOBzWTpWirPbrK.htm](feats/class-04-vVqOBzWTpWirPbrK.htm)|Seeker of Truths|Buscador de Verdades|modificada|
|[class-04-VVsYBmVi2E1u9E5Z.htm](feats/class-04-VVsYBmVi2E1u9E5Z.htm)|Reactive Pursuit|Persecución reactiva|modificada|
|[class-04-W7IfFi6MkTDfO2hb.htm](feats/class-04-W7IfFi6MkTDfO2hb.htm)|Disengaging Twist|Desenganche Twist|modificada|
|[class-04-wB1ONG2uO7RnD1iC.htm](feats/class-04-wB1ONG2uO7RnD1iC.htm)|Flying Kick|Patada voladora|modificada|
|[class-04-WBp5ybj3kLcsGdVr.htm](feats/class-04-WBp5ybj3kLcsGdVr.htm)|Claws Of The Dragon (Draconic Bloodline)|Garras de dragón (linaje dracónico)|modificada|
|[class-04-wBqQsXzqObrZM9Va.htm](feats/class-04-wBqQsXzqObrZM9Va.htm)|Basic Bloodline Spell|Conjuro de linaje básico|modificada|
|[class-04-wbS8f7R7KqHkwOzM.htm](feats/class-04-wbS8f7R7KqHkwOzM.htm)|Sacral Lord|Señor Sacro|modificada|
|[class-04-WHOCaVobY7N3UTtA.htm](feats/class-04-WHOCaVobY7N3UTtA.htm)|Divine Evolution|Evolución divina|modificada|
|[class-04-WPHq7MWHlGWpTnme.htm](feats/class-04-WPHq7MWHlGWpTnme.htm)|Strange Script|Strange Script|modificada|
|[class-04-wqrOVv9gnqF4nlLR.htm](feats/class-04-wqrOVv9gnqF4nlLR.htm)|Surprise Snare|Sorpresa Snare|modificada|
|[class-04-wscmghwNCXvZtKsz.htm](feats/class-04-wscmghwNCXvZtKsz.htm)|Rescuer's Press|Aprovechar del Rescatador|modificada|
|[class-04-wz2edbLFnDKDNWWZ.htm](feats/class-04-wz2edbLFnDKDNWWZ.htm)|Flurry of Maneuvers|Ráfaga de maniobras|modificada|
|[class-04-x62kd1CYJNqO2ZsS.htm](feats/class-04-x62kd1CYJNqO2ZsS.htm)|Magic Warrior Transformation|Transformación de guerrero mágico|modificada|
|[class-04-Xb8CyW9sYS27ElcC.htm](feats/class-04-Xb8CyW9sYS27ElcC.htm)|Lucky Escape|Huir con suerte|modificada|
|[class-04-XgMV11C8W72SX9Yg.htm](feats/class-04-XgMV11C8W72SX9Yg.htm)|Patch Job|Patch Job|modificada|
|[class-04-XGZUjc9I3sjfniDg.htm](feats/class-04-XGZUjc9I3sjfniDg.htm)|Dual Energy Heart|Corazón de Energía Dual|modificada|
|[class-04-Xjn4iYzJ6rLIpLV3.htm](feats/class-04-Xjn4iYzJ6rLIpLV3.htm)|Barrier Shield|Escudo de barrera|modificada|
|[class-04-xKKyS9nEQqolJ0SF.htm](feats/class-04-xKKyS9nEQqolJ0SF.htm)|Psychic Duelist Dedication|Dedicación de Duelista Psíquico|modificada|
|[class-04-XkYNCFdZMjZTw6nn.htm](feats/class-04-XkYNCFdZMjZTw6nn.htm)|Giant Hunter|Cazador de Gigantes|modificada|
|[class-04-xlparPCGhkgjdhx2.htm](feats/class-04-xlparPCGhkgjdhx2.htm)|Defend Summoner|Defender Invocador|modificada|
|[class-04-xMTC8UOerwGyCtaN.htm](feats/class-04-xMTC8UOerwGyCtaN.htm)|Basic Trickery|Artimas básicas|modificada|
|[class-04-xSYkYRKlSyLzPrH1.htm](feats/class-04-xSYkYRKlSyLzPrH1.htm)|Undying Conviction|Convicción imperecedera|modificada|
|[class-04-xtRcWpprFBiXeCOB.htm](feats/class-04-xtRcWpprFBiXeCOB.htm)|Triangulate|Triangular|modificada|
|[class-04-xUaEpnfd1FMGNG1z.htm](feats/class-04-xUaEpnfd1FMGNG1z.htm)|Frightful Moan|Frightful Moan|modificada|
|[class-04-XWtNGOkMHcrdrRw8.htm](feats/class-04-XWtNGOkMHcrdrRw8.htm)|Dual-Form Weapon|Arma de doble forma|modificada|
|[class-04-xXHoyccK5ZG2AJKg.htm](feats/class-04-xXHoyccK5ZG2AJKg.htm)|Skilled Partner|Compañero hábil|modificada|
|[class-04-xxvKWQa2olCHoJ03.htm](feats/class-04-xxvKWQa2olCHoJ03.htm)|Basic Skysage Divination|Adivinación craneal b_sica|modificada|
|[class-04-xYakFeP6olBsxpZN.htm](feats/class-04-xYakFeP6olBsxpZN.htm)|Command Undead|Comandar muertos vivientes|modificada|
|[class-04-Y4gYfrxjSir2Enui.htm](feats/class-04-Y4gYfrxjSir2Enui.htm)|Perfect Strike|Golpe Perfecto|modificada|
|[class-04-yantkiDNmsT5ONZX.htm](feats/class-04-yantkiDNmsT5ONZX.htm)|Mental Forge|Forja Mental|modificada|
|[class-04-YehcIyxY5KnhPlx5.htm](feats/class-04-YehcIyxY5KnhPlx5.htm)|Basic Thaumaturgy|Taumaturgia Básica|modificada|
|[class-04-YhclkX1nfyUU8RtO.htm](feats/class-04-YhclkX1nfyUU8RtO.htm)|Remember Your Training|Recuerda tu entrenamiento|modificada|
|[class-04-yMj9WfPctWQC7be2.htm](feats/class-04-yMj9WfPctWQC7be2.htm)|Thaumaturgic Ritualist|Ritualista Taumatúrgico|modificada|
|[class-04-yozSCfdLFHVBbTxj.htm](feats/class-04-yozSCfdLFHVBbTxj.htm)|Mature Animal Companion (Druid)|Comparo animal maduro (Druida).|modificada|
|[class-04-yqMBLt0GUTwiqnZT.htm](feats/class-04-yqMBLt0GUTwiqnZT.htm)|Cathartic Focus Spell|Conjuro de foco catártico|modificada|
|[class-04-yUZXOEGqqXKhkGNE.htm](feats/class-04-yUZXOEGqqXKhkGNE.htm)|Advanced Reanimated Companion|Compañero Reanimado Avanzado|modificada|
|[class-04-YYTZFBT2WZMU14om.htm](feats/class-04-YYTZFBT2WZMU14om.htm)|Lifelink Surge|Oleaje Lifelink|modificada|
|[class-04-z3ycxqT1XvjfQ0Oq.htm](feats/class-04-z3ycxqT1XvjfQ0Oq.htm)|Take the Wheel|Toma la Rueda|modificada|
|[class-04-zBLrZE5aCkpaTK2N.htm](feats/class-04-zBLrZE5aCkpaTK2N.htm)|Additional Companion|Compañero adicional|modificada|
|[class-04-zbnL5OP4zVaNFcq8.htm](feats/class-04-zbnL5OP4zVaNFcq8.htm)|Snap Out Of It! (Marshal)|¡Despierta! (Marshal)|modificada|
|[class-04-Ze9vb0wlRmWWqnXC.htm](feats/class-04-Ze9vb0wlRmWWqnXC.htm)|Jalmeri Heavenseeker Dedication|Jalmeri Heavenseeker Dedicación|modificada|
|[class-04-zfTmb78yGZzNpgU3.htm](feats/class-04-zfTmb78yGZzNpgU3.htm)|Dual Thrower|Dual Thrower|modificada|
|[class-04-zgljg4gVI6i1Fpb5.htm](feats/class-04-zgljg4gVI6i1Fpb5.htm)|Paired Shots|Disparos emparejados|modificada|
|[class-04-zilwynzk8lIujgZo.htm](feats/class-04-zilwynzk8lIujgZo.htm)|Mercy|Merced|modificada|
|[class-04-zWEu9xuAxBnPoSrv.htm](feats/class-04-zWEu9xuAxBnPoSrv.htm)|Disturbing Defense|Defensa Perturbadora|modificada|
|[class-06-00uTUJPgJ6kuGR8O.htm](feats/class-06-00uTUJPgJ6kuGR8O.htm)|Crown of the Saumen Kar|Corona del Saumen Kar|modificada|
|[class-06-03mVGvudDLyGEpTZ.htm](feats/class-06-03mVGvudDLyGEpTZ.htm)|Spirit's Absolution|Spirit's Absolution|modificada|
|[class-06-04RgXKFVC2A6Ryn6.htm](feats/class-06-04RgXKFVC2A6Ryn6.htm)|Surprise Strike|Golpe Sorpresa|modificada|
|[class-06-0Gao1ez4dGH6dIZ2.htm](feats/class-06-0Gao1ez4dGH6dIZ2.htm)|Expeditious Advance|Avance Expeditivo|modificada|
|[class-06-0idzh4dww7B7cbnW.htm](feats/class-06-0idzh4dww7B7cbnW.htm)|Reloading Trick|Truco para recargar|modificada|
|[class-06-0SbHdwYumvmzwWw3.htm](feats/class-06-0SbHdwYumvmzwWw3.htm)|Piston Punch|Golpe de Pistón|modificada|
|[class-06-0UdHPOv3DX8TY9yb.htm](feats/class-06-0UdHPOv3DX8TY9yb.htm)|Snap Shot|Disparo instantáneo|modificada|
|[class-06-10DbphslCihq8mxQ.htm](feats/class-06-10DbphslCihq8mxQ.htm)|Ki Blast|Explosión de ki|modificada|
|[class-06-18UQefmhcNq6tFav.htm](feats/class-06-18UQefmhcNq6tFav.htm)|Rapid Manifestation|Manifestación Rápida|modificada|
|[class-06-1p5ErCp33nGOzEsk.htm](feats/class-06-1p5ErCp33nGOzEsk.htm)|Disarming Stance|Posición desarmante|modificada|
|[class-06-1YFrl8I6ZGo7BIM9.htm](feats/class-06-1YFrl8I6ZGo7BIM9.htm)|Knight Vigilant Dedication|Caballero Dedicación Vigilante|modificada|
|[class-06-21YWBdoXGmj60vdI.htm](feats/class-06-21YWBdoXGmj60vdI.htm)|Harmonize|Armonizar|modificada|
|[class-06-2nSk6oOLBXCEbAhc.htm](feats/class-06-2nSk6oOLBXCEbAhc.htm)|Vacate Vision|Visión Vacía|modificada|
|[class-06-37CUdnWwJCfOCC2H.htm](feats/class-06-37CUdnWwJCfOCC2H.htm)|Thunder Clap|Tronante Clap|modificada|
|[class-06-3uavnVbCsqTvzpgt.htm](feats/class-06-3uavnVbCsqTvzpgt.htm)|Dragon's Rage Breath|Aliento de furia de dragón|modificada|
|[class-06-3vjOXL9ZD4ibaJL6.htm](feats/class-06-3vjOXL9ZD4ibaJL6.htm)|Green Empathy|Empatía vegetal|modificada|
|[class-06-4I1Kq53Qfzrrmg2E.htm](feats/class-06-4I1Kq53Qfzrrmg2E.htm)|Skirmish Strike|Golpe hostigador|modificada|
|[class-06-4o9g5g12yyrfZ3Xd.htm](feats/class-06-4o9g5g12yyrfZ3Xd.htm)|Light Step|Pisada ligera|modificada|
|[class-06-4T9HHOdTk3yVbeoO.htm](feats/class-06-4T9HHOdTk3yVbeoO.htm)|Instinct Ability|Aptitud de instinto|modificada|
|[class-06-52cygjzHfSD0YhEA.htm](feats/class-06-52cygjzHfSD0YhEA.htm)|Spell Penetration|Conjuros perforantes|modificada|
|[class-06-55wEtw47Zl11uqlr.htm](feats/class-06-55wEtw47Zl11uqlr.htm)|Numb|Numb|modificada|
|[class-06-588O3jurogttvqgm.htm](feats/class-06-588O3jurogttvqgm.htm)|Eidolon's Opportunity|Oportunidad del eidolon|modificada|
|[class-06-5Jc2ySGLVi053qpz.htm](feats/class-06-5Jc2ySGLVi053qpz.htm)|Twist the Knife|Retorcer el cuchillo|modificada|
|[class-06-5vXc2s2siR1ihpBT.htm](feats/class-06-5vXc2s2siR1ihpBT.htm)|Shield Warden (Fighter)|Guardián escudo (luchador).|modificada|
|[class-06-62hpJOuvYYSa4X7u.htm](feats/class-06-62hpJOuvYYSa4X7u.htm)|Hellknight Signifer Dedication|Dedicación de Caballero Infernal Signifer|modificada|
|[class-06-64DgI5CHP7K9kbbg.htm](feats/class-06-64DgI5CHP7K9kbbg.htm)|Practiced Opposition|Oposición practicada|modificada|
|[class-06-65gjc5KE4ZRoocbi.htm](feats/class-06-65gjc5KE4ZRoocbi.htm)|Abundant Step|Paso abundante|modificada|
|[class-06-6gLWr3xghsHSFwxc.htm](feats/class-06-6gLWr3xghsHSFwxc.htm)|Water Step|Paso acuático|modificada|
|[class-06-6J2ZSGNsXPKPcJGV.htm](feats/class-06-6J2ZSGNsXPKPcJGV.htm)|Tiger Slash|Zarpazo del tigre|modificada|
|[class-06-6vpgAMj87J6cWN0j.htm](feats/class-06-6vpgAMj87J6cWN0j.htm)|Drenching Mist|Sofocar Niebla|modificada|
|[class-06-74PGpEAVNS5xUDA3.htm](feats/class-06-74PGpEAVNS5xUDA3.htm)|Clinch Strike|Clinch Golpe|modificada|
|[class-06-7fU6e3HIT4NvwLYa.htm](feats/class-06-7fU6e3HIT4NvwLYa.htm)|Halcyon Speaker Dedication|Halcyon Speaker Dedication|modificada|
|[class-06-7GrACprIxZuarGDs.htm](feats/class-06-7GrACprIxZuarGDs.htm)|Grave Strength|Grave Strength|modificada|
|[class-06-7JjNWSuutkjrMrd0.htm](feats/class-06-7JjNWSuutkjrMrd0.htm)|Advanced Devotion|Devoción avanzada|modificada|
|[class-06-7NdMHszAiiveihoW.htm](feats/class-06-7NdMHszAiiveihoW.htm)|Armor Specialist|Especialista en armaduras|modificada|
|[class-06-7O0PrMoXd5L8dRfg.htm](feats/class-06-7O0PrMoXd5L8dRfg.htm)|Archaeologist's Warning|Advertencia del arqueólogo|modificada|
|[class-06-7wk6yVM3OJdc4LEU.htm](feats/class-06-7wk6yVM3OJdc4LEU.htm)|Champion's Reaction|Reacción del campeón|modificada|
|[class-06-8rKzUpDxAi8tMk7I.htm](feats/class-06-8rKzUpDxAi8tMk7I.htm)|Liberate Soul|Liberate Soul|modificada|
|[class-06-8RppI1i4LfI0CYsX.htm](feats/class-06-8RppI1i4LfI0CYsX.htm)|Aldori Riposte|Aldori Riposte|modificada|
|[class-06-8x3wqCZgYzJiSyR1.htm](feats/class-06-8x3wqCZgYzJiSyR1.htm)|Dazzling Bullet|Bala Deslumbrante|modificada|
|[class-06-9C6a6FXuPqWjXy8K.htm](feats/class-06-9C6a6FXuPqWjXy8K.htm)|Improved Familiar (Familiar Master)|Familiar Mejorado (Maestro Familiar)|modificada|
|[class-06-9DWxzEavOeymc6Ql.htm](feats/class-06-9DWxzEavOeymc6Ql.htm)|Spiritual Strike|Golpe Espiritual|modificada|
|[class-06-9E1FLGp4CNBEwiZE.htm](feats/class-06-9E1FLGp4CNBEwiZE.htm)|Relentless Disarm|Desarmar Implacable|modificada|
|[class-06-9p4oFIn791VAmzUn.htm](feats/class-06-9p4oFIn791VAmzUn.htm)|Cauterize|Cauterizar|modificada|
|[class-06-a9zzu4kb7vstq0HQ.htm](feats/class-06-a9zzu4kb7vstq0HQ.htm)|Cadence Call|Llamada de Cadencia|modificada|
|[class-06-adr9buwVuxgZV2B3.htm](feats/class-06-adr9buwVuxgZV2B3.htm)|Share Burden|Compartir Carga|modificada|
|[class-06-aQjWHbjK1pk8HESM.htm](feats/class-06-aQjWHbjK1pk8HESM.htm)|Perfect Ki Adept|Perfecto Ki Adepto|modificada|
|[class-06-AWd7uZBfba5mNXeT.htm](feats/class-06-AWd7uZBfba5mNXeT.htm)|Syu Tak-nwa's Deadly Hair|Cabello letal de Syu Tak-nwa|modificada|
|[class-06-axGS5bBJ9vl5AePc.htm](feats/class-06-axGS5bBJ9vl5AePc.htm)|Phase Out|Phase Out|modificada|
|[class-06-axvGLVoAvuD9jU78.htm](feats/class-06-axvGLVoAvuD9jU78.htm)|Loyal Warhorse|Caballo de guerra leal|modificada|
|[class-06-b532qrlTUqWxLd1j.htm](feats/class-06-b532qrlTUqWxLd1j.htm)|Shield Warden (Champion)|Guardián escudo (campeón).|modificada|
|[class-06-bi9w4Z9MQY8VWLZF.htm](feats/class-06-bi9w4Z9MQY8VWLZF.htm)|Animate Net|Red Animada|modificada|
|[class-06-BkEOwv3SRtefczpO.htm](feats/class-06-BkEOwv3SRtefczpO.htm)|Phalanx Breaker|Rompedor de falanges|modificada|
|[class-06-bmWvMfYxZbZtigDp.htm](feats/class-06-bmWvMfYxZbZtigDp.htm)|Flexible Ritualist|Ritualista Flexible|modificada|
|[class-06-BQzExsEZrwGsJD66.htm](feats/class-06-BQzExsEZrwGsJD66.htm)|Endemic Herbs|Hierbas Endémicas|modificada|
|[class-06-bTQVvkEuPj9QAiAi.htm](feats/class-06-bTQVvkEuPj9QAiAi.htm)|Time Mage Dedication|Dedicación de mago del tiempo|modificada|
|[class-06-bVng7Mkrj4UnQzLo.htm](feats/class-06-bVng7Mkrj4UnQzLo.htm)|Burning Spell|Hechizo Ardiente|modificada|
|[class-06-BxO5l9tH9y1xNzzi.htm](feats/class-06-BxO5l9tH9y1xNzzi.htm)|Advanced Thoughtform|Forma de pensamiento avanzada|modificada|
|[class-06-bzKBUK6CH8tuLCfo.htm](feats/class-06-bzKBUK6CH8tuLCfo.htm)|Advanced Red Mantis Magic|Magia avanzada de la mantis roja|modificada|
|[class-06-c5ns35FLvvxjimuH.htm](feats/class-06-c5ns35FLvvxjimuH.htm)|Swift Tracker|Rastreador rápido|modificada|
|[class-06-c5SfaSn6OEHkHxII.htm](feats/class-06-c5SfaSn6OEHkHxII.htm)|Feverish Enzymes|Enzimas Febriles|modificada|
|[class-06-cBYTVYqw1EFVEuzs.htm](feats/class-06-cBYTVYqw1EFVEuzs.htm)|Bolera's Interrogation|Interrogatorio de Bolera|modificada|
|[class-06-Ckglzh4dXcGWPNS3.htm](feats/class-06-Ckglzh4dXcGWPNS3.htm)|Beastmaster's Trance|Beastmaster's Trance|modificada|
|[class-06-ckukkEJj4Lc3ENjr.htm](feats/class-06-ckukkEJj4Lc3ENjr.htm)|Tap Vitality|Tap Vitality|modificada|
|[class-06-CmA8t7MRMOzLTeUj.htm](feats/class-06-CmA8t7MRMOzLTeUj.htm)|Advanced Breakthrough|Avance Avanzado|modificada|
|[class-06-cny7ouhsoiNsWJ7X.htm](feats/class-06-cny7ouhsoiNsWJ7X.htm)|Daring Act|Acto Atrevido|modificada|
|[class-06-CoVBrlyMToANvt2v.htm](feats/class-06-CoVBrlyMToANvt2v.htm)|Stone Blood|Sangre de piedra|modificada|
|[class-06-cTQMtd2IVlvgJwAn.htm](feats/class-06-cTQMtd2IVlvgJwAn.htm)|Slice and Swipe|Slice and Vaivén|modificada|
|[class-06-CvMCw6JqvgMPE5uk.htm](feats/class-06-CvMCw6JqvgMPE5uk.htm)|Blood Frenzy|Frenesí de Sangre|modificada|
|[class-06-d00Ip4YHVmk2tecD.htm](feats/class-06-d00Ip4YHVmk2tecD.htm)|Sympathetic Vulnerabilities|Vulnerabilidades Simpáticas|modificada|
|[class-06-dCCVqIcYrhCp3Bzl.htm](feats/class-06-dCCVqIcYrhCp3Bzl.htm)|Sound Mirror|Sound Mirror|modificada|
|[class-06-dDFQJem5K9Jzxgda.htm](feats/class-06-dDFQJem5K9Jzxgda.htm)|Expert Fireworks Crafter|Experto en fuegos artificiales|modificada|
|[class-06-df4cBV3qZn3qNUmP.htm](feats/class-06-df4cBV3qZn3qNUmP.htm)|Rallying Charge|Rallying Charge|modificada|
|[class-06-dHUoQVzDa9Cf4QCG.htm](feats/class-06-dHUoQVzDa9Cf4QCG.htm)|Reflexive Shield|Escudo reflectante|modificada|
|[class-06-dMKx0T629hpJCN8T.htm](feats/class-06-dMKx0T629hpJCN8T.htm)|Sixth Sense|Sexto Sentido|modificada|
|[class-06-DPk7a0cdFOjDOdn5.htm](feats/class-06-DPk7a0cdFOjDOdn5.htm)|Advanced Elemental Spell|Hechizo elemental avanzado|modificada|
|[class-06-DpmdmRNMg6LZpNB0.htm](feats/class-06-DpmdmRNMg6LZpNB0.htm)|Simple Crystal Magic|Simple Magia de Cristal|modificada|
|[class-06-dPmJ91qawZW2U8K3.htm](feats/class-06-dPmJ91qawZW2U8K3.htm)|Staff Sweep|Barrido de Bastón|modificada|
|[class-06-DR6vessIpXTLM6Xa.htm](feats/class-06-DR6vessIpXTLM6Xa.htm)|Crowd Mastery|Dominio de la multitud|modificada|
|[class-06-dVzPTpZoGSi5NR6y.htm](feats/class-06-dVzPTpZoGSi5NR6y.htm)|Cast Out|Cast Out|modificada|
|[class-06-dwL0Y0P2x4pn2cft.htm](feats/class-06-dwL0Y0P2x4pn2cft.htm)|Bellflower Dedication|Dedicación Bellflower|modificada|
|[class-06-dxUbA1dUEcVHnU5s.htm](feats/class-06-dxUbA1dUEcVHnU5s.htm)|Clockwork Celerity|Clockwork Celerity|modificada|
|[class-06-E5ewlRE6Mh9ZqUMu.htm](feats/class-06-E5ewlRE6Mh9ZqUMu.htm)|Ostentatious Arrival|Llegada ostentosa|modificada|
|[class-06-EBmZyzDWhFSLydlM.htm](feats/class-06-EBmZyzDWhFSLydlM.htm)|Storming Breath|Aliento de tormenta|modificada|
|[class-06-ef57tIj30IaPnSgC.htm](feats/class-06-ef57tIj30IaPnSgC.htm)|Spiral Sworn|Espiral Jurada|modificada|
|[class-06-eHkMcQQ4ejRAFJAt.htm](feats/class-06-eHkMcQQ4ejRAFJAt.htm)|Poison Coat|Poison Coat|modificada|
|[class-06-eHvJqBHYqx7UjpPg.htm](feats/class-06-eHvJqBHYqx7UjpPg.htm)|Perpetual Scout|Explorador perpetuo|modificada|
|[class-06-Ek1CoyGKxsozDsaD.htm](feats/class-06-Ek1CoyGKxsozDsaD.htm)|Guide the Timeline|Guía de la línea de tiempo|modificada|
|[class-06-elbj75qsUerbM725.htm](feats/class-06-elbj75qsUerbM725.htm)|Scout's Speed|Velocidad de Explorador|modificada|
|[class-06-em3glccrJ5ZIf8Uq.htm](feats/class-06-em3glccrJ5ZIf8Uq.htm)|Keep Pace (Game Hunter)|Mantén el ritmo (Cazador)|modificada|
|[class-06-eNeSl5UNaqDwyNkp.htm](feats/class-06-eNeSl5UNaqDwyNkp.htm)|Eidolon's Wrath|Ira de Eidolon|modificada|
|[class-06-eTqWgfojpvuigdvx.htm](feats/class-06-eTqWgfojpvuigdvx.htm)|Advanced Muse's Whispers|Susurros de la musa avanzados|modificada|
|[class-06-euWpgjPNcDjeXAWQ.htm](feats/class-06-euWpgjPNcDjeXAWQ.htm)|Advanced Maneuver|Maniobra avanzada|modificada|
|[class-06-eXNkcM7gtCGC7udi.htm](feats/class-06-eXNkcM7gtCGC7udi.htm)|Grave Sense|Grave Sense|modificada|
|[class-06-exVjUkpAdbzJsxxg.htm](feats/class-06-exVjUkpAdbzJsxxg.htm)|Tandem Strike|Golpe en Tándem|modificada|
|[class-06-eZrftEihfuJBldG5.htm](feats/class-06-eZrftEihfuJBldG5.htm)|Advanced Bloodline|Linaje avanzado|modificada|
|[class-06-F1DVDJRARfdb1Kjz.htm](feats/class-06-F1DVDJRARfdb1Kjz.htm)|Storm Retribution|Tormenta de represalia|modificada|
|[class-06-f754txt1ZyhVWXHk.htm](feats/class-06-f754txt1ZyhVWXHk.htm)|Advanced Hunter's Trick|Truco del cazador avanzado|modificada|
|[class-06-FGdKV40UiS6jvBmI.htm](feats/class-06-FGdKV40UiS6jvBmI.htm)|Exploit Opening|Exploit Apertura|modificada|
|[class-06-FIVuc2TRaLXiCGkn.htm](feats/class-06-FIVuc2TRaLXiCGkn.htm)|Advanced Thaumaturgy|Taumaturgia Avanzada|modificada|
|[class-06-FJdcQU6yjDVDBC4r.htm](feats/class-06-FJdcQU6yjDVDBC4r.htm)|Tempest-Sun Redirection|Tempestad-Sol Redirección|modificada|
|[class-06-FkOtiB52wIOi7SP7.htm](feats/class-06-FkOtiB52wIOi7SP7.htm)|Adaptive Mask Familiar|Adaptive Mask Familiar|modificada|
|[class-06-fngPLUD4Sltho2kn.htm](feats/class-06-fngPLUD4Sltho2kn.htm)|Staggering Fire|Fuego Asombroso|modificada|
|[class-06-FNO2hfGmxqJngD4A.htm](feats/class-06-FNO2hfGmxqJngD4A.htm)|Drifter's Juke|Amago del vagabundo|modificada|
|[class-06-Fpsd8Y8qJ3Tdbiyz.htm](feats/class-06-Fpsd8Y8qJ3Tdbiyz.htm)|Brains!|¡Cerebros!|modificada|
|[class-06-FqrfyUtoBWJNnSi6.htm](feats/class-06-FqrfyUtoBWJNnSi6.htm)|Pain Tolerance|Tolerancia al dolor|modificada|
|[class-06-fSNgVtt8y5uTCYvf.htm](feats/class-06-fSNgVtt8y5uTCYvf.htm)|Advanced Martial Magic|Magia Marcial Avanzada|modificada|
|[class-06-g8ZMeg1YFg9WZj3I.htm](feats/class-06-g8ZMeg1YFg9WZj3I.htm)|Second Shield|Segundo Escudo|modificada|
|[class-06-GE96a0UGPYM74qjI.htm](feats/class-06-GE96a0UGPYM74qjI.htm)|Cleave|Hendedura|modificada|
|[class-06-goFxIDlbWd8GN0kj.htm](feats/class-06-goFxIDlbWd8GN0kj.htm)|Clear The Way|Clear The Way|modificada|
|[class-06-gOHBzx5Rqa6TZcrm.htm](feats/class-06-gOHBzx5Rqa6TZcrm.htm)|Mesmerizing Gaze|Mesmerizing Gaze|modificada|
|[class-06-grPtqWYbdXXo7yhP.htm](feats/class-06-grPtqWYbdXXo7yhP.htm)|Fey Tracker|Rastreador feérico|modificada|
|[class-06-gv7PJVrnODu3qYB0.htm](feats/class-06-gv7PJVrnODu3qYB0.htm)|Shielded Tome|Tomo Escudo|modificada|
|[class-06-GvQ9FQ02i7GYuRRh.htm](feats/class-06-GvQ9FQ02i7GYuRRh.htm)|Pact of Draconic Fury|Pacto de furia dracónico|modificada|
|[class-06-H86VNC6cNWhBI8Ed.htm](feats/class-06-H86VNC6cNWhBI8Ed.htm)|Knight Reclaimant Dedication|Caballero Reclamador Dedicación|modificada|
|[class-06-HBZSP3ABqmsV9FXH.htm](feats/class-06-HBZSP3ABqmsV9FXH.htm)|Advanced Shooter|Advanced Shooter|modificada|
|[class-06-hCEoINqJ75ConqqA.htm](feats/class-06-hCEoINqJ75ConqqA.htm)|Mammoth Charge|Carga de Mamut|modificada|
|[class-06-HCoDbriaPwnurcNP.htm](feats/class-06-HCoDbriaPwnurcNP.htm)|Gear Up|Gear Up|modificada|
|[class-06-hE6fchGuHuPIeKlO.htm](feats/class-06-hE6fchGuHuPIeKlO.htm)|Fleeting Shadow|Sombra fugaz|modificada|
|[class-06-hiABcPXvcYa9QccF.htm](feats/class-06-hiABcPXvcYa9QccF.htm)|Death Warden|Guardián de la Muerte|modificada|
|[class-06-hjApw8AvYVuqQk2W.htm](feats/class-06-hjApw8AvYVuqQk2W.htm)|Spirits' Interference|Interferencia de Espíritus|modificada|
|[class-06-hJlLmjW0NNeV1Ous.htm](feats/class-06-hJlLmjW0NNeV1Ous.htm)|Cascade Countermeasure|Contramedidas en cascada|modificada|
|[class-06-HkbP5zbN2meRiP7w.htm](feats/class-06-HkbP5zbN2meRiP7w.htm)|Psi Development|Desarrollo Psi|modificada|
|[class-06-hkdP5tsTAoqJDR8v.htm](feats/class-06-hkdP5tsTAoqJDR8v.htm)|Triple Shot|Disparo triple|modificada|
|[class-06-hlX7jYoS1s6srZC2.htm](feats/class-06-hlX7jYoS1s6srZC2.htm)|Megavolt|Megavoltio|modificada|
|[class-06-HtH8MONAzx4eYuJY.htm](feats/class-06-HtH8MONAzx4eYuJY.htm)|Advanced Concoction|Brebaje avanzado|modificada|
|[class-06-i5LtFOpsUR5S74pC.htm](feats/class-06-i5LtFOpsUR5S74pC.htm)|Butterfly's Sting|Picadura de Mariposa|modificada|
|[class-06-IeAbL6fkRsd1hL6r.htm](feats/class-06-IeAbL6fkRsd1hL6r.htm)|Repulse the Wicked|Repulsión de los malvados|modificada|
|[class-06-IEbnal1VJySrhxFR.htm](feats/class-06-IEbnal1VJySrhxFR.htm)|Stella's Stab and Snag|Puñalada y enganche de Stella|modificada|
|[class-06-iJrHJKNGxV4z4Qi7.htm](feats/class-06-iJrHJKNGxV4z4Qi7.htm)|Artokus's Fire|Fuego de Artokus|modificada|
|[class-06-J06CgMzMDGOahXjf.htm](feats/class-06-J06CgMzMDGOahXjf.htm)|Uneasy Rest|Descanso Inquietante|modificada|
|[class-06-JdCRxwgtdQkJ1Ha6.htm](feats/class-06-JdCRxwgtdQkJ1Ha6.htm)|Guardian's Deflection (Fighter)|Desvío del guardián (Luchador)|modificada|
|[class-06-JHcvySfCM9uYNb9N.htm](feats/class-06-JHcvySfCM9uYNb9N.htm)|Revealing Stab|Pulada reveladora|modificada|
|[class-06-jRJqKkm6NnHcL8HO.htm](feats/class-06-jRJqKkm6NnHcL8HO.htm)|Rain-Scribe Sustenance|Escriba de la lluvia Sustento|modificada|
|[class-06-jSkJIWPfSZZzvYzq.htm](feats/class-06-jSkJIWPfSZZzvYzq.htm)|Combine Elixirs|Combinar elixires|modificada|
|[class-06-JxSxCTJOOayIdO4B.htm](feats/class-06-JxSxCTJOOayIdO4B.htm)|Slinger's Readiness|Disposición del hondero|modificada|
|[class-06-K5ZONljq5XzS8MQc.htm](feats/class-06-K5ZONljq5XzS8MQc.htm)|Detonating Spell|Detonating Spell|modificada|
|[class-06-K9SpdinLWc7YRVHP.htm](feats/class-06-K9SpdinLWc7YRVHP.htm)|Captivating Intensity|Intensidad Cautivadora|modificada|
|[class-06-KgD26HpSrKyciB8f.htm](feats/class-06-KgD26HpSrKyciB8f.htm)|Parallel Breakthrough|Avance paralelo|modificada|
|[class-06-KW6K5Zv4J7ClWkKA.htm](feats/class-06-KW6K5Zv4J7ClWkKA.htm)|Quick Snares|Trampas de lazo rápidas|modificada|
|[class-06-KWZqHwI82ae8fMML.htm](feats/class-06-KWZqHwI82ae8fMML.htm)|Grave Mummification|Momificación de tumbas|modificada|
|[class-06-L1gD3VMD5X9JNJzE.htm](feats/class-06-L1gD3VMD5X9JNJzE.htm)|Beast Gunner Dedication|Dedicación Artillero Bestia|modificada|
|[class-06-l60Ua5Ugv85GnF9b.htm](feats/class-06-l60Ua5Ugv85GnF9b.htm)|Nameless Anonymity|Anonimato sin nombre|modificada|
|[class-06-l9K62T7qMCvJXUoY.htm](feats/class-06-l9K62T7qMCvJXUoY.htm)|Advanced Wilding|Salvajismo avanzado|modificada|
|[class-06-l9PYldtyPr7Q8Xow.htm](feats/class-06-l9PYldtyPr7Q8Xow.htm)|Daywalker (Vampire)|Caminante diurno (Vampiro)|modificada|
|[class-06-L9sRaFl0tHT5AFIQ.htm](feats/class-06-L9sRaFl0tHT5AFIQ.htm)|Scatter Blast|Scatter Blast|modificada|
|[class-06-lGCFVYjL9Lp5m9Ex.htm](feats/class-06-lGCFVYjL9Lp5m9Ex.htm)|Cast Down|Derribar|modificada|
|[class-06-LgpATqbuTIfB4o6G.htm](feats/class-06-LgpATqbuTIfB4o6G.htm)|No Hard Feelings|No Hard Feelings|modificada|
|[class-06-lh3STEvbGnP7jVMr.htm](feats/class-06-lh3STEvbGnP7jVMr.htm)|Munitions Machinist|Fabricante de máquinas de municiones.|modificada|
|[class-06-lix0Utu4g8mQ0ZtI.htm](feats/class-06-lix0Utu4g8mQ0ZtI.htm)|Divine Ally|Aliado divino|modificada|
|[class-06-lkl5QaDb1mlSD7SC.htm](feats/class-06-lkl5QaDb1mlSD7SC.htm)|Visual Fidelity|Fidelidad visual|modificada|
|[class-06-lknYlp0ekVyBWQK9.htm](feats/class-06-lknYlp0ekVyBWQK9.htm)|Discerning Gaze|Mirada perspicaz|modificada|
|[class-06-lqs4MIlO1N1YglOi.htm](feats/class-06-lqs4MIlO1N1YglOi.htm)|Advanced Synergy|Sinergia Avanzada|modificada|
|[class-06-Ls3MiZ5RcAWaiQ7f.htm](feats/class-06-Ls3MiZ5RcAWaiQ7f.htm)|Construct Shell|Construct Shell|modificada|
|[class-06-lt9bQDI7ZXPA7wPw.htm](feats/class-06-lt9bQDI7ZXPA7wPw.htm)|Advanced Fury|Furia avanzada|modificada|
|[class-06-m0ot9Qydb9SYWHis.htm](feats/class-06-m0ot9Qydb9SYWHis.htm)|Follow-up Strike|Golpe de seguimiento|modificada|
|[class-06-M2V1vqAziOkWV30B.htm](feats/class-06-M2V1vqAziOkWV30B.htm)|Crimson Shroud|Crimson Shroud|modificada|
|[class-06-MM9NcRyXWX2LFiuF.htm](feats/class-06-MM9NcRyXWX2LFiuF.htm)|Living Rune|Runa Viviente|modificada|
|[class-06-MOAThpfl92zO5p08.htm](feats/class-06-MOAThpfl92zO5p08.htm)|Sacred Armaments|Armamento Sagrado|modificada|
|[class-06-moXYfz806x6uXIW9.htm](feats/class-06-moXYfz806x6uXIW9.htm)|Field Artillery|Artillería de Campaña|modificada|
|[class-06-mrM07U3MyElcLEx4.htm](feats/class-06-mrM07U3MyElcLEx4.htm)|Implement Initiate|Implementar Iniciar|modificada|
|[class-06-MROG87PQmuBTdCaB.htm](feats/class-06-MROG87PQmuBTdCaB.htm)|Jelly Body|Jelly Body|modificada|
|[class-06-mwBb8MlAmpbYH9T4.htm](feats/class-06-mwBb8MlAmpbYH9T4.htm)|It's Alive!|¡Está vivo!|modificada|
|[class-06-n2hawNmzW7DBn1Lm.htm](feats/class-06-n2hawNmzW7DBn1Lm.htm)|Mountain Stronghold|Baluarte de la monta|modificada|
|[class-06-n3eOMWQd4kdR3A0l.htm](feats/class-06-n3eOMWQd4kdR3A0l.htm)|Mature Trained Companion|Compañero maduro entrenado|modificada|
|[class-06-nDjTJq7PEbvRktnb.htm](feats/class-06-nDjTJq7PEbvRktnb.htm)|Advanced Weapon Training|Entrenamiento en armas avanzadas|modificada|
|[class-06-NIaNTFlPwi2ng1rZ.htm](feats/class-06-NIaNTFlPwi2ng1rZ.htm)|Superior Propulsion|Propulsión Superior|modificada|
|[class-06-nluD4gFLWePrBK5f.htm](feats/class-06-nluD4gFLWePrBK5f.htm)|Explosion|Explosión|modificada|
|[class-06-Nm4yeoJVXRy0Wyth.htm](feats/class-06-Nm4yeoJVXRy0Wyth.htm)|Inertial Barrier|Barrera Inercial|modificada|
|[class-06-Nm8n3urzpDqXni1i.htm](feats/class-06-Nm8n3urzpDqXni1i.htm)|Disrupting Strikes|Golpes Disruptores|modificada|
|[class-06-NQTpuGhB7Rq9zJkt.htm](feats/class-06-NQTpuGhB7Rq9zJkt.htm)|Polearm Tricks|Polearm Tricks|modificada|
|[class-06-nuBY8Ek4JBFQJzoh.htm](feats/class-06-nuBY8Ek4JBFQJzoh.htm)|Mastermind's Eye|Mastermind's Eye|modificada|
|[class-06-O0POcPD2aELYTcIK.htm](feats/class-06-O0POcPD2aELYTcIK.htm)|Rough Terrain Stance|Posición de terreno abrupto.|modificada|
|[class-06-o3J79hnr00ztcwtT.htm](feats/class-06-o3J79hnr00ztcwtT.htm)|Fulminating Shot|Tiro Fulminante|modificada|
|[class-06-of3G33qoA8oJZ0Le.htm](feats/class-06-of3G33qoA8oJZ0Le.htm)|Infiltrate Dream|Infiltrar Sue|modificada|
|[class-06-oIfCpkpH0Jb1mzj9.htm](feats/class-06-oIfCpkpH0Jb1mzj9.htm)|Basic Magus Spellcasting|Basic Magus Spellcasting|modificada|
|[class-06-OjvE7gaQgWiBqOhY.htm](feats/class-06-OjvE7gaQgWiBqOhY.htm)|Discerning Strike|Golpe Discernidor|modificada|
|[class-06-oNh2vedij8xbDbph.htm](feats/class-06-oNh2vedij8xbDbph.htm)|Hellknight Dedication|Dedicación de Caballero Infernal|modificada|
|[class-06-opP9j7RP7JPyt8Zj.htm](feats/class-06-opP9j7RP7JPyt8Zj.htm)|Mature Megafauna Companion|Compañero Megafauna Maduro|modificada|
|[class-06-OqObuRB8oVSAEKFR.htm](feats/class-06-OqObuRB8oVSAEKFR.htm)|Scroll Esoterica|Pergamino Esotérico|modificada|
|[class-06-oUhwrijg4rClCplO.htm](feats/class-06-oUhwrijg4rClCplO.htm)|Brutal Bully|Matón brutal|modificada|
|[class-06-OWedlrKGsVZVkSnT.htm](feats/class-06-OWedlrKGsVZVkSnT.htm)|Insect Shape|Forma de insecto|modificada|
|[class-06-owJorCBZmUi5lIV0.htm](feats/class-06-owJorCBZmUi5lIV0.htm)|Expert Herbalism|Experto Herboristería|modificada|
|[class-06-oyLkqhDGwGGj40ME.htm](feats/class-06-oyLkqhDGwGGj40ME.htm)|Eldritch Archer Dedication|Dedicación de Arquero Eldritch|modificada|
|[class-06-Ozm0xy2lrOq6GiWU.htm](feats/class-06-Ozm0xy2lrOq6GiWU.htm)|Ghostly Grasp|Ghostly Grasp|modificada|
|[class-06-OzvvsyjAWWij4mmm.htm](feats/class-06-OzvvsyjAWWij4mmm.htm)|Keep Pace (Bounty Hunter)|Mantén el ritmo (Cazador de recompensas)|modificada|
|[class-06-ozvYhY4hG1deXly8.htm](feats/class-06-ozvYhY4hG1deXly8.htm)|Directional Bombs|Bombas direccionales|modificada|
|[class-06-p6j4nLPRwksjfwPW.htm](feats/class-06-p6j4nLPRwksjfwPW.htm)|Snow Step|Snow Step|modificada|
|[class-06-PdQAAKtCW5dS9IYj.htm](feats/class-06-PdQAAKtCW5dS9IYj.htm)|Inner Fire (Monk)|Fuego interior (Monje)|modificada|
|[class-06-pewPAMlURmTqBqJx.htm](feats/class-06-pewPAMlURmTqBqJx.htm)|Axe Thrower|Lanzador de hachas|modificada|
|[class-06-PiBXXCeDNQGfQVoJ.htm](feats/class-06-PiBXXCeDNQGfQVoJ.htm)|Divine Emissary|Emisario divino|modificada|
|[class-06-PjsIiVM7yI0XgaFi.htm](feats/class-06-PjsIiVM7yI0XgaFi.htm)|Deathly Secrets|Secretos Mortales|modificada|
|[class-06-pkH4DPmMWcimMov7.htm](feats/class-06-pkH4DPmMWcimMov7.htm)|Westyr's Wayfinder Repository|Repositorio Orientador de Westyr|modificada|
|[class-06-pM0g4ColXTiQ3gTa.htm](feats/class-06-pM0g4ColXTiQ3gTa.htm)|Ghost Strike|Golpe fantasmal|modificada|
|[class-06-pph43ZrfvEnQjJXE.htm](feats/class-06-pph43ZrfvEnQjJXE.htm)|Cannon Corner Shot|Cañón Tiro de Esquina|modificada|
|[class-06-qg8TlLJRgvjzW9YK.htm](feats/class-06-qg8TlLJRgvjzW9YK.htm)|Startling Appearance (Vigilante)|Startling Appearance (Vigilante)|modificada|
|[class-06-qJdbK8vgIqeHU7bu.htm](feats/class-06-qJdbK8vgIqeHU7bu.htm)|Heaven's Thunder|Tronante del Cielo|modificada|
|[class-06-QpRzvfWdj6YH9TyE.htm](feats/class-06-QpRzvfWdj6YH9TyE.htm)|Shield Wall|Muro de escudos|modificada|
|[class-06-QUnN2wUfvOD6Tz69.htm](feats/class-06-QUnN2wUfvOD6Tz69.htm)|Defy Fey|Desafiar feérico|modificada|
|[class-06-RCjMbLyRnG70R7cO.htm](feats/class-06-RCjMbLyRnG70R7cO.htm)|Frighten Undead|Asustar muerto viviente|modificada|
|[class-06-reoylyGMDPl7H6L5.htm](feats/class-06-reoylyGMDPl7H6L5.htm)|Macabre Virtuoso|Macabro Virtuoso|modificada|
|[class-06-rFlBoYGI5OmfMvaO.htm](feats/class-06-rFlBoYGI5OmfMvaO.htm)|Scholarly Defense|Defensa erudita|modificada|
|[class-06-Rh3KSd7BUfV12GBT.htm](feats/class-06-Rh3KSd7BUfV12GBT.htm)|Swift Intervention|Celeridad Intervención|modificada|
|[class-06-RlKGaxQWWLa7xJSc.htm](feats/class-06-RlKGaxQWWLa7xJSc.htm)|Pirouette|Pirueta|modificada|
|[class-06-ROAUR1GhC19Pjk9C.htm](feats/class-06-ROAUR1GhC19Pjk9C.htm)|Basic Scroll Cache|Caché Básico de Pergaminos|modificada|
|[class-06-rS8uNb0C5GBHnKHH.htm](feats/class-06-rS8uNb0C5GBHnKHH.htm)|Viking Weapon Specialist|Especialista en armas vikingas|modificada|
|[class-06-rXY2fhyteYhaQnMl.htm](feats/class-06-rXY2fhyteYhaQnMl.htm)|Advanced Trickery|Artimas avanzadas|modificada|
|[class-06-S14S52HjszTgIy4l.htm](feats/class-06-S14S52HjszTgIy4l.htm)|Crane Flutter|Aleteo de la grulla|modificada|
|[class-06-S25iRw2X9hmGRMyO.htm](feats/class-06-S25iRw2X9hmGRMyO.htm)|Advanced Blood Potency|Potencia sanguínea avanzada|modificada|
|[class-06-SD3RlgQMVL6aWjtW.htm](feats/class-06-SD3RlgQMVL6aWjtW.htm)|Triggerbrand Salvo|Triggerbrand Salvo|modificada|
|[class-06-SELSj1vvVLx5cP72.htm](feats/class-06-SELSj1vvVLx5cP72.htm)|Dragon Roar|Rugido del dragón|modificada|
|[class-06-SHpVHkPxtQggD9Cf.htm](feats/class-06-SHpVHkPxtQggD9Cf.htm)|Swordmaster Dedication|Dedicación Swordmaster|modificada|
|[class-06-sk5HspGGnLW8b6bX.htm](feats/class-06-sk5HspGGnLW8b6bX.htm)|Remote Trigger|Desencadenante remoto|modificada|
|[class-06-sKuhYCfCbXeRWivv.htm](feats/class-06-sKuhYCfCbXeRWivv.htm)|Nimble Shield Hand|Ágil mano de escudo|modificada|
|[class-06-SOG0yVNtiDsaNbIO.htm](feats/class-06-SOG0yVNtiDsaNbIO.htm)|Performative Weapons Training|Entrenamiento de Armas Interpretativas|modificada|
|[class-06-soHLtpMM9h3AE7PD.htm](feats/class-06-soHLtpMM9h3AE7PD.htm)|Expert Alchemy|Alquimia experta|modificada|
|[class-06-SUNLm99CgsS5M3Eq.htm](feats/class-06-SUNLm99CgsS5M3Eq.htm)|Frightful Condemnation|Condena Espantosa|modificada|
|[class-06-sv4LeEbkOJyLen10.htm](feats/class-06-sv4LeEbkOJyLen10.htm)|Debilitating Bomb|Bomba debilitante|modificada|
|[class-06-t5zeg3m9rEnWnYXY.htm](feats/class-06-t5zeg3m9rEnWnYXY.htm)|Counter Perform|Contrainterpretación Interpretar|modificada|
|[class-06-TCLDccG80M5GeqGw.htm](feats/class-06-TCLDccG80M5GeqGw.htm)|Urgent Upwelling|Subida Urgente|modificada|
|[class-06-TfDvkTNaC1DmsB2C.htm](feats/class-06-TfDvkTNaC1DmsB2C.htm)|Elysium's Cadence|Cadencia de Elysium|modificada|
|[class-06-Tg3Iyq55xW9PTSW9.htm](feats/class-06-Tg3Iyq55xW9PTSW9.htm)|Advanced Arcana|Arcanos avanzados|modificada|
|[class-06-TjNkvawfWCqb1alg.htm](feats/class-06-TjNkvawfWCqb1alg.htm)|Night's Glow|Night's Glow|modificada|
|[class-06-tkMEdh0gM07teWkx.htm](feats/class-06-tkMEdh0gM07teWkx.htm)|Advanced Dogma|Dogma avanzado|modificada|
|[class-06-Tlqqim5TmijoPRRT.htm](feats/class-06-Tlqqim5TmijoPRRT.htm)|Soul Arsenal|Arsenal de almas|modificada|
|[class-06-tpkJXDpSuGznfzGJ.htm](feats/class-06-tpkJXDpSuGznfzGJ.htm)|Mature Animal Companion (Ranger)|Comparo animal maduro (Ranger)|modificada|
|[class-06-Trj5azJlaOk5jgBi.htm](feats/class-06-Trj5azJlaOk5jgBi.htm)|Divine Weapon|Arma divina|modificada|
|[class-06-TszXKspPffCzCD0X.htm](feats/class-06-TszXKspPffCzCD0X.htm)|Disciple of Shade|Discípulo de la Sombra|modificada|
|[class-06-TYP0Ee4o3p9LDodd.htm](feats/class-06-TYP0Ee4o3p9LDodd.htm)|Advanced Kata|Kata avanzada|modificada|
|[class-06-U3A0kqJ2HKBYiu7X.htm](feats/class-06-U3A0kqJ2HKBYiu7X.htm)|Bear Hug|Abrazo de oso|modificada|
|[class-06-u4idmXH5dd2gU9uA.htm](feats/class-06-u4idmXH5dd2gU9uA.htm)|Warding Rune|Warding Rune|modificada|
|[class-06-u8YnTCS2EGoJl90W.htm](feats/class-06-u8YnTCS2EGoJl90W.htm)|Strain Mind|Strain Mind|modificada|
|[class-06-uBOToHKQJr5JBEsg.htm](feats/class-06-uBOToHKQJr5JBEsg.htm)|Mummy's Despair|Mummy's Despair|modificada|
|[class-06-uc6JcNrI31wzSC2h.htm](feats/class-06-uc6JcNrI31wzSC2h.htm)|Bullet Dancer Burn|Bullet Dancer Burn|modificada|
|[class-06-UEbBOljjXvKsGKFu.htm](feats/class-06-UEbBOljjXvKsGKFu.htm)|Disk Rider|Disk Rider|modificada|
|[class-06-uhfZtjbfJ8pZIWrF.htm](feats/class-06-uhfZtjbfJ8pZIWrF.htm)|Scrollmaster Dedication|Dedicación Scrollmaster|modificada|
|[class-06-UjEeHamC2C8JfgJz.htm](feats/class-06-UjEeHamC2C8JfgJz.htm)|Sky and Heaven Stance|Posición del cielo y el cielo.|modificada|
|[class-06-uJpghjJtNbqKUxRd.htm](feats/class-06-uJpghjJtNbqKUxRd.htm)|Wolf Drag|Arrastre del lobo|modificada|
|[class-06-UtUT6JngJbQRHySX.htm](feats/class-06-UtUT6JngJbQRHySX.htm)|Boaster's Challenge|Boaster's Challenge|modificada|
|[class-06-V3lFDAQr3PfnAxMC.htm](feats/class-06-V3lFDAQr3PfnAxMC.htm)|Confounding Image|Confounding Image|modificada|
|[class-06-V7bwuYADV8huWeF7.htm](feats/class-06-V7bwuYADV8huWeF7.htm)|Unnerving Prowess|Proeza enervante|modificada|
|[class-06-VAxtUenSWEBWYBRt.htm](feats/class-06-VAxtUenSWEBWYBRt.htm)|Enervating Wail|Lamento Enervación|modificada|
|[class-06-vKFg7HMNu4cCDD8b.htm](feats/class-06-vKFg7HMNu4cCDD8b.htm)|Volatile Grease|Grasa Volátil|modificada|
|[class-06-vqLt1qjdrflTmdsw.htm](feats/class-06-vqLt1qjdrflTmdsw.htm)|Sun's Fury|Sun's Fury|modificada|
|[class-06-VruIzuysxw4tY6rk.htm](feats/class-06-VruIzuysxw4tY6rk.htm)|Expert Poisoner|Experto Envenenador|modificada|
|[class-06-vsSbYfsRTHveef24.htm](feats/class-06-vsSbYfsRTHveef24.htm)|Duel Spell Advantage|Duel Spell Advantage|modificada|
|[class-06-vuApM8xHOZs4o6oS.htm](feats/class-06-vuApM8xHOZs4o6oS.htm)|Spellmaster Dedication|Dedicación de Spellmaster|modificada|
|[class-06-vW8dGtOD3rZVOJoq.htm](feats/class-06-vW8dGtOD3rZVOJoq.htm)|High-quality Scrounger|Gorrón de alta calidad|modificada|
|[class-06-vWrGwqy4AhHMPz8V.htm](feats/class-06-vWrGwqy4AhHMPz8V.htm)|Dirge of Doom|Endecha de perdición|modificada|
|[class-06-w0nSRBNwexM5Dh0D.htm](feats/class-06-w0nSRBNwexM5Dh0D.htm)|Whirling Throw|Lanzamiento giratorio|modificada|
|[class-06-W21jKAcG0GtEtBiK.htm](feats/class-06-W21jKAcG0GtEtBiK.htm)|Giant's Stature|Estatura de gigante|modificada|
|[class-06-wHwjoK3E1Ot9kkV0.htm](feats/class-06-wHwjoK3E1Ot9kkV0.htm)|Obscured Terrain|Terreno Oscurecido|modificada|
|[class-06-wios5UDRwKXoUYUD.htm](feats/class-06-wios5UDRwKXoUYUD.htm)|Master Summoner|Maestro Invocador|modificada|
|[class-06-Wn3DWAXo2TRxrhI6.htm](feats/class-06-Wn3DWAXo2TRxrhI6.htm)|Sniper's Aim|Puntería de Francotirador|modificada|
|[class-06-x7EGJYZQuxHbP50X.htm](feats/class-06-x7EGJYZQuxHbP50X.htm)|Guided Skill|Habilidad guiada|modificada|
|[class-06-xaSlCFYUXlu5f0zw.htm](feats/class-06-xaSlCFYUXlu5f0zw.htm)|Mind Shards|Fragmentos mentales|modificada|
|[class-06-xDTjr415ZZM8x2WW.htm](feats/class-06-xDTjr415ZZM8x2WW.htm)|High-Speed Regeneration|Regeneración de alta Velocidad.|modificada|
|[class-06-xgvKXeTxns0gIdAn.htm](feats/class-06-xgvKXeTxns0gIdAn.htm)|Smite Evil|Castigar el maligno|modificada|
|[class-06-XkemuXgSQtxFAhZ8.htm](feats/class-06-XkemuXgSQtxFAhZ8.htm)|Advanced Shooting|Tiro Avanzado|modificada|
|[class-06-XLIJXGJ1JdJJZQHG.htm](feats/class-06-XLIJXGJ1JdJJZQHG.htm)|Vision of Foresight|Visión de presciencia|modificada|
|[class-06-xqyN4Nk5mLgUm09l.htm](feats/class-06-xqyN4Nk5mLgUm09l.htm)|Targeted Redirection|Redirección designada como objetivo|modificada|
|[class-06-XRahcvEPEAEdGUn8.htm](feats/class-06-XRahcvEPEAEdGUn8.htm)|Gang Up|Unir fuerzas|modificada|
|[class-06-XusuxqmXWPYc0JYA.htm](feats/class-06-XusuxqmXWPYc0JYA.htm)|Narrative Conduit|Conducto Narrativo|modificada|
|[class-06-XYaaj872JOO9CAws.htm](feats/class-06-XYaaj872JOO9CAws.htm)|Blazing Talon Surge|Blazing Talon Oleaje|modificada|
|[class-06-y61mDkTqk2k77b4x.htm](feats/class-06-y61mDkTqk2k77b4x.htm)|Furious Focus|Foco furioso|modificada|
|[class-06-Y8Rdt4gHBGAUf2zL.htm](feats/class-06-Y8Rdt4gHBGAUf2zL.htm)|Advantageous Assault|Asalto ventajoso|modificada|
|[class-06-yfzpmSF9IYsMeKxo.htm](feats/class-06-yfzpmSF9IYsMeKxo.htm)|One More Activation|Una Activación Más|modificada|
|[class-06-YhpR5bOquHx2JuIj.htm](feats/class-06-YhpR5bOquHx2JuIj.htm)|Litany Against Wrath|Letanía contra la ira|modificada|
|[class-06-YOU5eCD5S4cS6Qeu.htm](feats/class-06-YOU5eCD5S4cS6Qeu.htm)|Cascade Bearer's Flexibility|Flexibilidad del Portador de Cascada|modificada|
|[class-06-yOyMlFGAgLfRas8m.htm](feats/class-06-yOyMlFGAgLfRas8m.htm)|Pistolero's Challenge|Desafío de Pistolero|modificada|
|[class-06-YY7wmYQ9ccAO8fut.htm](feats/class-06-YY7wmYQ9ccAO8fut.htm)|Basic Summoner Spellcasting|Hechizo de invocador básico|modificada|
|[class-06-yyt2I2lGbRndXjbc.htm](feats/class-06-yyt2I2lGbRndXjbc.htm)|Selective Energy|Energía selectiva|modificada|
|[class-06-z8bozNJvUjBoKLPA.htm](feats/class-06-z8bozNJvUjBoKLPA.htm)|Arcane Sensitivity|Sensibilidad Arcana|modificada|
|[class-06-zbxqYhmn7KbqR2Sb.htm](feats/class-06-zbxqYhmn7KbqR2Sb.htm)|Shatter Defenses|Destrozar defensas|modificada|
|[class-06-ZhhITE2ZMX7UZUge.htm](feats/class-06-ZhhITE2ZMX7UZUge.htm)|Predatory Claws|Garras depredadoras|modificada|
|[class-06-zMT3etCcdPdtAdOn.htm](feats/class-06-zMT3etCcdPdtAdOn.htm)|Butterfly's Kiss|Butterfly's Kiss|modificada|
|[class-06-ZOr1FUlpJj1q6q3H.htm](feats/class-06-ZOr1FUlpJj1q6q3H.htm)|Knowledge is Power|El conocimiento es poder|modificada|
|[class-06-ZPclfDmiHzEqblry.htm](feats/class-06-ZPclfDmiHzEqblry.htm)|Animal Skin|Piel animal|modificada|
|[class-07-i6eWwJ67qBIPJZoK.htm](feats/class-07-i6eWwJ67qBIPJZoK.htm)|Explosive Entry|Entrada Explosión|modificada|
|[class-07-v1PtAazmEFhTp6fZ.htm](feats/class-07-v1PtAazmEFhTp6fZ.htm)|Quick Change|Cambio rápido|modificada|
|[class-08-0Bu48kn3Deq9gHQE.htm](feats/class-08-0Bu48kn3Deq9gHQE.htm)|Practiced Guidance|Orientación divina practicada|modificada|
|[class-08-0PcVi7eav6PMLOPl.htm](feats/class-08-0PcVi7eav6PMLOPl.htm)|Channeled Succor|Auxilio canalizado|modificada|
|[class-08-16MOW7deoOoDwE9z.htm](feats/class-08-16MOW7deoOoDwE9z.htm)|Hazard Finder|Buscador de peligros|modificada|
|[class-08-1AgirzUGkyDdmENy.htm](feats/class-08-1AgirzUGkyDdmENy.htm)|Shadowdancer Dedication|Dedicación Shadowdancer|modificada|
|[class-08-1NS6AM2oVNKb4LhX.htm](feats/class-08-1NS6AM2oVNKb4LhX.htm)|Flashing Shield|Escudo intermitente|modificada|
|[class-08-1qJCMbs3zcPMWDux.htm](feats/class-08-1qJCMbs3zcPMWDux.htm)|Know-It-All (Bard)|Sabelotodo (Bardo)|modificada|
|[class-08-1VLOhyq0IFMY2rqh.htm](feats/class-08-1VLOhyq0IFMY2rqh.htm)|Animal Rage|Furia animal|modificada|
|[class-08-27R8yZcY2uXH6pZN.htm](feats/class-08-27R8yZcY2uXH6pZN.htm)|Powerful Snares|Trampas de lazo poderosas|modificada|
|[class-08-2leK1eGVcIBTmx6J.htm](feats/class-08-2leK1eGVcIBTmx6J.htm)|Unseen Passage|Pasaje Invisible|modificada|
|[class-08-2mww0DmokYJXUEoA.htm](feats/class-08-2mww0DmokYJXUEoA.htm)|Ooze Empathy|Empatía rezumante|modificada|
|[class-08-2tUdsoPEnW9yS8so.htm](feats/class-08-2tUdsoPEnW9yS8so.htm)|Tangled Forest Stance|Posición del bosque enmarado|modificada|
|[class-08-31ozQ8lwNtiQi2N0.htm](feats/class-08-31ozQ8lwNtiQi2N0.htm)|Guardian Ghosts|Fantasmas guardianes|modificada|
|[class-08-3hhCDWPm021hvicR.htm](feats/class-08-3hhCDWPm021hvicR.htm)|Renewed Vigor|Vigor renovado|modificada|
|[class-08-3xD4LtEgpGZU0MNx.htm](feats/class-08-3xD4LtEgpGZU0MNx.htm)|Syu Tak-nwa's Hexed Locks|Syu Tak-nwa's Hexed Locks|modificada|
|[class-08-3XFKXB3ffeIkrQYe.htm](feats/class-08-3XFKXB3ffeIkrQYe.htm)|Sense Chaos|Sense Chaos|modificada|
|[class-08-3xiWBDSR8miAotpa.htm](feats/class-08-3xiWBDSR8miAotpa.htm)|Viking Vindicator|Viking Vindicator|modificada|
|[class-08-3xkFb2qlAdgLmdSf.htm](feats/class-08-3xkFb2qlAdgLmdSf.htm)|Counter Curse|Contra Maldición|modificada|
|[class-08-49TMxb8OA1Pp7AiF.htm](feats/class-08-49TMxb8OA1Pp7AiF.htm)|Scholar's Hunch|La corazonada del erudito|modificada|
|[class-08-4Bum44iMs6tQz90v.htm](feats/class-08-4Bum44iMs6tQz90v.htm)|Waking Dream|Sue despierto|modificada|
|[class-08-4Ek3Kyle2DsCPQQm.htm](feats/class-08-4Ek3Kyle2DsCPQQm.htm)|Great Boaster|Gran Fanfarrón|modificada|
|[class-08-4HqPkJeSDpqYeGNn.htm](feats/class-08-4HqPkJeSDpqYeGNn.htm)|Enhanced Psychopomp Familiar|Familiar psicopompo potenciado|modificada|
|[class-08-4jaCuX2JSjTSJ3wp.htm](feats/class-08-4jaCuX2JSjTSJ3wp.htm)|Phalanx Formation|Formación de Falange|modificada|
|[class-08-4nFsGmPdWvFrwjgF.htm](feats/class-08-4nFsGmPdWvFrwjgF.htm)|Advanced Hallowed Spell|Advanced Hallowed Spell|modificada|
|[class-08-4osf0dNeZ19KBoTb.htm](feats/class-08-4osf0dNeZ19KBoTb.htm)|Manifold Modifications|Manifold Modifications|modificada|
|[class-08-4xeDjt8eARzAGARP.htm](feats/class-08-4xeDjt8eARzAGARP.htm)|Masked Casting|Lanzamiento enmascarado|modificada|
|[class-08-4YeHOPzo3zOhZQCh.htm](feats/class-08-4YeHOPzo3zOhZQCh.htm)|Mantis Form|Forma Mantis|modificada|
|[class-08-505CCsBRft1P53gP.htm](feats/class-08-505CCsBRft1P53gP.htm)|Lingering Flames|Flamígera persistente|modificada|
|[class-08-58LYCoLrCzG2Ll8b.htm](feats/class-08-58LYCoLrCzG2Ll8b.htm)|Incredible Construct Companion|Comparo sensacional de construcción|modificada|
|[class-08-5d4AyZ0Y6Ht1OwIa.htm](feats/class-08-5d4AyZ0Y6Ht1OwIa.htm)|Sidestep|Paso lateral|modificada|
|[class-08-5hUj7glY8YnO5sBI.htm](feats/class-08-5hUj7glY8YnO5sBI.htm)|Advanced School Spell|Conjuro de escuela avanzado|modificada|
|[class-08-5MG4dBTsFZVbHcX7.htm](feats/class-08-5MG4dBTsFZVbHcX7.htm)|Basic Eldritch Archer Spellcasting|Hechizo básico de arquero eldritch|modificada|
|[class-08-5O6G488xu1p8ZHsS.htm](feats/class-08-5O6G488xu1p8ZHsS.htm)|Slayer's Blessing|Bendición del Cazador|modificada|
|[class-08-5Pj6pQ7N1qXCQLal.htm](feats/class-08-5Pj6pQ7N1qXCQLal.htm)|Accursed Touch|Toque maldito|modificada|
|[class-08-60o2Pf4IunqP6J0Z.htm](feats/class-08-60o2Pf4IunqP6J0Z.htm)|Equitable Defense|Defensa Equitativa|modificada|
|[class-08-6biVVoaqqUdQmQ37.htm](feats/class-08-6biVVoaqqUdQmQ37.htm)|Fiery Retort|Fiery Retort|modificada|
|[class-08-6qHoUiEudUgUB1Uq.htm](feats/class-08-6qHoUiEudUgUB1Uq.htm)|Divine Healing|Curación divina|modificada|
|[class-08-7IsHNime3WneCan6.htm](feats/class-08-7IsHNime3WneCan6.htm)|Wall Run|Correr por la pared|modificada|
|[class-08-7p2tNqYHsg6u05cU.htm](feats/class-08-7p2tNqYHsg6u05cU.htm)|Shoving Sweep|Empujar Barrido|modificada|
|[class-08-7PlGRHizgieuYzDR.htm](feats/class-08-7PlGRHizgieuYzDR.htm)|Mental Static|Mental Static|modificada|
|[class-08-7vOVPzsVuyE5a3Rp.htm](feats/class-08-7vOVPzsVuyE5a3Rp.htm)|Fortified Flesh|Carne Fortificada|modificada|
|[class-08-8cq6NO087Te3P9yw.htm](feats/class-08-8cq6NO087Te3P9yw.htm)|Bonds of Death|Lazos de Muerte|modificada|
|[class-08-8rE5zLEVe4putosB.htm](feats/class-08-8rE5zLEVe4putosB.htm)|Fey Caller|Llamador feérico|modificada|
|[class-08-8Sdw31YToKhBJ4v4.htm](feats/class-08-8Sdw31YToKhBJ4v4.htm)|Physical Training|Entrenamiento Físico|modificada|
|[class-08-8YLCu791osZNFKN2.htm](feats/class-08-8YLCu791osZNFKN2.htm)|Leap and Fire|Salto sin carrerilla y fuego|modificada|
|[class-08-8z9XkfZalQ5tUjfy.htm](feats/class-08-8z9XkfZalQ5tUjfy.htm)|Paralyzing Slash|Tajo paralizante|modificada|
|[class-08-94PGauGdzrVARMLc.htm](feats/class-08-94PGauGdzrVARMLc.htm)|Grit and Tenacity|Valor y tenacidad|modificada|
|[class-08-9EmJElnNVmXQ7Rzn.htm](feats/class-08-9EmJElnNVmXQ7Rzn.htm)|Greenwatch Veteran|Greenwatch Veterano|modificada|
|[class-08-9EqUTnbV8WHE2aKm.htm](feats/class-08-9EqUTnbV8WHE2aKm.htm)|Dualistic Synergy|Sinergia Dualista|modificada|
|[class-08-9Eufa07qvXG41QmG.htm](feats/class-08-9Eufa07qvXG41QmG.htm)|Improved Poison Weapon|Arma envenenada mejorada|modificada|
|[class-08-9LvJo3K2AjKcVvTc.htm](feats/class-08-9LvJo3K2AjKcVvTc.htm)|Exude Abyssal Corruption|Exudar Corrupción Abisal|modificada|
|[class-08-9LwOCcutlLxd4bfS.htm](feats/class-08-9LwOCcutlLxd4bfS.htm)|Reversing Charge|Inversión de carga|modificada|
|[class-08-9MiK0Lyro5dQgHij.htm](feats/class-08-9MiK0Lyro5dQgHij.htm)|Rippling Spin|Giro ondulante|modificada|
|[class-08-9sl2t3jb5ZdQA3K4.htm](feats/class-08-9sl2t3jb5ZdQA3K4.htm)|Can't You See?|¿No lo ves?|modificada|
|[class-08-9WMLIcHpwkQpUQfz.htm](feats/class-08-9WMLIcHpwkQpUQfz.htm)|Magic Finder|Buscador Mágico|modificada|
|[class-08-A0keRhzNlcB1u4gD.htm](feats/class-08-A0keRhzNlcB1u4gD.htm)|Stasian Smash|Stasian Smash|modificada|
|[class-08-A981119DMdqE9Pg1.htm](feats/class-08-A981119DMdqE9Pg1.htm)|Running Tackle|Running Tackle|modificada|
|[class-08-AbgHCPWOKULeXrJ2.htm](feats/class-08-AbgHCPWOKULeXrJ2.htm)|Sly Striker|Pegador astuto|modificada|
|[class-08-Ad0XBuETAkMD6doj.htm](feats/class-08-Ad0XBuETAkMD6doj.htm)|Felling Strike|Golpe derribador|modificada|
|[class-08-ADgFB8hjUgcXS4bF.htm](feats/class-08-ADgFB8hjUgcXS4bF.htm)|Mutable Familiar|Mutable Familiar|modificada|
|[class-08-aEws1NR19Lbu1Kio.htm](feats/class-08-aEws1NR19Lbu1Kio.htm)|Incredible Beastmaster Companion|Compañero sensacional de Beastmaster.|modificada|
|[class-08-AgMFfp6fdNZ1mAxn.htm](feats/class-08-AgMFfp6fdNZ1mAxn.htm)|Order Training|Orden Formación|modificada|
|[class-08-agWNAYqgyV58jlxm.htm](feats/class-08-agWNAYqgyV58jlxm.htm)|Foolproof Instructions|Instrucciones a prueba de tontos|modificada|
|[class-08-AiV2xFhYB90KHt2x.htm](feats/class-08-AiV2xFhYB90KHt2x.htm)|Pinning Fire|Fuego de inmovilización|modificada|
|[class-08-aKFYL4b5Bi7qla2j.htm](feats/class-08-aKFYL4b5Bi7qla2j.htm)|Peer Beyond|Compañero Más Allá|modificada|
|[class-08-AqVyKiIjISDBFRA0.htm](feats/class-08-AqVyKiIjISDBFRA0.htm)|Unkillable|Unkillable|modificada|
|[class-08-Ar6W97iun6yYI8Df.htm](feats/class-08-Ar6W97iun6yYI8Df.htm)|Delay Trap|Retrasar trampa|modificada|
|[class-08-ASAXLrckAyTBYi8E.htm](feats/class-08-ASAXLrckAyTBYi8E.htm)|Forgotten Presence|Presencia olvidada|modificada|
|[class-08-ASWqQ6RB7cfCsUo0.htm](feats/class-08-ASWqQ6RB7cfCsUo0.htm)|Protect Ally|Proteger aliado|modificada|
|[class-08-Aww98EQXcigRhY3v.htm](feats/class-08-Aww98EQXcigRhY3v.htm)|Sickening Bite|Muerdemuerde|modificada|
|[class-08-b1eGMNjBY3iqIt2S.htm](feats/class-08-b1eGMNjBY3iqIt2S.htm)|Magical Adept|Adepto Mágico|modificada|
|[class-08-BaKEnNbzbGlenmRv.htm](feats/class-08-BaKEnNbzbGlenmRv.htm)|Eerie Traces|Eerie Traces|modificada|
|[class-08-BASCKOPvNGgoHGid.htm](feats/class-08-BASCKOPvNGgoHGid.htm)|Achaekek's Grip|Achaekek's Grip|modificada|
|[class-08-BBvmmULFPLlHCeIK.htm](feats/class-08-BBvmmULFPLlHCeIK.htm)|Heightened Captivation|Cautivación aumentada|modificada|
|[class-08-bcExqs4CsG2Kc5Bs.htm](feats/class-08-bcExqs4CsG2Kc5Bs.htm)|Miniaturize|Miniaturizar|modificada|
|[class-08-BFgrPAK2v3GSKQ5e.htm](feats/class-08-BFgrPAK2v3GSKQ5e.htm)|Lore Seeker|Lore Seeker|modificada|
|[class-08-BkKWTt3ufaCN2ZdI.htm](feats/class-08-BkKWTt3ufaCN2ZdI.htm)|Sudden Leap|Salto sin carrerilla|modificada|
|[class-08-bkX8v744C62W8hol.htm](feats/class-08-bkX8v744C62W8hol.htm)|Attuned Stride|Zancada Attuned|modificada|
|[class-08-BVHDkBa4JMmmj5Sn.htm](feats/class-08-BVHDkBa4JMmmj5Sn.htm)|Opportunistic Grapple|Presa Oportunista|modificada|
|[class-08-bXoGskH0SYfdcEtJ.htm](feats/class-08-bXoGskH0SYfdcEtJ.htm)|Garden Path|Garden Path|modificada|
|[class-08-c1QTcMtI9957gQoB.htm](feats/class-08-c1QTcMtI9957gQoB.htm)|Remove Presence|Eliminar Presencia|modificada|
|[class-08-C3zKTQecexSbezhT.htm](feats/class-08-C3zKTQecexSbezhT.htm)|Grievous Blow|Golpe Grave|modificada|
|[class-08-C4ugUzUuQ4UznNhl.htm](feats/class-08-C4ugUzUuQ4UznNhl.htm)|Basic Modification|Modificación Básica|modificada|
|[class-08-c9rhGmKft1BVT4JO.htm](feats/class-08-c9rhGmKft1BVT4JO.htm)|Skill Mastery (Rogue)|Maestría en habilidades (Pícaro)|modificada|
|[class-08-cA1IIy6UEsgETXiX.htm](feats/class-08-cA1IIy6UEsgETXiX.htm)|Furious Bully|Matón furioso|modificada|
|[class-08-cdeVm8fTDzrP71Sw.htm](feats/class-08-cdeVm8fTDzrP71Sw.htm)|Lorefinder|Lorefinder|modificada|
|[class-08-cDNxHLIgvyD7yBfM.htm](feats/class-08-cDNxHLIgvyD7yBfM.htm)|Scattered Fire|Fuego disperso|modificada|
|[class-08-CIRseixX8dr36ZQK.htm](feats/class-08-CIRseixX8dr36ZQK.htm)|Inspirational Performance|Interpretación inspiradora|modificada|
|[class-08-CL9pFxxMXqzIyg4j.htm](feats/class-08-CL9pFxxMXqzIyg4j.htm)|Wild Winds Initiate|Iniciado del viento salvaje|modificada|
|[class-08-CN7tu5H6wTe9ENmO.htm](feats/class-08-CN7tu5H6wTe9ENmO.htm)|Crossblooded Evolution|Evolución mestiza|modificada|
|[class-08-cU5NdcwnMkFQNPjh.htm](feats/class-08-cU5NdcwnMkFQNPjh.htm)|To Battle!|¡A la batalla!|modificada|
|[class-08-d81PT3w97SkyAiXQ.htm](feats/class-08-d81PT3w97SkyAiXQ.htm)|Lion's Fury|Furia del León|modificada|
|[class-08-dCoX0TfsasMfwYnz.htm](feats/class-08-dCoX0TfsasMfwYnz.htm)|Larcenous Hand|Larcenous Hand|modificada|
|[class-08-DGO6kyjw2bQG7dbY.htm](feats/class-08-DGO6kyjw2bQG7dbY.htm)|Incredible Aim|Puntería sensacional|modificada|
|[class-08-dHJw5Yv0gY3suBZo.htm](feats/class-08-dHJw5Yv0gY3suBZo.htm)|Reminder of the Greater Fear|Recordatorio del mayor miedo|modificada|
|[class-08-dTPVRVzfVBlBUV2l.htm](feats/class-08-dTPVRVzfVBlBUV2l.htm)|Arrow Snatching|Arrebatar flechas|modificada|
|[class-08-dtPXuS8MiWrz5UNK.htm](feats/class-08-dtPXuS8MiWrz5UNK.htm)|Hot Foot|Hot Foot|modificada|
|[class-08-DwU6CmK4KsH8A3hu.htm](feats/class-08-DwU6CmK4KsH8A3hu.htm)|Arcane Breadth|Abundancia arcana|modificada|
|[class-08-E3NsAdbp7kkOvfnr.htm](feats/class-08-E3NsAdbp7kkOvfnr.htm)|Quick Positioning|Posicionamiento Rápido|modificada|
|[class-08-E7eceezD3NDmBVBb.htm](feats/class-08-E7eceezD3NDmBVBb.htm)|Back To Back|Back To Back|modificada|
|[class-08-EG27noJj9KzyB2i4.htm](feats/class-08-EG27noJj9KzyB2i4.htm)|Out of Hand|Fuera de control|modificada|
|[class-08-eGgSGU1LOaRNRYR7.htm](feats/class-08-eGgSGU1LOaRNRYR7.htm)|Shadow Spell|Hechizo de Sombra|modificada|
|[class-08-EGtuOZ3E9y0qZ1oJ.htm](feats/class-08-EGtuOZ3E9y0qZ1oJ.htm)|Familiar Form|Forma Familiar|modificada|
|[class-08-enPAJ1oSDutts7ic.htm](feats/class-08-enPAJ1oSDutts7ic.htm)|Deadly Aim|Puntería mortal|modificada|
|[class-08-EnpbhZr94ZyZI4hb.htm](feats/class-08-EnpbhZr94ZyZI4hb.htm)|Sense Evil|Sentir el maligno|modificada|
|[class-08-enxzAkPICXT4sSFU.htm](feats/class-08-enxzAkPICXT4sSFU.htm)|Fused Staff|Bastón fundido|modificada|
|[class-08-EoKgJXfNfHwsy2sk.htm](feats/class-08-EoKgJXfNfHwsy2sk.htm)|Superimpose Time Duplicates|Superponer Tiempo Duplicados|modificada|
|[class-08-F6VlPyZZpqV6d2CS.htm](feats/class-08-F6VlPyZZpqV6d2CS.htm)|Flensing Slice|Flensing Slice|modificada|
|[class-08-FEMXDBCf8gXBYdez.htm](feats/class-08-FEMXDBCf8gXBYdez.htm)|Dark Persona's Presence|Presencia de Persona Oscura|modificada|
|[class-08-ffdXSxl4lVFrOvyQ.htm](feats/class-08-ffdXSxl4lVFrOvyQ.htm)|Ambushing Knockdown|Emboscar Derribo|modificada|
|[class-08-fhwO3N3VoZ0ZpfjU.htm](feats/class-08-fhwO3N3VoZ0ZpfjU.htm)|Accursed Magic|Magia maldita|modificada|
|[class-08-fHWYICk6cSePr30c.htm](feats/class-08-fHWYICk6cSePr30c.htm)|Surreptitious Spellcaster|Hechicero subrepticio|modificada|
|[class-08-FjuuX0vUWlYchRNM.htm](feats/class-08-FjuuX0vUWlYchRNM.htm)|Inspire Heroics|Inspirar heroismo|modificada|
|[class-08-fJxIdcg7kWPwlULY.htm](feats/class-08-fJxIdcg7kWPwlULY.htm)|Guardian's Embrace|Abrazo del Guardián|modificada|
|[class-08-FNW66GfWXaiBSexb.htm](feats/class-08-FNW66GfWXaiBSexb.htm)|Impervious Vehicle|Impervious Vehicle|modificada|
|[class-08-Fs88vjez9px2mmrC.htm](feats/class-08-Fs88vjez9px2mmrC.htm)|Spell Swipe|Vaivén de hechizos|modificada|
|[class-08-gepQGtV8Ftr0JJ6O.htm](feats/class-08-gepQGtV8Ftr0JJ6O.htm)|Skyseeker|Skyseeker|modificada|
|[class-08-gHc9mqHiMqayiOIx.htm](feats/class-08-gHc9mqHiMqayiOIx.htm)|Boost Summons|Boost Summons|modificada|
|[class-08-gHcVxdMksOaGaiCx.htm](feats/class-08-gHcVxdMksOaGaiCx.htm)|Chronomancer's Secrets|Chronomancer's Secrets|modificada|
|[class-08-gQ2EvesPqLbISLQV.htm](feats/class-08-gQ2EvesPqLbISLQV.htm)|Feral Mutagen|Mutágeno feral|modificada|
|[class-08-gTg3D9Dv9L4NEVhV.htm](feats/class-08-gTg3D9Dv9L4NEVhV.htm)|Gardener's Resolve|Resolución del Jardinero.|modificada|
|[class-08-gtHTr77BkK4CckEH.htm](feats/class-08-gtHTr77BkK4CckEH.htm)|Basic Beast Gunner Spellcasting|Hechizo básico de artillero de bestias|modificada|
|[class-08-gYcmow7HM8J3giwL.htm](feats/class-08-gYcmow7HM8J3giwL.htm)|Grand Dance|Gran Baile|modificada|
|[class-08-H6qletvAJUCC1aIa.htm](feats/class-08-H6qletvAJUCC1aIa.htm)|Hulking Size|Tamaño Hulking|modificada|
|[class-08-HIyuVIh2XSDz3h2j.htm](feats/class-08-HIyuVIh2XSDz3h2j.htm)|Skim Scroll|Skim Scroll|modificada|
|[class-08-hLMsARkiwRf4SwqZ.htm](feats/class-08-hLMsARkiwRf4SwqZ.htm)|Improved Command Corpse|Cadáver de orden imperiosa mejorado|modificada|
|[class-08-HlSwpxreIfsglTJ8.htm](feats/class-08-HlSwpxreIfsglTJ8.htm)|Lethargy Poisoner|Veneno letárgico|modificada|
|[class-08-i7hNUqiJsB8hgIET.htm](feats/class-08-i7hNUqiJsB8hgIET.htm)|Quick Stow (Swordmaster)|Estiba Rápida (Swordmaster)|modificada|
|[class-08-ivSf8wSIUkK8gwej.htm](feats/class-08-ivSf8wSIUkK8gwej.htm)|Live Ammunition|Munición real|modificada|
|[class-08-IZupJre7o5We2VrK.htm](feats/class-08-IZupJre7o5We2VrK.htm)|Vicious Fangs|Colmillos viciosos|modificada|
|[class-08-jaekke9HomT4PZ9b.htm](feats/class-08-jaekke9HomT4PZ9b.htm)|Reflexive Grip|Agarre Reflexivo|modificada|
|[class-08-jCIBYryi6Y3JwmqH.htm](feats/class-08-jCIBYryi6Y3JwmqH.htm)|Mixed Maneuver|Maniobra mixta|modificada|
|[class-08-JHJBmiyILzWdFRJO.htm](feats/class-08-JHJBmiyILzWdFRJO.htm)|Advanced Deity's Domain|Dominio de tu dios avanzado.|modificada|
|[class-08-jIMeialR9CBo1bx9.htm](feats/class-08-jIMeialR9CBo1bx9.htm)|Levering Strike|Golpe de Palanca|modificada|
|[class-08-Jj6sVfIX81tgvSY4.htm](feats/class-08-Jj6sVfIX81tgvSY4.htm)|Wind Caller|Llamador del viento|modificada|
|[class-08-Joy1e6pdqx6fN9mH.htm](feats/class-08-Joy1e6pdqx6fN9mH.htm)|Incredible Familiar (Thaumaturge)|Familiar sensacional (Taumaturgo)|modificada|
|[class-08-jTOxxy19o2VRJTbH.htm](feats/class-08-jTOxxy19o2VRJTbH.htm)|Golden League Xun Dedication|Dedicación Xun de la Liga Dorada|modificada|
|[class-08-jwcNyPDVw313KXZU.htm](feats/class-08-jwcNyPDVw313KXZU.htm)|Armored Rebuff|Armored Rebuff|modificada|
|[class-08-Jwq5o13uZF3ooln1.htm](feats/class-08-Jwq5o13uZF3ooln1.htm)|Jellyfish Stance|Posición Medusa|modificada|
|[class-08-jYKKnr41OqQrf7hv.htm](feats/class-08-jYKKnr41OqQrf7hv.htm)|Efficient Rituals|Rituales Eficaces|modificada|
|[class-08-jZalt2bFGjK8XXcP.htm](feats/class-08-jZalt2bFGjK8XXcP.htm)|Many Guises|Many Guises|modificada|
|[class-08-jZy91ekcS9ZqmdEH.htm](feats/class-08-jZy91ekcS9ZqmdEH.htm)|Knight's Retaliation|Represalia del Caballero|modificada|
|[class-08-kPjBGlHMvBqFXNq2.htm](feats/class-08-kPjBGlHMvBqFXNq2.htm)|Cursed Effigy|Efigie Maldita|modificada|
|[class-08-kQfqJLpKOPORq7I6.htm](feats/class-08-kQfqJLpKOPORq7I6.htm)|Work Yourself Up|Work Yourself Up|modificada|
|[class-08-kYYB7ziQZjlgQWWu.htm](feats/class-08-kYYB7ziQZjlgQWWu.htm)|Call Ursine Ally|Llamar aliado de Ursine|modificada|
|[class-08-kZdcoaTD849QalR9.htm](feats/class-08-kZdcoaTD849QalR9.htm)|Greater Mercy|Merced mayor|modificada|
|[class-08-L9aBLPG2veBkVow9.htm](feats/class-08-L9aBLPG2veBkVow9.htm)|Persistent Creation|Creación persistente|modificada|
|[class-08-Le30algCdKIsxmeK.htm](feats/class-08-Le30algCdKIsxmeK.htm)|Ferocious Shape|Forma feroz|modificada|
|[class-08-lH8FwUM9bX9rpVsa.htm](feats/class-08-lH8FwUM9bX9rpVsa.htm)|Dream Magic|Magia del Sue|modificada|
|[class-08-lMA3F9SGzGV79P5C.htm](feats/class-08-lMA3F9SGzGV79P5C.htm)|Strangle|Estrangular|modificada|
|[class-08-LnSMRHjMArCkE4w1.htm](feats/class-08-LnSMRHjMArCkE4w1.htm)|Heal Mount|Curar montura|modificada|
|[class-08-lUjWE40fjAjHecNQ.htm](feats/class-08-lUjWE40fjAjHecNQ.htm)|Incredible Megafauna Companion|Compañero de Megafauna Increíble|modificada|
|[class-08-lVXk0fhZqjqKilhB.htm](feats/class-08-lVXk0fhZqjqKilhB.htm)|Energy Resistance|Resistencia a la energía|modificada|
|[class-08-lvxzgLNzmScMY0uC.htm](feats/class-08-lvxzgLNzmScMY0uC.htm)|Stalwart Mind|Mente Inquebrantable|modificada|
|[class-08-m8iP2OCzit9WUrMD.htm](feats/class-08-m8iP2OCzit9WUrMD.htm)|Enchanting Arrow|Flecha de encantamiento|modificada|
|[class-08-mf2cdCRV8uowOMOm.htm](feats/class-08-mf2cdCRV8uowOMOm.htm)|Dueling Riposte|Réplica de duelo|modificada|
|[class-08-MjPqgBQU9W4kelfz.htm](feats/class-08-MjPqgBQU9W4kelfz.htm)|Interrupt Charge|Interrumpir Carga|modificada|
|[class-08-MlxfNv98LZKfYl64.htm](feats/class-08-MlxfNv98LZKfYl64.htm)|Precious Arrow|Flecha Preciosa|modificada|
|[class-08-MSqgBffcAXTg700A.htm](feats/class-08-MSqgBffcAXTg700A.htm)|Preventative Treatment|Tratamiento preventivo|modificada|
|[class-08-mUL0C7O4HSnPSXea.htm](feats/class-08-mUL0C7O4HSnPSXea.htm)|Warped Constriction|Warped Constriction|modificada|
|[class-08-MxNb97qr1yMhbjiP.htm](feats/class-08-MxNb97qr1yMhbjiP.htm)|Golem Grafter Dedication|Golem Grafter Dedication|modificada|
|[class-08-n7LTwKUSATLaQ9FD.htm](feats/class-08-n7LTwKUSATLaQ9FD.htm)|Survivor of Desolation|Superviviente de la Desolación|modificada|
|[class-08-nAVLB5MLYWUu8N71.htm](feats/class-08-nAVLB5MLYWUu8N71.htm)|Storm Shroud|Mortaja de Tormenta|modificada|
|[class-08-NBvhmNqneeok7ZOr.htm](feats/class-08-NBvhmNqneeok7ZOr.htm)|Overdrive Ally|Overdrive Ally|modificada|
|[class-08-niKCBA11zzgGT1PU.htm](feats/class-08-niKCBA11zzgGT1PU.htm)|Primal Breadth|Abundancia primigenia|modificada|
|[class-08-nLTTph2tgwcQghVq.htm](feats/class-08-nLTTph2tgwcQghVq.htm)|Cremate Undead|Incinerar muertos vivientes|modificada|
|[class-08-nlXyh7828TgZIewv.htm](feats/class-08-nlXyh7828TgZIewv.htm)|Capture Magic|Capturar Magia|modificada|
|[class-08-NLyldMwxUWaanlzH.htm](feats/class-08-NLyldMwxUWaanlzH.htm)|Thoughtsense|Percibir pensamientos|modificada|
|[class-08-nwcsv54Vs4YBGFs9.htm](feats/class-08-nwcsv54Vs4YBGFs9.htm)|Whispering Steps|Pasos Susurrantes|modificada|
|[class-08-nx2nB10rypAuspAa.htm](feats/class-08-nx2nB10rypAuspAa.htm)|Acquired Tolerance|Tolerancia adquirida|modificada|
|[class-08-ny0nfGTDUE4p8TtO.htm](feats/class-08-ny0nfGTDUE4p8TtO.htm)|Ubiquitous Gadgets|Artilugios Ubicuos|modificada|
|[class-08-o98nJriWE95xgweg.htm](feats/class-08-o98nJriWE95xgweg.htm)|Wayfinder Resonance Infiltrator|Infiltrador de Resonancia Orientador|modificada|
|[class-08-OeUP3ASaS5deBsWw.htm](feats/class-08-OeUP3ASaS5deBsWw.htm)|Accurate Swing|Golpe preciso|modificada|
|[class-08-oHra9QanDFpAZ4hh.htm](feats/class-08-oHra9QanDFpAZ4hh.htm)|Warden's Boon|Merced del guardián|modificada|
|[class-08-Olx796SgBbHUFeHc.htm](feats/class-08-Olx796SgBbHUFeHc.htm)|Armiger's Mobility|Movilidad de Armiger|modificada|
|[class-08-oSi9mUtYWYekxX0B.htm](feats/class-08-oSi9mUtYWYekxX0B.htm)|Able Ritualist|Able Ritualist|modificada|
|[class-08-ossGwqEvC6ifPwgT.htm](feats/class-08-ossGwqEvC6ifPwgT.htm)|Swap Reflections|Reflexiones Swap|modificada|
|[class-08-OtM4pvPeIeEEdpuS.htm](feats/class-08-OtM4pvPeIeEEdpuS.htm)|Rule of Three|Regla de tres|modificada|
|[class-08-P5Zu46vfO4ptBBoj.htm](feats/class-08-P5Zu46vfO4ptBBoj.htm)|Mind Projectiles|Proyectiles Mentales|modificada|
|[class-08-PaUpesy5lyDLlwud.htm](feats/class-08-PaUpesy5lyDLlwud.htm)|Transcribe Moment|Transcribe Moment|modificada|
|[class-08-PccekOihIbRWdDky.htm](feats/class-08-PccekOihIbRWdDky.htm)|Malleable Mental Forge|Forja Mental Maleable|modificada|
|[class-08-PcDvw2vWTzBEIX7k.htm](feats/class-08-PcDvw2vWTzBEIX7k.htm)|Necromantic Tenacity|Necromantic Tenacity|modificada|
|[class-08-PmJHC91WEPwrMDNW.htm](feats/class-08-PmJHC91WEPwrMDNW.htm)|Know-It-All|Sabelotodo|modificada|
|[class-08-po0WNUVwtaEBK8LH.htm](feats/class-08-po0WNUVwtaEBK8LH.htm)|Eerie Environs|Eerie Environs|modificada|
|[class-08-pRqcm5P2ZFihSpVI.htm](feats/class-08-pRqcm5P2ZFihSpVI.htm)|Quick Shield Block|Bloqueo con el escudo rápido.|modificada|
|[class-08-pu8Mg2FIrhBYTNnB.htm](feats/class-08-pu8Mg2FIrhBYTNnB.htm)|Duo's Aim|Duo's Aim|modificada|
|[class-08-PuyvasWeofGMrhpu.htm](feats/class-08-PuyvasWeofGMrhpu.htm)|Opportune Backstab|Pulada trapera oportuna|modificada|
|[class-08-Q21KzuubUBBkoges.htm](feats/class-08-Q21KzuubUBBkoges.htm)|Gigaton Strike|Golpe Gigatón|modificada|
|[class-08-qgW8uHJXJGl3DKBS.htm](feats/class-08-qgW8uHJXJGl3DKBS.htm)|Incredible Companion (Druid)|Comparo sensacional (Druida)|modificada|
|[class-08-QkKMile0qqmuVY67.htm](feats/class-08-QkKMile0qqmuVY67.htm)|Clinging Shadows Initiate|Sombras aferradas Iniciar|modificada|
|[class-08-QN55ToyJIjxgPmhs.htm](feats/class-08-QN55ToyJIjxgPmhs.htm)|Perfect Resistance|Resistencia Perfecta|modificada|
|[class-08-qnMdgukNGmLWtSbZ.htm](feats/class-08-qnMdgukNGmLWtSbZ.htm)|The Harder They Fall (Kingmaker)|The Harder They Fall (Kingmaker)|modificada|
|[class-08-QQCxSQaX8UEZHfUz.htm](feats/class-08-QQCxSQaX8UEZHfUz.htm)|Unbelievable Luck|Suerte Increíble|modificada|
|[class-08-qUBr1YsQw3BSNy9c.htm](feats/class-08-qUBr1YsQw3BSNy9c.htm)|Glyph Expert|Experto en glifos|modificada|
|[class-08-qXWcmyHLkMlzRffC.htm](feats/class-08-qXWcmyHLkMlzRffC.htm)|Vantage Shot|Vantage Shot|modificada|
|[class-08-QYZovCDjOhBb1u01.htm](feats/class-08-QYZovCDjOhBb1u01.htm)|Innate Magical Intuition|Intuición Mágica Innata|modificada|
|[class-08-R337W7VveJxlEddE.htm](feats/class-08-R337W7VveJxlEddE.htm)|Necromantic Bulwark|Necromantic Bulwark|modificada|
|[class-08-r7srDh7Iz94vfwwN.htm](feats/class-08-r7srDh7Iz94vfwwN.htm)|Countercharm|Countercharm|modificada|
|[class-08-rByA8NDI6ZtNgBeT.htm](feats/class-08-rByA8NDI6ZtNgBeT.htm)|Mobile Shot Stance|Posición de disparo múltiple.|modificada|
|[class-08-RHEdvEFz3QKiRKlr.htm](feats/class-08-RHEdvEFz3QKiRKlr.htm)|Topple Giants|Derribar Gigantes|modificada|
|[class-08-RivbJYEBUyfLwPh7.htm](feats/class-08-RivbJYEBUyfLwPh7.htm)|Scarecrow|Espantapájaros|modificada|
|[class-08-rKZE8BA9IQHSSWoW.htm](feats/class-08-rKZE8BA9IQHSSWoW.htm)|Emblazon Energy|Estampar energía.|modificada|
|[class-08-rMPL11JRcmlutvRi.htm](feats/class-08-rMPL11JRcmlutvRi.htm)|Thrash|Sacudirse|modificada|
|[class-08-rpxFVUp8BuF31DYg.htm](feats/class-08-rpxFVUp8BuF31DYg.htm)|Universal Versatility|Versatilidad universal|modificada|
|[class-08-Rqj1kmrnF9M1E6pv.htm](feats/class-08-Rqj1kmrnF9M1E6pv.htm)|Supreme Psychic Center|Centro Psíquico Supremo|modificada|
|[class-08-s2GbcUIXG1ZBurSd.htm](feats/class-08-s2GbcUIXG1ZBurSd.htm)|Hamstringing Strike|Golpe Hamstringing|modificada|
|[class-08-SAOtGk9k8veaX3Ww.htm](feats/class-08-SAOtGk9k8veaX3Ww.htm)|Flamboyant Cruelty|Flamboyant Cruelty|modificada|
|[class-08-sbYDDDUWYN6Qx71k.htm](feats/class-08-sbYDDDUWYN6Qx71k.htm)|Crude Communication|Comunicación Cruda|modificada|
|[class-08-sCkzwTBLyE8FdzWI.htm](feats/class-08-sCkzwTBLyE8FdzWI.htm)|Smoldering Explosion|Explosión humeante|modificada|
|[class-08-SE38R6zpv2XelzZk.htm](feats/class-08-SE38R6zpv2XelzZk.htm)|Call Gun|Llamada Pistola|modificada|
|[class-08-SfBXPmADMFiZIBQt.htm](feats/class-08-SfBXPmADMFiZIBQt.htm)|Call Your Shot|Llama a tu tiro|modificada|
|[class-08-si8FGX2ZRxetdVHp.htm](feats/class-08-si8FGX2ZRxetdVHp.htm)|Constricting Hold|Constricting Hold|modificada|
|[class-08-sIN6t7nCWdI5u1HK.htm](feats/class-08-sIN6t7nCWdI5u1HK.htm)|Spirit's Anguish|Angustia del espíritu|modificada|
|[class-08-Sr75bQtqmCM6dyAM.htm](feats/class-08-Sr75bQtqmCM6dyAM.htm)|Breath Of The Dragon|Aliento de dragón|modificada|
|[class-08-T6Twdesb5niBhHuZ.htm](feats/class-08-T6Twdesb5niBhHuZ.htm)|Jumping Jenny Display|Exhibición de Jenny Saltando|modificada|
|[class-08-tBg3FZST3nX5TfLf.htm](feats/class-08-tBg3FZST3nX5TfLf.htm)|Safeguard Soul|Salvaguardar Alma|modificada|
|[class-08-TDaf3DbWymxPmHrO.htm](feats/class-08-TDaf3DbWymxPmHrO.htm)|What Could Have Been|Lo que podría haber sido|modificada|
|[class-08-TEH73yqZBqByO624.htm](feats/class-08-TEH73yqZBqByO624.htm)|Positioning Assault|Asalto de colocación|modificada|
|[class-08-TGFbFyv0AUi5gAGO.htm](feats/class-08-TGFbFyv0AUi5gAGO.htm)|Monk Moves|Movimientos de monje|modificada|
|[class-08-tH5w8vIfFCFNVP7T.htm](feats/class-08-tH5w8vIfFCFNVP7T.htm)|Invoke the Crimson Oath|Invocar el Juramento Carmesí|modificada|
|[class-08-TOyqtUUnOkOLl1Pm.htm](feats/class-08-TOyqtUUnOkOLl1Pm.htm)|Eclectic Skill|Habilidad ecléctica|modificada|
|[class-08-TzBP8yiZQHNhei1V.htm](feats/class-08-TzBP8yiZQHNhei1V.htm)|Walk The Plank|Walk The Plank|modificada|
|[class-08-u8hKEb71pI45XP1f.htm](feats/class-08-u8hKEb71pI45XP1f.htm)|Minor Omen|Presagio inferior|modificada|
|[class-08-UcIyf7bTDf6RwydU.htm](feats/class-08-UcIyf7bTDf6RwydU.htm)|Makeshift Strike|Golpe improvisado|modificada|
|[class-08-UfuYHdozZD586RWd.htm](feats/class-08-UfuYHdozZD586RWd.htm)|Bloodline Breadth|Abundancia de linaje|modificada|
|[class-08-UIMZe4QDJQ9k4npN.htm](feats/class-08-UIMZe4QDJQ9k4npN.htm)|Stab and Blast|Puñalada y Explosión|modificada|
|[class-08-uR44wELN9OlU68cL.htm](feats/class-08-uR44wELN9OlU68cL.htm)|Advanced Domain|Dominio avanzado|modificada|
|[class-08-URtcR1BU2OgfKHfm.htm](feats/class-08-URtcR1BU2OgfKHfm.htm)|Metabolize Element|Metabolizar Elemento|modificada|
|[class-08-V3nNlrdU2OxYJAjx.htm](feats/class-08-V3nNlrdU2OxYJAjx.htm)|Blessed Spell|Hechizo bendecido|modificada|
|[class-08-vayNZR1bTzU1oUa3.htm](feats/class-08-vayNZR1bTzU1oUa3.htm)|Share Rage|Furia compartida|modificada|
|[class-08-vhHKUooXX3PYqGaU.htm](feats/class-08-vhHKUooXX3PYqGaU.htm)|Bond Conservation|Conservación de vínculo|modificada|
|[class-08-VKNxblSUxYXQYlLr.htm](feats/class-08-VKNxblSUxYXQYlLr.htm)|Terrain Master|Maestro del terreno|modificada|
|[class-08-vNIimhmP636VOP01.htm](feats/class-08-vNIimhmP636VOP01.htm)|Bullet Split|Bullet Split|modificada|
|[class-08-VO8HbMQ79NULE4FQ.htm](feats/class-08-VO8HbMQ79NULE4FQ.htm)|Elaborate Talisman Esoterica|Elaborate Talisman Esoterica|modificada|
|[class-08-VqzXfdbQQ2e8Hndr.htm](feats/class-08-VqzXfdbQQ2e8Hndr.htm)|Unshakable Idealism|Idealismo inquebrantable|modificada|
|[class-08-vUFvcrvszXlHvz2Y.htm](feats/class-08-vUFvcrvszXlHvz2Y.htm)|Magic Arrow|Flecha mágica|modificada|
|[class-08-vVyX9IG8flxg81mc.htm](feats/class-08-vVyX9IG8flxg81mc.htm)|Elude the Divine|Eludir lo divino|modificada|
|[class-08-VZczZNj3ozCj1Lzk.htm](feats/class-08-VZczZNj3ozCj1Lzk.htm)|Second Ally|Segundo aliado|modificada|
|[class-08-WDid62NmmC6NiTE6.htm](feats/class-08-WDid62NmmC6NiTE6.htm)|Smoke Curtain|Cortina de humo|modificada|
|[class-08-wIAiDaNztd52ltT2.htm](feats/class-08-wIAiDaNztd52ltT2.htm)|Positive Luminance|Luminancia positiva|modificada|
|[class-08-WmSggxmf4iAe2aRD.htm](feats/class-08-WmSggxmf4iAe2aRD.htm)|Recycled Cogwheels|Ruedas dentadas recicladas|modificada|
|[class-08-wNHUryoRzlfDCFAd.htm](feats/class-08-wNHUryoRzlfDCFAd.htm)|Soaring Shape|Forma voladora|modificada|
|[class-08-X6nwiA2uJ2UQIOKB.htm](feats/class-08-X6nwiA2uJ2UQIOKB.htm)|Drive Back|Drive Back|modificada|
|[class-08-X71qmyGpKN1XAoT6.htm](feats/class-08-X71qmyGpKN1XAoT6.htm)|Shamble|Shamble|modificada|
|[class-08-x9cYkB8DrUBBwqJd.htm](feats/class-08-x9cYkB8DrUBBwqJd.htm)|Ironblood Stance|Posición de la sangre de hierro|modificada|
|[class-08-xhFsHEsMTAvzwJhr.htm](feats/class-08-xhFsHEsMTAvzwJhr.htm)|Incredible Reanimated Companion|Comparo sensacional reanimado|modificada|
|[class-08-XkmrLhyAoxQTLnza.htm](feats/class-08-XkmrLhyAoxQTLnza.htm)|Lobbed Attack|Lobbed Attack|modificada|
|[class-08-XoxawV3Fmn61VJcS.htm](feats/class-08-XoxawV3Fmn61VJcS.htm)|Steal Vitality|Robar Vitalidad|modificada|
|[class-08-XRIWWrXfghsQce4S.htm](feats/class-08-XRIWWrXfghsQce4S.htm)|Divine Breadth|Abundancia divina|modificada|
|[class-08-xtXWw3cUnVB25XSV.htm](feats/class-08-xtXWw3cUnVB25XSV.htm)|Align Armament|Alinear armamento|modificada|
|[class-08-XY2uS7pMKRLVNQKG.htm](feats/class-08-XY2uS7pMKRLVNQKG.htm)|Fey's Trickery|Engaño feérico|modificada|
|[class-08-y7SYHv0DWkkwjT95.htm](feats/class-08-y7SYHv0DWkkwjT95.htm)|Retaliatory Cleansing|Limpieza de represalias|modificada|
|[class-08-YDpSnLmnSLsItP45.htm](feats/class-08-YDpSnLmnSLsItP45.htm)|Improvised Critical|Crítico improvisado|modificada|
|[class-08-YeyOqNFKaeuOTiJr.htm](feats/class-08-YeyOqNFKaeuOTiJr.htm)|Impassable Wall Stance|Posición de muro infranqueable|modificada|
|[class-08-yg1ZTjHuSiQJFO0i.htm](feats/class-08-yg1ZTjHuSiQJFO0i.htm)|Future Spell Learning|Futuro aprendizaje de conjuros|modificada|
|[class-08-yIDJTe4kimRoZN0L.htm](feats/class-08-yIDJTe4kimRoZN0L.htm)|Controlled Blast|Ráfaga Controlada|modificada|
|[class-08-Yj0IKXM49Ver5Smc.htm](feats/class-08-Yj0IKXM49Ver5Smc.htm)|Siphoning Touch|Siphoning Touch|modificada|
|[class-08-YMrcnO2l3mA71ohM.htm](feats/class-08-YMrcnO2l3mA71ohM.htm)|Frozen Breadth|Anchura Congelada|modificada|
|[class-08-yOybxBkebeeuHWuy.htm](feats/class-08-yOybxBkebeeuHWuy.htm)|Magical Knowledge|Conocimiento Mágico|modificada|
|[class-08-YrRlbIzjsFlGGmVN.htm](feats/class-08-YrRlbIzjsFlGGmVN.htm)|Black Powder Blaze|Black Powder Blaze|modificada|
|[class-08-YTZhLWtrEnV9Pjf2.htm](feats/class-08-YTZhLWtrEnV9Pjf2.htm)|Bravo's Determination|Determinación de Bravo|modificada|
|[class-08-Yvxr1Q2TslWiKqqi.htm](feats/class-08-Yvxr1Q2TslWiKqqi.htm)|Occult Breadth|Abundancia ocultista|modificada|
|[class-08-Yy9LXo2GaLT7eCOL.htm](feats/class-08-Yy9LXo2GaLT7eCOL.htm)|Deadly Butterfly|Mariposa Letal|modificada|
|[class-08-YYPjndZ7tqRQLtAH.htm](feats/class-08-YYPjndZ7tqRQLtAH.htm)|Beacon Mark|Beacon Mark|modificada|
|[class-08-zAJY8rLvZjzOK0Mt.htm](feats/class-08-zAJY8rLvZjzOK0Mt.htm)|Sin Reservoir|Sin Reservoir|modificada|
|[class-08-ZdL8pPPV0QCkBML1.htm](feats/class-08-ZdL8pPPV0QCkBML1.htm)|Subtle Shank|Vástago Sutil|modificada|
|[class-08-zf6Poru1jNmrO3kk.htm](feats/class-08-zf6Poru1jNmrO3kk.htm)|Bloodline Resistance|Resistencia de linaje|modificada|
|[class-08-zJXsGF61lH0WHw5v.htm](feats/class-08-zJXsGF61lH0WHw5v.htm)|Ghost Flight|Vuelo Fantasma|modificada|
|[class-08-ZlL18xGsa76isqkX.htm](feats/class-08-ZlL18xGsa76isqkX.htm)|Runic Impression|Runic Impression|modificada|
|[class-08-Zn2uUL5i3MAZ0Zwc.htm](feats/class-08-Zn2uUL5i3MAZ0Zwc.htm)|Standby Spell|Hechizo de espera|modificada|
|[class-08-zQeglKcmBXvqfABR.htm](feats/class-08-zQeglKcmBXvqfABR.htm)|Brain Drain|Fuga de Cerebros|modificada|
|[class-08-ztxbGySxIEeWvsAT.htm](feats/class-08-ztxbGySxIEeWvsAT.htm)|Pact of Infernal Prowess|Pacto de Proeza Infernal|modificada|
|[class-08-ZXaDS4OJvsQYvhBZ.htm](feats/class-08-ZXaDS4OJvsQYvhBZ.htm)|Submission Hold|Submission Hold|modificada|
|[class-08-zXKfKKOxht0b0XNL.htm](feats/class-08-zXKfKKOxht0b0XNL.htm)|Sticky Bomb|Bomba adhesiva|modificada|
|[class-08-ZyQYP7i26DWhMNux.htm](feats/class-08-ZyQYP7i26DWhMNux.htm)|Unfazed Assessment|Evaluación imperturbable|modificada|
|[class-08-zytTsipimVTmPc5U.htm](feats/class-08-zytTsipimVTmPc5U.htm)|Selfless Parry|Parada desinteresada|modificada|
|[class-10-04Smj4ZsEBD8WIXv.htm](feats/class-10-04Smj4ZsEBD8WIXv.htm)|Dual-Weapon Blitz|Dual-Weapon Blitz|modificada|
|[class-10-07jxXvRZ8nD3JLF4.htm](feats/class-10-07jxXvRZ8nD3JLF4.htm)|Imposing Destrier|Destrero imponente|modificada|
|[class-10-1F4FD5OdDCEyiEvk.htm](feats/class-10-1F4FD5OdDCEyiEvk.htm)|Deny the Songs of War|Negar las Canciones de Guerra|modificada|
|[class-10-1k3H7cnARIzAVCsm.htm](feats/class-10-1k3H7cnARIzAVCsm.htm)|Fearsome Brute|Bruto temible|modificada|
|[class-10-23QgyEYjoslBvkra.htm](feats/class-10-23QgyEYjoslBvkra.htm)|Invigorating Breath|Invigorating Breath|modificada|
|[class-10-2Nu4ZdKQM8hx8x5D.htm](feats/class-10-2Nu4ZdKQM8hx8x5D.htm)|Sneak Savant|Sabio de la furtividad|modificada|
|[class-10-2OYJOFaEkhc8dFbl.htm](feats/class-10-2OYJOFaEkhc8dFbl.htm)|Knockback Strike|Golpe de revés|modificada|
|[class-10-2uQbQgz1AbjzcFSp.htm](feats/class-10-2uQbQgz1AbjzcFSp.htm)|Runic Mind Smithing|Runic Mind Smithing|modificada|
|[class-10-2xxFg9yRuCDpME3z.htm](feats/class-10-2xxFg9yRuCDpME3z.htm)|Spy's Countermeasures|Contramedidas del Espía|modificada|
|[class-10-3poGYUYCBTmbeCUs.htm](feats/class-10-3poGYUYCBTmbeCUs.htm)|Angel Of Death|Ángel de la Muerte|modificada|
|[class-10-3R09Hl6IDMgPcSs0.htm](feats/class-10-3R09Hl6IDMgPcSs0.htm)|Beastmaster Bond|Beastmaster Bond|modificada|
|[class-10-4AezsqaQRFtX024w.htm](feats/class-10-4AezsqaQRFtX024w.htm)|Shall not Falter, Shall not Rout|Shall not Falter, Shall not Rout|modificada|
|[class-10-4Cc0gQauzUqcYdLw.htm](feats/class-10-4Cc0gQauzUqcYdLw.htm)|Uncanny Dodge|Esquiva asombrosa|modificada|
|[class-10-4VbfFPuFpbGLMMKF.htm](feats/class-10-4VbfFPuFpbGLMMKF.htm)|Disarming Twist|Giro desarmante|modificada|
|[class-10-4w73cxvqhGRiozsK.htm](feats/class-10-4w73cxvqhGRiozsK.htm)|Bear Empathy|Empatía del Oso|modificada|
|[class-10-4YqmeJQwnrG1Lg07.htm](feats/class-10-4YqmeJQwnrG1Lg07.htm)|Phenom's Verve|Phenom's Verve|modificada|
|[class-10-5csEOLLbYUWJDJoS.htm](feats/class-10-5csEOLLbYUWJDJoS.htm)|Master Monster Hunter|Cazador de monstruos maestro|modificada|
|[class-10-5GC2iGtVp3UAH2nm.htm](feats/class-10-5GC2iGtVp3UAH2nm.htm)|Glutton For Flesh|Glutton For Flesh|modificada|
|[class-10-5nzeyuvzKL4T8eLt.htm](feats/class-10-5nzeyuvzKL4T8eLt.htm)|Weapon-Rune Shifter|Weapon-Rune Shifter|modificada|
|[class-10-5tlTRfyPXkGS9Coq.htm](feats/class-10-5tlTRfyPXkGS9Coq.htm)|Lunging Spellstrike|Lunging Spellstrike|modificada|
|[class-10-5TPKikTyN7lrCvzY.htm](feats/class-10-5TPKikTyN7lrCvzY.htm)|Mighty Bulwark|Mighty Bulwark|modificada|
|[class-10-6dJokwhIMvjAHL52.htm](feats/class-10-6dJokwhIMvjAHL52.htm)|Practiced Reloads|Practiced Reloads|modificada|
|[class-10-6Hv4kpHj8IexNUey.htm](feats/class-10-6Hv4kpHj8IexNUey.htm)|Tiller's Aid|Prestar ayuda al labrador|modificada|
|[class-10-6IrNSD0G5AAXPTJq.htm](feats/class-10-6IrNSD0G5AAXPTJq.htm)|Borrow Time|Borrow Time|modificada|
|[class-10-7fyWKASX6uNByJF5.htm](feats/class-10-7fyWKASX6uNByJF5.htm)|Pass Through|Pass Through|modificada|
|[class-10-7nAoHfB7nsRwWDmF.htm](feats/class-10-7nAoHfB7nsRwWDmF.htm)|Expert Skysage Divination|Experto en adivinación del cielo|modificada|
|[class-10-7x1r2w7C7A4Uy7wG.htm](feats/class-10-7x1r2w7C7A4Uy7wG.htm)|Topple Foe|Topple Foe|modificada|
|[class-10-81xlcIFJIAtsmXmd.htm](feats/class-10-81xlcIFJIAtsmXmd.htm)|Armor Rune Shifter|Cambiador de Runas de Armadura|modificada|
|[class-10-85V3vdew0gykEtmu.htm](feats/class-10-85V3vdew0gykEtmu.htm)|Healing Transformation|Transformación curativa.|modificada|
|[class-10-85xm82Z005CUNBMB.htm](feats/class-10-85xm82Z005CUNBMB.htm)|Twin Weakness|Debilidad Gemela|modificada|
|[class-10-8d3QxoKmqSkB9Mcj.htm](feats/class-10-8d3QxoKmqSkB9Mcj.htm)|Endure Death's Touch|Endure Death's Touch|modificada|
|[class-10-8NQaqtHheeMUNGYr.htm](feats/class-10-8NQaqtHheeMUNGYr.htm)|Shared Synergy|Sinergia Compartida|modificada|
|[class-10-9Qn5E7Ujye9KdxOj.htm](feats/class-10-9Qn5E7Ujye9KdxOj.htm)|Recover Spell|Recuperar hechizo|modificada|
|[class-10-9uvymmdphxsD3yEd.htm](feats/class-10-9uvymmdphxsD3yEd.htm)|Dazzling Block|Bloqueo Deslumbrante|modificada|
|[class-10-9XXtDeRF2egCCzcx.htm](feats/class-10-9XXtDeRF2egCCzcx.htm)|Tectonic Stomp|Pisotón Tectónico|modificada|
|[class-10-A0ZQpGSB7pvIDiou.htm](feats/class-10-A0ZQpGSB7pvIDiou.htm)|Improved Command Undead|Comandar muertos vivientes mejorado|modificada|
|[class-10-a2VAaXMzlQqALJeH.htm](feats/class-10-a2VAaXMzlQqALJeH.htm)|Share Weakness|Compartir debilidades|modificada|
|[class-10-aajcuwKYihsinPCt.htm](feats/class-10-aajcuwKYihsinPCt.htm)|Wronged Monk's Wrath|Wronged Monk's Wrath|modificada|
|[class-10-ADgQzThbtGKvp6hy.htm](feats/class-10-ADgQzThbtGKvp6hy.htm)|Bestiary Scholar|Erudito del Bestiario|modificada|
|[class-10-AGovcdAzmUqxgiOq.htm](feats/class-10-AGovcdAzmUqxgiOq.htm)|Fused Polearm|Fused Polearm|modificada|
|[class-10-aMef2VM4mSxl0pmy.htm](feats/class-10-aMef2VM4mSxl0pmy.htm)|Replenishment of War|Reabastecimiento de guerra|modificada|
|[class-10-amf2zZuW299eiHAZ.htm](feats/class-10-amf2zZuW299eiHAZ.htm)|Everyone Duck!|¡Agáchense todos!|modificada|
|[class-10-AMgTG6TwfoNmseWm.htm](feats/class-10-AMgTG6TwfoNmseWm.htm)|Channel Rot|Canalizar putridez|modificada|
|[class-10-aVzkG1UhNjQr22pE.htm](feats/class-10-aVzkG1UhNjQr22pE.htm)|I Meant to do That|I Meant to do That|modificada|
|[class-10-aWk0JaqVaUf2Cz9a.htm](feats/class-10-aWk0JaqVaUf2Cz9a.htm)|Twin Shot Knockdown|Derribo con doble disparo|modificada|
|[class-10-AYByEEgPXk3QbCiF.htm](feats/class-10-AYByEEgPXk3QbCiF.htm)|Silent Sting|Silent Sting|modificada|
|[class-10-b7KZ7Fg5u5z2gqvt.htm](feats/class-10-b7KZ7Fg5u5z2gqvt.htm)|Called Shot|Tiro Llamado|modificada|
|[class-10-B7VMXObJSNVI0ZGJ.htm](feats/class-10-B7VMXObJSNVI0ZGJ.htm)|Shattering Strike (Weapon Improviser)|Golpe demoledor (Mejorador de armas)|modificada|
|[class-10-bBORql3kKsSyXllk.htm](feats/class-10-bBORql3kKsSyXllk.htm)|Monk's Flurry|Ráfaga de monje|modificada|
|[class-10-BBPrlPncXg86I42D.htm](feats/class-10-BBPrlPncXg86I42D.htm)|Counterclockwork Focus|Counterclockwork Focus|modificada|
|[class-10-bJc477EbUYW2TlBx.htm](feats/class-10-bJc477EbUYW2TlBx.htm)|Drain Vitality|Drenar Vitalidad|modificada|
|[class-10-BJKTUGplI9nwhJxg.htm](feats/class-10-BJKTUGplI9nwhJxg.htm)|Agile Grace|Gracia ágil|modificada|
|[class-10-bqBCatllklPceA34.htm](feats/class-10-bqBCatllklPceA34.htm)|Turn to Mist|Turn to Mist|modificada|
|[class-10-C6BIHPqYiWM0wlvv.htm](feats/class-10-C6BIHPqYiWM0wlvv.htm)|Signature Spell Expansion (Psychic)|Expansión de conjuros de signatura (psíquico)|modificada|
|[class-10-CMDCWPC8m2b6HvEN.htm](feats/class-10-CMDCWPC8m2b6HvEN.htm)|Spiritual Flurry|Ráfaga Espiritual|modificada|
|[class-10-Cn4w9U7uk5m1bb2S.htm](feats/class-10-Cn4w9U7uk5m1bb2S.htm)|Greater Debilitating Bomb|Bomba debilitante mayor.|modificada|
|[class-10-cQ2jy4Utoxmk3MkG.htm](feats/class-10-cQ2jy4Utoxmk3MkG.htm)|Reactive Charm|Hechizo Reactivo|modificada|
|[class-10-cTeRwExCGhfwJbkl.htm](feats/class-10-cTeRwExCGhfwJbkl.htm)|Sixth Pillar Dedication|Dedicación al Sexto Pilar|modificada|
|[class-10-cxAvcFiZRT3ZVhie.htm](feats/class-10-cxAvcFiZRT3ZVhie.htm)|Emerald Boughs Hideaway|Escondite Rama esmeralda|modificada|
|[class-10-cznEQ1W61MSaXW0u.htm](feats/class-10-cznEQ1W61MSaXW0u.htm)|Great Cleave|Gran hendedura|modificada|
|[class-10-D6bncWAOvFYX7hCE.htm](feats/class-10-D6bncWAOvFYX7hCE.htm)|Strident Command|Orden imperiosa|modificada|
|[class-10-d8DI7wLxtUy99g9K.htm](feats/class-10-d8DI7wLxtUy99g9K.htm)|Emotional Surge|Oleaje Emocional|modificada|
|[class-10-DFtbxytrOrmkQRfm.htm](feats/class-10-DFtbxytrOrmkQRfm.htm)|Steal the Sky|Sustraer el cielo|modificada|
|[class-10-dnm6c3nzGDHt3A76.htm](feats/class-10-dnm6c3nzGDHt3A76.htm)|Greenwatcher|Greenwatcher|modificada|
|[class-10-DPaZurhC9uyIWPcu.htm](feats/class-10-DPaZurhC9uyIWPcu.htm)|Magical Adaptation|Magical Adaptation|modificada|
|[class-10-DpRMdytpPiCypmkJ.htm](feats/class-10-DpRMdytpPiCypmkJ.htm)|Greater Bloodline|Linaje mayor|modificada|
|[class-10-dutiFC41YFllm8fM.htm](feats/class-10-dutiFC41YFllm8fM.htm)|House of Imaginary Walls|La casa de los muros imaginarios|modificada|
|[class-10-dY0jhJOEj6DHc0ud.htm](feats/class-10-dY0jhJOEj6DHc0ud.htm)|Destructive Block|Bloque Destructivo|modificada|
|[class-10-DZcy4sY07w0zPsDb.htm](feats/class-10-DZcy4sY07w0zPsDb.htm)|Rapid Recharge|Recarga rápida|modificada|
|[class-10-dzeiVab4Cogzl5XS.htm](feats/class-10-dzeiVab4Cogzl5XS.htm)|Gift of the Hoard|Regalo del tesoro|modificada|
|[class-10-E1WXnYE2QwhHQxQb.htm](feats/class-10-E1WXnYE2QwhHQxQb.htm)|Sleeper Hold|Inmovilización durmiente|modificada|
|[class-10-EBqnbiuVL0ULFJWX.htm](feats/class-10-EBqnbiuVL0ULFJWX.htm)|Know It All (Eldritch Researcher)|Sabelotodo (investigador de lo sobrenatural)|modificada|
|[class-10-eceQmgvmoPplFKId.htm](feats/class-10-eceQmgvmoPplFKId.htm)|Rope Mastery|Dominio de la cuerda|modificada|
|[class-10-EpWgrMznGbm8gceW.htm](feats/class-10-EpWgrMznGbm8gceW.htm)|Cautious Delver|Cavador Cauteloso|modificada|
|[class-10-F0MYBfiyOD8YHq5t.htm](feats/class-10-F0MYBfiyOD8YHq5t.htm)|Elemental Shape|Forma elemental|modificada|
|[class-10-F3ZBkDEWZ24NOR2j.htm](feats/class-10-F3ZBkDEWZ24NOR2j.htm)|Saving Slash|Saving Slash|modificada|
|[class-10-fCsIyglmpb7NYwiy.htm](feats/class-10-fCsIyglmpb7NYwiy.htm)|Harrying Strike|Golpe Harrying|modificada|
|[class-10-FIsMdgvGji5Nci8l.htm](feats/class-10-FIsMdgvGji5Nci8l.htm)|Devoted Focus|Foco devoto|modificada|
|[class-10-FLQWJ2CIv9mCfSAx.htm](feats/class-10-FLQWJ2CIv9mCfSAx.htm)|Elastic Mutagen|Mutágeno elástico|modificada|
|[class-10-FWIQ3m5KZDWzDg47.htm](feats/class-10-FWIQ3m5KZDWzDg47.htm)|Wind Jump|Salto del viento|modificada|
|[class-10-fzERYW7BJQoxlvoD.htm](feats/class-10-fzERYW7BJQoxlvoD.htm)|Corpse-Killer's Defiance|Desafío del asesino de cadáveres|modificada|
|[class-10-gdGusdEKt5zmh3rR.htm](feats/class-10-gdGusdEKt5zmh3rR.htm)|Pushing Attack|Pushing Attack|modificada|
|[class-10-GJIAecRq1bD2r8O0.htm](feats/class-10-GJIAecRq1bD2r8O0.htm)|Twin Riposte|Réplica gemela|modificada|
|[class-10-gSc4ZUXkesN5vKrm.htm](feats/class-10-gSc4ZUXkesN5vKrm.htm)|Debilitating Shot|Disparo debilitante|modificada|
|[class-10-gxzTEt37M0z1WY1M.htm](feats/class-10-gxzTEt37M0z1WY1M.htm)|Spinebreaker|Rompeespinazos|modificada|
|[class-10-gyVcJfZTmBytLsXq.htm](feats/class-10-gyVcJfZTmBytLsXq.htm)|Expanded Splash|Salpicadura extendida|modificada|
|[class-10-h5DzKmKDADGhvmF9.htm](feats/class-10-h5DzKmKDADGhvmF9.htm)|Certain Strike|Golpe certero|modificada|
|[class-10-h5ksUZlrVGBjq6p4.htm](feats/class-10-h5ksUZlrVGBjq6p4.htm)|Radiant Blade Spirit|Espíritu del filo radiante|modificada|
|[class-10-HgBksiMTUibPK36M.htm](feats/class-10-HgBksiMTUibPK36M.htm)|Halcyon Spellcasting Initiate|Halcyon Spellcasting Initiate|modificada|
|[class-10-HjinlKihkadhkQ4Z.htm](feats/class-10-HjinlKihkadhkQ4Z.htm)|Terrifying Howl|Aullido terrorífico|modificada|
|[class-10-HKNOA5WGBWGEpdmH.htm](feats/class-10-HKNOA5WGBWGEpdmH.htm)|Redirecting Shot|Redirigir Tiro|modificada|
|[class-10-HlqAdfxmcd9gdgHa.htm](feats/class-10-HlqAdfxmcd9gdgHa.htm)|Draw From the Land|Draw From the Land|modificada|
|[class-10-hRJV7byfPUHx1b9P.htm](feats/class-10-hRJV7byfPUHx1b9P.htm)|Greater Spell Runes|Runas de hechizo mayores|modificada|
|[class-10-hsMcKK92ho39djgI.htm](feats/class-10-hsMcKK92ho39djgI.htm)|Communal Tale|Cuento Comunal|modificada|
|[class-10-i8MnyasCDo3j65Xd.htm](feats/class-10-i8MnyasCDo3j65Xd.htm)|Quicken Heartbeat|Latido acelerado|modificada|
|[class-10-I9rSWQyueWHQyNxe.htm](feats/class-10-I9rSWQyueWHQyNxe.htm)|Plant Shape|Forma de planta|modificada|
|[class-10-IeiTuTZExH5DQOqH.htm](feats/class-10-IeiTuTZExH5DQOqH.htm)|Burrowing Form|Forma de madriguera|modificada|
|[class-10-j5Xjr8vZuBhCixIr.htm](feats/class-10-j5Xjr8vZuBhCixIr.htm)|Side by Side (Druid)|Codo con codo (druida)|modificada|
|[class-10-jBHDVsTVOBeoMoO4.htm](feats/class-10-jBHDVsTVOBeoMoO4.htm)|Greater Magical Edification|Mayor Edificación Mágica|modificada|
|[class-10-Jnhkl2BOhxxRCTpp.htm](feats/class-10-Jnhkl2BOhxxRCTpp.htm)|Vigil's Walls Rise Anew|Los muros de la Vigilia se levantan de nuevo|modificada|
|[class-10-JQs2O2TTgKWXgJgZ.htm](feats/class-10-JQs2O2TTgKWXgJgZ.htm)|Consecrate Spell|Consagrar Hechizo|modificada|
|[class-10-Jv24QkykqdPB7brL.htm](feats/class-10-Jv24QkykqdPB7brL.htm)|Unbreakable Bond|Lazo irrompible|modificada|
|[class-10-jvQoupE76OeUpjZp.htm](feats/class-10-jvQoupE76OeUpjZp.htm)|Scout's Pounce|Abalanzarse Explorador|modificada|
|[class-10-JwosaDYoqfPiFMYa.htm](feats/class-10-JwosaDYoqfPiFMYa.htm)|Eidetic Memorization|Memorización Eidética|modificada|
|[class-10-JxZobgFhLBNcFCwE.htm](feats/class-10-JxZobgFhLBNcFCwE.htm)|Dream Guise|Sue Guise|modificada|
|[class-10-K0gyvvX0S2FdJZ5T.htm](feats/class-10-K0gyvvX0S2FdJZ5T.htm)|Weighty Impact|Impacto de peso|modificada|
|[class-10-K0x90HNHi5hONdDl.htm](feats/class-10-K0x90HNHi5hONdDl.htm)|Beast Dynamo Howl|Aullido de Dinamo de Bestia|modificada|
|[class-10-KcNXoSvULnuQjC9a.htm](feats/class-10-KcNXoSvULnuQjC9a.htm)|Incredible Familiar (Familiar Master)|Increíble Familiar (Maestro Familiar)|modificada|
|[class-10-Km6YO7Ky2bEwhAFD.htm](feats/class-10-Km6YO7Ky2bEwhAFD.htm)|Greater Awakened Power|Mayor Poder Despertado|modificada|
|[class-10-KNhmeUpZCSW5eHqg.htm](feats/class-10-KNhmeUpZCSW5eHqg.htm)|Scour The Library|Scour The Library|modificada|
|[class-10-knJlRpltciLNTZba.htm](feats/class-10-knJlRpltciLNTZba.htm)|Protective Bond|Vínculo de protección|modificada|
|[class-10-KNtnb9HwPnPDY2Mv.htm](feats/class-10-KNtnb9HwPnPDY2Mv.htm)|Wide Overwatch|Wide Overwatch|modificada|
|[class-10-kQEIPYoKTt69yXxV.htm](feats/class-10-kQEIPYoKTt69yXxV.htm)|Mirror Shield|Escudo espejo|modificada|
|[class-10-KR78kinMmAZQHeoa.htm](feats/class-10-KR78kinMmAZQHeoa.htm)|Heroic Recovery|Recuperación heroica|modificada|
|[class-10-L7hs5XOCbJmh0H0e.htm](feats/class-10-L7hs5XOCbJmh0H0e.htm)|Expand Spiral|Expandir Espiral|modificada|
|[class-10-lIr2kC561L7oX290.htm](feats/class-10-lIr2kC561L7oX290.htm)|Rebounding Assault|Asalto de rebote|modificada|
|[class-10-lKeJruYQutWlNXyZ.htm](feats/class-10-lKeJruYQutWlNXyZ.htm)|Martial Exercise|Ejercicio Marcial|modificada|
|[class-10-lszcn7eO3olp5vEt.htm](feats/class-10-lszcn7eO3olp5vEt.htm)|Grave Sight|Grave Sight|modificada|
|[class-10-lTwi4lyVk1UXomhK.htm](feats/class-10-lTwi4lyVk1UXomhK.htm)|Sustaining Steel|Acero de sustentación|modificada|
|[class-10-LvmYfUGX3uDCpIHY.htm](feats/class-10-LvmYfUGX3uDCpIHY.htm)|Electrify Armor|Electrify Armor|modificada|
|[class-10-McnLGEZnUbtYCNDW.htm](feats/class-10-McnLGEZnUbtYCNDW.htm)|Lock On|Cerrar|modificada|
|[class-10-MgLUbsvAkIA4fsZW.htm](feats/class-10-MgLUbsvAkIA4fsZW.htm)|Gaze of Veracity|Mirada de Veracidad|modificada|
|[class-10-MLiEMjyZXE43wmrq.htm](feats/class-10-MLiEMjyZXE43wmrq.htm)|Instant Armor|Armadura instantánea|modificada|
|[class-10-mnhsG4l53YJkJIeY.htm](feats/class-10-mnhsG4l53YJkJIeY.htm)|Widen the Gap|Widen the Gap|modificada|
|[class-10-muDbZAyrE1ObyuTL.htm](feats/class-10-muDbZAyrE1ObyuTL.htm)|Beneath Notice|Debajo de descubrimiento|modificada|
|[class-10-muyEI60L0FmCHuWb.htm](feats/class-10-muyEI60L0FmCHuWb.htm)|Ensnaring Wrappings|Enredar Envolturas|modificada|
|[class-10-Mvw3ZFrdwMHedAxY.htm](feats/class-10-Mvw3ZFrdwMHedAxY.htm)|Merciful Elixir|Elixir compasivo|modificada|
|[class-10-n0693gmx3k9wvb1N.htm](feats/class-10-n0693gmx3k9wvb1N.htm)|Demanding Challenge|Exigencia de Desafío|modificada|
|[class-10-N0gJ4Q69nslbdXHg.htm](feats/class-10-N0gJ4Q69nslbdXHg.htm)|Command Attention|Orden imperiosa Atención|modificada|
|[class-10-N16lctDPZpvk9Khq.htm](feats/class-10-N16lctDPZpvk9Khq.htm)|Shadow Sneak Attack|Sombra Ataque furtivo|modificada|
|[class-10-nDNmqez9McmrjBAV.htm](feats/class-10-nDNmqez9McmrjBAV.htm)|Attunement to Stone|Attunement to Stone|modificada|
|[class-10-NLjxAO9Mhx3k7gpc.htm](feats/class-10-NLjxAO9Mhx3k7gpc.htm)|Winter's Embrace|Abrazo del invierno|modificada|
|[class-10-nobsCgNmsDX6aKR5.htm](feats/class-10-nobsCgNmsDX6aKR5.htm)|Rain-Scribe Mobility|Movilidad escriba de la lluvia|modificada|
|[class-10-NPhH9XkHV0DG4WS9.htm](feats/class-10-NPhH9XkHV0DG4WS9.htm)|Transpose|Transponer|modificada|
|[class-10-nx7UPAwAyno2rM9f.htm](feats/class-10-nx7UPAwAyno2rM9f.htm)|Thaumaturge's Investiture|Investidura de taumaturgo|modificada|
|[class-10-nZK5oGakaSZcSOs1.htm](feats/class-10-nZK5oGakaSZcSOs1.htm)|Unbelievable Escape|Increíble Huir|modificada|
|[class-10-O1C6pMTOxIrWBO3G.htm](feats/class-10-O1C6pMTOxIrWBO3G.htm)|Daring Flourish|Floritura Atrevida|modificada|
|[class-10-o8ogNJ53l1JDIJud.htm](feats/class-10-o8ogNJ53l1JDIJud.htm)|Font of Knowledge|Fuente del Conocimiento|modificada|
|[class-10-o8qiAUuDHfurmgpP.htm](feats/class-10-o8qiAUuDHfurmgpP.htm)|Dimensional Disappearance|Desaparición dimensional|modificada|
|[class-10-Ob4w36tsd1WKSxZo.htm](feats/class-10-Ob4w36tsd1WKSxZo.htm)|Eerie Proclamation|Eerie Proclamation|modificada|
|[class-10-OCvgmAFz4qgj2Scf.htm](feats/class-10-OCvgmAFz4qgj2Scf.htm)|Stalwart Standard|Stalwart Standard|modificada|
|[class-10-oD4JyvTJj4kwe5vb.htm](feats/class-10-oD4JyvTJj4kwe5vb.htm)|Warden's Step|Paso del guardián|modificada|
|[class-10-OEGhbRgW6wRbccns.htm](feats/class-10-OEGhbRgW6wRbccns.htm)|Disruptive Stance|Posición perturbadora|modificada|
|[class-10-oiauCibmdgJ91kNI.htm](feats/class-10-oiauCibmdgJ91kNI.htm)|Cascading Ray|Rayo en cascada|modificada|
|[class-10-oltE3dIYZlM8s5BI.htm](feats/class-10-oltE3dIYZlM8s5BI.htm)|Blazing Streak|Blazing Streak|modificada|
|[class-10-OmjTt8eR1Q3SmkPp.htm](feats/class-10-OmjTt8eR1Q3SmkPp.htm)|Armored Rest|Armored Rest|modificada|
|[class-10-orTWiRwIQEc9FGJQ.htm](feats/class-10-orTWiRwIQEc9FGJQ.htm)|Fading|Fading|modificada|
|[class-10-otBBb0ndASgPdAXW.htm](feats/class-10-otBBb0ndASgPdAXW.htm)|Penetrating Shot|Disparo perforante|modificada|
|[class-10-p2I4o9Cc6UrXvjhO.htm](feats/class-10-p2I4o9Cc6UrXvjhO.htm)|Furious Sprint|Carrera furiosa|modificada|
|[class-10-phFRWFgeHdBzio2V.htm](feats/class-10-phFRWFgeHdBzio2V.htm)|Terrain Form|Forma del terreno|modificada|
|[class-10-pI5cG6x49ZQLchuy.htm](feats/class-10-pI5cG6x49ZQLchuy.htm)|Goblin Jubilee Display|Goblin Jubilee Display|modificada|
|[class-10-PlRAfeNof3xgJttZ.htm](feats/class-10-PlRAfeNof3xgJttZ.htm)|Surrounding Flames|Flamígera envolvente|modificada|
|[class-10-PSqxbLzKKzbMUnU9.htm](feats/class-10-PSqxbLzKKzbMUnU9.htm)|Entwined Energy Ki|Entwined Energy Ki|modificada|
|[class-10-pT6r5BVJjALJUmb3.htm](feats/class-10-pT6r5BVJjALJUmb3.htm)|Tag Team|Tag Team|modificada|
|[class-10-pVLdMOqYwul745k3.htm](feats/class-10-pVLdMOqYwul745k3.htm)|Knockback|Arredrar|modificada|
|[class-10-qbFRPTHP96Q9kGpk.htm](feats/class-10-qbFRPTHP96Q9kGpk.htm)|Flash Your Badge|Flash Your Badge|modificada|
|[class-10-QHh442n2CEtJr23B.htm](feats/class-10-QHh442n2CEtJr23B.htm)|Greater Deathly Secrets|Mayores secretos mortales|modificada|
|[class-10-QhKLXGJPLZaX1qDp.htm](feats/class-10-QhKLXGJPLZaX1qDp.htm)|Expert Captivator Spellcasting|Lanzador de conjuros experto cautivador|modificada|
|[class-10-Qjn9ErWY6wuIK9z6.htm](feats/class-10-Qjn9ErWY6wuIK9z6.htm)|Ward Casting|Ward Casting|modificada|
|[class-10-qo6oKL8mE32hSjSC.htm](feats/class-10-qo6oKL8mE32hSjSC.htm)|Slayer's Presence|Presencia de Cazadora|modificada|
|[class-10-qqe5VmcO1s8iTOfd.htm](feats/class-10-qqe5VmcO1s8iTOfd.htm)|Camouflage|Camuflaje|modificada|
|[class-10-qUSWPWxYF8gfhfHM.htm](feats/class-10-qUSWPWxYF8gfhfHM.htm)|Helpful Tinkering|Trastear útil|modificada|
|[class-10-R7EaZPYtsy5H8lwn.htm](feats/class-10-R7EaZPYtsy5H8lwn.htm)|Merciless Rend|Rasgadura despiadada|modificada|
|[class-10-REgTwIVgI3j1FQiJ.htm](feats/class-10-REgTwIVgI3j1FQiJ.htm)|Harden Flesh|Endurecer Carne|modificada|
|[class-10-RfOnsZrmT6z2ajBN.htm](feats/class-10-RfOnsZrmT6z2ajBN.htm)|Siphon Life|Succionar vida|modificada|
|[class-10-RLWbx2itF1jmW0OL.htm](feats/class-10-RLWbx2itF1jmW0OL.htm)|Shadow Reservoir|Sombra Reservoir|modificada|
|[class-10-RtYOsmb9R71J9ce2.htm](feats/class-10-RtYOsmb9R71J9ce2.htm)|Hilt Hammer|Hilt Hammer|modificada|
|[class-10-RWccAJJ2PaOI0Byp.htm](feats/class-10-RWccAJJ2PaOI0Byp.htm)|Litany Against Sloth|Letanía contra la pereza|modificada|
|[class-10-rzaoi5Roef9zO22G.htm](feats/class-10-rzaoi5Roef9zO22G.htm)|Combat Reflexes|Reflejos de combate|modificada|
|[class-10-s0P4JXagA3085wLW.htm](feats/class-10-s0P4JXagA3085wLW.htm)|Toppling Tentacles|Derribar Tentáculos|modificada|
|[class-10-S36WQ8o2MvPxQp0p.htm](feats/class-10-S36WQ8o2MvPxQp0p.htm)|Menacing Prowess|Proeza Amenazadora|modificada|
|[class-10-SGBLDcT4wI5VUDCZ.htm](feats/class-10-SGBLDcT4wI5VUDCZ.htm)|Emissary of Peace|Emisario de la Paz|modificada|
|[class-10-SgXvw6rzk2lhTpXL.htm](feats/class-10-SgXvw6rzk2lhTpXL.htm)|Giant Snare|Lazo Gigante|modificada|
|[class-10-sTUicpQkhiFVtMK1.htm](feats/class-10-sTUicpQkhiFVtMK1.htm)|Tempest-Sun Shielding|Escudo tempestad-sol|modificada|
|[class-10-stwRJTOKUru9AmQC.htm](feats/class-10-stwRJTOKUru9AmQC.htm)|Meteoric Spellstrike|Meteoric Spellstrike|modificada|
|[class-10-SVfD887ocfUFikVA.htm](feats/class-10-SVfD887ocfUFikVA.htm)|Starlit Spells|Hechizos Starlit|modificada|
|[class-10-SYVM6Z3sS50e5Vbd.htm](feats/class-10-SYVM6Z3sS50e5Vbd.htm)|Deny Support|Denegar Apoyo|modificada|
|[class-10-tha0L3Z6608JrUwN.htm](feats/class-10-tha0L3Z6608JrUwN.htm)|Shadow Magic|Magia de las Sombras|modificada|
|[class-10-tMsAj0H0B9XZQjtH.htm](feats/class-10-tMsAj0H0B9XZQjtH.htm)|Accursed Clay Fist|Puño de Arcilla Maldito|modificada|
|[class-10-tonJdHGheKZ16tMI.htm](feats/class-10-tonJdHGheKZ16tMI.htm)|Unusual Composition|Composición inusual.|modificada|
|[class-10-ToZw6ZjB0JhWwMeR.htm](feats/class-10-ToZw6ZjB0JhWwMeR.htm)|Deflecting Shot|Disparo desviado|modificada|
|[class-10-ts2A1XeuPRzaCZgA.htm](feats/class-10-ts2A1XeuPRzaCZgA.htm)|Roadkill|Roadkill|modificada|
|[class-10-tSmd0cxq9wokSCh4.htm](feats/class-10-tSmd0cxq9wokSCh4.htm)|Castigating Weapon|Arma castigadora|modificada|
|[class-10-TW0fUdqqB69rIbRx.htm](feats/class-10-TW0fUdqqB69rIbRx.htm)|Lead the Way|Lead the Way|modificada|
|[class-10-tXNfWDa6P7bCKrCt.htm](feats/class-10-tXNfWDa6P7bCKrCt.htm)|Overwhelming Energy|Energía abrumadora|modificada|
|[class-10-TyjW9VGtlH0Zkm0I.htm](feats/class-10-TyjW9VGtlH0Zkm0I.htm)|Signifer's Sight|Signifer's Sight|modificada|
|[class-10-U52NMdeSNbjSSRHE.htm](feats/class-10-U52NMdeSNbjSSRHE.htm)|Incredible Companion (Ranger)|Comparo sensacional (Ranger)|modificada|
|[class-10-u5DBg0LrBUKP0JsJ.htm](feats/class-10-u5DBg0LrBUKP0JsJ.htm)|Scroll Savant|Sabio de los pergaminos|modificada|
|[class-10-U9SrWgLJ8JoAKIy0.htm](feats/class-10-U9SrWgLJ8JoAKIy0.htm)|Rockslide Spell|Hechizo Rockslide|modificada|
|[class-10-UCFGUbmDnzRGmN9I.htm](feats/class-10-UCFGUbmDnzRGmN9I.htm)|Steal Time|Robar Tiempo|modificada|
|[class-10-UEqntGzFrFA7ncUO.htm](feats/class-10-UEqntGzFrFA7ncUO.htm)|Aerial Piledriver|Aerial Pilerdriver|modificada|
|[class-10-UJcQgQpZRqXBw0Nb.htm](feats/class-10-UJcQgQpZRqXBw0Nb.htm)|Repurposed Parts|Repurposed Parts|modificada|
|[class-10-v482u7QboZEbhgvv.htm](feats/class-10-v482u7QboZEbhgvv.htm)|Assured Ritualist|Ritualista Seguro|modificada|
|[class-10-V5FKAmcYQF4KRELG.htm](feats/class-10-V5FKAmcYQF4KRELG.htm)|Cascade Bearer's Spellcasting|Dedicación de Cascade Bearer|modificada|
|[class-10-V5Y9r31BCTaCiNzi.htm](feats/class-10-V5Y9r31BCTaCiNzi.htm)|Lastwall Warden|Lastwall Warden|modificada|
|[class-10-V7n3kVSl0G6a29KG.htm](feats/class-10-V7n3kVSl0G6a29KG.htm)|Flailtongue|Flailtongue|modificada|
|[class-10-VC8qdcCxtzCmG98M.htm](feats/class-10-VC8qdcCxtzCmG98M.htm)|Impose Order (Psychic)|Imponer Orden (Psíquico)|modificada|
|[class-10-VF5XFpzBlqUFd8Mw.htm](feats/class-10-VF5XFpzBlqUFd8Mw.htm)|Glass Skin|Piel de cristal|modificada|
|[class-10-VJ3wIsfmabK02SNg.htm](feats/class-10-VJ3wIsfmabK02SNg.htm)|Provocator Dedication|Dedicación Provocador|modificada|
|[class-10-VsGdiAuVbYmDTR82.htm](feats/class-10-VsGdiAuVbYmDTR82.htm)|Black Powder Flash|Destello de Pólvora Negra|modificada|
|[class-10-VsTmB32x9673ONJ0.htm](feats/class-10-VsTmB32x9673ONJ0.htm)|Shield of Reckoning|Escudo de ajuste de cuentas|modificada|
|[class-10-vu0R9VfRZ6RWTczZ.htm](feats/class-10-vu0R9VfRZ6RWTczZ.htm)|Practiced Defender|Defender Practicado|modificada|
|[class-10-vxDL78Yz0dPZdAJ5.htm](feats/class-10-vxDL78Yz0dPZdAJ5.htm)|Precious Munitions|Municiones Preciosas|modificada|
|[class-10-vXH0HWMHzevA1Wox.htm](feats/class-10-vXH0HWMHzevA1Wox.htm)|Winding Flow|Flujo sinuoso|modificada|
|[class-10-waZzVgfyr1Vcxfqk.htm](feats/class-10-waZzVgfyr1Vcxfqk.htm)|Staggering Blow|Golpe asombroso|modificada|
|[class-10-Wo0HNBh6CCgPN3Co.htm](feats/class-10-Wo0HNBh6CCgPN3Co.htm)|Tiller's Drive|Tiller's Drive|modificada|
|[class-10-wUHnauB3atxO1RIo.htm](feats/class-10-wUHnauB3atxO1RIo.htm)|Rubbery Skin|Rubbery Skin|modificada|
|[class-10-wuMa6iJyZ83LYJXH.htm](feats/class-10-wuMa6iJyZ83LYJXH.htm)|Into the Future|Hacia el futuro|modificada|
|[class-10-wvJBIzgS298ZRp6w.htm](feats/class-10-wvJBIzgS298ZRp6w.htm)|Precise Debilitations|Debilitaciones precisas|modificada|
|[class-10-wxIOuPFjVBADNYDP.htm](feats/class-10-wxIOuPFjVBADNYDP.htm)|Torrential Backlash|Torrential Backlash|modificada|
|[class-10-wZZyasfIqwiJBQAQ.htm](feats/class-10-wZZyasfIqwiJBQAQ.htm)|Whirlwind Stance|Posición Torbellino|modificada|
|[class-10-xCuPob9TuSBKz6Gn.htm](feats/class-10-xCuPob9TuSBKz6Gn.htm)|Perfect Ki Expert|Experto Ki Perfecto|modificada|
|[class-10-Xdmz09kzLjIxWz9C.htm](feats/class-10-Xdmz09kzLjIxWz9C.htm)|Tactical Debilitations|Debilitaciones tácticas|modificada|
|[class-10-XeFMWjJkZy2O7lou.htm](feats/class-10-XeFMWjJkZy2O7lou.htm)|Cloud Walk|Cloud Walk|modificada|
|[class-10-xmf6oUYarFJGajtr.htm](feats/class-10-xmf6oUYarFJGajtr.htm)|Holy Light|Sagrada Luz|modificada|
|[class-10-XPTSCUoA8c4o9RgQ.htm](feats/class-10-XPTSCUoA8c4o9RgQ.htm)|Distracting Explosion|Explosión distractora|modificada|
|[class-10-YdGHQjhUrNvP18AA.htm](feats/class-10-YdGHQjhUrNvP18AA.htm)|Spellmaster's Ward|Spellmaster's Ward|modificada|
|[class-10-YFPmD8BHv0XaF55G.htm](feats/class-10-YFPmD8BHv0XaF55G.htm)|Distant Wandering|Vagabundeo lejano|modificada|
|[class-10-YlaLePtgDXGB0Fsf.htm](feats/class-10-YlaLePtgDXGB0Fsf.htm)|Penetrating Fire|Fuego Penetrante|modificada|
|[class-10-ySr510TkpUO3N6Ty.htm](feats/class-10-ySr510TkpUO3N6Ty.htm)|Trick Shot|Trick Shot|modificada|
|[class-10-yTh9QwAf0hadP91j.htm](feats/class-10-yTh9QwAf0hadP91j.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[class-10-Z9gzWFk3qom2z904.htm](feats/class-10-Z9gzWFk3qom2z904.htm)|Come and Get Me|Ven si te atreves|modificada|
|[class-10-zKL1lRcIbFblp2M2.htm](feats/class-10-zKL1lRcIbFblp2M2.htm)|Potent Poisoner|Envenenador poderoso|modificada|
|[class-10-zt5CWn3UrPViwaB3.htm](feats/class-10-zt5CWn3UrPViwaB3.htm)|Vicious Debilitations|Debilitaciones crueles|modificada|
|[class-12-07YYaQBknzDTcbFz.htm](feats/class-12-07YYaQBknzDTcbFz.htm)|Directed Poison|Veneno Dirigido|modificada|
|[class-12-0Qr7Y1IOxny0vIQf.htm](feats/class-12-0Qr7Y1IOxny0vIQf.htm)|Tunneling Claws|Garras tuneladoras|modificada|
|[class-12-0RB3f3J7gOEv3fni.htm](feats/class-12-0RB3f3J7gOEv3fni.htm)|Expert Bard Spellcasting|Lanzamiento de conjuros experto de bardo|modificada|
|[class-12-178rqdgJBF9Rl0Gi.htm](feats/class-12-178rqdgJBF9Rl0Gi.htm)|Master of the Dead|Maestro de los Muertos|modificada|
|[class-12-1JTEWgonWlmeCE3w.htm](feats/class-12-1JTEWgonWlmeCE3w.htm)|Liberating Stride|Zancada liberadora|modificada|
|[class-12-1k5PZAYth8u4Fqyr.htm](feats/class-12-1k5PZAYth8u4Fqyr.htm)|Expert Sorcerer Spellcasting|Lanzamiento de conjuros experto de hechicero.|modificada|
|[class-12-1ZbLYY5m2YALrrgA.htm](feats/class-12-1ZbLYY5m2YALrrgA.htm)|Perfection's Path (Fortitude)|Camino a la perfección (Fortaleza)|modificada|
|[class-12-2UogDoEJ2M2tQFxn.htm](feats/class-12-2UogDoEJ2M2tQFxn.htm)|Expert Spellcasting|Lanzador de conjuros experto|modificada|
|[class-12-4bB3N36DmqllGJNx.htm](feats/class-12-4bB3N36DmqllGJNx.htm)|Shield Salvation|Escudo Salvación|modificada|
|[class-12-4l0ewDg4gMfkU2pi.htm](feats/class-12-4l0ewDg4gMfkU2pi.htm)|Link Focus|Foco enlazado|modificada|
|[class-12-4st3fSYSrJrxwHOP.htm](feats/class-12-4st3fSYSrJrxwHOP.htm)|Contingency Gadgets|Gadgets de contingencia|modificada|
|[class-12-56BMXlQlZtg39SMV.htm](feats/class-12-56BMXlQlZtg39SMV.htm)|Lasting Doubt|Duda persistente|modificada|
|[class-12-5kua1Kf5Ca85lbzb.htm](feats/class-12-5kua1Kf5Ca85lbzb.htm)|Space-Time Shift|Cambio Espacio-Tiempo|modificada|
|[class-12-5kUQuIP8N57MXhuz.htm](feats/class-12-5kUQuIP8N57MXhuz.htm)|Beastmaster's Call|Llamada del Maestro de Bestias|modificada|
|[class-12-5YcnoTYKvEtkWiHh.htm](feats/class-12-5YcnoTYKvEtkWiHh.htm)|Side by Side (Ranger)|Codo con codo (Ranger)|modificada|
|[class-12-5ZX4btrw5yjBr8IM.htm](feats/class-12-5ZX4btrw5yjBr8IM.htm)|Bullet Dancer Reload|Bullet Dancer Reload|modificada|
|[class-12-6DkylvU5RF1O6DTT.htm](feats/class-12-6DkylvU5RF1O6DTT.htm)|Daredevil's Gambit|Daredevil's Gambit|modificada|
|[class-12-7aHYM9fawA3PQwtM.htm](feats/class-12-7aHYM9fawA3PQwtM.htm)|Self Destruct|Autodestrucción|modificada|
|[class-12-7HPXQvPH3ovwtVae.htm](feats/class-12-7HPXQvPH3ovwtVae.htm)|Furious Grab|Agarrón furioso|modificada|
|[class-12-7IrZIPP5ePtzzKcI.htm](feats/class-12-7IrZIPP5ePtzzKcI.htm)|Oneiric Influence|Influencia onírica|modificada|
|[class-12-7r79fZBr37qBN2EF.htm](feats/class-12-7r79fZBr37qBN2EF.htm)|Shattering Shot|Disparo Estallar|modificada|
|[class-12-7sFhBYoz5GSBFNbY.htm](feats/class-12-7sFhBYoz5GSBFNbY.htm)|Aura of Faith|Aura de fe|modificada|
|[class-12-7spk6rZPiNk2S0yA.htm](feats/class-12-7spk6rZPiNk2S0yA.htm)|Blood in the Air|Sangre en el aire|modificada|
|[class-12-8FxKcuFtOrqsl1FH.htm](feats/class-12-8FxKcuFtOrqsl1FH.htm)|Roll With it (Kingmaker)|Roll With it (Kingmaker)|modificada|
|[class-12-8KUvJuAWCoxWg5FH.htm](feats/class-12-8KUvJuAWCoxWg5FH.htm)|Metallic Envisionment|Metallic Envisionment|modificada|
|[class-12-9Ht1eyBHsB1swpeE.htm](feats/class-12-9Ht1eyBHsB1swpeE.htm)|Uncanny Bombs|Bombas asombrosas|modificada|
|[class-12-9u0uW1vZThRayXk2.htm](feats/class-12-9u0uW1vZThRayXk2.htm)|Shaped Contaminant|Controlar contaminante|modificada|
|[class-12-a2Owk9WI4pjSPuHf.htm](feats/class-12-a2Owk9WI4pjSPuHf.htm)|Educated Assessment|Evaluación Educada|modificada|
|[class-12-a2wXdQHiIoj3lHoe.htm](feats/class-12-a2wXdQHiIoj3lHoe.htm)|Communal Sustain|Communal Sustain|modificada|
|[class-12-ADVrDjp75fr2BYMa.htm](feats/class-12-ADVrDjp75fr2BYMa.htm)|Charged Creation|Creación cargada|modificada|
|[class-12-aFUxNGur3Hma8DKy.htm](feats/class-12-aFUxNGur3Hma8DKy.htm)|Predator's Pounce|Abalanzarse de Depredador|modificada|
|[class-12-AmV13b9ncALtWJFt.htm](feats/class-12-AmV13b9ncALtWJFt.htm)|Redirect Elements|Redirigir elementos|modificada|
|[class-12-Ano4tRq88V39eyPq.htm](feats/class-12-Ano4tRq88V39eyPq.htm)|Frightening Appearance|Aspecto aterrador|modificada|
|[class-12-APfPNpUQlKlCAJkS.htm](feats/class-12-APfPNpUQlKlCAJkS.htm)|Miraculous Intervention|Intervención Milagrosa|modificada|
|[class-12-asRbkgW59DZUpvAq.htm](feats/class-12-asRbkgW59DZUpvAq.htm)|Extend Elixir|Elixir prolongado|modificada|
|[class-12-AtidbY3lU49taaUR.htm](feats/class-12-AtidbY3lU49taaUR.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[class-12-AvN95M5eVLLEu2qk.htm](feats/class-12-AvN95M5eVLLEu2qk.htm)|Splendid Companion|Splendid Companion|modificada|
|[class-12-aWHOcGLA7AhX4xpm.htm](feats/class-12-aWHOcGLA7AhX4xpm.htm)|Interfering Surge|Oleaje Interferente|modificada|
|[class-12-AYXherMu9gFTyXjp.htm](feats/class-12-AYXherMu9gFTyXjp.htm)|Deadly Poison Weapon|Arma envenenada letal|modificada|
|[class-12-bbpFExz1RXCkOhnb.htm](feats/class-12-bbpFExz1RXCkOhnb.htm)|Sense Ki|Sense Ki|modificada|
|[class-12-BQrhDpLIp9zjbjEP.htm](feats/class-12-BQrhDpLIp9zjbjEP.htm)|Distracting Shot|Tiro de distracción|modificada|
|[class-12-bqZkAFS6eq9TKXMO.htm](feats/class-12-bqZkAFS6eq9TKXMO.htm)|Dragon's Rage Wings|Alas de Furia de Dragónón|modificada|
|[class-12-bTNgHDqzfoqOLWu3.htm](feats/class-12-bTNgHDqzfoqOLWu3.htm)|Speaking Sky|Speaking Sky|modificada|
|[class-12-C4m59yjuDmZLnTqu.htm](feats/class-12-C4m59yjuDmZLnTqu.htm)|Shadow Illusion|Ilusión de Sombra|modificada|
|[class-12-c7mCj6wUdb47rrnw.htm](feats/class-12-c7mCj6wUdb47rrnw.htm)|Ricochet Shot|Ricochet Shot|modificada|
|[class-12-COe0bYyVCyC78rzP.htm](feats/class-12-COe0bYyVCyC78rzP.htm)|Second Sting|Segundo aguijón|modificada|
|[class-12-cppQZwnuoXqX8mgF.htm](feats/class-12-cppQZwnuoXqX8mgF.htm)|Vernai Training|Vernai Training|modificada|
|[class-12-CQfxxsRxf1BuUH4o.htm](feats/class-12-CQfxxsRxf1BuUH4o.htm)|Janatimo's Lessons|Lecciones de Janatimo|modificada|
|[class-12-cV2CZxD6HB7HFeFv.htm](feats/class-12-cV2CZxD6HB7HFeFv.htm)|Sing to the Steel|Canta al Acero|modificada|
|[class-12-d1jQ0HyIOyUdCCaN.htm](feats/class-12-d1jQ0HyIOyUdCCaN.htm)|Spring Attack|Ataque elástico|modificada|
|[class-12-d1ktdX1Fk37dG5ms.htm](feats/class-12-d1ktdX1Fk37dG5ms.htm)|Defensive Recovery|Recuperación defensiva|modificada|
|[class-12-D3ypq5kAmLbwrgjq.htm](feats/class-12-D3ypq5kAmLbwrgjq.htm)|No Stranger to Death|No Stranger to Death|modificada|
|[class-12-d6Vb8D9yOX93mdUI.htm](feats/class-12-d6Vb8D9yOX93mdUI.htm)|Flinging Shove|Empujón lanzador|modificada|
|[class-12-dWkf6LhYBfBkeyOA.htm](feats/class-12-dWkf6LhYBfBkeyOA.htm)|Spellmaster's Resilience|Spellmaster's Resistente|modificada|
|[class-12-E7PFg8xua1nGIqHQ.htm](feats/class-12-E7PFg8xua1nGIqHQ.htm)|Competitive Eater|Competitive Eater|modificada|
|[class-12-Eg7YZBmeNJeY9wkD.htm](feats/class-12-Eg7YZBmeNJeY9wkD.htm)|Drive-By Attack|Drive-By Attack|modificada|
|[class-12-eGZzifQWCf5gSq2y.htm](feats/class-12-eGZzifQWCf5gSq2y.htm)|Unshakable Grit|Unshakable Grit|modificada|
|[class-12-emjWa77ltL5FytvA.htm](feats/class-12-emjWa77ltL5FytvA.htm)|Spring from the Shadows|Saltar desde las sombras|modificada|
|[class-12-eoEYZJNdmvA5GfyK.htm](feats/class-12-eoEYZJNdmvA5GfyK.htm)|Expert Eldritch Archer Spellcasting|Lanzador de conjuros experto Eldritch Archer.|modificada|
|[class-12-eSoMkHGw38ld4gj2.htm](feats/class-12-eSoMkHGw38ld4gj2.htm)|Shared Warding|Shared Warding|modificada|
|[class-12-etaixAdHNlHnLH0i.htm](feats/class-12-etaixAdHNlHnLH0i.htm)|Guarded Advance|Avance con guardia|modificada|
|[class-12-f6k9lIrIS4SfnCnG.htm](feats/class-12-f6k9lIrIS4SfnCnG.htm)|Master Alchemy|Alquimia maestra|modificada|
|[class-12-FA1s2jjZqoBy58Xx.htm](feats/class-12-FA1s2jjZqoBy58Xx.htm)|Secrets of Shadow|Secretos de Sombra|modificada|
|[class-12-fCDC53WOOYrsyVIR.htm](feats/class-12-fCDC53WOOYrsyVIR.htm)|Incredible Ricochet|Rebote sensacional|modificada|
|[class-12-fcFrxvqbIX6k71os.htm](feats/class-12-fcFrxvqbIX6k71os.htm)|Meditative Focus|Foco meditativo|modificada|
|[class-12-fLlCodqKXyXbZR7C.htm](feats/class-12-fLlCodqKXyXbZR7C.htm)|Wave Spiral|Espiral de ondas|modificada|
|[class-12-FYz5eQeTox9IDkSd.htm](feats/class-12-FYz5eQeTox9IDkSd.htm)|Dueling Dance (Fighter)|Baile de duelo (Luchador)|modificada|
|[class-12-gHHnMCBi1gvG5wTL.htm](feats/class-12-gHHnMCBi1gvG5wTL.htm)|Student Of The Dueling Arts|Estudiante de las Artes del Duelo|modificada|
|[class-12-gipC9eWrBDFRpNkQ.htm](feats/class-12-gipC9eWrBDFRpNkQ.htm)|Frostbite Runes|Frostbite Runes|modificada|
|[class-12-GMrJdGwajADbL1y5.htm](feats/class-12-GMrJdGwajADbL1y5.htm)|Diamond Soul|Alma de diamante|modificada|
|[class-12-gO729iC9b5ypes2K.htm](feats/class-12-gO729iC9b5ypes2K.htm)|Spirit's Wrath|Ira del Espíritu|modificada|
|[class-12-Gu8IAYxwNdQM603P.htm](feats/class-12-Gu8IAYxwNdQM603P.htm)|Read the Land|Leer la Tierra|modificada|
|[class-12-HbBNuT1bqdcCU9hM.htm](feats/class-12-HbBNuT1bqdcCU9hM.htm)|Greater Sun Blessing|Bendición del Sol Mayor|modificada|
|[class-12-HJBDFHIaJ3lfxcbs.htm](feats/class-12-HJBDFHIaJ3lfxcbs.htm)|Maneuvering Spell|Hechizo de Maniobra|modificada|
|[class-12-hPanopG3TbXKr52O.htm](feats/class-12-hPanopG3TbXKr52O.htm)|Pesh Skin|Piel de pesh|modificada|
|[class-12-hpCBELEKGA4ynYv4.htm](feats/class-12-hpCBELEKGA4ynYv4.htm)|Expert Wizard Spellcasting|Lanzamiento de conjuros experto de mago|modificada|
|[class-12-hPDerDCYmag3s0dP.htm](feats/class-12-hPDerDCYmag3s0dP.htm)|Paragon's Guard|Guardia modelo de Paragon|modificada|
|[class-12-HPETR6zq8L6YJyi1.htm](feats/class-12-HPETR6zq8L6YJyi1.htm)|Improved Knockback|Revés mejorado de Arredrar.|modificada|
|[class-12-hT0pVPqFuiEsmRb8.htm](feats/class-12-hT0pVPqFuiEsmRb8.htm)|Six Pillars Stance|Posición de los Seis Pilares|modificada|
|[class-12-HxTqYtXRqFkkLfDQ.htm](feats/class-12-HxTqYtXRqFkkLfDQ.htm)|Gigantic Megafauna Companion|Gigantic Megafauna Companion|modificada|
|[class-12-hZueHQeYR5bgDh5W.htm](feats/class-12-hZueHQeYR5bgDh5W.htm)|Secret Eater|Secret Eater|modificada|
|[class-12-I00uuseTfPypVgLQ.htm](feats/class-12-I00uuseTfPypVgLQ.htm)|Primal Summons|Convocación primigenia|modificada|
|[class-12-I6su6Z9sJTgIrhQV.htm](feats/class-12-I6su6Z9sJTgIrhQV.htm)|Spiritual Aid|Ayuda Espiritual|modificada|
|[class-12-iFEecf9o6uhJxWcG.htm](feats/class-12-iFEecf9o6uhJxWcG.htm)|Hellknight Order Cross-Training|Caballero del Infierno Orden Cross-Training|modificada|
|[class-12-Ij6BBPzZvOFZ3prs.htm](feats/class-12-Ij6BBPzZvOFZ3prs.htm)|Felling Shot|Tiro de tala|modificada|
|[class-12-iJxbrXAdxhLqdT5E.htm](feats/class-12-iJxbrXAdxhLqdT5E.htm)|Assassinate|Assassinate|modificada|
|[class-12-IMPP5pa8AmvCby4W.htm](feats/class-12-IMPP5pa8AmvCby4W.htm)|Clever Counterspell|Contraconjuro astuto|modificada|
|[class-12-INqBWbbDHF4DIg8i.htm](feats/class-12-INqBWbbDHF4DIg8i.htm)|Escape Timeline|Cronología de Huir|modificada|
|[class-12-isdTXU8bV7ZVOAuQ.htm](feats/class-12-isdTXU8bV7ZVOAuQ.htm)|Flourishing Finish|Acabado Floritura|modificada|
|[class-12-ixIKF2LMmbFthI8Z.htm](feats/class-12-ixIKF2LMmbFthI8Z.htm)|Towering Size|Towering Size|modificada|
|[class-12-IZFw3Do0kBdgwZX0.htm](feats/class-12-IZFw3Do0kBdgwZX0.htm)|Expert Cathartic Spellcasting|Lanzador de conjuros experto catártico.|modificada|
|[class-12-J5s7NeFHYuFSdhrX.htm](feats/class-12-J5s7NeFHYuFSdhrX.htm)|Hell's Armaments|Armamento del Infierno|modificada|
|[class-12-JddrdXv4pfdePsGK.htm](feats/class-12-JddrdXv4pfdePsGK.htm)|Petrified Skin|Piel Petrificada|modificada|
|[class-12-JfLIWyqgEVggWaBL.htm](feats/class-12-JfLIWyqgEVggWaBL.htm)|Magic Sense (Magus)|Sentido mágico (Magus)|modificada|
|[class-12-JJPoMcxUf3QoKA6h.htm](feats/class-12-JJPoMcxUf3QoKA6h.htm)|Combat Premonition|Combat Premonition|modificada|
|[class-12-JmHHdGC1p53BBedu.htm](feats/class-12-JmHHdGC1p53BBedu.htm)|Expert Magus Spellcasting|Lanzador de conjuros experto magus.|modificada|
|[class-12-jNeIaFUFSGUXoSON.htm](feats/class-12-jNeIaFUFSGUXoSON.htm)|Divine Wall|Muro divinoón|modificada|
|[class-12-jNzjecRGyyAqkkrm.htm](feats/class-12-jNzjecRGyyAqkkrm.htm)|Deep Freeze|Deep Freeze|modificada|
|[class-12-jPBqvEH2jLlvDr6M.htm](feats/class-12-jPBqvEH2jLlvDr6M.htm)|Juggernaut's Fortitude|Fortaleza del juggernaut|modificada|
|[class-12-jsbe2d9lYGJ2MksT.htm](feats/class-12-jsbe2d9lYGJ2MksT.htm)|Banshee Cry Display|Banshee Cry Display|modificada|
|[class-12-K5pXeeJLmdE8XuvM.htm](feats/class-12-K5pXeeJLmdE8XuvM.htm)|Tense Negotiator|Negociador Tenso|modificada|
|[class-12-k9B7gDit3pXbq2XF.htm](feats/class-12-k9B7gDit3pXbq2XF.htm)|Finessed Features|Características|modificada|
|[class-12-kKoqqXOTdRYROmVV.htm](feats/class-12-kKoqqXOTdRYROmVV.htm)|Blessed Denial|Negación Bendecida|modificada|
|[class-12-KOslKkFO9N1kZY87.htm](feats/class-12-KOslKkFO9N1kZY87.htm)|Night's Shine|Brillo Nocturno|modificada|
|[class-12-KvKg9pBOpk2oLeO1.htm](feats/class-12-KvKg9pBOpk2oLeO1.htm)|Advanced Order Training|Entrenamiento de Orden Avanzado|modificada|
|[class-12-KYF9e4oeSjHKgbwM.htm](feats/class-12-KYF9e4oeSjHKgbwM.htm)|Coordinated Charge|Carga coordinada|modificada|
|[class-12-LI9VtCaL5ZRk0Wo8.htm](feats/class-12-LI9VtCaL5ZRk0Wo8.htm)|Stance Savant (Monk)|Sabio de las Posición (Monje)|modificada|
|[class-12-lIpfj1JeFFS7Zn6D.htm](feats/class-12-lIpfj1JeFFS7Zn6D.htm)|Vigilant Benediction|Bendición Vigilante|modificada|
|[class-12-llDw5DqnB4LbIUCV.htm](feats/class-12-llDw5DqnB4LbIUCV.htm)|Expert Snowcasting|Experto Lanzanieves|modificada|
|[class-12-lmAuoHPxzQdaSUmN.htm](feats/class-12-lmAuoHPxzQdaSUmN.htm)|Cartwheel Dodge|Cartwheel Dodge|modificada|
|[class-12-lPTcPIshChHWz4J6.htm](feats/class-12-lPTcPIshChHWz4J6.htm)|Critical Debilitation|Debilitación crítica|modificada|
|[class-12-lv8jRWK7bv7dR6SM.htm](feats/class-12-lv8jRWK7bv7dR6SM.htm)|Gigavolt|Gigavolt|modificada|
|[class-12-lx8Wt813qavwLISv.htm](feats/class-12-lx8Wt813qavwLISv.htm)|Expert Beast Gunner Spellcasting|Lanzador de conjuros bestia experto|modificada|
|[class-12-MD33E76f2olLnDZb.htm](feats/class-12-MD33E76f2olLnDZb.htm)|Uncanny Suction|Succión Asombrosa|modificada|
|[class-12-mgs7vxq6d3hQoswa.htm](feats/class-12-mgs7vxq6d3hQoswa.htm)|Improved Dueling Riposte|Réplica de duelo mejorada|modificada|
|[class-12-MrBHGo9nmzcVii3k.htm](feats/class-12-MrBHGo9nmzcVii3k.htm)|Desiccating Inhalation|Inhalación Desecante|modificada|
|[class-12-mSDKKRgK6sRMjqQo.htm](feats/class-12-mSDKKRgK6sRMjqQo.htm)|Summoner's Call|Llamada del Invocador|modificada|
|[class-12-MsieJK35tAP6gFGv.htm](feats/class-12-MsieJK35tAP6gFGv.htm)|Coffin Bound|Ataúd Bound|modificada|
|[class-12-mTkbgFOHJUBl0Qwg.htm](feats/class-12-mTkbgFOHJUBl0Qwg.htm)|Shared Replenishment|Reabastecimiento compartido|modificada|
|[class-12-mZcI1NKtQhteAQLn.htm](feats/class-12-mZcI1NKtQhteAQLn.htm)|Silver's Refrain|Silver's Refrain|modificada|
|[class-12-n1t6foOyrN48OVPK.htm](feats/class-12-n1t6foOyrN48OVPK.htm)|Collapse Wall|Colapso Muro|modificada|
|[class-12-nI67dTzKYg5kKMsd.htm](feats/class-12-nI67dTzKYg5kKMsd.htm)|Titan's Stature|Estatura de Titán|modificada|
|[class-12-NIzuFBqmHURIy2oI.htm](feats/class-12-NIzuFBqmHURIy2oI.htm)|Ward Slumber|Ward Slumber|modificada|
|[class-12-NJyyxInJ743OotKf.htm](feats/class-12-NJyyxInJ743OotKf.htm)|Deadeye|Deadeye|modificada|
|[class-12-NM5a1tF0OW5mVYdR.htm](feats/class-12-NM5a1tF0OW5mVYdR.htm)|Additional Shadow Magic|Magia de Sombra adicional|modificada|
|[class-12-nPwzElPvV29eM5as.htm](feats/class-12-nPwzElPvV29eM5as.htm)|Chain Reaction|Reacción en Cadena|modificada|
|[class-12-nsFnOLqYSkGWFhLD.htm](feats/class-12-nsFnOLqYSkGWFhLD.htm)|Affliction Mercy|Merced ante la aflicción|modificada|
|[class-12-NtaOLg9meDYfg8aV.htm](feats/class-12-NtaOLg9meDYfg8aV.htm)|Blade of Justice|Filo de la justicia|modificada|
|[class-12-nWd7m0yRcIEVUy7O.htm](feats/class-12-nWd7m0yRcIEVUy7O.htm)|Elaborate Scroll Esoterica|Pergamino Elaborado Esotérico|modificada|
|[class-12-O0QrBJfiMCTR0n0z.htm](feats/class-12-O0QrBJfiMCTR0n0z.htm)|Greater Despair|Gran desesperación|modificada|
|[class-12-Oa41bfBRO36lf1aE.htm](feats/class-12-Oa41bfBRO36lf1aE.htm)|Shoulder Catastrophe|Catástrofe de hombro|modificada|
|[class-12-OrNcWu1Y7c2O5zU6.htm](feats/class-12-OrNcWu1Y7c2O5zU6.htm)|Lightning Snares|Trampas de lazo muy rápidas|modificada|
|[class-12-OyOpHPOXC08bffVR.htm](feats/class-12-OyOpHPOXC08bffVR.htm)|Determined Lore Seeker|Determinación Buscador de Lore|modificada|
|[class-12-p0jZhb8PSswUsZaz.htm](feats/class-12-p0jZhb8PSswUsZaz.htm)|Dragon Shape|Forma de dragón|modificada|
|[class-12-P13ZhZcR67Ev0vrS.htm](feats/class-12-P13ZhZcR67Ev0vrS.htm)|Disrupt Ki|Perturbar Ki|modificada|
|[class-12-pbD4lfAPkK1NNag0.htm](feats/class-12-pbD4lfAPkK1NNag0.htm)|Double Prey|Doble presa|modificada|
|[class-12-PeBz8f9h8Y4OFdws.htm](feats/class-12-PeBz8f9h8Y4OFdws.htm)|Legs of Stone|Piernas de Piedra|modificada|
|[class-12-peUBtdoG5LyVaf5g.htm](feats/class-12-peUBtdoG5LyVaf5g.htm)|Lich Dedication|Dedicación Lich|modificada|
|[class-12-pKttKO84dxzRLBIC.htm](feats/class-12-pKttKO84dxzRLBIC.htm)|Expert Combat Eidolon|Eidolon Experto en Combate|modificada|
|[class-12-PLTOCEAvqBS05pZu.htm](feats/class-12-PLTOCEAvqBS05pZu.htm)|Expert Cleric Spellcasting|Lanzamiento de conjuros experto de clérigo|modificada|
|[class-12-pM57xTqy5CMYZeqD.htm](feats/class-12-pM57xTqy5CMYZeqD.htm)|Obscured Emergence|Emergencia Oscura|modificada|
|[class-12-PmhprI0vr3zTOkok.htm](feats/class-12-PmhprI0vr3zTOkok.htm)|Foreseen Failure|Fracaso previsto|modificada|
|[class-12-pmVaj53saxLE28Pl.htm](feats/class-12-pmVaj53saxLE28Pl.htm)|Overwhelming Spellstrike|Hechizo Abrumador|modificada|
|[class-12-pu1U9ZWVVG1Lc94t.htm](feats/class-12-pu1U9ZWVVG1Lc94t.htm)|Diverse Weapon Expert|Experto en armas diversas|modificada|
|[class-12-PxBQ4JdaPS2KTAG7.htm](feats/class-12-PxBQ4JdaPS2KTAG7.htm)|Necromancer's Visage|Necromancer's Visage|modificada|
|[class-12-pZUmoCqxBJfZmeqm.htm](feats/class-12-pZUmoCqxBJfZmeqm.htm)|Festering Wounds|Heridas Hirientes|modificada|
|[class-12-qgahXDIjevUmk5Ci.htm](feats/class-12-qgahXDIjevUmk5Ci.htm)|Pact of the Final Breath|Pacto del Aliento Final|modificada|
|[class-12-qI5ZyuNVME95iXhJ.htm](feats/class-12-qI5ZyuNVME95iXhJ.htm)|Fantastic Leap|Salto fantástico|modificada|
|[class-12-qmORiUubF2CVgIva.htm](feats/class-12-qmORiUubF2CVgIva.htm)|Reclaimant Plea|Reclaimant Plea|modificada|
|[class-12-Qss6GKzSYGqKgY4b.htm](feats/class-12-Qss6GKzSYGqKgY4b.htm)|Resolute|Resolute|modificada|
|[class-12-quqG3jfWFdopF0G2.htm](feats/class-12-quqG3jfWFdopF0G2.htm)|No!!!|¡¡¡No!!!|modificada|
|[class-12-qZsTI97BQUwoPgKF.htm](feats/class-12-qZsTI97BQUwoPgKF.htm)|Psi Catastrophe|Psi Catástrofe|modificada|
|[class-12-r0twuF5nxXN5lkLk.htm](feats/class-12-r0twuF5nxXN5lkLk.htm)|Cut the Bonds|Cortar los lazos|modificada|
|[class-12-rHV8GUqqt8WLgTJp.htm](feats/class-12-rHV8GUqqt8WLgTJp.htm)|Thaumaturge's Demesne|Thaumaturge's Demesne|modificada|
|[class-12-RJmnFuaGLXfhX3It.htm](feats/class-12-RJmnFuaGLXfhX3It.htm)|Expert Psychic Spellcasting|Lanzador de conjuros experto psíquico.|modificada|
|[class-12-rm8NgrdZNjqpGlC1.htm](feats/class-12-rm8NgrdZNjqpGlC1.htm)|Instigate Psychic Duel|Instigar Duelo Psíquico|modificada|
|[class-12-rqUJULinzDCUgimM.htm](feats/class-12-rqUJULinzDCUgimM.htm)|Shooter's Camouflage|Camuflaje de Tirador|modificada|
|[class-12-RSUmrIiFBEchdM8B.htm](feats/class-12-RSUmrIiFBEchdM8B.htm)|Primal Focus|Foco primigenio|modificada|
|[class-12-RSwrDo9i0RKoAI6D.htm](feats/class-12-RSwrDo9i0RKoAI6D.htm)|Shepherd of Desolation|Pastor de la Desolación|modificada|
|[class-12-rW1q7x5CMf9Rh1bi.htm](feats/class-12-rW1q7x5CMf9Rh1bi.htm)|Master Spotter (Ranger)|Avistador maestro (Ranger)|modificada|
|[class-12-s6y6JzPW2K8k4m8k.htm](feats/class-12-s6y6JzPW2K8k4m8k.htm)|Expert Summoner Spellcasting|Lanzador de conjuros experto invocador.|modificada|
|[class-12-saEwTvJuiemEIfLm.htm](feats/class-12-saEwTvJuiemEIfLm.htm)|Expert Druid Spellcasting|Lanzamiento de conjuros experto de druida|modificada|
|[class-12-sjqnusj5Py7AiofF.htm](feats/class-12-sjqnusj5Py7AiofF.htm)|Perfection's Path (Will)|Camino a la perfección (voluntad)|modificada|
|[class-12-ssrublnppwFSvVcb.htm](feats/class-12-ssrublnppwFSvVcb.htm)|Reach for the Sky|Alcanza el cielo|modificada|
|[class-12-SyxXnSk2R0AM9HSn.htm](feats/class-12-SyxXnSk2R0AM9HSn.htm)|Flicker|Flicker|modificada|
|[class-12-tBfalnbUZLkG9gs1.htm](feats/class-12-tBfalnbUZLkG9gs1.htm)|Blade of Law|Espada de la Ley|modificada|
|[class-12-tGXJU6yx7bYuyLvd.htm](feats/class-12-tGXJU6yx7bYuyLvd.htm)|Brutal Finish|Remate brutal|modificada|
|[class-12-U0XX0Mm2MbZcuNK2.htm](feats/class-12-U0XX0Mm2MbZcuNK2.htm)|Shared Overdrive|Sobremarcha compartida|modificada|
|[class-12-U4AoJMBhJaFq5O1S.htm](feats/class-12-U4AoJMBhJaFq5O1S.htm)|Champion's Sacrifice|Sacrificio del campeón|modificada|
|[class-12-U5hjS5qFrtGHWlVG.htm](feats/class-12-U5hjS5qFrtGHWlVG.htm)|Corpse Stench|Hedor de Cadáver|modificada|
|[class-12-U9uRb2TezYlQPNJd.htm](feats/class-12-U9uRb2TezYlQPNJd.htm)|Boost Modulation|Boost Modulation|modificada|
|[class-12-U9zIjPHPe2PyrtlU.htm](feats/class-12-U9zIjPHPe2PyrtlU.htm)|Flexible Transmogrification|Transmogrificación flexible|modificada|
|[class-12-uAh31Hnp1EZSjd40.htm](feats/class-12-uAh31Hnp1EZSjd40.htm)|Invincible Mutagen|Mutágeno invencible|modificada|
|[class-12-UH8pDxrq1xJq4Sid.htm](feats/class-12-UH8pDxrq1xJq4Sid.htm)|Master Spotter|Avistador maestro|modificada|
|[class-12-UhmPekgw2HO40zKC.htm](feats/class-12-UhmPekgw2HO40zKC.htm)|Eagle Eyes|Ojos de águila|modificada|
|[class-12-UIRcjHxuSedoDOj4.htm](feats/class-12-UIRcjHxuSedoDOj4.htm)|Inspirational Focus|Foco inspirativo|modificada|
|[class-12-uJgATfMW3kumS6Y0.htm](feats/class-12-uJgATfMW3kumS6Y0.htm)|Intensify Investiture|Intensificar Investidura|modificada|
|[class-12-UotFrqA7zAxtpJdE.htm](feats/class-12-UotFrqA7zAxtpJdE.htm)|Firearm Expert|Experto en armas de fuego|modificada|
|[class-12-UqA9GdO2pGQwg9cd.htm](feats/class-12-UqA9GdO2pGQwg9cd.htm)|Flexible Halcyon Spellcasting|Hechicería Halcyon flexible|modificada|
|[class-12-uQExVrBNPJeS66sO.htm](feats/class-12-uQExVrBNPJeS66sO.htm)|For Love, For Lightning|For Love, For Lightning|modificada|
|[class-12-UrOj9TROtn8nuxPf.htm](feats/class-12-UrOj9TROtn8nuxPf.htm)|Expert Scroll Cache|Caché de Pergamino Experto|modificada|
|[class-12-uvbPZR6dsQBimIUo.htm](feats/class-12-uvbPZR6dsQBimIUo.htm)|Imbue Mindlessness|Imbuir Descerebrado|modificada|
|[class-12-UxjAszOYAhUvCDt2.htm](feats/class-12-UxjAszOYAhUvCDt2.htm)|Inescapable Grasp|Inescapable Grasp|modificada|
|[class-12-UZDiqc5bBJzOTxUQ.htm](feats/class-12-UZDiqc5bBJzOTxUQ.htm)|Advanced Seeker of Truths|Buscador avanzado de verdades|modificada|
|[class-12-Uzz7oZit3FNO9FxO.htm](feats/class-12-Uzz7oZit3FNO9FxO.htm)|Wings Of The Dragon|Alas de dragón|modificada|
|[class-12-VD446AflrQ3kO1al.htm](feats/class-12-VD446AflrQ3kO1al.htm)|Evasiveness (Rogue)|Evasividad (Pícaro)|modificada|
|[class-12-vFa9crHKkNkPUjFl.htm](feats/class-12-vFa9crHKkNkPUjFl.htm)|Shared Sight|Shared Sight|modificada|
|[class-12-VN3OHDYcnLaw0nW1.htm](feats/class-12-VN3OHDYcnLaw0nW1.htm)|Wild Strider|Strider salvaje|modificada|
|[class-12-vPA0EGXBaNzGxlxM.htm](feats/class-12-vPA0EGXBaNzGxlxM.htm)|Forewarn|Forewarn|modificada|
|[class-12-vqHF9U2RkL2NVgMF.htm](feats/class-12-vqHF9U2RkL2NVgMF.htm)|Enticing Dwelling|Morada Seductora|modificada|
|[class-12-VS28L98uFhr3j1HC.htm](feats/class-12-VS28L98uFhr3j1HC.htm)|Their Master's Call|La Llamada de su Amo|modificada|
|[class-12-vVhgYkOU9mPTGTxF.htm](feats/class-12-vVhgYkOU9mPTGTxF.htm)|Domain Focus|Foco de dominio|modificada|
|[class-12-vwBD55BRDOatp4ZV.htm](feats/class-12-vwBD55BRDOatp4ZV.htm)|Green Tongue|Lengua verde|modificada|
|[class-12-VzQtyHSjq12E4Dzh.htm](feats/class-12-VzQtyHSjq12E4Dzh.htm)|Purifying Spell|Hechizo Purificador|modificada|
|[class-12-W7Rkw1L5QxHvgeUW.htm](feats/class-12-W7Rkw1L5QxHvgeUW.htm)|Eagle Eye|Ojos de águila|modificada|
|[class-12-wDo5dsSmyJqfmPgj.htm](feats/class-12-wDo5dsSmyJqfmPgj.htm)|Signifer Armor Expertise|Signifer Armor Expertise|modificada|
|[class-12-wGaxWwJhIXbMJft1.htm](feats/class-12-wGaxWwJhIXbMJft1.htm)|Plentiful Snares|Plentiful Snares|modificada|
|[class-12-wkJ6EjtXUztOqTwH.htm](feats/class-12-wkJ6EjtXUztOqTwH.htm)|Perfection's Path (Reflex)|Camino a la perfección (Reflejo)|modificada|
|[class-12-WO23FvCo8IYVB5RF.htm](feats/class-12-WO23FvCo8IYVB5RF.htm)|Blade of the Crimson Oath|Espada del Juramento Carmesí|modificada|
|[class-12-WsEVkMFe8ZEIRKLu.htm](feats/class-12-WsEVkMFe8ZEIRKLu.htm)|Grasping Limbs|Agarrar extremidades|modificada|
|[class-12-WyGeBz9U6Hovozdl.htm](feats/class-12-WyGeBz9U6Hovozdl.htm)|Reverse Curse|Reverse Curse|modificada|
|[class-12-xAFdoKl7aOP9rVkl.htm](feats/class-12-xAFdoKl7aOP9rVkl.htm)|Emblazon Antimagic|Estampar antimagia|modificada|
|[class-12-xBqDeQFzvuDfqhZC.htm](feats/class-12-xBqDeQFzvuDfqhZC.htm)|Reactive Interference|Interferencia distractora|modificada|
|[class-12-Xdf00Kmv5C3qqrtK.htm](feats/class-12-Xdf00Kmv5C3qqrtK.htm)|Flesh Wound|Hiriente de Carne|modificada|
|[class-12-XgUQ6Tm9LKxcZGHW.htm](feats/class-12-XgUQ6Tm9LKxcZGHW.htm)|Knight in Shining Armor|Caballero de brillante armadura|modificada|
|[class-12-xmccXo6U7P0IMM3z.htm](feats/class-12-xmccXo6U7P0IMM3z.htm)|Reaper of Repose|Reaper of Repose|modificada|
|[class-12-xNejAvuRXKYq2D6A.htm](feats/class-12-xNejAvuRXKYq2D6A.htm)|Swap Investment|Intercambiar inversión|modificada|
|[class-12-XWmlGnFNfxyJWw9V.htm](feats/class-12-XWmlGnFNfxyJWw9V.htm)|Choking Smoke|Humo Asfixiante|modificada|
|[class-12-xWWcxZUgQTaHZHkY.htm](feats/class-12-xWWcxZUgQTaHZHkY.htm)|School Counterspell|Contraconjuro Escolar|modificada|
|[class-12-XyQpqznuO5LGFvhz.htm](feats/class-12-XyQpqznuO5LGFvhz.htm)|Coordinated Distraction|Distracción Coordinada|modificada|
|[class-12-xzesXSIXTqsVxm1e.htm](feats/class-12-xzesXSIXTqsVxm1e.htm)|Golem Dynamo|Golem Dynamo|modificada|
|[class-12-yaxf1Tpk5iwPCSpW.htm](feats/class-12-yaxf1Tpk5iwPCSpW.htm)|Eclectic Polymath|Polifacético ecléctico|modificada|
|[class-12-YidYY7k2gvny9eSY.htm](feats/class-12-YidYY7k2gvny9eSY.htm)|Judgement of the Monolith|Juicio del Monolito|modificada|
|[class-12-ysXXa8K9fa383HAD.htm](feats/class-12-ysXXa8K9fa383HAD.htm)|Great Bear|Gran Oso|modificada|
|[class-12-YTHsakqXumdMU0dn.htm](feats/class-12-YTHsakqXumdMU0dn.htm)|Conflux Focus|Enfoque Conflujo|modificada|
|[class-12-YZEG7LsiIIhwRB91.htm](feats/class-12-YZEG7LsiIIhwRB91.htm)|Necromantic Deflection|Necromantic Deflection|modificada|
|[class-12-ZghzLmYgeE19GqjP.htm](feats/class-12-ZghzLmYgeE19GqjP.htm)|Lunging Stance|Posición de acometida|modificada|
|[class-12-Zn2ySapQ2gtgyWgW.htm](feats/class-12-Zn2ySapQ2gtgyWgW.htm)|Aegis of Arnisant|Aegis of Arnisant|modificada|
|[class-12-zSTPB1FFWMfA1JPi.htm](feats/class-12-zSTPB1FFWMfA1JPi.htm)|Bloodline Focus|Foco de linaje|modificada|
|[class-12-zybYSfeM0PLPpAaa.htm](feats/class-12-zybYSfeM0PLPpAaa.htm)|Infectious Emotions|Emociones Infecciosas|modificada|
|[class-14-0kkiE74cyHyxdPe6.htm](feats/class-14-0kkiE74cyHyxdPe6.htm)|Vengeful Strike|Golpe vengativo|modificada|
|[class-14-0qL4a3CarG1e0pfB.htm](feats/class-14-0qL4a3CarG1e0pfB.htm)|Forceful Shot|Tiro vigoroso|modificada|
|[class-14-0S92aZtljjTAwLdO.htm](feats/class-14-0S92aZtljjTAwLdO.htm)|Drain Soul Cage|Drenar Jaula de Almas|modificada|
|[class-14-0xh9ISHFUFHqngK0.htm](feats/class-14-0xh9ISHFUFHqngK0.htm)|Execution|Ejecución|modificada|
|[class-14-1DaSVLJEdJWYOWek.htm](feats/class-14-1DaSVLJEdJWYOWek.htm)|Guiding Riposte|Réplica orientadora|modificada|
|[class-14-1fVKWWYjlVtOECku.htm](feats/class-14-1fVKWWYjlVtOECku.htm)|Shape of the Cloud Dragon|Forma del dragón de las nubesón|modificada|
|[class-14-1SvBUzVH5tp0lmn5.htm](feats/class-14-1SvBUzVH5tp0lmn5.htm)|Two-Weapon Flurry|Ráfaga con dos armas|modificada|
|[class-14-2FJwXMTJycSZY80Q.htm](feats/class-14-2FJwXMTJycSZY80Q.htm)|Target Of Opportunity|Objetivo De Oportunidad|modificada|
|[class-14-2HMvAB6mIVwvwtjT.htm](feats/class-14-2HMvAB6mIVwvwtjT.htm)|True Debilitating Bomb|Bomba debilitante verdadera.|modificada|
|[class-14-2l15VfVHMw3ttgJ3.htm](feats/class-14-2l15VfVHMw3ttgJ3.htm)|Incredible Recollection|Memoria increible|modificada|
|[class-14-2VZxwS5LTi9YxikG.htm](feats/class-14-2VZxwS5LTi9YxikG.htm)|Reset the Past|Restablecer el pasado|modificada|
|[class-14-2wLfchKVDSKBOIpV.htm](feats/class-14-2wLfchKVDSKBOIpV.htm)|Soaring Armor|Soaring Armor|modificada|
|[class-14-3hnmQrhv4Bru5GKR.htm](feats/class-14-3hnmQrhv4Bru5GKR.htm)|Grand Talisman Esoterica|Gran Talismán Esotérico|modificada|
|[class-14-3r5rg0BCqSh5RBNS.htm](feats/class-14-3r5rg0BCqSh5RBNS.htm)|Specialized Companion (Druid)|Comparo especializado (druida)|modificada|
|[class-14-3tm0TzyjO1I378fw.htm](feats/class-14-3tm0TzyjO1I378fw.htm)|Conscious Spell Specialization|Especialización en Hechizos Conscientes|modificada|
|[class-14-45F4nNN8gxoBdSnk.htm](feats/class-14-45F4nNN8gxoBdSnk.htm)|Enshroud Soul Cage|Enjaular Alma Enshroud|modificada|
|[class-14-4dQcLroKQ13QYIT3.htm](feats/class-14-4dQcLroKQ13QYIT3.htm)|Tactical Cadence|Cadencia Táctica|modificada|
|[class-14-4n59y5tb9bxffKsi.htm](feats/class-14-4n59y5tb9bxffKsi.htm)|Esoteric Reflexes|Reflejos Esotéricos|modificada|
|[class-14-5cxkVY3mBsbYWd5K.htm](feats/class-14-5cxkVY3mBsbYWd5K.htm)|Timeless Body|Cuerpo eterno|modificada|
|[class-14-5dAFkOYPz8PPdFrw.htm](feats/class-14-5dAFkOYPz8PPdFrw.htm)|Leave an Opening|Dejar una abertura|modificada|
|[class-14-5NyX1WnXEO40yEaT.htm](feats/class-14-5NyX1WnXEO40yEaT.htm)|Wyrmbane Aura|Aura del azote de sierpes|modificada|
|[class-14-5zeKULjKDkiIenhu.htm](feats/class-14-5zeKULjKDkiIenhu.htm)|Unstable Redundancies|Redundancias Inestables|modificada|
|[class-14-6gtm0YCaDFpyVy35.htm](feats/class-14-6gtm0YCaDFpyVy35.htm)|Warden's Guidance|Orientación divina del guardián|modificada|
|[class-14-8nCxI2SZ64UmMuRZ.htm](feats/class-14-8nCxI2SZ64UmMuRZ.htm)|Entities From Afar|Aberaciones lejanas|modificada|
|[class-14-8PMxl8o5YXET58Pn.htm](feats/class-14-8PMxl8o5YXET58Pn.htm)|Iron Lung|Pulmón de Hierro|modificada|
|[class-14-9DECwTTiVpHJc4B6.htm](feats/class-14-9DECwTTiVpHJc4B6.htm)|Specialized Megafauna Companion|Compañero especializado en megafauna|modificada|
|[class-14-9t6Kfk8Yw8WJYY8Z.htm](feats/class-14-9t6Kfk8Yw8WJYY8Z.htm)|Guiding Finish|Remate orientador|modificada|
|[class-14-9zfcIXDG2mDpiypp.htm](feats/class-14-9zfcIXDG2mDpiypp.htm)|Armored Exercise|Ejercicio Blindado|modificada|
|[class-14-9zH7IOsmhRBEqXAV.htm](feats/class-14-9zH7IOsmhRBEqXAV.htm)|Instant Opening|Apertura instantánea|modificada|
|[class-14-A7uJCDT9odsuwlqW.htm](feats/class-14-A7uJCDT9odsuwlqW.htm)|Triggerbrand Blitz|Triggerbrand Blitz|modificada|
|[class-14-AGydz5DKJ2KHSO4S.htm](feats/class-14-AGydz5DKJ2KHSO4S.htm)|Whirlwind Strike|Golpe de torbellino|modificada|
|[class-14-AiDelwfA0uVT2lN3.htm](feats/class-14-AiDelwfA0uVT2lN3.htm)|Signature Synergy|Sinergia de firma|modificada|
|[class-14-aOYnK0xe9DKFtx7d.htm](feats/class-14-aOYnK0xe9DKFtx7d.htm)|Innocent Butterfly|Mariposa Inocente|modificada|
|[class-14-aPt0WfFoeLTzyQRA.htm](feats/class-14-aPt0WfFoeLTzyQRA.htm)|Defensive Roll|Rodar a la defensiva|modificada|
|[class-14-Aqhsx5duEpBgaPB0.htm](feats/class-14-Aqhsx5duEpBgaPB0.htm)|Shared Prey|Presa compartida|modificada|
|[class-14-As8cRK5jVzf62fEd.htm](feats/class-14-As8cRK5jVzf62fEd.htm)|Peculiar Anatomy|Peculiar Anatomy|modificada|
|[class-14-AtpMrGXaMPJtDIDR.htm](feats/class-14-AtpMrGXaMPJtDIDR.htm)|Prayer Attack|Ataque de Oración|modificada|
|[class-14-AV6KZvUed7GdhHzc.htm](feats/class-14-AV6KZvUed7GdhHzc.htm)|Bizarre Transformation|Bizarre Transformation|modificada|
|[class-14-b5K067Pma4Il9IeD.htm](feats/class-14-b5K067Pma4Il9IeD.htm)|Anchoring Aura|Aura anclante|modificada|
|[class-14-B912Vru18B6orzcs.htm](feats/class-14-B912Vru18B6orzcs.htm)|Trespass Teleportation|Teletransporte de intrusión|modificada|
|[class-14-bEdFgywri7fABhBT.htm](feats/class-14-bEdFgywri7fABhBT.htm)|Absorb Spell|Absorber Hechizo|modificada|
|[class-14-Bi4rdz48MgkSY7su.htm](feats/class-14-Bi4rdz48MgkSY7su.htm)|Crimson Oath Devotion|Juramento Carmesí Devoción|modificada|
|[class-14-bjNeSAldeTzRcEaQ.htm](feats/class-14-bjNeSAldeTzRcEaQ.htm)|Interweave Dispel|Entretejer disipación|modificada|
|[class-14-bki36RiEM5FR4aiT.htm](feats/class-14-bki36RiEM5FR4aiT.htm)|Specialized Beastmaster Companion|Compañero especializado en maestro de bestias|modificada|
|[class-14-bSC18SbdaNXfBHu9.htm](feats/class-14-bSC18SbdaNXfBHu9.htm)|Dance of Thunder|Danza del Trueno|modificada|
|[class-14-cgtVYaZRVaTIW4sk.htm](feats/class-14-cgtVYaZRVaTIW4sk.htm)|Spell-Repelling Form|Spell-Repelling Form|modificada|
|[class-14-CR9NcAIPTT4oWSEy.htm](feats/class-14-CR9NcAIPTT4oWSEy.htm)|Hammer Quake|Martillo Quake|modificada|
|[class-14-ct4dJHBn1Dj4cx4B.htm](feats/class-14-ct4dJHBn1Dj4cx4B.htm)|Tongue of Sun and Moon|Lengua del sol y de la luna|modificada|
|[class-14-dBB71rps7dbat4Vo.htm](feats/class-14-dBB71rps7dbat4Vo.htm)|Dream Logic|Lógica del Sue|modificada|
|[class-14-dloGUhZYG1xUPVE4.htm](feats/class-14-dloGUhZYG1xUPVE4.htm)|Whirling Knockdown|Derribo Torbellino|modificada|
|[class-14-DUb1VWSbTjdsbAkQ.htm](feats/class-14-DUb1VWSbTjdsbAkQ.htm)|Deity's Protection|Protección del dios|modificada|
|[class-14-Ec9Q8cvOKFgeezx6.htm](feats/class-14-Ec9Q8cvOKFgeezx6.htm)|Terrible Transformation|Transformación Terrible|modificada|
|[class-14-EeSP1SNlgAuASvkP.htm](feats/class-14-EeSP1SNlgAuASvkP.htm)|Preternatural Parry|Parada preternatural|modificada|
|[class-14-eR0sifECG27CC4do.htm](feats/class-14-eR0sifECG27CC4do.htm)|Shape Of The Dragon|Forma de dragón|modificada|
|[class-14-erCOcFZJPT2O3gwC.htm](feats/class-14-erCOcFZJPT2O3gwC.htm)|Aura of Righteousness|Aura de rectitud|modificada|
|[class-14-eUv2L0CLidxtA3sh.htm](feats/class-14-eUv2L0CLidxtA3sh.htm)|Dormant Eruption|Erupción latente|modificada|
|[class-14-EVNd9hZs49b1pScR.htm](feats/class-14-EVNd9hZs49b1pScR.htm)|Dual Onslaught|Dual Onslaught|modificada|
|[class-14-EvSfoYmuCDCRAvaF.htm](feats/class-14-EvSfoYmuCDCRAvaF.htm)|Divine Reflexes|Reflejos divinos|modificada|
|[class-14-ewbt80Yin18k6oLq.htm](feats/class-14-ewbt80Yin18k6oLq.htm)|Tangled Forest Rake|Rasgado del bosque enmarado|modificada|
|[class-14-F7Ao9p17ocf3JVvy.htm](feats/class-14-F7Ao9p17ocf3JVvy.htm)|Shadow Power|Poder de Sombra|modificada|
|[class-14-gQKPKSS5KyK3uUfs.htm](feats/class-14-gQKPKSS5KyK3uUfs.htm)|Resolute Defender|Defensor Resuelto|modificada|
|[class-14-GsrwoQ7DIjERXuPf.htm](feats/class-14-GsrwoQ7DIjERXuPf.htm)|Superior Bond|Vínculo superior|modificada|
|[class-14-h7KZXNRm1gLV1yTt.htm](feats/class-14-h7KZXNRm1gLV1yTt.htm)|Mist Escape|Niebla Huir|modificada|
|[class-14-h8OB6zcdIqUPS6PC.htm](feats/class-14-h8OB6zcdIqUPS6PC.htm)|Airborne Form|Forma Aérea|modificada|
|[class-14-hO4sKslTrSQMLbGx.htm](feats/class-14-hO4sKslTrSQMLbGx.htm)|Mountain Quake|Temblor de la monta|modificada|
|[class-14-HVwmYfSLhrnCksHV.htm](feats/class-14-HVwmYfSLhrnCksHV.htm)|Targeting Shot|Tiro de apuntar|modificada|
|[class-14-HxkAhxcGvua6SkfS.htm](feats/class-14-HxkAhxcGvua6SkfS.htm)|Glib Mutagen|Mutágeno elocuente|modificada|
|[class-14-I1xq9TDYRHNtqzGz.htm](feats/class-14-I1xq9TDYRHNtqzGz.htm)|Sink and Swim|Hundirse y Nadar|modificada|
|[class-14-ibaxh77mm8ttObdk.htm](feats/class-14-ibaxh77mm8ttObdk.htm)|Thwart Evil|Frustrar el maligno|modificada|
|[class-14-Ice8oNOTbPFXyOww.htm](feats/class-14-Ice8oNOTbPFXyOww.htm)|Aura of Vengeance|Aura de venganza|modificada|
|[class-14-IdKfWg48qMieuzl7.htm](feats/class-14-IdKfWg48qMieuzl7.htm)|Murderer's Circle|Murderer's Circle|modificada|
|[class-14-IqDbNiwHQH1xApo9.htm](feats/class-14-IqDbNiwHQH1xApo9.htm)|Ironblood Surge|Oleada de sangre de hierro|modificada|
|[class-14-j4QSlswoBCVrPYa8.htm](feats/class-14-j4QSlswoBCVrPYa8.htm)|Consecrated Aura|Aura consagrada|modificada|
|[class-14-jdrdkxqautAtszCX.htm](feats/class-14-jdrdkxqautAtszCX.htm)|Share Eidolon Magic|Compartir Magia Eidolon|modificada|
|[class-14-jEq4JcZb0LpKOZy1.htm](feats/class-14-jEq4JcZb0LpKOZy1.htm)|Diverse Armor Expert|Experto en armaduras diversas|modificada|
|[class-14-JgidFws6bOoGQcti.htm](feats/class-14-JgidFws6bOoGQcti.htm)|Words of Unraveling|Palabras de desentrañamiento|modificada|
|[class-14-jhNkuXg8vJ29GSUJ.htm](feats/class-14-jhNkuXg8vJ29GSUJ.htm)|Deep Roots|Raíces Profundas|modificada|
|[class-14-jK0hjx4qsRyKnpBd.htm](feats/class-14-jK0hjx4qsRyKnpBd.htm)|Starlight Armor|Armadura Starlight|modificada|
|[class-14-jwQERVkjtnlFp3Ec.htm](feats/class-14-jwQERVkjtnlFp3Ec.htm)|Fast Channel|Canalización rápida|modificada|
|[class-14-k0h4jvjxK0fYFvOU.htm](feats/class-14-k0h4jvjxK0fYFvOU.htm)|Pact of Eldritch Eyes|Pacto de los Ojos de Eldritch|modificada|
|[class-14-k42ntHdg70ZMEKrs.htm](feats/class-14-k42ntHdg70ZMEKrs.htm)|Purifying Breeze|Brisa purificadora|modificada|
|[class-14-kC92uxCvrxDkrCpO.htm](feats/class-14-kC92uxCvrxDkrCpO.htm)|Borrow Memories|Borrow Memories|modificada|
|[class-14-l5nqNPmZPG6lYFnZ.htm](feats/class-14-l5nqNPmZPG6lYFnZ.htm)|Hasted Assault|Asalto acelerado|modificada|
|[class-14-LDv6RVuDXJ9nOfhj.htm](feats/class-14-LDv6RVuDXJ9nOfhj.htm)|Halcyon Spellcasting Adept|Halcyon Spellcasting Adept|modificada|
|[class-14-LhqWrgeBH2sdRxND.htm](feats/class-14-LhqWrgeBH2sdRxND.htm)|Unlimited Ghost Flight|Vuelo fantasmal ilimitado|modificada|
|[class-14-llawV63qzdynbOkx.htm](feats/class-14-llawV63qzdynbOkx.htm)|Stone Communion|Comunión de Piedra|modificada|
|[class-14-LmdOWCDffhBiyzM3.htm](feats/class-14-LmdOWCDffhBiyzM3.htm)|Soothing Ballad|Balada tranquilizadora|modificada|
|[class-14-lPoP5TFfq266kR6g.htm](feats/class-14-lPoP5TFfq266kR6g.htm)|Spellmaster's Tenacity|Spellmaster's Tenacity|modificada|
|[class-14-lZA7SYBmSPFlKNr2.htm](feats/class-14-lZA7SYBmSPFlKNr2.htm)|Effortless Captivation|Cautivación sin esfuerzo|modificada|
|[class-14-matJDIUDvgaJqyiF.htm](feats/class-14-matJDIUDvgaJqyiF.htm)|Hand of the Lich|Mano del liche|modificada|
|[class-14-MFqFvuiYDAoADcft.htm](feats/class-14-MFqFvuiYDAoADcft.htm)|Giant's Lunge|Acometer Gigante|modificada|
|[class-14-MfvkZ9efD013H34Z.htm](feats/class-14-MfvkZ9efD013H34Z.htm)|Come at Me!|¡Ven hacia mí!|modificada|
|[class-14-mprVzno2BR2VhRSJ.htm](feats/class-14-mprVzno2BR2VhRSJ.htm)|Winter's Kiss|Beso de Invierno|modificada|
|[class-14-MwozkE6aj42WZ7Z1.htm](feats/class-14-MwozkE6aj42WZ7Z1.htm)|Stealthy Companion|Comparo sigiloso|modificada|
|[class-14-MyDluIbnX2sjm3pB.htm](feats/class-14-MyDluIbnX2sjm3pB.htm)|Ranged Disarm|Desarmar a distancia|modificada|
|[class-14-N02sHmzuF9XQj93m.htm](feats/class-14-N02sHmzuF9XQj93m.htm)|Resilient Shell|Resistente Shell|modificada|
|[class-14-NCsjkjEp6kHFS07h.htm](feats/class-14-NCsjkjEp6kHFS07h.htm)|Concentrated Assault|Asalto concentrado|modificada|
|[class-14-NDbNNMouxnnwBqwm.htm](feats/class-14-NDbNNMouxnnwBqwm.htm)|Explosive Maneuver|Maniobra Explosión|modificada|
|[class-14-nvPxCUOCMaYdhLp1.htm](feats/class-14-nvPxCUOCMaYdhLp1.htm)|Desperate Finisher|Rematador desesperado|modificada|
|[class-14-nY8HtHNJMqP0hz3v.htm](feats/class-14-nY8HtHNJMqP0hz3v.htm)|Phase Bullet|Fase Bala|modificada|
|[class-14-OEwNLolzBarx8icm.htm](feats/class-14-OEwNLolzBarx8icm.htm)|Explosive Death Drop|Explosión de muerte|modificada|
|[class-14-orjVLLoziFTmf1mz.htm](feats/class-14-orjVLLoziFTmf1mz.htm)|Verdant Metamorphosis|Metamorfosis verde|modificada|
|[class-14-Pk6qGMlef5SMhhwE.htm](feats/class-14-Pk6qGMlef5SMhhwE.htm)|Night's Warning|Night's Warning|modificada|
|[class-14-PSpwdvuddC9kXONz.htm](feats/class-14-PSpwdvuddC9kXONz.htm)|Death's Door|Death's Door|modificada|
|[class-14-psr9lxsnj9aOiqlK.htm](feats/class-14-psr9lxsnj9aOiqlK.htm)|Sacral Monarch|Monarca sacro|modificada|
|[class-14-q0DGOuLuJNLzgM8d.htm](feats/class-14-q0DGOuLuJNLzgM8d.htm)|Unending Emptiness|Vacío interminable|modificada|
|[class-14-q2t5qfgQHFJbbV8d.htm](feats/class-14-q2t5qfgQHFJbbV8d.htm)|Shift Spell|Hechizo Cambiante|modificada|
|[class-14-Q4NiHmThMtk8razS.htm](feats/class-14-Q4NiHmThMtk8razS.htm)|Headshot|Headshot|modificada|
|[class-14-QC2ecMZ57MRJlxco.htm](feats/class-14-QC2ecMZ57MRJlxco.htm)|Bonded Focus|Foco vinculado|modificada|
|[class-14-QpnZwabXOVICJL5i.htm](feats/class-14-QpnZwabXOVICJL5i.htm)|Greater Merciful Elixir|Elixir compasivo mayor|modificada|
|[class-14-r4ZVpDjX2X0yrmhw.htm](feats/class-14-r4ZVpDjX2X0yrmhw.htm)|Blast Tackle|Blast Tackle|modificada|
|[class-14-R5hdEbfKdIVAQs24.htm](feats/class-14-R5hdEbfKdIVAQs24.htm)|Paragon Companion|Paragon Companion|modificada|
|[class-14-RbHacJSoe6XHT8Ks.htm](feats/class-14-RbHacJSoe6XHT8Ks.htm)|Litany of Righteousness|Letanía de rectitud|modificada|
|[class-14-rCnaBbk0M1gBVHjG.htm](feats/class-14-rCnaBbk0M1gBVHjG.htm)|Awesome Blow|Golpe asombroso|modificada|
|[class-14-RHdY0xczRYzkIdJt.htm](feats/class-14-RHdY0xczRYzkIdJt.htm)|Instinctual Interception|Intercepción Instintiva|modificada|
|[class-14-RnxullWsNdbU7fuH.htm](feats/class-14-RnxullWsNdbU7fuH.htm)|Pivot Strike|Golpe de Pivote|modificada|
|[class-14-roItUHUbBqhHfwJr.htm](feats/class-14-roItUHUbBqhHfwJr.htm)|Seeker Arrow|Flecha Buscadora|modificada|
|[class-14-rOx7r8ygmPHPC6qF.htm](feats/class-14-rOx7r8ygmPHPC6qF.htm)|Allegro|Allegro|modificada|
|[class-14-S450JMWfF90oOcv9.htm](feats/class-14-S450JMWfF90oOcv9.htm)|Path Of Iron|Sendero De Hierro|modificada|
|[class-14-SA8rnheHFtjkATrJ.htm](feats/class-14-SA8rnheHFtjkATrJ.htm)|Shifting Terrain|Cambiante Terreno|modificada|
|[class-14-sFAqKrzqXQhtrfqN.htm](feats/class-14-sFAqKrzqXQhtrfqN.htm)|Sixth Pillar Mastery|Dominio del Sexto Pilar|modificada|
|[class-14-ST6AhCbEDSMxXf20.htm](feats/class-14-ST6AhCbEDSMxXf20.htm)|Perfect Ki Exemplar|Perfect Ki Exemplar|modificada|
|[class-14-t3unBu3PX6AO0uIW.htm](feats/class-14-t3unBu3PX6AO0uIW.htm)|Swift Banishment|Destierro rápido|modificada|
|[class-14-T3XFrLIBzir9IqD5.htm](feats/class-14-T3XFrLIBzir9IqD5.htm)|Extend Armament Alignment|Prolongar alineación de armamento|modificada|
|[class-14-TdA3oVj79KxOm2Kd.htm](feats/class-14-TdA3oVj79KxOm2Kd.htm)|Wind-Tossed Spell|Hechizo Lanzado por el Viento|modificada|
|[class-14-TNRB8IY6Wtk9BoMp.htm](feats/class-14-TNRB8IY6Wtk9BoMp.htm)|Terrain Shield|Escudo de terreno|modificada|
|[class-14-TpWS2b9ISHnXVfZg.htm](feats/class-14-TpWS2b9ISHnXVfZg.htm)|Timeless Nature|Naturaleza atemporal|modificada|
|[class-14-TQCpXi1hwYX6VIhp.htm](feats/class-14-TQCpXi1hwYX6VIhp.htm)|Speedy Rituals|Rituales rápidos|modificada|
|[class-14-UA3iZTAZrugKClKE.htm](feats/class-14-UA3iZTAZrugKClKE.htm)|Shatter Space|Estallar Espacio|modificada|
|[class-14-ubF6nGJrnfW7ocSg.htm](feats/class-14-ubF6nGJrnfW7ocSg.htm)|Graveshift|Graveshift|modificada|
|[class-14-UJcuACMlspc1raL1.htm](feats/class-14-UJcuACMlspc1raL1.htm)|Curse of the Saumen Kar|Maldición del Saumen Kar|modificada|
|[class-14-UxurtbOOvCkngsKN.htm](feats/class-14-UxurtbOOvCkngsKN.htm)|Paragon Reanimated Companion|Paragon Reanimated Companion|modificada|
|[class-14-UZpBsHE57rmS0x6S.htm](feats/class-14-UZpBsHE57rmS0x6S.htm)|Showstopper|Detenedor de espectáculos|modificada|
|[class-14-UzuMXY8G88ULDRex.htm](feats/class-14-UzuMXY8G88ULDRex.htm)|Disruptive Blur|Disruptora contorno borroso|modificada|
|[class-14-VW0Tp5rJDRjaJuSn.htm](feats/class-14-VW0Tp5rJDRjaJuSn.htm)|Two-Weapon Fusillade|Two-Weapon Fusillade|modificada|
|[class-14-Vzbu8tclrsS2IBYU.htm](feats/class-14-Vzbu8tclrsS2IBYU.htm)|Seize|Seize|modificada|
|[class-14-w2v5LZmpJy0MBxo5.htm](feats/class-14-w2v5LZmpJy0MBxo5.htm)|Improved Twin Riposte (Fighter)|Réplica gemela mejorada (Luchador)|modificada|
|[class-14-wBL5h0hTmVD4EJLw.htm](feats/class-14-wBL5h0hTmVD4EJLw.htm)|Control Tower|Torre de control|modificada|
|[class-14-wdkbfWKEjAFXAxto.htm](feats/class-14-wdkbfWKEjAFXAxto.htm)|Determination|Determinación|modificada|
|[class-14-WKRSBu7H9miAwUaR.htm](feats/class-14-WKRSBu7H9miAwUaR.htm)|Spirit Guide Form|Spirit Guide Form|modificada|
|[class-14-wKx7BWdQu5sEjL9j.htm](feats/class-14-wKx7BWdQu5sEjL9j.htm)|Wild Winds Gust|Ráfaga de viento salvaje|modificada|
|[class-14-wnrlBkwVr8BSVAZt.htm](feats/class-14-wnrlBkwVr8BSVAZt.htm)|Sepulchral Sublimation|Sublimación Sepulcral|modificada|
|[class-14-xdv75qkv5TOFlHmM.htm](feats/class-14-xdv75qkv5TOFlHmM.htm)|Keep up the Good Fight|Keep up the Good Fight|modificada|
|[class-14-Xk9inG3pln4UKbs3.htm](feats/class-14-Xk9inG3pln4UKbs3.htm)|True Hypercognition|Verdadera hipercognición|modificada|
|[class-14-XydqgTE4J119J5JV.htm](feats/class-14-XydqgTE4J119J5JV.htm)|Arcane Shroud|Mortaja arcana|modificada|
|[class-14-YawVDUc9uzREIAnO.htm](feats/class-14-YawVDUc9uzREIAnO.htm)|Aura of Life|Aura de vida|modificada|
|[class-14-yeSyGnYDkl2GUNmu.htm](feats/class-14-yeSyGnYDkl2GUNmu.htm)|Stance Savant (Fighter)|Sabio de las posiciones (luchador).|modificada|
|[class-14-zK0Dr1FZDOq2DMn8.htm](feats/class-14-zK0Dr1FZDOq2DMn8.htm)|Improved Hijack Undead|Improved Hijack Undead|modificada|
|[class-14-ZNq7Qgubi9gUqR0L.htm](feats/class-14-ZNq7Qgubi9gUqR0L.htm)|Bodysnatcher|Bodysnatcher|modificada|
|[class-14-ZTxiM8NExDmxHJDf.htm](feats/class-14-ZTxiM8NExDmxHJDf.htm)|Pin to the Spot|Pin to the Spot|modificada|
|[class-16-05k4nkjazjjEUoGu.htm](feats/class-16-05k4nkjazjjEUoGu.htm)|Blank Slate|Pizarra en blanco|modificada|
|[class-16-1re3J4hWW7raXIRB.htm](feats/class-16-1re3J4hWW7raXIRB.htm)|Spell Tinker|Manipular conjuros Trastear|modificada|
|[class-16-2LdncNMDNJW5Oeyu.htm](feats/class-16-2LdncNMDNJW5Oeyu.htm)|Opportune Throw|Lanzamiento Oportuno|modificada|
|[class-16-54JzsYCx3uoj7Wlz.htm](feats/class-16-54JzsYCx3uoj7Wlz.htm)|Monstrosity Shape|Forma de monstruosidad|modificada|
|[class-16-5ZoIJImgvpdhGcDR.htm](feats/class-16-5ZoIJImgvpdhGcDR.htm)|Just the Thing!|¡Just the Thing!|modificada|
|[class-16-6AXAJuMEMpxU27PJ.htm](feats/class-16-6AXAJuMEMpxU27PJ.htm)|Dispelling Slice|Tajo disipador|modificada|
|[class-16-7GGxxoYNA4YrtML9.htm](feats/class-16-7GGxxoYNA4YrtML9.htm)|Trample|Trample|modificada|
|[class-16-7IDFHh2ZJLaB1y59.htm](feats/class-16-7IDFHh2ZJLaB1y59.htm)|Shield of Grace|Escudo de gracia|modificada|
|[class-16-8HYfYT4fHtxXP199.htm](feats/class-16-8HYfYT4fHtxXP199.htm)|Greater Mental Evolution|Evolución mental mayor|modificada|
|[class-16-9wd9hhtfjayeZ6zF.htm](feats/class-16-9wd9hhtfjayeZ6zF.htm)|Giant Slayer|Cazagigantes|modificada|
|[class-16-a6xUSibsRaclbSz3.htm](feats/class-16-a6xUSibsRaclbSz3.htm)|Ever-Vigilant Senses|Sentidos siempre vigilantes|modificada|
|[class-16-Aeea44xyJqYArFun.htm](feats/class-16-Aeea44xyJqYArFun.htm)|Lead The Pack|Lead The Pack|modificada|
|[class-16-AObopXQkCIsaBAID.htm](feats/class-16-AObopXQkCIsaBAID.htm)|Implement's Flight|Vuelo del Implemento|modificada|
|[class-16-aoZYZm2PrTKEK0Ji.htm](feats/class-16-aoZYZm2PrTKEK0Ji.htm)|Converge|Convergencia|modificada|
|[class-16-AV9NkSLNvv0UcdcP.htm](feats/class-16-AV9NkSLNvv0UcdcP.htm)|Breath of Hungry Death|Aliento de Muerte Hambrienta|modificada|
|[class-16-aviDW7htxA77iarV.htm](feats/class-16-aviDW7htxA77iarV.htm)|Hair Trigger|Desencadenante de Pelo|modificada|
|[class-16-AyP23H3WJkEAIEKd.htm](feats/class-16-AyP23H3WJkEAIEKd.htm)|Mobile Magical Combat|Combate Mágico Móvil|modificada|
|[class-16-AYVf9MU8oo1QWbGv.htm](feats/class-16-AYVf9MU8oo1QWbGv.htm)|Wave the Flag|Wave the Flag|modificada|
|[class-16-bX2WI5k0afqPpCfm.htm](feats/class-16-bX2WI5k0afqPpCfm.htm)|Ubiquitous Snares|Trampas de lazo ubicuas|modificada|
|[class-16-Cgk4By6gEomD2bJ0.htm](feats/class-16-Cgk4By6gEomD2bJ0.htm)|Improved Twin Riposte (Ranger)|Réplica gemela mejorada (Ranger).|modificada|
|[class-16-cHOALlKY1XsCj3Fe.htm](feats/class-16-cHOALlKY1XsCj3Fe.htm)|Disciple's Breath|Aliento del discípulo|modificada|
|[class-16-CtC7DM1poHDwwu52.htm](feats/class-16-CtC7DM1poHDwwu52.htm)|Fearsome Fangs|Fearsome Fangs|modificada|
|[class-16-Dr6h8WRW6xnLRfxr.htm](feats/class-16-Dr6h8WRW6xnLRfxr.htm)|Resurrectionist|Resucitador|modificada|
|[class-16-E4xubBMtj81kX5Bk.htm](feats/class-16-E4xubBMtj81kX5Bk.htm)|Shattering Strike (Monk)|Golpe demoledor (Monje)|modificada|
|[class-16-EDdJFwarNIJIkP2E.htm](feats/class-16-EDdJFwarNIJIkP2E.htm)|Master's Counterspell|Contraconjuro del Maestro|modificada|
|[class-16-EwCDbWg8yPxDWF4a.htm](feats/class-16-EwCDbWg8yPxDWF4a.htm)|Stunning Appearance|Aspecto imponente|modificada|
|[class-16-EWeso1zDkCLGlnsW.htm](feats/class-16-EWeso1zDkCLGlnsW.htm)|Reckless Abandon (Barbarian)|Abandono temerario (Bárbaro)|modificada|
|[class-16-Fai5VMyrtOrYC5JL.htm](feats/class-16-Fai5VMyrtOrYC5JL.htm)|Specialized Companion (Ranger)|Comparo especializado (Ranger)|modificada|
|[class-16-fEfEabn53bubYVVT.htm](feats/class-16-fEfEabn53bubYVVT.htm)|Improved Reflexive Shield|Escudo reflectante mejorado.|modificada|
|[class-16-fgnfXwFcn9jZlXGD.htm](feats/class-16-fgnfXwFcn9jZlXGD.htm)|Advanced Runic Mind Smithing|Advanced Runic Mind Smithing|modificada|
|[class-16-g1wBP9Z5HRqDe9FE.htm](feats/class-16-g1wBP9Z5HRqDe9FE.htm)|Quivering Palm|Palma temblorosa|modificada|
|[class-16-giOEclnMp8txkRSU.htm](feats/class-16-giOEclnMp8txkRSU.htm)|Eternal Elixir|Elixir eterno|modificada|
|[class-16-giupBd3dyOwdeoFl.htm](feats/class-16-giupBd3dyOwdeoFl.htm)|Performance Weapon Expert|Experto en armas de Interpretar|modificada|
|[class-16-GKP2dHkgQw1o0k8g.htm](feats/class-16-GKP2dHkgQw1o0k8g.htm)|Persistent Mutagen|Mutágeno persistente|modificada|
|[class-16-gqISxjbTtZSYndZ4.htm](feats/class-16-gqISxjbTtZSYndZ4.htm)|Quickened Attunement|Sintonización acelerada|modificada|
|[class-16-h08Vfel5iIAARWdy.htm](feats/class-16-h08Vfel5iIAARWdy.htm)|Instrument of Zeal|Instrumento de celo|modificada|
|[class-16-hKemfam7Hvkllsgp.htm](feats/class-16-hKemfam7Hvkllsgp.htm)|Purge of Moments|Purga de Momentos|modificada|
|[class-16-hOZP7qOMsNYOe90o.htm](feats/class-16-hOZP7qOMsNYOe90o.htm)|Wandering Thoughts|Pensamientos errantes|modificada|
|[class-16-hr9maYUbtrNxpBPw.htm](feats/class-16-hr9maYUbtrNxpBPw.htm)|Exploitive Bomb|Bomba abusiva|modificada|
|[class-16-IevdA9OVFZE6FOVK.htm](feats/class-16-IevdA9OVFZE6FOVK.htm)|Sever Magic|Sever Magic|modificada|
|[class-16-JezNf3xbCi8h2qKe.htm](feats/class-16-JezNf3xbCi8h2qKe.htm)|Vessel's Form|Forma de la nave|modificada|
|[class-16-jI4Eoi6m0ogjXkGK.htm](feats/class-16-jI4Eoi6m0ogjXkGK.htm)|Master Captivator Spellcasting|Maestro Hechicero Cautivador|modificada|
|[class-16-jSu9h6UvZtUJ7InD.htm](feats/class-16-jSu9h6UvZtUJ7InD.htm)|Fey Life|La vida feérica|modificada|
|[class-16-kMLvQnx2vY7F3bjI.htm](feats/class-16-kMLvQnx2vY7F3bjI.htm)|Eternal Blessing|Bendición eterna|modificada|
|[class-16-kVSZQvsz1cYjOwxL.htm](feats/class-16-kVSZQvsz1cYjOwxL.htm)|Master Siege Engineer|Maestro Ingeniero de Asedio|modificada|
|[class-16-LbMPCAyUrLRHykGA.htm](feats/class-16-LbMPCAyUrLRHykGA.htm)|Persistent Boost|Persistent Boost|modificada|
|[class-16-lgEihn7deZwHczGE.htm](feats/class-16-lgEihn7deZwHczGE.htm)|Dragon Transformation|Transformación de dragón|modificada|
|[class-16-LiLnDkoFcwW1RxqZ.htm](feats/class-16-LiLnDkoFcwW1RxqZ.htm)|Withstand Death|Resistir la muerte|modificada|
|[class-16-lleedxE6fTDSK6og.htm](feats/class-16-lleedxE6fTDSK6og.htm)|Resuscitate|Resucitar|modificada|
|[class-16-LRSTjjBNNlD0XZX8.htm](feats/class-16-LRSTjjBNNlD0XZX8.htm)|Phase Arrow|Flecha de fase|modificada|
|[class-16-mB7ENhF10qoOwQyG.htm](feats/class-16-mB7ENhF10qoOwQyG.htm)|Resounding Cascade|Cascada resonante|modificada|
|[class-16-mDczSj3GolIbkhGJ.htm](feats/class-16-mDczSj3GolIbkhGJ.htm)|Instant Return|Retorno Instantáneo|modificada|
|[class-16-MGtlUc6cy4nCM4Lk.htm](feats/class-16-MGtlUc6cy4nCM4Lk.htm)|Bound in Ice|Bound in Ice|modificada|
|[class-16-Mix4IwZRuCz7JS3T.htm](feats/class-16-Mix4IwZRuCz7JS3T.htm)|Greater Snow Step|Paso de nieve mayor|modificada|
|[class-16-mj1pVVFtqGLKgCQM.htm](feats/class-16-mj1pVVFtqGLKgCQM.htm)|Genius Mutagen|Mutágeno genial|modificada|
|[class-16-MjhCcwGKyI5dpNIY.htm](feats/class-16-MjhCcwGKyI5dpNIY.htm)|Perfect Distraction|Distracción perfecta|modificada|
|[class-16-MUwIeDV0pBtqeU3p.htm](feats/class-16-MUwIeDV0pBtqeU3p.htm)|Walk on the Wind|Caminar con el viento|modificada|
|[class-16-ND3nKsXCDBShUgYc.htm](feats/class-16-ND3nKsXCDBShUgYc.htm)|Greater Vital Evolution|Evolución vital mayor|modificada|
|[class-16-Ndxu9YVGgenYmZSb.htm](feats/class-16-Ndxu9YVGgenYmZSb.htm)|Dispelling Spellstrike|Disipar Hechizo|modificada|
|[class-16-PiKWzPloAR2nqIcC.htm](feats/class-16-PiKWzPloAR2nqIcC.htm)|Constant Levitation|Levitación constante|modificada|
|[class-16-pzmob1HqVKZfL0BY.htm](feats/class-16-pzmob1HqVKZfL0BY.htm)|Eternal Bane|Azote eternoón|modificada|
|[class-16-QGpcyvIezLMgmTia.htm](feats/class-16-QGpcyvIezLMgmTia.htm)|Studious Capacity|Capacidad estudiosa|modificada|
|[class-16-qkHVva51N6H8NNGR.htm](feats/class-16-qkHVva51N6H8NNGR.htm)|Ricochet Master|Ricochet Master|modificada|
|[class-16-qwnyuCsl5YvX0fdY.htm](feats/class-16-qwnyuCsl5YvX0fdY.htm)|Fatal Bullet|Bala fatal|modificada|
|[class-16-qX9ZtfaAj6rxrVA7.htm](feats/class-16-qX9ZtfaAj6rxrVA7.htm)|Master of Many Styles|Maestro de muchos estilos|modificada|
|[class-16-qZzRXAa9mNQPUXoW.htm](feats/class-16-qZzRXAa9mNQPUXoW.htm)|Controlled Bullet|Bala Controlada|modificada|
|[class-16-r7FGPKl5e0xB4tuj.htm](feats/class-16-r7FGPKl5e0xB4tuj.htm)|Cloud Step|Pisada nubosa|modificada|
|[class-16-rFaUJtB46scuAidY.htm](feats/class-16-rFaUJtB46scuAidY.htm)|Graceful Poise|Elegancia grácil|modificada|
|[class-16-ROnjdPMvH0vkkWjQ.htm](feats/class-16-ROnjdPMvH0vkkWjQ.htm)|Enlightened Presence|Presencia iluminada|modificada|
|[class-16-Ruee0G3oZG5n5Auk.htm](feats/class-16-Ruee0G3oZG5n5Auk.htm)|Master Skysage Divination|Maestro Skysage Adivinación|modificada|
|[class-16-RzhnxgiAopWILCvs.htm](feats/class-16-RzhnxgiAopWILCvs.htm)|Multishot Stance|Posición de disparo móvil|modificada|
|[class-16-S8Hda5OtajS9gpqM.htm](feats/class-16-S8Hda5OtajS9gpqM.htm)|Ward Mind|Mente Ward|modificada|
|[class-16-SGgK4BoUooA0HhTj.htm](feats/class-16-SGgK4BoUooA0HhTj.htm)|Avalanche Strike|Golpe Avalancha|modificada|
|[class-16-UgZDirfFb4CFhuWh.htm](feats/class-16-UgZDirfFb4CFhuWh.htm)|Seven-Part Link|Enlace de siete partes|modificada|
|[class-16-uixN9wbfe0veOHRn.htm](feats/class-16-uixN9wbfe0veOHRn.htm)|Auspicious Mount|Montura propicia|modificada|
|[class-16-urYHB6VHhqvPMSy7.htm](feats/class-16-urYHB6VHhqvPMSy7.htm)|Impaling Briars|Los Zarzas empalador|modificada|
|[class-16-vDJRIKS27md3LudA.htm](feats/class-16-vDJRIKS27md3LudA.htm)|Greater Distracting Shot|Tiro de distracción mayor|modificada|
|[class-16-Veyt4x2Kb5YcPGTv.htm](feats/class-16-Veyt4x2Kb5YcPGTv.htm)|Legendary Monster Hunter|Cazador de monstruos legendario|modificada|
|[class-16-VYdZmTifZRkRF7ey.htm](feats/class-16-VYdZmTifZRkRF7ey.htm)|Deflecting Cloud|Nube deflectora|modificada|
|[class-16-w1GFspPIQ9f5MbFL.htm](feats/class-16-w1GFspPIQ9f5MbFL.htm)|Touch Focus|Enfoque táctil|modificada|
|[class-16-WoUwDqhA6i6abwen.htm](feats/class-16-WoUwDqhA6i6abwen.htm)|Song of Grace and Speed|Canción de Gracia y Velocidad|modificada|
|[class-16-wPJFEUOXwf7y5jN3.htm](feats/class-16-wPJFEUOXwf7y5jN3.htm)|Cognitive Loophole|Resquicio cognitivo|modificada|
|[class-16-wwvz79Hwp9qx96lL.htm](feats/class-16-wwvz79Hwp9qx96lL.htm)|Target of Psychic Ire|Objetivo de la ira psíquica|modificada|
|[class-16-WY7CjISdz6uwXwIb.htm](feats/class-16-WY7CjISdz6uwXwIb.htm)|Collateral Thrash|Paliza colateral Sacudirse|modificada|
|[class-16-X5LxcrZbjHfmX58a.htm](feats/class-16-X5LxcrZbjHfmX58a.htm)|Bolster Soul Cage|Jaula de Alma Bolster|modificada|
|[class-16-xjLbabfyQzBNT4y1.htm](feats/class-16-xjLbabfyQzBNT4y1.htm)|Twinned Defense (Fighter)|Defensa gemela (Luchador)|modificada|
|[class-16-XpZkzUV9PwUHvmyq.htm](feats/class-16-XpZkzUV9PwUHvmyq.htm)|Fulminating Synergy|Sinergia Fulminante|modificada|
|[class-16-XRCqj74dG27MHNxQ.htm](feats/class-16-XRCqj74dG27MHNxQ.htm)|Spell Gem|Gema de Hechizo|modificada|
|[class-16-XXOc3MxbkEBMgFeT.htm](feats/class-16-XXOc3MxbkEBMgFeT.htm)|Cross the Threshold|Cruza el Umbral|modificada|
|[class-16-xYNMWGEmpbtrtWXQ.htm](feats/class-16-xYNMWGEmpbtrtWXQ.htm)|Unwind Death|Desenrollar Muerte|modificada|
|[class-16-yqIorA6QGWmbKoOz.htm](feats/class-16-yqIorA6QGWmbKoOz.htm)|Dominating Gaze|Mirada Dominante|modificada|
|[class-16-zBS1qFyIpFuCGhWW.htm](feats/class-16-zBS1qFyIpFuCGhWW.htm)|You Failed to Account For... This!|You Failed to Account For... ¡Esto!|modificada|
|[class-16-zfnZki2CxmZXdNBO.htm](feats/class-16-zfnZki2CxmZXdNBO.htm)|Electric Counter|Contador Eléctrico|modificada|
|[class-16-zodscjgydZRUSOLO.htm](feats/class-16-zodscjgydZRUSOLO.htm)|Focus Ally|Focus Ally|modificada|
|[class-16-ZXLbIgL3NNplgnuB.htm](feats/class-16-ZXLbIgL3NNplgnuB.htm)|Shared Dream|Sue compartido|modificada|
|[class-18-218WeRqUqdnguRdS.htm](feats/class-18-218WeRqUqdnguRdS.htm)|Master Druid Spellcasting|Lanzamiento de conjuros maestro de druida|modificada|
|[class-18-2RaDe6Fi4t7S2IDF.htm](feats/class-18-2RaDe6Fi4t7S2IDF.htm)|Invoke Disaster|Invocar desastre|modificada|
|[class-18-5fd95NhJJ55hm0Qt.htm](feats/class-18-5fd95NhJJ55hm0Qt.htm)|Link Wellspring|Link Wellspring|modificada|
|[class-18-5JG0kjxukERBeayd.htm](feats/class-18-5JG0kjxukERBeayd.htm)|Frightful Aura|Aura espantosa|modificada|
|[class-18-5NWZct5OvSiVvMn8.htm](feats/class-18-5NWZct5OvSiVvMn8.htm)|Master Cathartic Spellcasting|Maestro lanzador de conjuros catártico|modificada|
|[class-18-6A1WTVY1PUlXy3rW.htm](feats/class-18-6A1WTVY1PUlXy3rW.htm)|Versatile Spellstrike|Versatile Spellstrike|modificada|
|[class-18-6GUl9WG7OKvfVQo4.htm](feats/class-18-6GUl9WG7OKvfVQo4.htm)|Empty Body|Cuerpo vacío|modificada|
|[class-18-6iOLxitjqHujH1Tj.htm](feats/class-18-6iOLxitjqHujH1Tj.htm)|Arrow of Death|Flecha de la Muerte|modificada|
|[class-18-6kz7FPdxDrsPiNti.htm](feats/class-18-6kz7FPdxDrsPiNti.htm)|Master Eldritch Archer Spellcasting|Maestro Arquero Eldritch Dedicación de conjuros|modificada|
|[class-18-6qNRVKwbnX381jVj.htm](feats/class-18-6qNRVKwbnX381jVj.htm)|Master Beast Gunner Spellcasting|Maestro Pistolero de Bestias Lanzamiento de Conjuros|modificada|
|[class-18-72qOqbwShnT2Apaw.htm](feats/class-18-72qOqbwShnT2Apaw.htm)|Implement's Assault|Asalto de Implemento|modificada|
|[class-18-7mGMHb7irGKZ0eQo.htm](feats/class-18-7mGMHb7irGKZ0eQo.htm)|Miraculous Possibility|Posibilidad Milagrosa|modificada|
|[class-18-7uofkNynjFy3ofk2.htm](feats/class-18-7uofkNynjFy3ofk2.htm)|Ever Dreaming|Siempre Sue|modificada|
|[class-18-9v3wpgvEEiZHO0SJ.htm](feats/class-18-9v3wpgvEEiZHO0SJ.htm)|Perfect Ki Grandmaster|Gran Maestro del Ki Perfecto|modificada|
|[class-18-a1TSGGsA6b5gjP3H.htm](feats/class-18-a1TSGGsA6b5gjP3H.htm)|Reprepare Spell|Volver a preparar conjuro|modificada|
|[class-18-AJRWIcBGCIxXG0RS.htm](feats/class-18-AJRWIcBGCIxXG0RS.htm)|Master Magus Spellcasting|Mago Maestro Lanzador de Conjuros|modificada|
|[class-18-aoIEVpJrQEold2Mi.htm](feats/class-18-aoIEVpJrQEold2Mi.htm)|Magical Master|Magical Master|modificada|
|[class-18-AwxJcaIrutqMcUC8.htm](feats/class-18-AwxJcaIrutqMcUC8.htm)|Masterful Companion|Comparo maestro|modificada|
|[class-18-bc6tcXSzakyCbQsS.htm](feats/class-18-bc6tcXSzakyCbQsS.htm)|Master Cleric Spellcasting|Lanzamiento de conjuros maestro de clérigo|modificada|
|[class-18-blOiU4LPlBjVHcgR.htm](feats/class-18-blOiU4LPlBjVHcgR.htm)|Look Again|Look Again|modificada|
|[class-18-BqcAwmNjDlKEI84X.htm](feats/class-18-BqcAwmNjDlKEI84X.htm)|Shadow Master|Maestro de Sombra|modificada|
|[class-18-BtMemftiktI0Mn6X.htm](feats/class-18-BtMemftiktI0Mn6X.htm)|Manifold Edge (Precision)|Ventaja múltiple (precisión)|modificada|
|[class-18-C9nb7H5u2ElBXvCR.htm](feats/class-18-C9nb7H5u2ElBXvCR.htm)|Mindblank Mutagen|Mutágeno de mente en blanco|modificada|
|[class-18-Cs0hRKBfWn2gOYzK.htm](feats/class-18-Cs0hRKBfWn2gOYzK.htm)|Devastating Weaponry|Armamento devastador|modificada|
|[class-18-Dcr63tofZUome1Ze.htm](feats/class-18-Dcr63tofZUome1Ze.htm)|Manifold Edge|Ventaja múltiple|modificada|
|[class-18-DFY3X7Mgl9rESQuu.htm](feats/class-18-DFY3X7Mgl9rESQuu.htm)|Mighty Dragon Shape|Poderosa Forma de dragón|modificada|
|[class-18-doD3jZylVXZ0oHWO.htm](feats/class-18-doD3jZylVXZ0oHWO.htm)|Greater Crossblooded Evolution|Evolución mestiza mayor|modificada|
|[class-18-EcQvuGYDJdqTfGuX.htm](feats/class-18-EcQvuGYDJdqTfGuX.htm)|Manifold Edge (Outwit)|Ventaja múltiple (Outwit)|modificada|
|[class-18-eJNzP21lFPV3zWkm.htm](feats/class-18-eJNzP21lFPV3zWkm.htm)|Master Bard Spellcasting|Lanzamiento de conjuros maestro de bardo|modificada|
|[class-18-f1acyuIGYVp2BpKc.htm](feats/class-18-f1acyuIGYVp2BpKc.htm)|Shadow Hunter|Cazador sombrío|modificada|
|[class-18-fLrwddS607eRFfHA.htm](feats/class-18-fLrwddS607eRFfHA.htm)|Implausible Infiltration|Infiltración inverosímil|modificada|
|[class-18-fyJ2slL98hnQH3On.htm](feats/class-18-fyJ2slL98hnQH3On.htm)|Soaring Dynamo|Soaring Dynamo|modificada|
|[class-18-G8iGrx1qBDwLk1HO.htm](feats/class-18-G8iGrx1qBDwLk1HO.htm)|Piercing Critical|Piercing Critical|modificada|
|[class-18-GLHh9MWlcINLC6Q0.htm](feats/class-18-GLHh9MWlcINLC6Q0.htm)|Engine of Destruction|Motor de Destrucción|modificada|
|[class-18-GWnQMjSAIG6qcV8J.htm](feats/class-18-GWnQMjSAIG6qcV8J.htm)|Reach for the Stars|Alcanzar las estrellas|modificada|
|[class-18-HBhLR980Q0cb2rxp.htm](feats/class-18-HBhLR980Q0cb2rxp.htm)|Perfect Debilitation|Debilitación perfecta|modificada|
|[class-18-hdt3RHZljLrO49kq.htm](feats/class-18-hdt3RHZljLrO49kq.htm)|Specialized Companion (Animal Trainer)|Comparo especializado (entrenador de animales)|modificada|
|[class-18-Hzzf7bi8xBMi6DCL.htm](feats/class-18-Hzzf7bi8xBMi6DCL.htm)|Powerful Sneak|Movimiento furtivo poderoso|modificada|
|[class-18-i3hALsjbjk9FdbGN.htm](feats/class-18-i3hALsjbjk9FdbGN.htm)|Vicious Evisceration|Destripamiento cruel|modificada|
|[class-18-iTtnN49D8ZJ2Ilur.htm](feats/class-18-iTtnN49D8ZJ2Ilur.htm)|Deep Lore|Saber profundo|modificada|
|[class-18-iuLLw1X5RjRcR4rH.htm](feats/class-18-iuLLw1X5RjRcR4rH.htm)|Halcyon Spellcasting Sage|Halcyon Spellcasting Sage|modificada|
|[class-18-IxggfXunfldeVOsQ.htm](feats/class-18-IxggfXunfldeVOsQ.htm)|Swift River|Río rápido|modificada|
|[class-18-J8HLcsOkAcXfTxYy.htm](feats/class-18-J8HLcsOkAcXfTxYy.htm)|Negate Damage|Negar Daño|modificada|
|[class-18-JkQjKyzfhMWLr9Gs.htm](feats/class-18-JkQjKyzfhMWLr9Gs.htm)|Perfect Clarity|Claridad perfecta|modificada|
|[class-18-JSkaMCO6pzKYZrZe.htm](feats/class-18-JSkaMCO6pzKYZrZe.htm)|Unerring Shot|Tiro infalible|modificada|
|[class-18-K9hM3AdWGbU3VE8L.htm](feats/class-18-K9hM3AdWGbU3VE8L.htm)|Final Shot|Disparo Final|modificada|
|[class-18-kPsJIz19viZy8YjO.htm](feats/class-18-kPsJIz19viZy8YjO.htm)|Master Summoning Spellcasting|Lanzador de conjuros maestro en Invocación|modificada|
|[class-18-kwz8el0Z8kK0D5az.htm](feats/class-18-kwz8el0Z8kK0D5az.htm)|True Transmogrification|True Transmogrification|modificada|
|[class-18-Ky7ZEtt4TchQNmFc.htm](feats/class-18-Ky7ZEtt4TchQNmFc.htm)|Timeline-Splitting Spell|Hechizo de división de la línea temporal|modificada|
|[class-18-kYLtH4PYfvqruLyo.htm](feats/class-18-kYLtH4PYfvqruLyo.htm)|All in Your Head|Todo está en tu cabeza|modificada|
|[class-18-L5n4PvQYhpl2WM9e.htm](feats/class-18-L5n4PvQYhpl2WM9e.htm)|Impossible Polymath|Polifacético imposible|modificada|
|[class-18-LAHiW98iPJKplFyK.htm](feats/class-18-LAHiW98iPJKplFyK.htm)|Infinite Possibilities|Posibilidades infinitas|modificada|
|[class-18-LHjPTV5vP3MOsPPJ.htm](feats/class-18-LHjPTV5vP3MOsPPJ.htm)|Grand Scroll Esoterica|Gran Pergamino Esotérico|modificada|
|[class-18-lIg5Gzz7W70jfbk1.htm](feats/class-18-lIg5Gzz7W70jfbk1.htm)|Master Scroll Cache|Caché de pergamino maestro|modificada|
|[class-18-LLCf2xPXA7FVQT1D.htm](feats/class-18-LLCf2xPXA7FVQT1D.htm)|Effortless Reach|Alcance sin esfuerzo|modificada|
|[class-18-m2pHkmyGvkwqfSSN.htm](feats/class-18-m2pHkmyGvkwqfSSN.htm)|Miracle Worker|Obrador de milagros|modificada|
|[class-18-mCXoiMLAbGHGsZS3.htm](feats/class-18-mCXoiMLAbGHGsZS3.htm)|Improved Swift Banishment|Destierro rápido mejorado.|modificada|
|[class-18-NAgaDfwUSjPAon4o.htm](feats/class-18-NAgaDfwUSjPAon4o.htm)|Master Spellcasting|Lanzador de conjuros maestro|modificada|
|[class-18-nnsoFOtuHnpz2QHc.htm](feats/class-18-nnsoFOtuHnpz2QHc.htm)|Improbable Elixirs|Elixires improbables|modificada|
|[class-18-NY2AkQscVIHEC8hQ.htm](feats/class-18-NY2AkQscVIHEC8hQ.htm)|Impossible Volley (Fighter)|Volea imposible (Luchador)|modificada|
|[class-18-o1Vo4PvU09UY8sj7.htm](feats/class-18-o1Vo4PvU09UY8sj7.htm)|Conflux Wellspring|Conflux Wellspring|modificada|
|[class-18-oNHmfpe8ezZ3eKDD.htm](feats/class-18-oNHmfpe8ezZ3eKDD.htm)|Brutal Critical|Crítico brutal|modificada|
|[class-18-OqHfUQQorVBkx34j.htm](feats/class-18-OqHfUQQorVBkx34j.htm)|Impossible Flurry|Ráfaga imposible|modificada|
|[class-18-ouKDey5RHQKN9YBT.htm](feats/class-18-ouKDey5RHQKN9YBT.htm)|Domain Wellspring|Manantial de dominio|modificada|
|[class-18-phD0PbElkEeldZ2U.htm](feats/class-18-phD0PbElkEeldZ2U.htm)|Master Sorcerer Spellcasting|Lanzamiento de conjuros maestro de hechicero|modificada|
|[class-18-PMckhnGYMyiwUNiL.htm](feats/class-18-PMckhnGYMyiwUNiL.htm)|Master Wizard Spellcasting|Lanzamiento de conjuros maestro de mago|modificada|
|[class-18-pWbNhfBiskV4n58a.htm](feats/class-18-pWbNhfBiskV4n58a.htm)|Black Powder Embodiment|Encarnación de Pólvora Negra|modificada|
|[class-18-PzbfpOQXOjpUWKkL.htm](feats/class-18-PzbfpOQXOjpUWKkL.htm)|Master Psychic Spellcasting|Lanzador de conjuros maestro psíquico|modificada|
|[class-18-QWPFf6NOObxaZJwW.htm](feats/class-18-QWPFf6NOObxaZJwW.htm)|Manifold Edge (Flurry)|Ventaja múltiple (Ráfaga)|modificada|
|[class-18-Rb16bcCiovwRqVgN.htm](feats/class-18-Rb16bcCiovwRqVgN.htm)|Ultimate Mercy|Merced definitiva|modificada|
|[class-18-Rbp08BSSzwpkWVjh.htm](feats/class-18-Rbp08BSSzwpkWVjh.htm)|Terrifying Countenance|Semblante Aterrador|modificada|
|[class-18-rmO7FP410nvCjFBB.htm](feats/class-18-rmO7FP410nvCjFBB.htm)|Stave off Catastrophe|Evita la Catástrofe|modificada|
|[class-18-S3hN7qK7aiCDTrpM.htm](feats/class-18-S3hN7qK7aiCDTrpM.htm)|Retain Absorbed Spell|Retener Hechizo Absorbido|modificada|
|[class-18-SlXLsuxBHeUyUPII.htm](feats/class-18-SlXLsuxBHeUyUPII.htm)|Mighty Wings|Alas Poderosas|modificada|
|[class-18-syxJQ48bxE8NY91a.htm](feats/class-18-syxJQ48bxE8NY91a.htm)|Master Snowcasting|Maestro Snowcasting|modificada|
|[class-18-SzWeWBuzg3e0k98A.htm](feats/class-18-SzWeWBuzg3e0k98A.htm)|Primal Wellspring|Manantial primigenio|modificada|
|[class-18-T4Xm8vYtnGMOM0Cw.htm](feats/class-18-T4Xm8vYtnGMOM0Cw.htm)|Echoing Channel|Canalización resonante|modificada|
|[class-18-v22FQuw17Dlr1b3x.htm](feats/class-18-v22FQuw17Dlr1b3x.htm)|Miraculous Flight|Vuelo Milagroso|modificada|
|[class-18-v88bFLoJEF3YfJKb.htm](feats/class-18-v88bFLoJEF3YfJKb.htm)|Savage Critical|Crítico salvaje|modificada|
|[class-18-VAbfepohLNtubfi3.htm](feats/class-18-VAbfepohLNtubfi3.htm)|Celestial Form|Forma celestial|modificada|
|[class-18-vCsprcXVTwgtUYAZ.htm](feats/class-18-vCsprcXVTwgtUYAZ.htm)|Cranial Detonation|Detonación Craneal|modificada|
|[class-18-W1CthcTSbsmOo7lP.htm](feats/class-18-W1CthcTSbsmOo7lP.htm)|Impossible Volley (Ranger)|Volea imposible (Ranger)|modificada|
|[class-18-xAIUuSw5A85XEStY.htm](feats/class-18-xAIUuSw5A85XEStY.htm)|Perfect Form Control|Control de forma perfecto.|modificada|
|[class-18-Xlz96xFxueCk47pb.htm](feats/class-18-Xlz96xFxueCk47pb.htm)|Deepest Wellspring|Manantial profundo|modificada|
|[class-18-Y5irKSCSBn8z1Qgx.htm](feats/class-18-Y5irKSCSBn8z1Qgx.htm)|Meditative Wellspring|Manantial meditativo|modificada|
|[class-18-YaJ1JTBMhpu1RWXV.htm](feats/class-18-YaJ1JTBMhpu1RWXV.htm)|Mighty Bear|Oso Poderoso|modificada|
|[class-18-yFoBVSOCnC2R2r8s.htm](feats/class-18-yFoBVSOCnC2R2r8s.htm)|Eternal Composition|Composición eterna|modificada|
|[class-18-yRRM1dsY6jakEMaC.htm](feats/class-18-yRRM1dsY6jakEMaC.htm)|Intense Implement|Intense Implement|modificada|
|[class-18-yUuvixlhM4mcjKMb.htm](feats/class-18-yUuvixlhM4mcjKMb.htm)|Bloodline Wellspring|Manantial de linaje|modificada|
|[class-18-ZB2WMbhALgQTGY3c.htm](feats/class-18-ZB2WMbhALgQTGY3c.htm)|School Spell Redirection|Redirección de hechizo escolar|modificada|
|[class-18-zrIrpVOvbGS6a3ux.htm](feats/class-18-zrIrpVOvbGS6a3ux.htm)|Perfect Shot|Tiro perfecto|modificada|
|[class-18-zTulA4sVXwLRm28Z.htm](feats/class-18-zTulA4sVXwLRm28Z.htm)|Diamond Fists|Pus de diamante|modificada|
|[class-20-0208T5UrkTY2ombM.htm](feats/class-20-0208T5UrkTY2ombM.htm)|Perfect Mutagen|Mutágeno perfecto|modificada|
|[class-20-0VUx8g8GJzuxvLSa.htm](feats/class-20-0VUx8g8GJzuxvLSa.htm)|Steal Essence|Sustraer Esencia|modificada|
|[class-20-1BAJxKpeQc8xGaxZ.htm](feats/class-20-1BAJxKpeQc8xGaxZ.htm)|All the Time in the World|Todo el tiempo del mundo|modificada|
|[class-20-1sD5Gu8jQL09Yz2j.htm](feats/class-20-1sD5Gu8jQL09Yz2j.htm)|Sever Space|Sever Space|modificada|
|[class-20-1ul2dasQBdlaMEC5.htm](feats/class-20-1ul2dasQBdlaMEC5.htm)|Heart of the Kaiju|Corazón del Kaiju|modificada|
|[class-20-20Yax5lEqjftKBHZ.htm](feats/class-20-20Yax5lEqjftKBHZ.htm)|Vigilant Mask|Máscara de Vigilante|modificada|
|[class-20-2FBZ0apnmZ7b61ct.htm](feats/class-20-2FBZ0apnmZ7b61ct.htm)|Efficient Alchemy (Paragon)|Alquimia eficaz (Paragon)|modificada|
|[class-20-2sCzFjq8sKvBR3Jh.htm](feats/class-20-2sCzFjq8sKvBR3Jh.htm)|Ultimate Skirmisher|Hostigador definitivo|modificada|
|[class-20-3w2SktSOZdG8f6Qr.htm](feats/class-20-3w2SktSOZdG8f6Qr.htm)|Fatal Aria|Aria fatal|modificada|
|[class-20-4E4121lbfWgxui4y.htm](feats/class-20-4E4121lbfWgxui4y.htm)|Song of the Fallen|Song of the Fallen|modificada|
|[class-20-5C0XMnfTuvgSKD7o.htm](feats/class-20-5C0XMnfTuvgSKD7o.htm)|Become Thought|Convertirse en pensamiento|modificada|
|[class-20-5f0EbNl7DHkiKCIr.htm](feats/class-20-5f0EbNl7DHkiKCIr.htm)|Empyreal Aura|Aura Empírea|modificada|
|[class-20-5uhKRkYDzLP7v3XY.htm](feats/class-20-5uhKRkYDzLP7v3XY.htm)|Zombie Horde|Horda de Zombis|modificada|
|[class-20-62USpCx1ewK03wzm.htm](feats/class-20-62USpCx1ewK03wzm.htm)|Ricochet Legend|Leyenda Ricochet|modificada|
|[class-20-6PCNYExygF5890Fl.htm](feats/class-20-6PCNYExygF5890Fl.htm)|Thick Hide Mask|Grueso Esconderse Máscara|modificada|
|[class-20-6SEDoht4dXEJE5SW.htm](feats/class-20-6SEDoht4dXEJE5SW.htm)|Bloodline Perfection|Perfección de linaje|modificada|
|[class-20-6yJxUx0W2hwHckNi.htm](feats/class-20-6yJxUx0W2hwHckNi.htm)|Full Automation|Automatización Completa|modificada|
|[class-20-79pGj9RC1bAt82UD.htm](feats/class-20-79pGj9RC1bAt82UD.htm)|Unlimited Demesne|Demesne ilimitado|modificada|
|[class-20-7QLLwcSKNGPWdOGG.htm](feats/class-20-7QLLwcSKNGPWdOGG.htm)|Annihilating Swing|Columpio Aniquilador|modificada|
|[class-20-85boZA8KRMu4rihN.htm](feats/class-20-85boZA8KRMu4rihN.htm)|Ubiquitous Overdrive|Overdrive Omnipresente|modificada|
|[class-20-8YSwzLNlmBLoEyUj.htm](feats/class-20-8YSwzLNlmBLoEyUj.htm)|Cross the Final Horizon|Cruzar el Horizonte Final|modificada|
|[class-20-9Slu8lSOYnDtKsIb.htm](feats/class-20-9Slu8lSOYnDtKsIb.htm)|Ruby Resurrection|Resucitar Rubí|modificada|
|[class-20-AmfO4FHmfFr0oNi9.htm](feats/class-20-AmfO4FHmfFr0oNi9.htm)|Bloodline Conduit|Conducto de linaje|modificada|
|[class-20-AXXlinOc2lq08NPH.htm](feats/class-20-AXXlinOc2lq08NPH.htm)|Celestial Mount|Montura celestial|modificada|
|[class-20-aZ5JdFKA7L8NYl4o.htm](feats/class-20-aZ5JdFKA7L8NYl4o.htm)|Tower Shield Mastery|Dominio del escudo de la torre|modificada|
|[class-20-B3Fz46wzUIzrxWsA.htm](feats/class-20-B3Fz46wzUIzrxWsA.htm)|Legendary Shot|Tiro legendario|modificada|
|[class-20-banx3uX4JjjcHEc8.htm](feats/class-20-banx3uX4JjjcHEc8.htm)|Superior Sight (Low-Light Vision)|Vista superior (visión con poca luz)|modificada|
|[class-20-BQkk7qSSRTFc5jNG.htm](feats/class-20-BQkk7qSSRTFc5jNG.htm)|Leyline Conduit|Conducto de líneas Ley|modificada|
|[class-20-bwbPuv4JsilmMnPz.htm](feats/class-20-bwbPuv4JsilmMnPz.htm)|Immortal Bear|Oso Inmortal|modificada|
|[class-20-Chu6s3xVnpOB64GH.htm](feats/class-20-Chu6s3xVnpOB64GH.htm)|Hierophant's Power|Poder del hierofante|modificada|
|[class-20-csjkzb5dsyZPeOtY.htm](feats/class-20-csjkzb5dsyZPeOtY.htm)|Wrath of the First Ghoul|Ira del Primer Ghoul|modificada|
|[class-20-CuJzCxGwd1EZDift.htm](feats/class-20-CuJzCxGwd1EZDift.htm)|Cunning Trickster Mask|Máscara de Arlequín Astucia|modificada|
|[class-20-czdqBRpzpml23la9.htm](feats/class-20-czdqBRpzpml23la9.htm)|Eternal Boost|Eternal Boost|modificada|
|[class-20-D8XoWk1brpyW6oO2.htm](feats/class-20-D8XoWk1brpyW6oO2.htm)|Plum Deluge|Diluvio de Ciruela|modificada|
|[class-20-dj0aOPPcDOqmptpX.htm](feats/class-20-dj0aOPPcDOqmptpX.htm)|Storyteller's Mask|Storyteller's Mask|modificada|
|[class-20-dJ1ZviNMpt4ID7lc.htm](feats/class-20-dJ1ZviNMpt4ID7lc.htm)|Spell Combination|Combinar conjuros|modificada|
|[class-20-DMECB9RwLAhY0T9o.htm](feats/class-20-DMECB9RwLAhY0T9o.htm)|Emblazon Divinity|Estampar Divinidad|modificada|
|[class-20-dwloLQzWgwjJWzXt.htm](feats/class-20-dwloLQzWgwjJWzXt.htm)|Banishing Blow|Golpe de destierro|modificada|
|[class-20-EHorYedQ8r05qAtk.htm](feats/class-20-EHorYedQ8r05qAtk.htm)|Triple Threat|Triple amenaza|modificada|
|[class-20-EP8kaXNmrMfxOFAf.htm](feats/class-20-EP8kaXNmrMfxOFAf.htm)|Everdistant Defense|Everdistant Defense|modificada|
|[class-20-epzeES7xJxvIXDdj.htm](feats/class-20-epzeES7xJxvIXDdj.htm)|Dance of Intercession|Danzante de Intercesión|modificada|
|[class-20-evMhKTjzdiuDKwMX.htm](feats/class-20-evMhKTjzdiuDKwMX.htm)|Aura of Unbreakable Virtue|Aura de Virtud Inquebrantable|modificada|
|[class-20-f4k5ripShCn5orZB.htm](feats/class-20-f4k5ripShCn5orZB.htm)|Grand Medic's Mask|Grand Medic's Mask|modificada|
|[class-20-flRXjabqedf6GjuU.htm](feats/class-20-flRXjabqedf6GjuU.htm)|Extradimensional Stash|Extradimensional Stash|modificada|
|[class-20-FMjihpGLn9eQ14Gw.htm](feats/class-20-FMjihpGLn9eQ14Gw.htm)|Quaking Stomp|Pisotón de terremoto|modificada|
|[class-20-GIKySPq1n7xUmICw.htm](feats/class-20-GIKySPq1n7xUmICw.htm)|Bloodline Metamorphosis|Metamorfosis de linaje|modificada|
|[class-20-H63SJLkenhLDkVnN.htm](feats/class-20-H63SJLkenhLDkVnN.htm)|The Tyrant Falls!|¡El Tirano Cae!|modificada|
|[class-20-HEZeZcBWQR1QeWDo.htm](feats/class-20-HEZeZcBWQR1QeWDo.htm)|Astonishing Explosion|Explosión Asombrosa|modificada|
|[class-20-HLCeP87w7qEy8PUH.htm](feats/class-20-HLCeP87w7qEy8PUH.htm)|To the Ends of the Earth|Hasta el fin del mundo|modificada|
|[class-20-HxO0Sh3hNFMoSsTB.htm](feats/class-20-HxO0Sh3hNFMoSsTB.htm)|Emancipator's Mask|Máscara del Emancipador|modificada|
|[class-20-hYW6MsPk1UcFROFD.htm](feats/class-20-hYW6MsPk1UcFROFD.htm)|Twin Eidolon|Eidolon Gemelo|modificada|
|[class-20-iHUuWrvkT2uR0PnK.htm](feats/class-20-iHUuWrvkT2uR0PnK.htm)|Whirlwind Spell|Hechizo Torbellino|modificada|
|[class-20-IMArawT1Sc2PTcYM.htm](feats/class-20-IMArawT1Sc2PTcYM.htm)|Boundless Reprisals|Represalia ilimitada|modificada|
|[class-20-JEFPufbvaCeiA0Zo.htm](feats/class-20-JEFPufbvaCeiA0Zo.htm)|Weapon Supremacy|Supremacía con las armas|modificada|
|[class-20-jYEMVfrXJLpXS6aC.htm](feats/class-20-jYEMVfrXJLpXS6aC.htm)|Radiant Blade Master|Maestro del filo radiante|modificada|
|[class-20-k2hxQ9SPOM7aFWQZ.htm](feats/class-20-k2hxQ9SPOM7aFWQZ.htm)|Vivacious Afterimage|Vivacious Afterimage|modificada|
|[class-20-kceciNwoldkzAMbq.htm](feats/class-20-kceciNwoldkzAMbq.htm)|Fuse Stance|Posición de Fusionar|modificada|
|[class-20-kKZ4gnT7okaWS6tB.htm](feats/class-20-kKZ4gnT7okaWS6tB.htm)|Metamagic Mastery|Maestría metamágica|modificada|
|[class-20-Kl1O0WK37KMTumv1.htm](feats/class-20-Kl1O0WK37KMTumv1.htm)|Hidden Paragon|Parangón oculto|modificada|
|[class-20-kOqW5ZtnOiWxKl9M.htm](feats/class-20-kOqW5ZtnOiWxKl9M.htm)|Icy Apotheosis|Apoteosis Helada|modificada|
|[class-20-l0Qy74a7CILdE4Th.htm](feats/class-20-l0Qy74a7CILdE4Th.htm)|Metamagic Channel|Canalizar metamagia|modificada|
|[class-20-Lbpm0OrQb4u2LVtj.htm](feats/class-20-Lbpm0OrQb4u2LVtj.htm)|Verdant Presence|Verdant Presence|modificada|
|[class-20-LCrBGoMGat2ZXuOo.htm](feats/class-20-LCrBGoMGat2ZXuOo.htm)|Ubiquitous Weakness|Debilidad omnipresente|modificada|
|[class-20-LDIZtE7saDLSBduG.htm](feats/class-20-LDIZtE7saDLSBduG.htm)|Reactive Distraction|Reacción distractora|modificada|
|[class-20-LLrGafdJij7qiWZi.htm](feats/class-20-LLrGafdJij7qiWZi.htm)|Reclaim Spell|Reclaim Spell|modificada|
|[class-20-lQpY6E5Zvc1kRnyC.htm](feats/class-20-lQpY6E5Zvc1kRnyC.htm)|Unlimited Potential|Potencial Ilimitado|modificada|
|[class-20-mGzPR7M9H733j2wN.htm](feats/class-20-mGzPR7M9H733j2wN.htm)|True Shapeshifter|Cambiaformas verdadero|modificada|
|[class-20-mjdLmXLCNaRgMLVw.htm](feats/class-20-mjdLmXLCNaRgMLVw.htm)|Mimic Protections|Mimic Protección|modificada|
|[class-20-mMMIHLVSr8fyvVQL.htm](feats/class-20-mMMIHLVSr8fyvVQL.htm)|Mega Bomb|Megabomba|modificada|
|[class-20-mSqzGGttJvj4LxK9.htm](feats/class-20-mSqzGGttJvj4LxK9.htm)|Impossible Striker|Pegador imposible|modificada|
|[class-20-NGv7sphkVgF3CtXK.htm](feats/class-20-NGv7sphkVgF3CtXK.htm)|Wonder Worker|Trabajador Maravilla|modificada|
|[class-20-nJe8eQUrIpKWLXh5.htm](feats/class-20-nJe8eQUrIpKWLXh5.htm)|Contagious Rage|Furia contagiosa|modificada|
|[class-20-nn7DiYYWinsSYrZy.htm](feats/class-20-nn7DiYYWinsSYrZy.htm)|Protective Spirit Mask|Máscara protectora de espíritus|modificada|
|[class-20-OliKxFIqzky2o6vk.htm](feats/class-20-OliKxFIqzky2o6vk.htm)|Ringmaster's Introduction|Ringmaster's Introduction|modificada|
|[class-20-opeP0JF9WGmNG0pb.htm](feats/class-20-opeP0JF9WGmNG0pb.htm)|Avatar's Audience|Audiencia con el avatar|modificada|
|[class-20-OYrcbyaV3v8ycksj.htm](feats/class-20-OYrcbyaV3v8ycksj.htm)|Head of the Night Parade|Jefe del Desfile Nocturno|modificada|
|[class-20-p353WH847errsNvh.htm](feats/class-20-p353WH847errsNvh.htm)|Apex Companion|Apex Companion|modificada|
|[class-20-P9swngiLXbhMegQ8.htm](feats/class-20-P9swngiLXbhMegQ8.htm)|Shield Paragon|Parangón del escudo|modificada|
|[class-20-PDFbfCrV2z0wfMz0.htm](feats/class-20-PDFbfCrV2z0wfMz0.htm)|Demon's Hair|Pelo de demonioíaco|modificada|
|[class-20-PTAdxHcTS7TjyBTg.htm](feats/class-20-PTAdxHcTS7TjyBTg.htm)|Twin Psyche|Twin Psyche|modificada|
|[class-20-QDjpZKOrWIV1G8XJ.htm](feats/class-20-QDjpZKOrWIV1G8XJ.htm)|Maker of Miracles|Obrador de milagros|modificada|
|[class-20-rMjlDss3Km1RQ8DE.htm](feats/class-20-rMjlDss3Km1RQ8DE.htm)|Slinger's Reflexes|Slinger's Reflexes|modificada|
|[class-20-RYUb5oxd46Yvdypz.htm](feats/class-20-RYUb5oxd46Yvdypz.htm)|Endurance of the Rooted Tree|Resistencia del árbol enraizado|modificada|
|[class-20-RzfWrOqHL2GcK0rr.htm](feats/class-20-RzfWrOqHL2GcK0rr.htm)|Enduring Debilitation|Debilitación Perdurable|modificada|
|[class-20-sfxLo9kz2WkCQiy4.htm](feats/class-20-sfxLo9kz2WkCQiy4.htm)|Symphony of the Muse|Sinfonía de la musa|modificada|
|[class-20-siegOEdEpevAJNFw.htm](feats/class-20-siegOEdEpevAJNFw.htm)|Denier of Destruction|Negador de la Destrucción|modificada|
|[class-20-srWsvDDdz77yieY1.htm](feats/class-20-srWsvDDdz77yieY1.htm)|Impossible Technique|Técnica imposible|modificada|
|[class-20-SS8JSDB2P0SFX1KH.htm](feats/class-20-SS8JSDB2P0SFX1KH.htm)|Sky Master Mask|Máscara de Maestro del Cielo|modificada|
|[class-20-tBWSxVxrojRcEzJt.htm](feats/class-20-tBWSxVxrojRcEzJt.htm)|Scapegoat Parallel Self|Yo Paralelo Chivo Expiatorio|modificada|
|[class-20-tFUo2tsdreWBxMfs.htm](feats/class-20-tFUo2tsdreWBxMfs.htm)|Stalking Feline Mask|Máscara de Felino Acechador|modificada|
|[class-20-Tj79ePSD212EZjRM.htm](feats/class-20-Tj79ePSD212EZjRM.htm)|Vitality-Manipulating Stance|Posición de manipular la vitalidad.|modificada|
|[class-20-tP26mgaFPpr6df1i.htm](feats/class-20-tP26mgaFPpr6df1i.htm)|Archwizard's Might|Archwizard's Might|modificada|
|[class-20-TsgW87kYudNr6Bfp.htm](feats/class-20-TsgW87kYudNr6Bfp.htm)|Wish Alchemy|Alquimia del deseo|modificada|
|[class-20-txLcSHu6kEfmrJj1.htm](feats/class-20-txLcSHu6kEfmrJj1.htm)|Enduring Quickness|Rapidez duradera|modificada|
|[class-20-UlCbjWWVEmfvaf5a.htm](feats/class-20-UlCbjWWVEmfvaf5a.htm)|Tenacious Blood Magic|Magia de sangre tenaz|modificada|
|[class-20-Uof5QNeGklGnks1h.htm](feats/class-20-Uof5QNeGklGnks1h.htm)|Superior Sight|Vista Superior|modificada|
|[class-20-uwp7Y4LNtPbhELjS.htm](feats/class-20-uwp7Y4LNtPbhELjS.htm)|Golden Body|Cuerpo Dorado|modificada|
|[class-20-Ux9cBtDRP92EM6rl.htm](feats/class-20-Ux9cBtDRP92EM6rl.htm)|Mind Over Matter|Mind Over Matter|modificada|
|[class-20-VSyuTWRuxdmgq2HS.htm](feats/class-20-VSyuTWRuxdmgq2HS.htm)|Invulnerable Juggernaut|Juggernaut Invulnerable|modificada|
|[class-20-WAWaew53zpTFVwQM.htm](feats/class-20-WAWaew53zpTFVwQM.htm)|Synergistic Spell|Hechizo Sinérgico|modificada|
|[class-20-wPjNjyh60fYKrDXl.htm](feats/class-20-wPjNjyh60fYKrDXl.htm)|Legendary Summoner|Invocador Legendario|modificada|
|[class-20-Wukrctjz2e8W4bbS.htm](feats/class-20-Wukrctjz2e8W4bbS.htm)|Perfect Readiness|Perfect Readiness|modificada|
|[class-20-WVm2dxPOI4tK2wsJ.htm](feats/class-20-WVm2dxPOI4tK2wsJ.htm)|Impossible Volley (Eldritch Archer)|Volea imposible (Arquero eldritch)|modificada|
|[class-20-XenKYUBMWZQQ7niM.htm](feats/class-20-XenKYUBMWZQQ7niM.htm)|Whirlwind Toss|Lanzar Torbellino|modificada|
|[class-20-XHOhAHv2HLdJQz2q.htm](feats/class-20-XHOhAHv2HLdJQz2q.htm)|Tireless Guide's Mask|Máscara de guía incansable|modificada|
|[class-20-xhuXpOFOxDpJgngm.htm](feats/class-20-xhuXpOFOxDpJgngm.htm)|Perfect Encore|Bis perfecto|modificada|
|[class-20-xM0vwRFLZgmmI4YJ.htm](feats/class-20-xM0vwRFLZgmmI4YJ.htm)|Time Dilation Cascade|Cascada de Dilatación Temporal|modificada|
|[class-20-xqjPVZezw1a73JAO.htm](feats/class-20-xqjPVZezw1a73JAO.htm)|Worldsphere Gravity|Worldsphere Gravity|modificada|
|[class-20-YbneCzvDEr76mrsS.htm](feats/class-20-YbneCzvDEr76mrsS.htm)|Superior Sight (Darkvision)|Vista superior (visión en la oscuridad).|modificada|
|[class-20-Yec6UwJf5FLvAbZ4.htm](feats/class-20-Yec6UwJf5FLvAbZ4.htm)|Reflecting Riposte|Reflecting Riposte|modificada|
|[class-20-zOK6IFSz3DIBRjEw.htm](feats/class-20-zOK6IFSz3DIBRjEw.htm)|Craft Philosopher's Stone|Elaborar piedra filosofal.|modificada|
|[class-20-zy7lx4SWkfDxqH6m.htm](feats/class-20-zy7lx4SWkfDxqH6m.htm)|Supreme Spellstrike|Hechizo Supremo|modificada|
|[general-01-z1Z22gTp7J1VRLSR.htm](feats/general-01-z1Z22gTp7J1VRLSR.htm)|Different Worlds|Mundos Diferentes|modificada|
|[general-03-38uOVS8fLZxraUrg.htm](feats/general-03-38uOVS8fLZxraUrg.htm)|Pick Up The Pace|Acelerar el paso|modificada|
|[general-03-bh2jHyyYrkLMsIdX.htm](feats/general-03-bh2jHyyYrkLMsIdX.htm)|Prescient Planner|Planificador presciente|modificada|
|[general-03-fD9xjrnPfJ8aQxYA.htm](feats/general-03-fD9xjrnPfJ8aQxYA.htm)|Keen Follower|Seguidor entusiasta|modificada|
|[general-03-jFmdevE4nKevovzo.htm](feats/general-03-jFmdevE4nKevovzo.htm)|Steel Your Resolve|Steel Your Resolución|modificada|
|[general-03-wPHZhgKzNw4VcCFt.htm](feats/general-03-wPHZhgKzNw4VcCFt.htm)|Skitter|Zafarse|modificada|
|[general-03-xT593tHyPkumPuzz.htm](feats/general-03-xT593tHyPkumPuzz.htm)|Improvised Repair|Reparación improvisada|modificada|
|[general-07-fOIUmDGa9gkeCHA0.htm](feats/general-07-fOIUmDGa9gkeCHA0.htm)|Supertaster|Supercatador|modificada|
|[general-11-5ZsmRm7HvFAw2XDZ.htm](feats/general-11-5ZsmRm7HvFAw2XDZ.htm)|Caravan Leader|Líder de caravana|modificada|
|[general-11-muMOxZyduEFv8UT6.htm](feats/general-11-muMOxZyduEFv8UT6.htm)|A Home In Every Port|Un hogar en cada puerto|modificada|
|[general-19-uudiUylT09WnHN7e.htm](feats/general-19-uudiUylT09WnHN7e.htm)|True Perception|Percepción verdadera|modificada|
|[skill-01-0N8TtGSk5enoLBZ8.htm](feats/skill-01-0N8TtGSk5enoLBZ8.htm)|Eye For Numbers|Ojo para los números|modificada|
|[skill-01-22P7IFyhrF7Fbw8B.htm](feats/skill-01-22P7IFyhrF7Fbw8B.htm)|Root Magic|Magia de las raíces|modificada|
|[skill-01-5s8FqK4YZTVOvP0v.htm](feats/skill-01-5s8FqK4YZTVOvP0v.htm)|Reveal True Name|Revelar verdadero nombre|modificada|
|[skill-01-6GO3dtFJnsNnSwWz.htm](feats/skill-01-6GO3dtFJnsNnSwWz.htm)|Medical Researcher|Investigador Médico|modificada|
|[skill-01-6O8MoMheHs5hNHX1.htm](feats/skill-01-6O8MoMheHs5hNHX1.htm)|Crystal Healing|Curación con Cristales|modificada|
|[skill-01-6ON8DjFXSMITZleX.htm](feats/skill-01-6ON8DjFXSMITZleX.htm)|No Cause For Alarm|No se alarmen|modificada|
|[skill-01-B9cQLRHtXoLlF0iR.htm](feats/skill-01-B9cQLRHtXoLlF0iR.htm)|Concealing Legerdemain|Prestidigitación de ocultación|modificada|
|[skill-01-CnqMJR8e9jqJR7MM.htm](feats/skill-01-CnqMJR8e9jqJR7MM.htm)|Steady Balance|Equilibrio firme|modificada|
|[skill-01-d8AjCqU30z7IOpos.htm](feats/skill-01-d8AjCqU30z7IOpos.htm)|Ravening's Desperation|Desesperación de Ravening|modificada|
|[skill-01-dZDmWXzZfIoBJ53Q.htm](feats/skill-01-dZDmWXzZfIoBJ53Q.htm)|Tame Animal|Animal domesticado|modificada|
|[skill-01-f0faBEUPtspdutKx.htm](feats/skill-01-f0faBEUPtspdutKx.htm)|Acrobatic Performer|Intérprete acrobático|modificada|
|[skill-01-gArdEleFCvUHtdGk.htm](feats/skill-01-gArdEleFCvUHtdGk.htm)|Express Rider|Jinete veloz|modificada|
|[skill-01-gnH9SpdNQegDqIar.htm](feats/skill-01-gnH9SpdNQegDqIar.htm)|All of the Animal|Todo el Animal|modificada|
|[skill-01-lEgYzFHransLkSvI.htm](feats/skill-01-lEgYzFHransLkSvI.htm)|Schooled In Secrets|Instruido en secretos|modificada|
|[skill-01-p5Bmj3d0uAGnrzIn.htm](feats/skill-01-p5Bmj3d0uAGnrzIn.htm)|Read Psychometric Resonance|Leer Resonancia Psicométrica|modificada|
|[skill-01-QShgLWlfKYJO750P.htm](feats/skill-01-QShgLWlfKYJO750P.htm)|Stitch Flesh|Puntada Carne|modificada|
|[skill-01-voYr7ygVcWmlg1f4.htm](feats/skill-01-voYr7ygVcWmlg1f4.htm)|Crafter's Appraisal|Valoración de artesano|modificada|
|[skill-01-wbjTkaKRygpaZS0r.htm](feats/skill-01-wbjTkaKRygpaZS0r.htm)|Secret Speech|Discurso secreto|modificada|
|[skill-01-WeQGWvlWdeLeOlCN.htm](feats/skill-01-WeQGWvlWdeLeOlCN.htm)|Glean Contents|Vislumbrar el contenido|modificada|
|[skill-01-Ws9JlysHcFoz6WAQ.htm](feats/skill-01-Ws9JlysHcFoz6WAQ.htm)|Forensic Acumen|Perspicacia forense|modificada|
|[skill-02-4UXyMtXLaOxuH6Js.htm](feats/skill-02-4UXyMtXLaOxuH6Js.htm)|Distracting Performance|Interpretación de distracción|modificada|
|[skill-02-5f8gQxVdioUcgsTD.htm](feats/skill-02-5f8gQxVdioUcgsTD.htm)|Familiar Oddities|Rarezas Familiares|modificada|
|[skill-02-7F3sTNRoNsQgD8tX.htm](feats/skill-02-7F3sTNRoNsQgD8tX.htm)|Backup Disguise|Disfraz de respaldo|modificada|
|[skill-02-7t2VGjVhYNU3MsEm.htm](feats/skill-02-7t2VGjVhYNU3MsEm.htm)|Spirit Speaker|Espíritu Orador|modificada|
|[skill-02-aMI39DZhWgNgJTAn.htm](feats/skill-02-aMI39DZhWgNgJTAn.htm)|Malleable Movement|Movimiento maleable|modificada|
|[skill-02-AYb8PmGJ37HwIMwj.htm](feats/skill-02-AYb8PmGJ37HwIMwj.htm)|Juggle|Malabarista|modificada|
|[skill-02-BV5jpSifVJsTwoO7.htm](feats/skill-02-BV5jpSifVJsTwoO7.htm)|Wilderness Spotter|Avistador agreste|modificada|
|[skill-02-cmuvvPJvt2R16vGe.htm](feats/skill-02-cmuvvPJvt2R16vGe.htm)|Fresh Ingredients|Ingredientes frescos|modificada|
|[skill-02-dUnT3HWMFD3d2eBJ.htm](feats/skill-02-dUnT3HWMFD3d2eBJ.htm)|Encouraging Words|Palabras alentadoras|modificada|
|[skill-02-EZrxp0XxYh9rjghB.htm](feats/skill-02-EZrxp0XxYh9rjghB.htm)|Embed Aeon Stone|Incrustar Piedra Aeon|modificada|
|[skill-02-gydOsP9VsdRw3Wg1.htm](feats/skill-02-gydOsP9VsdRw3Wg1.htm)|Underground Network|Red clandestina|modificada|
|[skill-02-iDimfGmQHacwxeh2.htm](feats/skill-02-iDimfGmQHacwxeh2.htm)|Terrifying Resistance|Resistencia aterradora|modificada|
|[skill-02-K2R1xGTgBnSCDMUl.htm](feats/skill-02-K2R1xGTgBnSCDMUl.htm)|Express Driver|Conductor exprés|modificada|
|[skill-02-lB9MVGCJr7aJQuIH.htm](feats/skill-02-lB9MVGCJr7aJQuIH.htm)|Armored Stealth|Sigilo con armadura|modificada|
|[skill-02-N7IsnLDFt73r7x56.htm](feats/skill-02-N7IsnLDFt73r7x56.htm)|Shadow Mark|Marca de la sombra|modificada|
|[skill-02-nJ3EBRat9yUgeWwv.htm](feats/skill-02-nJ3EBRat9yUgeWwv.htm)|Automatic Writing|Escritura Automática|modificada|
|[skill-02-NUHosufuAhQCnF7N.htm](feats/skill-02-NUHosufuAhQCnF7N.htm)|Triumphant Boast|Alarde triunfal|modificada|
|[skill-02-OtV7esAwza1U6Kwr.htm](feats/skill-02-OtV7esAwza1U6Kwr.htm)|Eye of the Arclords|Ojo de los Arclords|modificada|
|[skill-02-Peyf3L7c9esRTsgR.htm](feats/skill-02-Peyf3L7c9esRTsgR.htm)|Predict Weather|Predecir el tiempo|modificada|
|[skill-02-qvLcZGsV0HP2O0CG.htm](feats/skill-02-qvLcZGsV0HP2O0CG.htm)|Battle Planner|Planificador de batallas|modificada|
|[skill-02-RlFZ648UR0Q0YECL.htm](feats/skill-02-RlFZ648UR0Q0YECL.htm)|Chromotherapy|Cromoterapia|modificada|
|[skill-02-sLEawQueTV1wGn0B.htm](feats/skill-02-sLEawQueTV1wGn0B.htm)|Sow Rumor|Siembra Rumores|modificada|
|[skill-02-uF0ATN2Zw1Q67ew2.htm](feats/skill-02-uF0ATN2Zw1Q67ew2.htm)|Discreet Inquiry|Indagación discreta|modificada|
|[skill-02-vjQ3VUpYlTAAIx3b.htm](feats/skill-02-vjQ3VUpYlTAAIx3b.htm)|Lead Climber|Trepador líder|modificada|
|[skill-02-vWtPxwND60EpxBAU.htm](feats/skill-02-vWtPxwND60EpxBAU.htm)|Tweak Appearances|Ajustar Apariencias|modificada|
|[skill-02-yTLGclKtWVFZLKIz.htm](feats/skill-02-yTLGclKtWVFZLKIz.htm)|Godless Healing|Curación sin Dios|modificada|
|[skill-02-z0HX5L7bOOrKi0dD.htm](feats/skill-02-z0HX5L7bOOrKi0dD.htm)|Tattoo Artist|Artista del tatuaje|modificada|
|[skill-02-zdjPTg6vRwg8r2Lm.htm](feats/skill-02-zdjPTg6vRwg8r2Lm.htm)|Aura Sight|Aura Sight|modificada|
|[skill-03-9lyFxaoZjF1ZjVN9.htm](feats/skill-03-9lyFxaoZjF1ZjVN9.htm)|Know the Beat|Conoce el ritmo|modificada|
|[skill-03-FbGPETHJR9VKxf9i.htm](feats/skill-03-FbGPETHJR9VKxf9i.htm)|Folk Dowsing|Radiestesia popular|modificada|
|[skill-04-0Dy8RlFqrzCVOTl4.htm](feats/skill-04-0Dy8RlFqrzCVOTl4.htm)|Thorough Reports|Informes minuciosos|modificada|
|[skill-04-0UzxiSrTfVs0jvBa.htm](feats/skill-04-0UzxiSrTfVs0jvBa.htm)|Familiar Foe|Familiar Foe|modificada|
|[skill-04-5qXw5Gl9TxbPMZLB.htm](feats/skill-04-5qXw5Gl9TxbPMZLB.htm)|Orthographic Mastery|Maestría Ortográfica|modificada|
|[skill-04-68Kc4UyhnP4l8mxq.htm](feats/skill-04-68Kc4UyhnP4l8mxq.htm)|Dead Reckoning|La reconciliación con la muerte|modificada|
|[skill-04-7WBIXGqZbAKbqEU6.htm](feats/skill-04-7WBIXGqZbAKbqEU6.htm)|Named Artillery|Artillería con nombre|modificada|
|[skill-04-9SdFlVQW4vM8ggh8.htm](feats/skill-04-9SdFlVQW4vM8ggh8.htm)|Fane's Escape|Huir de Fane|modificada|
|[skill-04-B4nuabUvA1rk7Hej.htm](feats/skill-04-B4nuabUvA1rk7Hej.htm)|Multilingual Cipher|Cifrado Multilingüe|modificada|
|[skill-04-cQptGH6RUYZmS41Q.htm](feats/skill-04-cQptGH6RUYZmS41Q.htm)|Hideous Ululation|Hideous Ululation|modificada|
|[skill-04-f0s3WwaJN5f2UTYY.htm](feats/skill-04-f0s3WwaJN5f2UTYY.htm)|Reverse Engineering|Ingeniería inversa|modificada|
|[skill-04-HRVCODLOrhjRDtGb.htm](feats/skill-04-HRVCODLOrhjRDtGb.htm)|Sure Foot|Pie Seguro|modificada|
|[skill-04-Jivt1iaqKfT6EcwV.htm](feats/skill-04-Jivt1iaqKfT6EcwV.htm)|That's Not Natural!|¡Eso no es natural!|modificada|
|[skill-04-jzflcD1XnBp2bSZI.htm](feats/skill-04-jzflcD1XnBp2bSZI.htm)|Distracting Flattery|Adulación de distracción|modificada|
|[skill-04-K3Au5071pfvNwGob.htm](feats/skill-04-K3Au5071pfvNwGob.htm)|Hidden Magic|Magia Escondida|modificada|
|[skill-04-kIXFNPFBStOKunq4.htm](feats/skill-04-kIXFNPFBStOKunq4.htm)|Kreighton's Cognitive Crossover|Cruce cognitivo de Kreighton|modificada|
|[skill-04-knrawm4bMbDL9XS3.htm](feats/skill-04-knrawm4bMbDL9XS3.htm)|Ambush Tactics|Tácticas de Emboscar|modificada|
|[skill-04-MWjbG8L5JWg888kJ.htm](feats/skill-04-MWjbG8L5JWg888kJ.htm)|Efficient Explorer|Explorador Eficaz|modificada|
|[skill-04-mwZzcwYVcTvxbXDl.htm](feats/skill-04-mwZzcwYVcTvxbXDl.htm)|Fancy Moves|Movimientos elegantes|modificada|
|[skill-04-NADqgn78Rvl7TUG8.htm](feats/skill-04-NADqgn78Rvl7TUG8.htm)|Sociable Vow|Voto Sociable|modificada|
|[skill-04-p17VlHs0I4Yc4m34.htm](feats/skill-04-p17VlHs0I4Yc4m34.htm)|Engine Bay|Bahía del motor|modificada|
|[skill-04-rAbfuZ1mc3lUYH41.htm](feats/skill-04-rAbfuZ1mc3lUYH41.htm)|Minion Guise|Esbirro Guise|modificada|
|[skill-04-rfnEcjxIFqwlJwJT.htm](feats/skill-04-rfnEcjxIFqwlJwJT.htm)|Treat Condition|Tratar estado negativo (normalmente se abrevia a estado)|modificada|
|[skill-04-SmePERlF5BZl6cTo.htm](feats/skill-04-SmePERlF5BZl6cTo.htm)|Blessed Medicine|Medicina Bendecida|modificada|
|[skill-04-smYXWVI9WXmqpiCs.htm](feats/skill-04-smYXWVI9WXmqpiCs.htm)|Eclipsed Vitality|Vitalidad Eclipsada|modificada|
|[skill-04-SXXyAwe9FGBwhJIW.htm](feats/skill-04-SXXyAwe9FGBwhJIW.htm)|Power Slide|Power Slide|modificada|
|[skill-04-SZ8J3NbtiizFaNzz.htm](feats/skill-04-SZ8J3NbtiizFaNzz.htm)|Change of Face|Cambio de Rostro|modificada|
|[skill-04-uxwHHjWs3ehqtG4b.htm](feats/skill-04-uxwHHjWs3ehqtG4b.htm)|Steel Skin|Piel de Acero|modificada|
|[skill-04-wpqKltAoJjRQgWow.htm](feats/skill-04-wpqKltAoJjRQgWow.htm)|Rope Runner|Corredor de cuerda|modificada|
|[skill-04-Wsrw68pklQyaScMX.htm](feats/skill-04-Wsrw68pklQyaScMX.htm)|Phonetic Training|Entrenamiento Fonético|modificada|
|[skill-04-WxL8NMW9JQ5igu0C.htm](feats/skill-04-WxL8NMW9JQ5igu0C.htm)|Diabolic Certitude|Certeza diabólica|modificada|
|[skill-04-ZC9C6rxPJKrw6Ktx.htm](feats/skill-04-ZC9C6rxPJKrw6Ktx.htm)|In Plain Sight|In Plain Sight|modificada|
|[skill-04-ZiSmhTsnQMLqsmyw.htm](feats/skill-04-ZiSmhTsnQMLqsmyw.htm)|Final Rest|Descanso Final|modificada|
|[skill-06-1WfvvjjObPKeZyid.htm](feats/skill-06-1WfvvjjObPKeZyid.htm)|Holistic Care|Cuidado Holístico|modificada|
|[skill-06-AnTBWhLiIA1c7jkg.htm](feats/skill-06-AnTBWhLiIA1c7jkg.htm)|Megafauna Veterinarian|Veterinario Megafauna|modificada|
|[skill-06-dAckQkpg1qyTz8od.htm](feats/skill-06-dAckQkpg1qyTz8od.htm)|Resourceful Ritualist|Ritualista ingenioso|modificada|
|[skill-06-EZ24QwnFteLCrgLg.htm](feats/skill-06-EZ24QwnFteLCrgLg.htm)|Emerald Boughs Accustomation|Costumbre de Rama esmeralda|modificada|
|[skill-06-hAsaNx1dd3xvvAsE.htm](feats/skill-06-hAsaNx1dd3xvvAsE.htm)|Say That Again!|¡Dilo otra vez!|modificada|
|[skill-06-my4uFR8cnDC4mJE2.htm](feats/skill-06-my4uFR8cnDC4mJE2.htm)|Uzunjati Storytelling|Uzunjati Storytelling|modificada|
|[skill-06-TZASOwBqVveGjw77.htm](feats/skill-06-TZASOwBqVveGjw77.htm)|Analyze Idiolect|Analizar Idiolecto|modificada|
|[skill-06-uwJQUFLymAWtJu1a.htm](feats/skill-06-uwJQUFLymAWtJu1a.htm)|Forced Entry|Entrada Forzada|modificada|
|[skill-06-xLD9EmkEmT8hgwv7.htm](feats/skill-06-xLD9EmkEmT8hgwv7.htm)|Craft Facsimile|Elaborar Artesanía|modificada|
|[skill-07-17FAYfreumeKbSGr.htm](feats/skill-07-17FAYfreumeKbSGr.htm)|Advanced First Aid|Primeros auxilios avanzados|modificada|
|[skill-07-6vwLzzrFfeiR9pm0.htm](feats/skill-07-6vwLzzrFfeiR9pm0.htm)|Entourage|Entourage|modificada|
|[skill-07-bcEI0iIFhZXqjoaR.htm](feats/skill-07-bcEI0iIFhZXqjoaR.htm)|Quick Setup|Configuración rápida|modificada|
|[skill-07-C5CweUNPP7HlRfBM.htm](feats/skill-07-C5CweUNPP7HlRfBM.htm)|Muscle Mimicry|Mimetismo muscular|modificada|
|[skill-07-Crd3qMecF9FYHjuH.htm](feats/skill-07-Crd3qMecF9FYHjuH.htm)|Read Shibboleths|Leer Shibboleths|modificada|
|[skill-07-e6s2nIvlTycuzlR9.htm](feats/skill-07-e6s2nIvlTycuzlR9.htm)|Fabricated Connections|Conexión Fabricada|modificada|
|[skill-07-g3zmkEVJJIjE32fY.htm](feats/skill-07-g3zmkEVJJIjE32fY.htm)|Scholastic Identification|Identificación Escolástica|modificada|
|[skill-07-gEj8aJmCThMzAjKY.htm](feats/skill-07-gEj8aJmCThMzAjKY.htm)|Ashen Veil|Velo de Ceniza|modificada|
|[skill-07-JEzjyNbpsh05iymG.htm](feats/skill-07-JEzjyNbpsh05iymG.htm)|Prepare Papers|Preparar papeles|modificada|
|[skill-07-Jk9XEMfMXoPT0ua2.htm](feats/skill-07-Jk9XEMfMXoPT0ua2.htm)|Skeptic's Defense|Defensa del Escéptico|modificada|
|[skill-07-KP5VugClDb7I8enS.htm](feats/skill-07-KP5VugClDb7I8enS.htm)|That Was a Close One, Huh?|Estuvo cerca, ¿eh?|modificada|
|[skill-07-oXoQ9wwOmDe0hwbU.htm](feats/skill-07-oXoQ9wwOmDe0hwbU.htm)|Subjective Truth|Verdad subjetiva|modificada|
|[skill-07-pKnX7MGZayqZui0Z.htm](feats/skill-07-pKnX7MGZayqZui0Z.htm)|Aerobatics Mastery|Maestría en acrobacias aéreas|modificada|
|[skill-07-TkCy7jZUjhD8IypE.htm](feats/skill-07-TkCy7jZUjhD8IypE.htm)|Expert Disassembler|Desensamblador experto|modificada|
|[skill-07-TXXrkkFfNSWgHrn5.htm](feats/skill-07-TXXrkkFfNSWgHrn5.htm)|Chronocognizance|Chronocognizance|modificada|
|[skill-07-UaCHd5SpYsZwf2hM.htm](feats/skill-07-UaCHd5SpYsZwf2hM.htm)|Push It|Push It|modificada|
|[skill-07-ucmJ2Z5VXUtCiE3q.htm](feats/skill-07-ucmJ2Z5VXUtCiE3q.htm)|Efficient Controls|Controles eficientes|modificada|
|[skill-07-UkMG3wMvrw8X0I98.htm](feats/skill-07-UkMG3wMvrw8X0I98.htm)|Propeller Attachment|Aferrarse a la hélice|modificada|
|[skill-07-wlbfINUTHDPqbV7v.htm](feats/skill-07-wlbfINUTHDPqbV7v.htm)|Morphic Manipulation|Manipulación mórfica|modificada|
|[skill-07-wqhxZwB1TR8fvpHP.htm](feats/skill-07-wqhxZwB1TR8fvpHP.htm)|Party Crasher|Party Crasher|modificada|
|[skill-07-WqTqHPDbargixuej.htm](feats/skill-07-WqTqHPDbargixuej.htm)|Biographical Eye|Ojo biográfico|modificada|
|[skill-07-WQtt44keeBP8t25P.htm](feats/skill-07-WQtt44keeBP8t25P.htm)|Voice Cold as Death|Voz fría como la muerte|modificada|
|[skill-07-xOMwuKCf02aFzyp3.htm](feats/skill-07-xOMwuKCf02aFzyp3.htm)|Paragon Battle Medicine|Medicina de guerra Paragon|modificada|
|[skill-07-YBge8sTgeY5jncX2.htm](feats/skill-07-YBge8sTgeY5jncX2.htm)|Speech of the Mammoth Lords|Discurso de los Señores Mamut|modificada|
|[skill-08-2Tla5D1vpGioh42x.htm](feats/skill-08-2Tla5D1vpGioh42x.htm)|Unravel Mysteries|Desentrañar Misterios|modificada|
|[skill-08-5vRXZcGAbqKRoaqL.htm](feats/skill-08-5vRXZcGAbqKRoaqL.htm)|Recognize Threat|Reconocer Amenaza|modificada|
|[skill-08-n3vpCWPjXAInRTyR.htm](feats/skill-08-n3vpCWPjXAInRTyR.htm)|Snap Out Of It! (Pathfinder Agent)|¡Despierta! (Agente Pathfinder)|modificada|
|[skill-08-RcQv16RK80R6c4id.htm](feats/skill-08-RcQv16RK80R6c4id.htm)|Improvised Crafting|Artesanía improvisada|modificada|
|[skill-08-rhVL28qFl760qJQe.htm](feats/skill-08-rhVL28qFl760qJQe.htm)|Insistent Command|Orden imperiosa|modificada|
|[skill-08-yoeMOIgH8Snw1JCQ.htm](feats/skill-08-yoeMOIgH8Snw1JCQ.htm)|Diverse Recognition|Reconocimiento Diverso|modificada|
|[skill-10-TNV1cs1VFqdj4D2M.htm](feats/skill-10-TNV1cs1VFqdj4D2M.htm)|Masterful Obfuscation|Maestría en Ofuscación|modificada|
|[skill-10-UCen3Tq6BJlNI7rx.htm](feats/skill-10-UCen3Tq6BJlNI7rx.htm)|Uzunjati Recollection|Recolección Uzunjati|modificada|
|[skill-12-9AZjpeeS824VsYv8.htm](feats/skill-12-9AZjpeeS824VsYv8.htm)|Emergency Medical Assistance|Asistencia Médica de Emergencia|modificada|
|[skill-12-m7AOg13xEJRHyoTt.htm](feats/skill-12-m7AOg13xEJRHyoTt.htm)|Rugged Survivalist|Superviviente resistente|modificada|
|[skill-12-oFcn7SDOH6W2QhJl.htm](feats/skill-12-oFcn7SDOH6W2QhJl.htm)|Too Angry to Die|Demasiado enfadado para morir|modificada|
|[skill-12-QoPooHpBjPh1sjRD.htm](feats/skill-12-QoPooHpBjPh1sjRD.htm)|Recollect Studies|Estudios Recoletos|modificada|
|[skill-15-VRCBTEyrcBf7auGz.htm](feats/skill-15-VRCBTEyrcBf7auGz.htm)|Legendary Tattoo Artist|Tatuador legendario|modificada|
|[skill-15-XZcd1wFHy111klu2.htm](feats/skill-15-XZcd1wFHy111klu2.htm)|Reveal Machinations|Revelar Maquinaciones|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[ancestry-01-0YXRPqCaOQ3G73hh.htm](feats/ancestry-01-0YXRPqCaOQ3G73hh.htm)|Folksy Patter|Cháchara rústica|oficial|
|[ancestry-01-254C4rqrE8APDfCf.htm](feats/ancestry-01-254C4rqrE8APDfCf.htm)|Orc Lore|Saber de orco|oficial|
|[ancestry-01-2bNd89jYmEO8wSay.htm](feats/ancestry-01-2bNd89jYmEO8wSay.htm)|Extra Squishy|Extra Squishy|oficial|
|[ancestry-01-4mLlHpD41101H0Iy.htm](feats/ancestry-01-4mLlHpD41101H0Iy.htm)|Changeling Lore|Saber de los Changelings|oficial|
|[ancestry-01-5EpkOj9CFOjt8vsK.htm](feats/ancestry-01-5EpkOj9CFOjt8vsK.htm)|Eyes of the Night|Ojos de la Noche|oficial|
|[ancestry-01-5g7OFtvYQ7wPPJHC.htm](feats/ancestry-01-5g7OFtvYQ7wPPJHC.htm)|Fiendish Lore|Saber del infernal|oficial|
|[ancestry-01-5VkVWZq7ZqH6RaAW.htm](feats/ancestry-01-5VkVWZq7ZqH6RaAW.htm)|Voice Of The Night|Voz de la noche|oficial|
|[ancestry-01-7qBuzHY8kEG8SdEP.htm](feats/ancestry-01-7qBuzHY8kEG8SdEP.htm)|Cat's Luck|Suerte felina|oficial|
|[ancestry-01-9Orkgjgfx8AILuqD.htm](feats/ancestry-01-9Orkgjgfx8AILuqD.htm)|Warren Navigator|Orientación en madrigueras|oficial|
|[ancestry-01-9t4x8vRmQikEl4vP.htm](feats/ancestry-01-9t4x8vRmQikEl4vP.htm)|Well-Met Traveler|Viajero bien hallado|oficial|
|[ancestry-01-A9HQ2bMAge2aGgWx.htm](feats/ancestry-01-A9HQ2bMAge2aGgWx.htm)|Prairie Rider|Jinete de la Pradera|oficial|
|[ancestry-01-akCO62yBCJLjjCZJ.htm](feats/ancestry-01-akCO62yBCJLjjCZJ.htm)|Celestial Eyes|Ojos celestiales|oficial|
|[ancestry-01-AmFv3ClkAVRowHLI.htm](feats/ancestry-01-AmFv3ClkAVRowHLI.htm)|Tengu Weapon Familiarity|Familiaridad con las armas de los Tengu|oficial|
|[ancestry-01-bEh5qUgX5eFaQwzU.htm](feats/ancestry-01-bEh5qUgX5eFaQwzU.htm)|Catfolk Weapon Familiarity|Familiaridad con las armas de félido|oficial|
|[ancestry-01-Bni2NcuQn6Z546RE.htm](feats/ancestry-01-Bni2NcuQn6Z546RE.htm)|Twitchy|Nervioso|oficial|
|[ancestry-01-bWyY9NtLU5wXr03y.htm](feats/ancestry-01-bWyY9NtLU5wXr03y.htm)|Storm's Lash|Latigo de la tormenta|oficial|
|[ancestry-01-CCmiEmS7ZgyQUfhn.htm](feats/ancestry-01-CCmiEmS7ZgyQUfhn.htm)|Squawk!|¡Graznar!|oficial|
|[ancestry-01-DGtUIMliflzGXc6E.htm](feats/ancestry-01-DGtUIMliflzGXc6E.htm)|Ghost Hunter|Cazador de fantasmas|oficial|
|[ancestry-01-eCWQU16hRLfN1KaX.htm](feats/ancestry-01-eCWQU16hRLfN1KaX.htm)|Ancestral Linguistics|Lingüística ancestral|oficial|
|[ancestry-01-eMmdBpbMrpIuGowo.htm](feats/ancestry-01-eMmdBpbMrpIuGowo.htm)|Snare Setter|Colocador de trampas de lazo|oficial|
|[ancestry-01-eqBWab1J5Be24YAl.htm](feats/ancestry-01-eqBWab1J5Be24YAl.htm)|Catfolk Lore|Saber de Félido|oficial|
|[ancestry-01-eUyZBi8vV5QDxOXD.htm](feats/ancestry-01-eUyZBi8vV5QDxOXD.htm)|Form Of The Fiend|Forma del infernal|oficial|
|[ancestry-01-EXVHePH8alsTZ5TB.htm](feats/ancestry-01-EXVHePH8alsTZ5TB.htm)|Hellspawn|Engendro infernal|oficial|
|[ancestry-01-Ffi8L4EDO5OH5tpA.htm](feats/ancestry-01-Ffi8L4EDO5OH5tpA.htm)|Nimble Hooves|Pezuñas Ágiles|oficial|
|[ancestry-01-g3oJlWGHc74qX2z5.htm](feats/ancestry-01-g3oJlWGHc74qX2z5.htm)|Lawbringer|Legislador|oficial|
|[ancestry-01-GjQZMmw2sz8OyLxj.htm](feats/ancestry-01-GjQZMmw2sz8OyLxj.htm)|Tengu Lore|Saber de los Tengu|oficial|
|[ancestry-01-gxQheZ4xuDWwyzy4.htm](feats/ancestry-01-gxQheZ4xuDWwyzy4.htm)|Pitborn|Nacido del foso|oficial|
|[ancestry-01-hc4lWhWekIVb0wjL.htm](feats/ancestry-01-hc4lWhWekIVb0wjL.htm)|Pack Rat|Coleccionista|oficial|
|[ancestry-01-hxCqQPjlyVI57vQt.htm](feats/ancestry-01-hxCqQPjlyVI57vQt.htm)|Gravesight|Visión de la tumba|oficial|
|[ancestry-01-i0iyW1I7TylEgpV6.htm](feats/ancestry-01-i0iyW1I7TylEgpV6.htm)|Vampire Lore|Saber de vampiro|oficial|
|[ancestry-01-IElFaS5i10MFYIvq.htm](feats/ancestry-01-IElFaS5i10MFYIvq.htm)|Kobold Lore|Saber de Kóbold|oficial|
|[ancestry-01-IFvjnLMw3ht8f84U.htm](feats/ancestry-01-IFvjnLMw3ht8f84U.htm)|Callow May|Maia bisoña|oficial|
|[ancestry-01-jEJ6AWCctirMT7p0.htm](feats/ancestry-01-jEJ6AWCctirMT7p0.htm)|Brine May|Maya salina|oficial|
|[ancestry-01-JP5pptkl1Fx1JK4m.htm](feats/ancestry-01-JP5pptkl1Fx1JK4m.htm)|Iron Fists|Puños de Hierro|oficial|
|[ancestry-01-kqnFdIhToKTnOpMl.htm](feats/ancestry-01-kqnFdIhToKTnOpMl.htm)|Dream May|Maia onírica|oficial|
|[ancestry-01-LUXBuTAuK1glHOkJ.htm](feats/ancestry-01-LUXBuTAuK1glHOkJ.htm)|Fiendish Eyes|Ojos infernales|oficial|
|[ancestry-01-M5195FvfCN3X7gi9.htm](feats/ancestry-01-M5195FvfCN3X7gi9.htm)|Tusks (Orc)|Colmillos (Orco)|oficial|
|[ancestry-01-mQYO501xyMgtIQ3W.htm](feats/ancestry-01-mQYO501xyMgtIQ3W.htm)|Ratfolk Lore|Saber de rátido|oficial|
|[ancestry-01-MS53Ds75BT379ZFm.htm](feats/ancestry-01-MS53Ds75BT379ZFm.htm)|Empathetic Plea|Súplica empática|oficial|
|[ancestry-01-nB8BD9rIg9hfFGns.htm](feats/ancestry-01-nB8BD9rIg9hfFGns.htm)|Eye For Treasure|Ojo para los tesoros|oficial|
|[ancestry-01-nfERPRCITBp970HO.htm](feats/ancestry-01-nfERPRCITBp970HO.htm)|Earned Glory|Gloria obtenida|oficial|
|[ancestry-01-nyhQ9xB0rkoAoNbf.htm](feats/ancestry-01-nyhQ9xB0rkoAoNbf.htm)|Grimspawn|Engendro macabro|oficial|
|[ancestry-01-o4LycqplO14zn6It.htm](feats/ancestry-01-o4LycqplO14zn6It.htm)|Know Your Own|Conocer a los tuyos|oficial|
|[ancestry-01-O5v8yaeCbjKeXfyi.htm](feats/ancestry-01-O5v8yaeCbjKeXfyi.htm)|Duskwalker Lore|Saber de Crepúscular|oficial|
|[ancestry-01-P1dk0LTWkQ1LT1ai.htm](feats/ancestry-01-P1dk0LTWkQ1LT1ai.htm)|Svetocher|Svetocher|oficial|
|[ancestry-01-PGVXjbAi1Fa4uTmD.htm](feats/ancestry-01-PGVXjbAi1Fa4uTmD.htm)|Halo|Halo|oficial|
|[ancestry-01-Pox93XMBaFmeLIDM.htm](feats/ancestry-01-Pox93XMBaFmeLIDM.htm)|Cheek Pouches|Abazones|oficial|
|[ancestry-01-PPUNMjRLQYnmwQvF.htm](feats/ancestry-01-PPUNMjRLQYnmwQvF.htm)|Kobold Breath|Aliento de Kóbold|oficial|
|[ancestry-01-PVkAEBlRSJHe3JCz.htm](feats/ancestry-01-PVkAEBlRSJHe3JCz.htm)|Straveika|Straveika|oficial|
|[ancestry-01-Qb25uu1gT5CDMSWb.htm](feats/ancestry-01-Qb25uu1gT5CDMSWb.htm)|Hag Claws|Garras de saga|oficial|
|[ancestry-01-QHwajD5n8P3oS9Wb.htm](feats/ancestry-01-QHwajD5n8P3oS9Wb.htm)|Vicious Incisors|Incisivos crueles|oficial|
|[ancestry-01-sJ7WTLDwAbIA9Elc.htm](feats/ancestry-01-sJ7WTLDwAbIA9Elc.htm)|Burn It!|¡Quemadlo!|oficial|
|[ancestry-01-sKDCoxMz2yKWLGRJ.htm](feats/ancestry-01-sKDCoxMz2yKWLGRJ.htm)|Celestial Lore|Saber de celestial|oficial|
|[ancestry-01-SPyvwsiSghySIEw2.htm](feats/ancestry-01-SPyvwsiSghySIEw2.htm)|Dragon's Presence|Presencia del Dragón|oficial|
|[ancestry-01-UJ8AqzkkDqRCMNFW.htm](feats/ancestry-01-UJ8AqzkkDqRCMNFW.htm)|Dwarven Doughtiness|Combatividad enana|oficial|
|[ancestry-01-VcnOEAM3UR7oS0D5.htm](feats/ancestry-01-VcnOEAM3UR7oS0D5.htm)|Razzle-Dazzle|Escándalo|oficial|
|[ancestry-01-VDiMapgJoFI3CCol.htm](feats/ancestry-01-VDiMapgJoFI3CCol.htm)|Ratspeak|Hablar con las ratas|oficial|
|[ancestry-01-W2LmEXJH75tyeCSn.htm](feats/ancestry-01-W2LmEXJH75tyeCSn.htm)|Rat Familiar|Familiar rata|oficial|
|[ancestry-01-WoKiYMCXV27szBdy.htm](feats/ancestry-01-WoKiYMCXV27szBdy.htm)|Cringe|Encogerse|oficial|
|[ancestry-01-wTT8ieLvfWsZZaWT.htm](feats/ancestry-01-wTT8ieLvfWsZZaWT.htm)|Tinkering Fingers|Dedos juguetones|oficial|
|[ancestry-01-wtTye8OrC9cuK7YP.htm](feats/ancestry-01-wtTye8OrC9cuK7YP.htm)|Beast Trainer|Adiestrador de bestias|oficial|
|[ancestry-01-xTe8lNBp76jsrhYh.htm](feats/ancestry-01-xTe8lNBp76jsrhYh.htm)|Slag May|Maia ferrosa|oficial|
|[ancestry-01-xzpMQ2ZRn9zC23XG.htm](feats/ancestry-01-xzpMQ2ZRn9zC23XG.htm)|Scamper|Engaño|oficial|
|[ancestry-01-y7Or0CbcQBDdS9yG.htm](feats/ancestry-01-y7Or0CbcQBDdS9yG.htm)|Elven Aloofness|Elfo distante|oficial|
|[ancestry-01-Y8sKn8NH1wC7Mrui.htm](feats/ancestry-01-Y8sKn8NH1wC7Mrui.htm)|Fangs|Colmillos|oficial|
|[ancestry-01-ydgCsYsgqSkFWEDK.htm](feats/ancestry-01-ydgCsYsgqSkFWEDK.htm)|Angelkin|Angélico|oficial|
|[ancestry-01-zaGD9Og2p8Opa0oJ.htm](feats/ancestry-01-zaGD9Og2p8Opa0oJ.htm)|Scavenger's Search|Búsqueda de carroñero|oficial|
|[ancestry-05-14u6604yUXvoiIf3.htm](feats/ancestry-05-14u6604yUXvoiIf3.htm)|Malicious Bane|Perdición maliciosa|oficial|
|[ancestry-05-1muxLx8Vacn2SLHc.htm](feats/ancestry-05-1muxLx8Vacn2SLHc.htm)|Kneecap|Disparar a la rodilla|oficial|
|[ancestry-05-2RmsQrLySNYQ4uIn.htm](feats/ancestry-05-2RmsQrLySNYQ4uIn.htm)|Spirit Soother|Calmante de espíritus|oficial|
|[ancestry-05-2vlQ09QIBli5u9Gz.htm](feats/ancestry-05-2vlQ09QIBli5u9Gz.htm)|Called|Convocado|oficial|
|[ancestry-05-3HuiLoQuJKLAh5rV.htm](feats/ancestry-05-3HuiLoQuJKLAh5rV.htm)|Springing Leaper|Saltador a brincos|oficial|
|[ancestry-05-69h9D3syUYLgIPr7.htm](feats/ancestry-05-69h9D3syUYLgIPr7.htm)|Expanded Luck|Suerte extendida|oficial|
|[ancestry-05-7AuOURsTkkjrRekp.htm](feats/ancestry-05-7AuOURsTkkjrRekp.htm)|Loud Singer|Cantante ruidoso|oficial|
|[ancestry-05-7ZwXUlnqjj9zEYbO.htm](feats/ancestry-05-7ZwXUlnqjj9zEYbO.htm)|Vandal|Vándalo|oficial|
|[ancestry-05-8CSewrCVEmL8sjnk.htm](feats/ancestry-05-8CSewrCVEmL8sjnk.htm)|Celestial Resistance|Resistencia Celestial|oficial|
|[ancestry-05-8DIzXO1YpsU3DpJw.htm](feats/ancestry-05-8DIzXO1YpsU3DpJw.htm)|Snare Genius|Genio de las trampas de lazo|oficial|
|[ancestry-05-8MMYFIudSDkdo8cx.htm](feats/ancestry-05-8MMYFIudSDkdo8cx.htm)|Rat Magic|Magia de las ratas|oficial|
|[ancestry-05-8Mo0fKRAz1TVbrr1.htm](feats/ancestry-05-8Mo0fKRAz1TVbrr1.htm)|Catfolk Weapon Rake|Desgarrar con armas de Félido|oficial|
|[ancestry-05-aQNsD2t0Tb4vToA4.htm](feats/ancestry-05-aQNsD2t0Tb4vToA4.htm)|Hold Mark|Marca de clan|oficial|
|[ancestry-05-aVRuchEAJIvnd70k.htm](feats/ancestry-05-aVRuchEAJIvnd70k.htm)|Skillful Tail (Tiefling)|Cola hábil (Tiflin)|oficial|
|[ancestry-05-BuaOQzmRNgIhGsfN.htm](feats/ancestry-05-BuaOQzmRNgIhGsfN.htm)|Blessed Blood (Aasimar)|Sangre bendita (Aasimar)|oficial|
|[ancestry-05-CXgTn2sE7Do11rlv.htm](feats/ancestry-05-CXgTn2sE7Do11rlv.htm)|Mist Child|Hijo de la bruma|oficial|
|[ancestry-05-EdVMwFRNV1LX1VWh.htm](feats/ancestry-05-EdVMwFRNV1LX1VWh.htm)|Light Paws|Zarpas ligeras|oficial|
|[ancestry-05-eJ6CytjWx2sGnKnC.htm](feats/ancestry-05-eJ6CytjWx2sGnKnC.htm)|Fiendish Resistance|Resistencia infernal|oficial|
|[ancestry-05-fD3RSV9nIkJsW6lD.htm](feats/ancestry-05-fD3RSV9nIkJsW6lD.htm)|One-Toed Hop|Brinco a una pata|oficial|
|[ancestry-05-FGrIFDobGRFBPOuM.htm](feats/ancestry-05-FGrIFDobGRFBPOuM.htm)|Dwarven Reinforcement|Refuerzo enano|oficial|
|[ancestry-05-G1BPXzTzrUE4IndV.htm](feats/ancestry-05-G1BPXzTzrUE4IndV.htm)|Defy The Darkness|Desafiar la oscuridad|oficial|
|[ancestry-05-GgerQCCsGibaGWq0.htm](feats/ancestry-05-GgerQCCsGibaGWq0.htm)|Necromantic Physiology|Fisiología nigromántica|oficial|
|[ancestry-05-gS9FYlD0Vt8yyZkP.htm](feats/ancestry-05-gS9FYlD0Vt8yyZkP.htm)|Grovel|Denigrarse|oficial|
|[ancestry-05-iNrR4WOB1UO9iiNE.htm](feats/ancestry-05-iNrR4WOB1UO9iiNE.htm)|Lifesense|Sentir la vida|oficial|
|[ancestry-05-j0mlvJcuYGFuMG2S.htm](feats/ancestry-05-j0mlvJcuYGFuMG2S.htm)|Ally's Shelter|Refugio del aliado|oficial|
|[ancestry-05-JvTSfyCkG70bmY7f.htm](feats/ancestry-05-JvTSfyCkG70bmY7f.htm)|Bloody Blows|Golpes sangrientos|oficial|
|[ancestry-05-l3PosipTLXANeoT8.htm](feats/ancestry-05-l3PosipTLXANeoT8.htm)|Step Lively|Paso enérgico|oficial|
|[ancestry-05-LAArv2uv6TOkTzQO.htm](feats/ancestry-05-LAArv2uv6TOkTzQO.htm)|Quick Stow (Ratfolk)|Almacenamiento rápido (Rátido)|oficial|
|[ancestry-05-m5JYglEObpWC3dhP.htm](feats/ancestry-05-m5JYglEObpWC3dhP.htm)|Sense Allies|Sentir a los aliados|oficial|
|[ancestry-05-n3CbbtK4fgBznIMf.htm](feats/ancestry-05-n3CbbtK4fgBznIMf.htm)|Empyreal Blessing|Bendición Empírea|oficial|
|[ancestry-05-NFsQOa3ynthYLVj6.htm](feats/ancestry-05-NFsQOa3ynthYLVj6.htm)|Ward Against Corruption|Custodia contra la corrupción|oficial|
|[ancestry-05-QE8asCPqyrdenll0.htm](feats/ancestry-05-QE8asCPqyrdenll0.htm)|Lab Rat|Rata de laboratorio|oficial|
|[ancestry-05-qXl2cOh3wL3QszCy.htm](feats/ancestry-05-qXl2cOh3wL3QszCy.htm)|Martial Experience|Experiencia marcial|oficial|
|[ancestry-05-rFmJVDdB313EibTs.htm](feats/ancestry-05-rFmJVDdB313EibTs.htm)|Eat Fortune|Comer Fortuna|oficial|
|[ancestry-05-scNrNhnGTgPIzoj7.htm](feats/ancestry-05-scNrNhnGTgPIzoj7.htm)|Ancestral Suspicion|Sospecha ancestral|oficial|
|[ancestry-05-sGSf5BdopT0zWOWs.htm](feats/ancestry-05-sGSf5BdopT0zWOWs.htm)|Sheltering Slab|Losa protectora|oficial|
|[ancestry-05-TlMSleKR8Bh0EuSW.htm](feats/ancestry-05-TlMSleKR8Bh0EuSW.htm)|Climbing Claws|Garras Trepadoras|oficial|
|[ancestry-05-twnZopGlB392hmqH.htm](feats/ancestry-05-twnZopGlB392hmqH.htm)|Project Persona|Proyectar personaje|oficial|
|[ancestry-05-WPz97m5FNlbLIQ6p.htm](feats/ancestry-05-WPz97m5FNlbLIQ6p.htm)|Long-Nosed Form|Forma nariguda|oficial|
|[ancestry-05-Wwr8VilSybQgVtin.htm](feats/ancestry-05-Wwr8VilSybQgVtin.htm)|Athletic Might|Poder Atlético|oficial|
|[ancestry-05-X6tVQ5S7H7GuCZux.htm](feats/ancestry-05-X6tVQ5S7H7GuCZux.htm)|Tengu Weapon Study|Estudio de las armas de los Tengu|oficial|
|[ancestry-05-ZOJsLNziWQ7Ri3x9.htm](feats/ancestry-05-ZOJsLNziWQ7Ri3x9.htm)|Undead Slayer|Matador de muertos vivientes|oficial|
|[ancestry-05-Zz5A8Yg0jGSK8GNu.htm](feats/ancestry-05-Zz5A8Yg0jGSK8GNu.htm)|Enthralling Allure|Encanto cautivador|oficial|
|[ancestry-09-1WheVs50iwMBi6KC.htm](feats/ancestry-09-1WheVs50iwMBi6KC.htm)|Archon Magic|Magia de los arcontes|oficial|
|[ancestry-09-3N6T7AQETVKaAwiR.htm](feats/ancestry-09-3N6T7AQETVKaAwiR.htm)|Demon Magic|Magia de demonio|oficial|
|[ancestry-09-3Y1k2eAMqCdcCGmK.htm](feats/ancestry-09-3Y1k2eAMqCdcCGmK.htm)|Stone Bones|Huesos de piedra|oficial|
|[ancestry-09-4hFeaF4MlqIHb6gb.htm](feats/ancestry-09-4hFeaF4MlqIHb6gb.htm)|Azata Magic|Magia de los Azata|oficial|
|[ancestry-09-8ukixWL8MBJOhPbW.htm](feats/ancestry-09-8ukixWL8MBJOhPbW.htm)|Pinch Time|Sacar tiempo|oficial|
|[ancestry-09-aOIZvx5fx5jVHHOO.htm](feats/ancestry-09-aOIZvx5fx5jVHHOO.htm)|Life Leap|Salto vital|oficial|
|[ancestry-09-aznyI5mfMdEFSDr8.htm](feats/ancestry-09-aznyI5mfMdEFSDr8.htm)|Hardy Traveler|Viajero resistente|oficial|
|[ancestry-09-BaVO8UU5ZkL8OZZj.htm](feats/ancestry-09-BaVO8UU5ZkL8OZZj.htm)|Dragon's Breath|Aliento de dragón|oficial|
|[ancestry-09-cCv9pgyXzZ0TQmZg.htm](feats/ancestry-09-cCv9pgyXzZ0TQmZg.htm)|Big Mouth|Boca Grande|oficial|
|[ancestry-09-d022Gp8PjS4Q0ZAC.htm](feats/ancestry-09-d022Gp8PjS4Q0ZAC.htm)|Shared Luck (Catfolk)|Suerte compartida (Félido)|oficial|
|[ancestry-09-DunjA0vLZeozcNxu.htm](feats/ancestry-09-DunjA0vLZeozcNxu.htm)|Devil Magic|Magia del diablo|oficial|
|[ancestry-09-esKk5XrnlqRayDPG.htm](feats/ancestry-09-esKk5XrnlqRayDPG.htm)|Angelic Magic|Magia angélica|oficial|
|[ancestry-09-fu1cTh93zgGweduf.htm](feats/ancestry-09-fu1cTh93zgGweduf.htm)|Wary Skulker|Rondador cauteloso|oficial|
|[ancestry-09-G6rCbMrHacYWNu1K.htm](feats/ancestry-09-G6rCbMrHacYWNu1K.htm)|Aggravating Scratch|Arañazo agravante|oficial|
|[ancestry-09-h11M3QrIHKLj2ezy.htm](feats/ancestry-09-h11M3QrIHKLj2ezy.htm)|Well-Groomed|Arreglado|oficial|
|[ancestry-09-HfebybiUNW8mXOfP.htm](feats/ancestry-09-HfebybiUNW8mXOfP.htm)|Returning Throw|Lanzamiento Retornante|oficial|
|[ancestry-09-IQ9C7glCXVgYecz1.htm](feats/ancestry-09-IQ9C7glCXVgYecz1.htm)|Light From Darkness|Luz de la oscuridad|oficial|
|[ancestry-09-iRpVW1DPKVxlzIzt.htm](feats/ancestry-09-iRpVW1DPKVxlzIzt.htm)|Night Magic|Magia Nocturna|oficial|
|[ancestry-09-jNemcS5GqH8mnjV6.htm](feats/ancestry-09-jNemcS5GqH8mnjV6.htm)|Fiendish Wings|Alas infernales|oficial|
|[ancestry-09-KgiYiDz1lWoBMRlF.htm](feats/ancestry-09-KgiYiDz1lWoBMRlF.htm)|Otherworldly Acumen|Perspicacia de otro mundo|oficial|
|[ancestry-09-KRgzuwwjT30KKvV4.htm](feats/ancestry-09-KRgzuwwjT30KKvV4.htm)|Eclectic Sword Training|Entrenamiento ecléctico con espada|oficial|
|[ancestry-09-KsFrWhIPVLOqxV07.htm](feats/ancestry-09-KsFrWhIPVLOqxV07.htm)|Accursed Claws|Garras malditas|oficial|
|[ancestry-09-lbiFj4At5BxotaNY.htm](feats/ancestry-09-lbiFj4At5BxotaNY.htm)|Dracomancer|Dracomante|oficial|
|[ancestry-09-LK6niBb38mEraYRS.htm](feats/ancestry-09-LK6niBb38mEraYRS.htm)|Overcrowd|Hacinamiento|oficial|
|[ancestry-09-nKkbEKbE9vfKWKdd.htm](feats/ancestry-09-nKkbEKbE9vfKWKdd.htm)|Echoes In Stone|Ecos en la piedra|oficial|
|[ancestry-09-O9z1MJpEaf6Y1Acd.htm](feats/ancestry-09-O9z1MJpEaf6Y1Acd.htm)|Dance Underfoot|Bailar bajo los pies|oficial|
|[ancestry-09-ovMIxhiStlPE7tty.htm](feats/ancestry-09-ovMIxhiStlPE7tty.htm)|Divine Countermeasures|Contramedidas divinas|oficial|
|[ancestry-09-PxWdf4HAhZ8fUB3R.htm](feats/ancestry-09-PxWdf4HAhZ8fUB3R.htm)|Duskwalker Magic|Magia crepuscular|oficial|
|[ancestry-09-qS2VcFLez4PLOpIS.htm](feats/ancestry-09-qS2VcFLez4PLOpIS.htm)|Tree Climber (Elf)|Trepador de árboles (elfo)|oficial|
|[ancestry-09-RFXzsfEgz7WbDCQO.htm](feats/ancestry-09-RFXzsfEgz7WbDCQO.htm)|Unhampered Passage|Paso sin obstáculos|oficial|
|[ancestry-09-S7z1LbnSRlBep8rO.htm](feats/ancestry-09-S7z1LbnSRlBep8rO.htm)|Spirit Strikes|Golpes de espíritu|oficial|
|[ancestry-09-sGdREpnSJDzEacub.htm](feats/ancestry-09-sGdREpnSJDzEacub.htm)|Group Aid|Ayuda en grupo|oficial|
|[ancestry-09-SiedJ6hnDLEGeeBj.htm](feats/ancestry-09-SiedJ6hnDLEGeeBj.htm)|Cling|Adherirse|oficial|
|[ancestry-09-SuEmijj909yxmYOO.htm](feats/ancestry-09-SuEmijj909yxmYOO.htm)|Rat Form|Forma de rata|oficial|
|[ancestry-09-Tl8yckXeTCHnwrlM.htm](feats/ancestry-09-Tl8yckXeTCHnwrlM.htm)|Soaring Flight|Vuelo ascendente|oficial|
|[ancestry-09-UKYO5kiOnCY1hgCD.htm](feats/ancestry-09-UKYO5kiOnCY1hgCD.htm)|Daemon Magic|Magia de daimonion|oficial|
|[ancestry-09-vtCrMziYxNyj8kP7.htm](feats/ancestry-09-vtCrMziYxNyj8kP7.htm)|Celestial Wings|Alas Celestiales|oficial|
|[ancestry-09-YDHr12qVA3XRjkLP.htm](feats/ancestry-09-YDHr12qVA3XRjkLP.htm)|Bloodletting Fangs|Colmillos desangradores|oficial|
|[ancestry-09-ZcKW5n7F0oAqRw5o.htm](feats/ancestry-09-ZcKW5n7F0oAqRw5o.htm)|Death's Drums|Tambores de la Muerte|oficial|
|[ancestry-09-zL6zUrt44tZYyuh4.htm](feats/ancestry-09-zL6zUrt44tZYyuh4.htm)|Between The Scales|Entre las escamas|oficial|
|[ancestry-09-zPJ2NyMv97AfkN3P.htm](feats/ancestry-09-zPJ2NyMv97AfkN3P.htm)|Occult Resistance|Resistencia al ocultismo|oficial|
|[ancestry-09-ZPK2Un5ChzeNc9Dx.htm](feats/ancestry-09-ZPK2Un5ChzeNc9Dx.htm)|Cautious Curiosity|Curiosidad cautelosa|oficial|
|[ancestry-09-zZOycr00XDVTKuXa.htm](feats/ancestry-09-zZOycr00XDVTKuXa.htm)|Undying Ferocity|Ferocidad inmortal|oficial|
|[ancestry-13-05wQPB1Z20DOy4rH.htm](feats/ancestry-13-05wQPB1Z20DOy4rH.htm)|Summon Fiendish Kin|Convocar pariente infernal|oficial|
|[ancestry-13-55XNy1TVETEMc0vf.htm](feats/ancestry-13-55XNy1TVETEMc0vf.htm)|Stubborn Persistence|Persistencia obstinada|oficial|
|[ancestry-13-8H72RC7QI1i8wjJ1.htm](feats/ancestry-13-8H72RC7QI1i8wjJ1.htm)|Warren Digger|Excavador de madrigueras|oficial|
|[ancestry-13-8JOLT1UX5BVG4kVY.htm](feats/ancestry-13-8JOLT1UX5BVG4kVY.htm)|Tengu Weapon Expertise|Pericia con armas de los Tengu|oficial|
|[ancestry-13-ADDOwO0QWOXAV85x.htm](feats/ancestry-13-ADDOwO0QWOXAV85x.htm)|Telluric Power|Poder telúrico|oficial|
|[ancestry-13-dgpoTae18H4zc9fH.htm](feats/ancestry-13-dgpoTae18H4zc9fH.htm)|Catfolk Weapon Expertise|Pericia con las armas de félido|oficial|
|[ancestry-13-FqZKSSBU7M4zhsXM.htm](feats/ancestry-13-FqZKSSBU7M4zhsXM.htm)|Resist Ruin|Resistir la ruina|oficial|
|[ancestry-13-fX5FybM93HIQRRd1.htm](feats/ancestry-13-fX5FybM93HIQRRd1.htm)|Black Cat Curse|Maldición del Gato Negro|oficial|
|[ancestry-13-Ht6b8H9DpA9lWzAg.htm](feats/ancestry-13-Ht6b8H9DpA9lWzAg.htm)|Advanced General Training|Entrenamiento general avanzado|oficial|
|[ancestry-13-I9cJYC7anz7HmpcJ.htm](feats/ancestry-13-I9cJYC7anz7HmpcJ.htm)|Elite Dracomancer|Dracomante de Élite|oficial|
|[ancestry-13-jatfexNkXaTs9s5Q.htm](feats/ancestry-13-jatfexNkXaTs9s5Q.htm)|Instinctive Obfuscation|Ofuscación instintiva|oficial|
|[ancestry-13-L231BR4815B6hwKT.htm](feats/ancestry-13-L231BR4815B6hwKT.htm)|Summon Celestial Kin|Convocar a un pariente celestial|oficial|
|[ancestry-13-l78KwojlT0AWvS4l.htm](feats/ancestry-13-l78KwojlT0AWvS4l.htm)|Toppling Dance|Danza derribante|oficial|
|[ancestry-13-LTcpBbnngfuYTdB0.htm](feats/ancestry-13-LTcpBbnngfuYTdB0.htm)|Avenge Ally|Vengar aliado|oficial|
|[ancestry-13-MR4X38qgBj5tmkMw.htm](feats/ancestry-13-MR4X38qgBj5tmkMw.htm)|Bounce Back|Recuperarse|oficial|
|[ancestry-13-nePEcAp7lTL35uyx.htm](feats/ancestry-13-nePEcAp7lTL35uyx.htm)|Caterwaul|Maullar|oficial|
|[ancestry-13-Ot1jI91ccnB2ayfk.htm](feats/ancestry-13-Ot1jI91ccnB2ayfk.htm)|Celestial Strikes|Golpes celestiales|oficial|
|[ancestry-13-pedHIDAVLFzzjGO1.htm](feats/ancestry-13-pedHIDAVLFzzjGO1.htm)|Spell Devourer|Devorador de conjuros|oficial|
|[ancestry-13-rCajlx2KjGxzabAJ.htm](feats/ancestry-13-rCajlx2KjGxzabAJ.htm)|Fiend's Door|Puerta del infernal|oficial|
|[ancestry-13-t6GBBIwX7hvvxYyV.htm](feats/ancestry-13-t6GBBIwX7hvvxYyV.htm)|Hag Magic|Magia de saga|oficial|
|[ancestry-13-V6iJ4v15Qc8awVCH.htm](feats/ancestry-13-V6iJ4v15Qc8awVCH.htm)|Aasimar's Mercy|Misericordia del Aasimar|oficial|
|[ancestry-13-wteuGNhOXLvBudRQ.htm](feats/ancestry-13-wteuGNhOXLvBudRQ.htm)|Ferocious Beasts|Bestias feroces|oficial|
|[ancestry-13-xjE123p2rPTsz1eV.htm](feats/ancestry-13-xjE123p2rPTsz1eV.htm)|Fiendish Strikes|Golpes diabólicos|oficial|
|[ancestry-17-2pYCGPCQDHD3o7Jz.htm](feats/ancestry-17-2pYCGPCQDHD3o7Jz.htm)|Fiendish Word|Palabra diabólica|oficial|
|[ancestry-17-7YvOqcdp9Z0RALMp.htm](feats/ancestry-17-7YvOqcdp9Z0RALMp.htm)|Shadow Self|Yo Sombrío|oficial|
|[ancestry-17-9eaUS0jJCpxuNXO5.htm](feats/ancestry-17-9eaUS0jJCpxuNXO5.htm)|Rampaging Ferocity|Ferocidad desatada|oficial|
|[ancestry-17-ABXUfGoeBgCyFasg.htm](feats/ancestry-17-ABXUfGoeBgCyFasg.htm)|Great Tengu Form|Forma de Tengu mayor|oficial|
|[ancestry-17-fqw1ELaqavuKLHIj.htm](feats/ancestry-17-fqw1ELaqavuKLHIj.htm)|Reckless Abandon (Goblin)|Abandono temerario (Goblin)|oficial|
|[ancestry-17-iVwsLYjOJbfvL0Pe.htm](feats/ancestry-17-iVwsLYjOJbfvL0Pe.htm)|Heroic Presence|Presencia Heroica|oficial|
|[ancestry-17-KL3Pk10ReItAHTw9.htm](feats/ancestry-17-KL3Pk10ReItAHTw9.htm)|Celestial Word|Palabra Celestial|oficial|
|[ancestry-17-nJRcbt72wRk5Rmc4.htm](feats/ancestry-17-nJRcbt72wRk5Rmc4.htm)|Symphony Of Blood|Sinfonía De sangre|oficial|
|[ancestry-17-OWL6ZNVWMU0AFqvZ.htm](feats/ancestry-17-OWL6ZNVWMU0AFqvZ.htm)|Eternal Wings (Aasimar)|Alas Eternas (Aasimar)|oficial|
|[ancestry-17-QgNo1s6nVbKPU4St.htm](feats/ancestry-17-QgNo1s6nVbKPU4St.htm)|Reliable Luck|Suerte fiable|oficial|
|[ancestry-17-QXZFvsZDduxwTJYM.htm](feats/ancestry-17-QXZFvsZDduxwTJYM.htm)|Magic Rider|Jinete de la magia|oficial|
|[ancestry-17-UKHIKipBvOGhzcSQ.htm](feats/ancestry-17-UKHIKipBvOGhzcSQ.htm)|Homeward Bound|De vuelta a casa|oficial|
|[ancestry-17-UquGIEFKhutgJsEz.htm](feats/ancestry-17-UquGIEFKhutgJsEz.htm)|Stonegate|Portal de piedra|oficial|
|[ancestry-17-wfOGyLnuVMFo7Rwy.htm](feats/ancestry-17-wfOGyLnuVMFo7Rwy.htm)|Boneyard's Call|Llamada del osario|oficial|
|[class-01-0BR61rW4JFOfO7T7.htm](feats/class-01-0BR61rW4JFOfO7T7.htm)|Cackle|Risa estridente|oficial|
|[class-01-1MWL2uEmyiOfYtJn.htm](feats/class-01-1MWL2uEmyiOfYtJn.htm)|Disarming Flair|Estilo desarmante|oficial|
|[class-01-5FyvwI24mnROzh61.htm](feats/class-01-5FyvwI24mnROzh61.htm)|Combat Assessment|Evaluación de combate|oficial|
|[class-01-5GqjEM22n78Vmdpe.htm](feats/class-01-5GqjEM22n78Vmdpe.htm)|Focused Fascination|Fascinación concentrada|oficial|
|[class-01-7FRYyKXDKjGoANYj.htm](feats/class-01-7FRYyKXDKjGoANYj.htm)|Stumbling Stance|Posición del borracho|oficial|
|[class-01-82ATEfDMPkZDxV5H.htm](feats/class-01-82ATEfDMPkZDxV5H.htm)|Blessed Blood (Sorcerer)|Sangre bendita (Hechicero)|oficial|
|[class-01-9bgl6qYWKHzqWZj0.htm](feats/class-01-9bgl6qYWKHzqWZj0.htm)|Flexible Studies|Estudios Flexibles|oficial|
|[class-01-9CaaU5szcf3mtJXD.htm](feats/class-01-9CaaU5szcf3mtJXD.htm)|Overextending Feint|Finta excesiva|oficial|
|[class-01-avCmIwmwJH7d7gri.htm](feats/class-01-avCmIwmwJH7d7gri.htm)|Ancestral Blood Magic|Magia de sangre ancestral|oficial|
|[class-01-BBj6jrdyff7QOgjH.htm](feats/class-01-BBj6jrdyff7QOgjH.htm)|Adrenaline Rush|Descarga de adrenalina|oficial|
|[class-01-bCizH4ByTwbLcYA1.htm](feats/class-01-bCizH4ByTwbLcYA1.htm)|One For All|Uno Para Todos|oficial|
|[class-01-BWomK7EVY0WXxWgh.htm](feats/class-01-BWomK7EVY0WXxWgh.htm)|Reach Spell|Conjuro de alcance|oficial|
|[class-01-C5tSxOwDILefw4zq.htm](feats/class-01-C5tSxOwDILefw4zq.htm)|Buckler Expertise|Experiencia con la rodela|oficial|
|[class-01-dNH8OHEvx3vI9NBQ.htm](feats/class-01-dNH8OHEvx3vI9NBQ.htm)|Nimble Dodge|Esquiva ágil|oficial|
|[class-01-DqD7htz8Sd1dh3BT.htm](feats/class-01-DqD7htz8Sd1dh3BT.htm)|Gorilla Stance|Posición del Gorila|oficial|
|[class-01-EOmTf95t03y4IGdp.htm](feats/class-01-EOmTf95t03y4IGdp.htm)|Draconic Arrogance|Arrogancia dracónica|oficial|
|[class-01-EpBG4CFMNSZQx7vI.htm](feats/class-01-EpBG4CFMNSZQx7vI.htm)|Counterspell (Prepared)|Contraconjuro (Preparado)|oficial|
|[class-01-eww9tuiXPnZFZ3DU.htm](feats/class-01-eww9tuiXPnZFZ3DU.htm)|Plant Evidence|Colocar pruebas|oficial|
|[class-01-FCzfh8QHMo7QJpAM.htm](feats/class-01-FCzfh8QHMo7QJpAM.htm)|Spellbook Prodigy|Libro de conjuros prodigioso|oficial|
|[class-01-FhxkU6OftQeecpQW.htm](feats/class-01-FhxkU6OftQeecpQW.htm)|Heal Companion|Curar Compañero|oficial|
|[class-01-FoWO4RnHRwfEIC7Q.htm](feats/class-01-FoWO4RnHRwfEIC7Q.htm)|Widen Spell|Extender conjuro|oficial|
|[class-01-gjec0ts3wkFbjvHN.htm](feats/class-01-gjec0ts3wkFbjvHN.htm)|You're Next|Tú eres el siguiente|oficial|
|[class-01-hXYnwpi95E77qfAu.htm](feats/class-01-hXYnwpi95E77qfAu.htm)|Goading Feint|Finta provocadora|oficial|
|[class-01-iT39wlCEC1aWaSx7.htm](feats/class-01-iT39wlCEC1aWaSx7.htm)|Gravity Weapon|Arma de gravedad|oficial|
|[class-01-iWvpq3uDZcXvBJj8.htm](feats/class-01-iWvpq3uDZcXvBJj8.htm)|Known Weaknesses|Debilidades conocidas|oficial|
|[class-01-iX5HEqRImhKzfPR2.htm](feats/class-01-iX5HEqRImhKzfPR2.htm)|Well-Versed|Bien versado|oficial|
|[class-01-jGTRRCqxn1FIBxE2.htm](feats/class-01-jGTRRCqxn1FIBxE2.htm)|Hymn Of Healing|Himno De curación|oficial|
|[class-01-KaCpXuYuho3nnDUy.htm](feats/class-01-KaCpXuYuho3nnDUy.htm)|Vicious Vengeance|Venganza cruel|oficial|
|[class-01-KYl1rWFOHe0e6VqJ.htm](feats/class-01-KYl1rWFOHe0e6VqJ.htm)|Premonition Of Avoidance|Premonición de evitación|oficial|
|[class-01-MJW4VP7PjVAX131C.htm](feats/class-01-MJW4VP7PjVAX131C.htm)|Glean Lore|Vislumbrar saber|oficial|
|[class-01-MRMW0EiuOO20pzG2.htm](feats/class-01-MRMW0EiuOO20pzG2.htm)|Ongoing Selfishness|Egoísmo continuo|oficial|
|[class-01-oA866uVEFu1OrAX0.htm](feats/class-01-oA866uVEFu1OrAX0.htm)|Trap Finder|Buscador de trampas|oficial|
|[class-01-pgIrr8xrlCXbRAeo.htm](feats/class-01-pgIrr8xrlCXbRAeo.htm)|Underworld Investigator|Investigador de los bajos fondos|oficial|
|[class-01-pIHjH1x0AVtiX5Tv.htm](feats/class-01-pIHjH1x0AVtiX5Tv.htm)|That's Odd|Qué raro|oficial|
|[class-01-PqImBJ2JrPo5oFxc.htm](feats/class-01-PqImBJ2JrPo5oFxc.htm)|Vile Desecration|Profanación perversa|oficial|
|[class-01-q1iP3SjAF5uceI0M.htm](feats/class-01-q1iP3SjAF5uceI0M.htm)|Martial Performance|Interpretación marcial|oficial|
|[class-01-qgNc5XwjsaWET0Op.htm](feats/class-01-qgNc5XwjsaWET0Op.htm)|Flying Blade|Hoja Voladora|oficial|
|[class-01-qPFWEyihvbWsCcUv.htm](feats/class-01-qPFWEyihvbWsCcUv.htm)|Verdant Weapon|Arma de vegetación|oficial|
|[class-01-QZ00D2xdJnbWFzml.htm](feats/class-01-QZ00D2xdJnbWFzml.htm)|Subtle Delivery|Entrega sutil|oficial|
|[class-01-WYaKRREZUSH0jel5.htm](feats/class-01-WYaKRREZUSH0jel5.htm)|Desperate Prayer|Plegaria desesperada|oficial|
|[class-01-XttSGDuAsRDTuvgS.htm](feats/class-01-XttSGDuAsRDTuvgS.htm)|Iron Repercussions|Consecuencias férreas|oficial|
|[class-01-YG2RxXE9SMfwo6wP.htm](feats/class-01-YG2RxXE9SMfwo6wP.htm)|Monastic Archer Stance|Posición del arquero monástico|oficial|
|[class-01-YKqMuuC8j35NFh92.htm](feats/class-01-YKqMuuC8j35NFh92.htm)|Tumble Behind (Rogue)|Hacer acrobacias por detrás (Pícaro)|oficial|
|[class-01-yPWNbTqOIKdkwaVq.htm](feats/class-01-yPWNbTqOIKdkwaVq.htm)|Takedown Expert|Experto en derribo|oficial|
|[class-01-yVC5pVZaWczYWGTa.htm](feats/class-01-yVC5pVZaWczYWGTa.htm)|Syncretism|Sincretismo|oficial|
|[class-01-zUtdBd3IbM7UX0AD.htm](feats/class-01-zUtdBd3IbM7UX0AD.htm)|Cauldron|Caldero|oficial|
|[class-01-zY6y802bOouMYYFV.htm](feats/class-01-zY6y802bOouMYYFV.htm)|Wortwitch|Brujo de las hierbas|oficial|
|[class-02-1t5479E6bdvFs4E7.htm](feats/class-02-1t5479E6bdvFs4E7.htm)|Talisman Dabbler Dedication|Dedicación de aficionado a los talismanes|oficial|
|[class-02-38L5yCVNgRAFFMsZ.htm](feats/class-02-38L5yCVNgRAFFMsZ.htm)|Cantrip Expansion|Expansión de truco|oficial|
|[class-02-3kH0fGOIoYvPNQsq.htm](feats/class-02-3kH0fGOIoYvPNQsq.htm)|After You|Después de ti|oficial|
|[class-02-4QFElZoWjg1X0vsg.htm](feats/class-02-4QFElZoWjg1X0vsg.htm)|Archer Dedication|Dedicación de arquero|oficial|
|[class-02-5Q69PI8jdQVkb1ZT.htm](feats/class-02-5Q69PI8jdQVkb1ZT.htm)|Cavalier Dedication|Dedicación de adalid|oficial|
|[class-02-5SBFayX7JqKYANwa.htm](feats/class-02-5SBFayX7JqKYANwa.htm)|Rebounding Toss|Lanzamiento de rebote|oficial|
|[class-02-6cQSPqXoAO6oJl0i.htm](feats/class-02-6cQSPqXoAO6oJl0i.htm)|Shooting Stars Stance|Posición de la lluvia de estrellas|oficial|
|[class-02-6QOcQ8ooP1vjQACX.htm](feats/class-02-6QOcQ8ooP1vjQACX.htm)|Nonlethal Spell|Conjuro no letal|oficial|
|[class-02-9WzZc110jCNnjjRz.htm](feats/class-02-9WzZc110jCNnjjRz.htm)|Lightslayer Oath|Juramento del asesino de la luz|oficial|
|[class-02-A4sV0cRU9I8ztbHY.htm](feats/class-02-A4sV0cRU9I8ztbHY.htm)|Bashing Charge|Carga vapuleante|oficial|
|[class-02-aFygWxgSv82WyCsl.htm](feats/class-02-aFygWxgSv82WyCsl.htm)|Acrobat Dedication|Dedicación de acróbata|oficial|
|[class-02-aXdY2wgn0ItWwTr0.htm](feats/class-02-aXdY2wgn0ItWwTr0.htm)|Conceited Mindset|Actitud engreída|oficial|
|[class-02-bRftzbFvSF1pilIo.htm](feats/class-02-bRftzbFvSF1pilIo.htm)|Anoint Ally|Ungir a un aliado|oficial|
|[class-02-c2h9Z8exSFhraJ8j.htm](feats/class-02-c2h9Z8exSFhraJ8j.htm)|Esoteric Oath (Paladin)|Juramento Esotérico (Paladín)|oficial|
|[class-02-CZXhJS55rG5H6PpB.htm](feats/class-02-CZXhJS55rG5H6PpB.htm)|Investigator Dedication|Dedicación de investigador|oficial|
|[class-02-D1o7GUraoFFzjaub.htm](feats/class-02-D1o7GUraoFFzjaub.htm)|Clever Gambit|Engaño inteligente|oficial|
|[class-02-DkoxNw9tsFFXrfJY.htm](feats/class-02-DkoxNw9tsFFXrfJY.htm)|Charmed Life|Vida afortunada|oficial|
|[class-02-DY6pNO3GzHeKSxmQ.htm](feats/class-02-DY6pNO3GzHeKSxmQ.htm)|Finishing Follow-through|Conclusión rematante|oficial|
|[class-02-EaIczkGVI5DUo3c9.htm](feats/class-02-EaIczkGVI5DUo3c9.htm)|Entreat With Forebears|Rogar a los antepasados|oficial|
|[class-02-eAlrvPVb8qt8Lruw.htm](feats/class-02-eAlrvPVb8qt8Lruw.htm)|Swashbuckler Dedication|Dedicación de espadachín|oficial|
|[class-02-eoDDjIGAA67Z7rQt.htm](feats/class-02-eoDDjIGAA67Z7rQt.htm)|Underhanded Assault|Asalto engañoso|oficial|
|[class-02-FkN9QX1W2Iv56bkn.htm](feats/class-02-FkN9QX1W2Iv56bkn.htm)|Song Of Strength|Canción de fuerza|oficial|
|[class-02-fU7d5P6WrfAirgip.htm](feats/class-02-fU7d5P6WrfAirgip.htm)|Tumble Behind (Swashbuckler)|Hacer acrobacias por detrás (Espadachín)|oficial|
|[class-02-gKoNWXem1ikEqE2d.htm](feats/class-02-gKoNWXem1ikEqE2d.htm)|Familiar's Language|Idioma del familiar|oficial|
|[class-02-iS4Vc2zv7vgL5mnX.htm](feats/class-02-iS4Vc2zv7vgL5mnX.htm)|Energy Ablation|Ablación de energía|oficial|
|[class-02-KCwXj3y7Nm4e3NbI.htm](feats/class-02-KCwXj3y7Nm4e3NbI.htm)|Demolition Charge|Carga de demolición|oficial|
|[class-02-lIrPwGpJk9TldZ4c.htm](feats/class-02-lIrPwGpJk9TldZ4c.htm)|Snare Hopping|Trampas de lazo saltarinas|oficial|
|[class-02-N4wCUZH2KG6FoGqh.htm](feats/class-02-N4wCUZH2KG6FoGqh.htm)|Shared Stratagem|Estratagema Compartida|oficial|
|[class-02-N7dTFxpjXGn4ddq8.htm](feats/class-02-N7dTFxpjXGn4ddq8.htm)|Enhanced Familiar|Familiar potenciado|oficial|
|[class-02-OcBaEnGdDm6CuSnr.htm](feats/class-02-OcBaEnGdDm6CuSnr.htm)|Rapid Response|Respuesta rápida|oficial|
|[class-02-OiY0L3WvjwlQQ4iG.htm](feats/class-02-OiY0L3WvjwlQQ4iG.htm)|Strong Arm|Brazo fuerte|oficial|
|[class-02-ot0uyFtnC1Whz5bp.htm](feats/class-02-ot0uyFtnC1Whz5bp.htm)|Directed Audience|Audiencia dirigida|oficial|
|[class-02-OUNj8nXTHwGcEdlh.htm](feats/class-02-OUNj8nXTHwGcEdlh.htm)|Oracle Dedication|Dedicación de Oráculo|oficial|
|[class-02-PRKe5rWYZMZgEpFU.htm](feats/class-02-PRKe5rWYZMZgEpFU.htm)|Archaeologist Dedication|Dedicación de arqueólogo|oficial|
|[class-02-QFFbmQ5yrBSjbAj3.htm](feats/class-02-QFFbmQ5yrBSjbAj3.htm)|Esoteric Oath (Redeemer)|Juramento Esotérico (Redentor)|oficial|
|[class-02-qmFWCHOuubEl7VpX.htm](feats/class-02-qmFWCHOuubEl7VpX.htm)|Domain Acumen|Perspicacio de dominio|oficial|
|[class-02-sIeuPW0j39fTZm08.htm](feats/class-02-sIeuPW0j39fTZm08.htm)|Conceal Spell|Ocultar conjuro|oficial|
|[class-02-Tu1hOEr6Ko9Df54L.htm](feats/class-02-Tu1hOEr6Ko9Df54L.htm)|Athletic Strategist|Estratega altético|oficial|
|[class-02-VQz5VypVRLCloapa.htm](feats/class-02-VQz5VypVRLCloapa.htm)|Ancestral Weaponry|Armamento ancestral|oficial|
|[class-02-VU7aZC7L08Mk1GVA.htm](feats/class-02-VU7aZC7L08Mk1GVA.htm)|Esoteric Oath (Liberator)|Juramento Esotérico (Liberador)|oficial|
|[class-02-VXA50vhIRCBt4vvP.htm](feats/class-02-VXA50vhIRCBt4vvP.htm)|Unbalancing Finisher|Rematador desequilibrante|oficial|
|[class-02-wNr02jsG5nRF23YO.htm](feats/class-02-wNr02jsG5nRF23YO.htm)|Red Herring|Cortina de humo|oficial|
|[class-02-wsq8nncD25Q1fRn2.htm](feats/class-02-wsq8nncD25Q1fRn2.htm)|Basic Lesson|Lección básica|oficial|
|[class-02-Wx12NUjqTOjFrEoW.htm](feats/class-02-Wx12NUjqTOjFrEoW.htm)|Antagonize|Antagonizar|oficial|
|[class-02-WZWSaAwuDgne7Z0c.htm](feats/class-02-WZWSaAwuDgne7Z0c.htm)|Solid Lead|Pista sólida|oficial|
|[class-02-XHaxSBOaFMnBbBKt.htm](feats/class-02-XHaxSBOaFMnBbBKt.htm)|Living Hair|Pelo vivo|oficial|
|[class-02-xoIxiRtBVHV27Rvd.htm](feats/class-02-xoIxiRtBVHV27Rvd.htm)|Divine Aegis|Égida divina|oficial|
|[class-02-y0vdu6DGhKKElmE6.htm](feats/class-02-y0vdu6DGhKKElmE6.htm)|Witch Dedication|Dedicación de brujo|oficial|
|[class-02-y8VecqdECqyH1h6o.htm](feats/class-02-y8VecqdECqyH1h6o.htm)|Magic Hide|Piel mágica|oficial|
|[class-02-yAgFDUU8HfVK4KTy.htm](feats/class-02-yAgFDUU8HfVK4KTy.htm)|Dragging Strike|Golpe de arrastre|oficial|
|[class-04-0FNLI8APwj9NsBDa.htm](feats/class-04-0FNLI8APwj9NsBDa.htm)|Alchemical Discoveries|Descubrimientos alquímicos|oficial|
|[class-04-2VKV7jLRTxWyVjGa.htm](feats/class-04-2VKV7jLRTxWyVjGa.htm)|Improved Familiar (Witch)|Familiar Mejorado (Brujo)|oficial|
|[class-04-5cYFHKQK6OZCwavI.htm](feats/class-04-5cYFHKQK6OZCwavI.htm)|Detective's Readiness|Presteza del detective|oficial|
|[class-04-7XcQ8Ygz5cubGxdC.htm](feats/class-04-7XcQ8Ygz5cubGxdC.htm)|Investigator's Stratagem|Estratagema de investigador|oficial|
|[class-04-7ycF0fgSw1ovUPit.htm](feats/class-04-7ycF0fgSw1ovUPit.htm)|Basic Oracle Spellcasting|Lanzamiento de conjuros básico de oráculo|oficial|
|[class-04-8EbIznFgkG7PHqlE.htm](feats/class-04-8EbIznFgkG7PHqlE.htm)|Divine Access|Acceso divino|oficial|
|[class-04-9j90iE61ZToFR8cu.htm](feats/class-04-9j90iE61ZToFR8cu.htm)|Call Bonded Item|Llamar objeto vinculado|oficial|
|[class-04-agfosPInBLQXNQfa.htm](feats/class-04-agfosPInBLQXNQfa.htm)|Head Stomp|Pisotón en la cabeza|oficial|
|[class-04-aiHbS8FGNYAQBF62.htm](feats/class-04-aiHbS8FGNYAQBF62.htm)|Accelerating Touch|Toque Acelerador|oficial|
|[class-04-AkV4Jyllo6nlK2Sl.htm](feats/class-04-AkV4Jyllo6nlK2Sl.htm)|Cobra Stance|Posición de la cobra|oficial|
|[class-04-AuIE19F9rY3YvXf6.htm](feats/class-04-AuIE19F9rY3YvXf6.htm)|Magical Scholastics|Escolástica mágica|oficial|
|[class-04-baz18CdB13DVMHV9.htm](feats/class-04-baz18CdB13DVMHV9.htm)|Leshy Familiar Secrets|Secretos de familiar leshy|oficial|
|[class-04-C3MgEkPNaIhTddbr.htm](feats/class-04-C3MgEkPNaIhTddbr.htm)|Peafowl Stance|Posición del pavo real|oficial|
|[class-04-CAaXGhHDMRM3Pt4J.htm](feats/class-04-CAaXGhHDMRM3Pt4J.htm)|Cavalier's Banner|Estandarte del adalid|oficial|
|[class-04-cErltcAC7OVnIyO1.htm](feats/class-04-cErltcAC7OVnIyO1.htm)|Predictable!|¡Previsible!|oficial|
|[class-04-cEu8BUS41dlPyPGW.htm](feats/class-04-cEu8BUS41dlPyPGW.htm)|Spiritual Guides|Guías espirituales|oficial|
|[class-04-DS0XlHfi3ztb3ET7.htm](feats/class-04-DS0XlHfi3ztb3ET7.htm)|Split Shot|Dividir disparo|oficial|
|[class-04-DUuCOQ9FiZf7vS5b.htm](feats/class-04-DUuCOQ9FiZf7vS5b.htm)|Ritual Researcher|Investigador ritual|oficial|
|[class-04-eXuCYDzj0UJOxNu9.htm](feats/class-04-eXuCYDzj0UJOxNu9.htm)|Twin Distraction|Distracción gemela|oficial|
|[class-04-FdP21jbjHHGpHut1.htm](feats/class-04-FdP21jbjHHGpHut1.htm)|Tenacious Toxins|Toxinas Tenaces|oficial|
|[class-04-geESDWQVvwScyPph.htm](feats/class-04-geESDWQVvwScyPph.htm)|Basic Witchcraft|Brujería Básica|oficial|
|[class-04-HdhnAm9SNfDqxRSN.htm](feats/class-04-HdhnAm9SNfDqxRSN.htm)|First Revelation|Primera Revelación|oficial|
|[class-04-htOsE4hnSj2gzKdi.htm](feats/class-04-htOsE4hnSj2gzKdi.htm)|Mug|Atracar|oficial|
|[class-04-hYu6XxARNJYdf8Qe.htm](feats/class-04-hYu6XxARNJYdf8Qe.htm)|Scalpel's Point|La punta del Bisturí|oficial|
|[class-04-I10dkdvL6kAnqZWA.htm](feats/class-04-I10dkdvL6kAnqZWA.htm)|Lie Detector|Detector de Mentiras|oficial|
|[class-04-it2i6OXfGIizokpg.htm](feats/class-04-it2i6OXfGIizokpg.htm)|Animal Feature|Rasgo animal|oficial|
|[class-04-IvgLDtXmZwzbVJj1.htm](feats/class-04-IvgLDtXmZwzbVJj1.htm)|Strategic Assessment|Evaluación estratégica|oficial|
|[class-04-j01dM0ZAC7KzShx0.htm](feats/class-04-j01dM0ZAC7KzShx0.htm)|Rites Of Convocation|Ritos de convocación|oficial|
|[class-04-JcXzKwrdMkNszrJQ.htm](feats/class-04-JcXzKwrdMkNszrJQ.htm)|Radiant Infusion|Infusión Radiante|oficial|
|[class-04-JO3mcFvxjRp1V8XK.htm](feats/class-04-JO3mcFvxjRp1V8XK.htm)|Contortionist|Contorsionista|oficial|
|[class-04-k4QU2edqSoB23foo.htm](feats/class-04-k4QU2edqSoB23foo.htm)|The Harder They Fall|Peor será la caída|oficial|
|[class-04-kqW6d3Dfk4nApd7y.htm](feats/class-04-kqW6d3Dfk4nApd7y.htm)|Combat Reading|Lectura de combate|oficial|
|[class-04-KWXoo738KuddWMOB.htm](feats/class-04-KWXoo738KuddWMOB.htm)|Ongoing Investigation|Investigación en curso|oficial|
|[class-04-LJw5tRrX0dMnm9Vq.htm](feats/class-04-LJw5tRrX0dMnm9Vq.htm)|Soothing Mist|Niebla relajante|oficial|
|[class-04-m4FOYkzuqNjU0ETq.htm](feats/class-04-m4FOYkzuqNjU0ETq.htm)|Eldritch Nails|Uñas sobrenaturales|oficial|
|[class-04-N4TpzEzuFbInSgvz.htm](feats/class-04-N4TpzEzuFbInSgvz.htm)|Hunter's Luck|Suerte de cazador|oficial|
|[class-04-NdgMxlz5I1ddT0Zi.htm](feats/class-04-NdgMxlz5I1ddT0Zi.htm)|Basic Mysteries|Misterios básicos|oficial|
|[class-04-NHheDmNB7L4REmlr.htm](feats/class-04-NHheDmNB7L4REmlr.htm)|Swaggering Initiative|Iniciativa arrogante|oficial|
|[class-04-nU5Pow4HMzoDHa8Z.htm](feats/class-04-nU5Pow4HMzoDHa8Z.htm)|Basic Witch Spellcasting|Lanzamiento de conjuros básico de brujo|oficial|
|[class-04-Nxke8WzifQafSa4I.htm](feats/class-04-Nxke8WzifQafSa4I.htm)|Impressive Mount|Montura Impresionante|oficial|
|[class-04-pCVegyXxNibF4ulp.htm](feats/class-04-pCVegyXxNibF4ulp.htm)|Elaborate Flourish|Floritura elaborada|oficial|
|[class-04-Qfn7lmOeXfBtpG4O.htm](feats/class-04-Qfn7lmOeXfBtpG4O.htm)|Impaling Finisher|Rematador empalador|oficial|
|[class-04-RlhvppSmQRqL2RUe.htm](feats/class-04-RlhvppSmQRqL2RUe.htm)|Quick Fix|Colocar rápido|oficial|
|[class-04-sE7x3QAel4VGdkgn.htm](feats/class-04-sE7x3QAel4VGdkgn.htm)|Basic Flair|Estilo básico|oficial|
|[class-04-sEWYOllJ6rYoXK4K.htm](feats/class-04-sEWYOllJ6rYoXK4K.htm)|Raging Athlete|Atleta furioso|oficial|
|[class-04-so4v9xjBFaoJ8EQs.htm](feats/class-04-so4v9xjBFaoJ8EQs.htm)|Supernatural Senses|Sentidos Sobrenaturales|oficial|
|[class-04-sv3ywEHaab9oZ3Nj.htm](feats/class-04-sv3ywEHaab9oZ3Nj.htm)|Courageous Advance|Avance valeroso|oficial|
|[class-04-tIE2umG4rQOxm8D8.htm](feats/class-04-tIE2umG4rQOxm8D8.htm)|Oversized Throw|Lanzamiento desmesurado|oficial|
|[class-04-tmGsnUkPv8SIhBgn.htm](feats/class-04-tmGsnUkPv8SIhBgn.htm)|Flamboyant Athlete|Atleta extravagante|oficial|
|[class-04-to6s7QanfhHukW5r.htm](feats/class-04-to6s7QanfhHukW5r.htm)|Barreling Charge|Carga desenfrenada|oficial|
|[class-04-tWBK7Zbt80JlPryC.htm](feats/class-04-tWBK7Zbt80JlPryC.htm)|Bespell Weapon|Encantar arma|oficial|
|[class-04-Ux0DSklFlIlcvnhO.htm](feats/class-04-Ux0DSklFlIlcvnhO.htm)|Sacred Ki|Ki Sagrado|oficial|
|[class-04-Vab3XIirjs3KQh3t.htm](feats/class-04-Vab3XIirjs3KQh3t.htm)|Cavalier's Charge|Carga de adalid|oficial|
|[class-04-vxA0VRN10OwUkGAr.htm](feats/class-04-vxA0VRN10OwUkGAr.htm)|Cruelty|Crueldad|oficial|
|[class-04-wjnfdh6WzN7HbmeE.htm](feats/class-04-wjnfdh6WzN7HbmeE.htm)|Aura Of Despair|Aura de desesperación|oficial|
|[class-04-ww5AM2yFs0lqQhmD.htm](feats/class-04-ww5AM2yFs0lqQhmD.htm)|Vision Of Weakness|Visión de la debilidad|oficial|
|[class-04-X0NFLIn1bqj6bnd0.htm](feats/class-04-X0NFLIn1bqj6bnd0.htm)|Basic Deduction|Deducción Básica|oficial|
|[class-04-X5gNhaYNx1xu6NoH.htm](feats/class-04-X5gNhaYNx1xu6NoH.htm)|Finishing Precision|Precisión con los remates|oficial|
|[class-04-xRTlbvvBzURgC6M2.htm](feats/class-04-xRTlbvvBzURgC6M2.htm)|Quick Shot|Disparo rápido|oficial|
|[class-04-xXHwktc9SymSY8d6.htm](feats/class-04-xXHwktc9SymSY8d6.htm)|Healing Bomb|Bomba Curativa|oficial|
|[class-04-Y8LHfkzGyOhPlUou.htm](feats/class-04-Y8LHfkzGyOhPlUou.htm)|Twin Parry|Parada gemela|oficial|
|[class-04-YJIzE2RhGRGfbt9j.htm](feats/class-04-YJIzE2RhGRGfbt9j.htm)|Guardian's Deflection (Swashbuckler)|Desvío del guardián (Espadachín)|oficial|
|[class-04-ZHPSASbvbbshq1zG.htm](feats/class-04-ZHPSASbvbbshq1zG.htm)|Leading Dance|Primero en bailar|oficial|
|[class-06-0qGLCpggCcOVkbtT.htm](feats/class-06-0qGLCpggCcOVkbtT.htm)|Dodge Away|Esquivar hacia atrás|oficial|
|[class-06-0zSoSPwC4cpqRewj.htm](feats/class-06-0zSoSPwC4cpqRewj.htm)|Song Of Marching|Canción de marcha|oficial|
|[class-06-2HoDwBAmPIAoKUVF.htm](feats/class-06-2HoDwBAmPIAoKUVF.htm)|Dazing Blow|Golpe aontador|oficial|
|[class-06-3PHHiZjX16Dwyt65.htm](feats/class-06-3PHHiZjX16Dwyt65.htm)|Analyze Weakness|Analizar Debilidad|oficial|
|[class-06-515N9nl9ChZwLWKR.htm](feats/class-06-515N9nl9ChZwLWKR.htm)|Defensive Coordination|Coordinación defensiva|oficial|
|[class-06-52QyoGaysrfBOy5H.htm](feats/class-06-52QyoGaysrfBOy5H.htm)|Witch's Charge|Tutela del brujo|oficial|
|[class-06-5N6rLz4mdJg0NrQH.htm](feats/class-06-5N6rLz4mdJg0NrQH.htm)|Anticipate Ambush|Anticipar emboscada|oficial|
|[class-06-6iDd7CTzxkvMp6lB.htm](feats/class-06-6iDd7CTzxkvMp6lB.htm)|Align Ki|Alinear Ki|oficial|
|[class-06-7KT4huf0iPaBGD7R.htm](feats/class-06-7KT4huf0iPaBGD7R.htm)|Combination Finisher|Rematador combinado|oficial|
|[class-06-7RcCvziQBLL7Bumu.htm](feats/class-06-7RcCvziQBLL7Bumu.htm)|Advanced Bow Training|Entrenamiento en arco avanzado|oficial|
|[class-06-9cHQua33V35JPE3U.htm](feats/class-06-9cHQua33V35JPE3U.htm)|Educate Allies|Educar a los aliados|oficial|
|[class-06-9CXQhg4YprPhqzoL.htm](feats/class-06-9CXQhg4YprPhqzoL.htm)|Vexing Tumble|Pirueta exasperante|oficial|
|[class-06-ajesR7y0jWzqjAgc.htm](feats/class-06-ajesR7y0jWzqjAgc.htm)|Current Spell|Conjuro actual|oficial|
|[class-06-b7isszc8C75V3okn.htm](feats/class-06-b7isszc8C75V3okn.htm)|Sticky Poison|Veneno adhesivo|oficial|
|[class-06-Ba6SLqAghsZgqhua.htm](feats/class-06-Ba6SLqAghsZgqhua.htm)|Agile Maneuvers|Maniobras ágiles|oficial|
|[class-06-BBN5G6epRWXGwZHv.htm](feats/class-06-BBN5G6epRWXGwZHv.htm)|Ephemeral Tracking|Rastreo efímero|oficial|
|[class-06-bSXcyu7ExWq9qUzG.htm](feats/class-06-bSXcyu7ExWq9qUzG.htm)|Convincing Illusion|Ilusión convincente|oficial|
|[class-06-c6CS97Zs0DPmInaI.htm](feats/class-06-c6CS97Zs0DPmInaI.htm)|Assured Knowledge|Conocimiento asegurado|oficial|
|[class-06-Ea6Z5cxeBCCtPD5R.htm](feats/class-06-Ea6Z5cxeBCCtPD5R.htm)|Ranger's Bramble|Zarza del explorador|oficial|
|[class-06-FPVe3o7YctBicSQa.htm](feats/class-06-FPVe3o7YctBicSQa.htm)|Advanced Revelation|Revelación Avanzada|oficial|
|[class-06-fx50Ivl1ERxTijpT.htm](feats/class-06-fx50Ivl1ERxTijpT.htm)|Invigorating Mercy|Merced vigorizante|oficial|
|[class-06-gwIgB6bMh0sruyX7.htm](feats/class-06-gwIgB6bMh0sruyX7.htm)|Connect The Dots|Atar cabos|oficial|
|[class-06-HB0jvWCdim1p91q1.htm](feats/class-06-HB0jvWCdim1p91q1.htm)|Spiritual Sense|Sentido espiritual|oficial|
|[class-06-Ig431EeRy3FKMmMq.htm](feats/class-06-Ig431EeRy3FKMmMq.htm)|Keen Recollection|Memoria aguda|oficial|
|[class-06-jaAnxfXVmUQy0IKU.htm](feats/class-06-jaAnxfXVmUQy0IKU.htm)|One-Inch Punch|Puñetazo de una pulgada|oficial|
|[class-06-k0NNa5Ko4XhDdBYB.htm](feats/class-06-k0NNa5Ko4XhDdBYB.htm)|Nocturnal Sense|Sentido nocturno|oficial|
|[class-06-kYA6LkDw4AzKI156.htm](feats/class-06-kYA6LkDw4AzKI156.htm)|Stumbling Feint|Finta de borracho|oficial|
|[class-06-L1rCuwsCKWd9zlS3.htm](feats/class-06-L1rCuwsCKWd9zlS3.htm)|Advanced Deduction|Deducción avanzada|oficial|
|[class-06-lZ0swL9EEUgbAuaZ.htm](feats/class-06-lZ0swL9EEUgbAuaZ.htm)|Swashbuckler's Riposte|Réplica de espadachín|oficial|
|[class-06-mqLPCNdCSNyY7gyI.htm](feats/class-06-mqLPCNdCSNyY7gyI.htm)|Mage Hunter|Cazador de magos|oficial|
|[class-06-NMWXHGWUcZGoLDKb.htm](feats/class-06-NMWXHGWUcZGoLDKb.htm)|Attack of Opportunity|Ataque de oportunidad|oficial|
|[class-06-nRjyyDulHnP5OewA.htm](feats/class-06-nRjyyDulHnP5OewA.htm)|Gorilla Pound|Porrazo de gorila|oficial|
|[class-06-nU0r77AZXMXIlti6.htm](feats/class-06-nU0r77AZXMXIlti6.htm)|Additional Recollection|Recolección adicional|oficial|
|[class-06-oGOxrqT7DHI43SVk.htm](feats/class-06-oGOxrqT7DHI43SVk.htm)|Mounted Shield|Escudar montura|oficial|
|[class-06-OINfbwNZGnlyMqPR.htm](feats/class-06-OINfbwNZGnlyMqPR.htm)|Return Fire|Responder al fuego|oficial|
|[class-06-OY1Ewg0dbCp52Hl5.htm](feats/class-06-OY1Ewg0dbCp52Hl5.htm)|Inner Strength|Fuerza interior|oficial|
|[class-06-pIG5hWjZtzZJ3VOZ.htm](feats/class-06-pIG5hWjZtzZJ3VOZ.htm)|Advanced Witchcraft|Brujería avanzada|oficial|
|[class-06-PqZZSo06BH5N7x7C.htm](feats/class-06-PqZZSo06BH5N7x7C.htm)|Diverting Vortex|Vórtice desviador|oficial|
|[class-06-PxTRE0mFEO3tyt8h.htm](feats/class-06-PxTRE0mFEO3tyt8h.htm)|Advanced Mysteries|Misterios Avanzados|oficial|
|[class-06-qDfTqetM9UEpp8ty.htm](feats/class-06-qDfTqetM9UEpp8ty.htm)|Greater Lesson|Lección mayor|oficial|
|[class-06-qeLpqH2cMSmIrILV.htm](feats/class-06-qeLpqH2cMSmIrILV.htm)|Precise Finisher|Rematador preciso|oficial|
|[class-06-QSuwyX84U26OLzZI.htm](feats/class-06-QSuwyX84U26OLzZI.htm)|Predictive Purchase (Investigator)|Compra Predictiva (Investigador)|oficial|
|[class-06-sflJhnFzYfqZ2tDy.htm](feats/class-06-sflJhnFzYfqZ2tDy.htm)|Defend Mount|Defender montura|oficial|
|[class-06-SHhiLn0OSILEXNOj.htm](feats/class-06-SHhiLn0OSILEXNOj.htm)|Advanced Flair|Estilo avanzado|oficial|
|[class-06-Su4nbNnR0mjgusTT.htm](feats/class-06-Su4nbNnR0mjgusTT.htm)|Magic Hands|Manos mágicas|oficial|
|[class-06-T5xFirAE8VLL5Lbu.htm](feats/class-06-T5xFirAE8VLL5Lbu.htm)|Smite Good|Castigar el bien|oficial|
|[class-06-tRHjUCl0xqG97nok.htm](feats/class-06-tRHjUCl0xqG97nok.htm)|Ricochet Stance (Fighter)|Posición de rebote (Guerrero)|oficial|
|[class-06-uH3ZRkXPsXi1ChO2.htm](feats/class-06-uH3ZRkXPsXi1ChO2.htm)|Corrupted Shield|Escudo corrupto|oficial|
|[class-06-uiGsVmvRfujQQRlK.htm](feats/class-06-uiGsVmvRfujQQRlK.htm)|Steady Spellcasting|Lanzamiento de conjuros / Conjuros estables|oficial|
|[class-06-VqVgcqmG6xmYuDbK.htm](feats/class-06-VqVgcqmG6xmYuDbK.htm)|Scouring Rage|Furia implacable|oficial|
|[class-06-VYilg64xX9XpHeJr.htm](feats/class-06-VYilg64xX9XpHeJr.htm)|Far Throw|Lanzamiento a larga distancia|oficial|
|[class-06-WAx7RABHDvVVcRI8.htm](feats/class-06-WAx7RABHDvVVcRI8.htm)|Split Slot|Dividir espacio|oficial|
|[class-06-WjEwsu4kkexNvDcN.htm](feats/class-06-WjEwsu4kkexNvDcN.htm)|Thorough Research|Investigación minuciosa|oficial|
|[class-06-Xhphe5Lsa4kuU4RG.htm](feats/class-06-Xhphe5Lsa4kuU4RG.htm)|Crossbow Terror|Terror con la ballesta|oficial|
|[class-06-yUpZcrQHrz4mflKQ.htm](feats/class-06-yUpZcrQHrz4mflKQ.htm)|Energetic Resonance|Resonancia energética|oficial|
|[class-06-z5fUX9jeqfAViOd8.htm](feats/class-06-z5fUX9jeqfAViOd8.htm)|Shove Down|Derribar de un empujón|oficial|
|[class-06-zwEaXGKqnlBTllfE.htm](feats/class-06-zwEaXGKqnlBTllfE.htm)|Spell Relay|Relé de conjuros|oficial|
|[class-06-zZCyJhsaugHB6mZW.htm](feats/class-06-zZCyJhsaugHB6mZW.htm)|Watch Your Back|Ten cuidado|oficial|
|[class-08-0XGLdVbEIISOOuuO.htm](feats/class-08-0XGLdVbEIISOOuuO.htm)|Bullseye|Diana|oficial|
|[class-08-264KzmKMK4zqi6AR.htm](feats/class-08-264KzmKMK4zqi6AR.htm)|Clue Them All In|Pista para todos|oficial|
|[class-08-2MHzEh1KUQEbhjUf.htm](feats/class-08-2MHzEh1KUQEbhjUf.htm)|Vivacious Bravado|Bravuconada vivaz|oficial|
|[class-08-6KiB0SLYB1p8Th5U.htm](feats/class-08-6KiB0SLYB1p8Th5U.htm)|Safeguarded Spell|Conjuro de salvaguardia|oficial|
|[class-08-80DU0IvIzOIBGuUa.htm](feats/class-08-80DU0IvIzOIBGuUa.htm)|Loaner Spell|Conjuro de sustitución|oficial|
|[class-08-8CLbJAtgSfwxk2rk.htm](feats/class-08-8CLbJAtgSfwxk2rk.htm)|Murksight|Vista lúgubre|oficial|
|[class-08-9pTQrhbeF348bYky.htm](feats/class-08-9pTQrhbeF348bYky.htm)|Inspired Stratagem|Estratagema inspirada|oficial|
|[class-08-9Q0tPGtOawPTU2TU.htm](feats/class-08-9Q0tPGtOawPTU2TU.htm)|Follow-up Assault|Asalto reiterado|oficial|
|[class-08-AYBfwGImT28lUdue.htm](feats/class-08-AYBfwGImT28lUdue.htm)|Incredible Mount|Montura increíble|oficial|
|[class-08-bPqRneuJPqeXc65G.htm](feats/class-08-bPqRneuJPqeXc65G.htm)|Sense Good|Sentir el bien|oficial|
|[class-08-bYijGvCvCmJnW6aA.htm](feats/class-08-bYijGvCvCmJnW6aA.htm)|Sacrifice Armor|Sacrificar armadura|oficial|
|[class-08-dmXd68ilbuGR6eUP.htm](feats/class-08-dmXd68ilbuGR6eUP.htm)|Archaeologist's Luck|Suerte de arqueólogo|oficial|
|[class-08-dTO1ShJovbzrKUY4.htm](feats/class-08-dTO1ShJovbzrKUY4.htm)|Resounding Bravery|Valentía resonante|oficial|
|[class-08-dxujgA0NgiEvA0H8.htm](feats/class-08-dxujgA0NgiEvA0H8.htm)|Bleeding Finisher|Rematador sangrante|oficial|
|[class-08-DYc108IqRBP9N9W6.htm](feats/class-08-DYc108IqRBP9N9W6.htm)|Soulsight (Bard)|Visión del alma (Bardo)|oficial|
|[class-08-egmb8p3ZIYtx5aQN.htm](feats/class-08-egmb8p3ZIYtx5aQN.htm)|Archer's Aim|Puntería del arquero|oficial|
|[class-08-F6ZAceuDpiM9bUiF.htm](feats/class-08-F6ZAceuDpiM9bUiF.htm)|Nimble Roll|Rodar ágilmente|oficial|
|[class-08-fFfRsvDavUsTBDF2.htm](feats/class-08-fFfRsvDavUsTBDF2.htm)|Martyr|Mártir|oficial|
|[class-08-HLC9g1pwluDl6vy7.htm](feats/class-08-HLC9g1pwluDl6vy7.htm)|Read Disaster|Interpretar desastre|oficial|
|[class-08-iy9XKih5jIAdv67c.htm](feats/class-08-iy9XKih5jIAdv67c.htm)|Debilitating Dichotomy|Dicotomía debilitante|oficial|
|[class-08-j4zGMRiTi5t6guMF.htm](feats/class-08-j4zGMRiTi5t6guMF.htm)|Disorienting Opening|Apertura desorientadora|oficial|
|[class-08-jfsmtOx4QgXB19BL.htm](feats/class-08-jfsmtOx4QgXB19BL.htm)|Tumbling Strike|Golpe acrobático|oficial|
|[class-08-jkBzlMB4TS1sS2Fm.htm](feats/class-08-jkBzlMB4TS1sS2Fm.htm)|Stunning Finisher|Rematador aturdidor|oficial|
|[class-08-JOq4Xe49A04YycRz.htm](feats/class-08-JOq4Xe49A04YycRz.htm)|Call And Response|Llamada y respuesta|oficial|
|[class-08-l8KQgN8icNrzYIav.htm](feats/class-08-l8KQgN8icNrzYIav.htm)|Form Retention|Retención de forma|oficial|
|[class-08-LVTquA3DpqCJDika.htm](feats/class-08-LVTquA3DpqCJDika.htm)|Instinctive Strike|Golpe instintivo|oficial|
|[class-08-MhoGCLKI5zxQ4SFD.htm](feats/class-08-MhoGCLKI5zxQ4SFD.htm)|Tactical Entry|Entrada táctica|oficial|
|[class-08-MRxQDZFNPpUKC0CL.htm](feats/class-08-MRxQDZFNPpUKC0CL.htm)|Surging Focus|Oleada de foco|oficial|
|[class-08-Mvay7CiSN8snJ7DK.htm](feats/class-08-Mvay7CiSN8snJ7DK.htm)|Perpetual Breadth|Abundancia perpetua|oficial|
|[class-08-nEmaHLsZEBru1Jjv.htm](feats/class-08-nEmaHLsZEBru1Jjv.htm)|Courageous Opportunity|Oportunidad valerosa|oficial|
|[class-08-nL82Dzh0QwkNkJDA.htm](feats/class-08-nL82Dzh0QwkNkJDA.htm)|Disarming Assault|Asalto desarmante|oficial|
|[class-08-oEjRfI4ATIFxDCzL.htm](feats/class-08-oEjRfI4ATIFxDCzL.htm)|Witch's Bottle|Botella del brujo|oficial|
|[class-08-oTTddwzF9TPNkMyd.htm](feats/class-08-oTTddwzF9TPNkMyd.htm)|Accompany|Acompañamiento|oficial|
|[class-08-PP1gfRCc1YwnQGxp.htm](feats/class-08-PP1gfRCc1YwnQGxp.htm)|Dual Finisher|Rematador doble|oficial|
|[class-08-PTXZ2C3AV8tZf0iX.htm](feats/class-08-PTXZ2C3AV8tZf0iX.htm)|Deeper Dabbler|Aficionado profundo|oficial|
|[class-08-RsNvCSrCN7czHC0G.htm](feats/class-08-RsNvCSrCN7czHC0G.htm)|Ricochet Stance (Rogue)|Posición de rebote (Pícaro)|oficial|
|[class-08-RU86cGTryRAdaEqx.htm](feats/class-08-RU86cGTryRAdaEqx.htm)|Swashbuckler's Speed|Velocidad del espadachín|oficial|
|[class-08-tPb0FVkNDE89ACbC.htm](feats/class-08-tPb0FVkNDE89ACbC.htm)|Incredible Familiar (Witch)|Familiar increíble (Brujo)|oficial|
|[class-08-Tr2SnOE2WqFIIWIK.htm](feats/class-08-Tr2SnOE2WqFIIWIK.htm)|Enlarge Companion|Agrandar compañero|oficial|
|[class-08-u2rvvAqZBugZgcYg.htm](feats/class-08-u2rvvAqZBugZgcYg.htm)|Greater Cruelty|Crueldad mayor|oficial|
|[class-08-UFVw57jWNC4UCfyN.htm](feats/class-08-UFVw57jWNC4UCfyN.htm)|Deimatic Display|Exhibición deimática|oficial|
|[class-08-v4O6eDiSOkzQZHmT.htm](feats/class-08-v4O6eDiSOkzQZHmT.htm)|Skill Mastery (Investigator)|Maestría en habilidades (Investigador)|oficial|
|[class-08-V9kShXu84NlORfcg.htm](feats/class-08-V9kShXu84NlORfcg.htm)|Friendly Toss|Lanzamiento amistoso|oficial|
|[class-08-Wz0LLKjEi8GfKloV.htm](feats/class-08-Wz0LLKjEi8GfKloV.htm)|Patron's Breadth|Abundancia de patrón|oficial|
|[class-08-XtIPmZ3Ihq5NJHP2.htm](feats/class-08-XtIPmZ3Ihq5NJHP2.htm)|Pinpoint Poisoner|Envenenador preciso|oficial|
|[class-08-y2XeMe1F18lIyo59.htm](feats/class-08-y2XeMe1F18lIyo59.htm)|Blind-Fight|Lucha a ciegas|oficial|
|[class-08-y4Cws9vZj3Bf9uqH.htm](feats/class-08-y4Cws9vZj3Bf9uqH.htm)|Mysterious Breadth|Abundancia misteriosa|oficial|
|[class-08-YGBPIpHaOgCsa2qO.htm](feats/class-08-YGBPIpHaOgCsa2qO.htm)|Soulsight (Sorcerer)|Visión del alma (Hechicero)|oficial|
|[class-08-yTUIiE9LXBZaA7aG.htm](feats/class-08-yTUIiE9LXBZaA7aG.htm)|Whodunnit?|¿Quién es el culpable?|oficial|
|[class-08-Yw0qVCDu94Y5TgxQ.htm](feats/class-08-Yw0qVCDu94Y5TgxQ.htm)|Predictive Purchase (Rogue)|Compra Predictiva (Pícaro)|oficial|
|[class-10-39RJF47FLYr5gZ8p.htm](feats/class-10-39RJF47FLYr5gZ8p.htm)|Unseat|Desmontar jinete|oficial|
|[class-10-4IeAAmx2vZEHmRLX.htm](feats/class-10-4IeAAmx2vZEHmRLX.htm)|Witch's Communion|Comunión del brujo|oficial|
|[class-10-647GZOyhWgcjw6Jg.htm](feats/class-10-647GZOyhWgcjw6Jg.htm)|Elucidating Mercy|Elucidando la Merced|oficial|
|[class-10-6xBu4BewIkOIt9M0.htm](feats/class-10-6xBu4BewIkOIt9M0.htm)|Cut From The Air|Corte al vuelo|oficial|
|[class-10-9kY9B5WgtEleOicn.htm](feats/class-10-9kY9B5WgtEleOicn.htm)|Resounding Blow|Golpe resonante|oficial|
|[class-10-9VGmE7X4aK2W8YWj.htm](feats/class-10-9VGmE7X4aK2W8YWj.htm)|Dueling Dance (Swashbuckler)|Baile de duelo (Espadachín)|oficial|
|[class-10-Asb0UsQqeATsxqFJ.htm](feats/class-10-Asb0UsQqeATsxqFJ.htm)|Courageous Assault|Asalto valeroso|oficial|
|[class-10-C0ozuEhrKh9A1wMO.htm](feats/class-10-C0ozuEhrKh9A1wMO.htm)|Overpowering Charge|Carga Abrumadora|oficial|
|[class-10-CAk1NNG4aO0VuHnZ.htm](feats/class-10-CAk1NNG4aO0VuHnZ.htm)|Ongoing Strategy|Estrategia en curso|oficial|
|[class-10-CkK7WwaWnrLXK9sW.htm](feats/class-10-CkK7WwaWnrLXK9sW.htm)|Methodical Debilitations|Debilitaciónes metódicas|oficial|
|[class-10-cvo3DIL0BIRrDkQ6.htm](feats/class-10-cvo3DIL0BIRrDkQ6.htm)|Ode To Ouroboros|Oda al Uróboro|oficial|
|[class-10-DLkMoVb8qb4qxnx3.htm](feats/class-10-DLkMoVb8qb4qxnx3.htm)|Targeting Finisher|Rematador de puntería|oficial|
|[class-10-ErKwliHplziJY2BW.htm](feats/class-10-ErKwliHplziJY2BW.htm)|Greater Magical Scholastics|Escolástica mágica mayor|oficial|
|[class-10-FOk8xTCHcHYyENu2.htm](feats/class-10-FOk8xTCHcHYyENu2.htm)|Bat Form|Forma de murciélago|oficial|
|[class-10-Gcliatty0MGYbTVV.htm](feats/class-10-Gcliatty0MGYbTVV.htm)|Oracular Warning|Advertencia oracular|oficial|
|[class-10-GLbl3qoWCvvjJr4S.htm](feats/class-10-GLbl3qoWCvvjJr4S.htm)|Tangle Of Battle|Maraña de batalla|oficial|
|[class-10-H0tffYs7ODMQ3JJj.htm](feats/class-10-H0tffYs7ODMQ3JJj.htm)|Terrain Transposition|Transposición del terreno|oficial|
|[class-10-kUv9eiP8Zhck70WZ.htm](feats/class-10-kUv9eiP8Zhck70WZ.htm)|Eldritch Debilitations|Debilitaciónes sobrenaturales|oficial|
|[class-10-lG4dYrnkE42IgnGG.htm](feats/class-10-lG4dYrnkE42IgnGG.htm)|Energy Ward|Custodio de energía|oficial|
|[class-10-MgqRwyL8PWyYvoZs.htm](feats/class-10-MgqRwyL8PWyYvoZs.htm)|Energy Fusion|Fusión de energía|oficial|
|[class-10-nWxNV9pFeBHV671W.htm](feats/class-10-nWxNV9pFeBHV671W.htm)|Pristine Weapon|Arma prístina|oficial|
|[class-10-PIVC14saumGNKWbo.htm](feats/class-10-PIVC14saumGNKWbo.htm)|Annotate Composition|Anotar Composición|oficial|
|[class-10-qKpaZF0U9VV0YwTJ.htm](feats/class-10-qKpaZF0U9VV0YwTJ.htm)|Litany Of Self-Interest|Letanía del egoísmo|oficial|
|[class-10-QO8l5Dao8HnaFQE4.htm](feats/class-10-QO8l5Dao8HnaFQE4.htm)|Impressive Landing|Aterrizaje Impresionante|oficial|
|[class-10-R40U8hF0hWyRUze8.htm](feats/class-10-R40U8hF0hWyRUze8.htm)|Through Spell|Conjuro traspasante|oficial|
|[class-10-Sc9clbAXe97vlzxM.htm](feats/class-10-Sc9clbAXe97vlzxM.htm)|Suspect of Opportunity|Sospechoso de la oportunidad|oficial|
|[class-10-SlMkuKMny7hWdNxL.htm](feats/class-10-SlMkuKMny7hWdNxL.htm)|Derring-do|Audacia|oficial|
|[class-10-SY6bU7DOyfs22cJX.htm](feats/class-10-SY6bU7DOyfs22cJX.htm)|Temporary Potions|Pociones Temporales|oficial|
|[class-10-t6sey3cyV8n7a78l.htm](feats/class-10-t6sey3cyV8n7a78l.htm)|Trampling Charge|Carga arrolladora|oficial|
|[class-10-tCuMXQ0yMrCNwzqW.htm](feats/class-10-tCuMXQ0yMrCNwzqW.htm)|Resilient Touch|Toque resistente|oficial|
|[class-10-tDWc2LQNl0Op1Auq.htm](feats/class-10-tDWc2LQNl0Op1Auq.htm)|Buckler Dance|Danza de rodela|oficial|
|[class-10-tY3Zg0l14CdoKPpt.htm](feats/class-10-tY3Zg0l14CdoKPpt.htm)|Unstable Concoction|Brebaje inestable|oficial|
|[class-10-u2fgdFIdQDplKOS3.htm](feats/class-10-u2fgdFIdQDplKOS3.htm)|Peafowl Strut|Pavoneo de pavo real|oficial|
|[class-10-UjYHf7rlWTFJ0v0A.htm](feats/class-10-UjYHf7rlWTFJ0v0A.htm)|Signature Spell Expansion|Expansión de conjuros de signatura|oficial|
|[class-10-uotQ9yqetPoAWrfW.htm](feats/class-10-uotQ9yqetPoAWrfW.htm)|Reflexive Riposte|Réplica refleja|oficial|
|[class-10-vgsMKjAbRDNxT5TK.htm](feats/class-10-vgsMKjAbRDNxT5TK.htm)|Determined Dash|Carrera determinada|oficial|
|[class-10-VJl2xHDKr0HxTUrs.htm](feats/class-10-VJl2xHDKr0HxTUrs.htm)|Symphony Of The Unfettered Heart|Sinfonía del coracón indómito|oficial|
|[class-10-w3qyriA1YnzXaas3.htm](feats/class-10-w3qyriA1YnzXaas3.htm)|Shield Of Faith|Escudo de la Fe|oficial|
|[class-10-W7aT1UJOVFkYdQti.htm](feats/class-10-W7aT1UJOVFkYdQti.htm)|Hunter's Vision|Visión del cazador|oficial|
|[class-10-wIJC00ODLq9WYc1m.htm](feats/class-10-wIJC00ODLq9WYc1m.htm)|Shared Avoidance|Evitación compartida|oficial|
|[class-10-xQuNswWB3eg1UM28.htm](feats/class-10-xQuNswWB3eg1UM28.htm)|Cobra Envenom|Veneno de cobra|oficial|
|[class-10-Xw7qG0SHepXx24vl.htm](feats/class-10-Xw7qG0SHepXx24vl.htm)|Prevailing Position|Posición predominante|oficial|
|[class-10-Yazy4gex46FLwsph.htm](feats/class-10-Yazy4gex46FLwsph.htm)|Tumbling Opportunist|Oportunista acrobático|oficial|
|[class-10-Yk3QGpalWDn5MhBV.htm](feats/class-10-Yk3QGpalWDn5MhBV.htm)|Silencing Strike|Golpe Silenciador|oficial|
|[class-10-YluQPhevo0LKdF1p.htm](feats/class-10-YluQPhevo0LKdF1p.htm)|Just One More Thing|Sólo una cosa más|oficial|
|[class-10-YNk0BekymS3bBvCT.htm](feats/class-10-YNk0BekymS3bBvCT.htm)|Ancestral Mage|Mago Ancestral|oficial|
|[class-10-YV4X9u5Yuf0xvoCh.htm](feats/class-10-YV4X9u5Yuf0xvoCh.htm)|Surging Might|Poder creciente|oficial|
|[class-10-z2ptq23nNBOeEI7H.htm](feats/class-10-z2ptq23nNBOeEI7H.htm)|Quickened Casting|Lanzamiento apresurado|oficial|
|[class-10-ZFkCMl63ogK55Otq.htm](feats/class-10-ZFkCMl63ogK55Otq.htm)|Major Lesson|Lección superior|oficial|
|[class-12-2aFtxqRPnC4OXUGC.htm](feats/class-12-2aFtxqRPnC4OXUGC.htm)|Warden's Focus|Foco del guardián|oficial|
|[class-12-3WUL8ExEkZDRYeBu.htm](feats/class-12-3WUL8ExEkZDRYeBu.htm)|Diviner Sense|Sentido adivinación|oficial|
|[class-12-8cbSVw8RnVzy5USe.htm](feats/class-12-8cbSVw8RnVzy5USe.htm)|Enigma's Knowledge|Conocimiento del enigma|oficial|
|[class-12-8INrcMUv5vzWMG3X.htm](feats/class-12-8INrcMUv5vzWMG3X.htm)|Sunder Spell|Romper conjuro|oficial|
|[class-12-AXy4A7zTYk1JAiOV.htm](feats/class-12-AXy4A7zTYk1JAiOV.htm)|Magic Sense|Sentido mágico|oficial|
|[class-12-BJfIGuUMItalNYet.htm](feats/class-12-BJfIGuUMItalNYet.htm)|Evasiveness (Swashbuckler)|Evasividad (Espadachín)|oficial|
|[class-12-BlfgmJHjDyTVGdPs.htm](feats/class-12-BlfgmJHjDyTVGdPs.htm)|Wandering Oasis|Oasis errante|oficial|
|[class-12-Cy5W8U4yN9P1EvBy.htm](feats/class-12-Cy5W8U4yN9P1EvBy.htm)|Amplifying Touch|Toque Amplificador|oficial|
|[class-12-D2KSVHPRlBEibrV8.htm](feats/class-12-D2KSVHPRlBEibrV8.htm)|Cheat Death|Engañar a la muerte|oficial|
|[class-12-d7DQhCJKYcLxpHen.htm](feats/class-12-d7DQhCJKYcLxpHen.htm)|Focused Shot|Disparo enfocado|oficial|
|[class-12-dLof0i6LPcChJSBR.htm](feats/class-12-dLof0i6LPcChJSBR.htm)|Pale Horse|Caballo Pálido|oficial|
|[class-12-Dwxi1q1OWB1ufFvy.htm](feats/class-12-Dwxi1q1OWB1ufFvy.htm)|Overwhelming Breath|Aliento abrumador|oficial|
|[class-12-ecV3Nljvs4FOBS27.htm](feats/class-12-ecV3Nljvs4FOBS27.htm)|Reverberate|Reverberar|oficial|
|[class-12-ENoRkTXtdfsbs98S.htm](feats/class-12-ENoRkTXtdfsbs98S.htm)|Domain Fluency|Fluidez de dominio|oficial|
|[class-12-FtO8DjjMLBtWiRhZ.htm](feats/class-12-FtO8DjjMLBtWiRhZ.htm)|Expert Oracle Spellcasting|Lanzamiento de conjuros experto de oráculo|oficial|
|[class-12-gVLICIDQMvWN5D89.htm](feats/class-12-gVLICIDQMvWN5D89.htm)|Greater Spiritual Evolution|Evolución espiritual mayor|oficial|
|[class-12-HSW3N9pfHhM7upRB.htm](feats/class-12-HSW3N9pfHhM7upRB.htm)|Greater Revelation|Revelación mayor|oficial|
|[class-12-j20djiiuVwUf8MqL.htm](feats/class-12-j20djiiuVwUf8MqL.htm)|Embrace The Pain|Abrazar al dolor|oficial|
|[class-12-nBWoZ311FXFJC8Zl.htm](feats/class-12-nBWoZ311FXFJC8Zl.htm)|Dodging Roll|Voltereta de esquiva|oficial|
|[class-12-O1qdoz5N3G4yvHcH.htm](feats/class-12-O1qdoz5N3G4yvHcH.htm)|Greater Physical Evolution|Evolución física mayor|oficial|
|[class-12-oUcB71V1jVaM8SFx.htm](feats/class-12-oUcB71V1jVaM8SFx.htm)|Shared Assault|Asalto compartido|oficial|
|[class-12-p2tFR4yBauu8t3mC.htm](feats/class-12-p2tFR4yBauu8t3mC.htm)|Hex Focus|Soltura con maleficios|oficial|
|[class-12-PEszRpnrcB7VPS9G.htm](feats/class-12-PEszRpnrcB7VPS9G.htm)|Gruesome Strike|Golpe espantoso|oficial|
|[class-12-pm9PS32YNLJ2wp4o.htm](feats/class-12-pm9PS32YNLJ2wp4o.htm)|Reason Rapidly|Razona rápidamente|oficial|
|[class-12-pmz1itHp13JtcrjW.htm](feats/class-12-pmz1itHp13JtcrjW.htm)|Unbalancing Sweep|Barrido desequilibrante|oficial|
|[class-12-pVDgiaqu1RbCOhuv.htm](feats/class-12-pVDgiaqu1RbCOhuv.htm)|Foresee Danger|Prever el peligro|oficial|
|[class-12-Px1QZY0NdO9WAQQS.htm](feats/class-12-Px1QZY0NdO9WAQQS.htm)|Mobile Finisher|Rematador móvil|oficial|
|[class-12-Q4puGx4kBMXy45fa.htm](feats/class-12-Q4puGx4kBMXy45fa.htm)|Familiar's Eyes|Ojos del familiar|oficial|
|[class-12-QicYF43HqgpOBLzo.htm](feats/class-12-QicYF43HqgpOBLzo.htm)|Bloody Debilitation|Debilitación sangrienta|oficial|
|[class-12-t8CAK8ylu23PUxbn.htm](feats/class-12-t8CAK8ylu23PUxbn.htm)|Master Spotter (Investigator)|Avistador maestro (Investigador)|oficial|
|[class-12-UJafwv306v75Syy7.htm](feats/class-12-UJafwv306v75Syy7.htm)|Forcible Energy|Energía Forzosa|oficial|
|[class-12-va7YMidXZW21oFwA.htm](feats/class-12-va7YMidXZW21oFwA.htm)|Blood Component Substitution|Sustitución de componentes de sangre|oficial|
|[class-12-VIjI8PtkTFjeAA6a.htm](feats/class-12-VIjI8PtkTFjeAA6a.htm)|Ricochet Feint|Finta de rebote|oficial|
|[class-12-x7vMKBSrxXmfs5C2.htm](feats/class-12-x7vMKBSrxXmfs5C2.htm)|Expert Witch Spellcasting|Lanzador de conjuros experto de brujo|oficial|
|[class-12-YZ138OqflDhrkqmR.htm](feats/class-12-YZ138OqflDhrkqmR.htm)|Enforce Oath|Imponer juramento|oficial|
|[class-12-zzMugLCUkQQPa2qT.htm](feats/class-12-zzMugLCUkQQPa2qT.htm)|Preparation|Preparación|oficial|
|[class-14-0EY2WQC3Hb6Mitgz.htm](feats/class-14-0EY2WQC3Hb6Mitgz.htm)|Form Lock|Bloqueo de forma|oficial|
|[class-14-2h8a6pKhXTXwpJjP.htm](feats/class-14-2h8a6pKhXTXwpJjP.htm)|Premonition Of Clarity|Premonición de claridad|oficial|
|[class-14-2KBKXkRthBXpw48X.htm](feats/class-14-2KBKXkRthBXpw48X.htm)|Stay Down!|¡No te levantes!|oficial|
|[class-14-4Q9Q41KLPYJMdV4b.htm](feats/class-14-4Q9Q41KLPYJMdV4b.htm)|Reactive Transformation|Transformación reactiva|oficial|
|[class-14-4SKech3n0F38HrR5.htm](feats/class-14-4SKech3n0F38HrR5.htm)|Vigorous Inspiration|Inspiración vigorosa|oficial|
|[class-14-AfTMuAln2f0Pa3Lj.htm](feats/class-14-AfTMuAln2f0Pa3Lj.htm)|Shadow's Web|Maraña sombría|oficial|
|[class-14-AOLf6QX068LR9L9e.htm](feats/class-14-AOLf6QX068LR9L9e.htm)|Earworm|Gusano auditivo|oficial|
|[class-14-BuaTJxALqxM5EZav.htm](feats/class-14-BuaTJxALqxM5EZav.htm)|Perfect Finisher|Rematador perfecto|oficial|
|[class-14-d8yggbcJsKKyHip7.htm](feats/class-14-d8yggbcJsKKyHip7.htm)|Consume Spell|Consumir conjuro|oficial|
|[class-14-DM9rjXZrmx2MFX7k.htm](feats/class-14-DM9rjXZrmx2MFX7k.htm)|Sense The Unseen|Sentir lo que no ves|oficial|
|[class-14-DT1O80hPD7MX6oWp.htm](feats/class-14-DT1O80hPD7MX6oWp.htm)|Spell Shroud|Sudario de conjuro|oficial|
|[class-14-Ek3nCIFRreqnSxAQ.htm](feats/class-14-Ek3nCIFRreqnSxAQ.htm)|Mysterious Repertoire|Repertorio misterioso|oficial|
|[class-14-guSjEQS3WuXJqQxf.htm](feats/class-14-guSjEQS3WuXJqQxf.htm)|Impaling Thrust|Acometida empaladora|oficial|
|[class-14-HquaVwjOLSPzcJgB.htm](feats/class-14-HquaVwjOLSPzcJgB.htm)|Impossible Riposte|Réplica imposible|oficial|
|[class-14-IaiEZaA8erufMUCr.htm](feats/class-14-IaiEZaA8erufMUCr.htm)|Whirling Blade Stance|Posición de la hoja giratoria|oficial|
|[class-14-k72W0qMXsX5ekJTF.htm](feats/class-14-k72W0qMXsX5ekJTF.htm)|Reflect Spell|Reflejar conjuro|oficial|
|[class-14-kTRGAST9J9ZxJZ4A.htm](feats/class-14-kTRGAST9J9ZxJZ4A.htm)|Twinned Defense (Swashbuckler)|Defensa gemela (Espadachín)|oficial|
|[class-14-N7CM5CmHuZ1cylV9.htm](feats/class-14-N7CM5CmHuZ1cylV9.htm)|Plot The Future|Planear el futuro|oficial|
|[class-14-NgUB5toKxBd8RJmm.htm](feats/class-14-NgUB5toKxBd8RJmm.htm)|Strategic Bypass|Derivación estratégica|oficial|
|[class-14-NNeRv9Gcua1kMp4s.htm](feats/class-14-NNeRv9Gcua1kMp4s.htm)|Forestall Curse|Prevenir maldición|oficial|
|[class-14-RHLfM9NlIlHTH85w.htm](feats/class-14-RHLfM9NlIlHTH85w.htm)|Triumphant Inspiration|Inspiración triunfante|oficial|
|[class-14-RpXWOgLWQLGdx74I.htm](feats/class-14-RpXWOgLWQLGdx74I.htm)|Sunder Enchantment|Romper encantamiento|oficial|
|[class-14-TdwC9rTGgtF4CQ25.htm](feats/class-14-TdwC9rTGgtF4CQ25.htm)|Flamboyant Leap|Salto extravagante|oficial|
|[class-14-TyWFsX9DliAdAVs8.htm](feats/class-14-TyWFsX9DliAdAVs8.htm)|Sow Spell|Sembrar conjuro|oficial|
|[class-14-uPikeCzrTrgzEJT8.htm](feats/class-14-uPikeCzrTrgzEJT8.htm)|Talismanic Sage|Sabio de los talismanes|oficial|
|[class-14-wa9ZGBTlFuwOjPpH.htm](feats/class-14-wa9ZGBTlFuwOjPpH.htm)|Rites Of Transfiguration|Ritos de transfiguración|oficial|
|[class-14-WEUFs37Ids3ZRrqa.htm](feats/class-14-WEUFs37Ids3ZRrqa.htm)|Litany Of Depravity|Letanía de depravación|oficial|
|[class-14-wijzB1FDUT7SC86a.htm](feats/class-14-wijzB1FDUT7SC86a.htm)|Specialized Mount|Montura especializada|oficial|
|[class-14-xSJaOcdhqDF1CBs3.htm](feats/class-14-xSJaOcdhqDF1CBs3.htm)|Ebb And Flow|Flujo y reflujo|oficial|
|[class-14-ygPrwqiyDr1frUHw.htm](feats/class-14-ygPrwqiyDr1frUHw.htm)|Greater Interpose|Interposición mayor|oficial|
|[class-14-z7kwVNaCB4oJs3Fe.htm](feats/class-14-z7kwVNaCB4oJs3Fe.htm)|Aura Of Preservation|Aura de conservación|oficial|
|[class-16-2HeRmbcHcsRMccir.htm](feats/class-16-2HeRmbcHcsRMccir.htm)|Diverse Mystery|Misterio variado|oficial|
|[class-16-4caP26xpkQajkaDp.htm](feats/class-16-4caP26xpkQajkaDp.htm)|One-Millimeter Punch|Puñetazo de un milímetro|oficial|
|[class-16-6fojN9yBJByTZ1Q9.htm](feats/class-16-6fojN9yBJByTZ1Q9.htm)|Didactic Strike|Golpe Didáctico|oficial|
|[class-16-8C9qo5LL9J0UTNGc.htm](feats/class-16-8C9qo5LL9J0UTNGc.htm)|Courageous Onslaught|Acometida valerosa|oficial|
|[class-16-Bp02C07s4RTS4vsV.htm](feats/class-16-Bp02C07s4RTS4vsV.htm)|Reconstruct The Scene|Reconstruir La escena|oficial|
|[class-16-D71A2ZQfz9MVndqI.htm](feats/class-16-D71A2ZQfz9MVndqI.htm)|Felicitous Riposte|Replica oportuna|oficial|
|[class-16-DAFF7zJphhDPcAws.htm](feats/class-16-DAFF7zJphhDPcAws.htm)|Medusa's Wrath|Ira de la medusa|oficial|
|[class-16-DSmYJvCHMvZCP0aD.htm](feats/class-16-DSmYJvCHMvZCP0aD.htm)|Scintillating Spell|Hechizo Centelleante|oficial|
|[class-16-e8ChYAaQ9Aa8aZES.htm](feats/class-16-e8ChYAaQ9Aa8aZES.htm)|Deadly Grace|Gracia letal|oficial|
|[class-16-eOwjwVAbl99ZTy5D.htm](feats/class-16-eOwjwVAbl99ZTy5D.htm)|Overwhelming Blow|Golpe apabullante|oficial|
|[class-16-H5uZqYVClk3s62ce.htm](feats/class-16-H5uZqYVClk3s62ce.htm)|Remediate|Remediar|oficial|
|[class-16-H7Ocx80td7Sx7Cqn.htm](feats/class-16-H7Ocx80td7Sx7Cqn.htm)|Expand Aura|Extender aura|oficial|
|[class-16-i98NcWSAbWmNmBik.htm](feats/class-16-i98NcWSAbWmNmBik.htm)|Instrument Of Slaughter|Instrumento De Matanza|oficial|
|[class-16-IA8p9oYmsCbipmhw.htm](feats/class-16-IA8p9oYmsCbipmhw.htm)|Chemical Contagion|Contagio químico|oficial|
|[class-16-L0f3c0DkT7FLQF9W.htm](feats/class-16-L0f3c0DkT7FLQF9W.htm)|Siphon Power|Succionar poder|oficial|
|[class-16-lhSqWHXK1JShUabF.htm](feats/class-16-lhSqWHXK1JShUabF.htm)|Resounding Finale|Final resonante|oficial|
|[class-16-Oyml3OGNy468z3XI.htm](feats/class-16-Oyml3OGNy468z3XI.htm)|Furious Vengeance|Venganza furiosa|oficial|
|[class-16-rgs6OZJYCgi5At8J.htm](feats/class-16-rgs6OZJYCgi5At8J.htm)|Effortless Concentration|Concentración sin esfuerzo|oficial|
|[class-16-RL7faGkymMFLAqTU.htm](feats/class-16-RL7faGkymMFLAqTU.htm)|Shattering Blows|Golpes demoledores|oficial|
|[class-16-RMSVWMFoLUk0P1cC.htm](feats/class-16-RMSVWMFoLUk0P1cC.htm)|Terraforming Spell|Hechizo terraformación|oficial|
|[class-16-RqccUKf8DCPnsYXJ.htm](feats/class-16-RqccUKf8DCPnsYXJ.htm)|Implausible Purchase (Investigator)|Compra Inverosímil (Investigador)|oficial|
|[class-16-rRbMOxX1QTHIIwAi.htm](feats/class-16-rRbMOxX1QTHIIwAi.htm)|Portentous Spell|Conjuro portentoso|oficial|
|[class-16-tBMMKWYfzD2OMD30.htm](feats/class-16-tBMMKWYfzD2OMD30.htm)|Penetrating Projectile|Proyectil perforante|oficial|
|[class-16-TqODgJL1VS3eokhX.htm](feats/class-16-TqODgJL1VS3eokhX.htm)|Swift Elusion|Elusión rápida|oficial|
|[class-16-uL6q4wtwvuP8I4po.htm](feats/class-16-uL6q4wtwvuP8I4po.htm)|Flinging Blow|Golpe lanzador|oficial|
|[class-16-xjwlP306nuda2z03.htm](feats/class-16-xjwlP306nuda2z03.htm)|Steal Spell|Robar conjuro|oficial|
|[class-18-62glnJI2o0KnHULB.htm](feats/class-18-62glnJI2o0KnHULB.htm)|Triangle Shot|Disparo triangular|oficial|
|[class-18-7ATVpDUM6pRq6HOR.htm](feats/class-18-7ATVpDUM6pRq6HOR.htm)|Smash From The Air|Desbaratar al vuelo|oficial|
|[class-18-8JMOgtB3XG7o6ffW.htm](feats/class-18-8JMOgtB3XG7o6ffW.htm)|Discordant Voice|Voz Discordante|oficial|
|[class-18-AbjVJIBdNjbQbVnV.htm](feats/class-18-AbjVJIBdNjbQbVnV.htm)|Primal Aegis|Égida primigenia|oficial|
|[class-18-gtXy5gMNU0NDvkBL.htm](feats/class-18-gtXy5gMNU0NDvkBL.htm)|Fiendish Form|Forma infernal|oficial|
|[class-18-i7ibUqJCl1GXRFEa.htm](feats/class-18-i7ibUqJCl1GXRFEa.htm)|Master Witch Spellcasting|Lanzador de conjuros brujo maestro|oficial|
|[class-18-KpF7RenGBXIMMGPX.htm](feats/class-18-KpF7RenGBXIMMGPX.htm)|Split Hex|Dividir maleficio|oficial|
|[class-18-LKxAuH0mLyzNygIY.htm](feats/class-18-LKxAuH0mLyzNygIY.htm)|Hex Wellspring|Manantial de maleficios|oficial|
|[class-18-nLidn7L2z61Ktjzk.htm](feats/class-18-nLidn7L2z61Ktjzk.htm)|Blaze Of Revelation|Destello de revelación|oficial|
|[class-18-NvEYf0jIETEu2LtP.htm](feats/class-18-NvEYf0jIETEu2LtP.htm)|Echoing Spell|Conjuro con eco|oficial|
|[class-18-oYFsw3LdgsN2QKVs.htm](feats/class-18-oYFsw3LdgsN2QKVs.htm)|Ki Form|Forma de Ki|oficial|
|[class-18-POrE3ZgBRdBL9MsW.htm](feats/class-18-POrE3ZgBRdBL9MsW.htm)|Trickster's Ace|As del embaucamiento|oficial|
|[class-18-t1sM6Xj9T07fqpwN.htm](feats/class-18-t1sM6Xj9T07fqpwN.htm)|Divine Effusion|Efusión Divina|oficial|
|[class-18-uAdnQZSkUuxcpEwz.htm](feats/class-18-uAdnQZSkUuxcpEwz.htm)|Incredible Luck (Swashbuckler)|Suerte increíble (Espadachín)|oficial|
|[class-18-uIL1pbp7jhYMjYLS.htm](feats/class-18-uIL1pbp7jhYMjYLS.htm)|Parry And Riposte|Parada y réplica|oficial|
|[class-18-wBCt1wcgrjduwHbi.htm](feats/class-18-wBCt1wcgrjduwHbi.htm)|Warden's Wellspring|Manantial del guardián|oficial|
|[class-18-wIRZDXOacqWfI670.htm](feats/class-18-wIRZDXOacqWfI670.htm)|Ki Center|Centro Ki|oficial|
|[class-18-wqAdzjRUOvTpKFKq.htm](feats/class-18-wqAdzjRUOvTpKFKq.htm)|All In My Head|Imaginaciones mías|oficial|
|[class-18-XafnXTx9Bn0kN1RG.htm](feats/class-18-XafnXTx9Bn0kN1RG.htm)|Second Chance Spell|Conjuro de segunda oportunidad|oficial|
|[class-18-XfCPAoNdF2XMnH7K.htm](feats/class-18-XfCPAoNdF2XMnH7K.htm)|Lethal Finisher|Rematador letal|oficial|
|[class-18-xZrTjUub7V09sXZF.htm](feats/class-18-xZrTjUub7V09sXZF.htm)|Rejuvenating Touch|Toque revitalizador|oficial|
|[class-18-ybrx1nsg5J0L8d3j.htm](feats/class-18-ybrx1nsg5J0L8d3j.htm)|Shared Clarity|Claridad Compartida|oficial|
|[class-18-YWRdk9oGMY6WmgIC.htm](feats/class-18-YWRdk9oGMY6WmgIC.htm)|Lead Investigator|Investigador principal|oficial|
|[class-18-zCASpQconMmtJKQN.htm](feats/class-18-zCASpQconMmtJKQN.htm)|Implausible Purchase (Rogue)|Compra inverosímil (Pícaro)|oficial|
|[class-18-ZkGhDEpqe7fzJuSr.htm](feats/class-18-ZkGhDEpqe7fzJuSr.htm)|Master Oracle Spellcasting|Lanzamiento de conjuros maestro de oráculo|oficial|
|[class-20-5H3Sk1yhalQQzUys.htm](feats/class-20-5H3Sk1yhalQQzUys.htm)|Hex Master|Maestro con los maleficios|oficial|
|[class-20-84ML8enTqOOdXA8O.htm](feats/class-20-84ML8enTqOOdXA8O.htm)|Accurate Flurry|Ráfaga precisa|oficial|
|[class-20-eIW5p8qqvsx2MFkY.htm](feats/class-20-eIW5p8qqvsx2MFkY.htm)|Mystery Conduit|Conducto misterioso|oficial|
|[class-20-epNrbgmjZDjJe7Ry.htm](feats/class-20-epNrbgmjZDjJe7Ry.htm)|Legendary Rider|Jinete Legendario|oficial|
|[class-20-fMAUsLdBd5SDoHBz.htm](feats/class-20-fMAUsLdBd5SDoHBz.htm)|Paradoxical Mystery|Misterio paradójico|oficial|
|[class-20-JaBDNtNYYDfTGYad.htm](feats/class-20-JaBDNtNYYDfTGYad.htm)|Sacred Defender|Defensor sagrado|oficial|
|[class-20-jG9YwAAvNbCShumf.htm](feats/class-20-jG9YwAAvNbCShumf.htm)|Inexhaustible Countermoves|Contramaniobras inagotables|oficial|
|[class-20-KTMzVCd6xAqCvxa5.htm](feats/class-20-KTMzVCd6xAqCvxa5.htm)|Fiendish Mount|Montura infernal|oficial|
|[class-20-N1ajKcWRo3O0oMQg.htm](feats/class-20-N1ajKcWRo3O0oMQg.htm)|Bloodline Mutation|Mutación de linaje|oficial|
|[class-20-Oo3yRbmrgqi8dmVs.htm](feats/class-20-Oo3yRbmrgqi8dmVs.htm)|Panache Paragon|Parangón de la elegancia|oficial|
|[class-20-PExiZZTSP4p7TZaW.htm](feats/class-20-PExiZZTSP4p7TZaW.htm)|Oracular Providence|Providencia oracular|oficial|
|[class-20-QSBuAkJ5GMLcuZg9.htm](feats/class-20-QSBuAkJ5GMLcuZg9.htm)|Ultimate Polymath|Polifacético definitivo|oficial|
|[class-20-rwTbN2A2ZO7CdKoC.htm](feats/class-20-rwTbN2A2ZO7CdKoC.htm)|Impossible Snares|Trampas de lazo imposibles|oficial|
|[class-20-sayRIHE2V6vuxr4r.htm](feats/class-20-sayRIHE2V6vuxr4r.htm)|Ultimate Flexibility|Flexibilidad definitiva|oficial|
|[class-20-SelPslNtTfzxp7fs.htm](feats/class-20-SelPslNtTfzxp7fs.htm)|Patron's Truth|Verdad del patrón|oficial|
|[class-20-Sq9muixjFptJO8Ae.htm](feats/class-20-Sq9muixjFptJO8Ae.htm)|Witch's Hut|Cabaña del brujo|oficial|
|[class-20-TLCeFMDRXFB46sa8.htm](feats/class-20-TLCeFMDRXFB46sa8.htm)|Deadly Strikes|Golpes letales|oficial|
|[class-20-UOxDJt8Y7SiCR4xq.htm](feats/class-20-UOxDJt8Y7SiCR4xq.htm)|Pied Piping|Flauta cautivadora|oficial|
|[class-20-w4dijKncXx0ssBOQ.htm](feats/class-20-w4dijKncXx0ssBOQ.htm)|Everyone's A Suspect|Todos son sospechosos|oficial|
|[class-20-xhiwito5kneP4sjV.htm](feats/class-20-xhiwito5kneP4sjV.htm)|Spell Mastery|Maestría en conjuros|oficial|
|[class-20-YCqHaqn0TxdiGxiW.htm](feats/class-20-YCqHaqn0TxdiGxiW.htm)|Unstoppable Juggernaut|Juggernaut Imparable|oficial|
|[class-20-ZfycfbXlPXZlSqw5.htm](feats/class-20-ZfycfbXlPXZlSqw5.htm)|Just The Facts|Limitarse a los hechos|oficial|
|[general-01-2kwXTUjYYhoAGySr.htm](feats/general-01-2kwXTUjYYhoAGySr.htm)|Incredible Initiative|Iniciativa sensacional|oficial|
|[general-01-aJUXbe9HJVvv0Mxa.htm](feats/general-01-aJUXbe9HJVvv0Mxa.htm)|Breath Control|Control de la respiración.|oficial|
|[general-01-AmP0qu7c5dlBSath.htm](feats/general-01-AmP0qu7c5dlBSath.htm)|Toughness|Dureza|oficial|
|[general-01-BStw1cANwx5baL6d.htm](feats/general-01-BStw1cANwx5baL6d.htm)|Armor Proficiency|Competencia con armadura|oficial|
|[general-01-c9fHUSI5lRdXu1Ic.htm](feats/general-01-c9fHUSI5lRdXu1Ic.htm)|Feather Step|Paso de pluma|oficial|
|[general-01-I0BhPWqYf1bbzEYg.htm](feats/general-01-I0BhPWqYf1bbzEYg.htm)|Diehard|Duro de pelar|oficial|
|[general-01-i4yRvVwvXbGZDsD1.htm](feats/general-01-i4yRvVwvXbGZDsD1.htm)|Canny Acumen|Perspicacia astuta|oficial|
|[general-01-ihN8gkHSdPG9Trte.htm](feats/general-01-ihN8gkHSdPG9Trte.htm)|Adopted Ancestry|Ascendencia adoptada|oficial|
|[general-01-jM72TjJ965jocBV8.htm](feats/general-01-jM72TjJ965jocBV8.htm)|Shield Block|Bloqueo con el escudo|oficial|
|[general-01-N8Xz5fuW6o7GW124.htm](feats/general-01-N8Xz5fuW6o7GW124.htm)|Fast Recovery|Recuperación rápida|oficial|
|[general-01-Rq5wkA8DtsmbzoGV.htm](feats/general-01-Rq5wkA8DtsmbzoGV.htm)|Ride|Montar|oficial|
|[general-01-Ux73dmoF8KnavyUD.htm](feats/general-01-Ux73dmoF8KnavyUD.htm)|Fleet|Pies ligeros|oficial|
|[general-01-x9wxQ61HNkAVbDHr.htm](feats/general-01-x9wxQ61HNkAVbDHr.htm)|Weapon Proficiency|Competencia con armas|oficial|
|[general-03-9jGaBxLUtevZYcZO.htm](feats/general-03-9jGaBxLUtevZYcZO.htm)|Untrained Improvisation|Improvisación no entrenada|oficial|
|[general-03-9QQnv7nFpsNCGE89.htm](feats/general-03-9QQnv7nFpsNCGE89.htm)|Thorough Search|Búsqueda meticulosa|oficial|
|[general-03-m7KjpkAAh9PptJsY.htm](feats/general-03-m7KjpkAAh9PptJsY.htm)|Ancestral Paragon|Parangón ancestral|oficial|
|[general-03-Wb3FHiDuY6Nuc0N0.htm](feats/general-03-Wb3FHiDuY6Nuc0N0.htm)|Hireling Manager|Gerente de secuaces|oficial|
|[general-07-GdZLxDtFXaQI3Fop.htm](feats/general-07-GdZLxDtFXaQI3Fop.htm)|Expeditious Search|Búsqueda expeditiva|oficial|
|[general-07-gfMP2aMs3YGONVeB.htm](feats/general-07-gfMP2aMs3YGONVeB.htm)|Numb to Death|Insensible a la muerte|oficial|
|[general-07-lHFz4MmebvPqTb0A.htm](feats/general-07-lHFz4MmebvPqTb0A.htm)|Prescient Consumable|Consumible presciente|oficial|
|[general-11-4vu6P3cYoOOeUbLK.htm](feats/general-11-4vu6P3cYoOOeUbLK.htm)|Incredible Investiture|Investidura sensacional|oficial|
|[general-11-aFoVHsuInMOkTZoQ.htm](feats/general-11-aFoVHsuInMOkTZoQ.htm)|Incredible Scout|Batidor increíble|oficial|
|[skill-01-09PurtIanNUPfNRq.htm](feats/skill-01-09PurtIanNUPfNRq.htm)|Combat Climber|Trepador de combate|oficial|
|[skill-01-0GF2j54roPFIDmXf.htm](feats/skill-01-0GF2j54roPFIDmXf.htm)|Bon Mot|Comentario agudo|oficial|
|[skill-01-1Bt7uCW2WI4sM84P.htm](feats/skill-01-1Bt7uCW2WI4sM84P.htm)|Dubious Knowledge|Conocimiento dudoso|oficial|
|[skill-01-1Eceqc6zbMj2x0q9.htm](feats/skill-01-1Eceqc6zbMj2x0q9.htm)|Seasoned|Conocedor|oficial|
|[skill-01-3G8xUlgCjRmRJNfP.htm](feats/skill-01-3G8xUlgCjRmRJNfP.htm)|Quick Squeeze|Escurrirse rápidamente|oficial|
|[skill-01-3HChkcD1IRqv4DbA.htm](feats/skill-01-3HChkcD1IRqv4DbA.htm)|Improvise Tool|Improvisar Herramienta|oficial|
|[skill-01-4RjDxgvNXNl5GG9d.htm](feats/skill-01-4RjDxgvNXNl5GG9d.htm)|Hobnobber|Confraternizador|oficial|
|[skill-01-4tTkRyOQ0VuRBac3.htm](feats/skill-01-4tTkRyOQ0VuRBac3.htm)|Terrain Expertise|Experiencia en un terreno|oficial|
|[skill-01-5nc5ridFBfYpn2Om.htm](feats/skill-01-5nc5ridFBfYpn2Om.htm)|Bargain Hunter|Buscador de gangas|oficial|
|[skill-01-7LB00jkh6JaJr3vS.htm](feats/skill-01-7LB00jkh6JaJr3vS.htm)|Fascinating Performance|Interpretación fascinante|oficial|
|[skill-01-8qebBeOJsyRIchcu.htm](feats/skill-01-8qebBeOJsyRIchcu.htm)|Forager|Forrajeador|oficial|
|[skill-01-aAoFc10cOpxGypOY.htm](feats/skill-01-aAoFc10cOpxGypOY.htm)|Sign Language|Lenguaje de signos|oficial|
|[skill-01-ar2DUlvDK4LDcH9J.htm](feats/skill-01-ar2DUlvDK4LDcH9J.htm)|Quick Coercion|Intimidación rápida|oficial|
|[skill-01-ASy9AKEIRxPYUi5o.htm](feats/skill-01-ASy9AKEIRxPYUi5o.htm)|Quick Repair|Reparación rápida|oficial|
|[skill-01-B6HbYsLBWb1RR6Fx.htm](feats/skill-01-B6HbYsLBWb1RR6Fx.htm)|Charming Liar|Mentiroso encantador|oficial|
|[skill-01-beyw5bdA5hkQbmaG.htm](feats/skill-01-beyw5bdA5hkQbmaG.htm)|Terrain Stalker|Acechador del terreno|oficial|
|[skill-01-bkZgWFSFV4cAf5Ot.htm](feats/skill-01-bkZgWFSFV4cAf5Ot.htm)|Risky Surgery|Cirugía arriesgada|oficial|
|[skill-01-blMeVamjGz4ODWxq.htm](feats/skill-01-blMeVamjGz4ODWxq.htm)|Arcane Sense|Sentido arcano|oficial|
|[skill-01-BocFD2KV0qgUC76x.htm](feats/skill-01-BocFD2KV0qgUC76x.htm)|Additional Lore|Saber adicional|oficial|
|[skill-01-BqceQIKE0lwIS98s.htm](feats/skill-01-BqceQIKE0lwIS98s.htm)|Pilgrim's Token|Amuleto del peregrino|oficial|
|[skill-01-C0Tcelg3BAPhML6J.htm](feats/skill-01-C0Tcelg3BAPhML6J.htm)|Hefty Hauler|Porteador recio|oficial|
|[skill-01-DMetdzt1VJL2Y62i.htm](feats/skill-01-DMetdzt1VJL2Y62i.htm)|Snare Crafting|Preparar trampas de lazo|oficial|
|[skill-01-Dvz54d6aPhjsmUux.htm](feats/skill-01-Dvz54d6aPhjsmUux.htm)|Lie To Me|Miénteme|oficial|
|[skill-01-gUqvezs2zzoTXFAI.htm](feats/skill-01-gUqvezs2zzoTXFAI.htm)|Group Coercion|Intimidación de grupo|oficial|
|[skill-01-hDGosy2ZTwnyctEP.htm](feats/skill-01-hDGosy2ZTwnyctEP.htm)|Oddity Identification|Identificar extrazas|oficial|
|[skill-01-HEBXaS656MZTiWFu.htm](feats/skill-01-HEBXaS656MZTiWFu.htm)|Lengthy Diversion|Distracción extensa|oficial|
|[skill-01-hVZbnsDuXihggylt.htm](feats/skill-01-hVZbnsDuXihggylt.htm)|Subtle Theft|Sustracción sutil|oficial|
|[skill-01-IlOQuCQIhjJpig3S.htm](feats/skill-01-IlOQuCQIhjJpig3S.htm)|Quick Identification|Identificación rápida|oficial|
|[skill-01-iOY6YfGBaOvMNAor.htm](feats/skill-01-iOY6YfGBaOvMNAor.htm)|Underwater Marauder|Merodeador subacuático|oficial|
|[skill-01-is3Oz9wt11lNq62K.htm](feats/skill-01-is3Oz9wt11lNq62K.htm)|Alchemical Crafting|Elaborar Artesanía alquímica|oficial|
|[skill-01-jDdOqFmZLwE4dblQ.htm](feats/skill-01-jDdOqFmZLwE4dblQ.htm)|Pickpocket|Carterista|oficial|
|[skill-01-JtjnFsOToBLnSRO9.htm](feats/skill-01-JtjnFsOToBLnSRO9.htm)|Courtly Graces|Gracia cortesana|oficial|
|[skill-01-KpFetnUqTiweypZk.htm](feats/skill-01-KpFetnUqTiweypZk.htm)|Group Impression|Impresión de grupo|oficial|
|[skill-01-KxaYlC50zzHysJj8.htm](feats/skill-01-KxaYlC50zzHysJj8.htm)|Titan Wrestler|Luchador titánico|oficial|
|[skill-01-Lc4dJZivRwU3QEmT.htm](feats/skill-01-Lc4dJZivRwU3QEmT.htm)|Armor Assist|Ayuda con la armadura|oficial|
|[skill-01-lFwfUvH2708rl6i3.htm](feats/skill-01-lFwfUvH2708rl6i3.htm)|Virtuosic Performer|Intérprete virtuoso|oficial|
|[skill-01-lQs2i9L09MQiZSPC.htm](feats/skill-01-lQs2i9L09MQiZSPC.htm)|Charlatan|Charlatán|oficial|
|[skill-01-LQw0yIMDUJJkq1nD.htm](feats/skill-01-LQw0yIMDUJJkq1nD.htm)|Cat Fall|Caída de gato|oficial|
|[skill-01-MjQyTcV8Jiv1Jtln.htm](feats/skill-01-MjQyTcV8Jiv1Jtln.htm)|Recognize Spell|Reconocer conjuro|oficial|
|[skill-01-nowEaHgIyij7im8F.htm](feats/skill-01-nowEaHgIyij7im8F.htm)|Train Animal|Adiestrar animal|oficial|
|[skill-01-P6icK2DbRoZ3H6kc.htm](feats/skill-01-P6icK2DbRoZ3H6kc.htm)|Skill Training|Entrenamiento en habilidades|oficial|
|[skill-01-P9HCz0uR6xPHuw72.htm](feats/skill-01-P9HCz0uR6xPHuw72.htm)|Multilingual|Multilingüe|oficial|
|[skill-01-QLeMH5mQgh28sa5o.htm](feats/skill-01-QLeMH5mQgh28sa5o.htm)|Specialty Crafting|Especialidad artesana|oficial|
|[skill-01-r7cgrrHh75R8UEqN.htm](feats/skill-01-r7cgrrHh75R8UEqN.htm)|Deceptive Worship|Falsa adoración|oficial|
|[skill-01-sMCpihnBEpx18GBD.htm](feats/skill-01-sMCpihnBEpx18GBD.htm)|Inoculation|Inoculación|oficial|
|[skill-01-sMm0UfYxEPpq2Yzd.htm](feats/skill-01-sMm0UfYxEPpq2Yzd.htm)|Experienced Professional|Profesional experto|oficial|
|[skill-01-t3btih0O5RUwWynt.htm](feats/skill-01-t3btih0O5RUwWynt.htm)|Experienced Smuggler|Contrabandista experto|oficial|
|[skill-01-tGIXuk0XeWmG04CX.htm](feats/skill-01-tGIXuk0XeWmG04CX.htm)|Survey Wildlife|Supervisor de la fauna|oficial|
|[skill-01-uR62fVC9FyQAMCO1.htm](feats/skill-01-uR62fVC9FyQAMCO1.htm)|Trick Magic Item|Trucar objeto mágico|oficial|
|[skill-01-urQZwmzg2kS53vd5.htm](feats/skill-01-urQZwmzg2kS53vd5.htm)|Experienced Tracker|Rastreador experto|oficial|
|[skill-01-W6Gl9ePmItfDHji0.htm](feats/skill-01-W6Gl9ePmItfDHji0.htm)|Assurance|Seguro|oficial|
|[skill-01-WC4xLBGmBsdOdHWu.htm](feats/skill-01-WC4xLBGmBsdOdHWu.htm)|Natural Medicine|Medicina natural|oficial|
|[skill-01-wYerMk6F1RZb0Fwt.htm](feats/skill-01-wYerMk6F1RZb0Fwt.htm)|Battle Medicine|Medicina de guerra|oficial|
|[skill-01-X2jGFfLU5qI5XVot.htm](feats/skill-01-X2jGFfLU5qI5XVot.htm)|Streetwise|Callejeo|oficial|
|[skill-01-x7EMZNMavris2aHY.htm](feats/skill-01-x7EMZNMavris2aHY.htm)|Student of the Canon|Alumno del canon|oficial|
|[skill-01-xqAdXRd2gSQcqp5E.htm](feats/skill-01-xqAdXRd2gSQcqp5E.htm)|Impressive Performance|Interpretación impresionante|oficial|
|[skill-01-xQMz6eDgX75WX2ce.htm](feats/skill-01-xQMz6eDgX75WX2ce.htm)|Intimidating Glare|Mirada intimidante|oficial|
|[skill-01-yUuU9xyotrpwpTyC.htm](feats/skill-01-yUuU9xyotrpwpTyC.htm)|Read Lips|Leer los labios|oficial|
|[skill-01-ZBhvJ9O8MvBFAlhq.htm](feats/skill-01-ZBhvJ9O8MvBFAlhq.htm)|Quick Jump|Salto rápido|oficial|
|[skill-02-3ZerjLH8ls3JT6cD.htm](feats/skill-02-3ZerjLH8ls3JT6cD.htm)|Robust Recovery|Recuperación robusta|oficial|
|[skill-02-5I97q0FfAeXcUQhs.htm](feats/skill-02-5I97q0FfAeXcUQhs.htm)|Nimble Crawl|Gatear ágilmente|oficial|
|[skill-02-6vnbC90UQ3I57RrQ.htm](feats/skill-02-6vnbC90UQ3I57RrQ.htm)|Lasting Coercion|Intimidación duradera|oficial|
|[skill-02-6Z4e3B9vePYpibcy.htm](feats/skill-02-6Z4e3B9vePYpibcy.htm)|Confabulator|Confabulador|oficial|
|[skill-02-8c61nOIr5AM3KxZi.htm](feats/skill-02-8c61nOIr5AM3KxZi.htm)|Ward Medic|Médico de sala|oficial|
|[skill-02-c85a69mB1urW2Se2.htm](feats/skill-02-c85a69mB1urW2Se2.htm)|Continual Recovery|Recuperación continua|oficial|
|[skill-02-cc8O47KFsODReoBe.htm](feats/skill-02-cc8O47KFsODReoBe.htm)|Glad-Hand|Estrechar manos|oficial|
|[skill-02-e4KB4pSkx2lDBNw3.htm](feats/skill-02-e4KB4pSkx2lDBNw3.htm)|Quick Disguise|Disfraz rápido|oficial|
|[skill-02-FJK7JTLSgugRIlvS.htm](feats/skill-02-FJK7JTLSgugRIlvS.htm)|Rapid Mantel|Saliente rápido|oficial|
|[skill-02-H3I2X0f7v4EzwxuN.htm](feats/skill-02-H3I2X0f7v4EzwxuN.htm)|Automatic Knowledge|Conocimiento automático|oficial|
|[skill-02-HJYQlmGTdtyGWr6a.htm](feats/skill-02-HJYQlmGTdtyGWr6a.htm)|Powerful Leap|Salto sin carrerilla|oficial|
|[skill-02-iCv8KTZ5PcF4GqeV.htm](feats/skill-02-iCv8KTZ5PcF4GqeV.htm)|Connections|Conexiones|oficial|
|[skill-02-LDUWw2TeEEH0KA6M.htm](feats/skill-02-LDUWw2TeEEH0KA6M.htm)|Criminal Connections|Conexiones criminales|oficial|
|[skill-02-mEk2POFNU1Q0TQg2.htm](feats/skill-02-mEk2POFNU1Q0TQg2.htm)|Mortal Healing|Curación mortal|oficial|
|[skill-02-pekKtubQTkG9m1xK.htm](feats/skill-02-pekKtubQTkG9m1xK.htm)|Quiet Allies|Aliados silenciosos|oficial|
|[skill-02-pLjvgeqwHrYdg411.htm](feats/skill-02-pLjvgeqwHrYdg411.htm)|Bonded Animal|Animal vinculado|oficial|
|[skill-02-QvBIYW6aAqoiyim3.htm](feats/skill-02-QvBIYW6aAqoiyim3.htm)|Exhort The Faithful|Exhortar a los fieles|oficial|
|[skill-02-RLBYJiGMVkaGL5w9.htm](feats/skill-02-RLBYJiGMVkaGL5w9.htm)|Wary Disarmament|Desactivación cautelosa|oficial|
|[skill-02-ThoOsKjn5xCuZUqM.htm](feats/skill-02-ThoOsKjn5xCuZUqM.htm)|Quick Contacts|Contactos rápidos|oficial|
|[skill-02-v7Bt6hjmzYnLFLeG.htm](feats/skill-02-v7Bt6hjmzYnLFLeG.htm)|Magical Shorthand|Taquigrafía mágica|oficial|
|[skill-02-XFJiGllNZp8Xebda.htm](feats/skill-02-XFJiGllNZp8Xebda.htm)|Intimidating Prowess|Pericia intimidante|oficial|
|[skill-02-XmF4q4rzKWg55vG4.htm](feats/skill-02-XmF4q4rzKWg55vG4.htm)|Assured Identification|Identificación asegurada|oficial|
|[skill-02-XvX1EyxWbbBF32NV.htm](feats/skill-02-XvX1EyxWbbBF32NV.htm)|Unmistakable Lore|Saber inconfundible|oficial|
|[skill-02-xWY5omyIcILNR7y1.htm](feats/skill-02-xWY5omyIcILNR7y1.htm)|Magical Crafting|Artesanía mágica|oficial|
|[skill-04-G9l2g7sDpPVbZJza.htm](feats/skill-04-G9l2g7sDpPVbZJza.htm)|Quick Mount|Montura rápida|oficial|
|[skill-04-R7BO8br4BjCmpjit.htm](feats/skill-04-R7BO8br4BjCmpjit.htm)|Settlement Scholastics|Escolástica de asentamiento|oficial|
|[skill-07-2rSyTfPgAmNAo01r.htm](feats/skill-07-2rSyTfPgAmNAo01r.htm)|Quick Recognition|Reconocimiento rápido|oficial|
|[skill-07-63sSYk5yqiAyZGb9.htm](feats/skill-07-63sSYk5yqiAyZGb9.htm)|Graceful Leaper|Saltador elegante|oficial|
|[skill-07-7GmXKrkzmInkFyEr.htm](feats/skill-07-7GmXKrkzmInkFyEr.htm)|Quick Unlock|Forzado rápido|oficial|
|[skill-07-9o2VSlMQVPB4LN09.htm](feats/skill-07-9o2VSlMQVPB4LN09.htm)|Bizarre Magic|Magia extraña|oficial|
|[skill-07-bFoh3267kNLk68cU.htm](feats/skill-07-bFoh3267kNLk68cU.htm)|Quick Swim|Natación rápida|oficial|
|[skill-07-dc8X2Mbtwq6kGp7F.htm](feats/skill-07-dc8X2Mbtwq6kGp7F.htm)|Terrified Retreat|Retirada aterrada|oficial|
|[skill-07-dz1rHYk3n9pUFfgm.htm](feats/skill-07-dz1rHYk3n9pUFfgm.htm)|Bless Toxin|Bendecir Toxina|oficial|
|[skill-07-ePObIpaJDgDb9CQj.htm](feats/skill-07-ePObIpaJDgDb9CQj.htm)|Battle Cry|Grito de guerra|oficial|
|[skill-07-f5JOSyW1tKMpz6hU.htm](feats/skill-07-f5JOSyW1tKMpz6hU.htm)|Influence Nature|Influir en la naturaleza|oficial|
|[skill-07-fvYwsHM9O1twQa5N.htm](feats/skill-07-fvYwsHM9O1twQa5N.htm)|Consult The Spirits|Consultar a los espíritus|oficial|
|[skill-07-gBSPbQRXdagZTUwY.htm](feats/skill-07-gBSPbQRXdagZTUwY.htm)|Kip Up|Levantarse|oficial|
|[skill-07-gHBdjbEnIK8clK8u.htm](feats/skill-07-gHBdjbEnIK8clK8u.htm)|Sacred Defense|Defensa Sagrada|oficial|
|[skill-07-hkSuxXOc9qBleJbd.htm](feats/skill-07-hkSuxXOc9qBleJbd.htm)|Disturbing Knowledge|Conocimiento inquietante|oficial|
|[skill-07-IJQJBnD5CjKvFYEx.htm](feats/skill-07-IJQJBnD5CjKvFYEx.htm)|Foil Senses|Frustrar los sentidos|oficial|
|[skill-07-Imvu2RV2ggjJ2HZt.htm](feats/skill-07-Imvu2RV2ggjJ2HZt.htm)|Swift Sneak|Movimiento furtivo rápido|oficial|
|[skill-07-KIK2Eza9TK47MEb2.htm](feats/skill-07-KIK2Eza9TK47MEb2.htm)|Shameless Request|Petición descarada|oficial|
|[skill-07-MTO0spetPKyIa4sT.htm](feats/skill-07-MTO0spetPKyIa4sT.htm)|Water Sprint|Carrera acuática|oficial|
|[skill-07-nBlzWZnmYuFHrMyV.htm](feats/skill-07-nBlzWZnmYuFHrMyV.htm)|Battle Prayer|Plegaria de Batalla|oficial|
|[skill-07-P04Hw8E6WAWARKHP.htm](feats/skill-07-P04Hw8E6WAWARKHP.htm)|Quick Climb|Escalada rápida|oficial|
|[skill-07-P9dVBWB8nYZt4AFA.htm](feats/skill-07-P9dVBWB8nYZt4AFA.htm)|Sanctify Water|Santificar el agua|oficial|
|[skill-07-PiUe3tpv7UVtnfvS.htm](feats/skill-07-PiUe3tpv7UVtnfvS.htm)|Impeccable Crafting|Artesanía impecable|oficial|
|[skill-07-qibU5SZPFHMNnpAP.htm](feats/skill-07-qibU5SZPFHMNnpAP.htm)|Doublespeak|Doble lenguaje|oficial|
|[skill-07-RiuZT3H4QZIIEQXJ.htm](feats/skill-07-RiuZT3H4QZIIEQXJ.htm)|Slippery Secrets|Secretos escurridizos|oficial|
|[skill-07-u6tLp3zTBweq7CxO.htm](feats/skill-07-u6tLp3zTBweq7CxO.htm)|Environmental Grace|Gracia Ambiental|oficial|
|[skill-07-UHyoXbp8O6idQ6ee.htm](feats/skill-07-UHyoXbp8O6idQ6ee.htm)|Bless Tonic|Bendecir Tónico|oficial|
|[skill-07-X8iSUF1m0eezmrjs.htm](feats/skill-07-X8iSUF1m0eezmrjs.htm)|Wall Jump|Saltar paredes|oficial|
|[skill-07-x9xA8P2Vlz98He7C.htm](feats/skill-07-x9xA8P2Vlz98He7C.htm)|Rapid Affixture|Colocación rápida|oficial|
|[skill-07-XR95taODq1sq82Du.htm](feats/skill-07-XR95taODq1sq82Du.htm)|Inventor|Inventor|oficial|
|[skill-07-YgbcLfAEdi4xxvX5.htm](feats/skill-07-YgbcLfAEdi4xxvX5.htm)|Evangelize|Evangelizar|oficial|
|[skill-07-Yj4mpROEjdCjQzMd.htm](feats/skill-07-Yj4mpROEjdCjQzMd.htm)|Planar Survival|Supervivencia planaria|oficial|
|[skill-15-467qQoiy6bjWU1G8.htm](feats/skill-15-467qQoiy6bjWU1G8.htm)|Legendary Performer|Intérprete legendario|oficial|
|[skill-15-A0TNeMNvyY8QpmLz.htm](feats/skill-15-A0TNeMNvyY8QpmLz.htm)|Legendary Negotiation|Negociación legendaria|oficial|
|[skill-15-dYMxP8SsHrwOze8v.htm](feats/skill-15-dYMxP8SsHrwOze8v.htm)|Divine Guidance|Orientación divina|oficial|
|[skill-15-GZba7ped7ZxYHchf.htm](feats/skill-15-GZba7ped7ZxYHchf.htm)|Legendary Professional|Profesional legendario|oficial|
|[skill-15-hrq3NOpS6148aVY1.htm](feats/skill-15-hrq3NOpS6148aVY1.htm)|Unified Theory|Teoría unificada|oficial|
|[skill-15-IZbjUaZI5zHTd1Vp.htm](feats/skill-15-IZbjUaZI5zHTd1Vp.htm)|Legendary Thief|Ladrón legendario|oficial|
|[skill-15-Kk4AMZtpQnLEgN0b.htm](feats/skill-15-Kk4AMZtpQnLEgN0b.htm)|Legendary Medic|Médico legendario|oficial|
|[skill-15-mZttsiWl1ql5NvrH.htm](feats/skill-15-mZttsiWl1ql5NvrH.htm)|Scare to Death|Susto mortal|oficial|
|[skill-15-n0S0tJiOJPQk0Rne.htm](feats/skill-15-n0S0tJiOJPQk0Rne.htm)|Legendary Linguist|Lingüista legendario|oficial|
|[skill-15-n0urrOL8YlnVBVRQ.htm](feats/skill-15-n0urrOL8YlnVBVRQ.htm)|Legendary Sneak|Movimiento furtivo legendario|oficial|
|[skill-15-Ta61ObC8Lk7BxTFO.htm](feats/skill-15-Ta61ObC8Lk7BxTFO.htm)|Legendary Survivalist|Supervivencialista legendario|oficial|
|[skill-15-TVFfTP9fHRidwBlW.htm](feats/skill-15-TVFfTP9fHRidwBlW.htm)|Cloud Jump|Salto nuboso|oficial|
|[skill-15-v62QzTwHOT3t86cL.htm](feats/skill-15-v62QzTwHOT3t86cL.htm)|Craft Anything|Elaborar cualquier cosa|oficial|
|[skill-15-Vk7BzAb3D9r226sI.htm](feats/skill-15-Vk7BzAb3D9r226sI.htm)|Legendary Guide|Guía legendario|oficial|
|[skill-15-Zf4yiLDdxHPovEQI.htm](feats/skill-15-Zf4yiLDdxHPovEQI.htm)|Legendary Codebreaker|Descifrador legendario|oficial|
