# Estado de la traducción (action-macros)

 * **modificada**: 60
 * **ninguna**: 10


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[50Q0DYL33Kalu1BH.htm](action-macros/50Q0DYL33Kalu1BH.htm)|Escape: Acrobatics|
|[aOrMEnTVPpb1vbzj.htm](action-macros/aOrMEnTVPpb1vbzj.htm)|Escape: Unarmed Attack|
|[EpiQHXzHv0GS1tj0.htm](action-macros/EpiQHXzHv0GS1tj0.htm)|Escape|
|[Gj68YCVlDjc75iCP.htm](action-macros/Gj68YCVlDjc75iCP.htm)|Palm an Object: Thievery|
|[l5pbgrj8SSNtRGs8.htm](action-macros/l5pbgrj8SSNtRGs8.htm)|Administer First Aid - Stabilize: Medicine|
|[lkEcQQss16SIrVxM.htm](action-macros/lkEcQQss16SIrVxM.htm)|Escape: Athletics|
|[mNphXpAkmGsMadUv.htm](action-macros/mNphXpAkmGsMadUv.htm)|Create Forgery: Society|
|[ZEWD4zcEDQwYhVT8.htm](action-macros/ZEWD4zcEDQwYhVT8.htm)|Administer First Aid - Stop Bleeding: Medicine|
|[zjovbAeuLvyuWFKd.htm](action-macros/zjovbAeuLvyuWFKd.htm)|Steal: Thievery|
|[zn0HadZeoKDALxRu.htm](action-macros/zn0HadZeoKDALxRu.htm)|Conceal an Object: Stealth|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[1AIo5UcVbCmvpRL3.htm](action-macros/1AIo5UcVbCmvpRL3.htm)|Exploration: Avoid Notice|Exploración: Evitar ser visto|modificada|
|[1JpYPlIkjyseE9JU.htm](action-macros/1JpYPlIkjyseE9JU.htm)|Create a Diversion - Trick: Deception|Crear una distracción - Truco: Enga|modificada|
|[1Sj2Pz3VI2SFWqZw.htm](action-macros/1Sj2Pz3VI2SFWqZw.htm)|Make an Impression: Diplomacy|Causar impresión: Diplomacia|modificada|
|[2qhYHkcSsTJoSwrJ.htm](action-macros/2qhYHkcSsTJoSwrJ.htm)|Tumble Through: Acrobatics|Pasar haciendo acrobacias: Acróbata|modificada|
|[4hfQEMiEOBbqelAh.htm](action-macros/4hfQEMiEOBbqelAh.htm)|Raise a Shield|Alzar un escudo|modificada|
|[55mxH0w8UkY1o3Xv.htm](action-macros/55mxH0w8UkY1o3Xv.htm)|Balance: Acrobatics|Mantener el equilibrio: Acróbata|modificada|
|[7zqvzpR9gvHPUDqC.htm](action-macros/7zqvzpR9gvHPUDqC.htm)|Whirling Throw: Athletics|Lanzamiento giratorio: Atletismo|modificada|
|[8YrH37NzKRuiKFbF.htm](action-macros/8YrH37NzKRuiKFbF.htm)|Pick a Lock: Thievery|Forzar una cerradura: Latrocinio|modificada|
|[9RNumMausgG7adgL.htm](action-macros/9RNumMausgG7adgL.htm)|Coerce: Intimidation|Obligar: Intimidación|modificada|
|[9Ul5Op5OceT9P5SS.htm](action-macros/9Ul5Op5OceT9P5SS.htm)|Maneuver in Flight: Acrobatics|Maniobra en vuelo: Acrobacia|modificada|
|[aDsYSdRqiC6qQIOQ.htm](action-macros/aDsYSdRqiC6qQIOQ.htm)|Create a Diversion - Distracting Words: Deception|Elaborar una distracción - Palabras de distracción: Enga|modificada|
|[Al5LYMMdeDcpC9Br.htm](action-macros/Al5LYMMdeDcpC9Br.htm)|Track: Survival|Rastrear: Supervivencia|modificada|
|[BQTA7bL264189Xla.htm](action-macros/BQTA7bL264189Xla.htm)|Repair: Crafting|Reparar: Elaborar Artesanía|modificada|
|[Bw3rSN774LaHLELJ.htm](action-macros/Bw3rSN774LaHLELJ.htm)|Perform - Play keyboard instrument: Performance|Interpretar - Tocar instrumento de teclado: Interpretar|modificada|
|[dWcrojMk0d2WRPBq.htm](action-macros/dWcrojMk0d2WRPBq.htm)|Perform - Singing: Performance|Interpretar - Cantar: Interpretar|modificada|
|[EDLftLWLBefTFDtu.htm](action-macros/EDLftLWLBefTFDtu.htm)|Bon Mot: Diplomacy|Comentario agudo: Diplomacia|modificada|
|[FlM3HvpnsZpCKawG.htm](action-macros/FlM3HvpnsZpCKawG.htm)|Hide: Stealth|Esconderse: Stealth|modificada|
|[gRj7xUfcpUZQLrOC.htm](action-macros/gRj7xUfcpUZQLrOC.htm)|Trip: Athletics|Derribar: Atletismo|modificada|
|[HQrDnBbbSCQEXrSb.htm](action-macros/HQrDnBbbSCQEXrSb.htm)|Perform - Play wind instrument: Performance|Interpretar - Tocar instrumento de viento: Interpretar|modificada|
|[HSTkVuv0SjTNK3Xx.htm](action-macros/HSTkVuv0SjTNK3Xx.htm)|Sneak: Stealth|Movimiento furtivo: Stealth|modificada|
|[I6MMTpk9faBOHb2T.htm](action-macros/I6MMTpk9faBOHb2T.htm)|Encouraging Words|Palabras alentadoras|modificada|
|[i6WwSpxDuV2jCwB4.htm](action-macros/i6WwSpxDuV2jCwB4.htm)|Perform - Comedy: Performance|Interpretar - Comedia: Interpretar|modificada|
|[i95kcGLIQKOTsnv6.htm](action-macros/i95kcGLIQKOTsnv6.htm)|Grapple: Athletics|Presa: Atletismo|modificada|
|[jOM1w2GymtSuEurf.htm](action-macros/jOM1w2GymtSuEurf.htm)|Tamper: Crafting|Tamper: Artesanía|modificada|
|[k5nW4jGyXD0Oq9LR.htm](action-macros/k5nW4jGyXD0Oq9LR.htm)|Impersonate: Deception|Imitar: Enga|modificada|
|[LN67MgbGE8IHb2X0.htm](action-macros/LN67MgbGE8IHb2X0.htm)|Sense Direction: Survival|Sentir la dirección: Supervivencia|modificada|
|[LXCy1iJddD95Z91s.htm](action-macros/LXCy1iJddD95Z91s.htm)|Climb: Athletics|Trepar: Atletismo|modificada|
|[m4iM5r3TfvQs5Y2n.htm](action-macros/m4iM5r3TfvQs5Y2n.htm)|Treat Disease: Medicine|Tratar enfermedad: Medicina|modificada|
|[mkKko3CEBCyJVQw1.htm](action-macros/mkKko3CEBCyJVQw1.htm)|Subsist: Society|Subsistir: Sociedad|modificada|
|[nEwqNNWX6scLt4sc.htm](action-macros/nEwqNNWX6scLt4sc.htm)|Demoralize: Intimidation|Desmoralizar: Intimidación|modificada|
|[nvpRlDKUD4SjZaJ0.htm](action-macros/nvpRlDKUD4SjZaJ0.htm)|Perform - Play string instrument: Performance|Interpretar - Tocar instrumento de cuerda: Interpretar|modificada|
|[Om0rNX4K3pQcXDJn.htm](action-macros/Om0rNX4K3pQcXDJn.htm)|Perform - Oratory: Performance|Interpretar - Oratoria: Interpretar|modificada|
|[ooiO59Ch2QaebOmc.htm](action-macros/ooiO59Ch2QaebOmc.htm)|Disarm: Athletics|Desarmar: Atletismo|modificada|
|[oxowCzHbxSGOWRke.htm](action-macros/oxowCzHbxSGOWRke.htm)|Steel Your Resolve|Steel Your Resolución|modificada|
|[PgA74rWIAupEr7LU.htm](action-macros/PgA74rWIAupEr7LU.htm)|Perform - Dance: Performance|Interpretar - Danzante: Interpretar|modificada|
|[PmHt7Gb5fCrlWWTr.htm](action-macros/PmHt7Gb5fCrlWWTr.htm)|Sense Motive: Perception|Sense Motive: Percepción|modificada|
|[QPsV0qi2zXm7syt6.htm](action-macros/QPsV0qi2zXm7syt6.htm)|Long Jump: Athletics|Salto de longitud: Atletismo|modificada|
|[R03LRl2RBbsm6EcF.htm](action-macros/R03LRl2RBbsm6EcF.htm)|Treat Poison: Medicine|Tratar veneno: Medicina|modificada|
|[rCgGPEyXbzLFcio6.htm](action-macros/rCgGPEyXbzLFcio6.htm)|Gather Information: Diplomacy|Reunir información: Diplomacia|modificada|
|[RjfPFjqPrNve6eeh.htm](action-macros/RjfPFjqPrNve6eeh.htm)|Feint: Deception|Fintar: Enga|modificada|
|[RZyfkw1DiqVy3JUC.htm](action-macros/RZyfkw1DiqVy3JUC.htm)|Decipher Writing: Occultism|Descifrar escritura: Ocultismo|modificada|
|[sDUERv4E88G5BRPr.htm](action-macros/sDUERv4E88G5BRPr.htm)|Decipher Writing: Religion|Descifrar escritura: Religión|modificada|
|[T2QNEoRojMWEec4a.htm](action-macros/T2QNEoRojMWEec4a.htm)|Disable Device: Thievery|Inutilizar mecanismo: Latrocinio|modificada|
|[tbveXG4gaIoKnsWX.htm](action-macros/tbveXG4gaIoKnsWX.htm)|Request: Diplomacy|Pedir: Diplomacia|modificada|
|[tikhJ2b6AMh7wQU7.htm](action-macros/tikhJ2b6AMh7wQU7.htm)|Seek: Perception|Buscar: Percepción|modificada|
|[TIlUkCzviYxdVk4E.htm](action-macros/TIlUkCzviYxdVk4E.htm)|Swim: Athletics|Nadar: Atletismo|modificada|
|[Tu7LIRelQsiOuo1l.htm](action-macros/Tu7LIRelQsiOuo1l.htm)|Craft: Crafting|Elaborar: Elaborar Artesanía|modificada|
|[U6WjxFPn4fUqIrfl.htm](action-macros/U6WjxFPn4fUqIrfl.htm)|Decipher Writing: Arcana|Descifrar escritura: Arcanos|modificada|
|[UKHPveLpG7hUs4D0.htm](action-macros/UKHPveLpG7hUs4D0.htm)|Squeeze: Acrobatics|Escurrirse: Acróbata|modificada|
|[UoRM6ZMhhhjz1vu8.htm](action-macros/UoRM6ZMhhhjz1vu8.htm)|Perform - Play percussion instrument: Performance|Interpretar - Tocar instrumento de percusión: Interpretar|modificada|
|[v3dlDjFlOmT5T2gC.htm](action-macros/v3dlDjFlOmT5T2gC.htm)|High Jump: Athletics|Salto de altura: Atletismo|modificada|
|[vmoO2VasZyTgs3PH.htm](action-macros/vmoO2VasZyTgs3PH.htm)|Perform - Acting: Performance|Interpretar - Actuar: Interpretar|modificada|
|[VTg4t8kYTvXcHROq.htm](action-macros/VTg4t8kYTvXcHROq.htm)|Lie: Deception|Mentir: Enga|modificada|
|[xcrdOOiN0l6O1sIn.htm](action-macros/xcrdOOiN0l6O1sIn.htm)|Command an Animal: Nature|Comandar a un animal: Naturaleza|modificada|
|[yMTKMnaYSGtDz4wk.htm](action-macros/yMTKMnaYSGtDz4wk.htm)|Force Open: Athletics|Abrir por la fuerza: Atletismo|modificada|
|[yNry1xMZqdWHncbV.htm](action-macros/yNry1xMZqdWHncbV.htm)|Shove: Athletics|Empujar: Atletismo|modificada|
|[YWAvvDXpdW1fYPFo.htm](action-macros/YWAvvDXpdW1fYPFo.htm)|Decipher Writing: Society|Descifrar escritura: Sociedad|modificada|
|[zkqh01BoXDVgydzo.htm](action-macros/zkqh01BoXDVgydzo.htm)|Subsist: Survival|Subsistir: Supervivencia|modificada|
|[ZS5z1TqckWPIMGDN.htm](action-macros/ZS5z1TqckWPIMGDN.htm)|Athletics: Arcane Slam|Atletismo: Golpe Arcano|modificada|
|[zUJ0UhuoFt5a7tiN.htm](action-macros/zUJ0UhuoFt5a7tiN.htm)|Create a Diversion - Gesture: Deception|Crear una distracción - Gesto: Enga|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
