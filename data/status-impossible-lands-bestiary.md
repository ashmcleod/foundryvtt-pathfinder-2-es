# Estado de la traducción (impossible-lands-bestiary)

 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[xl2UxOFVSDGqaVS5.htm](impossible-lands-bestiary/xl2UxOFVSDGqaVS5.htm)|Ratajin Mastermind|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
