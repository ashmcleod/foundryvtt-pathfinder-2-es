# Estado de la traducción (equipment-effects)

 * **modificada**: 337
 * **ninguna**: 91
 * **vacía**: 6


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[5mQ51m1lqQlvfi8n.htm](equipment-effects/5mQ51m1lqQlvfi8n.htm)|Effect: Phantasmal Doorknob - Weapon|
|[9mIS76oZkxXQ4g3T.htm](equipment-effects/9mIS76oZkxXQ4g3T.htm)|Effect: Jolt Coil - Armor|
|[AwpSNXaKloq2KQNy.htm](equipment-effects/AwpSNXaKloq2KQNy.htm)|Effect: Sanguine Fang - Armor (Greater)|
|[bKEx53h6lrFOYvpu.htm](equipment-effects/bKEx53h6lrFOYvpu.htm)|Effect: Sanguine Fang - Weapon|
|[bMZ6Gw58sK8sFp5n.htm](equipment-effects/bMZ6Gw58sK8sFp5n.htm)|Effect: Crackling Bubble Gum (Failure)|
|[Bwh3kiNX8nxEwVZ5.htm](equipment-effects/Bwh3kiNX8nxEwVZ5.htm)|Effect: Sanguine Fang - Weapon (Greater)|
|[ceIvpmxWqBJpBHIn.htm](equipment-effects/ceIvpmxWqBJpBHIn.htm)|Effect: Jolt Coil - Weapon (Major)|
|[cOcHWeogJFIkEI0d.htm](equipment-effects/cOcHWeogJFIkEI0d.htm)|Effect: Energizing Lattice|
|[D8cI0uhI3YFFQssp.htm](equipment-effects/D8cI0uhI3YFFQssp.htm)|Effect: Phantasmal Doorknob - Armor (Major)|
|[DaN3N9bOzqDhOng0.htm](equipment-effects/DaN3N9bOzqDhOng0.htm)|Effect: Pickled Demon Tongue - Weapon (Major)|
|[dfnhwI5pJgLtXh2k.htm](equipment-effects/dfnhwI5pJgLtXh2k.htm)|Effect: Silversheen|
|[dMbRTs2YvnDwtUEj.htm](equipment-effects/dMbRTs2YvnDwtUEj.htm)|Effect: Phantasmal Doorknob - Weapon (Major)|
|[ECGvrM0eAaJlm1VC.htm](equipment-effects/ECGvrM0eAaJlm1VC.htm)|Effect: Desolation Locket - Armor (Greater)|
|[EjLVjt3GMeHM0Ai3.htm](equipment-effects/EjLVjt3GMeHM0Ai3.htm)|Effect: Ghostcaller's Planchette - Armor|
|[eLQABqabYp41Mw1R.htm](equipment-effects/eLQABqabYp41Mw1R.htm)|Effect: Immortal Bastion|
|[EPqnA5OlNpwr41Os.htm](equipment-effects/EPqnA5OlNpwr41Os.htm)|Effect: Beastmaster's Sigil - Ranged Weapon|
|[EwHufLQI1z1QzqZU.htm](equipment-effects/EwHufLQI1z1QzqZU.htm)|Effect: Jolt Coil - Weapon (Greater)|
|[Fb4QI4zlmoqfwHY0.htm](equipment-effects/Fb4QI4zlmoqfwHY0.htm)|Effect: Wyrm Claw - Armor (Major)|
|[ft5LjQSa8mZkklhM.htm](equipment-effects/ft5LjQSa8mZkklhM.htm)|Effect: Polished Demon Horn - Armor|
|[gaEXUewHgPpM3zfW.htm](equipment-effects/gaEXUewHgPpM3zfW.htm)|Effect: Jyoti's Feather - Armor (Greater)|
|[gAQaizpMbZLDbzg7.htm](equipment-effects/gAQaizpMbZLDbzg7.htm)|Effect: Rime Crystal - Armor|
|[grXFmNl8Zy3VRVpR.htm](equipment-effects/grXFmNl8Zy3VRVpR.htm)|Effect: Ghostcaller's Planchette - Weapon|
|[gU3uZE2ihLnpQN0b.htm](equipment-effects/gU3uZE2ihLnpQN0b.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Greater)|
|[GUHNFlNYiR38sTDE.htm](equipment-effects/GUHNFlNYiR38sTDE.htm)|Effect: Crackling Bubble Gum|
|[gZOED4T3o6giterN.htm](equipment-effects/gZOED4T3o6giterN.htm)|Effect: Beastmaster's Sigil - Armor (Greater)|
|[hozXQvKqp62DnawX.htm](equipment-effects/hozXQvKqp62DnawX.htm)|Effect: Jyoti's Feather - Armor|
|[i5agc4lBE6GfeCXq.htm](equipment-effects/i5agc4lBE6GfeCXq.htm)|Effect: Cold Iron Blanch (Lesser)|
|[IiDpW99zrh7zHxmQ.htm](equipment-effects/IiDpW99zrh7zHxmQ.htm)|Effect: Rime Crystal - Weapon|
|[IiPqlP4C7YTjkE9w.htm](equipment-effects/IiPqlP4C7YTjkE9w.htm)|Effect: Phantasmal Doorknob - Weapon (Greater)|
|[IlNjAwsIZShlVsCT.htm](equipment-effects/IlNjAwsIZShlVsCT.htm)|Effect: Architect's Pattern Book|
|[inqXnzrqYzbUBuOj.htm](equipment-effects/inqXnzrqYzbUBuOj.htm)|Effect: Diplomat's Charcuterie|
|[iOY4XqVZNiQ5esdu.htm](equipment-effects/iOY4XqVZNiQ5esdu.htm)|Effect: Pickled Demon Tongue - Armor (Major)|
|[JvwzM4rJWwtB9HAP.htm](equipment-effects/JvwzM4rJWwtB9HAP.htm)|Effect: Wolfjaw Armor|
|[KJXNLvJAl0mNnGvn.htm](equipment-effects/KJXNLvJAl0mNnGvn.htm)|Effect: Jyoti's Feather - Weapon|
|[LbaYzs0dQuFj8FXJ.htm](equipment-effects/LbaYzs0dQuFj8FXJ.htm)|Effect: Pickled Demon Tongue - Weapon|
|[LjaEu7gAGO77uVs2.htm](equipment-effects/LjaEu7gAGO77uVs2.htm)|Effect: Hexing Jar|
|[lU8IO9FIGK1DXVMy.htm](equipment-effects/lU8IO9FIGK1DXVMy.htm)|Effect: Conduct Energy|
|[M1HKNPpqkjFI9A4q.htm](equipment-effects/M1HKNPpqkjFI9A4q.htm)|Effect: Beastmaster's Sigil - Armor|
|[mHIdEC7RX6isILiM.htm](equipment-effects/mHIdEC7RX6isILiM.htm)|Effect: Jolt Coil - Weapon|
|[mQgA7XmjVI6WG6oq.htm](equipment-effects/mQgA7XmjVI6WG6oq.htm)|Effect: Sanguine Fang - Armor|
|[NBxTbdCCqmilAxqA.htm](equipment-effects/NBxTbdCCqmilAxqA.htm)|Effect: Whispering Staff (Enemy)|
|[NddLhLIQYgZYrPTR.htm](equipment-effects/NddLhLIQYgZYrPTR.htm)|Effect: Pickled Demon Tongue - Weapon (Greater)|
|[NwEVRZmLbM9QKoIH.htm](equipment-effects/NwEVRZmLbM9QKoIH.htm)|Effect: Desolation Locket - Weapon|
|[OFJVaPxdafc4ezWB.htm](equipment-effects/OFJVaPxdafc4ezWB.htm)|Effect: Cold Iron Blanch (Greater)|
|[omyZyfTnx3uYVgiP.htm](equipment-effects/omyZyfTnx3uYVgiP.htm)|Effect: Arachnid Harness|
|[owA1eQU6LTP3A3of.htm](equipment-effects/owA1eQU6LTP3A3of.htm)|Effect: Wyrm Claw - Armor (Greater)|
|[P882YXPpESinSvrJ.htm](equipment-effects/P882YXPpESinSvrJ.htm)|Effect: Polished Demon Horn - Weapon (Major)|
|[PBvLrztlLIfr2dlV.htm](equipment-effects/PBvLrztlLIfr2dlV.htm)|Effect: Phantasmal Doorknob - Armor|
|[PFtYVamw7de2cozU.htm](equipment-effects/PFtYVamw7de2cozU.htm)|Effect: Blast Suit|
|[pgWhwSGZd8JT5IlF.htm](equipment-effects/pgWhwSGZd8JT5IlF.htm)|Effect: Ghostcaller's Planchette - Weapon (Greater)|
|[pnBdSjOtQb9T1ajL.htm](equipment-effects/pnBdSjOtQb9T1ajL.htm)|Effect: Jolt Coil - Armor (Major)|
|[pr12dSHV4nIyVG5n.htm](equipment-effects/pr12dSHV4nIyVG5n.htm)|Effect: Polished Demon Horn - Armor (Greater)|
|[QCqYiSIR9DVPAHgR.htm](equipment-effects/QCqYiSIR9DVPAHgR.htm)|Effect: Assassin Vine Wine|
|[qFT6TJU5EObpoixe.htm](equipment-effects/qFT6TJU5EObpoixe.htm)|Effect: Ghostcaller's Planchette - Armor (Greater)|
|[qLl1jwybXY6EbOoI.htm](equipment-effects/qLl1jwybXY6EbOoI.htm)|Effect: Bewitching Bloom (Magnolia)|
|[qoV03Fk6HSzZUCmv.htm](equipment-effects/qoV03Fk6HSzZUCmv.htm)|Effect: Sanguine Fang - Weapon (Major)|
|[Qvjw5RYhglGcVRhF.htm](equipment-effects/Qvjw5RYhglGcVRhF.htm)|Effect: Rime Crystal - Weapon (Major)|
|[R5ywXEYZFV1WBe8t.htm](equipment-effects/R5ywXEYZFV1WBe8t.htm)|Effect: Energizing Rune|
|[rdHzCYZEWpy2rTfI.htm](equipment-effects/rdHzCYZEWpy2rTfI.htm)|Effect: Beastmaster's Sigil - Melee Weapon|
|[Rfw1T5NXIoeUbJzt.htm](equipment-effects/Rfw1T5NXIoeUbJzt.htm)|Effect: Polished Demon Horn - Armor (Major)|
|[ri5qxyVViva60ilN.htm](equipment-effects/ri5qxyVViva60ilN.htm)|Effect: Bewitching Bloom (Lotus)|
|[rQV8Azb3FeUJJ3fG.htm](equipment-effects/rQV8Azb3FeUJJ3fG.htm)|Effect: Delve Scale|
|[RxtpVyOywdrt29Q6.htm](equipment-effects/RxtpVyOywdrt29Q6.htm)|Effect: Desolation Locket - Armor (Major)|
|[Sf6UO6vgCeicggOK.htm](equipment-effects/Sf6UO6vgCeicggOK.htm)|Effect: Shining Ammunition|
|[T38SHe842S43a8bB.htm](equipment-effects/T38SHe842S43a8bB.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Major)|
|[Thd0XXhunYNk6jD7.htm](equipment-effects/Thd0XXhunYNk6jD7.htm)|Effect: Cinnamon Seers|
|[TM0LFTy30FG2wwI2.htm](equipment-effects/TM0LFTy30FG2wwI2.htm)|Effect: Star of Cynosure|
|[tNaFPSbNkcyHS50y.htm](equipment-effects/tNaFPSbNkcyHS50y.htm)|Effect: Polished Demon Horn - Weapon (Greater)|
|[TU67AK08CUsP7pl4.htm](equipment-effects/TU67AK08CUsP7pl4.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Greater)|
|[uHZ23fBG9HIdK5ht.htm](equipment-effects/uHZ23fBG9HIdK5ht.htm)|Effect: Tremorsensors|
|[UihgHdEj0GsaRaAL.htm](equipment-effects/UihgHdEj0GsaRaAL.htm)|Effect: Phantasmal Doorknob - Armor (Greater)|
|[uijpoXaiKXcCYrSD.htm](equipment-effects/uijpoXaiKXcCYrSD.htm)|Effect: Auric Noodles|
|[uK2vXk4WnleihqYI.htm](equipment-effects/uK2vXk4WnleihqYI.htm)|Effect: Cold Iron Blanch (Moderate)|
|[UPMZe0oKVpUgDaOE.htm](equipment-effects/UPMZe0oKVpUgDaOE.htm)|Effect: Pickled Demon Tongue - Armor (Greater)|
|[VKdiRnhrsgQTFSCM.htm](equipment-effects/VKdiRnhrsgQTFSCM.htm)|Effect: Whispering Staff (Ally)|
|[VVXjPCummVHQp7hG.htm](equipment-effects/VVXjPCummVHQp7hG.htm)|Effect: Bloodhound Olfactory Stimulators|
|[vw6BbXYsEgCR3dPt.htm](equipment-effects/vw6BbXYsEgCR3dPt.htm)|Effect: Rime Crystal - Armor (Greater)|
|[WARLTi8unmPgmnNw.htm](equipment-effects/WARLTi8unmPgmnNw.htm)|Effect: Polished Demon Horn - Weapon|
|[wcjEjFKLcPisk4jK.htm](equipment-effects/wcjEjFKLcPisk4jK.htm)|Effect: Jyoti's Feather - Weapon (Major)|
|[WGmrfhdQzlNzyMrq.htm](equipment-effects/WGmrfhdQzlNzyMrq.htm)|Effect: Jyoti's Feather - Weapon (Greater)|
|[WJ9L6rgUTZVV7vEE.htm](equipment-effects/WJ9L6rgUTZVV7vEE.htm)|Effect: Desolation Locket - Armor|
|[WLvFC2eE80SEZpUg.htm](equipment-effects/WLvFC2eE80SEZpUg.htm)|Effect: Wyrm Claw - Armor|
|[WRvZ2Nq3wquisD4Y.htm](equipment-effects/WRvZ2Nq3wquisD4Y.htm)|Effect: Pickled Demon Tongue - Armor|
|[Wylo8ttAkExaX6Gs.htm](equipment-effects/Wylo8ttAkExaX6Gs.htm)|Effect: Rime Crystal - Armor (Major)|
|[xiG3kmPJBpX2KA7l.htm](equipment-effects/xiG3kmPJBpX2KA7l.htm)|Effect: Oil of Corpse Restoration|
|[xMG5PrT6NvCFYGqI.htm](equipment-effects/xMG5PrT6NvCFYGqI.htm)|Effect: Sanguine Fang - Armor (Major)|
|[xSw7cTboMvP8sJAq.htm](equipment-effects/xSw7cTboMvP8sJAq.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Major)|
|[YAZ1iri403S8XcrH.htm](equipment-effects/YAZ1iri403S8XcrH.htm)|Effect: Jyoti's Feather - Armor (Major)|
|[YflZZ7EG7JJkdX0d.htm](equipment-effects/YflZZ7EG7JJkdX0d.htm)|Effect: Jolt Coil - Armor (Greater)|
|[YsV7tB15XrSCKNnB.htm](equipment-effects/YsV7tB15XrSCKNnB.htm)|Effect: Rime Crystal - Weapon (Greater)|
|[yzENPvcYIxegPflt.htm](equipment-effects/yzENPvcYIxegPflt.htm)|Effect: Beastmaster's Sigil - Armor (Major)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0qJEtpXWPb7JJBbY.htm](equipment-effects/0qJEtpXWPb7JJBbY.htm)|Effect: Aromatic Ammunition|Efecto: Munición Aromática|modificada|
|[0YbNzbW0HSKtgStQ.htm](equipment-effects/0YbNzbW0HSKtgStQ.htm)|Effect: Dragon's Blood Pudding (Major)|Efecto: Budín de sangre de dragón (superior)|modificada|
|[16tOZk4qy329s2aK.htm](equipment-effects/16tOZk4qy329s2aK.htm)|Effect: Shielding Salve|Efecto: Ungüento de protección.|modificada|
|[19ECULG5Zp593jQX.htm](equipment-effects/19ECULG5Zp593jQX.htm)|Effect: Dragon's Blood Pudding (Moderate)|Efecto: Budín de Sangre de Dragón (Moderada)|modificada|
|[1ihy7Jvw5PY4WYbP.htm](equipment-effects/1ihy7Jvw5PY4WYbP.htm)|Effect: Eye of the Unseen (Greater)|Efecto: Ojo de lo Invisible (Mayor)|modificada|
|[1l139A2Qik4lBHKO.htm](equipment-effects/1l139A2Qik4lBHKO.htm)|Effect: Juggernaut Mutagen (Lesser)|Efecto: Mutágeno de juggernaut, menor.|modificada|
|[1mKjaWC65KWPuFR4.htm](equipment-effects/1mKjaWC65KWPuFR4.htm)|Effect: Antidote (Major)|Efecto: Antídoto (Superior)|modificada|
|[1N28rGPbAl2IkGUf.htm](equipment-effects/1N28rGPbAl2IkGUf.htm)|Effect: Slime Whip|Efecto: Látigo del cieno|modificada|
|[1ouUo8lLK6H79Rqh.htm](equipment-effects/1ouUo8lLK6H79Rqh.htm)|Effect: Bestial Mutagen (Major)|Efecto: Mutágeno bestial, superior.|modificada|
|[1S51uIRb9bnZtpFU.htm](equipment-effects/1S51uIRb9bnZtpFU.htm)|Effect: Winged Boots|Efecto: Botas aladas|modificada|
|[1tweTwYQuQUV45wJ.htm](equipment-effects/1tweTwYQuQUV45wJ.htm)|Effect: Rowan Rifle (Cold)|Efecto: Rifle Rowan (Frío)|modificada|
|[1xHHvQlW4pRR89qj.htm](equipment-effects/1xHHvQlW4pRR89qj.htm)|Effect: Stone Body Mutagen (Moderate)|Efecto: Mutágeno de cuerpo pétreo (moderada).|modificada|
|[2Bds6d4UGQZqYSZM.htm](equipment-effects/2Bds6d4UGQZqYSZM.htm)|Effect: Quicksilver Mutagen (Greater)|Efecto: Mutágeno de mercurio (mayor).|modificada|
|[2C1HuKDQDGFZuv7l.htm](equipment-effects/2C1HuKDQDGFZuv7l.htm)|Effect: Boulderhead Bock|Efecto: Boulderhead Bock|modificada|
|[2iR5uP6vgPzgKKNO.htm](equipment-effects/2iR5uP6vgPzgKKNO.htm)|Effect: Red-Rib Gill Mask (Moderate)|Efecto: Máscara de Agallas (Moderada)|modificada|
|[2PNo8u4wxSbz5WEs.htm](equipment-effects/2PNo8u4wxSbz5WEs.htm)|Effect: Juggernaut Mutagen (Major)|Efecto: Mutágeno de juggernaut, superior.|modificada|
|[2YgXoHvJfrDHucMr.htm](equipment-effects/2YgXoHvJfrDHucMr.htm)|Effect: Raise a Shield|Efecto: Alzar un escudo.|modificada|
|[2ytxPqhGyLtEjYxW.htm](equipment-effects/2ytxPqhGyLtEjYxW.htm)|Effect: Static Snare|Efecto: Lazo Estático|modificada|
|[3LhreroLRmI4atE6.htm](equipment-effects/3LhreroLRmI4atE6.htm)|Effect: Clockwork Cloak|Efecto: Capa Mecánica|modificada|
|[3O5lvuX4VHqtpCkU.htm](equipment-effects/3O5lvuX4VHqtpCkU.htm)|Effect: Lover's Gloves|Efecto: Guantes de amante|modificada|
|[4aSqtBgvQr2TI3XT.htm](equipment-effects/4aSqtBgvQr2TI3XT.htm)|Effect: Grit (Stage 2)|Efecto: Grit (Etapa 2)|modificada|
|[4G9qnI0oRyL6eKFQ.htm](equipment-effects/4G9qnI0oRyL6eKFQ.htm)|Effect: Frost Vial (Major)|Efecto: Gélida Vial (superior)|modificada|
|[4JULykNCgQoypsu8.htm](equipment-effects/4JULykNCgQoypsu8.htm)|Effect: Spiderfoot Brew (Major)|Efecto: Brebaje de Pie Araña (superior)|modificada|
|[4RnEUeYEzC919GZR.htm](equipment-effects/4RnEUeYEzC919GZR.htm)|Effect: Energizing Rune (Sonic)|Efecto: Energizing Rune (Sonic)|modificada|
|[4tepFOJLhZSelPoa.htm](equipment-effects/4tepFOJLhZSelPoa.htm)|Effect: Dragon Turtle Scale|Efecto: Escama de tortuga dragón|modificada|
|[4uy4Ygf5KD2WrtGW.htm](equipment-effects/4uy4Ygf5KD2WrtGW.htm)|Effect: Nosoi Charm (Diplomacy)|Efecto: Hechizo Nosoi (Diplomacia)|modificada|
|[5dhm66yN0LQTOePw.htm](equipment-effects/5dhm66yN0LQTOePw.htm)|Effect: Holy Steam Ball|Efecto: Bola de Vapor Sagrada|modificada|
|[5Gof60StUppR2Xn9.htm](equipment-effects/5Gof60StUppR2Xn9.htm)|Effect: Skeptic's Elixir (Lesser)|Efecto: Elixir del escéptico (menor).|modificada|
|[5JYchreCttBg7RcD.htm](equipment-effects/5JYchreCttBg7RcD.htm)|Effect: Goo Grenade|Efecto: Granada Goo|modificada|
|[5KXsyN9J78glG25I.htm](equipment-effects/5KXsyN9J78glG25I.htm)|Effect: Ochre Fulcrum Lens|Efecto: Lente de fulcro ocre.|modificada|
|[5lZWAvm0oGxvF4bm.htm](equipment-effects/5lZWAvm0oGxvF4bm.htm)|Effect: Exsanguinating Ammunition (Greater)|Efecto: Munición desangradora (mayor)|modificada|
|[5o33sch67Z8j5Vom.htm](equipment-effects/5o33sch67Z8j5Vom.htm)|Effect: Lastwall Soup (Improved)|Efecto: Sopa Lastwall (Mejorada)|modificada|
|[5OABp099y6w3didN.htm](equipment-effects/5OABp099y6w3didN.htm)|Effect: Soulspark Candle|Efecto: Vela Soulspark|modificada|
|[5oYKYXAexr0vhx84.htm](equipment-effects/5oYKYXAexr0vhx84.htm)|Effect: Treat Disease (Critical Failure)|Efecto: Tratar enfermedad (Fallo crítico)|modificada|
|[5uK3fmGlfJrbWQz4.htm](equipment-effects/5uK3fmGlfJrbWQz4.htm)|Effect: Stalwart's Ring|Efecto: Anillo recio|modificada|
|[5WLda1tGUiKoSj1K.htm](equipment-effects/5WLda1tGUiKoSj1K.htm)|Effect: Stalk Goggles (Major)|Efecto: Gafas de acecho (superior)|modificada|
|[5xgapIXn5DwbXHKh.htm](equipment-effects/5xgapIXn5DwbXHKh.htm)|Effect: Serene Mutagen (Lesser)|Efecto: Mutágeno sereno, menor.|modificada|
|[68xcDyxsNgD3JddD.htm](equipment-effects/68xcDyxsNgD3JddD.htm)|Effect: Energizing Rune (Cold)|Efecto: Runa Energizante (Frío)|modificada|
|[6A8jsLR7upLGuRiv.htm](equipment-effects/6A8jsLR7upLGuRiv.htm)|Effect: Lastwall Soup|Efecto: Sopa Lastwall|modificada|
|[6alqXfVq0qWQC359.htm](equipment-effects/6alqXfVq0qWQC359.htm)|Effect: Energy Robe of Fire|Efecto: Túnica energética de fuego|modificada|
|[6dsPjRKjCPd9BWPt.htm](equipment-effects/6dsPjRKjCPd9BWPt.htm)|Effect: Greater Healer's Gel|Efecto: Gel de Sanador Mayor|modificada|
|[6p2Sjl7XxCc55ft4.htm](equipment-effects/6p2Sjl7XxCc55ft4.htm)|Effect: Mudrock Snare (Success)|Efecto: Lazo de Barro (Éxito)|modificada|
|[6PNLBIdlqqWNCFMy.htm](equipment-effects/6PNLBIdlqqWNCFMy.htm)|Effect: Quicksilver Mutagen (Lesser)|Efecto: Mutágeno de mercurio, menor.|modificada|
|[7dLsA9PAb5ij7Bc6.htm](equipment-effects/7dLsA9PAb5ij7Bc6.htm)|Effect: Dueling Cape|Efecto: Capa de duelo|modificada|
|[7MgpgF8tOXOiDEwv.htm](equipment-effects/7MgpgF8tOXOiDEwv.htm)|Effect: Vaultbreaker's Harness|Efecto: Arnés Rompebóvedas|modificada|
|[7UL8belWmo7U5YGM.htm](equipment-effects/7UL8belWmo7U5YGM.htm)|Effect: Darkvision Elixir (Lesser)|Efecto: Elixir de visión en la oscuridad (menor).|modificada|
|[7vCenP9j6FuHRv5C.htm](equipment-effects/7vCenP9j6FuHRv5C.htm)|Effect: Darkvision Elixir (Greater)|Efecto: Elixir de visión en la oscuridad, mayor.|modificada|
|[7z1iY4AaNEAIKuAU.htm](equipment-effects/7z1iY4AaNEAIKuAU.htm)|Effect: Antidote (Lesser)|Efecto: Antídoto (Menor)|modificada|
|[88kqcDmsoAEddzUt.htm](equipment-effects/88kqcDmsoAEddzUt.htm)|Effect: Boots of Elvenkind|Efecto: Botas élficas|modificada|
|[8ersuvNJXX00XaIQ.htm](equipment-effects/8ersuvNJXX00XaIQ.htm)|Effect: Euryale (Curse) Card|Efecto: Carta Euryale (Maldición)|modificada|
|[8kfSF8P4NOh09YvZ.htm](equipment-effects/8kfSF8P4NOh09YvZ.htm)|Effect: Grim Sandglass - Weapon (Greater)|Efecto: Reloj de arena sombrío - Arma (mayor)|modificada|
|[8RNPIAuV7ixaXeq5.htm](equipment-effects/8RNPIAuV7ixaXeq5.htm)|Effect: War Blood Mutagen (Greater)|Efecto: Mutágeno de sangre de guerra (mayor)|modificada|
|[988f6NpOo4YzFzIr.htm](equipment-effects/988f6NpOo4YzFzIr.htm)|Effect: Quicksilver Mutagen (Major)|Efecto: Mutágeno de mercurio (superior).|modificada|
|[9BsFdrEc7hkPWgSd.htm](equipment-effects/9BsFdrEc7hkPWgSd.htm)|Effect: Energizing Rune (Electricity)|Efecto: Runa energizante (Electricidad)|modificada|
|[9e6iVkPpGqJYwMyb.htm](equipment-effects/9e6iVkPpGqJYwMyb.htm)|Effect: Brewer's Regret|Efecto: Brewer's Regret|modificada|
|[9FfFhu2kl2wMTsiI.htm](equipment-effects/9FfFhu2kl2wMTsiI.htm)|Effect: Silvertongue Mutagen (Major)|Efecto: Mutágeno de pico de oro, superior.|modificada|
|[9j1uTGBGAc7GIhjm.htm](equipment-effects/9j1uTGBGAc7GIhjm.htm)|Effect: Dragonfly Potion|Efecto: Poción Libélula|modificada|
|[9keegq0GdS1eSrNr.htm](equipment-effects/9keegq0GdS1eSrNr.htm)|Effect: Sea Touch Elixir (Moderate)|Efecto: Elixir de toque del mar (moderada).|modificada|
|[9kOgG7BPEfIyWyqm.htm](equipment-effects/9kOgG7BPEfIyWyqm.htm)|Effect: Rowan Rifle (Electricity)|Efecto: Rifle Rowan (Electricidad)|modificada|
|[9MeHc072G4L8AJkp.htm](equipment-effects/9MeHc072G4L8AJkp.htm)|Effect: Elixir of Life (True)|Efecto: Elixir de la vida (verdadero)|modificada|
|[9PASRixhNM0ogqmG.htm](equipment-effects/9PASRixhNM0ogqmG.htm)|Effect: Triton's Conch|Efecto: Concha de Tritón|modificada|
|[agDVcRyoS4NTHkht.htm](equipment-effects/agDVcRyoS4NTHkht.htm)|Effect: Trinity Geode - Armor (Major)|Efecto: Geoda de la Trinidad - Armadura (superior)|modificada|
|[ah41XCrV4LFsVyzl.htm](equipment-effects/ah41XCrV4LFsVyzl.htm)|Effect: Shield of the Unified Legion|Efecto: Escudo de la Legión Unificada|modificada|
|[aIZsC56OdotiGb9M.htm](equipment-effects/aIZsC56OdotiGb9M.htm)|Effect: War Blood Mutagen (Lesser)|Efecto: Mutágeno de Sangre de Guerra (Menor)|modificada|
|[AJx8i8QX35vsG5Q4.htm](equipment-effects/AJx8i8QX35vsG5Q4.htm)|Effect: Stonethroat Ammunition (Success)|Efecto: Munición Stonethroat (Éxito)|modificada|
|[AMhUb42NAJ1aisZp.htm](equipment-effects/AMhUb42NAJ1aisZp.htm)|Effect: Stone Fist Elixir|Efecto: Elixir de pu de piedra.|modificada|
|[ApGnHnZEK7nv3IqL.htm](equipment-effects/ApGnHnZEK7nv3IqL.htm)|Effect: Greater Codex of Unimpeded Sight|Efecto: Codex mayor de visión sin obstáculos.|modificada|
|[ascxqSlMEN9R6OOy.htm](equipment-effects/ascxqSlMEN9R6OOy.htm)|Effect: Energizing Rune (Fire)|Efecto: Runa Energizante (Fuego)|modificada|
|[AUoiLqENVZlZohsn.htm](equipment-effects/AUoiLqENVZlZohsn.htm)|Effect: Spined Shield Spines|Efecto: Escudo espinado Espinas|modificada|
|[AvXNZ9I6s1H8C4wd.htm](equipment-effects/AvXNZ9I6s1H8C4wd.htm)|Effect: War Blood Mutagen (Moderate)|Efecto: Mutágeno de Sangre de Guerra (Moderada)|modificada|
|[aXDtl9vMp1vIznya.htm](equipment-effects/aXDtl9vMp1vIznya.htm)|Effect: Eye of the Unseen|Efecto: Ojo de lo Invisible|modificada|
|[b9DTIJyBT8kvIBpj.htm](equipment-effects/b9DTIJyBT8kvIBpj.htm)|Effect: Stone Body Mutagen (Greater)|Efecto: Mutágeno de cuerpo pétreo (mayor).|modificada|
|[bcxVvIbuZWOvsKcA.htm](equipment-effects/bcxVvIbuZWOvsKcA.htm)|Effect: Darkvision Elixir (Moderate)|Efecto: Elixir de visión en la oscuridad, moderada.|modificada|
|[Bg4hNMqBx0yqmWYJ.htm](equipment-effects/Bg4hNMqBx0yqmWYJ.htm)|Effect: Clockwork Goggles (Major)|Efecto: Gafas Mecánicas (Superior)|modificada|
|[bP40jr6wE6MCsRvY.htm](equipment-effects/bP40jr6wE6MCsRvY.htm)|Effect: Golden Legion Epaulet|Efecto: Epaulet Legión de Oro|modificada|
|[bri7UVNCfHhCIvXN.htm](equipment-effects/bri7UVNCfHhCIvXN.htm)|Effect: Immolation Clan Pistol|Efecto: pistola del clan de la inmolación.|modificada|
|[buJnkFBzL4e22ASp.htm](equipment-effects/buJnkFBzL4e22ASp.htm)|Effect: Gecko Potion|Efecto: Poción de geco|modificada|
|[BV8RPntjc9FUzD3g.htm](equipment-effects/BV8RPntjc9FUzD3g.htm)|Effect: Drakeheart Mutagen (Moderate)|Efecto: Mutágeno de corazón de draco (moderado).|modificada|
|[c0URo81HpSmCkuQc.htm](equipment-effects/c0URo81HpSmCkuQc.htm)|Effect: Energy Robe of Electricity|Efecto: Túnica de energía de la electricidad|modificada|
|[C9Tnl6Q7Z5Sbw5EY.htm](equipment-effects/C9Tnl6Q7Z5Sbw5EY.htm)|Effect: Energy Mutagen (Lesser)|Efecto: Mutágeno de energía (menor).|modificada|
|[ccMa75bqXo3ZnlHM.htm](equipment-effects/ccMa75bqXo3ZnlHM.htm)|Effect: Five-Feather Wreath - Armor (Major)|Efecto: Corona de cinco plumas - Armadura (Superior)|modificada|
|[cg5qyeMJUh6b4fta.htm](equipment-effects/cg5qyeMJUh6b4fta.htm)|Effect: Belt of the Five Kings (Wearer)|Efecto: Cinturón de los cinco reyes (portador).|modificada|
|[CIfqUEC0mITBjwmL.htm](equipment-effects/CIfqUEC0mITBjwmL.htm)|Effect: Sarkorian God-Caller Garb|Efecto: Sarkorian God-Caller Garb|modificada|
|[cjQHrvoXDCGOsptN.htm](equipment-effects/cjQHrvoXDCGOsptN.htm)|Effect: Flask of Fellowship|Efecto: Frasco de la Fraternidad|modificada|
|[ClsVhp5baFRjZQ23.htm](equipment-effects/ClsVhp5baFRjZQ23.htm)|Effect: Energizing Rune (Acid)|Efecto: Runa Energizante (Ácido)|modificada|
|[cozi2kUELY40Dcv3.htm](equipment-effects/cozi2kUELY40Dcv3.htm)|Effect: Malleable Mixture (Lesser)|Efecto: Mezcla Maleable (Menor)|modificada|
|[csA4UAD2tQq7RjT8.htm](equipment-effects/csA4UAD2tQq7RjT8.htm)|Effect: Tanglefoot Bag (Greater)|Efecto: Bolsa de mara (mayor).|modificada|
|[Cxa7MdgMCUoMqbKm.htm](equipment-effects/Cxa7MdgMCUoMqbKm.htm)|Effect: Bronze Bull Pendant|Efecto: Colgante de toro de bronce.|modificada|
|[cy42NXgx1vjYzSxN.htm](equipment-effects/cy42NXgx1vjYzSxN.htm)|Effect: Suit of Armoire Frustration|Efecto: Traje de Armoire Frustración|modificada|
|[d7BDxmsnM1BUoEeT.htm](equipment-effects/d7BDxmsnM1BUoEeT.htm)|Effect: Goggles of Night (Greater)|Efecto: Gafas de noche (mayores).|modificada|
|[D7teqZ68L21aZCpd.htm](equipment-effects/D7teqZ68L21aZCpd.htm)|Effect: Glittering Snare (Failure)|Efecto: Glittering Snare (Fallo)|modificada|
|[dchlrZqQ2oEmgNlN.htm](equipment-effects/dchlrZqQ2oEmgNlN.htm)|Effect: Silkspinner's Shield (Animated Strike)|Efecto: Escudo del hilandero de seda (Golpe animado).|modificada|
|[DfAyZW2vkhTygZVC.htm](equipment-effects/DfAyZW2vkhTygZVC.htm)|Effect: Prepared Camouflage Suit (Superb)|Efecto: Traje de Camuflaje Preparado (Soberbio)|modificada|
|[DlqcczhwjfaEf7G1.htm](equipment-effects/DlqcczhwjfaEf7G1.htm)|Effect: Ablative Armor Plating (Greater)|Efecto: Blindaje ablativo (Mayor)|modificada|
|[dpIrjd1UPY7EnWUD.htm](equipment-effects/dpIrjd1UPY7EnWUD.htm)|Effect: Silvertongue Mutagen (Lesser)|Efecto: Mutágeno de lengua de oro (menor).|modificada|
|[dv0IKm5syOdP759w.htm](equipment-effects/dv0IKm5syOdP759w.htm)|Effect: Frost Vial (Moderate)|Efecto: Vial de escarcha (moderada).|modificada|
|[E2uy6gqOXi1HRVBU.htm](equipment-effects/E2uy6gqOXi1HRVBU.htm)|Effect: Clockwork Goggles (Greater)|Efecto: Gafas Mecánicas (Mayor)|modificada|
|[e3RzlURndODzBnMt.htm](equipment-effects/e3RzlURndODzBnMt.htm)|Effect: Grit (Stage 1)|Efecto: Grit (Etapa 1)|modificada|
|[E4B02mJmNexQLa8F.htm](equipment-effects/E4B02mJmNexQLa8F.htm)|Effect: Inspiring Spotlight|Efecto: Foco Inspirador|modificada|
|[e6dXfbKzv5sNr1zh.htm](equipment-effects/e6dXfbKzv5sNr1zh.htm)|Effect: Vermin Repellent Agent (Major)|Efecto: Agente repelente de alimañas (superior)|modificada|
|[Ee2xfKX1yyqGIDZj.htm](equipment-effects/Ee2xfKX1yyqGIDZj.htm)|Effect: Treat Disease (Success)|Efecto: Tratar enfermedad (Éxito)|modificada|
|[eeGWTG9ZAha4IIOY.htm](equipment-effects/eeGWTG9ZAha4IIOY.htm)|Effect: Cloak of Elvenkind|Efecto: Capa élfica.|modificada|
|[eh7EqmDBDW30ShCu.htm](equipment-effects/eh7EqmDBDW30ShCu.htm)|Effect: Bravo's Brew (Lesser)|Efecto: Brebaje de Bravo (Menor)|modificada|
|[ehYmO1rFBt35zoOw.htm](equipment-effects/ehYmO1rFBt35zoOw.htm)|Effect: Server's Stew|Efecto: Guiso del servidor|modificada|
|[eNVSBXuOiAaN152C.htm](equipment-effects/eNVSBXuOiAaN152C.htm)|Effect: Energized Cartridge (Electricity)|Efecto: Cartucho Energizado (Electricidad)|modificada|
|[EpB7yJPEuG6ez4z3.htm](equipment-effects/EpB7yJPEuG6ez4z3.htm)|Effect: Elixir of Life (Lesser)|Efecto: Elixir de la vida (menor).|modificada|
|[EpNflrkmWzQ0lEb4.htm](equipment-effects/EpNflrkmWzQ0lEb4.htm)|Effect: Glaive of the Artist|Efecto: Glaive del artista|modificada|
|[eQqi3tWSHwV4SHqK.htm](equipment-effects/eQqi3tWSHwV4SHqK.htm)|Effect: South Wind's Scorch Song (Speed Boost)|Efecto: Canción abrasadora del viento del sur (Aumento de Velocidad).|modificada|
|[eSIYyxi6uTKiP6W5.htm](equipment-effects/eSIYyxi6uTKiP6W5.htm)|Effect: Improvised Weapon|Efecto: Arma Improvisada|modificada|
|[ESuBosh3t1pXEcBj.htm](equipment-effects/ESuBosh3t1pXEcBj.htm)|Effect: Treat Poison (Critical Failure)|Efecto: Tratar veneno (Fallo crítico)|modificada|
|[etJW0w4CiSFgMrWP.htm](equipment-effects/etJW0w4CiSFgMrWP.htm)|Effect: Aeon Stone (Orange Prism) (Nature)|Efecto: Piedra eón (Prisma naranja) (Naturaleza).|modificada|
|[exwQF6E1FWmuxwBc.htm](equipment-effects/exwQF6E1FWmuxwBc.htm)|Effect: Protective Barrier|Efecto: Barrera de protección.|modificada|
|[f2U0pvTwqrLYyOlC.htm](equipment-effects/f2U0pvTwqrLYyOlC.htm)|Effect: Azarim|Efecto: Azarim|modificada|
|[F8nQOLVWmpp9G5hZ.htm](equipment-effects/F8nQOLVWmpp9G5hZ.htm)|Effect: Dragon's Blood Pudding (Greater)|Efecto: Budín de Sangre de Dragón (Mayor)|modificada|
|[FbFl95WRpzrrijh3.htm](equipment-effects/FbFl95WRpzrrijh3.htm)|Effect: Aeon Stone (Orange Prism) (Religion)|Efecto: Piedra eón, prisma naranja (Religión).|modificada|
|[fbSFwwp60AuDDKpK.htm](equipment-effects/fbSFwwp60AuDDKpK.htm)|Effect: Belt of the Five Kings (Allies)|Efecto: Cinturón de los cinco reyes (Aliados).|modificada|
|[fIpzDpuwLdIS4tW5.htm](equipment-effects/fIpzDpuwLdIS4tW5.htm)|Effect: Bestial Mutagen (Lesser)|Efecto: Mutágeno bestial, menor.|modificada|
|[Fngb79C1VDGLJ1EQ.htm](equipment-effects/Fngb79C1VDGLJ1EQ.htm)|Effect: Feyfoul (Lesser)|Efecto: Feyfoul (Menor)|modificada|
|[FOZXp7QQDnny1600.htm](equipment-effects/FOZXp7QQDnny1600.htm)|Effect: Fire and Iceberg|Efecto: Fuego y Témpano|modificada|
|[fRlvmul3LbLo2xvR.htm](equipment-effects/fRlvmul3LbLo2xvR.htm)|Effect: Parry|Efecto: Parada|modificada|
|[fuQVJiPPUsvL6fi5.htm](equipment-effects/fuQVJiPPUsvL6fi5.htm)|Effect: Sulfur Bomb (Failure)|Efecto: Bomba de azufre (Fallo)|modificada|
|[fUrZ4xcMJz0CfTyG.htm](equipment-effects/fUrZ4xcMJz0CfTyG.htm)|Effect: Juggernaut Mutagen (Moderate)|Efecto: Mutágeno de juggernaut, moderado.|modificada|
|[fYe48HmFgfmcqbvL.htm](equipment-effects/fYe48HmFgfmcqbvL.htm)|Effect: Taljjae's Mask (The Hero)|Efecto: Máscara de Taljjae (El Héroe)|modificada|
|[fYjvLx9DHIdCHdDx.htm](equipment-effects/fYjvLx9DHIdCHdDx.htm)|Effect: Applereed Mutagen (Moderate)|Efecto: Applereed Mutagen (Moderada)|modificada|
|[fYZIanbYu0Vc4JEL.htm](equipment-effects/fYZIanbYu0Vc4JEL.htm)|Effect: Tanglefoot Bag (Lesser)|Efecto: Bolsa de mara, menor.|modificada|
|[Fz3cSffzDAxhCh2D.htm](equipment-effects/Fz3cSffzDAxhCh2D.htm)|Effect: Exsanguinating Ammunition|Efecto: Munición Exsanguinante|modificada|
|[G0lG7IIZnCZtYi6v.htm](equipment-effects/G0lG7IIZnCZtYi6v.htm)|Effect: Breastplate of Command|Efecto: Coraza de mando.|modificada|
|[g8JS6wsw5sRWOJLg.htm](equipment-effects/g8JS6wsw5sRWOJLg.htm)|Effect: Stalk Goggles|Efecto: Gafas de acecho|modificada|
|[GBBjw61g4ekJymT0.htm](equipment-effects/GBBjw61g4ekJymT0.htm)|Effect: Drakeheart Mutagen (Lesser)|Efecto: Mutágeno de corazón de draco (Menor).|modificada|
|[gDefAEEMXVVZgqXH.htm](equipment-effects/gDefAEEMXVVZgqXH.htm)|Effect: Celestial Armor|Efecto: Armadura celestial|modificada|
|[Gj6u2Za5okFlsTvT.htm](equipment-effects/Gj6u2Za5okFlsTvT.htm)|Effect: Deadweight Snare (Failure/Critical Failure)|Efecto: Lazo de peso muerto (Fallo/Fallo crítico)|modificada|
|[GNFNDyx8nfNXrgV6.htm](equipment-effects/GNFNDyx8nfNXrgV6.htm)|Effect: Glittering Snare (Critical Failure)|Efecto: Lazo Brillante (Fallo Crítico)|modificada|
|[h0Zh8tDF9zJBHZXA.htm](equipment-effects/h0Zh8tDF9zJBHZXA.htm)|Effect: Flaming Star - Weapon (Major)|Efecto: Estrella Flamígera - Arma (superior).|modificada|
|[H29JukjrSpHe5DXR.htm](equipment-effects/H29JukjrSpHe5DXR.htm)|Effect: Impossible Cake|Efecto: Tarta Imposible|modificada|
|[haywlcUtG6hV1LAy.htm](equipment-effects/haywlcUtG6hV1LAy.htm)|Effect: Trinity Geode - Armor (Greater)|Efecto: Geoda de la Trinidad - Armadura (Mayor)|modificada|
|[HaZ5LB1wh1LY5wUy.htm](equipment-effects/HaZ5LB1wh1LY5wUy.htm)|Effect: Potion of Minute Echoes|Efecto: Poción de Ecos Minutos|modificada|
|[hD0dUWYKM8FrVDZY.htm](equipment-effects/hD0dUWYKM8FrVDZY.htm)|Effect: Crown of the Kobold King|Efecto: Corona del Rey Kobold|modificada|
|[HeRHBo2NaKy5IxhU.htm](equipment-effects/HeRHBo2NaKy5IxhU.htm)|Effect: Antiplague (Moderate)|Efecto: Antiplaga (moderado)|modificada|
|[Hnt3Trd7TiFICB06.htm](equipment-effects/Hnt3Trd7TiFICB06.htm)|Effect: Vermin Repellent Agent (Moderate)|Efecto: Agente repelente de alimañas (moderado)|modificada|
|[HoZWmT4yvGso7pHM.htm](equipment-effects/HoZWmT4yvGso7pHM.htm)|Effect: Flaming Star - Weapon (Greater)|Efecto: Estrella Flamígera - Arma (Mayor).|modificada|
|[hPxrIpuL54XRlA2h.htm](equipment-effects/hPxrIpuL54XRlA2h.htm)|Effect: Earplugs|Efecto: Tapones para los oídos|modificada|
|[Hx4MOTujp5z6SlQu.htm](equipment-effects/Hx4MOTujp5z6SlQu.htm)|Effect: Arboreal's Revenge (Speed Penalty)|Efecto: Venganza arboral (Penalizador de Velocidad).|modificada|
|[hy6LAC13QIJNDYXm.htm](equipment-effects/hy6LAC13QIJNDYXm.htm)|Effect: South Wind's Scorch Song (Damage)|Efecto: Canción abrasadora del viento del sur (daño).|modificada|
|[i0tm2ZHekp7rGGR3.htm](equipment-effects/i0tm2ZHekp7rGGR3.htm)|Effect: Stole of Civility|Efecto: Sustraer de Civilidad|modificada|
|[id20P4pj7zDKeLmy.htm](equipment-effects/id20P4pj7zDKeLmy.htm)|Effect: Treat Disease (Critical Success)|Efecto: Tratar enfermedad (Éxito crítico)|modificada|
|[iEkH8BKLMUa2wxLX.htm](equipment-effects/iEkH8BKLMUa2wxLX.htm)|Effect: Glamorous Buckler|Efecto: Glamorous Buckler|modificada|
|[iK6JeCsZwm5Vakks.htm](equipment-effects/iK6JeCsZwm5Vakks.htm)|Effect: Anklets of Alacrity|Efecto: Tobilleras de celeridad|modificada|
|[IlTS2LTwYTyGXY49.htm](equipment-effects/IlTS2LTwYTyGXY49.htm)|Effect: Feyfoul (Moderate)|Efecto: Feyfoul (Moderada)|modificada|
|[ioGzmVSmMGXWWBYb.htm](equipment-effects/ioGzmVSmMGXWWBYb.htm)|Effect: Cloak of the Bat|Efecto: Capa del murciélago.|modificada|
|[ITAFsW3dQPupJ3DW.htm](equipment-effects/ITAFsW3dQPupJ3DW.htm)|Effect: Tanglefoot Bag (Major)|Efecto: Bolsa de mara, superior.|modificada|
|[IZkHdaqWBJIIWO7F.htm](equipment-effects/IZkHdaqWBJIIWO7F.htm)|Effect: Ebon Fulcrum Lens (Reaction)|Efecto: Lente de fulcro de ébano (Reacción).|modificada|
|[J0YS8mQsQ1BmT6Xv.htm](equipment-effects/J0YS8mQsQ1BmT6Xv.htm)|Effect: Emberheart|Efecto: Emberheart|modificada|
|[j9zVZwRBVAcnpEkE.htm](equipment-effects/j9zVZwRBVAcnpEkE.htm)|Effect: Cheetah's Elixir (Moderate)|Efecto: Elixir del guepardo (moderada)|modificada|
|[Ja3PlqLuD9aSaPNZ.htm](equipment-effects/Ja3PlqLuD9aSaPNZ.htm)|Effect: Brewer's Regret (Greater)|Efecto: Arrepentimiento del Cervecero (Mayor)|modificada|
|[jaBMZKdoywOTrQvP.htm](equipment-effects/jaBMZKdoywOTrQvP.htm)|Effect: Cognitive Mutagen (Lesser)|Efecto: Mutágeno cognitivo (Menor).|modificada|
|[JbJykktAYMR4BRav.htm](equipment-effects/JbJykktAYMR4BRav.htm)|Effect: Energy Mutagen (Greater)|Efecto: Mutágeno de energía (mayor)|modificada|
|[jgaDboqENQJaS1sW.htm](equipment-effects/jgaDboqENQJaS1sW.htm)|Effect: Prepared Camouflage Suit|Efecto: Traje de Camuflaje Preparado|modificada|
|[jlVYoiPVRRVGBj5G.htm](equipment-effects/jlVYoiPVRRVGBj5G.htm)|Effect: Ablative Armor Plating (Lesser)|Efecto: Blindaje ablativo (Menor)|modificada|
|[JnnyamqQrAEcyI6F.htm](equipment-effects/JnnyamqQrAEcyI6F.htm)|Effect: Grim Sandglass - Armor (Greater)|Efecto: Reloj de arena sombrío - Armadura (mayor)|modificada|
|[jw6Tr9FbErjLAFLQ.htm](equipment-effects/jw6Tr9FbErjLAFLQ.htm)|Effect: Serene Mutagen (Greater)|Efecto: Mutágeno sereno, mayor.|modificada|
|[K21XQMoDVSPqzRla.htm](equipment-effects/K21XQMoDVSPqzRla.htm)|Effect: Sage's Lash (Turquoise)|Efecto: Sage's Lash (Turquesa)|modificada|
|[KAEWiyE8TQwofNj9.htm](equipment-effects/KAEWiyE8TQwofNj9.htm)|Effect: Impossible Cake (Greater)|Efecto: Pastel Imposible (Mayor)|modificada|
|[KFnOWk5e7nwXT8IE.htm](equipment-effects/KFnOWk5e7nwXT8IE.htm)|Effect: Feyfoul (Greater)|Efecto: Feyfoul (Mayor)|modificada|
|[kgEOxqF1q4Sy6r97.htm](equipment-effects/kgEOxqF1q4Sy6r97.htm)|Effect: Flaming Star - Armor (Major)|Efecto: Estrella Flamígera - Armadura (superior).|modificada|
|[kgotU0sFmtAHYySB.htm](equipment-effects/kgotU0sFmtAHYySB.htm)|Effect: Eagle Eye Elixir (Greater)|Efecto: Elixir de ojo de águila, mayor.|modificada|
|[KiurLemTV8GV7OyM.htm](equipment-effects/KiurLemTV8GV7OyM.htm)|Effect: Rowan Rifle (Sonic)|Efecto: Rifle Rowan (Sonic)|modificada|
|[kkDbalYEavzRpYlp.htm](equipment-effects/kkDbalYEavzRpYlp.htm)|Effect: Antiplague (Lesser)|Efecto: Antiplaga (Menor)|modificada|
|[kMPPl4AqFb6GclOL.htm](equipment-effects/kMPPl4AqFb6GclOL.htm)|Effect: Malleable Mixture (Greater)|Efecto: Mezcla maleable (mayor)|modificada|
|[KSvkfMqMQ8mlGLiz.htm](equipment-effects/KSvkfMqMQ8mlGLiz.htm)|Effect: Goggles of Night|Efecto: Gafas de noche|modificada|
|[kwD0wuW5Ndkc9YXB.htm](equipment-effects/kwD0wuW5Ndkc9YXB.htm)|Effect: Bestial Mutagen (Greater)|Efecto: Mutágeno bestial (mayor).|modificada|
|[kwOtHtmlH69ctK0O.htm](equipment-effects/kwOtHtmlH69ctK0O.htm)|Effect: Sun Orchid Poultice|Efecto: Cataplasma de Orquídea del Sol|modificada|
|[kyLLXUQ9zSEvC4py.htm](equipment-effects/kyLLXUQ9zSEvC4py.htm)|Effect: Stalk Goggles (Greater)|Efecto: Gafas de tallo (mayor)|modificada|
|[lBMhT2W2raYMa8JS.htm](equipment-effects/lBMhT2W2raYMa8JS.htm)|Effect: Spellguard Shield|Efecto: Escudo de guardia contra conjuros.|modificada|
|[lgvjbbQiHBGKR3C6.htm](equipment-effects/lgvjbbQiHBGKR3C6.htm)|Effect: Rhino Shot|Efecto: Disparo de Rinoceronte|modificada|
|[LH0IDLLF4RsT3KvM.htm](equipment-effects/LH0IDLLF4RsT3KvM.htm)|Effect: Energized Cartridge (Fire)|Efecto: Cartucho Energizado (Fuego)|modificada|
|[lLP56tbG689TNKW5.htm](equipment-effects/lLP56tbG689TNKW5.htm)|Effect: Bracelet of Dashing|Efecto: Brazalete de celeridad|modificada|
|[lNWACCNe9RYgaFxb.htm](equipment-effects/lNWACCNe9RYgaFxb.htm)|Effect: Cheetah's Elixir (Lesser)|Efecto: Elixir del Guepardo (Menor)|modificada|
|[lO95TwgihBdTilAB.htm](equipment-effects/lO95TwgihBdTilAB.htm)|Effect: Thurible of Revelation|Efecto: Thurible de Revelación|modificada|
|[lPRuIRbu0rHBkoKY.htm](equipment-effects/lPRuIRbu0rHBkoKY.htm)|Effect: Elixir of Life (Minor)|Efecto: Elixir de la vida (Menor)|modificada|
|[LVy8SfUF8Jrd6X18.htm](equipment-effects/LVy8SfUF8Jrd6X18.htm)|Effect: Leopard's Armor|Efecto: Armadura de Leopardo|modificada|
|[M0hhLRC86sASVOk7.htm](equipment-effects/M0hhLRC86sASVOk7.htm)|Effect: Tteokguk of Time Advancement|Efecto: Tteokguk de Avance en el Tiempo|modificada|
|[M3EFomnN5xArdQmV.htm](equipment-effects/M3EFomnN5xArdQmV.htm)|Effect: Moderate Healer's Gel|Efecto: Gel de Sanador Moderado|modificada|
|[m4WpxepWRV1u1Kcw.htm](equipment-effects/m4WpxepWRV1u1Kcw.htm)|Effect: Grim Sandglass - Weapon|Efecto: Grim Sandglass - Arma|modificada|
|[MCny5ohCGf09a7Wl.htm](equipment-effects/MCny5ohCGf09a7Wl.htm)|Effect: Salve of Slipperiness|Efecto: Ungüento de resbalamiento.|modificada|
|[MEreOgnjoRiXPEuq.htm](equipment-effects/MEreOgnjoRiXPEuq.htm)|Effect: Tanglefoot Bag (Moderate)|Efecto: Bolsa de mara (moderada).|modificada|
|[Mf9EBLhYmZerf0nS.htm](equipment-effects/Mf9EBLhYmZerf0nS.htm)|Effect: Potion of Flying (Standard)|Efecto: Poción de volar (normal).|modificada|
|[mG6S6zm6hxaF7Tla.htm](equipment-effects/mG6S6zm6hxaF7Tla.htm)|Effect: Skeptic's Elixir (Moderate)|Efecto: Elixir del escéptico (moderada).|modificada|
|[mi4Md1fB2XThCand.htm](equipment-effects/mi4Md1fB2XThCand.htm)|Effect: Antidote (Moderate)|Efecto: Antídoto (Moderado)|modificada|
|[MI5OCkF9IXmD2lPF.htm](equipment-effects/MI5OCkF9IXmD2lPF.htm)|Effect: Bloodhound Mask (Greater)|Efecto: Máscara de sabueso (mayor).|modificada|
|[mkjcgwDBeaOUolVe.htm](equipment-effects/mkjcgwDBeaOUolVe.htm)|Effect: Major Fanged Rune Animal Form|Efecto: Forma de animal de runa colmilluda superior.|modificada|
|[mn39aML7EWKbttKT.htm](equipment-effects/mn39aML7EWKbttKT.htm)|Effect: Ablative Armor Plating (Moderate)|Efecto: Blindaje Ablativo (Moderado)|modificada|
|[ModBoFdCi7YQU4gP.htm](equipment-effects/ModBoFdCi7YQU4gP.htm)|Effect: Potion of Swimming (Greater)|Efecto: Poción de Nadar (Mayor).|modificada|
|[mQJk8R0vHzvpTz0e.htm](equipment-effects/mQJk8R0vHzvpTz0e.htm)|Effect: Chatterer of Follies|Efecto: Charlatán de Locuras|modificada|
|[mrwg2XftLtSLj197.htm](equipment-effects/mrwg2XftLtSLj197.htm)|Effect: Exsanguinating Ammunition (Major)|Efecto: Munición desangradora (superior).|modificada|
|[na2gf5mSkilFoHXk.htm](equipment-effects/na2gf5mSkilFoHXk.htm)|Effect: Energy Mutagen (Moderate)|Efecto: Mutágeno de energía (moderada).|modificada|
|[Nbgf8zvHimdQqIu6.htm](equipment-effects/Nbgf8zvHimdQqIu6.htm)|Effect: Skyrider Sword|Efecto: Skyrider Sword|modificada|
|[NdfhpKCjSS80LiUz.htm](equipment-effects/NdfhpKCjSS80LiUz.htm)|Effect: Nosoi Charm (Greater) (Diplomacy)|Efecto: Hechizo nosoi (mayor) (Diplomacia).|modificada|
|[NE7Fm5YnUhD4ySX3.htm](equipment-effects/NE7Fm5YnUhD4ySX3.htm)|Effect: Earplugs (PFS Guide)|Efecto: Tapones para los oídos (Guía PFS)|modificada|
|[neZPoQF4hW3A31dd.htm](equipment-effects/neZPoQF4hW3A31dd.htm)|Effect: Lastwall Soup (Greater)|Efecto: Lastwall Soup (Mayor)|modificada|
|[nJRoiSyd67eQ1dYj.htm](equipment-effects/nJRoiSyd67eQ1dYj.htm)|Effect: Frost Vial (Greater)|Efecto: Vial de escarcha (mayor).|modificada|
|[no7vnIiNBwWjh3w8.htm](equipment-effects/no7vnIiNBwWjh3w8.htm)|Effect: Grasp of Droskar|Efecto: Agarre de Droskar|modificada|
|[nQ6vM1CRLyvQdGLG.htm](equipment-effects/nQ6vM1CRLyvQdGLG.htm)|Effect: Five-Feather Wreath - Armor|Efecto: Corona de cinco plumas - Armadura|modificada|
|[NYOi1F9cW3axHrdc.htm](equipment-effects/NYOi1F9cW3axHrdc.htm)|Effect: Deadweight Snare (Success)|Efecto: Deadweight Snare (Éxito)|modificada|
|[oAewXfq9c0ecaSfw.htm](equipment-effects/oAewXfq9c0ecaSfw.htm)|Effect: Silvertongue Mutagen (Greater)|Efecto: Mutágeno de lengua de oro (mayor).|modificada|
|[OAN5Fj21PJPhIqRU.htm](equipment-effects/OAN5Fj21PJPhIqRU.htm)|Effect: Vermin Repellent Agent (Lesser)|Efecto: Agente repelente de alimañas (menor)|modificada|
|[oaR6YGiZKg8a2971.htm](equipment-effects/oaR6YGiZKg8a2971.htm)|Effect: Clockwork Goggles|Efecto: Gafas Mecánicas|modificada|
|[ohMdE8BmQHuLs40b.htm](equipment-effects/ohMdE8BmQHuLs40b.htm)|Effect: Impossible Cake (Major)|Efecto: Pastel Imposible (Superior)|modificada|
|[oiO3cQfqp8MuxR82.htm](equipment-effects/oiO3cQfqp8MuxR82.htm)|Effect: Blast Boots (Major)|Efecto: Botas explosivas (superior)|modificada|
|[OMW71UJzYCUr4ubh.htm](equipment-effects/OMW71UJzYCUr4ubh.htm)|Effect: Ablative Armor Plating (Major)|Efecto: Blindaje ablativo (Superior)|modificada|
|[oqwrw6XztVlS9tEG.htm](equipment-effects/oqwrw6XztVlS9tEG.htm)|Effect: Trinity Geode - Armor|Efecto: Geoda de la Trinidad - Armadura|modificada|
|[ORdhj3IAvYACNGkJ.htm](equipment-effects/ORdhj3IAvYACNGkJ.htm)|Effect: Shrine Inarizushi|Efecto: Santuario Inarizushi|modificada|
|[OxCVZSvWVJsOGAZN.htm](equipment-effects/OxCVZSvWVJsOGAZN.htm)|Effect: Flaming Star - Weapon|Efecto: Estrella Flamígera - Arma.|modificada|
|[p2aGtovaY1feytws.htm](equipment-effects/p2aGtovaY1feytws.htm)|Effect: Aeon Stone (Black Pearl)|Efecto: Piedra Aeon (Perla Negra)|modificada|
|[P7Y7pO2ulZ5wBgxU.htm](equipment-effects/P7Y7pO2ulZ5wBgxU.htm)|Effect: Barding of the Zephyr|Efecto: Bardas del céfiro.|modificada|
|[p9jROgkqozXB52UJ.htm](equipment-effects/p9jROgkqozXB52UJ.htm)|Effect: Trinity Geode - Weapon (Greater)|Efecto: Geoda de la Trinidad - Arma (Mayor)|modificada|
|[pAMyEbJzWBoYoGhs.htm](equipment-effects/pAMyEbJzWBoYoGhs.htm)|Effect: Diplomat's Badge|Efecto: Insignia de Diplomático|modificada|
|[PeiuJ951kkBPTCSM.htm](equipment-effects/PeiuJ951kkBPTCSM.htm)|Effect: Bracers of Missile Deflection|Efecto: Brazales de desvío de proyectiles.|modificada|
|[PEPOd38VfVzQMKG5.htm](equipment-effects/PEPOd38VfVzQMKG5.htm)|Effect: Stone Body Mutagen (Lesser)|Efecto: Mutágeno de cuerpo pétreo (Menor)|modificada|
|[PeuUz7JaabCgl6Yh.htm](equipment-effects/PeuUz7JaabCgl6Yh.htm)|Effect: Cheetah's Elixir (Greater)|Efecto: Elixir del guepardo (mayor)|modificada|
|[Pkk8m79MoT1RgtfW.htm](equipment-effects/Pkk8m79MoT1RgtfW.htm)|Effect: Succubus Kiss (Stage 1)|Efecto: Beso de súcubo (Etapa 1)|modificada|
|[pmWJxDjz3gqL29OM.htm](equipment-effects/pmWJxDjz3gqL29OM.htm)|Effect: Bottled Omen|Efecto: Presagio embotellado|modificada|
|[PpLxndUSgzgs6dd0.htm](equipment-effects/PpLxndUSgzgs6dd0.htm)|Effect: Elixir of Life (Major)|Efecto: Elixir de la vida (superior).|modificada|
|[PuWZyFzJCkbq1Inj.htm](equipment-effects/PuWZyFzJCkbq1Inj.htm)|Effect: Flaming Star - Armor|Efecto: Estrella Flamígera - Armadura.|modificada|
|[q1EhQ716bPSgJVnC.htm](equipment-effects/q1EhQ716bPSgJVnC.htm)|Effect: Bravo's Brew (Greater)|Efecto: Bravo's Brew (Mayor)|modificada|
|[q58ahUEjUzTXffRN.htm](equipment-effects/q58ahUEjUzTXffRN.htm)|Effect: Perfect Droplet - Armor (Greater)|Efecto: Gota Perfecta - Armadura (Mayor)|modificada|
|[QapoFh0tbUgMwSIB.htm](equipment-effects/QapoFh0tbUgMwSIB.htm)|Effect: Thurible of Revelation (Greater)|Efecto: Incensario de revelación, mayor.|modificada|
|[qit1mLbJUyRTYcPU.htm](equipment-effects/qit1mLbJUyRTYcPU.htm)|Effect: Cognitive Mutagen (Greater)|Efecto: Mutágeno cognitivo (mayor).|modificada|
|[qOBdeZ4FXYc5qHsm.htm](equipment-effects/qOBdeZ4FXYc5qHsm.htm)|Effect: Private Workshop (Using for Crafting)|Efecto: Taller Privado (Usar para Elaborar Artesanía).|modificada|
|[QrsPKOFuo3qzgxw5.htm](equipment-effects/QrsPKOFuo3qzgxw5.htm)|Effect: Red-Rib Gill Mask (Greater)|Efecto: Red-Rib Gill Mask (Mayor)|modificada|
|[QuZ5frBMJF3gi7RY.htm](equipment-effects/QuZ5frBMJF3gi7RY.htm)|Effect: Antidote (Greater)|Efecto: Antídoto (Mayor).|modificada|
|[qVKrrKpTghgMIuGH.htm](equipment-effects/qVKrrKpTghgMIuGH.htm)|Effect: Antiplague (Major)|Efecto: Antiplaga (Superior)|modificada|
|[qwoLV4awdezlEJ60.htm](equipment-effects/qwoLV4awdezlEJ60.htm)|Effect: Drakeheart Mutagen (Greater)|Efecto: Mutágeno de corazón de draco (mayor).|modificada|
|[QXJLvL2k3WqlF0SN.htm](equipment-effects/QXJLvL2k3WqlF0SN.htm)|Effect: Grim Sandglass - Armor (Major)|Efecto: Grim Sandglass - Armadura (superior)|modificada|
|[qzRcSQ0HTTp58hV2.htm](equipment-effects/qzRcSQ0HTTp58hV2.htm)|Effect: Sixfingers Elixir (Moderate)|Efecto: Elíxir de seis dedos (moderada).|modificada|
|[R106i7WCXvHLGMTu.htm](equipment-effects/R106i7WCXvHLGMTu.htm)|Effect: Antiplague (Greater)|Efecto: Antiplaga (Mayor)|modificada|
|[R1kZsBMZdGZ3ATkA.htm](equipment-effects/R1kZsBMZdGZ3ATkA.htm)|Effect: Flaming Star - Armor (Greater)|Efecto: Estrella Flamígera - Armadura (mayor).|modificada|
|[R5ugeFK3MPwkbv0s.htm](equipment-effects/R5ugeFK3MPwkbv0s.htm)|Effect: Potency Crystal|Efecto: Cristal de potencia.|modificada|
|[r6hDgfVLod0AmU7J.htm](equipment-effects/r6hDgfVLod0AmU7J.htm)|Effect: Heartripper Blade|Efecto: Hoja Rompecorazones|modificada|
|[rH6RHxy6sNTLusKX.htm](equipment-effects/rH6RHxy6sNTLusKX.htm)|Effect: Emerald Fulcrum Lens (Saving Throw)|Efecto: Lente de fulcro esmeralda (tirada de salvación).|modificada|
|[RIozNOntRJok5ZJt.htm](equipment-effects/RIozNOntRJok5ZJt.htm)|Effect: Energy Mutagen (Major)|Efecto: Mutágeno de energía (superior).|modificada|
|[RLsdvhmTh64Mmty9.htm](equipment-effects/RLsdvhmTh64Mmty9.htm)|Effect: Frost Vial (Lesser)|Efecto: Vial de escarcha (menor).|modificada|
|[RRusoN3HEGnDO1Dg.htm](equipment-effects/RRusoN3HEGnDO1Dg.htm)|Effect: Sea Touch Elixir (Greater)|Efecto: Elixir de toque del mar (mayor).|modificada|
|[RT1BxXrbbGgk40Ti.htm](equipment-effects/RT1BxXrbbGgk40Ti.htm)|Effect: Cognitive Mutagen (Major)|Efecto: Mutágeno cognitivo, superior.|modificada|
|[rVFDLzYrJVYLiQBL.htm](equipment-effects/rVFDLzYrJVYLiQBL.htm)|Effect: Qat (Stage 1)|Efecto: Qat (Etapa 1)|modificada|
|[rXM6njevpwqSMNRt.htm](equipment-effects/rXM6njevpwqSMNRt.htm)|Effect: Tallowheart Mass|Efecto: Masa de Corazón de Sebo|modificada|
|[S3Sv7SYwxozbG554.htm](equipment-effects/S3Sv7SYwxozbG554.htm)|Effect: War Blood Mutagen (Major)|Efecto: Mutágeno de sangre de guerra (superior)|modificada|
|[S4MZzALqFoXJsr6o.htm](equipment-effects/S4MZzALqFoXJsr6o.htm)|Effect: Bloodhound Mask (Lesser)|Efecto: Máscara de sabueso (menor).|modificada|
|[s95P3L72BDKvzYhn.htm](equipment-effects/s95P3L72BDKvzYhn.htm)|Effect: Curse of Potent Poison|Efecto: Maldición de Veneno Potente|modificada|
|[SbYcOry1cxbndSve.htm](equipment-effects/SbYcOry1cxbndSve.htm)|Effect: Silkspinner's Shield (Climb)|Efecto: Escudo del hilandero de seda (Trepar)|modificada|
|[sOwAqyQ6MaoSqaY1.htm](equipment-effects/sOwAqyQ6MaoSqaY1.htm)|Effect: Lesser Healer's Gel|Efecto: Gel de sanador menor.|modificada|
|[SqN1FGSgdNlyvRu9.htm](equipment-effects/SqN1FGSgdNlyvRu9.htm)|Effect: Containment Contraption|Efecto: Artilugio de Contención|modificada|
|[T27uDXdMVc5ZFwKw.htm](equipment-effects/T27uDXdMVc5ZFwKw.htm)|Effect: Enhanced Hearing Aids|Efecto: Audífonos mejorados|modificada|
|[t7VUJHSUT6bkVUjg.htm](equipment-effects/t7VUJHSUT6bkVUjg.htm)|Effect: Serene Mutagen (Major)|Efecto: Mutágeno sereno, superior.|modificada|
|[tcHG8NlsYmHdziko.htm](equipment-effects/tcHG8NlsYmHdziko.htm)|Effect: Grim Sandglass - Weapon (Major)|Efecto: Reloj de arena sombrío - Arma (superior)|modificada|
|[tGeMT4iHJcsjVbl3.htm](equipment-effects/tGeMT4iHJcsjVbl3.htm)|Effect: Metuak's Pendant|Efecto: Colgante de Metuak|modificada|
|[thOpQunbQr77XWdF.htm](equipment-effects/thOpQunbQr77XWdF.htm)|Effect: Sea Touch Elixir (Lesser)|Efecto: Elixir de toque del mar (menor).|modificada|
|[Tioloj3bTlFnQDqa.htm](equipment-effects/Tioloj3bTlFnQDqa.htm)|Effect: Perfect Droplet - Armor (Major)|Efecto: Gota perfecta - Armadura (superior)|modificada|
|[TkRuKKYyPHTGPfgf.htm](equipment-effects/TkRuKKYyPHTGPfgf.htm)|Effect: Sixfingers Elixir (Greater)|Efecto: Elíxir de seis dedos (mayor).|modificada|
|[tLGzSCcfxflLSqzw.htm](equipment-effects/tLGzSCcfxflLSqzw.htm)|Effect: Energized Cartridge (Cold)|Efecto: Cartucho Energizado (Frío)|modificada|
|[TsWUTODTVi487SEz.htm](equipment-effects/TsWUTODTVi487SEz.htm)|Effect: Skeptic's Elixir (Greater)|Efecto: Elixir del escéptico (Mayor)|modificada|
|[tTBJ33UGtzXjWOJp.htm](equipment-effects/tTBJ33UGtzXjWOJp.htm)|Effect: Applereed Mutagen (Greater)|Efecto: Applereed Mutagen (Mayor)|modificada|
|[u35Qzft0c84UySq2.htm](equipment-effects/u35Qzft0c84UySq2.htm)|Effect: Ebon Fulcrum Lens (2 Action)|Efecto: Lente de fulcro de ébano (2 acciones).|modificada|
|[u7200u7lh40am0jb.htm](equipment-effects/u7200u7lh40am0jb.htm)|Effect: Trinity Geode - Weapon (Major)|Efecto: Geoda de la Trinidad - Arma (superior)|modificada|
|[Uadsb25G18pKdZ2e.htm](equipment-effects/Uadsb25G18pKdZ2e.htm)|Effect: Clandestine Cloak|Efecto: Capa clandestina|modificada|
|[uC6KjfiWrTBXYtP8.htm](equipment-effects/uC6KjfiWrTBXYtP8.htm)|Effect: Storm Chair|Efecto: Silla de tormenta|modificada|
|[UDfVCATxdLdSzJYJ.htm](equipment-effects/UDfVCATxdLdSzJYJ.htm)|Effect: Red-Rib Gill Mask (Lesser)|Efecto: Red-Rib Gill Mask (Menor)|modificada|
|[UlalLihKzDxcOdXL.htm](equipment-effects/UlalLihKzDxcOdXL.htm)|Effect: Thurible of Revelation (Moderate)|Efecto: Incensario de revelación, moderado.|modificada|
|[UTtX0xLGYci6P43I.htm](equipment-effects/UTtX0xLGYci6P43I.htm)|Effect: Mudrock Snare (Failure)|Efecto: Lazo de Barro (Fallo)|modificada|
|[uVxs1qFMQsGWXNs6.htm](equipment-effects/uVxs1qFMQsGWXNs6.htm)|Effect: Potion of Stable Form|Efecto: Poción de Forma Estable|modificada|
|[uXEp1rPU5fY4OiBf.htm](equipment-effects/uXEp1rPU5fY4OiBf.htm)|Effect: Clandestine Cloak (Greater)|Efecto: Capa clandestina (mayor).|modificada|
|[UzSrsR9S2pgMDbbp.htm](equipment-effects/UzSrsR9S2pgMDbbp.htm)|Effect: Cape of the Open Sky|Efecto: Capa del Cielo Abierto|modificada|
|[V4JoVnOfKze8cRan.htm](equipment-effects/V4JoVnOfKze8cRan.htm)|Effect: Grim Sandglass - Armor|Efecto: Grim Sandglass - Armadura|modificada|
|[v5Ht1V4MZvRKRBjL.htm](equipment-effects/v5Ht1V4MZvRKRBjL.htm)|Effect: Silvertongue Mutagen (Moderate)|Efecto: Mutágeno de lengua de plata (moderada).|modificada|
|[VCypzSu659eC6jNi.htm](equipment-effects/VCypzSu659eC6jNi.htm)|Effect: Eagle Eye Elixir (Lesser)|Efecto: Elixir de ojo de águila (menor).|modificada|
|[vFOr2JAJxiVvvn2E.htm](equipment-effects/vFOr2JAJxiVvvn2E.htm)|Effect: Drakeheart Mutagen (Major)|Efecto: Mutágeno de corazón de draco (superior).|modificada|
|[vH4bEu3EnAhNpKEQ.htm](equipment-effects/vH4bEu3EnAhNpKEQ.htm)|Effect: Dragonscale Amulet|Efecto: Amuleto Dragonscale|modificada|
|[viCX9fZzTWGuoO85.htm](equipment-effects/viCX9fZzTWGuoO85.htm)|Effect: Cloak of Elvenkind (Greater)|Efecto: Capa élfica (mayor).|modificada|
|[VIzeuA9tQEQ7V1Ib.htm](equipment-effects/VIzeuA9tQEQ7V1Ib.htm)|Effect: Wand of Fey Flames|Efecto: Varita de llamas feígera.|modificada|
|[vj2hkcSbwwRYNLk5.htm](equipment-effects/vj2hkcSbwwRYNLk5.htm)|Effect: Jack's Tattered Cape|Efecto: Capa Andrajosa de Jack|modificada|
|[vKooOkXHvtqCgZYg.htm](equipment-effects/vKooOkXHvtqCgZYg.htm)|Effect: Energized Cartridge (Acid)|Efecto: Cartucho Energizado (Ácido)|modificada|
|[VlfuBfWkygsG8u5h.htm](equipment-effects/VlfuBfWkygsG8u5h.htm)|Effect: Blaze|Efecto: Blaze|modificada|
|[vOgD9wfStLX1utte.htm](equipment-effects/vOgD9wfStLX1utte.htm)|Effect: Skyrider Sword (Greater)|Efecto: Skyrider Espada (Mayor)|modificada|
|[vOU4Yv2MyAfYBbmF.htm](equipment-effects/vOU4Yv2MyAfYBbmF.htm)|Effect: Aeon Stone (Orange Prism) (Occultism)|Efecto: Piedra eón (prisma naranja) (Ocultismo).|modificada|
|[VPtsrpbP0AE642al.htm](equipment-effects/VPtsrpbP0AE642al.htm)|Effect: Quicksilver Mutagen (Moderate)|Efecto: Mutágeno de mercurio (moderado).|modificada|
|[VrYfR2WuyA15zFhq.htm](equipment-effects/VrYfR2WuyA15zFhq.htm)|Effect: Vermin Repellent Agent (Greater)|Efecto: Agente repelente de alimañas (mayor)|modificada|
|[VsHhBBLApZsOCJRL.htm](equipment-effects/VsHhBBLApZsOCJRL.htm)|Effect: Fire and Iceberg (Greater)|Efecto: Fuego y Témpano (Mayor)|modificada|
|[VVWvXiNudYYGV9sJ.htm](equipment-effects/VVWvXiNudYYGV9sJ.htm)|Effect: Nosoi Charm (Lifesense)|Efecto: Hechizo Nosoi (sentido de la vida)|modificada|
|[VZCcjwsQX1wnYlTn.htm](equipment-effects/VZCcjwsQX1wnYlTn.htm)|Effect: Perfect Droplet - Armor|Efecto: Gota Perfecta - Armadura|modificada|
|[vZPyQAt5T2L0Dfmq.htm](equipment-effects/vZPyQAt5T2L0Dfmq.htm)|Effect: Topology Protoplasm|Efecto: Topología Protoplasma|modificada|
|[w0LUnfS2whVhDBUF.htm](equipment-effects/w0LUnfS2whVhDBUF.htm)|Effect: Glittering Snare (Success)|Efecto: Glittering Snare (Éxito)|modificada|
|[W3BCLbX6j1IqL0uB.htm](equipment-effects/W3BCLbX6j1IqL0uB.htm)|Effect: Slippers of Spider Climbing|Efecto: Zapatillas de escalada de ara.|modificada|
|[W3xQBLj5hLOtb6Tj.htm](equipment-effects/W3xQBLj5hLOtb6Tj.htm)|Effect: Potion of Swimming (Moderate)|Efecto: Poción de nadar (moderada).|modificada|
|[W9tKQlA7tVIcAuzw.htm](equipment-effects/W9tKQlA7tVIcAuzw.htm)|Effect: Greater Potion of Stable Form|Efecto: Poción Mayor de Forma Estable|modificada|
|[Wa4317cqU4lJ8vAQ.htm](equipment-effects/Wa4317cqU4lJ8vAQ.htm)|Effect: Eagle Eye Elixir (Moderate)|Efecto: Elixir de ojo de águila (moderado).|modificada|
|[wacGBDbbQ1HaNZbX.htm](equipment-effects/wacGBDbbQ1HaNZbX.htm)|Effect: Hyldarf's Fang|Efecto: Colmillo de Hyldarf|modificada|
|[wFF0SZs1Hcf87Kk1.htm](equipment-effects/wFF0SZs1Hcf87Kk1.htm)|Effect: Bloodhound Mask (Moderate)|Efecto: Máscara de sabueso (moderada).|modificada|
|[wFP3SqPoO0bCPmyK.htm](equipment-effects/wFP3SqPoO0bCPmyK.htm)|Effect: Kraken's Guard|Efecto: Guardia del Kraken|modificada|
|[WHAp9cDOqnJ1VCcg.htm](equipment-effects/WHAp9cDOqnJ1VCcg.htm)|Effect: Orchestral Brooch|Efecto: Broche Orquestal|modificada|
|[WMKjWH4gyUrky4Hy.htm](equipment-effects/WMKjWH4gyUrky4Hy.htm)|Effect: Demon Dust (Stage 1)|Efecto: Polvo de demoníaco (Etapa 1)|modificada|
|[wNCxSxruzLVGtLE4.htm](equipment-effects/wNCxSxruzLVGtLE4.htm)|Effect: Spiderfoot Brew (Lesser)|Efecto: Brebaje de Pie Araña (Menor)|modificada|
|[WQ0DxUzMgAvo0zy9.htm](equipment-effects/WQ0DxUzMgAvo0zy9.htm)|Effect: Apricot of Bestial Might|Efecto: Albaricoque de Poder Bestial|modificada|
|[WRV0XjiEHdlBpduS.htm](equipment-effects/WRV0XjiEHdlBpduS.htm)|Effect: Trinity Geode - Weapon|Efecto: Geoda de la Trinidad - Arma|modificada|
|[wTZnKkT0K4Tdy8mD.htm](equipment-effects/wTZnKkT0K4Tdy8mD.htm)|Effect: Bravo's Brew (Moderate)|Efecto: Bravo's Brew (Moderada)|modificada|
|[WXrqEuLT4uP48Bvo.htm](equipment-effects/WXrqEuLT4uP48Bvo.htm)|Effect: Goggles of Night (Major)|Efecto: Gafas de noche (superior).|modificada|
|[WXsWkFosSGBrptwF.htm](equipment-effects/WXsWkFosSGBrptwF.htm)|Effect: Ivory Baton|Efecto: Bastón de Marfil|modificada|
|[wyLEew86nhNUXASu.htm](equipment-effects/wyLEew86nhNUXASu.htm)|Effect: Eagle Eye Elixir (Major)|Efecto: Elixir de ojo de águila (superior).|modificada|
|[X2ZZgTqanpoCMDmd.htm](equipment-effects/X2ZZgTqanpoCMDmd.htm)|Effect: Taljjae's Mask (The General)|Efecto: Máscara de Taljjae (El General)|modificada|
|[xFQRiVU6h8EA6Lw9.htm](equipment-effects/xFQRiVU6h8EA6Lw9.htm)|Effect: Bestial Mutagen (Moderate)|Efecto: Mutágeno bestial (moderado).|modificada|
|[XKsqQrabRlg9klGp.htm](equipment-effects/XKsqQrabRlg9klGp.htm)|Effect: Draft of Stellar Radiance|Efecto: Borrador de Resplandor Estelar|modificada|
|[XlHbUOTbK6PfBfCv.htm](equipment-effects/XlHbUOTbK6PfBfCv.htm)|Effect: Demon Dust (Stage 2)|Efecto: Polvo de Demonio (Etapa 2)|modificada|
|[xLilBqqf34ZJYO9i.htm](equipment-effects/xLilBqqf34ZJYO9i.htm)|Effect: Juggernaut Mutagen (Greater)|Efecto: Mutágeno de juggernaut (mayor).|modificada|
|[XrlChFETfe8avLsX.htm](equipment-effects/XrlChFETfe8avLsX.htm)|Effect: Sixfingers Elixir (Lesser)|Efecto: Elíxir de seis dedos (menor).|modificada|
|[xVAdPzFaSvJXPMKv.htm](equipment-effects/xVAdPzFaSvJXPMKv.htm)|Effect: Applereed Mutagen (Lesser)|Efecto: Applereed Mutagen (Menor)|modificada|
|[XwCBalKJf3CiEiFa.htm](equipment-effects/XwCBalKJf3CiEiFa.htm)|Effect: Treat Poison (Critical Success)|Efecto: Tratar veneno (Éxito crítico)|modificada|
|[XWenziR7J3mwKV4W.htm](equipment-effects/XWenziR7J3mwKV4W.htm)|Effect: Treat Poison (Success)|Efecto: Tratar veneno (Éxito)|modificada|
|[XzxADtNpUlRff972.htm](equipment-effects/XzxADtNpUlRff972.htm)|Effect: Greater Fanged Rune Animal Form|Efecto: Forma de animal de runa colmilluda mayor.|modificada|
|[YI7QQqXO6nosaAKr.htm](equipment-effects/YI7QQqXO6nosaAKr.htm)|Effect: Spiderfoot Brew (Greater)|Efecto: Brebaje de Pie Araña (Mayor)|modificada|
|[yP45Rqu4jvCfXBkp.htm](equipment-effects/yP45Rqu4jvCfXBkp.htm)|Effect: Fire and Iceberg (Major)|Efecto: Fuego y Témpano (Superior)|modificada|
|[yrbz0rZzp8aZEqbv.htm](equipment-effects/yrbz0rZzp8aZEqbv.htm)|Effect: Serene Mutagen (Moderate)|Efecto: Mutágeno sereno, moderado.|modificada|
|[yt8meGTS7wLa6Fg2.htm](equipment-effects/yt8meGTS7wLa6Fg2.htm)|Effect: Ablative Armor Plating (True)|Efecto: Blindaje ablativo (Verdadero)|modificada|
|[ytqvHyF0oMKbo65P.htm](equipment-effects/ytqvHyF0oMKbo65P.htm)|Effect: Crimson Fulcrum Lens|Efecto: Lente de fulcro carmesí.|modificada|
|[yvabfuAO74pvH8hh.htm](equipment-effects/yvabfuAO74pvH8hh.htm)|Effect: Aeon Stone (Orange Prism) (Arcana)|Efecto: Piedra eón, prisma naranja (Arcanos).|modificada|
|[Yxssrnh9UZJAM0V7.htm](equipment-effects/Yxssrnh9UZJAM0V7.htm)|Effect: Elixir of Life (Moderate)|Efecto: Elixir de la vida (moderada)|modificada|
|[z3ATL8DcRVrT0Uzt.htm](equipment-effects/z3ATL8DcRVrT0Uzt.htm)|Effect: Disarm (Success)|Efecto: Desarmar (Éxito)|modificada|
|[z8FvdsKEY4lB2L8b.htm](equipment-effects/z8FvdsKEY4lB2L8b.htm)|Effect: Phoenix Flask|Efecto: Frasco del Fénix|modificada|
|[Z9oPh462q82IYIZ6.htm](equipment-effects/Z9oPh462q82IYIZ6.htm)|Effect: Elixir of Life (Greater)|Efecto: Elixir de la vida (mayor)|modificada|
|[Zb8RYgmzCI6fQE0o.htm](equipment-effects/Zb8RYgmzCI6fQE0o.htm)|Effect: Throne Card|Efecto: Carta del Trono|modificada|
|[zd85Ny1RS46OL0TD.htm](equipment-effects/zd85Ny1RS46OL0TD.htm)|Effect: Shrinking Potion (Greater)|Efecto: Poción de disminución (mayor).|modificada|
|[Zdh2uO1vVYJmaqld.htm](equipment-effects/Zdh2uO1vVYJmaqld.htm)|Effect: Potion of Flying (Greater)|Efecto: Poción de volar (mayor).|modificada|
|[zDuuHVeHgd175pGf.htm](equipment-effects/zDuuHVeHgd175pGf.htm)|Effect: Succubus Kiss (Stage 2)|Efecto: Succubus Kiss (Etapa 2)|modificada|
|[zlSNbMDIlTOpcO8R.htm](equipment-effects/zlSNbMDIlTOpcO8R.htm)|Effect: Skinstitch Salve|Efecto: Ungüento para coser carne|modificada|
|[ZP9Uq4PVTgzJ3wEi.htm](equipment-effects/ZP9Uq4PVTgzJ3wEi.htm)|Effect: Five-Feather Wreath - Armor (Greater)|Efecto: Corona de cinco plumas - Armadura (mayor)|modificada|
|[zqKzWGLODgIvtiKf.htm](equipment-effects/zqKzWGLODgIvtiKf.htm)|Effect: Spellguard Blade|Efecto: Spellguard Blade|modificada|
|[ztxW3lBPRcesF7wK.htm](equipment-effects/ztxW3lBPRcesF7wK.htm)|Effect: Cognitive Mutagen (Moderate)|Efecto: Mutágeno cognitivo (moderado)|modificada|
|[ZV9rtVOD1eDTwcY4.htm](equipment-effects/ZV9rtVOD1eDTwcY4.htm)|Effect: Grit (Stage 3)|Efecto: Grit (Etapa 3)|modificada|
|[zY7cemRcFD2zAVbC.htm](equipment-effects/zY7cemRcFD2zAVbC.htm)|Effect: Oath of the Devoted|Efecto: Juramento de los Devotos|modificada|
|[zZsdex5orF5Odpus.htm](equipment-effects/zZsdex5orF5Odpus.htm)|Effect: Mask of Allure|Efecto: Mask of Allure|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[8YZX34sJOIH32VwI.htm](equipment-effects/8YZX34sJOIH32VwI.htm)|Effect: Illuminated Folio|vacía|
|[GqXIV46JqB8x8eEN.htm](equipment-effects/GqXIV46JqB8x8eEN.htm)|Effect: Book of Warding Prayers|vacía|
|[jlYPMOHplgkvzLa9.htm](equipment-effects/jlYPMOHplgkvzLa9.htm)|Effect: Standard of the Primeval Howl|vacía|
|[N54jx6GEz2NpGobK.htm](equipment-effects/N54jx6GEz2NpGobK.htm)|Effect: Breastplate of the Mountain|vacía|
|[PT1g0Ar47FVo2O4D.htm](equipment-effects/PT1g0Ar47FVo2O4D.htm)|Effect: Batsbreath Cane|vacía|
|[yykiQBIGqwxIDRZq.htm](equipment-effects/yykiQBIGqwxIDRZq.htm)|Effect: Viper Rapier|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
