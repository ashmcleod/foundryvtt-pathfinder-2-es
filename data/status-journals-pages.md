# Estado de la traducción (journals-pages)

 * **modificada**: 289
 * **ninguna**: 3


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[OtBPlgtudM4PlTWD.htm](journals-pages/OtBPlgtudM4PlTWD.htm)|Harrower|
|[qjnUXickBOBDBu2N.htm](journals-pages/qjnUXickBOBDBu2N.htm)|Introspection Domain|
|[YQXGtBHVoiSYUjtl.htm](journals-pages/YQXGtBHVoiSYUjtl.htm)|Twilight Speaker|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[09VwtSsYldMkVzU1.htm](journals-pages/09VwtSsYldMkVzU1.htm)|Runelord|Runelord|modificada|
|[0e2o62hyCxbIkVoC.htm](journals-pages/0e2o62hyCxbIkVoC.htm)|Assassin|Asesino|modificada|
|[0GwpYEjCHWyfQvgg.htm](journals-pages/0GwpYEjCHWyfQvgg.htm)|Knowledge Domain|Dominio del Conocimiento|modificada|
|[0pPtVXH6Duitakbp.htm](journals-pages/0pPtVXH6Duitakbp.htm)|Folklorist|Folclorista|modificada|
|[0RnXjFday2Lp4ECG.htm](journals-pages/0RnXjFday2Lp4ECG.htm)|Sample Chases|Sample Chases|modificada|
|[0UrqPv7XLDDRwZ13.htm](journals-pages/0UrqPv7XLDDRwZ13.htm)|Pactbinder|Pactbinder|modificada|
|[0wCEUwABKPdKPj8e.htm](journals-pages/0wCEUwABKPdKPj8e.htm)|Dreams Domain|Dominio de los Sues|modificada|
|[0ZXiLwvBG20MzkO6.htm](journals-pages/0ZXiLwvBG20MzkO6.htm)|Scrollmaster|Scrollmaster|modificada|
|[1F84oh5hoG86mZIw.htm](journals-pages/1F84oh5hoG86mZIw.htm)|Marshal|Marshal|modificada|
|[2CbiV0VtDKZfQNte.htm](journals-pages/2CbiV0VtDKZfQNte.htm)|Viking|Vikingo|modificada|
|[3a9kFb9x9UXeLA1s.htm](journals-pages/3a9kFb9x9UXeLA1s.htm)|Quick Environmental Details|Detalles ambientales rápidos|modificada|
|[3B1PqqSZYQj9YXac.htm](journals-pages/3B1PqqSZYQj9YXac.htm)|Hallowed Necromancer|Hallowed Necromancer|modificada|
|[3H168yyaP6ze75kp.htm](journals-pages/3H168yyaP6ze75kp.htm)|Inspiring Relationships|Inspiring Relationships|modificada|
|[3P0NWwP3s7bIiidH.htm](journals-pages/3P0NWwP3s7bIiidH.htm)|Time Domain|Time Domain|modificada|
|[4gKrDFB1GlILn9la.htm](journals-pages/4gKrDFB1GlILn9la.htm)|Scroll Trickster|Scroll Trickster|modificada|
|[4VMzG36Pm1oivS6Y.htm](journals-pages/4VMzG36Pm1oivS6Y.htm)|Beastmaster|Beastmaster|modificada|
|[4YtHynO8i1a33zo9.htm](journals-pages/4YtHynO8i1a33zo9.htm)|Flexible Spellcaster (Class Archetype)|Hechicero Flexible (Arquetipo de Clase)|modificada|
|[4ZPZqbe0Ai8ZIfcp.htm](journals-pages/4ZPZqbe0Ai8ZIfcp.htm)|Golden League Xun|Liga Dorada Xun|modificada|
|[57rElfZ9sWKFQ8Ep.htm](journals-pages/57rElfZ9sWKFQ8Ep.htm)|Knight Vigilant|Caballero Vigilante|modificada|
|[5MjSsuKOLBoiL8FB.htm](journals-pages/5MjSsuKOLBoiL8FB.htm)|Freedom Domain|Dominio de la libertad|modificada|
|[5nd8ON2kFGOsCdwQ.htm](journals-pages/5nd8ON2kFGOsCdwQ.htm)|Bullet Dancer|Bullet Dancer|modificada|
|[5RH1CkZQHYpsOium.htm](journals-pages/5RH1CkZQHYpsOium.htm)|Edgewatch Detective|Edgewatch Detective|modificada|
|[5TqEbLR9QT3gJGe3.htm](journals-pages/5TqEbLR9QT3gJGe3.htm)|Sorrow Domain|Dominio del Dolor|modificada|
|[5v7k1XWQxaP0DoGX.htm](journals-pages/5v7k1XWQxaP0DoGX.htm)|Monk|Monje|modificada|
|[6A9zBmTyt8cn6ioa.htm](journals-pages/6A9zBmTyt8cn6ioa.htm)|Eldritch Archer|Eldritch Archer|modificada|
|[6bDpXy7pQdGrd2og.htm](journals-pages/6bDpXy7pQdGrd2og.htm)|Star Domain|Dominio Estrella|modificada|
|[6BXJUVvwvzljaCFT.htm](journals-pages/6BXJUVvwvzljaCFT.htm)|Armor Traits|Armor Traits|modificada|
|[6DgCLFEGEHvqmy7Y.htm](journals-pages/6DgCLFEGEHvqmy7Y.htm)|Clockwork Reanimator|Clockwork Reanimator|modificada|
|[6KciIDJV6ZdJcAVa.htm](journals-pages/6KciIDJV6ZdJcAVa.htm)|Sterling Dynamo|Sterling Dynamo|modificada|
|[6qTjtFWaBO5b60zJ.htm](journals-pages/6qTjtFWaBO5b60zJ.htm)|Dust Domain|Dust Domain|modificada|
|[798PFdS8FmefcOl0.htm](journals-pages/798PFdS8FmefcOl0.htm)|Death Domain|Dominio de la Muerte|modificada|
|[7FsXXkrOUXHtROnq.htm](journals-pages/7FsXXkrOUXHtROnq.htm)|Falling|Falling|modificada|
|[7jQ2x83KUFD8Wj6w.htm](journals-pages/7jQ2x83KUFD8Wj6w.htm)|Light Effects|Efectos de luz|modificada|
|[7xrNAgAnBqBgE3yM.htm](journals-pages/7xrNAgAnBqBgE3yM.htm)|Change Domain|Cambiar Dominio|modificada|
|[8qYJAx4O97sSxbUF.htm](journals-pages/8qYJAx4O97sSxbUF.htm)|Basic Actions|Acciones básicas|modificada|
|[8RYKz1WDPMJBmMNt.htm](journals-pages/8RYKz1WDPMJBmMNt.htm)|Game Hunter|Cazador|modificada|
|[9g14DjBZNj27goTt.htm](journals-pages/9g14DjBZNj27goTt.htm)|Soul Warden|Soul Warden|modificada|
|[9g1dNytABTpmmGkG.htm](journals-pages/9g1dNytABTpmmGkG.htm)|Glyph Domain|Glifo Dominio|modificada|
|[9hfEG4kJmEbrjGS1.htm](journals-pages/9hfEG4kJmEbrjGS1.htm)|Encounter Building & XP Awards|Construcción de Encuentros y Premios XP|modificada|
|[a3gPiGqSiWCZ6kGl.htm](journals-pages/a3gPiGqSiWCZ6kGl.htm)|Worldbuilding|Worldbuilding|modificada|
|[A5HwvubNii1d2UND.htm](journals-pages/A5HwvubNii1d2UND.htm)|Sniping Duo|Sniping Duo|modificada|
|[A7vErdGAweYsFcW8.htm](journals-pages/A7vErdGAweYsFcW8.htm)|Healing Domain|Curar Dominio|modificada|
|[aAm6jY2k5qBuWETd.htm](journals-pages/aAm6jY2k5qBuWETd.htm)|Inventor|Inventor|modificada|
|[aBALUjLyOqXKlhrP.htm](journals-pages/aBALUjLyOqXKlhrP.htm)|Earn Income|Obtener ingresos|modificada|
|[ackdHdIAmSb0AiVL.htm](journals-pages/ackdHdIAmSb0AiVL.htm)|Loremaster|Loremaster|modificada|
|[act12TfHpnKx6ZTu.htm](journals-pages/act12TfHpnKx6ZTu.htm)|Different Size Items|Objetos de diferente tamaño|modificada|
|[aIpIUbupTjw2863C.htm](journals-pages/aIpIUbupTjw2863C.htm)|Ghost|Ghost|modificada|
|[ajCEExOaxuB4C1tY.htm](journals-pages/ajCEExOaxuB4C1tY.htm)|Passion Domain|Passion Domain|modificada|
|[AOQZjqgfafqqtHOB.htm](journals-pages/AOQZjqgfafqqtHOB.htm)|Destruction Domain|Destruction Domain|modificada|
|[ARYrFIOsT3JxpjDY.htm](journals-pages/ARYrFIOsT3JxpjDY.htm)|Hellknight Armiger|Hellknight Armiger|modificada|
|[b5VoSJNzoNuqbtvD.htm](journals-pages/b5VoSJNzoNuqbtvD.htm)|Summoner|Invocador|modificada|
|[b7oePIMEMdFFS5TH.htm](journals-pages/b7oePIMEMdFFS5TH.htm)|Captivator|Cautivador|modificada|
|[bAIAWQ7q3ycIAnoj.htm](journals-pages/bAIAWQ7q3ycIAnoj.htm)|Relationships|Relaciones|modificada|
|[bApp2BZEMuYQCTDM.htm](journals-pages/bApp2BZEMuYQCTDM.htm)|Scout|Explorador|modificada|
|[BHn7nSm4BMPh9QoZ.htm](journals-pages/BHn7nSm4BMPh9QoZ.htm)|Using Deep Backgrounds|Uso de fondos profundos|modificada|
|[bKoV037XO3qiakGw.htm](journals-pages/bKoV037XO3qiakGw.htm)|Ghoul|Ghoul|modificada|
|[bkTCYlTFNifrM3sh.htm](journals-pages/bkTCYlTFNifrM3sh.htm)|Artillerist|Artillero|modificada|
|[bL5cccuvnP5h7Bnn.htm](journals-pages/bL5cccuvnP5h7Bnn.htm)|Animal Trainer|Animal Trainer|modificada|
|[bQeLZt29NnHEn6fh.htm](journals-pages/bQeLZt29NnHEn6fh.htm)|Juggler|Malabarista|modificada|
|[BRxlFHXu8tN14TDI.htm](journals-pages/BRxlFHXu8tN14TDI.htm)|Staff Acrobat|Staff Acróbata|modificada|
|[BsaTOw6p74DW5d8t.htm](journals-pages/BsaTOw6p74DW5d8t.htm)|Wand Prices|Precios Varita|modificada|
|[bTujFcUut9RX4GCy.htm](journals-pages/bTujFcUut9RX4GCy.htm)|Travel Domain|Dominio de Viaje|modificada|
|[BwPUY2X7TSmlJj5c.htm](journals-pages/BwPUY2X7TSmlJj5c.htm)|Duelist|Duelista|modificada|
|[C3P0TycdFCN02p9u.htm](journals-pages/C3P0TycdFCN02p9u.htm)|Bastion|Bastión|modificada|
|[C4b2AvMpVpe5BcVv.htm](journals-pages/C4b2AvMpVpe5BcVv.htm)|Material Hardness, Hit Points and Broken Threshold|Dureza del material, puntos de golpe y umbral de rotura.|modificada|
|[c8psqIuH4YFi6msK.htm](journals-pages/c8psqIuH4YFi6msK.htm)|Family Background|Bagaje Familiar|modificada|
|[cAxBEZsej32riaY5.htm](journals-pages/cAxBEZsej32riaY5.htm)|Decay Domain|Decay Domain|modificada|
|[CB9YE4P7A2Wty1IX.htm](journals-pages/CB9YE4P7A2Wty1IX.htm)|Red Mantis Assassin|Red Mantis Assassin|modificada|
|[cBBwTnJrVmvaRMYq.htm](journals-pages/cBBwTnJrVmvaRMYq.htm)|Aldori Duelist|Aldori Duelista|modificada|
|[CbsAiY68e8n5vVVN.htm](journals-pages/CbsAiY68e8n5vVVN.htm)|Repose Domain|Dominio de reposo|modificada|
|[cdnkS3A8OpOnRjKu.htm](journals-pages/cdnkS3A8OpOnRjKu.htm)|Bellflower Tiller|Bellflower Tiller|modificada|
|[cjD6YVPQpmYiOPdC.htm](journals-pages/cjD6YVPQpmYiOPdC.htm)|Terrain in Encounters|Terreno en Encuentros|modificada|
|[CkBvj5y1lAm1jnsc.htm](journals-pages/CkBvj5y1lAm1jnsc.htm)|Sun Domain|Sun Domain|modificada|
|[CM9ZqWwl7myKn2X1.htm](journals-pages/CM9ZqWwl7myKn2X1.htm)|Darkness Domain|Dominio de la oscuridad|modificada|
|[CMgYob7Cy4meoQKg.htm](journals-pages/CMgYob7Cy4meoQKg.htm)|Ranger|Ranger|modificada|
|[COTDddn4psjIoWry.htm](journals-pages/COTDddn4psjIoWry.htm)|Lion Blade|Hoja del León|modificada|
|[CSVoyUvynmM5LzPW.htm](journals-pages/CSVoyUvynmM5LzPW.htm)|Gunslinger|Pistolero|modificada|
|[Czi3XXuNOSE7ISpd.htm](journals-pages/Czi3XXuNOSE7ISpd.htm)|Perfection Domain|Dominio de la perfección|modificada|
|[DAVBjDSysgXgtVQu.htm](journals-pages/DAVBjDSysgXgtVQu.htm)|Gray Gardener|Jardinero Gris|modificada|
|[DBxoE3TNDPtcKHxH.htm](journals-pages/DBxoE3TNDPtcKHxH.htm)|Animals & Barding|Animales & Barding|modificada|
|[DI3MYGIK8iEycanU.htm](journals-pages/DI3MYGIK8iEycanU.htm)|Zeal Domain|Dominio Zeal|modificada|
|[DJiYP5tFBrBMD0We.htm](journals-pages/DJiYP5tFBrBMD0We.htm)|Sorcerer|Hechicero|modificada|
|[dnbUao9yk8Bs99eT.htm](journals-pages/dnbUao9yk8Bs99eT.htm)|Weapon Traits|Rasgos de Armas|modificada|
|[dpL9RyApn4y7jJ3y.htm](journals-pages/dpL9RyApn4y7jJ3y.htm)|Homeland|Homeland|modificada|
|[drgCQcXZbIJU0Zhw.htm](journals-pages/drgCQcXZbIJU0Zhw.htm)|Wrestler|Luchador|modificada|
|[dRu66xNqJ9Ihe1or.htm](journals-pages/dRu66xNqJ9Ihe1or.htm)|Difficulty Classes|Clases de dificultad|modificada|
|[DS95vr2zmTsjsMhU.htm](journals-pages/DS95vr2zmTsjsMhU.htm)|Magic Domain|Dominio Mágico|modificada|
|[DUDsA3sIlOlibvIu.htm](journals-pages/DUDsA3sIlOlibvIu.htm)|High-Quality Items|Artículos de alta calidad|modificada|
|[duqPy1VMQYNJrw7q.htm](journals-pages/duqPy1VMQYNJrw7q.htm)|Conditions|Estados negativos (normalmente se abrevia a estado)|modificada|
|[Dx47K8wpx8KZUa9S.htm](journals-pages/Dx47K8wpx8KZUa9S.htm)|Protection Domain|Dominio de protección|modificada|
|[DXtjeVaWNB8zSjpA.htm](journals-pages/DXtjeVaWNB8zSjpA.htm)|Champion|Campeón|modificada|
|[DztQF2FexWvnzcaE.htm](journals-pages/DztQF2FexWvnzcaE.htm)|Spell Trickster|Hechizo Trickster|modificada|
|[EC2eB0JglDG5j1gT.htm](journals-pages/EC2eB0JglDG5j1gT.htm)|Fate Domain|Dominio del Destino|modificada|
|[Edn6qQNVcEVpkhPl.htm](journals-pages/Edn6qQNVcEVpkhPl.htm)|Detecting Creatures|Detección de criaturas|modificada|
|[egSErNozlL3HRK1y.htm](journals-pages/egSErNozlL3HRK1y.htm)|Fire Domain|Dominio del Fuego|modificada|
|[eLc6TSgWykjtSeuF.htm](journals-pages/eLc6TSgWykjtSeuF.htm)|Magaambyan Attendant|Magaambyan Attendant|modificada|
|[eNTStXeuABNPSLjw.htm](journals-pages/eNTStXeuABNPSLjw.htm)|Witch|Bruja|modificada|
|[ePfibeHcnRcjO6lC.htm](journals-pages/ePfibeHcnRcjO6lC.htm)|Reanimator|Reanimador|modificada|
|[EQfZepZX6rxxBRqG.htm](journals-pages/EQfZepZX6rxxBRqG.htm)|Toil Domain|Toil Domain|modificada|
|[EWiUv3UiR3RHSGlA.htm](journals-pages/EWiUv3UiR3RHSGlA.htm)|Cathartic Mage|Cathartic Mage|modificada|
|[EwlYs1OzaMj9BB5I.htm](journals-pages/EwlYs1OzaMj9BB5I.htm)|Student of Perfection|Estudiante de la Perfección|modificada|
|[f2wr86966A0oUUA0.htm](journals-pages/f2wr86966A0oUUA0.htm)|Adjusting Treasure|Ajustar Tesoro|modificada|
|[F5gBiVGbS8YSgPSI.htm](journals-pages/F5gBiVGbS8YSgPSI.htm)|Sixth Pillar|Sexto Pilar|modificada|
|[f7tFLR7aFonXTLQa.htm](journals-pages/f7tFLR7aFonXTLQa.htm)|Undead Master|Maestro muerto viviente|modificada|
|[F7W15pGOeSeNJD0C.htm](journals-pages/F7W15pGOeSeNJD0C.htm)|Firebrand Braggart|Braggart de los Instigadores|modificada|
|[Fea8ZereQhNolDoP.htm](journals-pages/Fea8ZereQhNolDoP.htm)|Bounty Hunter|Cazarrecompensas|modificada|
|[FJGPBhYmD7xTFsrW.htm](journals-pages/FJGPBhYmD7xTFsrW.htm)|Spellshot (Class Archetype)|Spellshot (Arquetipo de clase)|modificada|
|[flmxRzGxN2rRNyxZ.htm](journals-pages/flmxRzGxN2rRNyxZ.htm)|Confidence Domain|Dominio de confianza|modificada|
|[FOI43M8DJe2lkMwl.htm](journals-pages/FOI43M8DJe2lkMwl.htm)|Structures|Estructuras|modificada|
|[fPqSekmEm5byReOk.htm](journals-pages/fPqSekmEm5byReOk.htm)|Talisman Dabbler|Talisman Dabbler|modificada|
|[FtW1gtbHgO0KofPl.htm](journals-pages/FtW1gtbHgO0KofPl.htm)|Pain Domain|Dominio del dolor|modificada|
|[fTxZjmNLckRwZ4Po.htm](journals-pages/fTxZjmNLckRwZ4Po.htm)|Pathfinder Agent|Agente Pathfinder|modificada|
|[FzzMt7ybWy03D9qZ.htm](journals-pages/FzzMt7ybWy03D9qZ.htm)|Basic Services|Servicios básicos|modificada|
|[G1Rje9eYCFEByzjg.htm](journals-pages/G1Rje9eYCFEByzjg.htm)|Special Battles|Batallas Especiales|modificada|
|[G38usCLuTDmFYw7V.htm](journals-pages/G38usCLuTDmFYw7V.htm)|Zombie|Zombie|modificada|
|[G3AtnAkIKNHrahmd.htm](journals-pages/G3AtnAkIKNHrahmd.htm)|Vampire|Vampiro|modificada|
|[G9Fzy5ZK4KtAmcFb.htm](journals-pages/G9Fzy5ZK4KtAmcFb.htm)|Snarecrafter|Snarecrafter|modificada|
|[gbBf6x89m4SEFpsL.htm](journals-pages/gbBf6x89m4SEFpsL.htm)|Druid|Druida|modificada|
|[gE3x4USaU4VWdwKD.htm](journals-pages/gE3x4USaU4VWdwKD.htm)|Gamemastery|Gamemastery|modificada|
|[gfhnRp2TEy9JCfHI.htm](journals-pages/gfhnRp2TEy9JCfHI.htm)|Gladiator|Gladiador|modificada|
|[GiuzDTtkQAgtGW6n.htm](journals-pages/GiuzDTtkQAgtGW6n.htm)|Indulgence Domain|Indulgence Domain|modificada|
|[h1zdUIWX1k5PKTnc.htm](journals-pages/h1zdUIWX1k5PKTnc.htm)|Celebrity|Celebrity|modificada|
|[HDTW0vr8QL20M5ND.htm](journals-pages/HDTW0vr8QL20M5ND.htm)|Weapon Improviser|Improvisador de armas|modificada|
|[hGoWOjdsUz16oJUm.htm](journals-pages/hGoWOjdsUz16oJUm.htm)|Plague Domain|Plague Domain|modificada|
|[hH8gPjc3GxFzgLHR.htm](journals-pages/hH8gPjc3GxFzgLHR.htm)|Variant Rules|Variant Rules|modificada|
|[hOsFg2VpnOshaSOi.htm](journals-pages/hOsFg2VpnOshaSOi.htm)|Environmental Damage|Daño Ambiental|modificada|
|[HvbDEgCsLbzuMRiR.htm](journals-pages/HvbDEgCsLbzuMRiR.htm)|Poisoner|Envenenador|modificada|
|[HZHzpATOyhTjUDrR.htm](journals-pages/HZHzpATOyhTjUDrR.htm)|Alter Ego|Alter Ego|modificada|
|[I6FbkKYncDuu7eWq.htm](journals-pages/I6FbkKYncDuu7eWq.htm)|Geomancer|Geomancer|modificada|
|[I6lHRH3mWjfvhJrR.htm](journals-pages/I6lHRH3mWjfvhJrR.htm)|Resting|Descansando|modificada|
|[iH3Vrt35DyUbhxZj.htm](journals-pages/iH3Vrt35DyUbhxZj.htm)|Mammoth Lord|Mammoth Lord|modificada|
|[IliVJaEHaOklcg86.htm](journals-pages/IliVJaEHaOklcg86.htm)|Stamina|Stamina|modificada|
|[inxtk4rYj2UZaytg.htm](journals-pages/inxtk4rYj2UZaytg.htm)|Fighter|Caza|modificada|
|[INYPqM7rs9SNFRba.htm](journals-pages/INYPqM7rs9SNFRba.htm)|Downtime Events|Eventos de tiempo libre|modificada|
|[IQ56rAleMRc53Lcv.htm](journals-pages/IQ56rAleMRc53Lcv.htm)|Major Childhood Event|Acontecimiento superior de la infancia|modificada|
|[iVY32tNvfS1tPYU2.htm](journals-pages/iVY32tNvfS1tPYU2.htm)|Formula Price|Fórmula Precio|modificada|
|[j5uZbCwoHOOEO1bC.htm](journals-pages/j5uZbCwoHOOEO1bC.htm)|Runescarred|Runescarred|modificada|
|[JBPXisA4IdXI9gVn.htm](journals-pages/JBPXisA4IdXI9gVn.htm)|Eldritch Researcher|Investigador de lo sobrenatural|modificada|
|[jEf21wTIEXzGtPU6.htm](journals-pages/jEf21wTIEXzGtPU6.htm)|Detecting with Other Senses|Detectar con otros sentidos|modificada|
|[jFhTp57zO3ej6HDt.htm](journals-pages/jFhTp57zO3ej6HDt.htm)|Corpse Tender|Tender Cadáveres|modificada|
|[jq9O1tl76g2AzLOh.htm](journals-pages/jq9O1tl76g2AzLOh.htm)|Cold Domain|Dominio del Frío|modificada|
|[jTiXRtNNvMOnsg98.htm](journals-pages/jTiXRtNNvMOnsg98.htm)|Beast Gunner|Beast Gunner|modificada|
|[JzUDumJ3Dlyame4z.htm](journals-pages/JzUDumJ3Dlyame4z.htm)|Drow Shootist|Drow Shootist|modificada|
|[k9Ebp52kt0ZLHtMl.htm](journals-pages/k9Ebp52kt0ZLHtMl.htm)|Swashbuckler|Swashbuckler|modificada|
|[K9Krytj8OtUvQxoc.htm](journals-pages/K9Krytj8OtUvQxoc.htm)|Thaumaturge|Taumaturgo|modificada|
|[kAMseeY0TewNmnLQ.htm](journals-pages/kAMseeY0TewNmnLQ.htm)|Deep Backgrounds|Fondos profundos|modificada|
|[Kca7UPuMm44tOo9n.htm](journals-pages/Kca7UPuMm44tOo9n.htm)|Lightning Domain|Lightning Domain|modificada|
|[kL8yx8vz9FMCIYC1.htm](journals-pages/kL8yx8vz9FMCIYC1.htm)|Treat Wounds|Tratar heridas|modificada|
|[KORSADviZaSccs2W.htm](journals-pages/KORSADviZaSccs2W.htm)|Zephyr Guard|Guardia del Céfiro|modificada|
|[KRUWPDzuwG0LC26d.htm](journals-pages/KRUWPDzuwG0LC26d.htm)|Curse Maelstrom|Curse Maelstrom|modificada|
|[kVC4kgYKbhqPsaDt.htm](journals-pages/kVC4kgYKbhqPsaDt.htm)|Rogue|Rogue|modificada|
|[KXAxZfdMpnHQZQoc.htm](journals-pages/KXAxZfdMpnHQZQoc.htm)|Golarion Calendar|Calendario de Golarion|modificada|
|[kxDloJH5ImKnIV9P.htm](journals-pages/kxDloJH5ImKnIV9P.htm)|Temperature Effects|Efectos de Temperatura|modificada|
|[l0rlB52Pzy6Vt1Ic.htm](journals-pages/l0rlB52Pzy6Vt1Ic.htm)|Scroll Prices|Precios de desplazamiento|modificada|
|[L11XsA5G89xVKlDw.htm](journals-pages/L11XsA5G89xVKlDw.htm)|Luck Domain|Dominio de la Suerte|modificada|
|[lc3iBCamQ8jvr9dp.htm](journals-pages/lc3iBCamQ8jvr9dp.htm)|Trapsmith|Trapsmith|modificada|
|[lcCPztCqmXpwdmWx.htm](journals-pages/lcCPztCqmXpwdmWx.htm)|Elite/Weak Adjustments|Ajustes Élite/Débil|modificada|
|[LgkfmZnFP3TWtkuy.htm](journals-pages/LgkfmZnFP3TWtkuy.htm)|Living Vessel|Recipiente Viviente|modificada|
|[lgsJz7mZ1OTe340e.htm](journals-pages/lgsJz7mZ1OTe340e.htm)|Truth Domain|Dominio de la verdad|modificada|
|[LL4YD16kPqcGibKG.htm](journals-pages/LL4YD16kPqcGibKG.htm)|Ghost Hunter|Cazador de fantasmas|modificada|
|[lQ9MmX3zg4Bd9hjb.htm](journals-pages/lQ9MmX3zg4Bd9hjb.htm)|GM Screen|Pantalla GM|modificada|
|[LWgcWM8B85HHVCtZ.htm](journals-pages/LWgcWM8B85HHVCtZ.htm)|Soulforger|Soulforger|modificada|
|[Ly8l2GT6dbvuY3A2.htm](journals-pages/Ly8l2GT6dbvuY3A2.htm)|Treasure|Tesoro|modificada|
|[M5QOocGyP4zkMo9m.htm](journals-pages/M5QOocGyP4zkMo9m.htm)|Horizon Walker|Horizon Walker|modificada|
|[mJBp4KIszuqrmnp5.htm](journals-pages/mJBp4KIszuqrmnp5.htm)|Wealth Domain|Dominio de la riqueza|modificada|
|[Mk7ECwe1a971WyWl.htm](journals-pages/Mk7ECwe1a971WyWl.htm)|Demolitionist|Demoledor|modificada|
|[MKU4d2hIpLuXGN2J.htm](journals-pages/MKU4d2hIpLuXGN2J.htm)|Shadowdancer|Shadowdancer|modificada|
|[mmB3EkkdCpLke7Lk.htm](journals-pages/mmB3EkkdCpLke7Lk.htm)|Investigator|Investigador|modificada|
|[mmfWcV3Iyql5nzTo.htm](journals-pages/mmfWcV3Iyql5nzTo.htm)|Counteract|Contrarrestar|modificada|
|[MOVMHZU1SfkhNN1K.htm](journals-pages/MOVMHZU1SfkhNN1K.htm)|Might Domain|Dominio del poder|modificada|
|[mtc5frNxUHORGscz.htm](journals-pages/mtc5frNxUHORGscz.htm)|Linguist|Lingüista|modificada|
|[mV0K9sxD3TWnZDcy.htm](journals-pages/mV0K9sxD3TWnZDcy.htm)|Undead Slayer|Cazador de muertos vivientes|modificada|
|[mXywYJJCM9IVItZz.htm](journals-pages/mXywYJJCM9IVItZz.htm)|Mind Smith|Mind Smith|modificada|
|[N01RLMQPaSeoHEuL.htm](journals-pages/N01RLMQPaSeoHEuL.htm)|Martial Artist|Artista Marcial|modificada|
|[NaG4fy33coUdSFtH.htm](journals-pages/NaG4fy33coUdSFtH.htm)|Jalmeri Heavenseeker|Jalmeri Heavenseeker|modificada|
|[ngVnNmi1Qke3lTy0.htm](journals-pages/ngVnNmi1Qke3lTy0.htm)|Oracle|Oracle|modificada|
|[nmcEiM2gjqjGWp2c.htm](journals-pages/nmcEiM2gjqjGWp2c.htm)|Cavalier|Cavalier|modificada|
|[nPcma8UOqWo7xw0P.htm](journals-pages/nPcma8UOqWo7xw0P.htm)|Trick Driver|Trick Driver|modificada|
|[nuywscaiVGXLQpZ1.htm](journals-pages/nuywscaiVGXLQpZ1.htm)|Wyrmkin Domain|Dominio Wyrmkin|modificada|
|[nVfhX1aisz6jY8qf.htm](journals-pages/nVfhX1aisz6jY8qf.htm)|Unexpected Sharpshooter|Tirador Inesperado|modificada|
|[o71hqcfzhCKXcSml.htm](journals-pages/o71hqcfzhCKXcSml.htm)|Archer|Arquero|modificada|
|[O79hOcsaQyj3aQC5.htm](journals-pages/O79hOcsaQyj3aQC5.htm)|Archaeologist|Arqueólogo|modificada|
|[O7fKfFxCx3e1YasX.htm](journals-pages/O7fKfFxCx3e1YasX.htm)|Spellmaster|Spellmaster|modificada|
|[O9P1YgtiCgHlPNp5.htm](journals-pages/O9P1YgtiCgHlPNp5.htm)|Pirate|Pirata|modificada|
|[oAfVg9t7GGTW7R1H.htm](journals-pages/oAfVg9t7GGTW7R1H.htm)|Challenging Relationships|Relaciones desafiantes|modificada|
|[oBlKgVYRup5ORqx1.htm](journals-pages/oBlKgVYRup5ORqx1.htm)|Bard|Bardo|modificada|
|[oBOw6E6V7ncrB1rN.htm](journals-pages/oBOw6E6V7ncrB1rN.htm)|Treasure Per Encounter|Tesoro por encuentro|modificada|
|[OBTmo8rD2x8kFzeH.htm](journals-pages/OBTmo8rD2x8kFzeH.htm)|Sleepwalker|Sonámbulo|modificada|
|[OcioDiLTdgzvT8VX.htm](journals-pages/OcioDiLTdgzvT8VX.htm)|Knight Reclaimant|Knight Reclaimant|modificada|
|[ok2IPQGIrxj8h1vs.htm](journals-pages/ok2IPQGIrxj8h1vs.htm)|Three-Dimensional Combat|Combate tridimensional|modificada|
|[oM0DobZe9uEkz9kb.htm](journals-pages/oM0DobZe9uEkz9kb.htm)|Force Open|Abrir por la fuerza|modificada|
|[oMIA3aFKvpuV9f2H.htm](journals-pages/oMIA3aFKvpuV9f2H.htm)|Elementalist (Class Archetype)|Elementalista (Arquetipo de clase)|modificada|
|[pDmUITVao0FDMnVf.htm](journals-pages/pDmUITVao0FDMnVf.htm)|Halcyon Speaker|Halcyon Speaker|modificada|
|[pEjd65isUfXKSB18.htm](journals-pages/pEjd65isUfXKSB18.htm)|Hero Points|Puntos de Héroe|modificada|
|[PFBBmN58KtjXo79k.htm](journals-pages/PFBBmN58KtjXo79k.htm)|Basic Rules|Reglas Básicas|modificada|
|[PfoKLAsQeXHsDgzf.htm](journals-pages/PfoKLAsQeXHsDgzf.htm)|Lifestyle Expenses|Gastos de estilo de vida|modificada|
|[pg9fG1ikcgJZRWlk.htm](journals-pages/pg9fG1ikcgJZRWlk.htm)|Lastwall Sentry|Lastwall Sentry|modificada|
|[Ph7yWRR376aYh12T.htm](journals-pages/Ph7yWRR376aYh12T.htm)|Ritualist|Ritualista|modificada|
|[PpoPRKuwanrnhd0Y.htm](journals-pages/PpoPRKuwanrnhd0Y.htm)|Living Monolith|Monolito Viviente|modificada|
|[PVrMSF93KIejBTMm.htm](journals-pages/PVrMSF93KIejBTMm.htm)|Critical Specialization Effects|Efectos de especialización crítica|modificada|
|[PVwU1LMWcSgD7Q0m.htm](journals-pages/PVwU1LMWcSgD7Q0m.htm)|Oatia Skysage|Oatia Skysage|modificada|
|[Q72YDLPh0urLfWPm.htm](journals-pages/Q72YDLPh0urLfWPm.htm)|Vehicle Mechanic|Mecánico de vehículos|modificada|
|[qlQSYjCwnoCzfto2.htm](journals-pages/qlQSYjCwnoCzfto2.htm)|Golem Grafter|Golem Grafter|modificada|
|[qMNBD91gc5kIKPs2.htm](journals-pages/qMNBD91gc5kIKPs2.htm)|Spellcasting Services|Spellcasting Services|modificada|
|[qMS6QepvY7UQQjcr.htm](journals-pages/qMS6QepvY7UQQjcr.htm)|Abomination Domain|Dominio Abominación|modificada|
|[QSk78hQR3zskMlq2.htm](journals-pages/QSk78hQR3zskMlq2.htm)|Cities Domain|Dominio de las ciudades|modificada|
|[QWw0D2YPKdz6HJPu.htm](journals-pages/QWw0D2YPKdz6HJPu.htm)|Influential Associate|Asociado influyente|modificada|
|[QzsUe3Rt3SifTQvb.htm](journals-pages/QzsUe3Rt3SifTQvb.htm)|Naga Domain|Dominio Naga|modificada|
|[R20JXF43vU5RQyUj.htm](journals-pages/R20JXF43vU5RQyUj.htm)|Nightmares Domain|Dominio de las pesadillas|modificada|
|[r43lPEEL7WHOZjHL.htm](journals-pages/r43lPEEL7WHOZjHL.htm)|Lich|Lich|modificada|
|[r79jab1XgOdcgvKJ.htm](journals-pages/r79jab1XgOdcgvKJ.htm)|Magus|Magus|modificada|
|[rd0jQwvTK4jpv95o.htm](journals-pages/rd0jQwvTK4jpv95o.htm)|Swarm Domain|Dominio Enjambre|modificada|
|[rhDvoOHAhAlABiae.htm](journals-pages/rhDvoOHAhAlABiae.htm)|Acrobat|Acróbata|modificada|
|[RIlgBuWGfHC1rzYu.htm](journals-pages/RIlgBuWGfHC1rzYu.htm)|Undeath Domain|Undeath Domain|modificada|
|[rlKxNuq1obXe3m5J.htm](journals-pages/rlKxNuq1obXe3m5J.htm)|Scrounger|Scrounger|modificada|
|[RoF5NOFBefXAPftS.htm](journals-pages/RoF5NOFBefXAPftS.htm)|Mauler|Vapulear|modificada|
|[Rrjz5tMJtyVEQnh8.htm](journals-pages/Rrjz5tMJtyVEQnh8.htm)|Herbalist|Herbolario|modificada|
|[rtAFOZOjWHC2zRNO.htm](journals-pages/rtAFOZOjWHC2zRNO.htm)|Vigilante|Vigilante|modificada|
|[rtobUemb6vF2Yu3Y.htm](journals-pages/rtobUemb6vF2Yu3Y.htm)|Soul Domain|Dominio del Alma|modificada|
|[RxDsPgPCCxEdjcVQ.htm](journals-pages/RxDsPgPCCxEdjcVQ.htm)|Hellknight Signifer|Caballero del Infierno Signifer|modificada|
|[rxyJUbQosVfgRjLr.htm](journals-pages/rxyJUbQosVfgRjLr.htm)|Crystal Keeper|Guardián del Cristal|modificada|
|[S1gyomjojgtCdxc3.htm](journals-pages/S1gyomjojgtCdxc3.htm)|Secrecy Domain|Dominio del Secreto|modificada|
|[S4BZW9c5n6CctDxl.htm](journals-pages/S4BZW9c5n6CctDxl.htm)|Firework Technician|Técnico de fuegos artificiales|modificada|
|[s9kLzXOCl2GNT6TY.htm](journals-pages/s9kLzXOCl2GNT6TY.htm)|Dandy|Dandy|modificada|
|[sAFWD8D0RKH4m25n.htm](journals-pages/sAFWD8D0RKH4m25n.htm)|Dual-Weapon Warrior|Guerrero de doble arma|modificada|
|[SAnmegCTIqGW9S7S.htm](journals-pages/SAnmegCTIqGW9S7S.htm)|Family Domain|Dominio familiar|modificada|
|[SiMXtLf4YQeigzIE.htm](journals-pages/SiMXtLf4YQeigzIE.htm)|Senses|Sentidos|modificada|
|[SnSGU7DPiogZ9JOr.htm](journals-pages/SnSGU7DPiogZ9JOr.htm)|Automatic Bonus Progression|Progresión automática de bonificación|modificada|
|[SO9d7RSf1zqmTlmW.htm](journals-pages/SO9d7RSf1zqmTlmW.htm)|Dragon Disciple|Discípulo del Dragón|modificada|
|[SpWLU1ixQRT6rkUP.htm](journals-pages/SpWLU1ixQRT6rkUP.htm)|Downtime Tasks|Tareas en tiempo libre|modificada|
|[StXN6IHR6evRaeXF.htm](journals-pages/StXN6IHR6evRaeXF.htm)|Vigil Domain|Dominio de la Vigilia|modificada|
|[SUDV5hFZ9WocxWqv.htm](journals-pages/SUDV5hFZ9WocxWqv.htm)|Blessed One|Bendecido|modificada|
|[sY25SoDaHBPIG5Jw.htm](journals-pages/sY25SoDaHBPIG5Jw.htm)|Nantambu Chime-Ringer|Nantambu Chime-Ringer|modificada|
|[T0JHj79aGphlZ4Mt.htm](journals-pages/T0JHj79aGphlZ4Mt.htm)|Tyranny Domain|Tyranny Domain|modificada|
|[T2y0vuYibZCL7CH0.htm](journals-pages/T2y0vuYibZCL7CH0.htm)|Air Domain|Dominio del Aire|modificada|
|[tJllU1E8g7W1QxYe.htm](journals-pages/tJllU1E8g7W1QxYe.htm)|Cover|Cubrir|modificada|
|[tJQ5f8C8m5gpZsF1.htm](journals-pages/tJQ5f8C8m5gpZsF1.htm)|Oozemorph|Oozomorfo|modificada|
|[TnD2hTWTyjGKlw9b.htm](journals-pages/TnD2hTWTyjGKlw9b.htm)|Sentinel|Centinela|modificada|
|[ts7gtyAhDEI4V7Ve.htm](journals-pages/ts7gtyAhDEI4V7Ve.htm)|Character Wealth|Riqueza del personaje|modificada|
|[ttkG4geKXTao5pku.htm](journals-pages/ttkG4geKXTao5pku.htm)|Alkenstar Agent|Agente Alkenstar|modificada|
|[tuThzOCvMLbRVba8.htm](journals-pages/tuThzOCvMLbRVba8.htm)|Delirium Domain|Dominio del Delirio|modificada|
|[U8WVR6EDfmUaMCbu.htm](journals-pages/U8WVR6EDfmUaMCbu.htm)|Water Domain|Water Domain|modificada|
|[uA6XIArPfGSwBvZi.htm](journals-pages/uA6XIArPfGSwBvZi.htm)|Psychic Duelist|Duelista Psíquico|modificada|
|[uGQKjk2w4whzomky.htm](journals-pages/uGQKjk2w4whzomky.htm)|Duty Domain|Dominio del deber|modificada|
|[uGsGn5sgR7wn0TQD.htm](journals-pages/uGsGn5sgR7wn0TQD.htm)|Turpin Rowe Lumberjack|Turpin Rowe Leñador|modificada|
|[uWJfFysCcoU1o2Hs.htm](journals-pages/uWJfFysCcoU1o2Hs.htm)|Skill Actions|Acciones de Habilidad|modificada|
|[Ux0sa5SUBu616i5k.htm](journals-pages/Ux0sa5SUBu616i5k.htm)|Alchemist|Alquimista|modificada|
|[uxdPLsxY8fNrenDR.htm](journals-pages/uxdPLsxY8fNrenDR.htm)|Death and Dying|Muerte y Morir|modificada|
|[uYCTmos3yZtbD9qs.htm](journals-pages/uYCTmos3yZtbD9qs.htm)|Barbarian|Bárbaro|modificada|
|[vKuJVojicBmNsL1C.htm](journals-pages/vKuJVojicBmNsL1C.htm)|Butterfly Blade|Hoja de Mariposa|modificada|
|[VnR8gBBq1oCG7YQh.htm](journals-pages/VnR8gBBq1oCG7YQh.htm)|Reputation|Reputación|modificada|
|[VR9UdfhZphZGZdsx.htm](journals-pages/VR9UdfhZphZGZdsx.htm)|Bulk Conversions for Different Sizes|Conversiones a granel para diferentes tamaños|modificada|
|[vS9iMDapZN9uW33Q.htm](journals-pages/vS9iMDapZN9uW33Q.htm)|Bright Lion|Leones brillantes|modificada|
|[wBhgIgt47v9uspp3.htm](journals-pages/wBhgIgt47v9uspp3.htm)|Nature Domain|Dominio de la naturaleza|modificada|
|[wHs6IGCMaXsgFNop.htm](journals-pages/wHs6IGCMaXsgFNop.htm)|Shadowcaster|Shadowcaster|modificada|
|[wMUAm1PJ1HjJ8fFU.htm](journals-pages/wMUAm1PJ1HjJ8fFU.htm)|Pistol Phenom|Pistol Phenom|modificada|
|[WvleaXPfigawBvtN.htm](journals-pages/WvleaXPfigawBvtN.htm)|Exorcist|Exorcista|modificada|
|[XCxK6OGFY0KvJPo3.htm](journals-pages/XCxK6OGFY0KvJPo3.htm)|Learn a Spell|Aprender un conjuro|modificada|
|[xgk1Qc6nzGNJ7LSz.htm](journals-pages/xgk1Qc6nzGNJ7LSz.htm)|Chronoskimmer|Chronoskimmer|modificada|
|[XH3xabCB01FiCRal.htm](journals-pages/XH3xabCB01FiCRal.htm)|Proficiency Without Level|Proficiencia Sin Nivel|modificada|
|[xJtbGqoz3BcCjUik.htm](journals-pages/xJtbGqoz3BcCjUik.htm)|Trickery Domain|Trickery Domain|modificada|
|[XLug8rxIe1KPX6Nf.htm](journals-pages/XLug8rxIe1KPX6Nf.htm)|Provocator|Provocador|modificada|
|[xLxrtbsj4acqgsyC.htm](journals-pages/xLxrtbsj4acqgsyC.htm)|Void Domain|Void Domain|modificada|
|[xMdIENo4w9bEzZuK.htm](journals-pages/xMdIENo4w9bEzZuK.htm)|Ghost Eater|Ghost Eater|modificada|
|[XszmLWy174esRrlg.htm](journals-pages/XszmLWy174esRrlg.htm)|Wizard|Mago|modificada|
|[XWkyCVISmVtJ0ZY3.htm](journals-pages/XWkyCVISmVtJ0ZY3.htm)|Medic|Medic|modificada|
|[Y3DFBCWiM9GBIlfl.htm](journals-pages/Y3DFBCWiM9GBIlfl.htm)|Moon Domain|Dominio de la Luna|modificada|
|[Y4DiXz5I8krCh1fC.htm](journals-pages/Y4DiXz5I8krCh1fC.htm)|Swordmaster|Swordmaster|modificada|
|[yaMJsfYZmWJLqbFE.htm](journals-pages/yaMJsfYZmWJLqbFE.htm)|Ambition Domain|Ambition Domain|modificada|
|[yaR8E2drX6pFUFCK.htm](journals-pages/yaR8E2drX6pFUFCK.htm)|Familiar Master|Maestro Familiar|modificada|
|[ydbCjJ9PPmRzZhDN.htm](journals-pages/ydbCjJ9PPmRzZhDN.htm)|Creation Domain|Dominio de la creación|modificada|
|[ydELQFQq0lGLTvrA.htm](journals-pages/ydELQFQq0lGLTvrA.htm)|Time Mage|Mago del Tiempo|modificada|
|[Yepac621abWWw5qj.htm](journals-pages/Yepac621abWWw5qj.htm)|Monster Abilities|Habilidades del Monstruo|modificada|
|[ygboVjCAFRpcysUb.htm](journals-pages/ygboVjCAFRpcysUb.htm)|Player Screen|Pantalla del jugador|modificada|
|[ygzv72IJNmjh0SPB.htm](journals-pages/ygzv72IJNmjh0SPB.htm)|Psychic|Psíquico|modificada|
|[yHqDPytVY9bxWo9I.htm](journals-pages/yHqDPytVY9bxWo9I.htm)|Item Quirks|Item Quirks|modificada|
|[YODf1eNWi9jnR93y.htm](journals-pages/YODf1eNWi9jnR93y.htm)|Pactbound Initiate|Pactbound Initiate|modificada|
|[yOEiNjHLQ6XuOruq.htm](journals-pages/yOEiNjHLQ6XuOruq.htm)|Overwatch|Overwatch|modificada|
|[YVLMgNxXTUQuDJgp.htm](journals-pages/YVLMgNxXTUQuDJgp.htm)|Travel Speed|Velocidad de Viaje|modificada|
|[yWtwNUkGyj79Q04W.htm](journals-pages/yWtwNUkGyj79Q04W.htm)|Hellknight|Hellknight|modificada|
|[Z2xXPHkxGLFTXzdI.htm](journals-pages/Z2xXPHkxGLFTXzdI.htm)|Creature Identification|Identificación de criaturas|modificada|
|[zB1mqE9IeyfyQDnn.htm](journals-pages/zB1mqE9IeyfyQDnn.htm)|Exploration Activities|Actividades de exploración|modificada|
|[ZewC2i5YdZPsWO8X.htm](journals-pages/ZewC2i5YdZPsWO8X.htm)|Mummy|Momia|modificada|
|[zj9z2BwVX6TLHdx3.htm](journals-pages/zj9z2BwVX6TLHdx3.htm)|Wellspring Mage (Class Archetype)|Wellspring Mage (Arquetipo de clase)|modificada|
|[ZJHhPFjLnizAaUM1.htm](journals-pages/ZJHhPFjLnizAaUM1.htm)|Cleric|Clérigo|modificada|
|[zkiLWWYzzqoxmN2J.htm](journals-pages/zkiLWWYzzqoxmN2J.htm)|Earth Domain|Dominio Tierra|modificada|
|[zVcfWP1ac0JhNEhY.htm](journals-pages/zVcfWP1ac0JhNEhY.htm)|Quick Adventure Groups|Grupos de Aventura Rápida|modificada|
|[zxY04QNfB90gjLoh.htm](journals-pages/zxY04QNfB90gjLoh.htm)|Magic Warrior|Guerrero mágico|modificada|
|[zymZw8KQe0nkRf5L.htm](journals-pages/zymZw8KQe0nkRf5L.htm)|Victory Points|Puntos de Victoria|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
