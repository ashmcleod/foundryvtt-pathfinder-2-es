# Estado de la traducción (gmg-srd)

 * **modificada**: 98


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[2cyhXgGkmoFe3Phw.htm](gmg-srd/2cyhXgGkmoFe3Phw.htm)|Trained by a Mentor|Entrenado por un Mentor|modificada|
|[3g3uXFsHbfcad8eA.htm](gmg-srd/3g3uXFsHbfcad8eA.htm)|The Fool|El Loco|modificada|
|[3Uwfn0ui7tS277iE.htm](gmg-srd/3Uwfn0ui7tS277iE.htm)|Metropolis|Metrópolis|modificada|
|[5KDGPcdZpdZumUYy.htm](gmg-srd/5KDGPcdZpdZumUYy.htm)|Itinerant|Itinerante|modificada|
|[652cIWAXc8zAxkac.htm](gmg-srd/652cIWAXc8zAxkac.htm)|Matter of Might|Cuestión de poder|modificada|
|[6NOZe9gUfVEedECW.htm](gmg-srd/6NOZe9gUfVEedECW.htm)|Star|Star|modificada|
|[7qsQBWD5sSGz7Fik.htm](gmg-srd/7qsQBWD5sSGz7Fik.htm)|Mercantile Expertise|Pericia Mercantil|modificada|
|[8NKe8N5NsULPShPX.htm](gmg-srd/8NKe8N5NsULPShPX.htm)|The Dead One|El Muerto|modificada|
|[a0mJO7io1hcwMAjJ.htm](gmg-srd/a0mJO7io1hcwMAjJ.htm)|Lost in the Wilderness|Perdido en las tierras vírgenes|modificada|
|[AL562TPAhLCFX62S.htm](gmg-srd/AL562TPAhLCFX62S.htm)|Underground|Underground|modificada|
|[Alf16WU6qK8w1jmq.htm](gmg-srd/Alf16WU6qK8w1jmq.htm)|Sun|Sun|modificada|
|[amEb5dGmSvvAJz7J.htm](gmg-srd/amEb5dGmSvvAJz7J.htm)|Balance|Mantener equilibrio|modificada|
|[BAs1fCaUTmvRjvVT.htm](gmg-srd/BAs1fCaUTmvRjvVT.htm)|Dullard|Dullard|modificada|
|[BbgtR5LmB9ZdPpx1.htm](gmg-srd/BbgtR5LmB9ZdPpx1.htm)|Timely Cure|Cura Oportuna|modificada|
|[bNmSwKSFk6SWWrlj.htm](gmg-srd/bNmSwKSFk6SWWrlj.htm)|Abandoned in a Distant Land|Abandonado en una tierra lejana|modificada|
|[bRQz8uVlYtQ1cwwI.htm](gmg-srd/bRQz8uVlYtQ1cwwI.htm)|Wasteland Survivors|Wasteland Survivors|modificada|
|[bvhckAGjcCJVs0jn.htm](gmg-srd/bvhckAGjcCJVs0jn.htm)|Claimed an Inheritance|Reclamó una herencia|modificada|
|[C4KgMYzmyWVpe721.htm](gmg-srd/C4KgMYzmyWVpe721.htm)|Fool|Fool|modificada|
|[c8bBOo2RPxStZozM.htm](gmg-srd/c8bBOo2RPxStZozM.htm)|Moon|Luna|modificada|
|[ccG9pxYQ9Gg6262b.htm](gmg-srd/ccG9pxYQ9Gg6262b.htm)|Key|Clave|modificada|
|[cD51eAUtbGlp5UKr.htm](gmg-srd/cD51eAUtbGlp5UKr.htm)|Slander|Calumnia|modificada|
|[Cfd7ndT61VjR52OZ.htm](gmg-srd/Cfd7ndT61VjR52OZ.htm)|The Mercenary|El Mercenario|modificada|
|[CfD81LilQAtGuvyx.htm](gmg-srd/CfD81LilQAtGuvyx.htm)|The Void|El Vacío|modificada|
|[cIlNXxPJuy8MtaDM.htm](gmg-srd/cIlNXxPJuy8MtaDM.htm)|Vizier|Visir|modificada|
|[CW53y8R730XoNQSG.htm](gmg-srd/CW53y8R730XoNQSG.htm)|The Champion|El Campeón|modificada|
|[CwyYDIAAuRll2i5R.htm](gmg-srd/CwyYDIAAuRll2i5R.htm)|The Seer|El Vidente|modificada|
|[dcP3oqjsl85p6R2Z.htm](gmg-srd/dcP3oqjsl85p6R2Z.htm)|Kidnapped|Secuestrado|modificada|
|[Dyg3vJnEnReKyYHU.htm](gmg-srd/Dyg3vJnEnReKyYHU.htm)|The Fates|Las Parcas|modificada|
|[e4nkDjB1Y6nqaK2G.htm](gmg-srd/e4nkDjB1Y6nqaK2G.htm)|Betrayed|Betrayed|modificada|
|[eADadfrHPkGNTjEB.htm](gmg-srd/eADadfrHPkGNTjEB.htm)|Accidental Fall|Accidental Fall|modificada|
|[eCB10E0MSvD9s008.htm](gmg-srd/eCB10E0MSvD9s008.htm)|Frontier|Frontera|modificada|
|[ejRPZwAlci3jTli9.htm](gmg-srd/ejRPZwAlci3jTli9.htm)|The Liege Lord|El Señor Predilecto|modificada|
|[Erqgqv8WEtnzBtTu.htm](gmg-srd/Erqgqv8WEtnzBtTu.htm)|The Mystic|El Místico|modificada|
|[F7Tf6APgUKjnCZgv.htm](gmg-srd/F7Tf6APgUKjnCZgv.htm)|The Crafter|El Artesano|modificada|
|[fojjq3VXShnNSOsZ.htm](gmg-srd/fojjq3VXShnNSOsZ.htm)|Academic Community|Comunidad Académica|modificada|
|[FOs2eLbehUzPh9p9.htm](gmg-srd/FOs2eLbehUzPh9p9.htm)|Attained a Magical Gift|Obtuviste un Don Mágico|modificada|
|[G9058881e1ci9X1C.htm](gmg-srd/G9058881e1ci9X1C.htm)|Desperate Intimidation|Intimidación Desesperada|modificada|
|[Gm4AtuFNRgOnzsmF.htm](gmg-srd/Gm4AtuFNRgOnzsmF.htm)|Simple Village|Simple Village|modificada|
|[Gr4v8ROPCxi4hvAO.htm](gmg-srd/Gr4v8ROPCxi4hvAO.htm)|Religious Community|Comunidad religiosa|modificada|
|[HkMzwjBTGgQgsOCW.htm](gmg-srd/HkMzwjBTGgQgsOCW.htm)|Cosmopolitan City|Ciudad Cosmopolita|modificada|
|[iA9QWuYATBPB6thI.htm](gmg-srd/iA9QWuYATBPB6thI.htm)|Knight|Knight|modificada|
|[idZxjeEv8B3x44r1.htm](gmg-srd/idZxjeEv8B3x44r1.htm)|Captured by Giants|Capturado por Gigantes|modificada|
|[IJ3nFDu2YcoP6ynd.htm](gmg-srd/IJ3nFDu2YcoP6ynd.htm)|Throne|Trono|modificada|
|[Ip6kSKGhl3XHQZ93.htm](gmg-srd/Ip6kSKGhl3XHQZ93.htm)|Robbed|Robado|modificada|
|[j9kx5CzfulaFTMOE.htm](gmg-srd/j9kx5CzfulaFTMOE.htm)|Academy Trained|Entrenado en la Academia|modificada|
|[JHQI2skcksaLMPqA.htm](gmg-srd/JHQI2skcksaLMPqA.htm)|Kindly Witch|Kindly Witch|modificada|
|[jtgfTYpcv4FFpWsL.htm](gmg-srd/jtgfTYpcv4FFpWsL.htm)|Died|Murió|modificada|
|[jVh90bfsC6w6iVyy.htm](gmg-srd/jVh90bfsC6w6iVyy.htm)|Euryale (curse)|Euryale (maldición)|modificada|
|[KtkDaTrkBwwupKcE.htm](gmg-srd/KtkDaTrkBwwupKcE.htm)|The Wanderer|The Wanderer|modificada|
|[LekxXsOgEwMODqCS.htm](gmg-srd/LekxXsOgEwMODqCS.htm)|Animal Helpers|Ayudantes Animales|modificada|
|[llfG8iMTFMzVIF9s.htm](gmg-srd/llfG8iMTFMzVIF9s.htm)|Donjon|Donjon|modificada|
|[MCIp76IWZ2pk7dyz.htm](gmg-srd/MCIp76IWZ2pk7dyz.htm)|The Criminal|El delincuente|modificada|
|[mg7oMp0cNa0GJMCS.htm](gmg-srd/mg7oMp0cNa0GJMCS.htm)|The Lover|El Amante|modificada|
|[mHocbJQKBzbHM790.htm](gmg-srd/mHocbJQKBzbHM790.htm)|The Confidante|El Confidente|modificada|
|[niZPxqupDMfIXxJs.htm](gmg-srd/niZPxqupDMfIXxJs.htm)|Homelessness|Homelessness|modificada|
|[NSG1YxOYAtSowNJa.htm](gmg-srd/NSG1YxOYAtSowNJa.htm)|Comet|Cometa|modificada|
|[nsZ93vUEFucsLwRz.htm](gmg-srd/nsZ93vUEFucsLwRz.htm)|Privileged Position|Posición privilegiada|modificada|
|[nzOpdMVLPhoNTxzR.htm](gmg-srd/nzOpdMVLPhoNTxzR.htm)|Comrade-in-Arms|Camarada de armas|modificada|
|[OdD3NuxI5hNAEOVx.htm](gmg-srd/OdD3NuxI5hNAEOVx.htm)|Trade Town|Trade Town|modificada|
|[oDhTOO1WjEZk1qfD.htm](gmg-srd/oDhTOO1WjEZk1qfD.htm)|Witnessed War|Testigo de guerra|modificada|
|[OHKKQ7zTf61vz35h.htm](gmg-srd/OHKKQ7zTf61vz35h.htm)|Liberators|Liberadores|modificada|
|[pd6ZzGL5TA58Fa3p.htm](gmg-srd/pd6ZzGL5TA58Fa3p.htm)|The Hunter|El Cazador|modificada|
|[PVdcBHjBVySBchG1.htm](gmg-srd/PVdcBHjBVySBchG1.htm)|Religious Students|Estudiantes Religiosos|modificada|
|[pwn7lPcwqyGNZyOo.htm](gmg-srd/pwn7lPcwqyGNZyOo.htm)|Survived a Disaster|Supervivencia a una catástrofe|modificada|
|[qFgPiHDBVa3VSNgC.htm](gmg-srd/qFgPiHDBVa3VSNgC.htm)|Gem|Gema|modificada|
|[QhNWhs0isjxE67lu.htm](gmg-srd/QhNWhs0isjxE67lu.htm)|The Fiend|The Fiend|modificada|
|[QxQ0PheFSIJU9rfH.htm](gmg-srd/QxQ0PheFSIJU9rfH.htm)|Talons|Talons|modificada|
|[rDbdifseYxfEWgKg.htm](gmg-srd/rDbdifseYxfEWgKg.htm)|Patron of the Arts|Patrón de las Artes|modificada|
|[re8nmHRiQdU63AYk.htm](gmg-srd/re8nmHRiQdU63AYk.htm)|The Boss|The Boss|modificada|
|[Rh5XWscABJ43TLme.htm](gmg-srd/Rh5XWscABJ43TLme.htm)|The Mentor|El Mentor|modificada|
|[Runwg9xhNrN79JBN.htm](gmg-srd/Runwg9xhNrN79JBN.htm)|Had an Ordinary Childhood|Tuvo una infancia ordinaria|modificada|
|[SbfCzPannCdZEB7c.htm](gmg-srd/SbfCzPannCdZEB7c.htm)|Jester|Jester|modificada|
|[sHC4quiLAzdUe2uT.htm](gmg-srd/sHC4quiLAzdUe2uT.htm)|Accusation of Theft|Acusación de Robo|modificada|
|[SyEl079jcULQp7Ra.htm](gmg-srd/SyEl079jcULQp7Ra.htm)|Front Lines|Front Lines|modificada|
|[TG9L9JvKpomFQUfl.htm](gmg-srd/TG9L9JvKpomFQUfl.htm)|Flames|Flamígera|modificada|
|[tmrnZkUzHVC0s6D9.htm](gmg-srd/tmrnZkUzHVC0s6D9.htm)|Had Your First Kill|Had Your First Kill|modificada|
|[TNruMcXK4wizclQc.htm](gmg-srd/TNruMcXK4wizclQc.htm)|Raided|Asaltado|modificada|
|[UGxZ6vhfq7vQFXTt.htm](gmg-srd/UGxZ6vhfq7vQFXTt.htm)|Magician|Mago|modificada|
|[uK1fqqsUNk7FZsOk.htm](gmg-srd/uK1fqqsUNk7FZsOk.htm)|The Pariah|El Paria|modificada|
|[upnKpiEflAhhFLNQ.htm](gmg-srd/upnKpiEflAhhFLNQ.htm)|Missing Child|Niño Desaparecido|modificada|
|[vD1y8mIHxxw7c8J1.htm](gmg-srd/vD1y8mIHxxw7c8J1.htm)|Coastal Community|Comunidad costera|modificada|
|[VEFcfA4NnWdJSRtp.htm](gmg-srd/VEFcfA4NnWdJSRtp.htm)|Spy|Espía|modificada|
|[vMTOwBTqs8JeE8bH.htm](gmg-srd/vMTOwBTqs8JeE8bH.htm)|Rival Trackers|Rastreadores rivales|modificada|
|[VnrGXdVthTceOlXV.htm](gmg-srd/VnrGXdVthTceOlXV.htm)|Another Ancestry's Settlement|Asentamiento de otra ascendencia|modificada|
|[W2ccu0zeLNHDlCXe.htm](gmg-srd/W2ccu0zeLNHDlCXe.htm)|Bullied|Acosado|modificada|
|[wIGRRmXtzed74qQq.htm](gmg-srd/wIGRRmXtzed74qQq.htm)|Fell In with a Bad Crowd|Fell In with a Bad Crowd|modificada|
|[WXQbdAmQM9HBjc0R.htm](gmg-srd/WXQbdAmQM9HBjc0R.htm)|Relationship Ender|Relationship Ender|modificada|
|[x0vU6WsHcXUNY20y.htm](gmg-srd/x0vU6WsHcXUNY20y.htm)|Ruin|Ruin|modificada|
|[xe0iqQ3XSnDV4tLp.htm](gmg-srd/xe0iqQ3XSnDV4tLp.htm)|Rogue|Rogue|modificada|
|[xGkT6QZGfYE3kBqn.htm](gmg-srd/xGkT6QZGfYE3kBqn.htm)|Skull|Cráneo|modificada|
|[Xi5nvMNJrzsy09ws.htm](gmg-srd/Xi5nvMNJrzsy09ws.htm)|The Well-Connected|El Bien Conectado|modificada|
|[xmgvqyfwYPBXVELo.htm](gmg-srd/xmgvqyfwYPBXVELo.htm)|Met a Fantastic Creature|Met a Fantastic Creature|modificada|
|[y4quheEoo2bcmM6g.htm](gmg-srd/y4quheEoo2bcmM6g.htm)|Seeking Accolades|Buscar Reconocimientos|modificada|
|[yqCZWcX9e3iwmvSS.htm](gmg-srd/yqCZWcX9e3iwmvSS.htm)|Social Maneuvering|Maniobra Social|modificada|
|[YR0uxB5CPPueFjBs.htm](gmg-srd/YR0uxB5CPPueFjBs.htm)|The Relative|El Relativo|modificada|
|[YsY9euHOGCGc3mTE.htm](gmg-srd/YsY9euHOGCGc3mTE.htm)|Won a Competition|Gana una Competición|modificada|
|[yZUZDw8TXX1MvZ4P.htm](gmg-srd/yZUZDw8TXX1MvZ4P.htm)|Called Before Judges|Llamada ante los jueces|modificada|
|[ziWhyyygUG3Lemdu.htm](gmg-srd/ziWhyyygUG3Lemdu.htm)|The Academic|The Academic|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
