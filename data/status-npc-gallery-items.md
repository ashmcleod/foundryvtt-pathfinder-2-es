# Estado de la traducción (npc-gallery-items)

 * **modificada**: 503
 * **ninguna**: 124
 * **vacía**: 111


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0hxn526bxpU9psxJ.htm](npc-gallery-items/0hxn526bxpU9psxJ.htm)|Shiv|
|[18XOpkL2M0BH3sDN.htm](npc-gallery-items/18XOpkL2M0BH3sDN.htm)|Morningstar|+1,striking|
|[1caXZSSxHAApzo4g.htm](npc-gallery-items/1caXZSSxHAApzo4g.htm)|Law and Rhetoric Book|
|[1kzfqcU4yMR8Evgt.htm](npc-gallery-items/1kzfqcU4yMR8Evgt.htm)|Frost Vial (Lesser) (Infused)|
|[27l65icQwiqUX7yl.htm](npc-gallery-items/27l65icQwiqUX7yl.htm)|Trident|+1|
|[2B42tVGi3YO1t4rU.htm](npc-gallery-items/2B42tVGi3YO1t4rU.htm)|Leather Apron|
|[3pDy4fG7Gyy7veGs.htm](npc-gallery-items/3pDy4fG7Gyy7veGs.htm)|Warhammer|+1,striking|
|[4fVWXHO7ivGD8UMb.htm](npc-gallery-items/4fVWXHO7ivGD8UMb.htm)|Gavel|
|[4mQUcJRfogdhtD4e.htm](npc-gallery-items/4mQUcJRfogdhtD4e.htm)|Religious Symbol (Wooden) of Pharasma|
|[4SfHayudG99XPwJP.htm](npc-gallery-items/4SfHayudG99XPwJP.htm)|Shortsword|+1|
|[4zzNfH7d9409n3cP.htm](npc-gallery-items/4zzNfH7d9409n3cP.htm)|Robes|
|[64r9RgELkr6OXrX7.htm](npc-gallery-items/64r9RgELkr6OXrX7.htm)|Alchemist's Tools (Used as "Blessed Items" to Fool Marks)|
|[6FKPyy0QIzlrgYay.htm](npc-gallery-items/6FKPyy0QIzlrgYay.htm)|Scroll of Spectral Hand (Level 2)|
|[6HFhSXAXGd9TV4GQ.htm](npc-gallery-items/6HFhSXAXGd9TV4GQ.htm)|Shovel|
|[6XSEC6evi0cGuYXY.htm](npc-gallery-items/6XSEC6evi0cGuYXY.htm)|Formula Book|
|[6YMPTiP6aT0CgYpJ.htm](npc-gallery-items/6YMPTiP6aT0CgYpJ.htm)|Spellbook|
|[9bruXwALVDFhxs9a.htm](npc-gallery-items/9bruXwALVDFhxs9a.htm)|Broom|
|[9dDjsrPAI4FmqCth.htm](npc-gallery-items/9dDjsrPAI4FmqCth.htm)|Bomber's Eye Elixir (Lesser) (Infused)|
|[aAGb6LDoY8EcYRdE.htm](npc-gallery-items/aAGb6LDoY8EcYRdE.htm)|Pouch of Rocks|
|[Ao7VlJhCpiWg1j18.htm](npc-gallery-items/Ao7VlJhCpiWg1j18.htm)|Court Garb|
|[BIreghuAVYAZ0WoG.htm](npc-gallery-items/BIreghuAVYAZ0WoG.htm)|Medical Textbook|
|[blC0ZLHL8ZjzOyui.htm](npc-gallery-items/blC0ZLHL8ZjzOyui.htm)|Elixir of Life (Lesser) (Infused)|
|[BmfnMuD7K7AAnAI6.htm](npc-gallery-items/BmfnMuD7K7AAnAI6.htm)|Map|
|[bzK8nvv2KV60Xe8s.htm](npc-gallery-items/bzK8nvv2KV60Xe8s.htm)|Scroll of Heal (Level 1)|
|[CK3FzTPn8aAFEwnB.htm](npc-gallery-items/CK3FzTPn8aAFEwnB.htm)|Scroll of Speak with Animals (Level 2)|
|[csVpBw7aFwkZ7W2a.htm](npc-gallery-items/csVpBw7aFwkZ7W2a.htm)|Robes|
|[d1CLaCtFlghOslm4.htm](npc-gallery-items/d1CLaCtFlghOslm4.htm)|Religious Symbol of Nethys|
|[DJcssfmvEXxJU37q.htm](npc-gallery-items/DJcssfmvEXxJU37q.htm)|Poetry Book|
|[DNrmjXUT1oMnmx43.htm](npc-gallery-items/DNrmjXUT1oMnmx43.htm)|Pewter Mug|
|[EozEU2EXtYF3roQx.htm](npc-gallery-items/EozEU2EXtYF3roQx.htm)|Spellbook|
|[EYXqnzW1WGw1aWCa.htm](npc-gallery-items/EYXqnzW1WGw1aWCa.htm)|Longsword|+1|
|[f9Y5nPNe31oqVE1V.htm](npc-gallery-items/f9Y5nPNe31oqVE1V.htm)|Kukri|+1|
|[ggpEhjADs42d0Rdk.htm](npc-gallery-items/ggpEhjADs42d0Rdk.htm)|Arrows|
|[h15HeWZxI5S8VAFk.htm](npc-gallery-items/h15HeWZxI5S8VAFk.htm)|Law Book|
|[h3tZcwvLExRJno4u.htm](npc-gallery-items/h3tZcwvLExRJno4u.htm)|Scroll of Acid Arrow (Level 2)|
|[H91fpqLmPSX7S8ow.htm](npc-gallery-items/H91fpqLmPSX7S8ow.htm)|Longspear|+1|
|[hHCP08kAOR4XgVJL.htm](npc-gallery-items/hHCP08kAOR4XgVJL.htm)|Hood|
|[HuqsExeC8DG7dt5Z.htm](npc-gallery-items/HuqsExeC8DG7dt5Z.htm)|Barkeep's Apron|
|[I6H2KqzFAWfPj0ZV.htm](npc-gallery-items/I6H2KqzFAWfPj0ZV.htm)|Cane|
|[IbjfdrjbLUv06vFD.htm](npc-gallery-items/IbjfdrjbLUv06vFD.htm)|Rapier|+1|
|[iCIavuhoO4Uam2Vj.htm](npc-gallery-items/iCIavuhoO4Uam2Vj.htm)|Shovel|
|[ilIEY861r4bkrSfL.htm](npc-gallery-items/ilIEY861r4bkrSfL.htm)|Miner's Harness|
|[ImjzyAwR4eEDXf0q.htm](npc-gallery-items/ImjzyAwR4eEDXf0q.htm)|Cultist Garb|
|[InRtCxbhS6HJi1kR.htm](npc-gallery-items/InRtCxbhS6HJi1kR.htm)|Greataxe|+1|
|[ioBhBqoLt4XX6CZA.htm](npc-gallery-items/ioBhBqoLt4XX6CZA.htm)|Naval Pike|
|[ipz89wB16JTSERLW.htm](npc-gallery-items/ipz89wB16JTSERLW.htm)|Journal|
|[iYZVob3Hya2x788S.htm](npc-gallery-items/iYZVob3Hya2x788S.htm)|Spell Component Pouch|
|[Ja5JnCCeUp0qanDJ.htm](npc-gallery-items/Ja5JnCCeUp0qanDJ.htm)|Apple|
|[jMsBYZOEyBIRK1en.htm](npc-gallery-items/jMsBYZOEyBIRK1en.htm)|Sickle|+1|
|[JtEtXdQY26QW7Yh7.htm](npc-gallery-items/JtEtXdQY26QW7Yh7.htm)|Pewter Mug|
|[KT37rRaRxFL95Tl3.htm](npc-gallery-items/KT37rRaRxFL95Tl3.htm)|Work Coat|
|[l57vO8xlVLGunBbi.htm](npc-gallery-items/l57vO8xlVLGunBbi.htm)|Composite Longbow|+1|
|[ldL413k00pgvZnif.htm](npc-gallery-items/ldL413k00pgvZnif.htm)|Spellbook (Fiendish Hypotheses and Protections from Same)|
|[ljSnticrrCWLH49B.htm](npc-gallery-items/ljSnticrrCWLH49B.htm)|Ceremonial Robes|
|[lo1kjTX2r4gJJ3gT.htm](npc-gallery-items/lo1kjTX2r4gJJ3gT.htm)|Light Hammer|+1,striking|
|[lWk7EJdUr0IOisEA.htm](npc-gallery-items/lWk7EJdUr0IOisEA.htm)|Composite Longbow|+1,striking|
|[Mcqi9fIHy5a1ffpL.htm](npc-gallery-items/Mcqi9fIHy5a1ffpL.htm)|Rapier|+1|
|[MGSxc2vgOsjBpioT.htm](npc-gallery-items/MGSxc2vgOsjBpioT.htm)|Guildmaster's Uniform|
|[Mi0QstLGjxyJGIa5.htm](npc-gallery-items/Mi0QstLGjxyJGIa5.htm)|+1 Clothing (Explorer's)|
|[mjTbb3MZMBpSS8o5.htm](npc-gallery-items/mjTbb3MZMBpSS8o5.htm)|Bastard Sword|+1,striking|
|[mKFp9wl5UZg1QAXE.htm](npc-gallery-items/mKFp9wl5UZg1QAXE.htm)|Staff|+1|
|[MO84pY6jx8r53gTb.htm](npc-gallery-items/MO84pY6jx8r53gTb.htm)|Serving Platter|
|[mOC9pnzLIeqAQ8i0.htm](npc-gallery-items/mOC9pnzLIeqAQ8i0.htm)|Religious Symbol of Pharasma|
|[MW2HziWk52XbjMte.htm](npc-gallery-items/MW2HziWk52XbjMte.htm)|Cutlass|
|[Mz1hKhGITzmi33Pu.htm](npc-gallery-items/Mz1hKhGITzmi33Pu.htm)|Assorted Maps|
|[Nv6NVuyp91007LQt.htm](npc-gallery-items/Nv6NVuyp91007LQt.htm)|Loaded Dice|
|[nwfcLkf39CUdTMCe.htm](npc-gallery-items/nwfcLkf39CUdTMCe.htm)|Manifesto|
|[nXKjhXyXe4UIUDqy.htm](npc-gallery-items/nXKjhXyXe4UIUDqy.htm)|Crossbow|+1|
|[O3WnGGds29lL8Ajd.htm](npc-gallery-items/O3WnGGds29lL8Ajd.htm)|Small Harp|
|[O6gOcXC1xkeICXwL.htm](npc-gallery-items/O6gOcXC1xkeICXwL.htm)|Indecipherable Book of Sigils|
|[o8WBtfQQ8hDZ7qe7.htm](npc-gallery-items/o8WBtfQQ8hDZ7qe7.htm)|Judge's Robes|
|[oHeLKugv0k3a7CQB.htm](npc-gallery-items/oHeLKugv0k3a7CQB.htm)|Composite Longbow|
|[OkuUkkH9XVGKJSre.htm](npc-gallery-items/OkuUkkH9XVGKJSre.htm)|Composite Longbow|+1,striking|
|[Ow0z6F2Xw9PbQnD7.htm](npc-gallery-items/Ow0z6F2Xw9PbQnD7.htm)|Spellbook (Abominable Missives of the Atrophied)|
|[p6fuDAVu9s3zeg9e.htm](npc-gallery-items/p6fuDAVu9s3zeg9e.htm)|Ledger|
|[pb2Ck8v2MCrK8gzf.htm](npc-gallery-items/pb2Ck8v2MCrK8gzf.htm)|Crossbow|+1|
|[pV1Zg9nyy2H7ISQ6.htm](npc-gallery-items/pV1Zg9nyy2H7ISQ6.htm)|Innkeeper's Apron|
|[PvAkl60zgsMhzuXb.htm](npc-gallery-items/PvAkl60zgsMhzuXb.htm)|Acid Flask (Moderate) (Infused)|
|[q7PNw0PeVpAbdvGg.htm](npc-gallery-items/q7PNw0PeVpAbdvGg.htm)|Cloak|
|[QQXMLJd5WLldP21P.htm](npc-gallery-items/QQXMLJd5WLldP21P.htm)|Hooded Robe|
|[qX24xR2DF5OwD6iF.htm](npc-gallery-items/qX24xR2DF5OwD6iF.htm)|Pewter Mug|
|[qz40qujYeMWWRnDt.htm](npc-gallery-items/qz40qujYeMWWRnDt.htm)|Scroll of Remove Fear (Level 2)|
|[r7zVCfgSD6RfG3yN.htm](npc-gallery-items/r7zVCfgSD6RfG3yN.htm)|Hide Armor|
|[Rb6YoSdQpTTq789g.htm](npc-gallery-items/Rb6YoSdQpTTq789g.htm)|Textbook|
|[RDzTak37QN2P8E5h.htm](npc-gallery-items/RDzTak37QN2P8E5h.htm)|Staff|+1|
|[rJH4Sq14VL3sMhLc.htm](npc-gallery-items/rJH4Sq14VL3sMhLc.htm)|+1 Hellknight Plate|
|[RKa2PsbkATet8Zb9.htm](npc-gallery-items/RKa2PsbkATet8Zb9.htm)|Drunkard's Outfit|
|[RlqNIctK3YtpYMow.htm](npc-gallery-items/RlqNIctK3YtpYMow.htm)|Simple Injury Poison|
|[ro3cy6ShC6y96iaD.htm](npc-gallery-items/ro3cy6ShC6y96iaD.htm)|Rapier|+1|
|[Rr4yoc0Caa777jb7.htm](npc-gallery-items/Rr4yoc0Caa777jb7.htm)|Dagger|+1|
|[ryVaqSZq9cQ7wPix.htm](npc-gallery-items/ryVaqSZq9cQ7wPix.htm)|Scholarly Robes|
|[S5Yokvt6N9kVghRk.htm](npc-gallery-items/S5Yokvt6N9kVghRk.htm)|Composite Shortbow|+1,striking|
|[S6XXdREi3Cb7Xh3V.htm](npc-gallery-items/S6XXdREi3Cb7Xh3V.htm)|Servant's Uniform|
|[SHCWDZKcHBeqUqMh.htm](npc-gallery-items/SHCWDZKcHBeqUqMh.htm)|Tax Ledgers|
|[SXTvovlQ2R6oXYos.htm](npc-gallery-items/SXTvovlQ2R6oXYos.htm)|Rapier|+1,striking|
|[t6XJOXNfa5XsgeQT.htm](npc-gallery-items/t6XJOXNfa5XsgeQT.htm)|Chain Mail with Palace Insignia|
|[tW8uP6mKqhtlwmPP.htm](npc-gallery-items/tW8uP6mKqhtlwmPP.htm)|+1 Half Plate|
|[TyYuupAf39PFsWJO.htm](npc-gallery-items/TyYuupAf39PFsWJO.htm)|Spell Component Pouch|
|[uA4DNYAmIuhSQAgl.htm](npc-gallery-items/uA4DNYAmIuhSQAgl.htm)|Lute|
|[uBrl3zYakAsOXqbM.htm](npc-gallery-items/uBrl3zYakAsOXqbM.htm)|Clothing with Jewelry|
|[uf3cFRfFs23gJ4eW.htm](npc-gallery-items/uf3cFRfFs23gJ4eW.htm)|Religious Text of Nethys|
|[ukfbB1s02nAdmdwQ.htm](npc-gallery-items/ukfbB1s02nAdmdwQ.htm)|Scroll Case with Ship's Charts|
|[un2PSYOK7aGBLat2.htm](npc-gallery-items/un2PSYOK7aGBLat2.htm)|Flask of Whiskey|
|[VcNmSmQQR4qxCCxK.htm](npc-gallery-items/VcNmSmQQR4qxCCxK.htm)|Alchemist's Fire (Moderate) (Infused)|
|[vDou2pKWxCBzmvqr.htm](npc-gallery-items/vDou2pKWxCBzmvqr.htm)|Gravedigger's Garb|
|[W5ngvSMX8dRsptvJ.htm](npc-gallery-items/W5ngvSMX8dRsptvJ.htm)|Tax Documents|
|[weCX0QLmEtlkJj6e.htm](npc-gallery-items/weCX0QLmEtlkJj6e.htm)|Pitchfork|
|[wJwF7vhkxxkiLdKt.htm](npc-gallery-items/wJwF7vhkxxkiLdKt.htm)|Lute|
|[WKPSQhPteRbqC14q.htm](npc-gallery-items/WKPSQhPteRbqC14q.htm)|Mortar and Pestle|
|[WQEUzXA436GuObJu.htm](npc-gallery-items/WQEUzXA436GuObJu.htm)|Greataxe|+1|
|[wSGmAT3ciB7uLXoe.htm](npc-gallery-items/wSGmAT3ciB7uLXoe.htm)|Bottled Lightning (Lesser) (Infused)|
|[wtkxx3QceWjfCcym.htm](npc-gallery-items/wtkxx3QceWjfCcym.htm)|Frost Vial (Moderate) (Infused)|
|[XDBnPMR5BrHQO9Pj.htm](npc-gallery-items/XDBnPMR5BrHQO9Pj.htm)|Ledger|
|[Xja3Z5ROmRPkUeeO.htm](npc-gallery-items/Xja3Z5ROmRPkUeeO.htm)|Leather Apron|
|[yjTVn5zGiibr7UH8.htm](npc-gallery-items/yjTVn5zGiibr7UH8.htm)|Crossbow|+1,striking|
|[Yk9MTrG2nXJYu38f.htm](npc-gallery-items/Yk9MTrG2nXJYu38f.htm)|Thunderstone (Lesser) (Infused)|
|[ynKm4RDwMaA0vdpv.htm](npc-gallery-items/ynKm4RDwMaA0vdpv.htm)|Journal|
|[YVRMs40wzsiQC7Si.htm](npc-gallery-items/YVRMs40wzsiQC7Si.htm)|Collection of Expired Documents with Intact Seals|
|[YW1vBoCGfZcVfMiP.htm](npc-gallery-items/YW1vBoCGfZcVfMiP.htm)|Composite Longbow|+1|
|[z1TGYF24OzHmgdjp.htm](npc-gallery-items/z1TGYF24OzHmgdjp.htm)|Whiskey|
|[Z6BNfng94z1xdzn3.htm](npc-gallery-items/Z6BNfng94z1xdzn3.htm)|Scalpel|
|[ZMcT1sMmOY8rwe3w.htm](npc-gallery-items/ZMcT1sMmOY8rwe3w.htm)|Shiv|
|[ZraokHXxg7pBRXud.htm](npc-gallery-items/ZraokHXxg7pBRXud.htm)|Rugged Clothes with Tool Belt|
|[ZuiIsrJudzF67zHr.htm](npc-gallery-items/ZuiIsrJudzF67zHr.htm)|+1 Chain Shirt|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[06QUpYDzeWLuHjVF.htm](npc-gallery-items/06QUpYDzeWLuHjVF.htm)|Shortsword|Espada corta|modificada|
|[087PGXgVjLNGs7lG.htm](npc-gallery-items/087PGXgVjLNGs7lG.htm)|Javelin|Javelin|modificada|
|[0fWZn3xodpxwco8r.htm](npc-gallery-items/0fWZn3xodpxwco8r.htm)|Swear Vengeance|Jura Venganza|modificada|
|[0mY6UXKCPuoJcVKz.htm](npc-gallery-items/0mY6UXKCPuoJcVKz.htm)|Rapier|Estoque|modificada|
|[0osYGMTj1AfagtgG.htm](npc-gallery-items/0osYGMTj1AfagtgG.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[0OzSGNJ8fmj9Z2dh.htm](npc-gallery-items/0OzSGNJ8fmj9Z2dh.htm)|Scimitar|Cimitarra|modificada|
|[0PlCgdUO4JNfJMKr.htm](npc-gallery-items/0PlCgdUO4JNfJMKr.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[0WoLHxcEDfHig6LB.htm](npc-gallery-items/0WoLHxcEDfHig6LB.htm)|Scoundrel's Feint|Fintar de granuja|modificada|
|[0yKbSsTBHJzUb4J9.htm](npc-gallery-items/0yKbSsTBHJzUb4J9.htm)|Pitchfork|Horca|modificada|
|[0Z0tfKCoRaKagvVT.htm](npc-gallery-items/0Z0tfKCoRaKagvVT.htm)|Pike and Strike|Golpe y Golpe|modificada|
|[1dkm6dgNqE06TZD0.htm](npc-gallery-items/1dkm6dgNqE06TZD0.htm)|Pewter Mug|Taza de peltre|modificada|
|[1gLrx7OTMtRPbK9N.htm](npc-gallery-items/1gLrx7OTMtRPbK9N.htm)|Brutal Beating|Paliza brutal|modificada|
|[1VRZfyweUQjfqW3F.htm](npc-gallery-items/1VRZfyweUQjfqW3F.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[1Z1VM6NR01OCRpFf.htm](npc-gallery-items/1Z1VM6NR01OCRpFf.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[22gFqjuhKcKMgJKN.htm](npc-gallery-items/22gFqjuhKcKMgJKN.htm)|Warden's Protection|Protección de guardión|modificada|
|[22PsMMGp8JINhVoi.htm](npc-gallery-items/22PsMMGp8JINhVoi.htm)|Rapier|Estoque|modificada|
|[28SnHCyal79ByXz4.htm](npc-gallery-items/28SnHCyal79ByXz4.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[2HONqJ3KSR0h25We.htm](npc-gallery-items/2HONqJ3KSR0h25We.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[2LYwFejQFhQ3id2V.htm](npc-gallery-items/2LYwFejQFhQ3id2V.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[2pNLHufgvxymiXGj.htm](npc-gallery-items/2pNLHufgvxymiXGj.htm)|Rock|Roca|modificada|
|[2PrqruymHWvMXHjI.htm](npc-gallery-items/2PrqruymHWvMXHjI.htm)|Air of Authority|Aire de autoridad|modificada|
|[2uSSFi0lqFoWgzYI.htm](npc-gallery-items/2uSSFi0lqFoWgzYI.htm)|Home Turf|Home Turf|modificada|
|[2w1WQr635JVnsORb.htm](npc-gallery-items/2w1WQr635JVnsORb.htm)|Swift Sneak|Movimiento furtivo rápido|modificada|
|[2xwVAGUh92PRESt7.htm](npc-gallery-items/2xwVAGUh92PRESt7.htm)|Wizard School Spell|Hechizo de la escuela de magos|modificada|
|[3CHnQSHfFAroFmAn.htm](npc-gallery-items/3CHnQSHfFAroFmAn.htm)|Light Hammer|Martillo ligero|modificada|
|[3Cmc9ZLwNvRhmJBB.htm](npc-gallery-items/3Cmc9ZLwNvRhmJBB.htm)|Mobility|Movilidad|modificada|
|[3CtPYZLeE2j7fIME.htm](npc-gallery-items/3CtPYZLeE2j7fIME.htm)|Shiv|Shiv|modificada|
|[3EX2JaX17gPKVhcb.htm](npc-gallery-items/3EX2JaX17gPKVhcb.htm)|Main-Gauche|Main-gauche|modificada|
|[3FUfdj3U5vEftIPg.htm](npc-gallery-items/3FUfdj3U5vEftIPg.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[3Gsk5abTPsMK7L90.htm](npc-gallery-items/3Gsk5abTPsMK7L90.htm)|Dagger|Daga|modificada|
|[3Hp4zP3MQ5RzRFcu.htm](npc-gallery-items/3Hp4zP3MQ5RzRFcu.htm)|Fist|Puño|modificada|
|[3i4w75rSKQiwZzwD.htm](npc-gallery-items/3i4w75rSKQiwZzwD.htm)|Fated Doom|Fated Doom|modificada|
|[3k43BvFaLIDzoL5a.htm](npc-gallery-items/3k43BvFaLIDzoL5a.htm)|Crossbow|Ballesta|modificada|
|[3LcqeSNm2BGnGC1G.htm](npc-gallery-items/3LcqeSNm2BGnGC1G.htm)|Placate|Placate|modificada|
|[3lmMOkUuWRYkYOfO.htm](npc-gallery-items/3lmMOkUuWRYkYOfO.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[486JeF49AVqoD8d0.htm](npc-gallery-items/486JeF49AVqoD8d0.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[49sfbmwEvN94dGhS.htm](npc-gallery-items/49sfbmwEvN94dGhS.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[4baqnRP77X4lxpxU.htm](npc-gallery-items/4baqnRP77X4lxpxU.htm)|Wild Empathy|Empatía salvaje|modificada|
|[4E5jd0bmls7oDWHu.htm](npc-gallery-items/4E5jd0bmls7oDWHu.htm)|Trident|Trident|modificada|
|[4l9I2HyaE5ptxZSF.htm](npc-gallery-items/4l9I2HyaE5ptxZSF.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[4VbQq13CfCCwqfYt.htm](npc-gallery-items/4VbQq13CfCCwqfYt.htm)|Foot|Pie|modificada|
|[598qUP8bkS1TdMEY.htm](npc-gallery-items/598qUP8bkS1TdMEY.htm)|Fist|Puño|modificada|
|[5as8LbntWBRwhdy8.htm](npc-gallery-items/5as8LbntWBRwhdy8.htm)|Sentry's Aim|Sentry's Aim|modificada|
|[5DPI512dyzb4L4kR.htm](npc-gallery-items/5DPI512dyzb4L4kR.htm)|Shield Block|Bloquear con escudo|modificada|
|[5h9TgMqtTUK56q1p.htm](npc-gallery-items/5h9TgMqtTUK56q1p.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[5iTqDt4fDSnYuJIg.htm](npc-gallery-items/5iTqDt4fDSnYuJIg.htm)|Dual Disarm|Doble Desarmar|modificada|
|[5qtQA0th9HWkfecU.htm](npc-gallery-items/5qtQA0th9HWkfecU.htm)|Hunt Prey|Perseguir presa|modificada|
|[6048zx9AI7OaIsjX.htm](npc-gallery-items/6048zx9AI7OaIsjX.htm)|Reach Spell|Conjuro de alcance|modificada|
|[6bZGX49gZGcVxwBY.htm](npc-gallery-items/6bZGX49gZGcVxwBY.htm)|Primal Focus Spells|Conjuros de foco primigenio|modificada|
|[6KJ0ua534rpJ3q5Q.htm](npc-gallery-items/6KJ0ua534rpJ3q5Q.htm)|Shortsword|Espada corta|modificada|
|[6kOiyngYv38Ec0lB.htm](npc-gallery-items/6kOiyngYv38Ec0lB.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[6SS8rVfwSj7Oy7WY.htm](npc-gallery-items/6SS8rVfwSj7Oy7WY.htm)|Healing Hands|Manos curativas|modificada|
|[6UQShM6aleyfYDgH.htm](npc-gallery-items/6UQShM6aleyfYDgH.htm)|Big Swing|Gran barrido|modificada|
|[78xUeN4lAiJy7HB8.htm](npc-gallery-items/78xUeN4lAiJy7HB8.htm)|Paragon's Guard|Guardia modelo de Paragon|modificada|
|[7bwgpaZGG2Bs6DSx.htm](npc-gallery-items/7bwgpaZGG2Bs6DSx.htm)|Apple|Manzana|modificada|
|[7JKronzMIeoVdmyI.htm](npc-gallery-items/7JKronzMIeoVdmyI.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[7STN0PrV0AblMmmM.htm](npc-gallery-items/7STN0PrV0AblMmmM.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[7uuo0yXpPiywEFNQ.htm](npc-gallery-items/7uuo0yXpPiywEFNQ.htm)|Counterspell|Contraconjuro|modificada|
|[7uXdmkkS4JVZOIyN.htm](npc-gallery-items/7uXdmkkS4JVZOIyN.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[7ydMyqCO1oh7cHUR.htm](npc-gallery-items/7ydMyqCO1oh7cHUR.htm)|Fist|Puño|modificada|
|[80iH1mCHZeW5rWYE.htm](npc-gallery-items/80iH1mCHZeW5rWYE.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[85x1pisleQwObtMf.htm](npc-gallery-items/85x1pisleQwObtMf.htm)|Mark for Death|Marcar para morir|modificada|
|[86ELyZJGF5f8M7ew.htm](npc-gallery-items/86ELyZJGF5f8M7ew.htm)|Bravery|Bravery|modificada|
|[86tScORfgOX4zuix.htm](npc-gallery-items/86tScORfgOX4zuix.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[8812wH4PteCqa89F.htm](npc-gallery-items/8812wH4PteCqa89F.htm)|Dagger|Daga|modificada|
|[8EfthxjczAzYkpvD.htm](npc-gallery-items/8EfthxjczAzYkpvD.htm)|Dagger|Daga|modificada|
|[8fAOSxgE0FgWpvsE.htm](npc-gallery-items/8fAOSxgE0FgWpvsE.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[8l8fkQuORmfdmuFO.htm](npc-gallery-items/8l8fkQuORmfdmuFO.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[8sKYzAJzGo9HTaC9.htm](npc-gallery-items/8sKYzAJzGo9HTaC9.htm)|Group Impression|Impresión de grupo|modificada|
|[9CtaEDgxqQK3AGrO.htm](npc-gallery-items/9CtaEDgxqQK3AGrO.htm)|Dagger|Daga|modificada|
|[9hxxkHZZGdkhbZvg.htm](npc-gallery-items/9hxxkHZZGdkhbZvg.htm)|Shield Warden|Guardián escudo|modificada|
|[9KFKSYQInYw7RRFw.htm](npc-gallery-items/9KFKSYQInYw7RRFw.htm)|Greataxe|Greataxe|modificada|
|[9OxsjzXYOnydXvcb.htm](npc-gallery-items/9OxsjzXYOnydXvcb.htm)|Fist|Puño|modificada|
|[9PJJ2Aqeorbh7BnP.htm](npc-gallery-items/9PJJ2Aqeorbh7BnP.htm)|Throw Rock|Arrojar roca|modificada|
|[9UbRDyf3EovxiABp.htm](npc-gallery-items/9UbRDyf3EovxiABp.htm)|Fickle Prophecy|Fickle Prophecy|modificada|
|[9xXxlNwXM90sw8zk.htm](npc-gallery-items/9xXxlNwXM90sw8zk.htm)|Doctor's Hand|Doctor's Hand|modificada|
|[9xyeARIhxCpAj8BU.htm](npc-gallery-items/9xyeARIhxCpAj8BU.htm)|Greatclub|Greatclub|modificada|
|[a6VT956imq0UeyhM.htm](npc-gallery-items/a6VT956imq0UeyhM.htm)|Fist|Puño|modificada|
|[aAgpROGXTToyiZfx.htm](npc-gallery-items/aAgpROGXTToyiZfx.htm)|Foot|Pie|modificada|
|[agaJs5LEhgmy0sPO.htm](npc-gallery-items/agaJs5LEhgmy0sPO.htm)|Scout's Warning|Aviso del batidor|modificada|
|[aMJGSTnE6Wgp9MBr.htm](npc-gallery-items/aMJGSTnE6Wgp9MBr.htm)|Guiding Words|Palabras guía|modificada|
|[ammC8wH7C165yxrp.htm](npc-gallery-items/ammC8wH7C165yxrp.htm)|Light in the Dark|Luz en la oscuridad|modificada|
|[Ant0lStzSYo01Zhd.htm](npc-gallery-items/Ant0lStzSYo01Zhd.htm)|Halberd|Alabarda|modificada|
|[AtMtAwu4qZIQZq8E.htm](npc-gallery-items/AtMtAwu4qZIQZq8E.htm)|Dagger|Daga|modificada|
|[AuocSDVewXxiFzuz.htm](npc-gallery-items/AuocSDVewXxiFzuz.htm)|Fist|Puño|modificada|
|[AWKTmRlR5ZVNFq1A.htm](npc-gallery-items/AWKTmRlR5ZVNFq1A.htm)|Dagger|Daga|modificada|
|[axdkeSbnyjnpjO2h.htm](npc-gallery-items/axdkeSbnyjnpjO2h.htm)|Speaker of the Oceans|Altavoz de los océanos|modificada|
|[ayPb7Lg7AFYqIfzk.htm](npc-gallery-items/ayPb7Lg7AFYqIfzk.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[aYsvRHgZ6n5NX68c.htm](npc-gallery-items/aYsvRHgZ6n5NX68c.htm)|Trap Finder|Buscador de trampas|modificada|
|[Azm5VrtnHNvlTD1i.htm](npc-gallery-items/Azm5VrtnHNvlTD1i.htm)|Inspirational Presence|Presencia Inspiradora|modificada|
|[b65fCKLd7xxZXc5w.htm](npc-gallery-items/b65fCKLd7xxZXc5w.htm)|Aquatic Predator|Aquatic Predator|modificada|
|[b8lQp0O8fNE08x0h.htm](npc-gallery-items/b8lQp0O8fNE08x0h.htm)|Kukri|Kukri|modificada|
|[BbtsWaLbyjDHtboq.htm](npc-gallery-items/BbtsWaLbyjDHtboq.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[BDewONWVQqYhfwRL.htm](npc-gallery-items/BDewONWVQqYhfwRL.htm)|Gavel|Gavel|modificada|
|[Bg7DMaLAy2rtjxkb.htm](npc-gallery-items/Bg7DMaLAy2rtjxkb.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[BHcpl6Nlf6nVff26.htm](npc-gallery-items/BHcpl6Nlf6nVff26.htm)|Deadly Simplicity|Simplicidad mortal|modificada|
|[blPj44XSQdjiu1MS.htm](npc-gallery-items/blPj44XSQdjiu1MS.htm)|Fence's Eye|Ojo de barda|modificada|
|[BpD2byfrbMItSgfy.htm](npc-gallery-items/BpD2byfrbMItSgfy.htm)|+15 to Sense Motive|+15 a Sentido Motivo|modificada|
|[bqpvez0u4Rw93KXo.htm](npc-gallery-items/bqpvez0u4Rw93KXo.htm)|Font of Knowledge|Fuente del Conocimiento|modificada|
|[BR4ZECJ9kO4oph8g.htm](npc-gallery-items/BR4ZECJ9kO4oph8g.htm)|Bloodline Magic|Magia de linaje|modificada|
|[Bre79vI4roX7eGSB.htm](npc-gallery-items/Bre79vI4roX7eGSB.htm)|Fist|Puño|modificada|
|[Bz5e5nzwVDHVigQ0.htm](npc-gallery-items/Bz5e5nzwVDHVigQ0.htm)|Dagger|Daga|modificada|
|[C7PdppuFM1J5AfZc.htm](npc-gallery-items/C7PdppuFM1J5AfZc.htm)|Shiv|Shiv|modificada|
|[C9BvM6sI4AN0SssQ.htm](npc-gallery-items/C9BvM6sI4AN0SssQ.htm)|Shovel|Pala|modificada|
|[cah7DtpiMX62b2rT.htm](npc-gallery-items/cah7DtpiMX62b2rT.htm)|Longspear|Longspear|modificada|
|[cHWzyBoiaRRRBq99.htm](npc-gallery-items/cHWzyBoiaRRRBq99.htm)|-2 to Will Saves vs. Higher Ranking Cult Members|-2 a Salvaciones de Voluntad vs Miembros de Cultos de Alto Rango.|modificada|
|[CJp7VcMyI9Wp4QX0.htm](npc-gallery-items/CJp7VcMyI9Wp4QX0.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[cjVbggOxvEzXmIQm.htm](npc-gallery-items/cjVbggOxvEzXmIQm.htm)|Piton Pin|Piton Pin|modificada|
|[CKzyNTs3y8kKsSFY.htm](npc-gallery-items/CKzyNTs3y8kKsSFY.htm)|Efficient Capture|Captura eficiente|modificada|
|[Cmu4PgaVIciVRKDC.htm](npc-gallery-items/Cmu4PgaVIciVRKDC.htm)|Dagger|Daga|modificada|
|[CmUq3q1BL7VoIvyM.htm](npc-gallery-items/CmUq3q1BL7VoIvyM.htm)|Cutlass|Cutlass|modificada|
|[CmXRYDHVFD9zOTaa.htm](npc-gallery-items/CmXRYDHVFD9zOTaa.htm)|Dagger|Daga|modificada|
|[cMz56RGF3H0a0ZQi.htm](npc-gallery-items/cMz56RGF3H0a0ZQi.htm)|Quick Rummage|Hurgar Rápido|modificada|
|[CN4GwNgOryN6LNFo.htm](npc-gallery-items/CN4GwNgOryN6LNFo.htm)|Fist|Puño|modificada|
|[cnmmUfSvnBHbPJ3N.htm](npc-gallery-items/cnmmUfSvnBHbPJ3N.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[coq6krhuSNTUKuyB.htm](npc-gallery-items/coq6krhuSNTUKuyB.htm)|Deny Advantage|Denegar ventaja|modificada|
|[CqwxqwQxgNIp6UdX.htm](npc-gallery-items/CqwxqwQxgNIp6UdX.htm)|Staff Of Abjuration|Bastón de abjuración|modificada|
|[CRo6e5ADHqXxRyBr.htm](npc-gallery-items/CRo6e5ADHqXxRyBr.htm)|Occult Spells Known|Hechizos de Ocultismo Conocidos|modificada|
|[CUkJIaXcAVXlBYoH.htm](npc-gallery-items/CUkJIaXcAVXlBYoH.htm)|Smith's Fury|Furia de Smith|modificada|
|[cviTvCcO5isNFaU7.htm](npc-gallery-items/cviTvCcO5isNFaU7.htm)|Sickle|Hoz|modificada|
|[cxWmbPgxDBajNjqz.htm](npc-gallery-items/cxWmbPgxDBajNjqz.htm)|Scimitar|Cimitarra|modificada|
|[CY3VwzlwEsLmLp7q.htm](npc-gallery-items/CY3VwzlwEsLmLp7q.htm)|Dagger|Daga|modificada|
|[cYpzRpaINHeW2XxC.htm](npc-gallery-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|Arma envenenada|modificada|
|[cz2QOg6QSmJNA3YL.htm](npc-gallery-items/cz2QOg6QSmJNA3YL.htm)|Sway the Judge and Jury|Influir en el juez y el jurado|modificada|
|[D91SDgzufetsXtHT.htm](npc-gallery-items/D91SDgzufetsXtHT.htm)|Acid Flask|Frasco de ácido|modificada|
|[d9TOZvyquMo6o18K.htm](npc-gallery-items/d9TOZvyquMo6o18K.htm)|Quick Bomber|Bombardero rápido|modificada|
|[db0QJhDLMLOerNa6.htm](npc-gallery-items/db0QJhDLMLOerNa6.htm)|Channel Smite|Canalizar castigo|modificada|
|[dbDMr7uWS78nPenZ.htm](npc-gallery-items/dbDMr7uWS78nPenZ.htm)|Dagger|Daga|modificada|
|[dCBZN8wCmO7ipEHO.htm](npc-gallery-items/dCBZN8wCmO7ipEHO.htm)|Dagger|Daga|modificada|
|[ddMOXx1g84tLNThI.htm](npc-gallery-items/ddMOXx1g84tLNThI.htm)|+1 Status to All Saves vs. Poison|+1 situación a todas las salvaciones contra veneno.|modificada|
|[DDRbAS5r8U0XMma7.htm](npc-gallery-items/DDRbAS5r8U0XMma7.htm)|Greataxe|Greataxe|modificada|
|[DgGWQZ2UfWzKedET.htm](npc-gallery-items/DgGWQZ2UfWzKedET.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[DipxFEdiddYHWirp.htm](npc-gallery-items/DipxFEdiddYHWirp.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[DjeP7DBDv1Ue87eW.htm](npc-gallery-items/DjeP7DBDv1Ue87eW.htm)|Whip|Látigo|modificada|
|[dlhm2yJ4EUGym0CG.htm](npc-gallery-items/dlhm2yJ4EUGym0CG.htm)|Gang Up|Unir fuerzas|modificada|
|[drNAUPLqWo7QSh3l.htm](npc-gallery-items/drNAUPLqWo7QSh3l.htm)|Shielded Advance|Avance con escudo|modificada|
|[dSa8OQfOz63lYKQ2.htm](npc-gallery-items/dSa8OQfOz63lYKQ2.htm)|Versatile Performance|Interpretación versátil|modificada|
|[dTwii5UOFOwMyY0C.htm](npc-gallery-items/dTwii5UOFOwMyY0C.htm)|Font of Gossip|Fuente de chismes|modificada|
|[DV9NQLrI4FouRCpe.htm](npc-gallery-items/DV9NQLrI4FouRCpe.htm)|Fist|Puño|modificada|
|[dYLk3rxvErYoaP9G.htm](npc-gallery-items/dYLk3rxvErYoaP9G.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[dYlvFC5F1daX6NF8.htm](npc-gallery-items/dYlvFC5F1daX6NF8.htm)|Shortsword|Espada corta|modificada|
|[DzrwPVv4yZGfqMNf.htm](npc-gallery-items/DzrwPVv4yZGfqMNf.htm)|Improved Communal Healing|Curación en común mejorada|modificada|
|[eAaIdI0UJUrcmPzC.htm](npc-gallery-items/eAaIdI0UJUrcmPzC.htm)|Bar Brawler|Bar Brawler|modificada|
|[EbHva548irAoyT4c.htm](npc-gallery-items/EbHva548irAoyT4c.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ebZ4RtDXFWOGqY2i.htm](npc-gallery-items/ebZ4RtDXFWOGqY2i.htm)|Rapier|Estoque|modificada|
|[ECBmark63yV59X2C.htm](npc-gallery-items/ECBmark63yV59X2C.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[edMKJZQVDeLF48ZV.htm](npc-gallery-items/edMKJZQVDeLF48ZV.htm)|Reach Spell|Conjuro de alcance|modificada|
|[EHw3qV7vYGDQdfPB.htm](npc-gallery-items/EHw3qV7vYGDQdfPB.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[EMerfEpCLFxD8gAF.htm](npc-gallery-items/EMerfEpCLFxD8gAF.htm)|Staff|Báculo|modificada|
|[emTBNypG9xLkrGmv.htm](npc-gallery-items/emTBNypG9xLkrGmv.htm)|Greataxe|Greataxe|modificada|
|[eplixHfDeknEF97x.htm](npc-gallery-items/eplixHfDeknEF97x.htm)|Chameleon Step|Chameleon Paso|modificada|
|[Eq3KajvZR0XWgw2w.htm](npc-gallery-items/Eq3KajvZR0XWgw2w.htm)|Rapier|Estoque|modificada|
|[erMgbyNlTUBjOhqo.htm](npc-gallery-items/erMgbyNlTUBjOhqo.htm)|Falchion|Falchion|modificada|
|[EtuysfUGpQnBFAaU.htm](npc-gallery-items/EtuysfUGpQnBFAaU.htm)|Rapier|Estoque|modificada|
|[Ew9YdRtlJy7JUSHa.htm](npc-gallery-items/Ew9YdRtlJy7JUSHa.htm)|Fist|Puño|modificada|
|[ezgpbQSl4o3vFJaU.htm](npc-gallery-items/ezgpbQSl4o3vFJaU.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[F0Yrpyr0NczIzhGY.htm](npc-gallery-items/F0Yrpyr0NczIzhGY.htm)|Steady Balance|Equilibrio firme|modificada|
|[f6BZlUvuAkJjyx2T.htm](npc-gallery-items/f6BZlUvuAkJjyx2T.htm)|Vengeful Edge|Filo Vengativo|modificada|
|[F7IHKiavOdLoatuo.htm](npc-gallery-items/F7IHKiavOdLoatuo.htm)|Doctor's Hand|Doctor's Hand|modificada|
|[f9317vgvLL2FaL7N.htm](npc-gallery-items/f9317vgvLL2FaL7N.htm)|Fortitude Saves|Fortitude Saves|modificada|
|[F9lM0H0vX25rlQiB.htm](npc-gallery-items/F9lM0H0vX25rlQiB.htm)|Sworn Duty|Sworn Duty|modificada|
|[FDgG3VBojXvvTpd3.htm](npc-gallery-items/FDgG3VBojXvvTpd3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fGw7sipW1eI40byQ.htm](npc-gallery-items/fGw7sipW1eI40byQ.htm)|Dagger|Daga|modificada|
|[FkNhevhHp4HfH05a.htm](npc-gallery-items/FkNhevhHp4HfH05a.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[flLLoUGHSVJire4L.htm](npc-gallery-items/flLLoUGHSVJire4L.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[FMRFRcuXn2QHFX2v.htm](npc-gallery-items/FMRFRcuXn2QHFX2v.htm)|Bravery|Bravery|modificada|
|[FmriI9fZfj3I2QS1.htm](npc-gallery-items/FmriI9fZfj3I2QS1.htm)|Swig|Swig|modificada|
|[FMXvHmfCarIbLAk5.htm](npc-gallery-items/FMXvHmfCarIbLAk5.htm)|Broom|Escoba|modificada|
|[fp3XmyB0pcpjCBex.htm](npc-gallery-items/fp3XmyB0pcpjCBex.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[FQO3E18nwnNURI9p.htm](npc-gallery-items/FQO3E18nwnNURI9p.htm)|Universal Obedience|Obediencia Universal|modificada|
|[fSa7yGc6AlAoFW23.htm](npc-gallery-items/fSa7yGc6AlAoFW23.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[FuOqFNDYZSFUVtPi.htm](npc-gallery-items/FuOqFNDYZSFUVtPi.htm)|Cutlery|Cubiertos|modificada|
|[fWy0XIMLXO8QjFJn.htm](npc-gallery-items/fWy0XIMLXO8QjFJn.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[FXduVQD4iGJxgXX2.htm](npc-gallery-items/FXduVQD4iGJxgXX2.htm)|Naval Pike|Naval Pike|modificada|
|[G4RkhwmVzzHUmIqk.htm](npc-gallery-items/G4RkhwmVzzHUmIqk.htm)|Club|Club|modificada|
|[G5iBcxxzOecJApLi.htm](npc-gallery-items/G5iBcxxzOecJApLi.htm)|Crossbow|Ballesta|modificada|
|[geD4pfraSGQRXXvE.htm](npc-gallery-items/geD4pfraSGQRXXvE.htm)|+1 Status to All Saves vs. Fear|+1 situación a todas las salvaciones contra miedo.|modificada|
|[Gf4bCH3UInm7pjOu.htm](npc-gallery-items/Gf4bCH3UInm7pjOu.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[ggCUTswEGOjGvjj0.htm](npc-gallery-items/ggCUTswEGOjGvjj0.htm)|Sudden Charge|Carga súbita|modificada|
|[GhbdiBXGiH9AN1pw.htm](npc-gallery-items/GhbdiBXGiH9AN1pw.htm)|Staff|Báculo|modificada|
|[ghFGvItVlZL7x9SC.htm](npc-gallery-items/ghFGvItVlZL7x9SC.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[gHkZ5EkRCbXagaxl.htm](npc-gallery-items/gHkZ5EkRCbXagaxl.htm)|Drunken Rage|Furia de Borrachos|modificada|
|[GICVztYSUDv0IIkz.htm](npc-gallery-items/GICVztYSUDv0IIkz.htm)|Cat Fall|Caída de gato|modificada|
|[GmvgnOPx8qubo9xm.htm](npc-gallery-items/GmvgnOPx8qubo9xm.htm)|Advancing Flourish|Floritura de avance|modificada|
|[groFZoIcMCSJcNZO.htm](npc-gallery-items/groFZoIcMCSJcNZO.htm)|+2 Circumstance to All Saves vs. Dream and Sleep|+2 Circunstancia a todas las salvaciones contra soñar y dormir.|modificada|
|[GvFkJmDJIJRneI79.htm](npc-gallery-items/GvFkJmDJIJRneI79.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[GxVV9OrtL5eoSx2b.htm](npc-gallery-items/GxVV9OrtL5eoSx2b.htm)|Forager|Forrajeador|modificada|
|[gzBR3N7gO6KPegiv.htm](npc-gallery-items/gzBR3N7gO6KPegiv.htm)|Mask Bond|Lazo de Máscara|modificada|
|[h0oO1nc9v9yl5OgB.htm](npc-gallery-items/h0oO1nc9v9yl5OgB.htm)|Lip Reader|Lector de Labios|modificada|
|[h1SgfPkIaJazPwYz.htm](npc-gallery-items/h1SgfPkIaJazPwYz.htm)|Glittering Distraction|Glittering Distraction|modificada|
|[H8PMiZFsy2MQfLN2.htm](npc-gallery-items/H8PMiZFsy2MQfLN2.htm)|Gather Converts|Reunir conversos|modificada|
|[HBgAXRPFxVlqiDZ8.htm](npc-gallery-items/HBgAXRPFxVlqiDZ8.htm)|Shortbow|Arco corto|modificada|
|[hc2576PgevK5i1qc.htm](npc-gallery-items/hc2576PgevK5i1qc.htm)|Pitch Bale|Paca|modificada|
|[HDAF4Nls9WJAWOeR.htm](npc-gallery-items/HDAF4Nls9WJAWOeR.htm)|Submerged Stealth|Sigilo sumergido|modificada|
|[hHng07DmszKk5pNt.htm](npc-gallery-items/hHng07DmszKk5pNt.htm)|Crossbow|Ballesta|modificada|
|[hjekwptdXb14cWSj.htm](npc-gallery-items/hjekwptdXb14cWSj.htm)|Bravery|Bravery|modificada|
|[HnDaKsWEBEBi5eJX.htm](npc-gallery-items/HnDaKsWEBEBi5eJX.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[hoNnL8BsPzPuGERZ.htm](npc-gallery-items/hoNnL8BsPzPuGERZ.htm)|Swinging Strike|Golpe de Golpe|modificada|
|[HpLKljvtfXk5Em41.htm](npc-gallery-items/HpLKljvtfXk5Em41.htm)|Book|Libro|modificada|
|[hpPQrfATIi4fXq3L.htm](npc-gallery-items/hpPQrfATIi4fXq3L.htm)|Cane|Bastón|modificada|
|[Hrp3jkhyAGyCrwfW.htm](npc-gallery-items/Hrp3jkhyAGyCrwfW.htm)|Force Body|Cuerpo de Fuerza|modificada|
|[Ht7knXtXeyTkKi2n.htm](npc-gallery-items/Ht7knXtXeyTkKi2n.htm)|Evasion|Evasión|modificada|
|[Hw1KYT6hIaAHcXkI.htm](npc-gallery-items/Hw1KYT6hIaAHcXkI.htm)|Navigator's Edge|Navigator's Edge|modificada|
|[HXCEPfKkb2TP6NVy.htm](npc-gallery-items/HXCEPfKkb2TP6NVy.htm)|Bravery|Bravery|modificada|
|[hYh1jMda40LahOdv.htm](npc-gallery-items/hYh1jMda40LahOdv.htm)|Sickle|Hoz|modificada|
|[hyLN8l8Z2WSxFAXC.htm](npc-gallery-items/hyLN8l8Z2WSxFAXC.htm)|Running Reload|Recarga a la carrera|modificada|
|[i3KBgRUcT11VAT2c.htm](npc-gallery-items/i3KBgRUcT11VAT2c.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[i6RGiBt0UOTBg3go.htm](npc-gallery-items/i6RGiBt0UOTBg3go.htm)|Club|Club|modificada|
|[IAFE4UVaYTfZgp3J.htm](npc-gallery-items/IAFE4UVaYTfZgp3J.htm)|Fist|Puño|modificada|
|[icyjQtX7NGtgTdJj.htm](npc-gallery-items/icyjQtX7NGtgTdJj.htm)|Claw|Garra|modificada|
|[IEibCXh97z4dVqH9.htm](npc-gallery-items/IEibCXh97z4dVqH9.htm)|Bandit's Ambush|Emboscar Bandido|modificada|
|[ImzpAPq1SgMcf7t3.htm](npc-gallery-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|Perseguir presa|modificada|
|[iQWAwtvFxRVGqW0G.htm](npc-gallery-items/iQWAwtvFxRVGqW0G.htm)|Journal|Diario|modificada|
|[ItJWnelnPHXRTb86.htm](npc-gallery-items/ItJWnelnPHXRTb86.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[iUTM9WOzUZIkVt1D.htm](npc-gallery-items/iUTM9WOzUZIkVt1D.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[IWfm1LwDDnDCkgUi.htm](npc-gallery-items/IWfm1LwDDnDCkgUi.htm)|Fist|Puño|modificada|
|[iWiQjH6UZGPhap8G.htm](npc-gallery-items/iWiQjH6UZGPhap8G.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[Iwy3TBA4Dhrb3256.htm](npc-gallery-items/Iwy3TBA4Dhrb3256.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[IWzZhxjRQbZJM9bU.htm](npc-gallery-items/IWzZhxjRQbZJM9bU.htm)|+3 Status to Reflex vs. Damaging Effects|+3 situación a Reflejos contra efectos dañinos.|modificada|
|[J0gZtGInMyuCyCuB.htm](npc-gallery-items/J0gZtGInMyuCyCuB.htm)|Longsword|Longsword|modificada|
|[j1wf4dSXkGypJjRs.htm](npc-gallery-items/j1wf4dSXkGypJjRs.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[J2jz17JAQtHVQs5I.htm](npc-gallery-items/J2jz17JAQtHVQs5I.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[J6LhLBjnU1kyUhwK.htm](npc-gallery-items/J6LhLBjnU1kyUhwK.htm)|Shield Ally|Aliado de escudo|modificada|
|[Ja5QiBYnwlN8lKXg.htm](npc-gallery-items/Ja5QiBYnwlN8lKXg.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[jbBEFIXHqLEFkuxj.htm](npc-gallery-items/jbBEFIXHqLEFkuxj.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[JbbyThxk68mjKgkw.htm](npc-gallery-items/JbbyThxk68mjKgkw.htm)|Fist|Puño|modificada|
|[JbPgkPBw6I7dZFcQ.htm](npc-gallery-items/JbPgkPBw6I7dZFcQ.htm)|Scalpel|Bisturí|modificada|
|[JCFQaCMK8sKMFgL2.htm](npc-gallery-items/JCFQaCMK8sKMFgL2.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[jHMJPUtmDcCC20ND.htm](npc-gallery-items/jHMJPUtmDcCC20ND.htm)|Crossbow|Ballesta|modificada|
|[jhzTyAeLv0K1sK7c.htm](npc-gallery-items/jhzTyAeLv0K1sK7c.htm)|Book|Libro|modificada|
|[JLeDsdQFNsR2XCIX.htm](npc-gallery-items/JLeDsdQFNsR2XCIX.htm)|Torch|Antorcha|modificada|
|[jr5nhXsMkIwlhAOa.htm](npc-gallery-items/jr5nhXsMkIwlhAOa.htm)|Moderate Frost Vial|Vial de escarcha, moderada|modificada|
|[JUbm5Xf0IURQGSTX.htm](npc-gallery-items/JUbm5Xf0IURQGSTX.htm)|Ledger|Ledger|modificada|
|[JVQnk3tWMv7fuQax.htm](npc-gallery-items/JVQnk3tWMv7fuQax.htm)|Fist|Puño|modificada|
|[KbYGZddUNQGhrhEf.htm](npc-gallery-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[kcbRLFttvvktolJn.htm](npc-gallery-items/kcbRLFttvvktolJn.htm)|Hydration|Hidratación|modificada|
|[kcXoIhefcYSfr7CL.htm](npc-gallery-items/kcXoIhefcYSfr7CL.htm)|Guide's Warning|Advertencia del Guía|modificada|
|[KdjtCPQTvuJzzmNN.htm](npc-gallery-items/KdjtCPQTvuJzzmNN.htm)|Crossbow|Ballesta|modificada|
|[KDV6e31ms8EioLrR.htm](npc-gallery-items/KDV6e31ms8EioLrR.htm)|Dagger|Daga|modificada|
|[keHqnY2IJOfiVKRj.htm](npc-gallery-items/keHqnY2IJOfiVKRj.htm)|Rapier|Estoque|modificada|
|[KhEHpoNP1iXOpths.htm](npc-gallery-items/KhEHpoNP1iXOpths.htm)|Club|Club|modificada|
|[kotwyraYf9vVKqEY.htm](npc-gallery-items/kotwyraYf9vVKqEY.htm)|Fascinating Dance|Danza Fascinante|modificada|
|[kPjRjJyV9Xs4FsAB.htm](npc-gallery-items/kPjRjJyV9Xs4FsAB.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[KRLpKVZ3AZwkE4cj.htm](npc-gallery-items/KRLpKVZ3AZwkE4cj.htm)|+2 Status to Perception to Find Traps|+2 de situación a Percepción para encontrar trampas.|modificada|
|[kSd8rIlnXiVczYg0.htm](npc-gallery-items/kSd8rIlnXiVczYg0.htm)|Deny Advantage|Denegar ventaja|modificada|
|[kx6tL1KbenrQWtCB.htm](npc-gallery-items/kx6tL1KbenrQWtCB.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[kzNiR7bLBxL4aWfR.htm](npc-gallery-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|Desenvainado rápido|modificada|
|[L2NMWuwie5fk4t5D.htm](npc-gallery-items/L2NMWuwie5fk4t5D.htm)|Beat a Retreat|Beat a Retreat|modificada|
|[Laex9xerP0tAva7M.htm](npc-gallery-items/Laex9xerP0tAva7M.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[LbKXY3iqGS2l93BZ.htm](npc-gallery-items/LbKXY3iqGS2l93BZ.htm)|Fist|Puño|modificada|
|[lC7KFntiTwd1FRo7.htm](npc-gallery-items/lC7KFntiTwd1FRo7.htm)|Pickpocket|Carterista|modificada|
|[ld6AdRlIeDnctV9m.htm](npc-gallery-items/ld6AdRlIeDnctV9m.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[LEi8MMikbUZD1avz.htm](npc-gallery-items/LEi8MMikbUZD1avz.htm)|Shield Block|Bloquear con escudo|modificada|
|[LGdgJYiYY4gmLKOx.htm](npc-gallery-items/LGdgJYiYY4gmLKOx.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[lKG5nFZzi5NYvRU3.htm](npc-gallery-items/lKG5nFZzi5NYvRU3.htm)|Dagger|Daga|modificada|
|[ll3LaxvkACnszsxH.htm](npc-gallery-items/ll3LaxvkACnszsxH.htm)|Live to Tell the Tale|Vivir para contarlo|modificada|
|[LMckIZmrAQt7HWW3.htm](npc-gallery-items/LMckIZmrAQt7HWW3.htm)|Unstable Compounds|Compuestos Inestables|modificada|
|[lpMiICSGm7J1NIMh.htm](npc-gallery-items/lpMiICSGm7J1NIMh.htm)|Snare Crafting|Elaborar trampas de lazo Artesanía|modificada|
|[lrHNxw2aet6y4AAg.htm](npc-gallery-items/lrHNxw2aet6y4AAg.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[LtLpVRWe7Qsb7Y8A.htm](npc-gallery-items/LtLpVRWe7Qsb7Y8A.htm)|Bodyguard's Defense|Defensa del Guardaespaldas|modificada|
|[lTt5I810cFQrBGBC.htm](npc-gallery-items/lTt5I810cFQrBGBC.htm)|Arcane Focus Spells|Conjuros de foco arcano|modificada|
|[LvkfpNccs65oqyIV.htm](npc-gallery-items/LvkfpNccs65oqyIV.htm)|Precision Edge|Borde de precisión|modificada|
|[LzDRsXeV3aAZe1AF.htm](npc-gallery-items/LzDRsXeV3aAZe1AF.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[M6mU2n4cS5QZRHvV.htm](npc-gallery-items/M6mU2n4cS5QZRHvV.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[m9he0rG2ytGNLtE9.htm](npc-gallery-items/m9he0rG2ytGNLtE9.htm)|Apprentice's Ambition|Apprentice's Ambition|modificada|
|[ma0kNlN1eN2YPKXO.htm](npc-gallery-items/ma0kNlN1eN2YPKXO.htm)|Heft Crate|Heft Crate|modificada|
|[McH1gJqQMQRVSagF.htm](npc-gallery-items/McH1gJqQMQRVSagF.htm)|Pewter Mug|Taza de peltre|modificada|
|[mCWRIeeB6PFExTmR.htm](npc-gallery-items/mCWRIeeB6PFExTmR.htm)|Hunt Prey|Perseguir presa|modificada|
|[MePUvMSpXtRD7gNJ.htm](npc-gallery-items/MePUvMSpXtRD7gNJ.htm)|Flail|Mayal|modificada|
|[mgVHJtmU3oAVM8wL.htm](npc-gallery-items/mgVHJtmU3oAVM8wL.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[MKdjcAmWOXZkk2Pu.htm](npc-gallery-items/MKdjcAmWOXZkk2Pu.htm)|Catch Rock|Atrapar roca|modificada|
|[MLCrUvDlugE5BKqe.htm](npc-gallery-items/MLCrUvDlugE5BKqe.htm)|Staff|Báculo|modificada|
|[Mlr3n6w8aB9t1a5K.htm](npc-gallery-items/Mlr3n6w8aB9t1a5K.htm)|Infused Items|Equipos infundidos|modificada|
|[MlZNOt3aiHAeXqiD.htm](npc-gallery-items/MlZNOt3aiHAeXqiD.htm)|Sap|Sap|modificada|
|[mOaqXByjpJOQroyH.htm](npc-gallery-items/mOaqXByjpJOQroyH.htm)|Timely Advice|Consejo Oportuno|modificada|
|[MpZVk32vk8f54dFz.htm](npc-gallery-items/MpZVk32vk8f54dFz.htm)|Crossbow|Ballesta|modificada|
|[mtGgMNRGtkuPRFCN.htm](npc-gallery-items/mtGgMNRGtkuPRFCN.htm)|Longsword|Longsword|modificada|
|[muJs58oNvKAXYuXV.htm](npc-gallery-items/muJs58oNvKAXYuXV.htm)|Hatchet|Hacha|modificada|
|[N2HV6ZqTWrwK3PxJ.htm](npc-gallery-items/N2HV6ZqTWrwK3PxJ.htm)|Gavel|Gavel|modificada|
|[N85njvyWLxpngIQ4.htm](npc-gallery-items/N85njvyWLxpngIQ4.htm)|Druid Order Spells|Conjuros de orden druida|modificada|
|[naPxWIBQxbkOgD8M.htm](npc-gallery-items/naPxWIBQxbkOgD8M.htm)|Breach the Abyss|Emerger el Abismo|modificada|
|[NaQSwHuqBJmhgld8.htm](npc-gallery-items/NaQSwHuqBJmhgld8.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[nFxNDxHPYmc47Mlx.htm](npc-gallery-items/nFxNDxHPYmc47Mlx.htm)|Dagger|Daga|modificada|
|[nFYahNWZUkU0gnJ9.htm](npc-gallery-items/nFYahNWZUkU0gnJ9.htm)|Hatchet|Hacha|modificada|
|[nhdJ9mXQ9NXj6n7V.htm](npc-gallery-items/nhdJ9mXQ9NXj6n7V.htm)|+2 Status to Reflex Saves vs. Traps|+2 situación a las salvaciones de reflejos contra trampas.|modificada|
|[NjMo70hHW4elTqEW.htm](npc-gallery-items/NjMo70hHW4elTqEW.htm)|Rapier|Estoque|modificada|
|[NKmPaEo138UaRnmO.htm](npc-gallery-items/NKmPaEo138UaRnmO.htm)|Experienced Hand|Mano Experimentada|modificada|
|[NLeDblkhTEvLGjwy.htm](npc-gallery-items/NLeDblkhTEvLGjwy.htm)|Reflex Saves|Reflex Saves|modificada|
|[nN82jRXcz7hgkXiE.htm](npc-gallery-items/nN82jRXcz7hgkXiE.htm)|Dagger|Daga|modificada|
|[NQR9pSqVCQqJO0b7.htm](npc-gallery-items/NQR9pSqVCQqJO0b7.htm)|Bottle|Botella|modificada|
|[nu42JIeBWdi2MnrN.htm](npc-gallery-items/nu42JIeBWdi2MnrN.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[NUzbNVXkKMop7XIE.htm](npc-gallery-items/NUzbNVXkKMop7XIE.htm)|Dagger|Daga|modificada|
|[nV2KEb0hxYozbDOL.htm](npc-gallery-items/nV2KEb0hxYozbDOL.htm)|Weapon Mastery|Maestría con las armas|modificada|
|[O4GUtfCmOYOhNgf2.htm](npc-gallery-items/O4GUtfCmOYOhNgf2.htm)|Dagger|Daga|modificada|
|[o4oz661UQlAOVWiz.htm](npc-gallery-items/o4oz661UQlAOVWiz.htm)|Bottled Lightning|Rayo Embotellado|modificada|
|[o5K5Jo1qZTIda54i.htm](npc-gallery-items/o5K5Jo1qZTIda54i.htm)|Wizard Prepared Spells|Hechizos Preparados Mago|modificada|
|[o8GJ3AVTDFtS1dFG.htm](npc-gallery-items/o8GJ3AVTDFtS1dFG.htm)|Sickle|Hoz|modificada|
|[O8PG0gBxUuoMxwH2.htm](npc-gallery-items/O8PG0gBxUuoMxwH2.htm)|Retributive Strike|Golpe retributivo|modificada|
|[oAhc8baMBkq1f865.htm](npc-gallery-items/oAhc8baMBkq1f865.htm)|Medical Textbook|Libro de texto médico|modificada|
|[oc10jg3Rm2uXz5eg.htm](npc-gallery-items/oc10jg3Rm2uXz5eg.htm)|Crossbow|Ballesta|modificada|
|[oc2k5FQjYXACg6rP.htm](npc-gallery-items/oc2k5FQjYXACg6rP.htm)|Stone Pestle|Maja de piedra|modificada|
|[OcO2C4FT46HfzihZ.htm](npc-gallery-items/OcO2C4FT46HfzihZ.htm)|Shovel|Pala|modificada|
|[OjePIuXOLgAEb4RG.htm](npc-gallery-items/OjePIuXOLgAEb4RG.htm)|No Quarter!|¡Sin cuartel!|modificada|
|[OkdkQX03m4gr7M3e.htm](npc-gallery-items/OkdkQX03m4gr7M3e.htm)|Shield Block|Bloquear con escudo|modificada|
|[Oq7EJED40zeLYJUJ.htm](npc-gallery-items/Oq7EJED40zeLYJUJ.htm)|Deceiver's Surprise|Deceiver's Surprise|modificada|
|[OqsCqhHLnN6naAWU.htm](npc-gallery-items/OqsCqhHLnN6naAWU.htm)|Scroll Mastery|Scroll Mastery|modificada|
|[OSqvPT6Kb8Od7g2m.htm](npc-gallery-items/OSqvPT6Kb8Od7g2m.htm)|Shortsword|Espada corta|modificada|
|[oSXltTfrdndgGVfN.htm](npc-gallery-items/oSXltTfrdndgGVfN.htm)|Swinging Strike|Golpe de Golpe|modificada|
|[OTCwjYQZzd5T0upd.htm](npc-gallery-items/OTCwjYQZzd5T0upd.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[owBd74YHegCqxDhR.htm](npc-gallery-items/owBd74YHegCqxDhR.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[owq1vQqeFtTypbuU.htm](npc-gallery-items/owq1vQqeFtTypbuU.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[OzTxpPbEz9oZ7Oxz.htm](npc-gallery-items/OzTxpPbEz9oZ7Oxz.htm)|Mobility|Movilidad|modificada|
|[p0JGAWaRJxrH24fB.htm](npc-gallery-items/p0JGAWaRJxrH24fB.htm)|Bardic Lore|Saber bárdico|modificada|
|[p0JJ5Z6nze1w1CXi.htm](npc-gallery-items/p0JJ5Z6nze1w1CXi.htm)|Trained Animal|Adiestrar animal|modificada|
|[P0WSBDjQC4AcYXXj.htm](npc-gallery-items/P0WSBDjQC4AcYXXj.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[P2ky9f2YijdmoVk9.htm](npc-gallery-items/P2ky9f2YijdmoVk9.htm)|Sap|Sap|modificada|
|[P3HCHCil1R8QALJv.htm](npc-gallery-items/P3HCHCil1R8QALJv.htm)|Fist|Puño|modificada|
|[Pa0KJtbEU8gMH1Qh.htm](npc-gallery-items/Pa0KJtbEU8gMH1Qh.htm)|Torch Combatant|Antorcha Combatiente|modificada|
|[pCgvFzMbb4y6tLfW.htm](npc-gallery-items/pCgvFzMbb4y6tLfW.htm)|Sage's Analysis|Análisis de Sage|modificada|
|[pcW9etyXWo6vgBDy.htm](npc-gallery-items/pcW9etyXWo6vgBDy.htm)|Shortsword|Espada corta|modificada|
|[pCZpELfahQYNki30.htm](npc-gallery-items/pCZpELfahQYNki30.htm)|Cutlery|Cubiertos|modificada|
|[PDidp7qMSw5qRoXU.htm](npc-gallery-items/PDidp7qMSw5qRoXU.htm)|Quick Draw|Desenvainado rápido|modificada|
|[phnggMzXOyEB9A1S.htm](npc-gallery-items/phnggMzXOyEB9A1S.htm)|Club|Club|modificada|
|[PlvlB09wLGdgmyo3.htm](npc-gallery-items/PlvlB09wLGdgmyo3.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[PqeleLSAo4wtVlzd.htm](npc-gallery-items/PqeleLSAo4wtVlzd.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[pvLUpob2OzcBpXiY.htm](npc-gallery-items/pvLUpob2OzcBpXiY.htm)|Cite Precedent|Cite Precedent|modificada|
|[pZ0qTSs1z35vx57T.htm](npc-gallery-items/pZ0qTSs1z35vx57T.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[pZfnSGB52sgqtEKM.htm](npc-gallery-items/pZfnSGB52sgqtEKM.htm)|Crossbow|Ballesta|modificada|
|[q0BbteyMJiNAGthF.htm](npc-gallery-items/q0BbteyMJiNAGthF.htm)|Frost Vial|Gélida de frasco de escarcha|modificada|
|[q2DuiV2Er7pxJDvH.htm](npc-gallery-items/q2DuiV2Er7pxJDvH.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Q3RfoNXTp5dIw2Mx.htm](npc-gallery-items/Q3RfoNXTp5dIw2Mx.htm)|Behead|Decapitar|modificada|
|[q8RnxJPpBW9f5Ie5.htm](npc-gallery-items/q8RnxJPpBW9f5Ie5.htm)|Forager|Forrajeador|modificada|
|[Q95qJwP5SCa0IlGu.htm](npc-gallery-items/Q95qJwP5SCa0IlGu.htm)|Medical Malpractice|Negligencia Médica|modificada|
|[Q9h6X4WFBsWo4KlV.htm](npc-gallery-items/Q9h6X4WFBsWo4KlV.htm)|Rock|Roca|modificada|
|[qfzrHQkkZcEFrhFT.htm](npc-gallery-items/qfzrHQkkZcEFrhFT.htm)|You're Next|Ahora te toca a ti|modificada|
|[qIRZdfRLhqBnYMWt.htm](npc-gallery-items/qIRZdfRLhqBnYMWt.htm)|Chart a Course|Chart a Course|modificada|
|[qmCssarLjtwLY11r.htm](npc-gallery-items/qmCssarLjtwLY11r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qNEPB3RKC3SlV4YE.htm](npc-gallery-items/qNEPB3RKC3SlV4YE.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[qOhRxpDdOg4c6aNs.htm](npc-gallery-items/qOhRxpDdOg4c6aNs.htm)|Bedside Manner|Bedside Manner|modificada|
|[qQfETAyjFjGSYQV3.htm](npc-gallery-items/qQfETAyjFjGSYQV3.htm)|Shield Block|Bloquear con escudo|modificada|
|[qu7Gla2a7DxXInkc.htm](npc-gallery-items/qu7Gla2a7DxXInkc.htm)|Warhammer|Warhammer|modificada|
|[qw0aIOiOvQLo7bUz.htm](npc-gallery-items/qw0aIOiOvQLo7bUz.htm)|Crossbow|Ballesta|modificada|
|[Qw88JvXNINA3GNOT.htm](npc-gallery-items/Qw88JvXNINA3GNOT.htm)|Moderate Alchemist's Fire|Fuego de alquimista moderado|modificada|
|[QW9oExZljGxszfI0.htm](npc-gallery-items/QW9oExZljGxszfI0.htm)|Crossbow|Ballesta|modificada|
|[qy60SQR1jiyW4CXc.htm](npc-gallery-items/qy60SQR1jiyW4CXc.htm)|Snagging Strike|Golpe imprevisto|modificada|
|[r19Dz88pBsPS4lU2.htm](npc-gallery-items/r19Dz88pBsPS4lU2.htm)|Medical Wisdom|Sabiduría Médica|modificada|
|[R3Qe1BmMhgp4UHSo.htm](npc-gallery-items/R3Qe1BmMhgp4UHSo.htm)|Moderate Acid Flask|Frasco de ácido moderado|modificada|
|[R4Ai5HX7MjgLGEyG.htm](npc-gallery-items/R4Ai5HX7MjgLGEyG.htm)|Dangerous Sorcery|Hechicería peligrosa|modificada|
|[R5pKOSIyZ8tVn26G.htm](npc-gallery-items/R5pKOSIyZ8tVn26G.htm)|Dagger|Daga|modificada|
|[Rgd5z8pZXoiA9cop.htm](npc-gallery-items/Rgd5z8pZXoiA9cop.htm)|Staff|Báculo|modificada|
|[RnJieV2KgJwl8PgM.htm](npc-gallery-items/RnJieV2KgJwl8PgM.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[RoENPeDeCIhLDS5H.htm](npc-gallery-items/RoENPeDeCIhLDS5H.htm)|Trident|Trident|modificada|
|[rTeH6wIQdfQpy5CH.htm](npc-gallery-items/rTeH6wIQdfQpy5CH.htm)|Hunt Prey|Perseguir presa|modificada|
|[rWb3x7u63QvEkLfQ.htm](npc-gallery-items/rWb3x7u63QvEkLfQ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[rwsjmSGbF6Nuxl2R.htm](npc-gallery-items/rwsjmSGbF6Nuxl2R.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rxhsNPSPQ1q9Zs6z.htm](npc-gallery-items/rxhsNPSPQ1q9Zs6z.htm)|Living Sextant|Living Sextant|modificada|
|[RzSdb5m56ixW5tfw.htm](npc-gallery-items/RzSdb5m56ixW5tfw.htm)|Mace|Maza|modificada|
|[s0IuVvw3zTYVeGYV.htm](npc-gallery-items/s0IuVvw3zTYVeGYV.htm)|Bastard Sword|Espada Bastarda|modificada|
|[S0MOf3jyFwTZYtAP.htm](npc-gallery-items/S0MOf3jyFwTZYtAP.htm)|Naval Pike|Naval Pike|modificada|
|[s4BkXBiBAgyWmBrU.htm](npc-gallery-items/s4BkXBiBAgyWmBrU.htm)|Deny Advantage|Denegar ventaja|modificada|
|[S4syqUe0hjyn6BQn.htm](npc-gallery-items/S4syqUe0hjyn6BQn.htm)|Crossbow|Ballesta|modificada|
|[sDwLsrlMceRxlzIo.htm](npc-gallery-items/sDwLsrlMceRxlzIo.htm)|Deny Advantage|Denegar ventaja|modificada|
|[sDz2pTgrdn7OJC6y.htm](npc-gallery-items/sDz2pTgrdn7OJC6y.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[ShvU5c3eNvpu7NSs.htm](npc-gallery-items/ShvU5c3eNvpu7NSs.htm)|Shortsword|Espada corta|modificada|
|[SjfWr7t0YSZQdt8c.htm](npc-gallery-items/SjfWr7t0YSZQdt8c.htm)|Snare Crafting|Elaborar trampas de lazo Artesanía|modificada|
|[skjl8IwD4XG0LeLw.htm](npc-gallery-items/skjl8IwD4XG0LeLw.htm)|Sink or Swim|Nadar o hundirse|modificada|
|[SPtYCE6lLVtQjkar.htm](npc-gallery-items/SPtYCE6lLVtQjkar.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[SvCtowtaw8hmMZKz.htm](npc-gallery-items/SvCtowtaw8hmMZKz.htm)|Hatchet|Hacha|modificada|
|[SvKEuXIi5eUaqkLA.htm](npc-gallery-items/SvKEuXIi5eUaqkLA.htm)|Warhammer|Warhammer|modificada|
|[sw2mWVFaQb93J9el.htm](npc-gallery-items/sw2mWVFaQb93J9el.htm)|Forager|Forrajeador|modificada|
|[sWmESzQJUt1c3Xuu.htm](npc-gallery-items/sWmESzQJUt1c3Xuu.htm)|Sea Legs|Piernas de Mar|modificada|
|[T0cpCJqKGkWPXW4L.htm](npc-gallery-items/T0cpCJqKGkWPXW4L.htm)|Royal Defender|Royal Defender|modificada|
|[T3ZpHsIT64RGyhT0.htm](npc-gallery-items/T3ZpHsIT64RGyhT0.htm)|Power of the Mob|Poder de la mafia|modificada|
|[t5skSqnTS4aKuuan.htm](npc-gallery-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[t7aKwycZ4ehsFtFB.htm](npc-gallery-items/t7aKwycZ4ehsFtFB.htm)|Hatchet|Hacha|modificada|
|[T7oZcH7OKDm1O4cw.htm](npc-gallery-items/T7oZcH7OKDm1O4cw.htm)|Aura of Command|Aura de orden imperiosa|modificada|
|[t8D9jbYaoXXUxjCe.htm](npc-gallery-items/t8D9jbYaoXXUxjCe.htm)|Versatile Performance|Interpretación versátil|modificada|
|[taX3Q0r1h37NNYFO.htm](npc-gallery-items/taX3Q0r1h37NNYFO.htm)|Hazard Spotter|Hazard Spotter|modificada|
|[tCqd5uhyOKVs0yco.htm](npc-gallery-items/tCqd5uhyOKVs0yco.htm)|Protect the Master!|¡Protege al Maestro!|modificada|
|[tDia9PqZk9lPblrF.htm](npc-gallery-items/tDia9PqZk9lPblrF.htm)|Appraising Eye|Ojo evaluador|modificada|
|[TEKRQ6lf1xZxEPIz.htm](npc-gallery-items/TEKRQ6lf1xZxEPIz.htm)|Fist|Puño|modificada|
|[tMYrLjnqdp2h4CWk.htm](npc-gallery-items/tMYrLjnqdp2h4CWk.htm)|Swim Away|Nadar lejos|modificada|
|[ttkiwONkYUn5piHI.htm](npc-gallery-items/ttkiwONkYUn5piHI.htm)|War Flail|War Flail|modificada|
|[ttSdjINAYV0VxPZk.htm](npc-gallery-items/ttSdjINAYV0VxPZk.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[TvnwhUBwaljk26uo.htm](npc-gallery-items/TvnwhUBwaljk26uo.htm)|Dagger|Daga|modificada|
|[tWDvrIy7tMRedOAa.htm](npc-gallery-items/tWDvrIy7tMRedOAa.htm)|Master Tracker|Maestro Rastreador|modificada|
|[tYBrwLh84hOMoQb4.htm](npc-gallery-items/tYBrwLh84hOMoQb4.htm)|Bastard Sword|Espada Bastarda|modificada|
|[TztjddoEZphVsSDa.htm](npc-gallery-items/TztjddoEZphVsSDa.htm)|Focused Thinker|Pensador concentrado|modificada|
|[u3RFdo6uA3yPenzC.htm](npc-gallery-items/u3RFdo6uA3yPenzC.htm)|Light Hammer|Martillo ligero|modificada|
|[u6QEft9DxfittKI2.htm](npc-gallery-items/u6QEft9DxfittKI2.htm)|Bosun's Command|Orden imperiosa de contramaestre|modificada|
|[u7AfocCKAnj4LGQO.htm](npc-gallery-items/u7AfocCKAnj4LGQO.htm)|Staff|Báculo|modificada|
|[ue4Y2G3CCizn6XEG.htm](npc-gallery-items/ue4Y2G3CCizn6XEG.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[uE9nqIQImKOG8Vr0.htm](npc-gallery-items/uE9nqIQImKOG8Vr0.htm)|Hidden Blade|Hoja Escondida|modificada|
|[Uee48lu9JRn91tRp.htm](npc-gallery-items/Uee48lu9JRn91tRp.htm)|Pick|Pick|modificada|
|[Ufe5H0f9R4O2VBNV.htm](npc-gallery-items/Ufe5H0f9R4O2VBNV.htm)|Nature's Edge|Ventaja de la Naturaleza|modificada|
|[ufJVnIjBde8zNbAj.htm](npc-gallery-items/ufJVnIjBde8zNbAj.htm)|Dagger|Daga|modificada|
|[ug2TQxIeBOWQren2.htm](npc-gallery-items/ug2TQxIeBOWQren2.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[uglCIhXIhI66IGsk.htm](npc-gallery-items/uglCIhXIhI66IGsk.htm)|Crimson Vengeance|Crimson Vengeance|modificada|
|[UhDwKtMaA99YE2vC.htm](npc-gallery-items/UhDwKtMaA99YE2vC.htm)|Sudden Charge|Carga súbita|modificada|
|[uMvy7NgRGRADz6zB.htm](npc-gallery-items/uMvy7NgRGRADz6zB.htm)|Staff|Báculo|modificada|
|[uozTPO1slJcKBEax.htm](npc-gallery-items/uozTPO1slJcKBEax.htm)|Dagger|Daga|modificada|
|[UQoNtmlZuh2NnKd0.htm](npc-gallery-items/UQoNtmlZuh2NnKd0.htm)|Noble's Ruse|Artimaña del noble|modificada|
|[UticIspMVAd6QGkp.htm](npc-gallery-items/UticIspMVAd6QGkp.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[UTr5XgqL7eUu8pyH.htm](npc-gallery-items/UTr5XgqL7eUu8pyH.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[uUISfTBeVRNhnN9u.htm](npc-gallery-items/uUISfTBeVRNhnN9u.htm)|Fist|Puño|modificada|
|[UWagoDkGXzULZ0i4.htm](npc-gallery-items/UWagoDkGXzULZ0i4.htm)|Sap|Sap|modificada|
|[UwdvjmLBy5IaWIWn.htm](npc-gallery-items/UwdvjmLBy5IaWIWn.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[UZZJYmev69dOUgKu.htm](npc-gallery-items/UZZJYmev69dOUgKu.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[v3VQfpzwMQ3vAQCU.htm](npc-gallery-items/v3VQfpzwMQ3vAQCU.htm)|Scoundrel's Feint|Fintar de granuja|modificada|
|[V3WALYE32ZPtfiRt.htm](npc-gallery-items/V3WALYE32ZPtfiRt.htm)|Sap|Sap|modificada|
|[VCHM4U1i2BqAQJdz.htm](npc-gallery-items/VCHM4U1i2BqAQJdz.htm)|Staff|Báculo|modificada|
|[vfhKr3Ly9XjE7PhV.htm](npc-gallery-items/vfhKr3Ly9XjE7PhV.htm)|Occult Focus Spells|Conjuros de foco oculto|modificada|
|[VfIDgw7C87841Ehg.htm](npc-gallery-items/VfIDgw7C87841Ehg.htm)|Unshakable|Inquebrantable|modificada|
|[vgPpoUOgBxWLd4Ne.htm](npc-gallery-items/vgPpoUOgBxWLd4Ne.htm)|Steady Balance|Equilibrio firme|modificada|
|[VJxxeLCAgKz1s9HI.htm](npc-gallery-items/VJxxeLCAgKz1s9HI.htm)|Abyssal Temptation|Tentación Abisal|modificada|
|[VKwW4ylsv5KU7Nhr.htm](npc-gallery-items/VKwW4ylsv5KU7Nhr.htm)|Dread Stalker|Dread Stalker|modificada|
|[vmjC8rwYO5CA0CjY.htm](npc-gallery-items/vmjC8rwYO5CA0CjY.htm)|Bardic Lore|Saber bárdico|modificada|
|[vOk7ftvRMJXLkNWY.htm](npc-gallery-items/vOk7ftvRMJXLkNWY.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[vprpkjJKcOBq6m7X.htm](npc-gallery-items/vprpkjJKcOBq6m7X.htm)|Rock|Roca|modificada|
|[Vqjb5Zoa5sSTgnDg.htm](npc-gallery-items/Vqjb5Zoa5sSTgnDg.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[VQPPHF2M9xHf5XD3.htm](npc-gallery-items/VQPPHF2M9xHf5XD3.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[vrubZ4BfdgD4AmTp.htm](npc-gallery-items/vrubZ4BfdgD4AmTp.htm)|Shortsword|Espada corta|modificada|
|[vtgkJ23YrCFVTyTP.htm](npc-gallery-items/vtgkJ23YrCFVTyTP.htm)|Warding Strike|Golpe de Protección|modificada|
|[VtGXx6gZ4Ezl8Mcp.htm](npc-gallery-items/VtGXx6gZ4Ezl8Mcp.htm)|Pewter Mug|Taza de peltre|modificada|
|[vyJ3imlXVxUHGVq3.htm](npc-gallery-items/vyJ3imlXVxUHGVq3.htm)|Weapon Mastery (Hammer)|Maestría con las armas (Martillo)|modificada|
|[Vz4DYAJUl9kelFni.htm](npc-gallery-items/Vz4DYAJUl9kelFni.htm)|Staff|Báculo|modificada|
|[vZlRJzB7o08xfLuI.htm](npc-gallery-items/vZlRJzB7o08xfLuI.htm)|Light Hammer|Martillo ligero|modificada|
|[VzPHszVrqprSLuY7.htm](npc-gallery-items/VzPHszVrqprSLuY7.htm)|Primal Focus Spells|Conjuros de foco primigenio|modificada|
|[w021SRzRisqhAnKU.htm](npc-gallery-items/w021SRzRisqhAnKU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W3jbnf0RJFBtiRto.htm](npc-gallery-items/W3jbnf0RJFBtiRto.htm)|Halberd|Alabarda|modificada|
|[wDgSQGJhEyvpYeZE.htm](npc-gallery-items/wDgSQGJhEyvpYeZE.htm)|Persistent Lies|Mentiras persistentes|modificada|
|[we5UuMRtG5niRCBP.htm](npc-gallery-items/we5UuMRtG5niRCBP.htm)|Thunderstone|Thunderstone|modificada|
|[WFByucZQz0Q9slLk.htm](npc-gallery-items/WFByucZQz0Q9slLk.htm)|Sling|Sling|modificada|
|[wfNSTwrVHkHF5YLS.htm](npc-gallery-items/wfNSTwrVHkHF5YLS.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[WGBYwG2C4eElCrPd.htm](npc-gallery-items/WGBYwG2C4eElCrPd.htm)|Collaborative Thievery|Latrocinio colaborativo|modificada|
|[WHOLkEuYaCAMaFl1.htm](npc-gallery-items/WHOLkEuYaCAMaFl1.htm)|Noble's Ally|Aliado de noble|modificada|
|[wiKF84BoxIlLdggN.htm](npc-gallery-items/wiKF84BoxIlLdggN.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[wLxqjBiRMADtMrlV.htm](npc-gallery-items/wLxqjBiRMADtMrlV.htm)|Light Hammer|Martillo ligero|modificada|
|[wnR3RwCNys0DRqxB.htm](npc-gallery-items/wnR3RwCNys0DRqxB.htm)|Sap|Sap|modificada|
|[wOWNuciIMk1EpXis.htm](npc-gallery-items/wOWNuciIMk1EpXis.htm)|Holy Water|Agua bendita|modificada|
|[wPucfhAZe8Uqamyu.htm](npc-gallery-items/wPucfhAZe8Uqamyu.htm)|Crossbow|Ballesta|modificada|
|[WRQQun80G8om7SIp.htm](npc-gallery-items/WRQQun80G8om7SIp.htm)|Morningstar|Morningstar|modificada|
|[WrTvPVOo3UOYpMWC.htm](npc-gallery-items/WrTvPVOo3UOYpMWC.htm)|Boarding Action|Acción de abordaje|modificada|
|[wvyx4nEskJ9Imojt.htm](npc-gallery-items/wvyx4nEskJ9Imojt.htm)|Barkeep's Advice|Consejo del barman|modificada|
|[wwLWKH0F1mtPQ2lE.htm](npc-gallery-items/wwLWKH0F1mtPQ2lE.htm)|+1 to Sense Motive|+1 a Sentido Motivo|modificada|
|[wXgvnztT7r9jO7Px.htm](npc-gallery-items/wXgvnztT7r9jO7Px.htm)|Brutal Rally|Rally Brutal|modificada|
|[wxpvY7YmCPTSk3p9.htm](npc-gallery-items/wxpvY7YmCPTSk3p9.htm)|Raise a Shield|Alzar un escudo|modificada|
|[wYUI9xidJaJLYynz.htm](npc-gallery-items/wYUI9xidJaJLYynz.htm)|The Jig Is Up|The Jig Is Up|modificada|
|[WZzoPHc7f4dTn3iC.htm](npc-gallery-items/WZzoPHc7f4dTn3iC.htm)|Shortsword|Espada corta|modificada|
|[X7QR8UnnfPH6vOWK.htm](npc-gallery-items/X7QR8UnnfPH6vOWK.htm)|Subdue Prisoners|Someter presos|modificada|
|[X8NQcsCgEiQHiZj6.htm](npc-gallery-items/X8NQcsCgEiQHiZj6.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[x8s1JWo1Jcm4elFK.htm](npc-gallery-items/x8s1JWo1Jcm4elFK.htm)|+1 Status to All Saves vs. Poison|+1 situación a todas las salvaciones contra veneno.|modificada|
|[xBb6evQkhB3uYAvn.htm](npc-gallery-items/xBb6evQkhB3uYAvn.htm)|Rage|Furia|modificada|
|[Xbpr8x90JdxlOqrV.htm](npc-gallery-items/Xbpr8x90JdxlOqrV.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[XCCNdWgAwDmYJNUr.htm](npc-gallery-items/XCCNdWgAwDmYJNUr.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[XCtQzAvxzCKT6NIl.htm](npc-gallery-items/XCtQzAvxzCKT6NIl.htm)|Stench of Decay|Hedor de putrefacción|modificada|
|[XdNbqGPaLBs6RCOX.htm](npc-gallery-items/XdNbqGPaLBs6RCOX.htm)|Dagger|Daga|modificada|
|[xeEI4dqDE4PmxOeA.htm](npc-gallery-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|Caminante del Bosque|modificada|
|[XFo44PTU8F1rJam8.htm](npc-gallery-items/XFo44PTU8F1rJam8.htm)|Destructive Vengeance|Venganza Destructiva|modificada|
|[XHVHeWTU0zqmjJWZ.htm](npc-gallery-items/XHVHeWTU0zqmjJWZ.htm)|Whip|Látigo|modificada|
|[xhZKk4oQRgs08i0T.htm](npc-gallery-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[xI5EGbyi2XbV0z6e.htm](npc-gallery-items/xI5EGbyi2XbV0z6e.htm)|Fist|Puño|modificada|
|[xkwTvTqSPNbk86Un.htm](npc-gallery-items/xkwTvTqSPNbk86Un.htm)|Crossbow|Ballesta|modificada|
|[Xo7vz0G7GjTFg4yo.htm](npc-gallery-items/Xo7vz0G7GjTFg4yo.htm)|Light Mace|Maza de luz|modificada|
|[XPYr9hMESgVNjbVp.htm](npc-gallery-items/XPYr9hMESgVNjbVp.htm)|Trick Attack|Trick Attack|modificada|
|[XqNEuTQGsReodN8h.htm](npc-gallery-items/XqNEuTQGsReodN8h.htm)|Sap|Sap|modificada|
|[xS8f6u8VDcBepVGM.htm](npc-gallery-items/xS8f6u8VDcBepVGM.htm)|Demon Summoning|Invocación de demoniosíaco|modificada|
|[xuyjTEDoDAmb5acN.htm](npc-gallery-items/xuyjTEDoDAmb5acN.htm)|Sap|Sap|modificada|
|[XVoLXwXlo8BPWO8P.htm](npc-gallery-items/XVoLXwXlo8BPWO8P.htm)|Spiked Gauntlet|Spiked Gauntlet|modificada|
|[XxRBSksVZSF2n79w.htm](npc-gallery-items/XxRBSksVZSF2n79w.htm)|Dagger|Daga|modificada|
|[XXu57gmSKMHk38fa.htm](npc-gallery-items/XXu57gmSKMHk38fa.htm)|Rock|Roca|modificada|
|[Y4ZPmVCbnUAexfGS.htm](npc-gallery-items/Y4ZPmVCbnUAexfGS.htm)|+1 Circumstance to All Saves vs. Traps|+1 Circunstancia a todas las salvaciones contra trampas|modificada|
|[y5n9AfgPzGvGtS69.htm](npc-gallery-items/y5n9AfgPzGvGtS69.htm)|Rock|Roca|modificada|
|[y77GcQxQjHDAiISd.htm](npc-gallery-items/y77GcQxQjHDAiISd.htm)|Sling|Sling|modificada|
|[y9nL7kklEoVqLV06.htm](npc-gallery-items/y9nL7kklEoVqLV06.htm)|Favored Terrain|Terreno predilecto|modificada|
|[yAk7aiu08rTizR5c.htm](npc-gallery-items/yAk7aiu08rTizR5c.htm)|Rapier|Estoque|modificada|
|[YB9T72k74oWO9L6c.htm](npc-gallery-items/YB9T72k74oWO9L6c.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[YCf4WMESlKy1zF6l.htm](npc-gallery-items/YCf4WMESlKy1zF6l.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Ye6jF7F3YdRigibJ.htm](npc-gallery-items/Ye6jF7F3YdRigibJ.htm)|Methodical Research|Investigación Metódica|modificada|
|[yEcvRwueCdGqs9GC.htm](npc-gallery-items/yEcvRwueCdGqs9GC.htm)|Dagger|Daga|modificada|
|[YEsqZ1pmCAbdEHfI.htm](npc-gallery-items/YEsqZ1pmCAbdEHfI.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[YgMj9CaUadOgAwPl.htm](npc-gallery-items/YgMj9CaUadOgAwPl.htm)|Scalpel|Bisturí|modificada|
|[yJqGrkv3kpN6caAd.htm](npc-gallery-items/yJqGrkv3kpN6caAd.htm)|Weapon Mastery|Maestría con las armas|modificada|
|[yM3snMjrKoj8gXaq.htm](npc-gallery-items/yM3snMjrKoj8gXaq.htm)|Club|Club|modificada|
|[YMK6yeIqUwKCrXpF.htm](npc-gallery-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[YT49F2uCWIqKs6Ct.htm](npc-gallery-items/YT49F2uCWIqKs6Ct.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[YTWYrskTTtVm4yze.htm](npc-gallery-items/YTWYrskTTtVm4yze.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[Yxoxo2Zh5olZHceb.htm](npc-gallery-items/Yxoxo2Zh5olZHceb.htm)|Shield Block|Bloquear con escudo|modificada|
|[Yzt7iAgbGNFAn1NW.htm](npc-gallery-items/Yzt7iAgbGNFAn1NW.htm)|+1 Circumstance Bonus on Saves vs. Poisons|+1 Bonificador de Circunstancia en Salvaciones contra Venenos|modificada|
|[ZcVfcVHFVcJZhIQI.htm](npc-gallery-items/ZcVfcVHFVcJZhIQI.htm)|Greataxe|Greataxe|modificada|
|[zecbW6qIhXuKa8Yu.htm](npc-gallery-items/zecbW6qIhXuKa8Yu.htm)|Fist|Puño|modificada|
|[zeH3OrBLioIRoYb5.htm](npc-gallery-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|Denegar ventaja|modificada|
|[zg7ntU3VEOypwaws.htm](npc-gallery-items/zg7ntU3VEOypwaws.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[zICeN9ExAbhXEjzA.htm](npc-gallery-items/zICeN9ExAbhXEjzA.htm)|Infused Items|Equipos infundidos|modificada|
|[ziPz3z2a6Db63N9r.htm](npc-gallery-items/ziPz3z2a6Db63N9r.htm)|Reckless Alchemy|Reckless Alchimia|modificada|
|[zKVUU94IzSXLEDLT.htm](npc-gallery-items/zKVUU94IzSXLEDLT.htm)|Crossbow|Ballesta|modificada|
|[zmdPdUEf9SjhHnTL.htm](npc-gallery-items/zmdPdUEf9SjhHnTL.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[ZmfzXnEaPSxGh2J4.htm](npc-gallery-items/ZmfzXnEaPSxGh2J4.htm)|Quick Catch|Quick Catch|modificada|
|[zMkrKhyfRWFrFfuv.htm](npc-gallery-items/zMkrKhyfRWFrFfuv.htm)|Fanatical Frenzy|Frenesí fanático|modificada|
|[zS04tb23VRw9X9kX.htm](npc-gallery-items/zS04tb23VRw9X9kX.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[ZtFKTs16yHUY48WF.htm](npc-gallery-items/ZtFKTs16yHUY48WF.htm)|Healing Hands|Manos curativas|modificada|
|[ZuDGBQWUf1cSV69U.htm](npc-gallery-items/ZuDGBQWUf1cSV69U.htm)|Call to Action|Llamada a la Acción|modificada|
|[zYZvGGQfgJZyoNMQ.htm](npc-gallery-items/zYZvGGQfgJZyoNMQ.htm)|Dagger|Daga|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[2L7yj9lThk67SmMC.htm](npc-gallery-items/2L7yj9lThk67SmMC.htm)|Performance|vacía|
|[2m3sOYaFmlCapCZm.htm](npc-gallery-items/2m3sOYaFmlCapCZm.htm)|Graveyard Lore|vacía|
|[2WL0bJlVq6JxInsp.htm](npc-gallery-items/2WL0bJlVq6JxInsp.htm)|Warfare Lore|vacía|
|[3opCHH7my1xVzJSY.htm](npc-gallery-items/3opCHH7my1xVzJSY.htm)|Underwater Lore|vacía|
|[3YP4WRM7J9c4OkFT.htm](npc-gallery-items/3YP4WRM7J9c4OkFT.htm)|Sailing Lore|vacía|
|[4bQvbouLvfbJf5ml.htm](npc-gallery-items/4bQvbouLvfbJf5ml.htm)|Farming Lore|vacía|
|[4NNvg2SM3AoqAWLt.htm](npc-gallery-items/4NNvg2SM3AoqAWLt.htm)|Cult Lore (applies to cultist's own cult)|vacía|
|[4ZB9khLqIwNxe4j2.htm](npc-gallery-items/4ZB9khLqIwNxe4j2.htm)|Sailing Lore|vacía|
|[51OPbUesBItU8rTj.htm](npc-gallery-items/51OPbUesBItU8rTj.htm)|Forest Lore|vacía|
|[5dZchzh220BWuYbG.htm](npc-gallery-items/5dZchzh220BWuYbG.htm)|Theatre Lore|vacía|
|[5MyYDCSSRvOAcJCe.htm](npc-gallery-items/5MyYDCSSRvOAcJCe.htm)|Theatre Lore|vacía|
|[5oQ0ag3YkEb6hYwX.htm](npc-gallery-items/5oQ0ag3YkEb6hYwX.htm)|Academia Lore|vacía|
|[6r31Osvx8aSYtCts.htm](npc-gallery-items/6r31Osvx8aSYtCts.htm)|Athletics|vacía|
|[7d9Cb3ugSc4NMuC1.htm](npc-gallery-items/7d9Cb3ugSc4NMuC1.htm)|Geography Lore|vacía|
|[7JSOtd0bGE4q9bHf.htm](npc-gallery-items/7JSOtd0bGE4q9bHf.htm)|Legal Lore|vacía|
|[7RDjgYcmwKNdhNZc.htm](npc-gallery-items/7RDjgYcmwKNdhNZc.htm)|Performance|vacía|
|[7uL4FBIFV9kMEQKe.htm](npc-gallery-items/7uL4FBIFV9kMEQKe.htm)|Underworld Lore|vacía|
|[8DylMvT2Yh5SC6gF.htm](npc-gallery-items/8DylMvT2Yh5SC6gF.htm)|Sailing Lore|vacía|
|[9MoDarqE5LK66jA5.htm](npc-gallery-items/9MoDarqE5LK66jA5.htm)|Magaambya Lore|vacía|
|[9vYQdc1M4NTXvorr.htm](npc-gallery-items/9vYQdc1M4NTXvorr.htm)|Map Lore|vacía|
|[Aw1KbwX8Q6IJWqjz.htm](npc-gallery-items/Aw1KbwX8Q6IJWqjz.htm)|Underworld Lore|vacía|
|[b6M0e68kzny77o5h.htm](npc-gallery-items/b6M0e68kzny77o5h.htm)|Underworld Lore|vacía|
|[BE4Ax06mjpPipwKW.htm](npc-gallery-items/BE4Ax06mjpPipwKW.htm)|Sailing Lore|vacía|
|[Bk9v9oLDZmh7ITsB.htm](npc-gallery-items/Bk9v9oLDZmh7ITsB.htm)|Construction Schematics|vacía|
|[BRt0eTPvm8Gdvdd5.htm](npc-gallery-items/BRt0eTPvm8Gdvdd5.htm)|Bureaucracy Lore|vacía|
|[C0mzP5eUVDpwg5rI.htm](npc-gallery-items/C0mzP5eUVDpwg5rI.htm)|Legal Lore|vacía|
|[Cdbi86SZ0ydTRO91.htm](npc-gallery-items/Cdbi86SZ0ydTRO91.htm)|Underwater Lore|vacía|
|[CLyzEL754Aofobtz.htm](npc-gallery-items/CLyzEL754Aofobtz.htm)|Sailing Lore|vacía|
|[Cu79oDJClJ9C9Reu.htm](npc-gallery-items/Cu79oDJClJ9C9Reu.htm)|Demon Lore|vacía|
|[DsXup6n3udnCKVoX.htm](npc-gallery-items/DsXup6n3udnCKVoX.htm)|Serving Tray|vacía|
|[duBrf89grnmT2Qdn.htm](npc-gallery-items/duBrf89grnmT2Qdn.htm)|Engineering Lore|vacía|
|[e3i9M78EsoeT9Mu1.htm](npc-gallery-items/e3i9M78EsoeT9Mu1.htm)|Plague Lore|vacía|
|[E4oBlqhBaVqkShlq.htm](npc-gallery-items/E4oBlqhBaVqkShlq.htm)|Stealth|vacía|
|[egP992KxbiFYLiUr.htm](npc-gallery-items/egP992KxbiFYLiUr.htm)|Fortune-Telling Lore|vacía|
|[EnayAcuVFebid6Yd.htm](npc-gallery-items/EnayAcuVFebid6Yd.htm)|Labor Lore|vacía|
|[FFYi0YoImNuoninN.htm](npc-gallery-items/FFYi0YoImNuoninN.htm)|Underworld Lore|vacía|
|[fTKnuO5HCLkBEtPH.htm](npc-gallery-items/fTKnuO5HCLkBEtPH.htm)|Performance|vacía|
|[fwJUsC1cyxsHPo94.htm](npc-gallery-items/fwJUsC1cyxsHPo94.htm)|Architecture Lore|vacía|
|[G7t10rKHu8TabnQz.htm](npc-gallery-items/G7t10rKHu8TabnQz.htm)|Theatre Lore|vacía|
|[Gel19EmMnTqnNt87.htm](npc-gallery-items/Gel19EmMnTqnNt87.htm)|Legal Lore|vacía|
|[gUoaHaSoU57Z7C4z.htm](npc-gallery-items/gUoaHaSoU57Z7C4z.htm)|Underworld Lore|vacía|
|[gwyxM0numx6kL7On.htm](npc-gallery-items/gwyxM0numx6kL7On.htm)|Mercantile Lore|vacía|
|[h0OhlZfKQJOrUwr1.htm](npc-gallery-items/h0OhlZfKQJOrUwr1.htm)|Fishing Lore|vacía|
|[hF5pPWi7gLBNBaO0.htm](npc-gallery-items/hF5pPWi7gLBNBaO0.htm)|Warfare Lore|vacía|
|[hgkI7oRfyfC00JbV.htm](npc-gallery-items/hgkI7oRfyfC00JbV.htm)|Keyring|vacía|
|[iDsCIJ7Gzk9ENl2h.htm](npc-gallery-items/iDsCIJ7Gzk9ENl2h.htm)|Legal Lore|vacía|
|[ifnnMl3PxViiITbo.htm](npc-gallery-items/ifnnMl3PxViiITbo.htm)|Local Court Lore|vacía|
|[iGam6RcKFAl2V9TI.htm](npc-gallery-items/iGam6RcKFAl2V9TI.htm)|Silver Flask|vacía|
|[Jd088q8hRZquUT6t.htm](npc-gallery-items/Jd088q8hRZquUT6t.htm)|Scribing Lore|vacía|
|[jMBs4PyC1EahZHlA.htm](npc-gallery-items/jMBs4PyC1EahZHlA.htm)|Sack with 5 Rocks|vacía|
|[krDLK9NYOYjDsMUZ.htm](npc-gallery-items/krDLK9NYOYjDsMUZ.htm)|Underworld Lore|vacía|
|[KuVkUyVf2udJz4xG.htm](npc-gallery-items/KuVkUyVf2udJz4xG.htm)|Bardic Lore|vacía|
|[lh7skf3SdSowZIzD.htm](npc-gallery-items/lh7skf3SdSowZIzD.htm)|Empty Bottle|vacía|
|[lO3CMWjs9XkP1KVX.htm](npc-gallery-items/lO3CMWjs9XkP1KVX.htm)|Crafting|vacía|
|[lqVfEZu6MeCHDuky.htm](npc-gallery-items/lqVfEZu6MeCHDuky.htm)|Lore (any one related to their trade)|vacía|
|[lY3x1rCfroxrjphs.htm](npc-gallery-items/lY3x1rCfroxrjphs.htm)|Thievery|vacía|
|[mHiTwtQ3th4qU0Um.htm](npc-gallery-items/mHiTwtQ3th4qU0Um.htm)|Hell Lore|vacía|
|[n1RVqltrSIC94q8w.htm](npc-gallery-items/n1RVqltrSIC94q8w.htm)|Engineering Lore|vacía|
|[N6AEPmgihlaE1PZT.htm](npc-gallery-items/N6AEPmgihlaE1PZT.htm)|Astronomy Lore|vacía|
|[n6mv4Y0jGccZlwmK.htm](npc-gallery-items/n6mv4Y0jGccZlwmK.htm)|Signet Ring|vacía|
|[NfP2dKgjJXtvbtmr.htm](npc-gallery-items/NfP2dKgjJXtvbtmr.htm)|Mining Lore|vacía|
|[O4SjTzND1x4DfkNb.htm](npc-gallery-items/O4SjTzND1x4DfkNb.htm)|Athletics|vacía|
|[o82vOevA36JTvcnU.htm](npc-gallery-items/o82vOevA36JTvcnU.htm)|Forest Lore|vacía|
|[OmOeBNnG1vyNMitv.htm](npc-gallery-items/OmOeBNnG1vyNMitv.htm)|Legal Lore|vacía|
|[OmZUGABSf4Zc4qb6.htm](npc-gallery-items/OmZUGABSf4Zc4qb6.htm)|Sailing Lore|vacía|
|[OsAKupyZJwzBcBcb.htm](npc-gallery-items/OsAKupyZJwzBcBcb.htm)|Legal Lore|vacía|
|[oSzvqCM5pgm1hIvZ.htm](npc-gallery-items/oSzvqCM5pgm1hIvZ.htm)|Smithy Lore|vacía|
|[pHbOsoRctK7yp0Uj.htm](npc-gallery-items/pHbOsoRctK7yp0Uj.htm)|Stealth|vacía|
|[pmz2Md1ezhwMIY3D.htm](npc-gallery-items/pmz2Md1ezhwMIY3D.htm)|Underworld Lore|vacía|
|[ptceztheMpvUcPpL.htm](npc-gallery-items/ptceztheMpvUcPpL.htm)|Scribing Lore|vacía|
|[Q0NhoV8dgyILx0Gd.htm](npc-gallery-items/Q0NhoV8dgyILx0Gd.htm)|Underworld Lore|vacía|
|[qaGKeUrnOaPLyPKv.htm](npc-gallery-items/qaGKeUrnOaPLyPKv.htm)|Bardic Lore|vacía|
|[rBpjP1zIiLXuu1XU.htm](npc-gallery-items/rBpjP1zIiLXuu1XU.htm)|Gambling Lore|vacía|
|[RcwOpnDVjeLBkRqr.htm](npc-gallery-items/RcwOpnDVjeLBkRqr.htm)|Academia Lore|vacía|
|[RlKi9kBotypUiYQS.htm](npc-gallery-items/RlKi9kBotypUiYQS.htm)|Mercantile Lore|vacía|
|[Rpv1wLy0PVmx5FE6.htm](npc-gallery-items/Rpv1wLy0PVmx5FE6.htm)|Alcohol Lore|vacía|
|[rskoGYiEEGDi0ngN.htm](npc-gallery-items/rskoGYiEEGDi0ngN.htm)|Academia Lore|vacía|
|[RtyfDIKkjX6sanHn.htm](npc-gallery-items/RtyfDIKkjX6sanHn.htm)|Architecture Lore|vacía|
|[sAXGGq06HDnJjuhX.htm](npc-gallery-items/sAXGGq06HDnJjuhX.htm)|Circus Lore|vacía|
|[SeHfXdvpM8XZT2ue.htm](npc-gallery-items/SeHfXdvpM8XZT2ue.htm)|Library Lore|vacía|
|[SJ7vgCLVP54MCqjB.htm](npc-gallery-items/SJ7vgCLVP54MCqjB.htm)|Sleep Poison|vacía|
|[sJmK79CMVyreBL50.htm](npc-gallery-items/sJmK79CMVyreBL50.htm)|Forest Lore|vacía|
|[SLVD3NahEqkF3ybu.htm](npc-gallery-items/SLVD3NahEqkF3ybu.htm)|Magaambya Mask|vacía|
|[SMJAMYebYv4bS1yT.htm](npc-gallery-items/SMJAMYebYv4bS1yT.htm)|Performance|vacía|
|[ssNhLs1uWAzMgArR.htm](npc-gallery-items/ssNhLs1uWAzMgArR.htm)|Alcohol Lore|vacía|
|[TAnxvIqFKgGFyBhv.htm](npc-gallery-items/TAnxvIqFKgGFyBhv.htm)|Cooking Lore|vacía|
|[twFQitk8O2yJII8R.htm](npc-gallery-items/twFQitk8O2yJII8R.htm)|Household Lore|vacía|
|[U1pVgJkNIeXvdXNP.htm](npc-gallery-items/U1pVgJkNIeXvdXNP.htm)|Underworld Lore|vacía|
|[uaIlnacoQ95VuVMM.htm](npc-gallery-items/uaIlnacoQ95VuVMM.htm)|Sailing Lore|vacía|
|[uLm4nxop4jQv6iwA.htm](npc-gallery-items/uLm4nxop4jQv6iwA.htm)|Circus Lore|vacía|
|[UpuwyeFRhKKkWeGm.htm](npc-gallery-items/UpuwyeFRhKKkWeGm.htm)|Accounting Lore|vacía|
|[Uq9fVOOj1180ESoY.htm](npc-gallery-items/Uq9fVOOj1180ESoY.htm)|Diplomacy|vacía|
|[uYklkDltD5DuFVB7.htm](npc-gallery-items/uYklkDltD5DuFVB7.htm)|Astrolabe|vacía|
|[v2DAXboOXPMsm6x0.htm](npc-gallery-items/v2DAXboOXPMsm6x0.htm)|Crafting|vacía|
|[vnh6CQV0lhDgm0I8.htm](npc-gallery-items/vnh6CQV0lhDgm0I8.htm)|Shining Crusade Lore|vacía|
|[wlmOlag2TUOgMUOB.htm](npc-gallery-items/wlmOlag2TUOgMUOB.htm)|Sailing Lore|vacía|
|[XhzT06iOtcqrirxO.htm](npc-gallery-items/XhzT06iOtcqrirxO.htm)|Pathfinder Society Lore|vacía|
|[xLUuOeUKIbtUPL9R.htm](npc-gallery-items/xLUuOeUKIbtUPL9R.htm)|Athletics|vacía|
|[XnFDooIYZ58vGk4b.htm](npc-gallery-items/XnFDooIYZ58vGk4b.htm)|Crab Cage|vacía|
|[y6XpXIXPKaxphOrA.htm](npc-gallery-items/y6XpXIXPKaxphOrA.htm)|Architecture Lore|vacía|
|[ydSwmhudPEes84G7.htm](npc-gallery-items/ydSwmhudPEes84G7.htm)|Legal Lore|vacía|
|[YFx6uVJW2kREU7me.htm](npc-gallery-items/YFx6uVJW2kREU7me.htm)|One Additional Lore|vacía|
|[yqiy77WfCwOSI2ti.htm](npc-gallery-items/yqiy77WfCwOSI2ti.htm)|Alcohol Lore|vacía|
|[yqwZZgxhcWcjshla.htm](npc-gallery-items/yqwZZgxhcWcjshla.htm)|Accounting Lore|vacía|
|[yYnHoV3tJvO6D3bB.htm](npc-gallery-items/yYnHoV3tJvO6D3bB.htm)|Legal Lore|vacía|
|[YZqHMPt8ZBbo3FbI.htm](npc-gallery-items/YZqHMPt8ZBbo3FbI.htm)|Underworld Lore|vacía|
|[ZBaDNdF5HlekM2y5.htm](npc-gallery-items/ZBaDNdF5HlekM2y5.htm)|Monster Lore|vacía|
|[zewypHqdhJynMyRj.htm](npc-gallery-items/zewypHqdhJynMyRj.htm)|Engineering Lore|vacía|
|[zEyJJyZtRmrTwf2n.htm](npc-gallery-items/zEyJJyZtRmrTwf2n.htm)|Collection of Fake Relics|vacía|
|[zn2RGfRS6HoLPjTb.htm](npc-gallery-items/zn2RGfRS6HoLPjTb.htm)|Scouting Lore|vacía|
|[ZtSXv2TmdsOEyaIw.htm](npc-gallery-items/ZtSXv2TmdsOEyaIw.htm)|Cult Lore (Applies to the Leader's own cult)|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
