# Estado de la traducción (classes)

 * **oficial**: 16
 * **modificada**: 6


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[30qVs46dVNflgQNx.htm](classes/30qVs46dVNflgQNx.htm)|Inventor|Inventor|modificada|
|[Inq4gH3P5PYjSQbD.htm](classes/Inq4gH3P5PYjSQbD.htm)|Psychic|Psíquico|modificada|
|[RwjIZzIxzPpUglnK.htm](classes/RwjIZzIxzPpUglnK.htm)|Wizard|Mago|modificada|
|[Y5GsHqzCzJlKka6x.htm](classes/Y5GsHqzCzJlKka6x.htm)|Thaumaturge|Taumaturgo|modificada|
|[YtOm245r8GFSFYeD.htm](classes/YtOm245r8GFSFYeD.htm)|Summoner|Invocador|modificada|
|[Z9li154CPNmun29Q.htm](classes/Z9li154CPNmun29Q.htm)|Gunslinger|Pistolero|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[15Yc1r6s9CEhSTMe.htm](classes/15Yc1r6s9CEhSTMe.htm)|Sorcerer|Hechicero|oficial|
|[3gweRQ5gn7szIWAv.htm](classes/3gweRQ5gn7szIWAv.htm)|Bard|Bardo|oficial|
|[4wrSCyX6akmyo7Wj.htm](classes/4wrSCyX6akmyo7Wj.htm)|Investigator|Investigador|oficial|
|[7s57JDCaiYYCAdFx.htm](classes/7s57JDCaiYYCAdFx.htm)|Druid|Druida|oficial|
|[8zn3cD6GSmoo1LW4.htm](classes/8zn3cD6GSmoo1LW4.htm)|Fighter|Guerrero|oficial|
|[bYDXk9HUMKOuym9h.htm](classes/bYDXk9HUMKOuym9h.htm)|Witch|Brujo|oficial|
|[EizrWvUPMS67Pahd.htm](classes/EizrWvUPMS67Pahd.htm)|Cleric|Clérigo|oficial|
|[HQBA9Yx2s8ycvz3C.htm](classes/HQBA9Yx2s8ycvz3C.htm)|Magus|Magus|oficial|
|[LO9STvskJemPkiAI.htm](classes/LO9STvskJemPkiAI.htm)|Rogue|Pícaro|oficial|
|[pWHx4SXcft9O2udP.htm](classes/pWHx4SXcft9O2udP.htm)|Oracle|Oráculo|oficial|
|[uJ5aCzlw34GGdWjp.htm](classes/uJ5aCzlw34GGdWjp.htm)|Swashbuckler|Espadachín|oficial|
|[x8iwnpdLbfcoZkHA.htm](classes/x8iwnpdLbfcoZkHA.htm)|Champion|Campeón|oficial|
|[XwfcJuskrhI9GIjX.htm](classes/XwfcJuskrhI9GIjX.htm)|Alchemist|Alquimista|oficial|
|[YDRiP7uVvr9WRhOI.htm](classes/YDRiP7uVvr9WRhOI.htm)|Barbarian|Bárbaro|oficial|
|[Yix76sfxrIlltSTJ.htm](classes/Yix76sfxrIlltSTJ.htm)|Ranger|Explorador|oficial|
|[YPxpk9JbMnKjbNLc.htm](classes/YPxpk9JbMnKjbNLc.htm)|Monk|Monje|oficial|
