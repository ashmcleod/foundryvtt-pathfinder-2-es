# Estado de la traducción (actions)

 * **oficial**: 20
 * **modificada**: 317
 * **ninguna**: 14


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[5tOovJGK2TQIBHUZ.htm](actions/5tOovJGK2TQIBHUZ.htm)|Grow|
|[BRR0HS8yRhgBfipz.htm](actions/BRR0HS8yRhgBfipz.htm)|A Quick Glimpse Beyond|
|[CCMemaB1RoVa0aOu.htm](actions/CCMemaB1RoVa0aOu.htm)|Bestial Clarity|
|[ePfdZYZRmycMpSWU.htm](actions/ePfdZYZRmycMpSWU.htm)|Avoid Dire Fate|
|[EuoCFSHTzi0VOfuf.htm](actions/EuoCFSHTzi0VOfuf.htm)|Harrow the Fiend|
|[GmqHs4NEhAjqyAze.htm](actions/GmqHs4NEhAjqyAze.htm)|Spiritual Aid|
|[gU93T2UqxLsX9gyF.htm](actions/gU93T2UqxLsX9gyF.htm)|Fated Not to Die|
|[lH3mlvs1rCyXw6iY.htm](actions/lH3mlvs1rCyXw6iY.htm)|Recall Under Pressure|
|[RxIPBjodIRm7Pd6W.htm](actions/RxIPBjodIRm7Pd6W.htm)|Lucky Break|
|[Xc5MLIbT5OPhnFIJ.htm](actions/Xc5MLIbT5OPhnFIJ.htm)|Indomitable Act|
|[YjrM5G8Up0wv6x0u.htm](actions/YjrM5G8Up0wv6x0u.htm)|Chaotic Destiny|
|[yM20ZnvmNxu9qMDV.htm](actions/yM20ZnvmNxu9qMDV.htm)|Compose Missive|
|[ZUAaBIAntZhFQixm.htm](actions/ZUAaBIAntZhFQixm.htm)|Deconstruct|
|[ZXvJGxMJc0HRpcQh.htm](actions/ZXvJGxMJc0HRpcQh.htm)|Inspired Stratagem|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[04VQuQih77pxX06q.htm](actions/04VQuQih77pxX06q.htm)|Fling Magic|Lanzar por los aires magia|modificada|
|[06frwOOuo4HJtivl.htm](actions/06frwOOuo4HJtivl.htm)|Furious Strike|Golpe Furioso|modificada|
|[0XiPnnxKHWKFg1tN.htm](actions/0XiPnnxKHWKFg1tN.htm)|Breath Weapon (Piercing) (Cone)|Arma de Aliento (Perforante) (Cono)|modificada|
|[1If9lLVoZdO8woVg.htm](actions/1If9lLVoZdO8woVg.htm)|Consume Flesh|Consumir carne|modificada|
|[1kGNdIIhuglAjIp9.htm](actions/1kGNdIIhuglAjIp9.htm)|Treat Wounds|Tratar heridas|modificada|
|[1LDOfV8jEka09eXr.htm](actions/1LDOfV8jEka09eXr.htm)|Deceptive Sidestep|Enga lateral|modificada|
|[1xRFPTFtWtGJ9ELw.htm](actions/1xRFPTFtWtGJ9ELw.htm)|Sense Motive|Sense Motive|modificada|
|[21WIfSu7Xd7uKqV8.htm](actions/21WIfSu7Xd7uKqV8.htm)|Tumble Through|Pasar haciendo acrobacias|modificada|
|[2424g94rpoiN1IPh.htm](actions/2424g94rpoiN1IPh.htm)|Catharsis|Catarsis|modificada|
|[24PSsn1SKpUwYA1X.htm](actions/24PSsn1SKpUwYA1X.htm)|Retraining|Reentrenamiento|modificada|
|[25WDi1cVUrW92sUj.htm](actions/25WDi1cVUrW92sUj.htm)|Clue In|Clue In|modificada|
|[2EE4aF4SZpYf0R6H.htm](actions/2EE4aF4SZpYf0R6H.htm)|Pick a Lock|Forzar una cerradura|modificada|
|[2HJ4yuEFY1Cast4h.htm](actions/2HJ4yuEFY1Cast4h.htm)|High Jump|Salto de altura|modificada|
|[2KV87ponbWXgGIhZ.htm](actions/2KV87ponbWXgGIhZ.htm)|Act Together|Actuar juntos|modificada|
|[2u915NdUyQan6uKF.htm](actions/2u915NdUyQan6uKF.htm)|Demoralize|Desmoralizar|modificada|
|[34E7k2YRcsOU5uyl.htm](actions/34E7k2YRcsOU5uyl.htm)|Change Shape (Anadi)|Cambiar Forma (Anadi)|modificada|
|[3cuTA58ObXhuFX2r.htm](actions/3cuTA58ObXhuFX2r.htm)|Bend Time|Doblar el tiempo|modificada|
|[3D9kGfwg4LUZBR9A.htm](actions/3D9kGfwg4LUZBR9A.htm)|Overdrive|Sobremarcha|modificada|
|[3f5DMFu8fPiqHpRg.htm](actions/3f5DMFu8fPiqHpRg.htm)|Sustain a Spell|Mantener un conjuro|modificada|
|[3UEwtMA5y8foVqYP.htm](actions/3UEwtMA5y8foVqYP.htm)|Breath Weapon (Electricity) (Line)|Arma de Aliento (Electricidad) (Línea)|modificada|
|[3yoajuKjwHZ9ApUY.htm](actions/3yoajuKjwHZ9ApUY.htm)|Grab an Edge|Agarrarse a un saliente|modificada|
|[3ZzoI9MTtJFd1Kjl.htm](actions/3ZzoI9MTtJFd1Kjl.htm)|Share Senses|Compartir Sentidos|modificada|
|[49F65W6VwuXhmv8K.htm](actions/49F65W6VwuXhmv8K.htm)|Drink Blood|Beber Sangre|modificada|
|[4IxCKbGaEM9nUhld.htm](actions/4IxCKbGaEM9nUhld.htm)|Finish The Job|Finish The Job|modificada|
|[5bCZGpp9yFHXDz1j.htm](actions/5bCZGpp9yFHXDz1j.htm)|Entity's Resurgence|Resurgimiento de la Entidad|modificada|
|[5LW1k5DUkzbbYuYL.htm](actions/5LW1k5DUkzbbYuYL.htm)|Daydream Trance|Daydream Trance|modificada|
|[5p2AMqM9bOVnhwPT.htm](actions/5p2AMqM9bOVnhwPT.htm)|Recenter|Recenter|modificada|
|[5V49l0K460CcvBOO.htm](actions/5V49l0K460CcvBOO.htm)|Defend Life|Defender la Vida|modificada|
|[5Vuhf4DjvMvRqiEZ.htm](actions/5Vuhf4DjvMvRqiEZ.htm)|Breath Weapon (Negative) (Cone)|Arma de Aliento (Negativo) (Cono)|modificada|
|[6Oe9dg3Lu10slyeC.htm](actions/6Oe9dg3Lu10slyeC.htm)|Influence|Influencia|modificada|
|[6OZfIjiPjWTIe7Pr.htm](actions/6OZfIjiPjWTIe7Pr.htm)|Spinning Crush|Spinning Crush|modificada|
|[6SAdjml3OZw0BZnn.htm](actions/6SAdjml3OZw0BZnn.htm)|Thundering Roar|Rugido Tronante|modificada|
|[74iat04PtfG8gn2Q.htm](actions/74iat04PtfG8gn2Q.htm)|Mighty Rage|Furia poderosa|modificada|
|[7blmbDrQFNfdT731.htm](actions/7blmbDrQFNfdT731.htm)|Shove|Empujar|modificada|
|[7dq6bpYvLArJ5odx.htm](actions/7dq6bpYvLArJ5odx.htm)|Forge Documents|Forja Documentos|modificada|
|[7GeguyqyD1TjoC4r.htm](actions/7GeguyqyD1TjoC4r.htm)|Unleash Psyche|Unleash Psyche|modificada|
|[7qjfYsLNTr17Aftf.htm](actions/7qjfYsLNTr17Aftf.htm)|Energy Emanation (Acid)|Emanación de Energía (Ácido)|modificada|
|[8aIe57gXJ94mPpW4.htm](actions/8aIe57gXJ94mPpW4.htm)|Anadi Venom|Anadi Veneno|modificada|
|[8b63qGzqSUENQgOT.htm](actions/8b63qGzqSUENQgOT.htm)|Armor Up!|¡Aumentar armadura!|modificada|
|[8iCas8MeyqSXmE2a.htm](actions/8iCas8MeyqSXmE2a.htm)|Dream Research|Investigación del Sue|modificada|
|[8kGpUUUSX1sB2OqQ.htm](actions/8kGpUUUSX1sB2OqQ.htm)|Call Companion|Llamada Compañero|modificada|
|[8w6esW689NNbbq3i.htm](actions/8w6esW689NNbbq3i.htm)|Call on Ancient Blood|Llamada a Sangre Antigua|modificada|
|[90fHUm0HGZFAbG1K.htm](actions/90fHUm0HGZFAbG1K.htm)|Breath Weapon (Fire) (Line)|Arma de Aliento (Fuego) (Línea)|modificada|
|[9gDMkIfDifh61yLz.htm](actions/9gDMkIfDifh61yLz.htm)|Stop|Stop|modificada|
|[9kIvFxA5V9rvNWiH.htm](actions/9kIvFxA5V9rvNWiH.htm)|Thoughtful Reload|Recarga pensativa|modificada|
|[9MJcQEMVVrmMyGyq.htm](actions/9MJcQEMVVrmMyGyq.htm)|Living Fortification|Fortificación Viviente|modificada|
|[9Nmz5114UB87n7Aj.htm](actions/9Nmz5114UB87n7Aj.htm)|Affix a Fulu|Affix a Fulu|modificada|
|[9sXQf1SyppS5B6O1.htm](actions/9sXQf1SyppS5B6O1.htm)|Automaton Aim|Automaton Aim|modificada|
|[9X80J5RN21Uoaeiw.htm](actions/9X80J5RN21Uoaeiw.htm)|Binding Vow|Voto vinculante|modificada|
|[a47Npi5XHXo47y49.htm](actions/a47Npi5XHXo47y49.htm)|Blizzard Evasion|Evasión de Ventisca|modificada|
|[A4L90h7FIgO5EyBx.htm](actions/A4L90h7FIgO5EyBx.htm)|Siegebreaker|Siegebreaker|modificada|
|[aBQ8ajvEBByv45yz.htm](actions/aBQ8ajvEBByv45yz.htm)|Cast a Spell|Cast a Spell|modificada|
|[Ah5g9pDwWF9b9VW9.htm](actions/Ah5g9pDwWF9b9VW9.htm)|Rage|Furia|modificada|
|[aHE92WSosMDewe33.htm](actions/aHE92WSosMDewe33.htm)|Take a Taste|Take a Taste|modificada|
|[AJeLwbQBt1YH3S6v.htm](actions/AJeLwbQBt1YH3S6v.htm)|Fade Into Daydreams|Fade Into Daydreams|modificada|
|[AjLSHZSWQ90exdLo.htm](actions/AjLSHZSWQ90exdLo.htm)|Dismiss|Dismiss|modificada|
|[AJstokjdG6iDjVjE.htm](actions/AJstokjdG6iDjVjE.htm)|Impersonate|Imitar|modificada|
|[akmQzZoNhyfCKFpL.htm](actions/akmQzZoNhyfCKFpL.htm)|Airy Step|Airy Paso|modificada|
|[AOWwwXVDWwliFlWj.htm](actions/AOWwwXVDWwliFlWj.htm)|Blood Feast|Festín de sangre|modificada|
|[Aq2mXT2hLlstFL5C.htm](actions/Aq2mXT2hLlstFL5C.htm)|Invoke Celestial Privilege|Invocar Privilegio Celestial|modificada|
|[auOTjKr8gB1dqFh4.htm](actions/auOTjKr8gB1dqFh4.htm)|Breath Weapon (Cold) (Cone)|Arma de aliento (Frío) (Cono)|modificada|
|[AWXPiqz4xDN03iPs.htm](actions/AWXPiqz4xDN03iPs.htm)|Breath Weapon (Poison) (Line)|Arma envenenada (Línea)|modificada|
|[b3h3QqvwS5fnaiu1.htm](actions/b3h3QqvwS5fnaiu1.htm)|Banshee Cry|Grito de Banshee|modificada|
|[b6CanpXyUKJgxEwq.htm](actions/b6CanpXyUKJgxEwq.htm)|Salt Wound|Herida de Sal|modificada|
|[B7wDxyjX66JkiCOV.htm](actions/B7wDxyjX66JkiCOV.htm)|Venom Draw|Venom Draw|modificada|
|[Ba9EuLV1I2b1ue9P.htm](actions/Ba9EuLV1I2b1ue9P.htm)|Map the Area|Map the Area|modificada|
|[bCDMuu3tE4d6KHrJ.htm](actions/bCDMuu3tE4d6KHrJ.htm)|Pistolero's Retort|Pistolero's Retort|modificada|
|[BCp3tn2HATUOUEdQ.htm](actions/BCp3tn2HATUOUEdQ.htm)|Gossip|Cotilleo|modificada|
|[Bcxarzksqt9ezrs6.htm](actions/Bcxarzksqt9ezrs6.htm)|Stride|Zancada|modificada|
|[bG91dbtbgOnw7Ofx.htm](actions/bG91dbtbgOnw7Ofx.htm)|Board|Tablero|modificada|
|[BKMxC0ZvdYfdhx7C.htm](actions/BKMxC0ZvdYfdhx7C.htm)|Objection|Objeción|modificada|
|[BKnN9la3WNrRgZ6n.htm](actions/BKnN9la3WNrRgZ6n.htm)|Conduct Energy|Conducir Energía|modificada|
|[BlAOM2X92SI6HMtJ.htm](actions/BlAOM2X92SI6HMtJ.htm)|Seek|Buscar|modificada|
|[bp0Up04x3dzGK5bB.htm](actions/bp0Up04x3dzGK5bB.htm)|Debilitating Strike|Golpe debilitador|modificada|
|[bT3skovyLUtP22ME.htm](actions/bT3skovyLUtP22ME.htm)|Repair|Reparar|modificada|
|[c8TGiZ48ygoSPofx.htm](actions/c8TGiZ48ygoSPofx.htm)|Swim|Nadar|modificada|
|[CIqiFw9rqYnuzggq.htm](actions/CIqiFw9rqYnuzggq.htm)|Practical Research|Investigación Práctica|modificada|
|[CPTolKAF55p7D7Sn.htm](actions/CPTolKAF55p7D7Sn.htm)|Mirror-Trickery|Mirror-Trickery|modificada|
|[CrUPaPlsxy2bswaT.htm](actions/CrUPaPlsxy2bswaT.htm)|Mesmerizing Performance|Mesmerizing Interpretar|modificada|
|[cS9nfDRGD83bNU1p.htm](actions/cS9nfDRGD83bNU1p.htm)|Fly|Volar|modificada|
|[cx0juTYewwBmrYWv.htm](actions/cx0juTYewwBmrYWv.htm)|Beast's Charge|Beast's Charge|modificada|
|[cx5tBTy6weK6YSw9.htm](actions/cx5tBTy6weK6YSw9.htm)|Thermal Eruption|Erupción Térmica|modificada|
|[cYdz2grcOcRt4jk6.htm](actions/cYdz2grcOcRt4jk6.htm)|Disable Device|Inutilizar mecanismo|modificada|
|[cYtYKa1gDEl7y2N0.htm](actions/cYtYKa1gDEl7y2N0.htm)|Defend|Defender|modificada|
|[D2PNfIw7U6Ks0VY4.htm](actions/D2PNfIw7U6Ks0VY4.htm)|Steel Your Resolve|Steel Your Resolución|modificada|
|[d5I6018Mci2SWokk.htm](actions/d5I6018Mci2SWokk.htm)|Leap|Salto sin carrerilla|modificada|
|[D91jQs0wleU5ml4K.htm](actions/D91jQs0wleU5ml4K.htm)|Drink from the Chalice|Bebe del Cáliz|modificada|
|[DCb62iCBrJXy0Ik6.htm](actions/DCb62iCBrJXy0Ik6.htm)|Request|Pedir|modificada|
|[DFRLID6lIRw7CzOT.htm](actions/DFRLID6lIRw7CzOT.htm)|Spring the Trap|Salta la trampa|modificada|
|[dLgAMt3TbkmLkUqE.htm](actions/dLgAMt3TbkmLkUqE.htm)|Ready|Listo|modificada|
|[dnaPJfA0CDLNrWcW.htm](actions/dnaPJfA0CDLNrWcW.htm)|Implement's Interruption|Interrupción del implemento|modificada|
|[DS9sDOWkXrz2xmHi.htm](actions/DS9sDOWkXrz2xmHi.htm)|Eldritch Shot|Disparo Eldritch|modificada|
|[Dt6B1slsBy8ipJu9.htm](actions/Dt6B1slsBy8ipJu9.htm)|Disarm|Desarmar|modificada|
|[DXIZ4DHGxhZiWNWb.htm](actions/DXIZ4DHGxhZiWNWb.htm)|Long-Term Rest|Descanso prolongado|modificada|
|[DYn1igFjCGJEiP22.htm](actions/DYn1igFjCGJEiP22.htm)|Recall Ammunition|Recuperar Munición|modificada|
|[e2ePMDa7ixbLRryj.htm](actions/e2ePMDa7ixbLRryj.htm)|Encouraging Words|Palabras alentadoras|modificada|
|[e5JscXqvBUC867oo.htm](actions/e5JscXqvBUC867oo.htm)|Find Fault|Find Fault|modificada|
|[EA5vuSgJfiHH7plD.htm](actions/EA5vuSgJfiHH7plD.htm)|Track|Rastrear|modificada|
|[EAP98XaChJEbgKcK.htm](actions/EAP98XaChJEbgKcK.htm)|Retributive Strike|Golpe retributivo|modificada|
|[eBgO5gp5kKhGtmk9.htm](actions/eBgO5gp5kKhGtmk9.htm)|Water Transfer|Trasvase de Agua|modificada|
|[EcDcowQ8vS6cEfXd.htm](actions/EcDcowQ8vS6cEfXd.htm)|Scout Location|Localización del batidor|modificada|
|[EEDElIyin4z60PXx.htm](actions/EEDElIyin4z60PXx.htm)|Perform|Interpretar|modificada|
|[EeM0Czaep7G5ZSh5.htm](actions/EeM0Czaep7G5ZSh5.htm)|Ten Paces|Diez pasos|modificada|
|[EHa0owz6mccnmSBf.htm](actions/EHa0owz6mccnmSBf.htm)|Final Surge|Oleaje Final|modificada|
|[EhLvRWFKhZ3HtrZO.htm](actions/EhLvRWFKhZ3HtrZO.htm)|Settle Emotions|Settle Emotions|modificada|
|[EmFzFAwgQ0lJZwba.htm](actions/EmFzFAwgQ0lJZwba.htm)|Breath Weapon (Negative) (Line)|Arma de Aliento (Negativo) (Línea)|modificada|
|[enQieRrITuEQZxx2.htm](actions/enQieRrITuEQZxx2.htm)|Selfish Shield|Escudo egoísta|modificada|
|[ERNaJbLeFtdV96cZ.htm](actions/ERNaJbLeFtdV96cZ.htm)|De-Animating Gestures (True)|Gestos de Desanimación (Verdadero)|modificada|
|[ESMIHOOahLQoqxW1.htm](actions/ESMIHOOahLQoqxW1.htm)|Call Gun|Llamada Pistola|modificada|
|[EwgTZBWsc8qKaViP.htm](actions/EwgTZBWsc8qKaViP.htm)|Investigate|Investigar|modificada|
|[ewwCglB7XOPLUz72.htm](actions/ewwCglB7XOPLUz72.htm)|Lie|Mentir|modificada|
|[F0JgJR2rXKOg9k1z.htm](actions/F0JgJR2rXKOg9k1z.htm)|Upstage|Upstage|modificada|
|[F4Tz0YFz1Lr4eVZR.htm](actions/F4Tz0YFz1Lr4eVZR.htm)|De-Animating Gestures (False)|De-Animating Gestures (Falso)|modificada|
|[Fe487XZBdqEI2InL.htm](actions/Fe487XZBdqEI2InL.htm)|Dispelling Bullet|Bala Disipadora|modificada|
|[Fha8jFmfkOPxAsrZ.htm](actions/Fha8jFmfkOPxAsrZ.htm)|Calculate Threats|Calcular Amenazas|modificada|
|[fJImDBQfqfjKJOhk.htm](actions/fJImDBQfqfjKJOhk.htm)|Sense Direction|Sentir la dirección|modificada|
|[FkfWKq9jhhPzKAbb.htm](actions/FkfWKq9jhhPzKAbb.htm)|Rampaging Ferocity|Ferocidad desenfrenada|modificada|
|[fL7FhTBcKxMzLBAs.htm](actions/fL7FhTBcKxMzLBAs.htm)|Empty Vessel|Empty Vessel|modificada|
|[fodJ3zuwQsYnBbtk.htm](actions/fodJ3zuwQsYnBbtk.htm)|Exploit Vulnerability|Explotar vulnerabilidad|modificada|
|[ftG89SjTSa9DYDOD.htm](actions/ftG89SjTSa9DYDOD.htm)|Create Forgery|Elaborar Falsificar|modificada|
|[FzZAYGib08aEq5P2.htm](actions/FzZAYGib08aEq5P2.htm)|Accidental Shot|Disparo Accidental|modificada|
|[gBhWEy3ToxQeCLQm.htm](actions/gBhWEy3ToxQeCLQm.htm)|Covered Reload|Covered Reload|modificada|
|[ge56Lu1xXVFYUnLP.htm](actions/ge56Lu1xXVFYUnLP.htm)|Trip|Derribar|modificada|
|[GkmbTGfg8KcgynOA.htm](actions/GkmbTGfg8KcgynOA.htm)|Create a Diversion|Crear una distracción|modificada|
|[GPd3hmyUSbcSBj39.htm](actions/GPd3hmyUSbcSBj39.htm)|Stellar Misfortune|Stellar Misfortune|modificada|
|[grkggNQnOxWWsjBH.htm](actions/grkggNQnOxWWsjBH.htm)|Breath Weapon (Cold) (Line)|Arma de Aliento (Frío) (Línea)|modificada|
|[gxtq81VAhpmNvEgA.htm](actions/gxtq81VAhpmNvEgA.htm)|Tap Ley Line|Tap Ley Line|modificada|
|[h4Tzdhqfryp5m2fO.htm](actions/h4Tzdhqfryp5m2fO.htm)|Harvest Heartsliver|Harvest Heartsliver|modificada|
|[H6v1VgowHaKHnVlG.htm](actions/H6v1VgowHaKHnVlG.htm)|Burrow|Madriguera|modificada|
|[HAmGozJwLAal5v82.htm](actions/HAmGozJwLAal5v82.htm)|Intensify Vulnerability|Intensificar Vulnerabilidad|modificada|
|[HbejhIywqIufrmVM.htm](actions/HbejhIywqIufrmVM.htm)|Arcane Cascade|Cascada Arcana|modificada|
|[hFRHPBj6wjAayNtW.htm](actions/hFRHPBj6wjAayNtW.htm)|Jinx|Jinx|modificada|
|[hi56uHG1aAb84Zzu.htm](actions/hi56uHG1aAb84Zzu.htm)|Fight with Fear|Lucha con miedo|modificada|
|[hPZQ5vA9QHEPtjFW.htm](actions/hPZQ5vA9QHEPtjFW.htm)|Spin Tale|Spin Tale|modificada|
|[I75R9NSfsVrit6cU.htm](actions/I75R9NSfsVrit6cU.htm)|Cram|Cram|modificada|
|[I9k9qe4gOT8UVK4e.htm](actions/I9k9qe4gOT8UVK4e.htm)|Mist Blending|Mist Blending|modificada|
|[iBf9uGn5LOHkWpZ6.htm](actions/iBf9uGn5LOHkWpZ6.htm)|Craft Disharmonic Instrument|Elaborar Instrumento Disarmónico|modificada|
|[IE2nThCmoyhQA0Jn.htm](actions/IE2nThCmoyhQA0Jn.htm)|Avoid Notice|Evitar ser visto el descubrimiento|modificada|
|[iEYZ5PAim5Cvbila.htm](actions/iEYZ5PAim5Cvbila.htm)|Secure Disguises|Disfraz Seguro|modificada|
|[IGPbMkKjnlFW1w1a.htm](actions/IGPbMkKjnlFW1w1a.htm)|Bribe Contact|Sobornar Contacto|modificada|
|[iJLzVonevhsi2uPs.htm](actions/iJLzVonevhsi2uPs.htm)|Visions of Sin|Visiones del Pecado|modificada|
|[ijZ0DDFpMkWqaShd.htm](actions/ijZ0DDFpMkWqaShd.htm)|Palm an Object|Escamotear un objeto|modificada|
|[iTx0vXAhiS4lKwEi.htm](actions/iTx0vXAhiS4lKwEi.htm)|Psychic Defense|Defensa Psíquica|modificada|
|[Iuq8CeNqv3a0oWfQ.htm](actions/Iuq8CeNqv3a0oWfQ.htm)|Life Block|Bloqueo de Vida|modificada|
|[IV8sgoLO6ShD3DCJ.htm](actions/IV8sgoLO6ShD3DCJ.htm)|Expel Maelstrom|Expulsar Maelstrom|modificada|
|[IX1VlVCL5sFTptEE.htm](actions/IX1VlVCL5sFTptEE.htm)|Liberating Step|Paso liberador|modificada|
|[izvfZ561JTdeyh6i.htm](actions/izvfZ561JTdeyh6i.htm)|Goblin Jubilee|Goblin Jubilee|modificada|
|[jbmXxq56swDYw8hy.htm](actions/jbmXxq56swDYw8hy.htm)|Final Spite|Final Spite|modificada|
|[jftNJjBNxp2cleoi.htm](actions/jftNJjBNxp2cleoi.htm)|Expeditious Inspection|Inspección Expeditiva|modificada|
|[JLPY5hl4qiJ1zLi1.htm](actions/JLPY5hl4qiJ1zLi1.htm)|Discover|Descubrir|modificada|
|[JLyNyaRmxL51jebC.htm](actions/JLyNyaRmxL51jebC.htm)|Energy Emanation (Cold)|Emanación de Energía (Fría)|modificada|
|[JpcegMRqdizrkG0m.htm](actions/JpcegMRqdizrkG0m.htm)|Slayer's Identification|Identificación del Cazador|modificada|
|[JPHWzD2soqjffeSU.htm](actions/JPHWzD2soqjffeSU.htm)|Drain Life|Drenar Vida|modificada|
|[JqThDeWlGobiAPJd.htm](actions/JqThDeWlGobiAPJd.htm)|Breath Weapon (Fire) (Cone)|Arma de Aliento (Fuego) (Cono)|modificada|
|[jSC5AYEfliOPpO3H.htm](actions/jSC5AYEfliOPpO3H.htm)|Reloading Strike|Golpe de recarga|modificada|
|[jtuWJa6Iyyd3gkVv.htm](actions/jtuWJa6Iyyd3gkVv.htm)|Gallop|Galope|modificada|
|[JuqmIAnkL9hVGai8.htm](actions/JuqmIAnkL9hVGai8.htm)|Hustle|Aligerar|modificada|
|[JUvAvruz7yRQXfz2.htm](actions/JUvAvruz7yRQXfz2.htm)|Long Jump|Salto de longitud|modificada|
|[JYi4MnsdFu618hPm.htm](actions/JYi4MnsdFu618hPm.htm)|Hunt Prey|Perseguir presa|modificada|
|[jZQoAHmGJvi53NRR.htm](actions/jZQoAHmGJvi53NRR.htm)|Psychometric Assessment|Evaluación Psicométrica|modificada|
|[k5TASjIxghvGCy7g.htm](actions/k5TASjIxghvGCy7g.htm)|Call to Axis|Llamada al Eje|modificada|
|[K878asDgf1EF0B9S.htm](actions/K878asDgf1EF0B9S.htm)|Confident Finisher|Confident Finisher|modificada|
|[KAVf7AmRnbCAHrkT.htm](actions/KAVf7AmRnbCAHrkT.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[kbUymGTjbesOKsV6.htm](actions/kbUymGTjbesOKsV6.htm)|Primal Roar|Primal Roar|modificada|
|[KC6o1cvbr45xnMei.htm](actions/KC6o1cvbr45xnMei.htm)|Conjure Bullet|Conjurar Bala|modificada|
|[KjoCEEmPGTeFE4hh.htm](actions/KjoCEEmPGTeFE4hh.htm)|Treat Poison|Tratar veneno|modificada|
|[kKKHwVUnroKuAnOt.htm](actions/kKKHwVUnroKuAnOt.htm)|Toxic Skin|Piel tóxica|modificada|
|[kMcV8e5EZUxa6evt.htm](actions/kMcV8e5EZUxa6evt.htm)|Squeeze|Escurrirse|modificada|
|[KMKuJ0onVS72t9Fv.htm](actions/KMKuJ0onVS72t9Fv.htm)|Manifest Soulforged Armament|Manifiesto Soulforged Armamento|modificada|
|[kRxWINkrHUPSHHYq.htm](actions/kRxWINkrHUPSHHYq.htm)|Recall the Teachings|Recordar las Enseñanzas|modificada|
|[kUME7DMhhHH6eIiH.htm](actions/kUME7DMhhHH6eIiH.htm)|That's My Number|That's My Number|modificada|
|[kV3XM0YJeS2KCSOb.htm](actions/kV3XM0YJeS2KCSOb.htm)|Scout|Explorador|modificada|
|[KvVdGkUi0iJwapvD.htm](actions/KvVdGkUi0iJwapvD.htm)|Fortify Camp|Fortificar Campamento|modificada|
|[KyKXp599tK9BgodC.htm](actions/KyKXp599tK9BgodC.htm)|Dutiful Retaliation|Dutiful Retaliation|modificada|
|[lAvEK4yNL3Y7pVmr.htm](actions/lAvEK4yNL3Y7pVmr.htm)|Assume a Role|Asumir un rol|modificada|
|[LfsQy2T8GjpuMqAh.htm](actions/LfsQy2T8GjpuMqAh.htm)|Breath Weapon (Electricity) (Cone)|Arma de Aliento (Electricidad) (Cono)|modificada|
|[lID4rJHAVZB6tavf.htm](actions/lID4rJHAVZB6tavf.htm)|Run Over|Atropellado|modificada|
|[Lt591Og3XRhx5dRx.htm](actions/Lt591Og3XRhx5dRx.htm)|Breath Weapon (Acid) (Cone)|Arma de Aliento (Ácido) (Cono)|modificada|
|[lvqPQDdWT2DDO0k2.htm](actions/lvqPQDdWT2DDO0k2.htm)|Invest an Item|Invierte un objeto|modificada|
|[LWrR6UiGm3eCAALJ.htm](actions/LWrR6UiGm3eCAALJ.htm)|Collect Spirit Remnant|Recoge los remanentes de espíritu|modificada|
|[m0f2B7G9eaaTmhFL.htm](actions/m0f2B7G9eaaTmhFL.htm)|Devise a Stratagem|Devise a Stratagem|modificada|
|[M76ycLAqHoAgbcej.htm](actions/M76ycLAqHoAgbcej.htm)|Balance|Mantener equilibrio|modificada|
|[M8RCbthRhB4bxO9t.htm](actions/M8RCbthRhB4bxO9t.htm)|Iron Command|Orden imperiosa|modificada|
|[Ma93dpT4K7JbP9gu.htm](actions/Ma93dpT4K7JbP9gu.htm)|Prove Peace|Probar la paz|modificada|
|[mech0dhb4eKbCAu0.htm](actions/mech0dhb4eKbCAu0.htm)|Coughing Dragon|Dragón Tosedor|modificada|
|[Mh4Vdg6gu8g8RAjh.htm](actions/Mh4Vdg6gu8g8RAjh.htm)|Mirror's Reflection|Reflejo del Espejo|modificada|
|[MHLuKy4nQO2Z4Am1.htm](actions/MHLuKy4nQO2Z4Am1.htm)|Administer First Aid|Prestar Primeros auxilios|modificada|
|[mk6rzaAzsBBRGJnh.htm](actions/mk6rzaAzsBBRGJnh.htm)|Call Upon the Brightness|Llamada, la Luminosidad|modificada|
|[MLchOIG6uLvzK3r0.htm](actions/MLchOIG6uLvzK3r0.htm)|Gain Contact|Ganar Contacto|modificada|
|[MLOkyKi1Y3N6y56Q.htm](actions/MLOkyKi1Y3N6y56Q.htm)|Raise Neck|Levantar Cuello|modificada|
|[mVscmsZWWcVACdU5.htm](actions/mVscmsZWWcVACdU5.htm)|Soaring Flight|Soaring Flight|modificada|
|[MY6z2b4GPhAD2Eoa.htm](actions/MY6z2b4GPhAD2Eoa.htm)|Share Life|Compartir Vida|modificada|
|[n5vwBnLSlIXL9ptp.htm](actions/n5vwBnLSlIXL9ptp.htm)|Manifest Eidolon|Manifiesto Eidolon|modificada|
|[N6U02s9qJKQIvmQd.htm](actions/N6U02s9qJKQIvmQd.htm)|Wish for Luck|Deseo de Suerte|modificada|
|[naKVqd8POxcnGclz.htm](actions/naKVqd8POxcnGclz.htm)|Explode|Explotar|modificada|
|[nbfNETdpee8CVM17.htm](actions/nbfNETdpee8CVM17.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[ncdryKskPwHMgHFh.htm](actions/ncdryKskPwHMgHFh.htm)|Wicked Thorns|Wicked Thorns|modificada|
|[NEBuuhjZE9BL3I8v.htm](actions/NEBuuhjZE9BL3I8v.htm)|Breath Weapon (Poison) (Cone)|Arma envenenada (Cono)|modificada|
|[nSTMF6kYIt6rXhJx.htm](actions/nSTMF6kYIt6rXhJx.htm)|Seething Frenzy|Seething Frenzy|modificada|
|[NYR2IdfUiHevdt1V.htm](actions/NYR2IdfUiHevdt1V.htm)|Break Them Down|Drive Them Down|modificada|
|[O1iircZOOCo42r9Y.htm](actions/O1iircZOOCo42r9Y.htm)|Ghost Shot|Disparo fantasma|modificada|
|[oanRfXGLnBm6mMVg.htm](actions/oanRfXGLnBm6mMVg.htm)|Dragon Breath|Aliento de dragón|modificada|
|[oAWNluJaMlaGysXA.htm](actions/oAWNluJaMlaGysXA.htm)|Barbed Quills|Púas punzantes|modificada|
|[OdIUybJ3ddfL7wzj.htm](actions/OdIUybJ3ddfL7wzj.htm)|Stand|Ponerse de pie|modificada|
|[OizxuPb44g3eHPFh.htm](actions/OizxuPb44g3eHPFh.htm)|Borrow an Arcane Spell|Pedir prestado un conjuro arcano|modificada|
|[OJ9cIvPukPT0rppR.htm](actions/OJ9cIvPukPT0rppR.htm)|Wyrm's Breath|Wyrm's Breath|modificada|
|[On5CQjX4euWqToly.htm](actions/On5CQjX4euWqToly.htm)|Resist Elf Magic|Resistir magia elfa|modificada|
|[OQaFzDtVEOMWizJJ.htm](actions/OQaFzDtVEOMWizJJ.htm)|Repeat a Spell|Repetir un conjuro|modificada|
|[Or6RLXeoZkN8CLdi.htm](actions/Or6RLXeoZkN8CLdi.htm)|Amulet's Abeyance|Amulet's Abeyance|modificada|
|[orjJjLdm4XNAcFi8.htm](actions/orjJjLdm4XNAcFi8.htm)|Mark for Death|Marcar para morir|modificada|
|[OSefkMgojBLqmRDh.htm](actions/OSefkMgojBLqmRDh.htm)|Refocus|Reenfocar|modificada|
|[OX4fy22hQgUHDr0q.htm](actions/OX4fy22hQgUHDr0q.htm)|Make an Impression|Causar impresión|modificada|
|[plBGdZhqq5JBl1D8.htm](actions/plBGdZhqq5JBl1D8.htm)|Gather Information|Reunir información|modificada|
|[PM5jvValFkbFH3TV.htm](actions/PM5jvValFkbFH3TV.htm)|Mount|Monte|modificada|
|[PMbdMWc2QroouFGD.htm](actions/PMbdMWc2QroouFGD.htm)|Grapple|Presa|modificada|
|[pprgrYQ1QnIDGZiy.htm](actions/pprgrYQ1QnIDGZiy.htm)|Climb|Trepar|modificada|
|[pTHMZLqWDJ3lkan9.htm](actions/pTHMZLqWDJ3lkan9.htm)|Smoke Blending|Mezcla de humo|modificada|
|[Q4kdWVOf2ztIBFg1.htm](actions/Q4kdWVOf2ztIBFg1.htm)|Identify Alchemy|Identificar alquimia|modificada|
|[Q8H3lBgiUyGvZYTM.htm](actions/Q8H3lBgiUyGvZYTM.htm)|Telekinetic Assault|Asalto telecinético|modificada|
|[qc0VsZ0UesnurUUB.htm](actions/qc0VsZ0UesnurUUB.htm)|Take a Breather|Tómate un respiro|modificada|
|[QDW9H8XLIjuW2fE4.htm](actions/QDW9H8XLIjuW2fE4.htm)|Spellstrike|Spellstrike|modificada|
|[Qf1ylAbdVi1rkc8M.htm](actions/Qf1ylAbdVi1rkc8M.htm)|Maneuver in Flight|Maniobra en vuelo|modificada|
|[QHFMeJGzFWj2QczA.htm](actions/QHFMeJGzFWj2QczA.htm)|Quick Tincture|Tintura Rápida|modificada|
|[QIrJJ1pl4H6DctaQ.htm](actions/QIrJJ1pl4H6DctaQ.htm)|Reconnoiter|Reconocedor|modificada|
|[qm7xptMSozAinnPS.htm](actions/qm7xptMSozAinnPS.htm)|Arrest a Fall|Detén una caída|modificada|
|[QNAVeNKtHA0EUw4X.htm](actions/QNAVeNKtHA0EUw4X.htm)|Feint|Fintar|modificada|
|[QQQaV7pi9Gv2GpLj.htm](actions/QQQaV7pi9Gv2GpLj.htm)|Breath Weapon (Piercing) (Line)|Arma de Aliento (Perforante) (Línea)|modificada|
|[qVNVSmsgpKFGk9hV.htm](actions/qVNVSmsgpKFGk9hV.htm)|Conceal an Object|Ocultar un objeto|modificada|
|[r5Uth6yvCoE4tr9z.htm](actions/r5Uth6yvCoE4tr9z.htm)|Destructive Vengeance|Venganza Destructiva|modificada|
|[RADNqsvAt4gP9FOX.htm](actions/RADNqsvAt4gP9FOX.htm)|Raconteur's Reload|Raconteur's Reload|modificada|
|[RDXXE7wMrSPCLv5k.htm](actions/RDXXE7wMrSPCLv5k.htm)|Steal|Sustraer|modificada|
|[rI2MSXR1MQzgQUO7.htm](actions/rI2MSXR1MQzgQUO7.htm)|Grim Swagger|Grim Swagger|modificada|
|[Rlp7ND33yYfxiEWi.htm](actions/Rlp7ND33yYfxiEWi.htm)|Master Strike|Golpe maestro|modificada|
|[rmwa3OyhTZ2i2AHl.htm](actions/rmwa3OyhTZ2i2AHl.htm)|Craft|Artesanía|modificada|
|[rqT4LMH7qbfyScBi.htm](actions/rqT4LMH7qbfyScBi.htm)|Reclaim Destiny|Reclaim Destiny|modificada|
|[rSpCV0leurp2Bg2d.htm](actions/rSpCV0leurp2Bg2d.htm)|Instinctive Obfuscation|Ofuscación instintiva|modificada|
|[s2RrhZx1f1X4YnYV.htm](actions/s2RrhZx1f1X4YnYV.htm)|Divert Lightning|Desviar Rayo|modificada|
|[s4s40hJr5uRLOqix.htm](actions/s4s40hJr5uRLOqix.htm)|Change Tradition Focus|Cambiar Enfoque Tradición|modificada|
|[s4V7JWSMF9JPJAeX.htm](actions/s4V7JWSMF9JPJAeX.htm)|Envenom|Envenom|modificada|
|[S9PZFOVe7zhORkUc.htm](actions/S9PZFOVe7zhORkUc.htm)|Absorb into the Aegis|Absorber en la Égida|modificada|
|[SB7cMECVtE06kByk.htm](actions/SB7cMECVtE06kByk.htm)|Cover Tracks|Cubrir Rastrear|modificada|
|[SjmKHgI7a5Z9JzBx.htm](actions/SjmKHgI7a5Z9JzBx.htm)|Force Open|Abrir por la fuerza|modificada|
|[sL1J8cFwpy1lI359.htm](actions/sL1J8cFwpy1lI359.htm)|Study|Estudia|modificada|
|[slwllcUtVhoCdBuM.htm](actions/slwllcUtVhoCdBuM.htm)|Breath Weapon (Acid) (Line)|Arma de Aliento (Ácido) (Línea)|modificada|
|[SMF1hTWPHtmlS8Cd.htm](actions/SMF1hTWPHtmlS8Cd.htm)|Bullet Dancer Stance|Posición de bailarina de balas.|modificada|
|[sn2hIy1iIJX9Vpgj.htm](actions/sn2hIy1iIJX9Vpgj.htm)|Point Out|Point Out|modificada|
|[SPtzUNatWJvTK61y.htm](actions/SPtzUNatWJvTK61y.htm)|Spirit's Mercy|Merced del Espíritu|modificada|
|[t5nBkpjroaq7QBGK.htm](actions/t5nBkpjroaq7QBGK.htm)|Research|Investigación|modificada|
|[TC7OcDa7JlWbqMaN.htm](actions/TC7OcDa7JlWbqMaN.htm)|Treat Disease|Tratar enfermedad|modificada|
|[tfa4Sh7wcxCEqL29.htm](actions/tfa4Sh7wcxCEqL29.htm)|Follow the Expert|Seguir al experto|modificada|
|[tHCqgwjtQtzNqVvd.htm](actions/tHCqgwjtQtzNqVvd.htm)|Coerce|Obligar|modificada|
|[TiNDYUGlMmxzxBYU.htm](actions/TiNDYUGlMmxzxBYU.htm)|Search|Registrar|modificada|
|[Tlrde2xh7AhesXNB.htm](actions/Tlrde2xh7AhesXNB.htm)|One Shot, One Kill|One Shot, One Kill|modificada|
|[tNrBIYct9l1lrW1I.htm](actions/tNrBIYct9l1lrW1I.htm)|Field of Roots|Campo de raíces|modificada|
|[toxlmkoJqOuy4Vz1.htm](actions/toxlmkoJqOuy4Vz1.htm)|Energy Emanation (Electricity)|Emanación de Energía (Electricidad)|modificada|
|[TSDbyYRQwhIyY2Oq.htm](actions/TSDbyYRQwhIyY2Oq.htm)|Energy Shot|Disparo de Energía|modificada|
|[tu5viJZT4zFE1sYn.htm](actions/tu5viJZT4zFE1sYn.htm)|Whirlwind Maul|Zarpazo doble|modificada|
|[tuZnRWHixLArvaIf.htm](actions/tuZnRWHixLArvaIf.htm)|Glimpse of Redemption|Atisbo de redención|modificada|
|[tw1KDRPdBAkg5DlS.htm](actions/tw1KDRPdBAkg5DlS.htm)|Clear a Path|Clear a Path|modificada|
|[TXqTIwNGULs3j6CH.htm](actions/TXqTIwNGULs3j6CH.htm)|Bullying Press|Aprovechar|modificada|
|[UAaQk93a30nx0nYY.htm](actions/UAaQk93a30nx0nYY.htm)|Affix a Talisman|Colocar un Talismán|modificada|
|[ublVm5gmCIm3eRdQ.htm](actions/ublVm5gmCIm3eRdQ.htm)|Ring Bell|Ring Bell|modificada|
|[UEkGL7uAGYDPFNfK.htm](actions/UEkGL7uAGYDPFNfK.htm)|Fire in the Hole|Fuego en el Agujero|modificada|
|[UHpkTuCtyaPqiCAB.htm](actions/UHpkTuCtyaPqiCAB.htm)|Step|Paso|modificada|
|[Ul4I0ER6pj3U5eAk.htm](actions/Ul4I0ER6pj3U5eAk.htm)|Invigorating Fear|Miedo vigorizante|modificada|
|[uMFB3uw8WTWL0LZD.htm](actions/uMFB3uw8WTWL0LZD.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[uOlyklPCUWLtCaYI.htm](actions/uOlyklPCUWLtCaYI.htm)|Chassis Deflection|Desviación de chasis|modificada|
|[Uq2qy9aGNQ5JcPI1.htm](actions/Uq2qy9aGNQ5JcPI1.htm)|Into the Fray|Into the Fray|modificada|
|[uS3qDAgOkZ7b8ERL.htm](actions/uS3qDAgOkZ7b8ERL.htm)|Drive|Drive|modificada|
|[ust1jJSCZQUhBZIz.htm](actions/ust1jJSCZQUhBZIz.htm)|Take Cover|Ponerse a cubierto|modificada|
|[UWdRX1VelipCzrCg.htm](actions/UWdRX1VelipCzrCg.htm)|Avert Gaze|Avert Gaze|modificada|
|[uWTQxEOj2pl45Kns.htm](actions/uWTQxEOj2pl45Kns.htm)|Sense Weakness|Sentir Debilidad|modificada|
|[UyMkWfVqdabLTgkH.htm](actions/UyMkWfVqdabLTgkH.htm)|Wind Them Up|Dales cuerda|modificada|
|[v82XtjAVN4ffgVVR.htm](actions/v82XtjAVN4ffgVVR.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[VAcxOCFQLb3Bap7K.htm](actions/VAcxOCFQLb3Bap7K.htm)|Accept Echo|Aceptar Eco|modificada|
|[vblQt8LjvsBEB1J1.htm](actions/vblQt8LjvsBEB1J1.htm)|Energy Emanation (Fire)|Emanación de Energía (Fuego)|modificada|
|[VCUz5EnBUJF07j1a.htm](actions/VCUz5EnBUJF07j1a.htm)|Sever Conduit|Sever Conduit|modificada|
|[vdnuczo4ktS7ow7N.htm](actions/vdnuczo4ktS7ow7N.htm)|Prophecy's Pawn|Peón de la Profecía|modificada|
|[vFaNE7s7vFs9BxJW.htm](actions/vFaNE7s7vFs9BxJW.htm)|Set Free|Liberar|modificada|
|[VjxZFuUXrCU94MWR.htm](actions/VjxZFuUXrCU94MWR.htm)|Strike|Golpe|modificada|
|[VMozDqMMuK5kpoX4.htm](actions/VMozDqMMuK5kpoX4.htm)|Sneak|Movimiento furtivo|modificada|
|[VNuOwXIHafSLHvsZ.htm](actions/VNuOwXIHafSLHvsZ.htm)|Spellsling|Spellsling|modificada|
|[vO0Y1dVjNfbDyT4S.htm](actions/vO0Y1dVjNfbDyT4S.htm)|Vital Shot|Tiro Vital|modificada|
|[VOEWhPQfN3lvHivK.htm](actions/VOEWhPQfN3lvHivK.htm)|Foresight|Presciencia|modificada|
|[VQ5OxaDKE0lCj8Mr.htm](actions/VQ5OxaDKE0lCj8Mr.htm)|Shadow Step|Paso de Sombra|modificada|
|[vvYsE7OiSSCXhPdr.htm](actions/vvYsE7OiSSCXhPdr.htm)|Set Explosives|Establecer Explosión|modificada|
|[vZltxwRNvF5khf9a.htm](actions/vZltxwRNvF5khf9a.htm)|Boarding Assault|Asalto de abordaje|modificada|
|[W4M8L9WepGLamlHs.htm](actions/W4M8L9WepGLamlHs.htm)|Threatening Approach|Acercamiento Amenazante|modificada|
|[WpCs3QmPVn8SRbXy.htm](actions/WpCs3QmPVn8SRbXy.htm)|Touch and Go|Touch and Go|modificada|
|[wQYmDStjdjn0I26t.htm](actions/wQYmDStjdjn0I26t.htm)|Release|Soltar|modificada|
|[wt6jdjjje16Nx34f.htm](actions/wt6jdjjje16Nx34f.htm)|Jumping Jenny|Saltando Jenny|modificada|
|[wwvPiG2kET2rkSAG.htm](actions/wwvPiG2kET2rkSAG.htm)|Change Shape (Kitsune)|Cambiar Forma (Kitsune)|modificada|
|[x1qSEkzHAviQ5jry.htm](actions/x1qSEkzHAviQ5jry.htm)|Lay Down Arms|Lay Down Arms|modificada|
|[xccOiNL2W1EtfUYl.htm](actions/xccOiNL2W1EtfUYl.htm)|Pointed Question|Pregunta Puntual|modificada|
|[XeZwXzR1KBlJF770.htm](actions/XeZwXzR1KBlJF770.htm)|Resist Magic|Resistir Magia|modificada|
|[Xg57qG1rOfSSobke.htm](actions/Xg57qG1rOfSSobke.htm)|Breath Weapon (Riding Drake)|Arma de Aliento (Montar Drake)|modificada|
|[xGqOIheAOV12RGU4.htm](actions/xGqOIheAOV12RGU4.htm)|Dueling Counter|Contador de Duelo|modificada|
|[XH133DE3daobeav1.htm](actions/XH133DE3daobeav1.htm)|Screaming Skull|Screaming Skull|modificada|
|[xJEkXFJgEfEida27.htm](actions/xJEkXFJgEfEida27.htm)|Rally|Rally|modificada|
|[xjGwis0uaC2305pm.htm](actions/xjGwis0uaC2305pm.htm)|Raise a Shield|Alzar un escudo|modificada|
|[XkrN7gxdRXTYYBkX.htm](actions/XkrN7gxdRXTYYBkX.htm)|Restore the Mind|Restablecimiento de la Mente|modificada|
|[XMcnh4cSI32tljXa.htm](actions/XMcnh4cSI32tljXa.htm)|Hide|Esconderse|modificada|
|[xpsD4DsYHKXCB4ac.htm](actions/xpsD4DsYHKXCB4ac.htm)|Anchor|Ancla|modificada|
|[XSGlLjI80LDCimAP.htm](actions/XSGlLjI80LDCimAP.htm)|Sustain an Activation|Mantener una Activación|modificada|
|[xTK2zsWFyxSJvYbX.htm](actions/xTK2zsWFyxSJvYbX.htm)|Pursue a Lead|Perseguir una pista|modificada|
|[Yb0C1uLzeHrVLl7a.htm](actions/Yb0C1uLzeHrVLl7a.htm)|Detect Magic|Detectar magia|modificada|
|[Yfl6q6Pi42FttDRE.htm](actions/Yfl6q6Pi42FttDRE.htm)|Glimpse Vulnerability|Vislumbrar Vulnerabilidad|modificada|
|[yh9O9BQjwWrAIiuf.htm](actions/yh9O9BQjwWrAIiuf.htm)|Take Control|Toma el control|modificada|
|[YMhmebfXAoOFXeSB.htm](actions/YMhmebfXAoOFXeSB.htm)|Drifter's Wake|Drifter's Wake|modificada|
|[yOtu5X3qWfjuX8Vy.htm](actions/yOtu5X3qWfjuX8Vy.htm)|Learn Name|Aprender Nombre|modificada|
|[YpuEmI1fJBZD3kMc.htm](actions/YpuEmI1fJBZD3kMc.htm)|Tendril Strike|Golpe de zarcillo|modificada|
|[yUjuLbBMflVum8Yn.htm](actions/yUjuLbBMflVum8Yn.htm)|Launch Fireworks Display|Lanzar fuegos artificiales|modificada|
|[yXk0G8l0leaqHh1U.htm](actions/yXk0G8l0leaqHh1U.htm)|Host Spirit|Espíritu Anfitrión|modificada|
|[yzNJgwzV9XqEhKc6.htm](actions/yzNJgwzV9XqEhKc6.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[Z8aa6afUterlfh5i.htm](actions/Z8aa6afUterlfh5i.htm)|Travel|Viaje|modificada|
|[ZhKXvnw7ND2hQ2pp.htm](actions/ZhKXvnw7ND2hQ2pp.htm)|Cleanse Soul Path|Limpiar Camino del Alma|modificada|
|[ZJcc7KGOEsYvN6SE.htm](actions/ZJcc7KGOEsYvN6SE.htm)|Overload Vision|Visión de sobrecarga|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00LNVSCbwJ8pszwE.htm](actions/00LNVSCbwJ8pszwE.htm)|Mutagenic Flashback|Retropectiva Mutagénica|oficial|
|[0b3TTeQGFOiy6ykz.htm](actions/0b3TTeQGFOiy6ykz.htm)|Change Shape (Beastkin)|Cambiar la forma (Piel de bestia)|oficial|
|[1OagaWtBpVXExToo.htm](actions/1OagaWtBpVXExToo.htm)|Recall Knowledge|Recordar conocimiento|oficial|
|[49y9Ec4bDii8pcD3.htm](actions/49y9Ec4bDii8pcD3.htm)|Subsist|Subsistir|oficial|
|[7pdG8l9POMK76Lf2.htm](actions/7pdG8l9POMK76Lf2.htm)|Warding Sign|Símbolo de protección|oficial|
|[A72nHGUtNXgY5Ey9.htm](actions/A72nHGUtNXgY5Ey9.htm)|Delay|Retrasar|oficial|
|[d9gbpiQjChYDYA2L.htm](actions/d9gbpiQjChYDYA2L.htm)|Decipher Writing|Descifrar escritura|oficial|
|[dCuvfq3r2K9wXY9g.htm](actions/dCuvfq3r2K9wXY9g.htm)|Basic Finisher|Remate básico|oficial|
|[EfjoIuDmtUn4yiow.htm](actions/EfjoIuDmtUn4yiow.htm)|Opportune Riposte|Réplica oportuna|oficial|
|[eReSHVEPCsdkSL4G.htm](actions/eReSHVEPCsdkSL4G.htm)|Identify Magic|Identificar magia|oficial|
|[ev8OHpBO3xq3Zt08.htm](actions/ev8OHpBO3xq3Zt08.htm)|Tail Toxin|Toxina de Cola|oficial|
|[HCl3pzVefiv9ZKQW.htm](actions/HCl3pzVefiv9ZKQW.htm)|Aid|Prestar ayuda|oficial|
|[HYNhdaPtF1QmQbR3.htm](actions/HYNhdaPtF1QmQbR3.htm)|Drop Prone|Tumbarse|oficial|
|[pvQ5rY2zrtPI614F.htm](actions/pvQ5rY2zrtPI614F.htm)|Interact|Interactuar|oficial|
|[Q5iIYCFdqJFM31GW.htm](actions/Q5iIYCFdqJFM31GW.htm)|Learn a Spell|Aprender un conjuro|oficial|
|[q9nbyIF0PEBqMtYe.htm](actions/q9nbyIF0PEBqMtYe.htm)|Command an Animal|Comandar a un animal|oficial|
|[QyzlsLrqM0EEwd7j.htm](actions/QyzlsLrqM0EEwd7j.htm)|Earn Income|Obtener ingresos|oficial|
|[SkZAQRkLLkmBQNB9.htm](actions/SkZAQRkLLkmBQNB9.htm)|Escape|Huir|oficial|
|[Tj055UcNm6UEgtCg.htm](actions/Tj055UcNm6UEgtCg.htm)|Crawl|Gatear|oficial|
|[TMBXArwICQRJdwT6.htm](actions/TMBXArwICQRJdwT6.htm)|Fey's Fortune|Fortuna de las hadas|oficial|
