# Estado de la traducción (ancestries)

 * **oficial**: 20
 * **modificada**: 15
 * **libre**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[4BL5wf1VF9feC2rY.htm](ancestries/4BL5wf1VF9feC2rY.htm)|Kitsune|Kitsune|modificada|
|[58rL5sg2y4arW1i5.htm](ancestries/58rL5sg2y4arW1i5.htm)|Skeleton|Esqueleto|modificada|
|[6F2fSFC1Eo1JdpY4.htm](ancestries/6F2fSFC1Eo1JdpY4.htm)|Poppet|Poppet|modificada|
|[cLtOGIkuSSa4UDHY.htm](ancestries/cLtOGIkuSSa4UDHY.htm)|Vanara|Vanara|modificada|
|[dw2K1AJR9mQ25nDP.htm](ancestries/dw2K1AJR9mQ25nDP.htm)|Kashrishi|Kashrishi|modificada|
|[FXlXmNBFiiz9oasi.htm](ancestries/FXlXmNBFiiz9oasi.htm)|Fleshwarp|Modelado de carne|modificada|
|[GfLwE884NoRC7cRi.htm](ancestries/GfLwE884NoRC7cRi.htm)|Android|Androide|modificada|
|[GXcC6oVa5quzgNHD.htm](ancestries/GXcC6oVa5quzgNHD.htm)|Strix|Strix|modificada|
|[hIA3qiUsxvLZXrFP.htm](ancestries/hIA3qiUsxvLZXrFP.htm)|Fetchling|Fetchling|modificada|
|[J7T7bDLaQGoY1sMF.htm](ancestries/J7T7bDLaQGoY1sMF.htm)|Nagaji|Nagaji|modificada|
|[kYsBAJ103T44agJF.htm](ancestries/kYsBAJ103T44agJF.htm)|Automaton|Autómata|modificada|
|[q6rsqYARyOGXZA8F.htm](ancestries/q6rsqYARyOGXZA8F.htm)|Shoony|Shoony|modificada|
|[tSurOqRcfumadTfr.htm](ancestries/tSurOqRcfumadTfr.htm)|Ghoran|Ghoran|modificada|
|[u1VJEXsVlmh3Fyx0.htm](ancestries/u1VJEXsVlmh3Fyx0.htm)|Vishkanya|Vishkanya|modificada|
|[yFoojz6q3ZjvceFw.htm](ancestries/yFoojz6q3ZjvceFw.htm)|Azarketi|Azarketi|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[18xDKYPDBLEv2myX.htm](ancestries/18xDKYPDBLEv2myX.htm)|Tengu|Tengu|oficial|
|[7oQxL6wgsokD3QXG.htm](ancestries/7oQxL6wgsokD3QXG.htm)|Kobold|Kóbold|oficial|
|[972EkpJOPv9KkQIW.htm](ancestries/972EkpJOPv9KkQIW.htm)|Catfolk|Félido|oficial|
|[BYj5ZvlXZdpaEgA6.htm](ancestries/BYj5ZvlXZdpaEgA6.htm)|Dwarf|Enano|oficial|
|[c4secsSNG2AO7I5i.htm](ancestries/c4secsSNG2AO7I5i.htm)|Goloma|Goloma|oficial|
|[cdhgByGG1WtuaK73.htm](ancestries/cdhgByGG1WtuaK73.htm)|Leshy|Leshy|oficial|
|[CYlfsYLJcBOgqKtD.htm](ancestries/CYlfsYLJcBOgqKtD.htm)|Gnome|Gnomo|oficial|
|[GgZAHbrjnzWOZy2v.htm](ancestries/GgZAHbrjnzWOZy2v.htm)|Halfling|Mediano|oficial|
|[HWEgF7Gmoq55VhTL.htm](ancestries/HWEgF7Gmoq55VhTL.htm)|Lizardfolk|Hombre lagarto|oficial|
|[hXM5jXezIki1cMI2.htm](ancestries/hXM5jXezIki1cMI2.htm)|Grippli|Grippli|oficial|
|[IiG7DgeLWYrSNXuX.htm](ancestries/IiG7DgeLWYrSNXuX.htm)|Human|Humano|oficial|
|[lSGWXjcbOa6O5fTx.htm](ancestries/lSGWXjcbOa6O5fTx.htm)|Orc|Orco|oficial|
|[P6PcVnCkh4XMdefw.htm](ancestries/P6PcVnCkh4XMdefw.htm)|Ratfolk|Rátido|oficial|
|[PgKmsA2aKdbLU6O0.htm](ancestries/PgKmsA2aKdbLU6O0.htm)|Elf|Elfo|oficial|
|[piNLXUrm9iaGqD2i.htm](ancestries/piNLXUrm9iaGqD2i.htm)|Hobgoblin|Hobgoblin|oficial|
|[sQfjTMDaZbT9DThq.htm](ancestries/sQfjTMDaZbT9DThq.htm)|Goblin|Goblin|oficial|
|[TQEqWqc7BYiadUdY.htm](ancestries/TQEqWqc7BYiadUdY.htm)|Anadi|Anadi|oficial|
|[TRqoeYfGAFjQbviF.htm](ancestries/TRqoeYfGAFjQbviF.htm)|Sprite|Hada|libre|
|[tZn4qIHCUA6wCdnI.htm](ancestries/tZn4qIHCUA6wCdnI.htm)|Conrasu|Conrasu|oficial|
|[vxbQ1Yw4qwgjTzqo.htm](ancestries/vxbQ1Yw4qwgjTzqo.htm)|Gnoll|Gnoll|oficial|
|[x1YinOddgUxwOLqP.htm](ancestries/x1YinOddgUxwOLqP.htm)|Shisk|Shisk|oficial|
