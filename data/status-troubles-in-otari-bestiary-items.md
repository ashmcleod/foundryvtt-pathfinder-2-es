# Estado de la traducción (troubles-in-otari-bestiary-items)

 * **modificada**: 78
 * **vacía**: 6
 * **ninguna**: 8


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[5519SZd2Gx7R0pso.htm](troubles-in-otari-bestiary-items/5519SZd2Gx7R0pso.htm)|Spellbook (Advanced Arcanomnemonics)|
|[fq5jsjB5HkWo1JYN.htm](troubles-in-otari-bestiary-items/fq5jsjB5HkWo1JYN.htm)|Scroll of Magic Weapon (Level 1)|
|[HWwf9OfDEQH53F11.htm](troubles-in-otari-bestiary-items/HWwf9OfDEQH53F11.htm)|Religious Symbol of Asmodeus (Wooden)|
|[jgcCZHnrNbS5iREn.htm](troubles-in-otari-bestiary-items/jgcCZHnrNbS5iREn.htm)|Area Map|
|[nDnP6GpXjTbXqbXB.htm](troubles-in-otari-bestiary-items/nDnP6GpXjTbXqbXB.htm)|Spike Trap|
|[VmpGQOac5GRmdaDj.htm](troubles-in-otari-bestiary-items/VmpGQOac5GRmdaDj.htm)|Dagger|+1|
|[wlSTHbfmM751AGNy.htm](troubles-in-otari-bestiary-items/wlSTHbfmM751AGNy.htm)|Invisibility (Self Only)|
|[xQTqtFFkEcJJWCDW.htm](troubles-in-otari-bestiary-items/xQTqtFFkEcJJWCDW.htm)|Wand of Fear (Level 1)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[088hXYj7GIHJtlm7.htm](troubles-in-otari-bestiary-items/088hXYj7GIHJtlm7.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[0gQuuCA2OyaQRNwR.htm](troubles-in-otari-bestiary-items/0gQuuCA2OyaQRNwR.htm)|Stench|Hedor|modificada|
|[0SzWuQBdiVOEj4WC.htm](troubles-in-otari-bestiary-items/0SzWuQBdiVOEj4WC.htm)|Shattering Urn|Urna que estalla|modificada|
|[19bgEIET0naweDE4.htm](troubles-in-otari-bestiary-items/19bgEIET0naweDE4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1on1nKLods5yCirm.htm](troubles-in-otari-bestiary-items/1on1nKLods5yCirm.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[1uUxlDMn7nj3yjAy.htm](troubles-in-otari-bestiary-items/1uUxlDMn7nj3yjAy.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[56wLde8SVEfys89N.htm](troubles-in-otari-bestiary-items/56wLde8SVEfys89N.htm)|Vine|Vid|modificada|
|[5v6fepRCSyg6ZABC.htm](troubles-in-otari-bestiary-items/5v6fepRCSyg6ZABC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[71j8rIykcKntxLJQ.htm](troubles-in-otari-bestiary-items/71j8rIykcKntxLJQ.htm)|Collapse|Colapso|modificada|
|[73fsCGNUERkE3I1S.htm](troubles-in-otari-bestiary-items/73fsCGNUERkE3I1S.htm)|Hypnotic Stare|Mirada hipnótica|modificada|
|[742oUZV30i6otpgL.htm](troubles-in-otari-bestiary-items/742oUZV30i6otpgL.htm)|Stinky Leaves|Hojas apestosas|modificada|
|[7TD5aX6vNauxTw5T.htm](troubles-in-otari-bestiary-items/7TD5aX6vNauxTw5T.htm)|Arcane Bond|Vínculo arcano|modificada|
|[8hva3s7KDSqeUTKh.htm](troubles-in-otari-bestiary-items/8hva3s7KDSqeUTKh.htm)|Dagger|Daga|modificada|
|[8mPErkHbf2ju6RgN.htm](troubles-in-otari-bestiary-items/8mPErkHbf2ju6RgN.htm)|Ferocity|Ferocidad|modificada|
|[9d61GjN8ciAoj4Z2.htm](troubles-in-otari-bestiary-items/9d61GjN8ciAoj4Z2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[9JzCRTSPmLBMHyll.htm](troubles-in-otari-bestiary-items/9JzCRTSPmLBMHyll.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9Nve1oYElRWcJhIh.htm](troubles-in-otari-bestiary-items/9Nve1oYElRWcJhIh.htm)|Unburdened Iron|No impedido por el hierro|modificada|
|[aADQJA0W9vAaOmXT.htm](troubles-in-otari-bestiary-items/aADQJA0W9vAaOmXT.htm)|Maul|Zarpazo doble|modificada|
|[ae0LOykLqNxqAXLN.htm](troubles-in-otari-bestiary-items/ae0LOykLqNxqAXLN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[b200ENWSAzXNlSMf.htm](troubles-in-otari-bestiary-items/b200ENWSAzXNlSMf.htm)|Battle Axe|Hacha de batalla|modificada|
|[BH97LqHtuqNlighQ.htm](troubles-in-otari-bestiary-items/BH97LqHtuqNlighQ.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[c6BnmJR85HoUuFbA.htm](troubles-in-otari-bestiary-items/c6BnmJR85HoUuFbA.htm)|Inflict Pain|Infligir Dolor|modificada|
|[ccosYJxMalM6rfN8.htm](troubles-in-otari-bestiary-items/ccosYJxMalM6rfN8.htm)|Spike Trap|Trampa de pinchos|modificada|
|[d5T3nJCRgqqqQvzD.htm](troubles-in-otari-bestiary-items/d5T3nJCRgqqqQvzD.htm)|Battle Cry|Grito de guerra|modificada|
|[di2qBg3DVoOSKzDk.htm](troubles-in-otari-bestiary-items/di2qBg3DVoOSKzDk.htm)|Unburdened Iron|No impedido por el hierro|modificada|
|[dwvQyKJ9gUWC4S3f.htm](troubles-in-otari-bestiary-items/dwvQyKJ9gUWC4S3f.htm)|Starknife Attack|Starknife Attack|modificada|
|[eIrvs73CKJq05yZP.htm](troubles-in-otari-bestiary-items/eIrvs73CKJq05yZP.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[Fges35T0kVykR3rl.htm](troubles-in-otari-bestiary-items/Fges35T0kVykR3rl.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Fk4R8F6Vr2Lr7mZZ.htm](troubles-in-otari-bestiary-items/Fk4R8F6Vr2Lr7mZZ.htm)|Step into Nightmares|Paso a las pesadillas|modificada|
|[gqjrPshyFW4Y4vrS.htm](troubles-in-otari-bestiary-items/gqjrPshyFW4Y4vrS.htm)|Javelin|Javelin|modificada|
|[H8wipabcGbcu7thF.htm](troubles-in-otari-bestiary-items/H8wipabcGbcu7thF.htm)|Waving Weed|Agitar hierbas|modificada|
|[hyIUDH9G8UlF3pne.htm](troubles-in-otari-bestiary-items/hyIUDH9G8UlF3pne.htm)|Knockdown|Derribo|modificada|
|[I0AZeY12ooiFLuuW.htm](troubles-in-otari-bestiary-items/I0AZeY12ooiFLuuW.htm)|Powerful Shove|Empujón poderoso|modificada|
|[I6Vb3uZRlv6pkiNj.htm](troubles-in-otari-bestiary-items/I6Vb3uZRlv6pkiNj.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[i74S7XGGuuWnr88J.htm](troubles-in-otari-bestiary-items/i74S7XGGuuWnr88J.htm)|Mauler|Vapulear|modificada|
|[Ihvi8o6Gm4pwbhph.htm](troubles-in-otari-bestiary-items/Ihvi8o6Gm4pwbhph.htm)|Fangs|Colmillos|modificada|
|[iIaK3retIHJkXgrh.htm](troubles-in-otari-bestiary-items/iIaK3retIHJkXgrh.htm)|Bushwhack|Emboscar|modificada|
|[Iq79xetoAoYDXkjs.htm](troubles-in-otari-bestiary-items/Iq79xetoAoYDXkjs.htm)|Darkvision|Visión en la oscuridad|modificada|
|[J08BqvSNQN7R2E9v.htm](troubles-in-otari-bestiary-items/J08BqvSNQN7R2E9v.htm)|Ferocity|Ferocidad|modificada|
|[k5kr2ILvlJNg5oKb.htm](troubles-in-otari-bestiary-items/k5kr2ILvlJNg5oKb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[l8f0B8czvLKLsc06.htm](troubles-in-otari-bestiary-items/l8f0B8czvLKLsc06.htm)|Web Noose|Web Noose|modificada|
|[ll1kYssjNZ4S5vnd.htm](troubles-in-otari-bestiary-items/ll1kYssjNZ4S5vnd.htm)|Brutish Shove|Empujón brutal|modificada|
|[lLR6EoZxZTWifZUj.htm](troubles-in-otari-bestiary-items/lLR6EoZxZTWifZUj.htm)|Maul|Zarpazo doble|modificada|
|[lykdG2IwAxNTGqQo.htm](troubles-in-otari-bestiary-items/lykdG2IwAxNTGqQo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MKiOmYkknzLyP0Am.htm](troubles-in-otari-bestiary-items/MKiOmYkknzLyP0Am.htm)|Javelin|Javelin|modificada|
|[Nc23oEL8srJURx0P.htm](troubles-in-otari-bestiary-items/Nc23oEL8srJURx0P.htm)|Remove Memory|Borrar memoria|modificada|
|[NoYQle3HJatFQcgH.htm](troubles-in-otari-bestiary-items/NoYQle3HJatFQcgH.htm)|Noose|Lazo|modificada|
|[nUWlISx8ev0qZpTG.htm](troubles-in-otari-bestiary-items/nUWlISx8ev0qZpTG.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[O9himtaJvrfCALhE.htm](troubles-in-otari-bestiary-items/O9himtaJvrfCALhE.htm)|Magical Starknife|Magical Starknife|modificada|
|[OOCu4hNQg4NvyXLU.htm](troubles-in-otari-bestiary-items/OOCu4hNQg4NvyXLU.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[pgQRipY9qrXPcNlY.htm](troubles-in-otari-bestiary-items/pgQRipY9qrXPcNlY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qEbFimPOeE5aFvE7.htm](troubles-in-otari-bestiary-items/qEbFimPOeE5aFvE7.htm)|Dagger|Daga|modificada|
|[QfnsBk0XQ6x9wJSe.htm](troubles-in-otari-bestiary-items/QfnsBk0XQ6x9wJSe.htm)|Quick Trap|Trampa Rápida|modificada|
|[QfWg8mYxqbJly84b.htm](troubles-in-otari-bestiary-items/QfWg8mYxqbJly84b.htm)|Shortsword|Espada corta|modificada|
|[qunm5Cfit6Q1T4dI.htm](troubles-in-otari-bestiary-items/qunm5Cfit6Q1T4dI.htm)|Wing Flash|Alas cegadoras|modificada|
|[QVjozo4fqxCzzccc.htm](troubles-in-otari-bestiary-items/QVjozo4fqxCzzccc.htm)|Rapier|Estoque|modificada|
|[QZb6NNceGC3aMTMO.htm](troubles-in-otari-bestiary-items/QZb6NNceGC3aMTMO.htm)|Staff|Báculo|modificada|
|[STheFAvLWD42KLo9.htm](troubles-in-otari-bestiary-items/STheFAvLWD42KLo9.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[symt5PZrNoJWeIqe.htm](troubles-in-otari-bestiary-items/symt5PZrNoJWeIqe.htm)|Fist|Puño|modificada|
|[u4infcHTr2ku9Uxi.htm](troubles-in-otari-bestiary-items/u4infcHTr2ku9Uxi.htm)|Spear|Lanza|modificada|
|[uGR730eqkGcdJEfo.htm](troubles-in-otari-bestiary-items/uGR730eqkGcdJEfo.htm)|Javelin|Javelin|modificada|
|[UkurqY6wQGrS67eI.htm](troubles-in-otari-bestiary-items/UkurqY6wQGrS67eI.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[UMd6rwUlzKFS3lvJ.htm](troubles-in-otari-bestiary-items/UMd6rwUlzKFS3lvJ.htm)|Javelin|Javelin|modificada|
|[VePco7JeJmUM7ijK.htm](troubles-in-otari-bestiary-items/VePco7JeJmUM7ijK.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Vkudm63q6gEmBgGp.htm](troubles-in-otari-bestiary-items/Vkudm63q6gEmBgGp.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vlVKxFijTHI7cMPh.htm](troubles-in-otari-bestiary-items/vlVKxFijTHI7cMPh.htm)|Necromancer|Nigromante|modificada|
|[W6XkoyVR7QH9Z8lg.htm](troubles-in-otari-bestiary-items/W6XkoyVR7QH9Z8lg.htm)|Necromantic Defense|Necromantic Defense|modificada|
|[wBuoAb6EXYtmwPmo.htm](troubles-in-otari-bestiary-items/wBuoAb6EXYtmwPmo.htm)|Longsword|Longsword|modificada|
|[Wigs6zeZoSmogj8P.htm](troubles-in-otari-bestiary-items/Wigs6zeZoSmogj8P.htm)|Seedpod|Vaina de semillas|modificada|
|[WqgfgkwqUVMJK44b.htm](troubles-in-otari-bestiary-items/WqgfgkwqUVMJK44b.htm)|Jaws|Fauces|modificada|
|[wyzsbdPo28UWUYTD.htm](troubles-in-otari-bestiary-items/wyzsbdPo28UWUYTD.htm)|Unburdened Iron|No impedido por el hierro|modificada|
|[YB4lJDTFGnMJZxPX.htm](troubles-in-otari-bestiary-items/YB4lJDTFGnMJZxPX.htm)|Jaws|Fauces|modificada|
|[YhlP3I6uuAc1wEik.htm](troubles-in-otari-bestiary-items/YhlP3I6uuAc1wEik.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[z7UOnDRw58SSvHLY.htm](troubles-in-otari-bestiary-items/z7UOnDRw58SSvHLY.htm)|Fist|Puño|modificada|
|[ZhDg0uKQhEzdCReZ.htm](troubles-in-otari-bestiary-items/ZhDg0uKQhEzdCReZ.htm)|Fist|Puño|modificada|
|[zhR9Ys8AyYNjAz29.htm](troubles-in-otari-bestiary-items/zhR9Ys8AyYNjAz29.htm)|Arcane Focus Spells|Conjuros de foco arcano|modificada|
|[zNOKH6cGU4FaspAX.htm](troubles-in-otari-bestiary-items/zNOKH6cGU4FaspAX.htm)|Woodland Stride|Paso forestal|modificada|
|[ZxSGALYRVWtMBLTi.htm](troubles-in-otari-bestiary-items/ZxSGALYRVWtMBLTi.htm)|Darkvision|Visión en la oscuridad|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[2uDTTLnPGGNUafjb.htm](troubles-in-otari-bestiary-items/2uDTTLnPGGNUafjb.htm)|Fiend Lore|vacía|
|[8kScozUBrCo4DJdt.htm](troubles-in-otari-bestiary-items/8kScozUBrCo4DJdt.htm)|Athletics|vacía|
|[ahXVih7AtAIrRV01.htm](troubles-in-otari-bestiary-items/ahXVih7AtAIrRV01.htm)|Demon Lore|vacía|
|[OoNcWTdHVJ69lAop.htm](troubles-in-otari-bestiary-items/OoNcWTdHVJ69lAop.htm)|Diplomacy|vacía|
|[OW9XOD4ssqRZAZk2.htm](troubles-in-otari-bestiary-items/OW9XOD4ssqRZAZk2.htm)|Diplomacy|vacía|
|[Rocjsria9JUsNrFA.htm](troubles-in-otari-bestiary-items/Rocjsria9JUsNrFA.htm)|Athletics|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
