# Estado de la traducción (vehicles)

 * **modificada**: 48


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0H2BbJCAKauXYKE6.htm](vehicles/0H2BbJCAKauXYKE6.htm)|Sand Barge|Barcaza de Arena|modificada|
|[1ldIHIoe1aKHilX2.htm](vehicles/1ldIHIoe1aKHilX2.htm)|Sandsailer|Sandsailer|modificada|
|[2dLy1sfqVbNS2inJ.htm](vehicles/2dLy1sfqVbNS2inJ.htm)|Firefly|Luciérnaga|modificada|
|[66dAXEMUjL7BrX8I.htm](vehicles/66dAXEMUjL7BrX8I.htm)|Carriage|Carruaje|modificada|
|[6CFijrzNSXtS8ExJ.htm](vehicles/6CFijrzNSXtS8ExJ.htm)|Sky Chariot, Light|Carro Celeste, Luz|modificada|
|[7ye7abxWAFdVbATr.htm](vehicles/7ye7abxWAFdVbATr.htm)|Chariot, Heavy|Carro, Pesado|modificada|
|[bMgQftCEiSqa1pTk.htm](vehicles/bMgQftCEiSqa1pTk.htm)|Vonthos's Golden Bridge|Puente de Oro de Vonthos|modificada|
|[bUwBenWXjdUIKXkm.htm](vehicles/bUwBenWXjdUIKXkm.htm)|Clockwork Borer|Clockwork Borer|modificada|
|[cmLOaqR3qebmLA1P.htm](vehicles/cmLOaqR3qebmLA1P.htm)|Clockwork Bumblebee|Abejorro Mecánico|modificada|
|[Cr4T0TcaxMr3bUaj.htm](vehicles/Cr4T0TcaxMr3bUaj.htm)|Speedster|Speedster|modificada|
|[cvP9ytBY9SJbaNbo.htm](vehicles/cvP9ytBY9SJbaNbo.htm)|Apparatus of the Octopus|Aparato del Pulpo|modificada|
|[FbSRhHvCmXBSVkrn.htm](vehicles/FbSRhHvCmXBSVkrn.htm)|Steam Cart|Carro de vapor|modificada|
|[fiSvQBWyyJuXnVvn.htm](vehicles/fiSvQBWyyJuXnVvn.htm)|Automated Cycle|Ciclo Automatizado|modificada|
|[fUJ1RB9l9DppWdCH.htm](vehicles/fUJ1RB9l9DppWdCH.htm)|Titanic Stomper|Titanic Stomper|modificada|
|[GMoxs9RQ88uMxfW4.htm](vehicles/GMoxs9RQ88uMxfW4.htm)|Bathysphere|Batisfera|modificada|
|[HA93YJ0qjmRk0Nh8.htm](vehicles/HA93YJ0qjmRk0Nh8.htm)|Galley|Galera|modificada|
|[IhniEmlnkb3pFRZG.htm](vehicles/IhniEmlnkb3pFRZG.htm)|Clockwork Hopper|Tolva Mecánica|modificada|
|[JQAknpXhJo5bwgOM.htm](vehicles/JQAknpXhJo5bwgOM.htm)|Strider|Strider|modificada|
|[jrLiGrOwgOhr83PL.htm](vehicles/jrLiGrOwgOhr83PL.htm)|Clockwork Castle|Castillo Mecánico|modificada|
|[K0ThJViHQzzxRmqE.htm](vehicles/K0ThJViHQzzxRmqE.htm)|Second Kiss|Segundo Beso|modificada|
|[l0RfYgabnbNoHnkS.htm](vehicles/l0RfYgabnbNoHnkS.htm)|Rowboat|Bote de remos|modificada|
|[MCU4ZiNNAX7GQNJb.htm](vehicles/MCU4ZiNNAX7GQNJb.htm)|Firework Pogo|Firework Pogo|modificada|
|[mq8bsMnrY81gUEYb.htm](vehicles/mq8bsMnrY81gUEYb.htm)|Cart|Carro|modificada|
|[MrGgl8HDfQYITahL.htm](vehicles/MrGgl8HDfQYITahL.htm)|Clockwork Wagon|Vagón Mecánico|modificada|
|[oq6khbojns3YRi2g.htm](vehicles/oq6khbojns3YRi2g.htm)|Cliff Crawler|Oruga del acantilado|modificada|
|[oyKhe8J0h8G9IHE3.htm](vehicles/oyKhe8J0h8G9IHE3.htm)|Glider|Planeador|modificada|
|[Qlvei5jjRXdsXUPo.htm](vehicles/Qlvei5jjRXdsXUPo.htm)|Cutter|Cutter|modificada|
|[QMPVAWtaClZtD2Ip.htm](vehicles/QMPVAWtaClZtD2Ip.htm)|Shark Diver|Buzo Tiburón|modificada|
|[R2ga1eyeSQA9w6KF.htm](vehicles/R2ga1eyeSQA9w6KF.htm)|Sailing Ship|Velero|modificada|
|[RGEmINZa7YXvbruu.htm](vehicles/RGEmINZa7YXvbruu.htm)|Clunkerjunker|Clunkerjunker|modificada|
|[slkeDT68Zjmzz5l5.htm](vehicles/slkeDT68Zjmzz5l5.htm)|Steam Trolley|Carro de vapor|modificada|
|[t57KE8cj9MdG6Y5H.htm](vehicles/t57KE8cj9MdG6Y5H.htm)|Bone Ship|Barco de hueso|modificada|
|[tVBApIWuwKNwOics.htm](vehicles/tVBApIWuwKNwOics.htm)|Steam Giant|Gigante de vapor|modificada|
|[UNOwkQq5lc4cYdZP.htm](vehicles/UNOwkQq5lc4cYdZP.htm)|Wagon|Vagón|modificada|
|[V6ZuxECTGSyHAm7h.htm](vehicles/V6ZuxECTGSyHAm7h.htm)|Hillcross Glider|Planeador Hillcross|modificada|
|[vbJL4dPQcXfYigwv.htm](vehicles/vbJL4dPQcXfYigwv.htm)|Sky Chariot, Medium|Sky Chariot, Medium|modificada|
|[VNlJ7zPnOPThFEmR.htm](vehicles/VNlJ7zPnOPThFEmR.htm)|Airship|Dirigible|modificada|
|[VRhdfL78HS5sT4gF.htm](vehicles/VRhdfL78HS5sT4gF.htm)|Snail Coach|Caracol Entrenador|modificada|
|[vU3UnKWCRFhnvNLV.htm](vehicles/vU3UnKWCRFhnvNLV.htm)|Ambling Surveyor|Ambling Surveyor|modificada|
|[w6auGNQQ2VyMT001.htm](vehicles/w6auGNQQ2VyMT001.htm)|Sand Diver|Buzo de Arena|modificada|
|[WBMeO6BPqEzc6Yv8.htm](vehicles/WBMeO6BPqEzc6Yv8.htm)|Chariot, Light|Carro, Luz|modificada|
|[WjOkOaWOGkLmuSq5.htm](vehicles/WjOkOaWOGkLmuSq5.htm)|Armored Carriage|Carro blindado|modificada|
|[WsfgcVXqqmk1EVRY.htm](vehicles/WsfgcVXqqmk1EVRY.htm)|Velocipede|Velocipede|modificada|
|[WWvqHSAYOw4ysn3e.htm](vehicles/WWvqHSAYOw4ysn3e.htm)|Sleigh|Trineo|modificada|
|[WXwFZ3DrLe2PchNf.htm](vehicles/WXwFZ3DrLe2PchNf.htm)|Cauldron of Flying|Caldero de Volar|modificada|
|[Y2XrhWFdiGcVAUsz.htm](vehicles/Y2XrhWFdiGcVAUsz.htm)|Sky Chariot, Heavy|Carro Celeste, Pesado|modificada|
|[ySR1Ds607BSX1tCw.htm](vehicles/ySR1Ds607BSX1tCw.htm)|Adaptable Paddleboat|Paddleboat adaptable|modificada|
|[yXbxHtz3xVIMbxyh.htm](vehicles/yXbxHtz3xVIMbxyh.htm)|Mobile Inn|Posada Movilidad|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
