# Estado de la traducción (vehicles-items)

 * **modificada**: 75


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0CcYSQlAiZcoMkF0.htm](vehicles-items/0CcYSQlAiZcoMkF0.htm)|Skewering Step|Ensartar Paso|modificada|
|[0pWllu1H0c2ZTJbM.htm](vehicles-items/0pWllu1H0c2ZTJbM.htm)|Hauler|Hauler|modificada|
|[2UISHdYECuDVkedh.htm](vehicles-items/2UISHdYECuDVkedh.htm)|Sluggish|Sluggish|modificada|
|[38Zgv0sAfLXH2dfB.htm](vehicles-items/38Zgv0sAfLXH2dfB.htm)|Wind-Up|Wind-Up|modificada|
|[3Vt9tFjjJmSsJ99w.htm](vehicles-items/3Vt9tFjjJmSsJ99w.htm)|Wind-Up|Wind-Up|modificada|
|[3WQ9vWhVa2yyJCcV.htm](vehicles-items/3WQ9vWhVa2yyJCcV.htm)|Luxurious Accommodations|Alojamiento de lujo|modificada|
|[79YskaB7D1bAmIDD.htm](vehicles-items/79YskaB7D1bAmIDD.htm)|Volatile Flamethrower|Lanzallamas Volátil|modificada|
|[7LOtkjkF6S8p6CRA.htm](vehicles-items/7LOtkjkF6S8p6CRA.htm)|Tether Buoy|Boya de amarre|modificada|
|[7OqKezdHChnhLvmc.htm](vehicles-items/7OqKezdHChnhLvmc.htm)|Sluggish|Sluggish|modificada|
|[9UcPAPPrzEhaoWgY.htm](vehicles-items/9UcPAPPrzEhaoWgY.htm)|Portaged|Portaged|modificada|
|[b3OQJLYv4hEX7yhn.htm](vehicles-items/b3OQJLYv4hEX7yhn.htm)|Long Reach|Largo Alcance|modificada|
|[Buxx8NC3JQzeLsjs.htm](vehicles-items/Buxx8NC3JQzeLsjs.htm)|Sluggish|Sluggish|modificada|
|[cFwKXyV5W3o1v3KK.htm](vehicles-items/cFwKXyV5W3o1v3KK.htm)|Adhesive Secretions|Secreciones Adhesivas|modificada|
|[ChjOaa11CkBJeVJ8.htm](vehicles-items/ChjOaa11CkBJeVJ8.htm)|Glow|Glow|modificada|
|[dabct2Mb2XcbWKwb.htm](vehicles-items/dabct2Mb2XcbWKwb.htm)|Maneuverable|Maniobrable|modificada|
|[DoCusD26UNQbmFNI.htm](vehicles-items/DoCusD26UNQbmFNI.htm)|Continous Track|Rastrear continuamente|modificada|
|[eMf9xcynjFjF5ma8.htm](vehicles-items/eMf9xcynjFjF5ma8.htm)|Wind-Up|Wind-Up|modificada|
|[eXdF4IskaccqgTpS.htm](vehicles-items/eXdF4IskaccqgTpS.htm)|Survey|Encuesta|modificada|
|[fG2IE4D8GEe8e98D.htm](vehicles-items/fG2IE4D8GEe8e98D.htm)|Hover|Hover|modificada|
|[gafl9oZCmfSGtfxj.htm](vehicles-items/gafl9oZCmfSGtfxj.htm)|Hopper|Tolva|modificada|
|[Gy4AuNFAGBzfjRFq.htm](vehicles-items/Gy4AuNFAGBzfjRFq.htm)|Smog|Smog|modificada|
|[hCwx6Q30DbNVZa88.htm](vehicles-items/hCwx6Q30DbNVZa88.htm)|Sluggish|Sluggish|modificada|
|[iU2xj2sIMM3UdezU.htm](vehicles-items/iU2xj2sIMM3UdezU.htm)|Speed Boost|Aumento de Velocidad|modificada|
|[Ix4GHIqSUqHawiG8.htm](vehicles-items/Ix4GHIqSUqHawiG8.htm)|Starting Drop|Starting Drop|modificada|
|[izZwMB8zgdsWje9o.htm](vehicles-items/izZwMB8zgdsWje9o.htm)|Prismatic Defense|Defensa Prismática|modificada|
|[jUfFzSzu0PIwCSgB.htm](vehicles-items/jUfFzSzu0PIwCSgB.htm)|Mountain Traverser|Mountain Traverser|modificada|
|[jv3XUW2upC7aFRtR.htm](vehicles-items/jv3XUW2upC7aFRtR.htm)|Open Eyes|Ojos Abiertos|modificada|
|[kbVUEpQsOKNyw0BS.htm](vehicles-items/kbVUEpQsOKNyw0BS.htm)|Steam Cloud|Nube de vapor|modificada|
|[KFSpJiu6uoHoZl1b.htm](vehicles-items/KFSpJiu6uoHoZl1b.htm)|Manipulate Hands|Manipular Manos|modificada|
|[kQbyDtF75cOAr2Lc.htm](vehicles-items/kQbyDtF75cOAr2Lc.htm)|Wind-Up|Wind-Up|modificada|
|[kr1leAaQlGXDTwn6.htm](vehicles-items/kr1leAaQlGXDTwn6.htm)|Wind-Up|Wind-Up|modificada|
|[kt9xSEJnhJNdCBEy.htm](vehicles-items/kt9xSEJnhJNdCBEy.htm)|Captivating Wealth|Captivating Wealth|modificada|
|[L0S2jaWemV8ndKIE.htm](vehicles-items/L0S2jaWemV8ndKIE.htm)|Sidecars|Sidecars|modificada|
|[L5LOLKdKxPBT5LCB.htm](vehicles-items/L5LOLKdKxPBT5LCB.htm)|Iron Rim|Iron Rim|modificada|
|[lctMsuE0KxKUZgnH.htm](vehicles-items/lctMsuE0KxKUZgnH.htm)|Wind-Up|Wind-Up|modificada|
|[lTX8qPMdwaThuCnc.htm](vehicles-items/lTX8qPMdwaThuCnc.htm)|Quaking Step|Paso Tembloroso|modificada|
|[MBdI0c7xv3ufaJM8.htm](vehicles-items/MBdI0c7xv3ufaJM8.htm)|Environmental Protections|Protección del entorno|modificada|
|[mTjyYbkFbVeKoGO8.htm](vehicles-items/mTjyYbkFbVeKoGO8.htm)|Submersible|Sumergible|modificada|
|[O5JvDSJWNWG2VtpR.htm](vehicles-items/O5JvDSJWNWG2VtpR.htm)|Electrify|Electrify|modificada|
|[oWlyE6XYOLX2gBB6.htm](vehicles-items/oWlyE6XYOLX2gBB6.htm)|Weapon Mount|Arma Mount|modificada|
|[PcXltLxBnG3FD8dH.htm](vehicles-items/PcXltLxBnG3FD8dH.htm)|Portable|Portable|modificada|
|[PqsmKTcXOM0aNYo8.htm](vehicles-items/PqsmKTcXOM0aNYo8.htm)|Shard Trail|Shard Trail|modificada|
|[QUHIpCfam8YMHy6U.htm](vehicles-items/QUHIpCfam8YMHy6U.htm)|Electrical Absorption|Absorción Eléctrica|modificada|
|[QwXzg5zGAZg4h78t.htm](vehicles-items/QwXzg5zGAZg4h78t.htm)|Fragile|Frágil|modificada|
|[Qx3tJuimyRKT7l2W.htm](vehicles-items/Qx3tJuimyRKT7l2W.htm)|Cable|Cable|modificada|
|[R2SaS8szVUWwiHQr.htm](vehicles-items/R2SaS8szVUWwiHQr.htm)|Streamlined|Streamlined|modificada|
|[R4Co971n6x5ZNJHd.htm](vehicles-items/R4Co971n6x5ZNJHd.htm)|Wind-Up|Wind-Up|modificada|
|[Rb4MyOLlodPUZdJX.htm](vehicles-items/Rb4MyOLlodPUZdJX.htm)|Flame Jet|Flame Jet|modificada|
|[rDwfQJPnK5YyRx7v.htm](vehicles-items/rDwfQJPnK5YyRx7v.htm)|Sluggish|Sluggish|modificada|
|[RJf32Bczb9zQaFqs.htm](vehicles-items/RJf32Bczb9zQaFqs.htm)|Sluggish|Sluggish|modificada|
|[rvLRBCsFQMdUKtar.htm](vehicles-items/rvLRBCsFQMdUKtar.htm)|Sand Skimmer|Sand Skimmer|modificada|
|[scXXuno9YNm1GWXK.htm](vehicles-items/scXXuno9YNm1GWXK.htm)|Pivoting Seats|Asientos pivotantes|modificada|
|[tFb8nV3bxHVLeU8Z.htm](vehicles-items/tFb8nV3bxHVLeU8Z.htm)|Maneuverable|Maniobrable|modificada|
|[TMYxeMEQ1ilLoAlJ.htm](vehicles-items/TMYxeMEQ1ilLoAlJ.htm)|Wind Up|Wind Up|modificada|
|[TPj4AFDezXubgaJm.htm](vehicles-items/TPj4AFDezXubgaJm.htm)|Adamantine Drill|Taladro Adamantino|modificada|
|[tS9MnEsewMslgzhi.htm](vehicles-items/tS9MnEsewMslgzhi.htm)|Wind-Up|Wind-Up|modificada|
|[uoev6dRtG1YFWPrn.htm](vehicles-items/uoev6dRtG1YFWPrn.htm)|Open Portholes|Apertura de ojos de buey|modificada|
|[uP38cyn9BsX8ni0O.htm](vehicles-items/uP38cyn9BsX8ni0O.htm)|Massive Jump|Salto masivo|modificada|
|[upS8Tctv0gf8rQOC.htm](vehicles-items/upS8Tctv0gf8rQOC.htm)|Ballast Release|Soltar lastre|modificada|
|[UQr6y2RUCvhlcvZ3.htm](vehicles-items/UQr6y2RUCvhlcvZ3.htm)|Protective Barrier|Barrera de protección|modificada|
|[UZrxxtljdLoLewB9.htm](vehicles-items/UZrxxtljdLoLewB9.htm)|Deploy Wheels|Desplegar Ruedas|modificada|
|[V0HecEvj92RlltT5.htm](vehicles-items/V0HecEvj92RlltT5.htm)|Sluggish|Sluggish|modificada|
|[V0Iw3ZOvbTqoxlvz.htm](vehicles-items/V0Iw3ZOvbTqoxlvz.htm)|Jump Jet|Jump Jet|modificada|
|[V86H5yeH71jmelzY.htm](vehicles-items/V86H5yeH71jmelzY.htm)|Submersible|Sumergible|modificada|
|[vDMm6mf0S2i9JMzd.htm](vehicles-items/vDMm6mf0S2i9JMzd.htm)|Bolt Blast|Ráfaga de Pernos|modificada|
|[VqS08x8SzApqcmVy.htm](vehicles-items/VqS08x8SzApqcmVy.htm)|Weapon Mounts|Monturas de armas|modificada|
|[W01aasqmTJL5TpkW.htm](vehicles-items/W01aasqmTJL5TpkW.htm)|Unstable Launch|Lanzamiento inestable|modificada|
|[xB0A9RW1Zi6TC7g0.htm](vehicles-items/xB0A9RW1Zi6TC7g0.htm)|Starting Drop|Starting Drop|modificada|
|[XLttadhtgYoOq6f1.htm](vehicles-items/XLttadhtgYoOq6f1.htm)|Flash|Flash|modificada|
|[XvCWEJlIIDSfz2EA.htm](vehicles-items/XvCWEJlIIDSfz2EA.htm)|Ice Traverser|Ice Traverser|modificada|
|[yKd1wGx6zmlBwk0u.htm](vehicles-items/yKd1wGx6zmlBwk0u.htm)|Fall Apart|Fall Apart|modificada|
|[Yr2rxvC8Hzmb70KZ.htm](vehicles-items/Yr2rxvC8Hzmb70KZ.htm)|Sluggish|Sluggish|modificada|
|[zEf7XS6RlEjM0VSO.htm](vehicles-items/zEf7XS6RlEjM0VSO.htm)|Wind-Up|Wind-Up|modificada|
|[zGMODH6r1X3ZbwB1.htm](vehicles-items/zGMODH6r1X3ZbwB1.htm)|Sluggish|Sluggish|modificada|
|[zni42kvDwzT6lzuW.htm](vehicles-items/zni42kvDwzT6lzuW.htm)|Wind-Up|Wind-Up|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
