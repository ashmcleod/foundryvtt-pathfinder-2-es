# Estado de la traducción (abomination-vaults-bestiary-items)

 * **modificada**: 850
 * **ninguna**: 106
 * **vacía**: 48


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[01oCBNgr4LTyuncP.htm](abomination-vaults-bestiary-items/01oCBNgr4LTyuncP.htm)|Infused Acid Flask (Moderate)|
|[0gD4v7579cLZZRwx.htm](abomination-vaults-bestiary-items/0gD4v7579cLZZRwx.htm)|+1 Striking Handwraps of Mighty Blows|+1,striking|
|[0mexpNYfz6rpTLXa.htm](abomination-vaults-bestiary-items/0mexpNYfz6rpTLXa.htm)|Kukri|+1,striking|
|[0siiEWVFxVLV5eB0.htm](abomination-vaults-bestiary-items/0siiEWVFxVLV5eB0.htm)|Obscuring Mist (At Will)|
|[1M3Chy2EO0WvLGrF.htm](abomination-vaults-bestiary-items/1M3Chy2EO0WvLGrF.htm)|Darkness (At Will)|
|[2CDZBmJ8jMDA4sZk.htm](abomination-vaults-bestiary-items/2CDZBmJ8jMDA4sZk.htm)|Feather Fall (At Will) (Self Only)|
|[384uraaz7360Y99M.htm](abomination-vaults-bestiary-items/384uraaz7360Y99M.htm)|Nyzuros's Keys|
|[5FuqfP9EivTFAxuL.htm](abomination-vaults-bestiary-items/5FuqfP9EivTFAxuL.htm)|Soul Gem|
|[5ghMpQ8FJkhKBa0c.htm](abomination-vaults-bestiary-items/5ghMpQ8FJkhKBa0c.htm)|Darkness (At Will)|
|[5jRdb5HrdzxGCFRq.htm](abomination-vaults-bestiary-items/5jRdb5HrdzxGCFRq.htm)|Faerie Fire (At Will)|
|[5UlLFoV3FkK9X6Xn.htm](abomination-vaults-bestiary-items/5UlLFoV3FkK9X6Xn.htm)|True Seeing (Constant)|
|[5yALvKiWzaN3usex.htm](abomination-vaults-bestiary-items/5yALvKiWzaN3usex.htm)|Darkness (At Will)|
|[5ZtyVQVWawYV83pF.htm](abomination-vaults-bestiary-items/5ZtyVQVWawYV83pF.htm)|Feather Fall (At Will) (Self Only)|
|[5ZWQSqoOaC6KzTtA.htm](abomination-vaults-bestiary-items/5ZWQSqoOaC6KzTtA.htm)|Darkness (At Will)|
|[6sz5Y1Ptd0vhrLiM.htm](abomination-vaults-bestiary-items/6sz5Y1Ptd0vhrLiM.htm)|+1 Striking Corrosive Longsword|+1,striking,corrosive|
|[72l4ViSD8vfFJXpK.htm](abomination-vaults-bestiary-items/72l4ViSD8vfFJXpK.htm)|Battle Lute|+1,striking|
|[7JPnTGzj40aMM3Hk.htm](abomination-vaults-bestiary-items/7JPnTGzj40aMM3Hk.htm)|Wand of Status (Level 2)|
|[90JhVU3ps4TG9jNw.htm](abomination-vaults-bestiary-items/90JhVU3ps4TG9jNw.htm)|Faerie Fire (At Will)|
|[94npIYAEHcak0YRt.htm](abomination-vaults-bestiary-items/94npIYAEHcak0YRt.htm)|+1 Striking Kukri|+1,striking|
|[A5aJvo6ma9oQG3Hh.htm](abomination-vaults-bestiary-items/A5aJvo6ma9oQG3Hh.htm)|Dimension Door (At Will)|
|[aV7tH63nZZOcXqzb.htm](abomination-vaults-bestiary-items/aV7tH63nZZOcXqzb.htm)|Tongues (Constant)|
|[BEAgqEuzuu4pSB1t.htm](abomination-vaults-bestiary-items/BEAgqEuzuu4pSB1t.htm)|Infused Mistform Elixir (Moderate)|
|[BeVACGeSdq4dZpvw.htm](abomination-vaults-bestiary-items/BeVACGeSdq4dZpvw.htm)|Darkness (At Will)|
|[BkBEhqLM3mXNI13M.htm](abomination-vaults-bestiary-items/BkBEhqLM3mXNI13M.htm)|Dimension Door (At Will)|
|[c0PlxHB3aGqwm9qD.htm](abomination-vaults-bestiary-items/c0PlxHB3aGqwm9qD.htm)|Faerie Fire (At Will)|
|[c9PJ35ELMYksO3fR.htm](abomination-vaults-bestiary-items/c9PJ35ELMYksO3fR.htm)|Faerie Fire (at will)|
|[cNM2OqWLz3u5w7ai.htm](abomination-vaults-bestiary-items/cNM2OqWLz3u5w7ai.htm)|Wand of Gentle Repose (Level 2)|
|[Cs1TEn4HJrImL292.htm](abomination-vaults-bestiary-items/Cs1TEn4HJrImL292.htm)|War Flail|+1,striking|
|[d4M2oXHQpDvC4Crx.htm](abomination-vaults-bestiary-items/d4M2oXHQpDvC4Crx.htm)|Darkness (At Will)|
|[dD1t8c61JZZMf4Qo.htm](abomination-vaults-bestiary-items/dD1t8c61JZZMf4Qo.htm)|Scrying (At Will) (See Right of Inspection)|
|[E952Mn29Vyncndfi.htm](abomination-vaults-bestiary-items/E952Mn29Vyncndfi.htm)|Katar|+1,striking|
|[ediMsqeJ8TKtfzcR.htm](abomination-vaults-bestiary-items/ediMsqeJ8TKtfzcR.htm)|Darkness (At Will)|
|[ExTn8RH5Xp1BVvJk.htm](abomination-vaults-bestiary-items/ExTn8RH5Xp1BVvJk.htm)|Illusory Disguise (At Will)|
|[faxsM9HLLEPrf5fu.htm](abomination-vaults-bestiary-items/faxsM9HLLEPrf5fu.htm)|Wand of Magic Missile (Level 3)|
|[FgeH6g4PelOU0Npx.htm](abomination-vaults-bestiary-items/FgeH6g4PelOU0Npx.htm)|Tamchal Chakram|+1|
|[fL540WJoPfl2yEVm.htm](abomination-vaults-bestiary-items/fL540WJoPfl2yEVm.htm)|Darkness (at will)|
|[fpyaGQ7nPlsCH1nc.htm](abomination-vaults-bestiary-items/fpyaGQ7nPlsCH1nc.htm)|True Seeing (Constant)|
|[G0WFBGJRBnL3fapV.htm](abomination-vaults-bestiary-items/G0WFBGJRBnL3fapV.htm)|Composite Shortbow|+1|
|[GMWiXA8zpcvJD4Fp.htm](abomination-vaults-bestiary-items/GMWiXA8zpcvJD4Fp.htm)|Darkness (At Will)|
|[H3obBC81SMnIiDVI.htm](abomination-vaults-bestiary-items/H3obBC81SMnIiDVI.htm)|Rapier|+1,striking|
|[H44u0Lp3c8QPlUiB.htm](abomination-vaults-bestiary-items/H44u0Lp3c8QPlUiB.htm)|Obscuring Mist (At Will)|
|[Ir7VUc4vTn1TT3EX.htm](abomination-vaults-bestiary-items/Ir7VUc4vTn1TT3EX.htm)|100 feet of Erinys-Hair Rope|
|[IvhwAKjsxJV7jjkI.htm](abomination-vaults-bestiary-items/IvhwAKjsxJV7jjkI.htm)|Battle Axe|+1|
|[J0lZcblrLKOLNSZZ.htm](abomination-vaults-bestiary-items/J0lZcblrLKOLNSZZ.htm)|Occultism|
|[j2rbPzfS6n5EwSzO.htm](abomination-vaults-bestiary-items/j2rbPzfS6n5EwSzO.htm)|Cooperative Blade|+1,striking|
|[j5nOoy2JdxGbg96d.htm](abomination-vaults-bestiary-items/j5nOoy2JdxGbg96d.htm)|Dimension Door (At Will)|
|[ja1vr0cUJvPastvq.htm](abomination-vaults-bestiary-items/ja1vr0cUJvPastvq.htm)|Speak with Animals (At Will) (Arthropods Only)|
|[jbA5MFmKFD5CdN9h.htm](abomination-vaults-bestiary-items/jbA5MFmKFD5CdN9h.htm)|+1 Studded Leather Armor|
|[JFcLFPpOrpJo01np.htm](abomination-vaults-bestiary-items/JFcLFPpOrpJo01np.htm)|Feather Fall (At Will) (Self Only)|
|[jiUi7cG1ldGKfHsM.htm](abomination-vaults-bestiary-items/jiUi7cG1ldGKfHsM.htm)|+1 Flaming Striking Hellforged Glaive|+1,striking,flaming|
|[JLbHt0LM9WGhFhGj.htm](abomination-vaults-bestiary-items/JLbHt0LM9WGhFhGj.htm)|Summon Animal (Swarm creatures only)|
|[jTap9wmrEvrgaFTi.htm](abomination-vaults-bestiary-items/jTap9wmrEvrgaFTi.htm)|Darkness (At Will)|
|[kqpPg4HkBr9iLaWM.htm](abomination-vaults-bestiary-items/kqpPg4HkBr9iLaWM.htm)|Musical Instrument (Virtuoso handheld) (theorbo)|
|[kXOMAE7DzvqwCD6S.htm](abomination-vaults-bestiary-items/kXOMAE7DzvqwCD6S.htm)|Quara's Key|
|[l9pZATOZeQOajFDs.htm](abomination-vaults-bestiary-items/l9pZATOZeQOajFDs.htm)|Obscuring Mist (At Will)|
|[LFoqWJmgLEvSJxAw.htm](abomination-vaults-bestiary-items/LFoqWJmgLEvSJxAw.htm)|Darkness (At Will)|
|[lntTBnW0tn7XEMPJ.htm](abomination-vaults-bestiary-items/lntTBnW0tn7XEMPJ.htm)|Dimension Door (At Will)|
|[lQzCQf7gDhv2S88W.htm](abomination-vaults-bestiary-items/lQzCQf7gDhv2S88W.htm)|Invisibility (At Will) (Self Only)|
|[LuDFjIGjFpp0GFmQ.htm](abomination-vaults-bestiary-items/LuDFjIGjFpp0GFmQ.htm)|Composite Longbow|+1|
|[mfK2z6yvALRHoI2f.htm](abomination-vaults-bestiary-items/mfK2z6yvALRHoI2f.htm)|Jafaki's Round Key|
|[MHuoaCCeuKg2qzE4.htm](abomination-vaults-bestiary-items/MHuoaCCeuKg2qzE4.htm)|Haste (Constant)|
|[mL8fUkQ6DtVQj0dy.htm](abomination-vaults-bestiary-items/mL8fUkQ6DtVQj0dy.htm)|Enlarge (Self Only)|
|[mqqrqczmNE7FLbro.htm](abomination-vaults-bestiary-items/mqqrqczmNE7FLbro.htm)|Fear (At Will)|
|[mTtnkTVclwbLF3EX.htm](abomination-vaults-bestiary-items/mTtnkTVclwbLF3EX.htm)|Faerie Fire (At Will)|
|[mUBZliZ2MS11im0F.htm](abomination-vaults-bestiary-items/mUBZliZ2MS11im0F.htm)|Repeating Hand Crossbow|+1,striking|
|[mxpkzQTw0tzSwaUL.htm](abomination-vaults-bestiary-items/mxpkzQTw0tzSwaUL.htm)|Dimension Door (At Will)|
|[n5dgRSJ7OB2x6r3Y.htm](abomination-vaults-bestiary-items/n5dgRSJ7OB2x6r3Y.htm)|Infused Bottled Lightning (Moderate)|
|[NkWZvcWqJwezivmN.htm](abomination-vaults-bestiary-items/NkWZvcWqJwezivmN.htm)|Steel Shield|
|[nQVh2gtZd534YNJW.htm](abomination-vaults-bestiary-items/nQVh2gtZd534YNJW.htm)|Stealth|
|[O9UgZMqET8EKtovt.htm](abomination-vaults-bestiary-items/O9UgZMqET8EKtovt.htm)|Feather Fall (At Will) (Self Only)|
|[ocPEHKgADYZ7WIYS.htm](abomination-vaults-bestiary-items/ocPEHKgADYZ7WIYS.htm)|Caliddo's Keys|
|[oIJY4ycXpaF4IjIM.htm](abomination-vaults-bestiary-items/oIJY4ycXpaF4IjIM.htm)|See Invisibility (Constant)|
|[OjH9zugdZf9plsD2.htm](abomination-vaults-bestiary-items/OjH9zugdZf9plsD2.htm)|Composite Longbow|+1,striking|
|[okdtSA4yvXuAw47k.htm](abomination-vaults-bestiary-items/okdtSA4yvXuAw47k.htm)|Tongues (Constant)|
|[OZ0Sm3t3N9QrDKvO.htm](abomination-vaults-bestiary-items/OZ0Sm3t3N9QrDKvO.htm)|Infused Elixir of Life (Lesser)|
|[Q1GNuBqe5LEkrKBH.htm](abomination-vaults-bestiary-items/Q1GNuBqe5LEkrKBH.htm)|Mind Reading (At Will)|
|[q2sYrbAFzwVFBmAZ.htm](abomination-vaults-bestiary-items/q2sYrbAFzwVFBmAZ.htm)|Wand of Illusory Creature (Level 2)|
|[Q4CQqu46DoUxnpdv.htm](abomination-vaults-bestiary-items/Q4CQqu46DoUxnpdv.htm)|Invisibility (Self Only)|
|[QIxI3Cz1mN1x6wZt.htm](abomination-vaults-bestiary-items/QIxI3Cz1mN1x6wZt.htm)|Starknife|+1|
|[qU19blO5Z6vVE060.htm](abomination-vaults-bestiary-items/qU19blO5Z6vVE060.htm)|Infused Wyvern Poison|
|[QxLOTtEoMt74dE5J.htm](abomination-vaults-bestiary-items/QxLOTtEoMt74dE5J.htm)|Repeating Hand Crossbow|+1|
|[QzrBb53s8z16DDpm.htm](abomination-vaults-bestiary-items/QzrBb53s8z16DDpm.htm)|Sound Burst (At Will)|
|[rKb9CKHwR45IKSQQ.htm](abomination-vaults-bestiary-items/rKb9CKHwR45IKSQQ.htm)|Shauth Blade|+1|
|[RuO5xdjimVRC9EbR.htm](abomination-vaults-bestiary-items/RuO5xdjimVRC9EbR.htm)|Obscuring Mist (At Will)|
|[rXLsiUgGNMASCFTX.htm](abomination-vaults-bestiary-items/rXLsiUgGNMASCFTX.htm)|See Invisibility (Constant)|
|[SsAchSsgznoVg1Ws.htm](abomination-vaults-bestiary-items/SsAchSsgznoVg1Ws.htm)|Darkness (At Will)|
|[SWVNzLPTD6kuaZsY.htm](abomination-vaults-bestiary-items/SWVNzLPTD6kuaZsY.htm)|Wand of Magic Missile (Level 2)|
|[TwPr4oH0p59YtoDn.htm](abomination-vaults-bestiary-items/TwPr4oH0p59YtoDn.htm)|Harrow Fortune-Telling Deck|
|[txASc5iIQvLV4Nxv.htm](abomination-vaults-bestiary-items/txASc5iIQvLV4Nxv.htm)|Bejeweled Necklace featuring a Porpoise|
|[uc4ln8R3AB0uWMGh.htm](abomination-vaults-bestiary-items/uc4ln8R3AB0uWMGh.htm)|Darkness (At Will)|
|[uoAX9IiZSNNfElFr.htm](abomination-vaults-bestiary-items/uoAX9IiZSNNfElFr.htm)|Jafaki's Four-pronged Key|
|[v7ImH8CplEBf5cVO.htm](abomination-vaults-bestiary-items/v7ImH8CplEBf5cVO.htm)|Warhammer|+1,striking|
|[Vc1oe6EJUXOlITbu.htm](abomination-vaults-bestiary-items/Vc1oe6EJUXOlITbu.htm)|Wand of See Invisibility (Level 2)|
|[vYamsmUJUWGVxqsL.htm](abomination-vaults-bestiary-items/vYamsmUJUWGVxqsL.htm)|+1 Resilient Full Plate|
|[VYGdMMY84xwadkDM.htm](abomination-vaults-bestiary-items/VYGdMMY84xwadkDM.htm)|Faerie Fire (At Will)|
|[WfbnKRcDwQ4WCR3g.htm](abomination-vaults-bestiary-items/WfbnKRcDwQ4WCR3g.htm)|Feather Fall (At Will) (Self Only)|
|[wNrNVfIbe0JDWhOu.htm](abomination-vaults-bestiary-items/wNrNVfIbe0JDWhOu.htm)|Faerie Fire (At Will)|
|[WUQCPwFNjk75gqNw.htm](abomination-vaults-bestiary-items/WUQCPwFNjk75gqNw.htm)|Infused Cognitive Mutagen (Moderate)|
|[x07lPwTJOL6PzSxR.htm](abomination-vaults-bestiary-items/x07lPwTJOL6PzSxR.htm)|+1 Striking Rhoka Sword|+1,striking|
|[X9hw08u8m3fLbSva.htm](abomination-vaults-bestiary-items/X9hw08u8m3fLbSva.htm)|Invisibility (Self Only)|
|[xGaxH2mmKH0ttLJY.htm](abomination-vaults-bestiary-items/xGaxH2mmKH0ttLJY.htm)|Shauth Lash|+1,striking|
|[Xj2woyiQHPYlP7Uc.htm](abomination-vaults-bestiary-items/Xj2woyiQHPYlP7Uc.htm)|Darkness (At Will)|
|[xuheW5c3xyUS2F9E.htm](abomination-vaults-bestiary-items/xuheW5c3xyUS2F9E.htm)|Kukri|+1,striking|
|[xuzZonbgefgsArAz.htm](abomination-vaults-bestiary-items/xuzZonbgefgsArAz.htm)|Summon Entity (Will-o'-Wisp Only)|
|[YfnA27LEjbg8h3rR.htm](abomination-vaults-bestiary-items/YfnA27LEjbg8h3rR.htm)|Blowgun Darts (Poisoned)|
|[YvcIDxmDZnqrfRBa.htm](abomination-vaults-bestiary-items/YvcIDxmDZnqrfRBa.htm)|Locate (At Will)|
|[ZmA1lKUhd2XoWfNY.htm](abomination-vaults-bestiary-items/ZmA1lKUhd2XoWfNY.htm)|Drumstick|
|[ZZcgAENc8VCfxb7e.htm](abomination-vaults-bestiary-items/ZZcgAENc8VCfxb7e.htm)|Illusory Disguise (At Will)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[01CKBc5uwpZchw7t.htm](abomination-vaults-bestiary-items/01CKBc5uwpZchw7t.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[06lGWrOe3DOYvTgV.htm](abomination-vaults-bestiary-items/06lGWrOe3DOYvTgV.htm)|Bright Release|Soltar Brillante|modificada|
|[09R0B0sIkwFfNoZ5.htm](abomination-vaults-bestiary-items/09R0B0sIkwFfNoZ5.htm)|Shuriken|Shuriken|modificada|
|[0agBOEbezDUSkYUQ.htm](abomination-vaults-bestiary-items/0agBOEbezDUSkYUQ.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[0dO1DU5gt5v8bFwc.htm](abomination-vaults-bestiary-items/0dO1DU5gt5v8bFwc.htm)|Swallow Whole|Engullir Todo|modificada|
|[0Pe1uBGVehRLhBim.htm](abomination-vaults-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[0PqUwOqIbxXioOPh.htm](abomination-vaults-bestiary-items/0PqUwOqIbxXioOPh.htm)|Negative Healing|Curación negativa|modificada|
|[0QCPtkVtKzuuVVaK.htm](abomination-vaults-bestiary-items/0QCPtkVtKzuuVVaK.htm)|Bladed Limb|Bladed Limb|modificada|
|[0QhLcUbXJtWWVCmp.htm](abomination-vaults-bestiary-items/0QhLcUbXJtWWVCmp.htm)|Paralysis|Parálisis|modificada|
|[0UgwXoyN2VLdj2sc.htm](abomination-vaults-bestiary-items/0UgwXoyN2VLdj2sc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[11ioR4E9tTmB1Wak.htm](abomination-vaults-bestiary-items/11ioR4E9tTmB1Wak.htm)|Filth Fever|Filth Fever|modificada|
|[122ICOgqYvLmpu9U.htm](abomination-vaults-bestiary-items/122ICOgqYvLmpu9U.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[12LD6QHUzml95C9n.htm](abomination-vaults-bestiary-items/12LD6QHUzml95C9n.htm)|Frost Greatsword|Gélida Greatsword|modificada|
|[18GcGCMe0ekCnLyC.htm](abomination-vaults-bestiary-items/18GcGCMe0ekCnLyC.htm)|Dagger|Daga|modificada|
|[19toTT0P9kxuQAmz.htm](abomination-vaults-bestiary-items/19toTT0P9kxuQAmz.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[1B109bqthYI0DGyC.htm](abomination-vaults-bestiary-items/1B109bqthYI0DGyC.htm)|Innocuous|Inocuo|modificada|
|[1CumgFDHAf5o5Yfs.htm](abomination-vaults-bestiary-items/1CumgFDHAf5o5Yfs.htm)|Breath Weapon|Breath Weapon|modificada|
|[1FEKSwqCrM8p8ho7.htm](abomination-vaults-bestiary-items/1FEKSwqCrM8p8ho7.htm)|Void Healing|Curación negativa|modificada|
|[1G0kEOTWyivIisjY.htm](abomination-vaults-bestiary-items/1G0kEOTWyivIisjY.htm)|Psychokinetic Honing|Dirección psicocinética|modificada|
|[1IcPKJ8QCEzRvPPY.htm](abomination-vaults-bestiary-items/1IcPKJ8QCEzRvPPY.htm)|Light Hammer|Martillo ligero|modificada|
|[1lJU8hzSml2TMM3M.htm](abomination-vaults-bestiary-items/1lJU8hzSml2TMM3M.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[1SovHiGHboDM9yGe.htm](abomination-vaults-bestiary-items/1SovHiGHboDM9yGe.htm)|Light Aura|Aura de luz|modificada|
|[1tb7DMhGgvu0yKi2.htm](abomination-vaults-bestiary-items/1tb7DMhGgvu0yKi2.htm)|Seugathi Venom|Seugathi Venom|modificada|
|[1ubYo2q1E42ysH5K.htm](abomination-vaults-bestiary-items/1ubYo2q1E42ysH5K.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1UmNfowZ4HWxS9rx.htm](abomination-vaults-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|Explosión devastadora|modificada|
|[1zCZasUbrbz7SMAh.htm](abomination-vaults-bestiary-items/1zCZasUbrbz7SMAh.htm)|Flicker|Flicker|modificada|
|[29EzhqCR2u7R7K4N.htm](abomination-vaults-bestiary-items/29EzhqCR2u7R7K4N.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2a2eONlkTBanM6W0.htm](abomination-vaults-bestiary-items/2a2eONlkTBanM6W0.htm)|Drain Life|Drenar Vida|modificada|
|[2dHPDZu3BYN1GauB.htm](abomination-vaults-bestiary-items/2dHPDZu3BYN1GauB.htm)|No Breath|No Breath|modificada|
|[2l5Df8352WwEuAXJ.htm](abomination-vaults-bestiary-items/2l5Df8352WwEuAXJ.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[2lRJXkUSUIu9w7Vg.htm](abomination-vaults-bestiary-items/2lRJXkUSUIu9w7Vg.htm)|Elven Curve Blade|Hoja curva élfica|modificada|
|[2NEvfMuBqdlReHe7.htm](abomination-vaults-bestiary-items/2NEvfMuBqdlReHe7.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2pz6VpKOe8cRxqUY.htm](abomination-vaults-bestiary-items/2pz6VpKOe8cRxqUY.htm)|Guiding Rhythm|Ritmo que guía|modificada|
|[2rAKighB6Ywhk5s3.htm](abomination-vaults-bestiary-items/2rAKighB6Ywhk5s3.htm)|Flare|Flare|modificada|
|[2rGZ3A8JPafZZZur.htm](abomination-vaults-bestiary-items/2rGZ3A8JPafZZZur.htm)|Terrifying Charge|Carga terrorífica|modificada|
|[2Ufw7cZauI4kkDrY.htm](abomination-vaults-bestiary-items/2Ufw7cZauI4kkDrY.htm)|Starknife|Starknife|modificada|
|[37DJ9kew2cByiJDV.htm](abomination-vaults-bestiary-items/37DJ9kew2cByiJDV.htm)|Self-Loathing|Autodesprecio|modificada|
|[3AqKPdexSndZxd5X.htm](abomination-vaults-bestiary-items/3AqKPdexSndZxd5X.htm)|Rend|Rasgadura|modificada|
|[3b3sEGxcz23toRTi.htm](abomination-vaults-bestiary-items/3b3sEGxcz23toRTi.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[3Eksrmh390T4SrP7.htm](abomination-vaults-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[3mqnU8Lr3Tl3zgd2.htm](abomination-vaults-bestiary-items/3mqnU8Lr3Tl3zgd2.htm)|Claw|Garra|modificada|
|[3RI2spwyiEWuXdgF.htm](abomination-vaults-bestiary-items/3RI2spwyiEWuXdgF.htm)|Negative Healing|Curación negativa|modificada|
|[3sSrZrwFGWNyzaIj.htm](abomination-vaults-bestiary-items/3sSrZrwFGWNyzaIj.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[3vycv9ThXOQbgNBf.htm](abomination-vaults-bestiary-items/3vycv9ThXOQbgNBf.htm)|Instinctual Tinker|Trastear instintivo|modificada|
|[3Y7de8nXARx61nEk.htm](abomination-vaults-bestiary-items/3Y7de8nXARx61nEk.htm)|Spray Toxic Oil|Spray Toxic Oil|modificada|
|[41jyyqtM7TidzhJx.htm](abomination-vaults-bestiary-items/41jyyqtM7TidzhJx.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[43FJrgdgr667MXKo.htm](abomination-vaults-bestiary-items/43FJrgdgr667MXKo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[45yP2nFzQUcp3Zvm.htm](abomination-vaults-bestiary-items/45yP2nFzQUcp3Zvm.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[4adEu2Sh2OYsvkLV.htm](abomination-vaults-bestiary-items/4adEu2Sh2OYsvkLV.htm)|Fist|Puño|modificada|
|[4cuSJxJLUg7icXmj.htm](abomination-vaults-bestiary-items/4cuSJxJLUg7icXmj.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[4GEUTovCKhSoT0PD.htm](abomination-vaults-bestiary-items/4GEUTovCKhSoT0PD.htm)|Stupor Poison|Veneno de estupor|modificada|
|[4k6yZiC3ggdlq57F.htm](abomination-vaults-bestiary-items/4k6yZiC3ggdlq57F.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[4mS9YqseWjLaRiQq.htm](abomination-vaults-bestiary-items/4mS9YqseWjLaRiQq.htm)|Skirmishing Dash|Dash Escaramuza|modificada|
|[4pjPyWF2dnob8Tlm.htm](abomination-vaults-bestiary-items/4pjPyWF2dnob8Tlm.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[4UocioViS4VkF9mM.htm](abomination-vaults-bestiary-items/4UocioViS4VkF9mM.htm)|Scythe Shuffle|Scythe Shuffle|modificada|
|[51r9xhJpaBdZxkK3.htm](abomination-vaults-bestiary-items/51r9xhJpaBdZxkK3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5CnYknDSEBbdP7iX.htm](abomination-vaults-bestiary-items/5CnYknDSEBbdP7iX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[5dcaJKOKohQRyeQx.htm](abomination-vaults-bestiary-items/5dcaJKOKohQRyeQx.htm)|Glow|Glow|modificada|
|[5dn4uxBWvzHmjJF9.htm](abomination-vaults-bestiary-items/5dn4uxBWvzHmjJF9.htm)|Mummy Rot|Mummy Rot|modificada|
|[5e5FND78feSKXP99.htm](abomination-vaults-bestiary-items/5e5FND78feSKXP99.htm)|Impaling Chain|Cadena Empaladora|modificada|
|[5htUbZNH2cz3d5sv.htm](abomination-vaults-bestiary-items/5htUbZNH2cz3d5sv.htm)|Club|Club|modificada|
|[5NK2vxZn4QPORBlo.htm](abomination-vaults-bestiary-items/5NK2vxZn4QPORBlo.htm)|Scalathrax Venom|Veneno de Scalathrax|modificada|
|[5OrtHpltPsigs3A8.htm](abomination-vaults-bestiary-items/5OrtHpltPsigs3A8.htm)|Jaws|Fauces|modificada|
|[5pnWBWoyvDchoWWQ.htm](abomination-vaults-bestiary-items/5pnWBWoyvDchoWWQ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[5rrKtIIzYEri3htn.htm](abomination-vaults-bestiary-items/5rrKtIIzYEri3htn.htm)|Wolf Coordination|Coordinación Lobo|modificada|
|[5s3WbfpPg11BNAkO.htm](abomination-vaults-bestiary-items/5s3WbfpPg11BNAkO.htm)|Apocalypse Beam (From Tsunami)|Rayo Apocalipsis (De Tsunami)|modificada|
|[5Teyg9iopHoZ6xTN.htm](abomination-vaults-bestiary-items/5Teyg9iopHoZ6xTN.htm)|Death's Grace|Gracia de la muerte|modificada|
|[5TV0w8GnwrjMX6pX.htm](abomination-vaults-bestiary-items/5TV0w8GnwrjMX6pX.htm)|Apocalypse Beam (from Earthquake)|Rayo Apocalipsis (de Terremoto)|modificada|
|[5Vw9VTB3I1sie1g1.htm](abomination-vaults-bestiary-items/5Vw9VTB3I1sie1g1.htm)|Sarglagon Venom|Sarglagon Venom|modificada|
|[5YFP1XKwVqEkmjZV.htm](abomination-vaults-bestiary-items/5YFP1XKwVqEkmjZV.htm)|Swift Leap|Salto veloz|modificada|
|[5YNto5CTjVF4xtDp.htm](abomination-vaults-bestiary-items/5YNto5CTjVF4xtDp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[60GCM4eYgqqlPJau.htm](abomination-vaults-bestiary-items/60GCM4eYgqqlPJau.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[65o1CNgkOxBuaZU6.htm](abomination-vaults-bestiary-items/65o1CNgkOxBuaZU6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6BBZEA8kkI2GaLNH.htm](abomination-vaults-bestiary-items/6BBZEA8kkI2GaLNH.htm)|Shauth Lash|Shauth Lash|modificada|
|[6EdSmIm5HogvdHr3.htm](abomination-vaults-bestiary-items/6EdSmIm5HogvdHr3.htm)|Jaws|Fauces|modificada|
|[6F6RBqJmz0JUvLc3.htm](abomination-vaults-bestiary-items/6F6RBqJmz0JUvLc3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6FmfnLeOcKMh8v5n.htm](abomination-vaults-bestiary-items/6FmfnLeOcKMh8v5n.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[6glfioS5uXVucJjF.htm](abomination-vaults-bestiary-items/6glfioS5uXVucJjF.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[6j3vC2OBRq8nReei.htm](abomination-vaults-bestiary-items/6j3vC2OBRq8nReei.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6P6uGjuZLcSJBoME.htm](abomination-vaults-bestiary-items/6P6uGjuZLcSJBoME.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[6pIoss4a281w0Fed.htm](abomination-vaults-bestiary-items/6pIoss4a281w0Fed.htm)|Light Flare|Destello de luz|modificada|
|[6sW128BEd2N5fP96.htm](abomination-vaults-bestiary-items/6sW128BEd2N5fP96.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6TMEJEZsiK6x4E68.htm](abomination-vaults-bestiary-items/6TMEJEZsiK6x4E68.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[6uvSpdy8ezlFUIIo.htm](abomination-vaults-bestiary-items/6uvSpdy8ezlFUIIo.htm)|Shred Flesh|Destrozar Carne|modificada|
|[6XRN01EUnaz1YCgJ.htm](abomination-vaults-bestiary-items/6XRN01EUnaz1YCgJ.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[6zbiPHhWaAV5pJd6.htm](abomination-vaults-bestiary-items/6zbiPHhWaAV5pJd6.htm)|Wriggling Beard|Barba serpenteante|modificada|
|[72UcADd2WTnoNikm.htm](abomination-vaults-bestiary-items/72UcADd2WTnoNikm.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[78oc41E029XUkMoS.htm](abomination-vaults-bestiary-items/78oc41E029XUkMoS.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7hyO9Fv7Tl6MS4L3.htm](abomination-vaults-bestiary-items/7hyO9Fv7Tl6MS4L3.htm)|Skull Rot|Skull Rot|modificada|
|[7mOx4vdvxyfHNF71.htm](abomination-vaults-bestiary-items/7mOx4vdvxyfHNF71.htm)|Blood Magic|Magia de sangre|modificada|
|[7NjsgQKMg0FvxDmJ.htm](abomination-vaults-bestiary-items/7NjsgQKMg0FvxDmJ.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[7QmN8VFF7J1MzXXO.htm](abomination-vaults-bestiary-items/7QmN8VFF7J1MzXXO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7QQL3bbmfxHIb7Tz.htm](abomination-vaults-bestiary-items/7QQL3bbmfxHIb7Tz.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[7sBSRQrV9kFrhQxu.htm](abomination-vaults-bestiary-items/7sBSRQrV9kFrhQxu.htm)|Bite|Muerdemuerde|modificada|
|[7Scw9mC9n5d0Jon5.htm](abomination-vaults-bestiary-items/7Scw9mC9n5d0Jon5.htm)|Spear Frog Poison|Veneno de rana lanza|modificada|
|[7tCu8SPq0TpF1lJX.htm](abomination-vaults-bestiary-items/7tCu8SPq0TpF1lJX.htm)|Mindfog Aura|Mindfog Aura|modificada|
|[7VBvaubtWnbIVsZ9.htm](abomination-vaults-bestiary-items/7VBvaubtWnbIVsZ9.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[7wW5Mpc0dnnQnoko.htm](abomination-vaults-bestiary-items/7wW5Mpc0dnnQnoko.htm)|Wicked Bite|Muerdemuerde|modificada|
|[7Y7vl4vBRBKsb6wB.htm](abomination-vaults-bestiary-items/7Y7vl4vBRBKsb6wB.htm)|Jaws|Fauces|modificada|
|[800HkICSLpenBkq9.htm](abomination-vaults-bestiary-items/800HkICSLpenBkq9.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[83QmDBGigZxaHbbd.htm](abomination-vaults-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[87c42mEQ6XMyoNBu.htm](abomination-vaults-bestiary-items/87c42mEQ6XMyoNBu.htm)|Read the Stars|Leer las estrellas|modificada|
|[8aYfDhAzJOdFBLKs.htm](abomination-vaults-bestiary-items/8aYfDhAzJOdFBLKs.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[8g0Gpku3xbzwpSTx.htm](abomination-vaults-bestiary-items/8g0Gpku3xbzwpSTx.htm)|Go Dark|Oscurecerse|modificada|
|[8l05kDJb53W6NV0L.htm](abomination-vaults-bestiary-items/8l05kDJb53W6NV0L.htm)|Jaws|Fauces|modificada|
|[8nidqgL5pawfDVZa.htm](abomination-vaults-bestiary-items/8nidqgL5pawfDVZa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8QZHl09gya9bBM7r.htm](abomination-vaults-bestiary-items/8QZHl09gya9bBM7r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8R3jmOcyIfgNNdKr.htm](abomination-vaults-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|Curación negativa|modificada|
|[8rL4XxfF1Fb3WMBG.htm](abomination-vaults-bestiary-items/8rL4XxfF1Fb3WMBG.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[8SCH000Zv6X9BFPf.htm](abomination-vaults-bestiary-items/8SCH000Zv6X9BFPf.htm)|Fist|Puño|modificada|
|[8SE9FJ5v5zXgilOT.htm](abomination-vaults-bestiary-items/8SE9FJ5v5zXgilOT.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[8vdtmNasjIJH8Bi3.htm](abomination-vaults-bestiary-items/8vdtmNasjIJH8Bi3.htm)|Light Hammer|Martillo ligero|modificada|
|[8xM2M4RGdKhM7ex3.htm](abomination-vaults-bestiary-items/8xM2M4RGdKhM7ex3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[8z4D5pllZhfFA85D.htm](abomination-vaults-bestiary-items/8z4D5pllZhfFA85D.htm)|Stench Suppression|Supresión de Hedor|modificada|
|[8zJvcrLeqTjH3Eup.htm](abomination-vaults-bestiary-items/8zJvcrLeqTjH3Eup.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[94yfc2eSiwRZfq49.htm](abomination-vaults-bestiary-items/94yfc2eSiwRZfq49.htm)|Needle|Aguja|modificada|
|[99jBJtnLmD48VOSS.htm](abomination-vaults-bestiary-items/99jBJtnLmD48VOSS.htm)|Claw|Garra|modificada|
|[9C4djpvjI0fNA6Fa.htm](abomination-vaults-bestiary-items/9C4djpvjI0fNA6Fa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9DkYcHRtL1rgB4Ob.htm](abomination-vaults-bestiary-items/9DkYcHRtL1rgB4Ob.htm)|Constrict|Restringir|modificada|
|[9Eh0XQSnJ9rPgQ3d.htm](abomination-vaults-bestiary-items/9Eh0XQSnJ9rPgQ3d.htm)|Push|Push|modificada|
|[9eONvr5by0xfPFcf.htm](abomination-vaults-bestiary-items/9eONvr5by0xfPFcf.htm)|Witchflame Caress|Witchflame Caress|modificada|
|[9i3UjVYq4lp3MqZJ.htm](abomination-vaults-bestiary-items/9i3UjVYq4lp3MqZJ.htm)|Burning Lash|Burning Lash|modificada|
|[9J2GBQp8uuYCXtLo.htm](abomination-vaults-bestiary-items/9J2GBQp8uuYCXtLo.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[9xQRUNNxUssnoy4j.htm](abomination-vaults-bestiary-items/9xQRUNNxUssnoy4j.htm)|Chameleon Skin|Piel de Camaleón|modificada|
|[9yk5MBYML8Z0OmtD.htm](abomination-vaults-bestiary-items/9yk5MBYML8Z0OmtD.htm)|Telepathy (with spectral thralls only)|Telepatía (solo con thralls espectrales).|modificada|
|[a4lUBuJPU86WP0Gq.htm](abomination-vaults-bestiary-items/a4lUBuJPU86WP0Gq.htm)|Mindfog Aura|Mindfog Aura|modificada|
|[a4vxarsPYtIYy5Br.htm](abomination-vaults-bestiary-items/a4vxarsPYtIYy5Br.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[a6xLeWb5L726R1lN.htm](abomination-vaults-bestiary-items/a6xLeWb5L726R1lN.htm)|Push|Push|modificada|
|[AAFWcmRlGxb76Pps.htm](abomination-vaults-bestiary-items/AAFWcmRlGxb76Pps.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[ACtqtCeexz93EgK9.htm](abomination-vaults-bestiary-items/ACtqtCeexz93EgK9.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[agqHW8i0ToF0OIyF.htm](abomination-vaults-bestiary-items/agqHW8i0ToF0OIyF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[AGshXsiKjGrOkqWk.htm](abomination-vaults-bestiary-items/AGshXsiKjGrOkqWk.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[AHdJDOIrRS3IZSrc.htm](abomination-vaults-bestiary-items/AHdJDOIrRS3IZSrc.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[aK6ekR5eDVtsBkFZ.htm](abomination-vaults-bestiary-items/aK6ekR5eDVtsBkFZ.htm)|Occult Attack|Ataque Ocultismo|modificada|
|[AkS8oNJyeHZyFiyP.htm](abomination-vaults-bestiary-items/AkS8oNJyeHZyFiyP.htm)|Pitfall|Trampa de caída|modificada|
|[alpHOBrHeF3AD5cW.htm](abomination-vaults-bestiary-items/alpHOBrHeF3AD5cW.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[aSbHK3DUVAsb8eW6.htm](abomination-vaults-bestiary-items/aSbHK3DUVAsb8eW6.htm)|Jaws|Fauces|modificada|
|[aUiw4yrSZnbmjXeR.htm](abomination-vaults-bestiary-items/aUiw4yrSZnbmjXeR.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[AWNveQn0EQ1aYORU.htm](abomination-vaults-bestiary-items/AWNveQn0EQ1aYORU.htm)|Rise Up|Levántate|modificada|
|[AyfJyi6gCHBgaToD.htm](abomination-vaults-bestiary-items/AyfJyi6gCHBgaToD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ayqWkUKHp3vU6wiY.htm](abomination-vaults-bestiary-items/ayqWkUKHp3vU6wiY.htm)|Blood Offering|Blood Offering|modificada|
|[azGXpMTZu0Ft4DaJ.htm](abomination-vaults-bestiary-items/azGXpMTZu0Ft4DaJ.htm)|Horn|Cuerno|modificada|
|[b1w5wps2XBnNPOls.htm](abomination-vaults-bestiary-items/b1w5wps2XBnNPOls.htm)|Furious Claws|Garras furiosas|modificada|
|[b6BZWa1ISKLflSTs.htm](abomination-vaults-bestiary-items/b6BZWa1ISKLflSTs.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[b6FIdRGaxea1KX6k.htm](abomination-vaults-bestiary-items/b6FIdRGaxea1KX6k.htm)|Claw|Garra|modificada|
|[B8jNXhGz4H4CYK3k.htm](abomination-vaults-bestiary-items/B8jNXhGz4H4CYK3k.htm)|Negative Healing|Curación negativa|modificada|
|[BBdrCuAIiJoPRLOJ.htm](abomination-vaults-bestiary-items/BBdrCuAIiJoPRLOJ.htm)|Breath of the Bog|Aliento de la ciénaga|modificada|
|[BCdQbDHjNRy8IfGp.htm](abomination-vaults-bestiary-items/BCdQbDHjNRy8IfGp.htm)|Unwilling Teleportation|Teletransporte involuntario|modificada|
|[BeNcnupTKc7WS9e5.htm](abomination-vaults-bestiary-items/BeNcnupTKc7WS9e5.htm)|Dance Moves|Pasos de baile|modificada|
|[BFiMEADLB2Qw0lL1.htm](abomination-vaults-bestiary-items/BFiMEADLB2Qw0lL1.htm)|Spike|Spike|modificada|
|[BhqjfA3rcAJRma6F.htm](abomination-vaults-bestiary-items/BhqjfA3rcAJRma6F.htm)|Claw|Garra|modificada|
|[bIhvOKLmWJLAIfDK.htm](abomination-vaults-bestiary-items/bIhvOKLmWJLAIfDK.htm)|Immobilizing Blow|Golpe Inmovilizador|modificada|
|[BkxqdQTCmF312Eby.htm](abomination-vaults-bestiary-items/BkxqdQTCmF312Eby.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[BldzzBvbyFPySPXc.htm](abomination-vaults-bestiary-items/BldzzBvbyFPySPXc.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[BMv1jkvT4dSbEL8X.htm](abomination-vaults-bestiary-items/BMv1jkvT4dSbEL8X.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[bMX8ZvBxeu0Ty5cl.htm](abomination-vaults-bestiary-items/bMX8ZvBxeu0Ty5cl.htm)|Devour Soul|Devorar alma|modificada|
|[boE5h9E3qlCaVd4L.htm](abomination-vaults-bestiary-items/boE5h9E3qlCaVd4L.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BPJS4QzIXUC3Zmwf.htm](abomination-vaults-bestiary-items/BPJS4QzIXUC3Zmwf.htm)|Frightful Presence|Frightful Presence|modificada|
|[BPvAPhpd3Nzs8pvv.htm](abomination-vaults-bestiary-items/BPvAPhpd3Nzs8pvv.htm)|Needle Spray|Needle Spray|modificada|
|[buzf372gE5wTjjeH.htm](abomination-vaults-bestiary-items/buzf372gE5wTjjeH.htm)|Fist|Puño|modificada|
|[bxjYZ892TLi9AizW.htm](abomination-vaults-bestiary-items/bxjYZ892TLi9AizW.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[BZL7n09A5aQfGHjz.htm](abomination-vaults-bestiary-items/BZL7n09A5aQfGHjz.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[C2q1zvPuwqgQMvwX.htm](abomination-vaults-bestiary-items/C2q1zvPuwqgQMvwX.htm)|Change Shape|Change Shape|modificada|
|[C2yMhpgqYU1HM1NR.htm](abomination-vaults-bestiary-items/C2yMhpgqYU1HM1NR.htm)|Claw|Garra|modificada|
|[C46qe4d4dQ5DqKp4.htm](abomination-vaults-bestiary-items/C46qe4d4dQ5DqKp4.htm)|Vile Blowgun|Vile Blowgun|modificada|
|[c8DR2M4wTFOhiB7j.htm](abomination-vaults-bestiary-items/c8DR2M4wTFOhiB7j.htm)|Fast Healing 7|Curación rápida 7|modificada|
|[C91ggah5Ye65LrMH.htm](abomination-vaults-bestiary-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|Leng Ruby|modificada|
|[CAWJlqwSDN3w5gWy.htm](abomination-vaults-bestiary-items/CAWJlqwSDN3w5gWy.htm)|Overpowering Jaws|Fauces abrumadoras|modificada|
|[cb5fpVrLbNcM4VUi.htm](abomination-vaults-bestiary-items/cb5fpVrLbNcM4VUi.htm)|Suppress Aura|Suprimir Aura|modificada|
|[CcTlJ52vSeJqkx5b.htm](abomination-vaults-bestiary-items/CcTlJ52vSeJqkx5b.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[cdTaJad7jDM2QE73.htm](abomination-vaults-bestiary-items/cdTaJad7jDM2QE73.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[cfM8s4VVlCf81rzl.htm](abomination-vaults-bestiary-items/cfM8s4VVlCf81rzl.htm)|Repeating Hand Crossbow|Ballesta de mano de repetición|modificada|
|[CH1wifIGckLBQDfe.htm](abomination-vaults-bestiary-items/CH1wifIGckLBQDfe.htm)|All-Around Vision|All-Around Vision|modificada|
|[ch9nYPl6I99TJLu9.htm](abomination-vaults-bestiary-items/ch9nYPl6I99TJLu9.htm)|Swarming Stance|Posición de Enjambre|modificada|
|[CH9sJBUN2HtEAgtt.htm](abomination-vaults-bestiary-items/CH9sJBUN2HtEAgtt.htm)|Vocal Warm-Up|Calentamiento vocal|modificada|
|[Ci03R6SeE2d256my.htm](abomination-vaults-bestiary-items/Ci03R6SeE2d256my.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[cj3wll0YeOYjYWaL.htm](abomination-vaults-bestiary-items/cj3wll0YeOYjYWaL.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[cKCHCzZNwEgF3rgQ.htm](abomination-vaults-bestiary-items/cKCHCzZNwEgF3rgQ.htm)|Negative Healing|Curación negativa|modificada|
|[ckEhVhjZnjMqhSjZ.htm](abomination-vaults-bestiary-items/ckEhVhjZnjMqhSjZ.htm)|Shootist's Draw|Shootist's Draw|modificada|
|[CL3iEdGmI7RDCvqu.htm](abomination-vaults-bestiary-items/CL3iEdGmI7RDCvqu.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[CncVGQus926xFSl2.htm](abomination-vaults-bestiary-items/CncVGQus926xFSl2.htm)|Shauth Bite|Muerdemuerde|modificada|
|[cNnj1mdRc6n47fUX.htm](abomination-vaults-bestiary-items/cNnj1mdRc6n47fUX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[coOSYslrfzzpX9Qf.htm](abomination-vaults-bestiary-items/coOSYslrfzzpX9Qf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[COyeCB6tCf47WbAu.htm](abomination-vaults-bestiary-items/COyeCB6tCf47WbAu.htm)|Jaws|Fauces|modificada|
|[CpXxvpsrf7rcrJM4.htm](abomination-vaults-bestiary-items/CpXxvpsrf7rcrJM4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cQrkrn9uX9hV860V.htm](abomination-vaults-bestiary-items/cQrkrn9uX9hV860V.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[Cth0VtGbtTcGpsAR.htm](abomination-vaults-bestiary-items/Cth0VtGbtTcGpsAR.htm)|Red Claw|Garra Roja|modificada|
|[CtYG2Idf6Es4FDCt.htm](abomination-vaults-bestiary-items/CtYG2Idf6Es4FDCt.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[cU4WaqnerXGtIpEM.htm](abomination-vaults-bestiary-items/cU4WaqnerXGtIpEM.htm)|Jaws|Fauces|modificada|
|[cu7cx46lq0JVi00X.htm](abomination-vaults-bestiary-items/cu7cx46lq0JVi00X.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[cxiSU1lrmfq49Cag.htm](abomination-vaults-bestiary-items/cxiSU1lrmfq49Cag.htm)|Grab|Agarrado|modificada|
|[cYzNZC3Tju4useHD.htm](abomination-vaults-bestiary-items/cYzNZC3Tju4useHD.htm)|Phantom Mount|Phantom Mount|modificada|
|[cZSuVe7TToENdQod.htm](abomination-vaults-bestiary-items/cZSuVe7TToENdQod.htm)|Spore Jet|Spore Jet|modificada|
|[D0n53VOiJk5g1YnP.htm](abomination-vaults-bestiary-items/D0n53VOiJk5g1YnP.htm)|Necrotic Bomb|Bomba Necrótica|modificada|
|[D6oLgwNpejO3m2LR.htm](abomination-vaults-bestiary-items/D6oLgwNpejO3m2LR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D8o83tvKi46maS3U.htm](abomination-vaults-bestiary-items/D8o83tvKi46maS3U.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[d9oLx0w5SZt7KSEv.htm](abomination-vaults-bestiary-items/d9oLx0w5SZt7KSEv.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[dBpRjWleNemuJ0CH.htm](abomination-vaults-bestiary-items/dBpRjWleNemuJ0CH.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[dd53Mcobz1fpxVhN.htm](abomination-vaults-bestiary-items/dd53Mcobz1fpxVhN.htm)|Claw|Garra|modificada|
|[deTqYUQHSSEOChwZ.htm](abomination-vaults-bestiary-items/deTqYUQHSSEOChwZ.htm)|Tentacle|Tentáculo|modificada|
|[DfDsOQ6I6MwVdnPd.htm](abomination-vaults-bestiary-items/DfDsOQ6I6MwVdnPd.htm)|Fleshy Slap|Bofetada carnosa|modificada|
|[dFe2hAb3rrhlAVsx.htm](abomination-vaults-bestiary-items/dFe2hAb3rrhlAVsx.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[dFO4VUU2D6u32L0j.htm](abomination-vaults-bestiary-items/dFO4VUU2D6u32L0j.htm)|Wicked Bite|Muerdemuerde|modificada|
|[DGCZumwAduW04eMJ.htm](abomination-vaults-bestiary-items/DGCZumwAduW04eMJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dH7eKw8pDJQDkR3P.htm](abomination-vaults-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|Restringir|modificada|
|[DHJskQgx4Ucg690b.htm](abomination-vaults-bestiary-items/DHJskQgx4Ucg690b.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dHv4zBnoFd4KSWBz.htm](abomination-vaults-bestiary-items/dHv4zBnoFd4KSWBz.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[DI6Nw0XKEZZJLY8D.htm](abomination-vaults-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[DI73oBXVmI2VUsnm.htm](abomination-vaults-bestiary-items/DI73oBXVmI2VUsnm.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[DIqlApNIdzR6Eh6H.htm](abomination-vaults-bestiary-items/DIqlApNIdzR6Eh6H.htm)|Hunter's Wound|Herida del cazador|modificada|
|[dJQlNu9f34qL4mEx.htm](abomination-vaults-bestiary-items/dJQlNu9f34qL4mEx.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[DKikA8jlUkgQxScq.htm](abomination-vaults-bestiary-items/DKikA8jlUkgQxScq.htm)|Constant Spells|Constant Spells|modificada|
|[dl0aE5MkOn0AmMJs.htm](abomination-vaults-bestiary-items/dl0aE5MkOn0AmMJs.htm)|Jaws|Fauces|modificada|
|[DlQoJTMEjgmsWgCa.htm](abomination-vaults-bestiary-items/DlQoJTMEjgmsWgCa.htm)|Mandibles|Mandíbulas|modificada|
|[DLzdlzxfprHMmnS5.htm](abomination-vaults-bestiary-items/DLzdlzxfprHMmnS5.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[dNigd85c2ZydQi1Z.htm](abomination-vaults-bestiary-items/dNigd85c2ZydQi1Z.htm)|Swarm Mind|Swarm Mind|modificada|
|[DriJGrzzm1Qq0NNX.htm](abomination-vaults-bestiary-items/DriJGrzzm1Qq0NNX.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[dsBZk8GZYws5VlE1.htm](abomination-vaults-bestiary-items/dsBZk8GZYws5VlE1.htm)|Claustrophobia|Claustrofobia|modificada|
|[DSrUNlWJjUN1fnWL.htm](abomination-vaults-bestiary-items/DSrUNlWJjUN1fnWL.htm)|Poisoned Breath|Aliento Envenenado|modificada|
|[DSW55AZz6cwRj1rc.htm](abomination-vaults-bestiary-items/DSW55AZz6cwRj1rc.htm)|Jaws|Fauces|modificada|
|[dTlqxVR7MGmNsHHm.htm](abomination-vaults-bestiary-items/dTlqxVR7MGmNsHHm.htm)|Kukri|Kukri|modificada|
|[dwzqPqd8eTnASsxZ.htm](abomination-vaults-bestiary-items/dwzqPqd8eTnASsxZ.htm)|Stone Defense|Defensa de Piedra|modificada|
|[DYoEwMCwfSHk2Og2.htm](abomination-vaults-bestiary-items/DYoEwMCwfSHk2Og2.htm)|Kukri|Kukri|modificada|
|[DZ1joQ2LpXlkz2Jm.htm](abomination-vaults-bestiary-items/DZ1joQ2LpXlkz2Jm.htm)|Dagger|Daga|modificada|
|[DzJcNWKMqkWqtgAX.htm](abomination-vaults-bestiary-items/DzJcNWKMqkWqtgAX.htm)|Flaming Hellforged Glaive|Flamígera Glaive de forja infernal|modificada|
|[E1j9pEdjLX1NuzdV.htm](abomination-vaults-bestiary-items/E1j9pEdjLX1NuzdV.htm)|Staff|Báculo|modificada|
|[e2TItLJ7Ia5jTsir.htm](abomination-vaults-bestiary-items/e2TItLJ7Ia5jTsir.htm)|+2 Status To All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[eAyilI89Ya4AzPjX.htm](abomination-vaults-bestiary-items/eAyilI89Ya4AzPjX.htm)|Bounce|Bounce|modificada|
|[Eb8EhrTUrXxsrLz3.htm](abomination-vaults-bestiary-items/Eb8EhrTUrXxsrLz3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[EBgiFSyYf0W5Fxcr.htm](abomination-vaults-bestiary-items/EBgiFSyYf0W5Fxcr.htm)|Jaws|Fauces|modificada|
|[EDPFYDhj0ZOTpRmX.htm](abomination-vaults-bestiary-items/EDPFYDhj0ZOTpRmX.htm)|Animal Order Spells|Conjuros de orden animal|modificada|
|[eebzEKEUQ1IX3JhM.htm](abomination-vaults-bestiary-items/eebzEKEUQ1IX3JhM.htm)|Bottled Lightning|Rayo Embotellado|modificada|
|[EEwIe3zMikRnRWg6.htm](abomination-vaults-bestiary-items/EEwIe3zMikRnRWg6.htm)|Warp Reality|Warp Reality|modificada|
|[eGvWOYTY1N6i6QpT.htm](abomination-vaults-bestiary-items/eGvWOYTY1N6i6QpT.htm)|Blood Drain|Drenar sangre|modificada|
|[EHjIsHVK3BSAzKZW.htm](abomination-vaults-bestiary-items/EHjIsHVK3BSAzKZW.htm)|Focus Gaze|Centrar mirada|modificada|
|[eIBT42KNs5UfO41v.htm](abomination-vaults-bestiary-items/eIBT42KNs5UfO41v.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EiWEO80FhEG270MQ.htm](abomination-vaults-bestiary-items/EiWEO80FhEG270MQ.htm)|Paralysis|Parálisis|modificada|
|[Elphpw5BdTHWoPps.htm](abomination-vaults-bestiary-items/Elphpw5BdTHWoPps.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[eltHO3xRIKJCfFcF.htm](abomination-vaults-bestiary-items/eltHO3xRIKJCfFcF.htm)|Claw|Garra|modificada|
|[eMT2uTNjSReRvfV8.htm](abomination-vaults-bestiary-items/eMT2uTNjSReRvfV8.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[eP5tBspqEqP18oTY.htm](abomination-vaults-bestiary-items/eP5tBspqEqP18oTY.htm)|Site Bound|Ligado a una ubicación|modificada|
|[eRq3BYWv8B8rLpXL.htm](abomination-vaults-bestiary-items/eRq3BYWv8B8rLpXL.htm)|Shadow Flitter|Revoloteo sombrío|modificada|
|[ErwsWW6oip6zma6i.htm](abomination-vaults-bestiary-items/ErwsWW6oip6zma6i.htm)|Shauth Seize|Shauth Seize|modificada|
|[ESemIP7QRiArXvT4.htm](abomination-vaults-bestiary-items/ESemIP7QRiArXvT4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eugKdHe7dmAkjXP0.htm](abomination-vaults-bestiary-items/eugKdHe7dmAkjXP0.htm)|Witchflame Bolt|Witchflame Bolt|modificada|
|[EvzzltzygJO4ess9.htm](abomination-vaults-bestiary-items/EvzzltzygJO4ess9.htm)|Negative Healing|Curación negativa|modificada|
|[EXyt7FC2RKPKjWCX.htm](abomination-vaults-bestiary-items/EXyt7FC2RKPKjWCX.htm)|Despair|Desesperación|modificada|
|[EyBIlDKJTvz0g8ka.htm](abomination-vaults-bestiary-items/EyBIlDKJTvz0g8ka.htm)|Echoes of Defeat|Echoes of Defeat|modificada|
|[EzSKWtWM2tSTtZwA.htm](abomination-vaults-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|Garra|modificada|
|[eZt77JcDJLXdqusp.htm](abomination-vaults-bestiary-items/eZt77JcDJLXdqusp.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[eZxgYfIRD41tf3F9.htm](abomination-vaults-bestiary-items/eZxgYfIRD41tf3F9.htm)|Radiant Ray|Radiant Ray|modificada|
|[F1MZjO36rRuFQXYR.htm](abomination-vaults-bestiary-items/F1MZjO36rRuFQXYR.htm)|Envenom Weapon|Arma Envenom|modificada|
|[f2TrT4l8m1DRgTZ4.htm](abomination-vaults-bestiary-items/f2TrT4l8m1DRgTZ4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[F40AJYqQ0BCC2TF0.htm](abomination-vaults-bestiary-items/F40AJYqQ0BCC2TF0.htm)|Chain|Cadena|modificada|
|[f50ST8T8gY3pQJfx.htm](abomination-vaults-bestiary-items/f50ST8T8gY3pQJfx.htm)|Evasion|Evasión|modificada|
|[f7rkC7qodqH9NbVI.htm](abomination-vaults-bestiary-items/f7rkC7qodqH9NbVI.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[F7zqmFxLH2v3hOAA.htm](abomination-vaults-bestiary-items/F7zqmFxLH2v3hOAA.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[F8VzPOjdtS2Gmx87.htm](abomination-vaults-bestiary-items/F8VzPOjdtS2Gmx87.htm)|Mind Feeding|Alimentar la Mente|modificada|
|[F8wXzsUNliJVU8bv.htm](abomination-vaults-bestiary-items/F8wXzsUNliJVU8bv.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[F8xpT7h5a5ZviXU5.htm](abomination-vaults-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[Fap5Oo5cGmdLjzes.htm](abomination-vaults-bestiary-items/Fap5Oo5cGmdLjzes.htm)|Reloading Trick|Truco para recargar|modificada|
|[fBrZMabfv55LX5bY.htm](abomination-vaults-bestiary-items/fBrZMabfv55LX5bY.htm)|Show the Looming Moon|Show the Looming Moon|modificada|
|[fdAMCESh7OW3fKWY.htm](abomination-vaults-bestiary-items/fdAMCESh7OW3fKWY.htm)|Consume Confusion|Consume confusión|modificada|
|[fDft2vRYcFM3JXBe.htm](abomination-vaults-bestiary-items/fDft2vRYcFM3JXBe.htm)|Intense Performer|Intenso Interpretar|modificada|
|[fDmw9feXNMKuAci4.htm](abomination-vaults-bestiary-items/fDmw9feXNMKuAci4.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[fGmd67WxTzSxDIpe.htm](abomination-vaults-bestiary-items/fGmd67WxTzSxDIpe.htm)|Tendril|Tendril|modificada|
|[fIssfjOuDo0yfwLc.htm](abomination-vaults-bestiary-items/fIssfjOuDo0yfwLc.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[FLe1chjATs0hNKPw.htm](abomination-vaults-bestiary-items/FLe1chjATs0hNKPw.htm)|Explosive Decay|Deterioro explosivo|modificada|
|[fMZfXQMCUD23X5H9.htm](abomination-vaults-bestiary-items/fMZfXQMCUD23X5H9.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[FN5QndPNQPuzc0V3.htm](abomination-vaults-bestiary-items/FN5QndPNQPuzc0V3.htm)|Coven Spells|Coven Spells|modificada|
|[fNQ2aHpM6vTQ7iqr.htm](abomination-vaults-bestiary-items/fNQ2aHpM6vTQ7iqr.htm)|Command Confusion|Dirigir confusión de orden imperiosa.|modificada|
|[fOXEVyfWiHQRSyiC.htm](abomination-vaults-bestiary-items/fOXEVyfWiHQRSyiC.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[FsHTfpYkyQ2CCGzZ.htm](abomination-vaults-bestiary-items/FsHTfpYkyQ2CCGzZ.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[FteyjzEwACt18LfC.htm](abomination-vaults-bestiary-items/FteyjzEwACt18LfC.htm)|Gnawing Fog|Niebla de dentelladas|modificada|
|[FuDdgAP7wwq6LCFO.htm](abomination-vaults-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Serpent Venom|Serpent Venom|modificada|
|[fueyaPehWakc93nh.htm](abomination-vaults-bestiary-items/fueyaPehWakc93nh.htm)|Swarming Stance|Posición de Enjambre|modificada|
|[FZlSN0F2oSrYwSf4.htm](abomination-vaults-bestiary-items/FZlSN0F2oSrYwSf4.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[g2QS3ryiZhgirGmj.htm](abomination-vaults-bestiary-items/g2QS3ryiZhgirGmj.htm)|Bog Rot|Podredumbre del pantano|modificada|
|[G3bgPlOpXpztjTdf.htm](abomination-vaults-bestiary-items/G3bgPlOpXpztjTdf.htm)|Katar|Katar|modificada|
|[G412UtVloe7PIpa4.htm](abomination-vaults-bestiary-items/G412UtVloe7PIpa4.htm)|+2 Status to All Saves vs. Disease and Poison|+2 situación a todas las salvaciones contra enfermedad y veneno.|modificada|
|[G4UvThy3GwXSiOIo.htm](abomination-vaults-bestiary-items/G4UvThy3GwXSiOIo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[GA3sOIQc3gtd1dyN.htm](abomination-vaults-bestiary-items/GA3sOIQc3gtd1dyN.htm)|Shadow Jump|Sombra Salta|modificada|
|[gAui54rDueAP4nNh.htm](abomination-vaults-bestiary-items/gAui54rDueAP4nNh.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[GCu6OFgRjem6F0sI.htm](abomination-vaults-bestiary-items/GCu6OFgRjem6F0sI.htm)|Witchflame|Witchflame|modificada|
|[GDIw4d1nppVwAcDM.htm](abomination-vaults-bestiary-items/GDIw4d1nppVwAcDM.htm)|Blood Magic|Magia de sangre|modificada|
|[gdWSFRbbf21JhOa9.htm](abomination-vaults-bestiary-items/gdWSFRbbf21JhOa9.htm)|Ectoplasmic Web Trap|Trampa de telara ectoplasmática|modificada|
|[gH06P6rp2bswuGya.htm](abomination-vaults-bestiary-items/gH06P6rp2bswuGya.htm)|Jaws|Fauces|modificada|
|[gHMs8jNdIsdBy2OZ.htm](abomination-vaults-bestiary-items/gHMs8jNdIsdBy2OZ.htm)|Shock|Electrizante|modificada|
|[gkIxUkhYZg4oHj1t.htm](abomination-vaults-bestiary-items/gkIxUkhYZg4oHj1t.htm)|+2 Status to All Saves vs. Bleed, Death Effects, Disease, Doomed, Fatigued, Paralyzed, Poison, Sickened|+2 a la situación a todas las salvaciones contra sangrado, efectos de muerte, enfermedad, condenado/a, fatigado/a, paralizado/a, veneno, enfermo/a.|modificada|
|[gl7lB276y1xS70s3.htm](abomination-vaults-bestiary-items/gl7lB276y1xS70s3.htm)|Jaul Coordination|Coordinación Jaul|modificada|
|[GOUc826yPXfyrWNW.htm](abomination-vaults-bestiary-items/GOUc826yPXfyrWNW.htm)|Shootist's Luck|Shootist's Luck|modificada|
|[GoYV1mj4XKxDiBOM.htm](abomination-vaults-bestiary-items/GoYV1mj4XKxDiBOM.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GpoVeh1bdg8JH2SG.htm](abomination-vaults-bestiary-items/GpoVeh1bdg8JH2SG.htm)|Deny Advantage|Denegar ventaja|modificada|
|[GsXsiIe2IQXx1C8I.htm](abomination-vaults-bestiary-items/GsXsiIe2IQXx1C8I.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gTviRIemgZXPfEW1.htm](abomination-vaults-bestiary-items/gTviRIemgZXPfEW1.htm)|Draining Venom|Veneno drenante|modificada|
|[gyyRt5jyTNORzIpr.htm](abomination-vaults-bestiary-items/gyyRt5jyTNORzIpr.htm)|Defensive Needle|Aguja Defensiva|modificada|
|[GZ55vWFhwaHyhnfM.htm](abomination-vaults-bestiary-items/GZ55vWFhwaHyhnfM.htm)|Avernal Fever|Fiebre del Averno|modificada|
|[gZDMpjUjAgRTjrcL.htm](abomination-vaults-bestiary-items/gZDMpjUjAgRTjrcL.htm)|Spike|Spike|modificada|
|[GzMhkd0CBpUeUX63.htm](abomination-vaults-bestiary-items/GzMhkd0CBpUeUX63.htm)|Projectile Launcher|Lanzador de proyectiles|modificada|
|[h2qxWUvI0fbg4RxB.htm](abomination-vaults-bestiary-items/h2qxWUvI0fbg4RxB.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[H2rT9wdiejx3F1b0.htm](abomination-vaults-bestiary-items/H2rT9wdiejx3F1b0.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[H34w70sTO05v4xsB.htm](abomination-vaults-bestiary-items/H34w70sTO05v4xsB.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[H3dPRQwlEWXQFPUe.htm](abomination-vaults-bestiary-items/H3dPRQwlEWXQFPUe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[H61mcURmEhYdmt9L.htm](abomination-vaults-bestiary-items/H61mcURmEhYdmt9L.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[H6sBL4aimiKQcdtJ.htm](abomination-vaults-bestiary-items/H6sBL4aimiKQcdtJ.htm)|Snout|Hocico|modificada|
|[HaYQ8Cd4C0hYSFdc.htm](abomination-vaults-bestiary-items/HaYQ8Cd4C0hYSFdc.htm)|Leap Attack|Salto sin carrerilla|modificada|
|[HcrsT0ltPpWRSZG1.htm](abomination-vaults-bestiary-items/HcrsT0ltPpWRSZG1.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Hczu635vDDik3NIJ.htm](abomination-vaults-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|Agarrado|modificada|
|[hfZmDX2fsCvlGR4v.htm](abomination-vaults-bestiary-items/hfZmDX2fsCvlGR4v.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[HHbU5fdUwrxiWVG3.htm](abomination-vaults-bestiary-items/HHbU5fdUwrxiWVG3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[hIYUGmOfZ6ZD3cNt.htm](abomination-vaults-bestiary-items/hIYUGmOfZ6ZD3cNt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Hl7cINV4KNf7cW0s.htm](abomination-vaults-bestiary-items/Hl7cINV4KNf7cW0s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HLcMADTgT1nixxce.htm](abomination-vaults-bestiary-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|Aliento de la ciénaga|modificada|
|[HLLk7YCS6Ad1ibE5.htm](abomination-vaults-bestiary-items/HLLk7YCS6Ad1ibE5.htm)|Staff|Báculo|modificada|
|[hNe75I8nMGqBfaaP.htm](abomination-vaults-bestiary-items/hNe75I8nMGqBfaaP.htm)|Rise Up|Levántate|modificada|
|[Hor4EOQfUGAkFak1.htm](abomination-vaults-bestiary-items/Hor4EOQfUGAkFak1.htm)|Flaming Composite Longbow|Arco largo compuesto Flamógera|modificada|
|[hpMBDF24fDXi90pI.htm](abomination-vaults-bestiary-items/hpMBDF24fDXi90pI.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hr8UQcvJzWfTNGoH.htm](abomination-vaults-bestiary-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 5|Curación rápida planar 5|modificada|
|[hSFAedu25PZDak7p.htm](abomination-vaults-bestiary-items/hSFAedu25PZDak7p.htm)|Stench|Hedor|modificada|
|[hSGaU1UT8mzmPFWl.htm](abomination-vaults-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[HSgw6P66g1tETSK5.htm](abomination-vaults-bestiary-items/HSgw6P66g1tETSK5.htm)|Improvised Projectile|Proyectil improvisado|modificada|
|[HSib85eXEGK4xoXf.htm](abomination-vaults-bestiary-items/HSib85eXEGK4xoXf.htm)|Diabolic Quill|Pluma diabólica|modificada|
|[HtINDVVIov9yMEhV.htm](abomination-vaults-bestiary-items/HtINDVVIov9yMEhV.htm)|Site Bound|Ligado a una ubicación|modificada|
|[htwDT7obXSlNmWpU.htm](abomination-vaults-bestiary-items/htwDT7obXSlNmWpU.htm)|Longsword|Longsword|modificada|
|[Hu1SdyNVG4WJmNcx.htm](abomination-vaults-bestiary-items/Hu1SdyNVG4WJmNcx.htm)|Soul Spells|Hechizos de Alma|modificada|
|[huUPSBc3tD6r0B4N.htm](abomination-vaults-bestiary-items/huUPSBc3tD6r0B4N.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hvENSxQhO8lwlD1Y.htm](abomination-vaults-bestiary-items/hvENSxQhO8lwlD1Y.htm)|Commanding Aura|Orden imperiosa Aura|modificada|
|[hw9SLn99zC7mqeTR.htm](abomination-vaults-bestiary-items/hw9SLn99zC7mqeTR.htm)|Stupor Poison|Veneno de estupor|modificada|
|[hwoQ4oeElvvImKgQ.htm](abomination-vaults-bestiary-items/hwoQ4oeElvvImKgQ.htm)|Jaws|Fauces|modificada|
|[hwqvhAAeZ5lZGags.htm](abomination-vaults-bestiary-items/hwqvhAAeZ5lZGags.htm)|Landbound|Landbound|modificada|
|[HytGc8m6N3nhaxiO.htm](abomination-vaults-bestiary-items/HytGc8m6N3nhaxiO.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[hZ7GydaiKm8Egcxd.htm](abomination-vaults-bestiary-items/hZ7GydaiKm8Egcxd.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[I41NEY2RdcZSR1ft.htm](abomination-vaults-bestiary-items/I41NEY2RdcZSR1ft.htm)|Apocalypse Beam (from Undead Uprising)|Rayo Apocalipsis (de Sublevación de muertos vivientes)|modificada|
|[i4Z53PbHjjlFshli.htm](abomination-vaults-bestiary-items/i4Z53PbHjjlFshli.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[I4ZxiQRjcJ5rCCvp.htm](abomination-vaults-bestiary-items/I4ZxiQRjcJ5rCCvp.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[i68AZkuUsYHLKLWd.htm](abomination-vaults-bestiary-items/i68AZkuUsYHLKLWd.htm)|Jaws|Fauces|modificada|
|[i7kJARfn6bo7YXTy.htm](abomination-vaults-bestiary-items/i7kJARfn6bo7YXTy.htm)|Staff|Báculo|modificada|
|[I7Zg9nDgRBezMT5l.htm](abomination-vaults-bestiary-items/I7Zg9nDgRBezMT5l.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iaTfEZ50lrTlV5Ku.htm](abomination-vaults-bestiary-items/iaTfEZ50lrTlV5Ku.htm)|Personality Fragments|Fragmentos de personalidad|modificada|
|[IcalXla4INxKnCPk.htm](abomination-vaults-bestiary-items/IcalXla4INxKnCPk.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[IDg4tkvpzcQVtLok.htm](abomination-vaults-bestiary-items/IDg4tkvpzcQVtLok.htm)|Bloodline Spells|Conjuros de linaje|modificada|
|[IFp4VK6X80nXiTJu.htm](abomination-vaults-bestiary-items/IFp4VK6X80nXiTJu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[igyVJ2r1r0X9HNP8.htm](abomination-vaults-bestiary-items/igyVJ2r1r0X9HNP8.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[IHcY0GaB5rFEpIJT.htm](abomination-vaults-bestiary-items/IHcY0GaB5rFEpIJT.htm)|Site Bound|Ligado a una ubicación|modificada|
|[IiFLZ6cZ0dZBAcee.htm](abomination-vaults-bestiary-items/IiFLZ6cZ0dZBAcee.htm)|Squirming Embrace|Abrazo Retorcido|modificada|
|[iIggw0JAX98xzjTY.htm](abomination-vaults-bestiary-items/iIggw0JAX98xzjTY.htm)|Suction|Succión|modificada|
|[ijACdC2w6CsLO9Ks.htm](abomination-vaults-bestiary-items/ijACdC2w6CsLO9Ks.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[iJcL8jitqKxQqnx8.htm](abomination-vaults-bestiary-items/iJcL8jitqKxQqnx8.htm)|Grab|Agarrado|modificada|
|[IJTHmP61FLx42vVO.htm](abomination-vaults-bestiary-items/IJTHmP61FLx42vVO.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[ikaHblTttb4ck8mr.htm](abomination-vaults-bestiary-items/ikaHblTttb4ck8mr.htm)|Negative Healing|Curación negativa|modificada|
|[ikOLQArHJ3ox5p0j.htm](abomination-vaults-bestiary-items/ikOLQArHJ3ox5p0j.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[ilL5pHw5IAPVwhHU.htm](abomination-vaults-bestiary-items/ilL5pHw5IAPVwhHU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IM2z6ZQvAhXIgpsu.htm](abomination-vaults-bestiary-items/IM2z6ZQvAhXIgpsu.htm)|Confusing Confrontation|Enfrentamiento confuso|modificada|
|[IopEu45YRMrLuNzn.htm](abomination-vaults-bestiary-items/IopEu45YRMrLuNzn.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[iOvO0dFhneixzLvw.htm](abomination-vaults-bestiary-items/iOvO0dFhneixzLvw.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[Ipe3znrfNfiAvRCl.htm](abomination-vaults-bestiary-items/Ipe3znrfNfiAvRCl.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[IPH7UJLRufEnfg32.htm](abomination-vaults-bestiary-items/IPH7UJLRufEnfg32.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[iPIBtZVZ5TaPEFp6.htm](abomination-vaults-bestiary-items/iPIBtZVZ5TaPEFp6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IR6YUO9Gdy6kxYo5.htm](abomination-vaults-bestiary-items/IR6YUO9Gdy6kxYo5.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Irss8fPxNC91YnDe.htm](abomination-vaults-bestiary-items/Irss8fPxNC91YnDe.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[IS350crGK3U3r6SG.htm](abomination-vaults-bestiary-items/IS350crGK3U3r6SG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IuwOAyVqNq1LTtkA.htm](abomination-vaults-bestiary-items/IuwOAyVqNq1LTtkA.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[iWHE4a4vmetksWd7.htm](abomination-vaults-bestiary-items/iWHE4a4vmetksWd7.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[iwppReF9hEA7igPO.htm](abomination-vaults-bestiary-items/iwppReF9hEA7igPO.htm)|Jaws|Fauces|modificada|
|[iY5D7YKAlye71zpy.htm](abomination-vaults-bestiary-items/iY5D7YKAlye71zpy.htm)|Scythe|Guadaña|modificada|
|[iZFB4xCQhaumJQTC.htm](abomination-vaults-bestiary-items/iZFB4xCQhaumJQTC.htm)|Death Motes|Motas de Muerte|modificada|
|[j0uUpSfzz62wPB0o.htm](abomination-vaults-bestiary-items/j0uUpSfzz62wPB0o.htm)|Web Trap|Trampa telara|modificada|
|[j1CfaIqiOES2OC3Y.htm](abomination-vaults-bestiary-items/j1CfaIqiOES2OC3Y.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[J1YjLfTIk9vqgvRy.htm](abomination-vaults-bestiary-items/J1YjLfTIk9vqgvRy.htm)|Malevolent Possession|Posesión maléfica.|modificada|
|[J3s8xl5VZ1K6APbE.htm](abomination-vaults-bestiary-items/J3s8xl5VZ1K6APbE.htm)|Envenom Weapon|Arma Envenom|modificada|
|[J7wgD2uQamiPxQFV.htm](abomination-vaults-bestiary-items/J7wgD2uQamiPxQFV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[J82EHqf8yKe7pMEq.htm](abomination-vaults-bestiary-items/J82EHqf8yKe7pMEq.htm)|Tremorsense 30 feet|Tremorsense 30 pies|modificada|
|[J9CjxTTQRdZozTW4.htm](abomination-vaults-bestiary-items/J9CjxTTQRdZozTW4.htm)|Right of Inspection|Derecho de inspección|modificada|
|[j9vgTM71zw7kdn4T.htm](abomination-vaults-bestiary-items/j9vgTM71zw7kdn4T.htm)|+2 Status to All Saves vs. Disease and Poison|+2 situación a todas las salvaciones contra enfermedad y veneno.|modificada|
|[JEA54JYtf51IHnRH.htm](abomination-vaults-bestiary-items/JEA54JYtf51IHnRH.htm)|Kukri|Kukri|modificada|
|[jEONedyoxPV0Uymr.htm](abomination-vaults-bestiary-items/jEONedyoxPV0Uymr.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[Jf35tOxuG3lkWtlq.htm](abomination-vaults-bestiary-items/Jf35tOxuG3lkWtlq.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[JfQnN4zwkzfUcPK4.htm](abomination-vaults-bestiary-items/JfQnN4zwkzfUcPK4.htm)|Dread Flickering|Fluctuación pavorosa|modificada|
|[jG0R8rvY0cpJadHK.htm](abomination-vaults-bestiary-items/jG0R8rvY0cpJadHK.htm)|Fist|Puño|modificada|
|[JgGnEERmYAIJ8Hbc.htm](abomination-vaults-bestiary-items/JgGnEERmYAIJ8Hbc.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[jIbBcQcAKDXMVJzj.htm](abomination-vaults-bestiary-items/jIbBcQcAKDXMVJzj.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[jl8TRoHirSz5Q9Mt.htm](abomination-vaults-bestiary-items/jl8TRoHirSz5Q9Mt.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[JlHuStw8Ji1vTXMi.htm](abomination-vaults-bestiary-items/JlHuStw8Ji1vTXMi.htm)|Shoulder to Shoulder|Hombro con Hombro|modificada|
|[JMc7RSkWQTx1DQOo.htm](abomination-vaults-bestiary-items/JMc7RSkWQTx1DQOo.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[JMhh3dDSE8195tKc.htm](abomination-vaults-bestiary-items/JMhh3dDSE8195tKc.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[Jn4tXrVKX66VzX4O.htm](abomination-vaults-bestiary-items/Jn4tXrVKX66VzX4O.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[JPj4ayUtkVtkvYCy.htm](abomination-vaults-bestiary-items/JPj4ayUtkVtkvYCy.htm)|Consume Light|Consumir Luz|modificada|
|[JQ3FZtUvvt6RRRXF.htm](abomination-vaults-bestiary-items/JQ3FZtUvvt6RRRXF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jqJroJQSIk3eYFSA.htm](abomination-vaults-bestiary-items/jqJroJQSIk3eYFSA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[jr6lRjG0ZeZgW116.htm](abomination-vaults-bestiary-items/jr6lRjG0ZeZgW116.htm)|Soul Scream|Soul Scream|modificada|
|[jS78EFSX9XswID7b.htm](abomination-vaults-bestiary-items/jS78EFSX9XswID7b.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[JSPBoCm4ZF48k1Qw.htm](abomination-vaults-bestiary-items/JSPBoCm4ZF48k1Qw.htm)|Quick Consumption|Consumo Rápido|modificada|
|[JuEXPvUywaRE7BnJ.htm](abomination-vaults-bestiary-items/JuEXPvUywaRE7BnJ.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[JxMkMIPvp1ErxI39.htm](abomination-vaults-bestiary-items/JxMkMIPvp1ErxI39.htm)|Negative Healing|Curación negativa|modificada|
|[k0izMsqD3Kbrn6oD.htm](abomination-vaults-bestiary-items/k0izMsqD3Kbrn6oD.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[k1oAvKH4tGhwjVr5.htm](abomination-vaults-bestiary-items/k1oAvKH4tGhwjVr5.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[k2PMRhPXO7eus3sg.htm](abomination-vaults-bestiary-items/k2PMRhPXO7eus3sg.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[kaDLtkZWXkQssjdZ.htm](abomination-vaults-bestiary-items/kaDLtkZWXkQssjdZ.htm)|Adjust Shape|Adjust Shape|modificada|
|[kbtVRkQHlP5ap1MJ.htm](abomination-vaults-bestiary-items/kbtVRkQHlP5ap1MJ.htm)|Blowgun|Blowgun|modificada|
|[kCh7O0sugL7oCnHv.htm](abomination-vaults-bestiary-items/kCh7O0sugL7oCnHv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kcKV8At9w2cFZz1T.htm](abomination-vaults-bestiary-items/kcKV8At9w2cFZz1T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kcRvfKLlgq9sdmsj.htm](abomination-vaults-bestiary-items/kcRvfKLlgq9sdmsj.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[kFeAtCbycBHyIGcR.htm](abomination-vaults-bestiary-items/kFeAtCbycBHyIGcR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[khhjOlnkRw4wzweC.htm](abomination-vaults-bestiary-items/khhjOlnkRw4wzweC.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[khwh43CDMROkIfgf.htm](abomination-vaults-bestiary-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[kJAxB9HM1zsPkPDC.htm](abomination-vaults-bestiary-items/kJAxB9HM1zsPkPDC.htm)|Tentacle|Tentáculo|modificada|
|[KkhGaZRJP4cnQSOE.htm](abomination-vaults-bestiary-items/KkhGaZRJP4cnQSOE.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[koHANYfuvrmBm3Bc.htm](abomination-vaults-bestiary-items/koHANYfuvrmBm3Bc.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[kppTcx5TylWkpfF1.htm](abomination-vaults-bestiary-items/kppTcx5TylWkpfF1.htm)|Light Vulnerability|Vulnerabilidad a la luz|modificada|
|[kUtQvCE4ePTqjluS.htm](abomination-vaults-bestiary-items/kUtQvCE4ePTqjluS.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Kvdwxj4PR8US0RUi.htm](abomination-vaults-bestiary-items/Kvdwxj4PR8US0RUi.htm)|Shortbow|Arco corto|modificada|
|[kwMV2uj1jCdvIm7J.htm](abomination-vaults-bestiary-items/kwMV2uj1jCdvIm7J.htm)|Beard|Barba|modificada|
|[kzKe7Q6umOWzUJdN.htm](abomination-vaults-bestiary-items/kzKe7Q6umOWzUJdN.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[l18iYVUpNZjCrvzG.htm](abomination-vaults-bestiary-items/l18iYVUpNZjCrvzG.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[l2sX5eZSrv6H5EQ4.htm](abomination-vaults-bestiary-items/l2sX5eZSrv6H5EQ4.htm)|Sibilant Whispers|Sibilant Whispers|modificada|
|[L8z6SDNKm57PB639.htm](abomination-vaults-bestiary-items/L8z6SDNKm57PB639.htm)|Claw|Garra|modificada|
|[LAMXu0pzRQpS1K2p.htm](abomination-vaults-bestiary-items/LAMXu0pzRQpS1K2p.htm)|Infused Items|Equipos infundidos|modificada|
|[LbkW4zjvlyreuGX1.htm](abomination-vaults-bestiary-items/LbkW4zjvlyreuGX1.htm)|Fangs|Colmillos|modificada|
|[LBtHMsDMDaIqyB7z.htm](abomination-vaults-bestiary-items/LBtHMsDMDaIqyB7z.htm)|Radiant Touch|Toque Radiante|modificada|
|[LcG0o3PUyemEv5oK.htm](abomination-vaults-bestiary-items/LcG0o3PUyemEv5oK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ley6LXSrjfxUxe7T.htm](abomination-vaults-bestiary-items/ley6LXSrjfxUxe7T.htm)|Ward Contract|Custodiar contrato|modificada|
|[LfqV1vPTJG0eLpId.htm](abomination-vaults-bestiary-items/LfqV1vPTJG0eLpId.htm)|+2 Status to All Saves vs. Disease and Poison|+2 situación a todas las salvaciones contra enfermedad y veneno.|modificada|
|[LGBzL3vv4QNzTFJ1.htm](abomination-vaults-bestiary-items/LGBzL3vv4QNzTFJ1.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[LGGpAgf32ukoqCFX.htm](abomination-vaults-bestiary-items/LGGpAgf32ukoqCFX.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[lGZkyb82Eeg7M96l.htm](abomination-vaults-bestiary-items/lGZkyb82Eeg7M96l.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[lIpwTEFoDElIQYqN.htm](abomination-vaults-bestiary-items/lIpwTEFoDElIQYqN.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[lj2wYgitpa7YQNCt.htm](abomination-vaults-bestiary-items/lj2wYgitpa7YQNCt.htm)|Bouncing Slam|Bouncing Slam|modificada|
|[LkqFbyPhl9cNZFyq.htm](abomination-vaults-bestiary-items/LkqFbyPhl9cNZFyq.htm)|Constant Spells|Constant Spells|modificada|
|[LLnHvtR9xniSgkOM.htm](abomination-vaults-bestiary-items/LLnHvtR9xniSgkOM.htm)|Tentacle|Tentáculo|modificada|
|[LNbjPm5a6CTl4lko.htm](abomination-vaults-bestiary-items/LNbjPm5a6CTl4lko.htm)|Infernal Eye|Infernal Eye|modificada|
|[Lnc74mXrMbKNeEpe.htm](abomination-vaults-bestiary-items/Lnc74mXrMbKNeEpe.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[LOeBCSVBauL8spor.htm](abomination-vaults-bestiary-items/LOeBCSVBauL8spor.htm)|Black Smear Poison|Veneno de mancha negra|modificada|
|[Loj0LD68R46zC1Tx.htm](abomination-vaults-bestiary-items/Loj0LD68R46zC1Tx.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[lQZGx8hWnzcG2R6l.htm](abomination-vaults-bestiary-items/lQZGx8hWnzcG2R6l.htm)|Dazzling Brilliance|Dazzling Brilliance|modificada|
|[LrviNgUYKAYQl4i9.htm](abomination-vaults-bestiary-items/LrviNgUYKAYQl4i9.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[LRVsZzLZZ4VCKy1h.htm](abomination-vaults-bestiary-items/LRVsZzLZZ4VCKy1h.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LS15ASdQlOSeQgPH.htm](abomination-vaults-bestiary-items/LS15ASdQlOSeQgPH.htm)|Jaws|Fauces|modificada|
|[lTaNtOa2ywL54YZJ.htm](abomination-vaults-bestiary-items/lTaNtOa2ywL54YZJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ltvMN7K0itu2l3WZ.htm](abomination-vaults-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|Colmillos de Serpiente|modificada|
|[lVTp2LJQAZzKNaIY.htm](abomination-vaults-bestiary-items/lVTp2LJQAZzKNaIY.htm)|Storm of Blades|Storm of Blades|modificada|
|[LwBTFYgYLRf8gXzt.htm](abomination-vaults-bestiary-items/LwBTFYgYLRf8gXzt.htm)|Spell Reflection|Reflejar conjuros|modificada|
|[LwePStc0eHhM9L5l.htm](abomination-vaults-bestiary-items/LwePStc0eHhM9L5l.htm)|Jaws|Fauces|modificada|
|[lZIMAORXwdcjvy4H.htm](abomination-vaults-bestiary-items/lZIMAORXwdcjvy4H.htm)|Distracting Shot|Tiro de distracción|modificada|
|[LZjWlCLb24UlRVSo.htm](abomination-vaults-bestiary-items/LZjWlCLb24UlRVSo.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[m25t326n1Ovt0t5U.htm](abomination-vaults-bestiary-items/m25t326n1Ovt0t5U.htm)|Negative Healing|Curación negativa|modificada|
|[m2rnilFYTvOg589S.htm](abomination-vaults-bestiary-items/m2rnilFYTvOg589S.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[M7sZToYvFNrig8TH.htm](abomination-vaults-bestiary-items/M7sZToYvFNrig8TH.htm)|Constant Spells|Constant Spells|modificada|
|[M7VqYd1K43RsOInY.htm](abomination-vaults-bestiary-items/M7VqYd1K43RsOInY.htm)|Grim Glimmering|Grim Glimmering|modificada|
|[mA7HIkvCJVI5uu1m.htm](abomination-vaults-bestiary-items/mA7HIkvCJVI5uu1m.htm)|Death Light|Luz de la Muerte|modificada|
|[mC6jPQk1bRvlBdvh.htm](abomination-vaults-bestiary-items/mC6jPQk1bRvlBdvh.htm)|Repeating Hand Crossbow|Ballesta de mano de repetición|modificada|
|[McjPEAjdnUEE3tx6.htm](abomination-vaults-bestiary-items/McjPEAjdnUEE3tx6.htm)|Shauth Blade|Shauth Blade|modificada|
|[MDttImPA5a6D9APk.htm](abomination-vaults-bestiary-items/MDttImPA5a6D9APk.htm)|Feed on Magic|Alimentarse de la magia|modificada|
|[mEGMPmEBPm6Apx3y.htm](abomination-vaults-bestiary-items/mEGMPmEBPm6Apx3y.htm)|Wearying Touch|Wearing Touch|modificada|
|[melBrcTFYaugSkrF.htm](abomination-vaults-bestiary-items/melBrcTFYaugSkrF.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[MeT5lDeI05fBDoW3.htm](abomination-vaults-bestiary-items/MeT5lDeI05fBDoW3.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[mgluD1d7bZoq5JBg.htm](abomination-vaults-bestiary-items/mgluD1d7bZoq5JBg.htm)|Fangs|Colmillos|modificada|
|[MhPq4EOlBLppwQeu.htm](abomination-vaults-bestiary-items/MhPq4EOlBLppwQeu.htm)|Cocytan Filth|Cocytan Filth|modificada|
|[MJktJ33e2ejK5I1o.htm](abomination-vaults-bestiary-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|Muerdemuerde|modificada|
|[ml69ZMPwkVsbCTC7.htm](abomination-vaults-bestiary-items/ml69ZMPwkVsbCTC7.htm)|Storm of Tentacles|Tormenta de Tentáculos|modificada|
|[mlcDOwUA9F64jE1Y.htm](abomination-vaults-bestiary-items/mlcDOwUA9F64jE1Y.htm)|Mouth|Boca|modificada|
|[mlcNFWnWIzRngDv0.htm](abomination-vaults-bestiary-items/mlcNFWnWIzRngDv0.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[mmfsGHgB3hcCPClW.htm](abomination-vaults-bestiary-items/mmfsGHgB3hcCPClW.htm)|Apocalypse Beam (from Monster)|Rayo Apocalipsis (de Monstruo)|modificada|
|[MnAW3prtfTZxnv7T.htm](abomination-vaults-bestiary-items/MnAW3prtfTZxnv7T.htm)|Magic Item Mastery|Magic Item Mastery|modificada|
|[mQfS4yOFYXyHwwas.htm](abomination-vaults-bestiary-items/mQfS4yOFYXyHwwas.htm)|Surprise Attacker|Atacante por sorpresa|modificada|
|[mS0Mzbjj7mazrPnr.htm](abomination-vaults-bestiary-items/mS0Mzbjj7mazrPnr.htm)|Death Flame|Llama fúnebre de la muerte|modificada|
|[mSAztmtFp4kF0SdP.htm](abomination-vaults-bestiary-items/mSAztmtFp4kF0SdP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[MsljHqXO1LMWSpK4.htm](abomination-vaults-bestiary-items/MsljHqXO1LMWSpK4.htm)|Spit|Escupe|modificada|
|[mSS5Nol0cifKQmxM.htm](abomination-vaults-bestiary-items/mSS5Nol0cifKQmxM.htm)|Knockdown|Derribo|modificada|
|[mUJY9mXKNFT1G9Ri.htm](abomination-vaults-bestiary-items/mUJY9mXKNFT1G9Ri.htm)|Negative Healing|Curación negativa|modificada|
|[myXt3SwrT2L8VMW4.htm](abomination-vaults-bestiary-items/myXt3SwrT2L8VMW4.htm)|Stygian Guardian|Guardián Estigio|modificada|
|[MZelOTUvBCjq2zbz.htm](abomination-vaults-bestiary-items/MZelOTUvBCjq2zbz.htm)|Draft Contract|Redactar contrato|modificada|
|[MZkr0UEdl93FQyzP.htm](abomination-vaults-bestiary-items/MZkr0UEdl93FQyzP.htm)|Bloom|Bloom|modificada|
|[N0G4vzuAThb89i17.htm](abomination-vaults-bestiary-items/N0G4vzuAThb89i17.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[N1zdZTTLXEp3x5eJ.htm](abomination-vaults-bestiary-items/N1zdZTTLXEp3x5eJ.htm)|Wicked Bite|Muerdemuerde|modificada|
|[n2BHwPEdFYp07VvG.htm](abomination-vaults-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|Pseudópodo|modificada|
|[n2w1C4HwH3QTV7dw.htm](abomination-vaults-bestiary-items/n2w1C4HwH3QTV7dw.htm)|Swift Leap|Salto veloz|modificada|
|[N7x5OQAAhVdxvKnV.htm](abomination-vaults-bestiary-items/N7x5OQAAhVdxvKnV.htm)|Death Shadows|Sombras fúnebres|modificada|
|[N9qNd1NMgEeyNwcF.htm](abomination-vaults-bestiary-items/N9qNd1NMgEeyNwcF.htm)|Spell Deflection|Spell Deflection|modificada|
|[NAX30nf7nlVx5vTo.htm](abomination-vaults-bestiary-items/NAX30nf7nlVx5vTo.htm)|Cheek Pouches|Carrilleras|modificada|
|[nBbFutvNlcdf1cgZ.htm](abomination-vaults-bestiary-items/nBbFutvNlcdf1cgZ.htm)|Drown|Drown|modificada|
|[NBuJOqDORlJbI3m9.htm](abomination-vaults-bestiary-items/NBuJOqDORlJbI3m9.htm)|Wisp Form|Wisp Form|modificada|
|[NcZMn3pf8nbMeY8x.htm](abomination-vaults-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nETM5TEYK8ufocn0.htm](abomination-vaults-bestiary-items/nETM5TEYK8ufocn0.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NflnAPRnfgBSgygz.htm](abomination-vaults-bestiary-items/NflnAPRnfgBSgygz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[NiDXROLnOZXSEpNC.htm](abomination-vaults-bestiary-items/NiDXROLnOZXSEpNC.htm)|Jaws|Fauces|modificada|
|[NJwXFlfXwTfxdk7U.htm](abomination-vaults-bestiary-items/NJwXFlfXwTfxdk7U.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[nkgGXuCapE0gTVxH.htm](abomination-vaults-bestiary-items/nkgGXuCapE0gTVxH.htm)|Negative Healing|Curación negativa|modificada|
|[nLKHrHv3rcodZfOf.htm](abomination-vaults-bestiary-items/nLKHrHv3rcodZfOf.htm)|Seugathi Venom|Seugathi Venom|modificada|
|[nQoYz6B0Wx0OjS4e.htm](abomination-vaults-bestiary-items/nQoYz6B0Wx0OjS4e.htm)|Undulating Step|Paso ondulante|modificada|
|[NrJvXEDlrKMp5Eh0.htm](abomination-vaults-bestiary-items/NrJvXEDlrKMp5Eh0.htm)|Fist|Puño|modificada|
|[nSxXMYPDUloleNgn.htm](abomination-vaults-bestiary-items/nSxXMYPDUloleNgn.htm)|Warding Shove|Empujar Warding|modificada|
|[NTAt5R3Zz1e8OAHf.htm](abomination-vaults-bestiary-items/NTAt5R3Zz1e8OAHf.htm)|War Flail|War Flail|modificada|
|[NTmAGY84p18KWTa8.htm](abomination-vaults-bestiary-items/NTmAGY84p18KWTa8.htm)|+2 Status To All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[NuYsILwFjvqlwWPp.htm](abomination-vaults-bestiary-items/NuYsILwFjvqlwWPp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nvaQh9xjojojnL27.htm](abomination-vaults-bestiary-items/nvaQh9xjojojnL27.htm)|Attack Now!|¡Ataca ahora!|modificada|
|[NVdOFyf2EscpiksE.htm](abomination-vaults-bestiary-items/NVdOFyf2EscpiksE.htm)|Battle Axe|Hacha de batalla|modificada|
|[Nwtd1G94Ikab4Ejf.htm](abomination-vaults-bestiary-items/Nwtd1G94Ikab4Ejf.htm)|Scuttling Attack|Scuttling Attack|modificada|
|[NwuVG96DJON1Txro.htm](abomination-vaults-bestiary-items/NwuVG96DJON1Txro.htm)|Constant Spells|Constant Spells|modificada|
|[NXk6QjCfO8eW6Txp.htm](abomination-vaults-bestiary-items/NXk6QjCfO8eW6Txp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[NyXCVjhHwWZtpfOI.htm](abomination-vaults-bestiary-items/NyXCVjhHwWZtpfOI.htm)|Consume Flesh|Consumir carne|modificada|
|[NzFY8G1K3Mfksj2F.htm](abomination-vaults-bestiary-items/NzFY8G1K3Mfksj2F.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[nzWpj7yo8PguGBFG.htm](abomination-vaults-bestiary-items/nzWpj7yo8PguGBFG.htm)|Bite|Muerdemuerde|modificada|
|[O2CT9C7oM6YOLZNX.htm](abomination-vaults-bestiary-items/O2CT9C7oM6YOLZNX.htm)|Glow|Glow|modificada|
|[o40ddeHAtx8cJ0uY.htm](abomination-vaults-bestiary-items/o40ddeHAtx8cJ0uY.htm)|Mind Lash|Mind Lash|modificada|
|[oAOkRgFO0POryMdo.htm](abomination-vaults-bestiary-items/oAOkRgFO0POryMdo.htm)|Percussive Reverberation|Reverberación Percusiva|modificada|
|[obfPUaVoDmOFchMR.htm](abomination-vaults-bestiary-items/obfPUaVoDmOFchMR.htm)|Jaws|Fauces|modificada|
|[od2Tpdqgyx5vuTIw.htm](abomination-vaults-bestiary-items/od2Tpdqgyx5vuTIw.htm)|Poison Weapon|Arma envenenada|modificada|
|[Oevi96zftiaBjAOH.htm](abomination-vaults-bestiary-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[oeWKbVvK3lP66J9I.htm](abomination-vaults-bestiary-items/oeWKbVvK3lP66J9I.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[OHEkKMalMOyLMyRM.htm](abomination-vaults-bestiary-items/OHEkKMalMOyLMyRM.htm)|Draining Touch|Toque Drenante|modificada|
|[ohL0OcnbucaAroHs.htm](abomination-vaults-bestiary-items/ohL0OcnbucaAroHs.htm)|Deft Evasion|Hábil Evasión|modificada|
|[OIcVAsJUARlaypEm.htm](abomination-vaults-bestiary-items/OIcVAsJUARlaypEm.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Okdzr1mvuSTh4c4K.htm](abomination-vaults-bestiary-items/Okdzr1mvuSTh4c4K.htm)|Battle Lute|Laúd de batalla|modificada|
|[oKefVQZfmh0iqIhc.htm](abomination-vaults-bestiary-items/oKefVQZfmh0iqIhc.htm)|No MAP|No MAP|modificada|
|[OKsFbAOCubRBtq1O.htm](abomination-vaults-bestiary-items/OKsFbAOCubRBtq1O.htm)|Occult Ward|Custodia ocultista|modificada|
|[OksJxFiBFjS3xboM.htm](abomination-vaults-bestiary-items/OksJxFiBFjS3xboM.htm)|Mark for Death|Marcar para morir|modificada|
|[oKuYToU9Nv9ALIIv.htm](abomination-vaults-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[OlnYDBwZvO3CeHG3.htm](abomination-vaults-bestiary-items/OlnYDBwZvO3CeHG3.htm)|Force Blast|Ráfaga de Fuerza|modificada|
|[oLOpCzPI98yXuw1u.htm](abomination-vaults-bestiary-items/oLOpCzPI98yXuw1u.htm)|Consume Flesh|Consumir carne|modificada|
|[om8dBOucNQt5iWeZ.htm](abomination-vaults-bestiary-items/om8dBOucNQt5iWeZ.htm)|Domain Spells|Hechizos de Dominio|modificada|
|[oMcchH9BcuFxkNKA.htm](abomination-vaults-bestiary-items/oMcchH9BcuFxkNKA.htm)|Haunted Lighthouse|Haunted Lighthouse|modificada|
|[OMfPkRtQLgK6lUE8.htm](abomination-vaults-bestiary-items/OMfPkRtQLgK6lUE8.htm)|Gatekeeper's Will|Voluntad del guardián de la puerta|modificada|
|[onlqduhJCOKTLTGj.htm](abomination-vaults-bestiary-items/onlqduhJCOKTLTGj.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[OOe8PxQr9xXaWkvc.htm](abomination-vaults-bestiary-items/OOe8PxQr9xXaWkvc.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[OOpmi6uvkCP25AwD.htm](abomination-vaults-bestiary-items/OOpmi6uvkCP25AwD.htm)|Bravery|Bravery|modificada|
|[OpUEqqVcx7trG4mu.htm](abomination-vaults-bestiary-items/OpUEqqVcx7trG4mu.htm)|Kukri|Kukri|modificada|
|[OqYpi6JullFs4WMU.htm](abomination-vaults-bestiary-items/OqYpi6JullFs4WMU.htm)|Negative Healing|Curación negativa|modificada|
|[OSiq1CXChzTT7udW.htm](abomination-vaults-bestiary-items/OSiq1CXChzTT7udW.htm)|Spectral Corruption|Corrupción espectral|modificada|
|[osvhHIWUUPwyQszY.htm](abomination-vaults-bestiary-items/osvhHIWUUPwyQszY.htm)|Tremorsense 30 feet|Tremorsense 30 pies|modificada|
|[oviUeLIMpj5EZvoL.htm](abomination-vaults-bestiary-items/oviUeLIMpj5EZvoL.htm)|Negative Healing|Curación negativa|modificada|
|[OvlSSz4vVMTtlzue.htm](abomination-vaults-bestiary-items/OvlSSz4vVMTtlzue.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[owQaoWxiXKdJdh64.htm](abomination-vaults-bestiary-items/owQaoWxiXKdJdh64.htm)|Infernal Wound|Herida infernal|modificada|
|[OxVsLGBZ8k9rqQxY.htm](abomination-vaults-bestiary-items/OxVsLGBZ8k9rqQxY.htm)|Jaws|Fauces|modificada|
|[P0CkVE8kq3xALSWx.htm](abomination-vaults-bestiary-items/P0CkVE8kq3xALSWx.htm)|Negative Healing|Curación negativa|modificada|
|[P1ggE8aYgCQOaac1.htm](abomination-vaults-bestiary-items/P1ggE8aYgCQOaac1.htm)|Death Burst|Death Burst|modificada|
|[P2nBJI904M5iiMWb.htm](abomination-vaults-bestiary-items/P2nBJI904M5iiMWb.htm)|Shortsword|Espada corta|modificada|
|[P7RUoae9G01AaAHd.htm](abomination-vaults-bestiary-items/P7RUoae9G01AaAHd.htm)|Negative Healing|Curación negativa|modificada|
|[p9kCNA8Mq9cXm23t.htm](abomination-vaults-bestiary-items/p9kCNA8Mq9cXm23t.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pb8PZlMfMR2NS3Vt.htm](abomination-vaults-bestiary-items/pb8PZlMfMR2NS3Vt.htm)|War Leader|Líder de Guerra|modificada|
|[PBrXRWLJMqALmC7c.htm](abomination-vaults-bestiary-items/PBrXRWLJMqALmC7c.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[PCXa7Z3d10U79Quj.htm](abomination-vaults-bestiary-items/PCXa7Z3d10U79Quj.htm)|Vengeful Anger|Furia vengativa|modificada|
|[PDBItJftGnhDq2dg.htm](abomination-vaults-bestiary-items/PDBItJftGnhDq2dg.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[PFH04FMn9c52nnhf.htm](abomination-vaults-bestiary-items/PFH04FMn9c52nnhf.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Pgdez88Zqu0dqgGI.htm](abomination-vaults-bestiary-items/Pgdez88Zqu0dqgGI.htm)|Despairing Cry|Grito desesperado|modificada|
|[pi0jucwlFYIo5mn8.htm](abomination-vaults-bestiary-items/pi0jucwlFYIo5mn8.htm)|Discorporate|Discorporate|modificada|
|[pJ6eBDUVUixzX9UL.htm](abomination-vaults-bestiary-items/pJ6eBDUVUixzX9UL.htm)|Animate Chains|Animar Cadenas|modificada|
|[PJPuhtPtTcNoLZS4.htm](abomination-vaults-bestiary-items/PJPuhtPtTcNoLZS4.htm)|Tamchal Chakram|Tamchal Chakram|modificada|
|[PkDvZBCHiGCocxUn.htm](abomination-vaults-bestiary-items/PkDvZBCHiGCocxUn.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[pKPJ4u9g0HQyU7b6.htm](abomination-vaults-bestiary-items/pKPJ4u9g0HQyU7b6.htm)|Furious Fusillade|Bombardeo furioso|modificada|
|[PM9U1Ca1DQ2ZA7ZJ.htm](abomination-vaults-bestiary-items/PM9U1Ca1DQ2ZA7ZJ.htm)|Negative Healing|Curación negativa|modificada|
|[POLjXIhCSeQBrvcV.htm](abomination-vaults-bestiary-items/POLjXIhCSeQBrvcV.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[pozH1aOlixvNcvU6.htm](abomination-vaults-bestiary-items/pozH1aOlixvNcvU6.htm)|Burn Knowledge|Quemar el conocimiento|modificada|
|[PpFajj7OukFhYId7.htm](abomination-vaults-bestiary-items/PpFajj7OukFhYId7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Pr87C99kuBwyUE2x.htm](abomination-vaults-bestiary-items/Pr87C99kuBwyUE2x.htm)|Spirit Sight (Precise) 30 feet|Vista espiritual (precisa) 30 pies.|modificada|
|[pRRtkR99srCWUO1j.htm](abomination-vaults-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|Masa Corrosiva|modificada|
|[PrsRCxi6j7SHJeMu.htm](abomination-vaults-bestiary-items/PrsRCxi6j7SHJeMu.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[pViUeTjTLhTubzOw.htm](abomination-vaults-bestiary-items/pViUeTjTLhTubzOw.htm)|Coven|Coven|modificada|
|[PVrWJHEarOi9LFbD.htm](abomination-vaults-bestiary-items/PVrWJHEarOi9LFbD.htm)|Rope Snare|Atrapar con cuerda|modificada|
|[PXA2uFcT0VRiQkBO.htm](abomination-vaults-bestiary-items/PXA2uFcT0VRiQkBO.htm)|Fearful Curse|Maldición Temerosa|modificada|
|[pyBAuiHwkCVhaTe8.htm](abomination-vaults-bestiary-items/pyBAuiHwkCVhaTe8.htm)|Shock|Electrizante|modificada|
|[pZ4b9ZPNlMtoF8P7.htm](abomination-vaults-bestiary-items/pZ4b9ZPNlMtoF8P7.htm)|Bite|Muerdemuerde|modificada|
|[q0kSVZllWCY1CwD2.htm](abomination-vaults-bestiary-items/q0kSVZllWCY1CwD2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Q43gFqCqgbv9y5VO.htm](abomination-vaults-bestiary-items/Q43gFqCqgbv9y5VO.htm)|Skirmish Strike|Golpe hostigador|modificada|
|[Q4ib52AxEVe6nHUx.htm](abomination-vaults-bestiary-items/Q4ib52AxEVe6nHUx.htm)|Gauntlight Beam|Gauntlight Beam|modificada|
|[Q4NBsMEwBKrHlvNK.htm](abomination-vaults-bestiary-items/Q4NBsMEwBKrHlvNK.htm)|Oily Scales|Escamas aceitosas|modificada|
|[Q4RY5CXdMVoB93xD.htm](abomination-vaults-bestiary-items/Q4RY5CXdMVoB93xD.htm)|Underground Stride|Zancada subterránea|modificada|
|[Q8iuI7tt5UHoTXnP.htm](abomination-vaults-bestiary-items/Q8iuI7tt5UHoTXnP.htm)|Swift Leap|Salto veloz|modificada|
|[QaDq4G70qrufNlS5.htm](abomination-vaults-bestiary-items/QaDq4G70qrufNlS5.htm)|Sapping Squeeze|Estrujamiento agotador|modificada|
|[QaqXp54jr3aKQjWN.htm](abomination-vaults-bestiary-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|Curación negativa|modificada|
|[Qc2AY3uXl9fGeDBe.htm](abomination-vaults-bestiary-items/Qc2AY3uXl9fGeDBe.htm)|Hampering Slash|Tajo entorpecedor|modificada|
|[Qc5SGmGRuhj2vMYt.htm](abomination-vaults-bestiary-items/Qc5SGmGRuhj2vMYt.htm)|Loose Bones|Loose Bones|modificada|
|[QcW3MprdYsKVeRT7.htm](abomination-vaults-bestiary-items/QcW3MprdYsKVeRT7.htm)|Flood of Despair|Flood of Despair|modificada|
|[QdYWcouo4wSAer3W.htm](abomination-vaults-bestiary-items/QdYWcouo4wSAer3W.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[qFopdg6xXJDmE4k7.htm](abomination-vaults-bestiary-items/qFopdg6xXJDmE4k7.htm)|Cavern Distortion|Cavern Distortion|modificada|
|[qh8LFXVLWjILfZZx.htm](abomination-vaults-bestiary-items/qh8LFXVLWjILfZZx.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[QHc3i64UJqwenXAH.htm](abomination-vaults-bestiary-items/QHc3i64UJqwenXAH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Qhmy1EmaoDYJuVdZ.htm](abomination-vaults-bestiary-items/Qhmy1EmaoDYJuVdZ.htm)|Grab|Agarrado|modificada|
|[QJJWPGE3n8ZduC9D.htm](abomination-vaults-bestiary-items/QJJWPGE3n8ZduC9D.htm)|Light the Living Wick|Enciende la mecha viva|modificada|
|[Qkqj9KzvAaoHkxjH.htm](abomination-vaults-bestiary-items/Qkqj9KzvAaoHkxjH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Qkr3ZHA5WjD7gYCY.htm](abomination-vaults-bestiary-items/Qkr3ZHA5WjD7gYCY.htm)|Warding Script|Warding Script|modificada|
|[QlygOvoO8v1q3qZ1.htm](abomination-vaults-bestiary-items/QlygOvoO8v1q3qZ1.htm)|Swarming Stance|Posición de Enjambre|modificada|
|[Qmj5gqP1YgGLr1W3.htm](abomination-vaults-bestiary-items/Qmj5gqP1YgGLr1W3.htm)|Rhoka Sword|Espada Rhoka|modificada|
|[QOG4x5d8jOfeGb6l.htm](abomination-vaults-bestiary-items/QOG4x5d8jOfeGb6l.htm)|Negative Healing|Curación negativa|modificada|
|[QPbTNI3BxNTdGPuH.htm](abomination-vaults-bestiary-items/QPbTNI3BxNTdGPuH.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[QrVRCf0qabKVREzS.htm](abomination-vaults-bestiary-items/QrVRCf0qabKVREzS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QSEVW6fObs2AVajU.htm](abomination-vaults-bestiary-items/QSEVW6fObs2AVajU.htm)|Corrupting Gaze|Corrupting Gaze|modificada|
|[qtILk42sO65XzDmW.htm](abomination-vaults-bestiary-items/qtILk42sO65XzDmW.htm)|Consume Flesh|Consumir carne|modificada|
|[qTIxm70Zd6ETxfgu.htm](abomination-vaults-bestiary-items/qTIxm70Zd6ETxfgu.htm)|Insightful Swing|Insightful Swing|modificada|
|[qtqSaGbHHzYr1Zvx.htm](abomination-vaults-bestiary-items/qtqSaGbHHzYr1Zvx.htm)|Rusty Chains|Rusty Chains|modificada|
|[Qw2zvLnJQFja72D2.htm](abomination-vaults-bestiary-items/Qw2zvLnJQFja72D2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qwwxBH4SCVdtmTb2.htm](abomination-vaults-bestiary-items/qwwxBH4SCVdtmTb2.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[qykfSUOQXBbtu4gM.htm](abomination-vaults-bestiary-items/qykfSUOQXBbtu4gM.htm)|Frenzied Attack|Frenzied Attack|modificada|
|[qyWEf4ehAOLXTLjf.htm](abomination-vaults-bestiary-items/qyWEf4ehAOLXTLjf.htm)|Seugathi Venom|Seugathi Venom|modificada|
|[QZup3O3zcKH6TFZd.htm](abomination-vaults-bestiary-items/QZup3O3zcKH6TFZd.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[R0x59cKzEcWsxbbV.htm](abomination-vaults-bestiary-items/R0x59cKzEcWsxbbV.htm)|Longsword|Longsword|modificada|
|[R2IrxhTRU85GEr92.htm](abomination-vaults-bestiary-items/R2IrxhTRU85GEr92.htm)|Swallow Whole|Engullir Todo|modificada|
|[R6nrltKyDBZG2PNf.htm](abomination-vaults-bestiary-items/R6nrltKyDBZG2PNf.htm)|All-Around Vision|All-Around Vision|modificada|
|[RatED2kNCjAiVv2a.htm](abomination-vaults-bestiary-items/RatED2kNCjAiVv2a.htm)|Fist|Puño|modificada|
|[RBjfyeeaXd5gEMNe.htm](abomination-vaults-bestiary-items/RBjfyeeaXd5gEMNe.htm)|Curse of the Werewolf|Maldición del Hombre Lobo|modificada|
|[Rc6ZoOmMG3mlyrrV.htm](abomination-vaults-bestiary-items/Rc6ZoOmMG3mlyrrV.htm)|Spore Explosion|Explosión de Esporas.|modificada|
|[rdEPYGmtMUuJ7I3P.htm](abomination-vaults-bestiary-items/rdEPYGmtMUuJ7I3P.htm)|Fearful Strike|Golpe Temeroso|modificada|
|[re67Wr8G5ybq0iEY.htm](abomination-vaults-bestiary-items/re67Wr8G5ybq0iEY.htm)|Survivor's Nourishment|Alimentar al superviviente|modificada|
|[RGaUdIjwMpgnNYGg.htm](abomination-vaults-bestiary-items/RGaUdIjwMpgnNYGg.htm)|Pain Starvation|Hambruna de dolor|modificada|
|[rGX35xgDAZROYYQF.htm](abomination-vaults-bestiary-items/rGX35xgDAZROYYQF.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[RhLJUtGDkZup2Y2e.htm](abomination-vaults-bestiary-items/RhLJUtGDkZup2Y2e.htm)|Fleshy Slap|Bofetada carnosa|modificada|
|[Ri0eYhcAj6M7CX7f.htm](abomination-vaults-bestiary-items/Ri0eYhcAj6M7CX7f.htm)|All-Around Vision|All-Around Vision|modificada|
|[rJLYYTfUR7ew1R0A.htm](abomination-vaults-bestiary-items/rJLYYTfUR7ew1R0A.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[rLUQOqHBgpcUnpXn.htm](abomination-vaults-bestiary-items/rLUQOqHBgpcUnpXn.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[rq016ZULtzeq5Jdo.htm](abomination-vaults-bestiary-items/rq016ZULtzeq5Jdo.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[RQSPQLwfW9A5Ijew.htm](abomination-vaults-bestiary-items/RQSPQLwfW9A5Ijew.htm)|Apocalypse Beam (from Burning City)|Rayo Apocalipsis (de Ciudad Ardiente)|modificada|
|[Rt1kYgHWRK0u7WPS.htm](abomination-vaults-bestiary-items/Rt1kYgHWRK0u7WPS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rV1k0lGcAvoytfnj.htm](abomination-vaults-bestiary-items/rV1k0lGcAvoytfnj.htm)|Tendril|Tendril|modificada|
|[RwS0OFipezRfdrZG.htm](abomination-vaults-bestiary-items/RwS0OFipezRfdrZG.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[rzTC3Hz3hT9xd3W9.htm](abomination-vaults-bestiary-items/rzTC3Hz3hT9xd3W9.htm)|Consume Tattooed Flesh|Consumir carne tatuada|modificada|
|[S0eUokfR9NCiHTkD.htm](abomination-vaults-bestiary-items/S0eUokfR9NCiHTkD.htm)|Wolf Empathy|Wolf Empathy|modificada|
|[S0PyywlB6YgiEU7L.htm](abomination-vaults-bestiary-items/S0PyywlB6YgiEU7L.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[S31ljAQqgmPf1CMj.htm](abomination-vaults-bestiary-items/S31ljAQqgmPf1CMj.htm)|Constrict (Grabbed by Claws only)|Constrict (Agarrado por garras solamente)|modificada|
|[s48Fu4ttbbhaVZUd.htm](abomination-vaults-bestiary-items/s48Fu4ttbbhaVZUd.htm)|Jaws|Fauces|modificada|
|[S4gQomqdPJUG2IPl.htm](abomination-vaults-bestiary-items/S4gQomqdPJUG2IPl.htm)|Acid Flask|Frasco de ácido|modificada|
|[s57i0xdwt99HF3Tp.htm](abomination-vaults-bestiary-items/s57i0xdwt99HF3Tp.htm)|Trident|Trident|modificada|
|[S9nPU36oRplQB6mn.htm](abomination-vaults-bestiary-items/S9nPU36oRplQB6mn.htm)|Opportune Step|Paso Oportuno|modificada|
|[SaMUZistsZa0Bskd.htm](abomination-vaults-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|All-Around Vision|modificada|
|[sAuhwgcHiN60xIJT.htm](abomination-vaults-bestiary-items/sAuhwgcHiN60xIJT.htm)|Swarming|Enjambre|modificada|
|[SawmEH90X7bn6dTY.htm](abomination-vaults-bestiary-items/SawmEH90X7bn6dTY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SC9Vfc2U7mtfdimN.htm](abomination-vaults-bestiary-items/SC9Vfc2U7mtfdimN.htm)|Death Flame|Llama fúnebre de la muerte|modificada|
|[sdsJsKNzbvVK8R4I.htm](abomination-vaults-bestiary-items/sdsJsKNzbvVK8R4I.htm)|Wooden Chair|Silla de madera|modificada|
|[sEiZ1blIoaIiKSvz.htm](abomination-vaults-bestiary-items/sEiZ1blIoaIiKSvz.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[SeJXe222cuooTJUx.htm](abomination-vaults-bestiary-items/SeJXe222cuooTJUx.htm)|Split|Split|modificada|
|[SHNbQciOGHNt8z1c.htm](abomination-vaults-bestiary-items/SHNbQciOGHNt8z1c.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[sIBET4O7QFkxNkyF.htm](abomination-vaults-bestiary-items/sIBET4O7QFkxNkyF.htm)|Swamp Stride|Swamp Stride|modificada|
|[SiVeimRpwflBeUiq.htm](abomination-vaults-bestiary-items/SiVeimRpwflBeUiq.htm)|Consume Masterpiece|Consumir Obra Maestra|modificada|
|[sJW19BG0UI1qi3ct.htm](abomination-vaults-bestiary-items/sJW19BG0UI1qi3ct.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Ska9kA0i2bCxOPKc.htm](abomination-vaults-bestiary-items/Ska9kA0i2bCxOPKc.htm)|Hateful Memories|Hateful Memories|modificada|
|[smJkeUn705exUfuJ.htm](abomination-vaults-bestiary-items/smJkeUn705exUfuJ.htm)|Negative Healing|Curación negativa|modificada|
|[SNJ0RSCREyP9YYKY.htm](abomination-vaults-bestiary-items/SNJ0RSCREyP9YYKY.htm)|Infernal Wound|Herida infernal|modificada|
|[snOrUVs5f8v8gtuP.htm](abomination-vaults-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[sslSR9PEr8cWdVop.htm](abomination-vaults-bestiary-items/sslSR9PEr8cWdVop.htm)|Steal Shadow|Robar sombra|modificada|
|[STdfpuDxhppjlflP.htm](abomination-vaults-bestiary-items/STdfpuDxhppjlflP.htm)|Skillful Catch|Captura habilidosa|modificada|
|[SuNMXHWTyC2lF2Pa.htm](abomination-vaults-bestiary-items/SuNMXHWTyC2lF2Pa.htm)|Claw|Garra|modificada|
|[sVvkzILqsBCfcw03.htm](abomination-vaults-bestiary-items/sVvkzILqsBCfcw03.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[sXswvc2qcns84U92.htm](abomination-vaults-bestiary-items/sXswvc2qcns84U92.htm)|Dagger|Daga|modificada|
|[T108hwN1OhACYQdQ.htm](abomination-vaults-bestiary-items/T108hwN1OhACYQdQ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[t1m1XIVqXFEnCTKv.htm](abomination-vaults-bestiary-items/t1m1XIVqXFEnCTKv.htm)|Mindfog Aura|Mindfog Aura|modificada|
|[t4jPFFTR7Eur2Nml.htm](abomination-vaults-bestiary-items/t4jPFFTR7Eur2Nml.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[t4QXi08ulbzmSfHt.htm](abomination-vaults-bestiary-items/t4QXi08ulbzmSfHt.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[t6KHJCiZXdNJnqxB.htm](abomination-vaults-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|Maestro de armas|modificada|
|[T8KOcWxiqo3StVBo.htm](abomination-vaults-bestiary-items/T8KOcWxiqo3StVBo.htm)|Spit|Escupe|modificada|
|[t9ReCb126y5KXuru.htm](abomination-vaults-bestiary-items/t9ReCb126y5KXuru.htm)|Opportune Witchflame|Opportune Witchflame|modificada|
|[T9tAbwnyo4ZmiWkk.htm](abomination-vaults-bestiary-items/T9tAbwnyo4ZmiWkk.htm)|Fervent Command|Orden imperiosa|modificada|
|[taKy6ih4jlHzjBsw.htm](abomination-vaults-bestiary-items/taKy6ih4jlHzjBsw.htm)|Sapping Squeeze|Estrujamiento agotador|modificada|
|[TBlIXTNQSQ1e3bEo.htm](abomination-vaults-bestiary-items/TBlIXTNQSQ1e3bEo.htm)|Snout|Hocico|modificada|
|[tce3ovXnnblqwD77.htm](abomination-vaults-bestiary-items/tce3ovXnnblqwD77.htm)|Shred Flesh|Destrozar Carne|modificada|
|[TdzSpCHa1QM7Auhy.htm](abomination-vaults-bestiary-items/TdzSpCHa1QM7Auhy.htm)|Distracting Declaration|Declaración de distracción|modificada|
|[TfHuj8LvtfwsXfXQ.htm](abomination-vaults-bestiary-items/TfHuj8LvtfwsXfXQ.htm)|Tentacle Arm|Brazo Tentáculo|modificada|
|[thNxqBSaHQwe1kqj.htm](abomination-vaults-bestiary-items/thNxqBSaHQwe1kqj.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[tIvtO7TbZgSNlr3F.htm](abomination-vaults-bestiary-items/tIvtO7TbZgSNlr3F.htm)|Constant Spells|Constant Spells|modificada|
|[tjQCRx8UN4LQPnwJ.htm](abomination-vaults-bestiary-items/tjQCRx8UN4LQPnwJ.htm)|Witchflame Kindling|Le de llama de bruja|modificada|
|[tJRkLCUMgqynhzQ9.htm](abomination-vaults-bestiary-items/tJRkLCUMgqynhzQ9.htm)|Infested Shadow|Sombra Infestada|modificada|
|[tjRomYaatwAgMUvv.htm](abomination-vaults-bestiary-items/tjRomYaatwAgMUvv.htm)|Partially Technological|Parcialmente tecnológico|modificada|
|[TKCoF7GXS2ldg658.htm](abomination-vaults-bestiary-items/TKCoF7GXS2ldg658.htm)|Jaws|Fauces|modificada|
|[TM2S9CZ5bTyxKkXp.htm](abomination-vaults-bestiary-items/TM2S9CZ5bTyxKkXp.htm)|Unnerving Gaze|Unnerving Gaze|modificada|
|[tNfhf1JnOxOHfY86.htm](abomination-vaults-bestiary-items/tNfhf1JnOxOHfY86.htm)|Envenom Weapon|Arma Envenom|modificada|
|[TPGcPPTzpn0NMPGl.htm](abomination-vaults-bestiary-items/TPGcPPTzpn0NMPGl.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Tq0WVPPxrkpFbZn4.htm](abomination-vaults-bestiary-items/Tq0WVPPxrkpFbZn4.htm)|Fist|Puño|modificada|
|[tqSmdxFVDmY2CLns.htm](abomination-vaults-bestiary-items/tqSmdxFVDmY2CLns.htm)|Heavy Aura|Heavy Aura|modificada|
|[tW1jcJOAZ3y2pThe.htm](abomination-vaults-bestiary-items/tW1jcJOAZ3y2pThe.htm)|Starknife|Starknife|modificada|
|[tWnV5NxscBllgsSs.htm](abomination-vaults-bestiary-items/tWnV5NxscBllgsSs.htm)|Magic Sense|Sentido mágico|modificada|
|[Tx79k8XzQHlqc3ew.htm](abomination-vaults-bestiary-items/Tx79k8XzQHlqc3ew.htm)|Dagger|Daga|modificada|
|[TzpdMinu4WoEyAY9.htm](abomination-vaults-bestiary-items/TzpdMinu4WoEyAY9.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[U0pfy4tbvu2Y9KYj.htm](abomination-vaults-bestiary-items/U0pfy4tbvu2Y9KYj.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[U83VZh2S8gyX5Kvp.htm](abomination-vaults-bestiary-items/U83VZh2S8gyX5Kvp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[u8Tqev49a0Iwqy80.htm](abomination-vaults-bestiary-items/u8Tqev49a0Iwqy80.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[u9UlDj5Q35Cp8lmR.htm](abomination-vaults-bestiary-items/u9UlDj5Q35Cp8lmR.htm)|All-Around Vision|All-Around Vision|modificada|
|[UAaoMzfNeh44Gojl.htm](abomination-vaults-bestiary-items/UAaoMzfNeh44Gojl.htm)|Bone Shard|Bone Shard|modificada|
|[UakPnn8jYjY9gCdo.htm](abomination-vaults-bestiary-items/UakPnn8jYjY9gCdo.htm)|Rope|Cuerda|modificada|
|[UbtJOXzzsAmXD5rI.htm](abomination-vaults-bestiary-items/UbtJOXzzsAmXD5rI.htm)|Swarm Shape|Forma de Enjambre|modificada|
|[ufvpAL31F9ZaHsEo.htm](abomination-vaults-bestiary-items/ufvpAL31F9ZaHsEo.htm)|Bounding Swarm|Plaga saltarina|modificada|
|[UI4pCPb4yqFcryJ6.htm](abomination-vaults-bestiary-items/UI4pCPb4yqFcryJ6.htm)|+2 Status To All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[UIrt3wtjXEhvQtEL.htm](abomination-vaults-bestiary-items/UIrt3wtjXEhvQtEL.htm)|Powerful Stench|Hedor intenso|modificada|
|[uItSOCTzoCrocBon.htm](abomination-vaults-bestiary-items/uItSOCTzoCrocBon.htm)|Corpse Sense (Precise) 30 feet|Sentido del cadáver (precisión) 30 pies.|modificada|
|[UK5IIgsnGnQ6Oh6K.htm](abomination-vaults-bestiary-items/UK5IIgsnGnQ6Oh6K.htm)|Negative Healing|Curación negativa|modificada|
|[uKvKe5cNgTA0yIxl.htm](abomination-vaults-bestiary-items/uKvKe5cNgTA0yIxl.htm)|Overpowering Jaws|Fauces abrumadoras|modificada|
|[UluC5rRvQl0yriAj.htm](abomination-vaults-bestiary-items/UluC5rRvQl0yriAj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[UP3MBQg1Px5Bkp7a.htm](abomination-vaults-bestiary-items/UP3MBQg1Px5Bkp7a.htm)|Dagger|Daga|modificada|
|[upJaEvHTKGpsW57H.htm](abomination-vaults-bestiary-items/upJaEvHTKGpsW57H.htm)|Flaming Longsword|Espada Larga Flamígera|modificada|
|[UQ4kfO1kRCsEtz0l.htm](abomination-vaults-bestiary-items/UQ4kfO1kRCsEtz0l.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[USCoVo5LJNs12zB4.htm](abomination-vaults-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|Oscurecerse|modificada|
|[Usk5KyF9A2RQaJt2.htm](abomination-vaults-bestiary-items/Usk5KyF9A2RQaJt2.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[uuED3LvzlH3CHS3S.htm](abomination-vaults-bestiary-items/uuED3LvzlH3CHS3S.htm)|Sudden Throw|Lanzamiento Repentino|modificada|
|[UV3vQl5jrXwr1wQh.htm](abomination-vaults-bestiary-items/UV3vQl5jrXwr1wQh.htm)|Undulating Step|Paso ondulante|modificada|
|[uWu9iZkKF8LUjUoe.htm](abomination-vaults-bestiary-items/uWu9iZkKF8LUjUoe.htm)|+2 Status to All Saves vs. Disease and Poison|+2 situación a todas las salvaciones contra enfermedad y veneno.|modificada|
|[UYMJ2nbr7f8EgOHc.htm](abomination-vaults-bestiary-items/UYMJ2nbr7f8EgOHc.htm)|Negative Recovery|Recuperación Negativa|modificada|
|[V0Zof9p6jaQUrOl0.htm](abomination-vaults-bestiary-items/V0Zof9p6jaQUrOl0.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[v1N2fY4anfmaOUr3.htm](abomination-vaults-bestiary-items/v1N2fY4anfmaOUr3.htm)|Jaws|Fauces|modificada|
|[V5Ok21wotF3DQNBZ.htm](abomination-vaults-bestiary-items/V5Ok21wotF3DQNBZ.htm)|Someone is Watching|Someone is Watching|modificada|
|[VB81WcGgMngyyh3E.htm](abomination-vaults-bestiary-items/VB81WcGgMngyyh3E.htm)|Slink in Shadows|Escabullirse entre las sombras|modificada|
|[vBp53lYo9D4gq1jh.htm](abomination-vaults-bestiary-items/vBp53lYo9D4gq1jh.htm)|All-Around Vision|All-Around Vision|modificada|
|[VDa8nmlZXUFf1Y7z.htm](abomination-vaults-bestiary-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|Fauces|modificada|
|[vdPooFytAas2OzRw.htm](abomination-vaults-bestiary-items/vdPooFytAas2OzRw.htm)|Reloading Trick|Truco para recargar|modificada|
|[VgEW660oBhpCgGPZ.htm](abomination-vaults-bestiary-items/VgEW660oBhpCgGPZ.htm)|Claw|Garra|modificada|
|[VGPs2FUB6OC8igcY.htm](abomination-vaults-bestiary-items/VGPs2FUB6OC8igcY.htm)|Web|Telara|modificada|
|[vH2uvgGZ6JCaWrIl.htm](abomination-vaults-bestiary-items/vH2uvgGZ6JCaWrIl.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[VH3uvuMCquGbT6Wx.htm](abomination-vaults-bestiary-items/VH3uvuMCquGbT6Wx.htm)|Regeneration 10 (Deactivated by Good or Silver)|Regeneración 10 (Desactivado por Bien o Plata)|modificada|
|[vhcL0ctFDySpLekK.htm](abomination-vaults-bestiary-items/vhcL0ctFDySpLekK.htm)|Negative Healing|Curación negativa|modificada|
|[VJFdV0JcVaaqVlyL.htm](abomination-vaults-bestiary-items/VJFdV0JcVaaqVlyL.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[vKdhNyXTKHDMUfAc.htm](abomination-vaults-bestiary-items/vKdhNyXTKHDMUfAc.htm)|Flames of Fury|Llamas de la furia|modificada|
|[VkmU3zCSF1tm61N6.htm](abomination-vaults-bestiary-items/VkmU3zCSF1tm61N6.htm)|Command Confusion|Dirigir confusión de orden imperiosa.|modificada|
|[VkXxpkRx6E7dyCI4.htm](abomination-vaults-bestiary-items/VkXxpkRx6E7dyCI4.htm)|Vermin Empathy|Empatía con las sabandijas|modificada|
|[vMlj7QxxkX61zODC.htm](abomination-vaults-bestiary-items/vMlj7QxxkX61zODC.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[VNqQVxfzCgmliiPq.htm](abomination-vaults-bestiary-items/VNqQVxfzCgmliiPq.htm)|Tail|Tail|modificada|
|[Vo6AHVWsAnxqkwLW.htm](abomination-vaults-bestiary-items/Vo6AHVWsAnxqkwLW.htm)|Drover's Band|Drover's Band|modificada|
|[vO90s2UOixYIPRSk.htm](abomination-vaults-bestiary-items/vO90s2UOixYIPRSk.htm)|Jaws|Fauces|modificada|
|[VOrtrf3kSz1oIQkq.htm](abomination-vaults-bestiary-items/VOrtrf3kSz1oIQkq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VPyIMfFo4j5NMPBG.htm](abomination-vaults-bestiary-items/VPyIMfFo4j5NMPBG.htm)|Dagger|Daga|modificada|
|[Vq09pyw5K2mb3YeT.htm](abomination-vaults-bestiary-items/Vq09pyw5K2mb3YeT.htm)|Longsword|Longsword|modificada|
|[VqTDcQNmGBPYQ6d3.htm](abomination-vaults-bestiary-items/VqTDcQNmGBPYQ6d3.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[VQXGCkNvdRLDuyH1.htm](abomination-vaults-bestiary-items/VQXGCkNvdRLDuyH1.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Vr79KdOVhzih6EMo.htm](abomination-vaults-bestiary-items/Vr79KdOVhzih6EMo.htm)|Rapier|Estoque|modificada|
|[vTyv5W3SkeNgrprr.htm](abomination-vaults-bestiary-items/vTyv5W3SkeNgrprr.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[VUmbLmAZWhr6cx2b.htm](abomination-vaults-bestiary-items/VUmbLmAZWhr6cx2b.htm)|Longsword|Longsword|modificada|
|[VwIv1y01lqATWPRT.htm](abomination-vaults-bestiary-items/VwIv1y01lqATWPRT.htm)|Necrotic Decay|Putrefacción necrótica|modificada|
|[VWWosu7Wvs4yAKZy.htm](abomination-vaults-bestiary-items/VWWosu7Wvs4yAKZy.htm)|Grab|Agarrado|modificada|
|[VXZ8QpZ1EGC0hg4D.htm](abomination-vaults-bestiary-items/VXZ8QpZ1EGC0hg4D.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[Vy2pPgrGJ3SguoOn.htm](abomination-vaults-bestiary-items/Vy2pPgrGJ3SguoOn.htm)|Shadow Hand|Mano de Sombra|modificada|
|[VzobMtkoa82xICPm.htm](abomination-vaults-bestiary-items/VzobMtkoa82xICPm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W0VQcpug1ZFVNZox.htm](abomination-vaults-bestiary-items/W0VQcpug1ZFVNZox.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[W2nnShEx9f5pWTjq.htm](abomination-vaults-bestiary-items/W2nnShEx9f5pWTjq.htm)|Pewter Mug|Taza de peltre|modificada|
|[W4yMtkEVcpKOM2tE.htm](abomination-vaults-bestiary-items/W4yMtkEVcpKOM2tE.htm)|Broad Swipe|Vaivén amplio|modificada|
|[W7NJfdI36Wt2pLfo.htm](abomination-vaults-bestiary-items/W7NJfdI36Wt2pLfo.htm)|Camouflaged Step|Camuflaje Paso|modificada|
|[w8TbxNNTVBH6j4jP.htm](abomination-vaults-bestiary-items/w8TbxNNTVBH6j4jP.htm)|Shootist's Draw|Shootist's Draw|modificada|
|[WA3qxd5nl6en4BPF.htm](abomination-vaults-bestiary-items/WA3qxd5nl6en4BPF.htm)|Spore Cloud|Nube de esporas|modificada|
|[WAo4NMhw8o3YpQGq.htm](abomination-vaults-bestiary-items/WAo4NMhw8o3YpQGq.htm)|Wicked Bite|Muerdemuerde|modificada|
|[WCcurB9iEOCcU58H.htm](abomination-vaults-bestiary-items/WCcurB9iEOCcU58H.htm)|Paralysis|Parálisis|modificada|
|[wChn2PEw9GYziq2n.htm](abomination-vaults-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|Glow|modificada|
|[wcJ7xyyDuDpmKxyt.htm](abomination-vaults-bestiary-items/wcJ7xyyDuDpmKxyt.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[WersQyXKosWIiXXl.htm](abomination-vaults-bestiary-items/WersQyXKosWIiXXl.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[wImzz2Ucpz9gqh2J.htm](abomination-vaults-bestiary-items/wImzz2Ucpz9gqh2J.htm)|Claw|Garra|modificada|
|[wjbGJGSRPWZ49qZd.htm](abomination-vaults-bestiary-items/wjbGJGSRPWZ49qZd.htm)|Spittle|Escupitajo|modificada|
|[wKmUr6VixtVPXpXR.htm](abomination-vaults-bestiary-items/wKmUr6VixtVPXpXR.htm)|Tamchal Chakram|Tamchal Chakram|modificada|
|[WKywaKpRvPLFLVvG.htm](abomination-vaults-bestiary-items/WKywaKpRvPLFLVvG.htm)|Mosh|Rocanrolear|modificada|
|[wnV3crCWLBRnytqk.htm](abomination-vaults-bestiary-items/wnV3crCWLBRnytqk.htm)|Death Flame|Llama fúnebre de la muerte|modificada|
|[WsGIMLD86LgMNUWg.htm](abomination-vaults-bestiary-items/WsGIMLD86LgMNUWg.htm)|Claw|Garra|modificada|
|[WU1jExSnDYhncwTy.htm](abomination-vaults-bestiary-items/WU1jExSnDYhncwTy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WVPQDcIaUdkMfawy.htm](abomination-vaults-bestiary-items/WVPQDcIaUdkMfawy.htm)|Rapier|Estoque|modificada|
|[wWrvIQM5fOYBSMRQ.htm](abomination-vaults-bestiary-items/wWrvIQM5fOYBSMRQ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[wWufyBbAG3JXOfoE.htm](abomination-vaults-bestiary-items/wWufyBbAG3JXOfoE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wyQSk60Np0UavJiY.htm](abomination-vaults-bestiary-items/wyQSk60Np0UavJiY.htm)|Stasis Field|Stasis Field|modificada|
|[x13RVTp5gtE3XOTa.htm](abomination-vaults-bestiary-items/x13RVTp5gtE3XOTa.htm)|Defensive Shooter|Tirador Defensivo|modificada|
|[x2mDaJtGWiO1eS56.htm](abomination-vaults-bestiary-items/x2mDaJtGWiO1eS56.htm)|Counterfeit Haunting|Counterfeit Haunting|modificada|
|[x3yCpybINerCQY3Q.htm](abomination-vaults-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|Toque Corrosivo|modificada|
|[x97vWevUTEMqeOCJ.htm](abomination-vaults-bestiary-items/x97vWevUTEMqeOCJ.htm)|Painsight|Painsight|modificada|
|[xA9OK9ybw4WLdQ8o.htm](abomination-vaults-bestiary-items/xA9OK9ybw4WLdQ8o.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xCCM8tyhLkWqYUpL.htm](abomination-vaults-bestiary-items/xCCM8tyhLkWqYUpL.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[XCfMmPXvB7LJznlU.htm](abomination-vaults-bestiary-items/XCfMmPXvB7LJznlU.htm)|Frost Composite Longbow|Arco largo compuesto de Gélida.|modificada|
|[XETiXceuUZEG0el1.htm](abomination-vaults-bestiary-items/XETiXceuUZEG0el1.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[XezgRZTc5EmXVys0.htm](abomination-vaults-bestiary-items/XezgRZTc5EmXVys0.htm)|Fist|Puño|modificada|
|[XfR063rnMuiow4bN.htm](abomination-vaults-bestiary-items/XfR063rnMuiow4bN.htm)|Opportune Step|Paso Oportuno|modificada|
|[xH6NrBw4bYEHU7e3.htm](abomination-vaults-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[xiTvpYtL10U6KNkT.htm](abomination-vaults-bestiary-items/xiTvpYtL10U6KNkT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XJ7A5MPGGNFZpxXP.htm](abomination-vaults-bestiary-items/XJ7A5MPGGNFZpxXP.htm)|Negative Healing|Curación negativa|modificada|
|[XjvtH7IilCnzJpIN.htm](abomination-vaults-bestiary-items/XjvtH7IilCnzJpIN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xk8dbRocKTe3GSWC.htm](abomination-vaults-bestiary-items/xk8dbRocKTe3GSWC.htm)|Spore Explosion|Explosión de Esporas.|modificada|
|[XKiDDCClxzUDYSO8.htm](abomination-vaults-bestiary-items/XKiDDCClxzUDYSO8.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[XmKUjz23s0dNFOYW.htm](abomination-vaults-bestiary-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|Podredumbre del pantano|modificada|
|[XNJnpNGQCkQD00v3.htm](abomination-vaults-bestiary-items/XNJnpNGQCkQD00v3.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[xoFC8JZ1B0deUW2k.htm](abomination-vaults-bestiary-items/xoFC8JZ1B0deUW2k.htm)|Magic Item Mastery|Magic Item Mastery|modificada|
|[xpJLgdCTRRKsEqi3.htm](abomination-vaults-bestiary-items/xpJLgdCTRRKsEqi3.htm)|Magic Item Mastery|Magic Item Mastery|modificada|
|[XplLVQ3DKSYJCuYL.htm](abomination-vaults-bestiary-items/XplLVQ3DKSYJCuYL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XrEqDkgs6eaCAzhM.htm](abomination-vaults-bestiary-items/XrEqDkgs6eaCAzhM.htm)|Wicked Bite|Muerdemuerde|modificada|
|[XsfO4kZKUmGSNSI0.htm](abomination-vaults-bestiary-items/XsfO4kZKUmGSNSI0.htm)|+1 Status to All Saves vs. Poison|+1 situación a todas las salvaciones contra veneno.|modificada|
|[xTCkHuwFUsvdVThS.htm](abomination-vaults-bestiary-items/xTCkHuwFUsvdVThS.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[XvsfsnZoPvQNu6Be.htm](abomination-vaults-bestiary-items/XvsfsnZoPvQNu6Be.htm)|Negative Healing|Curación negativa|modificada|
|[xVxy6iARZLipqA8h.htm](abomination-vaults-bestiary-items/xVxy6iARZLipqA8h.htm)|Command Confusion|Dirigir confusión de orden imperiosa.|modificada|
|[xWD7oLqr7RfHGQVH.htm](abomination-vaults-bestiary-items/xWD7oLqr7RfHGQVH.htm)|Bone Shard|Bone Shard|modificada|
|[xxK8rXRDWK5Rwj9i.htm](abomination-vaults-bestiary-items/xxK8rXRDWK5Rwj9i.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[xze5oFK48fMv2xBU.htm](abomination-vaults-bestiary-items/xze5oFK48fMv2xBU.htm)|Tentacle Transfer|Transferir a tentáculos|modificada|
|[XzUUZ1FoJXIkVqZu.htm](abomination-vaults-bestiary-items/XzUUZ1FoJXIkVqZu.htm)|Uncanny Tinker|Uncanny Trastear|modificada|
|[Y0gOm1Uz3dMu6Q4W.htm](abomination-vaults-bestiary-items/Y0gOm1Uz3dMu6Q4W.htm)|Claim Corpse|Claim Corpse|modificada|
|[Y0zIOXUUeYuBTRst.htm](abomination-vaults-bestiary-items/Y0zIOXUUeYuBTRst.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[y1F32N9fSld9NydJ.htm](abomination-vaults-bestiary-items/y1F32N9fSld9NydJ.htm)|Negative Healing|Curación negativa|modificada|
|[y4g9ZcRWSHKAxsUe.htm](abomination-vaults-bestiary-items/y4g9ZcRWSHKAxsUe.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[y4um9lgRgEVJYHeJ.htm](abomination-vaults-bestiary-items/y4um9lgRgEVJYHeJ.htm)|Paralysis|Parálisis|modificada|
|[Y6Rqq2ahFM07WKRm.htm](abomination-vaults-bestiary-items/Y6Rqq2ahFM07WKRm.htm)|Swarming Stance|Posición de Enjambre|modificada|
|[Y7vFj13afzJKpHws.htm](abomination-vaults-bestiary-items/Y7vFj13afzJKpHws.htm)|Pinning Chomp|Pinning Chomp|modificada|
|[y87UDyuJmwKsI7Tq.htm](abomination-vaults-bestiary-items/y87UDyuJmwKsI7Tq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[y8rBowLYE52m6Vb7.htm](abomination-vaults-bestiary-items/y8rBowLYE52m6Vb7.htm)|Go Dark|Oscurecerse|modificada|
|[Y8ZISA3GaUO4MtHh.htm](abomination-vaults-bestiary-items/Y8ZISA3GaUO4MtHh.htm)|Jaws|Fauces|modificada|
|[Y9Z7x3aVBCSbKMxB.htm](abomination-vaults-bestiary-items/Y9Z7x3aVBCSbKMxB.htm)|Grab|Agarrado|modificada|
|[yafzmxKBfx68zshb.htm](abomination-vaults-bestiary-items/yafzmxKBfx68zshb.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[yajcaYxlIfR8IeG6.htm](abomination-vaults-bestiary-items/yajcaYxlIfR8IeG6.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[Ybx41yIdQO59hKjK.htm](abomination-vaults-bestiary-items/Ybx41yIdQO59hKjK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[yCKSVMhk8eLrvD0h.htm](abomination-vaults-bestiary-items/yCKSVMhk8eLrvD0h.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yCvj5N1H04hmAYuS.htm](abomination-vaults-bestiary-items/yCvj5N1H04hmAYuS.htm)|Gas Release|Soltar gas|modificada|
|[yD0yYDGEVE1rfNJJ.htm](abomination-vaults-bestiary-items/yD0yYDGEVE1rfNJJ.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[yDdnC4S3nrhfHYil.htm](abomination-vaults-bestiary-items/yDdnC4S3nrhfHYil.htm)|Heat Mirage|Heat Mirage|modificada|
|[YE5eTfCKX1ANEEP7.htm](abomination-vaults-bestiary-items/YE5eTfCKX1ANEEP7.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[YEnM0GJZ6D2RdXBg.htm](abomination-vaults-bestiary-items/YEnM0GJZ6D2RdXBg.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[ygCGxbBzOI9gN0fC.htm](abomination-vaults-bestiary-items/ygCGxbBzOI9gN0fC.htm)|Knock It Away|Knock It Away|modificada|
|[yI8fil9Hp8Ob0BcY.htm](abomination-vaults-bestiary-items/yI8fil9Hp8Ob0BcY.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[yJdnNORoa1KtMM7W.htm](abomination-vaults-bestiary-items/yJdnNORoa1KtMM7W.htm)|Site Bound|Ligado a una ubicación|modificada|
|[yJVTcQFJpy86gPDG.htm](abomination-vaults-bestiary-items/yJVTcQFJpy86gPDG.htm)|Claw|Garra|modificada|
|[YK1tWx8RaI3guzkW.htm](abomination-vaults-bestiary-items/YK1tWx8RaI3guzkW.htm)|Warhammer|Warhammer|modificada|
|[ykVZX3vwjXXl9WoS.htm](abomination-vaults-bestiary-items/ykVZX3vwjXXl9WoS.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[YnER4NJf6wi05tio.htm](abomination-vaults-bestiary-items/YnER4NJf6wi05tio.htm)|Claw|Garra|modificada|
|[yNjPKC3nPRiaD957.htm](abomination-vaults-bestiary-items/yNjPKC3nPRiaD957.htm)|Dicing Scythes|Dicing Scythes|modificada|
|[yp7FDjO7715BHNPw.htm](abomination-vaults-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YsiUbsAWRxZMjaWA.htm](abomination-vaults-bestiary-items/YsiUbsAWRxZMjaWA.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[yTXiHzmRavI1BmKJ.htm](abomination-vaults-bestiary-items/yTXiHzmRavI1BmKJ.htm)|Rend|Rasgadura|modificada|
|[YuDlX3Kr7cepFAW4.htm](abomination-vaults-bestiary-items/YuDlX3Kr7cepFAW4.htm)|Feed on Despair|Alimentarse de Desesperación|modificada|
|[YUgXFNB4xn08S3Cf.htm](abomination-vaults-bestiary-items/YUgXFNB4xn08S3Cf.htm)|Club|Club|modificada|
|[yUMwDlPA7DiVRv5u.htm](abomination-vaults-bestiary-items/yUMwDlPA7DiVRv5u.htm)|Negative Healing|Curación negativa|modificada|
|[YuXrcTfctRYHmw90.htm](abomination-vaults-bestiary-items/YuXrcTfctRYHmw90.htm)|Glow|Glow|modificada|
|[yvKEo91RnhVFPZ5L.htm](abomination-vaults-bestiary-items/yvKEo91RnhVFPZ5L.htm)|Negative Healing|Curación negativa|modificada|
|[YvmZQz32k7hFcqzM.htm](abomination-vaults-bestiary-items/YvmZQz32k7hFcqzM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YwUDfdiKYGDgdz8Q.htm](abomination-vaults-bestiary-items/YwUDfdiKYGDgdz8Q.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[yxXEQaBGVF13c0rW.htm](abomination-vaults-bestiary-items/yxXEQaBGVF13c0rW.htm)|Dagger|Daga|modificada|
|[Z60YSknQUrMbR4mB.htm](abomination-vaults-bestiary-items/Z60YSknQUrMbR4mB.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[z6U85IjlJZGBsdcA.htm](abomination-vaults-bestiary-items/z6U85IjlJZGBsdcA.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[z9bTah65DoASEDhP.htm](abomination-vaults-bestiary-items/z9bTah65DoASEDhP.htm)|Shortsword|Espada corta|modificada|
|[za8ix1WE9HbNfHvg.htm](abomination-vaults-bestiary-items/za8ix1WE9HbNfHvg.htm)|Grab|Agarrado|modificada|
|[zadQdUq4eeiDSHSq.htm](abomination-vaults-bestiary-items/zadQdUq4eeiDSHSq.htm)|Shadow Spawn|Sombra Spawn|modificada|
|[zALaGtcb55Te1IcS.htm](abomination-vaults-bestiary-items/zALaGtcb55Te1IcS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Zb3D43B4UbusNqAe.htm](abomination-vaults-bestiary-items/Zb3D43B4UbusNqAe.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[zb4lD1qa1tSyLe20.htm](abomination-vaults-bestiary-items/zb4lD1qa1tSyLe20.htm)|Claw|Garra|modificada|
|[ZCynz5sJ6bYK6fY6.htm](abomination-vaults-bestiary-items/ZCynz5sJ6bYK6fY6.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZfdrndtEZTKXbrq0.htm](abomination-vaults-bestiary-items/ZfdrndtEZTKXbrq0.htm)|Red Ruin Stance|Posición de la Ruina Roja|modificada|
|[zGg5a7ALqF7zVFCa.htm](abomination-vaults-bestiary-items/zGg5a7ALqF7zVFCa.htm)|Ectoplasmic Web|Telara ectoplasmatica|modificada|
|[zGXZKhk95VPsSdKN.htm](abomination-vaults-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|Serpientes mordedoras|modificada|
|[zjVbARImBBAx6EKv.htm](abomination-vaults-bestiary-items/zjVbARImBBAx6EKv.htm)|Vile Touch|Toque Vil|modificada|
|[ZkF8meq1sB3uUIcI.htm](abomination-vaults-bestiary-items/ZkF8meq1sB3uUIcI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zKQUDZzcDDJHuOiB.htm](abomination-vaults-bestiary-items/zKQUDZzcDDJHuOiB.htm)|Paralysis|Parálisis|modificada|
|[zm8POWGccsjyfuPN.htm](abomination-vaults-bestiary-items/zm8POWGccsjyfuPN.htm)|Bouncing Crush|Aplastar rebotando|modificada|
|[ZnFe1AJx67Diyerp.htm](abomination-vaults-bestiary-items/ZnFe1AJx67Diyerp.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[zNFSrrQNB1kgV8Go.htm](abomination-vaults-bestiary-items/zNFSrrQNB1kgV8Go.htm)|Stay in the Fight|Permanecer en la Lucha|modificada|
|[ZqBQutg0Cvs5YcxR.htm](abomination-vaults-bestiary-items/ZqBQutg0Cvs5YcxR.htm)|Negative Healing|Curación negativa|modificada|
|[zQUhdB7kvudfGv5Z.htm](abomination-vaults-bestiary-items/zQUhdB7kvudfGv5Z.htm)|Reposition|Reposicionar|modificada|
|[ZrjdCvEH7E5tQb4n.htm](abomination-vaults-bestiary-items/ZrjdCvEH7E5tQb4n.htm)|Frightful Presence|Frightful Presence|modificada|
|[zSR3MB9KNpQPcxA0.htm](abomination-vaults-bestiary-items/zSR3MB9KNpQPcxA0.htm)|Drumstick|Baqueta|modificada|
|[ZtEncTg58fTcUXvv.htm](abomination-vaults-bestiary-items/ZtEncTg58fTcUXvv.htm)|Apocalypse Beam|Apocalypse Beam|modificada|
|[ZU5jisN3KgoM2kPq.htm](abomination-vaults-bestiary-items/ZU5jisN3KgoM2kPq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zuOWrPyeR35yI1r7.htm](abomination-vaults-bestiary-items/zuOWrPyeR35yI1r7.htm)|Leg Quill|Leg Quill|modificada|
|[ZVpl1dcKnK3GldQS.htm](abomination-vaults-bestiary-items/ZVpl1dcKnK3GldQS.htm)|Trident|Trident|modificada|
|[zWjx9xYQFt0WhRW5.htm](abomination-vaults-bestiary-items/zWjx9xYQFt0WhRW5.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zXmPJ4IvqhUl8bad.htm](abomination-vaults-bestiary-items/zXmPJ4IvqhUl8bad.htm)|Grab|Agarrado|modificada|
|[zyawAgjl2eHH085S.htm](abomination-vaults-bestiary-items/zyawAgjl2eHH085S.htm)|Drumstick|Baqueta|modificada|
|[zYMKuVB2VBXOtrfe.htm](abomination-vaults-bestiary-items/zYMKuVB2VBXOtrfe.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[zzmMQYbPGJRgOH7E.htm](abomination-vaults-bestiary-items/zzmMQYbPGJRgOH7E.htm)|Ghostly Assault|Asalto fantasmal|modificada|
|[zZQXw3WG7NLAQrux.htm](abomination-vaults-bestiary-items/zZQXw3WG7NLAQrux.htm)|Swift Leap|Salto veloz|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[1gu2mF6EgXl6v3eM.htm](abomination-vaults-bestiary-items/1gu2mF6EgXl6v3eM.htm)|Crafting|vacía|
|[2agkr9lAnWdxtLza.htm](abomination-vaults-bestiary-items/2agkr9lAnWdxtLza.htm)|Salaisa's Key|vacía|
|[2wByf3NmPbP0YRzy.htm](abomination-vaults-bestiary-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|vacía|
|[41bzyFqCaaL6LCZF.htm](abomination-vaults-bestiary-items/41bzyFqCaaL6LCZF.htm)|Scriptorium Lore|vacía|
|[8CznKBBokxBMcUVf.htm](abomination-vaults-bestiary-items/8CznKBBokxBMcUVf.htm)|Warfare Lore|vacía|
|[8HBlAYPbdVvURrBC.htm](abomination-vaults-bestiary-items/8HBlAYPbdVvURrBC.htm)|Research Book|vacía|
|[a08Gf29QPITHX7mo.htm](abomination-vaults-bestiary-items/a08Gf29QPITHX7mo.htm)|Scriptorium Lore|vacía|
|[AO9HakVAxS0tqRrU.htm](abomination-vaults-bestiary-items/AO9HakVAxS0tqRrU.htm)|Fragment of Urevian's Pendant|vacía|
|[bo6bsp0fKrJ9ddNK.htm](abomination-vaults-bestiary-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|vacía|
|[BWdMcnFnD4W0LZpu.htm](abomination-vaults-bestiary-items/BWdMcnFnD4W0LZpu.htm)|Athletics|vacía|
|[C1gCdCYUAoqjpkXr.htm](abomination-vaults-bestiary-items/C1gCdCYUAoqjpkXr.htm)|Stealth|vacía|
|[cY5sdUKAQ65ORaUe.htm](abomination-vaults-bestiary-items/cY5sdUKAQ65ORaUe.htm)|Crafting|vacía|
|[DejWulVQHxSP4SbB.htm](abomination-vaults-bestiary-items/DejWulVQHxSP4SbB.htm)|Astrology Lore|vacía|
|[eDZZSOhzP0LwbDLp.htm](abomination-vaults-bestiary-items/eDZZSOhzP0LwbDLp.htm)|Fragment of Urevian's Pendant|vacía|
|[fJKkYX781x7u24J6.htm](abomination-vaults-bestiary-items/fJKkYX781x7u24J6.htm)|Mud and Bug Crown|vacía|
|[GqaPr3pgEY5WHWQc.htm](abomination-vaults-bestiary-items/GqaPr3pgEY5WHWQc.htm)|Athletics|vacía|
|[h0A9RTrbTZbrFUe8.htm](abomination-vaults-bestiary-items/h0A9RTrbTZbrFUe8.htm)|Spirit Lore|vacía|
|[hrgnNQ80aeUqTSge.htm](abomination-vaults-bestiary-items/hrgnNQ80aeUqTSge.htm)|Athletics|vacía|
|[i7f5BgAL5FJKJd7b.htm](abomination-vaults-bestiary-items/i7f5BgAL5FJKJd7b.htm)|Platinum Medal|vacía|
|[Iif4AgPk3mZlhi6z.htm](abomination-vaults-bestiary-items/Iif4AgPk3mZlhi6z.htm)|Library Lore|vacía|
|[JCCJdKRetmUpfNFn.htm](abomination-vaults-bestiary-items/JCCJdKRetmUpfNFn.htm)|Fragment of Urevian's Pendant|vacía|
|[JCTMNa7QfednvGsM.htm](abomination-vaults-bestiary-items/JCTMNa7QfednvGsM.htm)|Acrobatics|vacía|
|[JvcM7bdEkBwNZbGd.htm](abomination-vaults-bestiary-items/JvcM7bdEkBwNZbGd.htm)|Survival|vacía|
|[KJ3YTwQCESnulZkn.htm](abomination-vaults-bestiary-items/KJ3YTwQCESnulZkn.htm)|Athletics|vacía|
|[lJpU26bsaZ9RvxhZ.htm](abomination-vaults-bestiary-items/lJpU26bsaZ9RvxhZ.htm)|Crafting|vacía|
|[LjVRW54085z5XE2p.htm](abomination-vaults-bestiary-items/LjVRW54085z5XE2p.htm)|Legal Lore|vacía|
|[LUdvij5KTX4oc3yH.htm](abomination-vaults-bestiary-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|vacía|
|[M9wEcuvNMW8Uu4hV.htm](abomination-vaults-bestiary-items/M9wEcuvNMW8Uu4hV.htm)|Fragment of Urevian's Pendant|vacía|
|[mcWNQMlHvbDUhInI.htm](abomination-vaults-bestiary-items/mcWNQMlHvbDUhInI.htm)|Purpose Lore|vacía|
|[mj7RC0aTfmNbGcIT.htm](abomination-vaults-bestiary-items/mj7RC0aTfmNbGcIT.htm)|Seugathi's Key|vacía|
|[MOqlNipA5276gM0r.htm](abomination-vaults-bestiary-items/MOqlNipA5276gM0r.htm)|Diplomacy|vacía|
|[nhdPKzoIkY6tfcLb.htm](abomination-vaults-bestiary-items/nhdPKzoIkY6tfcLb.htm)|Fragment of Urevian's Pendant|vacía|
|[nksShZpMIUxollKt.htm](abomination-vaults-bestiary-items/nksShZpMIUxollKt.htm)|Handful of Junk|vacía|
|[pIZQ59XrVNVhyiJY.htm](abomination-vaults-bestiary-items/pIZQ59XrVNVhyiJY.htm)|Library Lore|vacía|
|[QcH6j6nAL93LSs8M.htm](abomination-vaults-bestiary-items/QcH6j6nAL93LSs8M.htm)|Athletics|vacía|
|[rdUiajy3hLLrKXt4.htm](abomination-vaults-bestiary-items/rdUiajy3hLLrKXt4.htm)|Azvalvigander's Key|vacía|
|[rET5SfCVJHTK7d5a.htm](abomination-vaults-bestiary-items/rET5SfCVJHTK7d5a.htm)|Athletics|vacía|
|[T7FvhRfA6QKYe0b7.htm](abomination-vaults-bestiary-items/T7FvhRfA6QKYe0b7.htm)|Athletics|vacía|
|[ujbUCBlQf3V81dRB.htm](abomination-vaults-bestiary-items/ujbUCBlQf3V81dRB.htm)|Torture Lore|vacía|
|[UnpIdfQqjGGuWR70.htm](abomination-vaults-bestiary-items/UnpIdfQqjGGuWR70.htm)|Padli's Key|vacía|
|[VK6P3hNaRnT5JPUT.htm](abomination-vaults-bestiary-items/VK6P3hNaRnT5JPUT.htm)|Quill|vacía|
|[vmnzh7dfffPn9ub8.htm](abomination-vaults-bestiary-items/vmnzh7dfffPn9ub8.htm)|Crafting|vacía|
|[VssG55TESDIyaiPP.htm](abomination-vaults-bestiary-items/VssG55TESDIyaiPP.htm)|Lump of Decayed Flesh Strung on a Necklace|vacía|
|[w1H2Uu7gjl1fTH9B.htm](abomination-vaults-bestiary-items/w1H2Uu7gjl1fTH9B.htm)|Writing Slate|vacía|
|[wVxlFqCWi3T6bOk1.htm](abomination-vaults-bestiary-items/wVxlFqCWi3T6bOk1.htm)|Projectile Launcher|vacía|
|[wyg04TlEIo9tJBCj.htm](abomination-vaults-bestiary-items/wyg04TlEIo9tJBCj.htm)|Athletics|vacía|
|[Xd2kZ5dL4yuq4lr3.htm](abomination-vaults-bestiary-items/Xd2kZ5dL4yuq4lr3.htm)|Athletics|vacía|
|[z7r4p7kLzt2ZbByW.htm](abomination-vaults-bestiary-items/z7r4p7kLzt2ZbByW.htm)|Violin|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
