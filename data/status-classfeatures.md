# Estado de la traducción (classfeatures)

 * **modificada**: 490
 * **oficial**: 16
 * **ninguna**: 18


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[inventor-01-fOulT6iKVqIK4jJX.htm](classfeatures/inventor-01-fOulT6iKVqIK4jJX.htm)|Razor Prongs|
|[inventor-01-Nbg4ZllDI9uCowZL.htm](classfeatures/inventor-01-Nbg4ZllDI9uCowZL.htm)|Hefty Composition|
|[inventor-01-O3r84Uv6HytaSIbX.htm](classfeatures/inventor-01-O3r84Uv6HytaSIbX.htm)|Blunt Shot|
|[inventor-01-O9wpXEKtKYJOMIlK.htm](classfeatures/inventor-01-O9wpXEKtKYJOMIlK.htm)|Dynamic Weighting|
|[inventor-01-pSXlZggdCCbkQqNr.htm](classfeatures/inventor-01-pSXlZggdCCbkQqNr.htm)|Pacification Tools|
|[inventor-01-qIOKqT93h6CX6V4k.htm](classfeatures/inventor-01-qIOKqT93h6CX6V4k.htm)|Complex Simplicity|
|[inventor-01-qwhfPgE2tTW0hvPe.htm](classfeatures/inventor-01-qwhfPgE2tTW0hvPe.htm)|Modular Head|
|[inventor-01-R8cfRNPdaCkd2bud.htm](classfeatures/inventor-01-R8cfRNPdaCkd2bud.htm)|Hampering Spikes|
|[inventor-01-Z1au5zxYcjZvdQpd.htm](classfeatures/inventor-01-Z1au5zxYcjZvdQpd.htm)|Entangling Form|
|[inventor-01-ZCfPjOn6JJ8Zrgvg.htm](classfeatures/inventor-01-ZCfPjOn6JJ8Zrgvg.htm)|Segmented Frame|
|[inventor-07-7Zw83ysONrNhJMr8.htm](classfeatures/inventor-07-7Zw83ysONrNhJMr8.htm)|Aerodynamic Construction|
|[inventor-07-h1hNr4jABip2ERDd.htm](classfeatures/inventor-07-h1hNr4jABip2ERDd.htm)|Inconspicuous Appearance|
|[inventor-07-P1GbGEePC8zDi8K4.htm](classfeatures/inventor-07-P1GbGEePC8zDi8K4.htm)|Advanced Rangefinder|
|[inventor-07-ptBLVvRqn1fA3A4l.htm](classfeatures/inventor-07-ptBLVvRqn1fA3A4l.htm)|Rope Shot|
|[inventor-07-tPL4lrGLCB5XbgeV.htm](classfeatures/inventor-07-tPL4lrGLCB5XbgeV.htm)|Integrated Gauntlet|
|[inventor-07-WxkZMlETXo165XnC.htm](classfeatures/inventor-07-WxkZMlETXo165XnC.htm)|Tangle Line|
|[inventor-07-y62b9R6RUBbkZECA.htm](classfeatures/inventor-07-y62b9R6RUBbkZECA.htm)|Manifold Alloy|
|[sorcerer-01-b6hyZTs1rVGHDexz.htm](classfeatures/sorcerer-01-b6hyZTs1rVGHDexz.htm)|Bloodline: Harrow|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[alchemist-01-7JbiaZ8bxODM5mzS.htm](classfeatures/alchemist-01-7JbiaZ8bxODM5mzS.htm)|Bomber|Bombardero|modificada|
|[alchemist-01-cU2ofQLj7pg6wTSi.htm](classfeatures/alchemist-01-cU2ofQLj7pg6wTSi.htm)|Research Field|Campo de investigacion|modificada|
|[alchemist-01-eNZnx4LISDNftbx2.htm](classfeatures/alchemist-01-eNZnx4LISDNftbx2.htm)|Chirurgeon|Cirujano|modificada|
|[alchemist-01-P9quO9XZi3OWFe1k.htm](classfeatures/alchemist-01-P9quO9XZi3OWFe1k.htm)|Toxicologist|Toxicólogo|modificada|
|[alchemist-01-Pe0zmIqyTBc2Td0I.htm](classfeatures/alchemist-01-Pe0zmIqyTBc2Td0I.htm)|Advanced Alchemy|Alquimia avanzada|modificada|
|[alchemist-01-sPtl05wwTpqFI0lL.htm](classfeatures/alchemist-01-sPtl05wwTpqFI0lL.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[alchemist-01-tvdb1jkjl2bRZjSp.htm](classfeatures/alchemist-01-tvdb1jkjl2bRZjSp.htm)|Mutagenist|Mutagenista|modificada|
|[alchemist-01-w3aS3tsvH2Ub6XMn.htm](classfeatures/alchemist-01-w3aS3tsvH2Ub6XMn.htm)|Alchemy|Alquimia|modificada|
|[alchemist-01-wySB9VHOW1v3TX1L.htm](classfeatures/alchemist-01-wySB9VHOW1v3TX1L.htm)|Infused Reagents|Reactivos infundidos|modificada|
|[alchemist-01-XPPG7nN9pxt0sjMg.htm](classfeatures/alchemist-01-XPPG7nN9pxt0sjMg.htm)|Formula Book|Libro de fórmulas|modificada|
|[alchemist-05-6zo2PJGYoig7nFpR.htm](classfeatures/alchemist-05-6zo2PJGYoig7nFpR.htm)|Field Discovery (Toxicologist)|Descubrimiento en campo (Toxicólogo)|modificada|
|[alchemist-05-7JK2a1D3VeWDcObo.htm](classfeatures/alchemist-05-7JK2a1D3VeWDcObo.htm)|Powerful Alchemy|Alquimia poderosa|modificada|
|[alchemist-05-8QAFgy9U8PxEa7Dw.htm](classfeatures/alchemist-05-8QAFgy9U8PxEa7Dw.htm)|Field Discovery (Bomber)|Descubrimiento en campo (bombardero)|modificada|
|[alchemist-05-IxxPEahbqXwIXum7.htm](classfeatures/alchemist-05-IxxPEahbqXwIXum7.htm)|Field Discovery|Descubrimiento en campo|modificada|
|[alchemist-05-qC0Iz6SlG2i9gv6g.htm](classfeatures/alchemist-05-qC0Iz6SlG2i9gv6g.htm)|Field Discovery (Chirurgeon)|Descubrimiento en campo (cirujano)|modificada|
|[alchemist-05-V4Jt7eDnJBLv5bDj.htm](classfeatures/alchemist-05-V4Jt7eDnJBLv5bDj.htm)|Field Discovery (Mutagenist)|Descubrimiento en un campo (mutagenista).|modificada|
|[alchemist-07-4ocPy4O0OCLY0XCM.htm](classfeatures/alchemist-07-4ocPy4O0OCLY0XCM.htm)|Alchemical Weapon Expertise|Experiencia con las armas alquímicas|modificada|
|[alchemist-07-DFQDtT1Van4fFEHi.htm](classfeatures/alchemist-07-DFQDtT1Van4fFEHi.htm)|Perpetual Infusions (Bomber)|Infusiones perpetuas (bombardero)|modificada|
|[alchemist-07-Dug1oaVYejLmYEFt.htm](classfeatures/alchemist-07-Dug1oaVYejLmYEFt.htm)|Perpetual Infusions (Mutagenist)|Infusiones perpetuas (mutagenista).|modificada|
|[alchemist-07-fzvIe6FwwCuIdnjX.htm](classfeatures/alchemist-07-fzvIe6FwwCuIdnjX.htm)|Perpetual Infusions (Chirurgeon)|Infusiones perpetuas (cirujano)|modificada|
|[alchemist-07-ZqwHAoIZrI1dGoqK.htm](classfeatures/alchemist-07-ZqwHAoIZrI1dGoqK.htm)|Perpetual Infusions|Infusiones perpetuas|modificada|
|[alchemist-09-3e1PlMXmlSwKoc6d.htm](classfeatures/alchemist-09-3e1PlMXmlSwKoc6d.htm)|Alchemical Expertise|Experiencia alquímica|modificada|
|[alchemist-09-76cwNLJEm4Yetnee.htm](classfeatures/alchemist-09-76cwNLJEm4Yetnee.htm)|Double Brew|Elaboración doble|modificada|
|[alchemist-11-8rEVg03QJ71ic3PP.htm](classfeatures/alchemist-11-8rEVg03QJ71ic3PP.htm)|Perpetual Potency (Bomber)|Potencia perpetua (bombardero).|modificada|
|[alchemist-11-MGn2wezOr3VAdO3U.htm](classfeatures/alchemist-11-MGn2wezOr3VAdO3U.htm)|Perpetual Potency|Potencia perpetua|modificada|
|[alchemist-11-mZFqRLYOQEqKA8ri.htm](classfeatures/alchemist-11-mZFqRLYOQEqKA8ri.htm)|Perpetual Potency (Mutagenist)|Potencia perpetua (mutagenista).|modificada|
|[alchemist-11-VS5vkqUQu4n7E28Y.htm](classfeatures/alchemist-11-VS5vkqUQu4n7E28Y.htm)|Perpetual Potency (Chirurgeon)|Potencia perpetua (cirujano).|modificada|
|[alchemist-13-1BKdOJ0HNL6Eg3xw.htm](classfeatures/alchemist-13-1BKdOJ0HNL6Eg3xw.htm)|Greater Field Discovery (Mutagenist)|Descubrimiento mayor en tu campo (mutagenista).|modificada|
|[alchemist-13-bv3Qel8v9tpoFbw4.htm](classfeatures/alchemist-13-bv3Qel8v9tpoFbw4.htm)|Alchemist Armor Expertise (Level 13)|Alchemist Armor Expertise (Nivel 13)|modificada|
|[alchemist-13-JJcaVijwRt9dsnac.htm](classfeatures/alchemist-13-JJcaVijwRt9dsnac.htm)|Greater Field Discovery (Chirurgeon)|Descubrimiento mayor en tu campo (cirujano)|modificada|
|[alchemist-13-MEwvBnT2VsO5lQ6I.htm](classfeatures/alchemist-13-MEwvBnT2VsO5lQ6I.htm)|Greater Field Discovery|Descubrimiento mayor en tu campo.|modificada|
|[alchemist-13-RGs4uR3CAvgbtBAA.htm](classfeatures/alchemist-13-RGs4uR3CAvgbtBAA.htm)|Greater Field Discovery (Bomber)|Descubrimiento mayor en tu campo (bombardero).|modificada|
|[alchemist-13-tnqyQrhrZeDtDvcO.htm](classfeatures/alchemist-13-tnqyQrhrZeDtDvcO.htm)|Greater Field Discovery (Toxicologist)|Descubrimiento mayor en tu campo (Toxicólogo).|modificada|
|[alchemist-15-Eood6pNPaJxuSgD1.htm](classfeatures/alchemist-15-Eood6pNPaJxuSgD1.htm)|Alchemical Alacrity|Presteza alquímica|modificada|
|[alchemist-17-11nGqrSJOoGRlDjO.htm](classfeatures/alchemist-17-11nGqrSJOoGRlDjO.htm)|Perpetual Perfection|Perfección perpetua|modificada|
|[alchemist-17-CGetAmSbv06fW7GT.htm](classfeatures/alchemist-17-CGetAmSbv06fW7GT.htm)|Perpetual Perfection (Mutagenist)|Perfección perpetua (mutagenista).|modificada|
|[alchemist-17-eG7FBDjCdEFzW9V9.htm](classfeatures/alchemist-17-eG7FBDjCdEFzW9V9.htm)|Alchemical Mastery|Maestría alquímica|modificada|
|[alchemist-17-xO90iBD8XNGyaCkz.htm](classfeatures/alchemist-17-xO90iBD8XNGyaCkz.htm)|Perpetual Perfection (Bomber)|Perfección perpetua (bombardero)|modificada|
|[alchemist-17-YByJ9O7oe8wxfbqs.htm](classfeatures/alchemist-17-YByJ9O7oe8wxfbqs.htm)|Perpetual Perfection (Chirurgeon)|Perfección perpetua (cirujano).|modificada|
|[alchemist-19-FiVYuIPTBzPzNP4E.htm](classfeatures/alchemist-19-FiVYuIPTBzPzNP4E.htm)|Alchemist Armor Mastery (Level 19)|Dominio de la armadura de alquimista (nivel 19).|modificada|
|[barbarian-01-0FtzFbUrN56KA67z.htm](classfeatures/barbarian-01-0FtzFbUrN56KA67z.htm)|Animal Instinct|Instinto animal|modificada|
|[barbarian-01-dU7xRpg4kFd01hwZ.htm](classfeatures/barbarian-01-dU7xRpg4kFd01hwZ.htm)|Instinct|Instinto|modificada|
|[barbarian-01-JuKD6k7nDwfO0Ckv.htm](classfeatures/barbarian-01-JuKD6k7nDwfO0Ckv.htm)|Giant Instinct|Instinto de gigante|modificada|
|[barbarian-01-k7M9jedvt31AJ5ZR.htm](classfeatures/barbarian-01-k7M9jedvt31AJ5ZR.htm)|Fury Instinct|Instinto de rabia|modificada|
|[barbarian-01-TQqv9Q5mB4PW6LH9.htm](classfeatures/barbarian-01-TQqv9Q5mB4PW6LH9.htm)|Spirit Instinct|Instinto de los espíritus|modificada|
|[barbarian-01-VDot7CDcXElxmkkz.htm](classfeatures/barbarian-01-VDot7CDcXElxmkkz.htm)|Dragon Instinct|Instinto de dragón|modificada|
|[barbarian-05-EEUTd0jAyfwTLzjk.htm](classfeatures/barbarian-05-EEUTd0jAyfwTLzjk.htm)|Brutality|Brutalidad|modificada|
|[barbarian-09-ie6xDX9GMEcA2Iuq.htm](classfeatures/barbarian-09-ie6xDX9GMEcA2Iuq.htm)|Raging Resistance|Resistencia furiosa|modificada|
|[barbarian-11-88Q33X2a0iYPkbzd.htm](classfeatures/barbarian-11-88Q33X2a0iYPkbzd.htm)|Mighty Rage|Furia poderosa|modificada|
|[barbarian-13-ejP4jVQkS48uKRFz.htm](classfeatures/barbarian-13-ejP4jVQkS48uKRFz.htm)|Weapon Fury|Furia con arma|modificada|
|[barbarian-13-TuL0UfqH14MtqYVh.htm](classfeatures/barbarian-13-TuL0UfqH14MtqYVh.htm)|Greater Juggernaut|Juggernaut mayor|modificada|
|[barbarian-15-7JjhxMFo8DMwpGx0.htm](classfeatures/barbarian-15-7JjhxMFo8DMwpGx0.htm)|Greater Weapon Specialization (Barbarian)|Especialización mayor en armas (bárbaro).|modificada|
|[barbarian-15-BZnqKnqKVImjSIFE.htm](classfeatures/barbarian-15-BZnqKnqKVImjSIFE.htm)|Indomitable Will|Voluntad indomable|modificada|
|[barbarian-17-7MhzrbOyue5GQsck.htm](classfeatures/barbarian-17-7MhzrbOyue5GQsck.htm)|Heightened Senses|Sentidos aumentados|modificada|
|[barbarian-17-qMtyQGUllPdgpzUo.htm](classfeatures/barbarian-17-qMtyQGUllPdgpzUo.htm)|Quick Rage|Furia rápida|modificada|
|[barbarian-19-QTCIahokREpnAYDi.htm](classfeatures/barbarian-19-QTCIahokREpnAYDi.htm)|Armor of Fury|Armadura de furia|modificada|
|[barbarian-19-VLiT503OLOM3vaDx.htm](classfeatures/barbarian-19-VLiT503OLOM3vaDx.htm)|Devastator|Devastador|modificada|
|[bard-01-4ripp6EfdVpS0d60.htm](classfeatures/bard-01-4ripp6EfdVpS0d60.htm)|Enigma|Enigma|modificada|
|[bard-01-6FsusoMYxxjyIkVh.htm](classfeatures/bard-01-6FsusoMYxxjyIkVh.htm)|Spell Repertoire (Bard)|Repertorio de conjuros (Bardo)|modificada|
|[bard-01-AIOBWGOS4nkfH3kW.htm](classfeatures/bard-01-AIOBWGOS4nkfH3kW.htm)|Muses|Musas|modificada|
|[bard-01-fEOj0eOBe34qYdAa.htm](classfeatures/bard-01-fEOj0eOBe34qYdAa.htm)|Occult Spellcasting|Hechizo Ocultismo|modificada|
|[bard-01-s0VbbQJNlSgPocui.htm](classfeatures/bard-01-s0VbbQJNlSgPocui.htm)|Composition Spells|Conjuros de composición|modificada|
|[bard-01-y0jGimYdMGDJWrEq.htm](classfeatures/bard-01-y0jGimYdMGDJWrEq.htm)|Polymath|Polifacético|modificada|
|[bard-01-YMBsi4bndRAk5CX4.htm](classfeatures/bard-01-YMBsi4bndRAk5CX4.htm)|Maestro|Maestro|modificada|
|[bard-11-4lp8oG9A3zuqhPBS.htm](classfeatures/bard-11-4lp8oG9A3zuqhPBS.htm)|Bard Weapon Expertise|Experiencia con las armas del bardo|modificada|
|[bard-19-NjsOpWbbzUY2Hpk3.htm](classfeatures/bard-19-NjsOpWbbzUY2Hpk3.htm)|Magnum Opus|Obra maestra|modificada|
|[champion-00-EtltLdiy9kNfHU0c.htm](classfeatures/champion-00-EtltLdiy9kNfHU0c.htm)|Blade Ally|Blade Ally|modificada|
|[champion-00-QQP0mu0cyWIwNUh9.htm](classfeatures/champion-00-QQP0mu0cyWIwNUh9.htm)|Shield Ally|Aliado de escudo|modificada|
|[champion-00-Z6E1O8X7CFcyczB1.htm](classfeatures/champion-00-Z6E1O8X7CFcyczB1.htm)|Steed Ally|Aliado Corcel|modificada|
|[champion-01-ehL7mnkqxN5wIkgu.htm](classfeatures/champion-01-ehL7mnkqxN5wIkgu.htm)|Deity and Cause|Deidad y Causa|modificada|
|[champion-01-FCoMFUsth4xB4veC.htm](classfeatures/champion-01-FCoMFUsth4xB4veC.htm)|Liberator|Liberador|modificada|
|[champion-01-FeBsYn2mHfMVDZvw.htm](classfeatures/champion-01-FeBsYn2mHfMVDZvw.htm)|Deific Weapon|Arma deífica|modificada|
|[champion-01-fykh5pE99O3I2sOI.htm](classfeatures/champion-01-fykh5pE99O3I2sOI.htm)|Champion's Code|Código del campeón|modificada|
|[champion-01-JiY2ZB4FkK8RJm4T.htm](classfeatures/champion-01-JiY2ZB4FkK8RJm4T.htm)|The Tenets of Evil|Los principios del maligno|modificada|
|[champion-01-nxZYP3KGfTSkaW6J.htm](classfeatures/champion-01-nxZYP3KGfTSkaW6J.htm)|The Tenets of Good|Los principios del bien|modificada|
|[champion-01-peEXunfbSD8WcMFk.htm](classfeatures/champion-01-peEXunfbSD8WcMFk.htm)|Paladin|Paladín|modificada|
|[champion-01-Q1VfQZp49hkhY0HY.htm](classfeatures/champion-01-Q1VfQZp49hkhY0HY.htm)|Devotion Spells|Conjuros de devoción|modificada|
|[champion-01-sXVX4ARUuo8Egrz5.htm](classfeatures/champion-01-sXVX4ARUuo8Egrz5.htm)|Champion's Reaction|Reacción del campeón|modificada|
|[champion-01-UyuwFp0jQqYL2AdF.htm](classfeatures/champion-01-UyuwFp0jQqYL2AdF.htm)|Redeemer|Redentor|modificada|
|[champion-03-ERwuazupczhUSZ73.htm](classfeatures/champion-03-ERwuazupczhUSZ73.htm)|Divine Ally|Aliado divino|modificada|
|[champion-09-3XK573A7GH1rrLgO.htm](classfeatures/champion-09-3XK573A7GH1rrLgO.htm)|Divine Smite|Impacto divino|modificada|
|[champion-09-VgmfNKtQLgBaNi5r.htm](classfeatures/champion-09-VgmfNKtQLgBaNi5r.htm)|Champion Expertise|Experiencia de campeón|modificada|
|[champion-11-uptzvOLrZ3fctrl2.htm](classfeatures/champion-11-uptzvOLrZ3fctrl2.htm)|Exalt|Exaltar|modificada|
|[champion-11-xygfZopqXBJ6dKBA.htm](classfeatures/champion-11-xygfZopqXBJ6dKBA.htm)|Divine Will|Voluntad divina|modificada|
|[champion-17-voiSCh7ZXA2ogwiC.htm](classfeatures/champion-17-voiSCh7ZXA2ogwiC.htm)|Legendary Armor|Armadura legendaria|modificada|
|[champion-17-z5G0o04uV65zyxDB.htm](classfeatures/champion-17-z5G0o04uV65zyxDB.htm)|Champion Mastery|Maestría de campeón|modificada|
|[champion-19-LzB6X9vOaq3wq1FZ.htm](classfeatures/champion-19-LzB6X9vOaq3wq1FZ.htm)|Hero's Defiance|El desafío del héroe|modificada|
|[cleric-01-0Aocw3igLwna9cjp.htm](classfeatures/cleric-01-0Aocw3igLwna9cjp.htm)|Warpriest|Sacerdote de guerra|modificada|
|[cleric-01-aiwxBj5MjnafCMyn.htm](classfeatures/cleric-01-aiwxBj5MjnafCMyn.htm)|First Doctrine (Cloistered Cleric)|Primera doctrina (clérigo de clausura)|modificada|
|[cleric-01-AvNbdGSOTWNRgcxs.htm](classfeatures/cleric-01-AvNbdGSOTWNRgcxs.htm)|Divine Spellcasting (Cleric)|Lanzamiento de conjuros divinos (Clérigo)|modificada|
|[cleric-01-DutW12WMFPHBoLTH.htm](classfeatures/cleric-01-DutW12WMFPHBoLTH.htm)|Deity|Deidad|modificada|
|[cleric-01-gblTFUOgolqFS9v4.htm](classfeatures/cleric-01-gblTFUOgolqFS9v4.htm)|Divine Font|Fuente divina|modificada|
|[cleric-01-Qejo7FUWQtPTpgWH.htm](classfeatures/cleric-01-Qejo7FUWQtPTpgWH.htm)|First Doctrine|Primera Doctrina|modificada|
|[cleric-01-tyrBwBTzo5t9Zho7.htm](classfeatures/cleric-01-tyrBwBTzo5t9Zho7.htm)|Doctrine|Doctrina|modificada|
|[cleric-01-UV1HlClbWCNcaKBZ.htm](classfeatures/cleric-01-UV1HlClbWCNcaKBZ.htm)|Anathema (Cleric)|Anatema (Clérigo)|modificada|
|[cleric-01-xxkszluN9icAiTO4.htm](classfeatures/cleric-01-xxkszluN9icAiTO4.htm)|First Doctrine (Warpriest)|Primera doctrina (sacerdote de guerra)|modificada|
|[cleric-01-ZZzLMOUAtBVgV1DF.htm](classfeatures/cleric-01-ZZzLMOUAtBVgV1DF.htm)|Cloistered Cleric|Clérigo de clausura|modificada|
|[cleric-03-D34mPo29r1J3DPaX.htm](classfeatures/cleric-03-D34mPo29r1J3DPaX.htm)|Second Doctrine (Warpriest)|Segunda doctrina (sacerdote de guerra).|modificada|
|[cleric-03-OnfrrwCfDFCFw0tc.htm](classfeatures/cleric-03-OnfrrwCfDFCFw0tc.htm)|Second Doctrine|Segunda Doctrina|modificada|
|[cleric-03-sa7BWfnyCswAvBVa.htm](classfeatures/cleric-03-sa7BWfnyCswAvBVa.htm)|Second Doctrine (Cloistered Cleric)|Segunda doctrina (clérigo de clausura)|modificada|
|[cleric-07-gxNxfN9OBlQ1icus.htm](classfeatures/cleric-07-gxNxfN9OBlQ1icus.htm)|Third Doctrine|Tercera Doctrina|modificada|
|[cleric-07-s8WEmc4GGZSHSC7q.htm](classfeatures/cleric-07-s8WEmc4GGZSHSC7q.htm)|Third Doctrine (Cloistered Cleric)|Tercera doctrina (clérigo de clausura).|modificada|
|[cleric-07-Zp81uTBItG1xlH4O.htm](classfeatures/cleric-07-Zp81uTBItG1xlH4O.htm)|Third Doctrine (Warpriest)|Tercera doctrina (sacerdote de guerra)|modificada|
|[cleric-11-o8nHreMyiLi64rZz.htm](classfeatures/cleric-11-o8nHreMyiLi64rZz.htm)|Fourth Doctrine|Cuarta Doctrina|modificada|
|[cleric-11-px3gVYp7zlEQIpcl.htm](classfeatures/cleric-11-px3gVYp7zlEQIpcl.htm)|Fourth Doctrine (Warpriest)|Cuarta doctrina (sacerdote de guerra)|modificada|
|[cleric-11-vxOf4LXZcqUG3P7a.htm](classfeatures/cleric-11-vxOf4LXZcqUG3P7a.htm)|Fourth Doctrine (Cloistered Cleric)|Cuarta doctrina (clérigo de clausura)|modificada|
|[cleric-13-0mJTp4LdEHBLInoe.htm](classfeatures/cleric-13-0mJTp4LdEHBLInoe.htm)|Divine Defense|Defensa divina|modificada|
|[cleric-15-kmimy4VOaoEOgOiQ.htm](classfeatures/cleric-15-kmimy4VOaoEOgOiQ.htm)|Fifth Doctrine (Warpriest)|Quinta doctrina (sacerdote de guerra)|modificada|
|[cleric-15-n9W8MjjRgPpUTvWf.htm](classfeatures/cleric-15-n9W8MjjRgPpUTvWf.htm)|Fifth Doctrine (Cloistered Cleric)|Quinta doctrina (clérigo de clausura)|modificada|
|[cleric-15-Zb7DuGbFoLEp0H1K.htm](classfeatures/cleric-15-Zb7DuGbFoLEp0H1K.htm)|Fifth Doctrine|Quinta Doctrina|modificada|
|[cleric-19-3uf31A91h3ywmlqm.htm](classfeatures/cleric-19-3uf31A91h3ywmlqm.htm)|Miraculous Spell|Conjuro milagroso|modificada|
|[cleric-19-DgGefatQ4v6xT6f9.htm](classfeatures/cleric-19-DgGefatQ4v6xT6f9.htm)|Final Doctrine (Cloistered Cleric)|Doctrina Final (Clérigo de clausura)|modificada|
|[cleric-19-N1ugDqZlslxbp3Uy.htm](classfeatures/cleric-19-N1ugDqZlslxbp3Uy.htm)|Final Doctrine (Warpriest)|Doctrina final (sacerdote de guerra)|modificada|
|[cleric-19-urBGOPrUwBmkixAo.htm](classfeatures/cleric-19-urBGOPrUwBmkixAo.htm)|Final Doctrine|Doctrina final|modificada|
|[druid-01-8STJEFVJISujgpMR.htm](classfeatures/druid-01-8STJEFVJISujgpMR.htm)|Druidic Order|Orden druídica|modificada|
|[druid-01-acqqlYmti8D9QJi0.htm](classfeatures/druid-01-acqqlYmti8D9QJi0.htm)|Storm Order|Orden de la Tormenta|modificada|
|[druid-01-b8pnRxGuNzG0buuh.htm](classfeatures/druid-01-b8pnRxGuNzG0buuh.htm)|Primal Spellcasting|Lanzamiento de conjuros primigenios|modificada|
|[druid-01-d5BFFHXFJYKs5LXr.htm](classfeatures/druid-01-d5BFFHXFJYKs5LXr.htm)|Wild Empathy|Empatía salvaje|modificada|
|[druid-01-fKTewWlYgFuhl4KA.htm](classfeatures/druid-01-fKTewWlYgFuhl4KA.htm)|Stone Order|Orden de Piedra|modificada|
|[druid-01-FuUXyv2yBs7zRgqT.htm](classfeatures/druid-01-FuUXyv2yBs7zRgqT.htm)|Wave Order|Wave Order|modificada|
|[druid-01-NdeFvIXdHwKYLiUj.htm](classfeatures/druid-01-NdeFvIXdHwKYLiUj.htm)|Flame Order|Orden Flamígera|modificada|
|[druid-01-nfBn8QB6HVdzpTFV.htm](classfeatures/druid-01-nfBn8QB6HVdzpTFV.htm)|Anathema (Druid)|Anatema (Druida)|modificada|
|[druid-01-POBvoXifa9HaejAg.htm](classfeatures/druid-01-POBvoXifa9HaejAg.htm)|Animal Order|Orden Animal|modificada|
|[druid-01-RiAGlnnp4S21BAG3.htm](classfeatures/druid-01-RiAGlnnp4S21BAG3.htm)|Druidic Language|Idioma druídico|modificada|
|[druid-01-u4nlOzPj2WHkIj9l.htm](classfeatures/druid-01-u4nlOzPj2WHkIj9l.htm)|Leaf Order|Orden de la hoja|modificada|
|[druid-01-v0EjtiwdeMj8ykI0.htm](classfeatures/druid-01-v0EjtiwdeMj8ykI0.htm)|Wild Order|Orden salvaje|modificada|
|[druid-11-Ra32tlqBxHzT6fzN.htm](classfeatures/druid-11-Ra32tlqBxHzT6fzN.htm)|Druid Weapon Expertise|Experiencia con las armas de druida|modificada|
|[druid-19-nzgb43mQmLgaqDoQ.htm](classfeatures/druid-19-nzgb43mQmLgaqDoQ.htm)|Primal Hierophant|Hierofante primigenio|modificada|
|[fighter-01-eZNCckLzbH3GyncH.htm](classfeatures/fighter-01-eZNCckLzbH3GyncH.htm)|Shield Block|Bloquear con escudo|modificada|
|[fighter-01-hmShTfPOcTaKgbf4.htm](classfeatures/fighter-01-hmShTfPOcTaKgbf4.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[fighter-03-GJKJafDGuX4BeAeN.htm](classfeatures/fighter-03-GJKJafDGuX4BeAeN.htm)|Bravery|Bravery|modificada|
|[fighter-05-gApJtAdNb9ST4Ms9.htm](classfeatures/fighter-05-gApJtAdNb9ST4Ms9.htm)|Fighter Weapon Mastery|Luchador maestría con las armas|modificada|
|[fighter-07-TIvzBALymvb56L79.htm](classfeatures/fighter-07-TIvzBALymvb56L79.htm)|Battlefield Surveyor|Topógrafo del campo de batalla|modificada|
|[fighter-09-8g6HzARbhfcgilP8.htm](classfeatures/fighter-09-8g6HzARbhfcgilP8.htm)|Combat Flexibility|Flexibilidad en combate|modificada|
|[fighter-11-bAaI7h937Nr3g93U.htm](classfeatures/fighter-11-bAaI7h937Nr3g93U.htm)|Fighter Expertise|Fighter Expertise|modificada|
|[fighter-13-F5VenhIQMDkeGvmV.htm](classfeatures/fighter-13-F5VenhIQMDkeGvmV.htm)|Weapon Legend|Arma Leyenda|modificada|
|[fighter-15-W2rwudMNcAxs8VoX.htm](classfeatures/fighter-15-W2rwudMNcAxs8VoX.htm)|Improved Flexibility|Flexibilidad mejorada|modificada|
|[fighter-19-0H2LxtiZTJ275pSD.htm](classfeatures/fighter-19-0H2LxtiZTJ275pSD.htm)|Versatile Legend|Leyenda Versátil|modificada|
|[gunslinger-01-a3pSIKkDVTvvNSRO.htm](classfeatures/gunslinger-01-a3pSIKkDVTvvNSRO.htm)|Way of the Drifter|Camino del Vagabundo|modificada|
|[gunslinger-01-LDqVxLKrwEqSegiu.htm](classfeatures/gunslinger-01-LDqVxLKrwEqSegiu.htm)|Gunslinger's Way|Gunslinger's Way|modificada|
|[gunslinger-01-OmgtSDV1FubDUqWR.htm](classfeatures/gunslinger-01-OmgtSDV1FubDUqWR.htm)|Way of the Spellshot|El Camino del Disparo|modificada|
|[gunslinger-01-qRLRrHf0kzaJ7xt0.htm](classfeatures/gunslinger-01-qRLRrHf0kzaJ7xt0.htm)|Way of the Pistolero|Camino del Pistolero|modificada|
|[gunslinger-01-QWXvksGJhOjXbBqi.htm](classfeatures/gunslinger-01-QWXvksGJhOjXbBqi.htm)|Way of the Sniper|Camino del Francotirador|modificada|
|[gunslinger-01-vB0yVFxJVZwalt2g.htm](classfeatures/gunslinger-01-vB0yVFxJVZwalt2g.htm)|Way of the Vanguard|Camino de la Vanguardia|modificada|
|[gunslinger-01-vXbk7Nm1TOTTUNvF.htm](classfeatures/gunslinger-01-vXbk7Nm1TOTTUNvF.htm)|Singular Expertise|Pericia Singular|modificada|
|[gunslinger-01-YryaWAGcHeaRnXzS.htm](classfeatures/gunslinger-01-YryaWAGcHeaRnXzS.htm)|Way of the Triggerbrand|Camino de la Triggerbrand|modificada|
|[gunslinger-03-Wpdeh6EkcAKA60oH.htm](classfeatures/gunslinger-03-Wpdeh6EkcAKA60oH.htm)|Stubborn|Terco|modificada|
|[gunslinger-05-9nRT8aq05Fy2D3i3.htm](classfeatures/gunslinger-05-9nRT8aq05Fy2D3i3.htm)|Gunslinger Weapon Mastery|Maestría con las armas de pistolero|modificada|
|[gunslinger-09-3JLW5vPshsJf3nCY.htm](classfeatures/gunslinger-09-3JLW5vPshsJf3nCY.htm)|Advanced Deed|Hazaña avanzada|modificada|
|[gunslinger-09-aKr6OE8vI2BsJzf1.htm](classfeatures/gunslinger-09-aKr6OE8vI2BsJzf1.htm)|Gunslinger Expertise|Pericia de Pistolero|modificada|
|[gunslinger-13-ULOAZWZEokbJC6Rq.htm](classfeatures/gunslinger-13-ULOAZWZEokbJC6Rq.htm)|Gunslinging Legend|Leyenda de pistolero|modificada|
|[gunslinger-15-yc9RuXXxmZ9YidH6.htm](classfeatures/gunslinger-15-yc9RuXXxmZ9YidH6.htm)|Greater Deed|Mayor Hazaña|modificada|
|[gunslinger-17-RkofVX55ciXZyfAA.htm](classfeatures/gunslinger-17-RkofVX55ciXZyfAA.htm)|Shootist's Edge|Shootist's Edge|modificada|
|[inventor-01-5Xj38QeMKcFdrzqH.htm](classfeatures/inventor-01-5Xj38QeMKcFdrzqH.htm)|Phlogistonic Regulator|Regulador Flogistónico|modificada|
|[inventor-01-bok3P78CMchFibxC.htm](classfeatures/inventor-01-bok3P78CMchFibxC.htm)|Weapon Innovation|Innovación de Armas|modificada|
|[inventor-01-dsy2w4LfjMIWgy5D.htm](classfeatures/inventor-01-dsy2w4LfjMIWgy5D.htm)|Harmonic Oscillator|Oscilador Armónico|modificada|
|[inventor-01-fpwtpm8pdwO1I6MO.htm](classfeatures/inventor-01-fpwtpm8pdwO1I6MO.htm)|Armor Innovation|Armadura Innovación|modificada|
|[inventor-01-JH6um0St37UrjLNG.htm](classfeatures/inventor-01-JH6um0St37UrjLNG.htm)|Peerless Inventor|Peerless Inventor|modificada|
|[inventor-01-jHE4fPwU0sSIAjMo.htm](classfeatures/inventor-01-jHE4fPwU0sSIAjMo.htm)|Otherworldly Protection|Protección de otro mundo|modificada|
|[inventor-01-jIAgXe2FetAKBwt7.htm](classfeatures/inventor-01-jIAgXe2FetAKBwt7.htm)|Innovation|Innovación|modificada|
|[inventor-01-o70O2FysDd7BS9e0.htm](classfeatures/inventor-01-o70O2FysDd7BS9e0.htm)|Construct Innovation|Construir Innovación|modificada|
|[inventor-01-OkxoJWrOXhM25mhi.htm](classfeatures/inventor-01-OkxoJWrOXhM25mhi.htm)|Muscular Exoskeleton|Exoesqueleto Muscular|modificada|
|[inventor-01-oP5zM5Yu41xcx3iu.htm](classfeatures/inventor-01-oP5zM5Yu41xcx3iu.htm)|Overdrive|Sobremarcha|modificada|
|[inventor-01-pEm1RTNuzzQVKkR0.htm](classfeatures/inventor-01-pEm1RTNuzzQVKkR0.htm)|Explode|Explotar|modificada|
|[inventor-01-sTWY6PLqr1X7icgZ.htm](classfeatures/inventor-01-sTWY6PLqr1X7icgZ.htm)|Speed Boosters|Aumentadores de Velocidad|modificada|
|[inventor-01-X3TtVdNhrydeQ3SX.htm](classfeatures/inventor-01-X3TtVdNhrydeQ3SX.htm)|Subtle Dampeners|Amortiguadores Sutiles|modificada|
|[inventor-01-xndlv9T3JgYYUtf8.htm](classfeatures/inventor-01-xndlv9T3JgYYUtf8.htm)|Metallic Reactance|Reactancia Metálica|modificada|
|[inventor-03-J46wcNqKXvtokBD1.htm](classfeatures/inventor-03-J46wcNqKXvtokBD1.htm)|Reconfigure|Reconfigurar|modificada|
|[inventor-03-YMKxN56w617BYwu4.htm](classfeatures/inventor-03-YMKxN56w617BYwu4.htm)|Expert Overdrive|Experto Overdrive|modificada|
|[inventor-05-0NyPgi6UACMTmAGE.htm](classfeatures/inventor-05-0NyPgi6UACMTmAGE.htm)|Inventor Weapon Expertise|Inventor Experiencia con las armas|modificada|
|[inventor-07-78HIjRbGoONMpF31.htm](classfeatures/inventor-07-78HIjRbGoONMpF31.htm)|Breakthrough Innovation|Innovación rompedora|modificada|
|[inventor-07-SXv9bJFbntDOMRIL.htm](classfeatures/inventor-07-SXv9bJFbntDOMRIL.htm)|Master Overdrive|Master Overdrive|modificada|
|[inventor-09-F8oXHnu9iNTcpXbJ.htm](classfeatures/inventor-09-F8oXHnu9iNTcpXbJ.htm)|Offensive Boost|Aumento ofensivo|modificada|
|[inventor-09-mQVC1iDyNi2tfsF8.htm](classfeatures/inventor-09-mQVC1iDyNi2tfsF8.htm)|Inventive Expertise|Pericia Inventiva|modificada|
|[inventor-13-j0klWHkH3AxUAgok.htm](classfeatures/inventor-13-j0klWHkH3AxUAgok.htm)|Complete Reconfiguration|Reconfiguración Completa|modificada|
|[inventor-13-mJpPaoVlNmTK47x1.htm](classfeatures/inventor-13-mJpPaoVlNmTK47x1.htm)|Inventor Weapon Mastery|Inventor maestría con las armas|modificada|
|[inventor-15-o1omL2LdHvjEwh3P.htm](classfeatures/inventor-15-o1omL2LdHvjEwh3P.htm)|Legendary Overdrive|Legendary Overdrive|modificada|
|[inventor-15-tXbadIT3LzwuSR19.htm](classfeatures/inventor-15-tXbadIT3LzwuSR19.htm)|Revolutionary Innovation|Innovación revolucionaria|modificada|
|[inventor-15-vUt71oX9QNLAUmZn.htm](classfeatures/inventor-15-vUt71oX9QNLAUmZn.htm)|Energy Barrier|Barrera de energía|modificada|
|[inventor-17-Uu8VnpAo3XZZEKPd.htm](classfeatures/inventor-17-Uu8VnpAo3XZZEKPd.htm)|Inventive Mastery|Maestría Inventiva|modificada|
|[inventor-19-rOaLbipkComjc6qh.htm](classfeatures/inventor-19-rOaLbipkComjc6qh.htm)|Infinite Invention|Invención infinita|modificada|
|[investigator-01-2Fe4YZCvAr9Yf6w7.htm](classfeatures/investigator-01-2Fe4YZCvAr9Yf6w7.htm)|Strategic Strike|Golpe Estratégico|modificada|
|[investigator-01-6FasgIXUJ1X8ekRn.htm](classfeatures/investigator-01-6FasgIXUJ1X8ekRn.htm)|On the Case|En el Caso|modificada|
|[investigator-01-g3mNzNphtVxyR9Xr.htm](classfeatures/investigator-01-g3mNzNphtVxyR9Xr.htm)|Empiricism Methodology|Empiricism Methodology|modificada|
|[investigator-01-lgo65ldX7WkXC8Ir.htm](classfeatures/investigator-01-lgo65ldX7WkXC8Ir.htm)|Devise a Stratagem|Devise a Stratagem|modificada|
|[investigator-01-ln2Y1a4SxlU9sizX.htm](classfeatures/investigator-01-ln2Y1a4SxlU9sizX.htm)|Alchemical Sciences Methodology|Alchemical Sciences Methodology|modificada|
|[investigator-01-O3IX7rTxXWWvDVM3.htm](classfeatures/investigator-01-O3IX7rTxXWWvDVM3.htm)|Forensic Medicine Methodology|Forensic Medicine Methodology|modificada|
|[investigator-01-uhHg9BXBiHpL5ndS.htm](classfeatures/investigator-01-uhHg9BXBiHpL5ndS.htm)|Methodology|Metodología|modificada|
|[investigator-01-UIHUNNYZyQ3p4Vmo.htm](classfeatures/investigator-01-UIHUNNYZyQ3p4Vmo.htm)|Interrogation Methodology|Metodología de interrogatorio|modificada|
|[investigator-03-dmK1wya8GBi9MmCB.htm](classfeatures/investigator-03-dmK1wya8GBi9MmCB.htm)|Skillful Lessons|Skillful Lessons|modificada|
|[investigator-09-PFvB79O2VFdiAeSj.htm](classfeatures/investigator-09-PFvB79O2VFdiAeSj.htm)|Investigator Expertise|Pericia de Investigador|modificada|
|[investigator-11-malYpr0CYL4fDGhr.htm](classfeatures/investigator-11-malYpr0CYL4fDGhr.htm)|Deductive Improvisation|Improvisación Deductiva|modificada|
|[investigator-19-flEx8eY0NinF9XZU.htm](classfeatures/investigator-19-flEx8eY0NinF9XZU.htm)|Master Detective|Maestro detective|modificada|
|[magus-01-09iL38CZZEa0q0Mt.htm](classfeatures/magus-01-09iL38CZZEa0q0Mt.htm)|Arcane Cascade|Cascada Arcana|modificada|
|[magus-01-3gVDqDPSz4fB5T9G.htm](classfeatures/magus-01-3gVDqDPSz4fB5T9G.htm)|Laughing Shadow|Sombra Risueña|modificada|
|[magus-01-6YJ8KFl7THkVy6Gm.htm](classfeatures/magus-01-6YJ8KFl7THkVy6Gm.htm)|Twisting Tree|Twisting Tree|modificada|
|[magus-01-FkbFgmoVz5lHhSMo.htm](classfeatures/magus-01-FkbFgmoVz5lHhSMo.htm)|Conflux Spells|Hechizos de Conflujo|modificada|
|[magus-01-FTeIs1Z1Qeli4BIF.htm](classfeatures/magus-01-FTeIs1Z1Qeli4BIF.htm)|Hybrid Study|Estudio híbrido|modificada|
|[magus-01-KVj5ofUwu3VJSrVw.htm](classfeatures/magus-01-KVj5ofUwu3VJSrVw.htm)|Spellstrike|Spellstrike|modificada|
|[magus-01-maGzhKLmgubAdUlN.htm](classfeatures/magus-01-maGzhKLmgubAdUlN.htm)|Sparkling Targe|Sparkling Targe|modificada|
|[magus-01-Pew7duAozEeAemif.htm](classfeatures/magus-01-Pew7duAozEeAemif.htm)|Starlit Span|Starlit Span|modificada|
|[magus-01-wXaz41gwqNtTn6tf.htm](classfeatures/magus-01-wXaz41gwqNtTn6tf.htm)|Arcane Spellcasting (Magus)|Lanzamiento de conjuros arcanos (Magus)|modificada|
|[magus-01-ZslXrvYRxHBXc1Ds.htm](classfeatures/magus-01-ZslXrvYRxHBXc1Ds.htm)|Inexorable Iron|Hierro Inexorable|modificada|
|[magus-07-6HCI2iHyBZAr7a4P.htm](classfeatures/magus-07-6HCI2iHyBZAr7a4P.htm)|Studious Spells|Conjuros Estudiosos|modificada|
|[magus-19-VmPIJomEdmgGrCMS.htm](classfeatures/magus-19-VmPIJomEdmgGrCMS.htm)|Double Spellstrike|Double Spellstrike|modificada|
|[mental-01-WZUCvxqbigXos1L9.htm](classfeatures/mental-01-WZUCvxqbigXos1L9.htm)|Rage|Furia|modificada|
|[monk-01-NLHHHiAcdnZ5ohc2.htm](classfeatures/monk-01-NLHHHiAcdnZ5ohc2.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[monk-01-SB8UJ8rZmvbcBweJ.htm](classfeatures/monk-01-SB8UJ8rZmvbcBweJ.htm)|Powerful Fist|Puño poderoso|modificada|
|[monk-03-Cq6NjvcKZOMySBVj.htm](classfeatures/monk-03-Cq6NjvcKZOMySBVj.htm)|Incredible Movement|Movimiento sensacional|modificada|
|[monk-03-D2AE8RfMlZ3D1FuV.htm](classfeatures/monk-03-D2AE8RfMlZ3D1FuV.htm)|Mystic Strikes|Golpes místicos|modificada|
|[monk-05-VgZIutWjFl8oZQFi.htm](classfeatures/monk-05-VgZIutWjFl8oZQFi.htm)|Expert Strikes|Golpes expertos|modificada|
|[monk-07-1K6m6AVmn3r8XZ9d.htm](classfeatures/monk-07-1K6m6AVmn3r8XZ9d.htm)|Path to Perfection|Camino a la perfección|modificada|
|[monk-07-7lanxgmoOHNdtDe2.htm](classfeatures/monk-07-7lanxgmoOHNdtDe2.htm)|Path to Perfection (Will)|Camino a la perfección (Voluntad)|modificada|
|[monk-07-Bwr9G9IR4ynm5wzz.htm](classfeatures/monk-07-Bwr9G9IR4ynm5wzz.htm)|Path to Perfection (Reflex)|Camino a la perfección (Reflejos)|modificada|
|[monk-07-KIqptJsjq9pS9CP7.htm](classfeatures/monk-07-KIqptJsjq9pS9CP7.htm)|Path to Perfection (Fortitude)|Camino a la perfección (Fortaleza)|modificada|
|[monk-09-CoRfFkisEsHE1e43.htm](classfeatures/monk-09-CoRfFkisEsHE1e43.htm)|Metal Strikes|Golpes de metal|modificada|
|[monk-09-lxImO5D0qWp0gXFB.htm](classfeatures/monk-09-lxImO5D0qWp0gXFB.htm)|Monk Expertise|Experiencia de monje|modificada|
|[monk-11-RVPhB0RqmoJg7xI6.htm](classfeatures/monk-11-RVPhB0RqmoJg7xI6.htm)|Second Path to Perfection (Will)|Segundo camino a la perfección (Voluntad)|modificada|
|[monk-11-XZnPwZ0ohlDXlFea.htm](classfeatures/monk-11-XZnPwZ0ohlDXlFea.htm)|Second Path to Perfection (Fortitude)|Segundo camino a la perfección (Fortaleza)|modificada|
|[monk-11-y6qnbUc8y0815QNE.htm](classfeatures/monk-11-y6qnbUc8y0815QNE.htm)|Second Path to Perfection|Segundo camino a la perfección|modificada|
|[monk-11-yDL9l9Klki6gE2ZD.htm](classfeatures/monk-11-yDL9l9Klki6gE2ZD.htm)|Second Path to Perfection (Reflex)|Segundo camino a la perfección (Reflejos)|modificada|
|[monk-13-0iidKkzC2yy13lIf.htm](classfeatures/monk-13-0iidKkzC2yy13lIf.htm)|Master Strikes|Golpes maestros|modificada|
|[monk-13-95LI24ZSx0d4qfKX.htm](classfeatures/monk-13-95LI24ZSx0d4qfKX.htm)|Graceful Mastery|Maestría grácil|modificada|
|[monk-15-8kukH9c4h82e3qjl.htm](classfeatures/monk-15-8kukH9c4h82e3qjl.htm)|Third Path to Perfection (Reflex)|Tercer camino a la perfección (Reflejo)|modificada|
|[monk-15-dUMsM0yDTCdV31p6.htm](classfeatures/monk-15-dUMsM0yDTCdV31p6.htm)|Third Path to Perfection (Fortitude)|Tercer camino a la perfección (Fortaleza)|modificada|
|[monk-15-haoTkr2U5k7kaAKN.htm](classfeatures/monk-15-haoTkr2U5k7kaAKN.htm)|Third Path to Perfection|Tercer camino a la perfección|modificada|
|[monk-15-oVNRYF0FHbH8NsJD.htm](classfeatures/monk-15-oVNRYF0FHbH8NsJD.htm)|Third Path to Perfection (Will)|Tercer camino a la perfección (Voluntad)|modificada|
|[monk-17-5cthRUkRqRtduVvN.htm](classfeatures/monk-17-5cthRUkRqRtduVvN.htm)|Adamantine Strikes|Golpes adamantinos|modificada|
|[monk-17-JWDfzYub3JfuEtth.htm](classfeatures/monk-17-JWDfzYub3JfuEtth.htm)|Graceful Legend|Leyenda grácil|modificada|
|[monk-19-KmTfg7Sg5va4yU00.htm](classfeatures/monk-19-KmTfg7Sg5va4yU00.htm)|Perfected Form|Forma perfeccionada|modificada|
|[None-00-6RAHnACKReBW68Sa.htm](classfeatures/None-00-6RAHnACKReBW68Sa.htm)|Encroaching Presence|Encroaching Presence|modificada|
|[None-00-7PutMrgKIaAeKBuU.htm](classfeatures/None-00-7PutMrgKIaAeKBuU.htm)|Catharsis Emotion (Fear)|Catharsis Emotion (Miedo)|modificada|
|[None-00-8Jjy7VsQAJJqCeyE.htm](classfeatures/None-00-8Jjy7VsQAJJqCeyE.htm)|Wraith Deviant Classification|Wraith Deviant Clasificación|modificada|
|[None-00-8PjTI21Mif26XWY7.htm](classfeatures/None-00-8PjTI21Mif26XWY7.htm)|Energetic Meltdown|Energetic Meltdown|modificada|
|[None-00-ccrTv2j7eplr2JU8.htm](classfeatures/None-00-ccrTv2j7eplr2JU8.htm)|Catharsis Emotion (Pride)|Catharsis Emotion (Orgullo)|modificada|
|[None-00-eNFYMQMS2BszErCX.htm](classfeatures/None-00-eNFYMQMS2BszErCX.htm)|Catharsis Emotion (Dedication)|Catharsis Emotion (Dedicación)|modificada|
|[None-00-HPYo5j8GgQIVoOOr.htm](classfeatures/None-00-HPYo5j8GgQIVoOOr.htm)|Troll Deviant Classification|Clasificación Troll Deviant|modificada|
|[None-00-huDGjojm3hnuA1E8.htm](classfeatures/None-00-huDGjojm3hnuA1E8.htm)|Strained Metabolism|Strained Metabolism|modificada|
|[None-00-igMHwREgpM9GsvLs.htm](classfeatures/None-00-igMHwREgpM9GsvLs.htm)|Order of the Gate|Orden del umbral|modificada|
|[None-00-jklanV9AbZDnZHaD.htm](classfeatures/None-00-jklanV9AbZDnZHaD.htm)|Catharsis Emotion (Misery)|Catharsis Emotion (Misery)|modificada|
|[None-00-lDjKVZ48zbqQf3CU.htm](classfeatures/None-00-lDjKVZ48zbqQf3CU.htm)|Catharsis Emotion (Love)|Catharsis Emotion (Love)|modificada|
|[None-00-lVdfcITy5bkywW5f.htm](classfeatures/None-00-lVdfcITy5bkywW5f.htm)|Order of the Rack|Orden del Estante|modificada|
|[None-00-MizJPiKnopfpGmvw.htm](classfeatures/None-00-MizJPiKnopfpGmvw.htm)|Catharsis Emotion (Anger)|Catharsis Emotion (Anger)|modificada|
|[None-00-R41sy7weOd0JhOiW.htm](classfeatures/None-00-R41sy7weOd0JhOiW.htm)|Dragon Deviant Classification|Dragon Deviant Clasificación|modificada|
|[None-00-rgLaxC3I2L7HSPNn.htm](classfeatures/None-00-rgLaxC3I2L7HSPNn.htm)|Catharsis Emotion (Awe)|Catharsis Emotion (Awe)|modificada|
|[None-00-S4uY5ap2yhDRqhd0.htm](classfeatures/None-00-S4uY5ap2yhDRqhd0.htm)|Catharsis Emotion (Hatred)|Catharsis Emotion (Odio)|modificada|
|[None-00-t01K3DB2qHnbt1q3.htm](classfeatures/None-00-t01K3DB2qHnbt1q3.htm)|Order of the Scourge|Orden del Azote|modificada|
|[None-00-T9gUmwghw16itUAO.htm](classfeatures/None-00-T9gUmwghw16itUAO.htm)|Catharsis Emotion (Remorse)|Catharsis Emotion (Remordimiento)|modificada|
|[None-00-ub9gwFXnMuKvhnPL.htm](classfeatures/None-00-ub9gwFXnMuKvhnPL.htm)|Order of the Nail|Orden del Clavo|modificada|
|[None-00-UTRDN1TAieBMjwP1.htm](classfeatures/None-00-UTRDN1TAieBMjwP1.htm)|Order of the Godclaw|Orden del Godclaw|modificada|
|[None-00-YrJj8UI0XpkHv0Ho.htm](classfeatures/None-00-YrJj8UI0XpkHv0Ho.htm)|Order of the Chain|Orden de la Cadena|modificada|
|[None-00-zFY2PlJTObXpcQW4.htm](classfeatures/None-00-zFY2PlJTObXpcQW4.htm)|Catharsis Emotion (Joy)|Catharsis Emotion (Joy)|modificada|
|[None-00-zGxO2cETUsXuvqRu.htm](classfeatures/None-00-zGxO2cETUsXuvqRu.htm)|Order of the Pyre|Orden de la Pira|modificada|
|[None-01-1FPVkksuE2ncw9rF.htm](classfeatures/None-01-1FPVkksuE2ncw9rF.htm)|Ki Spells|Conjuros de ki|modificada|
|[None-01-1tyNn9sduyexXLfL.htm](classfeatures/None-01-1tyNn9sduyexXLfL.htm)|Psi Cantrips and Amps|Psi Trucos y Amplificadores|modificada|
|[None-01-HwUps0waR29bwlTI.htm](classfeatures/None-01-HwUps0waR29bwlTI.htm)|Unleash Psyche|Unleash Psyche|modificada|
|[None-01-HYTaibaCGE85rhbZ.htm](classfeatures/None-01-HYTaibaCGE85rhbZ.htm)|Runelord Specialization|Especialización Runelord|modificada|
|[None-01-mRvyq7G0rqRP1EAr.htm](classfeatures/None-01-mRvyq7G0rqRP1EAr.htm)|Wellspring Magic|Wellspring Magic|modificada|
|[None-01-pUkUC8HHom2DmYzz.htm](classfeatures/None-01-pUkUC8HHom2DmYzz.htm)|Elemental Magic|Magia elemental|modificada|
|[None-01-T25ZLQWn6O4KchLo.htm](classfeatures/None-01-T25ZLQWn6O4KchLo.htm)|Focus Spells|Conjuros de foco|modificada|
|[None-01-Upf1LXtWNJ6eB5sm.htm](classfeatures/None-01-Upf1LXtWNJ6eB5sm.htm)|Flexible Spell Preparation|Preparación flexible de hechizos|modificada|
|[None-03-D8CSi8c9XiRpVc5M.htm](classfeatures/None-03-D8CSi8c9XiRpVc5M.htm)|Alertness|Alerta|modificada|
|[None-03-F57Na5VxfBp56kke.htm](classfeatures/None-03-F57Na5VxfBp56kke.htm)|Great Fortitude|Gran fortaleza|modificada|
|[None-03-TUOeATt52P43r5W0.htm](classfeatures/None-03-TUOeATt52P43r5W0.htm)|Lightning Reflexes|Reflejos rápidos|modificada|
|[None-03-wMyDcVNmA7xGK83S.htm](classfeatures/None-03-wMyDcVNmA7xGK83S.htm)|Iron Will|Voluntad de hierro|modificada|
|[None-05-70jqXP2eS4tRZ0Ok.htm](classfeatures/None-05-70jqXP2eS4tRZ0Ok.htm)|Magical Fortitude|Fortaleza mágica|modificada|
|[None-05-9XLUh9iMepZesdmc.htm](classfeatures/None-05-9XLUh9iMepZesdmc.htm)|Weapon Expertise|Experiencia con las armas|modificada|
|[None-07-0npO4rPscGm0dX13.htm](classfeatures/None-07-0npO4rPscGm0dX13.htm)|Vigilant Senses|Sentidos vigilantes|modificada|
|[None-07-9EqIasqfI8YIM3Pt.htm](classfeatures/None-07-9EqIasqfI8YIM3Pt.htm)|Weapon Specialization|Especialización en un arma|modificada|
|[None-07-cD3nSupdCvONuHiE.htm](classfeatures/None-07-cD3nSupdCvONuHiE.htm)|Expert Spellcaster|Lanzador de conjuros experto|modificada|
|[None-07-JQAujUXjczVnYDEI.htm](classfeatures/None-07-JQAujUXjczVnYDEI.htm)|Resolve|Resolución|modificada|
|[None-07-MV6XIuAgN9uSA0Da.htm](classfeatures/None-07-MV6XIuAgN9uSA0Da.htm)|Evasion|Evasión|modificada|
|[None-07-OMZs5y16jZRW9KQK.htm](classfeatures/None-07-OMZs5y16jZRW9KQK.htm)|Juggernaut|Juggernaut|modificada|
|[None-07-x5jaCJxsmD5sx3KB.htm](classfeatures/None-07-x5jaCJxsmD5sx3KB.htm)|Armor Expertise|Armor Expertise|modificada|
|[None-11-esyKPSDbFQPB4lhq.htm](classfeatures/None-11-esyKPSDbFQPB4lhq.htm)|Medium Armor Expertise (Inventor)|Experiencia con armadura intermedia (Inventor).|modificada|
|[None-11-FCEp9jjxxgRJDJV3.htm](classfeatures/None-11-FCEp9jjxxgRJDJV3.htm)|Medium Armor Expertise|Experiencia con armadura intermedia|modificada|
|[None-13-a58MGVX2L589sC9g.htm](classfeatures/None-13-a58MGVX2L589sC9g.htm)|Psychic Weapon Specialization|Especialización en arma psíquica|modificada|
|[None-13-CGB1TczFhQhdQxml.htm](classfeatures/None-13-CGB1TczFhQhdQxml.htm)|Armor Mastery|Dominio de la armadura|modificada|
|[None-13-L5D0NwFXdLiVSnk5.htm](classfeatures/None-13-L5D0NwFXdLiVSnk5.htm)|Improved Evasion|Evasión mejorada.|modificada|
|[None-13-nLwPMPLRne1HnL00.htm](classfeatures/None-13-nLwPMPLRne1HnL00.htm)|Incredible Senses|Sentidos increíbles|modificada|
|[None-15-l1InYvhnQSz6Ucxc.htm](classfeatures/None-15-l1InYvhnQSz6Ucxc.htm)|Master Spellcaster|Lanzador de conjuros maestro|modificada|
|[None-17-cGMSYAErbUG5E8X2.htm](classfeatures/None-17-cGMSYAErbUG5E8X2.htm)|Medium Armor Mastery|Medium Armor Mastery|modificada|
|[oracle-01-7AVspOB6ITNzGFZi.htm](classfeatures/oracle-01-7AVspOB6ITNzGFZi.htm)|Divine Spellcasting (Oracle)|Lanzamiento de conjuros divinos (Oráculo).|modificada|
|[oracle-01-cFe6vFb3gSDyNeS9.htm](classfeatures/oracle-01-cFe6vFb3gSDyNeS9.htm)|Spell Repertoire (Oracle)|Repertorio de conjuros (Oráculo)|modificada|
|[oracle-01-EslxR2sbDK9XJaAl.htm](classfeatures/oracle-01-EslxR2sbDK9XJaAl.htm)|Time|Tiempo|modificada|
|[oracle-01-gjOGOR30Czpnx3tM.htm](classfeatures/oracle-01-gjOGOR30Czpnx3tM.htm)|Battle|Batalla|modificada|
|[oracle-01-GTSvbFb36InvuH0w.htm](classfeatures/oracle-01-GTSvbFb36InvuH0w.htm)|Flames|Flamígera|modificada|
|[oracle-01-IaxmCkdsPlA52spu.htm](classfeatures/oracle-01-IaxmCkdsPlA52spu.htm)|Bones|Bones|modificada|
|[oracle-01-ibX2EhKkyUtbOHLj.htm](classfeatures/oracle-01-ibX2EhKkyUtbOHLj.htm)|Oracular Curse|Oracular Curse|modificada|
|[oracle-01-NXUOtO9NytHQurlg.htm](classfeatures/oracle-01-NXUOtO9NytHQurlg.htm)|Revelation Spells|Hechizos de Revelación|modificada|
|[oracle-01-o1gGG36wpn9mxeop.htm](classfeatures/oracle-01-o1gGG36wpn9mxeop.htm)|Life|Vida|modificada|
|[oracle-01-PRJYLksQEwT39bTl.htm](classfeatures/oracle-01-PRJYLksQEwT39bTl.htm)|Mystery|Misterio|modificada|
|[oracle-01-qvRlih3u7vK3FYUR.htm](classfeatures/oracle-01-qvRlih3u7vK3FYUR.htm)|Ancestors|Ancestros|modificada|
|[oracle-01-RI2EMRBBPNSoTJXu.htm](classfeatures/oracle-01-RI2EMRBBPNSoTJXu.htm)|Cosmos|Cosmos|modificada|
|[oracle-01-tZBb3Kh4nJcNoUFI.htm](classfeatures/oracle-01-tZBb3Kh4nJcNoUFI.htm)|Lore|Lore|modificada|
|[oracle-01-tzDW9l4lBwXCVYtz.htm](classfeatures/oracle-01-tzDW9l4lBwXCVYtz.htm)|Ashes|Cenizas|modificada|
|[oracle-01-W9cF7wZztLDb1WGY.htm](classfeatures/oracle-01-W9cF7wZztLDb1WGY.htm)|Tempest|Tempest|modificada|
|[oracle-11-rrzItB68Er0DzKx7.htm](classfeatures/oracle-11-rrzItB68Er0DzKx7.htm)|Major Curse|Maldición superior|modificada|
|[oracle-17-5LOARurr4qWkfS9K.htm](classfeatures/oracle-17-5LOARurr4qWkfS9K.htm)|Greater Resolve|Resolución mayor.|modificada|
|[oracle-17-F4brPlp1tHGUqyuI.htm](classfeatures/oracle-17-F4brPlp1tHGUqyuI.htm)|Extreme Curse|Maldición Extrema|modificada|
|[oracle-19-571c1aGnvNVwfF6b.htm](classfeatures/oracle-19-571c1aGnvNVwfF6b.htm)|Oracular Clarity|Claridad Oracular|modificada|
|[psychic-01-0fv6NVMZZ0peGL9e.htm](classfeatures/psychic-01-0fv6NVMZZ0peGL9e.htm)|Conscious Mind|Mente Consciente|modificada|
|[psychic-01-0UKDEtZ7ffTEsqCK.htm](classfeatures/psychic-01-0UKDEtZ7ffTEsqCK.htm)|Gathered Lore|Gathered Lore|modificada|
|[psychic-01-1mMdsSIVsyyqNr2t.htm](classfeatures/psychic-01-1mMdsSIVsyyqNr2t.htm)|Spell Repertoire (Psychic)|Repertorio de conjuros (Psíquico)|modificada|
|[psychic-01-1rlBo3LcwIuPDCvz.htm](classfeatures/psychic-01-1rlBo3LcwIuPDCvz.htm)|The Distant Grasp|The Distant Grasp|modificada|
|[psychic-01-5tSR0WzMPFn5s3Xs.htm](classfeatures/psychic-01-5tSR0WzMPFn5s3Xs.htm)|The Oscillating Wave|La onda oscilante|modificada|
|[psychic-01-79ZetrRF6S01P4Vf.htm](classfeatures/psychic-01-79ZetrRF6S01P4Vf.htm)|Subconscious Mind|Subconscious Mind|modificada|
|[psychic-01-8dAm3ULUqaK4N5a7.htm](classfeatures/psychic-01-8dAm3ULUqaK4N5a7.htm)|The Tangible Dream|El Sue tangible|modificada|
|[psychic-01-EMErBnhrgUHEKAsZ.htm](classfeatures/psychic-01-EMErBnhrgUHEKAsZ.htm)|Wandering Reverie|Wandering Reverie|modificada|
|[psychic-01-FaSY47siV1x6CAQp.htm](classfeatures/psychic-01-FaSY47siV1x6CAQp.htm)|The Unbound Step|El Paso Sin Ataduras|modificada|
|[psychic-01-iXwqJyBsjJNrKJae.htm](classfeatures/psychic-01-iXwqJyBsjJNrKJae.htm)|Psychic Spellcasting|Psychic Spellcasting|modificada|
|[psychic-01-PNihL10QAB1sYSRn.htm](classfeatures/psychic-01-PNihL10QAB1sYSRn.htm)|Emotional Acceptance|Aceptación emocional|modificada|
|[psychic-01-Qrhw4SILfT8YNQgB.htm](classfeatures/psychic-01-Qrhw4SILfT8YNQgB.htm)|Precise Discipline|Disciplina precisión|modificada|
|[psychic-01-rdWWlqvxXgfWDaSO.htm](classfeatures/psychic-01-rdWWlqvxXgfWDaSO.htm)|The Silent Whisper|El susurro silencioso|modificada|
|[psychic-01-rvpTsj9epRuNH3uB.htm](classfeatures/psychic-01-rvpTsj9epRuNH3uB.htm)|The Infinite Eye|El Ojo Infinito|modificada|
|[psychic-05-Ftz5jVa9X6aXybkC.htm](classfeatures/psychic-05-Ftz5jVa9X6aXybkC.htm)|Precognitive Reflexes|Reflejos Precognitivos|modificada|
|[psychic-05-k9MeSdp2DbGd1hFz.htm](classfeatures/psychic-05-k9MeSdp2DbGd1hFz.htm)|Clarity of Focus|Claridad de enfoque|modificada|
|[psychic-09-zAe95Uk5IPIT23K1.htm](classfeatures/psychic-09-zAe95Uk5IPIT23K1.htm)|Great Fortitude (Psychic)|Gran fortaleza (Psíquico)|modificada|
|[psychic-11-Kf9lSN6pVS2Hy4KI.htm](classfeatures/psychic-11-Kf9lSN6pVS2Hy4KI.htm)|Walls of Will|Muros de Voluntad|modificada|
|[psychic-11-kLschzVZFoe3U63C.htm](classfeatures/psychic-11-kLschzVZFoe3U63C.htm)|Psychic Weapon Expertise|Experiencia con las armas psíquicas|modificada|
|[psychic-11-wOl7EeF7S6i753Ef.htm](classfeatures/psychic-11-wOl7EeF7S6i753Ef.htm)|Extrasensory Perception|Percepción extrasensorial|modificada|
|[psychic-13-MtHLCQGD6OW98WC2.htm](classfeatures/psychic-13-MtHLCQGD6OW98WC2.htm)|Personal Barrier|Barrera personal|modificada|
|[psychic-17-Hw6Ji7Fgx0XkVkac.htm](classfeatures/psychic-17-Hw6Ji7Fgx0XkVkac.htm)|Fortress of Will|Fortaleza de la voluntad|modificada|
|[psychic-19-mZwD2brwXlyR9RAR.htm](classfeatures/psychic-19-mZwD2brwXlyR9RAR.htm)|Infinite Mind|Mente Infinita|modificada|
|[ranger-01-0nIOGpHQNHsKSFKT.htm](classfeatures/ranger-01-0nIOGpHQNHsKSFKT.htm)|Hunt Prey|Perseguir presa|modificada|
|[ranger-01-6v4Rj7wWfOH1882r.htm](classfeatures/ranger-01-6v4Rj7wWfOH1882r.htm)|Flurry|Ráfaga|modificada|
|[ranger-01-mzkkj9LEWjJPBhaq.htm](classfeatures/ranger-01-mzkkj9LEWjJPBhaq.htm)|Hunter's Edge|Ventaja del cazador|modificada|
|[ranger-01-NBHyoTrI8q62uDsU.htm](classfeatures/ranger-01-NBHyoTrI8q62uDsU.htm)|Outwit|Ser más listo|modificada|
|[ranger-01-u6cBjqz2fiRBadBt.htm](classfeatures/ranger-01-u6cBjqz2fiRBadBt.htm)|Precision|Precisión|modificada|
|[ranger-01-w3HysrCgDs5uFXKX.htm](classfeatures/ranger-01-w3HysrCgDs5uFXKX.htm)|Warden Spells|Hechizos Warden|modificada|
|[ranger-05-PeZi7E9lI4vz8EGY.htm](classfeatures/ranger-05-PeZi7E9lI4vz8EGY.htm)|Trackless Step|Pisada sin rastro|modificada|
|[ranger-05-QhoW8ivPvYmWzyEZ.htm](classfeatures/ranger-05-QhoW8ivPvYmWzyEZ.htm)|Ranger Weapon Expertise|Experiencia con las armas de explorador|modificada|
|[ranger-09-5likl5SAxQPrQ3KF.htm](classfeatures/ranger-09-5likl5SAxQPrQ3KF.htm)|Ranger Expertise|Experiencia de explorador|modificada|
|[ranger-09-j2R64kwUgEJ1TudD.htm](classfeatures/ranger-09-j2R64kwUgEJ1TudD.htm)|Nature's Edge|Ventaja de la Naturaleza|modificada|
|[ranger-11-RlwE99yKnhq8FUuy.htm](classfeatures/ranger-11-RlwE99yKnhq8FUuy.htm)|Wild Stride|Zancada salvaje|modificada|
|[ranger-17-BJYSUbFUGcTLaPDn.htm](classfeatures/ranger-17-BJYSUbFUGcTLaPDn.htm)|Masterful Hunter (Precision)|Cazador maestro (precisión)|modificada|
|[ranger-17-JhLncIB10GSQowWL.htm](classfeatures/ranger-17-JhLncIB10GSQowWL.htm)|Masterful Hunter (Flurry)|Cazador maestro (Ráfaga).|modificada|
|[ranger-17-RVZC4wVy5B5W2OeS.htm](classfeatures/ranger-17-RVZC4wVy5B5W2OeS.htm)|Masterful Hunter|Cazador maestro|modificada|
|[ranger-17-vWZaLE2fEKMBw3D5.htm](classfeatures/ranger-17-vWZaLE2fEKMBw3D5.htm)|Masterful Hunter (Outwit)|Cazador maestro (Outwit)|modificada|
|[ranger-19-bBGb1LcffXEqar0p.htm](classfeatures/ranger-19-bBGb1LcffXEqar0p.htm)|Swift Prey|Presa rápida|modificada|
|[ranger-19-phwQ2MrDZ13D2HxC.htm](classfeatures/ranger-19-phwQ2MrDZ13D2HxC.htm)|Second Skin|Segunda piel|modificada|
|[rogue-01-3KPZ7svIO6kmmEKH.htm](classfeatures/rogue-01-3KPZ7svIO6kmmEKH.htm)|Ruffian|Rufián|modificada|
|[rogue-01-j1JE61quDxdge4mg.htm](classfeatures/rogue-01-j1JE61quDxdge4mg.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[rogue-01-uGuCGQvUmioFV2Bd.htm](classfeatures/rogue-01-uGuCGQvUmioFV2Bd.htm)|Rogue's Racket|Enredos de pícaro|modificada|
|[rogue-01-w6rMqmGzhUahdnA7.htm](classfeatures/rogue-01-w6rMqmGzhUahdnA7.htm)|Surprise Attack|Atacante por sorpresa|modificada|
|[rogue-01-wAh2riuFRzz0edPl.htm](classfeatures/rogue-01-wAh2riuFRzz0edPl.htm)|Thief|Ladrón|modificada|
|[rogue-01-ZvfxtUMtfIOLYHyg.htm](classfeatures/rogue-01-ZvfxtUMtfIOLYHyg.htm)|Scoundrel|Granuja|modificada|
|[rogue-03-PNpmVmD21zViDtGC.htm](classfeatures/rogue-03-PNpmVmD21zViDtGC.htm)|Deny Advantage|Denegar ventaja|modificada|
|[rogue-05-v8UNEJR5IDKi8yqa.htm](classfeatures/rogue-05-v8UNEJR5IDKi8yqa.htm)|Weapon Tricks|Trucos con las armas|modificada|
|[rogue-09-9SruVg2lZpNaYLOB.htm](classfeatures/rogue-09-9SruVg2lZpNaYLOB.htm)|Debilitating Strikes|Golpes Debilitación|modificada|
|[rogue-11-f3Dh32EU4VsHu01b.htm](classfeatures/rogue-11-f3Dh32EU4VsHu01b.htm)|Rogue Expertise|Experiencia de pícaro|modificada|
|[rogue-13-myvcir1LEkaVxOlE.htm](classfeatures/rogue-13-myvcir1LEkaVxOlE.htm)|Master Tricks|Trucos maestros|modificada|
|[rogue-15-W1FkMHYVDg3yTU5r.htm](classfeatures/rogue-15-W1FkMHYVDg3yTU5r.htm)|Double Debilitation|Debilitación doble|modificada|
|[rogue-17-xmZ7oeTDcQVXegUP.htm](classfeatures/rogue-17-xmZ7oeTDcQVXegUP.htm)|Slippery Mind|Mente escurridiza|modificada|
|[rogue-19-SUUdWG0t33VKa5q4.htm](classfeatures/rogue-19-SUUdWG0t33VKa5q4.htm)|Master Strike|Golpe maestro|modificada|
|[sorcerer-01-2goYo6VNbwC6aKF1.htm](classfeatures/sorcerer-01-2goYo6VNbwC6aKF1.htm)|Bloodline|Linaje|modificada|
|[sorcerer-01-3qqvnC2U8W26yae7.htm](classfeatures/sorcerer-01-3qqvnC2U8W26yae7.htm)|Bloodline: Aberrant|Linaje: Aberrante|modificada|
|[sorcerer-01-7WBZ2kkhZ7JorWu2.htm](classfeatures/sorcerer-01-7WBZ2kkhZ7JorWu2.htm)|Bloodline: Undead|Linaje: Muertos vivientes|modificada|
|[sorcerer-01-dKTb959aCQIzSIXj.htm](classfeatures/sorcerer-01-dKTb959aCQIzSIXj.htm)|Bloodline: Wyrmblessed|Linaje: Bendito por las sierpes|modificada|
|[sorcerer-01-eW3cfCH7Wpx2vik2.htm](classfeatures/sorcerer-01-eW3cfCH7Wpx2vik2.htm)|Bloodline: Fey|Linaje: Feérico|modificada|
|[sorcerer-01-gmnx7e1g08bppbqt.htm](classfeatures/sorcerer-01-gmnx7e1g08bppbqt.htm)|Sorcerer Spellcasting|Lanzamiento de conjuros de hechicero|modificada|
|[sorcerer-01-H6ziAPvCipTPG8SH.htm](classfeatures/sorcerer-01-H6ziAPvCipTPG8SH.htm)|Bloodline Spells|Conjuros de linaje|modificada|
|[sorcerer-01-lURKSJZAGKVD6cH9.htm](classfeatures/sorcerer-01-lURKSJZAGKVD6cH9.htm)|Spell Repertoire (Sorcerer)|Repertorio de conjuros (Hechicero)|modificada|
|[sorcerer-01-O0uXZRWMNliDbkxU.htm](classfeatures/sorcerer-01-O0uXZRWMNliDbkxU.htm)|Bloodline: Hag|Linaje: Maléfico|modificada|
|[sorcerer-01-o39zQMIdERWtmBSB.htm](classfeatures/sorcerer-01-o39zQMIdERWtmBSB.htm)|Bloodline: Diabolic|Linaje: Diabólico|modificada|
|[sorcerer-01-RXRnJcG4XSabZ35a.htm](classfeatures/sorcerer-01-RXRnJcG4XSabZ35a.htm)|Bloodline: Elemental|Linaje: Elemental|modificada|
|[sorcerer-01-TWR1wbPJuCLnGdFZ.htm](classfeatures/sorcerer-01-TWR1wbPJuCLnGdFZ.htm)|Bloodline: Phoenix|Linaje: Fénix|modificada|
|[sorcerer-01-vhW3glAaEfq2DKrw.htm](classfeatures/sorcerer-01-vhW3glAaEfq2DKrw.htm)|Bloodline: Angelic|Linaje: Angelical|modificada|
|[sorcerer-01-w5koctOVrEcpxTIq.htm](classfeatures/sorcerer-01-w5koctOVrEcpxTIq.htm)|Bloodline: Demonic|Linaje: Demoníacaíaco|modificada|
|[sorcerer-01-ZEtJJ5UOlV5oTWWp.htm](classfeatures/sorcerer-01-ZEtJJ5UOlV5oTWWp.htm)|Bloodline: Imperial|Linaje: Imperial|modificada|
|[sorcerer-01-ZHabYxSgYK0XbjhM.htm](classfeatures/sorcerer-01-ZHabYxSgYK0XbjhM.htm)|Bloodline: Draconic|Linaje: Dracónico|modificada|
|[sorcerer-03-VKRjmXxBFLrJK01c.htm](classfeatures/sorcerer-03-VKRjmXxBFLrJK01c.htm)|Signature Spells|Conjuros de signatura|modificada|
|[sorcerer-19-feCnVrPPlKhl701x.htm](classfeatures/sorcerer-19-feCnVrPPlKhl701x.htm)|Bloodline Paragon|Parangón del linaje|modificada|
|[summoner-01-gWcN75VNpSZ4FqNb.htm](classfeatures/summoner-01-gWcN75VNpSZ4FqNb.htm)|Summoner Spellcasting|Hechizo de Invocador|modificada|
|[summoner-01-IPcdQAwJk0aZe5mg.htm](classfeatures/summoner-01-IPcdQAwJk0aZe5mg.htm)|Evolution Feat|Evolución Feat|modificada|
|[summoner-01-Ju2Tp5s5iBB76tQO.htm](classfeatures/summoner-01-Ju2Tp5s5iBB76tQO.htm)|Spell Repertoire (Summoner)|Repertorio de conjuros (Invocador)|modificada|
|[summoner-01-qOEpe596B0UjhcG0.htm](classfeatures/summoner-01-qOEpe596B0UjhcG0.htm)|Eidolon|Eidolon|modificada|
|[summoner-01-wguqw300DB5XdD8W.htm](classfeatures/summoner-01-wguqw300DB5XdD8W.htm)|Link Spells|Hechizos de Enlace|modificada|
|[summoner-03-P34Jx6i4GJGoqTtG.htm](classfeatures/summoner-03-P34Jx6i4GJGoqTtG.htm)|Unlimited Signature Spells|Conjuros de signatura ilimitados.|modificada|
|[summoner-03-QiMlJ33kNEoyh1M0.htm](classfeatures/summoner-03-QiMlJ33kNEoyh1M0.htm)|Shared Vigilance|Vigilancia Compartida|modificada|
|[summoner-05-GI5IAl4dkly4At8e.htm](classfeatures/summoner-05-GI5IAl4dkly4At8e.htm)|Ability Boosts|Mejoras de característica|modificada|
|[summoner-05-pda6iUaU9waXId5Q.htm](classfeatures/summoner-05-pda6iUaU9waXId5Q.htm)|Eidolon Unarmed Expertise|Eidolon pericia sin armas|modificada|
|[summoner-07-oCnyGRvkfjTsZXcX.htm](classfeatures/summoner-07-oCnyGRvkfjTsZXcX.htm)|Eidolon Weapon Specialization|Especialización en arma de eidolon.|modificada|
|[summoner-07-skQBrwRwJW2K6ACj.htm](classfeatures/summoner-07-skQBrwRwJW2K6ACj.htm)|Eidolon Symbiosis|Simbiosis Eidolon|modificada|
|[summoner-09-dZNAXTQovlWVvAyX.htm](classfeatures/summoner-09-dZNAXTQovlWVvAyX.htm)|Shared Reflexes|Reflejos Compartidos|modificada|
|[summoner-11-2CZPYoyWih6zYTcb.htm](classfeatures/summoner-11-2CZPYoyWih6zYTcb.htm)|Eidolon Defensive Expertise|Pericia Defensiva Eidolon|modificada|
|[summoner-11-q1Y12Pg2gQg2FJPR.htm](classfeatures/summoner-11-q1Y12Pg2gQg2FJPR.htm)|Twin Juggernauts|Juggernauts gemelos|modificada|
|[summoner-13-NIzHfVcVMhDmvA49.htm](classfeatures/summoner-13-NIzHfVcVMhDmvA49.htm)|Eidolon Unarmed Mastery|Eidolon Dominio sin armas|modificada|
|[summoner-15-B5SyM7qHrU0gTGR0.htm](classfeatures/summoner-15-B5SyM7qHrU0gTGR0.htm)|Greater Eidolon Specialization|Mayor Especialización Eidolon|modificada|
|[summoner-15-eZPfHVz14j42jCnS.htm](classfeatures/summoner-15-eZPfHVz14j42jCnS.htm)|Shared Resolve|Resolución compartida|modificada|
|[summoner-17-nCE9DzkugRefREqT.htm](classfeatures/summoner-17-nCE9DzkugRefREqT.htm)|Eidolon Transcendence|Trascendencia del Eidolon|modificada|
|[summoner-19-0WvI8KM5m0SaZ3MH.htm](classfeatures/summoner-19-0WvI8KM5m0SaZ3MH.htm)|Eidolon Defensive Mastery|Eidolon Dominio Defensivo|modificada|
|[summoner-19-H0iWhiyP0QqgmAKs.htm](classfeatures/summoner-19-H0iWhiyP0QqgmAKs.htm)|Instant Manifestation|Manifestación instantánea|modificada|
|[swashbuckler-01-4lGhbEjlEoGP4scl.htm](classfeatures/swashbuckler-01-4lGhbEjlEoGP4scl.htm)|Wit|Wit|modificada|
|[swashbuckler-01-5HoEwzLDJGTCZtFa.htm](classfeatures/swashbuckler-01-5HoEwzLDJGTCZtFa.htm)|Battledancer|Battledancer|modificada|
|[swashbuckler-01-B7RMnrHwQHlezlJT.htm](classfeatures/swashbuckler-01-B7RMnrHwQHlezlJT.htm)|Gymnast|Gimnasta|modificada|
|[swashbuckler-01-beW1OqibVQ3fBvRw.htm](classfeatures/swashbuckler-01-beW1OqibVQ3fBvRw.htm)|Swashbuckler's Style|Swashbuckler's Style|modificada|
|[swashbuckler-01-Jgid6Ja6Y879COlN.htm](classfeatures/swashbuckler-01-Jgid6Ja6Y879COlN.htm)|Fencer|Esgrimista|modificada|
|[swashbuckler-01-KBhwFjdptrKyN5EM.htm](classfeatures/swashbuckler-01-KBhwFjdptrKyN5EM.htm)|Braggart|Braggart|modificada|
|[swashbuckler-01-LzYi0OuOoypNb6jd.htm](classfeatures/swashbuckler-01-LzYi0OuOoypNb6jd.htm)|Panache|Panache|modificada|
|[swashbuckler-01-pyo0vmxUFIFX2GNl.htm](classfeatures/swashbuckler-01-pyo0vmxUFIFX2GNl.htm)|Confident Finisher|Confident Finisher|modificada|
|[swashbuckler-01-RQH6vigvhmiYKKjg.htm](classfeatures/swashbuckler-01-RQH6vigvhmiYKKjg.htm)|Precise Strike|Golpe precisión|modificada|
|[swashbuckler-03-8BOFeRE7ZfJ02N0O.htm](classfeatures/swashbuckler-03-8BOFeRE7ZfJ02N0O.htm)|Vivacious Speed|Velocidad Vivaz|modificada|
|[swashbuckler-03-Jtn7IugykXDlIoZq.htm](classfeatures/swashbuckler-03-Jtn7IugykXDlIoZq.htm)|Opportune Riposte|Opportune Riposte|modificada|
|[swashbuckler-03-pthjQIK9pDxnbER6.htm](classfeatures/swashbuckler-03-pthjQIK9pDxnbER6.htm)|Stylish Tricks|Trucos con estilo|modificada|
|[swashbuckler-05-F5BHEav90oOJ2LwN.htm](classfeatures/swashbuckler-05-F5BHEav90oOJ2LwN.htm)|Weapon Expertise (Swashbuckler)|Experiencia con las armas (Swashbuckler)|modificada|
|[swashbuckler-09-KxpaxUSuBC7hr4F7.htm](classfeatures/swashbuckler-09-KxpaxUSuBC7hr4F7.htm)|Exemplary Finisher|Rematador ejemplar|modificada|
|[swashbuckler-09-U74JoAcLHTOsZG6q.htm](classfeatures/swashbuckler-09-U74JoAcLHTOsZG6q.htm)|Swashbuckler Expertise|Pericia de espadachín|modificada|
|[swashbuckler-11-13QpCrR8a8XULbJa.htm](classfeatures/swashbuckler-11-13QpCrR8a8XULbJa.htm)|Continuous Flair|Continuous Flair|modificada|
|[swashbuckler-13-i6563IU7x4L9oRgC.htm](classfeatures/swashbuckler-13-i6563IU7x4L9oRgC.htm)|Weapon Mastery|Maestría con las armas|modificada|
|[swashbuckler-13-pZYkb12t5DSwtts7.htm](classfeatures/swashbuckler-13-pZYkb12t5DSwtts7.htm)|Light Armor Expertise|Experiencia con armadura ligera|modificada|
|[swashbuckler-15-Pk3Ht0KZyFxSeL07.htm](classfeatures/swashbuckler-15-Pk3Ht0KZyFxSeL07.htm)|Keen Flair|Afilada Flair|modificada|
|[swashbuckler-15-Z7HX6TeFsaup7Dx9.htm](classfeatures/swashbuckler-15-Z7HX6TeFsaup7Dx9.htm)|Greater Weapon Specialization|Mayor especialización en armas|modificada|
|[swashbuckler-19-SHpjmM4A3Sw4GgDz.htm](classfeatures/swashbuckler-19-SHpjmM4A3Sw4GgDz.htm)|Light Armor Mastery|Maestría con armadura ligera|modificada|
|[swashbuckler-19-ypfT3iybew6ZSIUl.htm](classfeatures/swashbuckler-19-ypfT3iybew6ZSIUl.htm)|Eternal Confidence|Confianza Eterna|modificada|
|[thaumaturge-01-1vgFGSnn0DIBmK7j.htm](classfeatures/thaumaturge-01-1vgFGSnn0DIBmK7j.htm)|Chalice|Cáliz|modificada|
|[thaumaturge-01-AltwHU7hCqTwpn48.htm](classfeatures/thaumaturge-01-AltwHU7hCqTwpn48.htm)|Lantern|Linterna|modificada|
|[thaumaturge-01-cvQmPkJtybMcHinK.htm](classfeatures/thaumaturge-01-cvQmPkJtybMcHinK.htm)|Esoteric Lore|Esoteric Lore|modificada|
|[thaumaturge-01-DhdLzrcMvB93Rjmt.htm](classfeatures/thaumaturge-01-DhdLzrcMvB93Rjmt.htm)|Regalia|Regalia|modificada|
|[thaumaturge-01-DK1LCE5pd0YCY11c.htm](classfeatures/thaumaturge-01-DK1LCE5pd0YCY11c.htm)|Bell|Campana|modificada|
|[thaumaturge-01-MyN1cQgE0HsLF20e.htm](classfeatures/thaumaturge-01-MyN1cQgE0HsLF20e.htm)|Tome|Tomo|modificada|
|[thaumaturge-01-N6KvTbaRsphc0Ymb.htm](classfeatures/thaumaturge-01-N6KvTbaRsphc0Ymb.htm)|Mirror|Espejo|modificada|
|[thaumaturge-01-PbNS8d3w3pYQYcVN.htm](classfeatures/thaumaturge-01-PbNS8d3w3pYQYcVN.htm)|Implement's Empowerment|Implement's Empowerment|modificada|
|[thaumaturge-01-pDxdE8S8QJV2PGiB.htm](classfeatures/thaumaturge-01-pDxdE8S8QJV2PGiB.htm)|Wand|Varita|modificada|
|[thaumaturge-01-PoclGJ7BCEyIuqJe.htm](classfeatures/thaumaturge-01-PoclGJ7BCEyIuqJe.htm)|Amulet|Amuleto|modificada|
|[thaumaturge-01-uwLNfBprqZw2osTb.htm](classfeatures/thaumaturge-01-uwLNfBprqZw2osTb.htm)|Exploit Vulnerability|Explotar vulnerabilidad|modificada|
|[thaumaturge-01-VSQJtzQE6ikKdsnP.htm](classfeatures/thaumaturge-01-VSQJtzQE6ikKdsnP.htm)|First Implement and Esoterica|Primer Implemento y Esoterica|modificada|
|[thaumaturge-01-YiDkrwaxiF7Gao7y.htm](classfeatures/thaumaturge-01-YiDkrwaxiF7Gao7y.htm)|Weapon|Arma|modificada|
|[thaumaturge-05-ABYmUcLdxDFXEtzu.htm](classfeatures/thaumaturge-05-ABYmUcLdxDFXEtzu.htm)|Thaumaturge Weapon Expertise|Experiencia con las armas de taumaturgo|modificada|
|[thaumaturge-05-Z8WpDAdAXyefLB7Q.htm](classfeatures/thaumaturge-05-Z8WpDAdAXyefLB7Q.htm)|Second Implement|Segundo Implemento|modificada|
|[thaumaturge-07-Obm4ItMIIr0whYeO.htm](classfeatures/thaumaturge-07-Obm4ItMIIr0whYeO.htm)|Implement Adept|Implementar Adepto|modificada|
|[thaumaturge-09-VdwNvQwq9sHflEwe.htm](classfeatures/thaumaturge-09-VdwNvQwq9sHflEwe.htm)|Intensify Vulnerability|Intensificar Vulnerabilidad|modificada|
|[thaumaturge-09-yvdSUIRU5uLr5eF2.htm](classfeatures/thaumaturge-09-yvdSUIRU5uLr5eF2.htm)|Thaumaturgic Expertise|Thaumaturgic Expertise|modificada|
|[thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm](classfeatures/thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm)|Second Adept|Segundo Adepto|modificada|
|[thaumaturge-15-zxZzjN2T53wnH4vU.htm](classfeatures/thaumaturge-15-zxZzjN2T53wnH4vU.htm)|Third Implement|Tercer Implemento|modificada|
|[thaumaturge-17-QEtgbY8N2V4wTbsI.htm](classfeatures/thaumaturge-17-QEtgbY8N2V4wTbsI.htm)|Implement Paragon|Implementar Paragon|modificada|
|[thaumaturge-17-VywXtJCa0Y9fdGVH.htm](classfeatures/thaumaturge-17-VywXtJCa0Y9fdGVH.htm)|Thaumaturgic Mastery|Maestría Taumatúrgica|modificada|
|[thaumaturge-19-9ItMYxEkvxqBHrV1.htm](classfeatures/thaumaturge-19-9ItMYxEkvxqBHrV1.htm)|Unlimited Esoterica|Esoterismo Ilimitado|modificada|
|[witch-01-4IfYHrQMosJNM8hv.htm](classfeatures/witch-01-4IfYHrQMosJNM8hv.htm)|Fervor Patron|Fervor Patron|modificada|
|[witch-01-9uLh5z2uPo6LDFRY.htm](classfeatures/witch-01-9uLh5z2uPo6LDFRY.htm)|Hexes|Hexes|modificada|
|[witch-01-ejmSQOJR5lJv1pzh.htm](classfeatures/witch-01-ejmSQOJR5lJv1pzh.htm)|Rune Patron|Rune Patron|modificada|
|[witch-01-IuQ7wMznDwvBnmXX.htm](classfeatures/witch-01-IuQ7wMznDwvBnmXX.htm)|Pacts Patron|Pactos Patron|modificada|
|[witch-01-KPtF29AaeX2sJW0K.htm](classfeatures/witch-01-KPtF29AaeX2sJW0K.htm)|Patron|Patrón|modificada|
|[witch-01-NAXRmMjj0gcyD7ie.htm](classfeatures/witch-01-NAXRmMjj0gcyD7ie.htm)|Curse Patron|Curse Patron|modificada|
|[witch-01-nocYmxbi4rqCC2qS.htm](classfeatures/witch-01-nocYmxbi4rqCC2qS.htm)|Patron Theme|Tema Patrón|modificada|
|[witch-01-qf12ubZ07Q0z0NcN.htm](classfeatures/witch-01-qf12ubZ07Q0z0NcN.htm)|Winter Patron|Patrón de Invierno|modificada|
|[witch-01-qMZiTugiLCEmkg8h.htm](classfeatures/witch-01-qMZiTugiLCEmkg8h.htm)|Fate Patron|Fate Patron|modificada|
|[witch-01-SOan0fqyFTrkqJLV.htm](classfeatures/witch-01-SOan0fqyFTrkqJLV.htm)|Witch Lessons|Witch Lessons|modificada|
|[witch-01-VVMMJdIWL7fAsQf3.htm](classfeatures/witch-01-VVMMJdIWL7fAsQf3.htm)|Baba Yaga Patron|Baba Yaga Patrón|modificada|
|[witch-01-x2gzQMPvLwHWDdAC.htm](classfeatures/witch-01-x2gzQMPvLwHWDdAC.htm)|Wild Patron|Patrón salvaje|modificada|
|[witch-01-XFTWJO6txmLNRLae.htm](classfeatures/witch-01-XFTWJO6txmLNRLae.htm)|Night Patron|Night Patron|modificada|
|[witch-01-yksPhweBZYVCsE1A.htm](classfeatures/witch-01-yksPhweBZYVCsE1A.htm)|Familiar (Witch)|Familiar (Bruja)|modificada|
|[witch-01-zT6QiTMxxj8JYoN9.htm](classfeatures/witch-01-zT6QiTMxxj8JYoN9.htm)|Witch Spellcasting|Hechizo de bruja|modificada|
|[witch-01-zy0toWeGIeQstbT4.htm](classfeatures/witch-01-zy0toWeGIeQstbT4.htm)|Mosquito Witch Patron|Mosquito Bruja Patrona|modificada|
|[witch-02-DtmKrCvsmutVLAhH.htm](classfeatures/witch-02-DtmKrCvsmutVLAhH.htm)|Lesson of Vengeance|Lección de Venganza|modificada|
|[witch-02-evKUM58ymuuypRn9.htm](classfeatures/witch-02-evKUM58ymuuypRn9.htm)|Lesson of Dreams|Lección de Sue|modificada|
|[witch-02-FuOHRoEU8nHOXZnk.htm](classfeatures/witch-02-FuOHRoEU8nHOXZnk.htm)|Lesson of Elements|Lección de Elementos|modificada|
|[witch-02-HbREpzudMXPscgCj.htm](classfeatures/witch-02-HbREpzudMXPscgCj.htm)|Lesson of Life|Lección de vida|modificada|
|[witch-02-KHKe3PmctOFUeh85.htm](classfeatures/witch-02-KHKe3PmctOFUeh85.htm)|Lesson of Protection|Lección de protección|modificada|
|[witch-02-PLMmDXJCDdMS0V5C.htm](classfeatures/witch-02-PLMmDXJCDdMS0V5C.htm)|Lesson of Calamity|Lección de Calamidad|modificada|
|[witch-06-2IhZbkx889pATIjq.htm](classfeatures/witch-06-2IhZbkx889pATIjq.htm)|Lesson of Favors|Lección de Favores|modificada|
|[witch-06-rECRfbkVwHuG06vO.htm](classfeatures/witch-06-rECRfbkVwHuG06vO.htm)|Lesson of Snow|Lesson of Snow|modificada|
|[witch-06-XBgBh3xZhgGQP7lF.htm](classfeatures/witch-06-XBgBh3xZhgGQP7lF.htm)|Lesson of Mischief|Lección de picardía|modificada|
|[witch-06-YSxymsAe2Dvq47f2.htm](classfeatures/witch-06-YSxymsAe2Dvq47f2.htm)|Lesson of Shadow|Lección de Sombra|modificada|
|[witch-10-29ynmOCdtIFDzrWx.htm](classfeatures/witch-10-29ynmOCdtIFDzrWx.htm)|Lesson of the Frozen Queen|Lección de la Reina Helada|modificada|
|[witch-10-7Y2XDqr4gRisjiAG.htm](classfeatures/witch-10-7Y2XDqr4gRisjiAG.htm)|Lesson of Renewal|Lección de renovación|modificada|
|[witch-10-RcmqV0cuOLcnKQr0.htm](classfeatures/witch-10-RcmqV0cuOLcnKQr0.htm)|Lesson of Bargains|Lección de gangas|modificada|
|[witch-10-wRHeY7tCN6HFFF3a.htm](classfeatures/witch-10-wRHeY7tCN6HFFF3a.htm)|Lesson of Death|Lección de muerte|modificada|
|[witch-19-cDnFXfl3i5Z2l7JP.htm](classfeatures/witch-19-cDnFXfl3i5Z2l7JP.htm)|Patron's Gift|Patron's Gift|modificada|
|[wizard-01-7nbKDBGvwSx9T27G.htm](classfeatures/wizard-01-7nbKDBGvwSx9T27G.htm)|Arcane School|Escuela arcana|modificada|
|[wizard-01-89zWKD2CN7nRu2xp.htm](classfeatures/wizard-01-89zWKD2CN7nRu2xp.htm)|Metamagical Experimentation|Experimentación metamágica|modificada|
|[wizard-01-au0lwQ1nAcNQwcGh.htm](classfeatures/wizard-01-au0lwQ1nAcNQwcGh.htm)|Arcane Bond|Vínculo arcano|modificada|
|[wizard-01-gCwcys8CnS102tji.htm](classfeatures/wizard-01-gCwcys8CnS102tji.htm)|Abjuration|Abjuración|modificada|
|[wizard-01-ibhml5y20g5M3Vgd.htm](classfeatures/wizard-01-ibhml5y20g5M3Vgd.htm)|Evocation|Evocación|modificada|
|[wizard-01-K6hG7nH8yjmbA0Q9.htm](classfeatures/wizard-01-K6hG7nH8yjmbA0Q9.htm)|Illusion|Ilusión|modificada|
|[wizard-01-M89l9FOnjHe63wD7.htm](classfeatures/wizard-01-M89l9FOnjHe63wD7.htm)|Arcane Thesis|Tesis arcana|modificada|
|[wizard-01-OAcxS625AXSGrQIC.htm](classfeatures/wizard-01-OAcxS625AXSGrQIC.htm)|Spell Blending|Mezcla de conjuros|modificada|
|[wizard-01-qczCKdg47eAmCOUD.htm](classfeatures/wizard-01-qczCKdg47eAmCOUD.htm)|Universalist|Universalista|modificada|
|[wizard-01-QzWXMCSGNfvvpYgF.htm](classfeatures/wizard-01-QzWXMCSGNfvvpYgF.htm)|Spell Substitution|Sustitución de conjuros|modificada|
|[wizard-01-rHxkPijLnQ9O9AGV.htm](classfeatures/wizard-01-rHxkPijLnQ9O9AGV.htm)|Transmutation|Transmutación|modificada|
|[wizard-01-S6WW4Yyg4XonXGHD.htm](classfeatures/wizard-01-S6WW4Yyg4XonXGHD.htm)|Arcane Spellcasting (Wizard)|Lanzamiento de conjuros arcanos (Mago)|modificada|
|[wizard-01-SNeVaUBTHwvoO6kr.htm](classfeatures/wizard-01-SNeVaUBTHwvoO6kr.htm)|Conjuration|Conjuración|modificada|
|[wizard-01-SNZ46g3u7U6x0XJj.htm](classfeatures/wizard-01-SNZ46g3u7U6x0XJj.htm)|Improved Familiar Attunement|Sintonización mejorada de Familiar|modificada|
|[wizard-01-uNM7qZQokRKAEd7k.htm](classfeatures/wizard-01-uNM7qZQokRKAEd7k.htm)|Necromancy|Nigromancia|modificada|
|[wizard-01-yobGgrHdgjs5QW5o.htm](classfeatures/wizard-01-yobGgrHdgjs5QW5o.htm)|Divination|Adivinación|modificada|
|[wizard-01-ZHwGACWQEy6kTzcP.htm](classfeatures/wizard-01-ZHwGACWQEy6kTzcP.htm)|Enchantment|Encantamiento|modificada|
|[wizard-11-GBsC2cARoFiqMi9V.htm](classfeatures/wizard-11-GBsC2cARoFiqMi9V.htm)|Wizard Weapon Expertise|Experiencia con armas de mago|modificada|
|[wizard-13-gU7epgcPSm0TD1UK.htm](classfeatures/wizard-13-gU7epgcPSm0TD1UK.htm)|Defensive Robes|Ropajes defensivos|modificada|
|[wizard-19-Hfaa7TuLn3nE8lr3.htm](classfeatures/wizard-19-Hfaa7TuLn3nE8lr3.htm)|Legendary Spellcaster|Lanzador de conjuros legendario|modificada|
|[wizard-19-ZjwJHmjPrSs6VDez.htm](classfeatures/wizard-19-ZjwJHmjPrSs6VDez.htm)|Archwizard's Spellcraft|Conocimiento de conjuros de archimago|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[alchemist-07-LlZ5R50z9j8jysZL.htm](classfeatures/alchemist-07-LlZ5R50z9j8jysZL.htm)|Perpetual Infusions (Toxicologist)|Infusiones perpetuas (Toxicólogo)|oficial|
|[alchemist-11-JOdbVu14phvdjhaY.htm](classfeatures/alchemist-11-JOdbVu14phvdjhaY.htm)|Perpetual Potency (Toxicologist)|Potencia perpetua (Toxicólogo)|oficial|
|[alchemist-17-3R19zS7gERhEX87F.htm](classfeatures/alchemist-17-3R19zS7gERhEX87F.htm)|Perpetual Perfection (Toxicologist)|Perfección perpetua (Toxicólogo)|oficial|
|[barbarian-01-SCYSjUbMmw8JD9P9.htm](classfeatures/barbarian-01-SCYSjUbMmw8JD9P9.htm)|Superstition Instinct|Instinto de superstición|oficial|
|[bard-01-N03BtRvjX9TeHRa4.htm](classfeatures/bard-01-N03BtRvjX9TeHRa4.htm)|Warrior|Combatiente|oficial|
|[champion-01-8YIA0jh64Ecz0TG6.htm](classfeatures/champion-01-8YIA0jh64Ecz0TG6.htm)|Desecrator|Profanador|oficial|
|[champion-01-EQ6DVIQHAUXUhY6Y.htm](classfeatures/champion-01-EQ6DVIQHAUXUhY6Y.htm)|Antipaladin|Antipaladín|oficial|
|[champion-01-HiIvez0TqESbleB5.htm](classfeatures/champion-01-HiIvez0TqESbleB5.htm)|Tyrant|Tirano|oficial|
|[investigator-03-DZWQspPi4IkfXV2E.htm](classfeatures/investigator-03-DZWQspPi4IkfXV2E.htm)|Keen Recollection|Memoria aguda|oficial|
|[rogue-01-D8qtAo2w4jsqjBrM.htm](classfeatures/rogue-01-D8qtAo2w4jsqjBrM.htm)|Eldritch Trickster|Embaucador sobrenatural|oficial|
|[rogue-01-RyOkmu0W9svavuAB.htm](classfeatures/rogue-01-RyOkmu0W9svavuAB.htm)|Mastermind|Mente maestra|oficial|
|[sorcerer-01-5Wxjghw7lHuCxjZz.htm](classfeatures/sorcerer-01-5Wxjghw7lHuCxjZz.htm)|Bloodline: Nymph|Linaje: Ninfa|oficial|
|[sorcerer-01-PpzH9tJULk5ksX9w.htm](classfeatures/sorcerer-01-PpzH9tJULk5ksX9w.htm)|Bloodline: Psychopomp|Linaje: Psicopompo|oficial|
|[sorcerer-01-tYOMBiH3HbViNWwn.htm](classfeatures/sorcerer-01-tYOMBiH3HbViNWwn.htm)|Bloodline: Genie|Linaje: Genio|oficial|
|[sorcerer-01-uoQOm41BVdSo6pAS.htm](classfeatures/sorcerer-01-uoQOm41BVdSo6pAS.htm)|Bloodline: Shadow|Linaje: Sombrío|oficial|
|[wizard-01-Klb35AwlkNrq1gpB.htm](classfeatures/wizard-01-Klb35AwlkNrq1gpB.htm)|Staff Nexus|Nexo de bastón|oficial|
