# Estado de la traducción (impossible-lands-bestiary-items)

 * **ninguna**: 4
 * **vacía**: 2


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[065iVlSPxA9kgUl3.htm](impossible-lands-bestiary-items/065iVlSPxA9kgUl3.htm)|Leather Armor|
|[1OjmZA7eMiNdkwiy.htm](impossible-lands-bestiary-items/1OjmZA7eMiNdkwiy.htm)|Rapier|
|[2vjvjlmw4lJ6Kx0X.htm](impossible-lands-bestiary-items/2vjvjlmw4lJ6Kx0X.htm)|Outsmart|
|[2Zb90teqdj9IhFCP.htm](impossible-lands-bestiary-items/2Zb90teqdj9IhFCP.htm)|Low-Light Vision|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[BfoANLA9WJBe5KCQ.htm](impossible-lands-bestiary-items/BfoANLA9WJBe5KCQ.htm)|Rapier|vacía|
|[DtYma4Xp1er3Tul4.htm](impossible-lands-bestiary-items/DtYma4Xp1er3Tul4.htm)|Shortbow|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
