# Estado de la traducción (spells)

 * **modificada**: 1341
 * **ninguna**: 22
 * **libre**: 2
 * **oficial**: 5


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[conjuration-01-3SJEBgXHolrSAFqD.htm](spells/conjuration-01-3SJEBgXHolrSAFqD.htm)|Helpful Steps|
|[conjuration-02-CI7F5qdwp6YngyFt.htm](spells/conjuration-02-CI7F5qdwp6YngyFt.htm)|Confetti Cloud|
|[conjuration-03-0aoRcxRyIPHfbifk.htm](spells/conjuration-03-0aoRcxRyIPHfbifk.htm)|Nothing Up My Sleeve|
|[conjuration-04-vGMbpV7GWIFPNUaZ.htm](spells/conjuration-04-vGMbpV7GWIFPNUaZ.htm)|Bursting Bloom|
|[conjuration-06-gBJtUhewnihlqxm7.htm](spells/conjuration-06-gBJtUhewnihlqxm7.htm)|Rose's Thorns|
|[divination-01-ofAQyLtLEGnOIs3N.htm](spells/divination-01-ofAQyLtLEGnOIs3N.htm)|Harrowing|
|[divination-01-PaHxcqXihXkkXPsB.htm](spells/divination-01-PaHxcqXihXkkXPsB.htm)|Unraveling Blast|
|[divination-02-LFcGNGLMkGlsnEKQ.htm](spells/divination-02-LFcGNGLMkGlsnEKQ.htm)|Spy's Mark|
|[divination-03-82gDiXTKsCmdIF6Q.htm](spells/divination-03-82gDiXTKsCmdIF6Q.htm)|Invoke the Harrow|
|[divination-05-BAu5AgqoO556clyK.htm](spells/divination-05-BAu5AgqoO556clyK.htm)|Rewrite Possibility|
|[enchantment-02-1hLxZznnA2kvXlIt.htm](spells/enchantment-02-1hLxZznnA2kvXlIt.htm)|Cutting Insult|
|[enchantment-05-qxJE2iSJwPFLZrcK.htm](spells/enchantment-05-qxJE2iSJwPFLZrcK.htm)|Belittling Boast|
|[evocation-03-iG1USiSRXMjCDGAr.htm](spells/evocation-03-iG1USiSRXMjCDGAr.htm)|Percussive Impact|
|[evocation-05-7uL4XhHpxZpgNJPh.htm](spells/evocation-05-7uL4XhHpxZpgNJPh.htm)|Blinding Foam|
|[illusion-01-mnhuvqTIELcpJOFX.htm](spells/illusion-01-mnhuvqTIELcpJOFX.htm)|Musical Accompaniment|
|[illusion-01-ro0omoBKiJiMuDRa.htm](spells/illusion-01-ro0omoBKiJiMuDRa.htm)|Overselling Flourish|
|[illusion-01-YgbYvkLvnWJ4WfEA.htm](spells/illusion-01-YgbYvkLvnWJ4WfEA.htm)|Flashy Disappearance|
|[illusion-02-kLyyJayja75K9rXX.htm](spells/illusion-02-kLyyJayja75K9rXX.htm)|Illusory Shroud|
|[illusion-02-xyRjD52YZl3DBsiy.htm](spells/illusion-02-xyRjD52YZl3DBsiy.htm)|Instant Parade|
|[illusion-05-Hp2wmnRS6TUGZlZ2.htm](spells/illusion-05-Hp2wmnRS6TUGZlZ2.htm)|Rallying Banner|
|[transmutation-01-W0YqtSVwfFImGgdK.htm](spells/transmutation-01-W0YqtSVwfFImGgdK.htm)|Fashionista|
|[transmutation-03-KAWQ5x2j4tUJ91ry.htm](spells/transmutation-03-KAWQ5x2j4tUJ91ry.htm)|Sparkleskin|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[abjuration-01-0cF9HvHzzWSbCFBP.htm](spells/abjuration-01-0cF9HvHzzWSbCFBP.htm)|Empty Inside|Empty Inside|modificada|
|[abjuration-01-0fKHBh5goe2eiFYL.htm](spells/abjuration-01-0fKHBh5goe2eiFYL.htm)|Negate Aroma|Negar aroma|modificada|
|[abjuration-01-0uRpypf1Hi7ahvTl.htm](spells/abjuration-01-0uRpypf1Hi7ahvTl.htm)|Shattering Gem|Gema que estalla|modificada|
|[abjuration-01-3wmX7htzOXiHLdAn.htm](spells/abjuration-01-3wmX7htzOXiHLdAn.htm)|Delay Consequence|Retraso Consecuencia|modificada|
|[abjuration-01-4WAib3GichxLjp5p.htm](spells/abjuration-01-4WAib3GichxLjp5p.htm)|Alarm|Alarma|modificada|
|[abjuration-01-53NqGTJOf4LcjVyD.htm](spells/abjuration-01-53NqGTJOf4LcjVyD.htm)|Shielding Strike|Golpe de escudo|modificada|
|[abjuration-01-8xRzLhwGL7Dgy3EZ.htm](spells/abjuration-01-8xRzLhwGL7Dgy3EZ.htm)|Sanctuary|Santuario|modificada|
|[abjuration-01-aAbfKn8maGjJjk2W.htm](spells/abjuration-01-aAbfKn8maGjJjk2W.htm)|Mage Armor|Armadura de mago|modificada|
|[abjuration-01-AfOpnnwdZwHi2Tnc.htm](spells/abjuration-01-AfOpnnwdZwHi2Tnc.htm)|Protect Companion|Protección Acompaóante|modificada|
|[abjuration-01-Azoh0BSoCASrA1lr.htm](spells/abjuration-01-Azoh0BSoCASrA1lr.htm)|Lock|Cerrar|modificada|
|[abjuration-01-cDFAQN7Z3es07WSA.htm](spells/abjuration-01-cDFAQN7Z3es07WSA.htm)|Perfected Mind|Mente perfeccionada|modificada|
|[abjuration-01-cJq5NarY0eOZN74A.htm](spells/abjuration-01-cJq5NarY0eOZN74A.htm)|Share Burden|Compartir Carga|modificada|
|[abjuration-01-dFejDNEmVj3CwYLL.htm](spells/abjuration-01-dFejDNEmVj3CwYLL.htm)|Blood Ward|Blood Ward|modificada|
|[abjuration-01-dhpXpzt7TCm8TbHM.htm](spells/abjuration-01-dhpXpzt7TCm8TbHM.htm)|Thermal Stasis|Thermal Stasis|modificada|
|[abjuration-01-gMODOGamz88rgHuf.htm](spells/abjuration-01-gMODOGamz88rgHuf.htm)|Protection|Protección|modificada|
|[abjuration-01-GzlVI3qYdCtUigLz.htm](spells/abjuration-01-GzlVI3qYdCtUigLz.htm)|Fey Abeyance|Feérico Abeyance|modificada|
|[abjuration-01-hW9pgce6vTme61g1.htm](spells/abjuration-01-hW9pgce6vTme61g1.htm)|Unfetter Eidolon|Unfetter Eidolon|modificada|
|[abjuration-01-k43PIYwuQqjeJ3S3.htm](spells/abjuration-01-k43PIYwuQqjeJ3S3.htm)|Forced Quiet|Silencio impuesta|modificada|
|[abjuration-01-lmHmKGs1N3yNAvln.htm](spells/abjuration-01-lmHmKGs1N3yNAvln.htm)|Wash Your Luck|Wash Your Luck|modificada|
|[abjuration-01-lY9fOk1qBDDhBT8s.htm](spells/abjuration-01-lY9fOk1qBDDhBT8s.htm)|Protective Ward|Custodia protectora|modificada|
|[abjuration-01-MVrxZarUTnJxAUN8.htm](spells/abjuration-01-MVrxZarUTnJxAUN8.htm)|Tether|Amarrado|modificada|
|[abjuration-01-OyiKIbWllLZC6sGz.htm](spells/abjuration-01-OyiKIbWllLZC6sGz.htm)|Genie's Veil|Velo del Genio|modificada|
|[abjuration-01-P9bqJsF3WkxGAJKJ.htm](spells/abjuration-01-P9bqJsF3WkxGAJKJ.htm)|Sudden Shift|Cambio Repentino|modificada|
|[abjuration-01-PGVhjDbzC4lf6aXF.htm](spells/abjuration-01-PGVhjDbzC4lf6aXF.htm)|Breadcrumbs|Migas de pan|modificada|
|[abjuration-01-qjUcAMgcSLIamjEq.htm](spells/abjuration-01-qjUcAMgcSLIamjEq.htm)|String of Fate|String of Fate|modificada|
|[abjuration-01-R569jdqpNry8m0TJ.htm](spells/abjuration-01-R569jdqpNry8m0TJ.htm)|Guided Introspection|Introspección guiada|modificada|
|[abjuration-01-RA7VKcen3p56rVyZ.htm](spells/abjuration-01-RA7VKcen3p56rVyZ.htm)|Forbidding Ward|Custodia amenazadora|modificada|
|[abjuration-01-rMOI8JFJ0nT2mrCF.htm](spells/abjuration-01-rMOI8JFJ0nT2mrCF.htm)|Phase Familiar|Fase Familiar|modificada|
|[abjuration-01-rQYob0QMJ0I1U2sU.htm](spells/abjuration-01-rQYob0QMJ0I1U2sU.htm)|Protector's Sacrifice|Sacrificio delprotector|modificada|
|[abjuration-01-TTwOKGqmZeKSyNMH.htm](spells/abjuration-01-TTwOKGqmZeKSyNMH.htm)|Feather Fall|Caída de pluma|modificada|
|[abjuration-01-TVKNbcgTee19PXZR.htm](spells/abjuration-01-TVKNbcgTee19PXZR.htm)|Shield|Escudo|modificada|
|[abjuration-01-TYbCj4dgXDOZou9k.htm](spells/abjuration-01-TYbCj4dgXDOZou9k.htm)|Reinforce Eidolon|Reforzar Eidolon|modificada|
|[abjuration-01-u2uSeH6YSbK1ajTy.htm](spells/abjuration-01-u2uSeH6YSbK1ajTy.htm)|Magic Hide|Esconderse mágicamente|modificada|
|[abjuration-01-w3uGXDVEdbLFZVO0.htm](spells/abjuration-01-w3uGXDVEdbLFZVO0.htm)|Angelic Halo|Halo angelical|modificada|
|[abjuration-01-xM35hJacTM1BSXUl.htm](spells/abjuration-01-xM35hJacTM1BSXUl.htm)|Silver's Refrain|Silver's Refrain|modificada|
|[abjuration-01-zlnXpME1T2uvn8Lr.htm](spells/abjuration-01-zlnXpME1T2uvn8Lr.htm)|Pass Without Trace|Pasar sin dejar rastro|modificada|
|[abjuration-02-02J0rDTk37KN2sjt.htm](spells/abjuration-02-02J0rDTk37KN2sjt.htm)|Quench|Apagar|modificada|
|[abjuration-02-24U1b9K4Lj94cgaj.htm](spells/abjuration-02-24U1b9K4Lj94cgaj.htm)|Magnetic Repulsion|Repulsión magnética|modificada|
|[abjuration-02-5esP2GVzvxWsMgaX.htm](spells/abjuration-02-5esP2GVzvxWsMgaX.htm)|Endure Elements|Endure Elements|modificada|
|[abjuration-02-66xBcxqzYcpbItBU.htm](spells/abjuration-02-66xBcxqzYcpbItBU.htm)|Purify Soul Path|Purify Soul Path|modificada|
|[abjuration-02-9HpwDN4MYQJnW0LG.htm](spells/abjuration-02-9HpwDN4MYQJnW0LG.htm)|Dispel Magic|Disipar magia|modificada|
|[abjuration-02-CLThxp8Qf43IQ3Sb.htm](spells/abjuration-02-CLThxp8Qf43IQ3Sb.htm)|Extract Poison|Extraer veneno|modificada|
|[abjuration-02-dZV8nZUKRhGIr6g9.htm](spells/abjuration-02-dZV8nZUKRhGIr6g9.htm)|Heartbond|Vínculo del corazón|modificada|
|[abjuration-02-Fr58LDSrbndgld9n.htm](spells/abjuration-02-Fr58LDSrbndgld9n.htm)|Resist Energy|Resistir energía|modificada|
|[abjuration-02-fWU7Qjp1JiX9g6eg.htm](spells/abjuration-02-fWU7Qjp1JiX9g6eg.htm)|Animus Mine|Mina Animus|modificada|
|[abjuration-02-FzAtX8yXBjTaisJK.htm](spells/abjuration-02-FzAtX8yXBjTaisJK.htm)|Undetectable Alignment|Alineamiento indetectable|modificada|
|[abjuration-02-qFO9HgplrShoaAPY.htm](spells/abjuration-02-qFO9HgplrShoaAPY.htm)|Lock Item|Cerrar elemento|modificada|
|[abjuration-02-Tw9e2rPaNdxcM1Rp.htm](spells/abjuration-02-Tw9e2rPaNdxcM1Rp.htm)|Erase Trail|Borrar rastro|modificada|
|[abjuration-02-Y8cSjhU33oUqccxJ.htm](spells/abjuration-02-Y8cSjhU33oUqccxJ.htm)|Brand the Impenitent|Marca del impenitente|modificada|
|[abjuration-02-YWrfKetOqDwVFut7.htm](spells/abjuration-02-YWrfKetOqDwVFut7.htm)|Barkskin|Piel robliza|modificada|
|[abjuration-03-3ipOKanMLnJrPwbr.htm](spells/abjuration-03-3ipOKanMLnJrPwbr.htm)|Guardian's Aegis|Guardian's Aegis|modificada|
|[abjuration-03-9TauMFkIsmvKJNzZ.htm](spells/abjuration-03-9TauMFkIsmvKJNzZ.htm)|Elemental Absorption|Absorción elemental|modificada|
|[abjuration-03-a0AMgATvQGwDR5dR.htm](spells/abjuration-03-a0AMgATvQGwDR5dR.htm)|Vector Screen|Vector Screen|modificada|
|[abjuration-03-CIjj9CU5ekeq1oLT.htm](spells/abjuration-03-CIjj9CU5ekeq1oLT.htm)|Warding Aggression|Warding Aggression|modificada|
|[abjuration-03-EoKBlgf6Smt8opaU.htm](spells/abjuration-03-EoKBlgf6Smt8opaU.htm)|Nondetection|Indetectabilidad|modificada|
|[abjuration-03-FPnkOYyWIaOzkmqn.htm](spells/abjuration-03-FPnkOYyWIaOzkmqn.htm)|Untwisting Iron Buffer|Untwisting Iron Buffer|modificada|
|[abjuration-03-IFuEzfmmWyNwVbhY.htm](spells/abjuration-03-IFuEzfmmWyNwVbhY.htm)|Safe Passage|Paso seguro|modificada|
|[abjuration-03-J5KrjQKCg2PrF1vz.htm](spells/abjuration-03-J5KrjQKCg2PrF1vz.htm)|Ancestral Defense|Defensa Ancestral|modificada|
|[abjuration-03-k9x6bXXpIgAXMDsx.htm](spells/abjuration-03-k9x6bXXpIgAXMDsx.htm)|Whirling Scarves|Bufandas Remolino|modificada|
|[abjuration-03-mpGCMTldMVa0pWYs.htm](spells/abjuration-03-mpGCMTldMVa0pWYs.htm)|Circle of Protection|Círculo de protección.|modificada|
|[abjuration-03-mriIWoSDtJTJIBjX.htm](spells/abjuration-03-mriIWoSDtJTJIBjX.htm)|Caster's Imposition|Caster's Imposition|modificada|
|[abjuration-03-o0l57UfBm9ScEUMW.htm](spells/abjuration-03-o0l57UfBm9ScEUMW.htm)|Glyph of Warding|Glifo custodio|modificada|
|[abjuration-03-rtisvvvhkpZPdgXc.htm](spells/abjuration-03-rtisvvvhkpZPdgXc.htm)|Cascade Countermeasure|Contramedidas en cascada|modificada|
|[abjuration-03-VBNevVovTCpn04vL.htm](spells/abjuration-03-VBNevVovTCpn04vL.htm)|Unblinking Flame Revelation|Revelación Flamígera sin parpadear.|modificada|
|[abjuration-03-y3jlOfgsQH1JjkJE.htm](spells/abjuration-03-y3jlOfgsQH1JjkJE.htm)|Sanctified Ground|Terreno santificado|modificada|
|[abjuration-04-2BV2yYPfVJ5zirZt.htm](spells/abjuration-04-2BV2yYPfVJ5zirZt.htm)|Stoneskin|Piel pétrea|modificada|
|[abjuration-04-5p8naLYjFcG13OkU.htm](spells/abjuration-04-5p8naLYjFcG13OkU.htm)|Rebounding Barrier|Barrera de rebote|modificada|
|[abjuration-04-7Fd4lxozd11MQ55N.htm](spells/abjuration-04-7Fd4lxozd11MQ55N.htm)|Atone|Expiar|modificada|
|[abjuration-04-8ifpNZkaxrbs3dBJ.htm](spells/abjuration-04-8ifpNZkaxrbs3dBJ.htm)|Perfected Form|Forma perfeccionada|modificada|
|[abjuration-04-aqRYNoSvxsVfqglH.htm](spells/abjuration-04-aqRYNoSvxsVfqglH.htm)|Freedom of Movement|Libertad de movimientos|modificada|
|[abjuration-04-caehfpQz7yp9yNzz.htm](spells/abjuration-04-caehfpQz7yp9yNzz.htm)|Dutiful Challenge|Dutiful Challenge|modificada|
|[abjuration-04-DH9Y3RQGWO0GzXGU.htm](spells/abjuration-04-DH9Y3RQGWO0GzXGU.htm)|Protector's Sphere|Esfera del protector|modificada|
|[abjuration-04-eG1fBodYwolaXK98.htm](spells/abjuration-04-eG1fBodYwolaXK98.htm)|Enduring Might|Poder duradero|modificada|
|[abjuration-04-ER1LOEgCLtmEKd05.htm](spells/abjuration-04-ER1LOEgCLtmEKd05.htm)|Ravenous Portal|Portal Voraz|modificada|
|[abjuration-04-GjmMFEJcDd6MDG2P.htm](spells/abjuration-04-GjmMFEJcDd6MDG2P.htm)|Spiritual Attunement|Spiritual Attunement|modificada|
|[abjuration-04-GoKkejPj5yWJPIPK.htm](spells/abjuration-04-GoKkejPj5yWJPIPK.htm)|Adaptive Ablation|Ablación Adaptativa|modificada|
|[abjuration-04-JOdOpbPDl7nqvJUm.htm](spells/abjuration-04-JOdOpbPDl7nqvJUm.htm)|Globe of Invulnerability|Globo de invulnerabilidad|modificada|
|[abjuration-04-ksLCg62cLOojw3gN.htm](spells/abjuration-04-ksLCg62cLOojw3gN.htm)|Dimensional Anchor|Ancla dimensional|modificada|
|[abjuration-04-LoBjvguamA12iyW0.htm](spells/abjuration-04-LoBjvguamA12iyW0.htm)|Energy Absorption|Absorción de energía|modificada|
|[abjuration-04-Mv5L4201uk8hnAtD.htm](spells/abjuration-04-Mv5L4201uk8hnAtD.htm)|Reflective Scales|Escalas de reflexión|modificada|
|[abjuration-04-NBSBFHxBm88qxQUy.htm](spells/abjuration-04-NBSBFHxBm88qxQUy.htm)|Chromatic Armor|Armadura cromática|modificada|
|[abjuration-04-onjZCEHs3JJJRTD0.htm](spells/abjuration-04-onjZCEHs3JJJRTD0.htm)|Private Sanctum|Sanctasanctórum privadoórum privado|modificada|
|[abjuration-04-OsrtOeG0TvDNnEFH.htm](spells/abjuration-04-OsrtOeG0TvDNnEFH.htm)|Safeguard Secret|Salvaguardar secreto|modificada|
|[abjuration-04-qJQADktwD0x8kLAy.htm](spells/abjuration-04-qJQADktwD0x8kLAy.htm)|Resilient Sphere|Esfera Resistente|modificada|
|[abjuration-04-SSsUC7rZo0CwayPn.htm](spells/abjuration-04-SSsUC7rZo0CwayPn.htm)|Retributive Pain|Dolor retributivo|modificada|
|[abjuration-04-tgIhRUFtgDSELpJE.htm](spells/abjuration-04-tgIhRUFtgDSELpJE.htm)|Spell Immunity|Inmunidad a los conjuros|modificada|
|[abjuration-04-TLmkJ8ga8s057HBp.htm](spells/abjuration-04-TLmkJ8ga8s057HBp.htm)|Soft Landing|Aterrizaje suave|modificada|
|[abjuration-04-tYLBjOTvBVn9JtRb.htm](spells/abjuration-04-tYLBjOTvBVn9JtRb.htm)|Unity|Unidad|modificada|
|[abjuration-04-y7Tusv3CieZktkkV.htm](spells/abjuration-04-y7Tusv3CieZktkkV.htm)|Flame Barrier|Barrera de llamas|modificada|
|[abjuration-04-yY1H5zhO5dHmD8lz.htm](spells/abjuration-04-yY1H5zhO5dHmD8lz.htm)|Font of Serenity|Fuente de la Serenidad|modificada|
|[abjuration-05-1b55SgYTV65JvmQd.htm](spells/abjuration-05-1b55SgYTV65JvmQd.htm)|Blessing of Defiance|Bendición de Desafío|modificada|
|[abjuration-05-8rAcQOvm0nFrqAUt.htm](spells/abjuration-05-8rAcQOvm0nFrqAUt.htm)|Lashunta's Life Bubble|Burbuja de vida de Lashunta|modificada|
|[abjuration-05-aVqsE5OedOwdHQvF.htm](spells/abjuration-05-aVqsE5OedOwdHQvF.htm)|Summerland Spell|Hechizo Summerland|modificada|
|[abjuration-05-bay4AfSu2iIozNNW.htm](spells/abjuration-05-bay4AfSu2iIozNNW.htm)|Banishment|Destierro|modificada|
|[abjuration-05-forsqeofEszBNtLq.htm](spells/abjuration-05-forsqeofEszBNtLq.htm)|Chromatic Wall|Muro cromático|modificada|
|[abjuration-05-h47yv6j6x1pUtzlr.htm](spells/abjuration-05-h47yv6j6x1pUtzlr.htm)|Arcane Countermeasure|Contramedidas arcana|modificada|
|[abjuration-05-h7h0wMxu4WOpveQ3.htm](spells/abjuration-05-h7h0wMxu4WOpveQ3.htm)|Ritual Obstruction|Obstrucción ritual|modificada|
|[abjuration-05-jQdm301h6e8hIY4U.htm](spells/abjuration-05-jQdm301h6e8hIY4U.htm)|Spiritual Guardian|Guardián espiritual|modificada|
|[abjuration-05-N9KjnGyVMKgqKcCw.htm](spells/abjuration-05-N9KjnGyVMKgqKcCw.htm)|Bandit's Doom|La perdición del bandido|modificada|
|[abjuration-05-VrhCpCIQB8PFax77.htm](spells/abjuration-05-VrhCpCIQB8PFax77.htm)|Spellmaster's Ward|Spellmaster's Ward|modificada|
|[abjuration-05-wrm1zIiLyFD1Is6h.htm](spells/abjuration-05-wrm1zIiLyFD1Is6h.htm)|Aspirational State|Estado Aspiracional|modificada|
|[abjuration-05-XlQBVvlDWGrGlApl.htm](spells/abjuration-05-XlQBVvlDWGrGlApl.htm)|Establish Ward|Establecer Ward|modificada|
|[abjuration-05-YvXKGlHOt7mdW2jZ.htm](spells/abjuration-05-YvXKGlHOt7mdW2jZ.htm)|Death Ward|Custodia contra la muerte|modificada|
|[abjuration-05-YWuyjhWbdoTiA1pw.htm](spells/abjuration-05-YWuyjhWbdoTiA1pw.htm)|Temporary Glyph|Glifo temporal|modificada|
|[abjuration-05-ZAX0OOcKtYMQlquR.htm](spells/abjuration-05-ZAX0OOcKtYMQlquR.htm)|Symphony of the Unfettered Heart|Symphony of the Unfettered Heart|modificada|
|[abjuration-05-ZQk2cGBCkATO25w0.htm](spells/abjuration-05-ZQk2cGBCkATO25w0.htm)|Unbreaking Wave Vapor|Unbreaking Wave Vapor|modificada|
|[abjuration-06-2ykmAVKrsAWcazcC.htm](spells/abjuration-06-2ykmAVKrsAWcazcC.htm)|Planar Binding|Ligadura de los Planos|modificada|
|[abjuration-06-8Umt1AzYfFbC4fui.htm](spells/abjuration-06-8Umt1AzYfFbC4fui.htm)|Spellwrack|Arruinaconjuros|modificada|
|[abjuration-06-9W2Qv0wXLg6tQg3y.htm](spells/abjuration-06-9W2Qv0wXLg6tQg3y.htm)|Scintillating Safeguard|Salvaguarda centelleante|modificada|
|[abjuration-06-Er9XNUlL0wB0PM36.htm](spells/abjuration-06-Er9XNUlL0wB0PM36.htm)|Ward Domain|Custodiar dominio|modificada|
|[abjuration-06-kLxJx7BECVgHh2vb.htm](spells/abjuration-06-kLxJx7BECVgHh2vb.htm)|Temporal Ward|Temporal Ward|modificada|
|[abjuration-06-pZc8ZwtsyWnxUUW0.htm](spells/abjuration-06-pZc8ZwtsyWnxUUW0.htm)|Primal Call|Llamada, Primal|modificada|
|[abjuration-06-USM530HlzZ1RMd99.htm](spells/abjuration-06-USM530HlzZ1RMd99.htm)|Champion's Sacrifice|Sacrificio del campeón|modificada|
|[abjuration-06-yrZA4k2VAqEP8xx7.htm](spells/abjuration-06-yrZA4k2VAqEP8xx7.htm)|Repulsion|Repulsión|modificada|
|[abjuration-07-4nEY2BcV85wavKLO.htm](spells/abjuration-07-4nEY2BcV85wavKLO.htm)|Prismatic Armor|Armadura Prismática|modificada|
|[abjuration-07-bVtkBJvGLP69qVGI.htm](spells/abjuration-07-bVtkBJvGLP69qVGI.htm)|Unfettered Pack|Jauría indómita sin trabas|modificada|
|[abjuration-07-m2xFMNyQiUKQDRaj.htm](spells/abjuration-07-m2xFMNyQiUKQDRaj.htm)|Energy Aegis|égida de energía|modificada|
|[abjuration-07-OOELvfkTedBMlWtq.htm](spells/abjuration-07-OOELvfkTedBMlWtq.htm)|Spell Turning|Retorno de conjuros|modificada|
|[abjuration-07-v8VnSMzaSYwkq6c7.htm](spells/abjuration-07-v8VnSMzaSYwkq6c7.htm)|Return To Essence|Retornar a la Esencia|modificada|
|[abjuration-07-WG91Z5TiR6oO5FOw.htm](spells/abjuration-07-WG91Z5TiR6oO5FOw.htm)|Contingency|Contingencia|modificada|
|[abjuration-07-XZE4BawIlTf88Yl9.htm](spells/abjuration-07-XZE4BawIlTf88Yl9.htm)|Dimensional Lock|Cerradura dimensional|modificada|
|[abjuration-07-YtJXpiu4ijkB6nP2.htm](spells/abjuration-07-YtJXpiu4ijkB6nP2.htm)|Unbreaking Wave Barrier|Unbreaking Wave Barrier|modificada|
|[abjuration-08-4ddJSjC9Zz5DX0oG.htm](spells/abjuration-08-4ddJSjC9Zz5DX0oG.htm)|Freedom|Libertad|modificada|
|[abjuration-08-C2w3YfBKjIRS07DP.htm](spells/abjuration-08-C2w3YfBKjIRS07DP.htm)|Mind Blank|Mente en blanco|modificada|
|[abjuration-08-iL6TujgTCtRRa0Y0.htm](spells/abjuration-08-iL6TujgTCtRRa0Y0.htm)|Prismatic Wall|Muro prismático|modificada|
|[abjuration-08-My7FvAoLYgGDDBzy.htm](spells/abjuration-08-My7FvAoLYgGDDBzy.htm)|Antimagic Field|Antimagia Field|modificada|
|[abjuration-08-nsQvjNyg4Whw2mek.htm](spells/abjuration-08-nsQvjNyg4Whw2mek.htm)|Divine Aura|Aura divina|modificada|
|[abjuration-08-TYydobaLN4U7Ol3M.htm](spells/abjuration-08-TYydobaLN4U7Ol3M.htm)|Unfettered Mark|Unfettered Mark|modificada|
|[abjuration-09-9DtuMEyHhtHFRp5a.htm](spells/abjuration-09-9DtuMEyHhtHFRp5a.htm)|Untwisting Iron Pillar|Untwisting Iron Pillar|modificada|
|[abjuration-09-AlCFTjSBaCHuRHBv.htm](spells/abjuration-09-AlCFTjSBaCHuRHBv.htm)|Prismatic Shield|Escudo Prismático|modificada|
|[abjuration-09-BI4iwu3nApyIG0zY.htm](spells/abjuration-09-BI4iwu3nApyIG0zY.htm)|Astral Labyrinth|Laberinto Astral|modificada|
|[abjuration-09-ihbRf964JDXztcy3.htm](spells/abjuration-09-ihbRf964JDXztcy3.htm)|Disjunction|Disyunción|modificada|
|[abjuration-09-KkimKrhDxEJVm6TH.htm](spells/abjuration-09-KkimKrhDxEJVm6TH.htm)|Vital Singularity|Vital Singularity|modificada|
|[abjuration-09-PngDCmU0MXZkbu0v.htm](spells/abjuration-09-PngDCmU0MXZkbu0v.htm)|Prismatic Sphere|Esfera prismática|modificada|
|[abjuration-10-KRcccPeNZOZ5Nweh.htm](spells/abjuration-10-KRcccPeNZOZ5Nweh.htm)|Nullify|Nullify|modificada|
|[abjuration-10-UG0SmRYSdbrx2rTA.htm](spells/abjuration-10-UG0SmRYSdbrx2rTA.htm)|Indestructibility|Indestructibilidad|modificada|
|[abjuration-10-X0t0gr7S7CeCt2Q5.htm](spells/abjuration-10-X0t0gr7S7CeCt2Q5.htm)|Anima Invocation (Modified)|Invocación del anima (Modificado)|modificada|
|[conjuration-01-30BBep9U4BDV0EgQ.htm](spells/conjuration-01-30BBep9U4BDV0EgQ.htm)|Infernal Pact|Pacto infernal|modificada|
|[conjuration-01-4YnON9JHYqtLzccu.htm](spells/conjuration-01-4YnON9JHYqtLzccu.htm)|Summon Animal|Convocar animal|modificada|
|[conjuration-01-5p9Y4ACrtRM4gTpN.htm](spells/conjuration-01-5p9Y4ACrtRM4gTpN.htm)|Dimensional Assault|Dimensional Assault|modificada|
|[conjuration-01-Aap7nGpC7neTWm78.htm](spells/conjuration-01-Aap7nGpC7neTWm78.htm)|Animal Allies|Aliados animales|modificada|
|[conjuration-01-B0FZLkoHsiRgw7gv.htm](spells/conjuration-01-B0FZLkoHsiRgw7gv.htm)|Summon Lesser Servitor|Invocar Servidor Menor|modificada|
|[conjuration-01-cokeXkDHUAo4zHsw.htm](spells/conjuration-01-cokeXkDHUAo4zHsw.htm)|Oathkeeper's Insignia|Oathkeeper's Insignia|modificada|
|[conjuration-01-D8cWhrzcsd43OlIX.htm](spells/conjuration-01-D8cWhrzcsd43OlIX.htm)|Div Pact|Pacto Div|modificada|
|[conjuration-01-dDt8VFuLuhznT19v.htm](spells/conjuration-01-dDt8VFuLuhznT19v.htm)|Snare Hopping|Snare Hopping|modificada|
|[conjuration-01-EDABphKEPUBiMmQC.htm](spells/conjuration-01-EDABphKEPUBiMmQC.htm)|Verdant Sprout|Germinación verde|modificada|
|[conjuration-01-Einy9RNTGVq1kY3j.htm](spells/conjuration-01-Einy9RNTGVq1kY3j.htm)|Inside Ropes|Inside Ropes|modificada|
|[conjuration-01-F1nlmqOIucch3Cmt.htm](spells/conjuration-01-F1nlmqOIucch3Cmt.htm)|Pet Cache|Escondrijo para mascotas|modificada|
|[conjuration-01-hs7h8f4Z1ZNdUt3s.htm](spells/conjuration-01-hs7h8f4Z1ZNdUt3s.htm)|Summon Fey|Convocar feérico|modificada|
|[conjuration-01-jks2h5pMsm8pCi8e.htm](spells/conjuration-01-jks2h5pMsm8pCi8e.htm)|Wildfire|Wildfire|modificada|
|[conjuration-01-joEruBVz31Uxczzq.htm](spells/conjuration-01-joEruBVz31Uxczzq.htm)|Angelic Messenger|Mensajero angelical|modificada|
|[conjuration-01-jSRAyd57kd4WZ4yE.htm](spells/conjuration-01-jSRAyd57kd4WZ4yE.htm)|Summon Plant or Fungus|Invocar Planta o Hongo|modificada|
|[conjuration-01-K9gI08enGtmih5X1.htm](spells/conjuration-01-K9gI08enGtmih5X1.htm)|Protector Tree|Árbol protector|modificada|
|[conjuration-01-lKcsmeOrgHtK4xQa.htm](spells/conjuration-01-lKcsmeOrgHtK4xQa.htm)|Summon Construct|Convocar constructo|modificada|
|[conjuration-01-lsR3RLEdBG4rcSzd.htm](spells/conjuration-01-lsR3RLEdBG4rcSzd.htm)|Efficient Apport|Efficient Apport|modificada|
|[conjuration-01-LvezN4a3kYf1OHMg.htm](spells/conjuration-01-LvezN4a3kYf1OHMg.htm)|Floating Disk|Disco flotante|modificada|
|[conjuration-01-MiTFNcqCI9f34A2V.htm](spells/conjuration-01-MiTFNcqCI9f34A2V.htm)|Inkshot|Inkshot|modificada|
|[conjuration-01-MraZBLJ4Be3ogmWL.htm](spells/conjuration-01-MraZBLJ4Be3ogmWL.htm)|Clinging Ice|Clinging Ice|modificada|
|[conjuration-01-myC2EIrsjmB8xosi.htm](spells/conjuration-01-myC2EIrsjmB8xosi.htm)|Pushing Gust|Ráfaga impelente|modificada|
|[conjuration-01-NtzNCW32UlPdY2xS.htm](spells/conjuration-01-NtzNCW32UlPdY2xS.htm)|Ashen Wind|Ashen Wind|modificada|
|[conjuration-01-qTr2oCgIXl703Whb.htm](spells/conjuration-01-qTr2oCgIXl703Whb.htm)|Thoughtful Gift|Regalo considerado|modificada|
|[conjuration-01-rOwOLlfVgLMg1FND.htm](spells/conjuration-01-rOwOLlfVgLMg1FND.htm)|Elemental Sentinel|Centinela elemental|modificada|
|[conjuration-01-sX2g6WFSQPNW9jzx.htm](spells/conjuration-01-sX2g6WFSQPNW9jzx.htm)|Warp Step|Paso de Urdimbre|modificada|
|[conjuration-01-TPI9fRCAYsDqpAe4.htm](spells/conjuration-01-TPI9fRCAYsDqpAe4.htm)|Temporary Tool|Herramienta temporal|modificada|
|[conjuration-01-tsKnoBuBbKMXkiz5.htm](spells/conjuration-01-tsKnoBuBbKMXkiz5.htm)|Abyssal Pact|Pacto abisal|modificada|
|[conjuration-01-tWzxuJdbXqvskdIo.htm](spells/conjuration-01-tWzxuJdbXqvskdIo.htm)|Augment Summoning|Aumentar convocación|modificada|
|[conjuration-01-uZK2BYzPnxUBnDjr.htm](spells/conjuration-01-uZK2BYzPnxUBnDjr.htm)|Tanglefoot|Mara|modificada|
|[conjuration-01-Vpohy4XH1DaH95hT.htm](spells/conjuration-01-Vpohy4XH1DaH95hT.htm)|Daemonic Pact|Pacto daimónico|modificada|
|[conjuration-01-W69zswpj0Trdy5rj.htm](spells/conjuration-01-W69zswpj0Trdy5rj.htm)|Air Bubble|Burbuja de aire|modificada|
|[conjuration-01-WFSazGLCUuPsBv1p.htm](spells/conjuration-01-WFSazGLCUuPsBv1p.htm)|Frost's Touch|Toque de Escarcha|modificada|
|[conjuration-01-WOM9alxiTdp0HEVD.htm](spells/conjuration-01-WOM9alxiTdp0HEVD.htm)|Distortion Lens|Lente de distorsión|modificada|
|[conjuration-01-Wu0xFpewMKRK3HG8.htm](spells/conjuration-01-Wu0xFpewMKRK3HG8.htm)|Grease|Grasa|modificada|
|[conjuration-01-WzLKjSw6hsBhuklC.htm](spells/conjuration-01-WzLKjSw6hsBhuklC.htm)|Create Water|Elaborar agua|modificada|
|[conjuration-01-xqmHD8JIjak15lRk.htm](spells/conjuration-01-xqmHD8JIjak15lRk.htm)|Unseen Servant|Sirviente invisible|modificada|
|[conjuration-01-yAiSNmz39qXOIlco.htm](spells/conjuration-01-yAiSNmz39qXOIlco.htm)|Mud Pit|Foso de barro|modificada|
|[conjuration-01-yvs1zN5Pai5U4CJX.htm](spells/conjuration-01-yvs1zN5Pai5U4CJX.htm)|Summon Instrument|Convocar instrumento|modificada|
|[conjuration-01-zTN6zuruDUKOea6h.htm](spells/conjuration-01-zTN6zuruDUKOea6h.htm)|Rising Surf|Rising Surf|modificada|
|[conjuration-02-23cv1p49NFd0Onng.htm](spells/conjuration-02-23cv1p49NFd0Onng.htm)|Ash Cloud|Nube de ceniza|modificada|
|[conjuration-02-3VxVbZqIRvpKkg3O.htm](spells/conjuration-02-3VxVbZqIRvpKkg3O.htm)|Fungal Infestation|Infestación fúngica|modificada|
|[conjuration-02-9s5tqqXNzcoKamWx.htm](spells/conjuration-02-9s5tqqXNzcoKamWx.htm)|Web|Telara|modificada|
|[conjuration-02-9XHmC2JgTUIQ1CCm.htm](spells/conjuration-02-9XHmC2JgTUIQ1CCm.htm)|Obscuring Mist|Niebla de oscurecimiento|modificada|
|[conjuration-02-crf7EL1JtBYwvAEg.htm](spells/conjuration-02-crf7EL1JtBYwvAEg.htm)|Blood Duplicate|Duplicado de sangre|modificada|
|[conjuration-02-lpT6LotUaQPfinjj.htm](spells/conjuration-02-lpT6LotUaQPfinjj.htm)|Summon Elemental|Convocar elemental|modificada|
|[conjuration-02-Mt6ZzkVX8Q4xigFq.htm](spells/conjuration-02-Mt6ZzkVX8Q4xigFq.htm)|Create Food|Elaborar comida|modificada|
|[conjuration-02-NacrNSvfODxpZena.htm](spells/conjuration-02-NacrNSvfODxpZena.htm)|Blazing Blade|Blazing Blade|modificada|
|[conjuration-02-oOFilBgsXTVIbJpN.htm](spells/conjuration-02-oOFilBgsXTVIbJpN.htm)|Phantom Ship|Phantom Ship|modificada|
|[conjuration-02-qCmihBL0G0G5ExPF.htm](spells/conjuration-02-qCmihBL0G0G5ExPF.htm)|Persistent Servant|Persistent Servant|modificada|
|[conjuration-02-uiv8FKuxE8HOWTxW.htm](spells/conjuration-02-uiv8FKuxE8HOWTxW.htm)|Instant Armor|Armadura instantánea|modificada|
|[conjuration-02-WPKJOhEihhcIm2uQ.htm](spells/conjuration-02-WPKJOhEihhcIm2uQ.htm)|Phantom Steed|Corcel fantasmal|modificada|
|[conjuration-02-Yv3AFIO55FONQYMH.htm](spells/conjuration-02-Yv3AFIO55FONQYMH.htm)|Sonata Span|Sonata Span|modificada|
|[conjuration-03-1bXLp8cyIKFjZtVx.htm](spells/conjuration-03-1bXLp8cyIKFjZtVx.htm)|Temporal Twin|Temporal Twin|modificada|
|[conjuration-03-2ZPqcM9wNoVnpwkK.htm](spells/conjuration-03-2ZPqcM9wNoVnpwkK.htm)|Magical Fetters|Magical Fetters|modificada|
|[conjuration-03-5XUn9NADr05IyiVw.htm](spells/conjuration-03-5XUn9NADr05IyiVw.htm)|Soothing Blossoms|Flores amansadoras|modificada|
|[conjuration-03-98gJvb8Xtn8OLIY7.htm](spells/conjuration-03-98gJvb8Xtn8OLIY7.htm)|Rally Point|Rally Point|modificada|
|[conjuration-03-AkF4yFcSCdhoULyZ.htm](spells/conjuration-03-AkF4yFcSCdhoULyZ.htm)|Awaken Portal|Awaken Portal|modificada|
|[conjuration-03-F9mA2Bg27QKniIdv.htm](spells/conjuration-03-F9mA2Bg27QKniIdv.htm)|Bottomless Stomach|Estómago sin fondo|modificada|
|[conjuration-03-g4MAIQodRDVfNp1B.htm](spells/conjuration-03-g4MAIQodRDVfNp1B.htm)|Personal Blizzard|Personal Blizzard|modificada|
|[conjuration-03-GKpEcy9WId6NJvtx.htm](spells/conjuration-03-GKpEcy9WId6NJvtx.htm)|Stinking Cloud|Nube apestosa|modificada|
|[conjuration-03-iQD8OhhkwhvD8Blw.htm](spells/conjuration-03-iQD8OhhkwhvD8Blw.htm)|Swamp of Sloth|Pantano de pereza|modificada|
|[conjuration-03-jOWQ5wPd4xvSkjI5.htm](spells/conjuration-03-jOWQ5wPd4xvSkjI5.htm)|Mystic Carriage|Mystic Carriage|modificada|
|[conjuration-03-JqHAxsMUZ4Mr5bTr.htm](spells/conjuration-03-JqHAxsMUZ4Mr5bTr.htm)|Ghostly Shift|Cambiante Fantasmal|modificada|
|[conjuration-03-KsWhliKfUs3IpW3c.htm](spells/conjuration-03-KsWhliKfUs3IpW3c.htm)|Wall of Thorns|Muro de espinos|modificada|
|[conjuration-03-LNOwaJAkCfsgTKgV.htm](spells/conjuration-03-LNOwaJAkCfsgTKgV.htm)|Wall of Water|Muro de Agua|modificada|
|[conjuration-03-mwPfoYfVGSMAaUec.htm](spells/conjuration-03-mwPfoYfVGSMAaUec.htm)|Cozy Cabin|Caba acogedora|modificada|
|[conjuration-03-oUDNCArkQTdhllxD.htm](spells/conjuration-03-oUDNCArkQTdhllxD.htm)|Aqueous Orb|Orbe acuoso|modificada|
|[conjuration-03-SxRVCc1Q2MtVuPMo.htm](spells/conjuration-03-SxRVCc1Q2MtVuPMo.htm)|Sign of Conviction|Signo de convicción|modificada|
|[conjuration-03-tFKJCPvOQZxKq6ON.htm](spells/conjuration-03-tFKJCPvOQZxKq6ON.htm)|Mad Monkeys|Monos locos|modificada|
|[conjuration-03-tSosbMsftXcRaQgT.htm](spells/conjuration-03-tSosbMsftXcRaQgT.htm)|Sea of Thought|Sea of Thought|modificada|
|[conjuration-03-u3G7KX1qpFJlSeWm.htm](spells/conjuration-03-u3G7KX1qpFJlSeWm.htm)|Owb Pact|Pacto Owb|modificada|
|[conjuration-03-uuoPmbjEtqzWZs0v.htm](spells/conjuration-03-uuoPmbjEtqzWZs0v.htm)|Unseen Custodians|Custodios invisibles|modificada|
|[conjuration-04-3xD8DYrr8YDVYGg7.htm](spells/conjuration-04-3xD8DYrr8YDVYGg7.htm)|Spike Stones|Púas de piedra|modificada|
|[conjuration-04-Az3PmWnlWSb5ELX9.htm](spells/conjuration-04-Az3PmWnlWSb5ELX9.htm)|Abundant Step|Paso abundante|modificada|
|[conjuration-04-D31YX7zvRBvenTAz.htm](spells/conjuration-04-D31YX7zvRBvenTAz.htm)|Petal Storm|Tormenta de pétalos|modificada|
|[conjuration-04-f9duI5d8xNYqxI8d.htm](spells/conjuration-04-f9duI5d8xNYqxI8d.htm)|Magic Mailbox|Buzón mágico|modificada|
|[conjuration-04-fMnjP4hpNRV9EfVM.htm](spells/conjuration-04-fMnjP4hpNRV9EfVM.htm)|Inevitable Destination|Destino Inevitable|modificada|
|[conjuration-04-GzdgM0m7wXKuFSho.htm](spells/conjuration-04-GzdgM0m7wXKuFSho.htm)|Rope Trick|Truco de la cuerda|modificada|
|[conjuration-04-h7RtKSKGViNtD5o4.htm](spells/conjuration-04-h7RtKSKGViNtD5o4.htm)|Coral Eruption|Erupción de Coral|modificada|
|[conjuration-04-iMmexY6ZosLS4I5R.htm](spells/conjuration-04-iMmexY6ZosLS4I5R.htm)|Door to Beyond|Puerta al Más Allá|modificada|
|[conjuration-04-K1wmI4qPmRhFczmy.htm](spells/conjuration-04-K1wmI4qPmRhFczmy.htm)|Dust Storm|Tormenta de polvo|modificada|
|[conjuration-04-kCgkreCT6g0dipMd.htm](spells/conjuration-04-kCgkreCT6g0dipMd.htm)|Murderous Vine|Murderous Vine|modificada|
|[conjuration-04-piMJO6aYeDJbrhEo.htm](spells/conjuration-04-piMJO6aYeDJbrhEo.htm)|Solid Fog|Niebla sólida|modificada|
|[conjuration-04-TUj8eugNqAvB1vVR.htm](spells/conjuration-04-TUj8eugNqAvB1vVR.htm)|Creation|Creación|modificada|
|[conjuration-04-VlNcjmYyu95vOUe8.htm](spells/conjuration-04-VlNcjmYyu95vOUe8.htm)|Dimension Door|Puerta dimensional|modificada|
|[conjuration-04-vSSKyUdrHu86E5Gk.htm](spells/conjuration-04-vSSKyUdrHu86E5Gk.htm)|Nature's Bounty|El botín de la Naturaleza|modificada|
|[conjuration-04-yfCykJ6cs0uUL79b.htm](spells/conjuration-04-yfCykJ6cs0uUL79b.htm)|Radiant Heart of Devotion|Corazón Radiante de Devoción|modificada|
|[conjuration-04-zjG6NncHyAKqSF7m.htm](spells/conjuration-04-zjG6NncHyAKqSF7m.htm)|Dimensional Steps|Pasos dimensionales|modificada|
|[conjuration-04-zR67Rt3UMHKC5evy.htm](spells/conjuration-04-zR67Rt3UMHKC5evy.htm)|Blink|Intermitencia|modificada|
|[conjuration-05-115Xp9E38CJENhNS.htm](spells/conjuration-05-115Xp9E38CJENhNS.htm)|Passwall|Pasamiento|modificada|
|[conjuration-05-29ytKctjg7qSW2ff.htm](spells/conjuration-05-29ytKctjg7qSW2ff.htm)|Summon Fiend|Convocar infernal|modificada|
|[conjuration-05-2w4OpAGihn1JSHFD.htm](spells/conjuration-05-2w4OpAGihn1JSHFD.htm)|Black Tentacles|Tentáculos negros|modificada|
|[conjuration-05-5ttAVJbWg2GVKmrN.htm](spells/conjuration-05-5ttAVJbWg2GVKmrN.htm)|Shadow Jump|Sombra Salta|modificada|
|[conjuration-05-6AqH5SGchbdhOJxA.htm](spells/conjuration-05-6AqH5SGchbdhOJxA.htm)|Flammable Fumes|Humos inflamables|modificada|
|[conjuration-05-ba12fO37w7O37gim.htm](spells/conjuration-05-ba12fO37w7O37gim.htm)|Summon Axiom|Invocar Axioma|modificada|
|[conjuration-05-BoA00y45uDlmou07.htm](spells/conjuration-05-BoA00y45uDlmou07.htm)|Secret Chest|Cofre Secreto|modificada|
|[conjuration-05-dNpIc5k1aCI3bHIg.htm](spells/conjuration-05-dNpIc5k1aCI3bHIg.htm)|Mosquito Blight|Mosquito Asolar|modificada|
|[conjuration-05-e9UJoVYUd5kJWUpi.htm](spells/conjuration-05-e9UJoVYUd5kJWUpi.htm)|Summon Giant|Convocar gigante|modificada|
|[conjuration-05-F1qxaqsEItmBura2.htm](spells/conjuration-05-F1qxaqsEItmBura2.htm)|Tree Stride|Zancada arbórea|modificada|
|[conjuration-05-i1TvBID5QLyXrUCa.htm](spells/conjuration-05-i1TvBID5QLyXrUCa.htm)|Summon Entity|Convocar a una aberración|modificada|
|[conjuration-05-I2fwPslQth0DTPQD.htm](spells/conjuration-05-I2fwPslQth0DTPQD.htm)|Incendiary Fog|Niebla Incendiaria|modificada|
|[conjuration-05-kghwmH3tQjMIhdH1.htm](spells/conjuration-05-kghwmH3tQjMIhdH1.htm)|Summon Dragon|Convocar dragón|modificada|
|[conjuration-05-kOa055FIrO9Smnya.htm](spells/conjuration-05-kOa055FIrO9Smnya.htm)|Wall of Stone|Muro de piedra|modificada|
|[conjuration-05-lTDixrrNKaCvLKwX.htm](spells/conjuration-05-lTDixrrNKaCvLKwX.htm)|Summon Celestial|Convocar celestial|modificada|
|[conjuration-05-n8ckecJpatSBEp7M.htm](spells/conjuration-05-n8ckecJpatSBEp7M.htm)|Summon Anarch|Invocar Anárquica|modificada|
|[conjuration-05-oXeEbcUdgJGWHGEJ.htm](spells/conjuration-05-oXeEbcUdgJGWHGEJ.htm)|Impaling Spike|Pincho empalador|modificada|
|[conjuration-05-qr0HOiiuqj5LKlDt.htm](spells/conjuration-05-qr0HOiiuqj5LKlDt.htm)|Pillars of Sand|Pilares de Arena|modificada|
|[conjuration-05-rtA3HRGoy7PQTOhq.htm](spells/conjuration-05-rtA3HRGoy7PQTOhq.htm)|Terrain Transposition|Transposición del terreno|modificada|
|[conjuration-05-ru3YdXajUREbKQDV.htm](spells/conjuration-05-ru3YdXajUREbKQDV.htm)|Return Beacon|Baliza Retornante|modificada|
|[conjuration-05-rxvS7EMJ7qmexAyA.htm](spells/conjuration-05-rxvS7EMJ7qmexAyA.htm)|Shadow Walk|Sombra Walk|modificada|
|[conjuration-05-TDaMnCtZ72uyYrz8.htm](spells/conjuration-05-TDaMnCtZ72uyYrz8.htm)|Blink Charge|Carga de intermitencia|modificada|
|[conjuration-05-vgy00hnqxN9VoeoF.htm](spells/conjuration-05-vgy00hnqxN9VoeoF.htm)|Planar Ally|Aliado de los Planos|modificada|
|[conjuration-05-yRf59eFtZ50cGlem.htm](spells/conjuration-05-yRf59eFtZ50cGlem.htm)|Heroes' Feast|Fiesta de los Héroes|modificada|
|[conjuration-05-Z1MoMTcgFQiCI90t.htm](spells/conjuration-05-Z1MoMTcgFQiCI90t.htm)|Tesseract Tunnel|Tesseract Tunnel|modificada|
|[conjuration-06-69L70wKfGDY66Mk9.htm](spells/conjuration-06-69L70wKfGDY66Mk9.htm)|Teleport|Teletransporte|modificada|
|[conjuration-06-c3XygMbzrZMgV1y3.htm](spells/conjuration-06-c3XygMbzrZMgV1y3.htm)|Collective Transposition|Transposición colectiva|modificada|
|[conjuration-06-Cpj05laa7ogqGwS3.htm](spells/conjuration-06-Cpj05laa7ogqGwS3.htm)|Elemental Confluence|Confluencia elemental|modificada|
|[conjuration-06-fRUxp4G9kG816XAt.htm](spells/conjuration-06-fRUxp4G9kG816XAt.htm)|Lure Dream|Sueño Lure|modificada|
|[conjuration-06-In2A7GCyxxaqZdPI.htm](spells/conjuration-06-In2A7GCyxxaqZdPI.htm)|Moonlight Bridge|Moonlight Bridge|modificada|
|[conjuration-06-JbAcSLu62TU1OgNF.htm](spells/conjuration-06-JbAcSLu62TU1OgNF.htm)|Tangling Creepers|Zarcillos enredadores|modificada|
|[conjuration-06-kwlKUxEuT8T15YW6.htm](spells/conjuration-06-kwlKUxEuT8T15YW6.htm)|Primal Summons|Convocación primigenia|modificada|
|[conjuration-06-OOMQ8Z6EIrGt5NJ6.htm](spells/conjuration-06-OOMQ8Z6EIrGt5NJ6.htm)|Unexpected Transposition|Transposición inesperada|modificada|
|[conjuration-07-1VOjUKd1pacI67RZ.htm](spells/conjuration-07-1VOjUKd1pacI67RZ.htm)|Curse of the Spirit Orchestra|Curse of the Spirit Orchestra|modificada|
|[conjuration-07-5bTt2CvYHPvaR7QQ.htm](spells/conjuration-07-5bTt2CvYHPvaR7QQ.htm)|Plane Shift|Cambio de Plano|modificada|
|[conjuration-07-5ZW1w9f4gWlSIuWA.htm](spells/conjuration-07-5ZW1w9f4gWlSIuWA.htm)|Teleportation Circle|Círculo de teletransporte|modificada|
|[conjuration-07-73rToy0v5Ra9NvL6.htm](spells/conjuration-07-73rToy0v5Ra9NvL6.htm)|Duplicate Foe|Duplicar enemigo|modificada|
|[conjuration-07-D2nPKbIS67m9199U.htm](spells/conjuration-07-D2nPKbIS67m9199U.htm)|Ethereal Jaunt|Excursión etérea|modificada|
|[conjuration-07-ggtZTxeyEnDYbt6f.htm](spells/conjuration-07-ggtZTxeyEnDYbt6f.htm)|Momentary Recovery|Recuperación momentánea|modificada|
|[conjuration-07-nzbnTqHgNKiGZkrZ.htm](spells/conjuration-07-nzbnTqHgNKiGZkrZ.htm)|Word of Recall|Palabra de regreso|modificada|
|[conjuration-07-qOeBQyC1z7OScHvP.htm](spells/conjuration-07-qOeBQyC1z7OScHvP.htm)|Maze of Locked Doors|Laberinto de Puertas Cerradas|modificada|
|[conjuration-07-vPWMEyVTreMOoFnm.htm](spells/conjuration-07-vPWMEyVTreMOoFnm.htm)|Magnificent Mansion|Mansión magnífica|modificada|
|[conjuration-08-4ONjK2hoMBmuAAyk.htm](spells/conjuration-08-4ONjK2hoMBmuAAyk.htm)|Summon Irii|Invocar Irii|modificada|
|[conjuration-08-A0sMo1L0271yLeDA.htm](spells/conjuration-08-A0sMo1L0271yLeDA.htm)|Clone Companion|Clonar Compañero|modificada|
|[conjuration-08-fLiowSDQXo3vCQDh.htm](spells/conjuration-08-fLiowSDQXo3vCQDh.htm)|Rite of the Red Star|Rito de la Estrella Roja|modificada|
|[conjuration-08-JAaETUBg0xlttpCH.htm](spells/conjuration-08-JAaETUBg0xlttpCH.htm)|Summon Archmage|Invocar Archimago|modificada|
|[conjuration-08-kIRWUBxocERjIBni.htm](spells/conjuration-08-kIRWUBxocERjIBni.htm)|Summon Deific Herald|Invocar Heraldo Deífico|modificada|
|[conjuration-08-oGV6YdpZLIG4G4gH.htm](spells/conjuration-08-oGV6YdpZLIG4G4gH.htm)|Impaling Briars|Los Zarzas empalador|modificada|
|[conjuration-08-Oj1PJBMQD9vuwCv7.htm](spells/conjuration-08-Oj1PJBMQD9vuwCv7.htm)|Maze|Laberinto|modificada|
|[conjuration-08-Y9w4TYnb8cXU4UKY.htm](spells/conjuration-08-Y9w4TYnb8cXU4UKY.htm)|Split Shadow|Sombra Partida|modificada|
|[conjuration-08-ZwwIUavMbEwcZz35.htm](spells/conjuration-08-ZwwIUavMbEwcZz35.htm)|Create Demiplane|Crear semiplano|modificada|
|[conjuration-09-2EIUqc8TCTQimggQ.htm](spells/conjuration-09-2EIUqc8TCTQimggQ.htm)|Summon Draconic Legion|Convocar Legión Dracónica|modificada|
|[conjuration-09-HHCgEEkeeShVQf8d.htm](spells/conjuration-09-HHCgEEkeeShVQf8d.htm)|Bilocation|Bilocación|modificada|
|[conjuration-09-KPDHmmjJiw7PhTYF.htm](spells/conjuration-09-KPDHmmjJiw7PhTYF.htm)|Resplendent Mansion|Mansión resplandeciente|modificada|
|[conjuration-09-mau1Olq58ECF0ZPi.htm](spells/conjuration-09-mau1Olq58ECF0ZPi.htm)|Empty Body|Cuerpo vacío|modificada|
|[conjuration-09-tgJTm276cikEL8vU.htm](spells/conjuration-09-tgJTm276cikEL8vU.htm)|Summon Ancient Fleshforged|Invocar Ancient Fleshforged|modificada|
|[conjuration-09-xxhS66k68u5iIOHC.htm](spells/conjuration-09-xxhS66k68u5iIOHC.htm)|Upheaval|Upheaval|modificada|
|[conjuration-10-dMKP4fkWx8V2cqAy.htm](spells/conjuration-10-dMKP4fkWx8V2cqAy.htm)|Remake|Rehacer|modificada|
|[conjuration-10-FA55Fxf8MBXhje95.htm](spells/conjuration-10-FA55Fxf8MBXhje95.htm)|Dinosaur Fort|Dinosaur Fort|modificada|
|[conjuration-10-U13bC0tNgrlHoeTK.htm](spells/conjuration-10-U13bC0tNgrlHoeTK.htm)|Gate|Umbral|modificada|
|[conjuration-10-WRP8TDf36hqHyGv1.htm](spells/conjuration-10-WRP8TDf36hqHyGv1.htm)|Summon Kaiju|Invocar Kaiju|modificada|
|[divination-01-3mINzPzup2m9qzFU.htm](spells/divination-01-3mINzPzup2m9qzFU.htm)|Sepulchral Mask|Máscara Sepulcral|modificada|
|[divination-01-5LYi9Efs6cko4GGL.htm](spells/divination-01-5LYi9Efs6cko4GGL.htm)|Object Reading|Leer objeto|modificada|
|[divination-01-5Pc55FGGqVpIAJ62.htm](spells/divination-01-5Pc55FGGqVpIAJ62.htm)|Loremaster's Etude|Estudio del maestro del saber|modificada|
|[divination-01-5wjl0ZwEvvUh7sor.htm](spells/divination-01-5wjl0ZwEvvUh7sor.htm)|Swarmsense|Swarmsense|modificada|
|[divination-01-6ZIKB0151LUR19Rw.htm](spells/divination-01-6ZIKB0151LUR19Rw.htm)|Ill Omen|Mal agüero|modificada|
|[divination-01-8bdt1TvNKzsCu9Ct.htm](spells/divination-01-8bdt1TvNKzsCu9Ct.htm)|Join Pasts|Unir Pasados|modificada|
|[divination-01-aF7RiG7c8GzSQLYt.htm](spells/divination-01-aF7RiG7c8GzSQLYt.htm)|Word of Truth|Palabra de verdad|modificada|
|[divination-01-BvNbDwFYaidKJG9j.htm](spells/divination-01-BvNbDwFYaidKJG9j.htm)|Time Sense|Time Sense|modificada|
|[divination-01-cNnHV97gPqxJ3Rrr.htm](spells/divination-01-cNnHV97gPqxJ3Rrr.htm)|Glimpse Weakness|Vislumbrar Debilidad|modificada|
|[divination-01-CR8OKDbeFJoZbOCu.htm](spells/divination-01-CR8OKDbeFJoZbOCu.htm)|Extend Boost|Extend Boost|modificada|
|[divination-01-D442XMADp01qJ7Cs.htm](spells/divination-01-D442XMADp01qJ7Cs.htm)|Mindlink|Enlace mental|modificada|
|[divination-01-DgcSiOCR1uDXGaEA.htm](spells/divination-01-DgcSiOCR1uDXGaEA.htm)|Synchronize|Sincronizar|modificada|
|[divination-01-dtOUkMC57izf93z5.htm](spells/divination-01-dtOUkMC57izf93z5.htm)|Ancestral Memories|Recuerdos ancestrales|modificada|
|[divination-01-dXIRotMLsABDQQSB.htm](spells/divination-01-dXIRotMLsABDQQSB.htm)|Scholarly Recollection|Recuerdo erudito|modificada|
|[divination-01-EqdSKqr8Qj5TUhMR.htm](spells/divination-01-EqdSKqr8Qj5TUhMR.htm)|Approximate|Aproximadamente|modificada|
|[divination-01-EUMjrJJwSgsqNidi.htm](spells/divination-01-EUMjrJJwSgsqNidi.htm)|Anticipate Peril|Anticipate Peril|modificada|
|[divination-01-Fr2CGvWcgSyLcUi7.htm](spells/divination-01-Fr2CGvWcgSyLcUi7.htm)|Bit of Luck|Un poco de suerte|modificada|
|[divination-01-G0T1xv1FoZ23Jxvt.htm](spells/divination-01-G0T1xv1FoZ23Jxvt.htm)|Nudge Fate|Nudge Fate|modificada|
|[divination-01-Gb7SeieEvd0pL2Eh.htm](spells/divination-01-Gb7SeieEvd0pL2Eh.htm)|True Strike|Golpe verdadero|modificada|
|[divination-01-gpzpAAAJ1Lza2JVl.htm](spells/divination-01-gpzpAAAJ1Lza2JVl.htm)|Detect Magic|Detectar magia|modificada|
|[divination-01-HOj2YsTpkoMpYJH9.htm](spells/divination-01-HOj2YsTpkoMpYJH9.htm)|Practice Makes Perfect|La práctica hace al maestro|modificada|
|[divination-01-izcxFQFwf3woCnFs.htm](spells/divination-01-izcxFQFwf3woCnFs.htm)|Guidance|Orientación divina|modificada|
|[divination-01-lg73SvJZno1ypPAj.htm](spells/divination-01-lg73SvJZno1ypPAj.htm)|Read the Air|Leer el aire|modificada|
|[divination-01-mOUwbIN1SUp8FyPR.htm](spells/divination-01-mOUwbIN1SUp8FyPR.htm)|Cinder Gaze|Cinder Gaze|modificada|
|[divination-01-nVfP43Xbs6I1PO8v.htm](spells/divination-01-nVfP43Xbs6I1PO8v.htm)|Shooting Star|Estrella fugaz|modificada|
|[divination-01-nXmC2Xx9WmS5NsAo.htm](spells/divination-01-nXmC2Xx9WmS5NsAo.htm)|Share Lore|Compartir Lore|modificada|
|[divination-01-oFwmdb6LlRrh9AUT.htm](spells/divination-01-oFwmdb6LlRrh9AUT.htm)|Diviner's Sight|La vista del adivinoón|modificada|
|[divination-01-OhD2Z6rIGGD5ocZA.htm](spells/divination-01-OhD2Z6rIGGD5ocZA.htm)|Read Aura|Leer Aura|modificada|
|[divination-01-q5vbYgLztQ04FQZg.htm](spells/divination-01-q5vbYgLztQ04FQZg.htm)|Seashell of Stolen Sound|Concha de Sonido Robada|modificada|
|[divination-01-QjdvYC1QkpMaemoX.htm](spells/divination-01-QjdvYC1QkpMaemoX.htm)|Nudge the Odds|Nudge the Odds|modificada|
|[divination-01-QnTtGCAvdWRU4spv.htm](spells/divination-01-QnTtGCAvdWRU4spv.htm)|Detect Alignment|Detectar alineamiento|modificada|
|[divination-01-QqxwHeYEVylkYjsO.htm](spells/divination-01-QqxwHeYEVylkYjsO.htm)|Detect Poison|Detectar veneno|modificada|
|[divination-01-RztmhJrLLQWoGVdB.htm](spells/divination-01-RztmhJrLLQWoGVdB.htm)|Object Memory|Memoria de objetos|modificada|
|[divination-01-TjWHgXqPv5jywMti.htm](spells/divination-01-TjWHgXqPv5jywMti.htm)|Pocket Library|Biblioteca de bolsillo|modificada|
|[divination-01-tXa5vOu5giBNCjdR.htm](spells/divination-01-tXa5vOu5giBNCjdR.htm)|Know Direction|Conocer la dirección|modificada|
|[divination-01-UmXhuKrYZR3W16mQ.htm](spells/divination-01-UmXhuKrYZR3W16mQ.htm)|Discern Secrets|Discernir Secretos|modificada|
|[divination-01-Vvxgn7saUPW2bJhb.htm](spells/divination-01-Vvxgn7saUPW2bJhb.htm)|Read Fate|Leer el destino|modificada|
|[divination-01-WJOQryAODgYmrL6g.htm](spells/divination-01-WJOQryAODgYmrL6g.htm)|Imprint Message|Estampar mensaje|modificada|
|[divination-01-XhgMx9WC6NfXd9RP.htm](spells/divination-01-XhgMx9WC6NfXd9RP.htm)|Hyperfocus|Hyperfocus|modificada|
|[divination-01-Yrek2Yd4k3DPC2zV.htm](spells/divination-01-Yrek2Yd4k3DPC2zV.htm)|Zenith Star|Zenith Star|modificada|
|[divination-01-Z7N5IxJCwrAdIgSg.htm](spells/divination-01-Z7N5IxJCwrAdIgSg.htm)|Message Rune|Mensaje Rune|modificada|
|[divination-02-3ehSrqTAm7IPqbIZ.htm](spells/divination-02-3ehSrqTAm7IPqbIZ.htm)|Spirit Sense|Sentido espiritual|modificada|
|[divination-02-41TZEjhO6D1nWw2X.htm](spells/divination-02-41TZEjhO6D1nWw2X.htm)|Augury|Augurio|modificada|
|[divination-02-8VzbumNyMEdSzZSz.htm](spells/divination-02-8VzbumNyMEdSzZSz.htm)|Impeccable Flow|Flujo Impecable|modificada|
|[divination-02-BBvV7qoXGdw09q1C.htm](spells/divination-02-BBvV7qoXGdw09q1C.htm)|Speak with Animals|Hablar con los animales|modificada|
|[divination-02-eW7DqGEvU50CDHqc.htm](spells/divination-02-eW7DqGEvU50CDHqc.htm)|Pack Attack|Ataque en manada|modificada|
|[divination-02-GfrKNJ9pNeATiKCc.htm](spells/divination-02-GfrKNJ9pNeATiKCc.htm)|Hunter's Luck|Suerte de cazador|modificada|
|[divination-02-GQopUYTuhmtb7WMG.htm](spells/divination-02-GQopUYTuhmtb7WMG.htm)|Perfect Strike|Golpe Perfecto|modificada|
|[divination-02-HTou8cG05yuSkesj.htm](spells/divination-02-HTou8cG05yuSkesj.htm)|Status|Situación|modificada|
|[divination-02-jwK43yKsHTkJQvQ9.htm](spells/divination-02-jwK43yKsHTkJQvQ9.htm)|See Invisibility|Ver lo invisible|modificada|
|[divination-02-L10q1ng6U3sega9S.htm](spells/divination-02-L10q1ng6U3sega9S.htm)|Guiding Star|Guiding Star|modificada|
|[divination-02-MkHshwYy2UOeEHRR.htm](spells/divination-02-MkHshwYy2UOeEHRR.htm)|Lucky Month|Mes de la suerte|modificada|
|[divination-02-NhNKzq1DvFxkvTEc.htm](spells/divination-02-NhNKzq1DvFxkvTEc.htm)|Vision of Weakness|Visión de la debilidad|modificada|
|[divination-02-obVA6duK5fGbfFUY.htm](spells/divination-02-obVA6duK5fGbfFUY.htm)|Empathic Link|Empathic Link|modificada|
|[divination-02-OhgfPzB5gymZ0IZM.htm](spells/divination-02-OhgfPzB5gymZ0IZM.htm)|Timely Tutor|Timely Tutor|modificada|
|[divination-02-ou56ShiFH7GWF8hX.htm](spells/divination-02-ou56ShiFH7GWF8hX.htm)|Light of Revelation|Luz del Apocalipsis|modificada|
|[divination-02-pZTqGY1MLRjgKasV.htm](spells/divination-02-pZTqGY1MLRjgKasV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[divination-02-rDDYGY2ekZ1yVv7q.htm](spells/divination-02-rDDYGY2ekZ1yVv7q.htm)|Lucky Number|Número de la suerte|modificada|
|[divination-02-vTQvfYu2llKQedmY.htm](spells/divination-02-vTQvfYu2llKQedmY.htm)|Comprehend Language|Comprensión idiomática|modificada|
|[divination-03-1HfusQ8NDWutGvMx.htm](spells/divination-03-1HfusQ8NDWutGvMx.htm)|Animal Vision|Visión animal|modificada|
|[divination-03-28kgh0JzBO6pt38C.htm](spells/divination-03-28kgh0JzBO6pt38C.htm)|Focusing Hum|Focusing Hum|modificada|
|[divination-03-2jWVNdVlbJq84dfT.htm](spells/divination-03-2jWVNdVlbJq84dfT.htm)|Battlefield Persistence|Persistencia del campo de batalla|modificada|
|[divination-03-32NpJbPblay77K6v.htm](spells/divination-03-32NpJbPblay77K6v.htm)|Tattoo Whispers|Tattoo Whispers|modificada|
|[divination-03-3mQkAjexj6hhAnkR.htm](spells/divination-03-3mQkAjexj6hhAnkR.htm)|Behold the Weave|He aquí la trama|modificada|
|[divination-03-5WM3WjshXgrkVCg6.htm](spells/divination-03-5WM3WjshXgrkVCg6.htm)|Beastmaster Trance|Beastmaster Trance|modificada|
|[divination-03-9GkOWDFDEMuV3hJr.htm](spells/divination-03-9GkOWDFDEMuV3hJr.htm)|Familiar's Face|Rostro del familiar|modificada|
|[divination-03-9vDmTCHyAWdWWPIs.htm](spells/divination-03-9vDmTCHyAWdWWPIs.htm)|Contact Friends|Contacto Amigos|modificada|
|[divination-03-bH9cH9aDByY91l1d.htm](spells/divination-03-bH9cH9aDByY91l1d.htm)|Organsight|Organsight|modificada|
|[divination-03-dL3A7H4LlRH26Imo.htm](spells/divination-03-dL3A7H4LlRH26Imo.htm)|Arcane Weaving|Tejido Arcano|modificada|
|[divination-03-DLFupJuCfzyLCQcO.htm](spells/divination-03-DLFupJuCfzyLCQcO.htm)|Eyes of the Dead|Los ojos de los muertos|modificada|
|[divination-03-ErCsWsqXe8ccRjgO.htm](spells/divination-03-ErCsWsqXe8ccRjgO.htm)|Far Sight|Far Sight|modificada|
|[divination-03-GurWFDj4IjKv73kL.htm](spells/divination-03-GurWFDj4IjKv73kL.htm)|Excise Lexicon|Excise Lexicon|modificada|
|[divination-03-GxxnhRIaoGKtu1iO.htm](spells/divination-03-GxxnhRIaoGKtu1iO.htm)|Extend Spell|Prolongar conjuro|modificada|
|[divination-03-HXhWYJviWalN5tQ2.htm](spells/divination-03-HXhWYJviWalN5tQ2.htm)|Clairaudience|Clariaudiencia|modificada|
|[divination-03-KHnhPHL4x1AQHfbC.htm](spells/divination-03-KHnhPHL4x1AQHfbC.htm)|Mind Reading|Leer la mente|modificada|
|[divination-03-l4bHFR9UW2XiY3kH.htm](spells/divination-03-l4bHFR9UW2XiY3kH.htm)|Omnidirectional Scan|Escaneo Omnidireccional|modificada|
|[divination-03-LbPLNWlLCxKCo5gF.htm](spells/divination-03-LbPLNWlLCxKCo5gF.htm)|Access Lore|Acceso Lore|modificada|
|[divination-03-LQzlKbYjZSMFQawP.htm](spells/divination-03-LQzlKbYjZSMFQawP.htm)|Locate|Localizar|modificada|
|[divination-03-oLylenH2jlP5UbRT.htm](spells/divination-03-oLylenH2jlP5UbRT.htm)|Painted Scout|Explorador Pintado|modificada|
|[divination-03-ovx7O2FHvkjXhMcA.htm](spells/divination-03-ovx7O2FHvkjXhMcA.htm)|Perseis's Precautions|Precauciones de Perseis|modificada|
|[divination-03-ppA1StEigPLKEQqR.htm](spells/divination-03-ppA1StEigPLKEQqR.htm)|Wanderer's Guide|Guía del vagabundo|modificada|
|[divination-03-QpjHqxwTGdILLvjD.htm](spells/divination-03-QpjHqxwTGdILLvjD.htm)|Ephemeral Tracking|Rastrear efímero|modificada|
|[divination-03-rfFZKfeCSweFv7P3.htm](spells/divination-03-rfFZKfeCSweFv7P3.htm)|Impending Doom|Perdición inminente|modificada|
|[divination-03-ThE5zPYKF4weiljj.htm](spells/divination-03-ThE5zPYKF4weiljj.htm)|Show the Way|Muestra el camino|modificada|
|[divination-03-XQSPJHOf3TyfqvgS.htm](spells/divination-03-XQSPJHOf3TyfqvgS.htm)|Web of Eyes|Telara de Ojos|modificada|
|[divination-03-YZnLggKDHkMY4cnw.htm](spells/divination-03-YZnLggKDHkMY4cnw.htm)|Transcribe Conflict|Transcribir Conflicto|modificada|
|[divination-03-ZYoC630tNGutgbE0.htm](spells/divination-03-ZYoC630tNGutgbE0.htm)|Hypercognition|Hipercognición|modificada|
|[divination-04-11P3KgDTMIeQIJD7.htm](spells/divination-04-11P3KgDTMIeQIJD7.htm)|Dawnflower's Light|Luz de la Aurora|modificada|
|[divination-04-4LSf04FFvDgMyDk6.htm](spells/divination-04-4LSf04FFvDgMyDk6.htm)|Vigilant Eye|Ojo vigilante|modificada|
|[divination-04-7OwZHalOdRCRnFmZ.htm](spells/divination-04-7OwZHalOdRCRnFmZ.htm)|Lucky Break|Buena racha|modificada|
|[divination-04-a5uwCgOe7ayHPtHe.htm](spells/divination-04-a5uwCgOe7ayHPtHe.htm)|Enhance Senses|Enhance Senses|modificada|
|[divination-04-AnWCohzPgK4L9GVl.htm](spells/divination-04-AnWCohzPgK4L9GVl.htm)|Detect Scrying|Detectar escudrimiento|modificada|
|[divination-04-EGTTGI1pkmsVPqHp.htm](spells/divination-04-EGTTGI1pkmsVPqHp.htm)|Winning Streak|Winning Streak|modificada|
|[divination-04-ehd9PtifLMwjk4ep.htm](spells/divination-04-ehd9PtifLMwjk4ep.htm)|Concealment's Curtain|Cortina de ocultación|modificada|
|[divination-04-FhOaQDTSnsY7tiam.htm](spells/divination-04-FhOaQDTSnsY7tiam.htm)|Modify Memory|Modificar recuerdo|modificada|
|[divination-04-GqvKSxzN7A7kuFk4.htm](spells/divination-04-GqvKSxzN7A7kuFk4.htm)|Tempt Fate|Tentar al Destino|modificada|
|[divination-04-HqTI6wRrck1YXp3F.htm](spells/divination-04-HqTI6wRrck1YXp3F.htm)|Telepathy|Telepatía|modificada|
|[divination-04-HqZ1VWIcXXZXWm3K.htm](spells/divination-04-HqZ1VWIcXXZXWm3K.htm)|Web of Influence|Telara de influencia|modificada|
|[divination-04-ivKnEtI1z4UqEKIA.htm](spells/divination-04-ivKnEtI1z4UqEKIA.htm)|Pulse of the City|Pulso de la ciudad|modificada|
|[divination-04-kREpsv78anJpIiq2.htm](spells/divination-04-kREpsv78anJpIiq2.htm)|Detect Creator|Detectar Creador|modificada|
|[divination-04-l6zjNysNedpJcmDT.htm](spells/divination-04-l6zjNysNedpJcmDT.htm)|Know the Enemy|Conocer al enemigo|modificada|
|[divination-04-nrW6lGV4xDMqLS3P.htm](spells/divination-04-nrW6lGV4xDMqLS3P.htm)|Remember the Lost|Recuerde que los perdidos|modificada|
|[divination-04-pkcOby5prOausy1k.htm](spells/divination-04-pkcOby5prOausy1k.htm)|Read Omens|Leer augurios|modificada|
|[divination-04-qvwIwJ9QBihy8R0t.htm](spells/divination-04-qvwIwJ9QBihy8R0t.htm)|Speak with Plants|Hablar con las plantas|modificada|
|[divination-04-s3LISC5Z55urpBgU.htm](spells/divination-04-s3LISC5Z55urpBgU.htm)|Path of Least Resistance|Camino de menor resistencia|modificada|
|[divination-04-TRpUjBNPQz3Eshaq.htm](spells/divination-04-TRpUjBNPQz3Eshaq.htm)|Weaponize Secret|Weaponize Secret|modificada|
|[divination-04-u0AtDZs6BhBPtjEs.htm](spells/divination-04-u0AtDZs6BhBPtjEs.htm)|Forgotten Lines|Líneas olvidadas|modificada|
|[divination-04-uNsliWpl8Q1JdFcM.htm](spells/divination-04-uNsliWpl8Q1JdFcM.htm)|Discern Lies|Discernir mentiras|modificada|
|[divination-04-WVc30DGbM7TRLLul.htm](spells/divination-04-WVc30DGbM7TRLLul.htm)|Ghostly Tragedy|Tragedia fantasmal|modificada|
|[divination-04-ykyKclKTCMp2SFXa.htm](spells/divination-04-ykyKclKTCMp2SFXa.htm)|Countless Eyes|Ojos innumerables|modificada|
|[divination-04-ZjbVgIIqMstmdkqP.htm](spells/divination-04-ZjbVgIIqMstmdkqP.htm)|Glimpse the Truth|Entrever la verdad|modificada|
|[divination-04-zvKWclOZ7A53DObE.htm](spells/divination-04-zvKWclOZ7A53DObE.htm)|Clairvoyance|Clarividencia|modificada|
|[divination-05-3DB3F2hIx2dMbX8n.htm](spells/divination-05-3DB3F2hIx2dMbX8n.htm)|The World's a Stage|The World's a Stage|modificada|
|[divination-05-4Cntq9odgW6xMpAs.htm](spells/divination-05-4Cntq9odgW6xMpAs.htm)|Astral Projection|Proyección astral|modificada|
|[divination-05-8THDHP0UC7SgOYYF.htm](spells/divination-05-8THDHP0UC7SgOYYF.htm)|Inevitable Disaster|Desastre Inevitable|modificada|
|[divination-05-9BnhadUO8FMLmeZ3.htm](spells/divination-05-9BnhadUO8FMLmeZ3.htm)|Mind Probe|Sonda mental|modificada|
|[divination-05-Ek5XI0aEdZhBgm21.htm](spells/divination-05-Ek5XI0aEdZhBgm21.htm)|Prying Eye|Ojo fisgón|modificada|
|[divination-05-floDCUOSzT0F6r77.htm](spells/divination-05-floDCUOSzT0F6r77.htm)|Unblinking Flame Aura|Aura de llamas sin parpadear.|modificada|
|[divination-05-gfVXAW95YWRz0pJC.htm](spells/divination-05-gfVXAW95YWRz0pJC.htm)|Telepathic Bond|Vínculo telepático|modificada|
|[divination-05-NIop4eI2i7cKFGad.htm](spells/divination-05-NIop4eI2i7cKFGad.htm)|Mirecloak|Mirecloak|modificada|
|[divination-05-OAt2ZEns1gIOCgrn.htm](spells/divination-05-OAt2ZEns1gIOCgrn.htm)|Synesthesia|Sinestesia|modificada|
|[divination-05-R9xqCBblkS5KE4y7.htm](spells/divination-05-R9xqCBblkS5KE4y7.htm)|Sending|Recado|modificada|
|[divination-05-s4HTepjhQWV1yfBs.htm](spells/divination-05-s4HTepjhQWV1yfBs.htm)|Foresee the Path|Prever el camino|modificada|
|[divination-05-SwUiVavHKMWG7t5K.htm](spells/divination-05-SwUiVavHKMWG7t5K.htm)|Tongues|Lenguas|modificada|
|[divination-05-tj86Rnq3QuQnDtG3.htm](spells/divination-05-tj86Rnq3QuQnDtG3.htm)|Hunter's Vision|Visión del Cazador|modificada|
|[divination-05-zaZieDHguhahmM2z.htm](spells/divination-05-zaZieDHguhahmM2z.htm)|Waters of Prediction|Aguas de Predicción|modificada|
|[divination-06-4j0FQ1mkidBAXuQV.htm](spells/divination-06-4j0FQ1mkidBAXuQV.htm)|Heroic Feat|Hazaña heroica|modificada|
|[divination-06-7DN13ILADW2N9Z1t.htm](spells/divination-06-7DN13ILADW2N9Z1t.htm)|Commune|comunión|modificada|
|[divination-06-9lz67JCNCLDfIEy3.htm](spells/divination-06-9lz67JCNCLDfIEy3.htm)|Awaken Object|Despertar Objeto|modificada|
|[divination-06-B4dDkYsHFo1H0CIF.htm](spells/divination-06-B4dDkYsHFo1H0CIF.htm)|Awaken Animal|Despertar de consciencia a un animal|modificada|
|[divination-06-BazbvgNmK46XjrVc.htm](spells/divination-06-BazbvgNmK46XjrVc.htm)|Word of Revision|Palabra de Revisión|modificada|
|[divination-06-FyOgUkq71LNC143w.htm](spells/divination-06-FyOgUkq71LNC143w.htm)|Catch Your Name|Catch Your Name|modificada|
|[divination-06-k0qJfSZH5xUEggwU.htm](spells/divination-06-k0qJfSZH5xUEggwU.htm)|Suspended Retribution|Retribución Suspendida|modificada|
|[divination-06-l4LFwY7iuzX6sDXr.htm](spells/divination-06-l4LFwY7iuzX6sDXr.htm)|Commune with Nature|Comunión con la Naturaleza|modificada|
|[divination-06-MT8usUfwudDVUm5H.htm](spells/divination-06-MT8usUfwudDVUm5H.htm)|Manifold Lives|Manifold Lives|modificada|
|[divination-06-NeyyAPwa639WjYGG.htm](spells/divination-06-NeyyAPwa639WjYGG.htm)|Asmodean Wager|Asmodean Wager|modificada|
|[divination-06-OdqM06M0wDUqZWiR.htm](spells/divination-06-OdqM06M0wDUqZWiR.htm)|Cast into Time|Fundido en el tiempo|modificada|
|[divination-06-r784cIz17eWujtQj.htm](spells/divination-06-r784cIz17eWujtQj.htm)|Scrying|Escudrimiento|modificada|
|[divination-06-UJmKPm1FC6pf6txP.htm](spells/divination-06-UJmKPm1FC6pf6txP.htm)|Halcyon Infusion|Infusión Halcyon|modificada|
|[divination-06-uqlxMQQeSGWEVjki.htm](spells/divination-06-uqlxMQQeSGWEVjki.htm)|True Seeing|Visión verdadera|modificada|
|[divination-06-XULNb8ItUsfupxqH.htm](spells/divination-06-XULNb8ItUsfupxqH.htm)|Dread Secret|Dread Secret|modificada|
|[divination-06-Zvg0FWzClGbzucFd.htm](spells/divination-06-Zvg0FWzClGbzucFd.htm)|Speaking Sky|Speaking Sky|modificada|
|[divination-07-6TKGaQm4PfEMkeRd.htm](spells/divination-07-6TKGaQm4PfEMkeRd.htm)|Supreme Connection|Conexión Suprema|modificada|
|[divination-07-AlbpWWN87yGegoAF.htm](spells/divination-07-AlbpWWN87yGegoAF.htm)|True Target|Objetivo verdadero|modificada|
|[divination-07-G56DJkxlUjFv0C4Z.htm](spells/divination-07-G56DJkxlUjFv0C4Z.htm)|Time Beacon|Time Beacon|modificada|
|[divination-07-hp6Q64dl7xbdn4gQ.htm](spells/divination-07-hp6Q64dl7xbdn4gQ.htm)|Legend Lore|Legend Lore|modificada|
|[divination-07-KADeCawMG6WAzYHa.htm](spells/divination-07-KADeCawMG6WAzYHa.htm)|Unblinking Flame Emblem|Emblema de la Llama sin parpadear.|modificada|
|[divination-07-rsZ5c0AUyywe5yoK.htm](spells/divination-07-rsZ5c0AUyywe5yoK.htm)|Retrocognition|Retrocognición|modificada|
|[divination-08-CeSh8QcVnqP5OlLj.htm](spells/divination-08-CeSh8QcVnqP5OlLj.htm)|Discern Location|Discernir ubicación|modificada|
|[divination-08-pt3gEnzA159uHcJC.htm](spells/divination-08-pt3gEnzA159uHcJC.htm)|Prying Survey|Prying Survey|modificada|
|[divination-08-y2cQYLr5mljDSu1G.htm](spells/divination-08-y2cQYLr5mljDSu1G.htm)|Unrelenting Observation|Observación implacable|modificada|
|[divination-09-aFd00WT266VYPqOG.htm](spells/divination-09-aFd00WT266VYPqOG.htm)|Unblinking Flame Ignition|Encendido de llama sin parpadear.|modificada|
|[divination-09-coKiMMBLESkaLNVa.htm](spells/divination-09-coKiMMBLESkaLNVa.htm)|Proliferating Eyes|Ojos Proliferantes|modificada|
|[divination-09-qsNeG9KZpODSACMq.htm](spells/divination-09-qsNeG9KZpODSACMq.htm)|Foresight|Presciencia|modificada|
|[divination-10-6dDtGIUerazSHIOu.htm](spells/divination-10-6dDtGIUerazSHIOu.htm)|Wish|Deseo|modificada|
|[divination-10-Di9gL6KIZ9eFgbs1.htm](spells/divination-10-Di9gL6KIZ9eFgbs1.htm)|Fated Confrontation|Fated Confrontation|modificada|
|[divination-10-h8zxY9hTeHtWsBVW.htm](spells/divination-10-h8zxY9hTeHtWsBVW.htm)|Alter Reality|Alterar la realidad|modificada|
|[divination-10-MLNeD5sAunV0E23j.htm](spells/divination-10-MLNeD5sAunV0E23j.htm)|Primal Phenomenon|Fenómeno primigenio|modificada|
|[divination-10-YfJTXyVGzLhM6V8U.htm](spells/divination-10-YfJTXyVGzLhM6V8U.htm)|Miracle|Milagro|modificada|
|[enchantment-01-09C2wvWZLCxwjINk.htm](spells/enchantment-01-09C2wvWZLCxwjINk.htm)|Invoke True Name|Invoke True Name|modificada|
|[enchantment-01-0Dcd4iEXqCrkm4Jn.htm](spells/enchantment-01-0Dcd4iEXqCrkm4Jn.htm)|Liberating Command|Orden imperiosa|modificada|
|[enchantment-01-4c1c6eNzU1PFGkAy.htm](spells/enchantment-01-4c1c6eNzU1PFGkAy.htm)|Endure|Endure|modificada|
|[enchantment-01-4gBIw4IDrSfFHik4.htm](spells/enchantment-01-4gBIw4IDrSfFHik4.htm)|Daze|Atontar|modificada|
|[enchantment-01-4koZzrnMXhhosn0D.htm](spells/enchantment-01-4koZzrnMXhhosn0D.htm)|Fear|Miedo|modificada|
|[enchantment-01-8E97SA9KAWCNdXfO.htm](spells/enchantment-01-8E97SA9KAWCNdXfO.htm)|Schadenfreude|Schadenfreude|modificada|
|[enchantment-01-aEitTTb9PnOyidRf.htm](spells/enchantment-01-aEitTTb9PnOyidRf.htm)|Needle of Vengeance|Aguja de la Venganza|modificada|
|[enchantment-01-aIHY2DArKFweIrpf.htm](spells/enchantment-01-aIHY2DArKFweIrpf.htm)|Command|Orden imperiosa|modificada|
|[enchantment-01-BQMEaeJiEFC1nepg.htm](spells/enchantment-01-BQMEaeJiEFC1nepg.htm)|Forced Mercy|Merced vigorosa|modificada|
|[enchantment-01-BRtKFk0PKfWIlCAB.htm](spells/enchantment-01-BRtKFk0PKfWIlCAB.htm)|Sweet Dream|Dulce Sue|modificada|
|[enchantment-01-d2pi7laQkzlr3wrS.htm](spells/enchantment-01-d2pi7laQkzlr3wrS.htm)|Ancestral Touch|Ancestral Touch|modificada|
|[enchantment-01-Dj44lViYKvOJ8a53.htm](spells/enchantment-01-Dj44lViYKvOJ8a53.htm)|Blind Ambition|Amición ciega|modificada|
|[enchantment-01-dqaCLzINHBiKjh4J.htm](spells/enchantment-01-dqaCLzINHBiKjh4J.htm)|Call to Arms|Llamada a las Armas|modificada|
|[enchantment-01-DU5daB09xwfE1y38.htm](spells/enchantment-01-DU5daB09xwfE1y38.htm)|Waking Nightmare|Pesadilla despierta|modificada|
|[enchantment-01-EeEgzWYcAY7ZQkS6.htm](spells/enchantment-01-EeEgzWYcAY7ZQkS6.htm)|Synchronize Steps|Sincronizar Pasos|modificada|
|[enchantment-01-f0Z5mqGA6Yu79B8x.htm](spells/enchantment-01-f0Z5mqGA6Yu79B8x.htm)|Inspire Competence|Inspirar gran aptitud|modificada|
|[enchantment-01-f45JpY7Ph2cAJGW2.htm](spells/enchantment-01-f45JpY7Ph2cAJGW2.htm)|Evil Eye|Ojo maligno|modificada|
|[enchantment-01-fxRaWoeOGyi6THYH.htm](spells/enchantment-01-fxRaWoeOGyi6THYH.htm)|Frenzied Revelry|Frenzied Revelry|modificada|
|[enchantment-01-GdN5YQE47gd79k7X.htm](spells/enchantment-01-GdN5YQE47gd79k7X.htm)|Wilding Word|Palabra salvaje|modificada|
|[enchantment-01-GeUbPvwdZ4B4l0up.htm](spells/enchantment-01-GeUbPvwdZ4B4l0up.htm)|Stoke the Heart|Stoke the Heart|modificada|
|[enchantment-01-IAjvwqgiDr3qGYxY.htm](spells/enchantment-01-IAjvwqgiDr3qGYxY.htm)|Inspire Courage|Inspirar valor|modificada|
|[enchantment-01-IkS3lDGUpIOMug7v.htm](spells/enchantment-01-IkS3lDGUpIOMug7v.htm)|Faerie Dust|Polvo feérico|modificada|
|[enchantment-01-irTdhxTixU9u9YUm.htm](spells/enchantment-01-irTdhxTixU9u9YUm.htm)|Lingering Composition|Composición persistente|modificada|
|[enchantment-01-jsWthW1Qy6tg3SwD.htm](spells/enchantment-01-jsWthW1Qy6tg3SwD.htm)|Draw Ire|Draw Ire|modificada|
|[enchantment-01-k2QrUk7jWMAWozMh.htm](spells/enchantment-01-k2QrUk7jWMAWozMh.htm)|Brain Drain|Fuga de Cerebros|modificada|
|[enchantment-01-k6f5nvSv0XIhbiHj.htm](spells/enchantment-01-k6f5nvSv0XIhbiHj.htm)|Hollow Heart|Hollow Heart|modificada|
|[enchantment-01-KMFRKzNCq7hVNH7H.htm](spells/enchantment-01-KMFRKzNCq7hVNH7H.htm)|Charming Words|Palabras hechiczadoras|modificada|
|[enchantment-01-lpoWfblSMLcJfxsZ.htm](spells/enchantment-01-lpoWfblSMLcJfxsZ.htm)|Forbidden Thought|Pensamiento Prohibido|modificada|
|[enchantment-01-MmQiEc7aM9PDLO2J.htm](spells/enchantment-01-MmQiEc7aM9PDLO2J.htm)|Touch of Obedience|Toque de obediencia|modificada|
|[enchantment-01-NNoKWiWKqJkdD2ln.htm](spells/enchantment-01-NNoKWiWKqJkdD2ln.htm)|Veil of Dreams|El Velo de los Sues|modificada|
|[enchantment-01-o4lRVTwSxnOOn5vl.htm](spells/enchantment-01-o4lRVTwSxnOOn5vl.htm)|Sleep|Dormir|modificada|
|[enchantment-01-oPVyu2a0K3aTVIR8.htm](spells/enchantment-01-oPVyu2a0K3aTVIR8.htm)|Elysian Whimsy|Elysian Whimsy|modificada|
|[enchantment-01-pBevG6bSQOiyflev.htm](spells/enchantment-01-pBevG6bSQOiyflev.htm)|Befuddle|Befuddle|modificada|
|[enchantment-01-pHrVvoTKygXeczVG.htm](spells/enchantment-01-pHrVvoTKygXeczVG.htm)|Nymph's Token|Nymph's Token|modificada|
|[enchantment-01-Q25JQAgnJSGgFDKZ.htm](spells/enchantment-01-Q25JQAgnJSGgFDKZ.htm)|Veil of Confidence|Velo de confianza|modificada|
|[enchantment-01-r8g7oSumKOHDqJsd.htm](spells/enchantment-01-r8g7oSumKOHDqJsd.htm)|Agitate|Agitar|modificada|
|[enchantment-01-rerNA6YZsdxuJYt3.htm](spells/enchantment-01-rerNA6YZsdxuJYt3.htm)|Déjà Vu|Déjà Vu|modificada|
|[enchantment-01-s7ILzY2xh1tc9U1v.htm](spells/enchantment-01-s7ILzY2xh1tc9U1v.htm)|Tame|Tame|modificada|
|[enchantment-01-szIyEsvihc5e1w8n.htm](spells/enchantment-01-szIyEsvihc5e1w8n.htm)|Soothe|Amansar|modificada|
|[enchantment-01-T90ij2uu6ZaBaSXV.htm](spells/enchantment-01-T90ij2uu6ZaBaSXV.htm)|Lament|Lament|modificada|
|[enchantment-01-u4FGIUQgruLjml7J.htm](spells/enchantment-01-u4FGIUQgruLjml7J.htm)|Magic's Vessel|Recepáculo de la magia|modificada|
|[enchantment-01-ut9IhJ9jSZSHDUop.htm](spells/enchantment-01-ut9IhJ9jSZSHDUop.htm)|Charming Touch|Toque hechizador|modificada|
|[enchantment-01-VACvA5VRbddjt5s9.htm](spells/enchantment-01-VACvA5VRbddjt5s9.htm)|Infectious Enthusiasm|Entusiasmo Infeccioso|modificada|
|[enchantment-01-Vctwx1ewa8HUOA94.htm](spells/enchantment-01-Vctwx1ewa8HUOA94.htm)|Diabolic Edict|Edicto diabólico|modificada|
|[enchantment-01-vLA0q0WOK2YPuJs6.htm](spells/enchantment-01-vLA0q0WOK2YPuJs6.htm)|Charm|Hechizar|modificada|
|[enchantment-01-vZSWzkw0uF4iFWSM.htm](spells/enchantment-01-vZSWzkw0uF4iFWSM.htm)|Celestial Accord|Acuerdo Celestial|modificada|
|[enchantment-01-WILXkjU5Yq3yw10r.htm](spells/enchantment-01-WILXkjU5Yq3yw10r.htm)|Counter Performance|Contrarrestar interpretación|modificada|
|[enchantment-01-xn0V2HDrmDWNzPEt.htm](spells/enchantment-01-xn0V2HDrmDWNzPEt.htm)|Savor the Sting|Saborear el aguijón|modificada|
|[enchantment-01-XSujb7EsSwKl19Uu.htm](spells/enchantment-01-XSujb7EsSwKl19Uu.htm)|Bless|Bendecir|modificada|
|[enchantment-01-Xxdwkt0EEDgP1LGc.htm](spells/enchantment-01-Xxdwkt0EEDgP1LGc.htm)|Song of Strength|Canción de la Fuerza|modificada|
|[enchantment-01-YGRpHU5yxw73mls8.htm](spells/enchantment-01-YGRpHU5yxw73mls8.htm)|Soothing Words|Palabras de calma|modificada|
|[enchantment-01-YNAthsgsJjQIXbc8.htm](spells/enchantment-01-YNAthsgsJjQIXbc8.htm)|Pact Broker|Pact Broker|modificada|
|[enchantment-01-YVK3JUkPVzHIeGXQ.htm](spells/enchantment-01-YVK3JUkPVzHIeGXQ.htm)|Cackle|Cackle|modificada|
|[enchantment-02-29JyWqqZ0thCFr1C.htm](spells/enchantment-02-29JyWqqZ0thCFr1C.htm)|Mind Games|Juegos mentales|modificada|
|[enchantment-02-5pwK2FZX6QwgtfqX.htm](spells/enchantment-02-5pwK2FZX6QwgtfqX.htm)|Inveigle|Embaucamiento|modificada|
|[enchantment-02-b515AZlB0sridKSq.htm](spells/enchantment-02-b515AZlB0sridKSq.htm)|Calm Emotions|Calmar emociones|modificada|
|[enchantment-02-bH0kPuf7UKxRvi2P.htm](spells/enchantment-02-bH0kPuf7UKxRvi2P.htm)|Inspire Defense|Inspirar defensa|modificada|
|[enchantment-02-c8R2fpk88fBwJ1ie.htm](spells/enchantment-02-c8R2fpk88fBwJ1ie.htm)|Triple Time|Triplicar la marcha|modificada|
|[enchantment-02-CQb8HtQ1BPeZmu9h.htm](spells/enchantment-02-CQb8HtQ1BPeZmu9h.htm)|Touch of Idiocy|Toque de idiotez|modificada|
|[enchantment-02-EfFMLVbmkBWmzoLF.htm](spells/enchantment-02-EfFMLVbmkBWmzoLF.htm)|Remove Fear|Quitar miedo|modificada|
|[enchantment-02-h6VoHgPC0JCTVzgP.htm](spells/enchantment-02-h6VoHgPC0JCTVzgP.htm)|Vicious Jealousy|Vicious Jealousy|modificada|
|[enchantment-02-hoR6w8BqX2F35Tdx.htm](spells/enchantment-02-hoR6w8BqX2F35Tdx.htm)|Blistering Invective|Invectiva ardiente|modificada|
|[enchantment-02-kIwA7kwp5E0AC3yM.htm](spells/enchantment-02-kIwA7kwp5E0AC3yM.htm)|Warrior's Regret|El arrepentimiento del combatiente|modificada|
|[enchantment-02-LrRyNA2bo5UwBxud.htm](spells/enchantment-02-LrRyNA2bo5UwBxud.htm)|Horrifying Blood Loss|Horrifying Blood Loss|modificada|
|[enchantment-02-mMgLHTLHjjGa206q.htm](spells/enchantment-02-mMgLHTLHjjGa206q.htm)|Thundering Dominance|Tronante Dominancia|modificada|
|[enchantment-02-tlSE7Ly8vi1Dgddv.htm](spells/enchantment-02-tlSE7Ly8vi1Dgddv.htm)|Hideous Laughter|Terribles carcajadas|modificada|
|[enchantment-02-UirEIHILQgip87qv.htm](spells/enchantment-02-UirEIHILQgip87qv.htm)|Charitable Urge|Impulso caritativo|modificada|
|[enchantment-02-VJW3IzDpAFio7uls.htm](spells/enchantment-02-VJW3IzDpAFio7uls.htm)|Impart Empathy|Impart Empathy|modificada|
|[enchantment-02-yhz9fF69uwRhnHix.htm](spells/enchantment-02-yhz9fF69uwRhnHix.htm)|Animal Messenger|Mensajero animal|modificada|
|[enchantment-03-1xLVcA8Y1onw7toT.htm](spells/enchantment-03-1xLVcA8Y1onw7toT.htm)|Dirge of Doom|Endecha de perdición|modificada|
|[enchantment-03-3x6eUCm17n6ROzUa.htm](spells/enchantment-03-3x6eUCm17n6ROzUa.htm)|Crisis of Faith|Crisis de fe|modificada|
|[enchantment-03-aewxsale5xWEPKLk.htm](spells/enchantment-03-aewxsale5xWEPKLk.htm)|Zone of Truth|Zona de verdad|modificada|
|[enchantment-03-czO0wbT1i320gcu9.htm](spells/enchantment-03-czO0wbT1i320gcu9.htm)|Roaring Applause|Roaring Applause|modificada|
|[enchantment-03-DCQHaLrYXMI37dvW.htm](spells/enchantment-03-DCQHaLrYXMI37dvW.htm)|Paralyze|Paralizar|modificada|
|[enchantment-03-fPlFRu4dp09qJs3K.htm](spells/enchantment-03-fPlFRu4dp09qJs3K.htm)|Mind of Menace|Mente de Amenaza|modificada|
|[enchantment-03-h5cMTygLpjY3IEF0.htm](spells/enchantment-03-h5cMTygLpjY3IEF0.htm)|Sudden Recollection|Recuerdo Repentino|modificada|
|[enchantment-03-IihxWhRfpsBgQ5jS.htm](spells/enchantment-03-IihxWhRfpsBgQ5jS.htm)|Enthrall|Cautivar|modificada|
|[enchantment-03-KqvqNAfGIE5a9wSv.htm](spells/enchantment-03-KqvqNAfGIE5a9wSv.htm)|Heroism|Heroísmo|modificada|
|[enchantment-03-lONs4r14LEBZRLLw.htm](spells/enchantment-03-lONs4r14LEBZRLLw.htm)|Shift Blame|Cambiante Blame|modificada|
|[enchantment-03-LTUaK3smfm5eDiFK.htm](spells/enchantment-03-LTUaK3smfm5eDiFK.htm)|Song of Marching|Song of Marching|modificada|
|[enchantment-03-mBojKJatf9PTYC38.htm](spells/enchantment-03-mBojKJatf9PTYC38.htm)|Fey Disappearance|Desaparición feérica|modificada|
|[enchantment-03-nplNt08TvokZUxtR.htm](spells/enchantment-03-nplNt08TvokZUxtR.htm)|Agonizing Despair|Desesperación agónica|modificada|
|[enchantment-03-oYBIs8MICYkFwtXD.htm](spells/enchantment-03-oYBIs8MICYkFwtXD.htm)|Infectious Ennui|Ennui Infeccioso|modificada|
|[enchantment-03-Q56HLIHVKY6bC5W3.htm](spells/enchantment-03-Q56HLIHVKY6bC5W3.htm)|Blinding Beauty|Belleza cegadora|modificada|
|[enchantment-03-Q690d3mw3TUrKX7E.htm](spells/enchantment-03-Q690d3mw3TUrKX7E.htm)|Geas|Geas|modificada|
|[enchantment-03-qhJfRnkCRrMI4G1O.htm](spells/enchantment-03-qhJfRnkCRrMI4G1O.htm)|Aberrant Whispers|Susurros aberrantes|modificada|
|[enchantment-03-yM3KTTSAIHhyuP14.htm](spells/enchantment-03-yM3KTTSAIHhyuP14.htm)|Dream Message|Mensaje onírico|modificada|
|[enchantment-04-2jB9eyX7YekwoCvA.htm](spells/enchantment-04-2jB9eyX7YekwoCvA.htm)|Word of Freedom|Palabra de libertad|modificada|
|[enchantment-04-4cEDhvchskRvxSw6.htm](spells/enchantment-04-4cEDhvchskRvxSw6.htm)|Procyal Philosophy|Filosofía Procyal|modificada|
|[enchantment-04-4DaHIgtMBTyxebY3.htm](spells/enchantment-04-4DaHIgtMBTyxebY3.htm)|Commanding Lash|Látigo de mando|modificada|
|[enchantment-04-53Kw9WQtvrEtABEu.htm](spells/enchantment-04-53Kw9WQtvrEtABEu.htm)|Compel True Name|Compel True Name|modificada|
|[enchantment-04-7tR29sQt35NfIWqN.htm](spells/enchantment-04-7tR29sQt35NfIWqN.htm)|Anathematic Reprisal|Represalia anatematica|modificada|
|[enchantment-04-9j35xXKVVtHCh1Pe.htm](spells/enchantment-04-9j35xXKVVtHCh1Pe.htm)|Shaken Confidence|Confianza agitada|modificada|
|[enchantment-04-APDrC83QljsyHenB.htm](spells/enchantment-04-APDrC83QljsyHenB.htm)|Savant's Curse|Savant's Curse|modificada|
|[enchantment-04-CPNlhDQP3aDmLzB3.htm](spells/enchantment-04-CPNlhDQP3aDmLzB3.htm)|Ordained Purpose|Ordained Purpose|modificada|
|[enchantment-04-dARE6VfJ3Uoq5M53.htm](spells/enchantment-04-dARE6VfJ3Uoq5M53.htm)|Daydreamer's Curse|Maldición del soñador|modificada|
|[enchantment-04-e9tVGZhYW37CtbHA.htm](spells/enchantment-04-e9tVGZhYW37CtbHA.htm)|Movanic Glimmer|Movanic Glimmer|modificada|
|[enchantment-04-eCniO6INHNfc9Svr.htm](spells/enchantment-04-eCniO6INHNfc9Svr.htm)|Overflowing Sorrow|Overflowing Sorrow|modificada|
|[enchantment-04-FSu6ZKxr3xdS75wq.htm](spells/enchantment-04-FSu6ZKxr3xdS75wq.htm)|Roar of the Wyrm|Roar of the Wyrm|modificada|
|[enchantment-04-i06YFOtfGfpUvcD7.htm](spells/enchantment-04-i06YFOtfGfpUvcD7.htm)|Wind Whispers|Susurros del Viento|modificada|
|[enchantment-04-i7u6gAdNcyIyyo3h.htm](spells/enchantment-04-i7u6gAdNcyIyyo3h.htm)|Favorable Review|Revisión favorable|modificada|
|[enchantment-04-I8CPe9Pp7GABqOyB.htm](spells/enchantment-04-I8CPe9Pp7GABqOyB.htm)|Zeal for Battle|Celo de batalla|modificada|
|[enchantment-04-J8pL8yTshga8QOk8.htm](spells/enchantment-04-J8pL8yTshga8QOk8.htm)|Delusional Pride|Orgullo delirante|modificada|
|[enchantment-04-jJphHQlENHFlSElH.htm](spells/enchantment-04-jJphHQlENHFlSElH.htm)|Captivating Adoration|Adoración cautivadora|modificada|
|[enchantment-04-JyxTmqjYYn63V5LY.htm](spells/enchantment-04-JyxTmqjYYn63V5LY.htm)|Glibness|Labios|modificada|
|[enchantment-04-kF0rs9mCPvJGfAZE.htm](spells/enchantment-04-kF0rs9mCPvJGfAZE.htm)|Inspire Heroics|Inspirar heroismo|modificada|
|[enchantment-04-KPGGkyBFbKse7KpK.htm](spells/enchantment-04-KPGGkyBFbKse7KpK.htm)|Dread Aura|Aura pavorosa|modificada|
|[enchantment-04-KSAEhNfZyXMO7Z7V.htm](spells/enchantment-04-KSAEhNfZyXMO7Z7V.htm)|Outcast's Curse|La maldición del paria|modificada|
|[enchantment-04-LiGbewa9pO0yjbsY.htm](spells/enchantment-04-LiGbewa9pO0yjbsY.htm)|Confusion|Confusión|modificada|
|[enchantment-04-LX4pCagYLpc9hEji.htm](spells/enchantment-04-LX4pCagYLpc9hEji.htm)|Aromatic Lure|Señuelo Aromático|modificada|
|[enchantment-04-MLdMUOdwSKegOGlo.htm](spells/enchantment-04-MLdMUOdwSKegOGlo.htm)|Dull Ambition|Apagar la ambición|modificada|
|[enchantment-04-mUz3wqbMQ5JQ06M5.htm](spells/enchantment-04-mUz3wqbMQ5JQ06M5.htm)|Infectious Melody|Melodía Infecciosa|modificada|
|[enchantment-04-NOB92Wpn7jXvtyVW.htm](spells/enchantment-04-NOB92Wpn7jXvtyVW.htm)|Competitive Edge|Ventaja competitiva|modificada|
|[enchantment-04-OmhnoYOzFhrp6rXv.htm](spells/enchantment-04-OmhnoYOzFhrp6rXv.htm)|Waking Dream|Sue despierto|modificada|
|[enchantment-04-oXCwHBeDja4e0Mx0.htm](spells/enchantment-04-oXCwHBeDja4e0Mx0.htm)|Touch of the Moon|Toque de la luna|modificada|
|[enchantment-04-pLEhF3f7KRxFo0vp.htm](spells/enchantment-04-pLEhF3f7KRxFo0vp.htm)|Girzanje's March|Marcha de Girzanje|modificada|
|[enchantment-04-PztLrElcZfLwRnEq.htm](spells/enchantment-04-PztLrElcZfLwRnEq.htm)|Dreamer's Call|Llamada del sodor|modificada|
|[enchantment-04-qwlh6aDgi86U3Q7H.htm](spells/enchantment-04-qwlh6aDgi86U3Q7H.htm)|Suggestion|Sugestión|modificada|
|[enchantment-04-S7ylpCJyq0CYkux9.htm](spells/enchantment-04-S7ylpCJyq0CYkux9.htm)|Clownish Curse|Clownish Curse|modificada|
|[enchantment-04-skvgOWNTitLehL0b.htm](spells/enchantment-04-skvgOWNTitLehL0b.htm)|Shared Nightmare|Pesadilla compartida|modificada|
|[enchantment-04-sUvTGELaggrBSgGm.htm](spells/enchantment-04-sUvTGELaggrBSgGm.htm)|Implement of Destruction|Implemento de Destrucción|modificada|
|[enchantment-05-1QMjWGq1DPOQr4VL.htm](spells/enchantment-05-1QMjWGq1DPOQr4VL.htm)|Dread Ambience|Dread Ambience|modificada|
|[enchantment-05-BilnTGuXrof9Dt9D.htm](spells/enchantment-05-BilnTGuXrof9Dt9D.htm)|Synaptic Pulse|Pulso sináptico|modificada|
|[enchantment-05-Dby1eQ7y5dXBWlyc.htm](spells/enchantment-05-Dby1eQ7y5dXBWlyc.htm)|Contagious Idea|Contagious Idea|modificada|
|[enchantment-05-ftZ750SD2iIJKIy3.htm](spells/enchantment-05-ftZ750SD2iIJKIy3.htm)|Glimmer of Charm|Destello de hechizo|modificada|
|[enchantment-05-GaRQlC9Yw1BGKHfN.htm](spells/enchantment-05-GaRQlC9Yw1BGKHfN.htm)|Crushing Despair|Desesperación aplastante|modificada|
|[enchantment-05-GP3wewkQXEPrLxYj.htm](spells/enchantment-05-GP3wewkQXEPrLxYj.htm)|Subconscious Suggestion|Sugestión inconsciente.|modificada|
|[enchantment-05-mm3hZ6jgaJaKK16n.htm](spells/enchantment-05-mm3hZ6jgaJaKK16n.htm)|Litany of Self-Interest|Letanía del interés propio|modificada|
|[enchantment-05-QIjc7Zyej0P3b9v5.htm](spells/enchantment-05-QIjc7Zyej0P3b9v5.htm)|Oblivious Expulsion|Oblivious Expulsion|modificada|
|[enchantment-05-SE3MddYAUyPKABuF.htm](spells/enchantment-05-SE3MddYAUyPKABuF.htm)|Infectious Comedy|Comedia Infecciosa|modificada|
|[enchantment-05-x2LALaHXO7644GQA.htm](spells/enchantment-05-x2LALaHXO7644GQA.htm)|You're Mine|Eres mío|modificada|
|[enchantment-05-y0Vy7iNL3ET8K00C.htm](spells/enchantment-05-y0Vy7iNL3ET8K00C.htm)|Dreaming Potential|Sue en potencia|modificada|
|[enchantment-06-5BbU1V6wGSGbrmRD.htm](spells/enchantment-06-5BbU1V6wGSGbrmRD.htm)|Feeblemind|Debilidad mental|modificada|
|[enchantment-06-GYD0XZ4t3tQq6shc.htm](spells/enchantment-06-GYD0XZ4t3tQq6shc.htm)|Zealous Conviction|Convicción fervorosa|modificada|
|[enchantment-06-M6HsK7W6JMkJXeog.htm](spells/enchantment-06-M6HsK7W6JMkJXeog.htm)|Ideal Mimicry|Ideal Mimicry|modificada|
|[enchantment-06-NhGXgmI3AjkkwnPk.htm](spells/enchantment-06-NhGXgmI3AjkkwnPk.htm)|Blinding Fury|Furia cegadora|modificada|
|[enchantment-06-OsOhx3TGIZ7AhD0P.htm](spells/enchantment-06-OsOhx3TGIZ7AhD0P.htm)|Dominate|Dominar|modificada|
|[enchantment-06-s0SerOrkkUd7SAH9.htm](spells/enchantment-06-s0SerOrkkUd7SAH9.htm)|Bacchanalia|Bacchanalia|modificada|
|[enchantment-07-0JigNJDRwevZOyjI.htm](spells/enchantment-07-0JigNJDRwevZOyjI.htm)|Soothing Ballad|Balada tranquilizadora|modificada|
|[enchantment-07-8kJbiBEjMWG4VUjs.htm](spells/enchantment-07-8kJbiBEjMWG4VUjs.htm)|Warp Mind|Distorsionar la mente|modificada|
|[enchantment-07-gMbrNFKz9NP3TR4s.htm](spells/enchantment-07-gMbrNFKz9NP3TR4s.htm)|Inexhaustible Cynicism|Cinismo Inagotable|modificada|
|[enchantment-07-IQchIYUwbsVTa9Mc.htm](spells/enchantment-07-IQchIYUwbsVTa9Mc.htm)|Allegro|Allegro|modificada|
|[enchantment-07-Vw2CNwlRRKABsuZi.htm](spells/enchantment-07-Vw2CNwlRRKABsuZi.htm)|Entrancing Eyes|Entrancing Eyes|modificada|
|[enchantment-07-Yk3t4ekEiFIoEz9c.htm](spells/enchantment-07-Yk3t4ekEiFIoEz9c.htm)|Power Word Blind|Palabra de poder ceguera|modificada|
|[enchantment-08-7PJSqUeKxTqOVrPk.htm](spells/enchantment-08-7PJSqUeKxTqOVrPk.htm)|Power Word Stun|Palabra de poder aturdidor|modificada|
|[enchantment-08-Jvyy6oVIQsD34MHB.htm](spells/enchantment-08-Jvyy6oVIQsD34MHB.htm)|Uncontrollable Dance|Danza incontrolable|modificada|
|[enchantment-08-KtTGLbLG9nqMbUYL.htm](spells/enchantment-08-KtTGLbLG9nqMbUYL.htm)|Divine Inspiration|Inspiración divina.|modificada|
|[enchantment-08-kWDt0JcKPgX6MvdD.htm](spells/enchantment-08-kWDt0JcKPgX6MvdD.htm)|Burning Blossoms|Burning Blossoms|modificada|
|[enchantment-08-qlxM7Ik3uUeUIOcv.htm](spells/enchantment-08-qlxM7Ik3uUeUIOcv.htm)|Canticle of Everlasting Grief|Canticle of Everlasting Grief|modificada|
|[enchantment-09-fkDeKktdmbeplYRY.htm](spells/enchantment-09-fkDeKktdmbeplYRY.htm)|Overwhelming Presence|Presencia abrumadora|modificada|
|[enchantment-09-FmNDwqMEjeTEGPrY.htm](spells/enchantment-09-FmNDwqMEjeTEGPrY.htm)|Unfathomable Song|Canción insondable|modificada|
|[enchantment-09-ItZgGznUBhgWBwFG.htm](spells/enchantment-09-ItZgGznUBhgWBwFG.htm)|Telepathic Demand|Exigencia telepática|modificada|
|[enchantment-09-m3lcOFm400lQCUps.htm](spells/enchantment-09-m3lcOFm400lQCUps.htm)|Power Word Kill|Palabra de poder mortal|modificada|
|[enchantment-09-T5Mt4jXFuh14uREv.htm](spells/enchantment-09-T5Mt4jXFuh14uREv.htm)|Divinity Leech|Sanguijuela de la Divinidad|modificada|
|[enchantment-09-Tc5NLaMu71vrGTJQ.htm](spells/enchantment-09-Tc5NLaMu71vrGTJQ.htm)|Nature's Enmity|La enemistad de la Naturaleza|modificada|
|[enchantment-09-xFY9RtDE4DQKlWNR.htm](spells/enchantment-09-xFY9RtDE4DQKlWNR.htm)|Crusade|Cruzada|modificada|
|[enchantment-10-6s0UW4bujggma9TC.htm](spells/enchantment-10-6s0UW4bujggma9TC.htm)|Fabricated Truth|Verdad inventada|modificada|
|[enchantment-10-lyJDBD9OFW11vLyT.htm](spells/enchantment-10-lyJDBD9OFW11vLyT.htm)|Fatal Aria|Aria fatal|modificada|
|[enchantment-10-Um0aaJotqMKGmAlR.htm](spells/enchantment-10-Um0aaJotqMKGmAlR.htm)|Pied Piping|Pied Piping|modificada|
|[evocation-01-0H1ozccQGGFLUwFI.htm](spells/evocation-01-0H1ozccQGGFLUwFI.htm)|Cry of Destruction|Grito de destrucción|modificada|
|[evocation-01-0JUOgbbFCapp3HlW.htm](spells/evocation-01-0JUOgbbFCapp3HlW.htm)|Elemental Toss|Lanzamiento elemental|modificada|
|[evocation-01-1meVElIu1CEVYWkv.htm](spells/evocation-01-1meVElIu1CEVYWkv.htm)|Noxious Vapors|Vapores nocivos|modificada|
|[evocation-01-5gophZ4AOKW4VW27.htm](spells/evocation-01-5gophZ4AOKW4VW27.htm)|Phase Bolt|Phase Bolt|modificada|
|[evocation-01-5QD75UbyB5EiG3yz.htm](spells/evocation-01-5QD75UbyB5EiG3yz.htm)|Scorching Blast|Scorching Blast|modificada|
|[evocation-01-60sgbuMWN0268dB7.htm](spells/evocation-01-60sgbuMWN0268dB7.htm)|Telekinetic Projectile|Proyectil telecinético|modificada|
|[evocation-01-6UafOE1ZUbHamsZJ.htm](spells/evocation-01-6UafOE1ZUbHamsZJ.htm)|Dim the Light|Atenuar la luz|modificada|
|[evocation-01-6wLY5LnehCo3tHlr.htm](spells/evocation-01-6wLY5LnehCo3tHlr.htm)|Aqueous Blast|Ráfaga acuosa|modificada|
|[evocation-01-7CUgqHunmHfW2lC5.htm](spells/evocation-01-7CUgqHunmHfW2lC5.htm)|Parch|Parch|modificada|
|[evocation-01-8lZhUreL1bRk1v4Z.htm](spells/evocation-01-8lZhUreL1bRk1v4Z.htm)|Briny Bolt|Briny Bolt|modificada|
|[evocation-01-8TQiFzGf4feoHeH0.htm](spells/evocation-01-8TQiFzGf4feoHeH0.htm)|Chilling Spray|Rociada gélida|modificada|
|[evocation-01-8Uzc9WKqRr755S5d.htm](spells/evocation-01-8Uzc9WKqRr755S5d.htm)|Horizon Thunder Sphere|Horizonte Tronante Esfera|modificada|
|[evocation-01-9Ga1AOQdHKYXUY4O.htm](spells/evocation-01-9Ga1AOQdHKYXUY4O.htm)|Purifying Icicle|Carámbano Purificador|modificada|
|[evocation-01-9mPEoOPN0AMuixIv.htm](spells/evocation-01-9mPEoOPN0AMuixIv.htm)|Force Fang|Colmillo de Fuerza|modificada|
|[evocation-01-AspA30tzKCHFWRf0.htm](spells/evocation-01-AspA30tzKCHFWRf0.htm)|Incendiary Aura|Aura Incendiaria|modificada|
|[evocation-01-b5BQbwmuBhgPXTyi.htm](spells/evocation-01-b5BQbwmuBhgPXTyi.htm)|Haunting Hymn|Haunting Hymn|modificada|
|[evocation-01-BItahht2hEHvR9Bt.htm](spells/evocation-01-BItahht2hEHvR9Bt.htm)|Buzzing Bites|Muerdemuerde|modificada|
|[evocation-01-bSDTWUIvgXkBaEv8.htm](spells/evocation-01-bSDTWUIvgXkBaEv8.htm)|Hand of the Apprentice|Mano del aprendiz|modificada|
|[evocation-01-cE7PRAX8Up7fmYef.htm](spells/evocation-01-cE7PRAX8Up7fmYef.htm)|Shroud of Night|Sudario de la Noche|modificada|
|[evocation-01-D7ZEhTNIDWDLC2J4.htm](spells/evocation-01-D7ZEhTNIDWDLC2J4.htm)|Puff of Poison|Puff of Poison|modificada|
|[evocation-01-dDiOnjcsBFbAvP6t.htm](spells/evocation-01-dDiOnjcsBFbAvP6t.htm)|Gale Blast|Gale Blast|modificada|
|[evocation-01-DeNz6eAUlE0IE9U3.htm](spells/evocation-01-DeNz6eAUlE0IE9U3.htm)|Winter Bolt|Winter Bolt|modificada|
|[evocation-01-dgCH2E0gMLMUgyFl.htm](spells/evocation-01-dgCH2E0gMLMUgyFl.htm)|Shockwave|Shockwave|modificada|
|[evocation-01-eSL5hVT9gXrnRLtd.htm](spells/evocation-01-eSL5hVT9gXrnRLtd.htm)|Spout|Caño|modificada|
|[evocation-01-EzB9i7R6aBRAtJCh.htm](spells/evocation-01-EzB9i7R6aBRAtJCh.htm)|Tempest Touch|Tempest Touch|modificada|
|[evocation-01-F1fQC9SarAkjNxpP.htm](spells/evocation-01-F1fQC9SarAkjNxpP.htm)|Gravitational Pull|Gravitational Pull|modificada|
|[evocation-01-f9uqHnNBMU0774SF.htm](spells/evocation-01-f9uqHnNBMU0774SF.htm)|Elemental Betrayal|Traición elemental|modificada|
|[evocation-01-fprqWKUc0jnMIyGU.htm](spells/evocation-01-fprqWKUc0jnMIyGU.htm)|Airburst|Airburst|modificada|
|[evocation-01-g1eY1vN44mgluE33.htm](spells/evocation-01-g1eY1vN44mgluE33.htm)|Charged Javelin|Javelin cargado|modificada|
|[evocation-01-g8QqHpv2CWDwmIm1.htm](spells/evocation-01-g8QqHpv2CWDwmIm1.htm)|Gust of Wind|Ráfaga de viento|modificada|
|[evocation-01-gISYsBFby1TiXfBt.htm](spells/evocation-01-gISYsBFby1TiXfBt.htm)|Acid Splash|Salpicadura de ácido|modificada|
|[evocation-01-gKKqvLohtrSJj3BM.htm](spells/evocation-01-gKKqvLohtrSJj3BM.htm)|Magic Missile|Proyectil mágico|modificada|
|[evocation-01-gYjPm7YwGtEa1oxh.htm](spells/evocation-01-gYjPm7YwGtEa1oxh.htm)|Ray of Frost|Rayo de escarcha|modificada|
|[evocation-01-HGmBY8KjgLV97nUp.htm](spells/evocation-01-HGmBY8KjgLV97nUp.htm)|Scouring Sand|Arena abrasiva|modificada|
|[evocation-01-ho1jSoYKrHUNnM90.htm](spells/evocation-01-ho1jSoYKrHUNnM90.htm)|Tempest Surge|Oleaje de tempestad|modificada|
|[evocation-01-HStu2Yhw3iQER9tY.htm](spells/evocation-01-HStu2Yhw3iQER9tY.htm)|Boost Eidolon|Boost Eidolon|modificada|
|[evocation-01-Hu38hoAUSYeFpkVa.htm](spells/evocation-01-Hu38hoAUSYeFpkVa.htm)|Force Bolt|Rayo de fuerza|modificada|
|[evocation-01-iAnpxrLaBU4V6Sej.htm](spells/evocation-01-iAnpxrLaBU4V6Sej.htm)|Tidal Surge|Oleaje de las Mareas|modificada|
|[evocation-01-IWUe32Y5k2QFd7YQ.htm](spells/evocation-01-IWUe32Y5k2QFd7YQ.htm)|Gravity Weapon|Arma de gravedad|modificada|
|[evocation-01-jfVCuOpzC6mUrf6f.htm](spells/evocation-01-jfVCuOpzC6mUrf6f.htm)|Hydraulic Push|Empujón hidráulico|modificada|
|[evocation-01-KAapTzGKJMbMQCL1.htm](spells/evocation-01-KAapTzGKJMbMQCL1.htm)|Spinning Staff|Bastón giratorio|modificada|
|[evocation-01-kBhaPuzLUSwS6vVf.htm](spells/evocation-01-kBhaPuzLUSwS6vVf.htm)|Electric Arc|Arco eléctrico|modificada|
|[evocation-01-kJKSLfCgqxmN2FY8.htm](spells/evocation-01-kJKSLfCgqxmN2FY8.htm)|Personal Rain Cloud|Nube de lluvia personal|modificada|
|[evocation-01-kl2q6JvBZwed4B6v.htm](spells/evocation-01-kl2q6JvBZwed4B6v.htm)|Dancing Lights|Luces Danzantes|modificada|
|[evocation-01-mlNYROcFrUF8nFgk.htm](spells/evocation-01-mlNYROcFrUF8nFgk.htm)|Spray of Stars|Spray de Estrellas|modificada|
|[evocation-01-O9w7r4BKgPogYDDe.htm](spells/evocation-01-O9w7r4BKgPogYDDe.htm)|Produce Flame|Produce Flamígera|modificada|
|[evocation-01-oJKZi8OQgmVXHOc0.htm](spells/evocation-01-oJKZi8OQgmVXHOc0.htm)|Fire Ray|Rayo de fuego|modificada|
|[evocation-01-pRKaEXnjGJXbPHPC.htm](spells/evocation-01-pRKaEXnjGJXbPHPC.htm)|Hurtling Stone|Pedrada|modificada|
|[evocation-01-pwzdSlJgYqN7bs2w.htm](spells/evocation-01-pwzdSlJgYqN7bs2w.htm)|Mage Hand|Mano del mago|modificada|
|[evocation-01-QozxgBbcmktLKdBs.htm](spells/evocation-01-QozxgBbcmktLKdBs.htm)|Updraft|Updraft|modificada|
|[evocation-01-Qw3fnUlaUbnn7ipC.htm](spells/evocation-01-Qw3fnUlaUbnn7ipC.htm)|Prestidigitation|Prestidigitación|modificada|
|[evocation-01-qwZBXN6zBoB9BHXE.htm](spells/evocation-01-qwZBXN6zBoB9BHXE.htm)|Divine Lance|Lanza divina|modificada|
|[evocation-01-r3NeUnsgt9mS03Sn.htm](spells/evocation-01-r3NeUnsgt9mS03Sn.htm)|Shocking Grasp|Electrizante Agarre|modificada|
|[evocation-01-r82rqcm0MmGaBFkM.htm](spells/evocation-01-r82rqcm0MmGaBFkM.htm)|Thunderous Strike|Golpe Tronante|modificada|
|[evocation-01-Rn2LkoSq1XhLsODV.htm](spells/evocation-01-Rn2LkoSq1XhLsODV.htm)|Pummeling Rubble|Escombros aporreantes|modificada|
|[evocation-01-rnNGALRtsjspFTws.htm](spells/evocation-01-rnNGALRtsjspFTws.htm)|Acidic Burst|Acidic Burst|modificada|
|[evocation-01-S6Kkk15MWGqzC00a.htm](spells/evocation-01-S6Kkk15MWGqzC00a.htm)|Draconic Barrage|Andanada dracónica|modificada|
|[evocation-01-s8gTmnNMg4H4bHEF.htm](spells/evocation-01-s8gTmnNMg4H4bHEF.htm)|Gritty Wheeze|Resoplido arenoso|modificada|
|[evocation-01-SE0fbgBj7atuukdv.htm](spells/evocation-01-SE0fbgBj7atuukdv.htm)|Cloak of Shadow|Capa de Sombra|modificada|
|[evocation-01-sPHcuLIKj9SDaDAD.htm](spells/evocation-01-sPHcuLIKj9SDaDAD.htm)|Kinetic Ram|Kinetic Ram|modificada|
|[evocation-01-SuBtUJiU6DbSJYIw.htm](spells/evocation-01-SuBtUJiU6DbSJYIw.htm)|Moonbeam|Rayo lunar|modificada|
|[evocation-01-sUr5KCpeE6AXfvPp.htm](spells/evocation-01-sUr5KCpeE6AXfvPp.htm)|Imaginary Weapon|Arma Imaginaria|modificada|
|[evocation-01-t4hGPdh6vAEgBFgZ.htm](spells/evocation-01-t4hGPdh6vAEgBFgZ.htm)|Victory Cry|Grito de Victoria|modificada|
|[evocation-01-TjrZGl8z2INgf3vi.htm](spells/evocation-01-TjrZGl8z2INgf3vi.htm)|Friendfetch|Friendfetch|modificada|
|[evocation-01-TSyFZNAbqfkRrcq0.htm](spells/evocation-01-TSyFZNAbqfkRrcq0.htm)|Concordant Choir|Coro Concordante|modificada|
|[evocation-01-vtCCVe869shMCJMj.htm](spells/evocation-01-vtCCVe869shMCJMj.htm)|Echoing Weapon|Echoing Weapon|modificada|
|[evocation-01-W37iBXLsY2trJ1rS.htm](spells/evocation-01-W37iBXLsY2trJ1rS.htm)|Weapon Surge|Oleaje de arma|modificada|
|[evocation-01-W6QlRwQLPoBSw6PZ.htm](spells/evocation-01-W6QlRwQLPoBSw6PZ.htm)|Snowball|Bola de nieve|modificada|
|[evocation-01-WBmvzNDfpwka3qT4.htm](spells/evocation-01-WBmvzNDfpwka3qT4.htm)|Light|Luz|modificada|
|[evocation-01-xGFAGlonNySXrunq.htm](spells/evocation-01-xGFAGlonNySXrunq.htm)|Buffeting Winds|Buffeting Winds|modificada|
|[evocation-01-y6rAdMK6EFlV6U0t.htm](spells/evocation-01-y6rAdMK6EFlV6U0t.htm)|Burning Hands|Manos ardientes|modificada|
|[evocation-01-yafsV0ni7rFgqJBj.htm](spells/evocation-01-yafsV0ni7rFgqJBj.htm)|Biting Words|Muerdemuerde Palabras|modificada|
|[evocation-01-yyz029C9eqfY38PT.htm](spells/evocation-01-yyz029C9eqfY38PT.htm)|Telekinetic Rend|Telekinetic Rasgadura|modificada|
|[evocation-01-zA0jNIBRgLsyTpbm.htm](spells/evocation-01-zA0jNIBRgLsyTpbm.htm)|Scatter Scree|Scatter Scree|modificada|
|[evocation-01-zdb8cjOIDVKYMWdr.htm](spells/evocation-01-zdb8cjOIDVKYMWdr.htm)|Penumbral Shroud|Penumbral Shroud|modificada|
|[evocation-01-zul5cBTfr7NXHBZf.htm](spells/evocation-01-zul5cBTfr7NXHBZf.htm)|Dazzling Flash|Fogonazo cegador|modificada|
|[evocation-02-0qaqksrGGDj74HXE.htm](spells/evocation-02-0qaqksrGGDj74HXE.htm)|Glitterdust|Partículas rutilantes|modificada|
|[evocation-02-1xbFBQDRs0hT5xZ9.htm](spells/evocation-02-1xbFBQDRs0hT5xZ9.htm)|Shatter|Estallar|modificada|
|[evocation-02-2ZdHjnpEQJuqOYSG.htm](spells/evocation-02-2ZdHjnpEQJuqOYSG.htm)|Flaming Sphere|Esfera flamígera|modificada|
|[evocation-02-3GXU3ugrwLv0P7AH.htm](spells/evocation-02-3GXU3ugrwLv0P7AH.htm)|Ignite Fireworks|Encender fuegos artificiales|modificada|
|[evocation-02-3q9tBMWsWQKlXPPJ.htm](spells/evocation-02-3q9tBMWsWQKlXPPJ.htm)|Flame Wisp|Flame Wisp|modificada|
|[evocation-02-4GE2ZdODgIQtg51c.htm](spells/evocation-02-4GE2ZdODgIQtg51c.htm)|Darkness|Oscuridad|modificada|
|[evocation-02-4Sg6ZngswhphxiBD.htm](spells/evocation-02-4Sg6ZngswhphxiBD.htm)|Swallow Light|Luz de golondrina|modificada|
|[evocation-02-7dKtMehJ6YrXwJLR.htm](spells/evocation-02-7dKtMehJ6YrXwJLR.htm)|Magnetic Attraction|Atracción Magnética|modificada|
|[evocation-02-8Hw3P6eurX1MYm7L.htm](spells/evocation-02-8Hw3P6eurX1MYm7L.htm)|Dancing Shield|Escudo Danzante|modificada|
|[evocation-02-AsKLseOo8hwv5Jha.htm](spells/evocation-02-AsKLseOo8hwv5Jha.htm)|Invoke the Crimson Oath|Invocar el Juramento Carmesí|modificada|
|[evocation-02-cB17yFc9456Pyfec.htm](spells/evocation-02-cB17yFc9456Pyfec.htm)|Vomit Swarm|Vomitar plaga|modificada|
|[evocation-02-cf7Jkm39uEjUFtHt.htm](spells/evocation-02-cf7Jkm39uEjUFtHt.htm)|Sea Surge|Oleaje Marino|modificada|
|[evocation-02-Dbd5W6G8U2vzWolN.htm](spells/evocation-02-Dbd5W6G8U2vzWolN.htm)|Consecrate|Consagrar|modificada|
|[evocation-02-f8hRqLJaxBVhF1u0.htm](spells/evocation-02-f8hRqLJaxBVhF1u0.htm)|Acid Arrow|Flecha ácida|modificada|
|[evocation-02-Fq9yCbqI2RDt6Orw.htm](spells/evocation-02-Fq9yCbqI2RDt6Orw.htm)|Spiritual Weapon|Arma espiritual|modificada|
|[evocation-02-HRb2doyaLtaoCfi3.htm](spells/evocation-02-HRb2doyaLtaoCfi3.htm)|Faerie Fire|Fuego feérico|modificada|
|[evocation-02-kMffb6SvhCBMMI4k.htm](spells/evocation-02-kMffb6SvhCBMMI4k.htm)|Elemental Zone|Zona elemental|modificada|
|[evocation-02-mrDi3v933gsmnw25.htm](spells/evocation-02-mrDi3v933gsmnw25.htm)|Telekinetic Maneuver|Maniobra telecinética|modificada|
|[evocation-02-pMTltbI3S3UIuFaR.htm](spells/evocation-02-pMTltbI3S3UIuFaR.htm)|Sun Blade|Sun Blade|modificada|
|[evocation-02-Popa5umI3H33levx.htm](spells/evocation-02-Popa5umI3H33levx.htm)|Rime Slick|Rime Resbaladiza|modificada|
|[evocation-02-SspI4ijjR7N7r4Cc.htm](spells/evocation-02-SspI4ijjR7N7r4Cc.htm)|Bralani Referendum|Referéndum Bralani|modificada|
|[evocation-02-v3vFzGazNSFEDdRB.htm](spells/evocation-02-v3vFzGazNSFEDdRB.htm)|Radiant Field|Radiant Field|modificada|
|[evocation-02-v4KzRPol5XQOOmk0.htm](spells/evocation-02-v4KzRPol5XQOOmk0.htm)|Heat Metal|Calentar metal|modificada|
|[evocation-02-WqPhJNzLa8vSjrH6.htm](spells/evocation-02-WqPhJNzLa8vSjrH6.htm)|Animated Assault|Asalto animado|modificada|
|[evocation-02-Wt94cw03L77sbud7.htm](spells/evocation-02-Wt94cw03L77sbud7.htm)|Breath of Drought|Aliento de sequía|modificada|
|[evocation-02-wzLkNU3AAqOSKFPR.htm](spells/evocation-02-wzLkNU3AAqOSKFPR.htm)|Sound Burst|Explosión de sonido|modificada|
|[evocation-02-x0rWq0wS06dns4G2.htm](spells/evocation-02-x0rWq0wS06dns4G2.htm)|Final Sacrifice|Sacrificio final|modificada|
|[evocation-02-ynm8JIU3sc3qUMpa.htm](spells/evocation-02-ynm8JIU3sc3qUMpa.htm)|Continual Flame|Flamígera continua|modificada|
|[evocation-02-zK0e9d9DSnxC4eAD.htm](spells/evocation-02-zK0e9d9DSnxC4eAD.htm)|Sudden Bolt|Sudden Bolt|modificada|
|[evocation-02-ZxHC7V7HtjUsB8zH.htm](spells/evocation-02-ZxHC7V7HtjUsB8zH.htm)|Scorching Ray|Rayo abrasador|modificada|
|[evocation-03-06pzGkKTyPE3tHR8.htm](spells/evocation-03-06pzGkKTyPE3tHR8.htm)|Gravity Well|Pozo de gravedad|modificada|
|[evocation-03-0jWBnIDFpJjJShdQ.htm](spells/evocation-03-0jWBnIDFpJjJShdQ.htm)|Dragon Breath (Blue or Bronze)|Aliento de dragón (azul o bronce).|modificada|
|[evocation-03-1OiPVy1wuoXi6LR5.htm](spells/evocation-03-1OiPVy1wuoXi6LR5.htm)|Firework Blast|Explosión de fuegos artificiales|modificada|
|[evocation-03-3P3M9SysSesALyoT.htm](spells/evocation-03-3P3M9SysSesALyoT.htm)|Dragon Breath (Sky)|Aliento de dragón (cielo)|modificada|
|[evocation-03-57ulIxg3Of2wCbEh.htm](spells/evocation-03-57ulIxg3Of2wCbEh.htm)|Electrified Crystal Ward|Electrified Cristalizar Ward|modificada|
|[evocation-03-6mmEz1UoTHGB7Sy9.htm](spells/evocation-03-6mmEz1UoTHGB7Sy9.htm)|Dragon Breath (Black, Brine or Copper)|Aliento de dragón (negro, salmuera o cobre).|modificada|
|[evocation-03-9AAkVUCwF6WVNNY2.htm](spells/evocation-03-9AAkVUCwF6WVNNY2.htm)|Lightning Bolt|Relámpago|modificada|
|[evocation-03-B8aCUMCHCIMUCEVK.htm](spells/evocation-03-B8aCUMCHCIMUCEVK.htm)|Elemental Motion|Movimiento elemental|modificada|
|[evocation-03-bmDgIbLa5NfkP97J.htm](spells/evocation-03-bmDgIbLa5NfkP97J.htm)|Dragon Breath (Silver or White)|Aliento de dragón (plateado o blanco)|modificada|
|[evocation-03-BqJAOPimCq5uCcEJ.htm](spells/evocation-03-BqJAOPimCq5uCcEJ.htm)|Shatter Mind|Estallar Mente|modificada|
|[evocation-03-DeF63UTmr7rchF60.htm](spells/evocation-03-DeF63UTmr7rchF60.htm)|Wall of Shadow|Muro de Sombra|modificada|
|[evocation-03-DyiD239dNS7RIxZE.htm](spells/evocation-03-DyiD239dNS7RIxZE.htm)|Searing Light|Luz abrasadora|modificada|
|[evocation-03-E80SrXuBdZViPGiH.htm](spells/evocation-03-E80SrXuBdZViPGiH.htm)|Pulverizing Cascade|Cascada de pulverización|modificada|
|[evocation-03-ECApRjIIxD0JogOa.htm](spells/evocation-03-ECApRjIIxD0JogOa.htm)|Stone Lance|Stone Lance|modificada|
|[evocation-03-eIPIZp2FUbFcLNdj.htm](spells/evocation-03-eIPIZp2FUbFcLNdj.htm)|Pillar of Water|Pilar de Agua|modificada|
|[evocation-03-fI20AVwOzJMHXRdo.htm](spells/evocation-03-fI20AVwOzJMHXRdo.htm)|Levitate|Levitar|modificada|
|[evocation-03-fnDBgdEeuzHRupDu.htm](spells/evocation-03-fnDBgdEeuzHRupDu.htm)|Dragon Breath (Umbral)|Aliento de dragón (Umbral)|modificada|
|[evocation-03-HWrNMQENi9WSGbnF.htm](spells/evocation-03-HWrNMQENi9WSGbnF.htm)|Wall of Radiance|Wall of Radiance|modificada|
|[evocation-03-it4ZsAi6XgvGcodc.htm](spells/evocation-03-it4ZsAi6XgvGcodc.htm)|Wall of Wind|Muro de viento|modificada|
|[evocation-03-JcobNl4iE9HmMYtE.htm](spells/evocation-03-JcobNl4iE9HmMYtE.htm)|Dragon Breath|Aliento de dragón|modificada|
|[evocation-03-jupppBeFqhe6LMIb.htm](spells/evocation-03-jupppBeFqhe6LMIb.htm)|Dragon Breath (Green)|Aliento de dragón (Verde)|modificada|
|[evocation-03-k9M0kUhhHHGYquiA.htm](spells/evocation-03-k9M0kUhhHHGYquiA.htm)|Dragon Breath (Cloud)|Aliento de dragón (nube)|modificada|
|[evocation-03-KEkPxx7Sm6Xf4W3s.htm](spells/evocation-03-KEkPxx7Sm6Xf4W3s.htm)|Blazing Dive|Blazing Dive|modificada|
|[evocation-03-kfsv7zSHb3pr7s9v.htm](spells/evocation-03-kfsv7zSHb3pr7s9v.htm)|Dragon Breath (Gold, Magma or Red)|Aliento de dragón (oro, magma o rojo).|modificada|
|[evocation-03-kRsmUlSWhi6PJvZ7.htm](spells/evocation-03-kRsmUlSWhi6PJvZ7.htm)|Angelic Wings|Alas angelicales|modificada|
|[evocation-03-kuZnUNrhXHRYQ2eM.htm](spells/evocation-03-kuZnUNrhXHRYQ2eM.htm)|Eidolon's Wrath|Ira de Eidolon|modificada|
|[evocation-03-kvZCQz4NoMAfjvif.htm](spells/evocation-03-kvZCQz4NoMAfjvif.htm)|Gasping Marsh|Gasping Marsh|modificada|
|[evocation-03-L37RTc7K79OUpZ7X.htm](spells/evocation-03-L37RTc7K79OUpZ7X.htm)|Interstellar Void|Interstellar Void|modificada|
|[evocation-03-LFSwMtQVP05EzlZe.htm](spells/evocation-03-LFSwMtQVP05EzlZe.htm)|Thunderburst|Thunderburst|modificada|
|[evocation-03-LrFUj76CHDBV0vHW.htm](spells/evocation-03-LrFUj76CHDBV0vHW.htm)|Sun's Fury|Sun's Fury|modificada|
|[evocation-03-mtlyAyf30JnIvxVn.htm](spells/evocation-03-mtlyAyf30JnIvxVn.htm)|Moonlight Ray|Moonlight Ray|modificada|
|[evocation-03-N0h3UodJFKdNQw1Y.htm](spells/evocation-03-N0h3UodJFKdNQw1Y.htm)|Dragon Breath (Brass)|Aliento de dragón (latón)|modificada|
|[evocation-03-NeoRkU7C4BIz8fUM.htm](spells/evocation-03-NeoRkU7C4BIz8fUM.htm)|Bracing Tendrils|Bracing Tendrils|modificada|
|[evocation-03-nUSi2B7RhIKjaiXQ.htm](spells/evocation-03-nUSi2B7RhIKjaiXQ.htm)|Astral Rain|Lluvia Astral|modificada|
|[evocation-03-NXm8z4W4YZazE2gn.htm](spells/evocation-03-NXm8z4W4YZazE2gn.htm)|Dragon Breath (Sea)|Aliento de dragón (mar)|modificada|
|[evocation-03-NyxQAazFBdBAqZGX.htm](spells/evocation-03-NyxQAazFBdBAqZGX.htm)|Elemental Annihilation Wave|Onda de Aniquilación Elemental|modificada|
|[evocation-03-oo7YcRC2gcez81PV.htm](spells/evocation-03-oo7YcRC2gcez81PV.htm)|Ki Blast|Explosión de ki|modificada|
|[evocation-03-OTd17oXwJH9qb1cS.htm](spells/evocation-03-OTd17oXwJH9qb1cS.htm)|Incendiary Ashes|Cenizas incendiarias|modificada|
|[evocation-03-Q1OWufw6dUiY8yEI.htm](spells/evocation-03-Q1OWufw6dUiY8yEI.htm)|Shroud of Flame|Sudario de Flamígera|modificada|
|[evocation-03-sRfSBHWHdbIa0aGc.htm](spells/evocation-03-sRfSBHWHdbIa0aGc.htm)|Chilling Darkness|Oscuridad pavorosa|modificada|
|[evocation-03-T4QKmtYPeCgYxVGe.htm](spells/evocation-03-T4QKmtYPeCgYxVGe.htm)|Crashing Wave|Ola rompiente|modificada|
|[evocation-03-t5HAnKSHfSfHAJwH.htm](spells/evocation-03-t5HAnKSHfSfHAJwH.htm)|Dragon Breath (Crystal or Forest)|Aliento de dragón (Cristalizar o Bosque)|modificada|
|[evocation-03-tC9HJ3sfQJFTacrE.htm](spells/evocation-03-tC9HJ3sfQJFTacrE.htm)|Dragon Breath (Sovereign)|Aliento de dragón (Soberano)|modificada|
|[evocation-03-TT9owkeMBXJxcERB.htm](spells/evocation-03-TT9owkeMBXJxcERB.htm)|Unseasonable Squall|Unseasonable Squall|modificada|
|[evocation-03-tvBhGfGcg2fsMOMe.htm](spells/evocation-03-tvBhGfGcg2fsMOMe.htm)|Dragon Breath (Underworld)|Aliento de dragón (Underworld)|modificada|
|[evocation-03-tvO6Kmc2pQve9DC5.htm](spells/evocation-03-tvO6Kmc2pQve9DC5.htm)|Unfolding Wind Rush|Unfolding Wind Embestida|modificada|
|[evocation-03-Tx8OqkBFA2QlaldW.htm](spells/evocation-03-Tx8OqkBFA2QlaldW.htm)|Magnetic Acceleration|Aceleración magnética|modificada|
|[evocation-03-v4QHuVOhFD1JMAqu.htm](spells/evocation-03-v4QHuVOhFD1JMAqu.htm)|Powerful Inhalation|Inhalación Potente|modificada|
|[evocation-03-vhMCd15ZwNJn0zen.htm](spells/evocation-03-vhMCd15ZwNJn0zen.htm)|Malicious Shadow|Sombra Maliciosa|modificada|
|[evocation-03-Wi2HcreCfujKiCvW.htm](spells/evocation-03-Wi2HcreCfujKiCvW.htm)|Whirling Flames|Whirling Flamígera|modificada|
|[evocation-03-X1b9ollVMSLXDN9o.htm](spells/evocation-03-X1b9ollVMSLXDN9o.htm)|Litany against Wrath|Letanía contra la ira|modificada|
|[evocation-03-X4T5RlQBrdpmA35n.htm](spells/evocation-03-X4T5RlQBrdpmA35n.htm)|Entropic Wheel|Rueda entrópica|modificada|
|[evocation-03-ytboJsyZEbE1MLeV.htm](spells/evocation-03-ytboJsyZEbE1MLeV.htm)|Unbreaking Wave Advance|Unbreaking Wave Advance|modificada|
|[evocation-03-Z3kJty995FkrsZRb.htm](spells/evocation-03-Z3kJty995FkrsZRb.htm)|Combustion|Combustión|modificada|
|[evocation-03-ZJOQBqkNErZu1QAa.htm](spells/evocation-03-ZJOQBqkNErZu1QAa.htm)|Wall of Virtue|Muro de la Virtud|modificada|
|[evocation-04-2SYq0ZTsOtJEigFx.htm](spells/evocation-04-2SYq0ZTsOtJEigFx.htm)|Mystic Beacon|Faro místico|modificada|
|[evocation-04-3ySPK8qwNcuESwa0.htm](spells/evocation-04-3ySPK8qwNcuESwa0.htm)|Purifying Veil|Velo Purificador|modificada|
|[evocation-04-5LGDygjRxURUOKGR.htm](spells/evocation-04-5LGDygjRxURUOKGR.htm)|Radiant Beam|Radiant Beam|modificada|
|[evocation-04-7d4DUTDIlzDa8OvX.htm](spells/evocation-04-7d4DUTDIlzDa8OvX.htm)|Destructive Aura|Aura destructiva|modificada|
|[evocation-04-8M03UxGXjYyDFAoy.htm](spells/evocation-04-8M03UxGXjYyDFAoy.htm)|Weapon Storm|Tormenta de armas|modificada|
|[evocation-04-DZ9bzXYqMjAK9TzC.htm](spells/evocation-04-DZ9bzXYqMjAK9TzC.htm)|Holy Cascade|Sagrada Cascada|modificada|
|[evocation-04-Hb8GdAhP0zBCv3zU.htm](spells/evocation-04-Hb8GdAhP0zBCv3zU.htm)|Chromatic Ray|Chromatic Ray|modificada|
|[evocation-04-hVU9msO9yGkxKZ3J.htm](spells/evocation-04-hVU9msO9yGkxKZ3J.htm)|Divine Wrath|Ira divina|modificada|
|[evocation-04-IarZrgCeaiUqOuRu.htm](spells/evocation-04-IarZrgCeaiUqOuRu.htm)|Wall of Fire|Muro de fuego|modificada|
|[evocation-04-K4LXpaBWrGy6jIER.htm](spells/evocation-04-K4LXpaBWrGy6jIER.htm)|Downpour|Aguacero|modificada|
|[evocation-04-KG7amdeXWc7MjGXe.htm](spells/evocation-04-KG7amdeXWc7MjGXe.htm)|Asterism|Asterismo|modificada|
|[evocation-04-kHyjQbibRGPNCixx.htm](spells/evocation-04-kHyjQbibRGPNCixx.htm)|Ice Storm|Tormenta de hielo|modificada|
|[evocation-04-LrhTFHUtSS9ahogL.htm](spells/evocation-04-LrhTFHUtSS9ahogL.htm)|Traveler's Transit|Tránsito del viajero|modificada|
|[evocation-04-n7OgbKme4hNwxVwQ.htm](spells/evocation-04-n7OgbKme4hNwxVwQ.htm)|Draw the Lightning|Draw the Lightning|modificada|
|[evocation-04-NxOYiKCqcuAHVRCj.htm](spells/evocation-04-NxOYiKCqcuAHVRCj.htm)|Transcribe Moment|Transcribe Moment|modificada|
|[evocation-04-oOaEfsdVpgTXRhrY.htm](spells/evocation-04-oOaEfsdVpgTXRhrY.htm)|Runic Impression|Runic Impression|modificada|
|[evocation-04-qTzvVnAMWL05VitC.htm](spells/evocation-04-qTzvVnAMWL05VitC.htm)|Painful Vibrations|Vibraciones dolorosas|modificada|
|[evocation-04-SkarN4VlNxSJSJNw.htm](spells/evocation-04-SkarN4VlNxSJSJNw.htm)|Wild Winds Stance|Posición de los vientos salvajes|modificada|
|[evocation-04-uJXs4M6IeixfPBLc.htm](spells/evocation-04-uJXs4M6IeixfPBLc.htm)|Clinging Shadows Stance|Posición de las Sombras aferradas|modificada|
|[evocation-04-vfHr1N8Rf2bBpdgn.htm](spells/evocation-04-vfHr1N8Rf2bBpdgn.htm)|Elemental Tempest|Tempestad elemental|modificada|
|[evocation-04-VmqdVWCb8zAUCW8S.htm](spells/evocation-04-VmqdVWCb8zAUCW8S.htm)|Debilitating Dichotomy|Debilitación Dicotomﾃｭa|modificada|
|[evocation-04-VUwpDY4Z91s9QCg0.htm](spells/evocation-04-VUwpDY4Z91s9QCg0.htm)|Bottle the Storm|Embotellar la tormenta|modificada|
|[evocation-04-wjJW9hWY5CkkMvY5.htm](spells/evocation-04-wjJW9hWY5CkkMvY5.htm)|Diamond Dust|Polvo de Diamante|modificada|
|[evocation-04-Y3G6Y6EDgCY0s3fq.htm](spells/evocation-04-Y3G6Y6EDgCY0s3fq.htm)|Hydraulic Torrent|Torrente Hidráulico|modificada|
|[evocation-05-3puDanGfpEt6jK5k.htm](spells/evocation-05-3puDanGfpEt6jK5k.htm)|Cone of Cold|Cono de frío|modificada|
|[evocation-05-8TBgEzjZxPaOJOm1.htm](spells/evocation-05-8TBgEzjZxPaOJOm1.htm)|Wronged Monk's Wrath|Wronged Monk's Wrath|modificada|
|[evocation-05-9BGEf9Sv5rgNBCk0.htm](spells/evocation-05-9BGEf9Sv5rgNBCk0.htm)|Dance of Darkness|Danza de la oscuridad|modificada|
|[evocation-05-9bOqFkewRz7Z3HZ5.htm](spells/evocation-05-9bOqFkewRz7Z3HZ5.htm)|Scouring Pulse|Scouring Pulse|modificada|
|[evocation-05-9LHr9SuDLTicdbXs.htm](spells/evocation-05-9LHr9SuDLTicdbXs.htm)|Hellfire Plume|Canal de fuego infernal|modificada|
|[evocation-05-ALuvl9GYawnsZZCx.htm](spells/evocation-05-ALuvl9GYawnsZZCx.htm)|Unfolding Wind Buffet|Buffet de viento desplegable|modificada|
|[evocation-05-bZMwSGc9t5K7uxZV.htm](spells/evocation-05-bZMwSGc9t5K7uxZV.htm)|Redistribute Potential|Redistribuir Potencial|modificada|
|[evocation-05-crF4g9jRN1y84MSD.htm](spells/evocation-05-crF4g9jRN1y84MSD.htm)|Abyssal Wrath|Ira abisal|modificada|
|[evocation-05-E3X2RbzWHCdz7gsk.htm](spells/evocation-05-E3X2RbzWHCdz7gsk.htm)|Flame Strike|Golpe flamígera|modificada|
|[evocation-05-gasOL6a0WhOhUsgL.htm](spells/evocation-05-gasOL6a0WhOhUsgL.htm)|Etheric Shards|Fragmentos Etéricos|modificada|
|[evocation-05-HMTloW1hvRFJ5Z2D.htm](spells/evocation-05-HMTloW1hvRFJ5Z2D.htm)|Consuming Darkness|Consumiendo oscuridad|modificada|
|[evocation-05-i88Vi47PDDoP6gQv.htm](spells/evocation-05-i88Vi47PDDoP6gQv.htm)|Geyser|Geyser|modificada|
|[evocation-05-Ifc2b6bNVdjKV7Si.htm](spells/evocation-05-Ifc2b6bNVdjKV7Si.htm)|Stormburst|Stormburst|modificada|
|[evocation-05-IqJ9URobmJ9L9UBG.htm](spells/evocation-05-IqJ9URobmJ9L9UBG.htm)|Shadow Blast|Estallido de sombra|modificada|
|[evocation-05-JyT346VmGtRLsDnV.htm](spells/evocation-05-JyT346VmGtRLsDnV.htm)|Lightning Storm|Tormenta de relámpagos|modificada|
|[evocation-05-OYOCWWefZPEfp8Nl.htm](spells/evocation-05-OYOCWWefZPEfp8Nl.htm)|Blazing Fissure|Blazing Fissure|modificada|
|[evocation-05-Qlp8G3knwLGhAxQ0.htm](spells/evocation-05-Qlp8G3knwLGhAxQ0.htm)|Elemental Blast|Explión elemental|modificada|
|[evocation-05-R5FHRv7VqyRnxg2t.htm](spells/evocation-05-R5FHRv7VqyRnxg2t.htm)|Wall of Ice|Muro de hielo|modificada|
|[evocation-05-rMGkk23qWvxTmgA8.htm](spells/evocation-05-rMGkk23qWvxTmgA8.htm)|Forceful Hand|Mano vigorosa|modificada|
|[evocation-05-TLqMFgCewxuricJw.htm](spells/evocation-05-TLqMFgCewxuricJw.htm)|Repelling Pulse|Pulso Repelente|modificada|
|[evocation-05-tpLTLbJUrYcMWGld.htm](spells/evocation-05-tpLTLbJUrYcMWGld.htm)|Telekinetic Haul|Carga telecinética|modificada|
|[evocation-05-ViqzVEprQVzCXZ9f.htm](spells/evocation-05-ViqzVEprQVzCXZ9f.htm)|Dancing Blade|Danzante Blade|modificada|
|[evocation-05-zCLyETFoPqCQXLVy.htm](spells/evocation-05-zCLyETFoPqCQXLVy.htm)|Flowing Strike|Golpe Fluido|modificada|
|[evocation-05-zfn5RqAdF63neqpP.htm](spells/evocation-05-zfn5RqAdF63neqpP.htm)|Control Water|Controlar las aguas|modificada|
|[evocation-05-zoY0fQYTF1NzezTg.htm](spells/evocation-05-zoY0fQYTF1NzezTg.htm)|Steal the Sky|Sustraer el cielo|modificada|
|[evocation-05-ZW8ovbu1etdfMre3.htm](spells/evocation-05-ZW8ovbu1etdfMre3.htm)|Acid Storm|Tormenta de ácido|modificada|
|[evocation-06-7Iela4GgVeO3LfAo.htm](spells/evocation-06-7Iela4GgVeO3LfAo.htm)|Wall of Force|Muro de fuerza|modificada|
|[evocation-06-bynT1UKaDqr8dLNM.htm](spells/evocation-06-bynT1UKaDqr8dLNM.htm)|Flaming Fusillade|Flamígera Fusilada|modificada|
|[evocation-06-Gn5deIoobHpd3SiR.htm](spells/evocation-06-Gn5deIoobHpd3SiR.htm)|Flame Vortex|Vórtice de Flamígera|modificada|
|[evocation-06-kuoYff1csM5eAcAP.htm](spells/evocation-06-kuoYff1csM5eAcAP.htm)|Fire Seeds|Semillas de fuego|modificada|
|[evocation-06-mtxMpGWpwwWSbySj.htm](spells/evocation-06-mtxMpGWpwwWSbySj.htm)|For Love, For Lightning|For Love, For Lightning|modificada|
|[evocation-06-P1iqzEIidRagUs7W.htm](spells/evocation-06-P1iqzEIidRagUs7W.htm)|Zero Gravity|Gravedad Cero|modificada|
|[evocation-06-peCF6VArm8urfwxZ.htm](spells/evocation-06-peCF6VArm8urfwxZ.htm)|Blade Barrier|Barrera de cuchillas|modificada|
|[evocation-06-qGWORxQ0aSsH2taf.htm](spells/evocation-06-qGWORxQ0aSsH2taf.htm)|Suffocate|Asfixiar|modificada|
|[evocation-06-r7ihOgKv19eJQnik.htm](spells/evocation-06-r7ihOgKv19eJQnik.htm)|Disintegrate|Desintegrar|modificada|
|[evocation-06-TDNlDWbYb58Y55Da.htm](spells/evocation-06-TDNlDWbYb58Y55Da.htm)|Chain Lightning|Cadena de relámpagos|modificada|
|[evocation-06-uhEjKSFdEhXzszh6.htm](spells/evocation-06-uhEjKSFdEhXzszh6.htm)|Poltergeist's Fury|Poltergeist's Fury|modificada|
|[evocation-06-YIMampGpij4Y30yE.htm](spells/evocation-06-YIMampGpij4Y30yE.htm)|Stone Tell|Piedra parlante|modificada|
|[evocation-07-37ESlJzUvVbOudOT.htm](spells/evocation-07-37ESlJzUvVbOudOT.htm)|Reverse Gravity|Invertir la gravedad|modificada|
|[evocation-07-a3aQxCpoj1q1NQxC.htm](spells/evocation-07-a3aQxCpoj1q1NQxC.htm)|Sunburst|Explosión solar|modificada|
|[evocation-07-d6o52BnjViNz7Gub.htm](spells/evocation-07-d6o52BnjViNz7Gub.htm)|Prismatic Spray|Rociada prismática|modificada|
|[evocation-07-EgkypvUZIZkx1UlQ.htm](spells/evocation-07-EgkypvUZIZkx1UlQ.htm)|Blightburn Blast|Blightburn Blast|modificada|
|[evocation-07-HES5jvGiNZZnJycK.htm](spells/evocation-07-HES5jvGiNZZnJycK.htm)|Force Cage|Jaula de fuerza|modificada|
|[evocation-07-n3b3pDmA6L5YRTyq.htm](spells/evocation-07-n3b3pDmA6L5YRTyq.htm)|Litany of Righteousness|Letanía de rectitud|modificada|
|[evocation-07-n8eEXXAtguoErW0y.htm](spells/evocation-07-n8eEXXAtguoErW0y.htm)|Shadow's Web|Telara de Sombra|modificada|
|[evocation-07-O7ZEqWjwdKyo2CUv.htm](spells/evocation-07-O7ZEqWjwdKyo2CUv.htm)|Volcanic Eruption|Erupción volcánica|modificada|
|[evocation-07-oahqARSgOGDRybBQ.htm](spells/evocation-07-oahqARSgOGDRybBQ.htm)|Control Sand|Arena de control|modificada|
|[evocation-07-sFwoKj0TsacsmoWj.htm](spells/evocation-07-sFwoKj0TsacsmoWj.htm)|Darklight|Darklight|modificada|
|[evocation-07-Shiuhdb2nO6Qgk3k.htm](spells/evocation-07-Shiuhdb2nO6Qgk3k.htm)|Moonburst|Moonburst|modificada|
|[evocation-07-sX2o0HH4RjJDAZ8C.htm](spells/evocation-07-sX2o0HH4RjJDAZ8C.htm)|Divine Decree|Decreto divino|modificada|
|[evocation-07-tYP8unoR0a5Dq9EA.htm](spells/evocation-07-tYP8unoR0a5Dq9EA.htm)|Litany of Depravity|Letanía de la Depravación|modificada|
|[evocation-07-uc4I1diSSX6XYzb3.htm](spells/evocation-07-uc4I1diSSX6XYzb3.htm)|Telekinetic Bombardment|Telekinetic Bombardment|modificada|
|[evocation-07-vFkz0gVBUT2gGnm1.htm](spells/evocation-07-vFkz0gVBUT2gGnm1.htm)|Frigid Flurry|Frigid Flurry|modificada|
|[evocation-07-x9RIFhquazom4p02.htm](spells/evocation-07-x9RIFhquazom4p02.htm)|Deity's Strike|Golpe de la Deidad|modificada|
|[evocation-07-XnpRRUVAVoY6z88H.htm](spells/evocation-07-XnpRRUVAVoY6z88H.htm)|Empower Ley Line|Empower Ley Line|modificada|
|[evocation-08-2CNqkt2s2IYkVnv6.htm](spells/evocation-08-2CNqkt2s2IYkVnv6.htm)|Imprisonment|Cautiverio|modificada|
|[evocation-08-8fEfjvC01gNclDKJ.htm](spells/evocation-08-8fEfjvC01gNclDKJ.htm)|Deluge|Diluvio|modificada|
|[evocation-08-8hKW4mWQyLnkHVta.htm](spells/evocation-08-8hKW4mWQyLnkHVta.htm)|Whirlwind|Whirlwind|modificada|
|[evocation-08-BKIet436snMNcnez.htm](spells/evocation-08-BKIet436snMNcnez.htm)|Polar Ray|Rayo polar|modificada|
|[evocation-08-fBoiPXmcIO50OFFR.htm](spells/evocation-08-fBoiPXmcIO50OFFR.htm)|Boil Blood|Hervir la sangre|modificada|
|[evocation-08-vm9O7ne48NM72yrJ.htm](spells/evocation-08-vm9O7ne48NM72yrJ.htm)|Falling Sky|Falling Sky|modificada|
|[evocation-08-wi405lBjPcbF1DeR.htm](spells/evocation-08-wi405lBjPcbF1DeR.htm)|Punishing Winds|Vientos de castigo|modificada|
|[evocation-08-x7SPrsRxGb2Vy2nu.htm](spells/evocation-08-x7SPrsRxGb2Vy2nu.htm)|Earthquake|Terremoto|modificada|
|[evocation-08-XkDCzMIyc0YOjw05.htm](spells/evocation-08-XkDCzMIyc0YOjw05.htm)|Control Weather|Controlar el clima|modificada|
|[evocation-09-4WS7HrFjwNvTn8T2.htm](spells/evocation-09-4WS7HrFjwNvTn8T2.htm)|Implosion|Implosión|modificada|
|[evocation-09-axYqY70kYu2lN20R.htm](spells/evocation-09-axYqY70kYu2lN20R.htm)|Unbreaking Wave Containment|Unbreaking Wave Containment|modificada|
|[evocation-09-jrBa9deU2ULFWvSl.htm](spells/evocation-09-jrBa9deU2ULFWvSl.htm)|Meteor Swarm|Enjambre de meteoritos|modificada|
|[evocation-09-r4HLQcYwB62bTayl.htm](spells/evocation-09-r4HLQcYwB62bTayl.htm)|Storm of Vengeance|Tormenta de venganza|modificada|
|[evocation-09-XjNROFtWnwovhCsq.htm](spells/evocation-09-XjNROFtWnwovhCsq.htm)|Unfolding Wind Crash|Unfolding Wind Crash|modificada|
|[evocation-09-XYhU3Wi94n1RKxTa.htm](spells/evocation-09-XYhU3Wi94n1RKxTa.htm)|Storm Lord|Ser tormentoso|modificada|
|[evocation-09-ZqmP9gijBmK7y8Xy.htm](spells/evocation-09-ZqmP9gijBmK7y8Xy.htm)|Weapon of Judgment|Arma del juicio|modificada|
|[evocation-10-wLIvH0AT1u7oa64N.htm](spells/evocation-10-wLIvH0AT1u7oa64N.htm)|Cataclysm|Cataclismo|modificada|
|[illusion-01-2oH5IufzdESuYxat.htm](spells/illusion-01-2oH5IufzdESuYxat.htm)|Illusory Object|Objeto ilusorio|modificada|
|[illusion-01-4ZGte0i9YbLh4dRi.htm](spells/illusion-01-4ZGte0i9YbLh4dRi.htm)|Item Facade|Fachada de objeto|modificada|
|[illusion-01-6GjJtLJnwC18Y0aZ.htm](spells/illusion-01-6GjJtLJnwC18Y0aZ.htm)|Lift Nature's Caul|Lift Nature's Caul|modificada|
|[illusion-01-atlgGNI1E1Ox3O3a.htm](spells/illusion-01-atlgGNI1E1Ox3O3a.htm)|Ghost Sound|Sonido fantasmal|modificada|
|[illusion-01-fXdADBwxmBsU9xPk.htm](spells/illusion-01-fXdADBwxmBsU9xPk.htm)|Warped Terrain|Terreno distorsionado|modificada|
|[illusion-01-i35dpZFI7jZcRoBo.htm](spells/illusion-01-i35dpZFI7jZcRoBo.htm)|Illusory Disguise|Disfraz ilusorio|modificada|
|[illusion-01-iohRnd4gUCmJlh54.htm](spells/illusion-01-iohRnd4gUCmJlh54.htm)|Lose the Path|Lose the Path|modificada|
|[illusion-01-nX85Brzax9f650aK.htm](spells/illusion-01-nX85Brzax9f650aK.htm)|Invisible Item|Objeto invisible|modificada|
|[illusion-01-PRrZ7anETWPm90YY.htm](spells/illusion-01-PRrZ7anETWPm90YY.htm)|Magic Aura|Aura mágica|modificada|
|[illusion-01-R8bqnYiThB6MYTxD.htm](spells/illusion-01-R8bqnYiThB6MYTxD.htm)|Phantom Pain|Dolor fantasmal|modificada|
|[illusion-01-UKsIOWmMx4hSpafl.htm](spells/illusion-01-UKsIOWmMx4hSpafl.htm)|Color Spray|Rociada de color|modificada|
|[illusion-01-vLzFcIaSXs7YTIqJ.htm](spells/illusion-01-vLzFcIaSXs7YTIqJ.htm)|Message|Mensaje|modificada|
|[illusion-01-W02bHXylIpoXbO4e.htm](spells/illusion-01-W02bHXylIpoXbO4e.htm)|Bullhorn|Megáfono|modificada|
|[illusion-01-X56VdA98VedAfGTU.htm](spells/illusion-01-X56VdA98VedAfGTU.htm)|Thicket of Knives|Thicket of Knives|modificada|
|[illusion-01-xTpp8dHZsNMDm75B.htm](spells/illusion-01-xTpp8dHZsNMDm75B.htm)|Splash of Art|Salpicadura de arte|modificada|
|[illusion-01-yH13KXUK2x093NUv.htm](spells/illusion-01-yH13KXUK2x093NUv.htm)|Face in the Crowd|Rostro en la multitud|modificada|
|[illusion-01-yV7Ouzaoe7DHLESI.htm](spells/illusion-01-yV7Ouzaoe7DHLESI.htm)|Ventriloquism|Ventriloquismía|modificada|
|[illusion-01-ZeftDoh0nFAXBAWY.htm](spells/illusion-01-ZeftDoh0nFAXBAWY.htm)|Appearance of Wealth|Apariencia de riqueza|modificada|
|[illusion-01-znMvKqcRDilIVMwA.htm](spells/illusion-01-znMvKqcRDilIVMwA.htm)|Exchange Image|Imagen de intercambio|modificada|
|[illusion-02-3JG1t3T4mWn6vTke.htm](spells/illusion-02-3JG1t3T4mWn6vTke.htm)|Blur|Contorno borroso|modificada|
|[illusion-02-avT46uIH3xYJPSv4.htm](spells/illusion-02-avT46uIH3xYJPSv4.htm)|Teeth to Terror|Dientes para aterrar|modificada|
|[illusion-02-c2bTWBNO1BYX4Zfg.htm](spells/illusion-02-c2bTWBNO1BYX4Zfg.htm)|Misdirection|Desorientar|modificada|
|[illusion-02-CQWTyDxiVSW4uRn7.htm](spells/illusion-02-CQWTyDxiVSW4uRn7.htm)|Empty Pack|Empty Pack|modificada|
|[illusion-02-EDwakYQTS1t6XHD4.htm](spells/illusion-02-EDwakYQTS1t6XHD4.htm)|Umbral Extraction|Extracción Umbral|modificada|
|[illusion-02-eIQ86FOXK34HiNLs.htm](spells/illusion-02-eIQ86FOXK34HiNLs.htm)|Magic Mouth|Boca mágica|modificada|
|[illusion-02-f8SBoXiXQjlCKqly.htm](spells/illusion-02-f8SBoXiXQjlCKqly.htm)|Illusory Creature|Criatura ilusoria|modificada|
|[illusion-02-gIdDLrbswTV3OBJy.htm](spells/illusion-02-gIdDLrbswTV3OBJy.htm)|Silence|Silencio|modificada|
|[illusion-02-j8vIoIEWElvpwkcI.htm](spells/illusion-02-j8vIoIEWElvpwkcI.htm)|Mirror Image|Imagen múltiple|modificada|
|[illusion-02-L0GoJpHxSD0wRY5k.htm](spells/illusion-02-L0GoJpHxSD0wRY5k.htm)|Phantasmal Treasure|Tesoro fantasmal|modificada|
|[illusion-02-Mkbq9xlAUxHUHyR2.htm](spells/illusion-02-Mkbq9xlAUxHUHyR2.htm)|Paranoia|Paranoia|modificada|
|[illusion-02-q5qmNn144ZJGxnvJ.htm](spells/illusion-02-q5qmNn144ZJGxnvJ.htm)|Befitting Attire|Atuendo Adecuado|modificada|
|[illusion-02-QZ7OHptO1xnwaruq.htm](spells/illusion-02-QZ7OHptO1xnwaruq.htm)|Penumbral Disguise|Disfraz Penumbral|modificada|
|[illusion-02-uopaLE01meX11Mbw.htm](spells/illusion-02-uopaLE01meX11Mbw.htm)|Umbral Mindtheft|Umbral Mindtheft|modificada|
|[illusion-02-WfINystwLv9ohYpS.htm](spells/illusion-02-WfINystwLv9ohYpS.htm)|Phantom Crowd|Phantom Crowd|modificada|
|[illusion-02-XXqE1eY3w3z6xJCB.htm](spells/illusion-02-XXqE1eY3w3z6xJCB.htm)|Invisibility|Invisibilidad|modificada|
|[illusion-03-87zLUuTMmZ9zc7gH.htm](spells/illusion-03-87zLUuTMmZ9zc7gH.htm)|Oneiric Mire|Oneiric Mire|modificada|
|[illusion-03-8cuqRFB3mWBOgy61.htm](spells/illusion-03-8cuqRFB3mWBOgy61.htm)|Shadow Projectile|Proyectil de Sombra|modificada|
|[illusion-03-8MAHUK6jphbME4BR.htm](spells/illusion-03-8MAHUK6jphbME4BR.htm)|Mindscape Door|Mindscape Door|modificada|
|[illusion-03-aZg3amDcrXz3cLCz.htm](spells/illusion-03-aZg3amDcrXz3cLCz.htm)|Horrific Visage|Apariencia horrenda|modificada|
|[illusion-03-FrKPwgFxWIGMGgs4.htm](spells/illusion-03-FrKPwgFxWIGMGgs4.htm)|Deceiver's Cloak|Capa del Engañador|modificada|
|[illusion-03-HHGUBGle4OjoxvNR.htm](spells/illusion-03-HHGUBGle4OjoxvNR.htm)|Sculpt Sound|Esculpir sonido|modificada|
|[illusion-03-I0j56TNRmGcTyoqJ.htm](spells/illusion-03-I0j56TNRmGcTyoqJ.htm)|Invisibility Sphere|Esfera de Invisibilidad|modificada|
|[illusion-03-JHntYF0SbaWKq7wR.htm](spells/illusion-03-JHntYF0SbaWKq7wR.htm)|Distracting Chatter|Distracting Chatter|modificada|
|[illusion-03-K2WpC3FFoHrqX9Q5.htm](spells/illusion-03-K2WpC3FFoHrqX9Q5.htm)|Hypnotic Pattern|Pauta hipnótica|modificada|
|[illusion-03-pQ3NIzZXeIIcU81C.htm](spells/illusion-03-pQ3NIzZXeIIcU81C.htm)|Spirit Veil|Spirit Veil|modificada|
|[illusion-03-PVXqMko4yGgw90uo.htm](spells/illusion-03-PVXqMko4yGgw90uo.htm)|Heart's Desire|Heart's Desire|modificada|
|[illusion-03-Pwq6T7xpfAJXV5aj.htm](spells/illusion-03-Pwq6T7xpfAJXV5aj.htm)|Phantom Prison|Prisión fantasma|modificada|
|[illusion-03-VVAZPCvd4d90qVA1.htm](spells/illusion-03-VVAZPCvd4d90qVA1.htm)|Secret Page|Página secreta|modificada|
|[illusion-03-YQWq1DZuLRk32M3h.htm](spells/illusion-03-YQWq1DZuLRk32M3h.htm)|Inscrutable Mask|Inscrutable Mask|modificada|
|[illusion-04-5z1700cfG8Ybm0e6.htm](spells/illusion-04-5z1700cfG8Ybm0e6.htm)|False Nature|Naturaleza falsa|modificada|
|[illusion-04-A7w3YQBrFNH8KYsB.htm](spells/illusion-04-A7w3YQBrFNH8KYsB.htm)|Mirror's Misfortune|Mirror's Misfortune|modificada|
|[illusion-04-a8xXWCQhIR7o6IvP.htm](spells/illusion-04-a8xXWCQhIR7o6IvP.htm)|Invisibility Curtain|Cortina de Invisibilidad|modificada|
|[illusion-04-h5UqEdeqK8iTcU0J.htm](spells/illusion-04-h5UqEdeqK8iTcU0J.htm)|Simulacrum|Simulacro|modificada|
|[illusion-04-HBJPsonQnWcC3qdX.htm](spells/illusion-04-HBJPsonQnWcC3qdX.htm)|Hallucinatory Terrain|Terreno alucinatorio|modificada|
|[illusion-04-HisaZTk67YAxLGBq.htm](spells/illusion-04-HisaZTk67YAxLGBq.htm)|Ephemeral Hazards|Peligros efímeros|modificada|
|[illusion-04-hkfH9Z53hPzcOwNB.htm](spells/illusion-04-hkfH9Z53hPzcOwNB.htm)|Veil|Velo|modificada|
|[illusion-04-HvBpSeUDg0KcK2Hg.htm](spells/illusion-04-HvBpSeUDg0KcK2Hg.htm)|Ocular Overload|Sobrecarga Ocular|modificada|
|[illusion-04-IKb9EOfsrhScO3GO.htm](spells/illusion-04-IKb9EOfsrhScO3GO.htm)|Phantasmal Protagonist|Phantasmal Protagonist|modificada|
|[illusion-04-jqb52wy7A0Eqwuzx.htm](spells/illusion-04-jqb52wy7A0Eqwuzx.htm)|Vision of Beauty|Visión de la Belleza|modificada|
|[illusion-04-juzb53CW6Uzz5pFY.htm](spells/illusion-04-juzb53CW6Uzz5pFY.htm)|Umbral Graft|Injerto Umbral|modificada|
|[illusion-04-mer4V7vWdTs1oLbG.htm](spells/illusion-04-mer4V7vWdTs1oLbG.htm)|Isolation|Aislamiento|modificada|
|[illusion-04-Nun72GTmb31YqSKh.htm](spells/illusion-04-Nun72GTmb31YqSKh.htm)|Invisibility Cloak|Capa de invisibilidad|modificada|
|[illusion-04-srEKUvym9kDVbEhD.htm](spells/illusion-04-srEKUvym9kDVbEhD.htm)|Replicate|Replicate|modificada|
|[illusion-04-tlcrVRqW1MSKJ5IC.htm](spells/illusion-04-tlcrVRqW1MSKJ5IC.htm)|Phantasmal Killer|Asesino fantasmal|modificada|
|[illusion-04-Uqj344bezBq3ESdq.htm](spells/illusion-04-Uqj344bezBq3ESdq.htm)|Nightmare|Pesadilla|modificada|
|[illusion-04-VGK9s6LCMMS027zP.htm](spells/illusion-04-VGK9s6LCMMS027zP.htm)|Confront Selves|Confrontarse a uno mismo|modificada|
|[illusion-04-vhe9DduqaivMs8FV.htm](spells/illusion-04-vhe9DduqaivMs8FV.htm)|Ghostly Transcription|Ghostly Transcription|modificada|
|[illusion-04-ZhLYJlOZzUB1OKoe.htm](spells/illusion-04-ZhLYJlOZzUB1OKoe.htm)|Trickster's Twin|Gemelo del bromista|modificada|
|[illusion-05-CmZCq4htcZ6W0TKk.htm](spells/illusion-05-CmZCq4htcZ6W0TKk.htm)|Mirror Malefactors|Malefactores Espejo|modificada|
|[illusion-05-CuRat8IXBUu9C3Yw.htm](spells/illusion-05-CuRat8IXBUu9C3Yw.htm)|Portrait of the Artist|Retrato del artista|modificada|
|[illusion-05-DdXKfIjDtORUtUvY.htm](spells/illusion-05-DdXKfIjDtORUtUvY.htm)|Fey Glamour|Engaéo feérico|modificada|
|[illusion-05-m4Mc5XbdML92BKOE.htm](spells/illusion-05-m4Mc5XbdML92BKOE.htm)|Strange Geometry|Geometría extra|modificada|
|[illusion-05-PEfSofHm73IT3Khc.htm](spells/illusion-05-PEfSofHm73IT3Khc.htm)|House of Imaginary Walls|La casa de los muros imaginarios|modificada|
|[illusion-05-pfFxj4NI5KwO119I.htm](spells/illusion-05-pfFxj4NI5KwO119I.htm)|Blind Eye|Ojo Ciego|modificada|
|[illusion-05-Pp88ueeq4AlkaL6g.htm](spells/illusion-05-Pp88ueeq4AlkaL6g.htm)|Construct Mindscape|Construct Mindscape|modificada|
|[illusion-05-RCbLd7dfquHnuvrZ.htm](spells/illusion-05-RCbLd7dfquHnuvrZ.htm)|False Vision|Ofuscar videncia|modificada|
|[illusion-05-scTRIrTfXquVYHGw.htm](spells/illusion-05-scTRIrTfXquVYHGw.htm)|Drop Dead|Caerse muerto|modificada|
|[illusion-05-TCk2MDwf5L5OYjFC.htm](spells/illusion-05-TCk2MDwf5L5OYjFC.htm)|Cloak of Colors|Capa de colores|modificada|
|[illusion-05-tcwT97RWKxsJiefG.htm](spells/illusion-05-tcwT97RWKxsJiefG.htm)|Shadow Siphon|Sifón sombrío|modificada|
|[illusion-05-U58aQWJ47VrI36yP.htm](spells/illusion-05-U58aQWJ47VrI36yP.htm)|Hallucination|Alucinación|modificada|
|[illusion-05-Ucf8eynbZMfUucjE.htm](spells/illusion-05-Ucf8eynbZMfUucjE.htm)|Illusory Scene|Escena ilusoria|modificada|
|[illusion-05-wykbu2KW9tMBRySr.htm](spells/illusion-05-wykbu2KW9tMBRySr.htm)|Hologram Cage|Jaula de hologramas|modificada|
|[illusion-05-xPrbxyOEwy9QaPVn.htm](spells/illusion-05-xPrbxyOEwy9QaPVn.htm)|Chameleon Coat|Capa de camaleón|modificada|
|[illusion-06-0XP2XOxT9VSiXFDr.htm](spells/illusion-06-0XP2XOxT9VSiXFDr.htm)|Phantasmal Calamity|Calamidad fantasmal|modificada|
|[illusion-06-EOWh6VVcSjB3WPjX.htm](spells/illusion-06-EOWh6VVcSjB3WPjX.htm)|Shadow Illusion|Ilusión de Sombra|modificada|
|[illusion-06-FgRpddPORsRFwNoX.htm](spells/illusion-06-FgRpddPORsRFwNoX.htm)|Chromatic Image|Imagen cromática|modificada|
|[illusion-06-m34WOIGZCEg1h76G.htm](spells/illusion-06-m34WOIGZCEg1h76G.htm)|Blanket Of Stars|Manto de estrellas|modificada|
|[illusion-06-RQjSQVZRG497cJhX.htm](spells/illusion-06-RQjSQVZRG497cJhX.htm)|Vibrant Pattern|Patrón vibrante|modificada|
|[illusion-06-VUMtDHr8CRwwr3Mj.htm](spells/illusion-06-VUMtDHr8CRwwr3Mj.htm)|Aura of the Unremarkable|Aura de lo anodino|modificada|
|[illusion-06-WPXzPl7YbMEIGWfi.htm](spells/illusion-06-WPXzPl7YbMEIGWfi.htm)|Mislead|Doble engaso|modificada|
|[illusion-07-0873MWM0qKDDv81O.htm](spells/illusion-07-0873MWM0qKDDv81O.htm)|Project Image|Proyectar imagen|modificada|
|[illusion-07-jBGAYmR0BkkbpJvG.htm](spells/illusion-07-jBGAYmR0BkkbpJvG.htm)|Visions of Danger|Visiones de peligro|modificada|
|[illusion-07-O6VQC1Bs4aSYDa6R.htm](spells/illusion-07-O6VQC1Bs4aSYDa6R.htm)|Mask of Terror|Máscara del terror|modificada|
|[illusion-07-VTb0yI6P1bLkzuRr.htm](spells/illusion-07-VTb0yI6P1bLkzuRr.htm)|Shadow Raid|Sombra Raid|modificada|
|[illusion-08-rwCh2qTYPA44KEoK.htm](spells/illusion-08-rwCh2qTYPA44KEoK.htm)|Dream Council|Consejo del Sueño onírico|modificada|
|[illusion-08-TvZiwZRianfTSbEg.htm](spells/illusion-08-TvZiwZRianfTSbEg.htm)|Hypnopompic Terrors|Terrores Hipnopómpicos|modificada|
|[illusion-08-uEyfLoFQsRKBRIcB.htm](spells/illusion-08-uEyfLoFQsRKBRIcB.htm)|Scintillating Pattern|Pauta centelleante|modificada|
|[illusion-08-wfleiawxsfhpRRwf.htm](spells/illusion-08-wfleiawxsfhpRRwf.htm)|Disappearance|Desaparición|modificada|
|[illusion-08-YiIOsc8T6E2iDxgh.htm](spells/illusion-08-YiIOsc8T6E2iDxgh.htm)|Undermine Reality|Undermine Reality|modificada|
|[illusion-09-hq57j7Nif1zuQ2Ab.htm](spells/illusion-09-hq57j7Nif1zuQ2Ab.htm)|Unspeakable Shadow|Sombra indecible|modificada|
|[illusion-09-nA0XlPsnMNrQMpio.htm](spells/illusion-09-nA0XlPsnMNrQMpio.htm)|Fantastic Facade|Fachada fantástica|modificada|
|[illusion-09-qDjeG6dxT4aEEC6J.htm](spells/illusion-09-qDjeG6dxT4aEEC6J.htm)|Weird|Némesis inexorable|modificada|
|[illusion-10-hvKtmoHwekDZ5iOH.htm](spells/illusion-10-hvKtmoHwekDZ5iOH.htm)|Shadow Army|Ejército de Sombra|modificada|
|[necromancy-01-2gQYrCPwBmwau26O.htm](spells/necromancy-01-2gQYrCPwBmwau26O.htm)|Life Link|Life Link|modificada|
|[necromancy-01-2iQKhCQBijhj5Rf3.htm](spells/necromancy-01-2iQKhCQBijhj5Rf3.htm)|Disrupting Weapons|Armas Disruptoras|modificada|
|[necromancy-01-4bFxUA59xeq6Snhw.htm](spells/necromancy-01-4bFxUA59xeq6Snhw.htm)|Hallowed Ground|Hallowed Ground|modificada|
|[necromancy-01-7tp97g0UCJ9wOrd5.htm](spells/necromancy-01-7tp97g0UCJ9wOrd5.htm)|Withering Grasp|Marchitamiento Grasp|modificada|
|[necromancy-01-9u6X9ykhzG11NK1n.htm](spells/necromancy-01-9u6X9ykhzG11NK1n.htm)|Magic Stone|Piedra mágica|modificada|
|[necromancy-01-9WGeBwIIbbUuWKq0.htm](spells/necromancy-01-9WGeBwIIbbUuWKq0.htm)|Animate Dead|Reanimar a los muertos|modificada|
|[necromancy-01-BH3sUerzMb2bWnv1.htm](spells/necromancy-01-BH3sUerzMb2bWnv1.htm)|Call of the Grave|Llamada de la sepultura|modificada|
|[necromancy-01-CxpFy4HJHf4ACbxF.htm](spells/necromancy-01-CxpFy4HJHf4ACbxF.htm)|Putrefy Food and Drink|Putrefacción de alimentos y bebidas|modificada|
|[necromancy-01-D6T17BdazhNy3KPm.htm](spells/necromancy-01-D6T17BdazhNy3KPm.htm)|Soul Siphon|Succionar almas|modificada|
|[necromancy-01-DYdvMZ8G2LiSLVWw.htm](spells/necromancy-01-DYdvMZ8G2LiSLVWw.htm)|Spider Sting|Aguijón de ara|modificada|
|[necromancy-01-ekGHLJSHGgWMUwkY.htm](spells/necromancy-01-ekGHLJSHGgWMUwkY.htm)|Touch of Corruption (Healing)|Toque de Corrupción (Curación)|modificada|
|[necromancy-01-fAlzXtQAASaJx0mY.htm](spells/necromancy-01-fAlzXtQAASaJx0mY.htm)|Life Boost|Life Boost|modificada|
|[necromancy-01-FedTjedva2rYk33r.htm](spells/necromancy-01-FedTjedva2rYk33r.htm)|Undeath's Blessing|Bendición de la muerte en vida|modificada|
|[necromancy-01-gSUQlTDYoLDGAsCP.htm](spells/necromancy-01-gSUQlTDYoLDGAsCP.htm)|Hymn of Healing|Himno de curación|modificada|
|[necromancy-01-GYI4xloAgkm6tTrT.htm](spells/necromancy-01-GYI4xloAgkm6tTrT.htm)|Touch of Undeath|Toque de muerte en vida|modificada|
|[necromancy-01-HG4afO9EOGEU9bZN.htm](spells/necromancy-01-HG4afO9EOGEU9bZN.htm)|Death's Call|Llamada de la muerte|modificada|
|[necromancy-01-IxyD7YdRbSSucxZp.htm](spells/necromancy-01-IxyD7YdRbSSucxZp.htm)|Lay on Hands (Vs. Undead)|Imposición de manos (Vs. muertos vivientes)|modificada|
|[necromancy-01-J7Y7tl0bbdz7TcCc.htm](spells/necromancy-01-J7Y7tl0bbdz7TcCc.htm)|Ray of Enfeeblement|Rayo de debilitamiento|modificada|
|[necromancy-01-jFmWSIpJGGebim6y.htm](spells/necromancy-01-jFmWSIpJGGebim6y.htm)|Touch of Corruption|Toque de Corrupción|modificada|
|[necromancy-01-JUJYVbtNNFdvyrBS.htm](spells/necromancy-01-JUJYVbtNNFdvyrBS.htm)|Necromancer's Generosity|Generosidad del Nigromante|modificada|
|[necromancy-01-k34hDOfIIMAxNL4a.htm](spells/necromancy-01-k34hDOfIIMAxNL4a.htm)|Grim Tendrils|Zarcillos macabros|modificada|
|[necromancy-01-kcelf6IHl3L9VXXg.htm](spells/necromancy-01-kcelf6IHl3L9VXXg.htm)|Disrupt Undead|Disrupt muertos vivientes|modificada|
|[necromancy-01-KIV2LqzS5KtqOItV.htm](spells/necromancy-01-KIV2LqzS5KtqOItV.htm)|Heal Companion|Curar Compañero|modificada|
|[necromancy-01-kvm68hVtmADiIvN4.htm](spells/necromancy-01-kvm68hVtmADiIvN4.htm)|Jealous Hex|Maleficio celoso|modificada|
|[necromancy-01-lZx8jZfKrMEtyGY0.htm](spells/necromancy-01-lZx8jZfKrMEtyGY0.htm)|Rejuvenating Flames|Flamígera rejuvenecedora|modificada|
|[necromancy-01-mAMEt4FFbdqoRnkN.htm](spells/necromancy-01-mAMEt4FFbdqoRnkN.htm)|Chill Touch|Toque gélido|modificada|
|[necromancy-01-NkeLctXo9FLGnDhi.htm](spells/necromancy-01-NkeLctXo9FLGnDhi.htm)|Divine Plagues|Plagas Divinas|modificada|
|[necromancy-01-OJ91rm1FkJSlf3nk.htm](spells/necromancy-01-OJ91rm1FkJSlf3nk.htm)|Ancient Dust|Ancient Dust|modificada|
|[necromancy-01-qXTB7Ec9yYh5JPPV.htm](spells/necromancy-01-qXTB7Ec9yYh5JPPV.htm)|Purify Food and Drink|Purificar comida y agua|modificada|
|[necromancy-01-rfZpqmj0AIIdkVIs.htm](spells/necromancy-01-rfZpqmj0AIIdkVIs.htm)|Heal|Curar|modificada|
|[necromancy-01-rhJyqB9g3ziImQgM.htm](spells/necromancy-01-rhJyqB9g3ziImQgM.htm)|Healer's Blessing|Bendición del sanador.|modificada|
|[necromancy-01-rVhHaWqUsVUO4GuY.htm](spells/necromancy-01-rVhHaWqUsVUO4GuY.htm)|Eject Soul|Eject Soul|modificada|
|[necromancy-01-SdXFiQ4Py8761sNO.htm](spells/necromancy-01-SdXFiQ4Py8761sNO.htm)|Glutton's Jaw|Mandíbulas del glotón|modificada|
|[necromancy-01-SnjhtQYexDtNDdEg.htm](spells/necromancy-01-SnjhtQYexDtNDdEg.htm)|Stabilize|Estabilizar|modificada|
|[necromancy-01-uToa7ksKAzmEpkKC.htm](spells/necromancy-01-uToa7ksKAzmEpkKC.htm)|Admonishing Ray|Admonishing Ray|modificada|
|[necromancy-01-vQuwLqtFFYt0K15N.htm](spells/necromancy-01-vQuwLqtFFYt0K15N.htm)|Goodberry|Buenas bayas|modificada|
|[necromancy-01-wdA52JJnsuQWeyqz.htm](spells/necromancy-01-wdA52JJnsuQWeyqz.htm)|Harm|Dar|modificada|
|[necromancy-01-WV2aMNN9DO5ZBjSv.htm](spells/necromancy-01-WV2aMNN9DO5ZBjSv.htm)|Flense|Flense|modificada|
|[necromancy-01-yHujiDQPdtXW797e.htm](spells/necromancy-01-yHujiDQPdtXW797e.htm)|Spirit Link|Vínculo espiritual|modificada|
|[necromancy-01-yp4w9SG4RuqRM8KD.htm](spells/necromancy-01-yp4w9SG4RuqRM8KD.htm)|Spirit Object|Spirit Object|modificada|
|[necromancy-01-zHSp4PzoOE72DV4o.htm](spells/necromancy-01-zHSp4PzoOE72DV4o.htm)|Torturous Trauma|Torturous Trauma|modificada|
|[necromancy-01-zJQnkKEKbJqGB3iB.htm](spells/necromancy-01-zJQnkKEKbJqGB3iB.htm)|Goblin Pox|Viruela de goblin|modificada|
|[necromancy-01-Zmh4ynfnCtwKeAYl.htm](spells/necromancy-01-Zmh4ynfnCtwKeAYl.htm)|Heal Animal|Curar animal|modificada|
|[necromancy-01-zNN9212H2FGfM7VS.htm](spells/necromancy-01-zNN9212H2FGfM7VS.htm)|Lay on Hands|Mentir de manos|modificada|
|[necromancy-02-4Gl3WSUqYjVVIsOg.htm](spells/necromancy-02-4Gl3WSUqYjVVIsOg.htm)|Shadow Zombie|Sombra Zombie|modificada|
|[necromancy-02-8ViwItUgwT4lOvvb.htm](spells/necromancy-02-8ViwItUgwT4lOvvb.htm)|False Life|Falsa vida|modificada|
|[necromancy-02-BCuHKrDeJ4eq53M6.htm](spells/necromancy-02-BCuHKrDeJ4eq53M6.htm)|Remove Paralysis|Quitar Parálisis|modificada|
|[necromancy-02-c3b6LdLlQDPngNIb.htm](spells/necromancy-02-c3b6LdLlQDPngNIb.htm)|Create Undead|Elaborar muertos vivientes|modificada|
|[necromancy-02-cwXiKPkZrIupjwlQ.htm](spells/necromancy-02-cwXiKPkZrIupjwlQ.htm)|Summoner's Precaution|Precaución del Invocador|modificada|
|[necromancy-02-d7Lwx6KAs47MtF0q.htm](spells/necromancy-02-d7Lwx6KAs47MtF0q.htm)|Shield Other|Escudo a otro|modificada|
|[necromancy-02-dLdRqT6UxTKlsPgp.htm](spells/necromancy-02-dLdRqT6UxTKlsPgp.htm)|Death Knell|Doblar a muerto|modificada|
|[necromancy-02-dxOF7d5kAWusLKWF.htm](spells/necromancy-02-dxOF7d5kAWusLKWF.htm)|Reaper's Lantern|Reaper's Lantern|modificada|
|[necromancy-02-eSEmqOBuywoJ6tYd.htm](spells/necromancy-02-eSEmqOBuywoJ6tYd.htm)|Inner Radiance Torrent|Inner Radiance Torrent|modificada|
|[necromancy-02-Et8RSCLx8w7uOLvo.htm](spells/necromancy-02-Et8RSCLx8w7uOLvo.htm)|Restore Senses|Restablecer los sentidos|modificada|
|[necromancy-02-fZPCv2VHuM2yPbC8.htm](spells/necromancy-02-fZPCv2VHuM2yPbC8.htm)|Deafness|Sordera|modificada|
|[necromancy-02-H4oF5szC7aogqtvw.htm](spells/necromancy-02-H4oF5szC7aogqtvw.htm)|Worm's Repast|Worm's Repast|modificada|
|[necromancy-02-JhRuR7Jj3ViShpq7.htm](spells/necromancy-02-JhRuR7Jj3ViShpq7.htm)|Ghoulish Cravings|Apetitos de gul|modificada|
|[necromancy-02-lOZhcvtej10TqlQm.htm](spells/necromancy-02-lOZhcvtej10TqlQm.htm)|Lifelink Surge|Oleaje Lifelink|modificada|
|[necromancy-02-oNUyCqbpGWHifS02.htm](spells/necromancy-02-oNUyCqbpGWHifS02.htm)|Fungal Hyphae|Hifas fúngicas|modificada|
|[necromancy-02-oryfsRK27jAUnziw.htm](spells/necromancy-02-oryfsRK27jAUnziw.htm)|Imp Sting|Imp Sting|modificada|
|[necromancy-02-qJZZdYBdNaWRJFER.htm](spells/necromancy-02-qJZZdYBdNaWRJFER.htm)|Wholeness of Body|Plenitud corporal|modificada|
|[necromancy-02-RGBZrVRIEDb2G48h.htm](spells/necromancy-02-RGBZrVRIEDb2G48h.htm)|Soothing Mist|Niebla amansadora|modificada|
|[necromancy-02-rthC6dGm3nNrt1xN.htm](spells/necromancy-02-rthC6dGm3nNrt1xN.htm)|Spectral Hand|Mano espectral|modificada|
|[necromancy-02-Ru9v8U9IRk3LtWx8.htm](spells/necromancy-02-Ru9v8U9IRk3LtWx8.htm)|Feral Shades|Feral Shades|modificada|
|[necromancy-02-S708JF3E0kuhuRzG.htm](spells/necromancy-02-S708JF3E0kuhuRzG.htm)|Bone Spray|Bone Spray|modificada|
|[necromancy-02-siU9xRlqWXeKT0mH.htm](spells/necromancy-02-siU9xRlqWXeKT0mH.htm)|Feast of Ashes|Fiesta de las Cenizas|modificada|
|[necromancy-02-SnaLVgxZ9ryUFmUr.htm](spells/necromancy-02-SnaLVgxZ9ryUFmUr.htm)|Restoration|Restablecimiento|modificada|
|[necromancy-02-TaaMEYdZXQXF0Sks.htm](spells/necromancy-02-TaaMEYdZXQXF0Sks.htm)|Blood Vendetta|Vendetta de sangre|modificada|
|[necromancy-02-WRmh1aKEo0dI0NJs.htm](spells/necromancy-02-WRmh1aKEo0dI0NJs.htm)|Corpse Communion|Corpse Communion|modificada|
|[necromancy-02-X3dYByf3YmkcdwG0.htm](spells/necromancy-02-X3dYByf3YmkcdwG0.htm)|Slough Skin|Piel de Fango|modificada|
|[necromancy-02-XEhzFKNTSARsofav.htm](spells/necromancy-02-XEhzFKNTSARsofav.htm)|Advanced Scurvy|Escorbuto Avanzado|modificada|
|[necromancy-02-XFtO4BBI22Uox2QP.htm](spells/necromancy-02-XFtO4BBI22Uox2QP.htm)|Sudden Blight|Desolación repentina|modificada|
|[necromancy-02-xlRv8vCTHh1NeMpw.htm](spells/necromancy-02-xlRv8vCTHh1NeMpw.htm)|Mimic Undead|Mimic Undead|modificada|
|[necromancy-02-xRgU9rrhmGAgG4Rc.htm](spells/necromancy-02-xRgU9rrhmGAgG4Rc.htm)|Gentle Repose|Apacible descanso|modificada|
|[necromancy-02-Xrz6wPXDBUr27izR.htm](spells/necromancy-02-Xrz6wPXDBUr27izR.htm)|Grave Impressions|Grave Impressions|modificada|
|[necromancy-02-zdNUbHqqZzjA07oM.htm](spells/necromancy-02-zdNUbHqqZzjA07oM.htm)|Boneshaker|Boneshaker|modificada|
|[necromancy-03-0chL1b4OFIZxpN3v.htm](spells/necromancy-03-0chL1b4OFIZxpN3v.htm)|Steal Shadow|Robar sombra|modificada|
|[necromancy-03-0JWyMwVnLxX9CDYQ.htm](spells/necromancy-03-0JWyMwVnLxX9CDYQ.htm)|Rouse Skeletons|Rouse Skeletons|modificada|
|[necromancy-03-10siFBMF4pIDhVmf.htm](spells/necromancy-03-10siFBMF4pIDhVmf.htm)|Cup of Dust|Taza de polvo|modificada|
|[necromancy-03-5OcZ3HBkrRFhSWCz.htm](spells/necromancy-03-5OcZ3HBkrRFhSWCz.htm)|Claim Curse|Claim Curse|modificada|
|[necromancy-03-9gMQPCaFM27PEIh4.htm](spells/necromancy-03-9gMQPCaFM27PEIh4.htm)|Return the Favor|Retornar el Favor|modificada|
|[necromancy-03-9iJaD4S7ptUeq5vO.htm](spells/necromancy-03-9iJaD4S7ptUeq5vO.htm)|Positive Attunement|Sintonización Positiva|modificada|
|[necromancy-03-aq76wpgsVEENDyau.htm](spells/necromancy-03-aq76wpgsVEENDyau.htm)|Shadow Spy|Sombra Espía|modificada|
|[necromancy-03-Cd6Crl4wpQaMSYrF.htm](spells/necromancy-03-Cd6Crl4wpQaMSYrF.htm)|Subjugate Undead|Subyugar muertos vivientes|modificada|
|[necromancy-03-cqdmSmQnM0q6wbWG.htm](spells/necromancy-03-cqdmSmQnM0q6wbWG.htm)|Drain Life|Drenar Vida|modificada|
|[necromancy-03-gIVaSCrLhhBzGHQY.htm](spells/necromancy-03-gIVaSCrLhhBzGHQY.htm)|Reincarnate|Reencarnar|modificada|
|[necromancy-03-GUeRTriJkMlMlVrk.htm](spells/necromancy-03-GUeRTriJkMlMlVrk.htm)|Bind Undead|Atar muertos vivientes|modificada|
|[necromancy-03-gWmLT5SkA0qH2mNE.htm](spells/necromancy-03-gWmLT5SkA0qH2mNE.htm)|Envenom Companion|Envenom Companion|modificada|
|[necromancy-03-HIY4XUJZKmFLAJTn.htm](spells/necromancy-03-HIY4XUJZKmFLAJTn.htm)|Life Connection|Conexión con la vida|modificada|
|[necromancy-03-M9TiCE1vlG1j2faM.htm](spells/necromancy-03-M9TiCE1vlG1j2faM.htm)|Martyr's Intervention|Intervención del mártir|modificada|
|[necromancy-03-N1Z1oLPdBxaSgrEE.htm](spells/necromancy-03-N1Z1oLPdBxaSgrEE.htm)|Vampiric Touch|Toque vampírico|modificada|
|[necromancy-03-pSNLufPPsReKQtJR.htm](spells/necromancy-03-pSNLufPPsReKQtJR.htm)|Armor of Bones|Armadura de Huesos|modificada|
|[necromancy-03-RneiyehRO6f7LP44.htm](spells/necromancy-03-RneiyehRO6f7LP44.htm)|Remove Disease|Quitar enfermedad|modificada|
|[necromancy-03-SUKaxVZW2TlM8lu0.htm](spells/necromancy-03-SUKaxVZW2TlM8lu0.htm)|Neutralize Poison|Neutralizar veneno|modificada|
|[necromancy-03-VosLNn2M8S7JH67D.htm](spells/necromancy-03-VosLNn2M8S7JH67D.htm)|Blindness|Ceguera|modificada|
|[necromancy-03-vvbS69EHZuUTq0dr.htm](spells/necromancy-03-vvbS69EHZuUTq0dr.htm)|Life Pact|Pacto de Vida|modificada|
|[necromancy-03-xabHqik5SoyDc5Db.htm](spells/necromancy-03-xabHqik5SoyDc5Db.htm)|Moth's Supper|Moth's Supper|modificada|
|[necromancy-03-ziHDISWkFSwz3pmn.htm](spells/necromancy-03-ziHDISWkFSwz3pmn.htm)|Delay Affliction|Delay Affliction|modificada|
|[necromancy-04-07xYlmGX32XtHGEt.htm](spells/necromancy-04-07xYlmGX32XtHGEt.htm)|Vampiric Maiden|Doncella vampírica|modificada|
|[necromancy-04-0qA4MfMkFklOz2Lk.htm](spells/necromancy-04-0qA4MfMkFklOz2Lk.htm)|Fearful Feast|Festín de Miedo|modificada|
|[necromancy-04-29p7NMY2OTpaINzt.htm](spells/necromancy-04-29p7NMY2OTpaINzt.htm)|Necrotic Radiation|Radiación necrótica|modificada|
|[necromancy-04-2RhZkHNv8ajq0yLq.htm](spells/necromancy-04-2RhZkHNv8ajq0yLq.htm)|Fallow Field|Campo en barbecho|modificada|
|[necromancy-04-4nHYSMHito1GUXlm.htm](spells/necromancy-04-4nHYSMHito1GUXlm.htm)|Rebuke Death|Rebuke Death|modificada|
|[necromancy-04-4y2DMq9DV5HvmnxC.htm](spells/necromancy-04-4y2DMq9DV5HvmnxC.htm)|Chroma Leach|Drenaje cromático|modificada|
|[necromancy-04-5LrQIlimsPXK2xAG.htm](spells/necromancy-04-5LrQIlimsPXK2xAG.htm)|Steal Voice|Sustraer Voz|modificada|
|[necromancy-04-5zeSxzDxMSr0wgSF.htm](spells/necromancy-04-5zeSxzDxMSr0wgSF.htm)|Sanguine Mist|Niebla Sanguina|modificada|
|[necromancy-04-6nTBr5XNuKOuPM5m.htm](spells/necromancy-04-6nTBr5XNuKOuPM5m.htm)|Foul Miasma|Foul Miasma|modificada|
|[necromancy-04-7yWXx3qC4eFNHhxD.htm](spells/necromancy-04-7yWXx3qC4eFNHhxD.htm)|Blight|Asolar|modificada|
|[necromancy-04-8XuNn0h0rHE24m3B.htm](spells/necromancy-04-8XuNn0h0rHE24m3B.htm)|Positive Luminance|Luminancia positiva|modificada|
|[necromancy-04-ba1RyDpwq6tfW8VM.htm](spells/necromancy-04-ba1RyDpwq6tfW8VM.htm)|Cloak of Light|Capa de luz|modificada|
|[necromancy-04-bqx1tJeOZq1Ufhcc.htm](spells/necromancy-04-bqx1tJeOZq1Ufhcc.htm)|Recall Past Life|Recall Past Life|modificada|
|[necromancy-04-eexkxcqnkXazsGfK.htm](spells/necromancy-04-eexkxcqnkXazsGfK.htm)|Enervation|Enervación|modificada|
|[necromancy-04-FCfd1zUrqPaLEtau.htm](spells/necromancy-04-FCfd1zUrqPaLEtau.htm)|Dirge of Remembrance|Dirge of Remembrance|modificada|
|[necromancy-04-fH08MI4KP0KH2EQ9.htm](spells/necromancy-04-fH08MI4KP0KH2EQ9.htm)|Soothing Spring|Primavera amansadora|modificada|
|[necromancy-04-FM3SmEW8N1FCRjqt.htm](spells/necromancy-04-FM3SmEW8N1FCRjqt.htm)|Talking Corpse|Cadáver parlante|modificada|
|[necromancy-04-gfKhtVsXF3HKSdmY.htm](spells/necromancy-04-gfKhtVsXF3HKSdmY.htm)|Seal Fate|Sellar destino|modificada|
|[necromancy-04-gzvRDpM6EvcfYHeu.htm](spells/necromancy-04-gzvRDpM6EvcfYHeu.htm)|Tireless Worker|Trabajador incansable|modificada|
|[necromancy-04-hdzyDrRdHSse88XM.htm](spells/necromancy-04-hdzyDrRdHSse88XM.htm)|Extract Brain|Extraer Cerebro|modificada|
|[necromancy-04-ikSb3LRGnrwXJBVX.htm](spells/necromancy-04-ikSb3LRGnrwXJBVX.htm)|Vital Beacon|Faro vital|modificada|
|[necromancy-04-IT1aaqDBAISlHDUV.htm](spells/necromancy-04-IT1aaqDBAISlHDUV.htm)|Achaekek's Clutch|Embrague de Achaekek|modificada|
|[necromancy-04-J5MNC4xq3CHH31qT.htm](spells/necromancy-04-J5MNC4xq3CHH31qT.htm)|Eradicate Undeath|Erradicar muerte en vida|modificada|
|[necromancy-04-Jli9WBjQZ2MmKJ8y.htm](spells/necromancy-04-Jli9WBjQZ2MmKJ8y.htm)|Spiritual Anamnesis|Anamnesis Espiritual|modificada|
|[necromancy-04-LVwmAH5NGvTuuQSU.htm](spells/necromancy-04-LVwmAH5NGvTuuQSU.htm)|Swarming Wasp Stings|Picaduras de enjambre de avispas|modificada|
|[necromancy-04-MTNlvZ0A9xY5sOg1.htm](spells/necromancy-04-MTNlvZ0A9xY5sOg1.htm)|Rest Eternal|Descanso eterno|modificada|
|[necromancy-04-mTpPZIJ2sdgusPP1.htm](spells/necromancy-04-mTpPZIJ2sdgusPP1.htm)|Euphoric Renewal|Euphoric Renewal|modificada|
|[necromancy-04-nEXkk85BQu1cREa4.htm](spells/necromancy-04-nEXkk85BQu1cREa4.htm)|Garden of Death|Jardín de la Muerte|modificada|
|[necromancy-04-OyFCwQuw8XRazsNr.htm](spells/necromancy-04-OyFCwQuw8XRazsNr.htm)|Remove Curse|Quitar maldición|modificada|
|[necromancy-04-PtX16vbWzDehj8qc.htm](spells/necromancy-04-PtX16vbWzDehj8qc.htm)|Pernicious Poltergeist|Pernicious Poltergeist|modificada|
|[necromancy-04-QE9f3OxvvBThymD4.htm](spells/necromancy-04-QE9f3OxvvBThymD4.htm)|Ectoplasmic Interstice|Intersticio ectoplasmático|modificada|
|[necromancy-04-qzsQmpiQodHBBWYI.htm](spells/necromancy-04-qzsQmpiQodHBBWYI.htm)|Malignant Sustenance|Sustento maligno|modificada|
|[necromancy-04-RbORUmnwlB8b3mNf.htm](spells/necromancy-04-RbORUmnwlB8b3mNf.htm)|Internal Insurrection|Insurrección interna|modificada|
|[necromancy-04-S1Msrwi990FE7uMO.htm](spells/necromancy-04-S1Msrwi990FE7uMO.htm)|Call The Blood|Llamada, la sangre|modificada|
|[necromancy-04-VXUrO8TwRqBpNzdU.htm](spells/necromancy-04-VXUrO8TwRqBpNzdU.htm)|Bloodspray Curse|Bloodspray Curse|modificada|
|[necromancy-04-ZeHeNQ5BNq6m5F1j.htm](spells/necromancy-04-ZeHeNQ5BNq6m5F1j.htm)|Take its Course|Seguir su curso|modificada|
|[necromancy-04-ZhJ8d9Uk4lwIx86b.htm](spells/necromancy-04-ZhJ8d9Uk4lwIx86b.htm)|Plant Growth|Crecimiento vegetal|modificada|
|[necromancy-04-zvvHOQV78WKUB33l.htm](spells/necromancy-04-zvvHOQV78WKUB33l.htm)|Life Siphon|Succionar vida|modificada|
|[necromancy-05-1bw6XJMOERcbC5Iq.htm](spells/necromancy-05-1bw6XJMOERcbC5Iq.htm)|Rip the Spirit|Rip the Spirit|modificada|
|[necromancy-05-2YIr0S2Gt14PMMQp.htm](spells/necromancy-05-2YIr0S2Gt14PMMQp.htm)|Grasping Grave|Sepultura aferrante|modificada|
|[necromancy-05-3r897dYO8oYvuyn5.htm](spells/necromancy-05-3r897dYO8oYvuyn5.htm)|Summon Healing Servitor|Invocar Servidor de Curación|modificada|
|[necromancy-05-59NR1hA2jPSgg2sW.htm](spells/necromancy-05-59NR1hA2jPSgg2sW.htm)|Blister|Ampollar|modificada|
|[necromancy-05-9WlTR9JlEcjRmGiD.htm](spells/necromancy-05-9WlTR9JlEcjRmGiD.htm)|Celestial Brand|Marca celestial|modificada|
|[necromancy-05-CzjQtkRuRlzRvwzg.htm](spells/necromancy-05-CzjQtkRuRlzRvwzg.htm)|Healing Well|Pozo Curativo|modificada|
|[necromancy-05-DgCS456mXKw97vNy.htm](spells/necromancy-05-DgCS456mXKw97vNy.htm)|Ode to Ouroboros|Oda a Ouroboros|modificada|
|[necromancy-05-ES6FkwXXqYr4ujQH.htm](spells/necromancy-05-ES6FkwXXqYr4ujQH.htm)|Blood Feast|Festín de sangre|modificada|
|[necromancy-05-gsYEuWv04XTDxe91.htm](spells/necromancy-05-gsYEuWv04XTDxe91.htm)|Call Spirit|Llamada espíritu|modificada|
|[necromancy-05-hghGRzOSzEl4UXdS.htm](spells/necromancy-05-hghGRzOSzEl4UXdS.htm)|Invoke Spirits|Invocar Espíritus|modificada|
|[necromancy-05-Hnc7eGi7vyZenAIm.htm](spells/necromancy-05-Hnc7eGi7vyZenAIm.htm)|Breath of Life|Aliento de vida|modificada|
|[necromancy-05-IoHxAkK0uGqrgtWl.htm](spells/necromancy-05-IoHxAkK0uGqrgtWl.htm)|Wyvern Sting|Wyvern Sting|modificada|
|[necromancy-05-kqhPt9344UkcGVYO.htm](spells/necromancy-05-kqhPt9344UkcGVYO.htm)|Resurrect|Resucitar|modificada|
|[necromancy-05-MlpbeZ61Euhl0d60.htm](spells/necromancy-05-MlpbeZ61Euhl0d60.htm)|Cloudkill|Nube aniquiladora|modificada|
|[necromancy-05-nQS4vPm5zprqkzFZ.htm](spells/necromancy-05-nQS4vPm5zprqkzFZ.htm)|Curse of Death|Maldición de la Muerte|modificada|
|[necromancy-05-PcmFpaHPCReNp1BD.htm](spells/necromancy-05-PcmFpaHPCReNp1BD.htm)|Over the Coals|Sobre las brasas|modificada|
|[necromancy-05-pCvJ4yoZJxDtgUMI.htm](spells/necromancy-05-pCvJ4yoZJxDtgUMI.htm)|Restorative Moment|Momento de restablecimiento|modificada|
|[necromancy-05-PgDFDvX64eswapSS.htm](spells/necromancy-05-PgDFDvX64eswapSS.htm)|Ectoplasmic Expulsion|Expulsión ectoplásmica|modificada|
|[necromancy-05-qVXraYXlTjissCaG.htm](spells/necromancy-05-qVXraYXlTjissCaG.htm)|Mother's Blessing|Bendición de la Madre|modificada|
|[necromancy-05-rnFAHvKpcsU4BJD4.htm](spells/necromancy-05-rnFAHvKpcsU4BJD4.htm)|Shall not Falter, Shall not Rout|Shall not Falter, Shall not Rout|modificada|
|[necromancy-05-RWwiFnjxoJXn7H6D.htm](spells/necromancy-05-RWwiFnjxoJXn7H6D.htm)|Mind Swap|Mind Swap|modificada|
|[necromancy-05-SzKkzq3Rr6vKIxbp.htm](spells/necromancy-05-SzKkzq3Rr6vKIxbp.htm)|Shepherd of Souls|Pastor de Almas|modificada|
|[necromancy-05-tf4PMMMzR5xxJDun.htm](spells/necromancy-05-tf4PMMMzR5xxJDun.htm)|Cleansing Flames|Cleansing Flamígera|modificada|
|[necromancy-05-VDWIZuLOJqwBthHc.htm](spells/necromancy-05-VDWIZuLOJqwBthHc.htm)|Ravening Maw|Ravening Maw|modificada|
|[necromancy-05-vJuaxTd6q11OjGqA.htm](spells/necromancy-05-vJuaxTd6q11OjGqA.htm)|Abyssal Plague|Plaga abisal|modificada|
|[necromancy-05-wYSLTNvmxbe78l2c.htm](spells/necromancy-05-wYSLTNvmxbe78l2c.htm)|Soulshelter Vessel|Soulshelter Vessel|modificada|
|[necromancy-05-Y1YysjZ40ft0aLFN.htm](spells/necromancy-05-Y1YysjZ40ft0aLFN.htm)|Spiritual Torrent|Torrent Espiritual|modificada|
|[necromancy-05-z2mfh3oPnfYqXflY.htm](spells/necromancy-05-z2mfh3oPnfYqXflY.htm)|Mariner's Curse|La maldición del marinero|modificada|
|[necromancy-05-Z60JRD78wT3afOEJ.htm](spells/necromancy-05-Z60JRD78wT3afOEJ.htm)|Portrait of Spite|Retrato de Spite|modificada|
|[necromancy-05-ZLLY6ThJXCCrO0rL.htm](spells/necromancy-05-ZLLY6ThJXCCrO0rL.htm)|Wall of Flesh|Muro de Carne|modificada|
|[necromancy-06-6lO6uMQxbqmYho0e.htm](spells/necromancy-06-6lO6uMQxbqmYho0e.htm)|Necrotize|Necrotize|modificada|
|[necromancy-06-9kOI14Jep97TzGO7.htm](spells/necromancy-06-9kOI14Jep97TzGO7.htm)|Life-Giving Form|Forma de vida|modificada|
|[necromancy-06-ayRXv0wQH00TTNZe.htm](spells/necromancy-06-ayRXv0wQH00TTNZe.htm)|Purple Worm Sting|Aguijón del gusano púrpura|modificada|
|[necromancy-06-B0Ng2VlhEJMuUXUH.htm](spells/necromancy-06-B0Ng2VlhEJMuUXUH.htm)|Bound in Death|Bound in Death|modificada|
|[necromancy-06-fd31tAHSSGXyOxW6.htm](spells/necromancy-06-fd31tAHSSGXyOxW6.htm)|Vampiric Exsanguination|Desangramiento vampírico|modificada|
|[necromancy-06-g1dDUKsIrdlpdqy9.htm](spells/necromancy-06-g1dDUKsIrdlpdqy9.htm)|Ravenous Darkness|Oscuridad Voraz|modificada|
|[necromancy-06-GzN9bG6cKZ96YC6l.htm](spells/necromancy-06-GzN9bG6cKZ96YC6l.htm)|Claim Undead|Reclamar muerto viviente|modificada|
|[necromancy-06-IkGYwHRLhkuoGReG.htm](spells/necromancy-06-IkGYwHRLhkuoGReG.htm)|Raise Dead|Revivir a los muertos|modificada|
|[necromancy-06-PHVHBbdHeQRfjLmE.htm](spells/necromancy-06-PHVHBbdHeQRfjLmE.htm)|Spirit Blast|Explión espiritual|modificada|
|[necromancy-06-rTvNWcKNpOnGklGF.htm](spells/necromancy-06-rTvNWcKNpOnGklGF.htm)|Gray Shadow|Sombra Gris|modificada|
|[necromancy-06-x5rGOmhDRDVQPrnW.htm](spells/necromancy-06-x5rGOmhDRDVQPrnW.htm)|Field of Life|Campo de vida|modificada|
|[necromancy-06-yUM5OYeMY8971b2S.htm](spells/necromancy-06-yUM5OYeMY8971b2S.htm)|Terminate Bloodline|Terminar linaje|modificada|
|[necromancy-06-Z82DvtSXafPmh4KV.htm](spells/necromancy-06-Z82DvtSXafPmh4KV.htm)|Shambling Horror|Shambling Horror|modificada|
|[necromancy-06-ZMvplR106Jxl7B15.htm](spells/necromancy-06-ZMvplR106Jxl7B15.htm)|Awaken Entropy|Awaken Entropy|modificada|
|[necromancy-07-0jadeyQIItIuRgeH.htm](spells/necromancy-07-0jadeyQIItIuRgeH.htm)|Eclipse Burst|Explosión de eclipse|modificada|
|[necromancy-07-2Vkd1IxylPceUAAF.htm](spells/necromancy-07-2Vkd1IxylPceUAAF.htm)|Regenerate|Regenerar|modificada|
|[necromancy-07-JLdbyGKhjwAAoRLs.htm](spells/necromancy-07-JLdbyGKhjwAAoRLs.htm)|Tempest of Shades|Tempestad de sombras|modificada|
|[necromancy-07-QVMjPfXlpnmeuWKS.htm](spells/necromancy-07-QVMjPfXlpnmeuWKS.htm)|Leng Sting|Leng Sting|modificada|
|[necromancy-07-u3pdkfmy0AWICdoM.htm](spells/necromancy-07-u3pdkfmy0AWICdoM.htm)|Ravenous Reanimation|Reanimación Voraz|modificada|
|[necromancy-07-wU6hNzK8Yfqdmc8m.htm](spells/necromancy-07-wU6hNzK8Yfqdmc8m.htm)|Possession|Posesión|modificada|
|[necromancy-07-Z9OrRXKgAPv6Hn5l.htm](spells/necromancy-07-Z9OrRXKgAPv6Hn5l.htm)|Finger of Death|Dedo de la muerte|modificada|
|[necromancy-08-4MOew29Z1oCX8O28.htm](spells/necromancy-08-4MOew29Z1oCX8O28.htm)|Moment of Renewal|Momento de renovación|modificada|
|[necromancy-08-bRW61IbjaFea0wcv.htm](spells/necromancy-08-bRW61IbjaFea0wcv.htm)|Bathe in Blood|Bathe in Blood|modificada|
|[necromancy-08-gtWxTfMbIN5RHQw6.htm](spells/necromancy-08-gtWxTfMbIN5RHQw6.htm)|All is One, One is All|Todo es Uno, Uno es Todo|modificada|
|[necromancy-08-Ht35SDf9PDStJfoC.htm](spells/necromancy-08-Ht35SDf9PDStJfoC.htm)|Spirit Song|Canción espiritual|modificada|
|[necromancy-08-it4wx2mDIJDlZGqS.htm](spells/necromancy-08-it4wx2mDIJDlZGqS.htm)|Divine Armageddon|Armagedón Divino|modificada|
|[necromancy-08-M0jQlpQYUr0pp2Sv.htm](spells/necromancy-08-M0jQlpQYUr0pp2Sv.htm)|Horrid Wilting|Horrible marchitamiento|modificada|
|[necromancy-08-MS60WhVifb45qORJ.htm](spells/necromancy-08-MS60WhVifb45qORJ.htm)|Spiritual Epidemic|Epidemia espiritual|modificada|
|[necromancy-08-Ovvflf5aFbmBxqq8.htm](spells/necromancy-08-Ovvflf5aFbmBxqq8.htm)|Quivering Palm|Palma temblorosa|modificada|
|[necromancy-08-PgLvO8UNHSj5f61m.htm](spells/necromancy-08-PgLvO8UNHSj5f61m.htm)|Devour Life|Devorar la vida|modificada|
|[necromancy-09-10VcmSYNBrvBphu1.htm](spells/necromancy-09-10VcmSYNBrvBphu1.htm)|Massacre|Masacre|modificada|
|[necromancy-09-drmvQJETA3WZzXyw.htm](spells/necromancy-09-drmvQJETA3WZzXyw.htm)|Voracious Gestalt|Gestalt Voraz|modificada|
|[necromancy-09-FEsuyf203wTNE2et.htm](spells/necromancy-09-FEsuyf203wTNE2et.htm)|Wail of the Banshee|Lamento de la banshe|modificada|
|[necromancy-09-GYmXvS9NJ7QwfWGg.htm](spells/necromancy-09-GYmXvS9NJ7QwfWGg.htm)|Bind Soul|Ligadura del alma|modificada|
|[necromancy-09-LEMfFL551YB120RN.htm](spells/necromancy-09-LEMfFL551YB120RN.htm)|Blunt the Final Blade|Blunt the Final Blade|modificada|
|[necromancy-09-pZr1xrCpaSu6qrXU.htm](spells/necromancy-09-pZr1xrCpaSu6qrXU.htm)|Clone|Clonar|modificada|
|[necromancy-09-z39jFoNJrobyn3MQ.htm](spells/necromancy-09-z39jFoNJrobyn3MQ.htm)|Undertaker|Enterrador|modificada|
|[necromancy-09-ZMY58Yk5hnyfeE3q.htm](spells/necromancy-09-ZMY58Yk5hnyfeE3q.htm)|Linnorm Sting|Linnorm Sting|modificada|
|[necromancy-10-HpIJTVqgXorH9X0L.htm](spells/necromancy-10-HpIJTVqgXorH9X0L.htm)|Revival|Reanimación|modificada|
|[necromancy-10-IGXGs9PlqUCvODcH.htm](spells/necromancy-10-IGXGs9PlqUCvODcH.htm)|Song of the Fallen|Song of the Fallen|modificada|
|[necromancy-10-nuE9qsY2HfPFEgAo.htm](spells/necromancy-10-nuE9qsY2HfPFEgAo.htm)|Raga of Remembrance|Raga del Recuerdo|modificada|
|[necromancy-10-uGXWkR2h8q9MRzEM.htm](spells/necromancy-10-uGXWkR2h8q9MRzEM.htm)|Hero's Defiance|El desafío del héroe|modificada|
|[rare-05-ZyREiMaul0VhDYh3.htm](spells/rare-05-ZyREiMaul0VhDYh3.htm)|Glacial Heart|Glacial Heart|modificada|
|[transmutation-01-0xR9vrt6uDFl0Umo.htm](spells/transmutation-01-0xR9vrt6uDFl0Umo.htm)|Wild Morph|Morfismo salvaje|modificada|
|[transmutation-01-1lmzILdCFENln8Cy.htm](spells/transmutation-01-1lmzILdCFENln8Cy.htm)|Physical Boost|Mejora física|modificada|
|[transmutation-01-8RWfKConLYFZpQ9X.htm](spells/transmutation-01-8RWfKConLYFZpQ9X.htm)|Wild Shape|Forma salvaje|modificada|
|[transmutation-01-aEM2cttJ2eYcLssW.htm](spells/transmutation-01-aEM2cttJ2eYcLssW.htm)|Fleet Step|Pies ligeros Paso|modificada|
|[transmutation-01-AiWtiVmyasyL42J8.htm](spells/transmutation-01-AiWtiVmyasyL42J8.htm)|Crushing Ground|Crushing Ground|modificada|
|[transmutation-01-AUctDF2fqPZN2w4W.htm](spells/transmutation-01-AUctDF2fqPZN2w4W.htm)|Sigil|Sello|modificada|
|[transmutation-01-BA143r8fCqmSjdRf.htm](spells/transmutation-01-BA143r8fCqmSjdRf.htm)|Nettleskin|Piel de ortiga|modificada|
|[transmutation-01-buhP7vfetUxQJlIJ.htm](spells/transmutation-01-buhP7vfetUxQJlIJ.htm)|Restyle|Restyle|modificada|
|[transmutation-01-cOjlzWerBwbPWVkX.htm](spells/transmutation-01-cOjlzWerBwbPWVkX.htm)|Agile Feet|Pies ágiles|modificada|
|[transmutation-01-dINQzhqGmIsqGMUY.htm](spells/transmutation-01-dINQzhqGmIsqGMUY.htm)|Mending|Remendar|modificada|
|[transmutation-01-EE7Q5BHIrfWNCPtT.htm](spells/transmutation-01-EE7Q5BHIrfWNCPtT.htm)|Magic Fang|Colmillo mágico|modificada|
|[transmutation-01-F23T5tHPo3WsFiHW.htm](spells/transmutation-01-F23T5tHPo3WsFiHW.htm)|Quick Sort|Ordenación rápida|modificada|
|[transmutation-01-gfPjmG6Fe6D3MFjl.htm](spells/transmutation-01-gfPjmG6Fe6D3MFjl.htm)|Pest Form|Forma de peste|modificada|
|[transmutation-01-GUnw9YXaW3YyaCAU.htm](spells/transmutation-01-GUnw9YXaW3YyaCAU.htm)|Adapt Self|Adapt Self|modificada|
|[transmutation-01-gwOYh5zMVZB0HNcT.htm](spells/transmutation-01-gwOYh5zMVZB0HNcT.htm)|Unimpeded Stride|Zancada sin trabas|modificada|
|[transmutation-01-i8PBZsnoCrK7IWph.htm](spells/transmutation-01-i8PBZsnoCrK7IWph.htm)|Tentacular Limbs|Miembros tentaculares|modificada|
|[transmutation-01-ifXNOhtmU4fKL68v.htm](spells/transmutation-01-ifXNOhtmU4fKL68v.htm)|Redact|Redactar|modificada|
|[transmutation-01-Juk3cD5385Ftybct.htm](spells/transmutation-01-Juk3cD5385Ftybct.htm)|Verminous Lure|Verminous Lure|modificada|
|[transmutation-01-K8vvrOgW4bGakXxm.htm](spells/transmutation-01-K8vvrOgW4bGakXxm.htm)|Dragon Claws|Garras de dragón|modificada|
|[transmutation-01-KcLVELhCUcKXxiKE.htm](spells/transmutation-01-KcLVELhCUcKXxiKE.htm)|Longstrider|Zancada prodigiosa|modificada|
|[transmutation-01-KFpBT6FPfSFhxQ27.htm](spells/transmutation-01-KFpBT6FPfSFhxQ27.htm)|Juvenile Companion|Compañero Juvenil|modificada|
|[transmutation-01-lbrWMnS2pecKaSVB.htm](spells/transmutation-01-lbrWMnS2pecKaSVB.htm)|Swampcall|Swampcall|modificada|
|[transmutation-01-lV8FkHZtzZu7Cy6j.htm](spells/transmutation-01-lV8FkHZtzZu7Cy6j.htm)|Evolution Surge|Oleaje Evolution|modificada|
|[transmutation-01-lXxP1ziyf4ozkpmv.htm](spells/transmutation-01-lXxP1ziyf4ozkpmv.htm)|Split the Tongue|Dividir la lengua|modificada|
|[transmutation-01-m5QS8q5M6K0euKcT.htm](spells/transmutation-01-m5QS8q5M6K0euKcT.htm)|Healing Plaster|Curar Yeso|modificada|
|[transmutation-01-mFHQ2u4LWiejqKQG.htm](spells/transmutation-01-mFHQ2u4LWiejqKQG.htm)|Overstuff|Atiborrar|modificada|
|[transmutation-01-MPxbKoR54gkYkqLO.htm](spells/transmutation-01-MPxbKoR54gkYkqLO.htm)|Gouging Claw|Gouging Claw|modificada|
|[transmutation-01-nnSipUPNd3sm5vYL.htm](spells/transmutation-01-nnSipUPNd3sm5vYL.htm)|Vibrant Thorns|Espinas vibrantes|modificada|
|[transmutation-01-ps0nmhclT6aIXgd8.htm](spells/transmutation-01-ps0nmhclT6aIXgd8.htm)|Ki Rush|Embestida de ki|modificada|
|[transmutation-01-Q7QQ91vQtyi1Ux36.htm](spells/transmutation-01-Q7QQ91vQtyi1Ux36.htm)|Jump|Saltar|modificada|
|[transmutation-01-rVANhQgB8Uqi9PTl.htm](spells/transmutation-01-rVANhQgB8Uqi9PTl.htm)|Animate Rope|Animar cuerda|modificada|
|[transmutation-01-s3abwDbTV43pGFFW.htm](spells/transmutation-01-s3abwDbTV43pGFFW.htm)|Shillelagh|Shillelagh|modificada|
|[transmutation-01-TFitdEOpQC4SzKQQ.htm](spells/transmutation-01-TFitdEOpQC4SzKQQ.htm)|Magic Weapon|Arma mágica|modificada|
|[transmutation-01-UbHK19RYbxRXWgWX.htm](spells/transmutation-01-UbHK19RYbxRXWgWX.htm)|Temporal Distortion|Distorsión Temporal|modificada|
|[transmutation-01-UGJzJRJDoonfWqqI.htm](spells/transmutation-01-UGJzJRJDoonfWqqI.htm)|Athletic Rush|Avance atlético|modificada|
|[transmutation-01-X8PSYw6WC2ePYSXd.htm](spells/transmutation-01-X8PSYw6WC2ePYSXd.htm)|Stumbling Curse|Stumbling Curse|modificada|
|[transmutation-01-X9dkmh23lFwMjrYd.htm](spells/transmutation-01-X9dkmh23lFwMjrYd.htm)|Ant Haul|Capacidad de carga de la hormiga|modificada|
|[transmutation-01-ZL8NTvB22NeEWhVG.htm](spells/transmutation-01-ZL8NTvB22NeEWhVG.htm)|Ki Strike|Golpe de ki|modificada|
|[transmutation-02-2qGqa33E4GPUCbMV.htm](spells/transmutation-02-2qGqa33E4GPUCbMV.htm)|Humanoid Form|Forma humanoide|modificada|
|[transmutation-02-5KobTMrZeZxuXMgl.htm](spells/transmutation-02-5KobTMrZeZxuXMgl.htm)|Spider Climb|Trepar de ara|modificada|
|[transmutation-02-6Ot4N22t5tPD51BO.htm](spells/transmutation-02-6Ot4N22t5tPD51BO.htm)|Knock|Apertura|modificada|
|[transmutation-02-7OFKYR1VY6EXDuiR.htm](spells/transmutation-02-7OFKYR1VY6EXDuiR.htm)|Clawsong|Clawsong|modificada|
|[transmutation-02-aXoh6OQAL57lgh0a.htm](spells/transmutation-02-aXoh6OQAL57lgh0a.htm)|Expeditious Excavation|Excavación Expeditiva|modificada|
|[transmutation-02-b6UnLNikoq2Std1f.htm](spells/transmutation-02-b6UnLNikoq2Std1f.htm)|Magic Warrior Aspect|Aspecto Guerrero Mágico|modificada|
|[transmutation-02-CXICME10TkEJxz0P.htm](spells/transmutation-02-CXICME10TkEJxz0P.htm)|Shape Wood|Moldear la madera|modificada|
|[transmutation-02-dileJ0Yxqg76LMvu.htm](spells/transmutation-02-dileJ0Yxqg76LMvu.htm)|Tree Shape|Forma de árbol|modificada|
|[transmutation-02-fTK6ysisGhy3hRvz.htm](spells/transmutation-02-fTK6ysisGhy3hRvz.htm)|Rapid Adaptation|Adaptación rápida|modificada|
|[transmutation-02-IhwREVWG0OzzrbWA.htm](spells/transmutation-02-IhwREVWG0OzzrbWA.htm)|Iron Gut|Iron Gut|modificada|
|[transmutation-02-J6vNvrUT3b1hx2iA.htm](spells/transmutation-02-J6vNvrUT3b1hx2iA.htm)|Entangle|Enredar|modificada|
|[transmutation-02-jW2asKFchuoxniSH.htm](spells/transmutation-02-jW2asKFchuoxniSH.htm)|Dismantle|Desmantelar|modificada|
|[transmutation-02-mLAYvbafVKfBgEhz.htm](spells/transmutation-02-mLAYvbafVKfBgEhz.htm)|Loose Time's Arrow|Loose Time's Arrow|modificada|
|[transmutation-02-MZGkMsPBztFN0pUO.htm](spells/transmutation-02-MZGkMsPBztFN0pUO.htm)|Water Breathing|Respiración acuática|modificada|
|[transmutation-02-PjhUmyKnq6K5uDby.htm](spells/transmutation-02-PjhUmyKnq6K5uDby.htm)|Shrink|Encoger|modificada|
|[transmutation-02-rdTEF1hfAWbN58NE.htm](spells/transmutation-02-rdTEF1hfAWbN58NE.htm)|Enhance Victuals|Mejorar víveres|modificada|
|[transmutation-02-Seaah9amXg70RKw2.htm](spells/transmutation-02-Seaah9amXg70RKw2.htm)|Water Walk|Caminar sobre las aguas|modificada|
|[transmutation-02-sLzPzk7DJnfuORJ0.htm](spells/transmutation-02-sLzPzk7DJnfuORJ0.htm)|Animate Object|Animar objeto|modificada|
|[transmutation-02-tp4K7mYDL5MRHvJc.htm](spells/transmutation-02-tp4K7mYDL5MRHvJc.htm)|Magic Warrior Transformation|Transformación de guerrero mágico|modificada|
|[transmutation-02-vb2dFNtbofJ7A9BW.htm](spells/transmutation-02-vb2dFNtbofJ7A9BW.htm)|Summoner's Visage|Summoner's Visage|modificada|
|[transmutation-02-vctIUOOgSmxAF0KG.htm](spells/transmutation-02-vctIUOOgSmxAF0KG.htm)|Fear the Sun|Teme al Sol|modificada|
|[transmutation-02-vGEgI8e7AW6FQ3tP.htm](spells/transmutation-02-vGEgI8e7AW6FQ3tP.htm)|Animal Feature|Animal Feature|modificada|
|[transmutation-02-wp09USMB3GIW1qbp.htm](spells/transmutation-02-wp09USMB3GIW1qbp.htm)|Animal Form|Forma de animal|modificada|
|[transmutation-03-0gdZrT9lwO17EIxc.htm](spells/transmutation-03-0gdZrT9lwO17EIxc.htm)|Ooze Form|Ooze Form|modificada|
|[transmutation-03-0Rl3W7kiq9xVZRcr.htm](spells/transmutation-03-0Rl3W7kiq9xVZRcr.htm)|Time Pocket|Time Pocket|modificada|
|[transmutation-03-8OWA91bgm5r6QPaH.htm](spells/transmutation-03-8OWA91bgm5r6QPaH.htm)|Dividing Trench|Fosa divisoria|modificada|
|[transmutation-03-AMEu5zzLN7uCX645.htm](spells/transmutation-03-AMEu5zzLN7uCX645.htm)|Ghostly Weapon|Arma fantasmal|modificada|
|[transmutation-03-B3eLlbaPxOYHcs1o.htm](spells/transmutation-03-B3eLlbaPxOYHcs1o.htm)|Curse Of Lost Time|Maldición del tiempo perdido|modificada|
|[transmutation-03-DgNOpb8H9MTAu9KL.htm](spells/transmutation-03-DgNOpb8H9MTAu9KL.htm)|Consecrate Flesh|Consagrar Carne|modificada|
|[transmutation-03-gPvtmKMRpg9I9D7H.htm](spells/transmutation-03-gPvtmKMRpg9I9D7H.htm)|Earthbind|Ligadura terrestre|modificada|
|[transmutation-03-HcIAQZjNXHemoXSU.htm](spells/transmutation-03-HcIAQZjNXHemoXSU.htm)|Shifting Sand|Cambiante Sand|modificada|
|[transmutation-03-ilGsyGLGjjIPHbyP.htm](spells/transmutation-03-ilGsyGLGjjIPHbyP.htm)|Embrace the Pit|Abrazar el foso|modificada|
|[transmutation-03-KktHf7zIAWOr499h.htm](spells/transmutation-03-KktHf7zIAWOr499h.htm)|Ranger's Bramble|Ranger's Bramble|modificada|
|[transmutation-03-kWh8sJH7yawidMyW.htm](spells/transmutation-03-kWh8sJH7yawidMyW.htm)|Shrink Item|Encoger objeto|modificada|
|[transmutation-03-LbqunTurwXB3u9Vp.htm](spells/transmutation-03-LbqunTurwXB3u9Vp.htm)|Time Skip|Time Skip|modificada|
|[transmutation-03-Llx0xKvtu8S4z6TI.htm](spells/transmutation-03-Llx0xKvtu8S4z6TI.htm)|Day's Weight|Peso del día|modificada|
|[transmutation-03-MNiT0dHol5fEcKlz.htm](spells/transmutation-03-MNiT0dHol5fEcKlz.htm)|Threefold Aspect|Aspecto triplicado|modificada|
|[transmutation-03-qJGW6BbIcU6sfA1d.htm](spells/transmutation-03-qJGW6BbIcU6sfA1d.htm)|Time Jump|Salto en el tiempo|modificada|
|[transmutation-03-RvBlSIJmxiqfCpR9.htm](spells/transmutation-03-RvBlSIJmxiqfCpR9.htm)|Feet to Fins|Pies a aletas|modificada|
|[transmutation-03-sbTxe4CGP4tn6y51.htm](spells/transmutation-03-sbTxe4CGP4tn6y51.htm)|Lashing Rope|Cuerda de amarre|modificada|
|[transmutation-03-TUbXnR4RAuYzRx1u.htm](spells/transmutation-03-TUbXnR4RAuYzRx1u.htm)|Pyrotechnics|Pirotecnia|modificada|
|[transmutation-03-vh1RpbWfqdNC4L3P.htm](spells/transmutation-03-vh1RpbWfqdNC4L3P.htm)|Meld into Stone|Fundirse con la piedra|modificada|
|[transmutation-03-WsUwpfmhKrKwoIe3.htm](spells/transmutation-03-WsUwpfmhKrKwoIe3.htm)|Slow|Lentificado/a|modificada|
|[transmutation-03-XI6Lzd2B5pernkPd.htm](spells/transmutation-03-XI6Lzd2B5pernkPd.htm)|Insect Form|Forma de insecto|modificada|
|[transmutation-04-0fYE64odlKqISzft.htm](spells/transmutation-04-0fYE64odlKqISzft.htm)|Rusting Grasp|Presa oxidante|modificada|
|[transmutation-04-1NLMPmyCB2MBoCuR.htm](spells/transmutation-04-1NLMPmyCB2MBoCuR.htm)|Tortoise and the Hare|La tortuga y la liebre|modificada|
|[transmutation-04-8rj45fKzCFcB0fxs.htm](spells/transmutation-04-8rj45fKzCFcB0fxs.htm)|Enlarge Companion|Aumentar Compañero|modificada|
|[transmutation-04-A2JfEKe6BZcTG1S8.htm](spells/transmutation-04-A2JfEKe6BZcTG1S8.htm)|Fly|Volar|modificada|
|[transmutation-04-APTMURAW1N0Wpk4w.htm](spells/transmutation-04-APTMURAW1N0Wpk4w.htm)|Precious Metals|Metales preciosos|modificada|
|[transmutation-04-b5sGjGlBf58f8jn0.htm](spells/transmutation-04-b5sGjGlBf58f8jn0.htm)|Air Walk|Caminar por el aire|modificada|
|[transmutation-04-bKDsmKVosexwJ80i.htm](spells/transmutation-04-bKDsmKVosexwJ80i.htm)|Mantis Form|Forma Mantis|modificada|
|[transmutation-04-cBUuG1yJHGeKffpg.htm](spells/transmutation-04-cBUuG1yJHGeKffpg.htm)|Localized Quake|Temblor localizado|modificada|
|[transmutation-04-CHZQJg7O7991Vl4m.htm](spells/transmutation-04-CHZQJg7O7991Vl4m.htm)|Familiar Form|Forma Familiar|modificada|
|[transmutation-04-cOwSsSXRsBaXUvlr.htm](spells/transmutation-04-cOwSsSXRsBaXUvlr.htm)|Stasis|Estasis|modificada|
|[transmutation-04-D6BcAoWFxHYTKwVZ.htm](spells/transmutation-04-D6BcAoWFxHYTKwVZ.htm)|Morass of Ages|Morass of Ages|modificada|
|[transmutation-04-DcmWrD0V5PWQQyDm.htm](spells/transmutation-04-DcmWrD0V5PWQQyDm.htm)|Variable Gravity|Gravedad Variable|modificada|
|[transmutation-04-e36Z2t6tLdW3RUzZ.htm](spells/transmutation-04-e36Z2t6tLdW3RUzZ.htm)|Elemental Gift|Regalo elemental|modificada|
|[transmutation-04-eb4FXf62NYArTqek.htm](spells/transmutation-04-eb4FXf62NYArTqek.htm)|Artistic Flourish|Floritura artística|modificada|
|[transmutation-04-gSFg9zKwgcNZLMEs.htm](spells/transmutation-04-gSFg9zKwgcNZLMEs.htm)|Stormwind Flight|Vuelo del viento de la tormenta|modificada|
|[transmutation-04-KhM8MhoUgoUjBMIz.htm](spells/transmutation-04-KhM8MhoUgoUjBMIz.htm)|Dinosaur Form|Forma de dinosaurio|modificada|
|[transmutation-04-L8pzCOi7Jzx5ALs9.htm](spells/transmutation-04-L8pzCOi7Jzx5ALs9.htm)|Disperse into Air|Dispersarse en el aire|modificada|
|[transmutation-04-McnPlLFvKtQVXNcG.htm](spells/transmutation-04-McnPlLFvKtQVXNcG.htm)|Shape Stone|Moldear la piedra|modificada|
|[transmutation-04-mSM659xN2VIAHiF3.htm](spells/transmutation-04-mSM659xN2VIAHiF3.htm)|Wordsmith|Wordsmith|modificada|
|[transmutation-04-NzXpEzcZAjuDTZjK.htm](spells/transmutation-04-NzXpEzcZAjuDTZjK.htm)|Aerial Form|Forma aérea|modificada|
|[transmutation-04-oiUhJbJ3YCKF62Fu.htm](spells/transmutation-04-oiUhJbJ3YCKF62Fu.htm)|Darkened Eyes|Ojos oscurecidos|modificada|
|[transmutation-04-oND3oFBjxrhyVvyG.htm](spells/transmutation-04-oND3oFBjxrhyVvyG.htm)|Community Repair|Reparar Comunidad|modificada|
|[transmutation-04-Pd2M1XY8EXrSfWgJ.htm](spells/transmutation-04-Pd2M1XY8EXrSfWgJ.htm)|Swarm Form|Forma de enjambre|modificada|
|[transmutation-04-SDkIFrrO1PsE02Kd.htm](spells/transmutation-04-SDkIFrrO1PsE02Kd.htm)|Shifting Form|Cambiante forma|modificada|
|[transmutation-04-V8wXOsoejQhe6CyG.htm](spells/transmutation-04-V8wXOsoejQhe6CyG.htm)|Gaseous Form|Forma gaseosa|modificada|
|[transmutation-04-VuPDHoVEPLbMfCJC.htm](spells/transmutation-04-VuPDHoVEPLbMfCJC.htm)|Bestial Curse|Maldición bestial|modificada|
|[transmutation-04-zjYQyuBNT5VjV4mz.htm](spells/transmutation-04-zjYQyuBNT5VjV4mz.htm)|Elephant Form|Forma de elefante|modificada|
|[transmutation-05-1aXX52MnVbBvT9S7.htm](spells/transmutation-05-1aXX52MnVbBvT9S7.htm)|Establish Nexus|Establecer Nexo|modificada|
|[transmutation-05-1K6AYGisvo9gqdhs.htm](spells/transmutation-05-1K6AYGisvo9gqdhs.htm)|Elemental Form|Forma elemental|modificada|
|[transmutation-05-3E1p58MNQMO4GAxt.htm](spells/transmutation-05-3E1p58MNQMO4GAxt.htm)|Stagnate Time|Tiempo Estancado|modificada|
|[transmutation-05-5tpk4Q2QI3FVhm99.htm](spells/transmutation-05-5tpk4Q2QI3FVhm99.htm)|Rewinding Step|Rebobinar Paso|modificada|
|[transmutation-05-92JNBuY5pfFmywOd.htm](spells/transmutation-05-92JNBuY5pfFmywOd.htm)|Desperate Repair|Reparar desesperadamente|modificada|
|[transmutation-05-9o5aG5025ZczjkPb.htm](spells/transmutation-05-9o5aG5025ZczjkPb.htm)|Untwisting Iron Roots|Enraizamiento del Hierro|modificada|
|[transmutation-05-B3tbO85GBpzQ3u8l.htm](spells/transmutation-05-B3tbO85GBpzQ3u8l.htm)|Wish-Twisted Form|Forma retorcida del deseo|modificada|
|[transmutation-05-CcXtbTxtdV9MKQfu.htm](spells/transmutation-05-CcXtbTxtdV9MKQfu.htm)|Mantle of the Frozen Heart|Manto del Corazón Helado|modificada|
|[transmutation-05-d9sBzPOXX3KT8uTu.htm](spells/transmutation-05-d9sBzPOXX3KT8uTu.htm)|Grisly Growths|Crecimientos espeluznantes|modificada|
|[transmutation-05-DcUGLLyaa9tHH1kN.htm](spells/transmutation-05-DcUGLLyaa9tHH1kN.htm)|Incarnate Ancestry|Ascendencia encarnada|modificada|
|[transmutation-05-ddKBoCjmSyPSHcws.htm](spells/transmutation-05-ddKBoCjmSyPSHcws.htm)|Unusual Anatomy|Anatomía inusual|modificada|
|[transmutation-05-Fu8Ml47ZfXpSYe7E.htm](spells/transmutation-05-Fu8Ml47ZfXpSYe7E.htm)|Aberrant Form|Forma aberrante|modificada|
|[transmutation-05-HWJODX2zPg5cg34F.htm](spells/transmutation-05-HWJODX2zPg5cg34F.htm)|Dragon Wings|Alas de dragón|modificada|
|[transmutation-05-j2kfNBFpLa0JjVr8.htm](spells/transmutation-05-j2kfNBFpLa0JjVr8.htm)|Mantle of the Magma Heart|Manto del Corazón de Magma|modificada|
|[transmutation-05-kRxlkPPe6Gr7Du59.htm](spells/transmutation-05-kRxlkPPe6Gr7Du59.htm)|Wind Jump|Salto del viento|modificada|
|[transmutation-05-lETZeqBPoj2htGVk.htm](spells/transmutation-05-lETZeqBPoj2htGVk.htm)|Quicken Time|Quicken Time|modificada|
|[transmutation-05-LzfrBDxxPTiuN7uL.htm](spells/transmutation-05-LzfrBDxxPTiuN7uL.htm)|Transmute Rock And Mud|Transmutar roca y barro|modificada|
|[transmutation-05-y5pzaNfb17CM1slC.htm](spells/transmutation-05-y5pzaNfb17CM1slC.htm)|Blackfinger's Blades|Blackfinger's Blades|modificada|
|[transmutation-05-YtesyvfAIwXOqISq.htm](spells/transmutation-05-YtesyvfAIwXOqISq.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[transmutation-05-zCcfPS4y5SrZzU2x.htm](spells/transmutation-05-zCcfPS4y5SrZzU2x.htm)|Plant Form|Forma de planta|modificada|
|[transmutation-06-29AyhknPKiDBcy8s.htm](spells/transmutation-06-29AyhknPKiDBcy8s.htm)|Statuette|Estatuilla|modificada|
|[transmutation-06-2B0C22OuX9YrIJ5y.htm](spells/transmutation-06-2B0C22OuX9YrIJ5y.htm)|Ash Form|Forma de Ceniza|modificada|
|[transmutation-06-3KiM09e8DN9AdTPA.htm](spells/transmutation-06-3KiM09e8DN9AdTPA.htm)|Hag's Fruit|Fruta del maléfico|modificada|
|[transmutation-06-5c692cCcTDXjSEzk.htm](spells/transmutation-06-5c692cCcTDXjSEzk.htm)|Dragon Form|Forma de dragón|modificada|
|[transmutation-06-aSDfGcUOUVGU5m1g.htm](spells/transmutation-06-aSDfGcUOUVGU5m1g.htm)|Daemon Form|Forma daimónico|modificada|
|[transmutation-06-dKbv80rlAWVpz83C.htm](spells/transmutation-06-dKbv80rlAWVpz83C.htm)|Demon Form|Forma demoníaca|modificada|
|[transmutation-06-dN8QBNuTiaBHCKUe.htm](spells/transmutation-06-dN8QBNuTiaBHCKUe.htm)|Baleful Polymorph|polimorfismo funesto|modificada|
|[transmutation-06-dR53f0p15EW7D0xC.htm](spells/transmutation-06-dR53f0p15EW7D0xC.htm)|Devil Form|Devil Form|modificada|
|[transmutation-06-FKtPRWcs4zhwn9N1.htm](spells/transmutation-06-FKtPRWcs4zhwn9N1.htm)|Form of the Sandpoint Devil|Forma del Diablo de Sandpoint|modificada|
|[transmutation-06-MMCQsh12TPaDdPbV.htm](spells/transmutation-06-MMCQsh12TPaDdPbV.htm)|Righteous Might|Poder justiciero|modificada|
|[transmutation-06-NCUXsaj2yFxOVhrK.htm](spells/transmutation-06-NCUXsaj2yFxOVhrK.htm)|Create Skinstitch|Elaborar Skinstitch|modificada|
|[transmutation-06-Pr1ruNTbzGn3H9w5.htm](spells/transmutation-06-Pr1ruNTbzGn3H9w5.htm)|Stone to Flesh|De piedra a la carne|modificada|
|[transmutation-06-vF52ktg0wUIAlf57.htm](spells/transmutation-06-vF52ktg0wUIAlf57.htm)|Mantle of Heaven's Slopes|Manto de las Laderas del Cielo|modificada|
|[transmutation-06-xEjGEBvTfDJECSki.htm](spells/transmutation-06-xEjGEBvTfDJECSki.htm)|Ancestral Form|Forma Ancestral|modificada|
|[transmutation-06-YtBZq49N4Um1cwm7.htm](spells/transmutation-06-YtBZq49N4Um1cwm7.htm)|Nature's Reprisal|Nature's Reprisal|modificada|
|[transmutation-06-zhqnMOVPzVvWSUbC.htm](spells/transmutation-06-zhqnMOVPzVvWSUbC.htm)|Tempest Form|Tempest Form|modificada|
|[transmutation-06-znv4ECL7ZtuiagtA.htm](spells/transmutation-06-znv4ECL7ZtuiagtA.htm)|Flesh to Stone|De la carne a la piedra|modificada|
|[transmutation-07-3MzuRDc7ccylpW2e.htm](spells/transmutation-07-3MzuRDc7ccylpW2e.htm)|Hasted Assault|Asalto acelerado|modificada|
|[transmutation-07-8eOMAIvT7S1CJSit.htm](spells/transmutation-07-8eOMAIvT7S1CJSit.htm)|Untwisting Iron Augmentation|Untwisting Iron Augmentation|modificada|
|[transmutation-07-9aFg4EuOBVz0i3Lb.htm](spells/transmutation-07-9aFg4EuOBVz0i3Lb.htm)|Corrosive Body|Cuerpo Corrosivo|modificada|
|[transmutation-07-E3zIZ4pjlOuWeGRz.htm](spells/transmutation-07-E3zIZ4pjlOuWeGRz.htm)|Angel Form|Forma de ángel|modificada|
|[transmutation-07-hiVL8qsnTJtpouw0.htm](spells/transmutation-07-hiVL8qsnTJtpouw0.htm)|Divine Vessel|Recipiente divino|modificada|
|[transmutation-07-IHUs4qn27RrdFQ5Y.htm](spells/transmutation-07-IHUs4qn27RrdFQ5Y.htm)|Cosmic Form|Forma cósmica|modificada|
|[transmutation-07-Ww4cPZ3QHTSpaM1m.htm](spells/transmutation-07-Ww4cPZ3QHTSpaM1m.htm)|Unfolding Wind Blitz|Unfolding Wind Blitz|modificada|
|[transmutation-07-XS7Wyh5YC0NWeWyB.htm](spells/transmutation-07-XS7Wyh5YC0NWeWyB.htm)|Fiery Body|Cuerpo flamígero|modificada|
|[transmutation-08-8AMvNVOUEtxBCDvJ.htm](spells/transmutation-08-8AMvNVOUEtxBCDvJ.htm)|Monstrosity Form|Forma monstruosa|modificada|
|[transmutation-08-U0hL0LLaprcnAyzC.htm](spells/transmutation-08-U0hL0LLaprcnAyzC.htm)|Wind Walk|Caminar con el viento|modificada|
|[transmutation-08-wTYxxYJWN348oV15.htm](spells/transmutation-08-wTYxxYJWN348oV15.htm)|Medusa's Wrath|Medusa's Wrath|modificada|
|[transmutation-09-pswdik31kuHEvdno.htm](spells/transmutation-09-pswdik31kuHEvdno.htm)|Shapechange|Cambio de forma|modificada|
|[transmutation-09-TbYqDhlNRiWHe146.htm](spells/transmutation-09-TbYqDhlNRiWHe146.htm)|One With the Land|Uno con la tierra|modificada|
|[transmutation-09-YDMOqndvYFu3OjA6.htm](spells/transmutation-09-YDMOqndvYFu3OjA6.htm)|Ki Form|Ki Form|modificada|
|[transmutation-10-1dsahW4g1ggXtypx.htm](spells/transmutation-10-1dsahW4g1ggXtypx.htm)|Time Stop|Detener el tiempo|modificada|
|[transmutation-10-ckUOoqOM7Kg7VqxB.htm](spells/transmutation-10-ckUOoqOM7Kg7VqxB.htm)|Avatar|Avatar|modificada|
|[transmutation-10-pmP8HhXvvEKP3LqU.htm](spells/transmutation-10-pmP8HhXvvEKP3LqU.htm)|Primal Herd|Manada primigenia|modificada|
|[transmutation-10-qqQYrXaRJXr7uc4i.htm](spells/transmutation-10-qqQYrXaRJXr7uc4i.htm)|Apex Companion|Apex Companion|modificada|
|[transmutation-10-vQMAdnIwnV9prPiG.htm](spells/transmutation-10-vQMAdnIwnV9prPiG.htm)|Element Embodied|Elemento encarnado|modificada|
|[transmutation-10-ZXwxs5tRjEGrjAJT.htm](spells/transmutation-10-ZXwxs5tRjEGrjAJT.htm)|Nature Incarnate|Naturaleza encarnada|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[enchantment-01-7ZinJNzxq0XF0oMx.htm](spells/enchantment-01-7ZinJNzxq0XF0oMx.htm)|Bane|Perdición|libre|
|[evocation-03-sxQZ6yqTn0czJxVd.htm](spells/evocation-03-sxQZ6yqTn0czJxVd.htm)|Fireball|Bola de fuego|oficial|
|[evocation-04-YrzBLPLd3r9m6t1p.htm](spells/evocation-04-YrzBLPLd3r9m6t1p.htm)|Fire Shield|Escudo de fuego|oficial|
|[evocation-05-2mVW1KT3AjW2pvDO.htm](spells/evocation-05-2mVW1KT3AjW2pvDO.htm)|Litany against Sloth|Letanía contra la pereza|libre|
|[transmutation-02-wzctak6BxOW8xvFV.htm](spells/transmutation-02-wzctak6BxOW8xvFV.htm)|Enlarge|Agrandar|oficial|
|[transmutation-03-o6YCGx4lycsYpww4.htm](spells/transmutation-03-o6YCGx4lycsYpww4.htm)|Haste|Acelerar|oficial|
|[transmutation-04-Qu4IThrk1wpONwjT.htm](spells/transmutation-04-Qu4IThrk1wpONwjT.htm)|Fey Form|Forma feérica|oficial|
