# Estado de la traducción (blood-lords-bestiary-items)

 * **vacía**: 1
 * **ninguna**: 5


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[D9I9Pa6Y6YFqsikI.htm](blood-lords-bestiary-items/D9I9Pa6Y6YFqsikI.htm)|Electrify|
|[IpL3t43EPH59ZjEM.htm](blood-lords-bestiary-items/IpL3t43EPH59ZjEM.htm)|Launch Darts|
|[lF9zmnxnCY2H8yr9.htm](blood-lords-bestiary-items/lF9zmnxnCY2H8yr9.htm)|Spin|
|[NL4AmhDWTt41yuGx.htm](blood-lords-bestiary-items/NL4AmhDWTt41yuGx.htm)|Keystone|
|[xBQ9apbaJI1Z2nTH.htm](blood-lords-bestiary-items/xBQ9apbaJI1Z2nTH.htm)|Exposed Vault|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[7kwxdgKmWzY7f5ii.htm](blood-lords-bestiary-items/7kwxdgKmWzY7f5ii.htm)|Poison Dart Spray|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
