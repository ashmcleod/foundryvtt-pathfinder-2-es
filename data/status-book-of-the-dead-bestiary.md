# Estado de la traducción (book-of-the-dead-bestiary)

 * **modificada**: 116


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0PrrwvV1936eSCQy.htm](book-of-the-dead-bestiary/0PrrwvV1936eSCQy.htm)|Tormented (Burning)|Tormented (Burning)|modificada|
|[1854X0YrbGyTnhCy.htm](book-of-the-dead-bestiary/1854X0YrbGyTnhCy.htm)|Sadistic Conductor|Conductor sádico|modificada|
|[1hdQjYLtup92Jlls.htm](book-of-the-dead-bestiary/1hdQjYLtup92Jlls.htm)|Obrousian|Obrousian|modificada|
|[27NxweHr9rB1hDCn.htm](book-of-the-dead-bestiary/27NxweHr9rB1hDCn.htm)|Deathless Acolyte Of Urgathoa|Acólito sin muerte de Urgathoa|modificada|
|[3CdslCv2CMP7O12F.htm](book-of-the-dead-bestiary/3CdslCv2CMP7O12F.htm)|Mummified Cat|Gato momificado|modificada|
|[3pQvzrGhxO3bUwYT.htm](book-of-the-dead-bestiary/3pQvzrGhxO3bUwYT.htm)|Iruxi Ossature|Iruxi Ossature|modificada|
|[40P14vZZXKLhBD6q.htm](book-of-the-dead-bestiary/40P14vZZXKLhBD6q.htm)|Sykever|Sykever|modificada|
|[5ukWvQqK17BQl31O.htm](book-of-the-dead-bestiary/5ukWvQqK17BQl31O.htm)|Lacedon|Lacedon|modificada|
|[7JWjoMGf7f7PZgSD.htm](book-of-the-dead-bestiary/7JWjoMGf7f7PZgSD.htm)|Ice Mummy|Momia de hielo|modificada|
|[7pk1DLc96LMcHBB5.htm](book-of-the-dead-bestiary/7pk1DLc96LMcHBB5.htm)|Vanyver|Vanyver|modificada|
|[7WqlOvjoqURmeorA.htm](book-of-the-dead-bestiary/7WqlOvjoqURmeorA.htm)|Death Coach|Entrenador de la muerte|modificada|
|[8jd6xVMgAeQEfect.htm](book-of-the-dead-bestiary/8jd6xVMgAeQEfect.htm)|Nasurgeth|Nasurgeth|modificada|
|[8o27tdIZFp0eTs5N.htm](book-of-the-dead-bestiary/8o27tdIZFp0eTs5N.htm)|Zombie Lord|Zombie Lord|modificada|
|[8qB0gj8salw8746I.htm](book-of-the-dead-bestiary/8qB0gj8salw8746I.htm)|Mummy Prophet Of Set|momia profeta de set|modificada|
|[8tX1seLMsXto5Kni.htm](book-of-the-dead-bestiary/8tX1seLMsXto5Kni.htm)|Ectoplasmic Grasp|Agarre ectoplásmico|modificada|
|[A5GOni2mXNhVsdRg.htm](book-of-the-dead-bestiary/A5GOni2mXNhVsdRg.htm)|Faithless Ecclesiarch|Eclesiarca infiel|modificada|
|[abnTemoEnsqZPbKZ.htm](book-of-the-dead-bestiary/abnTemoEnsqZPbKZ.htm)|Last Guard|Última guardia|modificada|
|[avBb4LvR3TsYpXma.htm](book-of-the-dead-bestiary/avBb4LvR3TsYpXma.htm)|Queen Sluagh|Reina Sluagh|modificada|
|[AXp5EzgP7p8FrYpN.htm](book-of-the-dead-bestiary/AXp5EzgP7p8FrYpN.htm)|Tormented (Crushing)|Atormentado (Aplastante)|modificada|
|[ayCK1wCQY7mCCyxh.htm](book-of-the-dead-bestiary/ayCK1wCQY7mCCyxh.htm)|Locking Door|Cerrar puerta|modificada|
|[bckYmdaq03CUDdc5.htm](book-of-the-dead-bestiary/bckYmdaq03CUDdc5.htm)|Gholdako|Gholdako|modificada|
|[c6qRZuHQ7RHJEAtj.htm](book-of-the-dead-bestiary/c6qRZuHQ7RHJEAtj.htm)|Grappling Spirit|Espíritu de Presa|modificada|
|[cEJh8SY1YDULyhI9.htm](book-of-the-dead-bestiary/cEJh8SY1YDULyhI9.htm)|Relictner Eroder|Relictner Eroder|modificada|
|[Cl1SZNiURw65rz4p.htm](book-of-the-dead-bestiary/Cl1SZNiURw65rz4p.htm)|Silent Stalker|Acechador silencioso|modificada|
|[dcLHA7tihCF2Mraj.htm](book-of-the-dead-bestiary/dcLHA7tihCF2Mraj.htm)|Tormented (Drowning)|Atormentado (Ahogamiento)|modificada|
|[dgQ9VMuC63T5LW5h.htm](book-of-the-dead-bestiary/dgQ9VMuC63T5LW5h.htm)|Weight of Guilt|El peso de la culpa|modificada|
|[dKkHFA4aBgk82QJO.htm](book-of-the-dead-bestiary/dKkHFA4aBgk82QJO.htm)|Skeletal Soldier|Soldado esquelético|modificada|
|[dlizffh8cFUFOhUf.htm](book-of-the-dead-bestiary/dlizffh8cFUFOhUf.htm)|Jitterbone Contortionist|Jitterbone Contortionist|modificada|
|[DMZo6DXmScDXJpJd.htm](book-of-the-dead-bestiary/DMZo6DXmScDXJpJd.htm)|Hunter Wight|Cazador Wight|modificada|
|[DUFaigFhqeKLbrMG.htm](book-of-the-dead-bestiary/DUFaigFhqeKLbrMG.htm)|Grasping Dead|Muerto aferrado|modificada|
|[DVpP5ObHDoT0OULK.htm](book-of-the-dead-bestiary/DVpP5ObHDoT0OULK.htm)|Prowler Wight|Prowler Wight|modificada|
|[e49MDE5dJQ1XFq3O.htm](book-of-the-dead-bestiary/e49MDE5dJQ1XFq3O.htm)|Predatory Rabbit|Conejo depredador|modificada|
|[EDhme1IKgO34NDrt.htm](book-of-the-dead-bestiary/EDhme1IKgO34NDrt.htm)|Festering Gnasher|Festering Gnasher|modificada|
|[eEE3lRhXCjGzlRLJ.htm](book-of-the-dead-bestiary/eEE3lRhXCjGzlRLJ.htm)|Shredskin|Shredskin|modificada|
|[eLAoo8l1I3OiibwW.htm](book-of-the-dead-bestiary/eLAoo8l1I3OiibwW.htm)|Cannibalistic Echoes|Ecos Caníbales|modificada|
|[Ez8wXPDKOzxvxnqS.htm](book-of-the-dead-bestiary/Ez8wXPDKOzxvxnqS.htm)|Blood-Soaked Soil|Suelo empapado de sangre|modificada|
|[f2dAjBXK55w6rnsh.htm](book-of-the-dead-bestiary/f2dAjBXK55w6rnsh.htm)|Tormented (Dislocation)|Atormentado (Dislocación)|modificada|
|[FVXCVh3Y0LdfoIC5.htm](book-of-the-dead-bestiary/FVXCVh3Y0LdfoIC5.htm)|Siphoning Spirit|Siphoning Spirit|modificada|
|[GabMKY8QJOulyqAr.htm](book-of-the-dead-bestiary/GabMKY8QJOulyqAr.htm)|Ghost Stampede|Estampida fantasma|modificada|
|[GcHzyaMYK5QeKUyM.htm](book-of-the-dead-bestiary/GcHzyaMYK5QeKUyM.htm)|Pale Stranger|Pale Stranger|modificada|
|[gXo04F7O4pwOY698.htm](book-of-the-dead-bestiary/gXo04F7O4pwOY698.htm)|Gallowdead|Gallowdead|modificada|
|[h0OtQsMR4OqnYatd.htm](book-of-the-dead-bestiary/h0OtQsMR4OqnYatd.htm)|Deathless Hierophant Of Urgathoa|Hierofante Inmortal De Urgathoa|modificada|
|[hDQmEtisrgRmufUW.htm](book-of-the-dead-bestiary/hDQmEtisrgRmufUW.htm)|Fluxwraith|Fluxwraith|modificada|
|[hhoSyH9QthtvFptC.htm](book-of-the-dead-bestiary/hhoSyH9QthtvFptC.htm)|Siabrae|Siabrae|modificada|
|[hjBcN7ulP6FSK7ie.htm](book-of-the-dead-bestiary/hjBcN7ulP6FSK7ie.htm)|Tormented (Starvation)|Tormented (Starvation)|modificada|
|[I2XdTFyxnnRdmWsi.htm](book-of-the-dead-bestiary/I2XdTFyxnnRdmWsi.htm)|Toppling Furniture|Derribar muebles|modificada|
|[iEQOUQk1wVHFsajW.htm](book-of-the-dead-bestiary/iEQOUQk1wVHFsajW.htm)|Geist|Geist|modificada|
|[iL2BkoRa6Zrvg1wN.htm](book-of-the-dead-bestiary/iL2BkoRa6Zrvg1wN.htm)|Desperate Hunger|Hambre desesperada|modificada|
|[ipVQuGff2OeTVwFK.htm](book-of-the-dead-bestiary/ipVQuGff2OeTVwFK.htm)|Skeletal Mage|Skeletal Mage|modificada|
|[IrzECfuPttgGMbLa.htm](book-of-the-dead-bestiary/IrzECfuPttgGMbLa.htm)|Glimpse Grave|Vislumbrar Tumba|modificada|
|[ishwgxZAlNJxNwGE.htm](book-of-the-dead-bestiary/ishwgxZAlNJxNwGE.htm)|Daqqanoenyent|Daqqanoenyent|modificada|
|[j777BjOqZff6S1v9.htm](book-of-the-dead-bestiary/j777BjOqZff6S1v9.htm)|Bhuta|Bhuta|modificada|
|[j88xR2MqqZrmF5Wz.htm](book-of-the-dead-bestiary/j88xR2MqqZrmF5Wz.htm)|Raw Nerve|Raw Nerve|modificada|
|[JazJz2crkoG9koQR.htm](book-of-the-dead-bestiary/JazJz2crkoG9koQR.htm)|Fallen Champion|Campeón Caído|modificada|
|[jIypNMJE7rVYpItG.htm](book-of-the-dead-bestiary/jIypNMJE7rVYpItG.htm)|Fiddling Bones|Fiddling Bones|modificada|
|[KhHVStbsPSuPElFI.htm](book-of-the-dead-bestiary/KhHVStbsPSuPElFI.htm)|Excorion|Excorion|modificada|
|[kY8MSttryLjbI5wN.htm](book-of-the-dead-bestiary/kY8MSttryLjbI5wN.htm)|Wight Commander|Comandante Wight|modificada|
|[L0JAq1IGEsjJwTbl.htm](book-of-the-dead-bestiary/L0JAq1IGEsjJwTbl.htm)|Minister Of Tumult|Ministro De Tumulto|modificada|
|[laYufBuih3cT95j4.htm](book-of-the-dead-bestiary/laYufBuih3cT95j4.htm)|Hollow Serpent|Serpiente hueca|modificada|
|[lhzNUX83TTYpJuma.htm](book-of-the-dead-bestiary/lhzNUX83TTYpJuma.htm)|Unrisen|Unrisen|modificada|
|[LNgHO51cJF4HPL78.htm](book-of-the-dead-bestiary/LNgHO51cJF4HPL78.htm)|Shattered Window|Ventana estallada|modificada|
|[LWLUaSHl8YCZdDMH.htm](book-of-the-dead-bestiary/LWLUaSHl8YCZdDMH.htm)|Vetalarana Emergent|Vetalarana Emergent|modificada|
|[M59XiYnJ4Z3bSwCC.htm](book-of-the-dead-bestiary/M59XiYnJ4Z3bSwCC.htm)|Polong|Polong|modificada|
|[mDe4WW1pSWXS113j.htm](book-of-the-dead-bestiary/mDe4WW1pSWXS113j.htm)|Flood of Spirits|Inundación de Espíritus|modificada|
|[mL4l2kwPPSMKwzQo.htm](book-of-the-dead-bestiary/mL4l2kwPPSMKwzQo.htm)|Onryo|Onryo|modificada|
|[nbVljZhCnWgGxA18.htm](book-of-the-dead-bestiary/nbVljZhCnWgGxA18.htm)|Zombie Owlbear|Zombie Owlbear|modificada|
|[NcqruxA82SFvTnD1.htm](book-of-the-dead-bestiary/NcqruxA82SFvTnD1.htm)|Child of Urgathoa|Niño de Urgathoa|modificada|
|[Nzf3AfA46cBiWCwN.htm](book-of-the-dead-bestiary/Nzf3AfA46cBiWCwN.htm)|Vetalarana Manipulator|Vetalarana Manipulator|modificada|
|[ol2lji9lH7PXh1uw.htm](book-of-the-dead-bestiary/ol2lji9lH7PXh1uw.htm)|Bone Croupier|Bone Croupier|modificada|
|[OsIjZpdtKmKrrBID.htm](book-of-the-dead-bestiary/OsIjZpdtKmKrrBID.htm)|Priest Of Kabriri|Sacerdote de Kabriri|modificada|
|[pDA6tCwQ3pE6ji4U.htm](book-of-the-dead-bestiary/pDA6tCwQ3pE6ji4U.htm)|Shadern Immolator|Shadern Immolator|modificada|
|[PH4GHPsekbDdyX3j.htm](book-of-the-dead-bestiary/PH4GHPsekbDdyX3j.htm)|Phantom Jailer|Phantom Jailer|modificada|
|[Pum2HQMMrFS89JnW.htm](book-of-the-dead-bestiary/Pum2HQMMrFS89JnW.htm)|Entombed Spirit|Espíritu Sepultado|modificada|
|[pzh6N7lfVk5261CV.htm](book-of-the-dead-bestiary/pzh6N7lfVk5261CV.htm)|Spirit Cyclone|Ciclón espiritual|modificada|
|[qO20so7Mv2pmsLL1.htm](book-of-the-dead-bestiary/qO20so7Mv2pmsLL1.htm)|Husk Zombie|Husk Zombie|modificada|
|[S5dXAmvghYCIrQDe.htm](book-of-the-dead-bestiary/S5dXAmvghYCIrQDe.htm)|Decrepit Mummy|Decrepit Mummy|modificada|
|[sgeULAtaLvg0Uhyn.htm](book-of-the-dead-bestiary/sgeULAtaLvg0Uhyn.htm)|Cadaverous Rake|Rastrillo Cadavérico|modificada|
|[sucEX2JFrVevTNjU.htm](book-of-the-dead-bestiary/sucEX2JFrVevTNjU.htm)|Drake Skeleton|Drake Skeleton|modificada|
|[T9osQMcC96l4X9lk.htm](book-of-the-dead-bestiary/T9osQMcC96l4X9lk.htm)|Beetle Carapace|Caparazón de escarabajo|modificada|
|[tb2nr2ycARadI3Pg.htm](book-of-the-dead-bestiary/tb2nr2ycARadI3Pg.htm)|Iroran Mummy|Iroran Mummy|modificada|
|[TipfHGlT5ctn1JV0.htm](book-of-the-dead-bestiary/TipfHGlT5ctn1JV0.htm)|Zombie Mammoth|Mamut zombi|modificada|
|[Tjgk2iMSjSbUnuXO.htm](book-of-the-dead-bestiary/Tjgk2iMSjSbUnuXO.htm)|Cold Spot|Punto frío|modificada|
|[ToGAEKKOplpXzaQQ.htm](book-of-the-dead-bestiary/ToGAEKKOplpXzaQQ.htm)|Wolf Skeleton|Esqueleto de Lobo|modificada|
|[tqTcM8VqFMyuQ0hY.htm](book-of-the-dead-bestiary/tqTcM8VqFMyuQ0hY.htm)|Final Words|Palabras finales|modificada|
|[uE6YuCCqU715E1Ay.htm](book-of-the-dead-bestiary/uE6YuCCqU715E1Ay.htm)|Bloodthirsty Toy|Juguete sanguinario|modificada|
|[UMDBlaViAdaw2lnN.htm](book-of-the-dead-bestiary/UMDBlaViAdaw2lnN.htm)|Ghost Pirate Captain|Capitán Pirata Fantasma|modificada|
|[uqY9TkQTO5n9rCLZ.htm](book-of-the-dead-bestiary/uqY9TkQTO5n9rCLZ.htm)|Llorona|Llorona|modificada|
|[Ur3dzfmvtN7lyPNG.htm](book-of-the-dead-bestiary/Ur3dzfmvtN7lyPNG.htm)|Taunting Skull|Calavera burlona|modificada|
|[uURI8Netd0ytD9vc.htm](book-of-the-dead-bestiary/uURI8Netd0ytD9vc.htm)|Sluagh Reaper|Sluagh Reaper|modificada|
|[vaVIJeQnKFUeaC8K.htm](book-of-the-dead-bestiary/vaVIJeQnKFUeaC8K.htm)|Combusted|Combustión|modificada|
|[vHEJOONP1ERjuxxl.htm](book-of-the-dead-bestiary/vHEJOONP1ERjuxxl.htm)|Urveth|Urveth|modificada|
|[VN2Vz1dxA9ti66bC.htm](book-of-the-dead-bestiary/VN2Vz1dxA9ti66bC.htm)|Tormented (Impalement)|Atormentado (Empalamiento)|modificada|
|[VNego1px6HN8mtUl.htm](book-of-the-dead-bestiary/VNego1px6HN8mtUl.htm)|Phantom Footsteps|Phantom Footsteps|modificada|
|[vOuiG3tRcr8yL4jh.htm](book-of-the-dead-bestiary/vOuiG3tRcr8yL4jh.htm)|Ghul|Ghul|modificada|
|[VQqdG5PKdCFHXquy.htm](book-of-the-dead-bestiary/VQqdG5PKdCFHXquy.htm)|Disembodied Voices|Voces sin cuerpo|modificada|
|[vvxnRuYBU2uCBPPI.htm](book-of-the-dead-bestiary/vvxnRuYBU2uCBPPI.htm)|Little Man In The Woods|Little Man In The Woods|modificada|
|[VYLTAIVA9qUsbujo.htm](book-of-the-dead-bestiary/VYLTAIVA9qUsbujo.htm)|Waldgeist|Waldgeist|modificada|
|[wGEZUOgoJNkgXx9Z.htm](book-of-the-dead-bestiary/wGEZUOgoJNkgXx9Z.htm)|Horde Lich|Horda Lich|modificada|
|[Wv9iS7JacYjymqlY.htm](book-of-the-dead-bestiary/Wv9iS7JacYjymqlY.htm)|Harlo Krant|Harlo Krant|modificada|
|[XrSjNpkJkBO6ysRG.htm](book-of-the-dead-bestiary/XrSjNpkJkBO6ysRG.htm)|Gashadokuro|Gashadokuro|modificada|
|[XuHQXwAGBdS1s2Kq.htm](book-of-the-dead-bestiary/XuHQXwAGBdS1s2Kq.htm)|Frenetic Musician|Músico frenético|modificada|
|[xxFiRpQdvbKd0P12.htm](book-of-the-dead-bestiary/xxFiRpQdvbKd0P12.htm)|Hungry Ghost|Fantasma Hambriento|modificada|
|[xyOxzfIUTrN5c8ex.htm](book-of-the-dead-bestiary/xyOxzfIUTrN5c8ex.htm)|Blood Tears|Lágrimas de sangre|modificada|
|[Y2Fiu9T1xwSBPVTV.htm](book-of-the-dead-bestiary/Y2Fiu9T1xwSBPVTV.htm)|Corpseroot|Corpseroot|modificada|
|[ypOgr8kPy6KqXAAd.htm](book-of-the-dead-bestiary/ypOgr8kPy6KqXAAd.htm)|Seetangeist|Seetangeist|modificada|
|[Yv9e7NSCzPuhlCnH.htm](book-of-the-dead-bestiary/Yv9e7NSCzPuhlCnH.htm)|Graveknight Warmaster|Graveknight Warmaster|modificada|
|[z3BpUMkUFoXqOFgf.htm](book-of-the-dead-bestiary/z3BpUMkUFoXqOFgf.htm)|Scorned Hound|Sabueso despechado|modificada|
|[z72hPIIjIiLIxnor.htm](book-of-the-dead-bestiary/z72hPIIjIiLIxnor.htm)|Withered|Marchitamiento|modificada|
|[z8WJtCyoVHlcSxYy.htm](book-of-the-dead-bestiary/z8WJtCyoVHlcSxYy.htm)|Skeletal Titan|Titán esquelético|modificada|
|[Ze8SKqa4T8lhSDup.htm](book-of-the-dead-bestiary/Ze8SKqa4T8lhSDup.htm)|Pale Sovereign|Pale Sovereign|modificada|
|[zOEuwWcvD7sQA1kc.htm](book-of-the-dead-bestiary/zOEuwWcvD7sQA1kc.htm)|Ichor Slinger|Ichor Slinger|modificada|
|[zPQZLswJISQrvPb7.htm](book-of-the-dead-bestiary/zPQZLswJISQrvPb7.htm)|Runecarved Lich|Runecarved Lich|modificada|
|[zqftTUpxqkdLx2IY.htm](book-of-the-dead-bestiary/zqftTUpxqkdLx2IY.htm)|Ecorche|Ecorche|modificada|
|[ZqZlji7aCGCGATMP.htm](book-of-the-dead-bestiary/ZqZlji7aCGCGATMP.htm)|Zombie Snake|Serpiente zombi|modificada|
|[ZSrMFLrb5CzFH5WX.htm](book-of-the-dead-bestiary/ZSrMFLrb5CzFH5WX.htm)|Provincial Jiang-shi|Jiang-shi provincial|modificada|
|[ZzKfcOf7CWSZIsAE.htm](book-of-the-dead-bestiary/ZzKfcOf7CWSZIsAE.htm)|Violent Shove|Empujar violentamente|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
