# Estado de la traducción (boons-and-curses)

 * **modificada**: 240


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[02FtcdSA8tjFNmvD.htm](boons-and-curses/02FtcdSA8tjFNmvD.htm)|Hei Feng - Major Boon|Hei Feng - Boon superior|modificada|
|[0bM6m3Gx8Th3NxXp.htm](boons-and-curses/0bM6m3Gx8Th3NxXp.htm)|Nocticula - Minor Boon|Nocticula - Boon inferior|modificada|
|[0hE3yCBhmReWtZGA.htm](boons-and-curses/0hE3yCBhmReWtZGA.htm)|Ghlaunder - Minor Boon|Ghlaunder - Boon inferior|modificada|
|[0JDLq5rMOjUskRTG.htm](boons-and-curses/0JDLq5rMOjUskRTG.htm)|Besmara - Moderate Curse|Besmara - Maldición moderada|modificada|
|[0kOGxG08y4bGgLto.htm](boons-and-curses/0kOGxG08y4bGgLto.htm)|Groetus - Major Curse|Groetus - Maldición superior|modificada|
|[0PMqydlsIjj8GNnl.htm](boons-and-curses/0PMqydlsIjj8GNnl.htm)|Norgorber - Moderate Curse|Norgorber - Maldición moderada|modificada|
|[0vmwZrIhm5rWHkYh.htm](boons-and-curses/0vmwZrIhm5rWHkYh.htm)|Pharasma - Minor Boon|Pharasma - Boon inferior|modificada|
|[10F2EKASFp9bjsjF.htm](boons-and-curses/10F2EKASFp9bjsjF.htm)|Shizuru - Moderate Boon|Shizuru - Boon Moderada|modificada|
|[1abspMhs13VKLklY.htm](boons-and-curses/1abspMhs13VKLklY.htm)|Arazni - Moderate Curse|Arazni - Maldición moderada|modificada|
|[1hdWNyedYvuZgtPr.htm](boons-and-curses/1hdWNyedYvuZgtPr.htm)|Pharasma - Moderate Curse|Pharasma - Maldición moderada|modificada|
|[1J5gmMhFP0Ul9o64.htm](boons-and-curses/1J5gmMhFP0Ul9o64.htm)|Besmara - Major Curse|Besmara - Maldición superior|modificada|
|[1nALC8yWtjBDEaOC.htm](boons-and-curses/1nALC8yWtjBDEaOC.htm)|Rovagug - Minor Curse|Rovagug - Maldición inferior|modificada|
|[1QtjogbV1MsJp7x5.htm](boons-and-curses/1QtjogbV1MsJp7x5.htm)|Achaekek - Minor Curse|Achaekek - Maldición inferior|modificada|
|[1WPIDGaeRd76EXm1.htm](boons-and-curses/1WPIDGaeRd76EXm1.htm)|Chaldira - Major Boon|Chaldira - Boon superior|modificada|
|[2EKgNifq3ozzKYfI.htm](boons-and-curses/2EKgNifq3ozzKYfI.htm)|Norgorber - Minor Boon|Norgorber - Boon inferior|modificada|
|[2i4AQ2tHph54SzRD.htm](boons-and-curses/2i4AQ2tHph54SzRD.htm)|Casandalee - Major Boon|Casandalee - Boon superior|modificada|
|[30Xdyvplx7MfX1nA.htm](boons-and-curses/30Xdyvplx7MfX1nA.htm)|Torag - Moderate Boon|Torag - Boon Moderada|modificada|
|[3hZjK8dKmjWUyotV.htm](boons-and-curses/3hZjK8dKmjWUyotV.htm)|Nivi Rhombodazzle - Minor Curse|Nivi Rhombodazzle - Maldición inferior|modificada|
|[3u3vav6qfAW6hPKE.htm](boons-and-curses/3u3vav6qfAW6hPKE.htm)|Erastil - Moderate Curse|Erastil - Maldición moderada|modificada|
|[496UIvkjyrae8xzb.htm](boons-and-curses/496UIvkjyrae8xzb.htm)|Casandalee - Moderate Curse|Casandalee - Maldición moderada|modificada|
|[4FkVB4uQK4eHZJ6Z.htm](boons-and-curses/4FkVB4uQK4eHZJ6Z.htm)|Rovagug - Minor Boon|Rovagug - Boon inferior|modificada|
|[4gVIjPeKV6ss1atA.htm](boons-and-curses/4gVIjPeKV6ss1atA.htm)|Brigh - Major Curse|Brigh - Maldición superior|modificada|
|[4m2dQ2fvVWxtokVe.htm](boons-and-curses/4m2dQ2fvVWxtokVe.htm)|Desna - Moderate Boon|Desna - Boon Moderada|modificada|
|[4sZ9tpmPj4LIgPvU.htm](boons-and-curses/4sZ9tpmPj4LIgPvU.htm)|Nethys - Major Curse|Nethys - Maldición superior|modificada|
|[5x0wEbgxYmXonBuN.htm](boons-and-curses/5x0wEbgxYmXonBuN.htm)|Lamashtu - Minor Curse|Lamashtu - Maldición inferior|modificada|
|[6CLe2ZOkLSi27S2Z.htm](boons-and-curses/6CLe2ZOkLSi27S2Z.htm)|Sivanah - Moderate Curse|Sivanah - Maldición moderada|modificada|
|[6Cpm04jBSzSwe2oC.htm](boons-and-curses/6Cpm04jBSzSwe2oC.htm)|Nocticula - Moderate Curse|Nocticula - Maldición moderada|modificada|
|[6f8zTNIs5XXzkhkR.htm](boons-and-curses/6f8zTNIs5XXzkhkR.htm)|Lamashtu - Minor Boon|Lamashtu - Boon inferior|modificada|
|[6HcZezAVWArSonu0.htm](boons-and-curses/6HcZezAVWArSonu0.htm)|Chaldira - Moderate Boon|Chaldira - Boon Moderado|modificada|
|[6Iz9b01O5t31ZioP.htm](boons-and-curses/6Iz9b01O5t31ZioP.htm)|Tsukiyo - Minor Curse|Tsukiyo - Maldición inferior|modificada|
|[7HOxIB1abpVRUjY9.htm](boons-and-curses/7HOxIB1abpVRUjY9.htm)|Grandmother Spider - Minor Boon|Abuela Araña - Boon inferior|modificada|
|[7Ky13a2fl5KUuaMd.htm](boons-and-curses/7Ky13a2fl5KUuaMd.htm)|Milani - Minor Boon|Milani - Boon inferior|modificada|
|[845vvISfgkk6sei0.htm](boons-and-curses/845vvISfgkk6sei0.htm)|Torag - Minor Curse|Torag - Maldición inferior|modificada|
|[8Be5VeyjWRZUdxde.htm](boons-and-curses/8Be5VeyjWRZUdxde.htm)|Nivi Rhombodazzle - Minor Boon|Nivi Rhombodazzle - Boon inferior|modificada|
|[8G0USrxM7d4B4EVI.htm](boons-and-curses/8G0USrxM7d4B4EVI.htm)|Gorum - Major Boon|Gorum - Boon superior|modificada|
|[8GElMYCPjhK5uHAj.htm](boons-and-curses/8GElMYCPjhK5uHAj.htm)|Nivi Rhombodazzle - Moderate Curse|Nivi Rhombodazzle - Maldición moderada|modificada|
|[8GyJK2tNVMIuV8Wa.htm](boons-and-curses/8GyJK2tNVMIuV8Wa.htm)|Chaldira - Moderate Curse|Chaldira - Maldición moderada|modificada|
|[8jHjxcB33N4BULE1.htm](boons-and-curses/8jHjxcB33N4BULE1.htm)|Casandalee - Minor Curse|Casandalee - Maldición inferior|modificada|
|[8x308UNvYYDuIHXH.htm](boons-and-curses/8x308UNvYYDuIHXH.htm)|Nocticula - Moderate Boon|Nocticula - Boon Moderado|modificada|
|[8ZNtiMxoJIP1DJ9Q.htm](boons-and-curses/8ZNtiMxoJIP1DJ9Q.htm)|Nethys - Major Boon|Nethys - Boon superior|modificada|
|[9EMaYf6odFEyjdSr.htm](boons-and-curses/9EMaYf6odFEyjdSr.htm)|Urgathoa - Moderate Curse|Urgathoa - Maldición moderada|modificada|
|[9jFdhTtL8zElWdRC.htm](boons-and-curses/9jFdhTtL8zElWdRC.htm)|Groetus - Moderate Boon|Groetus - Boon Moderada|modificada|
|[9jxwttnEfxrOaCsY.htm](boons-and-curses/9jxwttnEfxrOaCsY.htm)|Gruhastha - Major Boon|Gruhastha - Boon superior|modificada|
|[9nCMvbz3e71AgsoW.htm](boons-and-curses/9nCMvbz3e71AgsoW.htm)|Besmara - Minor Boon|Besmara - Boon inferior|modificada|
|[9neKaRaRi9ekttts.htm](boons-and-curses/9neKaRaRi9ekttts.htm)|Chaldira - Major Curse|Chaldira - Maldición superior|modificada|
|[9yABJNudAI1IvifR.htm](boons-and-curses/9yABJNudAI1IvifR.htm)|Kurgess - Minor Boon|Kurgess - Boon inferior|modificada|
|[a1OZtMQSzjYOm0P3.htm](boons-and-curses/a1OZtMQSzjYOm0P3.htm)|Rovagug - Moderate Boon|Rovagug - Boon Moderada|modificada|
|[A8HRlQB7reEHp50k.htm](boons-and-curses/A8HRlQB7reEHp50k.htm)|Nivi Rhombodazzle - Moderate Boon|Nivi Rhombodazzle - Mejora moderada|modificada|
|[AdNAeWhnq5Is3AZb.htm](boons-and-curses/AdNAeWhnq5Is3AZb.htm)|Torag - Major Boon|Torag - Boon superior|modificada|
|[AfuH02H6ib4KYS6C.htm](boons-and-curses/AfuH02H6ib4KYS6C.htm)|Torag - Moderate Curse|Torag - Maldición moderada|modificada|
|[AJj2o3uWKH3ARyFr.htm](boons-and-curses/AJj2o3uWKH3ARyFr.htm)|Casandalee - Major Curse|Casandalee - Maldición superior|modificada|
|[AOBhF0grlwKJSuxi.htm](boons-and-curses/AOBhF0grlwKJSuxi.htm)|Hei Feng - Minor Boon|Hei Feng - Boon inferior|modificada|
|[AodAErjWnvGZaL4M.htm](boons-and-curses/AodAErjWnvGZaL4M.htm)|Sivanah - Moderate Boon|Sivanah - Boon Moderada|modificada|
|[AohWtOzgk2Qr9ADl.htm](boons-and-curses/AohWtOzgk2Qr9ADl.htm)|Gozreh - Moderate Curse|Gozreh - Maldición moderada|modificada|
|[aOVXhHVkLw9dLCdR.htm](boons-and-curses/aOVXhHVkLw9dLCdR.htm)|Zon-Kuthon - Moderate Curse|Zon-Kuthon - Maldición moderada|modificada|
|[auoQ66gY4r7Tk3lc.htm](boons-and-curses/auoQ66gY4r7Tk3lc.htm)|Alseta - Major Boon|Alseta - Boon superior|modificada|
|[aV1I9VmMQBaFU9M9.htm](boons-and-curses/aV1I9VmMQBaFU9M9.htm)|Asmodeus - Moderate Boon|Asmodeus - Mejora moderada|modificada|
|[AvjRCVzLmxapFOLV.htm](boons-and-curses/AvjRCVzLmxapFOLV.htm)|Gozreh - Moderate Boon|Gozreh - Boon Moderada|modificada|
|[AySifuRSqWzDkCDt.htm](boons-and-curses/AySifuRSqWzDkCDt.htm)|Sivanah - Major Curse|Sivanah - Maldición superior|modificada|
|[B69aBTRn2BwSo23z.htm](boons-and-curses/B69aBTRn2BwSo23z.htm)|Nocticula - Minor Curse|Nocticula - Maldición inferior|modificada|
|[BdV4uubwrn0KgD8H.htm](boons-and-curses/BdV4uubwrn0KgD8H.htm)|Alseta - Moderate Curse|Alseta - Maldición moderada|modificada|
|[BKZRjVVt7hkvEpk3.htm](boons-and-curses/BKZRjVVt7hkvEpk3.htm)|Shizuru - Moderate Curse|Shizuru - Maldición moderada|modificada|
|[bvBzvLDKM5sAJm9s.htm](boons-and-curses/bvBzvLDKM5sAJm9s.htm)|Achaekek - Minor Boon|Achaekek - Boon inferior|modificada|
|[c2wwILmpgyXIjlfe.htm](boons-and-curses/c2wwILmpgyXIjlfe.htm)|Torag - Major Curse|Torag - Maldición superior|modificada|
|[C6Ercv1ugcTsbVA9.htm](boons-and-curses/C6Ercv1ugcTsbVA9.htm)|Erastil - Major Curse|Erastil - Maldición superior|modificada|
|[Ca8ZQssvIgH6WEwZ.htm](boons-and-curses/Ca8ZQssvIgH6WEwZ.htm)|Casandalee - Minor Boon|Casandalee - Boon inferior|modificada|
|[CboMubDaWRkG94Ff.htm](boons-and-curses/CboMubDaWRkG94Ff.htm)|Hei Feng - Minor Curse|Hei Feng - Maldición inferior|modificada|
|[cFz09CWKzff2cC73.htm](boons-and-curses/cFz09CWKzff2cC73.htm)|Rovagug - Major Boon|Rovagug - Boon superior|modificada|
|[CjRUOscsOwCmai8D.htm](boons-and-curses/CjRUOscsOwCmai8D.htm)|Desna - Moderate Curse|Desna - Maldición moderada|modificada|
|[CL8lLQJWd4N89QIm.htm](boons-and-curses/CL8lLQJWd4N89QIm.htm)|Erastil - Moderate Boon|Erastil - Boon Moderado|modificada|
|[CmHQGVyNZ7aOmMcd.htm](boons-and-curses/CmHQGVyNZ7aOmMcd.htm)|Nethys - Moderate Curse|Nethys - Maldición moderada|modificada|
|[COLF56taxqleYkcZ.htm](boons-and-curses/COLF56taxqleYkcZ.htm)|Groetus - Minor Boon|Groetus - Boon inferior|modificada|
|[ctcZm0WGiBKVWVYd.htm](boons-and-curses/ctcZm0WGiBKVWVYd.htm)|Alseta - Minor Boon|Alseta - Boon inferior|modificada|
|[d198p8UBPK3VcKi9.htm](boons-and-curses/d198p8UBPK3VcKi9.htm)|Rovagug - Moderate Curse|Rovagug - Maldición moderada|modificada|
|[d2mkwn858zIqhLS7.htm](boons-and-curses/d2mkwn858zIqhLS7.htm)|Ghlaunder - Major Boon|Ghlaunder - Boon superior|modificada|
|[d84VbnhyQ77abLKF.htm](boons-and-curses/d84VbnhyQ77abLKF.htm)|Tsukiyo - Minor Boon|Tsukiyo - Boon inferior|modificada|
|[dFWAMWqnioz6RZxc.htm](boons-and-curses/dFWAMWqnioz6RZxc.htm)|Grandmother Spider - Major Boon|Abuela Araña - Boon superior|modificada|
|[DfXlr5qqbcjJN3gh.htm](boons-and-curses/DfXlr5qqbcjJN3gh.htm)|Abadar - Moderate Boon|Abadar - Mejora moderada|modificada|
|[dHcGihuimtCf1NOs.htm](boons-and-curses/dHcGihuimtCf1NOs.htm)|Iomedae - Major Boon|Iomedae - Boon superior|modificada|
|[DjVTszddTHSo9fkZ.htm](boons-and-curses/DjVTszddTHSo9fkZ.htm)|Nivi Rhombodazzle - Major Curse|Nivi Rhombodazzle - Maldición superior|modificada|
|[DJxDsRXhqXvABYzK.htm](boons-and-curses/DJxDsRXhqXvABYzK.htm)|Sivanah - Minor Boon|Sivanah - Boon inferior|modificada|
|[dL6r0VfFIWlAazDW.htm](boons-and-curses/dL6r0VfFIWlAazDW.htm)|Sarenrae - Major Boon|Sarenrae - Boon superior|modificada|
|[dLudHCFcj4p7KG3j.htm](boons-and-curses/dLudHCFcj4p7KG3j.htm)|Asmodeus - Minor Curse|Asmodeus - Maldición inferior|modificada|
|[DnlAmlrrKv3jwaOq.htm](boons-and-curses/DnlAmlrrKv3jwaOq.htm)|Pharasma - Major Curse|Pharasma - Maldición superior|modificada|
|[dRk4kWmtau9PLQAk.htm](boons-and-curses/dRk4kWmtau9PLQAk.htm)|Chaldira - Minor Curse|Chaldira - Maldición inferior|modificada|
|[DSfHCm8PDL5hLzZ0.htm](boons-and-curses/DSfHCm8PDL5hLzZ0.htm)|Irori - Minor Curse|Irori - Maldición inferior|modificada|
|[dTJP2Hg4ZJu4Ck2y.htm](boons-and-curses/dTJP2Hg4ZJu4Ck2y.htm)|Iomedae - Minor Boon|Iomedae - Boon inferior|modificada|
|[DuGA57MDiwEKp7Y1.htm](boons-and-curses/DuGA57MDiwEKp7Y1.htm)|Sarenrae - Major Curse|Sarenrae - Maldición superior|modificada|
|[DYEN06j9VVTOXqFK.htm](boons-and-curses/DYEN06j9VVTOXqFK.htm)|Besmara - Minor Curse|Besmara - Maldición inferior|modificada|
|[dZU3HdI2oO8LFjGq.htm](boons-and-curses/dZU3HdI2oO8LFjGq.htm)|Urgathoa - Major Boon|Urgathoa - Boon superior|modificada|
|[e7Y4jKHQ3ptbSuxc.htm](boons-and-curses/e7Y4jKHQ3ptbSuxc.htm)|Torag - Minor Boon|Torag - Boon inferior|modificada|
|[e9bdt8UH7ZvpRrLz.htm](boons-and-curses/e9bdt8UH7ZvpRrLz.htm)|Sarenrae - Minor Boon|Sarenrae - Boon inferior|modificada|
|[eFwCopyvVam6GiCT.htm](boons-and-curses/eFwCopyvVam6GiCT.htm)|Achaekek - Major Boon|Achaekek - Bendición superior|modificada|
|[eibN2Uf0dsHCU5rE.htm](boons-and-curses/eibN2Uf0dsHCU5rE.htm)|Zon-Kuthon - Major Boon|Zon-Kuthon - Boon superior|modificada|
|[eVRjDKt0N0qSmpCn.htm](boons-and-curses/eVRjDKt0N0qSmpCn.htm)|Urgathoa - Major Curse|Urgathoa - Maldición superior|modificada|
|[FeCcooyIb1JDQhd7.htm](boons-and-curses/FeCcooyIb1JDQhd7.htm)|Calistria - Major Boon|Calistria - Boon superior|modificada|
|[ffflmrfFtpFrnVqC.htm](boons-and-curses/ffflmrfFtpFrnVqC.htm)|Kurgess - Moderate Curse|Kurgess - Maldición moderada|modificada|
|[fLBNXULLPalAKlYe.htm](boons-and-curses/fLBNXULLPalAKlYe.htm)|Cayden Cailean - Minor Curse|Cayden Cailean - Maldición inferior|modificada|
|[FRmpIgwDHeewpFkL.htm](boons-and-curses/FRmpIgwDHeewpFkL.htm)|Tsukiyo - Major Curse|Tsukiyo - Maldición superior|modificada|
|[fsKVskUhWwMgvUaT.htm](boons-and-curses/fsKVskUhWwMgvUaT.htm)|Cayden Cailean - Moderate Boon|Cayden Cailean - Boon Moderada|modificada|
|[fV0t8xagcbqwvTpQ.htm](boons-and-curses/fV0t8xagcbqwvTpQ.htm)|Asmodeus - Minor Boon|Asmodeus - Boon inferior|modificada|
|[fV0Xa1Bd3BoWACgT.htm](boons-and-curses/fV0Xa1Bd3BoWACgT.htm)|Irori - Minor Boon|Irori - Boon inferior|modificada|
|[fV940VM5RCsNwUvA.htm](boons-and-curses/fV940VM5RCsNwUvA.htm)|Calistria - Major Curse|Calistria - Maldición superior|modificada|
|[fXEeTSVINdWaKHhw.htm](boons-and-curses/fXEeTSVINdWaKHhw.htm)|Abadar - Minor Boon|Abadar - Boon inferior|modificada|
|[G4Y4qWEbFJXLdI2G.htm](boons-and-curses/G4Y4qWEbFJXLdI2G.htm)|Alseta - Minor Curse|Alseta - Maldición inferior|modificada|
|[gDBa72Y2jokr8Zzg.htm](boons-and-curses/gDBa72Y2jokr8Zzg.htm)|Lamashtu - Moderate Curse|Lamashtu - Maldición moderada|modificada|
|[GJdHcY56q2c3kSiA.htm](boons-and-curses/GJdHcY56q2c3kSiA.htm)|Achaekek - Major Curse|Achaekek - Maldición superior|modificada|
|[GKyH7IQHrmmCnuop.htm](boons-and-curses/GKyH7IQHrmmCnuop.htm)|Shizuru - Major Boon|Shizuru - Boon superior|modificada|
|[Gv05Y1IXI4RWK6YO.htm](boons-and-curses/Gv05Y1IXI4RWK6YO.htm)|Milani - Major Curse|Milani - Maldición superior|modificada|
|[gy4ThzGHW3plFZ2a.htm](boons-and-curses/gy4ThzGHW3plFZ2a.htm)|Brigh - Minor Boon|Brigh - Boon inferior|modificada|
|[HdPVL76QiDhsLCA9.htm](boons-and-curses/HdPVL76QiDhsLCA9.htm)|Nethys - Moderate Boon|Nethys - Moderada Boon|modificada|
|[hoXFKbMHeUabjW3s.htm](boons-and-curses/hoXFKbMHeUabjW3s.htm)|Sivanah - Minor Curse|Sivanah - Maldición inferior|modificada|
|[hrTl9kfSNrOQeNze.htm](boons-and-curses/hrTl9kfSNrOQeNze.htm)|Erastil - Minor Boon|Erastil - Boon inferior|modificada|
|[HrV31rAkNjV4KfCU.htm](boons-and-curses/HrV31rAkNjV4KfCU.htm)|Gorum - Minor Boon|Gorum - Boon inferior|modificada|
|[hUSknn3LYmT7eKzT.htm](boons-and-curses/hUSknn3LYmT7eKzT.htm)|Norgorber - Moderate Boon|Norgorber - Boon Moderada|modificada|
|[Hvz04dGqmV8x25bw.htm](boons-and-curses/Hvz04dGqmV8x25bw.htm)|Tsukiyo - Moderate Boon|Tsukiyo - Boon Moderada|modificada|
|[HXRLOkyO8F374X8R.htm](boons-and-curses/HXRLOkyO8F374X8R.htm)|Arazni - Moderate Boon|Arazni - Mejora moderada|modificada|
|[ILtU0TAUZcOcwkkG.htm](boons-and-curses/ILtU0TAUZcOcwkkG.htm)|Shelyn - Major Curse|Shelyn - Maldición superior|modificada|
|[innbfBPD1VsBIVDn.htm](boons-and-curses/innbfBPD1VsBIVDn.htm)|Ghlaunder - Moderate Boon|Ghlaunder - Moderada Boon|modificada|
|[IOaepssdwTanFooc.htm](boons-and-curses/IOaepssdwTanFooc.htm)|Iomedae - Minor Curse|Iomedae - Maldición inferior|modificada|
|[iT6MnLgzuVetCzm7.htm](boons-and-curses/iT6MnLgzuVetCzm7.htm)|Brigh - Moderate Curse|Brigh - Maldición moderada|modificada|
|[J0aztZRSjNjJzwGy.htm](boons-and-curses/J0aztZRSjNjJzwGy.htm)|Arazni - Major Curse|Arazni - Maldición superior|modificada|
|[jDVTrMaswJRJRTuf.htm](boons-and-curses/jDVTrMaswJRJRTuf.htm)|Besmara - Moderate Boon|Besmara - Boon Moderada|modificada|
|[jfWeNqYj3rn9EysM.htm](boons-and-curses/jfWeNqYj3rn9EysM.htm)|Urgathoa - Minor Boon|Urgathoa - Boon inferior|modificada|
|[jhixjUJZlCetNRjH.htm](boons-and-curses/jhixjUJZlCetNRjH.htm)|Shelyn - Minor Curse|Shelyn - Maldición inferior|modificada|
|[jUitGTczLgoUK0rv.htm](boons-and-curses/jUitGTczLgoUK0rv.htm)|Ghlaunder - Major Curse|Ghlaunder - Maldición superior|modificada|
|[jVeuBZsa4iFg0wC6.htm](boons-and-curses/jVeuBZsa4iFg0wC6.htm)|Nethys - Minor Boon|Nethys - Boon inferior|modificada|
|[kCCrjskDIbG0e74O.htm](boons-and-curses/kCCrjskDIbG0e74O.htm)|Irori - Major Boon|Irori - Boon superior|modificada|
|[kEAw5PMLdX2gHlwR.htm](boons-and-curses/kEAw5PMLdX2gHlwR.htm)|Gozreh - Major Curse|Gozreh - Maldición superior|modificada|
|[KjPTFfxMAqCngQFB.htm](boons-and-curses/KjPTFfxMAqCngQFB.htm)|Nethys - Minor Curse|Nethys - Maldición inferior|modificada|
|[Ks1HaYEuSRybaq8U.htm](boons-and-curses/Ks1HaYEuSRybaq8U.htm)|Calistria - Moderate Boon|Calistria - Moderada Boon|modificada|
|[kstqMWge32n0Lfdz.htm](boons-and-curses/kstqMWge32n0Lfdz.htm)|Gorum - Moderate Boon|Gorum - Boon Moderado|modificada|
|[KuNcNP42LxehKnaV.htm](boons-and-curses/KuNcNP42LxehKnaV.htm)|Shizuru - Minor Boon|Shizuru - Boon inferior|modificada|
|[l7n8n4fAoRLIn95W.htm](boons-and-curses/l7n8n4fAoRLIn95W.htm)|Shelyn - Major Boon|Shelyn - Boon superior|modificada|
|[La6t4fVId26PkmYk.htm](boons-and-curses/La6t4fVId26PkmYk.htm)|Cayden Cailean - Minor Boon|Cayden Cailean - Boon inferior|modificada|
|[LfiMah3iJnnjPi29.htm](boons-and-curses/LfiMah3iJnnjPi29.htm)|Shizuru - Minor Curse|Shizuru - Maldición inferior|modificada|
|[lI7bDExxA2zQq1sP.htm](boons-and-curses/lI7bDExxA2zQq1sP.htm)|Zon-Kuthon - Minor Curse|Zon-Kuthon - Maldición inferior|modificada|
|[LMK1RXvxxD0JP0L4.htm](boons-and-curses/LMK1RXvxxD0JP0L4.htm)|Arazni - Minor Boon|Arazni - Boon inferior|modificada|
|[lOIu9jYDAR2rDe4p.htm](boons-and-curses/lOIu9jYDAR2rDe4p.htm)|Norgorber - Major Boon|Norgorber - Boon superior|modificada|
|[LOJVm2Tt4K2XdVLs.htm](boons-and-curses/LOJVm2Tt4K2XdVLs.htm)|Milani - Major Boon|Milani - Boon superior|modificada|
|[LOlv11WEGUQWYKna.htm](boons-and-curses/LOlv11WEGUQWYKna.htm)|Desna - Minor Boon|Desna - Boon inferior|modificada|
|[lR9KS3pQ4gGKNspP.htm](boons-and-curses/lR9KS3pQ4gGKNspP.htm)|Gorum - Moderate Curse|Gorum - Maldición moderada|modificada|
|[lwkqTPPWeJ6olxa3.htm](boons-and-curses/lwkqTPPWeJ6olxa3.htm)|Groetus - Moderate Curse|Groetus - Maldición moderada|modificada|
|[LwXdmLWOBUIV1By0.htm](boons-and-curses/LwXdmLWOBUIV1By0.htm)|Kazutal - Moderate Boon|Kazutal - Moderada Boon|modificada|
|[m33aOQij1BDcArN9.htm](boons-and-curses/m33aOQij1BDcArN9.htm)|Abadar - Major Boon|Abadar - Boon superior|modificada|
|[M78A7uQaa4Ig8pGU.htm](boons-and-curses/M78A7uQaa4Ig8pGU.htm)|Gorum - Major Curse|Gorum - Maldición superior|modificada|
|[MaaMu5jrfCP8w8SW.htm](boons-and-curses/MaaMu5jrfCP8w8SW.htm)|Kazutal - Moderate Curse|Kazutal - Maldición moderada|modificada|
|[McPpckNcC9hoc39a.htm](boons-and-curses/McPpckNcC9hoc39a.htm)|Hei Feng - Moderate Boon|Hei Feng - Boon Moderada|modificada|
|[mcVlGufQ8rQRe61Y.htm](boons-and-curses/mcVlGufQ8rQRe61Y.htm)|Desna - Major Boon|Desna - Boon superior|modificada|
|[MLmgyIZRJX1hfaPR.htm](boons-and-curses/MLmgyIZRJX1hfaPR.htm)|Nivi Rhombodazzle - Major Boon|Nivi Rhombodazzle - Boon superior|modificada|
|[MZWAF1mNNUJOzeWW.htm](boons-and-curses/MZWAF1mNNUJOzeWW.htm)|Hei Feng - Moderate Curse|Hei Feng - Maldición moderada|modificada|
|[N29LCwYfRWHtEqrS.htm](boons-and-curses/N29LCwYfRWHtEqrS.htm)|Urgathoa - Moderate Boon|Urgathoa - Boon Moderado|modificada|
|[n5p8QfZdptRZp0ii.htm](boons-and-curses/n5p8QfZdptRZp0ii.htm)|Abadar - Moderate Curse|Abadar - Maldición moderada|modificada|
|[NeAA1BHGGkRAmBDe.htm](boons-and-curses/NeAA1BHGGkRAmBDe.htm)|Abadar - Minor Curse|Abadar - Maldición inferior|modificada|
|[nFrCY6tT2B8uxaO3.htm](boons-and-curses/nFrCY6tT2B8uxaO3.htm)|Erastil - Major Boon|Erastil - Boon superior|modificada|
|[NgwW7Wt2gO9SEwPX.htm](boons-and-curses/NgwW7Wt2gO9SEwPX.htm)|Gorum - Minor Curse|Gorum - Maldición inferior|modificada|
|[nqOoUH4r8NEi1i2g.htm](boons-and-curses/nqOoUH4r8NEi1i2g.htm)|Calistria - Moderate Curse|Calistria - Maldición moderada|modificada|
|[NV1w0rwc7Tkhhkac.htm](boons-and-curses/NV1w0rwc7Tkhhkac.htm)|Grandmother Spider - Major Curse|Abuela Araña - Maldición superior|modificada|
|[nz77SkH0xWbcw5SY.htm](boons-and-curses/nz77SkH0xWbcw5SY.htm)|Gruhastha - Minor Curse|Gruhastha - Maldición inferior|modificada|
|[NZFTnX1xzI40q8Qr.htm](boons-and-curses/NZFTnX1xzI40q8Qr.htm)|Chaldira - Minor Boon|Chaldira - Boon inferior|modificada|
|[OaUt41v2OrQkHpM4.htm](boons-and-curses/OaUt41v2OrQkHpM4.htm)|Abadar - Major Curse|Abadar - Maldición superior|modificada|
|[oqM1fFyHNigCNBsf.htm](boons-and-curses/oqM1fFyHNigCNBsf.htm)|Milani - Moderate Curse|Milani - Maldición moderada|modificada|
|[OV7cKDx5b50sz875.htm](boons-and-curses/OV7cKDx5b50sz875.htm)|Milani - Moderate Boon|Milani - Mejora moderada|modificada|
|[P9ujY9f0o779TeEn.htm](boons-and-curses/P9ujY9f0o779TeEn.htm)|Tsukiyo - Moderate Curse|Tsukiyo - Maldición moderada|modificada|
|[ParIWsb6B4fdhrHF.htm](boons-and-curses/ParIWsb6B4fdhrHF.htm)|Pharasma - Minor Curse|Pharasma - Maldición inferior|modificada|
|[pEtqyTx6Oa0OqNKl.htm](boons-and-curses/pEtqyTx6Oa0OqNKl.htm)|Brigh - Moderate Boon|Brigh - Moderada Boon|modificada|
|[pf2pAvNcz1Qcbowg.htm](boons-and-curses/pf2pAvNcz1Qcbowg.htm)|Alseta - Moderate Boon|Alseta - Moderada Boon|modificada|
|[PoHos7qriDzQN7Gw.htm](boons-and-curses/PoHos7qriDzQN7Gw.htm)|Rovagug - Major Curse|Rovagug - Maldición superior|modificada|
|[pt4dHeoDwfj8adUE.htm](boons-and-curses/pt4dHeoDwfj8adUE.htm)|Ghlaunder - Minor Curse|Ghlaunder - Maldición inferior|modificada|
|[pUtQOAKeXslaD5g3.htm](boons-and-curses/pUtQOAKeXslaD5g3.htm)|Arazni - Minor Curse|Arazni - Maldición inferior|modificada|
|[pwmFrNkLscXrDSPN.htm](boons-and-curses/pwmFrNkLscXrDSPN.htm)|Erastil - Minor Curse|Erastil - Maldición inferior|modificada|
|[py9v5HfCIzCoyC0C.htm](boons-and-curses/py9v5HfCIzCoyC0C.htm)|Lamashtu - Major Curse|Lamashtu - Maldición superior|modificada|
|[qbo52AnXV8KDXLnL.htm](boons-and-curses/qbo52AnXV8KDXLnL.htm)|Grandmother Spider - Minor Curse|Abuela Araña - Maldición inferior|modificada|
|[QGUFWuz9uF49EIhy.htm](boons-and-curses/QGUFWuz9uF49EIhy.htm)|Cayden Cailean - Major Curse|Cayden Cailean - Maldición superior|modificada|
|[qhVfbZCxZJS8NmB3.htm](boons-and-curses/qhVfbZCxZJS8NmB3.htm)|Irori - Moderate Curse|Irori - Maldición moderada|modificada|
|[qTmjEC528YnvPgXE.htm](boons-and-curses/qTmjEC528YnvPgXE.htm)|Iomedae - Moderate Boon|Iomedae - Boon Moderada|modificada|
|[QuNXNPHvh4DBmyZH.htm](boons-and-curses/QuNXNPHvh4DBmyZH.htm)|Kurgess - Major Curse|Kurgess - Maldición superior|modificada|
|[QVEkEdxCnoGxMo9l.htm](boons-and-curses/QVEkEdxCnoGxMo9l.htm)|Zon-Kuthon - Moderate Boon|Zon-Kuthon - Mejora moderada|modificada|
|[r4VjA1Wje3mtK2M4.htm](boons-and-curses/r4VjA1Wje3mtK2M4.htm)|Nocticula - Major Boon|Nocticula - Boon superior|modificada|
|[r9hOxyTwN0DsrTPU.htm](boons-and-curses/r9hOxyTwN0DsrTPU.htm)|Arazni - Major Boon|Arazni - Boon superior|modificada|
|[rA7ZM6WGnJNrWTmo.htm](boons-and-curses/rA7ZM6WGnJNrWTmo.htm)|Iomedae - Moderate Curse|Iomedae - Maldición moderada|modificada|
|[RLdclcC8zoNAVony.htm](boons-and-curses/RLdclcC8zoNAVony.htm)|Groetus - Minor Curse|Groetus - Maldición inferior|modificada|
|[rN9uF0XUMm4LPFOk.htm](boons-and-curses/rN9uF0XUMm4LPFOk.htm)|Grandmother Spider - Moderate Boon|Abuela Araña - Mejora moderada|modificada|
|[S1qGyGG25YImRhmS.htm](boons-and-curses/S1qGyGG25YImRhmS.htm)|Sarenrae - Moderate Boon|Sarenrae - Boon Moderada|modificada|
|[SaiJX9KBN3lC0RUy.htm](boons-and-curses/SaiJX9KBN3lC0RUy.htm)|Zon-Kuthon - Major Curse|Zon-Kuthon - Maldición superior|modificada|
|[sL6WZ4EIHcIuEwVW.htm](boons-and-curses/sL6WZ4EIHcIuEwVW.htm)|Gruhastha - Moderate Curse|Gruhastha - Maldición moderada|modificada|
|[sUi427H7MJnbiMxh.htm](boons-and-curses/sUi427H7MJnbiMxh.htm)|Hei Feng - Major Curse|Hei Feng - Maldición superior|modificada|
|[sy5UyOvxitqX244D.htm](boons-and-curses/sy5UyOvxitqX244D.htm)|Asmodeus - Major Boon|Asmodeus - Boon superior|modificada|
|[T2csDhA9WsNiwpd5.htm](boons-and-curses/T2csDhA9WsNiwpd5.htm)|Kazutal - Major Boon|Kazutal - Boon superior|modificada|
|[tFnPBYcAZ0X3GbI5.htm](boons-and-curses/tFnPBYcAZ0X3GbI5.htm)|Lamashtu - Moderate Boon|Lamashtu - Boon Moderada|modificada|
|[tilqIbJZLqeknTYo.htm](boons-and-curses/tilqIbJZLqeknTYo.htm)|Shelyn - Minor Boon|Shelyn - Boon inferior|modificada|
|[to4pGXcJqF1VpqBt.htm](boons-and-curses/to4pGXcJqF1VpqBt.htm)|Kurgess - Major Boon|Kurgess - Boon superior|modificada|
|[U7ZJfuPLQPyoaj4M.htm](boons-and-curses/U7ZJfuPLQPyoaj4M.htm)|Pharasma - Major Boon|Pharasma - Boon superior|modificada|
|[ubXii3t9Jf9YHcCk.htm](boons-and-curses/ubXii3t9Jf9YHcCk.htm)|Kazutal - Minor Boon|Kazutal - Boon inferior|modificada|
|[Ul5jjHfvvfB14a3z.htm](boons-and-curses/Ul5jjHfvvfB14a3z.htm)|Kurgess - Minor Curse|Kurgess - Maldición inferior|modificada|
|[uM9dzDmXfnpYCCrc.htm](boons-and-curses/uM9dzDmXfnpYCCrc.htm)|Casandalee - Moderate Boon|Casandalee - Moderada Boon|modificada|
|[unfh0BOx2PEhxWGB.htm](boons-and-curses/unfh0BOx2PEhxWGB.htm)|Besmara - Major Boon|Besmara - Boon superior|modificada|
|[Up7nUIT42zwhgZf4.htm](boons-and-curses/Up7nUIT42zwhgZf4.htm)|Desna - Minor Curse|Desna - Maldición inferior|modificada|
|[uVZrqxtOOutr4Ss9.htm](boons-and-curses/uVZrqxtOOutr4Ss9.htm)|Gozreh - Minor Boon|Gozreh - Boon inferior|modificada|
|[V7FYIgphCCkYsEXF.htm](boons-and-curses/V7FYIgphCCkYsEXF.htm)|Sarenrae - Moderate Curse|Sarenrae - Maldición moderada|modificada|
|[vfQ7TsScMBTNiznn.htm](boons-and-curses/vfQ7TsScMBTNiznn.htm)|Gozreh - Major Boon|Gozreh - Boon superior|modificada|
|[vJd6Ikya7B6XH4lb.htm](boons-and-curses/vJd6Ikya7B6XH4lb.htm)|Kazutal - Minor Curse|Kazutal - Maldición inferior|modificada|
|[vU5leGDNAZSfPgTz.htm](boons-and-curses/vU5leGDNAZSfPgTz.htm)|Shelyn - Moderate Curse|Shelyn - Maldición moderada|modificada|
|[VZodqtMF3qrMl8a7.htm](boons-and-curses/VZodqtMF3qrMl8a7.htm)|Sarenrae - Minor Curse|Sarenrae - Maldición inferior|modificada|
|[wEqPbkQMuqlL2TPn.htm](boons-and-curses/wEqPbkQMuqlL2TPn.htm)|Irori - Major Curse|Irori - Maldición superior|modificada|
|[wIjqVRP0Hm2bIfyc.htm](boons-and-curses/wIjqVRP0Hm2bIfyc.htm)|Calistria - Minor Curse|Calistria - Maldición inferior|modificada|
|[wlBiDpWvjKiw9k2z.htm](boons-and-curses/wlBiDpWvjKiw9k2z.htm)|Zon-Kuthon - Minor Boon|Zon-Kuthon - Boon inferior|modificada|
|[wpOtcMUdymzpbtC4.htm](boons-and-curses/wpOtcMUdymzpbtC4.htm)|Gruhastha - Minor Boon|Gruhastha - Boon inferior|modificada|
|[wREsKC8CTifEyj6v.htm](boons-and-curses/wREsKC8CTifEyj6v.htm)|Norgorber - Major Curse|Norgorber - Maldición superior|modificada|
|[WVlWzn918RNExxpe.htm](boons-and-curses/WVlWzn918RNExxpe.htm)|Sivanah - Major Boon|Sivanah - Boon superior|modificada|
|[WWyPOtmJc6lDLfgL.htm](boons-and-curses/WWyPOtmJc6lDLfgL.htm)|Groetus - Major Boon|Groetus - Boon superior|modificada|
|[wXAVESzPURB1iBWW.htm](boons-and-curses/wXAVESzPURB1iBWW.htm)|Achaekek - Moderate Curse|Achaekek - Maldición moderada|modificada|
|[wZWklm7jLry2rTOk.htm](boons-and-curses/wZWklm7jLry2rTOk.htm)|Shizuru - Major Curse|Shizuru - Maldición superior|modificada|
|[x1QrKuvZ2TbTvLUY.htm](boons-and-curses/x1QrKuvZ2TbTvLUY.htm)|Cayden Cailean - Moderate Curse|Cayden Cailean - Maldición moderada|modificada|
|[XFd7rnb9cchNw0WN.htm](boons-and-curses/XFd7rnb9cchNw0WN.htm)|Norgorber - Minor Curse|Norgorber - Maldición inferior|modificada|
|[XFiI9qC4MNX4WUdh.htm](boons-and-curses/XFiI9qC4MNX4WUdh.htm)|Lamashtu - Major Boon|Lamashtu - Boon superior|modificada|
|[XfMZ83VA7dtF7fL5.htm](boons-and-curses/XfMZ83VA7dtF7fL5.htm)|Urgathoa - Minor Curse|Urgathoa - Maldición inferior|modificada|
|[Xlu60yqtTXutwG2G.htm](boons-and-curses/Xlu60yqtTXutwG2G.htm)|Tsukiyo - Major Boon|Tsukiyo - Boon superior|modificada|
|[XQg4eq4yIyEO7z3M.htm](boons-and-curses/XQg4eq4yIyEO7z3M.htm)|Brigh - Major Boon|Brigh - Boon superior|modificada|
|[xrczhivVmi7MrEfz.htm](boons-and-curses/xrczhivVmi7MrEfz.htm)|Irori - Moderate Boon|Irori - Boon Moderado|modificada|
|[xrSgGobm5QtTcABQ.htm](boons-and-curses/xrSgGobm5QtTcABQ.htm)|Pharasma - Moderate Boon|Pharasma - Boon Moderado|modificada|
|[XujNRFYY4w1NgArr.htm](boons-and-curses/XujNRFYY4w1NgArr.htm)|Kurgess - Moderate Boon|Kurgess - Boon moderado|modificada|
|[XV9hL75EXA9tIusv.htm](boons-and-curses/XV9hL75EXA9tIusv.htm)|Kazutal - Major Curse|Kazutal - Maldición superior|modificada|
|[xwhPzOXxX1fON3j4.htm](boons-and-curses/xwhPzOXxX1fON3j4.htm)|Achaekek - Moderate Boon|Achaekek - Moderada Boon|modificada|
|[XYM7v72doPR3LwgA.htm](boons-and-curses/XYM7v72doPR3LwgA.htm)|Gozreh - Minor Curse|Gozreh - Maldición inferior|modificada|
|[y736aBzm4RKwB7RQ.htm](boons-and-curses/y736aBzm4RKwB7RQ.htm)|Ghlaunder - Moderate Curse|Ghlaunder - Maldición moderada|modificada|
|[y9DNpX7krUiOq0YI.htm](boons-and-curses/y9DNpX7krUiOq0YI.htm)|Asmodeus - Moderate Curse|Asmodeus - Maldición moderada|modificada|
|[yBoHo1dcZpKygfJE.htm](boons-and-curses/yBoHo1dcZpKygfJE.htm)|Brigh - Minor Curse|Brigh - Maldición inferior|modificada|
|[YeWT4MjQ8vDmSThs.htm](boons-and-curses/YeWT4MjQ8vDmSThs.htm)|Nocticula - Major Curse|Nocticula - Maldición superior|modificada|
|[YHeCTSaw1sOnXhre.htm](boons-and-curses/YHeCTSaw1sOnXhre.htm)|Gruhastha - Major Curse|Gruhastha - Maldición superior|modificada|
|[yoXU4CqZtzgKXdlB.htm](boons-and-curses/yoXU4CqZtzgKXdlB.htm)|Asmodeus - Major Curse|Asmodeus - Maldición superior|modificada|
|[yWASKzumwmv4TgSe.htm](boons-and-curses/yWASKzumwmv4TgSe.htm)|Calistria - Minor Boon|Calistria - Boon inferior|modificada|
|[YXjcQMRNqJbosjan.htm](boons-and-curses/YXjcQMRNqJbosjan.htm)|Grandmother Spider - Moderate Curse|Abuela Araña - Maldición moderada|modificada|
|[YYc9SJ6t3gOMWYDi.htm](boons-and-curses/YYc9SJ6t3gOMWYDi.htm)|Gruhastha - Moderate Boon|Gruhastha - Boon Moderado|modificada|
|[z4GfG3GatAOELKyA.htm](boons-and-curses/z4GfG3GatAOELKyA.htm)|Desna - Major Curse|Desna - Maldición superior|modificada|
|[zMuLnoGROhRhqbDp.htm](boons-and-curses/zMuLnoGROhRhqbDp.htm)|Milani - Minor Curse|Milani - Maldición inferior|modificada|
|[zoJjzir97Ie7tbp0.htm](boons-and-curses/zoJjzir97Ie7tbp0.htm)|Shelyn - Moderate Boon|Shelyn - Boon Moderada|modificada|
|[ZPlvqkjEv9a7J76T.htm](boons-and-curses/ZPlvqkjEv9a7J76T.htm)|Cayden Cailean - Major Boon|Cayden Cailean - Boon superior|modificada|
|[zu2yJaXxOus4tNqd.htm](boons-and-curses/zu2yJaXxOus4tNqd.htm)|Alseta - Major Curse|Alseta - Maldición superior|modificada|
|[zWshZRICuzP7DfFV.htm](boons-and-curses/zWshZRICuzP7DfFV.htm)|Iomedae - Major Curse|Iomedae - Maldición superior|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
