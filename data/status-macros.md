# Estado de la traducción (macros)

 * **modificada**: 9


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[084MOWWSEVNwpHVG.htm](macros/084MOWWSEVNwpHVG.htm)|Perception for Selected Tokens|Percepción para fichas seleccionadas|modificada|
|[0GU2sdy3r2MeC56x.htm](macros/0GU2sdy3r2MeC56x.htm)|Rest for the Night|Descanso para la noche|modificada|
|[6duZj0Ygiqv712rq.htm](macros/6duZj0Ygiqv712rq.htm)|Treat Wounds|Tratar heridas|modificada|
|[aS6F7PSUlS9JM5jr.htm](macros/aS6F7PSUlS9JM5jr.htm)|Take a Breather|Tómate un respiro|modificada|
|[MAHxEeGf31wqv3jp.htm](macros/MAHxEeGf31wqv3jp.htm)|XP|XP|modificada|
|[mxHKWibjPrgfJTDg.htm](macros/mxHKWibjPrgfJTDg.htm)|Earn Income|Obtener ingresos|modificada|
|[NQkc5rKoeFemdVHr.htm](macros/NQkc5rKoeFemdVHr.htm)|Travel Duration|Duración del viaje|modificada|
|[s2sa8lo9dcIA6UGe.htm](macros/s2sa8lo9dcIA6UGe.htm)|Toggle Compendium Browser|Toggle Compendium Browser|modificada|
|[yBuEphSaJJ7V9Yw3.htm](macros/yBuEphSaJJ7V9Yw3.htm)|Stealth for Selected Tokens|Stealth for Selected Tokens|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
