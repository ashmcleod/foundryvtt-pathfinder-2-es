# Estado de la traducción (bestiary-family-ability-glossary)

 * **modificada**: 286
 * **ninguna**: 5


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[hEeHuM3OVzJADsSa.htm](bestiary-family-ability-glossary/hEeHuM3OVzJADsSa.htm)|(Blackfrost Dead) Mindburning Gaze|
|[kLQ0eq18SvKEvggc.htm](bestiary-family-ability-glossary/kLQ0eq18SvKEvggc.htm)|(Blackfrost Dead) Blackfrost|
|[v14HanKkdPGQd7Km.htm](bestiary-family-ability-glossary/v14HanKkdPGQd7Km.htm)|(Blackfrost Dead) Shattering Death|
|[ydrDZ7lqioLPWRlB.htm](bestiary-family-ability-glossary/ydrDZ7lqioLPWRlB.htm)|(Blackfrost Dead) Blackfrost Breath|
|[yR9VLXIjJ2iOX77z.htm](bestiary-family-ability-glossary/yR9VLXIjJ2iOX77z.htm)|(Blackfrost Dead) Ice Climb|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[03knx0BWuYBNciXI.htm](bestiary-family-ability-glossary/03knx0BWuYBNciXI.htm)|(Graveknight) Empty Save for Dust|(Graveknight) Empty Save for Dust|modificada|
|[08egiRxOvMX97XTc.htm](bestiary-family-ability-glossary/08egiRxOvMX97XTc.htm)|(Werecreature) Change Shape|(Hombre-criatura) Cambiar de forma.|modificada|
|[0B9eEetIbeSPhulD.htm](bestiary-family-ability-glossary/0B9eEetIbeSPhulD.htm)|(Divine Warden) Divine Domain Spells|(Guardián Divino) Hechizos de dominio divino.|modificada|
|[0K3vhX14UEPftqu8.htm](bestiary-family-ability-glossary/0K3vhX14UEPftqu8.htm)|(Ghost) Telekinetic Assault|(Fantasma) Asalto telecinético|modificada|
|[0MBZJdTv863X2jwz.htm](bestiary-family-ability-glossary/0MBZJdTv863X2jwz.htm)|(Graveknight) Portentous Glare|(Graveknight) Portentous Glare|modificada|
|[1AnBJbwfdlvzA7SK.htm](bestiary-family-ability-glossary/1AnBJbwfdlvzA7SK.htm)|(Vampire, Jiang-Shi, Minister) Distant Steps|(Vampiro, Jiang-Shi, Ministro) Pasos Lejanos|modificada|
|[1gsgecz7rhvHeiCX.htm](bestiary-family-ability-glossary/1gsgecz7rhvHeiCX.htm)|(Beheaded) Fiendish|(Decapitado) Fiendish|modificada|
|[1p2UcxwgYDQzVqov.htm](bestiary-family-ability-glossary/1p2UcxwgYDQzVqov.htm)|(Beheaded) Furious Headbutt|(Decapitado) Cabezazo Furioso|modificada|
|[268Q7HdtylwpznvG.htm](bestiary-family-ability-glossary/268Q7HdtylwpznvG.htm)|(Vampire, Vetalarana, Basic) Mental Rebirth|(Vampiro, Vetalarana, Básico) Renacimiento Mental|modificada|
|[2BjqIO36k0Y6tgZf.htm](bestiary-family-ability-glossary/2BjqIO36k0Y6tgZf.htm)|(Vampire, Nosferatu Thrall) Fast Healing|(Vampiro, Nosferatu Thrall) Curación rápida|modificada|
|[2GhEuIlgLj02gV9r.htm](bestiary-family-ability-glossary/2GhEuIlgLj02gV9r.htm)|(Skeleton) Rotten|(Esqueleto) Rotten|modificada|
|[2k0X7JdI9MMt4pp0.htm](bestiary-family-ability-glossary/2k0X7JdI9MMt4pp0.htm)|(Zombie) Unholy Speed|(Zombie) Profana Velocidad|modificada|
|[3oajfAIPSf77dSBL.htm](bestiary-family-ability-glossary/3oajfAIPSf77dSBL.htm)|(Secret Society) Get out of Jail|(Sociedad Secreta) Salir de la Cárcel|modificada|
|[3vcDqURRKk0mtdav.htm](bestiary-family-ability-glossary/3vcDqURRKk0mtdav.htm)|(Mana Wastes Mutant) Grasping Tentacle|(Mana Wastes Mutant) Tentáculo Agarrador|modificada|
|[3VN2BLNE5zakeDCM.htm](bestiary-family-ability-glossary/3VN2BLNE5zakeDCM.htm)|(Lich) Metamagic Alteration|(Lich) Metamagia Alteración|modificada|
|[49aAeGbH5p2IJ5Fz.htm](bestiary-family-ability-glossary/49aAeGbH5p2IJ5Fz.htm)|(Kuworsys) Rapid Bombardment|(Kuworsys) Bombardeo Rápido|modificada|
|[4Aj4FWKObfMYoSvh.htm](bestiary-family-ability-glossary/4Aj4FWKObfMYoSvh.htm)|(Ghost) Beatific Appearance|(Fantasma) Apariencia Beatífica|modificada|
|[4AN6IgzsbhjCPYIZ.htm](bestiary-family-ability-glossary/4AN6IgzsbhjCPYIZ.htm)|(Zombie, Shock) Breath Weapon|(Zombie, Electrizante) Arma de Aliento|modificada|
|[4B8O9QBJuDhJkVcz.htm](bestiary-family-ability-glossary/4B8O9QBJuDhJkVcz.htm)|(Vampire, Nosferatu) Dominate|(Vampiro, Nosferatu) Dominar|modificada|
|[4p06ewIsPnWZwuwc.htm](bestiary-family-ability-glossary/4p06ewIsPnWZwuwc.htm)|(Ghost) Lynchpin|(Ghost) Lynchpin|modificada|
|[4pBHIGtTK9yQmZ7h.htm](bestiary-family-ability-glossary/4pBHIGtTK9yQmZ7h.htm)|(Beheaded) Lifesense 60 feet|(Decapitado) Lifesense 60 pies|modificada|
|[4UEZY3G3CiCP78Kx.htm](bestiary-family-ability-glossary/4UEZY3G3CiCP78Kx.htm)|(Visitant) Wrestle|(Visitante) Placar|modificada|
|[5arYoSY5kcRo8TeM.htm](bestiary-family-ability-glossary/5arYoSY5kcRo8TeM.htm)|(Vampire, Vrykolakas) Vrykolakas Vulnerabilities|(Vampiro, Vrykolakas) Vulnerabilidades de Vrykolakas|modificada|
|[5dbzWZbiTyPKgwKS.htm](bestiary-family-ability-glossary/5dbzWZbiTyPKgwKS.htm)|(Graveknight) Graveknight's Curse|(Caballero sepulcral) Maldición del caballero sepulcral.|modificada|
|[5DuBTf37u88IrphJ.htm](bestiary-family-ability-glossary/5DuBTf37u88IrphJ.htm)|(Vampire, Basic) Vampire Weaknesses|(Vampiro, Básico) Vampiro Debilidades|modificada|
|[5lv9r2ubDCov4dFn.htm](bestiary-family-ability-glossary/5lv9r2ubDCov4dFn.htm)|(Ghost) Pyre's Memory|(Fantasma) Pyre's Memory|modificada|
|[6Alm4mLj3ORxCXC2.htm](bestiary-family-ability-glossary/6Alm4mLj3ORxCXC2.htm)|(Ravener) Cowering Fear|(Ravener) Miedo acobardado|modificada|
|[6isn9nqnvfRrC1wW.htm](bestiary-family-ability-glossary/6isn9nqnvfRrC1wW.htm)|(Graveknight) Weapon Master|(Graveknight) Maestro de armas|modificada|
|[7jWANvnDmNcQvFve.htm](bestiary-family-ability-glossary/7jWANvnDmNcQvFve.htm)|(Graveknight) Ruinous Weapons|(Graveknight) Armas de la ruina|modificada|
|[7llQJrvVuCh7KjZO.htm](bestiary-family-ability-glossary/7llQJrvVuCh7KjZO.htm)|(Cryptid, Primeval) Stench|(Cryptid, Primeval) Hedor|modificada|
|[7TYLgIMDGgUfbgGY.htm](bestiary-family-ability-glossary/7TYLgIMDGgUfbgGY.htm)|(Lich) Mask Death|(Lich) Máscara Muerte|modificada|
|[7V3M78uYKXxJTW37.htm](bestiary-family-ability-glossary/7V3M78uYKXxJTW37.htm)|(Castrovelian) Gaseous Adaptation|(Castrovelian) Adaptación gaseosa|modificada|
|[7W6v0oULg9TCz9ym.htm](bestiary-family-ability-glossary/7W6v0oULg9TCz9ym.htm)|(Zombie) Spitting Zombie|(Zombie) Zombie Escupidor|modificada|
|[871jo3ZGnybF6dC8.htm](bestiary-family-ability-glossary/871jo3ZGnybF6dC8.htm)|(Lich) Blasphemous Utterances|(Lich) Proclamaciones blasfemas|modificada|
|[8IQqWkLqzvWA1JRJ.htm](bestiary-family-ability-glossary/8IQqWkLqzvWA1JRJ.htm)|(Vampire, Strigoi Progenitor) Create Spawn|(Vampiro, Progenitor Strigoi) Elaborar Engendro|modificada|
|[8MACWp05F4SacE7E.htm](bestiary-family-ability-glossary/8MACWp05F4SacE7E.htm)|(Vampire, Nosferatu Thrall) Weakness|(Vampiro, Nosferatu Thrall) Debilidad|modificada|
|[8UQWXpYfn9oE1ZHu.htm](bestiary-family-ability-glossary/8UQWXpYfn9oE1ZHu.htm)|(Vampire, Nosferatu) Command Thrall|(Vampiro, Nosferatu) Orden imperiosa Thrall|modificada|
|[97Eo5oA1WeMJA7nR.htm](bestiary-family-ability-glossary/97Eo5oA1WeMJA7nR.htm)|(Skeleton) Grave Eruption|(Esqueleto) Erupción de Tumba|modificada|
|[98twzz56mMGN5Ftw.htm](bestiary-family-ability-glossary/98twzz56mMGN5Ftw.htm)|(Vampire, Vrykolakas) Drink Blood|(Vampiro, brucolaco) Beber Sangre|modificada|
|[9oy2zWxy7Klk9FxR.htm](bestiary-family-ability-glossary/9oy2zWxy7Klk9FxR.htm)|(Skeleton) Blaze|(Esqueleto) Blaze|modificada|
|[9w9hhvqGhrxdAzMH.htm](bestiary-family-ability-glossary/9w9hhvqGhrxdAzMH.htm)|(Secret Society) Connected|(Sociedad secreta) Conexión|modificada|
|[9wX6EFimVFYFZjD7.htm](bestiary-family-ability-glossary/9wX6EFimVFYFZjD7.htm)|(Ulgrem-Axaan) Ichor Coating|(Ulgrem-Axaan) Recubrimiento de Ichor|modificada|
|[a7A4xY3NeWI2Ya4Z.htm](bestiary-family-ability-glossary/a7A4xY3NeWI2Ya4Z.htm)|(Lich) Rejuvenation|(Lich) Rejuvenecimiento|modificada|
|[a7SNHoP22SBoOAmA.htm](bestiary-family-ability-glossary/a7SNHoP22SBoOAmA.htm)|(Cryptid, Rumored) Hybrid Form|(Cryptid, Rumored) Forma híbrida|modificada|
|[aiwlPSmNY9b6Psvd.htm](bestiary-family-ability-glossary/aiwlPSmNY9b6Psvd.htm)|(Ghost) Frightful Moan|(Fantasma) Gemido Espantoso|modificada|
|[AjTkksppymqzCivT.htm](bestiary-family-ability-glossary/AjTkksppymqzCivT.htm)|(Vampire, True) Dominate|(Vampiro, Verdadero) Dominar|modificada|
|[AkkmaixGqzw75h7W.htm](bestiary-family-ability-glossary/AkkmaixGqzw75h7W.htm)|(Graveknight) Eager for Battle|(Graveknight) Ansioso de Batalla|modificada|
|[apVBHJT95t9Fhxpb.htm](bestiary-family-ability-glossary/apVBHJT95t9Fhxpb.htm)|(Zombie) Infested Zombie|(Zombie) Zombie infestado|modificada|
|[AvoSAlqB8mZfjwr9.htm](bestiary-family-ability-glossary/AvoSAlqB8mZfjwr9.htm)|(Skeleton) Aquatic Bones|(Esqueleto) Huesos Acuáticos|modificada|
|[awFTdoQfYva84HkU.htm](bestiary-family-ability-glossary/awFTdoQfYva84HkU.htm)|(Ulgrem-Axaan) Befouling Odor|(Ulgrem-Axaan) Befouling Odor|modificada|
|[AydIHjDNbOZizj5U.htm](bestiary-family-ability-glossary/AydIHjDNbOZizj5U.htm)|(Ghost) Cold Spot|(Fantasma) Punto Frío|modificada|
|[AzvCvgx9SUAB2blo.htm](bestiary-family-ability-glossary/AzvCvgx9SUAB2blo.htm)|(Vampire, Strigoi) Domain of Dusk|(Vampiro, Strigoi) Dominio del Crepúsculo|modificada|
|[b3p8x6sgTa0BOvAb.htm](bestiary-family-ability-glossary/b3p8x6sgTa0BOvAb.htm)|(Lich) Siphon Life|(Lich) Succionar vida|modificada|
|[BBqjQN5Gbe4PWP56.htm](bestiary-family-ability-glossary/BBqjQN5Gbe4PWP56.htm)|(Vampire, Jiang-Shi, Basic) One More Breath|(Vampiro, Jiang-Shi, Básico) Un Aliento Más|modificada|
|[BcSlVpaN72LoQ5BV.htm](bestiary-family-ability-glossary/BcSlVpaN72LoQ5BV.htm)|(Ghost) Site Bound|(Fantasma) Ligado a una ubicación|modificada|
|[BgbSHRdkGH7raOgA.htm](bestiary-family-ability-glossary/BgbSHRdkGH7raOgA.htm)|(Graveknight) Phantom Mount|(Graveknight) Montura Fantasma|modificada|
|[Bqiumr3LEo05d8x1.htm](bestiary-family-ability-glossary/Bqiumr3LEo05d8x1.htm)|(Mana Wastes Mutant) Hungry Maw|(Mutante de los Yermos de Maná) Fauces Hambrientas|modificada|
|[Bqnh5wiXVymfDgTw.htm](bestiary-family-ability-glossary/Bqnh5wiXVymfDgTw.htm)|(Cryptid, Rumored) Burning Eyes|(Cryptid, Rumored) Escozor de ojos|modificada|
|[br5Oup4USIUXQani.htm](bestiary-family-ability-glossary/br5Oup4USIUXQani.htm)|(Vampire, Vrykolakas) Swift Tracker|(Vampiro, brucolaco) Rastreador rápido.|modificada|
|[BSLqIYqxcCVBb2Vp.htm](bestiary-family-ability-glossary/BSLqIYqxcCVBb2Vp.htm)|(Zombie) Unkillable|(Zombie) Unkillable|modificada|
|[bTJnxBQjr7G8yr30.htm](bestiary-family-ability-glossary/bTJnxBQjr7G8yr30.htm)|(Graveknight) Sacrilegious Aura|(Graveknight) Aura Sacrílega|modificada|
|[buyzzomLTrIdVvTU.htm](bestiary-family-ability-glossary/buyzzomLTrIdVvTU.htm)|(Skeleton) Frozen|(Esqueleto) Congelado|modificada|
|[bVZ6KizWVTLJUBXi.htm](bestiary-family-ability-glossary/bVZ6KizWVTLJUBXi.htm)|(Ghast) Paralysis|(Ghast) Parálisis|modificada|
|[c04ICnrzygyFG3PK.htm](bestiary-family-ability-glossary/c04ICnrzygyFG3PK.htm)|(Vampire, Basic) Drink Blood|(Vampiro, Básico) Beber Sangre|modificada|
|[CdjqkgAexKk8khbB.htm](bestiary-family-ability-glossary/CdjqkgAexKk8khbB.htm)|(Zombie) Ankle Biter|(Zombie) Muerde Tobillos|modificada|
|[cfqRc4clMniqQNsl.htm](bestiary-family-ability-glossary/cfqRc4clMniqQNsl.htm)|(Cryptid, Mutant) Explosive End|(Cryptid, Mutant) Fin Explosión|modificada|
|[CxiEpXt7Gw3tSIOh.htm](bestiary-family-ability-glossary/CxiEpXt7Gw3tSIOh.htm)|(Zombie) Rotting Aura|(Zombi) Aura putrefacta|modificada|
|[cYkEpJzpMu3mCrFc.htm](bestiary-family-ability-glossary/cYkEpJzpMu3mCrFc.htm)|(Ghost) Phantasmagoria|(Fantasma) Phantasmagoria|modificada|
|[d6eWuFd6pw6ffvPC.htm](bestiary-family-ability-glossary/d6eWuFd6pw6ffvPC.htm)|(Melfesh Monster) Swell|(Monstruo Melfesh) Hincharse|modificada|
|[DIYZNvVP28U2UnDb.htm](bestiary-family-ability-glossary/DIYZNvVP28U2UnDb.htm)|(Siabrae) Earth Glide|(Siabrae) Deslizamiento de Tierra|modificada|
|[DlHTe9jLssEELY6a.htm](bestiary-family-ability-glossary/DlHTe9jLssEELY6a.htm)|(Divine Warden) Divine Innate Spells|(Guardián divino) Hechizos innatos divinos.|modificada|
|[DWku0nZzkqmYjrQ5.htm](bestiary-family-ability-glossary/DWku0nZzkqmYjrQ5.htm)|(Vampire, Nosferatu) Change Shape|(Vampiro, Nosferatu) Cambiar Forma|modificada|
|[EpDyZ0mG9beLkBna.htm](bestiary-family-ability-glossary/EpDyZ0mG9beLkBna.htm)|(Vampire, Vetalarana, Basic) Drain Thoughts|(Vampiro, Vetalarana, Básico) Drenar Pensamientos|modificada|
|[eQLGYrJhu787pEwc.htm](bestiary-family-ability-glossary/eQLGYrJhu787pEwc.htm)|(Vampire, Strigoi) Shadow Form|(Vampiro, Strigoi) Forma de Sombra|modificada|
|[Es8g7kZrLAuNdiD1.htm](bestiary-family-ability-glossary/Es8g7kZrLAuNdiD1.htm)|(Siabrae) Miasma|(Siabrae) Miasma|modificada|
|[EsbdyB9DwvPvhCCC.htm](bestiary-family-ability-glossary/EsbdyB9DwvPvhCCC.htm)|(Vampire, Nosferatu Overlord) Paralytic Fear|(Vampiro, Nosferatu Overlord) Miedo Paralítico|modificada|
|[eSp48kG7v1GNuGgh.htm](bestiary-family-ability-glossary/eSp48kG7v1GNuGgh.htm)|(Visitant) Vengeful Presence|(Visitante) Presencia vengativa|modificada|
|[EwhjPJGLSW9v1Fbb.htm](bestiary-family-ability-glossary/EwhjPJGLSW9v1Fbb.htm)|(Vampire, Nosferatu) Divine Innate Spells|(Vampiro, Nosferatu) Hechizos innatos divinos.|modificada|
|[eXleBXdAemiEoHA8.htm](bestiary-family-ability-glossary/eXleBXdAemiEoHA8.htm)|(Nymph Queen) Change Shape|(Reina Ninfa) Cambiar Forma|modificada|
|[EyFVoDiReJsBx1rf.htm](bestiary-family-ability-glossary/EyFVoDiReJsBx1rf.htm)|(Lich) Animate Cage|(Lich) Jaula Animada|modificada|
|[Ez8sVqv1EBcJuorK.htm](bestiary-family-ability-glossary/Ez8sVqv1EBcJuorK.htm)|(Zombie) Putrid Stench|(Zombie) Hedor Pútrido|modificada|
|[FA0ri2fAcMa1HgZe.htm](bestiary-family-ability-glossary/FA0ri2fAcMa1HgZe.htm)|(Werecreature) Moon Frenzy|(Hombre-criatura) Frenesí lunar.|modificada|
|[FdUuCIkQxVoTGM78.htm](bestiary-family-ability-glossary/FdUuCIkQxVoTGM78.htm)|(Mana Wastes Mutant) Mirror Thing|(Mutante de Mana Wastes) Mirror Thing|modificada|
|[fh7ar6QrFT8YWgQ9.htm](bestiary-family-ability-glossary/fh7ar6QrFT8YWgQ9.htm)|(Vampire, Strigoi Progenitor) Drink Essence|(Vampiro, Progenitor Strigoi) Beber Esencia|modificada|
|[fmUBaLklyVmNt3VD.htm](bestiary-family-ability-glossary/fmUBaLklyVmNt3VD.htm)|(Graveknight) Clutching Armor|(Graveknight) Armadura de Agarre|modificada|
|[fOpw4k05kayaL13a.htm](bestiary-family-ability-glossary/fOpw4k05kayaL13a.htm)|(Mana Wastes Mutant) Magic Hunger|(Mana Wastes Mutant) Hambre Mágica|modificada|
|[fqZhojt2M5LfSKSH.htm](bestiary-family-ability-glossary/fqZhojt2M5LfSKSH.htm)|(Lich) Drain Soul Cage|(Lich) Drenar jaula de almas.|modificada|
|[FsqVhavMWAoFro1L.htm](bestiary-family-ability-glossary/FsqVhavMWAoFro1L.htm)|(Ravener) Soul Ward|(Ravener) Soul Ward|modificada|
|[fTk5nmm7HHtXOdSo.htm](bestiary-family-ability-glossary/fTk5nmm7HHtXOdSo.htm)|(Lich) Dark Deliverance|(Lich) Salvación tenebrosa.|modificada|
|[fVyoHEO3fSR737M1.htm](bestiary-family-ability-glossary/fVyoHEO3fSR737M1.htm)|(Ghost) Draining Touch|(Toque fantasmal de drenaje.|modificada|
|[FW4KAUHb7r8WkxUc.htm](bestiary-family-ability-glossary/FW4KAUHb7r8WkxUc.htm)|(Ghoul) Paralysis|(Ghoul) Parálisis|modificada|
|[FXHjmH1oce7Z3tZb.htm](bestiary-family-ability-glossary/FXHjmH1oce7Z3tZb.htm)|(Vampire, Basic) Coffin Restoration|(Vampiro, Básico) Restablecimiento de ataúd|modificada|
|[fYDrunTldWmFvfjl.htm](bestiary-family-ability-glossary/fYDrunTldWmFvfjl.htm)|(Ravener) Soulsense|(Ravener) Sentido de las almas|modificada|
|[G4yIjHMzD0KVpaCm.htm](bestiary-family-ability-glossary/G4yIjHMzD0KVpaCm.htm)|(Melfesh Monster) Burrowing Grasp|(Monstruo Melfesh) Agarre de Madriguera|modificada|
|[ga0Oj7mmjSwWQgmR.htm](bestiary-family-ability-glossary/ga0Oj7mmjSwWQgmR.htm)|(Ghost) Corrupting Gaze|(Fantasma) Mirada Corruptora|modificada|
|[gbKpCw21tEmehq6e.htm](bestiary-family-ability-glossary/gbKpCw21tEmehq6e.htm)|(Siabrae) Rejuvenation|(Siabrae) Rejuvenecimiento|modificada|
|[GD1ZD4Rl2hTPhvjL.htm](bestiary-family-ability-glossary/GD1ZD4Rl2hTPhvjL.htm)|(Zombie, Shock) Arcing Strikes|(Zombie, Electrizante) Golpes con Arco|modificada|
|[GgmLWLBLGaFvunTS.htm](bestiary-family-ability-glossary/GgmLWLBLGaFvunTS.htm)|(Vampire, Vetalarana, Basic) Thoughtsense 100 feet|(Vampiro, Vetalarana, Básico) Percibir pensamientos 100 pies.|modificada|
|[GpKQH8hnSYWiyuQU.htm](bestiary-family-ability-glossary/GpKQH8hnSYWiyuQU.htm)|(Vampire, Nosferatu) Drink Blood|(Vampiro, Nosferatu) Beber Sangre|modificada|
|[gSXxJ2FYEGrX1Psy.htm](bestiary-family-ability-glossary/gSXxJ2FYEGrX1Psy.htm)|(Secret Society) Not Today!|(Sociedad Secreta) ¡Hoy no!|modificada|
|[h2C99bXIwPGRdZQ0.htm](bestiary-family-ability-glossary/h2C99bXIwPGRdZQ0.htm)|(Castrovelian) Extra Large|(Castrovelian) Extra Grande|modificada|
|[hA6HsM4i4yPfEsDH.htm](bestiary-family-ability-glossary/hA6HsM4i4yPfEsDH.htm)|(Ghast) Ghast Fever|(Ghast) Fiebre del ghast|modificada|
|[haMVGGDY3mLuKy9s.htm](bestiary-family-ability-glossary/haMVGGDY3mLuKy9s.htm)|(Vampire, Vrykolakas Master) Bubonic Plague|(Vampiro, Maestro brucolaco) Peste bubónica|modificada|
|[HdSaMUb4uEsbtQTn.htm](bestiary-family-ability-glossary/HdSaMUb4uEsbtQTn.htm)|(Skeleton) Skeleton of Roses|(Esqueleto) Esqueleto de Rosas|modificada|
|[HEmOAVbJ3T9pon6T.htm](bestiary-family-ability-glossary/HEmOAVbJ3T9pon6T.htm)|(Beheaded) Entangling|(Decapitado) Enredar|modificada|
|[hEWAzlIoJg3NzA7Q.htm](bestiary-family-ability-glossary/hEWAzlIoJg3NzA7Q.htm)|(Spring-Heeled Jack) Change Shape|(Spring-Heeled Jack) Cambiar de forma|modificada|
|[hfT7YDwx0UX4expm.htm](bestiary-family-ability-glossary/hfT7YDwx0UX4expm.htm)|(Lich) Steal Soul|(Lich) Sustraer Alma|modificada|
|[HKFoOZZV4WdjkeeJ.htm](bestiary-family-ability-glossary/HKFoOZZV4WdjkeeJ.htm)|(Protean) Warpwave|(Protean) Warpwave|modificada|
|[HLPLYJ9bOazb2ZPX.htm](bestiary-family-ability-glossary/HLPLYJ9bOazb2ZPX.htm)|(Ravener) Discorporate|(Ravener) Discorporate|modificada|
|[hOOXkWcTw9EFKJev.htm](bestiary-family-ability-glossary/hOOXkWcTw9EFKJev.htm)|(Vampire, Nosferatu) Plagued Coffin Restoration|(Vampiro, Nosferatu) Restablecimiento de ataúd plagado|modificada|
|[hwrZQSADT36TjDdv.htm](bestiary-family-ability-glossary/hwrZQSADT36TjDdv.htm)|(Vampire, Jiang-Shi, Basic) Warped Fulu|(Vampiro, Jiang-Shi, Básico) Warped Fulu|modificada|
|[Hy7bO4TSNoQNMzA6.htm](bestiary-family-ability-glossary/Hy7bO4TSNoQNMzA6.htm)|(Mana Wastes Mutant) Energy Blast|(Mana Wastes Mutant) Explosión de Energía|modificada|
|[i4WBOAb7CmY53doM.htm](bestiary-family-ability-glossary/i4WBOAb7CmY53doM.htm)|(Melfesh Monster) False Synapses|(Monstruo Melfesh) Sinapsis Falsa|modificada|
|[i7m74TphAiFYvzPL.htm](bestiary-family-ability-glossary/i7m74TphAiFYvzPL.htm)|(Ghost) Ghost Storm|(Fantasma) Tormenta Fantasma|modificada|
|[iAXHLkxuuCUOwqkN.htm](bestiary-family-ability-glossary/iAXHLkxuuCUOwqkN.htm)|(Werecreature) Animal Empathy|(Hombre-criatura) Empatía animal.|modificada|
|[ICnpftxZEilrYjn0.htm](bestiary-family-ability-glossary/ICnpftxZEilrYjn0.htm)|(Werecreature) Curse of the Werecreature|(Werecreature) Maldición de la Werecreature|modificada|
|[IFQd4GiHlV33p7oK.htm](bestiary-family-ability-glossary/IFQd4GiHlV33p7oK.htm)|(Nymph Queen) Inspiration|(Reina Ninfa) Inspiración|modificada|
|[ii98NEWkpvIJXgFZ.htm](bestiary-family-ability-glossary/ii98NEWkpvIJXgFZ.htm)|(Zombie, Shock) Lightning Rod|(Zombie, Electrizante) Pararrayos|modificada|
|[iiIhLkjuPYJ93Upw.htm](bestiary-family-ability-glossary/iiIhLkjuPYJ93Upw.htm)|(Melfesh Monster) Smolderstench|(Monstruo Melfesh) Smolderstench|modificada|
|[IL8wtCi23ch62zXG.htm](bestiary-family-ability-glossary/IL8wtCi23ch62zXG.htm)|(Vampire, Nosferatu Thrall) Rally|(Vampiro, Nosferatu Thrall) Rally|modificada|
|[IOk0MRs3f9FrarKL.htm](bestiary-family-ability-glossary/IOk0MRs3f9FrarKL.htm)|(Protean) Protean Anatomy|(Protean) Anatomía Protean|modificada|
|[ipGBYIXk4u47Mp1D.htm](bestiary-family-ability-glossary/ipGBYIXk4u47Mp1D.htm)|(Zombie) Feast|(Zombie) Festín|modificada|
|[iwLj14liESK5OBN8.htm](bestiary-family-ability-glossary/iwLj14liESK5OBN8.htm)|(Ghost) Malevolent Possession|(Fantasma) Posesión maléfica.|modificada|
|[IxJbFJ8dG5RbZWBD.htm](bestiary-family-ability-glossary/IxJbFJ8dG5RbZWBD.htm)|(Graveknight) Devastating Blast|(Graveknight) Explosión Devastadora|modificada|
|[ixPqVlqLaYTB1b23.htm](bestiary-family-ability-glossary/ixPqVlqLaYTB1b23.htm)|(Ghoul) Consume Flesh|(Ghoul) Consumir carne|modificada|
|[jciRdEHgGNGFXLsg.htm](bestiary-family-ability-glossary/jciRdEHgGNGFXLsg.htm)|(Siabrae) Blight Mastery|(Siabrae) Maestría asolar|modificada|
|[jD0M3yV6gjkXafsJ.htm](bestiary-family-ability-glossary/jD0M3yV6gjkXafsJ.htm)|(Divine Warden) Divine Destruction|(Guardián divino) Destrucción divina.|modificada|
|[jjWisLWwcdxsqv8o.htm](bestiary-family-ability-glossary/jjWisLWwcdxsqv8o.htm)|(Kallas Devil) Blameless|(Diablo de Kallas) Intachable|modificada|
|[JSBmboE6bYVxDT9d.htm](bestiary-family-ability-glossary/JSBmboE6bYVxDT9d.htm)|(Ghost) Inhabit Object|(Fantasma) Habitar objeto|modificada|
|[jsZZJDd4ZuYMlEVV.htm](bestiary-family-ability-glossary/jsZZJDd4ZuYMlEVV.htm)|(Coven) Share Senses|(Aquelarre) Compartir Sentidos|modificada|
|[jTuK430yY8VgIByB.htm](bestiary-family-ability-glossary/jTuK430yY8VgIByB.htm)|(Ghost) Haunted House|(Ghost) Haunted House|modificada|
|[K0scCV18j5FzM2ei.htm](bestiary-family-ability-glossary/K0scCV18j5FzM2ei.htm)|(Cryptid, Rumored) Shifting Form|(Cryptid, Rumored) Forma cambiante.|modificada|
|[K5bNE90vmeFzktnM.htm](bestiary-family-ability-glossary/K5bNE90vmeFzktnM.htm)|(Skeleton) Collapse|(Esqueleto) Colapso|modificada|
|[KcUHCVhHnkMD8j3k.htm](bestiary-family-ability-glossary/KcUHCVhHnkMD8j3k.htm)|(Greater Barghest) Mutation - Toxic Breath|(Mayor Barghest) Mutación - Aliento Tóxico|modificada|
|[kew9yPbf83smsCyL.htm](bestiary-family-ability-glossary/kew9yPbf83smsCyL.htm)|(Mana Wastes Mutant) Energy Resistance|(Mutante de los yacimientos de maná) Resistencia a la energía.|modificada|
|[kG4fDd16fYEFvmgy.htm](bestiary-family-ability-glossary/kG4fDd16fYEFvmgy.htm)|(Nymph Queen) Nymph's Beauty|(Nymph Queen) Belleza de Ninfa|modificada|
|[KJrQdJQ0ZEmKYH4Y.htm](bestiary-family-ability-glossary/KJrQdJQ0ZEmKYH4Y.htm)|(Kallas Devil) Suffer the Children|(Kallas Devil) Suffer the Children|modificada|
|[KLMdplDgOfXSLh6g.htm](bestiary-family-ability-glossary/KLMdplDgOfXSLh6g.htm)|(Cryptid, Rumored) Howl|(Cryptid, Rumored) Aullido|modificada|
|[KOnVQPlRY1CqcJXy.htm](bestiary-family-ability-glossary/KOnVQPlRY1CqcJXy.htm)|(Beheaded) Bleeding|(Decapitado) Sangrado|modificada|
|[kQZ6pzdSn6FaxWF2.htm](bestiary-family-ability-glossary/kQZ6pzdSn6FaxWF2.htm)|(Visitant) Visitant Spells|(Visitante) Visitante Hechizos|modificada|
|[kUApLn0cOsXQNSrL.htm](bestiary-family-ability-glossary/kUApLn0cOsXQNSrL.htm)|(Clockwork Creature) Malfunction - Damaged Propulsion|(Criatura Mecánica) Avería - Propulsión Dañada.|modificada|
|[KwmYKsaXHKfZgB2c.htm](bestiary-family-ability-glossary/KwmYKsaXHKfZgB2c.htm)|(Spring-Heeled Jack) Vanishing Leap|(Spring-Heeled Jack) Salto sin carrerilla|modificada|
|[l2ov5uPpfOAoyXAL.htm](bestiary-family-ability-glossary/l2ov5uPpfOAoyXAL.htm)|(Cryptid, Rumored) Obscura Vulnerability|(Cryptid, Rumored) Vulnerabilidad Obscura|modificada|
|[l5FyTQQ0OfICCS1c.htm](bestiary-family-ability-glossary/l5FyTQQ0OfICCS1c.htm)|(Cryptid, Primeval) Shockwave|(Cryptid, Primeval) Onda de Choque|modificada|
|[L6EypYQTdK4XPldM.htm](bestiary-family-ability-glossary/L6EypYQTdK4XPldM.htm)|(Secret Society) Prepared Trap|(Sociedad Secreta) Trampa Preparada|modificada|
|[L7kL2ps5k80XLtwV.htm](bestiary-family-ability-glossary/L7kL2ps5k80XLtwV.htm)|(Vampire, Jiang-Shi, Minister) Tumult of the Blood|(Vampiro, Jiang-Shi, Ministro) Tumulto de la Sangre|modificada|
|[L80gn6WOWi9roJW3.htm](bestiary-family-ability-glossary/L80gn6WOWi9roJW3.htm)|(Divine Warden) Instrument of Faith|(Guardián divino) Instrumento de fe.|modificada|
|[lAnJ25DSs44Ya8jg.htm](bestiary-family-ability-glossary/lAnJ25DSs44Ya8jg.htm)|(Ghost) Fade|(Fantasma) Fade|modificada|
|[lD9Onw05RxdcmM2e.htm](bestiary-family-ability-glossary/lD9Onw05RxdcmM2e.htm)|(Mana Wastes Mutant) Revolting Appearance|(Mutante de los Yacimientos de Maná) Aspecto Revoltoso|modificada|
|[LKxsPOf0hAS32Sp8.htm](bestiary-family-ability-glossary/LKxsPOf0hAS32Sp8.htm)|(Vampire, Nosferatu Thrall) Mindbound|(Vampiro, Nosferatu Thrall) Mindbound|modificada|
|[LOd8QwaKP3hnyGkc.htm](bestiary-family-ability-glossary/LOd8QwaKP3hnyGkc.htm)|(Kallas Devil) Cold Currents|(Kallas Devil) Corrientes Frías|modificada|
|[lv1T0kAmG2JS7PWs.htm](bestiary-family-ability-glossary/lv1T0kAmG2JS7PWs.htm)|(Mana Wastes Mutant) Sprouted Limb|(Mutante de los yacimientos de maná) Extremidad germinación.|modificada|
|[m6teF5ADh7vuM8Zr.htm](bestiary-family-ability-glossary/m6teF5ADh7vuM8Zr.htm)|(Ghast) Consume Flesh|(Ghast) Consumir carne|modificada|
|[MAC97gjFcdiqLyhp.htm](bestiary-family-ability-glossary/MAC97gjFcdiqLyhp.htm)|(Skeleton) Screaming Skull|(Esqueleto) Cráneo Gritón|modificada|
|[mEbrInCpag7YThH2.htm](bestiary-family-ability-glossary/mEbrInCpag7YThH2.htm)|(Dragon) Change Shape|(Dragón) Cambiar Forma de dragón|modificada|
|[MhymIeTQoxbacG1o.htm](bestiary-family-ability-glossary/MhymIeTQoxbacG1o.htm)|(Zombie) Plague-Ridden|(Zombie) Plague-Ridden|modificada|
|[mmotrGIfshEHi8Rr.htm](bestiary-family-ability-glossary/mmotrGIfshEHi8Rr.htm)|(Mana Wastes Mutant) Chameleon Skin|(Mana Wastes Mutant) Piel de Camaleón|modificada|
|[mqai5e7YAuK2tbB9.htm](bestiary-family-ability-glossary/mqai5e7YAuK2tbB9.htm)|(Mana Wastes Mutant) Vengeful Bite|(Mutante de los Yermos de Maná) Muerda vengativa|modificada|
|[mwEig0MYM7EIibSU.htm](bestiary-family-ability-glossary/mwEig0MYM7EIibSU.htm)|(Greater Barghest) Mutation - Vestigial Arm Strike|(Barghest mayor) Mutación - Golpe de brazo vestigial.|modificada|
|[mZpX54PgTxPta5y4.htm](bestiary-family-ability-glossary/mZpX54PgTxPta5y4.htm)|(Graveknight) Graveknight's Shield|(Graveknight) Escudo de Graveknight|modificada|
|[N7UV5CZXtcoxDxCF.htm](bestiary-family-ability-glossary/N7UV5CZXtcoxDxCF.htm)|(Ghoul) Ghoul Fever|(Ghoul) Ghoul Fever|modificada|
|[N9OUII3LfRr6hNP8.htm](bestiary-family-ability-glossary/N9OUII3LfRr6hNP8.htm)|(Cryptid, Rumored) Vanishing Escape|(Cryptid, Rumored) Desaparecido Huir|modificada|
|[na1WJDEaoqpcQuOR.htm](bestiary-family-ability-glossary/na1WJDEaoqpcQuOR.htm)|(Kallas Devil) Infectious Water|(Diablo de Kallas) Agua infecciosa|modificada|
|[ndN9zEIheRZGOEUW.htm](bestiary-family-ability-glossary/ndN9zEIheRZGOEUW.htm)|(Secret Society) Tag Team|(Sociedad Secreta) Tag Team|modificada|
|[nF7RKPONY5H9kEIo.htm](bestiary-family-ability-glossary/nF7RKPONY5H9kEIo.htm)|(Ghost) Memento Mori|(Fantasma) Memento Mori|modificada|
|[NgiwaeUqMPfkYvQq.htm](bestiary-family-ability-glossary/NgiwaeUqMPfkYvQq.htm)|(Vampire, Nosferatu) Plague of Ancients|(Vampiro, Nosferatu) Plaga de los Antiguos|modificada|
|[Nnl5wg6smOzieTop.htm](bestiary-family-ability-glossary/Nnl5wg6smOzieTop.htm)|(Vampire, True) Turn to Mist|(Vampiro, Verdadero) Convertirse en Niebla|modificada|
|[noOXyIXmwYN2rRd1.htm](bestiary-family-ability-glossary/noOXyIXmwYN2rRd1.htm)|(Vampire, True) Children of the Night|(Vampiro, Verdadero) Hijos de la Noche|modificada|
|[NSO2l1jXK32oTnVP.htm](bestiary-family-ability-glossary/NSO2l1jXK32oTnVP.htm)|(Kallas Devil) Freezing Touch|(Kallas Devil) Freezing Touch|modificada|
|[nxF03w5vKrw1jmxQ.htm](bestiary-family-ability-glossary/nxF03w5vKrw1jmxQ.htm)|(Lich) Familiar Soul|(Lich) Alma Familiar|modificada|
|[O6AXB9aZB0K14sS5.htm](bestiary-family-ability-glossary/O6AXB9aZB0K14sS5.htm)|(Vampire, Jiang-Shi, Basic) Rigor Mortis|(Vampiro, Jiang-Shi, Básico) Rigor Mortis|modificada|
|[oD1cA9uXYQ8evT8f.htm](bestiary-family-ability-glossary/oD1cA9uXYQ8evT8f.htm)|(Melfesh Monster) Vent Flames|(Monstruo Melfesh) Expulsa gases.|modificada|
|[OD3VPUalSKDTYFmF.htm](bestiary-family-ability-glossary/OD3VPUalSKDTYFmF.htm)|(Greater Barghest) Mutation - Wings|(Mayor Barghest) Mutación - Alas|modificada|
|[OeLdzrhIrDOvsm3E.htm](bestiary-family-ability-glossary/OeLdzrhIrDOvsm3E.htm)|(Lich) Unholy Touch|(Lich) Toque Profano|modificada|
|[OMSsLUcnRj6ycEUa.htm](bestiary-family-ability-glossary/OMSsLUcnRj6ycEUa.htm)|(Graveknight) Channel Magic|(Graveknight) Canalizar Magia|modificada|
|[OPGu0WsUHpHSmu80.htm](bestiary-family-ability-glossary/OPGu0WsUHpHSmu80.htm)|(Divine Warden) Faith Bound|(Guardián Divino) Fe Atada|modificada|
|[OQt7nHInDBAmHaG6.htm](bestiary-family-ability-glossary/OQt7nHInDBAmHaG6.htm)|(Vampire, Nosferatu Thrall) Mortal Shield|(Vampiro, Nosferatu Thrall) Escudo Mortal|modificada|
|[OSnNiMdqgARyEVuv.htm](bestiary-family-ability-glossary/OSnNiMdqgARyEVuv.htm)|(Vampire, Jiang-Shi, Basic) Breathsense 60 feet|(Vampiro, Jiang-Shi, Básico) Sentido del Aliento 60 pies|modificada|
|[oXRnrQQ04oi8OkDG.htm](bestiary-family-ability-glossary/oXRnrQQ04oi8OkDG.htm)|(Vampire, Vrykolakas) Feral Possession|(Vampiro, brucolaco) Posesión feral.|modificada|
|[P5YTG6I8ci4lwhZ1.htm](bestiary-family-ability-glossary/P5YTG6I8ci4lwhZ1.htm)|(Skeleton) Bloody|(Esqueleto) Sangriento|modificada|
|[pEJkjqjjBXj4YjYZ.htm](bestiary-family-ability-glossary/pEJkjqjjBXj4YjYZ.htm)|(Kuworsys) Careless Block|(Kuworsys) Bloque descuidado|modificada|
|[PjYwPIUUrirQFiee.htm](bestiary-family-ability-glossary/PjYwPIUUrirQFiee.htm)|(Kallas Devil) Underwater Views|(Kallas Devil) Vistas submarinas|modificada|
|[px2XMvw2s5RLV0X1.htm](bestiary-family-ability-glossary/px2XMvw2s5RLV0X1.htm)|(Lich) Aura of Rot|(Lich) Aura de putrefacción|modificada|
|[pxiSbjfWaKCG1xLD.htm](bestiary-family-ability-glossary/pxiSbjfWaKCG1xLD.htm)|(Graveknight) Betrayed Revivification|(Graveknight) Revivificación por traición traicionada.|modificada|
|[pyBxJKGeheGA8et4.htm](bestiary-family-ability-glossary/pyBxJKGeheGA8et4.htm)|(Clockwork Creature) Malfunction - Backfire|(Criatura Mecánica) Avería - Contragolpe|modificada|
|[Qf94y985g0o6lEoN.htm](bestiary-family-ability-glossary/Qf94y985g0o6lEoN.htm)|(Zombie) Tearing Grapple|(Zombie) Presa Desgarradora|modificada|
|[qfDwBrCXeIYp0W8T.htm](bestiary-family-ability-glossary/qfDwBrCXeIYp0W8T.htm)|(Cryptid, Rumored) Creature Obscura|(Cryptid, Rumored) Criatura Obscura|modificada|
|[qjgrMhwz78T31kLU.htm](bestiary-family-ability-glossary/qjgrMhwz78T31kLU.htm)|(Vampire, Strigoi) Strigoi Weaknesses|(Vampiro, Strigoi) Debilidades Strigoi|modificada|
|[qlNOqsfJH2gj7DCs.htm](bestiary-family-ability-glossary/qlNOqsfJH2gj7DCs.htm)|(Kallas Devil) Waterfall Torrent|(Kallas Devil) Waterfall Torrent|modificada|
|[QlrbnkeZu6M4kvOy.htm](bestiary-family-ability-glossary/QlrbnkeZu6M4kvOy.htm)|(Worm That Walks) Discorporate|(Gusano Que Camina) Discorporate|modificada|
|[QMlQ5AvcunCvjlfM.htm](bestiary-family-ability-glossary/QMlQ5AvcunCvjlfM.htm)|(Vampire, Jiang-Shi, Basic) Drain Qi|(Vampiro, Jiang-Shi, Básico) Drenar Qi|modificada|
|[QMWvYf8Qowj2Fzmx.htm](bestiary-family-ability-glossary/QMWvYf8Qowj2Fzmx.htm)|(Graveknight) Dark Deliverance|(Graveknight) Salvación tenebrosa.|modificada|
|[qs21GajPmFYel3pc.htm](bestiary-family-ability-glossary/qs21GajPmFYel3pc.htm)|(Melfesh Monster) Corpse Bomb|(Monstruo Melfesh) Bomba Cadáver|modificada|
|[qt2exWwQTzoObKfW.htm](bestiary-family-ability-glossary/qt2exWwQTzoObKfW.htm)|(Golem) Inexorable March|(Golem) Marcha inexorable|modificada|
|[QuHeuVNDnmOCS9M1.htm](bestiary-family-ability-glossary/QuHeuVNDnmOCS9M1.htm)|(Vampire, Vrykolakas Master) Create Spawn|(Vampiro, Maestro brucolaco) Elaborar engendro.|modificada|
|[qYVKpytVx46oBVox.htm](bestiary-family-ability-glossary/qYVKpytVx46oBVox.htm)|(Cryptid, Experimental) Energy Wave|(Cryptid, Experimental) Energy Wave|modificada|
|[qZnZriRRtjK7FtP8.htm](bestiary-family-ability-glossary/qZnZriRRtjK7FtP8.htm)|(Zombie) Persistent Limbs|(Zombie) Extremidades Persistentes|modificada|
|[R0tsWv6QHd2jbQON.htm](bestiary-family-ability-glossary/R0tsWv6QHd2jbQON.htm)|(Cryptid, Primeval) Grasp for Life|(Cryptid, Primeval) Aferrarse a la vida|modificada|
|[R1vYhCJ2KvT8uAy1.htm](bestiary-family-ability-glossary/R1vYhCJ2KvT8uAy1.htm)|(Vampire, Jiang-Shi, Basic) Jiang-Shi Vulnerabilities|(Vampiro, Jiang-Shi, Básico) Vulnerabilidades Jiang-Shi|modificada|
|[r34QDwKiWZoVymJa.htm](bestiary-family-ability-glossary/r34QDwKiWZoVymJa.htm)|(Golem) Golem Antimagic|(Golem) Antimagia de gólem|modificada|
|[R5VgwNbwc3QbqVXC.htm](bestiary-family-ability-glossary/R5VgwNbwc3QbqVXC.htm)|(Vampire, Strigoi Progenitor) Shadow Escape|(Vampiro, Strigoi Progenitor) Sombra Huir|modificada|
|[rBc9fJlMXhzvn05L.htm](bestiary-family-ability-glossary/rBc9fJlMXhzvn05L.htm)|(Vampire, Strigoi) Drink Essence|(Vampiro, Strigoi) Beber Esencia|modificada|
|[rdQzd5ClgGPP4Qmt.htm](bestiary-family-ability-glossary/rdQzd5ClgGPP4Qmt.htm)|(Skeleton) Nimble|(Esqueleto) Ágil|modificada|
|[RgYpzTk3XapgVeXZ.htm](bestiary-family-ability-glossary/RgYpzTk3XapgVeXZ.htm)|(Skeleton) Bone Missile|(Esqueleto) Misil Óseo|modificada|
|[rlNiZiUb9pRKFKQo.htm](bestiary-family-ability-glossary/rlNiZiUb9pRKFKQo.htm)|(Beheaded) Giant|(Decapitado) Gigante|modificada|
|[RQ9NaIerG95wkhjl.htm](bestiary-family-ability-glossary/RQ9NaIerG95wkhjl.htm)|(Ulgrem-Axaan) Fallen Victim|(Ulgrem-Axaan) Víctima Caída|modificada|
|[RvzFTym3r22VhMrm.htm](bestiary-family-ability-glossary/RvzFTym3r22VhMrm.htm)|(Kuworsys) Smash and Grab|(Kuworsys) Aplastar y agarrar|modificada|
|[rwEFqSLw4yIscGrO.htm](bestiary-family-ability-glossary/rwEFqSLw4yIscGrO.htm)|(Vampire, Vetalarana, Basic) Vetalarana Vulnerabilities|(Vampiro, Vetalarana, Básico) Vulnerabilidades de Vetalarana|modificada|
|[s6JRMjgA7hSGUAYX.htm](bestiary-family-ability-glossary/s6JRMjgA7hSGUAYX.htm)|(Vampire, Vrykolakas Master) Pestilential Aura|(Vampiro, Maestro brucolaco) Aura pestilente.|modificada|
|[sajLbVE6VpCsR0Kl.htm](bestiary-family-ability-glossary/sajLbVE6VpCsR0Kl.htm)|(Skeleton) Bone Powder|(Esqueleto) Polvo de Hueso|modificada|
|[sAyR4uJbsWukZFZf.htm](bestiary-family-ability-glossary/sAyR4uJbsWukZFZf.htm)|(Vampire, True) Mist Escape|(Vampiro, Verdadero) Niebla Huir|modificada|
|[sECjzfaYaW68vbLV.htm](bestiary-family-ability-glossary/sECjzfaYaW68vbLV.htm)|(Vampire, Vrykolakas Master) Change Shape|(Vampiro, Maestro brucolaco) Cambiar de forma.|modificada|
|[SEmSk1INZDmeoB5R.htm](bestiary-family-ability-glossary/SEmSk1INZDmeoB5R.htm)|(Nymph Queen) Focus Beauty|(Reina ninfa) Belleza de enfoque|modificada|
|[SEU4K3QRAUHEMRl2.htm](bestiary-family-ability-glossary/SEU4K3QRAUHEMRl2.htm)|(Cryptid, Mutant) Unusual Bane|(Criptido, Mutante) Perdición inusual.|modificada|
|[SEzkqVJxr2eJDsuJ.htm](bestiary-family-ability-glossary/SEzkqVJxr2eJDsuJ.htm)|(Ghast) Stench|(Ghast) Hedor|modificada|
|[SFDNK6AauPO6io5B.htm](bestiary-family-ability-glossary/SFDNK6AauPO6io5B.htm)|(Mana Wastes Mutant) Eldritch Attraction|(Mana Wastes Mutant) Eldritch Attraction|modificada|
|[Sj1IGDjUmlwFpC35.htm](bestiary-family-ability-glossary/Sj1IGDjUmlwFpC35.htm)|(Vampire, Nosferatu) Nosferatu Vulnerabilities|(Vampiro, Nosferatu) Vulnerabilidades Nosferatu|modificada|
|[sWDlb5vJRK092MMt.htm](bestiary-family-ability-glossary/sWDlb5vJRK092MMt.htm)|(Vampire, Vetalarana, Manipulator) Control Comatose|(Vampiro, Vetalarana, Manipulador) Control Comatoso|modificada|
|[tDfv2oEPal19NtSM.htm](bestiary-family-ability-glossary/tDfv2oEPal19NtSM.htm)|(Ghost) Fetch|(Fantasma) Buscar|modificada|
|[tdqw4QXzA2x7fDLT.htm](bestiary-family-ability-glossary/tdqw4QXzA2x7fDLT.htm)|(Lich) Pillage Mind|(Lich) Pillage Mind|modificada|
|[TIJ7r9lXvNYSIMLI.htm](bestiary-family-ability-glossary/TIJ7r9lXvNYSIMLI.htm)|(Siabrae) Stone Antlers|(Siabrae) Cornamenta de Piedra|modificada|
|[tKixfds07IRH2nnh.htm](bestiary-family-ability-glossary/tKixfds07IRH2nnh.htm)|(Vampire, Vrykolakas Master) Children of the Night|(Vampiro, Maestro brucolaco) Hijos de la noche.|modificada|
|[tQY2xmcIhzCM7oCC.htm](bestiary-family-ability-glossary/tQY2xmcIhzCM7oCC.htm)|(Melfesh Monster) Torrential Advance|(Monstruo Melfesh) Avance Torrencial|modificada|
|[trYchslD8fLokkT9.htm](bestiary-family-ability-glossary/trYchslD8fLokkT9.htm)|(Mana Wastes Mutant) Hulking Form|(Mana Wastes Mutant) Forma Hulking|modificada|
|[uDjn2b2ZrZycQQyv.htm](bestiary-family-ability-glossary/uDjn2b2ZrZycQQyv.htm)|(Cryptid, Experimental) Clobber|(Cryptid, Experimental) Clobber|modificada|
|[UHuA1hue5xrRM6KK.htm](bestiary-family-ability-glossary/UHuA1hue5xrRM6KK.htm)|(Nymph Queen) Tied to the Land|(Reina Ninfa) Atada a la Tierra|modificada|
|[UlKGxPaKtRGWuq3V.htm](bestiary-family-ability-glossary/UlKGxPaKtRGWuq3V.htm)|(Divine Warden) Faithful Weapon|(Guardián divino) Arma fiel.|modificada|
|[uMedzgKYui5X3Qtn.htm](bestiary-family-ability-glossary/uMedzgKYui5X3Qtn.htm)|(Vampire, Nosferatu Overlord) Divine Innate Spells|(Vampiro, Nosferatu Overlord) Hechizos innatos divinos.|modificada|
|[unR8VVR4yyRnsmnB.htm](bestiary-family-ability-glossary/unR8VVR4yyRnsmnB.htm)|(Ghost) Rejuvenation|(Fantasma) Rejuvenecimiento|modificada|
|[UOqq8lkBPcPtKoCN.htm](bestiary-family-ability-glossary/UOqq8lkBPcPtKoCN.htm)|(Melfesh Monster) Flame Lash|(Monstruo Melfesh) Latigazo de llamasígera.|modificada|
|[uQ7cI0oerU7lDLhT.htm](bestiary-family-ability-glossary/uQ7cI0oerU7lDLhT.htm)|(Cryptid, Experimental) Power Surge|(Cryptid, Experimental) Oleaje de Poder|modificada|
|[UsWJ13sDyOgOGWvm.htm](bestiary-family-ability-glossary/UsWJ13sDyOgOGWvm.htm)|(Cryptid, Experimental) Augmented|(Cryptid, Experimental) Aumentada|modificada|
|[UU1Fp3PRuTFONjC9.htm](bestiary-family-ability-glossary/UU1Fp3PRuTFONjC9.htm)|(Ghoul) Swift Leap|(Ghoul) Salto veloz|modificada|
|[uV4jf2pkMRGLdhJX.htm](bestiary-family-ability-glossary/uV4jf2pkMRGLdhJX.htm)|(Ghost) Dreamwalker|(Fantasma) Caminante de Sueños|modificada|
|[v6he7HLxYGzaMnvL.htm](bestiary-family-ability-glossary/v6he7HLxYGzaMnvL.htm)|(Zombie) Disgusting Pustules|(Zombie) Pústulas Repugnantes|modificada|
|[VLCECgjkL5bNOxpx.htm](bestiary-family-ability-glossary/VLCECgjkL5bNOxpx.htm)|(Ravener) Consume Soul|(Ravener) Consumir alma|modificada|
|[vOocS1EiRCXPtbgB.htm](bestiary-family-ability-glossary/vOocS1EiRCXPtbgB.htm)|(Visitant) Roar|(Visitante) Roar|modificada|
|[vOvzh8XCMmvtW0Ws.htm](bestiary-family-ability-glossary/vOvzh8XCMmvtW0Ws.htm)|(Zombie, Shock) Electromechanical Phasing|(Zombie, Electrizante) Electromechanical Phasing|modificada|
|[vX0CRZWok5ghasb3.htm](bestiary-family-ability-glossary/vX0CRZWok5ghasb3.htm)|(Secret Society) Prepared Diversion|(Sociedad Secreta) Desvío Preparado|modificada|
|[VXKiqiW7gXfFCz1U.htm](bestiary-family-ability-glossary/VXKiqiW7gXfFCz1U.htm)|(Melfesh Monster) Mycelial Tomb|(Monstruo Melfesh) Tumba Micelial|modificada|
|[VzKVJlX2ocv1ezzp.htm](bestiary-family-ability-glossary/VzKVJlX2ocv1ezzp.htm)|(Visitant) Noxious Breath|(Visitante) Aliento Nocivo|modificada|
|[W5fD1ebH6Ri8HNzh.htm](bestiary-family-ability-glossary/W5fD1ebH6Ri8HNzh.htm)|(Graveknight) Create Grave Squire|(Graveknight) Crear escudero sepulcral|modificada|
|[W77QhVNE46WDyMXi.htm](bestiary-family-ability-glossary/W77QhVNE46WDyMXi.htm)|(Skeleton) Explosive Death|(Esqueleto) Muerte Explosión|modificada|
|[wdO3EvDCGoDYW44S.htm](bestiary-family-ability-glossary/wdO3EvDCGoDYW44S.htm)|(Melfesh Monster) Death Throes|(Monstruo Melfesh) Estertores de muerte|modificada|
|[wEL7ZDMaSWSSYQG9.htm](bestiary-family-ability-glossary/wEL7ZDMaSWSSYQG9.htm)|(Vampire, Vetalarana, Manipulator) Drain Thoughts|(Vampiro, Vetalarana, Manipulador) Drenar Pensamientos|modificada|
|[wh2T2L5SMsa32RyE.htm](bestiary-family-ability-glossary/wh2T2L5SMsa32RyE.htm)|(Lich) Cold Beyond Cold|(Lich) Frío de ultratumba|modificada|
|[wJDfLmOJ2eJTmSwQ.htm](bestiary-family-ability-glossary/wJDfLmOJ2eJTmSwQ.htm)|(Mana Wastes Mutant) Increased Speed|(Mutante de los residuos de maná) Velocidad aumentada.|modificada|
|[wlhvSroB6r5cSd8Y.htm](bestiary-family-ability-glossary/wlhvSroB6r5cSd8Y.htm)|(Greater Barghest) Mutation - Poison Fangs|(Barghest mayor) Mutación - Colmillos venenosos|modificada|
|[WLmFKSzR6Xz9RqAu.htm](bestiary-family-ability-glossary/WLmFKSzR6Xz9RqAu.htm)|(Cryptid, Mutant) Marrowlance|(Cryptid, Mutant) Marrowlance|modificada|
|[WQjFPBQPDu6vyJny.htm](bestiary-family-ability-glossary/WQjFPBQPDu6vyJny.htm)|(Ghost) Corporeal Manifestation|(Fantasma) Manifestación corpórea|modificada|
|[wqptDG0IW6ExnISC.htm](bestiary-family-ability-glossary/wqptDG0IW6ExnISC.htm)|(Vampire, True) Create Spawn|(Vampiro, Verdadero) Elaborar Engendro|modificada|
|[WtRxZEPH963RkUCj.htm](bestiary-family-ability-glossary/WtRxZEPH963RkUCj.htm)|(Kuworsys) Rain Blows|(Kuworsys) Golpes de Lluvia|modificada|
|[WUXY9F8SLVQSC2b8.htm](bestiary-family-ability-glossary/WUXY9F8SLVQSC2b8.htm)|(Siabrae) Stony Shards|(Siabrae) Stony Shards|modificada|
|[wzezkgOufQev7BLK.htm](bestiary-family-ability-glossary/wzezkgOufQev7BLK.htm)|(Ghost) Revenant|(Fantasma) Revenant|modificada|
|[X0KoXRDEJYYtwGgK.htm](bestiary-family-ability-glossary/X0KoXRDEJYYtwGgK.htm)|(Beheaded) Whispering|(Decapitado) Susurro|modificada|
|[X2mdpb1Dhl890YbA.htm](bestiary-family-ability-glossary/X2mdpb1Dhl890YbA.htm)|(Worm That Walks) Swarm Shape|(Gusano que camina) Forma de enjambre|modificada|
|[XbHMVjHtbPaPr9P5.htm](bestiary-family-ability-glossary/XbHMVjHtbPaPr9P5.htm)|(Ravener) Vicious Criticals|(Ravener) Críticos despiadados|modificada|
|[XHut4MN0JBgm7WaN.htm](bestiary-family-ability-glossary/XHut4MN0JBgm7WaN.htm)|(Vampire, True) Drink Blood|(Vampiro, Verdadero) Beber Sangre|modificada|
|[XpGCN9KJN0CCIlzU.htm](bestiary-family-ability-glossary/XpGCN9KJN0CCIlzU.htm)|(Cryptid, Rumored) Stalk|(Cryptid, Rumored) Acechar|modificada|
|[xS8ybzuqPSi3Jb8k.htm](bestiary-family-ability-glossary/xS8ybzuqPSi3Jb8k.htm)|(Clockwork Creature) Wind-Up|(Criatura Mecánica) Wind-Up|modificada|
|[xuXloICuGtfA42AN.htm](bestiary-family-ability-glossary/xuXloICuGtfA42AN.htm)|(Clockwork Creature) Malfunction - Loose Screws|(Clockwork Creature) Mal funcionamiento - Tornillos sueltos.|modificada|
|[XvPm866TKSfclErJ.htm](bestiary-family-ability-glossary/XvPm866TKSfclErJ.htm)|(Lich) Paralyzing Touch|(Lich) Toque Paralizante|modificada|
|[xwbiTvx3qyVEOqh6.htm](bestiary-family-ability-glossary/xwbiTvx3qyVEOqh6.htm)|(Vampire, Jiang-Shi, Minister) Dark Enlightenment|(Vampiro, Jiang-Shi, Ministro) Oscura Iluminación|modificada|
|[XWtIvqCWmg8Tfr1N.htm](bestiary-family-ability-glossary/XWtIvqCWmg8Tfr1N.htm)|(Secret Society) Skill Savvy|(Sociedad Secreta) Habilidad Savvy|modificada|
|[xxI7QpVWLiGjdu4B.htm](bestiary-family-ability-glossary/xxI7QpVWLiGjdu4B.htm)|(Cryptid, Experimental) Operational Flaw|(Cryptid, Experimental) Operational Flaw|modificada|
|[Y46uZK6X7J7iMsgq.htm](bestiary-family-ability-glossary/Y46uZK6X7J7iMsgq.htm)|(Ulgrem-Axaan) Crocodile Tears|(Ulgrem-Axaan) Lágrimas de Cocodrilo|modificada|
|[Y7cRjlRkdo3siz5y.htm](bestiary-family-ability-glossary/Y7cRjlRkdo3siz5y.htm)|(Coven) Locate Coven|(Aquelarre) Localizar Aquelarre|modificada|
|[yaDe9hO9fm2N4SqH.htm](bestiary-family-ability-glossary/yaDe9hO9fm2N4SqH.htm)|(Spring-Heeled Jack) Resonant Terror|(Spring-Heeled Jack) Resonante Terror|modificada|
|[yDE3ZEoxRUqQmAsX.htm](bestiary-family-ability-glossary/yDE3ZEoxRUqQmAsX.htm)|(Vampire, Nosferatu Overlord) Air of Sickness|(Vampiro, Nosferatu Overlord) Aire de Enfermedad|modificada|
|[YELSFD2oTNMFkPJ2.htm](bestiary-family-ability-glossary/YELSFD2oTNMFkPJ2.htm)|(Secret Society) Shibboleth|(Sociedad Secreta) Shibboleth|modificada|
|[yfcqbMRmCkfPWV9O.htm](bestiary-family-ability-glossary/yfcqbMRmCkfPWV9O.htm)|(Ulgrem-Axaan) Crushing Weight|(Ulgrem-Axaan) Peso Aplastante|modificada|
|[yfNDFjz7VBvvLwee.htm](bestiary-family-ability-glossary/yfNDFjz7VBvvLwee.htm)|(Vampire, Vetalarana, Manipulator) Paralyzing Claws|(Vampiro, Vetalarana, Manipulador) Garras paralizantes|modificada|
|[yLdiZ2AnjZ8KuT7v.htm](bestiary-family-ability-glossary/yLdiZ2AnjZ8KuT7v.htm)|(Skeleton) Bone Storm|(Esqueleto) Tormenta de Huesos|modificada|
|[YQmf1Otd4xoxsSGU.htm](bestiary-family-ability-glossary/YQmf1Otd4xoxsSGU.htm)|(Mana Wastes Mutant) Caustic Pustules|(Mana Wastes Mutant) Pústulas Cáusticas|modificada|
|[Yv0mQAeVuQ2Id9vk.htm](bestiary-family-ability-glossary/Yv0mQAeVuQ2Id9vk.htm)|(Mana Wastes Mutant) Too Many Eyes|(Mana Wastes Mutant) Demasiados Ojos|modificada|
|[YVw626nVHlWwm4ej.htm](bestiary-family-ability-glossary/YVw626nVHlWwm4ej.htm)|(Skeleton) Crumbling Bones|(Esqueleto) Desmoronarse Huesos|modificada|
|[yWNcEsEJIoeXKBnk.htm](bestiary-family-ability-glossary/yWNcEsEJIoeXKBnk.htm)|(Lich) Void Shroud|(Lich) Mortaja del vacío|modificada|
|[z9yqLBExOMk1y9cg.htm](bestiary-family-ability-glossary/z9yqLBExOMk1y9cg.htm)|(Cryptid, Primeval) Broken Arsenal|(Cryptid, Primeval) Arsenal roto|modificada|
|[zehifmU1fTeGs2ev.htm](bestiary-family-ability-glossary/zehifmU1fTeGs2ev.htm)|(Vampire, Strigoi) Levitation|(Vampiro, Strigoi) Levitar|modificada|
|[ZIFaA4jDQjM0vq8q.htm](bestiary-family-ability-glossary/ZIFaA4jDQjM0vq8q.htm)|(Graveknight) Rejuvenation|(Graveknight) Rejuvenecimiento|modificada|
|[zkjsfsweJmsB66CS.htm](bestiary-family-ability-glossary/zkjsfsweJmsB66CS.htm)|(Coven) Contribute Spell|(Aquelarre) Contribuir Hechizo|modificada|
|[ZMTnwyXjyfy7Ryi6.htm](bestiary-family-ability-glossary/ZMTnwyXjyfy7Ryi6.htm)|(Vampire, True) Change Shape|(Vampiro, Verdadero) Cambiar de Forma|modificada|
|[ZRHUnhaHK2w1el8f.htm](bestiary-family-ability-glossary/ZRHUnhaHK2w1el8f.htm)|(Skeleton) Lacquered|(Esqueleto) Lacado|modificada|
|[Zu6feO9NUZJlsKuc.htm](bestiary-family-ability-glossary/Zu6feO9NUZJlsKuc.htm)|(Worm That Walks) Squirming Embrace|(Gusano Que Camina) Abrazo Retorcido|modificada|
|[ZUxt6s54TMgydXoW.htm](bestiary-family-ability-glossary/ZUxt6s54TMgydXoW.htm)|(Cryptid, Mutant) Shifting Iridescence|(Cryptid, Mutant) Iridiscencia Cambiante|modificada|
|[zwgUxJBNFqWOuaBX.htm](bestiary-family-ability-glossary/zwgUxJBNFqWOuaBX.htm)|(Vampire, Vrykolakas Master) Dominate Animal|(Vampiro, Maestro brucolaco) Dominar Animal|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
