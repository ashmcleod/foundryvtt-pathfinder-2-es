# Estado de la traducción (pathfinder-bestiary-items)

 * **modificada**: 3695
 * **ninguna**: 429
 * **vacía**: 120


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0clpZVy4Z3ymkbRk.htm](pathfinder-bestiary-items/0clpZVy4Z3ymkbRk.htm)|Invisibility (At Will)|
|[0exgyYYie8t1Xsi7.htm](pathfinder-bestiary-items/0exgyYYie8t1Xsi7.htm)|Veil (Self Only)|
|[0h0uqGkDaifDECB9.htm](pathfinder-bestiary-items/0h0uqGkDaifDECB9.htm)|Continual Flame (At Will)|
|[0iakBJFyQ3Jfxb9x.htm](pathfinder-bestiary-items/0iakBJFyQ3Jfxb9x.htm)|Spear|+1|
|[0PvvdloURD8opkYS.htm](pathfinder-bestiary-items/0PvvdloURD8opkYS.htm)|Clairvoyance (At Will)|
|[0pwYaNnJG2Q55PW5.htm](pathfinder-bestiary-items/0pwYaNnJG2Q55PW5.htm)|Ventriloquism (At Will)|
|[0qKIgNFS4fQjI3ej.htm](pathfinder-bestiary-items/0qKIgNFS4fQjI3ej.htm)|Mind Reading (At Will)|
|[0WKg5GTbHXuMxsLl.htm](pathfinder-bestiary-items/0WKg5GTbHXuMxsLl.htm)|Tongues (Constant)|
|[0yJUHf23Yd5Mb0jv.htm](pathfinder-bestiary-items/0yJUHf23Yd5Mb0jv.htm)|Illusory Scene (At Will)|
|[184dbY1EHWDY9oeu.htm](pathfinder-bestiary-items/184dbY1EHWDY9oeu.htm)|Detect Alignment (Evil Only)|
|[1AwjaaSvFQ5Zczfi.htm](pathfinder-bestiary-items/1AwjaaSvFQ5Zczfi.htm)|Composite Longbow|+1|
|[1HAY92fkCJghVqP3.htm](pathfinder-bestiary-items/1HAY92fkCJghVqP3.htm)|Obscuring Mist (At Will)|
|[1jpe4LxJZKxw1Rmq.htm](pathfinder-bestiary-items/1jpe4LxJZKxw1Rmq.htm)|Suggestion (At Will)|
|[1KpcQXgkKmJR4pYK.htm](pathfinder-bestiary-items/1KpcQXgkKmJR4pYK.htm)|Dominate (At Will) (See Dominate)|
|[1NXWQAJohaHClByj.htm](pathfinder-bestiary-items/1NXWQAJohaHClByj.htm)|True Seeing (Constant)|
|[2061tD7sNzE7eVL8.htm](pathfinder-bestiary-items/2061tD7sNzE7eVL8.htm)|Tangling Creepers (At Will)|
|[2dn1x9gUL9VsuED3.htm](pathfinder-bestiary-items/2dn1x9gUL9VsuED3.htm)|Locate (At Will)|
|[2gsH3ODIUNTtb5cQ.htm](pathfinder-bestiary-items/2gsH3ODIUNTtb5cQ.htm)|Nightmare (see Dream Haunting)|
|[2n2EvBlcKOTTOQao.htm](pathfinder-bestiary-items/2n2EvBlcKOTTOQao.htm)|Virtuoso Harp|
|[2RPJFPnuGdtE3i7O.htm](pathfinder-bestiary-items/2RPJFPnuGdtE3i7O.htm)|Entangle (At Will)|
|[2sVva7C3aRsTfi3F.htm](pathfinder-bestiary-items/2sVva7C3aRsTfi3F.htm)|Levitate (At Will)|
|[2WC1TuzgwcIU8iQO.htm](pathfinder-bestiary-items/2WC1TuzgwcIU8iQO.htm)|Speak with Animals (Constant)|
|[35CTre21nZQseuFC.htm](pathfinder-bestiary-items/35CTre21nZQseuFC.htm)|Speak with Animals (At Will)|
|[3L9SYneHtnfjVRWi.htm](pathfinder-bestiary-items/3L9SYneHtnfjVRWi.htm)|Read Omens|
|[3p2JdQQKOnQPCHS8.htm](pathfinder-bestiary-items/3p2JdQQKOnQPCHS8.htm)|Shadow Blast (Heartstone)|
|[3VlLp5tyHAozkdRS.htm](pathfinder-bestiary-items/3VlLp5tyHAozkdRS.htm)|Dimension Door (At Will)|
|[3zTnlxQMlpnNM9jU.htm](pathfinder-bestiary-items/3zTnlxQMlpnNM9jU.htm)|Fear (At Will)|
|[4ujaJEFOHxbaPKHG.htm](pathfinder-bestiary-items/4ujaJEFOHxbaPKHG.htm)|Ethereal Jaunt (At Will) (Heartstone)|
|[5PWV3tdENKwFgwRx.htm](pathfinder-bestiary-items/5PWV3tdENKwFgwRx.htm)|Dimension Door (Self Only)|
|[5x6u3I7q9dG4bb1P.htm](pathfinder-bestiary-items/5x6u3I7q9dG4bb1P.htm)|Trident|+1,striking|
|[60MvA4fpkW887kFL.htm](pathfinder-bestiary-items/60MvA4fpkW887kFL.htm)|Illusory Object (At Will)|
|[6fZ4UA9ABfLyGv7s.htm](pathfinder-bestiary-items/6fZ4UA9ABfLyGv7s.htm)|Creation (At Will)|
|[6GxNeMKadhnSQa6b.htm](pathfinder-bestiary-items/6GxNeMKadhnSQa6b.htm)|Illusory Disguise (At Will)|
|[6IgDVX3zLu078y16.htm](pathfinder-bestiary-items/6IgDVX3zLu078y16.htm)|Falchion|+1|
|[6IX4ybw5O0N1oM20.htm](pathfinder-bestiary-items/6IX4ybw5O0N1oM20.htm)|Mind Reading (Constant)|
|[6rhZZNbor9JgkLtT.htm](pathfinder-bestiary-items/6rhZZNbor9JgkLtT.htm)|Command (At Will)|
|[75DxWIhLD9bX4O9C.htm](pathfinder-bestiary-items/75DxWIhLD9bX4O9C.htm)|Illusory Object (At Will)|
|[7Gjlo5OkNRJvfb31.htm](pathfinder-bestiary-items/7Gjlo5OkNRJvfb31.htm)|Entangle (At Will)|
|[7jzwcYFJerQ5t5ms.htm](pathfinder-bestiary-items/7jzwcYFJerQ5t5ms.htm)|Detect Magic (Constant 3rd)|
|[7pJhkrdCMRHYdTFf.htm](pathfinder-bestiary-items/7pJhkrdCMRHYdTFf.htm)|Rapier|+1|
|[7qE6PiM12QgkWctV.htm](pathfinder-bestiary-items/7qE6PiM12QgkWctV.htm)|See Invisibility (Constant)|
|[7SNrRc2Yy8vbN9MM.htm](pathfinder-bestiary-items/7SNrRc2Yy8vbN9MM.htm)|Illusory Disguise (At Will)|
|[7WeAEXR3VyG3ZRND.htm](pathfinder-bestiary-items/7WeAEXR3VyG3ZRND.htm)|Grease (At Will)|
|[8BweqVFR5GkGzySD.htm](pathfinder-bestiary-items/8BweqVFR5GkGzySD.htm)|Dream Message (At Will)|
|[8e3vdJRcFBLL1E8q.htm](pathfinder-bestiary-items/8e3vdJRcFBLL1E8q.htm)|Invisibility (At Will) (Self Only)|
|[8glkghiOL3IQb37J.htm](pathfinder-bestiary-items/8glkghiOL3IQb37J.htm)|Breath Weapon|
|[8iJ5gIuTmtO4DQWI.htm](pathfinder-bestiary-items/8iJ5gIuTmtO4DQWI.htm)|Locate (Gems Only)|
|[8iJjfyT1q12S0WmO.htm](pathfinder-bestiary-items/8iJjfyT1q12S0WmO.htm)|Longspear|+1,striking|
|[8Js0FRMnRTqIViIM.htm](pathfinder-bestiary-items/8Js0FRMnRTqIViIM.htm)|True Seeing (Constant)|
|[8rWpQcyGXcSdQoeg.htm](pathfinder-bestiary-items/8rWpQcyGXcSdQoeg.htm)|Create Water (At Will) (See Desert Thirst)|
|[8uigP4T6d8IHuLQL.htm](pathfinder-bestiary-items/8uigP4T6d8IHuLQL.htm)|Divine Decree (See Harmonize)|
|[8VZ27iPC1CLKHIH6.htm](pathfinder-bestiary-items/8VZ27iPC1CLKHIH6.htm)|Spider Climb (Constant)|
|[8WQwygzmnLFUnRlx.htm](pathfinder-bestiary-items/8WQwygzmnLFUnRlx.htm)|Detect Alignment (Evil Only)|
|[970NWMlxLf3ar7pG.htm](pathfinder-bestiary-items/970NWMlxLf3ar7pG.htm)|Darkness (At Will)|
|[97InfsmjP5YZ2B8m.htm](pathfinder-bestiary-items/97InfsmjP5YZ2B8m.htm)|Shape Stone (At Will)|
|[9PPhZHN8g9n73rD5.htm](pathfinder-bestiary-items/9PPhZHN8g9n73rD5.htm)|Charm (At Will)|
|[9xvF2KBmoTPgqFeW.htm](pathfinder-bestiary-items/9xvF2KBmoTPgqFeW.htm)|Haste (Constant)|
|[9ztcjLanJtJWqWRz.htm](pathfinder-bestiary-items/9ztcjLanJtJWqWRz.htm)|Warhammer|+1,striking|
|[A130wI05EcUXJPnN.htm](pathfinder-bestiary-items/A130wI05EcUXJPnN.htm)|Shape Wood (At Will)|
|[A2jJk9jpF7BYcn0P.htm](pathfinder-bestiary-items/A2jJk9jpF7BYcn0P.htm)|Detect Alignment (At Will) (Good Only)|
|[a2Xlbnvf5yFI6lqM.htm](pathfinder-bestiary-items/a2Xlbnvf5yFI6lqM.htm)|Charm (At Will)|
|[ABhY9VZVaL77tgAg.htm](pathfinder-bestiary-items/ABhY9VZVaL77tgAg.htm)|Faerie Fire (At Will)|
|[abJ4KU3qfoXEExEU.htm](pathfinder-bestiary-items/abJ4KU3qfoXEExEU.htm)|Wall of Fire (At Will)|
|[ac9hRct9QTDrzhy1.htm](pathfinder-bestiary-items/ac9hRct9QTDrzhy1.htm)|Obscuring Mist (At Will)|
|[ADTDXJfN5pg4yTij.htm](pathfinder-bestiary-items/ADTDXJfN5pg4yTij.htm)|Illusory Disguise (At Will)|
|[aEgcIWUuEAn6TaqB.htm](pathfinder-bestiary-items/aEgcIWUuEAn6TaqB.htm)|Delusional Pride (At Will)|
|[aFTYkeNI6jd2TfGi.htm](pathfinder-bestiary-items/aFTYkeNI6jd2TfGi.htm)|Plane Shift (Self and Rider Only)|
|[AFXLJNAW0dhc0PkH.htm](pathfinder-bestiary-items/AFXLJNAW0dhc0PkH.htm)|Wineskin|
|[AIivVBT37yLmpoiE.htm](pathfinder-bestiary-items/AIivVBT37yLmpoiE.htm)|Invisibility (At Will) (Self Only)|
|[aIlJ68HccllslBhH.htm](pathfinder-bestiary-items/aIlJ68HccllslBhH.htm)|Tongues (Constant)|
|[Aj7yfMuoeLfNlfqn.htm](pathfinder-bestiary-items/Aj7yfMuoeLfNlfqn.htm)|Veil (At Will)|
|[ALc0LqW9wBY9yrEJ.htm](pathfinder-bestiary-items/ALc0LqW9wBY9yrEJ.htm)|Plane Shift (Self and Rider Only)|
|[AlibNTP9NftVxXAs.htm](pathfinder-bestiary-items/AlibNTP9NftVxXAs.htm)|Longspear|+1,striking|
|[AqE6dlxr2Tx5zoLP.htm](pathfinder-bestiary-items/AqE6dlxr2Tx5zoLP.htm)|Darkness (At Will)|
|[AWm9hzQmJPQ8BWn0.htm](pathfinder-bestiary-items/AWm9hzQmJPQ8BWn0.htm)|Faerie Fire (At Will)|
|[aX7NI0MaSS3ulX3D.htm](pathfinder-bestiary-items/aX7NI0MaSS3ulX3D.htm)|Hand Crossbow|+1,striking|
|[Az2vK9ZawWFjYNTp.htm](pathfinder-bestiary-items/Az2vK9ZawWFjYNTp.htm)|Dispel Magic (At Will)|
|[B5CQWqh21Ag4fN7D.htm](pathfinder-bestiary-items/B5CQWqh21Ag4fN7D.htm)|Longsword|+1|
|[B9kPTy7tmWyCtPY6.htm](pathfinder-bestiary-items/B9kPTy7tmWyCtPY6.htm)|Sleep (At Will)|
|[bCgL1BzKQHBGHqPL.htm](pathfinder-bestiary-items/bCgL1BzKQHBGHqPL.htm)|Air Walk (Constant)|
|[BcNlhGDkMuQwNYSZ.htm](pathfinder-bestiary-items/BcNlhGDkMuQwNYSZ.htm)|See Invisibility (Constant)|
|[BhEYreCA4HM7Wx7r.htm](pathfinder-bestiary-items/BhEYreCA4HM7Wx7r.htm)|Create Food (At Will)|
|[BHo40spxmEC4gZmk.htm](pathfinder-bestiary-items/BHo40spxmEC4gZmk.htm)|Plane Shift (to Astral Plane, Elemental Planes, or Material Plane Only)|
|[bjXxmJTfu45UygBo.htm](pathfinder-bestiary-items/bjXxmJTfu45UygBo.htm)|Enlarge (Self Only)|
|[BO3vCNXXYXKFGJaD.htm](pathfinder-bestiary-items/BO3vCNXXYXKFGJaD.htm)|Freedom of Movement (Constant)|
|[BpmOhvvFOeWcwME5.htm](pathfinder-bestiary-items/BpmOhvvFOeWcwME5.htm)|Charm (At Will)|
|[bQgMo8rXOJtdre8t.htm](pathfinder-bestiary-items/bQgMo8rXOJtdre8t.htm)|Telekinetic Maneuver (At Will)|
|[bTOywO9RTYBgmCfo.htm](pathfinder-bestiary-items/bTOywO9RTYBgmCfo.htm)|Dominate (Animals Only)|
|[C2v10qegDk2YZNwd.htm](pathfinder-bestiary-items/C2v10qegDk2YZNwd.htm)|Hideous Laughter (At Will)|
|[c31tqgUkSZeM5KZJ.htm](pathfinder-bestiary-items/c31tqgUkSZeM5KZJ.htm)|Gust of Wind (At Will)|
|[c5VUg13xjwOgh0QQ.htm](pathfinder-bestiary-items/c5VUg13xjwOgh0QQ.htm)|Darkness (At Will)|
|[CAkNdqzE0WhD6leV.htm](pathfinder-bestiary-items/CAkNdqzE0WhD6leV.htm)|Greatsword|+2,greaterStriking|
|[cfR1BSTJeK8YSbvs.htm](pathfinder-bestiary-items/cfR1BSTJeK8YSbvs.htm)|Divine Decree (At Will)|
|[CMeCQrhGto6e8KFb.htm](pathfinder-bestiary-items/CMeCQrhGto6e8KFb.htm)|Ventriloquism (At Will)|
|[codfFNHqT8HXOyAY.htm](pathfinder-bestiary-items/codfFNHqT8HXOyAY.htm)|Fly (Constant)|
|[cr6g653rUml9TFnT.htm](pathfinder-bestiary-items/cr6g653rUml9TFnT.htm)|Invisibility (Self Only)|
|[crdKpfEf0xqRNZES.htm](pathfinder-bestiary-items/crdKpfEf0xqRNZES.htm)|Greataxe|+1,striking|
|[CRjz5QeNCuFvnsRa.htm](pathfinder-bestiary-items/CRjz5QeNCuFvnsRa.htm)|Project Image (At Will)|
|[CvIMfgYZeFLh5Mzj.htm](pathfinder-bestiary-items/CvIMfgYZeFLh5Mzj.htm)|Light|
|[cWNji7yLlqINJYfN.htm](pathfinder-bestiary-items/cWNji7yLlqINJYfN.htm)|Dimension Door (At Will)|
|[D2WKvtBGhuuoGEom.htm](pathfinder-bestiary-items/D2WKvtBGhuuoGEom.htm)|Locate (Gems Only)|
|[d4dYU7PgAI7RkI0w.htm](pathfinder-bestiary-items/d4dYU7PgAI7RkI0w.htm)|Kukri|+1,striking|
|[DdGeeX7fXKyIDYAa.htm](pathfinder-bestiary-items/DdGeeX7fXKyIDYAa.htm)|Hallucinatory Terrain (At Will)|
|[dHcsjNvdIZCsP0zY.htm](pathfinder-bestiary-items/dHcsjNvdIZCsP0zY.htm)|Levitate (At Will)|
|[djz4joarJVDQyibC.htm](pathfinder-bestiary-items/djz4joarJVDQyibC.htm)|Ray of Enfeeblement (At Will)|
|[drbxOhoSGY2fBfCd.htm](pathfinder-bestiary-items/drbxOhoSGY2fBfCd.htm)|True Seeing (Constant)|
|[dTQnIqFEKQGXaABe.htm](pathfinder-bestiary-items/dTQnIqFEKQGXaABe.htm)|Mind Reading (At Will)|
|[dw2vQuK7O2lZihH8.htm](pathfinder-bestiary-items/dw2vQuK7O2lZihH8.htm)|Divine Wrath (Lawful)|
|[DwI3CqVDrb0R2LgX.htm](pathfinder-bestiary-items/DwI3CqVDrb0R2LgX.htm)|Warhammer|+2,striking|
|[DzVgAESVeOVKmXjF.htm](pathfinder-bestiary-items/DzVgAESVeOVKmXjF.htm)|Enlarge (Self Only)|
|[E9rT9B8EjzqD6Ox9.htm](pathfinder-bestiary-items/E9rT9B8EjzqD6Ox9.htm)|Haste (Constant)|
|[E9yEjfkAzFYDW6Iy.htm](pathfinder-bestiary-items/E9yEjfkAzFYDW6Iy.htm)|Darkness (At Will)|
|[EDb4l4r9VNgvmSCI.htm](pathfinder-bestiary-items/EDb4l4r9VNgvmSCI.htm)|Blur (At Will)|
|[EfDe5JdUcCswZdko.htm](pathfinder-bestiary-items/EfDe5JdUcCswZdko.htm)|Detect Magic (Constant)|
|[Eis3bEsqm4vb29Uc.htm](pathfinder-bestiary-items/Eis3bEsqm4vb29Uc.htm)|Dimension Door (At Will)|
|[eN762ag1BwsEj2mG.htm](pathfinder-bestiary-items/eN762ag1BwsEj2mG.htm)|Invisibility (Self Only)|
|[EnaHR33bWeO1hz4Z.htm](pathfinder-bestiary-items/EnaHR33bWeO1hz4Z.htm)|Invisibility (At Will) (Self Only)|
|[EnBhUXkHOrsrB0rH.htm](pathfinder-bestiary-items/EnBhUXkHOrsrB0rH.htm)|Detect Alignment (At Will) (Good Only)|
|[eQk520pm05mjgSFh.htm](pathfinder-bestiary-items/eQk520pm05mjgSFh.htm)|Speak with Animals (Constant)|
|[Erlm9PRm5Sz22CZb.htm](pathfinder-bestiary-items/Erlm9PRm5Sz22CZb.htm)|Longspear|+1,striking|
|[etv02A5iCRxbUkwf.htm](pathfinder-bestiary-items/etv02A5iCRxbUkwf.htm)|Blackaxe|+4,majorStriking,adamantine,greaterCorrosive|
|[EuaT5qjP1u5HzIZa.htm](pathfinder-bestiary-items/EuaT5qjP1u5HzIZa.htm)|True Seeing (Constant)|
|[eVPsvySsrEJWnpGV.htm](pathfinder-bestiary-items/eVPsvySsrEJWnpGV.htm)|Fireball (At Will)|
|[eWfKYEEWPxEx5oaP.htm](pathfinder-bestiary-items/eWfKYEEWPxEx5oaP.htm)|Bind Soul (At Will)|
|[ewvr4EfhRY6mvWP1.htm](pathfinder-bestiary-items/ewvr4EfhRY6mvWP1.htm)|Dimension Door (At Will)|
|[EzBWZa36185FReyi.htm](pathfinder-bestiary-items/EzBWZa36185FReyi.htm)|Mind Reading (At Will)|
|[EZnppdlfMteD4W2Z.htm](pathfinder-bestiary-items/EZnppdlfMteD4W2Z.htm)|Invisibility (At Will) (Self Only)|
|[f0Ymg46dc5u2BPA6.htm](pathfinder-bestiary-items/f0Ymg46dc5u2BPA6.htm)|Command (At Will)|
|[F4uFCludKReEZgYt.htm](pathfinder-bestiary-items/F4uFCludKReEZgYt.htm)|Detect Alignment (At Will) (Evil Only)|
|[f7bd20dy89WdBEHW.htm](pathfinder-bestiary-items/f7bd20dy89WdBEHW.htm)|Hatchet|+1|
|[F8nRkJXtcFFzX0NM.htm](pathfinder-bestiary-items/F8nRkJXtcFFzX0NM.htm)|Dimension Door (At Will)|
|[F99N1qLgQghEZNBq.htm](pathfinder-bestiary-items/F99N1qLgQghEZNBq.htm)|Darkness (At Will)|
|[FcJ7lGjvmOJZvtmO.htm](pathfinder-bestiary-items/FcJ7lGjvmOJZvtmO.htm)|Blink (At Will)|
|[fEOaF0IuTdPQkeq9.htm](pathfinder-bestiary-items/fEOaF0IuTdPQkeq9.htm)|Detect Alignment (At Will) (Good Only)|
|[ff4wdxelwlYQwvOS.htm](pathfinder-bestiary-items/ff4wdxelwlYQwvOS.htm)|Zone of Truth (At Will)|
|[ffXBW5zCqVdRH2Mq.htm](pathfinder-bestiary-items/ffXBW5zCqVdRH2Mq.htm)|Freedom of Movement (Constant)|
|[FghvQA4Q7lAFIiEl.htm](pathfinder-bestiary-items/FghvQA4Q7lAFIiEl.htm)|Tongues (Constant)|
|[fhI1w4ye0jYus2y3.htm](pathfinder-bestiary-items/fhI1w4ye0jYus2y3.htm)|Dimension Door (At Will) (Self Only)|
|[fMLm9tfax3zKvQiH.htm](pathfinder-bestiary-items/fMLm9tfax3zKvQiH.htm)|Detect Alignment (At Will) (Lawful Only)|
|[fpyaGQ7nPlsCH1nc.htm](pathfinder-bestiary-items/fpyaGQ7nPlsCH1nc.htm)|True Seeing (Constant)|
|[fv7QW4VtrW3K3j6F.htm](pathfinder-bestiary-items/fv7QW4VtrW3K3j6F.htm)|Longsword|+1|
|[fx5g45f1vX9UtDKI.htm](pathfinder-bestiary-items/fx5g45f1vX9UtDKI.htm)|See Invisibility (Constant)|
|[G0iMqJT4TeHMVS0N.htm](pathfinder-bestiary-items/G0iMqJT4TeHMVS0N.htm)|Hallucinatory Terrain (At Will)|
|[G0WFBGJRBnL3fapV.htm](pathfinder-bestiary-items/G0WFBGJRBnL3fapV.htm)|Composite Shortbow|+1|
|[g2NHoIs5k3anoSdi.htm](pathfinder-bestiary-items/g2NHoIs5k3anoSdi.htm)|Air Walk (Constant)|
|[g4YpY2bku4a31XWO.htm](pathfinder-bestiary-items/g4YpY2bku4a31XWO.htm)|True Seeing (Constant)|
|[G8atrV1L3RSnUege.htm](pathfinder-bestiary-items/G8atrV1L3RSnUege.htm)|Mind Reading (At Will)|
|[G8jGvZckaWvyJEIl.htm](pathfinder-bestiary-items/G8jGvZckaWvyJEIl.htm)|Grease (At Will)|
|[Ga6GRFsOzY8ecxgf.htm](pathfinder-bestiary-items/Ga6GRFsOzY8ecxgf.htm)|Composite Longbow|+1,striking|
|[gb4nDUEQffh7bVIu.htm](pathfinder-bestiary-items/gb4nDUEQffh7bVIu.htm)|Counter Performance (Visual Only)|
|[GcOylKqDGhKS2Ut0.htm](pathfinder-bestiary-items/GcOylKqDGhKS2Ut0.htm)|Glaive|+1|
|[GetPArMiHDcSPIjf.htm](pathfinder-bestiary-items/GetPArMiHDcSPIjf.htm)|Teleport (At Will) (Self Only)|
|[gJXsZM1X2ClLUV54.htm](pathfinder-bestiary-items/gJXsZM1X2ClLUV54.htm)|Invisibility (At Will)|
|[GkuKYyD5W2XX5dpx.htm](pathfinder-bestiary-items/GkuKYyD5W2XX5dpx.htm)|Speak with Animals (Constant)|
|[gQoduyb8g1gthAxV.htm](pathfinder-bestiary-items/gQoduyb8g1gthAxV.htm)|Detect Alignment (At Will) (Evil Only)|
|[Gtqiu1gMAKSCHwT2.htm](pathfinder-bestiary-items/Gtqiu1gMAKSCHwT2.htm)|Levitate (At Will)|
|[GU2jdu9qK8wjZI4y.htm](pathfinder-bestiary-items/GU2jdu9qK8wjZI4y.htm)|Abyssal Plague (At Will)|
|[GUFkIF2lbvGvkLj5.htm](pathfinder-bestiary-items/GUFkIF2lbvGvkLj5.htm)|Plane Shift (At Will) (to Astral Plane, Elemental Planes, or Material Plane Only)|
|[H8p1fVftX2xCpcdM.htm](pathfinder-bestiary-items/H8p1fVftX2xCpcdM.htm)|Freedom of Movement (Constant)|
|[haaQ5lL943som2So.htm](pathfinder-bestiary-items/haaQ5lL943som2So.htm)|Darkness (At Will)|
|[HBXRcdXGAmxhP7u3.htm](pathfinder-bestiary-items/HBXRcdXGAmxhP7u3.htm)|Freedom of Movement (Constant)|
|[hDc6QDQc92oPCzYA.htm](pathfinder-bestiary-items/hDc6QDQc92oPCzYA.htm)|Illusory Object (At Will)|
|[hdF0CzgKgx2Zm51h.htm](pathfinder-bestiary-items/hdF0CzgKgx2Zm51h.htm)|Speak with Plants (Constant)|
|[Hf8TaBvmNOD8vMvG.htm](pathfinder-bestiary-items/Hf8TaBvmNOD8vMvG.htm)|Enlarge (Self Only)|
|[HH4UDZ1qREynyOUN.htm](pathfinder-bestiary-items/HH4UDZ1qREynyOUN.htm)|Project Image (See Mirage)|
|[HRjw0KnPqOC21GRz.htm](pathfinder-bestiary-items/HRjw0KnPqOC21GRz.htm)|Rapier|silver|
|[hrpJ4LH6ospAmcRG.htm](pathfinder-bestiary-items/hrpJ4LH6ospAmcRG.htm)|Blowgun Darts (1 coated in giant centipede venom)|
|[HTq6wxTPGq2vIAyk.htm](pathfinder-bestiary-items/HTq6wxTPGq2vIAyk.htm)|Earthbind (At Will)|
|[HYQnPm7xqWXqvWNi.htm](pathfinder-bestiary-items/HYQnPm7xqWXqvWNi.htm)|Blind Ambition (At Will)|
|[hZQtHKUw6xKXQCeF.htm](pathfinder-bestiary-items/hZQtHKUw6xKXQCeF.htm)|Speak with Animals (Constant)|
|[HZYAj7VYAUuppbJG.htm](pathfinder-bestiary-items/HZYAj7VYAUuppbJG.htm)|Invisibility (Self Only)|
|[I9tYnS5FyCSmLBYY.htm](pathfinder-bestiary-items/I9tYnS5FyCSmLBYY.htm)|Tree Shape (At Will)|
|[IeYHBdNAWLIqzRuF.htm](pathfinder-bestiary-items/IeYHBdNAWLIqzRuF.htm)|Invisibility (At Will) (Self Only)|
|[Ig3ZBIjZHGA9nqg6.htm](pathfinder-bestiary-items/Ig3ZBIjZHGA9nqg6.htm)|Dispel Magic (At Will)|
|[IIuFfE9guAYmy0QA.htm](pathfinder-bestiary-items/IIuFfE9guAYmy0QA.htm)|Dispel Magic (At Will)|
|[iMgpmBiunrNQEx9Y.htm](pathfinder-bestiary-items/iMgpmBiunrNQEx9Y.htm)|Detect Alignment (At Will) (Evil Only)|
|[iO5Xw9g7tRODM1vg.htm](pathfinder-bestiary-items/iO5Xw9g7tRODM1vg.htm)|Detect Alignment (Constant) (All Alignments)|
|[Ir7VUc4vTn1TT3EX.htm](pathfinder-bestiary-items/Ir7VUc4vTn1TT3EX.htm)|100 feet of Erinys-Hair Rope|
|[iRnNuTXaYCxU3pTc.htm](pathfinder-bestiary-items/iRnNuTXaYCxU3pTc.htm)|Plane Shift (At Will) (to Astral Plane, Elemental Planes, or Material Plane Only)|
|[IsERU8I2n8sZhbmW.htm](pathfinder-bestiary-items/IsERU8I2n8sZhbmW.htm)|Shape Stone (At Will)|
|[iTuhCEc10dCv7cTY.htm](pathfinder-bestiary-items/iTuhCEc10dCv7cTY.htm)|Create Water (At Will) (See Desert Thirst)|
|[j5nOoy2JdxGbg96d.htm](pathfinder-bestiary-items/j5nOoy2JdxGbg96d.htm)|Dimension Door (At Will)|
|[J6HpVacgDjE0FPaz.htm](pathfinder-bestiary-items/J6HpVacgDjE0FPaz.htm)|Dispel Magic (At Will)|
|[J8DYyAUH2MgtwPeM.htm](pathfinder-bestiary-items/J8DYyAUH2MgtwPeM.htm)|Tongues (Constant)|
|[JcNx2oSCr0ITXfqs.htm](pathfinder-bestiary-items/JcNx2oSCr0ITXfqs.htm)|Detect Alignment (At Will) (Good Only)|
|[jcx4KGQO0eO1X2yD.htm](pathfinder-bestiary-items/jcx4KGQO0eO1X2yD.htm)|Blink (At Will)|
|[JgLAUNOR58PPWtCz.htm](pathfinder-bestiary-items/JgLAUNOR58PPWtCz.htm)|Speak with Plants (Constant)|
|[jiTqy8zD4pcvkN63.htm](pathfinder-bestiary-items/jiTqy8zD4pcvkN63.htm)|Speak with Plants (Constant)|
|[jJ8SC2rd2xXCFTiy.htm](pathfinder-bestiary-items/jJ8SC2rd2xXCFTiy.htm)|Tongues (Constant)|
|[jkqXGxKyDHGb9lqV.htm](pathfinder-bestiary-items/jkqXGxKyDHGb9lqV.htm)|Illusory Disguise (At Will)|
|[JqkLVZDjUtyAhPnf.htm](pathfinder-bestiary-items/JqkLVZDjUtyAhPnf.htm)|Discern Lies (At Will)|
|[JTC7dqzt6oF49qIZ.htm](pathfinder-bestiary-items/JTC7dqzt6oF49qIZ.htm)|Heroism (See Harmonize)|
|[jTCynZS3NcEx1n1y.htm](pathfinder-bestiary-items/jTCynZS3NcEx1n1y.htm)|Suggestion (At Will)|
|[JVw9Lbq6vKp5vmAb.htm](pathfinder-bestiary-items/JVw9Lbq6vKp5vmAb.htm)|Speak with Animals (Constant)|
|[jwzA0HSTix25cCzk.htm](pathfinder-bestiary-items/jwzA0HSTix25cCzk.htm)|Greatsword|+1,striking|
|[K5wtDTBvqyxmQgdA.htm](pathfinder-bestiary-items/K5wtDTBvqyxmQgdA.htm)|Hypnotic Pattern (At Will)|
|[K6Ty6fjWIqDqupgg.htm](pathfinder-bestiary-items/K6Ty6fjWIqDqupgg.htm)|Detect Alignment (At Will) (Evil Only)|
|[k71389gZFoOINHgu.htm](pathfinder-bestiary-items/k71389gZFoOINHgu.htm)|Scrying (At Will) (See Infernal Investment)|
|[K8Hzt8FXFiK5vce3.htm](pathfinder-bestiary-items/K8Hzt8FXFiK5vce3.htm)|Confusion (At Will)|
|[KdYdd17OoSplEhz8.htm](pathfinder-bestiary-items/KdYdd17OoSplEhz8.htm)|Speak with Plants (Constant)|
|[KiV6ULlu39sXHCjs.htm](pathfinder-bestiary-items/KiV6ULlu39sXHCjs.htm)|Plane Shift (to Astral Plane, Elemental Planes, or Material Plane Only)|
|[KOtaGptbhDnaFa4U.htm](pathfinder-bestiary-items/KOtaGptbhDnaFa4U.htm)|Air Walk (Constant)|
|[Kp4bo7vO06rxYNfy.htm](pathfinder-bestiary-items/Kp4bo7vO06rxYNfy.htm)|Shatter (At Will)|
|[kQ3f06MvY2faGWKA.htm](pathfinder-bestiary-items/kQ3f06MvY2faGWKA.htm)|See Invisibility (Constant)|
|[kSiSN7PO1p0yX2fR.htm](pathfinder-bestiary-items/kSiSN7PO1p0yX2fR.htm)|Ranseur|+1|
|[KuKxJ2Kk7PAZFs0r.htm](pathfinder-bestiary-items/KuKxJ2Kk7PAZFs0r.htm)|Locate (Gems Only)|
|[L05P9dGXjwURIjtP.htm](pathfinder-bestiary-items/L05P9dGXjwURIjtP.htm)|Remove Fear (At Will)|
|[L0ZwpIxnrQrYM4sK.htm](pathfinder-bestiary-items/L0ZwpIxnrQrYM4sK.htm)|Dimension Door (At Will)|
|[L225wlWMZnWKniMQ.htm](pathfinder-bestiary-items/L225wlWMZnWKniMQ.htm)|Detect Magic (At Will)|
|[LDs80bqAdM5lXyY1.htm](pathfinder-bestiary-items/LDs80bqAdM5lXyY1.htm)|True Seeing (Constant)|
|[LDw297Vz4uENjRYC.htm](pathfinder-bestiary-items/LDw297Vz4uENjRYC.htm)|Stone Longsword|
|[lGrwi0lRYYYUqgHg.htm](pathfinder-bestiary-items/lGrwi0lRYYYUqgHg.htm)|Dimension Door (At Will)|
|[ljGlIbY53EAJKiBi.htm](pathfinder-bestiary-items/ljGlIbY53EAJKiBi.htm)|Telekinetic Maneuver (At Will)|
|[lntTBnW0tn7XEMPJ.htm](pathfinder-bestiary-items/lntTBnW0tn7XEMPJ.htm)|Dimension Door (At Will)|
|[lo9zjzoOZSRs5svZ.htm](pathfinder-bestiary-items/lo9zjzoOZSRs5svZ.htm)|Darkness (At Will)|
|[lPGdMvfNZrgPjMZO.htm](pathfinder-bestiary-items/lPGdMvfNZrgPjMZO.htm)|Gust of Wind (At Will)|
|[Lr9ykhJaIn18XV6L.htm](pathfinder-bestiary-items/Lr9ykhJaIn18XV6L.htm)|Primal Focus|
|[lt3BOPlp3xHdHIpR.htm](pathfinder-bestiary-items/lt3BOPlp3xHdHIpR.htm)|Tongues (Constant)|
|[lUx3uVYEFr3kvziO.htm](pathfinder-bestiary-items/lUx3uVYEFr3kvziO.htm)|Sound Burst (At Will)|
|[m0zG0H17EryWyKWQ.htm](pathfinder-bestiary-items/m0zG0H17EryWyKWQ.htm)|Sleep (At Will)|
|[m6uKGiZBbvS4RFVu.htm](pathfinder-bestiary-items/m6uKGiZBbvS4RFVu.htm)|Dominate (At Will)|
|[m7io5ugLsLLhISWa.htm](pathfinder-bestiary-items/m7io5ugLsLLhISWa.htm)|Tongues (Constant)|
|[m8EZncnl9JW785it.htm](pathfinder-bestiary-items/m8EZncnl9JW785it.htm)|Infused Alchemist's Fire (Moderate)|
|[MEF840zKnNuFMJEz.htm](pathfinder-bestiary-items/MEF840zKnNuFMJEz.htm)|Obscuring Mist (At Will)|
|[MjQUEa9YSvr5hUp0.htm](pathfinder-bestiary-items/MjQUEa9YSvr5hUp0.htm)|Tree Shape (At Will)|
|[mKJTvwjtXZJUzRZA.htm](pathfinder-bestiary-items/mKJTvwjtXZJUzRZA.htm)|Invisibility (At Will) (Self Only)|
|[MN9rdmuzbKRt8SBK.htm](pathfinder-bestiary-items/MN9rdmuzbKRt8SBK.htm)|Speak with Animals (Constant)|
|[mqKNqpPb4SYPm9ck.htm](pathfinder-bestiary-items/mqKNqpPb4SYPm9ck.htm)|Mind Reading (At Will)|
|[mqqrqczmNE7FLbro.htm](pathfinder-bestiary-items/mqqrqczmNE7FLbro.htm)|Fear (At Will)|
|[mqUmbBpM7QVWWGIg.htm](pathfinder-bestiary-items/mqUmbBpM7QVWWGIg.htm)|Control Water (At Will)|
|[mRcunWMMtA2wBZ4C.htm](pathfinder-bestiary-items/mRcunWMMtA2wBZ4C.htm)|Miracle (Once per Year)|
|[MUQwZ93FSpa5PAib.htm](pathfinder-bestiary-items/MUQwZ93FSpa5PAib.htm)|Entangle (At Will)|
|[mW91KOH1mqBVJgBw.htm](pathfinder-bestiary-items/mW91KOH1mqBVJgBw.htm)|Remove Fear (At Will)|
|[mxcJoEf3oDyFBZYp.htm](pathfinder-bestiary-items/mxcJoEf3oDyFBZYp.htm)|Scroll of Teleport (Level 6)|
|[mzLQxxUbDe77Znfh.htm](pathfinder-bestiary-items/mzLQxxUbDe77Znfh.htm)|Hallucinatory Terrain (At Will)|
|[N0tdBKYLEcJAPSBf.htm](pathfinder-bestiary-items/N0tdBKYLEcJAPSBf.htm)|Darkness (At Will)|
|[n9vV2nkxbDMh4XuO.htm](pathfinder-bestiary-items/n9vV2nkxbDMh4XuO.htm)|Illusory Creature (At Will)|
|[NfHgwhhignIzYPcC.htm](pathfinder-bestiary-items/NfHgwhhignIzYPcC.htm)|Grease (At Will)|
|[Ni0PvgRNnOE6CtCK.htm](pathfinder-bestiary-items/Ni0PvgRNnOE6CtCK.htm)|Scimitar|+1,striking|
|[ni8nEa0MJXc6OvPt.htm](pathfinder-bestiary-items/ni8nEa0MJXc6OvPt.htm)|Scimitar|+1,striking|
|[NSZBHCyCvmYgDUpL.htm](pathfinder-bestiary-items/NSZBHCyCvmYgDUpL.htm)|Obscuring Mist (At Will)|
|[Nt7T41ncTB9oAvqg.htm](pathfinder-bestiary-items/Nt7T41ncTB9oAvqg.htm)|Project Image (At Will)|
|[nufYCaM1XvwwNNWe.htm](pathfinder-bestiary-items/nufYCaM1XvwwNNWe.htm)|Faerie Fire (At Will)|
|[NX1A0WbKKkzk7WUo.htm](pathfinder-bestiary-items/NX1A0WbKKkzk7WUo.htm)|Confusion (At Will)|
|[NyG8RsXlgvfQa4LP.htm](pathfinder-bestiary-items/NyG8RsXlgvfQa4LP.htm)|Tree Shape (At Will)|
|[NYmcjhvAsrctDwBW.htm](pathfinder-bestiary-items/NYmcjhvAsrctDwBW.htm)|Levitate (At Will)|
|[o5Zxivy6gPz9uwu3.htm](pathfinder-bestiary-items/o5Zxivy6gPz9uwu3.htm)|Dimension Door (At Will)|
|[O99DLEbVCEWFTial.htm](pathfinder-bestiary-items/O99DLEbVCEWFTial.htm)|+1 Half Plate|
|[oAIpxgEZ30V9uBkj.htm](pathfinder-bestiary-items/oAIpxgEZ30V9uBkj.htm)|Captivating Adoration (At Will)|
|[oce5SfF9Q7CSbVQZ.htm](pathfinder-bestiary-items/oce5SfF9Q7CSbVQZ.htm)|Sending (At Will)|
|[OdVgOWE6KnzPm27D.htm](pathfinder-bestiary-items/OdVgOWE6KnzPm27D.htm)|Hydraulic Push (At Will)|
|[oEggkejJSMydzPBu.htm](pathfinder-bestiary-items/oEggkejJSMydzPBu.htm)|Shape Stone (At Will)|
|[OFkqgGXYjl87F0Nt.htm](pathfinder-bestiary-items/OFkqgGXYjl87F0Nt.htm)|Dimension Door (At Will)|
|[ohj6QzkUa6E7SC0Y.htm](pathfinder-bestiary-items/ohj6QzkUa6E7SC0Y.htm)|Divine Wrath (Chaotic Only)|
|[oHu944Q5McwtiHBT.htm](pathfinder-bestiary-items/oHu944Q5McwtiHBT.htm)|Delusional Pride (At Will)|
|[OImSgHtFF2C6yTuh.htm](pathfinder-bestiary-items/OImSgHtFF2C6yTuh.htm)|Continual Flame (At Will)|
|[OjH9zugdZf9plsD2.htm](pathfinder-bestiary-items/OjH9zugdZf9plsD2.htm)|Composite Longbow|+1,striking|
|[OJPKm3WpqeYWDkqt.htm](pathfinder-bestiary-items/OJPKm3WpqeYWDkqt.htm)|Detect Magic (Constant)|
|[OLfOwMCSsuRGrS9K.htm](pathfinder-bestiary-items/OLfOwMCSsuRGrS9K.htm)|Creation (At Will)|
|[oLiLZB5JEXhWtE64.htm](pathfinder-bestiary-items/oLiLZB5JEXhWtE64.htm)|Speak with Animals (Constant)|
|[oOjIZXqrVKZq2br0.htm](pathfinder-bestiary-items/oOjIZXqrVKZq2br0.htm)|Hypercognition (At Will)|
|[OSbtb7hOIAcilbFJ.htm](pathfinder-bestiary-items/OSbtb7hOIAcilbFJ.htm)|Spell Turning (At Will)|
|[OuCr8UICQ5a4kIDG.htm](pathfinder-bestiary-items/OuCr8UICQ5a4kIDG.htm)|Dimensional Anchor (At Will) (Constant)|
|[Oy7DyDa1Fc1uUyJg.htm](pathfinder-bestiary-items/Oy7DyDa1Fc1uUyJg.htm)|Detect Alignment (At Will) (Lawful Only)|
|[oyPqohzoJUH8UsHV.htm](pathfinder-bestiary-items/oyPqohzoJUH8UsHV.htm)|Spear|+1,striking,holy|
|[p0Pe1njRhm7KfrxH.htm](pathfinder-bestiary-items/p0Pe1njRhm7KfrxH.htm)|Detect Alignment (At Will) (Evil Only)|
|[P9xeBnqa1Hg3dsTX.htm](pathfinder-bestiary-items/P9xeBnqa1Hg3dsTX.htm)|Tongues (Constant)|
|[pDnKPlqU39ZA6bTX.htm](pathfinder-bestiary-items/pDnKPlqU39ZA6bTX.htm)|Detect Alignment (At Will) (Good Only)|
|[PekoTdQc6oDRhqlf.htm](pathfinder-bestiary-items/PekoTdQc6oDRhqlf.htm)|Fly (Constant)|
|[pI4YRwa86fcwb3E9.htm](pathfinder-bestiary-items/pI4YRwa86fcwb3E9.htm)|Invisibility (At Will)|
|[PKOuAKr4I81uIS3W.htm](pathfinder-bestiary-items/PKOuAKr4I81uIS3W.htm)|Blind Ambition (At Will)|
|[pkX4RMcX6QZp0wuV.htm](pathfinder-bestiary-items/pkX4RMcX6QZp0wuV.htm)|Obscuring Mist (At Will)|
|[pNlVk6Eq16L9OBYt.htm](pathfinder-bestiary-items/pNlVk6Eq16L9OBYt.htm)|Tongues (Constant)|
|[Po7qk3bmoY9QU4EJ.htm](pathfinder-bestiary-items/Po7qk3bmoY9QU4EJ.htm)|Invisibility (At Will) (Self Only)|
|[ppjNzzAe843rXraz.htm](pathfinder-bestiary-items/ppjNzzAe843rXraz.htm)|Humanoid Form (At Will)|
|[psEMpr7a7TZs1sND.htm](pathfinder-bestiary-items/psEMpr7a7TZs1sND.htm)|Invisibility (At Will) (Self Only)|
|[pTkdWpwWTgPbS918.htm](pathfinder-bestiary-items/pTkdWpwWTgPbS918.htm)|Dimension Door (At Will)|
|[PUSzBjopjmdEqswm.htm](pathfinder-bestiary-items/PUSzBjopjmdEqswm.htm)|Obscuring Mist (At Will)|
|[PYlSwGscGRdPPJeE.htm](pathfinder-bestiary-items/PYlSwGscGRdPPJeE.htm)|Tongues (Constant)|
|[q02Jrj8JB2UQ75MG.htm](pathfinder-bestiary-items/q02Jrj8JB2UQ75MG.htm)|Detect Alignment (At Will) (Evil Only)|
|[Q1MuNYAdloug8lgC.htm](pathfinder-bestiary-items/Q1MuNYAdloug8lgC.htm)|True Seeing (Constant)|
|[Q64gJkNmFi1chKiM.htm](pathfinder-bestiary-items/Q64gJkNmFi1chKiM.htm)|Counter Performance (At Will)|
|[Q6aECCBC8XJs3Eke.htm](pathfinder-bestiary-items/Q6aECCBC8XJs3Eke.htm)|Water Breathing (Constant)|
|[q70BgV7abbPeYT1b.htm](pathfinder-bestiary-items/q70BgV7abbPeYT1b.htm)|Detect Magic (Constant)|
|[q8F4JeOhDTotBaIW.htm](pathfinder-bestiary-items/q8F4JeOhDTotBaIW.htm)|Tongues (Constant)|
|[qAZd9xSWWwAjD8tO.htm](pathfinder-bestiary-items/qAZd9xSWWwAjD8tO.htm)|Wall of Ice (At Will)|
|[qc6ELk3InJa6GZ8u.htm](pathfinder-bestiary-items/qc6ELk3InJa6GZ8u.htm)|Tongues (Constant)|
|[QFDEJZjNW689owzG.htm](pathfinder-bestiary-items/QFDEJZjNW689owzG.htm)|Speak with Animals (Constant)|
|[QFF18wlKJOIrFquv.htm](pathfinder-bestiary-items/QFF18wlKJOIrFquv.htm)|Infused Frost Vial (Moderate)|
|[QFnrAylc3zLAAIRC.htm](pathfinder-bestiary-items/QFnrAylc3zLAAIRC.htm)|Charm (At Will)|
|[QgUyKi2NczmiII2x.htm](pathfinder-bestiary-items/QgUyKi2NczmiII2x.htm)|See Invisibility (Constant)|
|[QjDbAH40mGfKBBKn.htm](pathfinder-bestiary-items/QjDbAH40mGfKBBKn.htm)|True Seeing (Constant)|
|[qLjogmwZzW2UEq03.htm](pathfinder-bestiary-items/qLjogmwZzW2UEq03.htm)|Captivating Adoration (At Will)|
|[QlnqT9co3HINOMCw.htm](pathfinder-bestiary-items/QlnqT9co3HINOMCw.htm)|Obscuring Mist (At Will)|
|[QLpCft5YSm98kbx4.htm](pathfinder-bestiary-items/QLpCft5YSm98kbx4.htm)|Speak with Animals (At Will) (Arthropods Only)|
|[QN8xHmKgrI6lQLO3.htm](pathfinder-bestiary-items/QN8xHmKgrI6lQLO3.htm)|Plane Shift (At Will) (to Astral Plane, Elemental Planes, or Material Plane Only)|
|[QRnahY34q6Ut5lgn.htm](pathfinder-bestiary-items/QRnahY34q6Ut5lgn.htm)|Suggestion (At Will)|
|[qUukMNLwiLbVk7VM.htm](pathfinder-bestiary-items/qUukMNLwiLbVk7VM.htm)|Faerie Fire (At Will)|
|[QVGrvv3SiimPHWMd.htm](pathfinder-bestiary-items/QVGrvv3SiimPHWMd.htm)|Scimitar|+1,striking|
|[QxoUvPwEhbtilrhX.htm](pathfinder-bestiary-items/QxoUvPwEhbtilrhX.htm)|Dimension Door (At Will)|
|[R3C7okiNrevQjh4K.htm](pathfinder-bestiary-items/R3C7okiNrevQjh4K.htm)|True Seeing (Constant)|
|[r3lAOkFyEmPI0FDI.htm](pathfinder-bestiary-items/r3lAOkFyEmPI0FDI.htm)|Tongues (Constant)|
|[r4LAxgcGmPA1smfD.htm](pathfinder-bestiary-items/r4LAxgcGmPA1smfD.htm)|Ranseur|+1,striking|
|[RgFmd7UFsdE5qwLk.htm](pathfinder-bestiary-items/RgFmd7UFsdE5qwLk.htm)|True Seeing (Constant)|
|[rjvFPfDJN8mX263J.htm](pathfinder-bestiary-items/rjvFPfDJN8mX263J.htm)|Dominate (At Will) (See Dominate)|
|[RM1LE1Hf6klj9jeF.htm](pathfinder-bestiary-items/RM1LE1Hf6klj9jeF.htm)|Charm (Animals Only)|
|[rmOTng6TpYdcY50N.htm](pathfinder-bestiary-items/rmOTng6TpYdcY50N.htm)|Illusory Scene (At Will)|
|[RMvjozPBvuaFNpnM.htm](pathfinder-bestiary-items/RMvjozPBvuaFNpnM.htm)|Detect Alignment (Evil Only)|
|[rrgWq5ZjlViY9gU0.htm](pathfinder-bestiary-items/rrgWq5ZjlViY9gU0.htm)|Wall of Fire (At Will)|
|[RUBYPaAp1dlf9rWN.htm](pathfinder-bestiary-items/RUBYPaAp1dlf9rWN.htm)|Shoddy Breastplate|
|[RUyTWgEbyPh0RqWP.htm](pathfinder-bestiary-items/RUyTWgEbyPh0RqWP.htm)|Ventriloquism (At Will)|
|[rZ4kg6QQPutBXcpf.htm](pathfinder-bestiary-items/rZ4kg6QQPutBXcpf.htm)|True Seeing (Constant)|
|[rztSmsGE00F86gz4.htm](pathfinder-bestiary-items/rztSmsGE00F86gz4.htm)|Baton (Light Mace)|
|[S0TahDKKs9d1dqVY.htm](pathfinder-bestiary-items/S0TahDKKs9d1dqVY.htm)|Invisibility (At Will) (Self Only)|
|[S1A2oxm7chP51Hhb.htm](pathfinder-bestiary-items/S1A2oxm7chP51Hhb.htm)|See Invisibility (Constant)|
|[s1EiBDZD5WaFxheZ.htm](pathfinder-bestiary-items/s1EiBDZD5WaFxheZ.htm)|Detect Alignment (Evil Only)|
|[s4LwP4bMWZ2JSjZt.htm](pathfinder-bestiary-items/s4LwP4bMWZ2JSjZt.htm)|Hideous Laughter (At Will)|
|[SaFV0KJG5qfiJ37S.htm](pathfinder-bestiary-items/SaFV0KJG5qfiJ37S.htm)|Tongues (Constant)|
|[SB4qHGggtBKdeqj4.htm](pathfinder-bestiary-items/SB4qHGggtBKdeqj4.htm)|+1 Full Plate|
|[SFylGVsdK0tZfHgs.htm](pathfinder-bestiary-items/SFylGVsdK0tZfHgs.htm)|Heal|
|[sG9kkKfuPYIgwtc6.htm](pathfinder-bestiary-items/sG9kkKfuPYIgwtc6.htm)|Illusory Object (At Will)|
|[Si2S8oXrk09v5hHB.htm](pathfinder-bestiary-items/Si2S8oXrk09v5hHB.htm)|Shape Wood (At Will)|
|[SMkVM2bU3GVaaAAp.htm](pathfinder-bestiary-items/SMkVM2bU3GVaaAAp.htm)|Ventriloquism (At Will)|
|[sneqvfkcdu2lPuDv.htm](pathfinder-bestiary-items/sneqvfkcdu2lPuDv.htm)|True Seeing (Constant)|
|[Snt1v6bS8OH5a0BZ.htm](pathfinder-bestiary-items/Snt1v6bS8OH5a0BZ.htm)|Greatclub|+1,striking|
|[Sp4QTnqBVliDDjlZ.htm](pathfinder-bestiary-items/Sp4QTnqBVliDDjlZ.htm)|Freedom of Movement (Constant)|
|[STQtx0d91jcuUsAc.htm](pathfinder-bestiary-items/STQtx0d91jcuUsAc.htm)|Dimension Door (At Will)|
|[sumisHn6vBim7rI9.htm](pathfinder-bestiary-items/sumisHn6vBim7rI9.htm)|Invisibility (Self Only)|
|[SY1G16Ui8bYouUtr.htm](pathfinder-bestiary-items/SY1G16Ui8bYouUtr.htm)|Magic Missile (At Will)|
|[sYmCAhCfd1mBhKgP.htm](pathfinder-bestiary-items/sYmCAhCfd1mBhKgP.htm)|Enlarge (Self Only)|
|[szbtX0OvOcuhJmwr.htm](pathfinder-bestiary-items/szbtX0OvOcuhJmwr.htm)|Fear (At Will)|
|[T456QjgYhKt7Lzu0.htm](pathfinder-bestiary-items/T456QjgYhKt7Lzu0.htm)|Tidal Surge (At Will)|
|[t66rwfJhvjrEDP2J.htm](pathfinder-bestiary-items/t66rwfJhvjrEDP2J.htm)|Detect Alignment (At Will) (Evil Only)|
|[TAYmc59V4sXfSfw5.htm](pathfinder-bestiary-items/TAYmc59V4sXfSfw5.htm)|Darkness (At Will)|
|[Too6BHEsJmbtAhSy.htm](pathfinder-bestiary-items/Too6BHEsJmbtAhSy.htm)|Speak with Animals (Constant)|
|[tP4ZIVuA54L3bsJ5.htm](pathfinder-bestiary-items/tP4ZIVuA54L3bsJ5.htm)|Illusory Disguise (At Will)|
|[TpRnmEvCOoqUyTVg.htm](pathfinder-bestiary-items/TpRnmEvCOoqUyTVg.htm)|Know Direction|
|[tQdxiNeNIuzDNd4P.htm](pathfinder-bestiary-items/tQdxiNeNIuzDNd4P.htm)|Tongues (Constant)|
|[tUxELKWISy3m7jkP.htm](pathfinder-bestiary-items/tUxELKWISy3m7jkP.htm)|True Seeing (Constant)|
|[tVIy0kx6j3AyRLhC.htm](pathfinder-bestiary-items/tVIy0kx6j3AyRLhC.htm)|Invisibility (At Will) (Self Only)|
|[tYN9Wb2fCavnKdOo.htm](pathfinder-bestiary-items/tYN9Wb2fCavnKdOo.htm)|Detect Alignment (Evil Only)|
|[TYPM6Hiecz1NvBZD.htm](pathfinder-bestiary-items/TYPM6Hiecz1NvBZD.htm)|Mind Reading (At Will)|
|[u1TxAuWi9W8ZlwIx.htm](pathfinder-bestiary-items/u1TxAuWi9W8ZlwIx.htm)|Veil (At Will)|
|[u4K70JcBJiHmv4ei.htm](pathfinder-bestiary-items/u4K70JcBJiHmv4ei.htm)|Speak with Animals (Constant)|
|[u7KKqiGPDfFHLyot.htm](pathfinder-bestiary-items/u7KKqiGPDfFHLyot.htm)|Mind Reading (At Will)|
|[Ua4Khu52dxBq42sM.htm](pathfinder-bestiary-items/Ua4Khu52dxBq42sM.htm)|Detect Magic (Constant)|
|[UaoMjikl5YrplwCF.htm](pathfinder-bestiary-items/UaoMjikl5YrplwCF.htm)|Detect Alignment (At Will)|
|[uCNW6nYpJPObPffy.htm](pathfinder-bestiary-items/uCNW6nYpJPObPffy.htm)|Create Water (At Will)|
|[UDMJfwXSlRTQ7XYM.htm](pathfinder-bestiary-items/UDMJfwXSlRTQ7XYM.htm)|Bo Staff|+2,striking|
|[ui2iVt9gJcMIs6H5.htm](pathfinder-bestiary-items/ui2iVt9gJcMIs6H5.htm)|Gust of Wind (At Will)|
|[UPJ7JuB9V216KKIx.htm](pathfinder-bestiary-items/UPJ7JuB9V216KKIx.htm)|Divine Decree (Evil Only)|
|[uq6NvrLNEXMkTsQf.htm](pathfinder-bestiary-items/uq6NvrLNEXMkTsQf.htm)|Pass Without Trace (Constant)|
|[uqDhwoaUPCRyB1if.htm](pathfinder-bestiary-items/uqDhwoaUPCRyB1if.htm)|Darkness (At Will)|
|[UQTsotgAYqaouOS9.htm](pathfinder-bestiary-items/UQTsotgAYqaouOS9.htm)|Detect Alignment (At Will) (Lawful Only)|
|[usIMO5SmssEYlw1a.htm](pathfinder-bestiary-items/usIMO5SmssEYlw1a.htm)|Hideous Laughter (At Will)|
|[uxpBDVyA3sf3skst.htm](pathfinder-bestiary-items/uxpBDVyA3sf3skst.htm)|Bind Soul (At Will) (Heartstone)|
|[V5tm2O5zVlteOfH8.htm](pathfinder-bestiary-items/V5tm2O5zVlteOfH8.htm)|Dimension Door (At Will)|
|[V6bHuF179ATM9acB.htm](pathfinder-bestiary-items/V6bHuF179ATM9acB.htm)|Detect Alignment (Constant) (Good or Evil Only)|
|[VboJYb1ueCUJCm1Z.htm](pathfinder-bestiary-items/VboJYb1ueCUJCm1Z.htm)|Freedom of Movement (Constant)|
|[vdcGFn6EEbN3R7lP.htm](pathfinder-bestiary-items/vdcGFn6EEbN3R7lP.htm)|Charm (At Will)|
|[VdlSAnqSKP3luZC0.htm](pathfinder-bestiary-items/VdlSAnqSKP3luZC0.htm)|Dimension Door (At Will)|
|[VdNSLn7tS9xLZ2zI.htm](pathfinder-bestiary-items/VdNSLn7tS9xLZ2zI.htm)|Hypnotic Pattern (At Will)|
|[VEB7ZWhSHnvPdG3l.htm](pathfinder-bestiary-items/VEB7ZWhSHnvPdG3l.htm)|Invisibility (Self Only)|
|[VEO9wBF3d341xMhp.htm](pathfinder-bestiary-items/VEO9wBF3d341xMhp.htm)|Shatter (At Will)|
|[VmJDhZPxSn9TOQtO.htm](pathfinder-bestiary-items/VmJDhZPxSn9TOQtO.htm)|Infused Acid Flask (Moderate)|
|[VpJedVOR1dcpVdbe.htm](pathfinder-bestiary-items/VpJedVOR1dcpVdbe.htm)|Invisibility (At Will) (Self Only)|
|[VRgvIwuUtl5eBQdf.htm](pathfinder-bestiary-items/VRgvIwuUtl5eBQdf.htm)|Bastard Sword|+1,striking|
|[VUC2XUgP1Wl8GP6M.htm](pathfinder-bestiary-items/VUC2XUgP1Wl8GP6M.htm)|Tongues (Constant)|
|[VvjAbWhcvdKqnrml.htm](pathfinder-bestiary-items/VvjAbWhcvdKqnrml.htm)|Dispel Magic (At Will)|
|[VVqysFMoKtZN6DF5.htm](pathfinder-bestiary-items/VVqysFMoKtZN6DF5.htm)|Abyssal Wrath (At Will)|
|[vwrcT517AjGLCADE.htm](pathfinder-bestiary-items/vwrcT517AjGLCADE.htm)|Detect Alignment (At Will)|
|[VwVV639b1fqL180o.htm](pathfinder-bestiary-items/VwVV639b1fqL180o.htm)|Levitate (At Will)|
|[vYamsmUJUWGVxqsL.htm](pathfinder-bestiary-items/vYamsmUJUWGVxqsL.htm)|+1 Resilient Full Plate|
|[w1NzMkjL2WpASvwU.htm](pathfinder-bestiary-items/w1NzMkjL2WpASvwU.htm)|Tongues (Constant)|
|[W5lF6i5jG8ky5PSh.htm](pathfinder-bestiary-items/W5lF6i5jG8ky5PSh.htm)|Blink (At Will)|
|[w6XaAi7XP5jkc5DB.htm](pathfinder-bestiary-items/w6XaAi7XP5jkc5DB.htm)|Freedom of Movement (Constant)|
|[WdZlJxUtjDe77LMQ.htm](pathfinder-bestiary-items/WdZlJxUtjDe77LMQ.htm)|Suggestion (At Will)|
|[wf1nuRfEXOq0xBtc.htm](pathfinder-bestiary-items/wf1nuRfEXOq0xBtc.htm)|Create Water (At Will) (See Desert Thirst)|
|[wLITcsSw02suvEfW.htm](pathfinder-bestiary-items/wLITcsSw02suvEfW.htm)|Clairaudience (At Will)|
|[Wrt1wZjyxEYP1Ra0.htm](pathfinder-bestiary-items/Wrt1wZjyxEYP1Ra0.htm)|Greatsword|+1,striking|
|[WV4gTktX9AuRsFMo.htm](pathfinder-bestiary-items/WV4gTktX9AuRsFMo.htm)|Freedom of Movement (Constant)|
|[wvIjsYC3zqSnr2SA.htm](pathfinder-bestiary-items/wvIjsYC3zqSnr2SA.htm)|Spellbook Containing Their Prepared Spells|
|[WxXnLt5HTzEPK07R.htm](pathfinder-bestiary-items/WxXnLt5HTzEPK07R.htm)|True Seeing (Constant)|
|[wyBUi7dtwu1BxxLV.htm](pathfinder-bestiary-items/wyBUi7dtwu1BxxLV.htm)|Control Water (At Will)|
|[x9smkTQ1YyyY2Zcx.htm](pathfinder-bestiary-items/x9smkTQ1YyyY2Zcx.htm)|Invisibility (At Will)|
|[XAeQDTuQiMg6MDpT.htm](pathfinder-bestiary-items/XAeQDTuQiMg6MDpT.htm)|Invisibility (Self Only)|
|[xCKQqdctWOr8NXdK.htm](pathfinder-bestiary-items/xCKQqdctWOr8NXdK.htm)|Tongues (Constant)|
|[XDcfEKeWZX4di6Tq.htm](pathfinder-bestiary-items/XDcfEKeWZX4di6Tq.htm)|Secret Page (At Will)|
|[xeRH37MTBNYg5VvM.htm](pathfinder-bestiary-items/xeRH37MTBNYg5VvM.htm)|Charm (At Will)|
|[XgP4n1zKwtJiZkhR.htm](pathfinder-bestiary-items/XgP4n1zKwtJiZkhR.htm)|Calm Emotions (See Harmonize)|
|[XHYldAxPYwosrcOh.htm](pathfinder-bestiary-items/XHYldAxPYwosrcOh.htm)|Detect Alignment (At Will) (Good Only)|
|[Xj2woyiQHPYlP7Uc.htm](pathfinder-bestiary-items/Xj2woyiQHPYlP7Uc.htm)|Darkness (At Will)|
|[xlpQWhQgbWlDApYs.htm](pathfinder-bestiary-items/xlpQWhQgbWlDApYs.htm)|Tongues (Constant)|
|[xwaCzt4DsfuEQrxR.htm](pathfinder-bestiary-items/xwaCzt4DsfuEQrxR.htm)|Wind Walk (At Will)|
|[XWE3mNKb23Clyj7Y.htm](pathfinder-bestiary-items/XWE3mNKb23Clyj7Y.htm)|Scroll of Nondetection (Level 3)|
|[xZgTV6lAPmt6SkwZ.htm](pathfinder-bestiary-items/xZgTV6lAPmt6SkwZ.htm)|Fly (Constant)|
|[y2DYzWNaDHJhFnqG.htm](pathfinder-bestiary-items/y2DYzWNaDHJhFnqG.htm)|Detect Magic (Constant)|
|[y3WGfwZSPUcdAfcZ.htm](pathfinder-bestiary-items/y3WGfwZSPUcdAfcZ.htm)|Suggestion (At Will)|
|[y8Hw2ikjEBXBnei9.htm](pathfinder-bestiary-items/y8Hw2ikjEBXBnei9.htm)|Mirror Image (At Will)|
|[YBc2ftFQZOcgTMZ9.htm](pathfinder-bestiary-items/YBc2ftFQZOcgTMZ9.htm)|Blur (Self Only)|
|[YcbVbnypSBl3Jj3E.htm](pathfinder-bestiary-items/YcbVbnypSBl3Jj3E.htm)|Telekinetic Maneuver (At Will)|
|[ydxJEhxUHDUufocn.htm](pathfinder-bestiary-items/ydxJEhxUHDUufocn.htm)|Dimension Door (At Will)|
|[yfLFfSeyPeThnDPE.htm](pathfinder-bestiary-items/yfLFfSeyPeThnDPE.htm)|Ogre Hook|+1|
|[yG4up2qUxytla5BD.htm](pathfinder-bestiary-items/yG4up2qUxytla5BD.htm)|Dispel Magic (At Will)|
|[YhrNZj9qOxhmr7qj.htm](pathfinder-bestiary-items/YhrNZj9qOxhmr7qj.htm)|True Seeing (Constant)|
|[YjdrDwRb0TLvzMqQ.htm](pathfinder-bestiary-items/YjdrDwRb0TLvzMqQ.htm)|Detect Alignment (At Will) (Chaotic Only)|
|[YNOC3Kd8aTteoXJf.htm](pathfinder-bestiary-items/YNOC3Kd8aTteoXJf.htm)|Hallucinatory Terrain (See Reshape Reality)|
|[YP2dkzzsqekjVEye.htm](pathfinder-bestiary-items/YP2dkzzsqekjVEye.htm)|Greatsword|+1|
|[yp6FkMK5s9kORjtU.htm](pathfinder-bestiary-items/yp6FkMK5s9kORjtU.htm)|Mirror Image (At Will)|
|[YPkNYxysmFk49ni2.htm](pathfinder-bestiary-items/YPkNYxysmFk49ni2.htm)|Freedom of Movement (Constant)|
|[YQSLCIq7hpOxQjiS.htm](pathfinder-bestiary-items/YQSLCIq7hpOxQjiS.htm)|+1 Splint Mail|
|[yxACAkCMYt76qy2Y.htm](pathfinder-bestiary-items/yxACAkCMYt76qy2Y.htm)|Ethereal Jaunt (Self and Rider Only)|
|[YzVY8LfTkSAM0SMG.htm](pathfinder-bestiary-items/YzVY8LfTkSAM0SMG.htm)|Dimension Door (At Will)|
|[z3fpgg3t8dRw9aNR.htm](pathfinder-bestiary-items/z3fpgg3t8dRw9aNR.htm)|Illusory Object (At Will)|
|[Z3yXd9es9dSBC9ED.htm](pathfinder-bestiary-items/Z3yXd9es9dSBC9ED.htm)|Dispel Magic (At Will)|
|[z4wuFZXT6O7ZpC4m.htm](pathfinder-bestiary-items/z4wuFZXT6O7ZpC4m.htm)|Illusory Object (At Will)|
|[ZbrVT8ZXTW8YYHBJ.htm](pathfinder-bestiary-items/ZbrVT8ZXTW8YYHBJ.htm)|True Seeing (Constant)|
|[zDBzZ3PMzyUuWTwx.htm](pathfinder-bestiary-items/zDBzZ3PMzyUuWTwx.htm)|Detect Alignment (At Will) (Good Only)|
|[ZGnffTgetHNUHYLI.htm](pathfinder-bestiary-items/ZGnffTgetHNUHYLI.htm)|Detect Alignment (Evil Only)|
|[zHxfOvUJuKUsqaR4.htm](pathfinder-bestiary-items/zHxfOvUJuKUsqaR4.htm)|Haste (Constant)|
|[ZJmFD3ZikLbMpLo8.htm](pathfinder-bestiary-items/ZJmFD3ZikLbMpLo8.htm)|Sound Burst (See Harmonize)|
|[ZjPs2BJXU8fI4dyP.htm](pathfinder-bestiary-items/ZjPs2BJXU8fI4dyP.htm)|Freedom of Movement (Constant)|
|[ZNAHsWeRdyO9ciJP.htm](pathfinder-bestiary-items/ZNAHsWeRdyO9ciJP.htm)|Freedom of Movement (Constant)|
|[ZnTTiuOGrLAY6c9F.htm](pathfinder-bestiary-items/ZnTTiuOGrLAY6c9F.htm)|Control Weather (At Will)|
|[Zo1gKFEHyKae5EQL.htm](pathfinder-bestiary-items/Zo1gKFEHyKae5EQL.htm)|Hydraulic Push (At Will)|
|[ZR4nPmliirndqVhC.htm](pathfinder-bestiary-items/ZR4nPmliirndqVhC.htm)|Bola Bolts|
|[ZrwNjjoKGEvYhiL4.htm](pathfinder-bestiary-items/ZrwNjjoKGEvYhiL4.htm)|True Strike (At Will)|
|[ztlqtFH0QOR0xEwo.htm](pathfinder-bestiary-items/ztlqtFH0QOR0xEwo.htm)|Enhance Victuals (At Will)|
|[ZZcgAENc8VCfxb7e.htm](pathfinder-bestiary-items/ZZcgAENc8VCfxb7e.htm)|Illusory Disguise (At Will)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00dvi5eqFTozS4ym.htm](pathfinder-bestiary-items/00dvi5eqFTozS4ym.htm)|Vortex|Vortex|modificada|
|[00veaqCowh9RkTrj.htm](pathfinder-bestiary-items/00veaqCowh9RkTrj.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[03GLgyeAGjU6XdPt.htm](pathfinder-bestiary-items/03GLgyeAGjU6XdPt.htm)|Nature Empathy|Nature Empathy|modificada|
|[055jXlPNVGmKtAdN.htm](pathfinder-bestiary-items/055jXlPNVGmKtAdN.htm)|Water Mastery|Dominio del agua|modificada|
|[093ZnioHRqaohRiq.htm](pathfinder-bestiary-items/093ZnioHRqaohRiq.htm)|Throw Rock|Arrojar roca|modificada|
|[09dEV7gYGOvdo2T6.htm](pathfinder-bestiary-items/09dEV7gYGOvdo2T6.htm)|Water Mastery|Dominio del agua|modificada|
|[09tr8RqXPJwNExtR.htm](pathfinder-bestiary-items/09tr8RqXPJwNExtR.htm)|Deafening Aria|Aria ensordecedora|modificada|
|[0A47mfqDMbHBxJqs.htm](pathfinder-bestiary-items/0A47mfqDMbHBxJqs.htm)|Frightful Presence|Frightful Presence|modificada|
|[0bcvSP6hBMQd3ATb.htm](pathfinder-bestiary-items/0bcvSP6hBMQd3ATb.htm)|Snow Vision|Snow Vision|modificada|
|[0BMCA9P7mLMAMs10.htm](pathfinder-bestiary-items/0BMCA9P7mLMAMs10.htm)|Sunder Objects|Quebrar objetos|modificada|
|[0C8sO34ceJyWY2uw.htm](pathfinder-bestiary-items/0C8sO34ceJyWY2uw.htm)|Tail|Tail|modificada|
|[0d64w4IL9e8s6Tft.htm](pathfinder-bestiary-items/0d64w4IL9e8s6Tft.htm)|Frightful Presence|Frightful Presence|modificada|
|[0EEefBsju5qv29p5.htm](pathfinder-bestiary-items/0EEefBsju5qv29p5.htm)|Fangs|Colmillos|modificada|
|[0Ew7KigrRdcxAx7K.htm](pathfinder-bestiary-items/0Ew7KigrRdcxAx7K.htm)|Jaws|Fauces|modificada|
|[0F1cIENDKxVfgMvZ.htm](pathfinder-bestiary-items/0F1cIENDKxVfgMvZ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0FCCLjtCWvBlUoFA.htm](pathfinder-bestiary-items/0FCCLjtCWvBlUoFA.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[0fQVIqLvgnfSP4JM.htm](pathfinder-bestiary-items/0fQVIqLvgnfSP4JM.htm)|Stinger|Aguijón|modificada|
|[0Hf2BPIsl4GkboQ4.htm](pathfinder-bestiary-items/0Hf2BPIsl4GkboQ4.htm)|Dagger|Daga|modificada|
|[0HSgD6W8PvrZ62ts.htm](pathfinder-bestiary-items/0HSgD6W8PvrZ62ts.htm)|Shark Commune 150 feet|Tiburón comunión 150 pies|modificada|
|[0IoDOGJ3snGokTeZ.htm](pathfinder-bestiary-items/0IoDOGJ3snGokTeZ.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[0IyzaM7Q6UJqM2St.htm](pathfinder-bestiary-items/0IyzaM7Q6UJqM2St.htm)|Spear|Lanza|modificada|
|[0IZG1AJ5MeYSR9mc.htm](pathfinder-bestiary-items/0IZG1AJ5MeYSR9mc.htm)|Tail|Tail|modificada|
|[0JCakW3iPRouHFU2.htm](pathfinder-bestiary-items/0JCakW3iPRouHFU2.htm)|Swallow Whole|Engullir Todo|modificada|
|[0kjAz4Df4rLFyFN6.htm](pathfinder-bestiary-items/0kjAz4Df4rLFyFN6.htm)|Grab|Agarrado|modificada|
|[0KLll6op6LwSp19s.htm](pathfinder-bestiary-items/0KLll6op6LwSp19s.htm)|Hydra Regeneration|Regeneración de hidra|modificada|
|[0L6qwj5JpEiwbnph.htm](pathfinder-bestiary-items/0L6qwj5JpEiwbnph.htm)|Storm of Jaws|Tormenta de mordiscos|modificada|
|[0lFm2C1Slc60GuoD.htm](pathfinder-bestiary-items/0lFm2C1Slc60GuoD.htm)|Change Shape|Change Shape|modificada|
|[0lokdD3KSOzhNMOi.htm](pathfinder-bestiary-items/0lokdD3KSOzhNMOi.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[0lvwb1cNAaOUc6Be.htm](pathfinder-bestiary-items/0lvwb1cNAaOUc6Be.htm)|End the Charade|Poner fin a la farsa|modificada|
|[0mC5JVblweUZ0WxU.htm](pathfinder-bestiary-items/0mC5JVblweUZ0WxU.htm)|Jaws|Fauces|modificada|
|[0mf0RfMRzuYHP7t3.htm](pathfinder-bestiary-items/0mf0RfMRzuYHP7t3.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0mOkmWdA59zQVh9W.htm](pathfinder-bestiary-items/0mOkmWdA59zQVh9W.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0naynsYzlUHn8xiZ.htm](pathfinder-bestiary-items/0naynsYzlUHn8xiZ.htm)|Breath Weapon|Breath Weapon|modificada|
|[0odea1XiQLeiXEH3.htm](pathfinder-bestiary-items/0odea1XiQLeiXEH3.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[0oICLdGDJMUmfVhn.htm](pathfinder-bestiary-items/0oICLdGDJMUmfVhn.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[0OOTRB9nI9G0zd8v.htm](pathfinder-bestiary-items/0OOTRB9nI9G0zd8v.htm)|Breath Weapon|Breath Weapon|modificada|
|[0oxd4GYRm9A47FeP.htm](pathfinder-bestiary-items/0oxd4GYRm9A47FeP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0Pe1uBGVehRLhBim.htm](pathfinder-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[0PlcoqdlSWJK4XCo.htm](pathfinder-bestiary-items/0PlcoqdlSWJK4XCo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0QQqIESJlQrO4KUL.htm](pathfinder-bestiary-items/0QQqIESJlQrO4KUL.htm)|Talon|Talon|modificada|
|[0Qt9ULbGzJV8ZG3W.htm](pathfinder-bestiary-items/0Qt9ULbGzJV8ZG3W.htm)|Shadow Spawn|Sombra Spawn|modificada|
|[0qVGuAvlfZ7EplZ5.htm](pathfinder-bestiary-items/0qVGuAvlfZ7EplZ5.htm)|Infuse Weapon|Infundir armas|modificada|
|[0qvSsLdTulsncbsi.htm](pathfinder-bestiary-items/0qvSsLdTulsncbsi.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[0QWVJ7Of8cQNKp0N.htm](pathfinder-bestiary-items/0QWVJ7Of8cQNKp0N.htm)|Slink in Shadows|Escabullirse entre las sombras|modificada|
|[0rf640gBxRc7gfuM.htm](pathfinder-bestiary-items/0rf640gBxRc7gfuM.htm)|Spatial Riptide|Riptide espacial|modificada|
|[0RsLlXY365ytSohG.htm](pathfinder-bestiary-items/0RsLlXY365ytSohG.htm)|Countermeasures|Contramedidas|modificada|
|[0RWfxFkqPwKKKdiY.htm](pathfinder-bestiary-items/0RWfxFkqPwKKKdiY.htm)|Ground Slam|Aporrear el suelo|modificada|
|[0SVph284K2Css2S6.htm](pathfinder-bestiary-items/0SVph284K2Css2S6.htm)|Implant Eggs|Implantar Huevos|modificada|
|[0t42IbY5dtDOP3PD.htm](pathfinder-bestiary-items/0t42IbY5dtDOP3PD.htm)|Jaws|Fauces|modificada|
|[0UgwXoyN2VLdj2sc.htm](pathfinder-bestiary-items/0UgwXoyN2VLdj2sc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0UIiiS68BvgFMTnC.htm](pathfinder-bestiary-items/0UIiiS68BvgFMTnC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0uJ1CwhddCrEZUCC.htm](pathfinder-bestiary-items/0uJ1CwhddCrEZUCC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0uxUMo5bNxKA5eIg.htm](pathfinder-bestiary-items/0uxUMo5bNxKA5eIg.htm)|Change Shape|Change Shape|modificada|
|[0uxyJLMRCpbV8CsL.htm](pathfinder-bestiary-items/0uxyJLMRCpbV8CsL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[0vN6RMlNxCUWYSPj.htm](pathfinder-bestiary-items/0vN6RMlNxCUWYSPj.htm)|Spore Pod|Spore Pod|modificada|
|[0vSVpFhBgh2bSdVc.htm](pathfinder-bestiary-items/0vSVpFhBgh2bSdVc.htm)|Fist|Puño|modificada|
|[0w3FhoNrrNel3ZcH.htm](pathfinder-bestiary-items/0w3FhoNrrNel3ZcH.htm)|Negative Healing|Curación negativa|modificada|
|[0WRHP09tquSt6GBg.htm](pathfinder-bestiary-items/0WRHP09tquSt6GBg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[0xfJmXxtDph8nDjZ.htm](pathfinder-bestiary-items/0xfJmXxtDph8nDjZ.htm)|Dagger|Daga|modificada|
|[0xo9GafMlJXTsSNk.htm](pathfinder-bestiary-items/0xo9GafMlJXTsSNk.htm)|Predator's Advantage|Predator's Advantage|modificada|
|[0Xocyn67jFjoV2Ff.htm](pathfinder-bestiary-items/0Xocyn67jFjoV2Ff.htm)|Catch Rock|Atrapar roca|modificada|
|[0XydXCehPsY2yGto.htm](pathfinder-bestiary-items/0XydXCehPsY2yGto.htm)|Bristles|Erizarse|modificada|
|[0Xz9Bpg1z49xuutq.htm](pathfinder-bestiary-items/0Xz9Bpg1z49xuutq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[0yW0yjKdtDS5ur0I.htm](pathfinder-bestiary-items/0yW0yjKdtDS5ur0I.htm)|Breath Weapon|Breath Weapon|modificada|
|[0ZKaotgANUyAmZDc.htm](pathfinder-bestiary-items/0ZKaotgANUyAmZDc.htm)|Cat's Luck|Suerte felina|modificada|
|[11mRPoFSEt1nYAnE.htm](pathfinder-bestiary-items/11mRPoFSEt1nYAnE.htm)|Slowing Pulse|Pulso de lentitud|modificada|
|[12LD6QHUzml95C9n.htm](pathfinder-bestiary-items/12LD6QHUzml95C9n.htm)|Frost Greatsword|Gélida Greatsword|modificada|
|[13YXYsRSLVocnnzA.htm](pathfinder-bestiary-items/13YXYsRSLVocnnzA.htm)|Jaws|Fauces|modificada|
|[16mvgME2H3Zp8aMW.htm](pathfinder-bestiary-items/16mvgME2H3Zp8aMW.htm)|Jaws|Fauces|modificada|
|[19cNQjbQEVxmiger.htm](pathfinder-bestiary-items/19cNQjbQEVxmiger.htm)|Telekinetic Whirlwind|Remolino telecinético|modificada|
|[1AbBtZhPY7lYpSm0.htm](pathfinder-bestiary-items/1AbBtZhPY7lYpSm0.htm)|Goblin Pox|Viruela de goblin|modificada|
|[1AemwaFBoSAJ1UHj.htm](pathfinder-bestiary-items/1AemwaFBoSAJ1UHj.htm)|Change Shape|Change Shape|modificada|
|[1ArJ2KyY45DuuLtC.htm](pathfinder-bestiary-items/1ArJ2KyY45DuuLtC.htm)|Crashing Wind|Sacudida de viento|modificada|
|[1AzFNDZFiaJxTAJf.htm](pathfinder-bestiary-items/1AzFNDZFiaJxTAJf.htm)|Undead Steed|Corcel muerto viviente|modificada|
|[1BwIo7mgXKcuS2Vg.htm](pathfinder-bestiary-items/1BwIo7mgXKcuS2Vg.htm)|Weakening Strike|Golpe debilitador|modificada|
|[1cbIF1wUJSF3WWPk.htm](pathfinder-bestiary-items/1cbIF1wUJSF3WWPk.htm)|Telekinetic Object (Bludgeoning)|Objeto Telequinético (Bludgeoning)|modificada|
|[1dUCb8dBnYQ0kq4t.htm](pathfinder-bestiary-items/1dUCb8dBnYQ0kq4t.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[1eQUxiVPWo5ndGVE.htm](pathfinder-bestiary-items/1eQUxiVPWo5ndGVE.htm)|Frightful Presence|Frightful Presence|modificada|
|[1EVHcXa6gUWUY0Cl.htm](pathfinder-bestiary-items/1EVHcXa6gUWUY0Cl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1EXMfAvy6zP7X2ix.htm](pathfinder-bestiary-items/1EXMfAvy6zP7X2ix.htm)|Consume Memories|Consumir recuerdos|modificada|
|[1fzwu2J1jldVzBPc.htm](pathfinder-bestiary-items/1fzwu2J1jldVzBPc.htm)|Coil|Retorcer|modificada|
|[1GBTPnqIIGld3Wu1.htm](pathfinder-bestiary-items/1GBTPnqIIGld3Wu1.htm)|Dragonscaled|Escamas de dragón|modificada|
|[1gKtaE366xqxGDdV.htm](pathfinder-bestiary-items/1gKtaE366xqxGDdV.htm)|Formation|Formación|modificada|
|[1HJ58kI3YTxmr0Cw.htm](pathfinder-bestiary-items/1HJ58kI3YTxmr0Cw.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[1hjabmTtR1jzUEh9.htm](pathfinder-bestiary-items/1hjabmTtR1jzUEh9.htm)|Hellish Revenge|Venganza infernal|modificada|
|[1ich4lSlipgnuhyC.htm](pathfinder-bestiary-items/1ich4lSlipgnuhyC.htm)|Despair|Desesperación|modificada|
|[1jREnmv7xdTiRRHp.htm](pathfinder-bestiary-items/1jREnmv7xdTiRRHp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[1L0T2L67vRGgNyeg.htm](pathfinder-bestiary-items/1L0T2L67vRGgNyeg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1lCsSRhidCzWS3Hd.htm](pathfinder-bestiary-items/1lCsSRhidCzWS3Hd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1msRWqXGXC7BMldV.htm](pathfinder-bestiary-items/1msRWqXGXC7BMldV.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[1N1JI8DtK2Tm842J.htm](pathfinder-bestiary-items/1N1JI8DtK2Tm842J.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[1N7APEdVVZHtrMup.htm](pathfinder-bestiary-items/1N7APEdVVZHtrMup.htm)|Cacodaemonia|Cacodaemonia|modificada|
|[1O3Wpc1lqSdFezG5.htm](pathfinder-bestiary-items/1O3Wpc1lqSdFezG5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1oQJIAQyfTpYuBCX.htm](pathfinder-bestiary-items/1oQJIAQyfTpYuBCX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[1P2Kly3ELf2X8jdv.htm](pathfinder-bestiary-items/1P2Kly3ELf2X8jdv.htm)|Jaw|Fauces|modificada|
|[1pyQC74ZkhcOllQ8.htm](pathfinder-bestiary-items/1pyQC74ZkhcOllQ8.htm)|Hatchet|Hacha|modificada|
|[1qFhgFYCKIDaF8mk.htm](pathfinder-bestiary-items/1qFhgFYCKIDaF8mk.htm)|Devour Soul|Devorar alma|modificada|
|[1qswe7SVM8T7WkWr.htm](pathfinder-bestiary-items/1qswe7SVM8T7WkWr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1QVkgQh59Pmq5fP3.htm](pathfinder-bestiary-items/1QVkgQh59Pmq5fP3.htm)|Splintered Ground|Splintered Ground|modificada|
|[1RIzUNU0v2cfgsjD.htm](pathfinder-bestiary-items/1RIzUNU0v2cfgsjD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1rQD15HWHf8e3q5K.htm](pathfinder-bestiary-items/1rQD15HWHf8e3q5K.htm)|Jaws|Fauces|modificada|
|[1TeUoHVS0bkCD9jX.htm](pathfinder-bestiary-items/1TeUoHVS0bkCD9jX.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[1TgSjt8UKPwTjAH9.htm](pathfinder-bestiary-items/1TgSjt8UKPwTjAH9.htm)|Rugged Travel|Viajero curtido|modificada|
|[1U37Lsr3NMgzLqLM.htm](pathfinder-bestiary-items/1U37Lsr3NMgzLqLM.htm)|Mandibles|Mandíbulas|modificada|
|[1UAh4ooLm8sQPmeN.htm](pathfinder-bestiary-items/1UAh4ooLm8sQPmeN.htm)|Spirit Touch|Spirit Touch|modificada|
|[1UmNfowZ4HWxS9rx.htm](pathfinder-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|Explosión devastadora|modificada|
|[1uWYYut6bpKdXgNp.htm](pathfinder-bestiary-items/1uWYYut6bpKdXgNp.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[1uZuWVNr0woQanKG.htm](pathfinder-bestiary-items/1uZuWVNr0woQanKG.htm)|Claw Rake|Rastrillo de Garras|modificada|
|[1V9uaQOnCMGgMxl2.htm](pathfinder-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1VdxeWdeAuS34ymv.htm](pathfinder-bestiary-items/1VdxeWdeAuS34ymv.htm)|Grab|Agarrado|modificada|
|[1Vdy1h0xvUBMB9Mg.htm](pathfinder-bestiary-items/1Vdy1h0xvUBMB9Mg.htm)|Sticky Spores|Sticky Spores|modificada|
|[1vjI6FDegZD6lS85.htm](pathfinder-bestiary-items/1vjI6FDegZD6lS85.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[1VqUGL35cW3NbTOz.htm](pathfinder-bestiary-items/1VqUGL35cW3NbTOz.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[1wtLTJx2lkvun6gV.htm](pathfinder-bestiary-items/1wtLTJx2lkvun6gV.htm)|Ice Climb|Trepar por el hielo|modificada|
|[1xcDVCY3sx0uDDys.htm](pathfinder-bestiary-items/1xcDVCY3sx0uDDys.htm)|Greater Constrict|Mayor Restricción|modificada|
|[1xLTVMSCCIoZiFMc.htm](pathfinder-bestiary-items/1xLTVMSCCIoZiFMc.htm)|Constant Spells|Constant Spells|modificada|
|[1yMkzmd8oyagICBZ.htm](pathfinder-bestiary-items/1yMkzmd8oyagICBZ.htm)|Ice Climb|Trepar por el hielo|modificada|
|[1zCVgprb4WVHkIld.htm](pathfinder-bestiary-items/1zCVgprb4WVHkIld.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[206EmbcftIC6vntv.htm](pathfinder-bestiary-items/206EmbcftIC6vntv.htm)|Big Swing|Gran barrido|modificada|
|[209XT5BBx9vzGaVU.htm](pathfinder-bestiary-items/209XT5BBx9vzGaVU.htm)|Frost Longspear|Gélida Longspear|modificada|
|[20rnB59A7ShaRC2n.htm](pathfinder-bestiary-items/20rnB59A7ShaRC2n.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[20wHQZMjGAPl30DH.htm](pathfinder-bestiary-items/20wHQZMjGAPl30DH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[20xbqCZ1qrIT5Yqx.htm](pathfinder-bestiary-items/20xbqCZ1qrIT5Yqx.htm)|Ensnare|Enredar|modificada|
|[21Edo8B2yXlM0Sd7.htm](pathfinder-bestiary-items/21Edo8B2yXlM0Sd7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[21tFYXEcsuyuh5GQ.htm](pathfinder-bestiary-items/21tFYXEcsuyuh5GQ.htm)|Warbling Song|Canto gorjeante|modificada|
|[23duj3dLvK5Gn9tK.htm](pathfinder-bestiary-items/23duj3dLvK5Gn9tK.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[24g2FSPaGiTvJB6C.htm](pathfinder-bestiary-items/24g2FSPaGiTvJB6C.htm)|Fling|Lanzar por los aires|modificada|
|[27wudVSiScZejHwt.htm](pathfinder-bestiary-items/27wudVSiScZejHwt.htm)|Negative Healing|Curación negativa|modificada|
|[28p2N35JT0y99D6q.htm](pathfinder-bestiary-items/28p2N35JT0y99D6q.htm)|Trackless Step|Pisada sin rastro|modificada|
|[28P5kFSjVh3w1R9o.htm](pathfinder-bestiary-items/28P5kFSjVh3w1R9o.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[28PBBJfMs67GTIf3.htm](pathfinder-bestiary-items/28PBBJfMs67GTIf3.htm)|Leg|Pierna|modificada|
|[29EzhqCR2u7R7K4N.htm](pathfinder-bestiary-items/29EzhqCR2u7R7K4N.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2arq3zK9f6JQ5ZcN.htm](pathfinder-bestiary-items/2arq3zK9f6JQ5ZcN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2BBPsLk2YiOeDwpm.htm](pathfinder-bestiary-items/2BBPsLk2YiOeDwpm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2bECIomVCBjAE2ET.htm](pathfinder-bestiary-items/2bECIomVCBjAE2ET.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[2d3ZGRIXERcjqnmJ.htm](pathfinder-bestiary-items/2d3ZGRIXERcjqnmJ.htm)|Grab|Agarrado|modificada|
|[2DfR3FEW268SuAYe.htm](pathfinder-bestiary-items/2DfR3FEW268SuAYe.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[2ECIwvcXVQ6w8vi5.htm](pathfinder-bestiary-items/2ECIwvcXVQ6w8vi5.htm)|Punishing Momentum|Impulso castigador|modificada|
|[2ER4MKCdewC2qqrQ.htm](pathfinder-bestiary-items/2ER4MKCdewC2qqrQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2eV3trm8nKeqQ2Z4.htm](pathfinder-bestiary-items/2eV3trm8nKeqQ2Z4.htm)|Distracting Frolic|Cabriolas desconcertantes|modificada|
|[2fZ7E297Z4PtEWQg.htm](pathfinder-bestiary-items/2fZ7E297Z4PtEWQg.htm)|Hoof|Hoof|modificada|
|[2gy1EqZ99rbJPoec.htm](pathfinder-bestiary-items/2gy1EqZ99rbJPoec.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[2h0yKU2nKX609M3Q.htm](pathfinder-bestiary-items/2h0yKU2nKX609M3Q.htm)|Jaws|Fauces|modificada|
|[2hjq0oo2EsTcXPJe.htm](pathfinder-bestiary-items/2hjq0oo2EsTcXPJe.htm)|Tail|Tail|modificada|
|[2iGIMuumKfirLAzh.htm](pathfinder-bestiary-items/2iGIMuumKfirLAzh.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[2in6dEOh1ygunO7S.htm](pathfinder-bestiary-items/2in6dEOh1ygunO7S.htm)|Constant Spells|Constant Spells|modificada|
|[2J5aw0w2MvuE4ISK.htm](pathfinder-bestiary-items/2J5aw0w2MvuE4ISK.htm)|Claw|Garra|modificada|
|[2JiFczs0m1iIKqPZ.htm](pathfinder-bestiary-items/2JiFczs0m1iIKqPZ.htm)|Flurry of Strands|Ráfaga de Hebras|modificada|
|[2jnfsvQ0IphAdWs2.htm](pathfinder-bestiary-items/2jnfsvQ0IphAdWs2.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[2lGYb3ZO8pwxgMhD.htm](pathfinder-bestiary-items/2lGYb3ZO8pwxgMhD.htm)|Focus Gaze|Centrar mirada|modificada|
|[2m9M9QruB9jYdEjd.htm](pathfinder-bestiary-items/2m9M9QruB9jYdEjd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2MJUBT30Vn8bziTk.htm](pathfinder-bestiary-items/2MJUBT30Vn8bziTk.htm)|Two-Headed Strike|Golpe bicéfalo|modificada|
|[2NA51H5N0n0VDjGt.htm](pathfinder-bestiary-items/2NA51H5N0n0VDjGt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2nd3Ete1Wir3K3bj.htm](pathfinder-bestiary-items/2nd3Ete1Wir3K3bj.htm)|Wave|Wave|modificada|
|[2nWoUs5yTHmZJd6s.htm](pathfinder-bestiary-items/2nWoUs5yTHmZJd6s.htm)|Breath Weapon|Breath Weapon|modificada|
|[2OIVJJBOOtBD6Bce.htm](pathfinder-bestiary-items/2OIVJJBOOtBD6Bce.htm)|Horn|Cuerno|modificada|
|[2OPayJrYVYfP1LZk.htm](pathfinder-bestiary-items/2OPayJrYVYfP1LZk.htm)|Red Cap|Gorro rojo|modificada|
|[2PnyMPtvBpBFJJyw.htm](pathfinder-bestiary-items/2PnyMPtvBpBFJJyw.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[2QFZHcrVJOpbzlkr.htm](pathfinder-bestiary-items/2QFZHcrVJOpbzlkr.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2QnPX6haUy6RjKvT.htm](pathfinder-bestiary-items/2QnPX6haUy6RjKvT.htm)|Frenzy Pheromone|Feromona de frenesí|modificada|
|[2rAKighB6Ywhk5s3.htm](pathfinder-bestiary-items/2rAKighB6Ywhk5s3.htm)|Shock|Electrizante|modificada|
|[2rqAbhpsIBtY1lZp.htm](pathfinder-bestiary-items/2rqAbhpsIBtY1lZp.htm)|Regeneration 15 (Deactivated by Chaotic)|Regeneración 15 (Desactivado por Caótico).|modificada|
|[2S4BfxyE7xLYIYr3.htm](pathfinder-bestiary-items/2S4BfxyE7xLYIYr3.htm)|Grab|Agarrado|modificada|
|[2sLJMpYvshlQx7Ck.htm](pathfinder-bestiary-items/2sLJMpYvshlQx7Ck.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2T8gBoUiFYXYQ2wp.htm](pathfinder-bestiary-items/2T8gBoUiFYXYQ2wp.htm)|Rock|Roca|modificada|
|[2TBzK79NxowRc0As.htm](pathfinder-bestiary-items/2TBzK79NxowRc0As.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2Tk2P2vXOBYquTtd.htm](pathfinder-bestiary-items/2Tk2P2vXOBYquTtd.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[2u5CXi7tZODYDpbJ.htm](pathfinder-bestiary-items/2u5CXi7tZODYDpbJ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[2ujro0hgy7xKZGw3.htm](pathfinder-bestiary-items/2ujro0hgy7xKZGw3.htm)|Constant Spells|Constant Spells|modificada|
|[2UpzHQ985k9cDkOT.htm](pathfinder-bestiary-items/2UpzHQ985k9cDkOT.htm)|Swiftness|Celeridad|modificada|
|[2V1LMw2K5gNCyFjF.htm](pathfinder-bestiary-items/2V1LMw2K5gNCyFjF.htm)|Shuln Saliva|Shuln Saliva|modificada|
|[2V6mUk75bI02ShGa.htm](pathfinder-bestiary-items/2V6mUk75bI02ShGa.htm)|Silver Rapier|Estoque de plata|modificada|
|[2vBMTexIigEtaKt3.htm](pathfinder-bestiary-items/2vBMTexIigEtaKt3.htm)|Breath Weapon|Breath Weapon|modificada|
|[2vkb6KDhuhqgqrlz.htm](pathfinder-bestiary-items/2vkb6KDhuhqgqrlz.htm)|Warhammer|Warhammer|modificada|
|[2vwlAmF93myITMjw.htm](pathfinder-bestiary-items/2vwlAmF93myITMjw.htm)|Sling|Sling|modificada|
|[2vzPRuTJZNWARxiC.htm](pathfinder-bestiary-items/2vzPRuTJZNWARxiC.htm)|Tail Lash|Azote con la cola|modificada|
|[2wdfh7JIT7SjhXIz.htm](pathfinder-bestiary-items/2wdfh7JIT7SjhXIz.htm)|Claw|Garra|modificada|
|[2wvbMbHq2fdLmeju.htm](pathfinder-bestiary-items/2wvbMbHq2fdLmeju.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[2xaMJwr5gjV30x3x.htm](pathfinder-bestiary-items/2xaMJwr5gjV30x3x.htm)|Drench|Sofocar|modificada|
|[2XSVapoeItmk2zU0.htm](pathfinder-bestiary-items/2XSVapoeItmk2zU0.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[2YipTTPgQfiSBAHG.htm](pathfinder-bestiary-items/2YipTTPgQfiSBAHG.htm)|Climb Stone|Trepar por la piedra|modificada|
|[2YwZxBJyyprGUZdO.htm](pathfinder-bestiary-items/2YwZxBJyyprGUZdO.htm)|Jaws|Fauces|modificada|
|[30Jii0vTB1Vs9wP6.htm](pathfinder-bestiary-items/30Jii0vTB1Vs9wP6.htm)|Jaws|Fauces|modificada|
|[311KXJPh4RSeBSYh.htm](pathfinder-bestiary-items/311KXJPh4RSeBSYh.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[314KNkmZCbFk7BMQ.htm](pathfinder-bestiary-items/314KNkmZCbFk7BMQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[36QKwrAooz1cygR4.htm](pathfinder-bestiary-items/36QKwrAooz1cygR4.htm)|Cytillesh Stare|Mirada de cytilesh|modificada|
|[36TWBrEib7ZN4zrU.htm](pathfinder-bestiary-items/36TWBrEib7ZN4zrU.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[36UmeWHlowHAjVXV.htm](pathfinder-bestiary-items/36UmeWHlowHAjVXV.htm)|Combat Grab|Agarrar en combate|modificada|
|[383lohD54D45DNRU.htm](pathfinder-bestiary-items/383lohD54D45DNRU.htm)|Lance|Lance|modificada|
|[387c1CSiNx4uWeqZ.htm](pathfinder-bestiary-items/387c1CSiNx4uWeqZ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[38uEEYKxUvG8TDFw.htm](pathfinder-bestiary-items/38uEEYKxUvG8TDFw.htm)|Crossbow|Ballesta|modificada|
|[39ZIhV5v80216sO2.htm](pathfinder-bestiary-items/39ZIhV5v80216sO2.htm)|Manifold Vision|Visión multiplicada|modificada|
|[3A8TmAuVP2C3XDsX.htm](pathfinder-bestiary-items/3A8TmAuVP2C3XDsX.htm)|Refuse Pile|Refuse Pile|modificada|
|[3aQmojgOn0OyqrYB.htm](pathfinder-bestiary-items/3aQmojgOn0OyqrYB.htm)|Retributive Strike|Golpe retributivo|modificada|
|[3b7IgOx3d3REF2NR.htm](pathfinder-bestiary-items/3b7IgOx3d3REF2NR.htm)|Swoop|Swoop|modificada|
|[3BMDL7DFtWP5HozZ.htm](pathfinder-bestiary-items/3BMDL7DFtWP5HozZ.htm)|Traveler's Aura|Aura del viajero|modificada|
|[3cGYB1usBk6SXYto.htm](pathfinder-bestiary-items/3cGYB1usBk6SXYto.htm)|Jaws|Fauces|modificada|
|[3CJoA00OmtSsKcBR.htm](pathfinder-bestiary-items/3CJoA00OmtSsKcBR.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[3d5cEC77yvSI6Hub.htm](pathfinder-bestiary-items/3d5cEC77yvSI6Hub.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3EFMknJHLeVqcKmf.htm](pathfinder-bestiary-items/3EFMknJHLeVqcKmf.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[3EkDCh4CkGrgHhlf.htm](pathfinder-bestiary-items/3EkDCh4CkGrgHhlf.htm)|Antenna Disarm|Antenna Desarmar|modificada|
|[3Eksrmh390T4SrP7.htm](pathfinder-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[3g9yO52wil4VC4Dz.htm](pathfinder-bestiary-items/3g9yO52wil4VC4Dz.htm)|Fangs|Colmillos|modificada|
|[3GFUGIkhpZDZer4p.htm](pathfinder-bestiary-items/3GFUGIkhpZDZer4p.htm)|Hoof|Hoof|modificada|
|[3GqX6rg7gxLsmuvd.htm](pathfinder-bestiary-items/3GqX6rg7gxLsmuvd.htm)|Rock|Roca|modificada|
|[3gYmeheE54dPEZnX.htm](pathfinder-bestiary-items/3gYmeheE54dPEZnX.htm)|Storm Breath|Aliento de tormenta|modificada|
|[3H0tPL2AeonSy2gM.htm](pathfinder-bestiary-items/3H0tPL2AeonSy2gM.htm)|Jet|Jet|modificada|
|[3HkWPtBVUhYiB7al.htm](pathfinder-bestiary-items/3HkWPtBVUhYiB7al.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[3HPvO9ipnRupy7G0.htm](pathfinder-bestiary-items/3HPvO9ipnRupy7G0.htm)|Living Shield|Escudo viviente|modificada|
|[3Jg7XQGrHZ2lpqc1.htm](pathfinder-bestiary-items/3Jg7XQGrHZ2lpqc1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3jppXkfB2qjbeRic.htm](pathfinder-bestiary-items/3jppXkfB2qjbeRic.htm)|Vortex|Vortex|modificada|
|[3KkpzgxFNPh9aZJ4.htm](pathfinder-bestiary-items/3KkpzgxFNPh9aZJ4.htm)|Talon|Talon|modificada|
|[3nwKZarzSffmcKzH.htm](pathfinder-bestiary-items/3nwKZarzSffmcKzH.htm)|Improved Grab|Agarrado mejorado|modificada|
|[3o5i63MTjWyPqXG3.htm](pathfinder-bestiary-items/3o5i63MTjWyPqXG3.htm)|Transfer Protection|Protección de transferencias|modificada|
|[3psvmOBMbToGGKnF.htm](pathfinder-bestiary-items/3psvmOBMbToGGKnF.htm)|Claw|Garra|modificada|
|[3RchvgPIdNc2KUu9.htm](pathfinder-bestiary-items/3RchvgPIdNc2KUu9.htm)|Fist|Puño|modificada|
|[3RCPLD1d6zfYm2VI.htm](pathfinder-bestiary-items/3RCPLD1d6zfYm2VI.htm)|Rugged Travel|Viajero curtido|modificada|
|[3SNH7v25J8TbPWvC.htm](pathfinder-bestiary-items/3SNH7v25J8TbPWvC.htm)|Construct Armor (Hardness 2)|Construir Armadura (Dureza 2)|modificada|
|[3SpNnQV9X8P44yW0.htm](pathfinder-bestiary-items/3SpNnQV9X8P44yW0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3TfIMe12rJpDf9LZ.htm](pathfinder-bestiary-items/3TfIMe12rJpDf9LZ.htm)|Self-Loathing|Autodesprecio|modificada|
|[3tXLO7Kl4rqCcOSV.htm](pathfinder-bestiary-items/3tXLO7Kl4rqCcOSV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[3tyBaMgyINlZCj76.htm](pathfinder-bestiary-items/3tyBaMgyINlZCj76.htm)|Fist|Puño|modificada|
|[3TZWlkMSDjauBP7y.htm](pathfinder-bestiary-items/3TZWlkMSDjauBP7y.htm)|Jaws|Fauces|modificada|
|[3uMuKyVKQxpFxP0o.htm](pathfinder-bestiary-items/3uMuKyVKQxpFxP0o.htm)|Flash of Brutality|Destello de brutalidad|modificada|
|[3USivzG0qztGd1dK.htm](pathfinder-bestiary-items/3USivzG0qztGd1dK.htm)|Knockdown|Derribo|modificada|
|[3VDIY4vCKlW0qvFl.htm](pathfinder-bestiary-items/3VDIY4vCKlW0qvFl.htm)|Fist|Puño|modificada|
|[3ViZWXlxM2CUiPGG.htm](pathfinder-bestiary-items/3ViZWXlxM2CUiPGG.htm)|Quicken Pestilence|Acelerar Pestilencia|modificada|
|[3wMfK5zZSqFhhTgs.htm](pathfinder-bestiary-items/3wMfK5zZSqFhhTgs.htm)|Primordial Roar|Primordial Roar|modificada|
|[3WumD6XU2ACGghbI.htm](pathfinder-bestiary-items/3WumD6XU2ACGghbI.htm)|+2 Circumstance to All Saves vs. Dream and Sleep|+2 Circunstancia a todas las salvaciones contra soñar y dormir.|modificada|
|[3wWPzjr3ra62ks2k.htm](pathfinder-bestiary-items/3wWPzjr3ra62ks2k.htm)|Swarm Mind|Swarm Mind|modificada|
|[3X0mRQVlCkn2NC2d.htm](pathfinder-bestiary-items/3X0mRQVlCkn2NC2d.htm)|Acid Glob|Globo Ácido|modificada|
|[3XJEBmHGHfUj7TbN.htm](pathfinder-bestiary-items/3XJEBmHGHfUj7TbN.htm)|Piteous Moan|Gemido lastimero|modificada|
|[3XNydMhIMm8EBfhy.htm](pathfinder-bestiary-items/3XNydMhIMm8EBfhy.htm)|Dagger|Daga|modificada|
|[3xoqcUVujuOLU773.htm](pathfinder-bestiary-items/3xoqcUVujuOLU773.htm)|Menacing Guardian|Guardián amenazante|modificada|
|[3ZSVEZBMKleuEd84.htm](pathfinder-bestiary-items/3ZSVEZBMKleuEd84.htm)|Dominate|Dominar|modificada|
|[40xrU4USJeyzTqIE.htm](pathfinder-bestiary-items/40xrU4USJeyzTqIE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[42Uo3GU6JFA7w7G8.htm](pathfinder-bestiary-items/42Uo3GU6JFA7w7G8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[44gZk9CBkA5y9VOb.htm](pathfinder-bestiary-items/44gZk9CBkA5y9VOb.htm)|Mass Laughter|Mass Laughter|modificada|
|[44mTKtGhXhfJhnBh.htm](pathfinder-bestiary-items/44mTKtGhXhfJhnBh.htm)|Fast Healing 2 (While Touching Fire)|Curación rápida 2 (mientras tocas el fuego).|modificada|
|[465xP8CbWAyUkrIj.htm](pathfinder-bestiary-items/465xP8CbWAyUkrIj.htm)|Flame of Justice|Llama de la justicia|modificada|
|[47dklzJrGaCDSZ2N.htm](pathfinder-bestiary-items/47dklzJrGaCDSZ2N.htm)|Tremorsense (Imprecise) 30 feet (Creatures Touching its Web)|Sentido del temblor (Impreciso) 30 pies (Criaturas que tocan su telara).|modificada|
|[47Ksb5cr0D7tx1KE.htm](pathfinder-bestiary-items/47Ksb5cr0D7tx1KE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4adEu2Sh2OYsvkLV.htm](pathfinder-bestiary-items/4adEu2Sh2OYsvkLV.htm)|Fist|Puño|modificada|
|[4BNXIgVORiBDrL95.htm](pathfinder-bestiary-items/4BNXIgVORiBDrL95.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[4eIvZ5FrB8u5CTbJ.htm](pathfinder-bestiary-items/4eIvZ5FrB8u5CTbJ.htm)|Tentacle Flurry|Ráfaga de tentáculos|modificada|
|[4ESvAYPL0OBd4s6Q.htm](pathfinder-bestiary-items/4ESvAYPL0OBd4s6Q.htm)|Gestalt|Gestalt|modificada|
|[4eVppzidbAbwJeF2.htm](pathfinder-bestiary-items/4eVppzidbAbwJeF2.htm)|Claw|Garra|modificada|
|[4EXgRMqNIRLXxB1c.htm](pathfinder-bestiary-items/4EXgRMqNIRLXxB1c.htm)|Shortbow|Arco corto|modificada|
|[4fZp6kK5h1WOCAAq.htm](pathfinder-bestiary-items/4fZp6kK5h1WOCAAq.htm)|Compression|Compresión|modificada|
|[4gkC6A8D27Jzv7nH.htm](pathfinder-bestiary-items/4gkC6A8D27Jzv7nH.htm)|Broad Swipe|Vaivén amplio|modificada|
|[4gO7xPscU8GkItt4.htm](pathfinder-bestiary-items/4gO7xPscU8GkItt4.htm)|Spear|Lanza|modificada|
|[4gP2dSNnL1rudfAt.htm](pathfinder-bestiary-items/4gP2dSNnL1rudfAt.htm)|Tongue|Lengua|modificada|
|[4hDLxVe3DC4KrgiS.htm](pathfinder-bestiary-items/4hDLxVe3DC4KrgiS.htm)|Tail|Tail|modificada|
|[4ii4lkO4XldDodwj.htm](pathfinder-bestiary-items/4ii4lkO4XldDodwj.htm)|Frightful Presence|Frightful Presence|modificada|
|[4Iw8N4ANO4rwmrYg.htm](pathfinder-bestiary-items/4Iw8N4ANO4rwmrYg.htm)|Tentacle|Tentáculo|modificada|
|[4JCt8tmxCghbphU5.htm](pathfinder-bestiary-items/4JCt8tmxCghbphU5.htm)|Catch Rock|Atrapar roca|modificada|
|[4jo5EjR59Kycpzcy.htm](pathfinder-bestiary-items/4jo5EjR59Kycpzcy.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[4k5fQuKObwYhz1PU.htm](pathfinder-bestiary-items/4k5fQuKObwYhz1PU.htm)|Jaws|Fauces|modificada|
|[4kBeGDMZLn75niF4.htm](pathfinder-bestiary-items/4kBeGDMZLn75niF4.htm)|Sound Imitation|Imitación de sonidos|modificada|
|[4lmj5hd20I5GvQJz.htm](pathfinder-bestiary-items/4lmj5hd20I5GvQJz.htm)|Spring Upon Prey|Spring Upon Prey|modificada|
|[4Lxyi7ekwyeXWdZe.htm](pathfinder-bestiary-items/4Lxyi7ekwyeXWdZe.htm)|Claw|Garra|modificada|
|[4n19omVeZZAAGXBk.htm](pathfinder-bestiary-items/4n19omVeZZAAGXBk.htm)|Coven|Coven|modificada|
|[4N1vw9fFoiAgxSn1.htm](pathfinder-bestiary-items/4N1vw9fFoiAgxSn1.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[4NBL0GmmD67fgNk5.htm](pathfinder-bestiary-items/4NBL0GmmD67fgNk5.htm)|Claw|Garra|modificada|
|[4NNU4WrjaWJrngyL.htm](pathfinder-bestiary-items/4NNU4WrjaWJrngyL.htm)|Engulf|Envolver|modificada|
|[4NqTHQqAkLmiUovi.htm](pathfinder-bestiary-items/4NqTHQqAkLmiUovi.htm)|Stench|Hedor|modificada|
|[4NvG4sOzNrtZikHp.htm](pathfinder-bestiary-items/4NvG4sOzNrtZikHp.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4P8IOAM51aWrHmLb.htm](pathfinder-bestiary-items/4P8IOAM51aWrHmLb.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[4pbTTxqHWnbnAW7T.htm](pathfinder-bestiary-items/4pbTTxqHWnbnAW7T.htm)|Spider Minions|Esbirros araña|modificada|
|[4pcckxSu3lSKQKx9.htm](pathfinder-bestiary-items/4pcckxSu3lSKQKx9.htm)|+2 Circumstance to All Saves vs. Disease|+2 Circunstancia a todas las salvaciones contra enfermedad|modificada|
|[4pzu3XHcK4Rd1GQX.htm](pathfinder-bestiary-items/4pzu3XHcK4Rd1GQX.htm)|Wing Deflection|Desvío con el ala|modificada|
|[4QNftOG1O0v3IrVA.htm](pathfinder-bestiary-items/4QNftOG1O0v3IrVA.htm)|Trip Up|Zancadilla|modificada|
|[4RfTIZfhWkxNhBsn.htm](pathfinder-bestiary-items/4RfTIZfhWkxNhBsn.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[4rs5uETeGIZjVUCC.htm](pathfinder-bestiary-items/4rs5uETeGIZjVUCC.htm)|Dimensional Dervish|Derviche dimensional|modificada|
|[4s0p8fCnssqfycUf.htm](pathfinder-bestiary-items/4s0p8fCnssqfycUf.htm)|Swallow Whole|Engullir Todo|modificada|
|[4SeHhCM0ulS4TIjL.htm](pathfinder-bestiary-items/4SeHhCM0ulS4TIjL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4tkIBY4fl80WZyr6.htm](pathfinder-bestiary-items/4tkIBY4fl80WZyr6.htm)|Spit Venom|Escupe Veneno|modificada|
|[4tRvd9W1xA0JvgPJ.htm](pathfinder-bestiary-items/4tRvd9W1xA0JvgPJ.htm)|Claw|Garra|modificada|
|[4tsSsz41K5abcM8D.htm](pathfinder-bestiary-items/4tsSsz41K5abcM8D.htm)|Swallow Whole|Engullir Todo|modificada|
|[4TtLSpRhmqV1Dvp1.htm](pathfinder-bestiary-items/4TtLSpRhmqV1Dvp1.htm)|Tail Whip|Coletazo|modificada|
|[4uh4OO7zv1oZkB4W.htm](pathfinder-bestiary-items/4uh4OO7zv1oZkB4W.htm)|Dogslicer|Dogslicer|modificada|
|[4uV5kuuPiKE59I3w.htm](pathfinder-bestiary-items/4uV5kuuPiKE59I3w.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[4uWQE7ermpG8oEr3.htm](pathfinder-bestiary-items/4uWQE7ermpG8oEr3.htm)|Glaive|Glaive|modificada|
|[4uy8ctqHkWgUS4KY.htm](pathfinder-bestiary-items/4uy8ctqHkWgUS4KY.htm)|Breath Weapon|Breath Weapon|modificada|
|[4vfOgL2x3vJgpsBu.htm](pathfinder-bestiary-items/4vfOgL2x3vJgpsBu.htm)|Glide|Glide|modificada|
|[4VHRW4Y0OmyqAHpQ.htm](pathfinder-bestiary-items/4VHRW4Y0OmyqAHpQ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[4vrCCq5VVcSm3rwb.htm](pathfinder-bestiary-items/4vrCCq5VVcSm3rwb.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[4WUyRzJH8pu1zq5J.htm](pathfinder-bestiary-items/4WUyRzJH8pu1zq5J.htm)|Channel Rot|Canalizar putridez|modificada|
|[4Y4K0inFOH3iWaot.htm](pathfinder-bestiary-items/4Y4K0inFOH3iWaot.htm)|Jaws|Fauces|modificada|
|[4Y6ugZExUjpvvoVy.htm](pathfinder-bestiary-items/4Y6ugZExUjpvvoVy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[4ysvHeALdBNPXE0e.htm](pathfinder-bestiary-items/4ysvHeALdBNPXE0e.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[50Meu5uWTOPYtMu6.htm](pathfinder-bestiary-items/50Meu5uWTOPYtMu6.htm)|Regeneration 20 (Deactivated by Acid or Fire)|Regeneración 20 (Desactivado por Ácido o Fuego)|modificada|
|[50tgcNZoPBn2RCy3.htm](pathfinder-bestiary-items/50tgcNZoPBn2RCy3.htm)|Claw|Garra|modificada|
|[53FM1WWIJj3p1kzG.htm](pathfinder-bestiary-items/53FM1WWIJj3p1kzG.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[53LkbbyoyISSppMe.htm](pathfinder-bestiary-items/53LkbbyoyISSppMe.htm)|Purple Worm Venom|Veneno de gusano púrpura|modificada|
|[53TdkHMcaLAUgDXf.htm](pathfinder-bestiary-items/53TdkHMcaLAUgDXf.htm)|Dragon Heat|Calor dracónico|modificada|
|[53xmmigXl7qYER3Y.htm](pathfinder-bestiary-items/53xmmigXl7qYER3Y.htm)|Infuse Weapons|Infundir armas|modificada|
|[54j0gybWlIAueFkZ.htm](pathfinder-bestiary-items/54j0gybWlIAueFkZ.htm)|Shortsword|Espada corta|modificada|
|[54pvmAtUtSvN1SQx.htm](pathfinder-bestiary-items/54pvmAtUtSvN1SQx.htm)|Shield Block|Bloquear con escudo|modificada|
|[55EdzLqRdJ3A5Win.htm](pathfinder-bestiary-items/55EdzLqRdJ3A5Win.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[55Hvc61MUgVoahwb.htm](pathfinder-bestiary-items/55Hvc61MUgVoahwb.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[56yBsaqp9GmItJzk.htm](pathfinder-bestiary-items/56yBsaqp9GmItJzk.htm)|Breath Weapon|Breath Weapon|modificada|
|[57lDKo6dcd3jNhcT.htm](pathfinder-bestiary-items/57lDKo6dcd3jNhcT.htm)|Jaws|Fauces|modificada|
|[57QWHxHOoldzPbcN.htm](pathfinder-bestiary-items/57QWHxHOoldzPbcN.htm)|Claw|Garra|modificada|
|[59wy5xK7MEQmYRxg.htm](pathfinder-bestiary-items/59wy5xK7MEQmYRxg.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[5CiCYm71I6kB4UPC.htm](pathfinder-bestiary-items/5CiCYm71I6kB4UPC.htm)|Masterful Quickened Casting|Conjuros apresurados magistrales Lanzamiento apresurado/a.|modificada|
|[5dshGJm0RD445JZG.htm](pathfinder-bestiary-items/5dshGJm0RD445JZG.htm)|Horns|Cuernos|modificada|
|[5e9xpAPgJWQxMqDB.htm](pathfinder-bestiary-items/5e9xpAPgJWQxMqDB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5ednVEQACX3S93gP.htm](pathfinder-bestiary-items/5ednVEQACX3S93gP.htm)|Constant Spells|Constant Spells|modificada|
|[5Em7mvb9ul24rwbD.htm](pathfinder-bestiary-items/5Em7mvb9ul24rwbD.htm)|Frightful Presence|Frightful Presence|modificada|
|[5EvbvNghD1THDwUR.htm](pathfinder-bestiary-items/5EvbvNghD1THDwUR.htm)|Negative Healing|Curación negativa|modificada|
|[5fkFF7Wnl8Mmf8Zu.htm](pathfinder-bestiary-items/5fkFF7Wnl8Mmf8Zu.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[5HpQse2MIBQbs10B.htm](pathfinder-bestiary-items/5HpQse2MIBQbs10B.htm)|Web|Telara|modificada|
|[5HRKFbIvbD2qXTXD.htm](pathfinder-bestiary-items/5HRKFbIvbD2qXTXD.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[5I2ELiZx1Aa9j37J.htm](pathfinder-bestiary-items/5I2ELiZx1Aa9j37J.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[5IHmSdZpGxrw76lc.htm](pathfinder-bestiary-items/5IHmSdZpGxrw76lc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5k5qe7LZeapjusIM.htm](pathfinder-bestiary-items/5k5qe7LZeapjusIM.htm)|Breath Weapon|Breath Weapon|modificada|
|[5kEltylMi3m28RpQ.htm](pathfinder-bestiary-items/5kEltylMi3m28RpQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5kWr1s58IkPKdMcL.htm](pathfinder-bestiary-items/5kWr1s58IkPKdMcL.htm)|Mirage|Mirage|modificada|
|[5lNem5v3czqg4yta.htm](pathfinder-bestiary-items/5lNem5v3czqg4yta.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[5McIXOemuKXFd7Ep.htm](pathfinder-bestiary-items/5McIXOemuKXFd7Ep.htm)|Vicious Wounds|Heridas severas|modificada|
|[5NPT2o7xLwm4QVG0.htm](pathfinder-bestiary-items/5NPT2o7xLwm4QVG0.htm)|Grab|Agarrado|modificada|
|[5nrxrLm9SgLXHRhi.htm](pathfinder-bestiary-items/5nrxrLm9SgLXHRhi.htm)|Staff|Báculo|modificada|
|[5O3D1KLFuAtqX80b.htm](pathfinder-bestiary-items/5O3D1KLFuAtqX80b.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[5o4Qh26axxAvJeUp.htm](pathfinder-bestiary-items/5o4Qh26axxAvJeUp.htm)|Grab|Agarrado|modificada|
|[5ogrmYbXtCuT4HPN.htm](pathfinder-bestiary-items/5ogrmYbXtCuT4HPN.htm)|Corrupt Water|Corromper agua|modificada|
|[5ozQMc30WbtUYecf.htm](pathfinder-bestiary-items/5ozQMc30WbtUYecf.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[5P26T61QIn85Jbfk.htm](pathfinder-bestiary-items/5P26T61QIn85Jbfk.htm)|Constrict|Restringir|modificada|
|[5PDURAzo537r6kje.htm](pathfinder-bestiary-items/5PDURAzo537r6kje.htm)|Constrict|Restringir|modificada|
|[5pgk77sKtK8gCgSb.htm](pathfinder-bestiary-items/5pgk77sKtK8gCgSb.htm)|Jaws|Fauces|modificada|
|[5prFU0v8oG5s1kHj.htm](pathfinder-bestiary-items/5prFU0v8oG5s1kHj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5pZXZ7pLE1RdGKX5.htm](pathfinder-bestiary-items/5pZXZ7pLE1RdGKX5.htm)|Improved Grab|Agarrado mejorado|modificada|
|[5Qq44Y2MrPleByoB.htm](pathfinder-bestiary-items/5Qq44Y2MrPleByoB.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[5Rd1LNWiczurzj0C.htm](pathfinder-bestiary-items/5Rd1LNWiczurzj0C.htm)|Breath Weapon|Breath Weapon|modificada|
|[5rP5cz0JHgxkYHIF.htm](pathfinder-bestiary-items/5rP5cz0JHgxkYHIF.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[5rSjCpmIctYFNqcn.htm](pathfinder-bestiary-items/5rSjCpmIctYFNqcn.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[5THRshFo5sJLDIle.htm](pathfinder-bestiary-items/5THRshFo5sJLDIle.htm)|Improved Grab|Agarrado mejorado|modificada|
|[5TLoRDxeWBDh6a6e.htm](pathfinder-bestiary-items/5TLoRDxeWBDh6a6e.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[5TVlHOsmwK2Om8pu.htm](pathfinder-bestiary-items/5TVlHOsmwK2Om8pu.htm)|Hunt Prey|Perseguir presa|modificada|
|[5U4QH2sM8ipWEAcR.htm](pathfinder-bestiary-items/5U4QH2sM8ipWEAcR.htm)|Fast Healing 1|Curación rápida 1|modificada|
|[5ulvx0zqprJryzvq.htm](pathfinder-bestiary-items/5ulvx0zqprJryzvq.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[5UO7IHt7Yn75Z9rL.htm](pathfinder-bestiary-items/5UO7IHt7Yn75Z9rL.htm)|Grab|Agarrado|modificada|
|[5Uq09OsuqP9JPmlt.htm](pathfinder-bestiary-items/5Uq09OsuqP9JPmlt.htm)|Wavesense 30 feet|Wavesense 30 pies|modificada|
|[5wsaNGdGzezNTnea.htm](pathfinder-bestiary-items/5wsaNGdGzezNTnea.htm)|Twisting Tail|Enredar con la cola|modificada|
|[5xEEdMhQDZQFuUpv.htm](pathfinder-bestiary-items/5xEEdMhQDZQFuUpv.htm)|Archon's Door|Puerta del arconte|modificada|
|[5yDMt3Vna9iVApvJ.htm](pathfinder-bestiary-items/5yDMt3Vna9iVApvJ.htm)|Blood Scent|Blood Scent|modificada|
|[5Yp3kZDHE8gZhhLe.htm](pathfinder-bestiary-items/5Yp3kZDHE8gZhhLe.htm)|Fist|Puño|modificada|
|[62IVWtvdiJi9PSZ1.htm](pathfinder-bestiary-items/62IVWtvdiJi9PSZ1.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[62o3Ky65LWW5uKNE.htm](pathfinder-bestiary-items/62o3Ky65LWW5uKNE.htm)|Trample|Trample|modificada|
|[62TQ4X0N6cfOxwQQ.htm](pathfinder-bestiary-items/62TQ4X0N6cfOxwQQ.htm)|Acid Flask (Lesser)|Frasco de ácido (menor)|modificada|
|[63a8hHsiAUlNCTdu.htm](pathfinder-bestiary-items/63a8hHsiAUlNCTdu.htm)|Surprise Attacker|Atacante por sorpresa|modificada|
|[63iXwIDFMddnz4kz.htm](pathfinder-bestiary-items/63iXwIDFMddnz4kz.htm)|Eagle Dive|Picado de águila|modificada|
|[64dsDb6VCwVbCOBD.htm](pathfinder-bestiary-items/64dsDb6VCwVbCOBD.htm)|Jaws|Fauces|modificada|
|[64oqzNrwPutYdBRh.htm](pathfinder-bestiary-items/64oqzNrwPutYdBRh.htm)|Regeneration 20 (Deactivated by Cold or Evil)|Regeneración 20 (Desactivado por frío o maligno)|modificada|
|[65E6IqqYN0IW2HsH.htm](pathfinder-bestiary-items/65E6IqqYN0IW2HsH.htm)|Powerful Charge|Carga Poderosa|modificada|
|[67MCs85SzER633da.htm](pathfinder-bestiary-items/67MCs85SzER633da.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[69DgtYVywDs0D5pB.htm](pathfinder-bestiary-items/69DgtYVywDs0D5pB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6aClhFTvO5NOf1iA.htm](pathfinder-bestiary-items/6aClhFTvO5NOf1iA.htm)|Aura of Misfortune|Aura de infortunio|modificada|
|[6aID4H750Zqy0EOH.htm](pathfinder-bestiary-items/6aID4H750Zqy0EOH.htm)|Cold Iron Silver Longsword|Cold Iron Silver Longsword|modificada|
|[6BFQljbOQZkiMjjS.htm](pathfinder-bestiary-items/6BFQljbOQZkiMjjS.htm)|Vulnerable to Sunlight|Vulnerabilidad a la luz del sol|modificada|
|[6bt34Jgx0jymQKsU.htm](pathfinder-bestiary-items/6bt34Jgx0jymQKsU.htm)|Bloodletting|Sangría|modificada|
|[6CWD9zTeiLEB09vN.htm](pathfinder-bestiary-items/6CWD9zTeiLEB09vN.htm)|Fast Healing 2 (While Underground)|Curación rápida 2 (mientras están bajo tierra)|modificada|
|[6ESE4WVjACGF6VGi.htm](pathfinder-bestiary-items/6ESE4WVjACGF6VGi.htm)|Coven Spells|Coven Spells|modificada|
|[6evj7LX0P0Xyv3W8.htm](pathfinder-bestiary-items/6evj7LX0P0Xyv3W8.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[6F6RBqJmz0JUvLc3.htm](pathfinder-bestiary-items/6F6RBqJmz0JUvLc3.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6f6zqdb6XsTeZuRW.htm](pathfinder-bestiary-items/6f6zqdb6XsTeZuRW.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[6fRxttEYXtfGW1OF.htm](pathfinder-bestiary-items/6fRxttEYXtfGW1OF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6gO1vT5DmiULZ2Bv.htm](pathfinder-bestiary-items/6gO1vT5DmiULZ2Bv.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[6gzE3WOZs1801eyS.htm](pathfinder-bestiary-items/6gzE3WOZs1801eyS.htm)|Acid Spit|Acid Spit|modificada|
|[6h9jexgK3OdaDWsq.htm](pathfinder-bestiary-items/6h9jexgK3OdaDWsq.htm)|Peace Vulnerability|Vulnerabilidad a la paz|modificada|
|[6hIXdmiCBTVmIryb.htm](pathfinder-bestiary-items/6hIXdmiCBTVmIryb.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6HJGU4tyzZ6JeEbD.htm](pathfinder-bestiary-items/6HJGU4tyzZ6JeEbD.htm)|Constant Spells|Constant Spells|modificada|
|[6HOLWC4q1wPrnZql.htm](pathfinder-bestiary-items/6HOLWC4q1wPrnZql.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[6HYP2meWyAXuDtrA.htm](pathfinder-bestiary-items/6HYP2meWyAXuDtrA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6i8TGhbPoCodXsn0.htm](pathfinder-bestiary-items/6i8TGhbPoCodXsn0.htm)|Constant Spells|Constant Spells|modificada|
|[6ibGu5FoaHhUfj6a.htm](pathfinder-bestiary-items/6ibGu5FoaHhUfj6a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6ii2IeWpDJ46u3xM.htm](pathfinder-bestiary-items/6ii2IeWpDJ46u3xM.htm)|Rapier|Estoque|modificada|
|[6inoR3FiOAzUbSZ8.htm](pathfinder-bestiary-items/6inoR3FiOAzUbSZ8.htm)|Savage|Salvajismo|modificada|
|[6iyj26yHFPai9vr3.htm](pathfinder-bestiary-items/6iyj26yHFPai9vr3.htm)|Wind Strike|Golpe de vendaval|modificada|
|[6JIPvxKV7RTv9wq3.htm](pathfinder-bestiary-items/6JIPvxKV7RTv9wq3.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[6MX0hGFGosdch5VS.htm](pathfinder-bestiary-items/6MX0hGFGosdch5VS.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[6N6oDDqKb0CaILEx.htm](pathfinder-bestiary-items/6N6oDDqKb0CaILEx.htm)|Aklys|Aklys|modificada|
|[6nUf7iLY8JoKVMkD.htm](pathfinder-bestiary-items/6nUf7iLY8JoKVMkD.htm)|Rock|Roca|modificada|
|[6NxJOtjBC8UhEkqr.htm](pathfinder-bestiary-items/6NxJOtjBC8UhEkqr.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6ORqPbdQEDKtPs55.htm](pathfinder-bestiary-items/6ORqPbdQEDKtPs55.htm)|Grab|Agarrado|modificada|
|[6OspJylC319MBk17.htm](pathfinder-bestiary-items/6OspJylC319MBk17.htm)|Change Shape|Change Shape|modificada|
|[6pFoOIHjIKB8RSDO.htm](pathfinder-bestiary-items/6pFoOIHjIKB8RSDO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6pKzCiashjeFYDdw.htm](pathfinder-bestiary-items/6pKzCiashjeFYDdw.htm)|Constant Spells|Constant Spells|modificada|
|[6pmMLkbrsF7wRVrQ.htm](pathfinder-bestiary-items/6pmMLkbrsF7wRVrQ.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[6PogEuLJfheJMDfl.htm](pathfinder-bestiary-items/6PogEuLJfheJMDfl.htm)|Fist|Puño|modificada|
|[6ptXWWAwrafuOZ7Z.htm](pathfinder-bestiary-items/6ptXWWAwrafuOZ7Z.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[6QbeDmIcvY8S3ldj.htm](pathfinder-bestiary-items/6QbeDmIcvY8S3ldj.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[6ROvbbPgi1hsjjkb.htm](pathfinder-bestiary-items/6ROvbbPgi1hsjjkb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[6SbDtVTo4XWUEGO7.htm](pathfinder-bestiary-items/6SbDtVTo4XWUEGO7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6SHvRrPDTa74t0cD.htm](pathfinder-bestiary-items/6SHvRrPDTa74t0cD.htm)|Rock|Roca|modificada|
|[6TdllGeax8h6457M.htm](pathfinder-bestiary-items/6TdllGeax8h6457M.htm)|Naturally Invisible|Naturalmente Invisible|modificada|
|[6THNxAaI4CZSj0oW.htm](pathfinder-bestiary-items/6THNxAaI4CZSj0oW.htm)|Pit Fiend Venom|Veneno de diablo de la sima|modificada|
|[6tSYdMbuSY1NSMOt.htm](pathfinder-bestiary-items/6tSYdMbuSY1NSMOt.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[6Upo1ddHWd1c49bj.htm](pathfinder-bestiary-items/6Upo1ddHWd1c49bj.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[6UZkK4IZ3193CYxr.htm](pathfinder-bestiary-items/6UZkK4IZ3193CYxr.htm)|Frightful Presence|Frightful Presence|modificada|
|[6V0TnokqVZcRCz7V.htm](pathfinder-bestiary-items/6V0TnokqVZcRCz7V.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[6vLerwjydBOU4Iv7.htm](pathfinder-bestiary-items/6vLerwjydBOU4Iv7.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[6VrL0WH1rLsYXFpD.htm](pathfinder-bestiary-items/6VrL0WH1rLsYXFpD.htm)|Claw|Garra|modificada|
|[6WolzdmuMWdkmSpl.htm](pathfinder-bestiary-items/6WolzdmuMWdkmSpl.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[6wXxUMuYVHcPeCm1.htm](pathfinder-bestiary-items/6wXxUMuYVHcPeCm1.htm)|Hatchet|Hacha|modificada|
|[6xP4wJsTXGXVjqRg.htm](pathfinder-bestiary-items/6xP4wJsTXGXVjqRg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6xrvE4IdhDnQNzDW.htm](pathfinder-bestiary-items/6xrvE4IdhDnQNzDW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6Y0H1qKcOuLLE2PK.htm](pathfinder-bestiary-items/6Y0H1qKcOuLLE2PK.htm)|Maddening Cacophony|Maddening Cacophony|modificada|
|[6yBN6jKH0nlzgvOT.htm](pathfinder-bestiary-items/6yBN6jKH0nlzgvOT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6yZ26OupmzzPO1vZ.htm](pathfinder-bestiary-items/6yZ26OupmzzPO1vZ.htm)|Miasma|Miasma|modificada|
|[6Z8Si7EyPGbAS0VJ.htm](pathfinder-bestiary-items/6Z8Si7EyPGbAS0VJ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[6zbiPHhWaAV5pJd6.htm](pathfinder-bestiary-items/6zbiPHhWaAV5pJd6.htm)|Wriggling Beard|Barba serpenteante|modificada|
|[748MxBiZMmMCjxzu.htm](pathfinder-bestiary-items/748MxBiZMmMCjxzu.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[74wDKknM5LCOcK73.htm](pathfinder-bestiary-items/74wDKknM5LCOcK73.htm)|Hears Heartbeats (Imprecise) 60 feet|Escucha latidos (Impreciso) 60 pies.|modificada|
|[74Xu2C6KzxtSFcNe.htm](pathfinder-bestiary-items/74Xu2C6KzxtSFcNe.htm)|Two Heads|Dos cabezas|modificada|
|[765hQtSimhXMwS7m.htm](pathfinder-bestiary-items/765hQtSimhXMwS7m.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[76JTH3gizL3NZAYl.htm](pathfinder-bestiary-items/76JTH3gizL3NZAYl.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[78Qh6xRwrVxslcAx.htm](pathfinder-bestiary-items/78Qh6xRwrVxslcAx.htm)|Shield Block|Bloquear con escudo|modificada|
|[7aI3LWA9cnoKonHZ.htm](pathfinder-bestiary-items/7aI3LWA9cnoKonHZ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[7aIGZtbifYYVXpiV.htm](pathfinder-bestiary-items/7aIGZtbifYYVXpiV.htm)|Grab|Agarrado|modificada|
|[7bniftpjEDrMSUN2.htm](pathfinder-bestiary-items/7bniftpjEDrMSUN2.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[7brI3OuzocwPLZcW.htm](pathfinder-bestiary-items/7brI3OuzocwPLZcW.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[7CAtWwnu0VOFzaK9.htm](pathfinder-bestiary-items/7CAtWwnu0VOFzaK9.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[7CcUIlaj6l3irOmm.htm](pathfinder-bestiary-items/7CcUIlaj6l3irOmm.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[7CjSawx6weIPrIFL.htm](pathfinder-bestiary-items/7CjSawx6weIPrIFL.htm)|Flashing Runes|Runas brillantes|modificada|
|[7CQGpLGBAg2Jbtjv.htm](pathfinder-bestiary-items/7CQGpLGBAg2Jbtjv.htm)|Three Headed|Tres cabezas|modificada|
|[7CwkOMYGSOhef0ns.htm](pathfinder-bestiary-items/7CwkOMYGSOhef0ns.htm)|Breath Weapon|Breath Weapon|modificada|
|[7DB79rjng9ShAwNU.htm](pathfinder-bestiary-items/7DB79rjng9ShAwNU.htm)|Infrasonic Moan|Gemido infrasónico|modificada|
|[7DhYY8OTjN08dR6B.htm](pathfinder-bestiary-items/7DhYY8OTjN08dR6B.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7ECMLvSVWJWwwgKi.htm](pathfinder-bestiary-items/7ECMLvSVWJWwwgKi.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[7EDZA6PRAkgmOYfB.htm](pathfinder-bestiary-items/7EDZA6PRAkgmOYfB.htm)|All-Around Vision|All-Around Vision|modificada|
|[7h8dpLvJlgmuZE2b.htm](pathfinder-bestiary-items/7h8dpLvJlgmuZE2b.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7HbtTKCzPMHlRdFT.htm](pathfinder-bestiary-items/7HbtTKCzPMHlRdFT.htm)|Tail|Tail|modificada|
|[7Hcv7QQPQAQcct0I.htm](pathfinder-bestiary-items/7Hcv7QQPQAQcct0I.htm)|Breath Weapon|Breath Weapon|modificada|
|[7HCvV8Nrw6EphM2a.htm](pathfinder-bestiary-items/7HCvV8Nrw6EphM2a.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[7hIKn4hwekvsOCAW.htm](pathfinder-bestiary-items/7hIKn4hwekvsOCAW.htm)|Aklys|Aklys|modificada|
|[7hUX8z2syN9gmg7D.htm](pathfinder-bestiary-items/7hUX8z2syN9gmg7D.htm)|Tail|Tail|modificada|
|[7HZont6X72rI49Ax.htm](pathfinder-bestiary-items/7HZont6X72rI49Ax.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[7I0I09vYwkMcNZoL.htm](pathfinder-bestiary-items/7I0I09vYwkMcNZoL.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[7ioQp451JDOrH2ZM.htm](pathfinder-bestiary-items/7ioQp451JDOrH2ZM.htm)|Throw Rock|Arrojar roca|modificada|
|[7ivzzqnDwOSXosG5.htm](pathfinder-bestiary-items/7ivzzqnDwOSXosG5.htm)|Delayed Suggestion|Sugestión programada.|modificada|
|[7IWkM6MOEOAu4DTP.htm](pathfinder-bestiary-items/7IWkM6MOEOAu4DTP.htm)|Tree Meld|Tree Meld|modificada|
|[7Iww6mLyrRcTXqWv.htm](pathfinder-bestiary-items/7Iww6mLyrRcTXqWv.htm)|Halberd|Alabarda|modificada|
|[7J6yDmYeMAWvM46S.htm](pathfinder-bestiary-items/7J6yDmYeMAWvM46S.htm)|Sound Imitation|Imitación de sonidos|modificada|
|[7JcMtxcKSyHdqEQX.htm](pathfinder-bestiary-items/7JcMtxcKSyHdqEQX.htm)|Spitting Rage|Rabia escupidora|modificada|
|[7JJXsi69PzGUt6UR.htm](pathfinder-bestiary-items/7JJXsi69PzGUt6UR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7KN50dFHLzPdjJNH.htm](pathfinder-bestiary-items/7KN50dFHLzPdjJNH.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[7KT3DPupZMzKyxiY.htm](pathfinder-bestiary-items/7KT3DPupZMzKyxiY.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[7KT3KfzD3HVUhRjt.htm](pathfinder-bestiary-items/7KT3KfzD3HVUhRjt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7LQPJRBvruncsAz1.htm](pathfinder-bestiary-items/7LQPJRBvruncsAz1.htm)|Horns|Cuernos|modificada|
|[7lto9FCIXzooWeAW.htm](pathfinder-bestiary-items/7lto9FCIXzooWeAW.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[7MCn4tFozydZTxeL.htm](pathfinder-bestiary-items/7MCn4tFozydZTxeL.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[7nAuNsDTN5NBtC7a.htm](pathfinder-bestiary-items/7nAuNsDTN5NBtC7a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7O6Ud0aHDjF3hlFD.htm](pathfinder-bestiary-items/7O6Ud0aHDjF3hlFD.htm)|Infest Environs|Infest Environs|modificada|
|[7ODNVUAaNraSivsT.htm](pathfinder-bestiary-items/7ODNVUAaNraSivsT.htm)|Shortbow|Arco corto|modificada|
|[7ogTgoQe4WAvtdJ7.htm](pathfinder-bestiary-items/7ogTgoQe4WAvtdJ7.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[7P4VEdy4KccmQ5Gp.htm](pathfinder-bestiary-items/7P4VEdy4KccmQ5Gp.htm)|Echolocation (Precise) 20 feet|Ecolocalización (Precisión) 20 pies.|modificada|
|[7pkevpR7Am9eNCD5.htm](pathfinder-bestiary-items/7pkevpR7Am9eNCD5.htm)|Swallow Whole|Engullir Todo|modificada|
|[7PQkG1Vfc7aynb2X.htm](pathfinder-bestiary-items/7PQkG1Vfc7aynb2X.htm)|Frightening Display|Exhibición pavorosa|modificada|
|[7pRxaQFmTGlbnKEr.htm](pathfinder-bestiary-items/7pRxaQFmTGlbnKEr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7PTn0lPspj6Mo8E7.htm](pathfinder-bestiary-items/7PTn0lPspj6Mo8E7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7pYPgvPhNqIQ9RwA.htm](pathfinder-bestiary-items/7pYPgvPhNqIQ9RwA.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[7q0tz0RZaa6kAE5S.htm](pathfinder-bestiary-items/7q0tz0RZaa6kAE5S.htm)|Constant Spells|Constant Spells|modificada|
|[7Q1l6xgw6nJEEbEr.htm](pathfinder-bestiary-items/7Q1l6xgw6nJEEbEr.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[7qULZEHZX6JmtrVP.htm](pathfinder-bestiary-items/7qULZEHZX6JmtrVP.htm)|Breath Weapon|Breath Weapon|modificada|
|[7rqZnXnqggRlsoMe.htm](pathfinder-bestiary-items/7rqZnXnqggRlsoMe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7RTjFlVzsxhjtwX4.htm](pathfinder-bestiary-items/7RTjFlVzsxhjtwX4.htm)|Consume Flesh|Consumir carne|modificada|
|[7SNUgV6SLqC5TzXJ.htm](pathfinder-bestiary-items/7SNUgV6SLqC5TzXJ.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[7swXlOjSl2oHLuDv.htm](pathfinder-bestiary-items/7swXlOjSl2oHLuDv.htm)|Horsechopper|Horsechopper|modificada|
|[7Tl0EsClc5Q8xh8G.htm](pathfinder-bestiary-items/7Tl0EsClc5Q8xh8G.htm)|Jaws|Fauces|modificada|
|[7tWhbtD4uoAp2v5H.htm](pathfinder-bestiary-items/7tWhbtD4uoAp2v5H.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7WxioYo3ZgMSuL5O.htm](pathfinder-bestiary-items/7WxioYo3ZgMSuL5O.htm)|Leg|Pierna|modificada|
|[7XdX39vruCh59MIe.htm](pathfinder-bestiary-items/7XdX39vruCh59MIe.htm)|Divine Revulsion|Revulsión divina|modificada|
|[7XIYM7XKRyeIRUn6.htm](pathfinder-bestiary-items/7XIYM7XKRyeIRUn6.htm)|Snow Vision|Snow Vision|modificada|
|[7xKIhg7lon987ZWq.htm](pathfinder-bestiary-items/7xKIhg7lon987ZWq.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[7XN2uoXvAP0XCvYn.htm](pathfinder-bestiary-items/7XN2uoXvAP0XCvYn.htm)|Holy Blade|Filo sagrado|modificada|
|[7Z1lYeu3xHpx2SPZ.htm](pathfinder-bestiary-items/7Z1lYeu3xHpx2SPZ.htm)|Claw|Garra|modificada|
|[7ZnWKFvAT28mjh65.htm](pathfinder-bestiary-items/7ZnWKFvAT28mjh65.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[7ZZXXt4mJ5JwYsKh.htm](pathfinder-bestiary-items/7ZZXXt4mJ5JwYsKh.htm)|Claw|Garra|modificada|
|[80b2NmnlvaPumpDV.htm](pathfinder-bestiary-items/80b2NmnlvaPumpDV.htm)|Earthbound|Ligado a la tierra|modificada|
|[80CuEcGtDQ6fUIyY.htm](pathfinder-bestiary-items/80CuEcGtDQ6fUIyY.htm)|Drowning Drone|Zumbido ensordecedor|modificada|
|[80jq5AttlXMgdD6Q.htm](pathfinder-bestiary-items/80jq5AttlXMgdD6Q.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[80wSAXH5wAj5t4fT.htm](pathfinder-bestiary-items/80wSAXH5wAj5t4fT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[81HaFjfnZXozTasw.htm](pathfinder-bestiary-items/81HaFjfnZXozTasw.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[81RWhoONaToFh2uz.htm](pathfinder-bestiary-items/81RWhoONaToFh2uz.htm)|Claw|Garra|modificada|
|[82cDSepgqLoFDjw8.htm](pathfinder-bestiary-items/82cDSepgqLoFDjw8.htm)|Frightful Presence|Frightful Presence|modificada|
|[82HFjbpYVotosFsA.htm](pathfinder-bestiary-items/82HFjbpYVotosFsA.htm)|Regeneration 50 (Deactivated by Sonic)|Regeneración 50 (Desactivado por Sonic)|modificada|
|[82KaMhK0BRaEZMbn.htm](pathfinder-bestiary-items/82KaMhK0BRaEZMbn.htm)|Retributive Strike|Golpe retributivo|modificada|
|[83qHKEXcHOEaa6iS.htm](pathfinder-bestiary-items/83qHKEXcHOEaa6iS.htm)|Absorb|Absorb|modificada|
|[83QmDBGigZxaHbbd.htm](pathfinder-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[860MOhDdvHc8KanP.htm](pathfinder-bestiary-items/860MOhDdvHc8KanP.htm)|Hand|Mano|modificada|
|[86ZM2qT4ANthqpil.htm](pathfinder-bestiary-items/86ZM2qT4ANthqpil.htm)|Constant Spells|Constant Spells|modificada|
|[871udt0tIPBiUS63.htm](pathfinder-bestiary-items/871udt0tIPBiUS63.htm)|Defoliation|Defoliación|modificada|
|[88O2MErrb1dOjHJh.htm](pathfinder-bestiary-items/88O2MErrb1dOjHJh.htm)|Curse of Frost|Maldición de Gélida|modificada|
|[8akVcjr4Y8JZR5Ht.htm](pathfinder-bestiary-items/8akVcjr4Y8JZR5Ht.htm)|Club|Club|modificada|
|[8av6UdLVMPok6hmO.htm](pathfinder-bestiary-items/8av6UdLVMPok6hmO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[8Aw8YNHiQ1odM8VY.htm](pathfinder-bestiary-items/8Aw8YNHiQ1odM8VY.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[8aYfDhAzJOdFBLKs.htm](pathfinder-bestiary-items/8aYfDhAzJOdFBLKs.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[8B4xwRLas4rp2NnR.htm](pathfinder-bestiary-items/8B4xwRLas4rp2NnR.htm)|Falchion|Falchion|modificada|
|[8bDtJfwyHARgU5uM.htm](pathfinder-bestiary-items/8bDtJfwyHARgU5uM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8cb0jB7NP8JMjdkx.htm](pathfinder-bestiary-items/8cb0jB7NP8JMjdkx.htm)|Echolocation (Precise) 40 feet|Ecolocalización (precisión) 40 pies.|modificada|
|[8cJQY3xEuodUaxz6.htm](pathfinder-bestiary-items/8cJQY3xEuodUaxz6.htm)|Harmonizing Aura|Aura armonizante|modificada|
|[8CNN6le6tEVrWmR5.htm](pathfinder-bestiary-items/8CNN6le6tEVrWmR5.htm)|Scythe|Guadaña|modificada|
|[8cpYftgTlNcCeNyN.htm](pathfinder-bestiary-items/8cpYftgTlNcCeNyN.htm)|Tail|Tail|modificada|
|[8CYcAwzfjTavahvb.htm](pathfinder-bestiary-items/8CYcAwzfjTavahvb.htm)|Constrict|Restringir|modificada|
|[8DJ83sSlRno4pkws.htm](pathfinder-bestiary-items/8DJ83sSlRno4pkws.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[8EmRJwYnesD02GOh.htm](pathfinder-bestiary-items/8EmRJwYnesD02GOh.htm)|Scimitar|Cimitarra|modificada|
|[8Eq3jy4LyDtP6mLP.htm](pathfinder-bestiary-items/8Eq3jy4LyDtP6mLP.htm)|Starlight Ray|Starlight Ray|modificada|
|[8EY2qfk7YZdvckfJ.htm](pathfinder-bestiary-items/8EY2qfk7YZdvckfJ.htm)|Rock Tunneler|Excavar roca|modificada|
|[8EYAIEzeZ8GJ1Diq.htm](pathfinder-bestiary-items/8EYAIEzeZ8GJ1Diq.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[8f0fnlt5StWq0i9j.htm](pathfinder-bestiary-items/8f0fnlt5StWq0i9j.htm)|Catch Rock|Atrapar roca|modificada|
|[8fKFzS7zqR9XbC5k.htm](pathfinder-bestiary-items/8fKFzS7zqR9XbC5k.htm)|Curse of the Werebear|Curse of the Werebear|modificada|
|[8H68KpZMl3WGb1NP.htm](pathfinder-bestiary-items/8H68KpZMl3WGb1NP.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[8H7QxUhMXhZWzFdG.htm](pathfinder-bestiary-items/8H7QxUhMXhZWzFdG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8Hc25NUrYBwSHQuO.htm](pathfinder-bestiary-items/8Hc25NUrYBwSHQuO.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[8jF0NZz94zAMhCHA.htm](pathfinder-bestiary-items/8jF0NZz94zAMhCHA.htm)|Spores|Esporas|modificada|
|[8JKL1RJJmQI5QjnF.htm](pathfinder-bestiary-items/8JKL1RJJmQI5QjnF.htm)|Tentacle|Tentáculo|modificada|
|[8JqF84SJfKy5TmkS.htm](pathfinder-bestiary-items/8JqF84SJfKy5TmkS.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[8JslG7YLjptMOVC0.htm](pathfinder-bestiary-items/8JslG7YLjptMOVC0.htm)|Sudden Betrayal|Traición inesperada|modificada|
|[8jwjSmQRQW8Nk5NY.htm](pathfinder-bestiary-items/8jwjSmQRQW8Nk5NY.htm)|Fist|Puño|modificada|
|[8KbESiYnn2ngs6MZ.htm](pathfinder-bestiary-items/8KbESiYnn2ngs6MZ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[8ks0Roogu62RATk0.htm](pathfinder-bestiary-items/8ks0Roogu62RATk0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8LvVStEHJB30KVCE.htm](pathfinder-bestiary-items/8LvVStEHJB30KVCE.htm)|Terrifying Croak|Aterrador Croak|modificada|
|[8LYUZymM9ZN8WDw9.htm](pathfinder-bestiary-items/8LYUZymM9ZN8WDw9.htm)|Claw|Garra|modificada|
|[8MiIAzmA9gWbpF5s.htm](pathfinder-bestiary-items/8MiIAzmA9gWbpF5s.htm)|Breath Weapon|Breath Weapon|modificada|
|[8MSWR1BxkhlhSm54.htm](pathfinder-bestiary-items/8MSWR1BxkhlhSm54.htm)|Catch Rock|Atrapar roca|modificada|
|[8oiunFF5LcYpynHM.htm](pathfinder-bestiary-items/8oiunFF5LcYpynHM.htm)|Grab|Agarrado|modificada|
|[8PkUYYXKF3YdJiBM.htm](pathfinder-bestiary-items/8PkUYYXKF3YdJiBM.htm)|Draconic Bite|Mordisco dracónico|modificada|
|[8R3jmOcyIfgNNdKr.htm](pathfinder-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|Curación negativa|modificada|
|[8RIFanFu6JDiIRH1.htm](pathfinder-bestiary-items/8RIFanFu6JDiIRH1.htm)|Thrash|Sacudirse|modificada|
|[8SogXDgTbT7ghBVs.htm](pathfinder-bestiary-items/8SogXDgTbT7ghBVs.htm)|Jaws|Fauces|modificada|
|[8u1Hcx3t6JzOkXtF.htm](pathfinder-bestiary-items/8u1Hcx3t6JzOkXtF.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[8uoOHgzV0mRLhzhE.htm](pathfinder-bestiary-items/8uoOHgzV0mRLhzhE.htm)|Tree Dependent|Tree Dependent|modificada|
|[8VCoSCtEbUqhtmnI.htm](pathfinder-bestiary-items/8VCoSCtEbUqhtmnI.htm)|Swallow Whole|Engullir Todo|modificada|
|[8W4is1CtFAUBw0Zd.htm](pathfinder-bestiary-items/8W4is1CtFAUBw0Zd.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[8W67Bxaonilv5KO0.htm](pathfinder-bestiary-items/8W67Bxaonilv5KO0.htm)|Exhale Miasma|Exhalar miasma|modificada|
|[8wBdJ7S9sRjGCmmT.htm](pathfinder-bestiary-items/8wBdJ7S9sRjGCmmT.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[8WeL4lERSooxfBXE.htm](pathfinder-bestiary-items/8WeL4lERSooxfBXE.htm)|Breath Weapon|Breath Weapon|modificada|
|[8WFwsPMOCNHrJBF8.htm](pathfinder-bestiary-items/8WFwsPMOCNHrJBF8.htm)|Blood Frenzy|Frenesí de Sangre|modificada|
|[8WMc08Sjn5CVzmDo.htm](pathfinder-bestiary-items/8WMc08Sjn5CVzmDo.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[8wxpM0AIu2KtOOy1.htm](pathfinder-bestiary-items/8wxpM0AIu2KtOOy1.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[8wYxLklNMjT1ZPCT.htm](pathfinder-bestiary-items/8wYxLklNMjT1ZPCT.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[8x287oYylzurAA7X.htm](pathfinder-bestiary-items/8x287oYylzurAA7X.htm)|Jaws|Fauces|modificada|
|[8xYRzzHeY6uZztPe.htm](pathfinder-bestiary-items/8xYRzzHeY6uZztPe.htm)|Unstoppable Charge|Carga Imparable|modificada|
|[8yN0Tb58TCHXqGYI.htm](pathfinder-bestiary-items/8yN0Tb58TCHXqGYI.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[8yXu3fI5NfUS8lDH.htm](pathfinder-bestiary-items/8yXu3fI5NfUS8lDH.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[8ZY3GiKagZnMX8T1.htm](pathfinder-bestiary-items/8ZY3GiKagZnMX8T1.htm)|High Winds|Fuertes vientos|modificada|
|[90eQFUneXcsHeKSK.htm](pathfinder-bestiary-items/90eQFUneXcsHeKSK.htm)|Staff|Báculo|modificada|
|[91Bz1Au1eeA0F5Kc.htm](pathfinder-bestiary-items/91Bz1Au1eeA0F5Kc.htm)|Breach|Emerger|modificada|
|[91iOrkVbzeP7cK2M.htm](pathfinder-bestiary-items/91iOrkVbzeP7cK2M.htm)|Adhesive|Adhesivo|modificada|
|[94oa0RrcsKbas3ys.htm](pathfinder-bestiary-items/94oa0RrcsKbas3ys.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[95k2q11INtOhFqCd.htm](pathfinder-bestiary-items/95k2q11INtOhFqCd.htm)|Double Attack|Double Attack|modificada|
|[95kvqPPSJNio8vAK.htm](pathfinder-bestiary-items/95kvqPPSJNio8vAK.htm)|Berserk|Bersérker|modificada|
|[96XPNyAYt1NHy3H5.htm](pathfinder-bestiary-items/96XPNyAYt1NHy3H5.htm)|Blood Leech|Sanguijuela|modificada|
|[9750NAX3RoGRTvAW.htm](pathfinder-bestiary-items/9750NAX3RoGRTvAW.htm)|Claw|Garra|modificada|
|[977xdYIDaM8Nrj7X.htm](pathfinder-bestiary-items/977xdYIDaM8Nrj7X.htm)|Tail|Tail|modificada|
|[97U65T0kh2mLB67Q.htm](pathfinder-bestiary-items/97U65T0kh2mLB67Q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[98fcfAaERbR2T29q.htm](pathfinder-bestiary-items/98fcfAaERbR2T29q.htm)|Overwhelming Light|Luz abrumadora|modificada|
|[99jBJtnLmD48VOSS.htm](pathfinder-bestiary-items/99jBJtnLmD48VOSS.htm)|Claw|Garra|modificada|
|[99NmF28aoamnNDi6.htm](pathfinder-bestiary-items/99NmF28aoamnNDi6.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[9Aa0Gupq2BHChPd3.htm](pathfinder-bestiary-items/9Aa0Gupq2BHChPd3.htm)|Greataxe|Greataxe|modificada|
|[9AEzHj5y2T93e7EF.htm](pathfinder-bestiary-items/9AEzHj5y2T93e7EF.htm)|Regurgitate|Regurgitar|modificada|
|[9aYwSAhxxa0rnngi.htm](pathfinder-bestiary-items/9aYwSAhxxa0rnngi.htm)|Tail|Tail|modificada|
|[9B8Sr2aqKRJttXog.htm](pathfinder-bestiary-items/9B8Sr2aqKRJttXog.htm)|Constant Spells|Constant Spells|modificada|
|[9boiwlXx7yggBu6F.htm](pathfinder-bestiary-items/9boiwlXx7yggBu6F.htm)|Abyssal Healing|Curación abisal|modificada|
|[9bomxaBEHSLvRrkq.htm](pathfinder-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|Estatua|modificada|
|[9D1QwW0n9RRjhkh0.htm](pathfinder-bestiary-items/9D1QwW0n9RRjhkh0.htm)|Frightful Presence|Frightful Presence|modificada|
|[9dhNquuh2uBurLft.htm](pathfinder-bestiary-items/9dhNquuh2uBurLft.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[9DJr6sgB54eTLxDn.htm](pathfinder-bestiary-items/9DJr6sgB54eTLxDn.htm)|Hoof|Hoof|modificada|
|[9EIwBDwTdZ56ngZa.htm](pathfinder-bestiary-items/9EIwBDwTdZ56ngZa.htm)|Jaws|Fauces|modificada|
|[9ewh7tMyC0NRIpZx.htm](pathfinder-bestiary-items/9ewh7tMyC0NRIpZx.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[9falExFfOIxN5EKp.htm](pathfinder-bestiary-items/9falExFfOIxN5EKp.htm)|Stench|Hedor|modificada|
|[9FQ3eh9wkGZD4b1O.htm](pathfinder-bestiary-items/9FQ3eh9wkGZD4b1O.htm)|Retributive Strike|Golpe retributivo|modificada|
|[9g6k1mFztcJlfe5v.htm](pathfinder-bestiary-items/9g6k1mFztcJlfe5v.htm)|Frightful Presence|Frightful Presence|modificada|
|[9Gpl3fy3X5489jJF.htm](pathfinder-bestiary-items/9Gpl3fy3X5489jJF.htm)|Inferno Leap|Salto sin carrerilla|modificada|
|[9gYASQQf328TmVpW.htm](pathfinder-bestiary-items/9gYASQQf328TmVpW.htm)|Pin Prey|Inmovilizar presa|modificada|
|[9h7bBppeEIWnTtJq.htm](pathfinder-bestiary-items/9h7bBppeEIWnTtJq.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9hjHckpMu7NDpfRe.htm](pathfinder-bestiary-items/9hjHckpMu7NDpfRe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9il80y0VHh8ZAWdY.htm](pathfinder-bestiary-items/9il80y0VHh8ZAWdY.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[9InxDbn3EL8Jsg7a.htm](pathfinder-bestiary-items/9InxDbn3EL8Jsg7a.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[9IuPTxCvRCR7HHp2.htm](pathfinder-bestiary-items/9IuPTxCvRCR7HHp2.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[9iYmbVoVyCe6SNu9.htm](pathfinder-bestiary-items/9iYmbVoVyCe6SNu9.htm)|Coffin Restoration|Restauración de ataúdes|modificada|
|[9ja6Q3mPufB7rxnt.htm](pathfinder-bestiary-items/9ja6Q3mPufB7rxnt.htm)|Running Reload|Recarga a la carrera|modificada|
|[9jVKyOMk9msVj1hz.htm](pathfinder-bestiary-items/9jVKyOMk9msVj1hz.htm)|Capsize|Volcar|modificada|
|[9KaWgpJiexo6SDru.htm](pathfinder-bestiary-items/9KaWgpJiexo6SDru.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[9KbMP0mPbueRMViR.htm](pathfinder-bestiary-items/9KbMP0mPbueRMViR.htm)|Lightning Crash|Lightning Crash|modificada|
|[9MKpsapkMcaRXfuH.htm](pathfinder-bestiary-items/9MKpsapkMcaRXfuH.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[9nfzrmPzAAStRjAe.htm](pathfinder-bestiary-items/9nfzrmPzAAStRjAe.htm)|Quick Draw|Desenvainado rápido|modificada|
|[9NSBH5zk6mEIcPjF.htm](pathfinder-bestiary-items/9NSBH5zk6mEIcPjF.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[9OItti667EywItzF.htm](pathfinder-bestiary-items/9OItti667EywItzF.htm)|Favored Prey|Presa predilecta|modificada|
|[9OiXFTpXGJPjEwUL.htm](pathfinder-bestiary-items/9OiXFTpXGJPjEwUL.htm)|Grab|Agarrado|modificada|
|[9oO34owbXFTjCGng.htm](pathfinder-bestiary-items/9oO34owbXFTjCGng.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9oOOWjBu08zVDEgp.htm](pathfinder-bestiary-items/9oOOWjBu08zVDEgp.htm)|Greatsword|Greatsword|modificada|
|[9OZuMP6qLeoyehVx.htm](pathfinder-bestiary-items/9OZuMP6qLeoyehVx.htm)|Nymph's Beauty|Nymph's Beauty|modificada|
|[9RDkmFyfywWSOj2V.htm](pathfinder-bestiary-items/9RDkmFyfywWSOj2V.htm)|Breath Weapon|Breath Weapon|modificada|
|[9ROgeFg0LqFYjWto.htm](pathfinder-bestiary-items/9ROgeFg0LqFYjWto.htm)|Awaken Tree|Animar árbol animado|modificada|
|[9RRqDOZEOM8rUI1P.htm](pathfinder-bestiary-items/9RRqDOZEOM8rUI1P.htm)|Pack Attack|Ataque en manada|modificada|
|[9S7ODMh2ZM6NXVpF.htm](pathfinder-bestiary-items/9S7ODMh2ZM6NXVpF.htm)|Enfeebling Humors|Humores debilitantes|modificada|
|[9SboqcQona3PXQIx.htm](pathfinder-bestiary-items/9SboqcQona3PXQIx.htm)|Destructive Croak|Croar destructivo|modificada|
|[9SCUupqh4itf7aTx.htm](pathfinder-bestiary-items/9SCUupqh4itf7aTx.htm)|Brain Blisters|Cápsulas cerebrales|modificada|
|[9TCF9GcaMoTKkPSj.htm](pathfinder-bestiary-items/9TCF9GcaMoTKkPSj.htm)|Paralysis|Parálisis|modificada|
|[9tXYGUH9QFkHOzla.htm](pathfinder-bestiary-items/9tXYGUH9QFkHOzla.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[9UvuMlLHVEKID1Jq.htm](pathfinder-bestiary-items/9UvuMlLHVEKID1Jq.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[9V1gE8pVVGhGFEWQ.htm](pathfinder-bestiary-items/9V1gE8pVVGhGFEWQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9v2mzebQFMTkRDd9.htm](pathfinder-bestiary-items/9v2mzebQFMTkRDd9.htm)|Jaws|Fauces|modificada|
|[9VHbtuY8gJuurG3U.htm](pathfinder-bestiary-items/9VHbtuY8gJuurG3U.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[9wiqhTppGBReF5MG.htm](pathfinder-bestiary-items/9wiqhTppGBReF5MG.htm)|Trample|Trample|modificada|
|[9yPuatISy0aUTsVE.htm](pathfinder-bestiary-items/9yPuatISy0aUTsVE.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[9YuL1pu4ZHOAaxcZ.htm](pathfinder-bestiary-items/9YuL1pu4ZHOAaxcZ.htm)|Spore Cloud|Nube de esporas|modificada|
|[9yzexwwuWU0ABr5D.htm](pathfinder-bestiary-items/9yzexwwuWU0ABr5D.htm)|Tail|Tail|modificada|
|[A0HAbzH4Gjn59ZLw.htm](pathfinder-bestiary-items/A0HAbzH4Gjn59ZLw.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[a0noDtcOBbRDMiow.htm](pathfinder-bestiary-items/a0noDtcOBbRDMiow.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[a0WPpTOOGc0FAn2o.htm](pathfinder-bestiary-items/a0WPpTOOGc0FAn2o.htm)|Swarm Mind|Swarm Mind|modificada|
|[a1diHlUPKBt1e8ZM.htm](pathfinder-bestiary-items/a1diHlUPKBt1e8ZM.htm)|Frightful Presence|Frightful Presence|modificada|
|[a2pZeef5wn9y5xAA.htm](pathfinder-bestiary-items/a2pZeef5wn9y5xAA.htm)|Slow|Lentificado/a|modificada|
|[a2vmCuPpbC3JaPX5.htm](pathfinder-bestiary-items/a2vmCuPpbC3JaPX5.htm)|Club|Club|modificada|
|[A33mGw1DCgRJRDSe.htm](pathfinder-bestiary-items/A33mGw1DCgRJRDSe.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[a3oRCd6Fs4j19XDe.htm](pathfinder-bestiary-items/a3oRCd6Fs4j19XDe.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[a3pejXzpAHLXkYWu.htm](pathfinder-bestiary-items/a3pejXzpAHLXkYWu.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[a4bag8JDNGO7xCiD.htm](pathfinder-bestiary-items/a4bag8JDNGO7xCiD.htm)|Savage|Salvajismo|modificada|
|[a4I77q6CzdLCZCG6.htm](pathfinder-bestiary-items/a4I77q6CzdLCZCG6.htm)|Shortbow|Arco corto|modificada|
|[A4Iy8pTKRCuXe0bh.htm](pathfinder-bestiary-items/A4Iy8pTKRCuXe0bh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[a4Zz1r0Kbk6SA66t.htm](pathfinder-bestiary-items/a4Zz1r0Kbk6SA66t.htm)|Spear|Lanza|modificada|
|[A53fmiOtIr4DZqy0.htm](pathfinder-bestiary-items/A53fmiOtIr4DZqy0.htm)|Pack Attack|Ataque en manada|modificada|
|[A6gmij9OXIW0o9vj.htm](pathfinder-bestiary-items/A6gmij9OXIW0o9vj.htm)|Giant Centipede Venom|Veneno de ciempiés gigante|modificada|
|[A7AIAW82kyBDKhWn.htm](pathfinder-bestiary-items/A7AIAW82kyBDKhWn.htm)|Quick Capture|Captura veloz|modificada|
|[a7b4jERQkCw4Wcb8.htm](pathfinder-bestiary-items/a7b4jERQkCw4Wcb8.htm)|Tail|Tail|modificada|
|[a8iHr4CAafw5H0VD.htm](pathfinder-bestiary-items/a8iHr4CAafw5H0VD.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[A8YjaMzEWbFC9JWF.htm](pathfinder-bestiary-items/A8YjaMzEWbFC9JWF.htm)|Grab|Agarrado|modificada|
|[a8yy3kqZmWzMBKci.htm](pathfinder-bestiary-items/a8yy3kqZmWzMBKci.htm)|Brain Collector Venom|Veneno de recolector de cerebros|modificada|
|[A97t9GcQF8k7V7By.htm](pathfinder-bestiary-items/A97t9GcQF8k7V7By.htm)|Claw|Garra|modificada|
|[a9hdRp8sRfsMk1cJ.htm](pathfinder-bestiary-items/a9hdRp8sRfsMk1cJ.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[a9nhBEqNFJ9N1rH5.htm](pathfinder-bestiary-items/a9nhBEqNFJ9N1rH5.htm)|Barbed Leg|Pierna de púas|modificada|
|[a9UivdapXwh89X3a.htm](pathfinder-bestiary-items/a9UivdapXwh89X3a.htm)|Rock|Roca|modificada|
|[AAFWcmRlGxb76Pps.htm](pathfinder-bestiary-items/AAFWcmRlGxb76Pps.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[aAGnyUk4Vj6iSM7n.htm](pathfinder-bestiary-items/aAGnyUk4Vj6iSM7n.htm)|Spring Upon Prey|Spring Upon Prey|modificada|
|[aat9fAhVKzDZsfyl.htm](pathfinder-bestiary-items/aat9fAhVKzDZsfyl.htm)|Powerful Jumper|Salto formidable|modificada|
|[AazKaSGAUXE2To0x.htm](pathfinder-bestiary-items/AazKaSGAUXE2To0x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[aB9m1CJfYd8FCIEx.htm](pathfinder-bestiary-items/aB9m1CJfYd8FCIEx.htm)|Tail|Tail|modificada|
|[AbbMWup6eJg0RC0O.htm](pathfinder-bestiary-items/AbbMWup6eJg0RC0O.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ABE8at8xt3SIvjqx.htm](pathfinder-bestiary-items/ABE8at8xt3SIvjqx.htm)|Implant Core|Implantar núcleo|modificada|
|[abEaFObv46UrcGMU.htm](pathfinder-bestiary-items/abEaFObv46UrcGMU.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[abrvwmCxto1mr7Xu.htm](pathfinder-bestiary-items/abrvwmCxto1mr7Xu.htm)|Sand Glide|Sand Glide|modificada|
|[acAG7Wir41UpIJRx.htm](pathfinder-bestiary-items/acAG7Wir41UpIJRx.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[AcBYEH0GxgM4iV5v.htm](pathfinder-bestiary-items/AcBYEH0GxgM4iV5v.htm)|Ice Tunneler|Ice Tunneler|modificada|
|[AcetPc7srVrIa4vs.htm](pathfinder-bestiary-items/AcetPc7srVrIa4vs.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[achgTT7jV4XPMG5S.htm](pathfinder-bestiary-items/achgTT7jV4XPMG5S.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[ACHlnYzXVxizzqPw.htm](pathfinder-bestiary-items/ACHlnYzXVxizzqPw.htm)|Light Vulnerability|Vulnerabilidad a la luz|modificada|
|[aCvMYE328wAIa1Kx.htm](pathfinder-bestiary-items/aCvMYE328wAIa1Kx.htm)|Dagger|Daga|modificada|
|[aD6MGcEF5IHZE5vn.htm](pathfinder-bestiary-items/aD6MGcEF5IHZE5vn.htm)|Whirlwind|Whirlwind|modificada|
|[ade7S9RWaPXF7KmF.htm](pathfinder-bestiary-items/ade7S9RWaPXF7KmF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ADJpet8hkChh9ljl.htm](pathfinder-bestiary-items/ADJpet8hkChh9ljl.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[ADmvv8bwerUunNDn.htm](pathfinder-bestiary-items/ADmvv8bwerUunNDn.htm)|Negative Healing|Curación negativa|modificada|
|[Adq96cMm6jUHjnw3.htm](pathfinder-bestiary-items/Adq96cMm6jUHjnw3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Ads9d3lRyv0Z8u5p.htm](pathfinder-bestiary-items/Ads9d3lRyv0Z8u5p.htm)|Constant Spells|Constant Spells|modificada|
|[ady0G6RCsUbZ7gev.htm](pathfinder-bestiary-items/ady0G6RCsUbZ7gev.htm)|Emotional Frenzy|Emotional Frenzy|modificada|
|[aE5LiTLYluL3FG7u.htm](pathfinder-bestiary-items/aE5LiTLYluL3FG7u.htm)|Desert Thirst|Sed del desierto|modificada|
|[aeJceILHyteCIR57.htm](pathfinder-bestiary-items/aeJceILHyteCIR57.htm)|Jaws|Fauces|modificada|
|[Aern77CIRMOy8Se3.htm](pathfinder-bestiary-items/Aern77CIRMOy8Se3.htm)|Claw|Garra|modificada|
|[aeZh7jl8AKKWs8Qv.htm](pathfinder-bestiary-items/aeZh7jl8AKKWs8Qv.htm)|Rust|Rust|modificada|
|[Af1bf54ub8OEzyPl.htm](pathfinder-bestiary-items/Af1bf54ub8OEzyPl.htm)|Pseudopod|Pseudópodo|modificada|
|[AFLPUVObYcXb0ATY.htm](pathfinder-bestiary-items/AFLPUVObYcXb0ATY.htm)|Beak|Beak|modificada|
|[AflyUplOQICTA75a.htm](pathfinder-bestiary-items/AflyUplOQICTA75a.htm)|Rush of Water|Chorro de agua|modificada|
|[afRIl1Q7FwNUtxuI.htm](pathfinder-bestiary-items/afRIl1Q7FwNUtxuI.htm)|Nature Empathy|Nature Empathy|modificada|
|[Afsdsjz6Zk8smEoN.htm](pathfinder-bestiary-items/Afsdsjz6Zk8smEoN.htm)|Bonecrunching Bite|Mordisco quiebrahuesos|modificada|
|[AFuY9kW00fzj7WH5.htm](pathfinder-bestiary-items/AFuY9kW00fzj7WH5.htm)|Ranseur|Ranseur|modificada|
|[aGg1kw2fRdZFdeew.htm](pathfinder-bestiary-items/aGg1kw2fRdZFdeew.htm)|Stone Fist|Puño de Piedra|modificada|
|[AGjtcz0cAWzBXlEx.htm](pathfinder-bestiary-items/AGjtcz0cAWzBXlEx.htm)|Unluck Aura|Aura de mala suerte|modificada|
|[agqHW8i0ToF0OIyF.htm](pathfinder-bestiary-items/agqHW8i0ToF0OIyF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[AH4yYBLDuS7Gvkck.htm](pathfinder-bestiary-items/AH4yYBLDuS7Gvkck.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[ah6fU0jl6IqyfBw4.htm](pathfinder-bestiary-items/ah6fU0jl6IqyfBw4.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[AHGv0wFC9jb2bwlQ.htm](pathfinder-bestiary-items/AHGv0wFC9jb2bwlQ.htm)|Claw|Garra|modificada|
|[AHjbBF5emgeLzhga.htm](pathfinder-bestiary-items/AHjbBF5emgeLzhga.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ahtVNJjKjna3LTc1.htm](pathfinder-bestiary-items/ahtVNJjKjna3LTc1.htm)|Alchemical Injection|Inyección alquímica|modificada|
|[AIacQlgZQnMm7C6j.htm](pathfinder-bestiary-items/AIacQlgZQnMm7C6j.htm)|Greatsword|Greatsword|modificada|
|[AIcffB6vSk2oEmqt.htm](pathfinder-bestiary-items/AIcffB6vSk2oEmqt.htm)|Ground Slam|Aporrear el suelo|modificada|
|[aidpPDwvXoVTKC6j.htm](pathfinder-bestiary-items/aidpPDwvXoVTKC6j.htm)|Grab|Agarrado|modificada|
|[AiKIcXFJx9lEEfnt.htm](pathfinder-bestiary-items/AiKIcXFJx9lEEfnt.htm)|Surge|Oleaje|modificada|
|[aIqWT8KUlYwUY9x2.htm](pathfinder-bestiary-items/aIqWT8KUlYwUY9x2.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[aivVKzlEOwieqgj1.htm](pathfinder-bestiary-items/aivVKzlEOwieqgj1.htm)|Flame of Justice|Llama de la justicia|modificada|
|[ajDZIRytCVKqw5RN.htm](pathfinder-bestiary-items/ajDZIRytCVKqw5RN.htm)|Constrict|Restringir|modificada|
|[AJXSn9fEM5rYks8x.htm](pathfinder-bestiary-items/AJXSn9fEM5rYks8x.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[AjYmWzjrt54sexgk.htm](pathfinder-bestiary-items/AjYmWzjrt54sexgk.htm)|Feed on Emotion|Alimentarse de Emoción|modificada|
|[AkGZKjw58GNy5tNW.htm](pathfinder-bestiary-items/AkGZKjw58GNy5tNW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[AkOZsS5Ar1Py89Zo.htm](pathfinder-bestiary-items/AkOZsS5Ar1Py89Zo.htm)|Tendril|Tendril|modificada|
|[AKwJMkLiCXFd1s8q.htm](pathfinder-bestiary-items/AKwJMkLiCXFd1s8q.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[AMKIm1QMEUOXLP3p.htm](pathfinder-bestiary-items/AMKIm1QMEUOXLP3p.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[aMNM0X0dVvVRdlZT.htm](pathfinder-bestiary-items/aMNM0X0dVvVRdlZT.htm)|Vulnerable to Neutralize Poison|Vulnerable a neutralizar veneno|modificada|
|[AMOJU0wX8r6flwf4.htm](pathfinder-bestiary-items/AMOJU0wX8r6flwf4.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[AmPgQIXqmES8fImF.htm](pathfinder-bestiary-items/AmPgQIXqmES8fImF.htm)|Climb Stone|Trepar por la piedra|modificada|
|[an6fdel1IBQ8fJmP.htm](pathfinder-bestiary-items/an6fdel1IBQ8fJmP.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[AniyCnlqZmw3IO9L.htm](pathfinder-bestiary-items/AniyCnlqZmw3IO9L.htm)|Change Shape|Change Shape|modificada|
|[ANrh02werSO57j4X.htm](pathfinder-bestiary-items/ANrh02werSO57j4X.htm)|Wing|Ala|modificada|
|[anRs7G7U6p0N15yh.htm](pathfinder-bestiary-items/anRs7G7U6p0N15yh.htm)|Negative Healing|Curación negativa|modificada|
|[aoLTJq3XbJ29K6yY.htm](pathfinder-bestiary-items/aoLTJq3XbJ29K6yY.htm)|Throw Rock|Arrojar roca|modificada|
|[aoyjyhv7Dw4gIwd3.htm](pathfinder-bestiary-items/aoyjyhv7Dw4gIwd3.htm)|Claw|Garra|modificada|
|[Ap0BhEjEIF39DlVA.htm](pathfinder-bestiary-items/Ap0BhEjEIF39DlVA.htm)|Tail|Tail|modificada|
|[apIdQFYWsMBgRVKk.htm](pathfinder-bestiary-items/apIdQFYWsMBgRVKk.htm)|Pincer|Pinza|modificada|
|[aPjm9C2ibRhYAr0x.htm](pathfinder-bestiary-items/aPjm9C2ibRhYAr0x.htm)|Telepathy 120 feet|Telepatía 120 pies.|modificada|
|[APm9xzjMvRMHIYon.htm](pathfinder-bestiary-items/APm9xzjMvRMHIYon.htm)|Scorpion Venom|Veneno de Escorpión|modificada|
|[aPpWgGl2IQgPJjH1.htm](pathfinder-bestiary-items/aPpWgGl2IQgPJjH1.htm)|Fist|Puño|modificada|
|[Aq3s4Ckr3PMnxD4H.htm](pathfinder-bestiary-items/Aq3s4Ckr3PMnxD4H.htm)|Breath Weapon|Breath Weapon|modificada|
|[aQJcYgTKQBvgKvIv.htm](pathfinder-bestiary-items/aQJcYgTKQBvgKvIv.htm)|Wing Deflection|Desvío con el ala|modificada|
|[aqwHyoUR6cj9y6s8.htm](pathfinder-bestiary-items/aqwHyoUR6cj9y6s8.htm)|Focused Gaze|Centrar mirada|modificada|
|[aQZZc8Ut3mpK2lO3.htm](pathfinder-bestiary-items/aQZZc8Ut3mpK2lO3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[aR1dX4hkVtUEcDD2.htm](pathfinder-bestiary-items/aR1dX4hkVtUEcDD2.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[aRbpPF0TFfUG64dX.htm](pathfinder-bestiary-items/aRbpPF0TFfUG64dX.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ArpzcwnZJV6NMI5C.htm](pathfinder-bestiary-items/ArpzcwnZJV6NMI5C.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ARWbmjbxbCd0JMUv.htm](pathfinder-bestiary-items/ARWbmjbxbCd0JMUv.htm)|Spores|Esporas|modificada|
|[AS5E8Dafjstrafqy.htm](pathfinder-bestiary-items/AS5E8Dafjstrafqy.htm)|Constant Spells|Constant Spells|modificada|
|[asa3KdXDutuDly1Q.htm](pathfinder-bestiary-items/asa3KdXDutuDly1Q.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[AsGPSfdLJ7pQhwZZ.htm](pathfinder-bestiary-items/AsGPSfdLJ7pQhwZZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[AsXbDxRSKFmiQ6jb.htm](pathfinder-bestiary-items/AsXbDxRSKFmiQ6jb.htm)|Enfeebling Bite|Mordisco debilitante|modificada|
|[atH4c29XMbIXGwpG.htm](pathfinder-bestiary-items/atH4c29XMbIXGwpG.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[ATs94SggOJoPD71u.htm](pathfinder-bestiary-items/ATs94SggOJoPD71u.htm)|Diabolic Healing|Curación diabólica|modificada|
|[AtTvfif8mNvTl8HD.htm](pathfinder-bestiary-items/AtTvfif8mNvTl8HD.htm)|Ferocity|Ferocidad|modificada|
|[aTtxivkD4mgfosfH.htm](pathfinder-bestiary-items/aTtxivkD4mgfosfH.htm)|Change Shape|Change Shape|modificada|
|[AtU6PHgBXlRy0fUh.htm](pathfinder-bestiary-items/AtU6PHgBXlRy0fUh.htm)|Jaws|Fauces|modificada|
|[AuBndTd199sHLYXX.htm](pathfinder-bestiary-items/AuBndTd199sHLYXX.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[AuhMdlvvBkXqKNG8.htm](pathfinder-bestiary-items/AuhMdlvvBkXqKNG8.htm)|Telekinetic Object (Slashing)|Objeto Telequinético (Cortante)|modificada|
|[AUlLGXQgQx21Rynz.htm](pathfinder-bestiary-items/AUlLGXQgQx21Rynz.htm)|Wing Deflection|Desvío con el ala|modificada|
|[AuM50DWRYYFugkl6.htm](pathfinder-bestiary-items/AuM50DWRYYFugkl6.htm)|Terrifying Display|Exhibición terrorífica|modificada|
|[auqHGoaXoCI4Z25O.htm](pathfinder-bestiary-items/auqHGoaXoCI4Z25O.htm)|Beak|Beak|modificada|
|[AV27N9R0VVXyHIcK.htm](pathfinder-bestiary-items/AV27N9R0VVXyHIcK.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Av5F60Xe1vXr81pb.htm](pathfinder-bestiary-items/Av5F60Xe1vXr81pb.htm)|+2 to Will Saves vs. Mental|+2 a Salvaciones de Voluntad contra Mental|modificada|
|[AVpXnwBBGoOo68pw.htm](pathfinder-bestiary-items/AVpXnwBBGoOo68pw.htm)|Turn to Mist|Turn to Mist|modificada|
|[avV91CD5YGdSAFSf.htm](pathfinder-bestiary-items/avV91CD5YGdSAFSf.htm)|Eat Away|Eat Away|modificada|
|[aWJTgaTUKotr2LWA.htm](pathfinder-bestiary-items/aWJTgaTUKotr2LWA.htm)|Constrict|Restringir|modificada|
|[aWM3N85sMlll5djQ.htm](pathfinder-bestiary-items/aWM3N85sMlll5djQ.htm)|Abyssal Plague|Plaga abisal|modificada|
|[AWS8p85NHI3IGL2n.htm](pathfinder-bestiary-items/AWS8p85NHI3IGL2n.htm)|Dominate|Dominar|modificada|
|[AWTYToB1yUp2T16A.htm](pathfinder-bestiary-items/AWTYToB1yUp2T16A.htm)|Cold Aura|Cold Aura|modificada|
|[aWzRjtpeOVb7ece0.htm](pathfinder-bestiary-items/aWzRjtpeOVb7ece0.htm)|Branch|Rama|modificada|
|[axeTxBouSunEePNo.htm](pathfinder-bestiary-items/axeTxBouSunEePNo.htm)|Acid Flask|Frasco de ácido|modificada|
|[Axg3G5azQ1xnZunc.htm](pathfinder-bestiary-items/Axg3G5azQ1xnZunc.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[aY1GSHaV78XgFK6W.htm](pathfinder-bestiary-items/aY1GSHaV78XgFK6W.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[aZ0lzdc6SrYbvfX6.htm](pathfinder-bestiary-items/aZ0lzdc6SrYbvfX6.htm)|Constrict|Restringir|modificada|
|[AzIejcCA4nY9zlmB.htm](pathfinder-bestiary-items/AzIejcCA4nY9zlmB.htm)|Sandstorm Breath|Aliento de tormenta de arena|modificada|
|[AzJK5a45GubW0U4t.htm](pathfinder-bestiary-items/AzJK5a45GubW0U4t.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[aZNmBtBro1xYaubO.htm](pathfinder-bestiary-items/aZNmBtBro1xYaubO.htm)|Drider Venom|Veneno de dra|modificada|
|[azSRd7to2x2eVjTv.htm](pathfinder-bestiary-items/azSRd7to2x2eVjTv.htm)|Scimitar|Cimitarra|modificada|
|[b0CZ1TMWNNWhztYK.htm](pathfinder-bestiary-items/b0CZ1TMWNNWhztYK.htm)|Fist|Puño|modificada|
|[b1CPKzVXEGyolvUo.htm](pathfinder-bestiary-items/b1CPKzVXEGyolvUo.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[b1w5wps2XBnNPOls.htm](pathfinder-bestiary-items/b1w5wps2XBnNPOls.htm)|Furious Claws|Garras furiosas|modificada|
|[b24uVlcva1St9NKy.htm](pathfinder-bestiary-items/b24uVlcva1St9NKy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[B2QWyyEvVmWZPUkk.htm](pathfinder-bestiary-items/B2QWyyEvVmWZPUkk.htm)|Lurching Charge|Cargar dando bandazos|modificada|
|[b2WLYPv43nso3X3U.htm](pathfinder-bestiary-items/b2WLYPv43nso3X3U.htm)|Jaws|Fauces|modificada|
|[b3OSC1hy3efFqhm7.htm](pathfinder-bestiary-items/b3OSC1hy3efFqhm7.htm)|Seductive Presence|Presencia seductora|modificada|
|[B3yqLomVN3P7lLnV.htm](pathfinder-bestiary-items/B3yqLomVN3P7lLnV.htm)|Trident|Trident|modificada|
|[b4GFgu6HgDqvji6X.htm](pathfinder-bestiary-items/b4GFgu6HgDqvji6X.htm)|Tail|Tail|modificada|
|[B4uQ3uTmODEiTxnM.htm](pathfinder-bestiary-items/B4uQ3uTmODEiTxnM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[b5sQlwDziqEPeMij.htm](pathfinder-bestiary-items/b5sQlwDziqEPeMij.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[B5U0kqS1gFhaSpqc.htm](pathfinder-bestiary-items/B5U0kqS1gFhaSpqc.htm)|Fist|Puño|modificada|
|[B6szcvpWsKu9fNp6.htm](pathfinder-bestiary-items/B6szcvpWsKu9fNp6.htm)|Claw|Garra|modificada|
|[B7oDXvjrHROptNlb.htm](pathfinder-bestiary-items/B7oDXvjrHROptNlb.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[B7rzf0nBJmzg8x0y.htm](pathfinder-bestiary-items/B7rzf0nBJmzg8x0y.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[B7v1R6e83IW0B1SR.htm](pathfinder-bestiary-items/B7v1R6e83IW0B1SR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[B8eTnDbhYn9WARrP.htm](pathfinder-bestiary-items/B8eTnDbhYn9WARrP.htm)|Regeneration 10 (Deactivated by Cold Iron)|Regeneración 10 (Desactivado por Cold Iron)|modificada|
|[b8kddR2Im2nSOnDM.htm](pathfinder-bestiary-items/b8kddR2Im2nSOnDM.htm)|Gallop|Galope|modificada|
|[b8LAOUk743PuI1qO.htm](pathfinder-bestiary-items/b8LAOUk743PuI1qO.htm)|Constant Spells|Constant Spells|modificada|
|[B9C716BDIWeqDrLI.htm](pathfinder-bestiary-items/B9C716BDIWeqDrLI.htm)|Jaws|Fauces|modificada|
|[b9p4QWRBH39ePPdV.htm](pathfinder-bestiary-items/b9p4QWRBH39ePPdV.htm)|Morningstar|Morningstar|modificada|
|[B9QrAlNM1MoP8sDk.htm](pathfinder-bestiary-items/B9QrAlNM1MoP8sDk.htm)|Bardic Lore|Saber bárdico|modificada|
|[B9VILNOR0aKT4LQ0.htm](pathfinder-bestiary-items/B9VILNOR0aKT4LQ0.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[B9YtjVsCCmeVw39y.htm](pathfinder-bestiary-items/B9YtjVsCCmeVw39y.htm)|Fist|Puño|modificada|
|[bA2QzO0CkyheAgub.htm](pathfinder-bestiary-items/bA2QzO0CkyheAgub.htm)|Claw|Garra|modificada|
|[BaQdyDW5VvRs5ZV4.htm](pathfinder-bestiary-items/BaQdyDW5VvRs5ZV4.htm)|Claw|Garra|modificada|
|[BbFyjI0o6Aed5DYL.htm](pathfinder-bestiary-items/BbFyjI0o6Aed5DYL.htm)|Strafing Chomp|Strafing Chomp|modificada|
|[BblU1XBSC9IdeWlp.htm](pathfinder-bestiary-items/BblU1XBSC9IdeWlp.htm)|Jaws|Fauces|modificada|
|[bcGhVPJyGDyUGvLK.htm](pathfinder-bestiary-items/bcGhVPJyGDyUGvLK.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[bCgl0VBfwaBeDgOh.htm](pathfinder-bestiary-items/bCgl0VBfwaBeDgOh.htm)|Longspear|Longspear|modificada|
|[bcl3zcX6TOOupNt9.htm](pathfinder-bestiary-items/bcl3zcX6TOOupNt9.htm)|Knockdown|Derribo|modificada|
|[Bcm7iwcliF6ChhW7.htm](pathfinder-bestiary-items/Bcm7iwcliF6ChhW7.htm)|Tarn Linnorm Venom|Veneno de linnorm del lago|modificada|
|[bdbMvBqIkhaOvg4j.htm](pathfinder-bestiary-items/bdbMvBqIkhaOvg4j.htm)|Slime Squirt|Slime Squirt|modificada|
|[bDkXVdgOwJv0PBRc.htm](pathfinder-bestiary-items/bDkXVdgOwJv0PBRc.htm)|Barbed Maw|Fauces con púas|modificada|
|[bDogEMyUIoxPMls3.htm](pathfinder-bestiary-items/bDogEMyUIoxPMls3.htm)|Aquatic Dash|Maniobras acuáticas|modificada|
|[bdrrSnIH32ERMP25.htm](pathfinder-bestiary-items/bdrrSnIH32ERMP25.htm)|Roar|Roar|modificada|
|[Be8ZCKKmNDNPgCM0.htm](pathfinder-bestiary-items/Be8ZCKKmNDNPgCM0.htm)|Goliath Spider Venom|Goliath Spider Venom|modificada|
|[BER1sjugNCn7ml14.htm](pathfinder-bestiary-items/BER1sjugNCn7ml14.htm)|Crumble|Desmoronarse|modificada|
|[bEsmMOS1PqhLHHbh.htm](pathfinder-bestiary-items/bEsmMOS1PqhLHHbh.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[BeuWnsp3t3NisaY3.htm](pathfinder-bestiary-items/BeuWnsp3t3NisaY3.htm)|Mandibles|Mandíbulas|modificada|
|[BF9LWkoQrDrBUzhl.htm](pathfinder-bestiary-items/BF9LWkoQrDrBUzhl.htm)|Shape Ice|Moldear hielo|modificada|
|[bftpGvdJeX5iLTfn.htm](pathfinder-bestiary-items/bftpGvdJeX5iLTfn.htm)|Hunk Of Meat|Hunk Of Meat|modificada|
|[bg3DA0xcxyfSwJZm.htm](pathfinder-bestiary-items/bg3DA0xcxyfSwJZm.htm)|Staff|Báculo|modificada|
|[bgkXxmVMntPQOmND.htm](pathfinder-bestiary-items/bgkXxmVMntPQOmND.htm)|Rapier|Estoque|modificada|
|[bgmY8mouK9Yal1iO.htm](pathfinder-bestiary-items/bgmY8mouK9Yal1iO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[bGNrdbl4JTRtmeMy.htm](pathfinder-bestiary-items/bGNrdbl4JTRtmeMy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bgQ0VnpFrkVAGfAI.htm](pathfinder-bestiary-items/bgQ0VnpFrkVAGfAI.htm)|Claw|Garra|modificada|
|[bGvsYcaEa1DdcONa.htm](pathfinder-bestiary-items/bGvsYcaEa1DdcONa.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[BhAemzsQzTnyMQTs.htm](pathfinder-bestiary-items/BhAemzsQzTnyMQTs.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[bhI0M1dnsLjItat7.htm](pathfinder-bestiary-items/bhI0M1dnsLjItat7.htm)|Mandibles|Mandíbulas|modificada|
|[bhRfrSYjod5132GE.htm](pathfinder-bestiary-items/bhRfrSYjod5132GE.htm)|Acid Maw|Fauces ácidas|modificada|
|[bHxNMVIdbA3qM962.htm](pathfinder-bestiary-items/bHxNMVIdbA3qM962.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Bie1u5IuxXpAhbC7.htm](pathfinder-bestiary-items/Bie1u5IuxXpAhbC7.htm)|Jaws|Fauces|modificada|
|[BJ1PhJfeJAlUL4sL.htm](pathfinder-bestiary-items/BJ1PhJfeJAlUL4sL.htm)|Tremorsense (Imprecise) 100 feet|Sentido del Temblor (Impreciso) 100 pies|modificada|
|[Bj9AvzdEeKXM1Lpy.htm](pathfinder-bestiary-items/Bj9AvzdEeKXM1Lpy.htm)|Leg|Pierna|modificada|
|[BJ9pqonMdpZOYbY7.htm](pathfinder-bestiary-items/BJ9pqonMdpZOYbY7.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bjF022r2jEalWdPs.htm](pathfinder-bestiary-items/bjF022r2jEalWdPs.htm)|Swarm Mind|Swarm Mind|modificada|
|[BjiGVDonTx2JHmxh.htm](pathfinder-bestiary-items/BjiGVDonTx2JHmxh.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[BJiQlSKAx6WhMT1p.htm](pathfinder-bestiary-items/BJiQlSKAx6WhMT1p.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bjN59WI6VS1niQFU.htm](pathfinder-bestiary-items/bjN59WI6VS1niQFU.htm)|Fast Swallow|Tragar de golpe|modificada|
|[bjVIgy73LNkpiuOw.htm](pathfinder-bestiary-items/bjVIgy73LNkpiuOw.htm)|Dagger|Daga|modificada|
|[bK065dhAE5PbYbis.htm](pathfinder-bestiary-items/bK065dhAE5PbYbis.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Bk52AZEcClvtQXB0.htm](pathfinder-bestiary-items/Bk52AZEcClvtQXB0.htm)|Jaws|Fauces|modificada|
|[Bk5ADVa0aOQ7APmH.htm](pathfinder-bestiary-items/Bk5ADVa0aOQ7APmH.htm)|Tail|Tail|modificada|
|[bKldotvWkspZfjHv.htm](pathfinder-bestiary-items/bKldotvWkspZfjHv.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[BKmS1pjKN4QsePV7.htm](pathfinder-bestiary-items/BKmS1pjKN4QsePV7.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bKmTRJI4PjS70sWZ.htm](pathfinder-bestiary-items/bKmTRJI4PjS70sWZ.htm)|Inexorable March|Marcha inexorable|modificada|
|[bKPOLbNfdVcrEpjV.htm](pathfinder-bestiary-items/bKPOLbNfdVcrEpjV.htm)|Bonds of Iron|Ligaduras de hierro|modificada|
|[BkQtrsdTqOIyZInG.htm](pathfinder-bestiary-items/BkQtrsdTqOIyZInG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bkVvJziJZPgu8vV0.htm](pathfinder-bestiary-items/bkVvJziJZPgu8vV0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BleHEEpt3dwEMeDo.htm](pathfinder-bestiary-items/BleHEEpt3dwEMeDo.htm)|Treacherous Aura|Aura traicionera|modificada|
|[blGUawuZyyQVnVXf.htm](pathfinder-bestiary-items/blGUawuZyyQVnVXf.htm)|Pounce|Abalanzarse|modificada|
|[BLleGAYzhc0cIyIz.htm](pathfinder-bestiary-items/BLleGAYzhc0cIyIz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bLrblJlMOfhn39My.htm](pathfinder-bestiary-items/bLrblJlMOfhn39My.htm)|Swarm Mind|Swarm Mind|modificada|
|[BlUA1CHpOuotxrV7.htm](pathfinder-bestiary-items/BlUA1CHpOuotxrV7.htm)|Foot|Pie|modificada|
|[bmbqZlQmEmeXm2DS.htm](pathfinder-bestiary-items/bmbqZlQmEmeXm2DS.htm)|Treacherous Veil|Velo engaso|modificada|
|[BMgUFhUSXPid36Be.htm](pathfinder-bestiary-items/BMgUFhUSXPid36Be.htm)|Ethereal Web Trap|Trampa de redes etaraéreas|modificada|
|[BmhB6hl9Ivt5pnw6.htm](pathfinder-bestiary-items/BmhB6hl9Ivt5pnw6.htm)|Talon|Talon|modificada|
|[bmmCoxENGAgHtWP1.htm](pathfinder-bestiary-items/bmmCoxENGAgHtWP1.htm)|Web Sense|Web Sense|modificada|
|[bmQdHng3a04PljTz.htm](pathfinder-bestiary-items/bmQdHng3a04PljTz.htm)|Divine Spontaneous Spells|Hechizos Divinos Espontáneos|modificada|
|[BmQOyLxye6DAZpWI.htm](pathfinder-bestiary-items/BmQOyLxye6DAZpWI.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[BMYNq39vlF0aowNr.htm](pathfinder-bestiary-items/BMYNq39vlF0aowNr.htm)|Capsize|Volcar|modificada|
|[bnuflGRdVoZz0Rf9.htm](pathfinder-bestiary-items/bnuflGRdVoZz0Rf9.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[BnuggroyV0iJLdlp.htm](pathfinder-bestiary-items/BnuggroyV0iJLdlp.htm)|Constant Spells|Constant Spells|modificada|
|[bo8qDCWaF4txPsi1.htm](pathfinder-bestiary-items/bo8qDCWaF4txPsi1.htm)|Keen Returning Hatchet|Afilada Hacha Retornante|modificada|
|[BOFFwI5nKDfABluP.htm](pathfinder-bestiary-items/BOFFwI5nKDfABluP.htm)|Negative Healing|Curación negativa|modificada|
|[bOieIj9yjp8Tyi0J.htm](pathfinder-bestiary-items/bOieIj9yjp8Tyi0J.htm)|Claw|Garra|modificada|
|[BoUOzgE6tap1MfMd.htm](pathfinder-bestiary-items/BoUOzgE6tap1MfMd.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[bPBjZcWUOmlUEYIw.htm](pathfinder-bestiary-items/bPBjZcWUOmlUEYIw.htm)|Jaws|Fauces|modificada|
|[BPCk1pWTtkLjllYB.htm](pathfinder-bestiary-items/BPCk1pWTtkLjllYB.htm)|Claw|Garra|modificada|
|[bpECAdKNxGcT7pXU.htm](pathfinder-bestiary-items/bpECAdKNxGcT7pXU.htm)|Twin Feint|Doble finta gemela|modificada|
|[bpsAcAXXrfV4uyDd.htm](pathfinder-bestiary-items/bpsAcAXXrfV4uyDd.htm)|Tail|Tail|modificada|
|[bQaD3ryddLU1MPFx.htm](pathfinder-bestiary-items/bQaD3ryddLU1MPFx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[bqcTHqymDNnpRDg9.htm](pathfinder-bestiary-items/bqcTHqymDNnpRDg9.htm)|Buck|Encabritarse|modificada|
|[bQOfAaZEAHrScsHM.htm](pathfinder-bestiary-items/bQOfAaZEAHrScsHM.htm)|Grab|Agarrado|modificada|
|[BqxVlrDpgV0pxdb6.htm](pathfinder-bestiary-items/BqxVlrDpgV0pxdb6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BsLJO9VEEJtk5wse.htm](pathfinder-bestiary-items/BsLJO9VEEJtk5wse.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[BsoxzxmGvzcKefKT.htm](pathfinder-bestiary-items/BsoxzxmGvzcKefKT.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[bsSr0xdB56iFuS70.htm](pathfinder-bestiary-items/bsSr0xdB56iFuS70.htm)|All-Around Vision|All-Around Vision|modificada|
|[bT8ABzfkEP7llB3c.htm](pathfinder-bestiary-items/bT8ABzfkEP7llB3c.htm)|Frightful Presence|Frightful Presence|modificada|
|[BtBJ8FVnBLJgGYxG.htm](pathfinder-bestiary-items/BtBJ8FVnBLJgGYxG.htm)|Wing|Ala|modificada|
|[bthV65bmBiFxafJ6.htm](pathfinder-bestiary-items/bthV65bmBiFxafJ6.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[BudK3JDDVjbWqw8G.htm](pathfinder-bestiary-items/BudK3JDDVjbWqw8G.htm)|Warpwave Strike|Golpe de Onda Warp|modificada|
|[BuEcwGM7NH9is0w9.htm](pathfinder-bestiary-items/BuEcwGM7NH9is0w9.htm)|Eye Beams|Eye Beams|modificada|
|[BuTCzXbSgKU068vi.htm](pathfinder-bestiary-items/BuTCzXbSgKU068vi.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[BuvtHVq16FzLQ1Fi.htm](pathfinder-bestiary-items/BuvtHVq16FzLQ1Fi.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[BVtWjIcXEObj4Rr9.htm](pathfinder-bestiary-items/BVtWjIcXEObj4Rr9.htm)|Hunted Shot|Tiro cazado|modificada|
|[bVwQccP3j81toBAq.htm](pathfinder-bestiary-items/bVwQccP3j81toBAq.htm)|Flame of Justice|Llama de la justicia|modificada|
|[bW0FVpeKKVStkJVf.htm](pathfinder-bestiary-items/bW0FVpeKKVStkJVf.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[bWfE1BR1kz8fIanF.htm](pathfinder-bestiary-items/bWfE1BR1kz8fIanF.htm)|Vicious Gore|Cornear|modificada|
|[bwU9HIhgyMLShCWw.htm](pathfinder-bestiary-items/bwU9HIhgyMLShCWw.htm)|Crossbow|Ballesta|modificada|
|[bWytg8B4wFHQLuzN.htm](pathfinder-bestiary-items/bWytg8B4wFHQLuzN.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[Bx7nabKeB4DLhe7B.htm](pathfinder-bestiary-items/Bx7nabKeB4DLhe7B.htm)|Darkvision|Visión en la oscuridad|modificada|
|[BXuDhj7X68wG6zma.htm](pathfinder-bestiary-items/BXuDhj7X68wG6zma.htm)|Javelin|Javelin|modificada|
|[by7p3pdmrMKbVYk8.htm](pathfinder-bestiary-items/by7p3pdmrMKbVYk8.htm)|Tail|Tail|modificada|
|[bY7rkyUV1wBS80Uz.htm](pathfinder-bestiary-items/bY7rkyUV1wBS80Uz.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bYG1y6PsSmulLnqZ.htm](pathfinder-bestiary-items/bYG1y6PsSmulLnqZ.htm)|Change Shape|Change Shape|modificada|
|[ByItiXXLE0ZzOvm8.htm](pathfinder-bestiary-items/ByItiXXLE0ZzOvm8.htm)|Improved Grab|Agarrado mejorado|modificada|
|[bz1ZQoFYo4soy8jN.htm](pathfinder-bestiary-items/bz1ZQoFYo4soy8jN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bZ4JNU36mcWHHrmR.htm](pathfinder-bestiary-items/bZ4JNU36mcWHHrmR.htm)|Grasping Tendrils|Agarrar con zarcillos|modificada|
|[bZL57I2PUMygGVWO.htm](pathfinder-bestiary-items/bZL57I2PUMygGVWO.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[bzNpoAXw0BCP1dGK.htm](pathfinder-bestiary-items/bzNpoAXw0BCP1dGK.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[BzrCf46VVV0IyaJb.htm](pathfinder-bestiary-items/BzrCf46VVV0IyaJb.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[c09zz4QRa6hJSZ6q.htm](pathfinder-bestiary-items/c09zz4QRa6hJSZ6q.htm)|Blackaxe - Teleport|Blackaxe - Teletransporte|modificada|
|[C0y22etsAkFpPbDt.htm](pathfinder-bestiary-items/C0y22etsAkFpPbDt.htm)|Negative Healing|Curación negativa|modificada|
|[C2OIrqZ6uMpRgLEF.htm](pathfinder-bestiary-items/C2OIrqZ6uMpRgLEF.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[C2pJTs2tt6oHDcFW.htm](pathfinder-bestiary-items/C2pJTs2tt6oHDcFW.htm)|Change Shape|Change Shape|modificada|
|[c2UaCFr89s3CtUzy.htm](pathfinder-bestiary-items/c2UaCFr89s3CtUzy.htm)|Rock|Roca|modificada|
|[C35zZH9Tm4QXYQ4y.htm](pathfinder-bestiary-items/C35zZH9Tm4QXYQ4y.htm)|Breath Weapon|Breath Weapon|modificada|
|[c3JTz3NjO3wCrokf.htm](pathfinder-bestiary-items/c3JTz3NjO3wCrokf.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[c3nJwvrVvJlQrgWU.htm](pathfinder-bestiary-items/c3nJwvrVvJlQrgWU.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[C3vgtExLgNdN7GHz.htm](pathfinder-bestiary-items/C3vgtExLgNdN7GHz.htm)|Vorpal Cold Iron Silver Longsword|Espada larga de plata de hierro frío Vorpalina.|modificada|
|[c4LXwYKMdM5R7B7K.htm](pathfinder-bestiary-items/c4LXwYKMdM5R7B7K.htm)|Hidden Movement|Movimiento oculto|modificada|
|[C5CTHxWjBYotqr2K.htm](pathfinder-bestiary-items/C5CTHxWjBYotqr2K.htm)|Breath Weapon|Breath Weapon|modificada|
|[c5Kaa1L8XCouqJy1.htm](pathfinder-bestiary-items/c5Kaa1L8XCouqJy1.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[C5Knn5mQqyHvaE7Z.htm](pathfinder-bestiary-items/C5Knn5mQqyHvaE7Z.htm)|Fist|Puño|modificada|
|[C5NNlv2jeZwTRcPG.htm](pathfinder-bestiary-items/C5NNlv2jeZwTRcPG.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[C5O7yPqYGSLmmeA2.htm](pathfinder-bestiary-items/C5O7yPqYGSLmmeA2.htm)|Light Mace|Maza de luz|modificada|
|[C6VJPsP2T62aUaDR.htm](pathfinder-bestiary-items/C6VJPsP2T62aUaDR.htm)|Fangs|Colmillos|modificada|
|[C77jgOGQyRA9OepX.htm](pathfinder-bestiary-items/C77jgOGQyRA9OepX.htm)|Claw|Garra|modificada|
|[C7DnHa0hbdRjZtG9.htm](pathfinder-bestiary-items/C7DnHa0hbdRjZtG9.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[c8cI240EHdwf8MM1.htm](pathfinder-bestiary-items/c8cI240EHdwf8MM1.htm)|Deadly Mandibles|Mandíbulas letales|modificada|
|[casqOYm41JuCEUGY.htm](pathfinder-bestiary-items/casqOYm41JuCEUGY.htm)|Wide Swing|Barrido ancho|modificada|
|[CAyvne51mSjL5IzV.htm](pathfinder-bestiary-items/CAyvne51mSjL5IzV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Cb2C8LIPyMVI1r9J.htm](pathfinder-bestiary-items/Cb2C8LIPyMVI1r9J.htm)|Vent|Expulsar gases|modificada|
|[CblyGPqXFB5ALfDo.htm](pathfinder-bestiary-items/CblyGPqXFB5ALfDo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CBSPcFxMeCnKziJ9.htm](pathfinder-bestiary-items/CBSPcFxMeCnKziJ9.htm)|Constant Spells|Constant Spells|modificada|
|[cbuxKyYefdlX5AAX.htm](pathfinder-bestiary-items/cbuxKyYefdlX5AAX.htm)|Bloodletting|Sangría|modificada|
|[CBZgnwJ9S03gtLHf.htm](pathfinder-bestiary-items/CBZgnwJ9S03gtLHf.htm)|Focused Assault|Asalto concentrado|modificada|
|[cC853WQiGJtDTkRc.htm](pathfinder-bestiary-items/cC853WQiGJtDTkRc.htm)|Frightful Presence|Frightful Presence|modificada|
|[cckOUw1gK3GNCnML.htm](pathfinder-bestiary-items/cckOUw1gK3GNCnML.htm)|Slime|Slime|modificada|
|[CcLeMibU2tmiAzzF.htm](pathfinder-bestiary-items/CcLeMibU2tmiAzzF.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Cd0NzEZrJzdhDSY8.htm](pathfinder-bestiary-items/Cd0NzEZrJzdhDSY8.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[CDkktCbVeomEuX4D.htm](pathfinder-bestiary-items/CDkktCbVeomEuX4D.htm)|Filth Wallow|Filth Wallow|modificada|
|[ce9aKxH1S1gitPNO.htm](pathfinder-bestiary-items/ce9aKxH1S1gitPNO.htm)|Fangs|Colmillos|modificada|
|[CellesdPVQW5Dk8g.htm](pathfinder-bestiary-items/CellesdPVQW5Dk8g.htm)|Ferocity|Ferocidad|modificada|
|[CEngjOQBRH1DpQeo.htm](pathfinder-bestiary-items/CEngjOQBRH1DpQeo.htm)|Breach|Emerger|modificada|
|[Cf4Skii5emWWMeWd.htm](pathfinder-bestiary-items/Cf4Skii5emWWMeWd.htm)|Breath Weapon|Breath Weapon|modificada|
|[cFtfekZXzWpLtSzL.htm](pathfinder-bestiary-items/cFtfekZXzWpLtSzL.htm)|Amnesia Venom|Veneno de amnesia|modificada|
|[Cfu9UWLk4FBlTQTo.htm](pathfinder-bestiary-items/Cfu9UWLk4FBlTQTo.htm)|Reactive Gnaw|Mordisco reactivo|modificada|
|[CfzB5r8CSHETuXfA.htm](pathfinder-bestiary-items/CfzB5r8CSHETuXfA.htm)|Site Bound|Ligado a una ubicación|modificada|
|[cg8c4dIdBFiff9le.htm](pathfinder-bestiary-items/cg8c4dIdBFiff9le.htm)|Sweeping Hook|Gancho de barrido|modificada|
|[cGjIW67qp4XNCsT3.htm](pathfinder-bestiary-items/cGjIW67qp4XNCsT3.htm)|Jaws|Fauces|modificada|
|[cGPPwZfpkBdll6m3.htm](pathfinder-bestiary-items/cGPPwZfpkBdll6m3.htm)|Constant Spells|Constant Spells|modificada|
|[cgTMxtmKD5ESJlyX.htm](pathfinder-bestiary-items/cgTMxtmKD5ESJlyX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[CGWJ8z84ruiOubNo.htm](pathfinder-bestiary-items/CGWJ8z84ruiOubNo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ch3sywJ2yffhRgXI.htm](pathfinder-bestiary-items/Ch3sywJ2yffhRgXI.htm)|Jaws|Fauces|modificada|
|[cHbcVpImsTWKA9DW.htm](pathfinder-bestiary-items/cHbcVpImsTWKA9DW.htm)|Tail Sweep|Barrido de Cola|modificada|
|[CHG9uWZmFmSExlPo.htm](pathfinder-bestiary-items/CHG9uWZmFmSExlPo.htm)|Tail|Tail|modificada|
|[CHS0DAe3ekrw8ve1.htm](pathfinder-bestiary-items/CHS0DAe3ekrw8ve1.htm)|Shortbow|Arco corto|modificada|
|[CHt0fL7eCWfy5Rm7.htm](pathfinder-bestiary-items/CHt0fL7eCWfy5Rm7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Chvo35nO1IOSkoxZ.htm](pathfinder-bestiary-items/Chvo35nO1IOSkoxZ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[cIAptRydc370Xjbd.htm](pathfinder-bestiary-items/cIAptRydc370Xjbd.htm)|Intense Heat|Calor intenso|modificada|
|[CiirPvlIj0ZtuHce.htm](pathfinder-bestiary-items/CiirPvlIj0ZtuHce.htm)|Vengeful Anger|Furia vengativa|modificada|
|[cIlup2qYJhZmAmRn.htm](pathfinder-bestiary-items/cIlup2qYJhZmAmRn.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ciyqPmkNFmHZPuIP.htm](pathfinder-bestiary-items/ciyqPmkNFmHZPuIP.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[CjdoOLjRkqyfp25v.htm](pathfinder-bestiary-items/CjdoOLjRkqyfp25v.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[cjHi3qrYqK60e3Nn.htm](pathfinder-bestiary-items/cjHi3qrYqK60e3Nn.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[cJo1BrfvKmNfCJOd.htm](pathfinder-bestiary-items/cJo1BrfvKmNfCJOd.htm)|Jaw|Fauces|modificada|
|[cjpf10fNDqFN7zh9.htm](pathfinder-bestiary-items/cjpf10fNDqFN7zh9.htm)|Jaws|Fauces|modificada|
|[ckyWLmudGWk8ota8.htm](pathfinder-bestiary-items/ckyWLmudGWk8ota8.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[cKztX9AO2dzSFhZu.htm](pathfinder-bestiary-items/cKztX9AO2dzSFhZu.htm)|Spike Stones|Púas de piedra|modificada|
|[cL0kuYeKKUFMb9w0.htm](pathfinder-bestiary-items/cL0kuYeKKUFMb9w0.htm)|Confounding Lash|Latigazo confuso|modificada|
|[cL9t5xg0FTPCRpjR.htm](pathfinder-bestiary-items/cL9t5xg0FTPCRpjR.htm)|Improved Grab|Agarrado mejorado|modificada|
|[clqibIDmibjUw6f7.htm](pathfinder-bestiary-items/clqibIDmibjUw6f7.htm)|Jaws|Fauces|modificada|
|[CLQtaRBZzEVhkEpc.htm](pathfinder-bestiary-items/CLQtaRBZzEVhkEpc.htm)|Staff Gems|Gemas de bastón|modificada|
|[cM51Cf58q73IyWOj.htm](pathfinder-bestiary-items/cM51Cf58q73IyWOj.htm)|Trackless Step|Pisada sin rastro|modificada|
|[CMfpQM5btdzVWxB5.htm](pathfinder-bestiary-items/CMfpQM5btdzVWxB5.htm)|Slashing Claws|Garras lacerantes|modificada|
|[CMFwSAQpmkkqeHxH.htm](pathfinder-bestiary-items/CMFwSAQpmkkqeHxH.htm)|Grab|Agarrado|modificada|
|[cMyzo0AivvJxq0Ly.htm](pathfinder-bestiary-items/cMyzo0AivvJxq0Ly.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[cndA0H7XPHDhDL2I.htm](pathfinder-bestiary-items/cndA0H7XPHDhDL2I.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[CnmgzGmi2Avq67QA.htm](pathfinder-bestiary-items/CnmgzGmi2Avq67QA.htm)|Dogslicer|Dogslicer|modificada|
|[cnueIB7ag8UbEdIb.htm](pathfinder-bestiary-items/cnueIB7ag8UbEdIb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[COesoRf5oc4fLJhF.htm](pathfinder-bestiary-items/COesoRf5oc4fLJhF.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[coknqLEZIU2gBBH1.htm](pathfinder-bestiary-items/coknqLEZIU2gBBH1.htm)|Writhing Arms|Brazos retorcidos|modificada|
|[ComzTzx9Gu4QDQxN.htm](pathfinder-bestiary-items/ComzTzx9Gu4QDQxN.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[cOsFGJ38rHgozF9z.htm](pathfinder-bestiary-items/cOsFGJ38rHgozF9z.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[COureLl9794sHLIb.htm](pathfinder-bestiary-items/COureLl9794sHLIb.htm)|Blowgun|Blowgun|modificada|
|[Cp2wPvwVrx6QHzjb.htm](pathfinder-bestiary-items/Cp2wPvwVrx6QHzjb.htm)|Trample|Trample|modificada|
|[cP4AQbueWMfANEBz.htm](pathfinder-bestiary-items/cP4AQbueWMfANEBz.htm)|Greatclub|Greatclub|modificada|
|[cpfFNNa5pJRIFFS4.htm](pathfinder-bestiary-items/cpfFNNa5pJRIFFS4.htm)|Ranseur|Ranseur|modificada|
|[cPMrUDg67L1MxOgP.htm](pathfinder-bestiary-items/cPMrUDg67L1MxOgP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cPq1a6i8dlZo6SPu.htm](pathfinder-bestiary-items/cPq1a6i8dlZo6SPu.htm)|Breath Weapon|Breath Weapon|modificada|
|[cprgu1LyMWRqzI0w.htm](pathfinder-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|Garra|modificada|
|[cPuEYKhbrReSjt1J.htm](pathfinder-bestiary-items/cPuEYKhbrReSjt1J.htm)|Touch of Charity|Toque de caridad|modificada|
|[cpZUZlhLCvv2qK9o.htm](pathfinder-bestiary-items/cpZUZlhLCvv2qK9o.htm)|Swipe|Vaivén|modificada|
|[cqn01fpxgrAl4NvI.htm](pathfinder-bestiary-items/cqn01fpxgrAl4NvI.htm)|Wraith Spawn|Engendro Espectro|modificada|
|[cQnMBBEfd291uJ4j.htm](pathfinder-bestiary-items/cQnMBBEfd291uJ4j.htm)|Stinger|Aguijón|modificada|
|[CQRb2PNLd84eBkAH.htm](pathfinder-bestiary-items/CQRb2PNLd84eBkAH.htm)|Claw|Garra|modificada|
|[cQrkrn9uX9hV860V.htm](pathfinder-bestiary-items/cQrkrn9uX9hV860V.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[CqU1NxH3VCWX924G.htm](pathfinder-bestiary-items/CqU1NxH3VCWX924G.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[cr8chy1goN1ZSrnT.htm](pathfinder-bestiary-items/cr8chy1goN1ZSrnT.htm)|Mound|Montículo|modificada|
|[crbSG0me6XVQjR9f.htm](pathfinder-bestiary-items/crbSG0me6XVQjR9f.htm)|Gust|Gust|modificada|
|[cRpfBVlR934upDge.htm](pathfinder-bestiary-items/cRpfBVlR934upDge.htm)|Negative Healing|Curación negativa|modificada|
|[Crv6pRa77FWxBY6M.htm](pathfinder-bestiary-items/Crv6pRa77FWxBY6M.htm)|Wasp Larva|Wasp Larva|modificada|
|[crXV18abZr2DcEh8.htm](pathfinder-bestiary-items/crXV18abZr2DcEh8.htm)|Regurgitate|Regurgitar|modificada|
|[CSxqThhPm4iTCCOP.htm](pathfinder-bestiary-items/CSxqThhPm4iTCCOP.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Cti6KJQlr8VT1UaM.htm](pathfinder-bestiary-items/Cti6KJQlr8VT1UaM.htm)|Light Form|Forma de luz|modificada|
|[cTKVS3tS8RBLruwP.htm](pathfinder-bestiary-items/cTKVS3tS8RBLruwP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cTy576jgZ9YTvVnf.htm](pathfinder-bestiary-items/cTy576jgZ9YTvVnf.htm)|Jaws|Fauces|modificada|
|[CtYKJciKGy9Wtza1.htm](pathfinder-bestiary-items/CtYKJciKGy9Wtza1.htm)|Aklys|Aklys|modificada|
|[Cu6kJlXyx18Zj7JW.htm](pathfinder-bestiary-items/Cu6kJlXyx18Zj7JW.htm)|Jaws|Fauces|modificada|
|[Cu6pedSvnaK0fqOF.htm](pathfinder-bestiary-items/Cu6pedSvnaK0fqOF.htm)|Demand|Exigencia|modificada|
|[CUwhBzgJAPCU9WcT.htm](pathfinder-bestiary-items/CUwhBzgJAPCU9WcT.htm)|Paralyzing Touch|Toque paralizante|modificada|
|[CUZdx9N2AmVtEjB4.htm](pathfinder-bestiary-items/CUZdx9N2AmVtEjB4.htm)|Sudden Strike|Golpe súbito|modificada|
|[CvfLXQsndA3wb5S1.htm](pathfinder-bestiary-items/CvfLXQsndA3wb5S1.htm)|Foot|Pie|modificada|
|[cwbkGiwxXBEcaKO5.htm](pathfinder-bestiary-items/cwbkGiwxXBEcaKO5.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[cxCbteeoXr6WayNl.htm](pathfinder-bestiary-items/cxCbteeoXr6WayNl.htm)|Claw|Garra|modificada|
|[CyCkk2GjnKUtz2Va.htm](pathfinder-bestiary-items/CyCkk2GjnKUtz2Va.htm)|Improved Grab|Agarrado mejorado|modificada|
|[cYDXg8exhcvpvPs8.htm](pathfinder-bestiary-items/cYDXg8exhcvpvPs8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[CyJqBVfPXX4Fp283.htm](pathfinder-bestiary-items/CyJqBVfPXX4Fp283.htm)|Gallop|Galope|modificada|
|[CyjUUa3DmRYWXY48.htm](pathfinder-bestiary-items/CyjUUa3DmRYWXY48.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cyykndggOJBnOeK7.htm](pathfinder-bestiary-items/cyykndggOJBnOeK7.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[cYzNZC3Tju4useHD.htm](pathfinder-bestiary-items/cYzNZC3Tju4useHD.htm)|Phantom Mount|Phantom Mount|modificada|
|[CZ7Y6ryNge5pbthf.htm](pathfinder-bestiary-items/CZ7Y6ryNge5pbthf.htm)|Claw|Garra|modificada|
|[cZc6wNhJDpipF2uK.htm](pathfinder-bestiary-items/cZc6wNhJDpipF2uK.htm)|Shield Block|Bloquear con escudo|modificada|
|[czqtIybNHs8mmEeg.htm](pathfinder-bestiary-items/czqtIybNHs8mmEeg.htm)|Create Spawn|Elaborar Spawn|modificada|
|[czVzxuwWmCRLgnuc.htm](pathfinder-bestiary-items/czVzxuwWmCRLgnuc.htm)|Forced Regeneration|Regeneración vigorosa|modificada|
|[czwGWW73rkPW7vBg.htm](pathfinder-bestiary-items/czwGWW73rkPW7vBg.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[D01DgsftPfTKC645.htm](pathfinder-bestiary-items/D01DgsftPfTKC645.htm)|Focus Beauty|Focus Belleza|modificada|
|[D0Bef8hMS0mQSDNY.htm](pathfinder-bestiary-items/D0Bef8hMS0mQSDNY.htm)|Tentacle|Tentáculo|modificada|
|[D0BJOT03V2FE7mKT.htm](pathfinder-bestiary-items/D0BJOT03V2FE7mKT.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[D0q0aNWo1X51cNe4.htm](pathfinder-bestiary-items/D0q0aNWo1X51cNe4.htm)|Annihilation Beams|Rayos de aniquilación|modificada|
|[d0rdYMltT1ZmbABn.htm](pathfinder-bestiary-items/d0rdYMltT1ZmbABn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[d15C0z784CMcZVi4.htm](pathfinder-bestiary-items/d15C0z784CMcZVi4.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[d1Cp4BUTiymkJs98.htm](pathfinder-bestiary-items/d1Cp4BUTiymkJs98.htm)|All-Around Vision|All-Around Vision|modificada|
|[d1U4RaKdh9zQHieX.htm](pathfinder-bestiary-items/d1U4RaKdh9zQHieX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[D1ZUrE1107HIW39r.htm](pathfinder-bestiary-items/D1ZUrE1107HIW39r.htm)|Thrash|Sacudirse|modificada|
|[d26CTzuFe7OR9K5L.htm](pathfinder-bestiary-items/d26CTzuFe7OR9K5L.htm)|Claw|Garra|modificada|
|[D2ezhwANZLcMNbz9.htm](pathfinder-bestiary-items/D2ezhwANZLcMNbz9.htm)|Death Frenzy|Frenesí Mortal|modificada|
|[D2tbPVYsNe8XmlsW.htm](pathfinder-bestiary-items/D2tbPVYsNe8XmlsW.htm)|Frill Defense|Cresta defensiva|modificada|
|[d3l2xwpTV1XkZBAO.htm](pathfinder-bestiary-items/d3l2xwpTV1XkZBAO.htm)|Spine Rake|Rastrillo de columna|modificada|
|[D4C1yHkCqpQckxgr.htm](pathfinder-bestiary-items/D4C1yHkCqpQckxgr.htm)|Tail Lash|Azote con la cola|modificada|
|[D4kBzUzbrcoM0VDr.htm](pathfinder-bestiary-items/D4kBzUzbrcoM0VDr.htm)|Jaws|Fauces|modificada|
|[D4XA5YtJ0G4E1and.htm](pathfinder-bestiary-items/D4XA5YtJ0G4E1and.htm)|Jaws|Fauces|modificada|
|[d574MMQ1MIrxdW3F.htm](pathfinder-bestiary-items/d574MMQ1MIrxdW3F.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[D5HDqkSWA0AZ7YAh.htm](pathfinder-bestiary-items/D5HDqkSWA0AZ7YAh.htm)|Constant Spells|Constant Spells|modificada|
|[D6yxsFtZFG5ysaek.htm](pathfinder-bestiary-items/D6yxsFtZFG5ysaek.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D71sSjwa6Oa7yHvw.htm](pathfinder-bestiary-items/D71sSjwa6Oa7yHvw.htm)|Grab|Agarrado|modificada|
|[D78seCDEZ9opnhed.htm](pathfinder-bestiary-items/D78seCDEZ9opnhed.htm)|Fangs|Colmillos|modificada|
|[D7nCoDXcwdCyO16R.htm](pathfinder-bestiary-items/D7nCoDXcwdCyO16R.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[d7roS2QZUtlhKfcO.htm](pathfinder-bestiary-items/d7roS2QZUtlhKfcO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[D89X40fEKxTsupQC.htm](pathfinder-bestiary-items/D89X40fEKxTsupQC.htm)|Ochre Acid|Ochre Acid|modificada|
|[d8HnQHeqQlzrKyFt.htm](pathfinder-bestiary-items/d8HnQHeqQlzrKyFt.htm)|Shove into Stone|Empujar dentro de la piedra|modificada|
|[d9nsBoLgYoGWORv2.htm](pathfinder-bestiary-items/d9nsBoLgYoGWORv2.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[daIbHqtjaWyJns1q.htm](pathfinder-bestiary-items/daIbHqtjaWyJns1q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dALR9L0wlW2puqiD.htm](pathfinder-bestiary-items/dALR9L0wlW2puqiD.htm)|Stench|Hedor|modificada|
|[dB4Pr3yiNKH32fE8.htm](pathfinder-bestiary-items/dB4Pr3yiNKH32fE8.htm)|Knockdown|Derribo|modificada|
|[DbfLZXVZVTQMJoyI.htm](pathfinder-bestiary-items/DbfLZXVZVTQMJoyI.htm)|Web Sense|Web Sense|modificada|
|[dBX2IpwGpcj3w0iE.htm](pathfinder-bestiary-items/dBX2IpwGpcj3w0iE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dCLTLCDtx7ax8PwY.htm](pathfinder-bestiary-items/dCLTLCDtx7ax8PwY.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[DcTsGXo4ifg7cNUg.htm](pathfinder-bestiary-items/DcTsGXo4ifg7cNUg.htm)|Jaws|Fauces|modificada|
|[dCuU1ER0aEC8sDuN.htm](pathfinder-bestiary-items/dCuU1ER0aEC8sDuN.htm)|Breath Weapon|Breath Weapon|modificada|
|[DCviUG6ackduxBeM.htm](pathfinder-bestiary-items/DCviUG6ackduxBeM.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[dcxAPtLWxZkmq1es.htm](pathfinder-bestiary-items/dcxAPtLWxZkmq1es.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[dD0COaIGpOldIcFD.htm](pathfinder-bestiary-items/dD0COaIGpOldIcFD.htm)|Shield Bash|Escudo Bash|modificada|
|[DD4lBYDa99ebm7Cb.htm](pathfinder-bestiary-items/DD4lBYDa99ebm7Cb.htm)|Rend|Rasgadura|modificada|
|[DD9mP7VmHRk4Lm97.htm](pathfinder-bestiary-items/DD9mP7VmHRk4Lm97.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[DDA6tlj5qsfP9Xb2.htm](pathfinder-bestiary-items/DDA6tlj5qsfP9Xb2.htm)|Constant Spells|Constant Spells|modificada|
|[dDIdPBqIyM1WM4kD.htm](pathfinder-bestiary-items/dDIdPBqIyM1WM4kD.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[dECHo3MtMAC4Dkh8.htm](pathfinder-bestiary-items/dECHo3MtMAC4Dkh8.htm)|Claw|Garra|modificada|
|[Dee07oE1Pj8EjAKU.htm](pathfinder-bestiary-items/Dee07oE1Pj8EjAKU.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[deEbLCLRy4mZDvLo.htm](pathfinder-bestiary-items/deEbLCLRy4mZDvLo.htm)|Breath Weapon|Breath Weapon|modificada|
|[deLQWsbCp3jKrpIv.htm](pathfinder-bestiary-items/deLQWsbCp3jKrpIv.htm)|Mauler|Vapulear|modificada|
|[deSS0MPuciBz6LLK.htm](pathfinder-bestiary-items/deSS0MPuciBz6LLK.htm)|Syringe|Jeringa|modificada|
|[dF7ibA1On3Nvjxqk.htm](pathfinder-bestiary-items/dF7ibA1On3Nvjxqk.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[DFaamiHPt04lXEiq.htm](pathfinder-bestiary-items/DFaamiHPt04lXEiq.htm)|Mucus Cloud|Nube de mucosa|modificada|
|[DFldqlT6ytrPqmmo.htm](pathfinder-bestiary-items/DFldqlT6ytrPqmmo.htm)|Bastard Sword|Espada Bastarda|modificada|
|[DfnmY7gUQzG365ax.htm](pathfinder-bestiary-items/DfnmY7gUQzG365ax.htm)|Web Wrappings|Envolturas telara|modificada|
|[DG1zbZJXQydDgeBF.htm](pathfinder-bestiary-items/DG1zbZJXQydDgeBF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[DG6zH2k37QwOHJ9R.htm](pathfinder-bestiary-items/DG6zH2k37QwOHJ9R.htm)|Web Burst|Descarga de telaras|modificada|
|[DGMkGaAm8sHK2QzD.htm](pathfinder-bestiary-items/DGMkGaAm8sHK2QzD.htm)|Beak|Beak|modificada|
|[dH7eKw8pDJQDkR3P.htm](pathfinder-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|Restringir|modificada|
|[DI6Nw0XKEZZJLY8D.htm](pathfinder-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[DiC6r2dHBydpjZtt.htm](pathfinder-bestiary-items/DiC6r2dHBydpjZtt.htm)|Breath Weapon|Breath Weapon|modificada|
|[DicxiMfpeviQRxSo.htm](pathfinder-bestiary-items/DicxiMfpeviQRxSo.htm)|Reel In|Arrastrar presa|modificada|
|[dId4UcnaI1F6ww0V.htm](pathfinder-bestiary-items/dId4UcnaI1F6ww0V.htm)|Frost Vial|Gélida de frasco de escarcha|modificada|
|[DiGgJNOY5Na3eYp8.htm](pathfinder-bestiary-items/DiGgJNOY5Na3eYp8.htm)|Filth Fever|Filth Fever|modificada|
|[dIKN1uas299xNK54.htm](pathfinder-bestiary-items/dIKN1uas299xNK54.htm)|Stinger|Aguijón|modificada|
|[DiXBVXgoU2aPsjbA.htm](pathfinder-bestiary-items/DiXBVXgoU2aPsjbA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DJ1EqBElcu3IaCwK.htm](pathfinder-bestiary-items/DJ1EqBElcu3IaCwK.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[dJE6KlD0Qh5yDU7w.htm](pathfinder-bestiary-items/dJE6KlD0Qh5yDU7w.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[DjE8LDaTqE6Y8m1b.htm](pathfinder-bestiary-items/DjE8LDaTqE6Y8m1b.htm)|Daemonic Pestilence|Pestilencia daimónico|modificada|
|[DjU3nUmp5OOFcVhl.htm](pathfinder-bestiary-items/DjU3nUmp5OOFcVhl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dJxa1u0JvHmiIXKG.htm](pathfinder-bestiary-items/dJxa1u0JvHmiIXKG.htm)|Sound Imitation|Imitación de sonidos|modificada|
|[dK5Meg25B2t4B34g.htm](pathfinder-bestiary-items/dK5Meg25B2t4B34g.htm)|Rejection Vulnerability|Vulnerabilidad al rechazo|modificada|
|[dkfqFFDoPeLbQKNp.htm](pathfinder-bestiary-items/dkfqFFDoPeLbQKNp.htm)|Construct Armor (Hardness 6)|Construir Armadura (Dureza 6)|modificada|
|[Dkveyw8m1JIyOZch.htm](pathfinder-bestiary-items/Dkveyw8m1JIyOZch.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DLFCqMiTmmTYgmtk.htm](pathfinder-bestiary-items/DLFCqMiTmmTYgmtk.htm)|Aklys|Aklys|modificada|
|[dlQJzPWPs48f9b4c.htm](pathfinder-bestiary-items/dlQJzPWPs48f9b4c.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[DlqsOMt7KvKvez2f.htm](pathfinder-bestiary-items/DlqsOMt7KvKvez2f.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[DM0bCE11AyauvvEH.htm](pathfinder-bestiary-items/DM0bCE11AyauvvEH.htm)|Constant Spells|Constant Spells|modificada|
|[DmNGJHLmXcMGdmUS.htm](pathfinder-bestiary-items/DmNGJHLmXcMGdmUS.htm)|Orc Knuckle Dagger|Daga Nudillo Orco|modificada|
|[dmZlzmRsiydMPknc.htm](pathfinder-bestiary-items/dmZlzmRsiydMPknc.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[DmZnAZGJF17LOU4y.htm](pathfinder-bestiary-items/DmZnAZGJF17LOU4y.htm)|Free Blade|Espada indomable|modificada|
|[Dn7B8j6FSbVD4U9x.htm](pathfinder-bestiary-items/Dn7B8j6FSbVD4U9x.htm)|Stench|Hedor|modificada|
|[dNxKxgTAaIDS3UWL.htm](pathfinder-bestiary-items/dNxKxgTAaIDS3UWL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[dOeNHCZ73nqH6mGc.htm](pathfinder-bestiary-items/dOeNHCZ73nqH6mGc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[doHZQiWCurTpCiMw.htm](pathfinder-bestiary-items/doHZQiWCurTpCiMw.htm)|Constant Spells|Constant Spells|modificada|
|[DOoR2SsbZF96IxlF.htm](pathfinder-bestiary-items/DOoR2SsbZF96IxlF.htm)|Coven Spells|Coven Spells|modificada|
|[DOr9FvQdsMfK9He0.htm](pathfinder-bestiary-items/DOr9FvQdsMfK9He0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DOWxg5PQXXUG8Ddy.htm](pathfinder-bestiary-items/DOWxg5PQXXUG8Ddy.htm)|Beak|Beak|modificada|
|[dp1n1eKrdXWBGI47.htm](pathfinder-bestiary-items/dp1n1eKrdXWBGI47.htm)|Jaws|Fauces|modificada|
|[DpI5QZQJMNjqef3w.htm](pathfinder-bestiary-items/DpI5QZQJMNjqef3w.htm)|Surprise Attacker|Atacante por sorpresa|modificada|
|[dpljRBjacJBvCCw1.htm](pathfinder-bestiary-items/dpljRBjacJBvCCw1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dPsbBLbGfNPn6v9L.htm](pathfinder-bestiary-items/dPsbBLbGfNPn6v9L.htm)|Breath Weapon|Breath Weapon|modificada|
|[DqJFXur9S0e0C54j.htm](pathfinder-bestiary-items/DqJFXur9S0e0C54j.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dRF3nPzB91HYIbXx.htm](pathfinder-bestiary-items/dRF3nPzB91HYIbXx.htm)|Ice Climb|Trepar por el hielo|modificada|
|[drMUmP8Z6LhZyhRI.htm](pathfinder-bestiary-items/drMUmP8Z6LhZyhRI.htm)|Carry Off Prey|Llevarse a la presa|modificada|
|[DRnLEIRSbgIT3V0F.htm](pathfinder-bestiary-items/DRnLEIRSbgIT3V0F.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[dRtVAtuxu9SD6zpN.htm](pathfinder-bestiary-items/dRtVAtuxu9SD6zpN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dsA6QTAsFP5Juo0N.htm](pathfinder-bestiary-items/dsA6QTAsFP5Juo0N.htm)|Jaws|Fauces|modificada|
|[dSMYHVMIltxeN4ms.htm](pathfinder-bestiary-items/dSMYHVMIltxeN4ms.htm)|Negative Healing|Curación negativa|modificada|
|[DsvyLX53TeuZHMQ3.htm](pathfinder-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|Pies con garras|modificada|
|[DtiXNG3ROuAaBhN3.htm](pathfinder-bestiary-items/DtiXNG3ROuAaBhN3.htm)|Constrict|Restringir|modificada|
|[DTXq4teaiLU9haNE.htm](pathfinder-bestiary-items/DTXq4teaiLU9haNE.htm)|Wide Swing|Barrido ancho|modificada|
|[DuOMgloNkM9gTAZj.htm](pathfinder-bestiary-items/DuOMgloNkM9gTAZj.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[dupLF5DYOhmcq8LG.htm](pathfinder-bestiary-items/dupLF5DYOhmcq8LG.htm)|Tail|Tail|modificada|
|[DV3sGNI2RVQcTbmS.htm](pathfinder-bestiary-items/DV3sGNI2RVQcTbmS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DVDscRqpim10UFQ4.htm](pathfinder-bestiary-items/DVDscRqpim10UFQ4.htm)|Constant Spells|Constant Spells|modificada|
|[dvenwngmH8FFaPNZ.htm](pathfinder-bestiary-items/dvenwngmH8FFaPNZ.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[dVGwhqUf0F3FAVqi.htm](pathfinder-bestiary-items/dVGwhqUf0F3FAVqi.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[DvjFG8xZGQmhN7AK.htm](pathfinder-bestiary-items/DvjFG8xZGQmhN7AK.htm)|Glutton's Rush|La Embestida del Glotón|modificada|
|[dvuK2slsg05eHdpZ.htm](pathfinder-bestiary-items/dvuK2slsg05eHdpZ.htm)|Fist|Puño|modificada|
|[dWgrFKYnx0FM8zl3.htm](pathfinder-bestiary-items/dWgrFKYnx0FM8zl3.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[DWRBHDiwImN0PCp9.htm](pathfinder-bestiary-items/DWRBHDiwImN0PCp9.htm)|Overwhelming Breath|Aliento abrumador|modificada|
|[dX7SlBGB2vdMkHMB.htm](pathfinder-bestiary-items/dX7SlBGB2vdMkHMB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[dXBqzoJaaPfabPMM.htm](pathfinder-bestiary-items/dXBqzoJaaPfabPMM.htm)|Constant Spells|Constant Spells|modificada|
|[DxjaCZWO3bSIqi7x.htm](pathfinder-bestiary-items/DxjaCZWO3bSIqi7x.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[DxJcguaM5VY5ipoX.htm](pathfinder-bestiary-items/DxJcguaM5VY5ipoX.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[dxLrvmfMtHkddYo4.htm](pathfinder-bestiary-items/dxLrvmfMtHkddYo4.htm)|Spirit Touch|Spirit Touch|modificada|
|[DXn63TBMmkKeN9Wu.htm](pathfinder-bestiary-items/DXn63TBMmkKeN9Wu.htm)|Coven|Coven|modificada|
|[DxRFNpdsIZg6cyAK.htm](pathfinder-bestiary-items/DxRFNpdsIZg6cyAK.htm)|Flash of Insight|Destello de perspicacia|modificada|
|[Dy5EMIJUvL46ibmR.htm](pathfinder-bestiary-items/Dy5EMIJUvL46ibmR.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[DyABhSgZNJ3ICo85.htm](pathfinder-bestiary-items/DyABhSgZNJ3ICo85.htm)|Spear|Lanza|modificada|
|[DYRACVN0h19oKZpk.htm](pathfinder-bestiary-items/DYRACVN0h19oKZpk.htm)|Radiance Dependence|Radiance Dependence|modificada|
|[dZJ67fQHX1jc4FRl.htm](pathfinder-bestiary-items/dZJ67fQHX1jc4FRl.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[DzJcNWKMqkWqtgAX.htm](pathfinder-bestiary-items/DzJcNWKMqkWqtgAX.htm)|Glaive|Glaive|modificada|
|[DZQzqTM8bO1vt4ps.htm](pathfinder-bestiary-items/DZQzqTM8bO1vt4ps.htm)|Extend Strands|Extend Strands|modificada|
|[dZrpK1Q6edVEhAZ7.htm](pathfinder-bestiary-items/dZrpK1Q6edVEhAZ7.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[DzWzbp1qMJwPTy0T.htm](pathfinder-bestiary-items/DzWzbp1qMJwPTy0T.htm)|Shield Block|Bloquear con escudo|modificada|
|[Dzxj1t6HVexa7Wyz.htm](pathfinder-bestiary-items/Dzxj1t6HVexa7Wyz.htm)|Plummet|Plummet|modificada|
|[DZXUlh01CHmN4S3z.htm](pathfinder-bestiary-items/DZXUlh01CHmN4S3z.htm)|Jaws|Fauces|modificada|
|[dZZfqVkrZJJMxi6A.htm](pathfinder-bestiary-items/dZZfqVkrZJJMxi6A.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[dzZj81bjKpvnxgLG.htm](pathfinder-bestiary-items/dzZj81bjKpvnxgLG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[e0fcFHdf91dsur91.htm](pathfinder-bestiary-items/e0fcFHdf91dsur91.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[E0hXra1OswZG4gQM.htm](pathfinder-bestiary-items/E0hXra1OswZG4gQM.htm)|Reactive Lash|Reactive Lash|modificada|
|[E11uRLNnDwbjSmiw.htm](pathfinder-bestiary-items/E11uRLNnDwbjSmiw.htm)|Change Shape|Change Shape|modificada|
|[e1GGSqrMS3yqPrVw.htm](pathfinder-bestiary-items/e1GGSqrMS3yqPrVw.htm)|Fiddle|Fiddle|modificada|
|[E36GUJSTq2NvkRmA.htm](pathfinder-bestiary-items/E36GUJSTq2NvkRmA.htm)|Confessor's Aura|Aura del confesor|modificada|
|[e3mEtYHIO4f0NZZc.htm](pathfinder-bestiary-items/e3mEtYHIO4f0NZZc.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[E3WrwhsH06axDfAu.htm](pathfinder-bestiary-items/E3WrwhsH06axDfAu.htm)|Improved Grab|Agarrado mejorado|modificada|
|[E5pqXYYcXl9kYesX.htm](pathfinder-bestiary-items/E5pqXYYcXl9kYesX.htm)|Tail|Tail|modificada|
|[e5zu7QFkUzeeeoVM.htm](pathfinder-bestiary-items/e5zu7QFkUzeeeoVM.htm)|Smoke Vision|Visión de Humo|modificada|
|[e6gQA5jXDMbCKyDj.htm](pathfinder-bestiary-items/e6gQA5jXDMbCKyDj.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[E6yHKgr49GpD3BXa.htm](pathfinder-bestiary-items/E6yHKgr49GpD3BXa.htm)|Abyssal Knowledge|Conocimiento abisal|modificada|
|[e7CxhtU5yoASkey8.htm](pathfinder-bestiary-items/e7CxhtU5yoASkey8.htm)|Swift Leap|Salto veloz|modificada|
|[e7Rk8yVGVqEP62yA.htm](pathfinder-bestiary-items/e7Rk8yVGVqEP62yA.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[e7t6qbZcSApHA7cS.htm](pathfinder-bestiary-items/e7t6qbZcSApHA7cS.htm)|Undetectable|No detectado/a|modificada|
|[e800cwjTJcAqZ2x0.htm](pathfinder-bestiary-items/e800cwjTJcAqZ2x0.htm)|Feed|Alimentarse|modificada|
|[e8BUlHLevzRJnHEc.htm](pathfinder-bestiary-items/e8BUlHLevzRJnHEc.htm)|Bushwhack|Emboscar|modificada|
|[e8oPQH0Ss9z5oimc.htm](pathfinder-bestiary-items/e8oPQH0Ss9z5oimc.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[E8SrLXAiFextgqNP.htm](pathfinder-bestiary-items/E8SrLXAiFextgqNP.htm)|Jaws|Fauces|modificada|
|[EaebstxdYxHFj1bR.htm](pathfinder-bestiary-items/EaebstxdYxHFj1bR.htm)|Jaws|Fauces|modificada|
|[EajnVKV3NKyICaQ8.htm](pathfinder-bestiary-items/EajnVKV3NKyICaQ8.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[Eaucw0lxqJ7eKezw.htm](pathfinder-bestiary-items/Eaucw0lxqJ7eKezw.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Eb5mdjpNuWacl147.htm](pathfinder-bestiary-items/Eb5mdjpNuWacl147.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[eBfnitbq2Wb56ZRG.htm](pathfinder-bestiary-items/eBfnitbq2Wb56ZRG.htm)|Flaming Strafe|Flamígera Strafeígera|modificada|
|[EbIYH4fdrCUr8wIS.htm](pathfinder-bestiary-items/EbIYH4fdrCUr8wIS.htm)|Force Body|Cuerpo de Fuerza|modificada|
|[ebr1KbgBZOCMt92B.htm](pathfinder-bestiary-items/ebr1KbgBZOCMt92B.htm)|Jaws|Fauces|modificada|
|[EbuMoQ5idyQJDEXD.htm](pathfinder-bestiary-items/EbuMoQ5idyQJDEXD.htm)|Paralyzing Gaze|Mirada paralizante|modificada|
|[EC797WDulwqw96ch.htm](pathfinder-bestiary-items/EC797WDulwqw96ch.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eczygEywMNK3dNd7.htm](pathfinder-bestiary-items/eczygEywMNK3dNd7.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[eDKxGQf3yPUpgqh0.htm](pathfinder-bestiary-items/eDKxGQf3yPUpgqh0.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[EdlfzUPi8lEnewu1.htm](pathfinder-bestiary-items/EdlfzUPi8lEnewu1.htm)|Dual Tusks|Dobles colmillos|modificada|
|[eDMgpSPjK69nCKMZ.htm](pathfinder-bestiary-items/eDMgpSPjK69nCKMZ.htm)|Wing|Ala|modificada|
|[Eejxi0k01WzbvUUh.htm](pathfinder-bestiary-items/Eejxi0k01WzbvUUh.htm)|Shield Block|Bloquear con escudo|modificada|
|[EEVvFBuUd4ji3NX1.htm](pathfinder-bestiary-items/EEVvFBuUd4ji3NX1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eEwJYKPQM7YTAFt4.htm](pathfinder-bestiary-items/eEwJYKPQM7YTAFt4.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Ef73Htwp9vIyrZWi.htm](pathfinder-bestiary-items/Ef73Htwp9vIyrZWi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[efQXyayOwi0nURrx.htm](pathfinder-bestiary-items/efQXyayOwi0nURrx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eFUBcFVqZ2zpFt8W.htm](pathfinder-bestiary-items/eFUBcFVqZ2zpFt8W.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[eG1Bf18XYFvkALTq.htm](pathfinder-bestiary-items/eG1Bf18XYFvkALTq.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[eg7TTGELwmYoMNM7.htm](pathfinder-bestiary-items/eg7TTGELwmYoMNM7.htm)|Seed|Semilla|modificada|
|[eGGqJI9VH7B5iZnC.htm](pathfinder-bestiary-items/eGGqJI9VH7B5iZnC.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[eGqp10EE3Ein6SqI.htm](pathfinder-bestiary-items/eGqp10EE3Ein6SqI.htm)|Tusk|Tusk|modificada|
|[egrcinyTFaF0OUfA.htm](pathfinder-bestiary-items/egrcinyTFaF0OUfA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[egV0y60X9Okkmuoc.htm](pathfinder-bestiary-items/egV0y60X9Okkmuoc.htm)|Hunting Spider Venom|Veneno de ara cazadora|modificada|
|[EhdD8hFrqyISTFtx.htm](pathfinder-bestiary-items/EhdD8hFrqyISTFtx.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[Ehvmz1GzuHGjHUz3.htm](pathfinder-bestiary-items/Ehvmz1GzuHGjHUz3.htm)|Tail|Tail|modificada|
|[EhZr8qLYudW24Syo.htm](pathfinder-bestiary-items/EhZr8qLYudW24Syo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eI1CqQ6uSb4iGiLS.htm](pathfinder-bestiary-items/eI1CqQ6uSb4iGiLS.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[ei3nQVCq6MHIPrMy.htm](pathfinder-bestiary-items/ei3nQVCq6MHIPrMy.htm)|Lurking Death|La muerte acecha|modificada|
|[EI3OMbX3GaC67Bnt.htm](pathfinder-bestiary-items/EI3OMbX3GaC67Bnt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eIFUAYDyIaHqOP6A.htm](pathfinder-bestiary-items/eIFUAYDyIaHqOP6A.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EIOiFljIAD1Zzdzr.htm](pathfinder-bestiary-items/EIOiFljIAD1Zzdzr.htm)|Spike Stones|Púas de piedra|modificada|
|[eIPX17zhmSnmtddW.htm](pathfinder-bestiary-items/eIPX17zhmSnmtddW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eIQH5yy2l8qe0bvR.htm](pathfinder-bestiary-items/eIQH5yy2l8qe0bvR.htm)|Constrict|Restringir|modificada|
|[EISoRK24Azg22k0m.htm](pathfinder-bestiary-items/EISoRK24Azg22k0m.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[Eizm8JTXmxGFPtFL.htm](pathfinder-bestiary-items/Eizm8JTXmxGFPtFL.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[Ej293LsxUUFjWNK8.htm](pathfinder-bestiary-items/Ej293LsxUUFjWNK8.htm)|Spear|Lanza|modificada|
|[EkSi5KM6hOWO5WxD.htm](pathfinder-bestiary-items/EkSi5KM6hOWO5WxD.htm)|Kraken Ink|Kraken Ink|modificada|
|[el4yrHrtMr3LobtV.htm](pathfinder-bestiary-items/el4yrHrtMr3LobtV.htm)|Gust|Gust|modificada|
|[El6yEwdcVoYxSpbv.htm](pathfinder-bestiary-items/El6yEwdcVoYxSpbv.htm)|Claw|Garra|modificada|
|[elZBS29nJWZadS4G.htm](pathfinder-bestiary-items/elZBS29nJWZadS4G.htm)|Wing Deflection|Desvío con el ala|modificada|
|[EmEYyJxT9UcKh4lK.htm](pathfinder-bestiary-items/EmEYyJxT9UcKh4lK.htm)|Jaws|Fauces|modificada|
|[emPcqeSFXdyr6CgV.htm](pathfinder-bestiary-items/emPcqeSFXdyr6CgV.htm)|Terrifying Charge|Carga terrorífica|modificada|
|[Emupr9krjKNn5xjb.htm](pathfinder-bestiary-items/Emupr9krjKNn5xjb.htm)|Mauler|Vapulear|modificada|
|[EMWzNZneWpK3nLER.htm](pathfinder-bestiary-items/EMWzNZneWpK3nLER.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[EMzbdikMCvVBaXbA.htm](pathfinder-bestiary-items/EMzbdikMCvVBaXbA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[EnBK4J1yRnqIdTCA.htm](pathfinder-bestiary-items/EnBK4J1yRnqIdTCA.htm)|Battle Axe|Hacha de batalla|modificada|
|[endjF5MDlvADZc05.htm](pathfinder-bestiary-items/endjF5MDlvADZc05.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Eng6Mv69WWeKb4Wq.htm](pathfinder-bestiary-items/Eng6Mv69WWeKb4Wq.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[enj0s2MOCB4NTllp.htm](pathfinder-bestiary-items/enj0s2MOCB4NTllp.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[eNTumcCOMnTdNzht.htm](pathfinder-bestiary-items/eNTumcCOMnTdNzht.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[eOgrqVcnppzRiPms.htm](pathfinder-bestiary-items/eOgrqVcnppzRiPms.htm)|Push 10 feet|Empuje 10 pies|modificada|
|[eoHwXTgs9Au82i4J.htm](pathfinder-bestiary-items/eoHwXTgs9Au82i4J.htm)|Nightmare Guardian|Guardián de pesadillas|modificada|
|[eOj1lBtBYURSS36K.htm](pathfinder-bestiary-items/eOj1lBtBYURSS36K.htm)|Glimpse of Redemption|Atisbo de redención|modificada|
|[eOkkPMnXhWSlJr5h.htm](pathfinder-bestiary-items/eOkkPMnXhWSlJr5h.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ePRrl3MsWtcXXzD7.htm](pathfinder-bestiary-items/ePRrl3MsWtcXXzD7.htm)|Terrifying Touch|Toque terrorífico|modificada|
|[epscB9eQsqVToqtK.htm](pathfinder-bestiary-items/epscB9eQsqVToqtK.htm)|Rock Or Metal Debris|Escombros de roca o metal|modificada|
|[eqCGFR9UmHOuUpMI.htm](pathfinder-bestiary-items/eqCGFR9UmHOuUpMI.htm)|Greataxe|Greataxe|modificada|
|[eqiwUtaFLOTC1oND.htm](pathfinder-bestiary-items/eqiwUtaFLOTC1oND.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[eqROgg0bgVWksPcZ.htm](pathfinder-bestiary-items/eqROgg0bgVWksPcZ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[EQs2G0f4rbGphFRv.htm](pathfinder-bestiary-items/EQs2G0f4rbGphFRv.htm)|Banishing Swipe|Vaivén de destierro|modificada|
|[ERaLBCCkqUJWJVvG.htm](pathfinder-bestiary-items/ERaLBCCkqUJWJVvG.htm)|Buck|Encabritarse|modificada|
|[erc12GafQUSAqN7h.htm](pathfinder-bestiary-items/erc12GafQUSAqN7h.htm)|Pummel|Pummel|modificada|
|[ERGEti8Jc1RonWRj.htm](pathfinder-bestiary-items/ERGEti8Jc1RonWRj.htm)|Change Shape|Change Shape|modificada|
|[ErhX9xAIKos8xjgd.htm](pathfinder-bestiary-items/ErhX9xAIKos8xjgd.htm)|Trident|Trident|modificada|
|[ErNsSmjjx1k38FuX.htm](pathfinder-bestiary-items/ErNsSmjjx1k38FuX.htm)|Regeneration 10 (Deactivated by Cold Iron)|Regeneración 10 (Desactivado por Cold Iron)|modificada|
|[erqQJ4VimGjwkftc.htm](pathfinder-bestiary-items/erqQJ4VimGjwkftc.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[esaeLxI8stHVxilE.htm](pathfinder-bestiary-items/esaeLxI8stHVxilE.htm)|Twisting Tail|Enredar con la cola|modificada|
|[escewXwusyFQw1t9.htm](pathfinder-bestiary-items/escewXwusyFQw1t9.htm)|Breath Weapon|Breath Weapon|modificada|
|[ESgOv0J4fLFsukAx.htm](pathfinder-bestiary-items/ESgOv0J4fLFsukAx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[esNHCG2RP8SOfTuA.htm](pathfinder-bestiary-items/esNHCG2RP8SOfTuA.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[ESSGqodO4c5dPMOG.htm](pathfinder-bestiary-items/ESSGqodO4c5dPMOG.htm)|Jaws|Fauces|modificada|
|[ESyhQLjcnQvrHNwr.htm](pathfinder-bestiary-items/ESyhQLjcnQvrHNwr.htm)|Tail|Tail|modificada|
|[ETwwR7V1iIbvqCuB.htm](pathfinder-bestiary-items/ETwwR7V1iIbvqCuB.htm)|Claw|Garra|modificada|
|[Eu72dpTXiYJQD7Hm.htm](pathfinder-bestiary-items/Eu72dpTXiYJQD7Hm.htm)|Web Tether|Telara|modificada|
|[EUf1gqfe5un9Aprz.htm](pathfinder-bestiary-items/EUf1gqfe5un9Aprz.htm)|Vine|Vid|modificada|
|[eUjlMs7XKcwlCfZQ.htm](pathfinder-bestiary-items/eUjlMs7XKcwlCfZQ.htm)|Claw|Garra|modificada|
|[eulyI60JHNUYs39w.htm](pathfinder-bestiary-items/eulyI60JHNUYs39w.htm)|Slow|Lentificado/a|modificada|
|[eumI77vCN35YAXRJ.htm](pathfinder-bestiary-items/eumI77vCN35YAXRJ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[euRdPTOZHeLTJZHA.htm](pathfinder-bestiary-items/euRdPTOZHeLTJZHA.htm)|Swiftness|Celeridad|modificada|
|[eUtG7jyrUZUQhm6G.htm](pathfinder-bestiary-items/eUtG7jyrUZUQhm6G.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[eWc160opF23yBpjF.htm](pathfinder-bestiary-items/eWc160opF23yBpjF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eWlvpYGryxxhSJkS.htm](pathfinder-bestiary-items/eWlvpYGryxxhSJkS.htm)|Infernal Investment|Inversión infernal|modificada|
|[EwP4uiI1GXQ7aYst.htm](pathfinder-bestiary-items/EwP4uiI1GXQ7aYst.htm)|Thoughtlance|Lanza mental|modificada|
|[EWqlu1saEnvsN9kN.htm](pathfinder-bestiary-items/EWqlu1saEnvsN9kN.htm)|Fist|Puño|modificada|
|[EwYIwG40atiWUwhI.htm](pathfinder-bestiary-items/EwYIwG40atiWUwhI.htm)|Breath Weapon|Breath Weapon|modificada|
|[ex0dTLedYlwxNB3T.htm](pathfinder-bestiary-items/ex0dTLedYlwxNB3T.htm)|Swallow Whole|Engullir Todo|modificada|
|[ExqWcZmrd3RhXgLt.htm](pathfinder-bestiary-items/ExqWcZmrd3RhXgLt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[exYCtLHRcdeuOCdp.htm](pathfinder-bestiary-items/exYCtLHRcdeuOCdp.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[eYD6EiiIYvny4Vq2.htm](pathfinder-bestiary-items/eYD6EiiIYvny4Vq2.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[EydNDPNpUXcKp0PD.htm](pathfinder-bestiary-items/EydNDPNpUXcKp0PD.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[EyTNAb0HgeYGoaPL.htm](pathfinder-bestiary-items/EyTNAb0HgeYGoaPL.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[EzSKWtWM2tSTtZwA.htm](pathfinder-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|Garra|modificada|
|[f024HTNWMSNizvqn.htm](pathfinder-bestiary-items/f024HTNWMSNizvqn.htm)|Jaws|Fauces|modificada|
|[f0JUzUCQYwMZkmpA.htm](pathfinder-bestiary-items/f0JUzUCQYwMZkmpA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[F0YALhW0VHUpxUtT.htm](pathfinder-bestiary-items/F0YALhW0VHUpxUtT.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[F2mCARScyPh6sD0k.htm](pathfinder-bestiary-items/F2mCARScyPh6sD0k.htm)|Smoke Vision|Visión de Humo|modificada|
|[f3DzwJJdLBEbrQYp.htm](pathfinder-bestiary-items/f3DzwJJdLBEbrQYp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[f4rcDncEwu2OdC1a.htm](pathfinder-bestiary-items/f4rcDncEwu2OdC1a.htm)|Jaws|Fauces|modificada|
|[f4yHe8ZeeX1UHyWc.htm](pathfinder-bestiary-items/f4yHe8ZeeX1UHyWc.htm)|Javelin|Javelin|modificada|
|[f52Whk1lgUmI3JmT.htm](pathfinder-bestiary-items/f52Whk1lgUmI3JmT.htm)|High Winds|Fuertes vientos|modificada|
|[F8wXzsUNliJVU8bv.htm](pathfinder-bestiary-items/F8wXzsUNliJVU8bv.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[F8xpT7h5a5ZviXU5.htm](pathfinder-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[Fa3Nn9PdJzbO1Fwg.htm](pathfinder-bestiary-items/Fa3Nn9PdJzbO1Fwg.htm)|Dart|Dardo|modificada|
|[FaGxK1ootpG6uFmP.htm](pathfinder-bestiary-items/FaGxK1ootpG6uFmP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Fb73mys1NUmxKq5Q.htm](pathfinder-bestiary-items/Fb73mys1NUmxKq5Q.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[fb8hCFDmwVrYAiRL.htm](pathfinder-bestiary-items/fb8hCFDmwVrYAiRL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Fc10tuSKXi4OAWw6.htm](pathfinder-bestiary-items/Fc10tuSKXi4OAWw6.htm)|Deep Plunge|Zambullida profunda|modificada|
|[fchD9qieGM9L8Vag.htm](pathfinder-bestiary-items/fchD9qieGM9L8Vag.htm)|Breath Weapon|Breath Weapon|modificada|
|[fCL0YMWwrX0efcpJ.htm](pathfinder-bestiary-items/fCL0YMWwrX0efcpJ.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[fctwwJ6S2jeNgFwD.htm](pathfinder-bestiary-items/fctwwJ6S2jeNgFwD.htm)|Constant Spells|Constant Spells|modificada|
|[FCz6EfXJsvdhlDq5.htm](pathfinder-bestiary-items/FCz6EfXJsvdhlDq5.htm)|Tail|Tail|modificada|
|[fD21lHfQwFLm6D9N.htm](pathfinder-bestiary-items/fD21lHfQwFLm6D9N.htm)|Claw|Garra|modificada|
|[fd6LoAjku28Cm4oo.htm](pathfinder-bestiary-items/fd6LoAjku28Cm4oo.htm)|Kukri|Kukri|modificada|
|[FdJtaUOMkZdu6F7C.htm](pathfinder-bestiary-items/FdJtaUOMkZdu6F7C.htm)|Fangs|Colmillos|modificada|
|[fDmw9feXNMKuAci4.htm](pathfinder-bestiary-items/fDmw9feXNMKuAci4.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[FdQL1f8wOuuy69Tz.htm](pathfinder-bestiary-items/FdQL1f8wOuuy69Tz.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[FDVxe2jOvdrTKmQY.htm](pathfinder-bestiary-items/FDVxe2jOvdrTKmQY.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[FE2mBRuPXC4LPn2b.htm](pathfinder-bestiary-items/FE2mBRuPXC4LPn2b.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[fEbZopQgOxVNZDnI.htm](pathfinder-bestiary-items/fEbZopQgOxVNZDnI.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[fegp7nmVFKZeTSA5.htm](pathfinder-bestiary-items/fegp7nmVFKZeTSA5.htm)|Piercing Hymn|Piercing Hymn|modificada|
|[FEiMSx6vAjQ1oKaY.htm](pathfinder-bestiary-items/FEiMSx6vAjQ1oKaY.htm)|Swallow Whole|Engullir Todo|modificada|
|[Ff5wpbYD7hi6qmph.htm](pathfinder-bestiary-items/Ff5wpbYD7hi6qmph.htm)|Constant Spells|Constant Spells|modificada|
|[FFjYHtFi991MRIlJ.htm](pathfinder-bestiary-items/FFjYHtFi991MRIlJ.htm)|Fire Mote|Mota de Fuego|modificada|
|[ffZLFXRhI2leU0qq.htm](pathfinder-bestiary-items/ffZLFXRhI2leU0qq.htm)|Push 5 feet|Empujar 5 pies|modificada|
|[fG5TPYbHYwwOdPbD.htm](pathfinder-bestiary-items/fG5TPYbHYwwOdPbD.htm)|Claw|Garra|modificada|
|[FGDntcY8lQY0p68N.htm](pathfinder-bestiary-items/FGDntcY8lQY0p68N.htm)|Guisarme|Guisarme|modificada|
|[FGm1xbUPgE0BRiSf.htm](pathfinder-bestiary-items/FGm1xbUPgE0BRiSf.htm)|Focused Assault|Asalto concentrado|modificada|
|[fgRAIdUznaID2VqA.htm](pathfinder-bestiary-items/fgRAIdUznaID2VqA.htm)|Smoke Vision|Visión de Humo|modificada|
|[fHam88iT0r3U5tcl.htm](pathfinder-bestiary-items/fHam88iT0r3U5tcl.htm)|Tinker|Trastear|modificada|
|[fhjQVAdn1BOQnXvF.htm](pathfinder-bestiary-items/fhjQVAdn1BOQnXvF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FhLr7TEb7n9FRT5I.htm](pathfinder-bestiary-items/FhLr7TEb7n9FRT5I.htm)|Pack Attack|Ataque en manada|modificada|
|[FHuTUU6djcJ06GPe.htm](pathfinder-bestiary-items/FHuTUU6djcJ06GPe.htm)|Foot|Pie|modificada|
|[FHYZijWKqxyPWq4b.htm](pathfinder-bestiary-items/FHYZijWKqxyPWq4b.htm)|Construct Armor (Hardness 10)|Construir Armadura (Dureza 10)|modificada|
|[fI8bsqTQlNTlSpdF.htm](pathfinder-bestiary-items/fI8bsqTQlNTlSpdF.htm)|Greatclub|Greatclub|modificada|
|[fifv91vxtwlIpzXx.htm](pathfinder-bestiary-items/fifv91vxtwlIpzXx.htm)|Fast Swallow|Tragar de golpe|modificada|
|[fIssfjOuDo0yfwLc.htm](pathfinder-bestiary-items/fIssfjOuDo0yfwLc.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[FJrLVkHRXOOEh4xr.htm](pathfinder-bestiary-items/FJrLVkHRXOOEh4xr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fjsDRLGq7Hpbxrfn.htm](pathfinder-bestiary-items/fjsDRLGq7Hpbxrfn.htm)|Bear Empathy|Empatía del Oso|modificada|
|[fK43gOGpvZ5SUgT8.htm](pathfinder-bestiary-items/fK43gOGpvZ5SUgT8.htm)|Push 10 feet|Empuje 10 pies|modificada|
|[FKd3nk9u6PpHnAtX.htm](pathfinder-bestiary-items/FKd3nk9u6PpHnAtX.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[FkFTivkiFFdzjwbO.htm](pathfinder-bestiary-items/FkFTivkiFFdzjwbO.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[fKjSxZ4xLl3lTtl0.htm](pathfinder-bestiary-items/fKjSxZ4xLl3lTtl0.htm)|Grab|Agarrado|modificada|
|[fkSqSiW3qLpaq6hp.htm](pathfinder-bestiary-items/fkSqSiW3qLpaq6hp.htm)|Body|Cuerpo|modificada|
|[fLIOuEGoDxmXYqNf.htm](pathfinder-bestiary-items/fLIOuEGoDxmXYqNf.htm)|Snack|Snack|modificada|
|[FLKA2EOS4qdy02od.htm](pathfinder-bestiary-items/FLKA2EOS4qdy02od.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[FLxvnmMACdVSSD13.htm](pathfinder-bestiary-items/FLxvnmMACdVSSD13.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[fLzaGzA3jMdzslmt.htm](pathfinder-bestiary-items/fLzaGzA3jMdzslmt.htm)|Metal Scent 30 feet|Metal Scent 30 pies|modificada|
|[fm9psBEIByOlaBKs.htm](pathfinder-bestiary-items/fm9psBEIByOlaBKs.htm)|Shape Ice|Moldear hielo|modificada|
|[FMmsMdd150OLekUM.htm](pathfinder-bestiary-items/FMmsMdd150OLekUM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fMWLAq5jGWQRA55Z.htm](pathfinder-bestiary-items/fMWLAq5jGWQRA55Z.htm)|Claw|Garra|modificada|
|[Fn227VCdI0YQvtkg.htm](pathfinder-bestiary-items/Fn227VCdI0YQvtkg.htm)|Frightful Presence|Frightful Presence|modificada|
|[FOakiHyYV4zfX0lj.htm](pathfinder-bestiary-items/FOakiHyYV4zfX0lj.htm)|Smoke|Humo|modificada|
|[fOaKOMONKFNcP7B5.htm](pathfinder-bestiary-items/fOaKOMONKFNcP7B5.htm)|Claw|Garra|modificada|
|[FoGyFrPjwdSydW4U.htm](pathfinder-bestiary-items/FoGyFrPjwdSydW4U.htm)|Leaping Charge|Carga en salto|modificada|
|[fOnFVdptokDlFcTW.htm](pathfinder-bestiary-items/fOnFVdptokDlFcTW.htm)|Whirlwind of Hooks|Torbellino de garfios|modificada|
|[fOXEVyfWiHQRSyiC.htm](pathfinder-bestiary-items/fOXEVyfWiHQRSyiC.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[fP1MPx5mw7OZ0cma.htm](pathfinder-bestiary-items/fP1MPx5mw7OZ0cma.htm)|Slime|Slime|modificada|
|[fP5Iv5QbWHvLChA4.htm](pathfinder-bestiary-items/fP5Iv5QbWHvLChA4.htm)|Infused Items|Equipos infundidos|modificada|
|[Fpalcgxc6alPgOA4.htm](pathfinder-bestiary-items/Fpalcgxc6alPgOA4.htm)|Scent (Imprecise) 80 feet|Olor (Impreciso) 80 pies|modificada|
|[FPjjWa8f7H8lZRFS.htm](pathfinder-bestiary-items/FPjjWa8f7H8lZRFS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fQ0vMmyTwWHXQy7V.htm](pathfinder-bestiary-items/fQ0vMmyTwWHXQy7V.htm)|Horns|Cuernos|modificada|
|[fQiZcSYvDH1xIFor.htm](pathfinder-bestiary-items/fQiZcSYvDH1xIFor.htm)|Jaws|Fauces|modificada|
|[fQv7cRqi12UCfasq.htm](pathfinder-bestiary-items/fQv7cRqi12UCfasq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fqwCiWMkfhbYojJI.htm](pathfinder-bestiary-items/fqwCiWMkfhbYojJI.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[fRDoPX53siUmNijE.htm](pathfinder-bestiary-items/fRDoPX53siUmNijE.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[FrDW4cm9w2zRRONj.htm](pathfinder-bestiary-items/FrDW4cm9w2zRRONj.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[FReUvGRM1Bo8CeGZ.htm](pathfinder-bestiary-items/FReUvGRM1Bo8CeGZ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[freYXZtmDhD5zIrA.htm](pathfinder-bestiary-items/freYXZtmDhD5zIrA.htm)|Javelin|Javelin|modificada|
|[fRkzUI9I50wuPJvX.htm](pathfinder-bestiary-items/fRkzUI9I50wuPJvX.htm)|Skittering Assault|Asalto correteante|modificada|
|[FrlYBDNV4RyfqLaY.htm](pathfinder-bestiary-items/FrlYBDNV4RyfqLaY.htm)|Jaws|Fauces|modificada|
|[FrOech6HlG52AZBy.htm](pathfinder-bestiary-items/FrOech6HlG52AZBy.htm)|Jaws|Fauces|modificada|
|[FRSknzZLlvXTktV6.htm](pathfinder-bestiary-items/FRSknzZLlvXTktV6.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[fs49A2QDgfg74MTh.htm](pathfinder-bestiary-items/fs49A2QDgfg74MTh.htm)|Independent Brains|Cerebros independientes|modificada|
|[FsFg8KtMHYPQyyIC.htm](pathfinder-bestiary-items/FsFg8KtMHYPQyyIC.htm)|Fire Healing|Curación por fuego|modificada|
|[FSk7wzzH1ToTF3ie.htm](pathfinder-bestiary-items/FSk7wzzH1ToTF3ie.htm)|Grab|Agarrado|modificada|
|[fSUvadSPPhS9kOAB.htm](pathfinder-bestiary-items/fSUvadSPPhS9kOAB.htm)|Greater Constrict|Mayor Restricción|modificada|
|[FTA0YvX69f46caFj.htm](pathfinder-bestiary-items/FTA0YvX69f46caFj.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[ftbiIlDYazAiyESd.htm](pathfinder-bestiary-items/ftbiIlDYazAiyESd.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[fTBOn2wHEnOkR4t9.htm](pathfinder-bestiary-items/fTBOn2wHEnOkR4t9.htm)|Scoff at the Divine|Burlarse de lo divino.|modificada|
|[FtCodDvKYE5srIOI.htm](pathfinder-bestiary-items/FtCodDvKYE5srIOI.htm)|Foot|Pie|modificada|
|[fTIWo8M9tHghfIgU.htm](pathfinder-bestiary-items/fTIWo8M9tHghfIgU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FTMgXssm5HK4Io0O.htm](pathfinder-bestiary-items/FTMgXssm5HK4Io0O.htm)|Vortex|Vortex|modificada|
|[fTPjypXzhscP42dn.htm](pathfinder-bestiary-items/fTPjypXzhscP42dn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FtQATmD9TKhRQwoZ.htm](pathfinder-bestiary-items/FtQATmD9TKhRQwoZ.htm)|Constant Spells|Constant Spells|modificada|
|[FuDdgAP7wwq6LCFO.htm](pathfinder-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Serpent Venom|Serpent Venom|modificada|
|[FUgfYGhsCxf8KU54.htm](pathfinder-bestiary-items/FUgfYGhsCxf8KU54.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[FUIS7buU2stLWVpK.htm](pathfinder-bestiary-items/FUIS7buU2stLWVpK.htm)|Claws|Garras|modificada|
|[Fuv2PWZEnIbyJ99B.htm](pathfinder-bestiary-items/Fuv2PWZEnIbyJ99B.htm)|Take Them Down!|¬°¡Abatidlos!|modificada|
|[FUwL7sL45SGQB0N6.htm](pathfinder-bestiary-items/FUwL7sL45SGQB0N6.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[fUzY6OZeD7f6IHE4.htm](pathfinder-bestiary-items/fUzY6OZeD7f6IHE4.htm)|Elemental Endurance|Aguante elemental|modificada|
|[fvIjW0iSvoVCNnVG.htm](pathfinder-bestiary-items/fvIjW0iSvoVCNnVG.htm)|Disturbing Vision|Disturbing Vision|modificada|
|[FVn9vebx0n5CJW76.htm](pathfinder-bestiary-items/FVn9vebx0n5CJW76.htm)|Tail|Tail|modificada|
|[fvSVZqV8GEHEzMVu.htm](pathfinder-bestiary-items/fvSVZqV8GEHEzMVu.htm)|Spore Blight|Tizón de esporas|modificada|
|[fWTtIUDtbSfOzgqv.htm](pathfinder-bestiary-items/fWTtIUDtbSfOzgqv.htm)|General's Cry|Grito del general|modificada|
|[FX1GZJoTSgtAlICB.htm](pathfinder-bestiary-items/FX1GZJoTSgtAlICB.htm)|Arm|Brazo|modificada|
|[fX3ZS6kW7CqzkJuu.htm](pathfinder-bestiary-items/fX3ZS6kW7CqzkJuu.htm)|Drag|Arrastre|modificada|
|[fxVaypsTZKjHprzh.htm](pathfinder-bestiary-items/fxVaypsTZKjHprzh.htm)|Change Shape|Change Shape|modificada|
|[fxy74wWSDj5riJSB.htm](pathfinder-bestiary-items/fxy74wWSDj5riJSB.htm)|Constant Spells|Constant Spells|modificada|
|[fYKBzNcZosPojEyp.htm](pathfinder-bestiary-items/fYKBzNcZosPojEyp.htm)|Desert Thirst|Sed del desierto|modificada|
|[FyUL1Dfsy8YJIJH4.htm](pathfinder-bestiary-items/FyUL1Dfsy8YJIJH4.htm)|Sticky Strand|Sticky Strand|modificada|
|[Fz4qoI14He66tmpf.htm](pathfinder-bestiary-items/Fz4qoI14He66tmpf.htm)|Commander's Aura|Aura de comandante|modificada|
|[Fzou6YacIfdWk8Rx.htm](pathfinder-bestiary-items/Fzou6YacIfdWk8Rx.htm)|Windsense 240 feet|Sentido de viento 240 pies|modificada|
|[fZTQMVZbBOPoJ37I.htm](pathfinder-bestiary-items/fZTQMVZbBOPoJ37I.htm)|Constrict|Restringir|modificada|
|[fZx0EfFDzx8aK7tG.htm](pathfinder-bestiary-items/fZx0EfFDzx8aK7tG.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[g1KH4siW7BJpYTfM.htm](pathfinder-bestiary-items/g1KH4siW7BJpYTfM.htm)|Claw|Garra|modificada|
|[g2irWtwE4YUnllGP.htm](pathfinder-bestiary-items/g2irWtwE4YUnllGP.htm)|Jaws|Fauces|modificada|
|[g2OkSiICf1gw8aes.htm](pathfinder-bestiary-items/g2OkSiICf1gw8aes.htm)|Quicken|Quicken|modificada|
|[g2qQpMZqRaylNASw.htm](pathfinder-bestiary-items/g2qQpMZqRaylNASw.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[G3Hz9uWIYCEyoBRq.htm](pathfinder-bestiary-items/G3Hz9uWIYCEyoBRq.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[g4bJ3hTpL9J2uQKp.htm](pathfinder-bestiary-items/g4bJ3hTpL9J2uQKp.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[G4HjijhXg27QNTqn.htm](pathfinder-bestiary-items/G4HjijhXg27QNTqn.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[g51BzhmOH3vHLWup.htm](pathfinder-bestiary-items/g51BzhmOH3vHLWup.htm)|Wrestle|Placar|modificada|
|[g5dltca5pjhRzJMP.htm](pathfinder-bestiary-items/g5dltca5pjhRzJMP.htm)|Twisting Tail|Enredar con la cola|modificada|
|[G5jsqgUC6MzdXURo.htm](pathfinder-bestiary-items/G5jsqgUC6MzdXURo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[g6xJoBw3WBMO4VqW.htm](pathfinder-bestiary-items/g6xJoBw3WBMO4VqW.htm)|Claw|Garra|modificada|
|[G7hedsdXlwV3pW3J.htm](pathfinder-bestiary-items/G7hedsdXlwV3pW3J.htm)|Darkvision|Visión en la oscuridad|modificada|
|[g7nl0QahmH5JaunK.htm](pathfinder-bestiary-items/g7nl0QahmH5JaunK.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[g832bFUMFhXs1T6E.htm](pathfinder-bestiary-items/g832bFUMFhXs1T6E.htm)|Push|Push|modificada|
|[G9f4Fx5llYjESmPt.htm](pathfinder-bestiary-items/G9f4Fx5llYjESmPt.htm)|Grab|Agarrado|modificada|
|[GA9tSWJmy7KFgUjB.htm](pathfinder-bestiary-items/GA9tSWJmy7KFgUjB.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[gakDYFEsPKyFUdIg.htm](pathfinder-bestiary-items/gakDYFEsPKyFUdIg.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[GB4t0y9suPOQZ9o5.htm](pathfinder-bestiary-items/GB4t0y9suPOQZ9o5.htm)|Fangs|Colmillos|modificada|
|[GBiarRqqq8qwyZtJ.htm](pathfinder-bestiary-items/GBiarRqqq8qwyZtJ.htm)|Paralysis|Parálisis|modificada|
|[GBuKg3VtBcivhUos.htm](pathfinder-bestiary-items/GBuKg3VtBcivhUos.htm)|Improved Grab|Agarrado mejorado|modificada|
|[gC0TpsGsMQ5RDDau.htm](pathfinder-bestiary-items/gC0TpsGsMQ5RDDau.htm)|Swallow Whole|Engullir Todo|modificada|
|[GcJJKFDfrv0jW0K3.htm](pathfinder-bestiary-items/GcJJKFDfrv0jW0K3.htm)|Freezing Mist Breath|Aliento gélido de niebla|modificada|
|[gCm9tVCYX0lSCKOq.htm](pathfinder-bestiary-items/gCm9tVCYX0lSCKOq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gCMdnmRVTS96OQxR.htm](pathfinder-bestiary-items/gCMdnmRVTS96OQxR.htm)|Web|Telara|modificada|
|[gCQ2k3U8EhFrjpCO.htm](pathfinder-bestiary-items/gCQ2k3U8EhFrjpCO.htm)|Wing Deflection|Desvío con el ala|modificada|
|[GCyLSeOtTMt3wLur.htm](pathfinder-bestiary-items/GCyLSeOtTMt3wLur.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[gD3kGPuZGxBNXQTG.htm](pathfinder-bestiary-items/gD3kGPuZGxBNXQTG.htm)|Longsword|Longsword|modificada|
|[gdGHPerbX8DzQ8D5.htm](pathfinder-bestiary-items/gdGHPerbX8DzQ8D5.htm)|Negative Healing|Curación negativa|modificada|
|[GDRN1E60FSxKpOv0.htm](pathfinder-bestiary-items/GDRN1E60FSxKpOv0.htm)|Mandibles|Mandíbulas|modificada|
|[gdUSuIptZvtLZ7ts.htm](pathfinder-bestiary-items/gdUSuIptZvtLZ7ts.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GdWAi69mvxSgSJOn.htm](pathfinder-bestiary-items/GdWAi69mvxSgSJOn.htm)|Virtue Aversion|Aversión a la virtud|modificada|
|[GE9sBpIRZwKGL1HJ.htm](pathfinder-bestiary-items/GE9sBpIRZwKGL1HJ.htm)|Beak|Beak|modificada|
|[GeAJNEdtRdXSvZvk.htm](pathfinder-bestiary-items/GeAJNEdtRdXSvZvk.htm)|Breath Weapon|Breath Weapon|modificada|
|[GEvdLrMrI8fbxU9C.htm](pathfinder-bestiary-items/GEvdLrMrI8fbxU9C.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[GF7VbTGWzxeNZeOf.htm](pathfinder-bestiary-items/GF7VbTGWzxeNZeOf.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Gfs6EH6FQLkgQKGz.htm](pathfinder-bestiary-items/Gfs6EH6FQLkgQKGz.htm)|Drench|Sofocar|modificada|
|[GGJBUd4JmwxnZtYj.htm](pathfinder-bestiary-items/GGJBUd4JmwxnZtYj.htm)|Pincer|Pinza|modificada|
|[GGPFGv65lu6xKmVn.htm](pathfinder-bestiary-items/GGPFGv65lu6xKmVn.htm)|Greatclub|Greatclub|modificada|
|[gGPWUqUPfUG7gzbI.htm](pathfinder-bestiary-items/gGPWUqUPfUG7gzbI.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[ghngQIhTKh6viVvP.htm](pathfinder-bestiary-items/ghngQIhTKh6viVvP.htm)|Change Shape|Change Shape|modificada|
|[GhnXpFtWM1vyvu8V.htm](pathfinder-bestiary-items/GhnXpFtWM1vyvu8V.htm)|Construct Armor (Hardness 9)|Construir Armadura (Dureza 9)|modificada|
|[GhRRqxvK6T21zheo.htm](pathfinder-bestiary-items/GhRRqxvK6T21zheo.htm)|Pack Attack|Ataque en manada|modificada|
|[GHuGn3ZCUkbeSFiQ.htm](pathfinder-bestiary-items/GHuGn3ZCUkbeSFiQ.htm)|Filth Fever|Filth Fever|modificada|
|[giE56Vm25ZKR3ZJm.htm](pathfinder-bestiary-items/giE56Vm25ZKR3ZJm.htm)|Sprinkle Pixie Dust|Sprinkle Pixie Dust|modificada|
|[GIxGQeMwhYMa5aqt.htm](pathfinder-bestiary-items/GIxGQeMwhYMa5aqt.htm)|Sling|Sling|modificada|
|[GjB9p0GotK5A2v4S.htm](pathfinder-bestiary-items/GjB9p0GotK5A2v4S.htm)|Desert Thirst|Sed del desierto|modificada|
|[GJh2y3nd5dKEWe8I.htm](pathfinder-bestiary-items/GJh2y3nd5dKEWe8I.htm)|Displacement|Desplazamiento|modificada|
|[gjHycBNENtIXpvMF.htm](pathfinder-bestiary-items/gjHycBNENtIXpvMF.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[gJlDdyxfSGMpZN7a.htm](pathfinder-bestiary-items/gJlDdyxfSGMpZN7a.htm)|Tusks|Tusks|modificada|
|[GJNrxWQdmK1EykYc.htm](pathfinder-bestiary-items/GJNrxWQdmK1EykYc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[gjQiQcNp4NjkcKFX.htm](pathfinder-bestiary-items/gjQiQcNp4NjkcKFX.htm)|Tail|Tail|modificada|
|[gjrtuvnDxfzWNWcA.htm](pathfinder-bestiary-items/gjrtuvnDxfzWNWcA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gjZuHZMNsUIskGJf.htm](pathfinder-bestiary-items/gjZuHZMNsUIskGJf.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[GkFwtCILr5PvesJA.htm](pathfinder-bestiary-items/GkFwtCILr5PvesJA.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Gkho6QKI4TcQF5wN.htm](pathfinder-bestiary-items/Gkho6QKI4TcQF5wN.htm)|Wing|Ala|modificada|
|[GKRApZm7r1VxqXRt.htm](pathfinder-bestiary-items/GKRApZm7r1VxqXRt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GliiYsgCIg3rdZ5a.htm](pathfinder-bestiary-items/GliiYsgCIg3rdZ5a.htm)|Vulnerable to Dispelling|Vulnerable a Disipar|modificada|
|[GlLMuMgpxJ9FZVfm.htm](pathfinder-bestiary-items/GlLMuMgpxJ9FZVfm.htm)|Armor-Rending Bite|Mordisco rasgaarmaduras|modificada|
|[GLucEXJUhwdRS7q9.htm](pathfinder-bestiary-items/GLucEXJUhwdRS7q9.htm)|Claw|Garra|modificada|
|[glwIs2UR3O1wG0tc.htm](pathfinder-bestiary-items/glwIs2UR3O1wG0tc.htm)|Soul Lock|Encerrar alma|modificada|
|[GM1uYxLGklcQScOG.htm](pathfinder-bestiary-items/GM1uYxLGklcQScOG.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[Gm902B0XQR5W4eLu.htm](pathfinder-bestiary-items/Gm902B0XQR5W4eLu.htm)|Tied to the Land|Tied to the Land|modificada|
|[gMf2LQ1HIE4bCW8p.htm](pathfinder-bestiary-items/gMf2LQ1HIE4bCW8p.htm)|Spring upon Prey|Primavera sobre presa|modificada|
|[GmPqLo8cPqMgY6V2.htm](pathfinder-bestiary-items/GmPqLo8cPqMgY6V2.htm)|Grab|Agarrado|modificada|
|[GmwDsrs27J6RnwHo.htm](pathfinder-bestiary-items/GmwDsrs27J6RnwHo.htm)|Jaws|Fauces|modificada|
|[GNpyzaHN2uioOy3x.htm](pathfinder-bestiary-items/GNpyzaHN2uioOy3x.htm)|Pick|Pick|modificada|
|[GNtcN0AsjAwwwARU.htm](pathfinder-bestiary-items/GNtcN0AsjAwwwARU.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GOB41k6iHfNnb28d.htm](pathfinder-bestiary-items/GOB41k6iHfNnb28d.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GoEK3KTZm6r0vhaU.htm](pathfinder-bestiary-items/GoEK3KTZm6r0vhaU.htm)|Infectious Aura|Aura infecciosa|modificada|
|[GOGgw91M2vSnmc7P.htm](pathfinder-bestiary-items/GOGgw91M2vSnmc7P.htm)|Claw|Garra|modificada|
|[gooqrR0z9rN4mnlN.htm](pathfinder-bestiary-items/gooqrR0z9rN4mnlN.htm)|Bola Bolt|Virote con boleadoras|modificada|
|[goqMldxoGCuLp3Zc.htm](pathfinder-bestiary-items/goqMldxoGCuLp3Zc.htm)|Cloud Walk|Cloud Walk|modificada|
|[goSE2P0nlOvFsVEH.htm](pathfinder-bestiary-items/goSE2P0nlOvFsVEH.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GoUIra7KXFd9kUhH.htm](pathfinder-bestiary-items/GoUIra7KXFd9kUhH.htm)|Trample|Trample|modificada|
|[GpF7MRIldG6kiEBK.htm](pathfinder-bestiary-items/GpF7MRIldG6kiEBK.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[gPoK6Dl5m43T6mpg.htm](pathfinder-bestiary-items/gPoK6Dl5m43T6mpg.htm)|+1 Status to All Saves vs. Disease|+1 situación a todas las salvaciones contra enfermedad.|modificada|
|[gpQkk5cFJ03uvxyX.htm](pathfinder-bestiary-items/gpQkk5cFJ03uvxyX.htm)|Talon|Talon|modificada|
|[gQ7hVeDi4X11cJlc.htm](pathfinder-bestiary-items/gQ7hVeDi4X11cJlc.htm)|Cursed Wound|Herida maldita|modificada|
|[GqnL43j2bn0yBMad.htm](pathfinder-bestiary-items/GqnL43j2bn0yBMad.htm)|Greataxe|Greataxe|modificada|
|[gQsn9Gd92k5s2c0r.htm](pathfinder-bestiary-items/gQsn9Gd92k5s2c0r.htm)|Fast Healing 2|Curación rápida 2|modificada|
|[GrDnRSSifZtVibbk.htm](pathfinder-bestiary-items/GrDnRSSifZtVibbk.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[gRn39hK7VbDU7fip.htm](pathfinder-bestiary-items/gRn39hK7VbDU7fip.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[gRnmV8LgZS6qZ09j.htm](pathfinder-bestiary-items/gRnmV8LgZS6qZ09j.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[gRQ32Y4BWrQbWOlS.htm](pathfinder-bestiary-items/gRQ32Y4BWrQbWOlS.htm)|Coffin Restoration|Restauración de ataúdes|modificada|
|[gRR4m0LvmYETY1sR.htm](pathfinder-bestiary-items/gRR4m0LvmYETY1sR.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[grrGcmbt2tnL36tN.htm](pathfinder-bestiary-items/grrGcmbt2tnL36tN.htm)|Destructive Strike|Golpe Destructivo|modificada|
|[grrOo0mi2kz2urJF.htm](pathfinder-bestiary-items/grrOo0mi2kz2urJF.htm)|Cinder Dispersal|Dispersión de cenizas|modificada|
|[Gs9kfR3NhiKWAumE.htm](pathfinder-bestiary-items/Gs9kfR3NhiKWAumE.htm)|Lantern of Hope|Faro de esperanza|modificada|
|[GsaqDG9VKiNEXisx.htm](pathfinder-bestiary-items/GsaqDG9VKiNEXisx.htm)|Constant Spells|Constant Spells|modificada|
|[gSG1OZxFHX2zLrNP.htm](pathfinder-bestiary-items/gSG1OZxFHX2zLrNP.htm)|Counterspell|Contraconjuro|modificada|
|[gSKgEf1cka4ughqp.htm](pathfinder-bestiary-items/gSKgEf1cka4ughqp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GsQeeSV97T6FPvoY.htm](pathfinder-bestiary-items/GsQeeSV97T6FPvoY.htm)|Breath Weapon|Breath Weapon|modificada|
|[gSQGpCpvE3YRtvLr.htm](pathfinder-bestiary-items/gSQGpCpvE3YRtvLr.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[GsSxwd5EPNBqxdWC.htm](pathfinder-bestiary-items/GsSxwd5EPNBqxdWC.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[GsuMeqU77NP6vTqI.htm](pathfinder-bestiary-items/GsuMeqU77NP6vTqI.htm)|Protean Anatomy|Anatomía Proteica|modificada|
|[GSyhdPMQNpUa9LQJ.htm](pathfinder-bestiary-items/GSyhdPMQNpUa9LQJ.htm)|Engulf|Envolver|modificada|
|[gUMIzpDGWwaygeXn.htm](pathfinder-bestiary-items/gUMIzpDGWwaygeXn.htm)|Tail|Tail|modificada|
|[gUqFH9kEouWhTaTk.htm](pathfinder-bestiary-items/gUqFH9kEouWhTaTk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gurbv9AAgZYNEghZ.htm](pathfinder-bestiary-items/gurbv9AAgZYNEghZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GurQ6TuFcLhxNewa.htm](pathfinder-bestiary-items/GurQ6TuFcLhxNewa.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[gV8plzH46OeVvlNG.htm](pathfinder-bestiary-items/gV8plzH46OeVvlNG.htm)|Stench|Hedor|modificada|
|[gVHdzhF7CYS5L3SK.htm](pathfinder-bestiary-items/gVHdzhF7CYS5L3SK.htm)|Burning Grasp|Presa ardiente|modificada|
|[GVNTgGaXcebR64nT.htm](pathfinder-bestiary-items/GVNTgGaXcebR64nT.htm)|Partitioned Anatomy|Anatomía segmentada|modificada|
|[GvYWVCMKVWLO9jDK.htm](pathfinder-bestiary-items/GvYWVCMKVWLO9jDK.htm)|Shock Maw|Fauces Electrizantes|modificada|
|[GW3yAvIc0LiXOGo0.htm](pathfinder-bestiary-items/GW3yAvIc0LiXOGo0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gw66iZEKmcFi0JjW.htm](pathfinder-bestiary-items/gw66iZEKmcFi0JjW.htm)|Howl|Howl|modificada|
|[GWK22q1k4VFPbsKf.htm](pathfinder-bestiary-items/GWK22q1k4VFPbsKf.htm)|Pack Attack|Ataque en manada|modificada|
|[gwswBzTnckFDYGZR.htm](pathfinder-bestiary-items/gwswBzTnckFDYGZR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gx4QtoqFUMcJKh9b.htm](pathfinder-bestiary-items/gx4QtoqFUMcJKh9b.htm)|Grab|Agarrado|modificada|
|[gxbMXYNwJTWTgbf6.htm](pathfinder-bestiary-items/gxbMXYNwJTWTgbf6.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[gxj7cGSco9mq9DF4.htm](pathfinder-bestiary-items/gxj7cGSco9mq9DF4.htm)|Fast Healing 5|Curación rápida 5|modificada|
|[gxJrJPqtpVlM0Xe6.htm](pathfinder-bestiary-items/gxJrJPqtpVlM0Xe6.htm)|Swallow Whole|Engullir Todo|modificada|
|[gxq2qiGUPuQ5wCW9.htm](pathfinder-bestiary-items/gxq2qiGUPuQ5wCW9.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[GXr4u9s0uuvI5J6d.htm](pathfinder-bestiary-items/GXr4u9s0uuvI5J6d.htm)|Vampire Weaknesses|Vampire Weaknesses|modificada|
|[gXUEOhZBMg1NCOMw.htm](pathfinder-bestiary-items/gXUEOhZBMg1NCOMw.htm)|Quick Capture|Captura veloz|modificada|
|[gXusGY96gXnuOJVw.htm](pathfinder-bestiary-items/gXusGY96gXnuOJVw.htm)|Frightful Presence|Frightful Presence|modificada|
|[gYnpwjAaRfAevtAE.htm](pathfinder-bestiary-items/gYnpwjAaRfAevtAE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[gYS1Pu6DMoqNba15.htm](pathfinder-bestiary-items/gYS1Pu6DMoqNba15.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[GZ55vWFhwaHyhnfM.htm](pathfinder-bestiary-items/GZ55vWFhwaHyhnfM.htm)|Avernal Fever|Fiebre del Averno|modificada|
|[gZHu2APmbYnWCFLm.htm](pathfinder-bestiary-items/gZHu2APmbYnWCFLm.htm)|Bark Orders|Órdenes de Ladrido|modificada|
|[GzpsUYZPjfrvA0Yh.htm](pathfinder-bestiary-items/GzpsUYZPjfrvA0Yh.htm)|Object Meld|Object Meld|modificada|
|[gzVcZTMtslepZxcC.htm](pathfinder-bestiary-items/gzVcZTMtslepZxcC.htm)|Unstoppable Burrow|Madriguera Imparable|modificada|
|[gZXfTOLjvJ7GkHxM.htm](pathfinder-bestiary-items/gZXfTOLjvJ7GkHxM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gzYwg0Ob1iEiRCDc.htm](pathfinder-bestiary-items/gzYwg0Ob1iEiRCDc.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[gZywv8nUOzTjloep.htm](pathfinder-bestiary-items/gZywv8nUOzTjloep.htm)|Fire Mote|Mota de Fuego|modificada|
|[h0WWEsFcTFj9dtG9.htm](pathfinder-bestiary-items/h0WWEsFcTFj9dtG9.htm)|Constant Spells|Constant Spells|modificada|
|[h2IphaAIOQxjJB7Y.htm](pathfinder-bestiary-items/h2IphaAIOQxjJB7Y.htm)|Fist|Puño|modificada|
|[h2qxWUvI0fbg4RxB.htm](pathfinder-bestiary-items/h2qxWUvI0fbg4RxB.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[h2xw74YF04RtIxtd.htm](pathfinder-bestiary-items/h2xw74YF04RtIxtd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[h3RAiOfN4DCwKqV0.htm](pathfinder-bestiary-items/h3RAiOfN4DCwKqV0.htm)|Putrid Stench|Hedor pútrido|modificada|
|[h4fFfvaiDiqgTpTz.htm](pathfinder-bestiary-items/h4fFfvaiDiqgTpTz.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[h4qPfDnAjrfUEFX6.htm](pathfinder-bestiary-items/h4qPfDnAjrfUEFX6.htm)|Sylvan Wine|Sylvan Wine|modificada|
|[H64oYdSbWbITvHiH.htm](pathfinder-bestiary-items/H64oYdSbWbITvHiH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[h6rad5LTezkxMym3.htm](pathfinder-bestiary-items/h6rad5LTezkxMym3.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[h6wQ76GXx0XhL0rW.htm](pathfinder-bestiary-items/h6wQ76GXx0XhL0rW.htm)|Claw|Garra|modificada|
|[h7FJgyxQFttUHPeA.htm](pathfinder-bestiary-items/h7FJgyxQFttUHPeA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[H7ILhQt7vbCdQYmv.htm](pathfinder-bestiary-items/H7ILhQt7vbCdQYmv.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[h7p7zhkW6OBX6N85.htm](pathfinder-bestiary-items/h7p7zhkW6OBX6N85.htm)|Luminous Fire|Fuego Luminoso|modificada|
|[H7YaFV40o1FTZLkj.htm](pathfinder-bestiary-items/H7YaFV40o1FTZLkj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[H85aThyyHpy8z06u.htm](pathfinder-bestiary-items/H85aThyyHpy8z06u.htm)|Pull the Strands|Tirar de las hebras|modificada|
|[H88Ay3ZweyXm4sL3.htm](pathfinder-bestiary-items/H88Ay3ZweyXm4sL3.htm)|Staff|Báculo|modificada|
|[h8HPqnXI3dI7KFxV.htm](pathfinder-bestiary-items/h8HPqnXI3dI7KFxV.htm)|Hoof|Hoof|modificada|
|[H8zaV182Ql2iZqTg.htm](pathfinder-bestiary-items/H8zaV182Ql2iZqTg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[h9dbqoHHEOOBy05q.htm](pathfinder-bestiary-items/h9dbqoHHEOOBy05q.htm)|Shape Ice|Moldear hielo|modificada|
|[HAG0BZUyUX7lvLH9.htm](pathfinder-bestiary-items/HAG0BZUyUX7lvLH9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HAOtod2wxXg71n8U.htm](pathfinder-bestiary-items/HAOtod2wxXg71n8U.htm)|Jaws|Fauces|modificada|
|[HavBnXAT02rDNben.htm](pathfinder-bestiary-items/HavBnXAT02rDNben.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[HaWsVELJ0bpMTvaP.htm](pathfinder-bestiary-items/HaWsVELJ0bpMTvaP.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[hbBhekpAkC55ljY1.htm](pathfinder-bestiary-items/hbBhekpAkC55ljY1.htm)|Fleet Performer|Pies ligeros Interpretar|modificada|
|[hbdeUFe3D16BSKa0.htm](pathfinder-bestiary-items/hbdeUFe3D16BSKa0.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[hbr6Uqs0GRk0t1JI.htm](pathfinder-bestiary-items/hbr6Uqs0GRk0t1JI.htm)|Punishing Tail|Coletazo castigador|modificada|
|[hcHsi8D2704vMZZf.htm](pathfinder-bestiary-items/hcHsi8D2704vMZZf.htm)|Constant Spells|Constant Spells|modificada|
|[hCQUre41fnib5cSf.htm](pathfinder-bestiary-items/hCQUre41fnib5cSf.htm)|Claw|Garra|modificada|
|[hcXr2FC36bTLJ9kF.htm](pathfinder-bestiary-items/hcXr2FC36bTLJ9kF.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[Hczu635vDDik3NIJ.htm](pathfinder-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|Agarrado|modificada|
|[HDOKj3GkjZCeH2k3.htm](pathfinder-bestiary-items/HDOKj3GkjZCeH2k3.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[hdxkFShEWbTlfmhb.htm](pathfinder-bestiary-items/hdxkFShEWbTlfmhb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[hdyD6t2duHPLOLyi.htm](pathfinder-bestiary-items/hdyD6t2duHPLOLyi.htm)|Buck|Encabritarse|modificada|
|[hDYxH5VuENin7Q0I.htm](pathfinder-bestiary-items/hDYxH5VuENin7Q0I.htm)|Crystal Sense (Imprecise) 60 feet|Sentido cristales (Impreciso) 60 pies|modificada|
|[hdZW9dGdp8kUzUjv.htm](pathfinder-bestiary-items/hdZW9dGdp8kUzUjv.htm)|Breath Weapon|Breath Weapon|modificada|
|[heW1rCJfQ2KMmuqd.htm](pathfinder-bestiary-items/heW1rCJfQ2KMmuqd.htm)|Ink Cloud|Nube de Tinta|modificada|
|[HF4Dnnv97ezTQ1sy.htm](pathfinder-bestiary-items/HF4Dnnv97ezTQ1sy.htm)|Tail|Tail|modificada|
|[HFETDQgqq1x4amjT.htm](pathfinder-bestiary-items/HFETDQgqq1x4amjT.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[hfNcV2xUlTHlcRk0.htm](pathfinder-bestiary-items/hfNcV2xUlTHlcRk0.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[hFpiEAg9uOUfaval.htm](pathfinder-bestiary-items/hFpiEAg9uOUfaval.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[HFyEFMKsCld64B15.htm](pathfinder-bestiary-items/HFyEFMKsCld64B15.htm)|Avenging Bite|Muerdemuerde Vengador|modificada|
|[HG3k71Iu7dao9DNr.htm](pathfinder-bestiary-items/HG3k71Iu7dao9DNr.htm)|Jaws|Fauces|modificada|
|[HGrl3b1F23RjZkR7.htm](pathfinder-bestiary-items/HGrl3b1F23RjZkR7.htm)|Jaws|Fauces|modificada|
|[HgRvaTh0mJv6CbTu.htm](pathfinder-bestiary-items/HgRvaTh0mJv6CbTu.htm)|Swarm Mind|Swarm Mind|modificada|
|[hH9lWhV3jA9NQJuG.htm](pathfinder-bestiary-items/hH9lWhV3jA9NQJuG.htm)|Hidden Movement|Movimiento oculto|modificada|
|[HhkalK1aMfqDSiWQ.htm](pathfinder-bestiary-items/HhkalK1aMfqDSiWQ.htm)|Drink Blood|Beber Sangre|modificada|
|[hhL0DgwVWFclIPih.htm](pathfinder-bestiary-items/hhL0DgwVWFclIPih.htm)|Jaws|Fauces|modificada|
|[hHLGOCYOj0KZUdYI.htm](pathfinder-bestiary-items/hHLGOCYOj0KZUdYI.htm)|Spike Volley|Volea de pinchos|modificada|
|[hHrGBCeIy4565BFU.htm](pathfinder-bestiary-items/hHrGBCeIy4565BFU.htm)|Vortex Pull|Corrientes del vórtice|modificada|
|[hIpNUSZedk66VUWy.htm](pathfinder-bestiary-items/hIpNUSZedk66VUWy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hIxE6LMA92SPk1eC.htm](pathfinder-bestiary-items/hIxE6LMA92SPk1eC.htm)|Death Flash|Destello fúnebre de la Muerte|modificada|
|[hJ1mAKCkNvCy1GY0.htm](pathfinder-bestiary-items/hJ1mAKCkNvCy1GY0.htm)|Fangs|Colmillos|modificada|
|[hj2HivadIMJbB0cD.htm](pathfinder-bestiary-items/hj2HivadIMJbB0cD.htm)|Stinger|Aguijón|modificada|
|[hJ2RMo3aTyVEm4yz.htm](pathfinder-bestiary-items/hJ2RMo3aTyVEm4yz.htm)|Spittle|Escupitajo|modificada|
|[HjqV5crA9cRbpUVj.htm](pathfinder-bestiary-items/HjqV5crA9cRbpUVj.htm)|Electric Surge|Oleaje Eléctrico|modificada|
|[hk3y1r8nU2o8QA4C.htm](pathfinder-bestiary-items/hk3y1r8nU2o8QA4C.htm)|Thrash|Sacudirse|modificada|
|[hk7qH9BpvzM7rFGe.htm](pathfinder-bestiary-items/hk7qH9BpvzM7rFGe.htm)|Sin Scent (Imprecise) 30 feet|Olor a Pecado (Impreciso) 30 pies|modificada|
|[HknPH5MLDixmpO4o.htm](pathfinder-bestiary-items/HknPH5MLDixmpO4o.htm)|Constrict|Restringir|modificada|
|[HKsUocYSHIn1WVXR.htm](pathfinder-bestiary-items/HKsUocYSHIn1WVXR.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[HkUjt1e6CiwdfHoy.htm](pathfinder-bestiary-items/HkUjt1e6CiwdfHoy.htm)|Breath Weapon|Breath Weapon|modificada|
|[HkvlzHti2lgPShwU.htm](pathfinder-bestiary-items/HkvlzHti2lgPShwU.htm)|Breath Weapon|Breath Weapon|modificada|
|[HKY0WgGamDu2JbUq.htm](pathfinder-bestiary-items/HKY0WgGamDu2JbUq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HkzWmjfjVp6p9yR8.htm](pathfinder-bestiary-items/HkzWmjfjVp6p9yR8.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[HM9U7vUmg5uUan4k.htm](pathfinder-bestiary-items/HM9U7vUmg5uUan4k.htm)|Fast Healing 2 (While Underwater)|Curación rápida 2 (bajo el agua)|modificada|
|[HMeH7MYJTTTICR3k.htm](pathfinder-bestiary-items/HMeH7MYJTTTICR3k.htm)|Jaws|Fauces|modificada|
|[hN4sFUVjQOMN0Gvq.htm](pathfinder-bestiary-items/hN4sFUVjQOMN0Gvq.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[hn5GBufYeA5C1oOg.htm](pathfinder-bestiary-items/hn5GBufYeA5C1oOg.htm)|Drink Blood|Beber Sangre|modificada|
|[hnjL5DfkOGkcdjPb.htm](pathfinder-bestiary-items/hnjL5DfkOGkcdjPb.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[HNM58d8JH28lGstw.htm](pathfinder-bestiary-items/HNM58d8JH28lGstw.htm)|Hoof|Hoof|modificada|
|[Ho2TRxPdMdaTfr2V.htm](pathfinder-bestiary-items/Ho2TRxPdMdaTfr2V.htm)|Claw|Garra|modificada|
|[HO9oEygHulZKXSSM.htm](pathfinder-bestiary-items/HO9oEygHulZKXSSM.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[HOMxdkDlcB6S1y9k.htm](pathfinder-bestiary-items/HOMxdkDlcB6S1y9k.htm)|Ghaele's Gaze|Mirada del ghaele|modificada|
|[hook1XGZNlxXtjvJ.htm](pathfinder-bestiary-items/hook1XGZNlxXtjvJ.htm)|Fangs|Colmillos|modificada|
|[hopAbtHoJov7g8zJ.htm](pathfinder-bestiary-items/hopAbtHoJov7g8zJ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Hor4EOQfUGAkFak1.htm](pathfinder-bestiary-items/Hor4EOQfUGAkFak1.htm)|Flaming Composite Longbow|Arco largo compuesto Flamógera|modificada|
|[hperRozJh0EXe7iX.htm](pathfinder-bestiary-items/hperRozJh0EXe7iX.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[HPknnMADVjl3vadK.htm](pathfinder-bestiary-items/HPknnMADVjl3vadK.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[hQCxv2A5wNHVDzkj.htm](pathfinder-bestiary-items/hQCxv2A5wNHVDzkj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hQNxvkHklgW9fvpf.htm](pathfinder-bestiary-items/hQNxvkHklgW9fvpf.htm)|Explosion|Explosión|modificada|
|[HQrZGDENHs9wuZxK.htm](pathfinder-bestiary-items/HQrZGDENHs9wuZxK.htm)|Sling|Sling|modificada|
|[HQToKWTC3drtqduN.htm](pathfinder-bestiary-items/HQToKWTC3drtqduN.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[hr7MMe0oUKYOwUNV.htm](pathfinder-bestiary-items/hr7MMe0oUKYOwUNV.htm)|Tusk|Tusk|modificada|
|[hr88vSwb8UXcgsA0.htm](pathfinder-bestiary-items/hr88vSwb8UXcgsA0.htm)|Claw|Garra|modificada|
|[Hrk3kc8RGNxK9XoN.htm](pathfinder-bestiary-items/Hrk3kc8RGNxK9XoN.htm)|Freezing Blood|Sangre congelante|modificada|
|[HRuAKCIYZlriHOGv.htm](pathfinder-bestiary-items/HRuAKCIYZlriHOGv.htm)|Jaws|Fauces|modificada|
|[hRXUEhTtGJCD8X7J.htm](pathfinder-bestiary-items/hRXUEhTtGJCD8X7J.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[hrylbarG87YtJSgb.htm](pathfinder-bestiary-items/hrylbarG87YtJSgb.htm)|Breath Weapon|Breath Weapon|modificada|
|[HrzOkjm7joQRK88q.htm](pathfinder-bestiary-items/HrzOkjm7joQRK88q.htm)|Breath Weapon|Breath Weapon|modificada|
|[HScepbVGbS75R0W3.htm](pathfinder-bestiary-items/HScepbVGbS75R0W3.htm)|Negative Healing|Curación negativa|modificada|
|[hsDNG89JDWAbCW9D.htm](pathfinder-bestiary-items/hsDNG89JDWAbCW9D.htm)|Beak|Beak|modificada|
|[hSGaU1UT8mzmPFWl.htm](pathfinder-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[hskirvOc2VeALRbT.htm](pathfinder-bestiary-items/hskirvOc2VeALRbT.htm)|Trident|Trident|modificada|
|[Hss992fc1sCOcDVo.htm](pathfinder-bestiary-items/Hss992fc1sCOcDVo.htm)|Electric Reflexes|Reflejos eléctricos|modificada|
|[hsU755t5zFbchRub.htm](pathfinder-bestiary-items/hsU755t5zFbchRub.htm)|Grizzly Arrival|Grizzly Arrival|modificada|
|[hT4tTJSS8Sm6qFZz.htm](pathfinder-bestiary-items/hT4tTJSS8Sm6qFZz.htm)|Shortbow|Arco corto|modificada|
|[hTNAzHRu1qlREBai.htm](pathfinder-bestiary-items/hTNAzHRu1qlREBai.htm)|Talon|Talon|modificada|
|[HtPykjM3gCE3oQiR.htm](pathfinder-bestiary-items/HtPykjM3gCE3oQiR.htm)|Torpor|Torpor|modificada|
|[hU1RHjKaWqS7tyUN.htm](pathfinder-bestiary-items/hU1RHjKaWqS7tyUN.htm)|Jaws|Fauces|modificada|
|[huAbJHogJo4HKgXV.htm](pathfinder-bestiary-items/huAbJHogJo4HKgXV.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[Huqh2NJEkx1xBpoO.htm](pathfinder-bestiary-items/Huqh2NJEkx1xBpoO.htm)|Flaming Gallop|Galope Flamígera|modificada|
|[HUT5houFl0hCEPKU.htm](pathfinder-bestiary-items/HUT5houFl0hCEPKU.htm)|Fist|Puño|modificada|
|[HuxqXHbNPMVfb98R.htm](pathfinder-bestiary-items/HuxqXHbNPMVfb98R.htm)|Buck|Encabritarse|modificada|
|[hvdEekJwNvI4RMw1.htm](pathfinder-bestiary-items/hvdEekJwNvI4RMw1.htm)|-2 to All Saves (If Heartstone is Lost)|-2 a todas las salvaciones (si se pierde la Heartstone)|modificada|
|[HvKkuW09C9ugdKrX.htm](pathfinder-bestiary-items/HvKkuW09C9ugdKrX.htm)|Claw|Garra|modificada|
|[HvOzFzF3j47WTzIv.htm](pathfinder-bestiary-items/HvOzFzF3j47WTzIv.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[hvrW7fEOtezPrmRv.htm](pathfinder-bestiary-items/hvrW7fEOtezPrmRv.htm)|Jaws|Fauces|modificada|
|[hvTnI184eIoLfwPq.htm](pathfinder-bestiary-items/hvTnI184eIoLfwPq.htm)|Berserk Slam|Porrazo bersérker|modificada|
|[hWaYvdDBpwGUYAcD.htm](pathfinder-bestiary-items/hWaYvdDBpwGUYAcD.htm)|Claw|Garra|modificada|
|[HwtOh4jUfSRblUG1.htm](pathfinder-bestiary-items/HwtOh4jUfSRblUG1.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[hwUA4Pu9qRQurhWj.htm](pathfinder-bestiary-items/hwUA4Pu9qRQurhWj.htm)|Status Sight|Ver signos vitales|modificada|
|[hWuV8klRcI0Txecg.htm](pathfinder-bestiary-items/hWuV8klRcI0Txecg.htm)|Constant Spells|Constant Spells|modificada|
|[hwvMSx3qTN1uk4iN.htm](pathfinder-bestiary-items/hwvMSx3qTN1uk4iN.htm)|Imp Venom|Imp Venom|modificada|
|[hX1EY7o7nb8MZxHG.htm](pathfinder-bestiary-items/hX1EY7o7nb8MZxHG.htm)|Constant Spells|Constant Spells|modificada|
|[Hxm0zO7GdMBB0p4S.htm](pathfinder-bestiary-items/Hxm0zO7GdMBB0p4S.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hXmPoWQ69iXdq42A.htm](pathfinder-bestiary-items/hXmPoWQ69iXdq42A.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[hxUuVqQu31iQwmv1.htm](pathfinder-bestiary-items/hxUuVqQu31iQwmv1.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[HXXy2GteYPnNjM0H.htm](pathfinder-bestiary-items/HXXy2GteYPnNjM0H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Hy8sShAxrQfV8CpX.htm](pathfinder-bestiary-items/Hy8sShAxrQfV8CpX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hYcrjabm50sPyqBV.htm](pathfinder-bestiary-items/hYcrjabm50sPyqBV.htm)|Pack Attack|Ataque en manada|modificada|
|[hYuh2ahMpMeOTyoZ.htm](pathfinder-bestiary-items/hYuh2ahMpMeOTyoZ.htm)|Wing|Ala|modificada|
|[hZbArhODPV9EmZ0m.htm](pathfinder-bestiary-items/hZbArhODPV9EmZ0m.htm)|Sound Imitation|Imitación de sonidos|modificada|
|[hzcpRbbSGtnAZDHc.htm](pathfinder-bestiary-items/hzcpRbbSGtnAZDHc.htm)|Tail|Tail|modificada|
|[HzOfOnqjDdkk5Alq.htm](pathfinder-bestiary-items/HzOfOnqjDdkk5Alq.htm)|Change Shape|Change Shape|modificada|
|[hZqtsipwFmuBYwC2.htm](pathfinder-bestiary-items/hZqtsipwFmuBYwC2.htm)|Protean Anatomy|Anatomía Proteica|modificada|
|[HzveIcuKPfW88Zc6.htm](pathfinder-bestiary-items/HzveIcuKPfW88Zc6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[I0GaQls3ghJMJ5Ib.htm](pathfinder-bestiary-items/I0GaQls3ghJMJ5Ib.htm)|Children of the Night|Niños de la Noche|modificada|
|[i0HAcFnNZ6dlKhwT.htm](pathfinder-bestiary-items/i0HAcFnNZ6dlKhwT.htm)|Sickle|Hoz|modificada|
|[I0zRRHgyNtxaiNb1.htm](pathfinder-bestiary-items/I0zRRHgyNtxaiNb1.htm)|Claw|Garra|modificada|
|[I1o6uYgy06gzdYSk.htm](pathfinder-bestiary-items/I1o6uYgy06gzdYSk.htm)|Gallop|Galope|modificada|
|[I2ll4xGsFEeBgdRL.htm](pathfinder-bestiary-items/I2ll4xGsFEeBgdRL.htm)|Fast Swoop|Picado veloz|modificada|
|[i3byEluD3eLH89OG.htm](pathfinder-bestiary-items/i3byEluD3eLH89OG.htm)|Horns|Cuernos|modificada|
|[i3ESVmWhqnA7Sq5u.htm](pathfinder-bestiary-items/i3ESVmWhqnA7Sq5u.htm)|Obscuring Spores|Esporas de obscurecimiento|modificada|
|[I3pFXc7fvS41popk.htm](pathfinder-bestiary-items/I3pFXc7fvS41popk.htm)|Jet|Jet|modificada|
|[i561TRHP40EPtkbg.htm](pathfinder-bestiary-items/i561TRHP40EPtkbg.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[i5jZOGRVSvmKW82C.htm](pathfinder-bestiary-items/i5jZOGRVSvmKW82C.htm)|Golden Luck|Suerte de oro|modificada|
|[I5zNTSZyjvhCKz3D.htm](pathfinder-bestiary-items/I5zNTSZyjvhCKz3D.htm)|Pack Attack|Ataque en manada|modificada|
|[I6eF9oDAdSJChUG3.htm](pathfinder-bestiary-items/I6eF9oDAdSJChUG3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[I6lxEQY0rHjkqhZ1.htm](pathfinder-bestiary-items/I6lxEQY0rHjkqhZ1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[I6M7YvqndCsq8oLj.htm](pathfinder-bestiary-items/I6M7YvqndCsq8oLj.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[i7CeNawHh8IVMpSe.htm](pathfinder-bestiary-items/i7CeNawHh8IVMpSe.htm)|Longspear|Longspear|modificada|
|[i7ekQiw5lntgdpAT.htm](pathfinder-bestiary-items/i7ekQiw5lntgdpAT.htm)|Crossbow|Ballesta|modificada|
|[I7lbbBC8haIiOXzD.htm](pathfinder-bestiary-items/I7lbbBC8haIiOXzD.htm)|Insidious Mummy Rot|Putridez de momia agravada|modificada|
|[I7qCrIGJsKvrguZB.htm](pathfinder-bestiary-items/I7qCrIGJsKvrguZB.htm)|Swoop|Swoop|modificada|
|[i8xGkDyTBirx7HaW.htm](pathfinder-bestiary-items/i8xGkDyTBirx7HaW.htm)|Blood Scent|Blood Scent|modificada|
|[I98w77MsvsKISF0z.htm](pathfinder-bestiary-items/I98w77MsvsKISF0z.htm)|Frightful Presence|Frightful Presence|modificada|
|[I9gdtsVJjPZVXvSq.htm](pathfinder-bestiary-items/I9gdtsVJjPZVXvSq.htm)|Tail|Tail|modificada|
|[i9LmbR4DKZeW0lRN.htm](pathfinder-bestiary-items/i9LmbR4DKZeW0lRN.htm)|Shard Storm|Tormenta de esquirlas|modificada|
|[iA3NXKVAiWzxGVaO.htm](pathfinder-bestiary-items/iA3NXKVAiWzxGVaO.htm)|Stench Suppression|Supresión de Hedor|modificada|
|[iaTfEZ50lrTlV5Ku.htm](pathfinder-bestiary-items/iaTfEZ50lrTlV5Ku.htm)|Personality Fragments|Fragmentos de personalidad|modificada|
|[ib3hNOxGFodAt0fV.htm](pathfinder-bestiary-items/ib3hNOxGFodAt0fV.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[ibaCsks8puTwETeL.htm](pathfinder-bestiary-items/ibaCsks8puTwETeL.htm)|Jaws|Fauces|modificada|
|[Ibj6bgSG7PZrEoiR.htm](pathfinder-bestiary-items/Ibj6bgSG7PZrEoiR.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[ICgKjXXhG8h1g0BE.htm](pathfinder-bestiary-items/ICgKjXXhG8h1g0BE.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[ICjSF2djJxtdnASi.htm](pathfinder-bestiary-items/ICjSF2djJxtdnASi.htm)|Jaws|Fauces|modificada|
|[IcOzxaLaimK6QUsg.htm](pathfinder-bestiary-items/IcOzxaLaimK6QUsg.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[icuRBAbmsZKQdilF.htm](pathfinder-bestiary-items/icuRBAbmsZKQdilF.htm)|Dread Gaze|Mirada de fatalidad|modificada|
|[iCXXt9CjiqFfuluI.htm](pathfinder-bestiary-items/iCXXt9CjiqFfuluI.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[Id4tvbdVJaXtgOOu.htm](pathfinder-bestiary-items/Id4tvbdVJaXtgOOu.htm)|Jaws|Fauces|modificada|
|[iDCc02ZuFBClDhnt.htm](pathfinder-bestiary-items/iDCc02ZuFBClDhnt.htm)|Fangs|Colmillos|modificada|
|[IdfK6A7UpGkscuOG.htm](pathfinder-bestiary-items/IdfK6A7UpGkscuOG.htm)|Ghast Fever|Fiebre del ghast|modificada|
|[idWgJAjZ5pi1uI2C.htm](pathfinder-bestiary-items/idWgJAjZ5pi1uI2C.htm)|Trunk|Tronco|modificada|
|[iE5eD6UZECDALlPI.htm](pathfinder-bestiary-items/iE5eD6UZECDALlPI.htm)|Aura of Flame|Aura de llamas|modificada|
|[iEPnPUyNUxfYs63B.htm](pathfinder-bestiary-items/iEPnPUyNUxfYs63B.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[iEqHLBXlRUlbfb37.htm](pathfinder-bestiary-items/iEqHLBXlRUlbfb37.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IFPnmQpUJLW9TJa9.htm](pathfinder-bestiary-items/IFPnmQpUJLW9TJa9.htm)|Wail|Lamento|modificada|
|[IGHgjWHfJlqh674F.htm](pathfinder-bestiary-items/IGHgjWHfJlqh674F.htm)|Change Shape|Change Shape|modificada|
|[igXtaJ0MMd7h8u6O.htm](pathfinder-bestiary-items/igXtaJ0MMd7h8u6O.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[iHa81hIBKMF5eXxi.htm](pathfinder-bestiary-items/iHa81hIBKMF5eXxi.htm)|Fangs|Colmillos|modificada|
|[ihI9TF3YdNJFYytf.htm](pathfinder-bestiary-items/ihI9TF3YdNJFYytf.htm)|Trample|Trample|modificada|
|[ii0tl0zE2tEgfPD4.htm](pathfinder-bestiary-items/ii0tl0zE2tEgfPD4.htm)|Wolf Empathy|Wolf Empathy|modificada|
|[iIggw0JAX98xzjTY.htm](pathfinder-bestiary-items/iIggw0JAX98xzjTY.htm)|Suction|Succión|modificada|
|[Iiw19xLS3nyusjz1.htm](pathfinder-bestiary-items/Iiw19xLS3nyusjz1.htm)|Constrict|Restringir|modificada|
|[IJhY1kNU4OIS7fL4.htm](pathfinder-bestiary-items/IJhY1kNU4OIS7fL4.htm)|Jaws|Fauces|modificada|
|[ijsE8IzzuXckprkM.htm](pathfinder-bestiary-items/ijsE8IzzuXckprkM.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[IjsncXWh3Ja5Fvdo.htm](pathfinder-bestiary-items/IjsncXWh3Ja5Fvdo.htm)|Push|Push|modificada|
|[iJVGp2XuaslAOfcc.htm](pathfinder-bestiary-items/iJVGp2XuaslAOfcc.htm)|Shortsword|Espada corta|modificada|
|[ikE6Os9KaQ3E0ApZ.htm](pathfinder-bestiary-items/ikE6Os9KaQ3E0ApZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IkemOnt4GVgmYXBn.htm](pathfinder-bestiary-items/IkemOnt4GVgmYXBn.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ikNRBFaxo41oGwAQ.htm](pathfinder-bestiary-items/ikNRBFaxo41oGwAQ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[IKXHj9lMhQ6Qy07y.htm](pathfinder-bestiary-items/IKXHj9lMhQ6Qy07y.htm)|Rend|Rasgadura|modificada|
|[IL3VPaw5B0JI2Rce.htm](pathfinder-bestiary-items/IL3VPaw5B0JI2Rce.htm)|Constrict|Restringir|modificada|
|[iL66w8THCpYdsALT.htm](pathfinder-bestiary-items/iL66w8THCpYdsALT.htm)|Nightmare Rider|Jinete de pesadilla|modificada|
|[IlmvsLcNfbO7OmVF.htm](pathfinder-bestiary-items/IlmvsLcNfbO7OmVF.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Imio5DQk9gfplk5I.htm](pathfinder-bestiary-items/Imio5DQk9gfplk5I.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[inK7m9JBf4286NW2.htm](pathfinder-bestiary-items/inK7m9JBf4286NW2.htm)|Tail|Tail|modificada|
|[InkIGrftuKGUgSoo.htm](pathfinder-bestiary-items/InkIGrftuKGUgSoo.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[inQcsNlqlQ4umup7.htm](pathfinder-bestiary-items/inQcsNlqlQ4umup7.htm)|Unimpeded Throw|Lanzamiento sin obstáculos|modificada|
|[IoAJluZoGb9mgLGv.htm](pathfinder-bestiary-items/IoAJluZoGb9mgLGv.htm)|Inexorable|Inexorable|modificada|
|[IOgQmb8AR9d7yAlu.htm](pathfinder-bestiary-items/IOgQmb8AR9d7yAlu.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ioskOBeviPp2ll8D.htm](pathfinder-bestiary-items/ioskOBeviPp2ll8D.htm)|Web|Telara|modificada|
|[iP02Y8PDHLNbctbc.htm](pathfinder-bestiary-items/iP02Y8PDHLNbctbc.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[IqlIzn7bY9FduX6g.htm](pathfinder-bestiary-items/IqlIzn7bY9FduX6g.htm)|Menacing Guardian|Guardián amenazante|modificada|
|[iqVhVxGvfUFnJ2uq.htm](pathfinder-bestiary-items/iqVhVxGvfUFnJ2uq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IQwShRAySaUkCUW5.htm](pathfinder-bestiary-items/IQwShRAySaUkCUW5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iQz3YxQPSZdpvSJi.htm](pathfinder-bestiary-items/iQz3YxQPSZdpvSJi.htm)|Regeneration 30 (Deactivated by Cold Iron)|Regeneración 30 (Desactivado por Hierro Frío)|modificada|
|[ir8tXGoz964QTWmv.htm](pathfinder-bestiary-items/ir8tXGoz964QTWmv.htm)|Web|Telara|modificada|
|[ira6umGEGOXqSyIk.htm](pathfinder-bestiary-items/ira6umGEGOXqSyIk.htm)|Water Spout|Caño de agua|modificada|
|[IrW6qhUnZpM1uAqK.htm](pathfinder-bestiary-items/IrW6qhUnZpM1uAqK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[is4tKm65LxMVSzQV.htm](pathfinder-bestiary-items/is4tKm65LxMVSzQV.htm)|Trident|Trident|modificada|
|[Is4V63biDtNBVpi2.htm](pathfinder-bestiary-items/Is4V63biDtNBVpi2.htm)|Improved Push 5 feet|Empuje mejorado 5 pies|modificada|
|[IsMVcoiRI9sN4iQe.htm](pathfinder-bestiary-items/IsMVcoiRI9sN4iQe.htm)|Frightful Presence|Frightful Presence|modificada|
|[IsRnnfl27oJF1UGY.htm](pathfinder-bestiary-items/IsRnnfl27oJF1UGY.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[IsvDyyM2ZO6XxnsU.htm](pathfinder-bestiary-items/IsvDyyM2ZO6XxnsU.htm)|Blood Frenzy|Frenesí de Sangre|modificada|
|[iSVjVMy4wUULtEXe.htm](pathfinder-bestiary-items/iSVjVMy4wUULtEXe.htm)|Infuse Weapons|Infundir armas|modificada|
|[Ita7hOSbzKOHxexv.htm](pathfinder-bestiary-items/Ita7hOSbzKOHxexv.htm)|Drench|Sofocar|modificada|
|[iTaObjgnXpF66Sz5.htm](pathfinder-bestiary-items/iTaObjgnXpF66Sz5.htm)|Rapier|Estoque|modificada|
|[iTAZj4rMOPRWWJDP.htm](pathfinder-bestiary-items/iTAZj4rMOPRWWJDP.htm)|Corpse Throwing|Lanzamiento de cadáveres|modificada|
|[itJzZD6rxkMhtQQZ.htm](pathfinder-bestiary-items/itJzZD6rxkMhtQQZ.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[ITkyellSfoRYxVGk.htm](pathfinder-bestiary-items/ITkyellSfoRYxVGk.htm)|Divine Grace|Gracia divina|modificada|
|[itPzrsYivMMWwHm3.htm](pathfinder-bestiary-items/itPzrsYivMMWwHm3.htm)|Coven Spells|Coven Spells|modificada|
|[itqvdf5hwWK45QOe.htm](pathfinder-bestiary-items/itqvdf5hwWK45QOe.htm)|Flail|Mayal|modificada|
|[Itw1pfAvHSiAptUZ.htm](pathfinder-bestiary-items/Itw1pfAvHSiAptUZ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[iu1YHlbvh64HBaor.htm](pathfinder-bestiary-items/iu1YHlbvh64HBaor.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[IUA7ZWVTepsameDv.htm](pathfinder-bestiary-items/IUA7ZWVTepsameDv.htm)|Fangs|Colmillos|modificada|
|[iUAQwRninJ973LoN.htm](pathfinder-bestiary-items/iUAQwRninJ973LoN.htm)|Jaws|Fauces|modificada|
|[iuFRjKw2PLJl3E3F.htm](pathfinder-bestiary-items/iuFRjKw2PLJl3E3F.htm)|Pounce|Abalanzarse|modificada|
|[IuK4DrLEyP6BmKi2.htm](pathfinder-bestiary-items/IuK4DrLEyP6BmKi2.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[IUmBG7HtxvrIOHUR.htm](pathfinder-bestiary-items/IUmBG7HtxvrIOHUR.htm)|Generate Bomb|Generar bomba|modificada|
|[IUNBun7zEVrlp6AY.htm](pathfinder-bestiary-items/IUNBun7zEVrlp6AY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iUQXXOate2Qv004f.htm](pathfinder-bestiary-items/iUQXXOate2Qv004f.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[iuyu0qM1TjakNK8f.htm](pathfinder-bestiary-items/iuyu0qM1TjakNK8f.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[iuyYNQtDMprDDoZH.htm](pathfinder-bestiary-items/iuyYNQtDMprDDoZH.htm)|Mist Escape|Niebla Huir|modificada|
|[IvEp0FjtG1bxA4Xw.htm](pathfinder-bestiary-items/IvEp0FjtG1bxA4Xw.htm)|Jaws|Fauces|modificada|
|[ivsgBmmJjZW5Wr5S.htm](pathfinder-bestiary-items/ivsgBmmJjZW5Wr5S.htm)|Blood Soak|Blood Soak|modificada|
|[iVZkQBGcIBXlxkcc.htm](pathfinder-bestiary-items/iVZkQBGcIBXlxkcc.htm)|Frightful Presence|Frightful Presence|modificada|
|[iw0W5WgXZN9Y21ul.htm](pathfinder-bestiary-items/iw0W5WgXZN9Y21ul.htm)|Grab|Agarrado|modificada|
|[IWJbZROV26XdSO2U.htm](pathfinder-bestiary-items/IWJbZROV26XdSO2U.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[IWkgxOKsoYOGSw3E.htm](pathfinder-bestiary-items/IWkgxOKsoYOGSw3E.htm)|Split|Split|modificada|
|[iwppReF9hEA7igPO.htm](pathfinder-bestiary-items/iwppReF9hEA7igPO.htm)|Jaws|Fauces|modificada|
|[ix1KQX8Ji2g1e9ID.htm](pathfinder-bestiary-items/ix1KQX8Ji2g1e9ID.htm)|Skewer|Ensartar|modificada|
|[iXBVxZ5RntmImlUb.htm](pathfinder-bestiary-items/iXBVxZ5RntmImlUb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[IXjaAxkgZsz5958E.htm](pathfinder-bestiary-items/IXjaAxkgZsz5958E.htm)|Free Expression|Expresión libre|modificada|
|[ixOGpstVQFBXDi3I.htm](pathfinder-bestiary-items/ixOGpstVQFBXDi3I.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[IXWkQwpWIcMopGvV.htm](pathfinder-bestiary-items/IXWkQwpWIcMopGvV.htm)|Arm|Brazo|modificada|
|[iXwmnPnBaxNyz50d.htm](pathfinder-bestiary-items/iXwmnPnBaxNyz50d.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[IyKWWIgBkLnO2AXB.htm](pathfinder-bestiary-items/IyKWWIgBkLnO2AXB.htm)|Tremorsense (Imprecise) 100 feet|Sentido del Temblor (Impreciso) 100 pies|modificada|
|[iySuinX75ehOzFDa.htm](pathfinder-bestiary-items/iySuinX75ehOzFDa.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[IYYi7B0j0PD17IXo.htm](pathfinder-bestiary-items/IYYi7B0j0PD17IXo.htm)|Fast Swallow|Tragar de golpe|modificada|
|[IYZ7DcFhTBCpe9MA.htm](pathfinder-bestiary-items/IYZ7DcFhTBCpe9MA.htm)|Staff|Báculo|modificada|
|[iz8CpHkcAiNnZfvZ.htm](pathfinder-bestiary-items/iz8CpHkcAiNnZfvZ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[IzdeO18d4uSX3LyW.htm](pathfinder-bestiary-items/IzdeO18d4uSX3LyW.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iZTnwEyJWIaqvI8B.htm](pathfinder-bestiary-items/iZTnwEyJWIaqvI8B.htm)|Ferocity|Ferocidad|modificada|
|[izty6IGZL6BowyUS.htm](pathfinder-bestiary-items/izty6IGZL6BowyUS.htm)|Pack Attack|Ataque en manada|modificada|
|[j0pCK3mNfxlgQemu.htm](pathfinder-bestiary-items/j0pCK3mNfxlgQemu.htm)|Tail|Tail|modificada|
|[J1HcaTe5BjGFYDgL.htm](pathfinder-bestiary-items/J1HcaTe5BjGFYDgL.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[J2jO45MMtWXrfesA.htm](pathfinder-bestiary-items/J2jO45MMtWXrfesA.htm)|Grab|Agarrado|modificada|
|[j2n6AMiW1Ve4yYnf.htm](pathfinder-bestiary-items/j2n6AMiW1Ve4yYnf.htm)|Claw|Garra|modificada|
|[J3HCGhXaQnSxfglZ.htm](pathfinder-bestiary-items/J3HCGhXaQnSxfglZ.htm)|Tail|Tail|modificada|
|[j3ILBYm7ssA8p2Nz.htm](pathfinder-bestiary-items/j3ILBYm7ssA8p2Nz.htm)|Fire Mote|Mota de Fuego|modificada|
|[J3OtjjfFKxnDTSmx.htm](pathfinder-bestiary-items/J3OtjjfFKxnDTSmx.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[J4J0VphHvmjPrss7.htm](pathfinder-bestiary-items/J4J0VphHvmjPrss7.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[J53J6OFXMFOq27dA.htm](pathfinder-bestiary-items/J53J6OFXMFOq27dA.htm)|Woodland Stride|Paso forestal|modificada|
|[j5LBfiOQPypQQTM6.htm](pathfinder-bestiary-items/j5LBfiOQPypQQTM6.htm)|Dissonant Note|Nota Disonante|modificada|
|[j6bfMiK3Ghz6Y86o.htm](pathfinder-bestiary-items/j6bfMiK3Ghz6Y86o.htm)|Swiftness|Celeridad|modificada|
|[J6pZtpCOYKA1AxsU.htm](pathfinder-bestiary-items/J6pZtpCOYKA1AxsU.htm)|Blood of the Night|Sangre nocturna|modificada|
|[j6xu1V9riuAOrTpK.htm](pathfinder-bestiary-items/j6xu1V9riuAOrTpK.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[J73AKz9wgVvUQWFo.htm](pathfinder-bestiary-items/J73AKz9wgVvUQWFo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[J7jljAzKdEbTuHtB.htm](pathfinder-bestiary-items/J7jljAzKdEbTuHtB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j7Nh5AH6RZ2HuPM1.htm](pathfinder-bestiary-items/j7Nh5AH6RZ2HuPM1.htm)|Breath Weapon|Breath Weapon|modificada|
|[j7PKag1rt4Uml8gg.htm](pathfinder-bestiary-items/j7PKag1rt4Uml8gg.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[j7RuQ48Pmg4V96SN.htm](pathfinder-bestiary-items/j7RuQ48Pmg4V96SN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[J8d6vqQrqGXi53bU.htm](pathfinder-bestiary-items/J8d6vqQrqGXi53bU.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[J8dl99lVefC7uvbV.htm](pathfinder-bestiary-items/J8dl99lVefC7uvbV.htm)|Lightning Lash|Lightning Lash|modificada|
|[j8IZenEYeIuNHysF.htm](pathfinder-bestiary-items/j8IZenEYeIuNHysF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j8XDp3PWM6N8DyRV.htm](pathfinder-bestiary-items/j8XDp3PWM6N8DyRV.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[J9d2rpUp8ZsHKZWW.htm](pathfinder-bestiary-items/J9d2rpUp8ZsHKZWW.htm)|Spear|Lanza|modificada|
|[j9R2dejFnXClVzKR.htm](pathfinder-bestiary-items/j9R2dejFnXClVzKR.htm)|Breath Weapon|Breath Weapon|modificada|
|[ja5ZUKm30pmuCb0m.htm](pathfinder-bestiary-items/ja5ZUKm30pmuCb0m.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[JaEjyp5taeG7YCCo.htm](pathfinder-bestiary-items/JaEjyp5taeG7YCCo.htm)|Ethereal Step|Paso etéreo|modificada|
|[jAZY8WC1BtImPyEF.htm](pathfinder-bestiary-items/jAZY8WC1BtImPyEF.htm)|Gauntlet|Guantelete|modificada|
|[jb3f12sfPd9nzwVN.htm](pathfinder-bestiary-items/jb3f12sfPd9nzwVN.htm)|Claw|Garra|modificada|
|[jbjsVEFvVsslwVHa.htm](pathfinder-bestiary-items/jbjsVEFvVsslwVHa.htm)|Tail Lash|Azote con la cola|modificada|
|[JBksJycZVWAJ0ESS.htm](pathfinder-bestiary-items/JBksJycZVWAJ0ESS.htm)|Invoke Rune|Invocar Runa|modificada|
|[JBSyvkullKxEJ94w.htm](pathfinder-bestiary-items/JBSyvkullKxEJ94w.htm)|Jaws|Fauces|modificada|
|[JCdcBeryZm0DvHqA.htm](pathfinder-bestiary-items/JCdcBeryZm0DvHqA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[JChdzk7WZ7KUXqF7.htm](pathfinder-bestiary-items/JChdzk7WZ7KUXqF7.htm)|Calcification|Calcificación|modificada|
|[jcHkvPONg4SesmVb.htm](pathfinder-bestiary-items/jcHkvPONg4SesmVb.htm)|Electricity Aura|Aura de Electricidad|modificada|
|[Jcl5rz38QkHYDfHx.htm](pathfinder-bestiary-items/Jcl5rz38QkHYDfHx.htm)|Negative Healing|Curación negativa|modificada|
|[jD38LrotmqVxwz0a.htm](pathfinder-bestiary-items/jD38LrotmqVxwz0a.htm)|Claw|Garra|modificada|
|[jdam6t7WxAcXyrY6.htm](pathfinder-bestiary-items/jdam6t7WxAcXyrY6.htm)|Claw|Garra|modificada|
|[jdNRBv6RogEW3DdF.htm](pathfinder-bestiary-items/jdNRBv6RogEW3DdF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[JEBTVmt5rc6G8tOb.htm](pathfinder-bestiary-items/JEBTVmt5rc6G8tOb.htm)|Snatch|Arrebatar|modificada|
|[jeN7zh5S6wH6ciYZ.htm](pathfinder-bestiary-items/jeN7zh5S6wH6ciYZ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Jf6SQTejkZnEWA2T.htm](pathfinder-bestiary-items/Jf6SQTejkZnEWA2T.htm)|Bo Staff|Báculo Bo|modificada|
|[jfGKdhMjdkxsNHCf.htm](pathfinder-bestiary-items/jfGKdhMjdkxsNHCf.htm)|Tentacle|Tentáculo|modificada|
|[jfJXOP78x7xyHplx.htm](pathfinder-bestiary-items/jfJXOP78x7xyHplx.htm)|Pack Attack|Ataque en manada|modificada|
|[JfUL91jTnBSObypg.htm](pathfinder-bestiary-items/JfUL91jTnBSObypg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[jG0R8rvY0cpJadHK.htm](pathfinder-bestiary-items/jG0R8rvY0cpJadHK.htm)|Fist|Puño|modificada|
|[Jg5WkDHWpi1JRQ9k.htm](pathfinder-bestiary-items/Jg5WkDHWpi1JRQ9k.htm)|Vulnerable to Stone to Flesh|Vulnerable a Piedra a la carne|modificada|
|[JGlsPzMEmEwis19F.htm](pathfinder-bestiary-items/JGlsPzMEmEwis19F.htm)|Constant Spells|Constant Spells|modificada|
|[JgxdK2Dmby1lwpfp.htm](pathfinder-bestiary-items/JgxdK2Dmby1lwpfp.htm)|Claw|Garra|modificada|
|[jhCCqt7r2yw8bad1.htm](pathfinder-bestiary-items/jhCCqt7r2yw8bad1.htm)|Leaves|Hojas|modificada|
|[JhurblUzrKIPeUrP.htm](pathfinder-bestiary-items/JhurblUzrKIPeUrP.htm)|Flaming Gallop|Galope Flamígera|modificada|
|[jHv2A4kh5nyPGSxF.htm](pathfinder-bestiary-items/jHv2A4kh5nyPGSxF.htm)|Jaws|Fauces|modificada|
|[ji4Sith8FiIfdvlL.htm](pathfinder-bestiary-items/ji4Sith8FiIfdvlL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jInvivt7qyjg6Q4R.htm](pathfinder-bestiary-items/jInvivt7qyjg6Q4R.htm)|Dagger|Daga|modificada|
|[JIRrOHVjqWOmt2TV.htm](pathfinder-bestiary-items/JIRrOHVjqWOmt2TV.htm)|Improved Grab|Agarrado mejorado|modificada|
|[jJ0nUZ8hgrpWlXtS.htm](pathfinder-bestiary-items/jJ0nUZ8hgrpWlXtS.htm)|Snatch|Arrebatar|modificada|
|[jj8vv07lGAp4FELU.htm](pathfinder-bestiary-items/jj8vv07lGAp4FELU.htm)|Skeletal Lore|Saber de los esqueletos|modificada|
|[JJc1A4ggjxl0zB12.htm](pathfinder-bestiary-items/JJc1A4ggjxl0zB12.htm)|Grab|Agarrado|modificada|
|[jjcSFQRRi1OZjeeX.htm](pathfinder-bestiary-items/jjcSFQRRi1OZjeeX.htm)|Inspiration|Inspiración|modificada|
|[JjOstNeMHauECGcr.htm](pathfinder-bestiary-items/JjOstNeMHauECGcr.htm)|Luminescent Aura|Aura luminiscente|modificada|
|[JjvXFkGRXacPPnXd.htm](pathfinder-bestiary-items/JjvXFkGRXacPPnXd.htm)|Tail|Tail|modificada|
|[jK2StyBBiefgpRNw.htm](pathfinder-bestiary-items/jK2StyBBiefgpRNw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JkIYhF9MSDwxXdYc.htm](pathfinder-bestiary-items/JkIYhF9MSDwxXdYc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jKUKY0ByHy5vvU8u.htm](pathfinder-bestiary-items/jKUKY0ByHy5vvU8u.htm)|Longbow|Arco largo|modificada|
|[jL2ksRd3mnetwWV2.htm](pathfinder-bestiary-items/jL2ksRd3mnetwWV2.htm)|Tongue|Lengua|modificada|
|[jL9PDQ27Dz6UAMOH.htm](pathfinder-bestiary-items/jL9PDQ27Dz6UAMOH.htm)|Scimitar|Cimitarra|modificada|
|[jmDi2EBAEE9yAyK3.htm](pathfinder-bestiary-items/jmDi2EBAEE9yAyK3.htm)|Sea Serpent Algae|Alga Serpiente Marina|modificada|
|[jmPDVPebOQ3XVOwA.htm](pathfinder-bestiary-items/jmPDVPebOQ3XVOwA.htm)|Boar Charge|Carga de jabalí|modificada|
|[jMTCxlT7XAx5M91e.htm](pathfinder-bestiary-items/jMTCxlT7XAx5M91e.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jN4atm9Y2nSfZFtO.htm](pathfinder-bestiary-items/jN4atm9Y2nSfZFtO.htm)|Improved Grab|Agarrado mejorado|modificada|
|[jnZDE6piOq3rzmdg.htm](pathfinder-bestiary-items/jnZDE6piOq3rzmdg.htm)|Create Spawn|Elaborar Spawn|modificada|
|[joBYS96mXSnZC1WB.htm](pathfinder-bestiary-items/joBYS96mXSnZC1WB.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[JOcNBwBFEc7623Hf.htm](pathfinder-bestiary-items/JOcNBwBFEc7623Hf.htm)|Lightning Lash|Lightning Lash|modificada|
|[JOlynEzl2UrSa16n.htm](pathfinder-bestiary-items/JOlynEzl2UrSa16n.htm)|Energy Drain|Drenaje de Energía|modificada|
|[jpBSdZzftYrVeiU0.htm](pathfinder-bestiary-items/jpBSdZzftYrVeiU0.htm)|Dragon Chill|Frío dracónico|modificada|
|[JQ3FZtUvvt6RRRXF.htm](pathfinder-bestiary-items/JQ3FZtUvvt6RRRXF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JqajO4nisN5Ddc6s.htm](pathfinder-bestiary-items/JqajO4nisN5Ddc6s.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[jqPcPNAR4c3cEI3M.htm](pathfinder-bestiary-items/jqPcPNAR4c3cEI3M.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[JQxkxEB6JK80gsvb.htm](pathfinder-bestiary-items/JQxkxEB6JK80gsvb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JrkBo5yZM19wv3KX.htm](pathfinder-bestiary-items/JrkBo5yZM19wv3KX.htm)|Enormous|Gigantesca|modificada|
|[JRoSNngd9XENOzDl.htm](pathfinder-bestiary-items/JRoSNngd9XENOzDl.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[jrRWYmGu9vVkAfdv.htm](pathfinder-bestiary-items/jrRWYmGu9vVkAfdv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[JRWZ0pJnxuJyvINn.htm](pathfinder-bestiary-items/JRWZ0pJnxuJyvINn.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[jsgHC6kdtZdnAJMT.htm](pathfinder-bestiary-items/jsgHC6kdtZdnAJMT.htm)|Ink Cloud|Nube de Tinta|modificada|
|[jt4PpVxiITWfM8ox.htm](pathfinder-bestiary-items/jt4PpVxiITWfM8ox.htm)|Jaws|Fauces|modificada|
|[jt6yBdIcUHJLCmAw.htm](pathfinder-bestiary-items/jt6yBdIcUHJLCmAw.htm)|Vine|Vid|modificada|
|[jtb1mM7gOuCllfQf.htm](pathfinder-bestiary-items/jtb1mM7gOuCllfQf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jTsP7LKJDD7LlPlH.htm](pathfinder-bestiary-items/jTsP7LKJDD7LlPlH.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[JttXYbMjKJ7Vv7Zy.htm](pathfinder-bestiary-items/JttXYbMjKJ7Vv7Zy.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jUfHzGj1QzWogY7T.htm](pathfinder-bestiary-items/jUfHzGj1QzWogY7T.htm)|Frightful Presence|Frightful Presence|modificada|
|[JUgPxjF2b7DYZwEu.htm](pathfinder-bestiary-items/JUgPxjF2b7DYZwEu.htm)|Grab|Agarrado|modificada|
|[juKKPoneZ4x5mqlo.htm](pathfinder-bestiary-items/juKKPoneZ4x5mqlo.htm)|Negative Healing|Curación negativa|modificada|
|[JUlSpx5USh1nfM12.htm](pathfinder-bestiary-items/JUlSpx5USh1nfM12.htm)|Jaws|Fauces|modificada|
|[jv3hOJvw3g4A5KWY.htm](pathfinder-bestiary-items/jv3hOJvw3g4A5KWY.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[JV5W1wo8td9aBqBd.htm](pathfinder-bestiary-items/JV5W1wo8td9aBqBd.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[JvFUFSxHH2EThNAC.htm](pathfinder-bestiary-items/JvFUFSxHH2EThNAC.htm)|Breath Weapon|Breath Weapon|modificada|
|[jvmfyWcZp1aZiZqS.htm](pathfinder-bestiary-items/jvmfyWcZp1aZiZqS.htm)|Claw Frenzy|Frenesí de garras|modificada|
|[Jvmi4nru2sCMB5Hn.htm](pathfinder-bestiary-items/Jvmi4nru2sCMB5Hn.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[JVyi4C8jthjCBGN6.htm](pathfinder-bestiary-items/JVyi4C8jthjCBGN6.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[jvZUoPLS8OA1SQTw.htm](pathfinder-bestiary-items/jvZUoPLS8OA1SQTw.htm)|Shortsword|Espada corta|modificada|
|[jW0BtgVKN8XQhLKs.htm](pathfinder-bestiary-items/jW0BtgVKN8XQhLKs.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[JwLrjprg0tRZcFsL.htm](pathfinder-bestiary-items/JwLrjprg0tRZcFsL.htm)|Claw|Garra|modificada|
|[JWlYLXxlI4IRYbeF.htm](pathfinder-bestiary-items/JWlYLXxlI4IRYbeF.htm)|Greataxe|Greataxe|modificada|
|[JWS021oC5szbCrAS.htm](pathfinder-bestiary-items/JWS021oC5szbCrAS.htm)|Push 15 feet|Empujar 15 pies|modificada|
|[jwtPWph6mWdPZhf5.htm](pathfinder-bestiary-items/jwtPWph6mWdPZhf5.htm)|Crossbow|Ballesta|modificada|
|[Jxa5SzH4ybrGFB9q.htm](pathfinder-bestiary-items/Jxa5SzH4ybrGFB9q.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Jxw7RvEERHGRzycq.htm](pathfinder-bestiary-items/Jxw7RvEERHGRzycq.htm)|Negative Healing|Curación negativa|modificada|
|[jYjpln4mc6udm6oF.htm](pathfinder-bestiary-items/jYjpln4mc6udm6oF.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[JYkKoCx49R8WXnXW.htm](pathfinder-bestiary-items/JYkKoCx49R8WXnXW.htm)|Tied to the Land|Tied to the Land|modificada|
|[JysKih2FAoEH8Jdb.htm](pathfinder-bestiary-items/JysKih2FAoEH8Jdb.htm)|Frightful Presence|Frightful Presence|modificada|
|[jYT3UtsBe68guWz1.htm](pathfinder-bestiary-items/jYT3UtsBe68guWz1.htm)|Claw|Garra|modificada|
|[jZ8U8eTiEuKU6toZ.htm](pathfinder-bestiary-items/jZ8U8eTiEuKU6toZ.htm)|Heat|Calor|modificada|
|[JzE14vwBQYTNvg3Q.htm](pathfinder-bestiary-items/JzE14vwBQYTNvg3Q.htm)|Shadow Shift|Sombra Cambiante|modificada|
|[JzIDoWltig01Ezsi.htm](pathfinder-bestiary-items/JzIDoWltig01Ezsi.htm)|Fist|Puño|modificada|
|[jzku4qcmz6H3dk9C.htm](pathfinder-bestiary-items/jzku4qcmz6H3dk9C.htm)|Torch|Antorcha|modificada|
|[jZxRFq5KrTWsA6Ut.htm](pathfinder-bestiary-items/jZxRFq5KrTWsA6Ut.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[jZXTqe1YmviCFvap.htm](pathfinder-bestiary-items/jZXTqe1YmviCFvap.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[K09n3CnAeuaSeD9k.htm](pathfinder-bestiary-items/K09n3CnAeuaSeD9k.htm)|Shake It Off|Recupérate|modificada|
|[k0b4P1F8sPglTEue.htm](pathfinder-bestiary-items/k0b4P1F8sPglTEue.htm)|Hurricane Blast|Descarga huracanada|modificada|
|[k1kPrQ3Krhcipl4E.htm](pathfinder-bestiary-items/k1kPrQ3Krhcipl4E.htm)|Fist|Puño|modificada|
|[K2aOOR33x8FPHyVI.htm](pathfinder-bestiary-items/K2aOOR33x8FPHyVI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[K2pwGyKWkDm1x4Xh.htm](pathfinder-bestiary-items/K2pwGyKWkDm1x4Xh.htm)|Assume Form|Adoptar forma|modificada|
|[k2r2GtyzX2R9mQrE.htm](pathfinder-bestiary-items/k2r2GtyzX2R9mQrE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[K2x7jUEcU74IIJ3D.htm](pathfinder-bestiary-items/K2x7jUEcU74IIJ3D.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[K33vakNPDetkh1iP.htm](pathfinder-bestiary-items/K33vakNPDetkh1iP.htm)|Cold Iron Silver Flame Whip|Látigo de la llama de plata de hierro fríoígera|modificada|
|[k3r3UHSeKp4tTrp0.htm](pathfinder-bestiary-items/k3r3UHSeKp4tTrp0.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[k3uphptV5MjVwlbH.htm](pathfinder-bestiary-items/k3uphptV5MjVwlbH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[k45QQ0xBwRtwJCeF.htm](pathfinder-bestiary-items/k45QQ0xBwRtwJCeF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[K56mdzOHeveTvQ5D.htm](pathfinder-bestiary-items/K56mdzOHeveTvQ5D.htm)|Giant Wasp Venom|Veneno de avispa gigante|modificada|
|[k72C42LOvjUNSP6f.htm](pathfinder-bestiary-items/k72C42LOvjUNSP6f.htm)|Freezing Blood|Sangre congelante|modificada|
|[k8k6bZAdPTdhZfDp.htm](pathfinder-bestiary-items/k8k6bZAdPTdhZfDp.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[k8mtTYrWrnHjBMCO.htm](pathfinder-bestiary-items/k8mtTYrWrnHjBMCO.htm)|Breach|Emerger|modificada|
|[K8rZwgv45XvIe8vL.htm](pathfinder-bestiary-items/K8rZwgv45XvIe8vL.htm)|Ogre Hook|Garfio de Ogro|modificada|
|[k9T6UTS86VkzB0LP.htm](pathfinder-bestiary-items/k9T6UTS86VkzB0LP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Ka4LviiNUhy4FNzo.htm](pathfinder-bestiary-items/Ka4LviiNUhy4FNzo.htm)|Talon|Talon|modificada|
|[kAUwyvDAhkgyFkme.htm](pathfinder-bestiary-items/kAUwyvDAhkgyFkme.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[kayBhD4IXys5Ppt8.htm](pathfinder-bestiary-items/kayBhD4IXys5Ppt8.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[kB3jH0EObomim8bP.htm](pathfinder-bestiary-items/kB3jH0EObomim8bP.htm)|Tail Lash|Azote con la cola|modificada|
|[kBcZLCEv6TSReExJ.htm](pathfinder-bestiary-items/kBcZLCEv6TSReExJ.htm)|Claw|Garra|modificada|
|[kBOVK9j2yxJEBYt7.htm](pathfinder-bestiary-items/kBOVK9j2yxJEBYt7.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[kBr3AKRl4vlCwVK2.htm](pathfinder-bestiary-items/kBr3AKRl4vlCwVK2.htm)|Greatclub|Greatclub|modificada|
|[KcCw29PtlpuADI1x.htm](pathfinder-bestiary-items/KcCw29PtlpuADI1x.htm)|Pack Attack|Ataque en manada|modificada|
|[kCfoRidunIYcTXeZ.htm](pathfinder-bestiary-items/kCfoRidunIYcTXeZ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[KCUHGmNla57hO2ZK.htm](pathfinder-bestiary-items/KCUHGmNla57hO2ZK.htm)|Reshape Reality|Reshape Reality|modificada|
|[kcYtaqPWsMiKHYty.htm](pathfinder-bestiary-items/kcYtaqPWsMiKHYty.htm)|Haunting Melody|Haunting Melody|modificada|
|[KD8sQ1IJ6El9ImGR.htm](pathfinder-bestiary-items/KD8sQ1IJ6El9ImGR.htm)|Tail|Tail|modificada|
|[KddvbFPsHJAfqGAc.htm](pathfinder-bestiary-items/KddvbFPsHJAfqGAc.htm)|Wing Deflection|Desvío con el ala|modificada|
|[kDtkieHx63QY56es.htm](pathfinder-bestiary-items/kDtkieHx63QY56es.htm)|Horn|Cuerno|modificada|
|[KDy693oZjUA79500.htm](pathfinder-bestiary-items/KDy693oZjUA79500.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KESgZ7EFPclSlLY8.htm](pathfinder-bestiary-items/KESgZ7EFPclSlLY8.htm)|Drink Blood|Beber Sangre|modificada|
|[KeTrwbKWih6HyuoM.htm](pathfinder-bestiary-items/KeTrwbKWih6HyuoM.htm)|Fast Healing 7|Curación rápida 7|modificada|
|[kFeAtCbycBHyIGcR.htm](pathfinder-bestiary-items/kFeAtCbycBHyIGcR.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[KfFb9f7ZnoNzfyNt.htm](pathfinder-bestiary-items/KfFb9f7ZnoNzfyNt.htm)|Extra Reaction|Reacción adicional|modificada|
|[kFgfAbXaJbI8otfZ.htm](pathfinder-bestiary-items/kFgfAbXaJbI8otfZ.htm)|Shortsword|Espada corta|modificada|
|[kfjRR8MHRHoxgCkF.htm](pathfinder-bestiary-items/kfjRR8MHRHoxgCkF.htm)|Grab|Agarrado|modificada|
|[KFObwL3fuLfRhdtm.htm](pathfinder-bestiary-items/KFObwL3fuLfRhdtm.htm)|Death Flame|Llama fúnebre de la muerte|modificada|
|[kGKrPttfeXrnHGWr.htm](pathfinder-bestiary-items/kGKrPttfeXrnHGWr.htm)|Mummy Rot|Mummy Rot|modificada|
|[kGQg9F0ZgW7fp4DB.htm](pathfinder-bestiary-items/kGQg9F0ZgW7fp4DB.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[kgt9znbnxYGyxtvV.htm](pathfinder-bestiary-items/kgt9znbnxYGyxtvV.htm)|Fog Vision|Fog Vision|modificada|
|[KhgTIBtP3uB7Ls0w.htm](pathfinder-bestiary-items/KhgTIBtP3uB7Ls0w.htm)|Fireball Breath|Aliento de bola de fuego|modificada|
|[KHmyhQ0lVwwKOLlC.htm](pathfinder-bestiary-items/KHmyhQ0lVwwKOLlC.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kHnahHvifJfCVS11.htm](pathfinder-bestiary-items/kHnahHvifJfCVS11.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[kHo1nNFrNvhFTdVm.htm](pathfinder-bestiary-items/kHo1nNFrNvhFTdVm.htm)|Gallop|Galope|modificada|
|[khvFVWc6itmsBv7T.htm](pathfinder-bestiary-items/khvFVWc6itmsBv7T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ki3C4xNREH5G9Knv.htm](pathfinder-bestiary-items/ki3C4xNREH5G9Knv.htm)|Change Shape|Change Shape|modificada|
|[KiBIFxCCOgpsuMvK.htm](pathfinder-bestiary-items/KiBIFxCCOgpsuMvK.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[KidhmTPJWxNcC0db.htm](pathfinder-bestiary-items/KidhmTPJWxNcC0db.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kiIWJXY9ATN5NdpV.htm](pathfinder-bestiary-items/kiIWJXY9ATN5NdpV.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[kiLj66ryWPKBgZKh.htm](pathfinder-bestiary-items/kiLj66ryWPKBgZKh.htm)|Woodland Stride|Paso forestal|modificada|
|[Kj1uqisygxPxuaC9.htm](pathfinder-bestiary-items/Kj1uqisygxPxuaC9.htm)|Spider Speak|Spider Speak|modificada|
|[KjBveNLcC22szBCP.htm](pathfinder-bestiary-items/KjBveNLcC22szBCP.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Kjifw1jgDvfmrEGU.htm](pathfinder-bestiary-items/Kjifw1jgDvfmrEGU.htm)|Steal Shadow|Robar sombra|modificada|
|[kjpNSY1WlZHMXIS6.htm](pathfinder-bestiary-items/kjpNSY1WlZHMXIS6.htm)|Powerful Charge|Carga Poderosa|modificada|
|[KjSDkdNQbNUj0pJe.htm](pathfinder-bestiary-items/KjSDkdNQbNUj0pJe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KkDrWaAN2q7y1azB.htm](pathfinder-bestiary-items/KkDrWaAN2q7y1azB.htm)|Jaws|Fauces|modificada|
|[kkOWzP3DmckLh5vC.htm](pathfinder-bestiary-items/kkOWzP3DmckLh5vC.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[KlVVVsJad72RahoI.htm](pathfinder-bestiary-items/KlVVVsJad72RahoI.htm)|Javelin|Javelin|modificada|
|[KLX51VJomJOpNAgE.htm](pathfinder-bestiary-items/KLX51VJomJOpNAgE.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[kMmbg9uYz80oPVxp.htm](pathfinder-bestiary-items/kMmbg9uYz80oPVxp.htm)|Crossbow Precision|Precisión con ballesta|modificada|
|[kmUfrNR3jYGHG5co.htm](pathfinder-bestiary-items/kmUfrNR3jYGHG5co.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[kN3B1jnfTS7LsbUS.htm](pathfinder-bestiary-items/kN3B1jnfTS7LsbUS.htm)|Javelin|Javelin|modificada|
|[kN3GBlB8fFVIQvRu.htm](pathfinder-bestiary-items/kN3GBlB8fFVIQvRu.htm)|Falchion|Falchion|modificada|
|[kNOnudI7TvbeSrjS.htm](pathfinder-bestiary-items/kNOnudI7TvbeSrjS.htm)|Fist|Puño|modificada|
|[KNWnF8Mev9iFqlOl.htm](pathfinder-bestiary-items/KNWnF8Mev9iFqlOl.htm)|Jaws|Fauces|modificada|
|[Ko6ZjYsY5A063K9J.htm](pathfinder-bestiary-items/Ko6ZjYsY5A063K9J.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KoI4JtfGIqzDY32e.htm](pathfinder-bestiary-items/KoI4JtfGIqzDY32e.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[koIcuQqgttVU1vlz.htm](pathfinder-bestiary-items/koIcuQqgttVU1vlz.htm)|Jaws|Fauces|modificada|
|[kOpD7rFhEbxHb0Eb.htm](pathfinder-bestiary-items/kOpD7rFhEbxHb0Eb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KpFmMp8bJz2ZkMQN.htm](pathfinder-bestiary-items/KpFmMp8bJz2ZkMQN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kPkRH8AsC6BaXLPY.htm](pathfinder-bestiary-items/kPkRH8AsC6BaXLPY.htm)|Claw|Garra|modificada|
|[Kpl3lLjfHM0hWhT5.htm](pathfinder-bestiary-items/Kpl3lLjfHM0hWhT5.htm)|Final Spite|Final Spite|modificada|
|[kppTcx5TylWkpfF1.htm](pathfinder-bestiary-items/kppTcx5TylWkpfF1.htm)|Light Vulnerability|Vulnerabilidad a la luz|modificada|
|[kpuj3LrmPskjllD8.htm](pathfinder-bestiary-items/kpuj3LrmPskjllD8.htm)|Harmonize|Armonizar|modificada|
|[kq0boAEZsA01NsEH.htm](pathfinder-bestiary-items/kq0boAEZsA01NsEH.htm)|Powerful Dive|Picado poderoso|modificada|
|[KQARbveR4b7ZZ6yz.htm](pathfinder-bestiary-items/KQARbveR4b7ZZ6yz.htm)|Stunning Shock|Electrizante|modificada|
|[KqNNUdMvi8dUvAxl.htm](pathfinder-bestiary-items/KqNNUdMvi8dUvAxl.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KQrGNPdR20X4aQo8.htm](pathfinder-bestiary-items/KQrGNPdR20X4aQo8.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[KR3Cm5a8kO5abrJc.htm](pathfinder-bestiary-items/KR3Cm5a8kO5abrJc.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[krB0fRntUZ4twRdd.htm](pathfinder-bestiary-items/krB0fRntUZ4twRdd.htm)|Crumble|Desmoronarse|modificada|
|[krINP0tRSuTbpzys.htm](pathfinder-bestiary-items/krINP0tRSuTbpzys.htm)|Water-Bound|Ligado al agua|modificada|
|[KrjSsDuS9l6Y69eQ.htm](pathfinder-bestiary-items/KrjSsDuS9l6Y69eQ.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[ksFT2wzECyEtNKFL.htm](pathfinder-bestiary-items/ksFT2wzECyEtNKFL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ksGHYkwsnnxc2Vmk.htm](pathfinder-bestiary-items/ksGHYkwsnnxc2Vmk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kswB083sGau5ZaJ5.htm](pathfinder-bestiary-items/kswB083sGau5ZaJ5.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[Kt4KG0QuJD0Qf4kp.htm](pathfinder-bestiary-items/Kt4KG0QuJD0Qf4kp.htm)|Climb Stone|Trepar por la piedra|modificada|
|[ktAqGeswWwl10ORn.htm](pathfinder-bestiary-items/ktAqGeswWwl10ORn.htm)|Quick Stow|Estiba Rápida|modificada|
|[kTCYz9Dnx9Zw1RHa.htm](pathfinder-bestiary-items/kTCYz9Dnx9Zw1RHa.htm)|Jaws|Fauces|modificada|
|[kUNkiqNpajCyvKVy.htm](pathfinder-bestiary-items/kUNkiqNpajCyvKVy.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[KuURUnbzeAf0EaR1.htm](pathfinder-bestiary-items/KuURUnbzeAf0EaR1.htm)|Catch Rock|Atrapar roca|modificada|
|[kv1mRTVtv9mHLftL.htm](pathfinder-bestiary-items/kv1mRTVtv9mHLftL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kvbp3xXviiIXnVhS.htm](pathfinder-bestiary-items/kvbp3xXviiIXnVhS.htm)|Predatory Grab|Agarre depredador|modificada|
|[kvPHtl3puURCTaGk.htm](pathfinder-bestiary-items/kvPHtl3puURCTaGk.htm)|Digestive Spew|Vómitos digestivos|modificada|
|[kwMV2uj1jCdvIm7J.htm](pathfinder-bestiary-items/kwMV2uj1jCdvIm7J.htm)|Beard|Barba|modificada|
|[KWrM4BkUQ8ldHG0I.htm](pathfinder-bestiary-items/KWrM4BkUQ8ldHG0I.htm)|Eagle Dive|Picado de águila|modificada|
|[kXAwzMH9r670KlCp.htm](pathfinder-bestiary-items/kXAwzMH9r670KlCp.htm)|Glaive|Glaive|modificada|
|[kXKhHfS3HFvs1QoO.htm](pathfinder-bestiary-items/kXKhHfS3HFvs1QoO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KxNiF9bYl6kA6FRd.htm](pathfinder-bestiary-items/KxNiF9bYl6kA6FRd.htm)|Bellowing Command|Orden imperiosa|modificada|
|[KXqIPpRkKZEFXqNo.htm](pathfinder-bestiary-items/KXqIPpRkKZEFXqNo.htm)|Divine Armament|Armamento divino|modificada|
|[kxRli3zYiks3nckW.htm](pathfinder-bestiary-items/kxRli3zYiks3nckW.htm)|Grab|Agarrado|modificada|
|[KYLDzjAZn2ClEtPA.htm](pathfinder-bestiary-items/KYLDzjAZn2ClEtPA.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[KyPmodj8yfI4cI99.htm](pathfinder-bestiary-items/KyPmodj8yfI4cI99.htm)|Devour Soul|Devorar alma|modificada|
|[KYq91Bu5HDnQoSkG.htm](pathfinder-bestiary-items/KYq91Bu5HDnQoSkG.htm)|Club|Club|modificada|
|[KyvMw6gZ8XlbMZI6.htm](pathfinder-bestiary-items/KyvMw6gZ8XlbMZI6.htm)|Quick Bomber|Bombardero rápido|modificada|
|[kyw9lRkA8ktftjsi.htm](pathfinder-bestiary-items/kyw9lRkA8ktftjsi.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[KZ4sC23Z3r4PS4V6.htm](pathfinder-bestiary-items/KZ4sC23Z3r4PS4V6.htm)|Telekinetic Defense|Telekinetic Defense|modificada|
|[kZEJMwLRJaBvJMSU.htm](pathfinder-bestiary-items/kZEJMwLRJaBvJMSU.htm)|Change Shape|Change Shape|modificada|
|[KZgYdjuQuicrfTgT.htm](pathfinder-bestiary-items/KZgYdjuQuicrfTgT.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[KzNMNX33tOGWTxJY.htm](pathfinder-bestiary-items/KzNMNX33tOGWTxJY.htm)|Constrict|Restringir|modificada|
|[kZqLmEhJGdtImTFg.htm](pathfinder-bestiary-items/kZqLmEhJGdtImTFg.htm)|Push or Pull 5 feet|Empujar o tirar 5 pies|modificada|
|[L0FrL7jDYSHuRsdS.htm](pathfinder-bestiary-items/L0FrL7jDYSHuRsdS.htm)|Woodland Stride|Paso forestal|modificada|
|[L107mBB0Pcsz3T2q.htm](pathfinder-bestiary-items/L107mBB0Pcsz3T2q.htm)|Lurking Death|La muerte acecha|modificada|
|[L1NeMv3ZDcttKLw2.htm](pathfinder-bestiary-items/L1NeMv3ZDcttKLw2.htm)|Snatch|Arrebatar|modificada|
|[l279FtexxY5UgeM3.htm](pathfinder-bestiary-items/l279FtexxY5UgeM3.htm)|Horn|Cuerno|modificada|
|[l2XpWL7iwCvRPkHe.htm](pathfinder-bestiary-items/l2XpWL7iwCvRPkHe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[L4dKqPtdmyspmZtd.htm](pathfinder-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|Fauces|modificada|
|[l4eldMZrkZle4K3Y.htm](pathfinder-bestiary-items/l4eldMZrkZle4K3Y.htm)|Horns|Cuernos|modificada|
|[L6gROwj7mQVM2d4k.htm](pathfinder-bestiary-items/L6gROwj7mQVM2d4k.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[l6OEi0DzuPlz6OeT.htm](pathfinder-bestiary-items/l6OEi0DzuPlz6OeT.htm)|Wrap in Coils|Retorcer en Retorcer|modificada|
|[l6oPZE6Gcn9lJVoU.htm](pathfinder-bestiary-items/l6oPZE6Gcn9lJVoU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[L7wqc9Ph3bPMKfRq.htm](pathfinder-bestiary-items/L7wqc9Ph3bPMKfRq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[L8QPL1ttGhwzsJOz.htm](pathfinder-bestiary-items/L8QPL1ttGhwzsJOz.htm)|Weak Acid|Ácido débil|modificada|
|[l8txqobaScKbGksB.htm](pathfinder-bestiary-items/l8txqobaScKbGksB.htm)|Constant Spells|Constant Spells|modificada|
|[L8ZBoNle0ekONgCk.htm](pathfinder-bestiary-items/L8ZBoNle0ekONgCk.htm)|Sphere of Creation|Esfera de la creación|modificada|
|[L9mTxpzjmfkBOHto.htm](pathfinder-bestiary-items/L9mTxpzjmfkBOHto.htm)|Grab|Agarrado|modificada|
|[la6YsHRtpkwWWHV7.htm](pathfinder-bestiary-items/la6YsHRtpkwWWHV7.htm)|Tail|Tail|modificada|
|[LAE8pFe5hAYvWcTH.htm](pathfinder-bestiary-items/LAE8pFe5hAYvWcTH.htm)|Branch|Rama|modificada|
|[LAo739thLOTwv571.htm](pathfinder-bestiary-items/LAo739thLOTwv571.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[laPA1RpDHNNksUzO.htm](pathfinder-bestiary-items/laPA1RpDHNNksUzO.htm)|Claw|Garra|modificada|
|[LaQ2wdVBDXDNbGSR.htm](pathfinder-bestiary-items/LaQ2wdVBDXDNbGSR.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[LBCwTLpo7BDaFfpf.htm](pathfinder-bestiary-items/LBCwTLpo7BDaFfpf.htm)|+4 Status to All Saves vs. Fear|+4 situación a todas las salvaciones contra miedo.|modificada|
|[LbGQE0qrzbORDYH9.htm](pathfinder-bestiary-items/LbGQE0qrzbORDYH9.htm)|Sudden Charge|Carga súbita|modificada|
|[lBuNOTGbx6zBFceT.htm](pathfinder-bestiary-items/lBuNOTGbx6zBFceT.htm)|Desert Wind|Viento del Desierto|modificada|
|[LCFMKpxw0iDxWGXu.htm](pathfinder-bestiary-items/LCFMKpxw0iDxWGXu.htm)|Spore Cloud|Nube de esporas|modificada|
|[LcpFmBryvzaDNDcL.htm](pathfinder-bestiary-items/LcpFmBryvzaDNDcL.htm)|Perfect Aim|Puntería perfecta|modificada|
|[lcQfDBQrhLj0Evxf.htm](pathfinder-bestiary-items/lcQfDBQrhLj0Evxf.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[LD8TK8mGL7ubLU4I.htm](pathfinder-bestiary-items/LD8TK8mGL7ubLU4I.htm)|Stinger|Aguijón|modificada|
|[LdDQZazfe2aGupQ2.htm](pathfinder-bestiary-items/LdDQZazfe2aGupQ2.htm)|Constant Spells|Constant Spells|modificada|
|[LDExFIGLfarrbJQa.htm](pathfinder-bestiary-items/LDExFIGLfarrbJQa.htm)|Speed Surge|Arranque de velocidad|modificada|
|[LDLJlCLplbi1fPsU.htm](pathfinder-bestiary-items/LDLJlCLplbi1fPsU.htm)|Disperse|Dispersarse|modificada|
|[LE4LVjvLH5qWmZJo.htm](pathfinder-bestiary-items/LE4LVjvLH5qWmZJo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lenb7f8O728YEnIr.htm](pathfinder-bestiary-items/lenb7f8O728YEnIr.htm)|Grab|Agarrado|modificada|
|[LfFpWQJ2gEIy0oiZ.htm](pathfinder-bestiary-items/LfFpWQJ2gEIy0oiZ.htm)|Fist|Puño|modificada|
|[LfhW8MrjdGdRlQSS.htm](pathfinder-bestiary-items/LfhW8MrjdGdRlQSS.htm)|Smoke Vision|Visión de Humo|modificada|
|[lFKVQJSD5lmRIdIT.htm](pathfinder-bestiary-items/lFKVQJSD5lmRIdIT.htm)|Illusory Retreat|Retirada ilusoria|modificada|
|[LfvR6qODwUM0OxAB.htm](pathfinder-bestiary-items/LfvR6qODwUM0OxAB.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[LfYdkPPdY3ZvMyEk.htm](pathfinder-bestiary-items/LfYdkPPdY3ZvMyEk.htm)|Grab|Agarrado|modificada|
|[lg0ZOYopXDTR0Atj.htm](pathfinder-bestiary-items/lg0ZOYopXDTR0Atj.htm)|Fangs|Colmillos|modificada|
|[LG7PBXCgNj0UWIb8.htm](pathfinder-bestiary-items/LG7PBXCgNj0UWIb8.htm)|Root|Enraizarse|modificada|
|[LGBzL3vv4QNzTFJ1.htm](pathfinder-bestiary-items/LGBzL3vv4QNzTFJ1.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[LGlJmmg8EvpeOhGW.htm](pathfinder-bestiary-items/LGlJmmg8EvpeOhGW.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[lgo6Chb44AWipQBi.htm](pathfinder-bestiary-items/lgo6Chb44AWipQBi.htm)|Constrict|Restringir|modificada|
|[lhQcYifFNlqvbw4G.htm](pathfinder-bestiary-items/lhQcYifFNlqvbw4G.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[LIdmD7VqztundfTh.htm](pathfinder-bestiary-items/LIdmD7VqztundfTh.htm)|Beak|Beak|modificada|
|[lieMSuY1PxRJK0CX.htm](pathfinder-bestiary-items/lieMSuY1PxRJK0CX.htm)|Fist|Puño|modificada|
|[LiSV4xTOMgWGk32H.htm](pathfinder-bestiary-items/LiSV4xTOMgWGk32H.htm)|Beak|Beak|modificada|
|[lizgWV0pNr6rV2SG.htm](pathfinder-bestiary-items/lizgWV0pNr6rV2SG.htm)|Tail|Tail|modificada|
|[Lj4RQQp6b9XUoSUb.htm](pathfinder-bestiary-items/Lj4RQQp6b9XUoSUb.htm)|Wavesense 30 feet|Wavesense 30 pies|modificada|
|[lJGzVTdIOY1p14kj.htm](pathfinder-bestiary-items/lJGzVTdIOY1p14kj.htm)|All-Around Vision|All-Around Vision|modificada|
|[LJKNy957UOv4LqE5.htm](pathfinder-bestiary-items/LJKNy957UOv4LqE5.htm)|Change Shape|Change Shape|modificada|
|[lkaHyBENnIei6Qaf.htm](pathfinder-bestiary-items/lkaHyBENnIei6Qaf.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[LkBIOwt4NW6BfYFE.htm](pathfinder-bestiary-items/LkBIOwt4NW6BfYFE.htm)|Psychic Static Aura|Aura de estática psíquica.|modificada|
|[lKC4So6E50bV5RRz.htm](pathfinder-bestiary-items/lKC4So6E50bV5RRz.htm)|Radiant Feathers|Radiant Feathers|modificada|
|[Lkl0ZskZuJbD0vlP.htm](pathfinder-bestiary-items/Lkl0ZskZuJbD0vlP.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[lKQyAi6slANJZp0U.htm](pathfinder-bestiary-items/lKQyAi6slANJZp0U.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LkSajmBAzMTZ4Wk4.htm](pathfinder-bestiary-items/LkSajmBAzMTZ4Wk4.htm)|Beak|Beak|modificada|
|[lL38DigkATyHvflx.htm](pathfinder-bestiary-items/lL38DigkATyHvflx.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[lLANYeMvjjIbtdDB.htm](pathfinder-bestiary-items/lLANYeMvjjIbtdDB.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[LLDqAQlq7w0YWyO0.htm](pathfinder-bestiary-items/LLDqAQlq7w0YWyO0.htm)|Talon|Talon|modificada|
|[LLFFYMBrnhFoDMRh.htm](pathfinder-bestiary-items/LLFFYMBrnhFoDMRh.htm)|Crumble|Desmoronarse|modificada|
|[LLJiTLsfaDzD1nZo.htm](pathfinder-bestiary-items/LLJiTLsfaDzD1nZo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lLkkEbmwGunnyE2e.htm](pathfinder-bestiary-items/lLkkEbmwGunnyE2e.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[lLsSuZ6Tev4yEFrU.htm](pathfinder-bestiary-items/lLsSuZ6Tev4yEFrU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lLwtc5h1JK4M6Nzi.htm](pathfinder-bestiary-items/lLwtc5h1JK4M6Nzi.htm)|Knockdown|Derribo|modificada|
|[LmcmQNgsnrAuSu2l.htm](pathfinder-bestiary-items/LmcmQNgsnrAuSu2l.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[LMiVMxKOuhUjGd45.htm](pathfinder-bestiary-items/LMiVMxKOuhUjGd45.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lmmVhyJxHrIHi4Je.htm](pathfinder-bestiary-items/lmmVhyJxHrIHi4Je.htm)|Antenna|Antena|modificada|
|[lmrBUipZFBXl2NgJ.htm](pathfinder-bestiary-items/lmrBUipZFBXl2NgJ.htm)|Claw|Garra|modificada|
|[LmRU9VN6HvnRCcJO.htm](pathfinder-bestiary-items/LmRU9VN6HvnRCcJO.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Ln5FLSMEoZNQuxUn.htm](pathfinder-bestiary-items/Ln5FLSMEoZNQuxUn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LnCX5HAN8VUMv1f5.htm](pathfinder-bestiary-items/LnCX5HAN8VUMv1f5.htm)|Hop-Dodge|Brinco evasivo|modificada|
|[LNJkhJSFpg8gvkwp.htm](pathfinder-bestiary-items/LNJkhJSFpg8gvkwp.htm)|Heatsight 60 feet|Heatsight 60 pies|modificada|
|[lnlNJpjHNjw06Wzi.htm](pathfinder-bestiary-items/lnlNJpjHNjw06Wzi.htm)|Tusk|Tusk|modificada|
|[lOGhyEufGuD2T8aX.htm](pathfinder-bestiary-items/lOGhyEufGuD2T8aX.htm)|Jaws|Fauces|modificada|
|[lOUQcokkfOtGgPNG.htm](pathfinder-bestiary-items/lOUQcokkfOtGgPNG.htm)|Jaws|Fauces|modificada|
|[LPgRXjxPg5sOcWEv.htm](pathfinder-bestiary-items/LPgRXjxPg5sOcWEv.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[LpOv4VRyaN38UswN.htm](pathfinder-bestiary-items/LpOv4VRyaN38UswN.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[lPRNx4hWnBppqbVp.htm](pathfinder-bestiary-items/lPRNx4hWnBppqbVp.htm)|Glaive|Glaive|modificada|
|[lPUoJQI3bTRTV4ct.htm](pathfinder-bestiary-items/lPUoJQI3bTRTV4ct.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[lQHT37uBw4LDc2OY.htm](pathfinder-bestiary-items/lQHT37uBw4LDc2OY.htm)|Inexorable March|Marcha inexorable|modificada|
|[lQKrggSbzpvqq4YB.htm](pathfinder-bestiary-items/lQKrggSbzpvqq4YB.htm)|Inspiration|Inspiración|modificada|
|[LqM7JU0cOL7itoch.htm](pathfinder-bestiary-items/LqM7JU0cOL7itoch.htm)|Change Shape|Change Shape|modificada|
|[lqMgsu5wZAOsEwK2.htm](pathfinder-bestiary-items/lqMgsu5wZAOsEwK2.htm)|Giant Viper Venom|Veneno de Víbora Gigante|modificada|
|[lQO6RbVplN7WWnna.htm](pathfinder-bestiary-items/lQO6RbVplN7WWnna.htm)|Constant Spells|Constant Spells|modificada|
|[LQPUOdcFtZk2FzJz.htm](pathfinder-bestiary-items/LQPUOdcFtZk2FzJz.htm)|Jaws|Fauces|modificada|
|[lqWh4YqCJffKqcoX.htm](pathfinder-bestiary-items/lqWh4YqCJffKqcoX.htm)|Rapier|Estoque|modificada|
|[lRaDhQKMgeR6IjAB.htm](pathfinder-bestiary-items/lRaDhQKMgeR6IjAB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LRAh1zPRfZ1jQ8a6.htm](pathfinder-bestiary-items/LRAh1zPRfZ1jQ8a6.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[LRoHaUgN8RugcqOi.htm](pathfinder-bestiary-items/LRoHaUgN8RugcqOi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lrqS9oUNMEepLga2.htm](pathfinder-bestiary-items/lrqS9oUNMEepLga2.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[LRT2YJWR1S18u4vp.htm](pathfinder-bestiary-items/LRT2YJWR1S18u4vp.htm)|Shortsword|Espada corta|modificada|
|[LRVsZzLZZ4VCKy1h.htm](pathfinder-bestiary-items/LRVsZzLZZ4VCKy1h.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LRxNywKFwiFUofE7.htm](pathfinder-bestiary-items/LRxNywKFwiFUofE7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lRYUK2abQpyhIlbv.htm](pathfinder-bestiary-items/lRYUK2abQpyhIlbv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lszUCBGYOxZzhPEU.htm](pathfinder-bestiary-items/lszUCBGYOxZzhPEU.htm)|Tremorsense (Imprecise) 90 feet|Sentido del Temblor (Impreciso) 90 pies|modificada|
|[Lt1Gndh1LAlY6Q2u.htm](pathfinder-bestiary-items/Lt1Gndh1LAlY6Q2u.htm)|Greataxe|Greataxe|modificada|
|[lT2fWWJ081ynQOOQ.htm](pathfinder-bestiary-items/lT2fWWJ081ynQOOQ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Lt7N9rIlyS76DtRj.htm](pathfinder-bestiary-items/Lt7N9rIlyS76DtRj.htm)|Tail|Tail|modificada|
|[lTAUfkDQ3VVnBakO.htm](pathfinder-bestiary-items/lTAUfkDQ3VVnBakO.htm)|Frightful Presence|Frightful Presence|modificada|
|[ltUht9L2rQGxrSt7.htm](pathfinder-bestiary-items/ltUht9L2rQGxrSt7.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[ltvMN7K0itu2l3WZ.htm](pathfinder-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|Colmillos de Serpiente|modificada|
|[LTwSnVSVWZeWrLAC.htm](pathfinder-bestiary-items/LTwSnVSVWZeWrLAC.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[LUK2Wptf0GJyjbxC.htm](pathfinder-bestiary-items/LUK2Wptf0GJyjbxC.htm)|Staff|Báculo|modificada|
|[LUPkzJMY0W8x0Vsd.htm](pathfinder-bestiary-items/LUPkzJMY0W8x0Vsd.htm)|Fist|Puño|modificada|
|[lUtqPs1qhhuLgmEE.htm](pathfinder-bestiary-items/lUtqPs1qhhuLgmEE.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[luxLK4RgZT83aYTa.htm](pathfinder-bestiary-items/luxLK4RgZT83aYTa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LuxUX599os0xTuFk.htm](pathfinder-bestiary-items/LuxUX599os0xTuFk.htm)|Mind Crush|Mind Crush|modificada|
|[luyr4NvU4euUL7Dh.htm](pathfinder-bestiary-items/luyr4NvU4euUL7Dh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LUZas9EK4qLmUsox.htm](pathfinder-bestiary-items/LUZas9EK4qLmUsox.htm)|Slowing Frost|Gélida lentificadora|modificada|
|[lV6uRtAWXQ1eYrO3.htm](pathfinder-bestiary-items/lV6uRtAWXQ1eYrO3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[lvw0wlyhNfMvvQ6Q.htm](pathfinder-bestiary-items/lvw0wlyhNfMvvQ6Q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LwPsr1H56GuAEnbm.htm](pathfinder-bestiary-items/LwPsr1H56GuAEnbm.htm)|Jaws|Fauces|modificada|
|[LwT17u6xCyB2oygz.htm](pathfinder-bestiary-items/LwT17u6xCyB2oygz.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[lwUnUfQ1EWHSs1xW.htm](pathfinder-bestiary-items/lwUnUfQ1EWHSs1xW.htm)|Gust|Gust|modificada|
|[LxhUmOCBKZi7FNfa.htm](pathfinder-bestiary-items/LxhUmOCBKZi7FNfa.htm)|Betraying Touch|Toque traicionero|modificada|
|[LxnGNDFd9yCEkOAt.htm](pathfinder-bestiary-items/LxnGNDFd9yCEkOAt.htm)|Pounce|Abalanzarse|modificada|
|[lxNScLMLEw8NbtpU.htm](pathfinder-bestiary-items/lxNScLMLEw8NbtpU.htm)|Knockdown (Wolf Form)|Derribo (Forma de lobo)|modificada|
|[lYeYZZa66BG9v3KY.htm](pathfinder-bestiary-items/lYeYZZa66BG9v3KY.htm)|Spectral Hand|Mano espectral|modificada|
|[lYpvPmdtgwwrvYGd.htm](pathfinder-bestiary-items/lYpvPmdtgwwrvYGd.htm)|Tail|Tail|modificada|
|[lYUnEWGhnlYFvH6B.htm](pathfinder-bestiary-items/lYUnEWGhnlYFvH6B.htm)|Swallow Whole|Engullir Todo|modificada|
|[LZ3htI7JRHvcwAhU.htm](pathfinder-bestiary-items/LZ3htI7JRHvcwAhU.htm)|Double Opportunity|Doble Oportunidad|modificada|
|[LzMPMpH1MlWA2nAt.htm](pathfinder-bestiary-items/LzMPMpH1MlWA2nAt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[lzmS1xCoWnHVa39l.htm](pathfinder-bestiary-items/lzmS1xCoWnHVa39l.htm)|Darkvision|Visión en la oscuridad|modificada|
|[M05pu8jIHs0qEKzP.htm](pathfinder-bestiary-items/M05pu8jIHs0qEKzP.htm)|Hand|Mano|modificada|
|[M0BuH1pWhtWsVIYm.htm](pathfinder-bestiary-items/M0BuH1pWhtWsVIYm.htm)|Worry|Worry|modificada|
|[m0piXEdTBGvW1oIY.htm](pathfinder-bestiary-items/m0piXEdTBGvW1oIY.htm)|Push 10 feet|Empuje 10 pies|modificada|
|[M1Iy4WjF2HRmGv0e.htm](pathfinder-bestiary-items/M1Iy4WjF2HRmGv0e.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[M1Uh0E4VLZObTo3I.htm](pathfinder-bestiary-items/M1Uh0E4VLZObTo3I.htm)|Buck|Encabritarse|modificada|
|[M203mlcBNFvAqyIJ.htm](pathfinder-bestiary-items/M203mlcBNFvAqyIJ.htm)|Burn Alive|Burn Alive|modificada|
|[M22baNr3EaRn3JZr.htm](pathfinder-bestiary-items/M22baNr3EaRn3JZr.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[m25t326n1Ovt0t5U.htm](pathfinder-bestiary-items/m25t326n1Ovt0t5U.htm)|Negative Healing|Curación negativa|modificada|
|[m2aEAnMnepSKN57G.htm](pathfinder-bestiary-items/m2aEAnMnepSKN57G.htm)|Head Regrowth|Crecer cabezas|modificada|
|[M2JcdJZTsdKY31Ed.htm](pathfinder-bestiary-items/M2JcdJZTsdKY31Ed.htm)|Constant Spells|Constant Spells|modificada|
|[m2lc6stQx0fMmqPO.htm](pathfinder-bestiary-items/m2lc6stQx0fMmqPO.htm)|Knockdown|Derribo|modificada|
|[M2ncaZFg1bVL9O6X.htm](pathfinder-bestiary-items/M2ncaZFg1bVL9O6X.htm)|Tail|Tail|modificada|
|[M2XLTTlSc1yU99ym.htm](pathfinder-bestiary-items/M2XLTTlSc1yU99ym.htm)|Darkvision|Visión en la oscuridad|modificada|
|[m3iQuuvCSejqA1Ue.htm](pathfinder-bestiary-items/m3iQuuvCSejqA1Ue.htm)|Grab|Agarrado|modificada|
|[M3QwWIU2rzRHWndo.htm](pathfinder-bestiary-items/M3QwWIU2rzRHWndo.htm)|Mimic Object|Objeto Mímico|modificada|
|[M4f06NOUIcSyQNr2.htm](pathfinder-bestiary-items/M4f06NOUIcSyQNr2.htm)|Regeneration 30 (Deactivated by Acid or Fire)|Regeneración 30 (Desactivado por Ácido o Fuego).|modificada|
|[m53HcXgUGgipalh0.htm](pathfinder-bestiary-items/m53HcXgUGgipalh0.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[m5FtOPWhFYV34o9q.htm](pathfinder-bestiary-items/m5FtOPWhFYV34o9q.htm)|Tail|Tail|modificada|
|[m5n5I6Z3Upfe9pSe.htm](pathfinder-bestiary-items/m5n5I6Z3Upfe9pSe.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[m6CbK52GoU471gez.htm](pathfinder-bestiary-items/m6CbK52GoU471gez.htm)|Drain Soul Cage|Drenar Jaula de Almas|modificada|
|[m6E1gSnAhXp72yBC.htm](pathfinder-bestiary-items/m6E1gSnAhXp72yBC.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[M6l0kUINWUBkdXJ1.htm](pathfinder-bestiary-items/M6l0kUINWUBkdXJ1.htm)|Constant Spells|Constant Spells|modificada|
|[M6LXusA9trhjoaTp.htm](pathfinder-bestiary-items/M6LXusA9trhjoaTp.htm)|Breath Weapon|Breath Weapon|modificada|
|[M6OcH3aIVdTWr7gq.htm](pathfinder-bestiary-items/M6OcH3aIVdTWr7gq.htm)|Tail|Tail|modificada|
|[m6WhfbsOdnetGUAE.htm](pathfinder-bestiary-items/m6WhfbsOdnetGUAE.htm)|Camouflage|Camuflaje|modificada|
|[M6zs0ZmCJlyphS5N.htm](pathfinder-bestiary-items/M6zs0ZmCJlyphS5N.htm)|Fangs|Colmillos|modificada|
|[M7mGcRgzVr9C3wsW.htm](pathfinder-bestiary-items/M7mGcRgzVr9C3wsW.htm)|Telekinetic Assault|Asalto telecinético|modificada|
|[M7sBCYj1yfPikRDu.htm](pathfinder-bestiary-items/M7sBCYj1yfPikRDu.htm)|Grab|Agarrado|modificada|
|[M8RNAgw0OgnUOpKP.htm](pathfinder-bestiary-items/M8RNAgw0OgnUOpKP.htm)|Polearm Critical Specialization|Polearm Critical Specialization|modificada|
|[M99t6H3so0fp0wTH.htm](pathfinder-bestiary-items/M99t6H3so0fp0wTH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[m9xKqsUED5sH6vqA.htm](pathfinder-bestiary-items/m9xKqsUED5sH6vqA.htm)|Change Shape|Change Shape|modificada|
|[mAzjVKaBRjpMwszN.htm](pathfinder-bestiary-items/mAzjVKaBRjpMwszN.htm)|Telepathy (Touch)|Telepatía (Tacto)|modificada|
|[mB1mKRbvNa6edeUB.htm](pathfinder-bestiary-items/mB1mKRbvNa6edeUB.htm)|Sandblast|Sandblast|modificada|
|[Mb8JbA884rcKgMkO.htm](pathfinder-bestiary-items/Mb8JbA884rcKgMkO.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[mB9tYATe35GSN6Tm.htm](pathfinder-bestiary-items/mB9tYATe35GSN6Tm.htm)|Starlight Blast|Estallido estelar|modificada|
|[mbDpJL0NCrFdM2bb.htm](pathfinder-bestiary-items/mbDpJL0NCrFdM2bb.htm)|Explosion|Explosión|modificada|
|[MBIM9DzCh32Rrta8.htm](pathfinder-bestiary-items/MBIM9DzCh32Rrta8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MbjqqpFVm0Xwv00X.htm](pathfinder-bestiary-items/MbjqqpFVm0Xwv00X.htm)|Tremorsense (Imprecise) 100 feet|Sentido del Temblor (Impreciso) 100 pies|modificada|
|[mbkNd6gFHEqUiVeO.htm](pathfinder-bestiary-items/mbkNd6gFHEqUiVeO.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[mbrIsofRTRy9BMDp.htm](pathfinder-bestiary-items/mbrIsofRTRy9BMDp.htm)|Fist|Puño|modificada|
|[MCe44ome2cf0lQ8W.htm](pathfinder-bestiary-items/MCe44ome2cf0lQ8W.htm)|Pull Apart|Despedazar|modificada|
|[MCgl5uKpwtTh48E4.htm](pathfinder-bestiary-items/MCgl5uKpwtTh48E4.htm)|Glaive|Glaive|modificada|
|[Md6z01uI0caPW7Vl.htm](pathfinder-bestiary-items/Md6z01uI0caPW7Vl.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[mDnVfeIEWGgfP7Xq.htm](pathfinder-bestiary-items/mDnVfeIEWGgfP7Xq.htm)|Repository of Lore|Repository of Lore|modificada|
|[mdqK7RuSjtezVyS8.htm](pathfinder-bestiary-items/mdqK7RuSjtezVyS8.htm)|Filth Wave|Ola de suciedad|modificada|
|[mE1ez2SoxG8F6mVq.htm](pathfinder-bestiary-items/mE1ez2SoxG8F6mVq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mE1G6otJjruqyHkD.htm](pathfinder-bestiary-items/mE1G6otJjruqyHkD.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[MEAPfylvHHHbTqOf.htm](pathfinder-bestiary-items/MEAPfylvHHHbTqOf.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[MEjtnkYdPhtWGyzF.htm](pathfinder-bestiary-items/MEjtnkYdPhtWGyzF.htm)|Coiled Opportunity (Special)|Oportunidad retorcida (Especial)|modificada|
|[MEMnTHEnlQDgPkfq.htm](pathfinder-bestiary-items/MEMnTHEnlQDgPkfq.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[mf1G3pa8hN8LNJ4n.htm](pathfinder-bestiary-items/mf1G3pa8hN8LNJ4n.htm)|Leaf|Hoja|modificada|
|[MF3wGJmHdgsg1PY3.htm](pathfinder-bestiary-items/MF3wGJmHdgsg1PY3.htm)|Beak|Beak|modificada|
|[Mf6oyfENXSeMgM7o.htm](pathfinder-bestiary-items/Mf6oyfENXSeMgM7o.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Mf7Y2TxYr2e5o2Ht.htm](pathfinder-bestiary-items/Mf7Y2TxYr2e5o2Ht.htm)|Buck|Encabritarse|modificada|
|[MfBsuZaAMldux4Xb.htm](pathfinder-bestiary-items/MfBsuZaAMldux4Xb.htm)|Javelin|Javelin|modificada|
|[mFf9kwHNM5V0Un7y.htm](pathfinder-bestiary-items/mFf9kwHNM5V0Un7y.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[mfPiHF5tCFHLTxDa.htm](pathfinder-bestiary-items/mfPiHF5tCFHLTxDa.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[MFz1cM1MqbifkyHH.htm](pathfinder-bestiary-items/MFz1cM1MqbifkyHH.htm)|Vulnerable to Smoke|Vulnerabilidad al humo|modificada|
|[mgb2nO0WkJsnZogZ.htm](pathfinder-bestiary-items/mgb2nO0WkJsnZogZ.htm)|Lava Affinity|Afinidad por la lava|modificada|
|[mGL7tbh4lfdWIVfJ.htm](pathfinder-bestiary-items/mGL7tbh4lfdWIVfJ.htm)|Hidden Movement|Movimiento oculto|modificada|
|[MGX1U8Kv59BVYAOI.htm](pathfinder-bestiary-items/MGX1U8Kv59BVYAOI.htm)|Push or Pull 10 feet|Empuja o tira 3 metros|modificada|
|[mh9LAEOA50HTVL1T.htm](pathfinder-bestiary-items/mh9LAEOA50HTVL1T.htm)|Jaws|Fauces|modificada|
|[mhBBm02GHgEZ30NR.htm](pathfinder-bestiary-items/mhBBm02GHgEZ30NR.htm)|Web Trap|Trampa telara|modificada|
|[MHobLN5Z39cpkjWr.htm](pathfinder-bestiary-items/MHobLN5Z39cpkjWr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MhpbBKngUDFRNsQF.htm](pathfinder-bestiary-items/MhpbBKngUDFRNsQF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mHS7Uj0GKGKhQiNo.htm](pathfinder-bestiary-items/mHS7Uj0GKGKhQiNo.htm)|Orc Necksplitter|Orc Necksplitter|modificada|
|[MJ00yNypQKm6JLmB.htm](pathfinder-bestiary-items/MJ00yNypQKm6JLmB.htm)|Claw|Garra|modificada|
|[mJ4kL0oQk4xWhXtX.htm](pathfinder-bestiary-items/mJ4kL0oQk4xWhXtX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Mj7eIolH2KmKRos4.htm](pathfinder-bestiary-items/Mj7eIolH2KmKRos4.htm)|Wyvern Venom|Wyvern Veneno|modificada|
|[mjClQEAEbQfY2Zfr.htm](pathfinder-bestiary-items/mjClQEAEbQfY2Zfr.htm)|Adamantine Claw|Adamantine Claw|modificada|
|[Mjlek9OZ25Tga7Cg.htm](pathfinder-bestiary-items/Mjlek9OZ25Tga7Cg.htm)|Filth Fever|Filth Fever|modificada|
|[MjvAem9y7U9Z9AnV.htm](pathfinder-bestiary-items/MjvAem9y7U9Z9AnV.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[MJVKsTpwu0QDO6t7.htm](pathfinder-bestiary-items/MJVKsTpwu0QDO6t7.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[mK3REH0Jcz0NP7Bh.htm](pathfinder-bestiary-items/mK3REH0Jcz0NP7Bh.htm)|Vampire Weaknesses|Vampire Weaknesses|modificada|
|[mKaR7LWvVhRIbJCG.htm](pathfinder-bestiary-items/mKaR7LWvVhRIbJCG.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[mkbavR53jwVXw293.htm](pathfinder-bestiary-items/mkbavR53jwVXw293.htm)|Claw|Garra|modificada|
|[mKMByZgGjo3KcHwj.htm](pathfinder-bestiary-items/mKMByZgGjo3KcHwj.htm)|Jaws|Fauces|modificada|
|[MKqjvJ1d8GxOUuDF.htm](pathfinder-bestiary-items/MKqjvJ1d8GxOUuDF.htm)|Tail|Tail|modificada|
|[mkwN7Fgq3DRnThOV.htm](pathfinder-bestiary-items/mkwN7Fgq3DRnThOV.htm)|Consume Flesh|Consumir carne|modificada|
|[mld7PgekazbX4cCh.htm](pathfinder-bestiary-items/mld7PgekazbX4cCh.htm)|Glaive|Glaive|modificada|
|[MlLvVLQolivhuZFD.htm](pathfinder-bestiary-items/MlLvVLQolivhuZFD.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[Mn9ThITOCOXSNkEv.htm](pathfinder-bestiary-items/Mn9ThITOCOXSNkEv.htm)|Regeneration 20 (Deactivated by Chaotic)|Regeneración 20 (Desactivado por Caótico)|modificada|
|[MNeTs8gNujcsFNkv.htm](pathfinder-bestiary-items/MNeTs8gNujcsFNkv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mNfAyKfUwSuMResA.htm](pathfinder-bestiary-items/mNfAyKfUwSuMResA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mnmPiXMTDMbeqRnI.htm](pathfinder-bestiary-items/mnmPiXMTDMbeqRnI.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[mNomS6NBwvlheidl.htm](pathfinder-bestiary-items/mNomS6NBwvlheidl.htm)|Cube Face|Cube Face|modificada|
|[MNTsZcEEgZLgHBMp.htm](pathfinder-bestiary-items/MNTsZcEEgZLgHBMp.htm)|Constrict|Restringir|modificada|
|[mo2rgNZQlmzNFBze.htm](pathfinder-bestiary-items/mo2rgNZQlmzNFBze.htm)|Fist|Puño|modificada|
|[Mo3NNcPkDUwnRiIF.htm](pathfinder-bestiary-items/Mo3NNcPkDUwnRiIF.htm)|Leg|Pierna|modificada|
|[MO9W8NpBALoHHtTS.htm](pathfinder-bestiary-items/MO9W8NpBALoHHtTS.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[MOmmOyY0sUO8EEIX.htm](pathfinder-bestiary-items/MOmmOyY0sUO8EEIX.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[momzHh7zAZRC7Hob.htm](pathfinder-bestiary-items/momzHh7zAZRC7Hob.htm)|Swamp Stride|Swamp Stride|modificada|
|[MPbClVjJK4l020IS.htm](pathfinder-bestiary-items/MPbClVjJK4l020IS.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[MPJDgS7lWLTOqNeT.htm](pathfinder-bestiary-items/MPJDgS7lWLTOqNeT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MpNlJrdPvv9bECNE.htm](pathfinder-bestiary-items/MpNlJrdPvv9bECNE.htm)|Sinful Bite|Muerdemuerde el Pecado|modificada|
|[MQbK2a6aQPbnYNCw.htm](pathfinder-bestiary-items/MQbK2a6aQPbnYNCw.htm)|Negative Healing|Curación negativa|modificada|
|[mqhNvxuxfh9K2nAg.htm](pathfinder-bestiary-items/mqhNvxuxfh9K2nAg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[mrCcKL7iLIAFrAys.htm](pathfinder-bestiary-items/mrCcKL7iLIAFrAys.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[MrCFRuiA29CCYxgb.htm](pathfinder-bestiary-items/MrCFRuiA29CCYxgb.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[mRElwzY41P9cR0SN.htm](pathfinder-bestiary-items/mRElwzY41P9cR0SN.htm)|Jaws|Fauces|modificada|
|[MRMj6jc7E8e5c7UF.htm](pathfinder-bestiary-items/MRMj6jc7E8e5c7UF.htm)|Grab|Agarrado|modificada|
|[mrrLoMpl5WnNGk4e.htm](pathfinder-bestiary-items/mrrLoMpl5WnNGk4e.htm)|Earthbound|Ligado a la tierra|modificada|
|[MrSMiZ1i5groiYX1.htm](pathfinder-bestiary-items/MrSMiZ1i5groiYX1.htm)|Regeneration 15 (Deactivated by Cold Iron)|Regeneración 15 (Desactivado por Cold Iron)|modificada|
|[mS16mfAS5hbK048v.htm](pathfinder-bestiary-items/mS16mfAS5hbK048v.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[mS77pmvBFQBdJrrS.htm](pathfinder-bestiary-items/mS77pmvBFQBdJrrS.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[MScqq1IasW1Ei33T.htm](pathfinder-bestiary-items/MScqq1IasW1Ei33T.htm)|Mandibles|Mandíbulas|modificada|
|[MsksF4NK5nEBIyq6.htm](pathfinder-bestiary-items/MsksF4NK5nEBIyq6.htm)|Tail|Tail|modificada|
|[Msmv4qRjU8FEJbcm.htm](pathfinder-bestiary-items/Msmv4qRjU8FEJbcm.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[msrruiotI1gRnWN9.htm](pathfinder-bestiary-items/msrruiotI1gRnWN9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MTJ6P2pU1320fO7f.htm](pathfinder-bestiary-items/MTJ6P2pU1320fO7f.htm)|Berserk|Bersérker|modificada|
|[mU73p7EYpTXgNAqN.htm](pathfinder-bestiary-items/mU73p7EYpTXgNAqN.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[MuiyXm8KyQF57NaW.htm](pathfinder-bestiary-items/MuiyXm8KyQF57NaW.htm)|Jaws|Fauces|modificada|
|[muQiaR4uAaAcFhJ0.htm](pathfinder-bestiary-items/muQiaR4uAaAcFhJ0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MUtrSZzTqwA22XOa.htm](pathfinder-bestiary-items/MUtrSZzTqwA22XOa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MVhuX4yQFrWGRGUi.htm](pathfinder-bestiary-items/MVhuX4yQFrWGRGUi.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[MVKA8eKi1YTMeHKK.htm](pathfinder-bestiary-items/MVKA8eKi1YTMeHKK.htm)|Powerful Charge|Carga Poderosa|modificada|
|[mvKy4fox7C9GJNq4.htm](pathfinder-bestiary-items/mvKy4fox7C9GJNq4.htm)|Collect Brain|Recolectar cerebro|modificada|
|[mvM0UYDSareH0kWj.htm](pathfinder-bestiary-items/mvM0UYDSareH0kWj.htm)|Aklys|Aklys|modificada|
|[MVOUIn7qGVbFyHG8.htm](pathfinder-bestiary-items/MVOUIn7qGVbFyHG8.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[MVS8VVm9BG19yQpr.htm](pathfinder-bestiary-items/MVS8VVm9BG19yQpr.htm)|Cheek Pouches|Carrilleras|modificada|
|[mw30E878JJp7tVVh.htm](pathfinder-bestiary-items/mw30E878JJp7tVVh.htm)|Tongue|Lengua|modificada|
|[MWKHdXGNx9JkkNqR.htm](pathfinder-bestiary-items/MWKHdXGNx9JkkNqR.htm)|Envisioning|Visualizado|modificada|
|[MWmyo46mgKFCREnk.htm](pathfinder-bestiary-items/MWmyo46mgKFCREnk.htm)|Claw|Garra|modificada|
|[MwNIK1P57HlN0q90.htm](pathfinder-bestiary-items/MwNIK1P57HlN0q90.htm)|Improved Grab|Agarrado mejorado|modificada|
|[MwXQ4IgNkQaCxTcf.htm](pathfinder-bestiary-items/MwXQ4IgNkQaCxTcf.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[mxb42HlUBxVyw38F.htm](pathfinder-bestiary-items/mxb42HlUBxVyw38F.htm)|Rend|Rasgadura|modificada|
|[MxiNEcQrKuXmCuWD.htm](pathfinder-bestiary-items/MxiNEcQrKuXmCuWD.htm)|Fangs|Colmillos|modificada|
|[My4dOJkuaFfSHA5r.htm](pathfinder-bestiary-items/My4dOJkuaFfSHA5r.htm)|Grab|Agarrado|modificada|
|[MY6hTtpdYnUlbipf.htm](pathfinder-bestiary-items/MY6hTtpdYnUlbipf.htm)|Jaws|Fauces|modificada|
|[myHidV8LBPQYfgGg.htm](pathfinder-bestiary-items/myHidV8LBPQYfgGg.htm)|Constant Spells|Constant Spells|modificada|
|[mYNDkS27YQRyy1F0.htm](pathfinder-bestiary-items/mYNDkS27YQRyy1F0.htm)|Tail|Tail|modificada|
|[myP1KQdadPQMg8sJ.htm](pathfinder-bestiary-items/myP1KQdadPQMg8sJ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[mZDSdkhKHanRDmIg.htm](pathfinder-bestiary-items/mZDSdkhKHanRDmIg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[n06PGBhrUKKUVnsV.htm](pathfinder-bestiary-items/n06PGBhrUKKUVnsV.htm)|Knockdown|Derribo|modificada|
|[N0g5xdIF3LC3JDxP.htm](pathfinder-bestiary-items/N0g5xdIF3LC3JDxP.htm)|Alchemical Rupture|Fractura alquímica|modificada|
|[n1ci9JWzjxR4IICp.htm](pathfinder-bestiary-items/n1ci9JWzjxR4IICp.htm)|Headbutt|Headbutt|modificada|
|[n1wsbA3ExF7FB1bG.htm](pathfinder-bestiary-items/n1wsbA3ExF7FB1bG.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[n2BHwPEdFYp07VvG.htm](pathfinder-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|Pseudópodo|modificada|
|[n4Cc3VF0F86FO0sG.htm](pathfinder-bestiary-items/n4Cc3VF0F86FO0sG.htm)|Cold Adaptation|Adaptación al frío|modificada|
|[n4ny8F5wuAdt0WPM.htm](pathfinder-bestiary-items/n4ny8F5wuAdt0WPM.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[N5NccQ5JtdXMWjA9.htm](pathfinder-bestiary-items/N5NccQ5JtdXMWjA9.htm)|Engulf|Envolver|modificada|
|[n7k0vUN2EEQBVEku.htm](pathfinder-bestiary-items/n7k0vUN2EEQBVEku.htm)|Frightful Presence|Frightful Presence|modificada|
|[n8B8DCAuDAVcZqQx.htm](pathfinder-bestiary-items/n8B8DCAuDAVcZqQx.htm)|Frightful Presence|Frightful Presence|modificada|
|[N9vcXhHw71tS2MZS.htm](pathfinder-bestiary-items/N9vcXhHw71tS2MZS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nA5lgWKn3AryAuFs.htm](pathfinder-bestiary-items/nA5lgWKn3AryAuFs.htm)|Twisted Desires|Deseos corrompidos|modificada|
|[NaDfoZlbd0uAIh3t.htm](pathfinder-bestiary-items/NaDfoZlbd0uAIh3t.htm)|Trunk|Tronco|modificada|
|[naotn6A029XPnJc1.htm](pathfinder-bestiary-items/naotn6A029XPnJc1.htm)|Mauler|Vapulear|modificada|
|[nbClUIeaboa77zlN.htm](pathfinder-bestiary-items/nbClUIeaboa77zlN.htm)|Swallow Whole|Engullir Todo|modificada|
|[nbUmQWTABz3t1MCW.htm](pathfinder-bestiary-items/nbUmQWTABz3t1MCW.htm)|Wing Deflection|Desvío con el ala|modificada|
|[nbVDArIMsO5ljcOd.htm](pathfinder-bestiary-items/nbVDArIMsO5ljcOd.htm)|Claw|Garra|modificada|
|[NBwQlH8AscMgK3TC.htm](pathfinder-bestiary-items/NBwQlH8AscMgK3TC.htm)|Scimitar|Cimitarra|modificada|
|[nbWXPfh7WyVCERN9.htm](pathfinder-bestiary-items/nbWXPfh7WyVCERN9.htm)|Smoke|Humo|modificada|
|[NBXY2jJ5QKUZlI60.htm](pathfinder-bestiary-items/NBXY2jJ5QKUZlI60.htm)|Rock Tunneler|Excavar roca|modificada|
|[NC141aBluvAWkdH5.htm](pathfinder-bestiary-items/NC141aBluvAWkdH5.htm)|Grab|Agarrado|modificada|
|[NCdnViWXztATwZA5.htm](pathfinder-bestiary-items/NCdnViWXztATwZA5.htm)|Fog Vision|Fog Vision|modificada|
|[nCvEdtYwtDcpBscl.htm](pathfinder-bestiary-items/nCvEdtYwtDcpBscl.htm)|Immunity to Magic|Inmunidad a la Magia|modificada|
|[NcZMn3pf8nbMeY8x.htm](pathfinder-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NDdkhcyKoaZMjFh6.htm](pathfinder-bestiary-items/NDdkhcyKoaZMjFh6.htm)|Constrict|Restringir|modificada|
|[NDm80a7dNC5LKMIS.htm](pathfinder-bestiary-items/NDm80a7dNC5LKMIS.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[nE61lAuAYvElBffl.htm](pathfinder-bestiary-items/nE61lAuAYvElBffl.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[NeJvQpMPGMIfzdCG.htm](pathfinder-bestiary-items/NeJvQpMPGMIfzdCG.htm)|Tighten Coils|Tensar espirales|modificada|
|[NFeHIUnX3NMyZX5I.htm](pathfinder-bestiary-items/NFeHIUnX3NMyZX5I.htm)|Fangs|Colmillos|modificada|
|[NFnbEaaMOHmME3CN.htm](pathfinder-bestiary-items/NFnbEaaMOHmME3CN.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[NFPV2IK6zYWCTJbC.htm](pathfinder-bestiary-items/NFPV2IK6zYWCTJbC.htm)|Passionate Kiss|Beso pasional|modificada|
|[NfSCCIAzcvdGsuLs.htm](pathfinder-bestiary-items/NfSCCIAzcvdGsuLs.htm)|Keepsake|Custodiar|modificada|
|[nfw4zm2GbHBD2wum.htm](pathfinder-bestiary-items/nfw4zm2GbHBD2wum.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NGiITXvvb7TI4B4y.htm](pathfinder-bestiary-items/NGiITXvvb7TI4B4y.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nGzfYxHKFp5jqc76.htm](pathfinder-bestiary-items/nGzfYxHKFp5jqc76.htm)|Smoke Vision|Visión de Humo|modificada|
|[nhAXBxeC21S71BiM.htm](pathfinder-bestiary-items/nhAXBxeC21S71BiM.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[nHfBhYOOHdy8Wx7p.htm](pathfinder-bestiary-items/nHfBhYOOHdy8Wx7p.htm)|Whirlwind Form|Whirlwind Form|modificada|
|[NhKBmdf3sh4wyA1u.htm](pathfinder-bestiary-items/NhKBmdf3sh4wyA1u.htm)|Trample|Trample|modificada|
|[NHmjLWLocmpqvoi9.htm](pathfinder-bestiary-items/NHmjLWLocmpqvoi9.htm)|Tail|Tail|modificada|
|[NHSqYE5ZeWiln3Vx.htm](pathfinder-bestiary-items/NHSqYE5ZeWiln3Vx.htm)|Hoof|Hoof|modificada|
|[nHYtHMopSyGQg3WT.htm](pathfinder-bestiary-items/nHYtHMopSyGQg3WT.htm)|Regeneration 20 (Deactivated by Cold Iron)|Regeneración 20 (Desactivado por Cold Iron)|modificada|
|[NIgj8uAlgosxMdVf.htm](pathfinder-bestiary-items/NIgj8uAlgosxMdVf.htm)|Spider Swarm Venom|Spider Swarm Venom|modificada|
|[NiysQhQQKa7jp0YE.htm](pathfinder-bestiary-items/NiysQhQQKa7jp0YE.htm)|Fangs|Colmillos|modificada|
|[NJYQQMNbIh1ENrZb.htm](pathfinder-bestiary-items/NJYQQMNbIh1ENrZb.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[NkG3QwP5kR1wTHv9.htm](pathfinder-bestiary-items/NkG3QwP5kR1wTHv9.htm)|Fist|Puño|modificada|
|[NKOHZ7Ua7fqQNsAy.htm](pathfinder-bestiary-items/NKOHZ7Ua7fqQNsAy.htm)|Disgusting Demise|Muerte repugnante|modificada|
|[NKPJH9dvADsJ0zhQ.htm](pathfinder-bestiary-items/NKPJH9dvADsJ0zhQ.htm)|Great Despair|Gran desesperación|modificada|
|[NKs8hoGb4IyfFWCw.htm](pathfinder-bestiary-items/NKs8hoGb4IyfFWCw.htm)|Vengeful Spite|Rencor vengativo|modificada|
|[Nl15bkRTLq869IPi.htm](pathfinder-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[nLB0CpyqlvJpfjnr.htm](pathfinder-bestiary-items/nLB0CpyqlvJpfjnr.htm)|Stunning Screech|Chillido aturdidor|modificada|
|[nlmR2shVVcAynbhT.htm](pathfinder-bestiary-items/nlmR2shVVcAynbhT.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nlnYY25ImmMf9WxI.htm](pathfinder-bestiary-items/nlnYY25ImmMf9WxI.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[NM1DnsbI55f4G1ix.htm](pathfinder-bestiary-items/NM1DnsbI55f4G1ix.htm)|Paralytic Venom|Veneno paralizante|modificada|
|[nmSxnKrk324bG1w4.htm](pathfinder-bestiary-items/nmSxnKrk324bG1w4.htm)|Sudden Strike|Golpe súbito|modificada|
|[NN3qDwLQRgho4Tub.htm](pathfinder-bestiary-items/NN3qDwLQRgho4Tub.htm)|Constant Spells|Constant Spells|modificada|
|[nnTlOr0YpS0mwSt7.htm](pathfinder-bestiary-items/nnTlOr0YpS0mwSt7.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[NoNrnnXaKdOmoBx6.htm](pathfinder-bestiary-items/NoNrnnXaKdOmoBx6.htm)|Hunted Fear|Temor a ser cazado|modificada|
|[npD19zlFQ72xXHfo.htm](pathfinder-bestiary-items/npD19zlFQ72xXHfo.htm)|Jaws|Fauces|modificada|
|[NpgXzERVlP06NMCx.htm](pathfinder-bestiary-items/NpgXzERVlP06NMCx.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[npH3O5fYofr6M5MP.htm](pathfinder-bestiary-items/npH3O5fYofr6M5MP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[nPlH09DpLlqhUZvO.htm](pathfinder-bestiary-items/nPlH09DpLlqhUZvO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nQAtVtNKfvmpm1T4.htm](pathfinder-bestiary-items/nQAtVtNKfvmpm1T4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NQF0fD75864Ytav1.htm](pathfinder-bestiary-items/NQF0fD75864Ytav1.htm)|Captivating Song|Canción cautivadora|modificada|
|[nQr3N8AIARUFBqx8.htm](pathfinder-bestiary-items/nQr3N8AIARUFBqx8.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[nQudJs5Bky0XvlDC.htm](pathfinder-bestiary-items/nQudJs5Bky0XvlDC.htm)|Reposition|Reposicionar|modificada|
|[Ns2pok7e6UtuKUd6.htm](pathfinder-bestiary-items/Ns2pok7e6UtuKUd6.htm)|Object Lesson|Lección práctica|modificada|
|[nS51JCooM2dV2JTC.htm](pathfinder-bestiary-items/nS51JCooM2dV2JTC.htm)|Wild Empathy|Empatía salvaje|modificada|
|[nS74JTfKVbDhxOa2.htm](pathfinder-bestiary-items/nS74JTfKVbDhxOa2.htm)|Shield Block|Bloquear con escudo|modificada|
|[NS7yHszQCqQS0Bfj.htm](pathfinder-bestiary-items/NS7yHszQCqQS0Bfj.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[NSJZXBcVLdrnmMBh.htm](pathfinder-bestiary-items/NSJZXBcVLdrnmMBh.htm)|Focused Assault|Asalto concentrado|modificada|
|[NtYoVyYPS90gzKOB.htm](pathfinder-bestiary-items/NtYoVyYPS90gzKOB.htm)|Flaming Armament|Armamento flamígero|modificada|
|[Nu3jevz0ikfeGuLs.htm](pathfinder-bestiary-items/Nu3jevz0ikfeGuLs.htm)|Holy Striking Spear|Lanza Sagrada Golpeadora|modificada|
|[nu6HxTKMW550KwDx.htm](pathfinder-bestiary-items/nu6HxTKMW550KwDx.htm)|Tree Meld|Tree Meld|modificada|
|[NUE54wJoG2LAVMO1.htm](pathfinder-bestiary-items/NUE54wJoG2LAVMO1.htm)|Iron Mind|Mente férrea|modificada|
|[NUQlBj6YHAwX4fEr.htm](pathfinder-bestiary-items/NUQlBj6YHAwX4fEr.htm)|Ghost Hunter|Cazador de fantasmas|modificada|
|[NURzeRkXcWn2KGXj.htm](pathfinder-bestiary-items/NURzeRkXcWn2KGXj.htm)|Constant Spells|Constant Spells|modificada|
|[NUuZmLjmz4NPx29q.htm](pathfinder-bestiary-items/NUuZmLjmz4NPx29q.htm)|Wing|Ala|modificada|
|[NV6YJ68cn3RFInT8.htm](pathfinder-bestiary-items/NV6YJ68cn3RFInT8.htm)|Constant Spells|Constant Spells|modificada|
|[NvaOvaZnru4aSp7g.htm](pathfinder-bestiary-items/NvaOvaZnru4aSp7g.htm)|Tentacle|Tentáculo|modificada|
|[nVHHHvNcvXZZYYnc.htm](pathfinder-bestiary-items/nVHHHvNcvXZZYYnc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nvkbBLwKlX05vPo2.htm](pathfinder-bestiary-items/nvkbBLwKlX05vPo2.htm)|Breath Weapon|Breath Weapon|modificada|
|[nVzYnoOpZcD3KXg9.htm](pathfinder-bestiary-items/nVzYnoOpZcD3KXg9.htm)|Flame of Justice|Llama de la justicia|modificada|
|[nW2h9FRerHtWsX41.htm](pathfinder-bestiary-items/nW2h9FRerHtWsX41.htm)|Rock|Roca|modificada|
|[NWBV7oqS64HKJSkz.htm](pathfinder-bestiary-items/NWBV7oqS64HKJSkz.htm)|Blackaxe - Link|Blackaxe - Link|modificada|
|[nwc3AiZwtHc7DnZV.htm](pathfinder-bestiary-items/nwc3AiZwtHc7DnZV.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[NWQ4r5yoAoqIoqvJ.htm](pathfinder-bestiary-items/NWQ4r5yoAoqIoqvJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nWtiD4ASp2FFsobD.htm](pathfinder-bestiary-items/nWtiD4ASp2FFsobD.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[NwuVG96DJON1Txro.htm](pathfinder-bestiary-items/NwuVG96DJON1Txro.htm)|Constant Spells|Constant Spells|modificada|
|[NX0owCPUZbsVJ4az.htm](pathfinder-bestiary-items/NX0owCPUZbsVJ4az.htm)|Jaws|Fauces|modificada|
|[NXmjfJq99Q2rTjVD.htm](pathfinder-bestiary-items/NXmjfJq99Q2rTjVD.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[nXo4IM1jEOhgvr4C.htm](pathfinder-bestiary-items/nXo4IM1jEOhgvr4C.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[nXQUyl9JGuo2YBLG.htm](pathfinder-bestiary-items/nXQUyl9JGuo2YBLG.htm)|Transparent|Transparente|modificada|
|[nxwLnaNtKUpRi5Rp.htm](pathfinder-bestiary-items/nxwLnaNtKUpRi5Rp.htm)|Constant Spells|Constant Spells|modificada|
|[NxXkXhUBYYX8UsfS.htm](pathfinder-bestiary-items/NxXkXhUBYYX8UsfS.htm)|Rugged Travel|Viajero curtido|modificada|
|[nZfgUjjwZVrdR1bZ.htm](pathfinder-bestiary-items/nZfgUjjwZVrdR1bZ.htm)|Trident|Trident|modificada|
|[O01HSm8iaUsEoGok.htm](pathfinder-bestiary-items/O01HSm8iaUsEoGok.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[o1Dh44Cl7px3KGea.htm](pathfinder-bestiary-items/o1Dh44Cl7px3KGea.htm)|Grab|Agarrado|modificada|
|[O1iPSxTdSnBAxGt1.htm](pathfinder-bestiary-items/O1iPSxTdSnBAxGt1.htm)|Constrict|Restringir|modificada|
|[O1YTrDKri9e9hU6w.htm](pathfinder-bestiary-items/O1YTrDKri9e9hU6w.htm)|Darkvision|Visión en la oscuridad|modificada|
|[O2U2on80bNw6GMEe.htm](pathfinder-bestiary-items/O2U2on80bNw6GMEe.htm)|Trample|Trample|modificada|
|[O40irN7ET0rqNTrO.htm](pathfinder-bestiary-items/O40irN7ET0rqNTrO.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[O5gqolsMM0WkB0GZ.htm](pathfinder-bestiary-items/O5gqolsMM0WkB0GZ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[O5Hb34r44xT4xQZB.htm](pathfinder-bestiary-items/O5Hb34r44xT4xQZB.htm)|Smoke Vision|Visión de Humo|modificada|
|[O5nC82tyENuQH3SJ.htm](pathfinder-bestiary-items/O5nC82tyENuQH3SJ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[O5vFGncnfOVbJhh7.htm](pathfinder-bestiary-items/O5vFGncnfOVbJhh7.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[O6armi7YSCPK5JDa.htm](pathfinder-bestiary-items/O6armi7YSCPK5JDa.htm)|Throw Rock|Arrojar roca|modificada|
|[o6Y376tDDVyc2Cf8.htm](pathfinder-bestiary-items/o6Y376tDDVyc2Cf8.htm)|Bonetaker|Bonetaker|modificada|
|[o78IIdQ4zpSjDrZe.htm](pathfinder-bestiary-items/o78IIdQ4zpSjDrZe.htm)|Talon|Talon|modificada|
|[O7BM6p23R9nIMLOP.htm](pathfinder-bestiary-items/O7BM6p23R9nIMLOP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[o7HPbnFCe1XPngQQ.htm](pathfinder-bestiary-items/o7HPbnFCe1XPngQQ.htm)|Change Shape|Change Shape|modificada|
|[O7YsO4EwyIY6RIDs.htm](pathfinder-bestiary-items/O7YsO4EwyIY6RIDs.htm)|Jaws|Fauces|modificada|
|[o8i1KXLMvT5NmVxw.htm](pathfinder-bestiary-items/o8i1KXLMvT5NmVxw.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[oA3OqSUqWQPylGlB.htm](pathfinder-bestiary-items/oA3OqSUqWQPylGlB.htm)|Ranseur|Ranseur|modificada|
|[oa6JQCeL4B2WQMPI.htm](pathfinder-bestiary-items/oa6JQCeL4B2WQMPI.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[oAdazPfRl3wntiS1.htm](pathfinder-bestiary-items/oAdazPfRl3wntiS1.htm)|Attack of Opportunity (Fangs Only)|Ataque de oportunidad (sólo colmillos)|modificada|
|[OaKZ88EQ5UVii9xw.htm](pathfinder-bestiary-items/OaKZ88EQ5UVii9xw.htm)|Undead Mastery|Maestría muerto viviente|modificada|
|[oAM6tsN2NTZwvnTf.htm](pathfinder-bestiary-items/oAM6tsN2NTZwvnTf.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[OAt8guQQNXMQRYHW.htm](pathfinder-bestiary-items/OAt8guQQNXMQRYHW.htm)|Pounce|Abalanzarse|modificada|
|[oB9Lh7ZFScaG3Z04.htm](pathfinder-bestiary-items/oB9Lh7ZFScaG3Z04.htm)|Hungry Flurry|Ráfaga hambrienta|modificada|
|[ObIC1c8HIIuC53Nb.htm](pathfinder-bestiary-items/ObIC1c8HIIuC53Nb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oblGn3AB742xJUfT.htm](pathfinder-bestiary-items/oblGn3AB742xJUfT.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[ObX0PxwHHLMvaTJn.htm](pathfinder-bestiary-items/ObX0PxwHHLMvaTJn.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[oBZiGvpEg2BfHRnx.htm](pathfinder-bestiary-items/oBZiGvpEg2BfHRnx.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[oc0bsYm11TxBi7AZ.htm](pathfinder-bestiary-items/oc0bsYm11TxBi7AZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[oCe57tAkaWpqe976.htm](pathfinder-bestiary-items/oCe57tAkaWpqe976.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ocNVtm84bTjxf93b.htm](pathfinder-bestiary-items/ocNVtm84bTjxf93b.htm)|Spit|Escupe|modificada|
|[OcYo8NrM7fSjcymh.htm](pathfinder-bestiary-items/OcYo8NrM7fSjcymh.htm)|Claw|Garra|modificada|
|[ODGzZo0VrXZyNziZ.htm](pathfinder-bestiary-items/ODGzZo0VrXZyNziZ.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[oDkPJwQC7jVjhBRN.htm](pathfinder-bestiary-items/oDkPJwQC7jVjhBRN.htm)|Regeneration 20 (Deactivated by Acid or Fire)|Regeneración 20 (Desactivado por Ácido o Fuego)|modificada|
|[odPtrANIuNiSlgo5.htm](pathfinder-bestiary-items/odPtrANIuNiSlgo5.htm)|Infernal Wound|Herida infernal|modificada|
|[oe1VqqJvUvIMKRRa.htm](pathfinder-bestiary-items/oe1VqqJvUvIMKRRa.htm)|Wing|Ala|modificada|
|[oeg1zG9t1NO1R9l3.htm](pathfinder-bestiary-items/oeg1zG9t1NO1R9l3.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[oEHHDWURCYEb1t7W.htm](pathfinder-bestiary-items/oEHHDWURCYEb1t7W.htm)|Jaws|Fauces|modificada|
|[oEnTmvBRzYBAWuou.htm](pathfinder-bestiary-items/oEnTmvBRzYBAWuou.htm)|Scratch|Rascarse|modificada|
|[oeOZCitfRoZXCLfy.htm](pathfinder-bestiary-items/oeOZCitfRoZXCLfy.htm)|Tor Linnorm Venom|Veneno de linnorm del pe≥n|modificada|
|[OFEj9wltNrZpJ77C.htm](pathfinder-bestiary-items/OFEj9wltNrZpJ77C.htm)|Improved Grab|Agarrado mejorado|modificada|
|[oFnlUf41ozZQtvKs.htm](pathfinder-bestiary-items/oFnlUf41ozZQtvKs.htm)|Dagger|Daga|modificada|
|[OFtZ11CEYiceib4W.htm](pathfinder-bestiary-items/OFtZ11CEYiceib4W.htm)|Rend|Rasgadura|modificada|
|[OG9zHmjCiir50sUt.htm](pathfinder-bestiary-items/OG9zHmjCiir50sUt.htm)|Fast Healing 2 (In Open Air)|Curación Rápida 2 (En Apertura)|modificada|
|[ogjALspcg7GVz79c.htm](pathfinder-bestiary-items/ogjALspcg7GVz79c.htm)|Buck|Encabritarse|modificada|
|[OgkLVLhW0uOMdbGj.htm](pathfinder-bestiary-items/OgkLVLhW0uOMdbGj.htm)|Formation|Formación|modificada|
|[OgOkCo3ZpgcUbZr1.htm](pathfinder-bestiary-items/OgOkCo3ZpgcUbZr1.htm)|Vampire Weaknesses|Vampire Weaknesses|modificada|
|[oGR3hLNhUlAR66Kw.htm](pathfinder-bestiary-items/oGR3hLNhUlAR66Kw.htm)|Claw|Garra|modificada|
|[OhbQTfhDygZDiO3p.htm](pathfinder-bestiary-items/OhbQTfhDygZDiO3p.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ohby2hZPzWDoCkLo.htm](pathfinder-bestiary-items/Ohby2hZPzWDoCkLo.htm)|Aura of Misfortune|Aura de infortunio|modificada|
|[ohDZuFtUD8Y2pTMN.htm](pathfinder-bestiary-items/ohDZuFtUD8Y2pTMN.htm)|Claw|Garra|modificada|
|[OHs6oHOrlsXUSGkS.htm](pathfinder-bestiary-items/OHs6oHOrlsXUSGkS.htm)|Golden Luck|Suerte de oro|modificada|
|[ohwAZH20CeE3uGMp.htm](pathfinder-bestiary-items/ohwAZH20CeE3uGMp.htm)|Gallop|Galope|modificada|
|[Oi2IinqJzQhrUE4R.htm](pathfinder-bestiary-items/Oi2IinqJzQhrUE4R.htm)|Mandibles|Mandíbulas|modificada|
|[oiDsSoQk3eAUnpKj.htm](pathfinder-bestiary-items/oiDsSoQk3eAUnpKj.htm)|Body|Cuerpo|modificada|
|[oikg7iKnE0OwX6CV.htm](pathfinder-bestiary-items/oikg7iKnE0OwX6CV.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[OIpLEpHTRmAWKVGT.htm](pathfinder-bestiary-items/OIpLEpHTRmAWKVGT.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[OjJQfX1hTXQZsKNH.htm](pathfinder-bestiary-items/OjJQfX1hTXQZsKNH.htm)|Jaws|Fauces|modificada|
|[OkpY1zJx7qxKu6nk.htm](pathfinder-bestiary-items/OkpY1zJx7qxKu6nk.htm)|Jaws|Fauces|modificada|
|[okSVMAHfKVBCEcfy.htm](pathfinder-bestiary-items/okSVMAHfKVBCEcfy.htm)|Tail|Tail|modificada|
|[oKuYToU9Nv9ALIIv.htm](pathfinder-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[OKzqL9XBxnlVikDo.htm](pathfinder-bestiary-items/OKzqL9XBxnlVikDo.htm)|Trample|Trample|modificada|
|[oL6JyyShxXN8PigD.htm](pathfinder-bestiary-items/oL6JyyShxXN8PigD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OLoz41FJFungrpr5.htm](pathfinder-bestiary-items/OLoz41FJFungrpr5.htm)|Gibbering|Gibbering|modificada|
|[omXkYKF6c76YYVUV.htm](pathfinder-bestiary-items/omXkYKF6c76YYVUV.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[oMykAWUXRiU2dRig.htm](pathfinder-bestiary-items/oMykAWUXRiU2dRig.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[oN5KE37Rg66tMPMp.htm](pathfinder-bestiary-items/oN5KE37Rg66tMPMp.htm)|Beak|Beak|modificada|
|[oncBOpovIROpa0vf.htm](pathfinder-bestiary-items/oncBOpovIROpa0vf.htm)|Explosion|Explosión|modificada|
|[ONfkRVRg1gkRXJAJ.htm](pathfinder-bestiary-items/ONfkRVRg1gkRXJAJ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Onifi8y9OVLqK3Ea.htm](pathfinder-bestiary-items/Onifi8y9OVLqK3Ea.htm)|Ferocity|Ferocidad|modificada|
|[ONIHyZ3rL5vCjJod.htm](pathfinder-bestiary-items/ONIHyZ3rL5vCjJod.htm)|Intimidating Display|Exhibición intimidante|modificada|
|[ONo2JspAa1GnoAlR.htm](pathfinder-bestiary-items/ONo2JspAa1GnoAlR.htm)|Pierce Armor|Perforar armadura|modificada|
|[onqCyoeB5jfvwrI0.htm](pathfinder-bestiary-items/onqCyoeB5jfvwrI0.htm)|Foot|Pie|modificada|
|[oObpBdGWzojxRdfS.htm](pathfinder-bestiary-items/oObpBdGWzojxRdfS.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[OOhSHhrOf35DMGaw.htm](pathfinder-bestiary-items/OOhSHhrOf35DMGaw.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[oOto6dxiQZFhZzKY.htm](pathfinder-bestiary-items/oOto6dxiQZFhZzKY.htm)|Grab|Agarrado|modificada|
|[opGxn2Tkn6TRXGQh.htm](pathfinder-bestiary-items/opGxn2Tkn6TRXGQh.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[oPKgkFop9yTr0x9k.htm](pathfinder-bestiary-items/oPKgkFop9yTr0x9k.htm)|Dagger|Daga|modificada|
|[Oqdlw7Axr5oYYU4m.htm](pathfinder-bestiary-items/Oqdlw7Axr5oYYU4m.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[oqGxkGNFqdTqq3p0.htm](pathfinder-bestiary-items/oqGxkGNFqdTqq3p0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OQICdYHqGHNlnuut.htm](pathfinder-bestiary-items/OQICdYHqGHNlnuut.htm)|Swallow Whole|Engullir Todo|modificada|
|[oQxGeLSQxmxGUQ1l.htm](pathfinder-bestiary-items/oQxGeLSQxmxGUQ1l.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OQyH3C1xWSTxRaOA.htm](pathfinder-bestiary-items/OQyH3C1xWSTxRaOA.htm)|Shortbow|Arco corto|modificada|
|[ORBRYCSr88txQyIM.htm](pathfinder-bestiary-items/ORBRYCSr88txQyIM.htm)|Keen Returning Hatchet|Afilada Hacha Retornante|modificada|
|[orlHVFqe7EEW7ruO.htm](pathfinder-bestiary-items/orlHVFqe7EEW7ruO.htm)|Foot|Pie|modificada|
|[ORqdD8bp2Bs2WWlC.htm](pathfinder-bestiary-items/ORqdD8bp2Bs2WWlC.htm)|Constant Spells|Constant Spells|modificada|
|[osaWxy5RrqJdKuni.htm](pathfinder-bestiary-items/osaWxy5RrqJdKuni.htm)|Armor of Flames|Armadura de llamasígera|modificada|
|[oSoU8REx8w5hSHcD.htm](pathfinder-bestiary-items/oSoU8REx8w5hSHcD.htm)|Twisting Tail|Enredar con la cola|modificada|
|[oSq2qYAln4B4sFY0.htm](pathfinder-bestiary-items/oSq2qYAln4B4sFY0.htm)|Branch|Rama|modificada|
|[osyjgXeJV4vv5i3F.htm](pathfinder-bestiary-items/osyjgXeJV4vv5i3F.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[OTDwVMsK2PJ4wuY6.htm](pathfinder-bestiary-items/OTDwVMsK2PJ4wuY6.htm)|Jaws|Fauces|modificada|
|[OTKM4OSjZGvMhEEJ.htm](pathfinder-bestiary-items/OTKM4OSjZGvMhEEJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[otqsXx9gjh7rnOpU.htm](pathfinder-bestiary-items/otqsXx9gjh7rnOpU.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[OTuZbxVdoEtjN7ap.htm](pathfinder-bestiary-items/OTuZbxVdoEtjN7ap.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[otWGnycqKoeQC7U9.htm](pathfinder-bestiary-items/otWGnycqKoeQC7U9.htm)|Inexorable March|Marcha inexorable|modificada|
|[oTXMvXAgkbwzgGhM.htm](pathfinder-bestiary-items/oTXMvXAgkbwzgGhM.htm)|Jaws|Fauces|modificada|
|[oU0YlVUVw2Jjoabk.htm](pathfinder-bestiary-items/oU0YlVUVw2Jjoabk.htm)|Claw|Garra|modificada|
|[ouaeCAenWwGPbpdb.htm](pathfinder-bestiary-items/ouaeCAenWwGPbpdb.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[OUrfzECO9Dy3ncIO.htm](pathfinder-bestiary-items/OUrfzECO9Dy3ncIO.htm)|Paralysis|Parálisis|modificada|
|[OuwilwDvrI1Rqfzi.htm](pathfinder-bestiary-items/OuwilwDvrI1Rqfzi.htm)|Spore Tendrils|Zarcillos de esporas|modificada|
|[ovaI2nqsEYxoo4Ys.htm](pathfinder-bestiary-items/ovaI2nqsEYxoo4Ys.htm)|Shortsword|Espada corta|modificada|
|[OvbTCZLdp8qg7KX2.htm](pathfinder-bestiary-items/OvbTCZLdp8qg7KX2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OVmDA2DweiDfmQwN.htm](pathfinder-bestiary-items/OVmDA2DweiDfmQwN.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[OvsLPiSD2whyqgU0.htm](pathfinder-bestiary-items/OvsLPiSD2whyqgU0.htm)|Lightning Blade|Filo del relámpago|modificada|
|[OW9KKx3Offpslmah.htm](pathfinder-bestiary-items/OW9KKx3Offpslmah.htm)|Lumbering Charge|Carga pesada|modificada|
|[OwDiH7gzfhf1i4hu.htm](pathfinder-bestiary-items/OwDiH7gzfhf1i4hu.htm)|Wasp Venom|Veneno de Avispa|modificada|
|[OWImE1DpRm2byzUX.htm](pathfinder-bestiary-items/OWImE1DpRm2byzUX.htm)|Juke|Amago|modificada|
|[oWIxJUR2edElUUod.htm](pathfinder-bestiary-items/oWIxJUR2edElUUod.htm)|Fist|Puño|modificada|
|[OWlPaR3WuuSoTaGl.htm](pathfinder-bestiary-items/OWlPaR3WuuSoTaGl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[OwVFI2xNdo532d71.htm](pathfinder-bestiary-items/OwVFI2xNdo532d71.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[oWvZ2SZzMcdxkRRj.htm](pathfinder-bestiary-items/oWvZ2SZzMcdxkRRj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[oXq7JKUPh72N7tPy.htm](pathfinder-bestiary-items/oXq7JKUPh72N7tPy.htm)|Change Shape|Change Shape|modificada|
|[oXQB3BHjejzv1QJD.htm](pathfinder-bestiary-items/oXQB3BHjejzv1QJD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[oXZ3YdyxyoVGRGJs.htm](pathfinder-bestiary-items/oXZ3YdyxyoVGRGJs.htm)|Web Sense|Web Sense|modificada|
|[oXZ9gBxLTC4PuQec.htm](pathfinder-bestiary-items/oXZ9gBxLTC4PuQec.htm)|Horns|Cuernos|modificada|
|[oyHWuTde1I33SKT6.htm](pathfinder-bestiary-items/oyHWuTde1I33SKT6.htm)|Lamia's Caress|Caricia de la lamia|modificada|
|[OZD3jO9gpPebw7bB.htm](pathfinder-bestiary-items/OZD3jO9gpPebw7bB.htm)|Jaws|Fauces|modificada|
|[OzGatkJy0uyM9zZJ.htm](pathfinder-bestiary-items/OzGatkJy0uyM9zZJ.htm)|Grab|Agarrado|modificada|
|[OZmaS9RmTVLBAzbH.htm](pathfinder-bestiary-items/OZmaS9RmTVLBAzbH.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[P0MPVynVKmKMh3FI.htm](pathfinder-bestiary-items/P0MPVynVKmKMh3FI.htm)|Bloodletting|Sangría|modificada|
|[P0Q4Hgs9e8hwRX5G.htm](pathfinder-bestiary-items/P0Q4Hgs9e8hwRX5G.htm)|Envelop|Amortajar|modificada|
|[p1LTV3WNbUGTsSUY.htm](pathfinder-bestiary-items/p1LTV3WNbUGTsSUY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[p1OayYNJswICCP7g.htm](pathfinder-bestiary-items/p1OayYNJswICCP7g.htm)|Jaws|Fauces|modificada|
|[p1oWXCxvOv782fSp.htm](pathfinder-bestiary-items/p1oWXCxvOv782fSp.htm)|Javelin|Javelin|modificada|
|[p1rNOTBScTO9LxBR.htm](pathfinder-bestiary-items/p1rNOTBScTO9LxBR.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[p1roWrlYRw0ZNWUR.htm](pathfinder-bestiary-items/p1roWrlYRw0ZNWUR.htm)|Alchemical Formulas|Alchemical Formulas|modificada|
|[p1yl81yMK4bvehu5.htm](pathfinder-bestiary-items/p1yl81yMK4bvehu5.htm)|Scimitar|Cimitarra|modificada|
|[p4gHmixTKN3Gb87J.htm](pathfinder-bestiary-items/p4gHmixTKN3Gb87J.htm)|Dragon Jaws|Fauces de Dragón|modificada|
|[p51DG64tQvgJwgyu.htm](pathfinder-bestiary-items/p51DG64tQvgJwgyu.htm)|Claw|Garra|modificada|
|[p5mFSmPHyGqg2ima.htm](pathfinder-bestiary-items/p5mFSmPHyGqg2ima.htm)|Innate Divine Spells|Hechizos Divinos Innatos|modificada|
|[p5WXqIz3tJJCdoiz.htm](pathfinder-bestiary-items/p5WXqIz3tJJCdoiz.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[P658sPuiyqAqTdkz.htm](pathfinder-bestiary-items/P658sPuiyqAqTdkz.htm)|Focus Beauty|Focus Belleza|modificada|
|[P7JMDrm2HB1QPV3N.htm](pathfinder-bestiary-items/P7JMDrm2HB1QPV3N.htm)|Web Lurker Venom|Veneno del merodeador de telara.|modificada|
|[P7S7nuGr7qWkygVT.htm](pathfinder-bestiary-items/P7S7nuGr7qWkygVT.htm)|Stomp|Stomp|modificada|
|[p80DPc7fm2auFNQg.htm](pathfinder-bestiary-items/p80DPc7fm2auFNQg.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[P8ybqvwXM5UVHrfF.htm](pathfinder-bestiary-items/P8ybqvwXM5UVHrfF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[p90CXzyif81DD51u.htm](pathfinder-bestiary-items/p90CXzyif81DD51u.htm)|Spiral of Despair|Espiral de Desesperación|modificada|
|[PAbtJjm0RNsZSr1R.htm](pathfinder-bestiary-items/PAbtJjm0RNsZSr1R.htm)|Dark Naga Venom|Veneno Naga Oscuro|modificada|
|[PAN3R11XOGsaiIbK.htm](pathfinder-bestiary-items/PAN3R11XOGsaiIbK.htm)|Claw|Garra|modificada|
|[PAY2I7fJGT3XbkIX.htm](pathfinder-bestiary-items/PAY2I7fJGT3XbkIX.htm)|Hoof|Hoof|modificada|
|[pB0p0rg2zJNouFjM.htm](pathfinder-bestiary-items/pB0p0rg2zJNouFjM.htm)|Talon|Talon|modificada|
|[PB7yguG7pz4ib0ps.htm](pathfinder-bestiary-items/PB7yguG7pz4ib0ps.htm)|Shark Commune 150 feet|Tiburón comunión 150 pies|modificada|
|[pbydRoHQnSpw50iV.htm](pathfinder-bestiary-items/pbydRoHQnSpw50iV.htm)|Belly Grease|Grasa para el vientre|modificada|
|[pChhLSd048EpUiL0.htm](pathfinder-bestiary-items/pChhLSd048EpUiL0.htm)|Fangs|Colmillos|modificada|
|[pcv6j9xM8Qcw0h9R.htm](pathfinder-bestiary-items/pcv6j9xM8Qcw0h9R.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[pCYQE7ydvnjQGmPi.htm](pathfinder-bestiary-items/pCYQE7ydvnjQGmPi.htm)|Breath Weapon|Breath Weapon|modificada|
|[pCZn7Y6OrzZU4XgW.htm](pathfinder-bestiary-items/pCZn7Y6OrzZU4XgW.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[pD6a7tBQWhYfzUSt.htm](pathfinder-bestiary-items/pD6a7tBQWhYfzUSt.htm)|Jaws|Fauces|modificada|
|[PDtsBEnskV921hyI.htm](pathfinder-bestiary-items/PDtsBEnskV921hyI.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[pdwlm8jY2MDoJUAN.htm](pathfinder-bestiary-items/pdwlm8jY2MDoJUAN.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[PdyYBEM94DTjouMT.htm](pathfinder-bestiary-items/PdyYBEM94DTjouMT.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[PeH9Tlr3y5ABJu0g.htm](pathfinder-bestiary-items/PeH9Tlr3y5ABJu0g.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[peHpZeJ7VwkuSurw.htm](pathfinder-bestiary-items/peHpZeJ7VwkuSurw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pEpgPRttHusG3elF.htm](pathfinder-bestiary-items/pEpgPRttHusG3elF.htm)|Three-Headed Strike|Golpe tricéfalo|modificada|
|[PF9LIocpqPbxqnvR.htm](pathfinder-bestiary-items/PF9LIocpqPbxqnvR.htm)|Thrash|Sacudirse|modificada|
|[PfK4YgueBXw7cjqo.htm](pathfinder-bestiary-items/PfK4YgueBXw7cjqo.htm)|Vine Lash|Vine Lash|modificada|
|[pFP0EC770FgwQQ6K.htm](pathfinder-bestiary-items/pFP0EC770FgwQQ6K.htm)|Fangs|Colmillos|modificada|
|[pFruogrBBB4pvImp.htm](pathfinder-bestiary-items/pFruogrBBB4pvImp.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[pG17snAKOgqgqba1.htm](pathfinder-bestiary-items/pG17snAKOgqgqba1.htm)|Reap|Segar|modificada|
|[pg6Z939BkFzl6ZUZ.htm](pathfinder-bestiary-items/pg6Z939BkFzl6ZUZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Pgjjt3dWPTpOGD8c.htm](pathfinder-bestiary-items/Pgjjt3dWPTpOGD8c.htm)|Ambush|Emboscar|modificada|
|[PgvThxPk7tH5CoXV.htm](pathfinder-bestiary-items/PgvThxPk7tH5CoXV.htm)|Constrict|Restringir|modificada|
|[ph3U8sPdi5HCaPC6.htm](pathfinder-bestiary-items/ph3U8sPdi5HCaPC6.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[pHkzyTUC7cfSCoYP.htm](pathfinder-bestiary-items/pHkzyTUC7cfSCoYP.htm)|Tail|Tail|modificada|
|[phmie8CQGIpk0d0n.htm](pathfinder-bestiary-items/phmie8CQGIpk0d0n.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PHogXv9VGXwEm0k8.htm](pathfinder-bestiary-items/PHogXv9VGXwEm0k8.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[phoQ0yOkbf9saI2l.htm](pathfinder-bestiary-items/phoQ0yOkbf9saI2l.htm)|Jaws|Fauces|modificada|
|[PHrmAQv1FUnGiNrw.htm](pathfinder-bestiary-items/PHrmAQv1FUnGiNrw.htm)|Engulf|Envolver|modificada|
|[PHro3bLYo9Br5Q7R.htm](pathfinder-bestiary-items/PHro3bLYo9Br5Q7R.htm)|Claw|Garra|modificada|
|[PHSSOZNHO0AKJt1q.htm](pathfinder-bestiary-items/PHSSOZNHO0AKJt1q.htm)|Blood Nourishment|Alimentarse de sangre|modificada|
|[piJXVVEPai5UFibA.htm](pathfinder-bestiary-items/piJXVVEPai5UFibA.htm)|Sporesight (Imprecise) 60 feet|Visión mediante esporas (Impreciso) 60 pies|modificada|
|[PIjZTZsbQlriq5X3.htm](pathfinder-bestiary-items/PIjZTZsbQlriq5X3.htm)|Giant Centipede Venom|Veneno de ciempiés gigante|modificada|
|[PIKiUX4h9vUWIM3Y.htm](pathfinder-bestiary-items/PIKiUX4h9vUWIM3Y.htm)|Armor-Rending Bite|Mordisco rasgaarmaduras|modificada|
|[pIuRGB2pXEwdYSox.htm](pathfinder-bestiary-items/pIuRGB2pXEwdYSox.htm)|Constant Spells|Constant Spells|modificada|
|[PIznHIEc3sGmoAUM.htm](pathfinder-bestiary-items/PIznHIEc3sGmoAUM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pJ0MYG8bkvto7yHb.htm](pathfinder-bestiary-items/pJ0MYG8bkvto7yHb.htm)|Grab|Agarrado|modificada|
|[pJ0uK52j5EXZwNhb.htm](pathfinder-bestiary-items/pJ0uK52j5EXZwNhb.htm)|Shortsword|Espada corta|modificada|
|[pJ5VmBxu1TYtLUuZ.htm](pathfinder-bestiary-items/pJ5VmBxu1TYtLUuZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[PJbq8TKDuFGf9n7l.htm](pathfinder-bestiary-items/PJbq8TKDuFGf9n7l.htm)|Quasit Venom|Veneno de quásit|modificada|
|[pK7TW7mAgEFfJu0Q.htm](pathfinder-bestiary-items/pK7TW7mAgEFfJu0Q.htm)|Curse of the Wererat|Curse of the Wererat|modificada|
|[pkCKdy44WRzMrMB3.htm](pathfinder-bestiary-items/pkCKdy44WRzMrMB3.htm)|Change Shape|Change Shape|modificada|
|[pkJjWhKaHykBdCEO.htm](pathfinder-bestiary-items/pkJjWhKaHykBdCEO.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[pKPJ4u9g0HQyU7b6.htm](pathfinder-bestiary-items/pKPJ4u9g0HQyU7b6.htm)|Furious Fusillade|Bombardeo furioso|modificada|
|[pkTWnzYC0Yob3mzf.htm](pathfinder-bestiary-items/pkTWnzYC0Yob3mzf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pl4CFdDU1X88olbS.htm](pathfinder-bestiary-items/pl4CFdDU1X88olbS.htm)|Captive Rake|Desgarrar cautivo|modificada|
|[pLB0Pp01hzJSCr2W.htm](pathfinder-bestiary-items/pLB0Pp01hzJSCr2W.htm)|Darkvision|Visión en la oscuridad|modificada|
|[plka6anhQ8CltSBM.htm](pathfinder-bestiary-items/plka6anhQ8CltSBM.htm)|Greater Constrict|Mayor Restricción|modificada|
|[PLLmu0Mc4VizpZYv.htm](pathfinder-bestiary-items/PLLmu0Mc4VizpZYv.htm)|Jaws|Fauces|modificada|
|[PlWNcIaHKORc6Khe.htm](pathfinder-bestiary-items/PlWNcIaHKORc6Khe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pm2fdTcLLvGAYrvI.htm](pathfinder-bestiary-items/pm2fdTcLLvGAYrvI.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[PMBSc3Ypq9HMYeKR.htm](pathfinder-bestiary-items/PMBSc3Ypq9HMYeKR.htm)|Water Mastery|Dominio del agua|modificada|
|[PmcZB53eH1gUzaBq.htm](pathfinder-bestiary-items/PmcZB53eH1gUzaBq.htm)|Water-Bound|Ligado al agua|modificada|
|[PmnlcdA8wcJdRNmR.htm](pathfinder-bestiary-items/PmnlcdA8wcJdRNmR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pMqdrNgUYaFMO0PK.htm](pathfinder-bestiary-items/pMqdrNgUYaFMO0PK.htm)|Tail|Tail|modificada|
|[pmqefsGShf77p3Zd.htm](pathfinder-bestiary-items/pmqefsGShf77p3Zd.htm)|Jaws|Fauces|modificada|
|[PMu8PzzMLndJzxnZ.htm](pathfinder-bestiary-items/PMu8PzzMLndJzxnZ.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[PNc43N7H5LeJXDQj.htm](pathfinder-bestiary-items/PNc43N7H5LeJXDQj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pO7Lt2BaAG6zgDvw.htm](pathfinder-bestiary-items/pO7Lt2BaAG6zgDvw.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[PoFwl1bniXcbhkld.htm](pathfinder-bestiary-items/PoFwl1bniXcbhkld.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[pOtIHukgdOyL8rMp.htm](pathfinder-bestiary-items/pOtIHukgdOyL8rMp.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[PPtTcRr6xJGUyX3Q.htm](pathfinder-bestiary-items/PPtTcRr6xJGUyX3Q.htm)|Constant Spells|Constant Spells|modificada|
|[pPuHLDfAI4W6qEr6.htm](pathfinder-bestiary-items/pPuHLDfAI4W6qEr6.htm)|Messenger's Amnesty|Amnistía del mensajero|modificada|
|[pqbk6QDDIpUVuKRX.htm](pathfinder-bestiary-items/pqbk6QDDIpUVuKRX.htm)|Root|Enraizarse|modificada|
|[PQMwXaG8Zqcd02Oe.htm](pathfinder-bestiary-items/PQMwXaG8Zqcd02Oe.htm)|Triple Opportunity|Oportunista triple|modificada|
|[pqQTzMV3UX2rq1M9.htm](pathfinder-bestiary-items/pqQTzMV3UX2rq1M9.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[pR70Cym5tQf7vIJt.htm](pathfinder-bestiary-items/pR70Cym5tQf7vIJt.htm)|Claw|Garra|modificada|
|[pR9Yyd3sYM5iPspf.htm](pathfinder-bestiary-items/pR9Yyd3sYM5iPspf.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[prLWsWvW54YcYyNa.htm](pathfinder-bestiary-items/prLWsWvW54YcYyNa.htm)|Guiding Angel|Ángel guía.|modificada|
|[pRRtkR99srCWUO1j.htm](pathfinder-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|Masa Corrosiva|modificada|
|[PrsRCxi6j7SHJeMu.htm](pathfinder-bestiary-items/PrsRCxi6j7SHJeMu.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[PrtfnlNRqEPNsEDh.htm](pathfinder-bestiary-items/PrtfnlNRqEPNsEDh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pSG95fJhQ8ufTKho.htm](pathfinder-bestiary-items/pSG95fJhQ8ufTKho.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[PsH13WVcjnQPM2Iy.htm](pathfinder-bestiary-items/PsH13WVcjnQPM2Iy.htm)|Fangs|Colmillos|modificada|
|[pSI2QoLhvn9RpREJ.htm](pathfinder-bestiary-items/pSI2QoLhvn9RpREJ.htm)|Trample|Trample|modificada|
|[pSi2yAlBjhVTBwq8.htm](pathfinder-bestiary-items/pSi2yAlBjhVTBwq8.htm)|Dance of Ruin|Danza de la ruina|modificada|
|[pSJ8UuUcEbstSlWE.htm](pathfinder-bestiary-items/pSJ8UuUcEbstSlWE.htm)|Breath Weapon|Breath Weapon|modificada|
|[pSKjTPdO8e9KO9LI.htm](pathfinder-bestiary-items/pSKjTPdO8e9KO9LI.htm)|Multiple Opportunities|Oportunidades múltiple|modificada|
|[pSpabH5fc53i9K81.htm](pathfinder-bestiary-items/pSpabH5fc53i9K81.htm)|Frightful Presence|Frightful Presence|modificada|
|[Ptn3Zb1Syk7YdL4y.htm](pathfinder-bestiary-items/Ptn3Zb1Syk7YdL4y.htm)|Claw|Garra|modificada|
|[PtwaILORaUXLPA4E.htm](pathfinder-bestiary-items/PtwaILORaUXLPA4E.htm)|+2 Status to All Saves vs. Cold|+2 situación a todas las salvaciones contra el Frío.|modificada|
|[pu2uW299sl7fVAeT.htm](pathfinder-bestiary-items/pu2uW299sl7fVAeT.htm)|Hoof|Hoof|modificada|
|[PuCVCBzPaKVJYf9n.htm](pathfinder-bestiary-items/PuCVCBzPaKVJYf9n.htm)|Jaws|Fauces|modificada|
|[pugIMNhBHc9VHl1z.htm](pathfinder-bestiary-items/pugIMNhBHc9VHl1z.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[puh52kwYDM4gZi1Z.htm](pathfinder-bestiary-items/puh52kwYDM4gZi1Z.htm)|Club|Club|modificada|
|[pV2kE2wYrZjLyBiR.htm](pathfinder-bestiary-items/pV2kE2wYrZjLyBiR.htm)|Push 10 feet|Empuje 10 pies|modificada|
|[pv4cYBnhSh901lFA.htm](pathfinder-bestiary-items/pv4cYBnhSh901lFA.htm)|Scent (Imprecise) 120 feet|Olor (Impreciso) 120 pies|modificada|
|[pVCudVc1B7mQVjP2.htm](pathfinder-bestiary-items/pVCudVc1B7mQVjP2.htm)|Longspear|Longspear|modificada|
|[pvdHMw62l3uanob8.htm](pathfinder-bestiary-items/pvdHMw62l3uanob8.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[pVi8f6cZ7YOZDd1k.htm](pathfinder-bestiary-items/pVi8f6cZ7YOZDd1k.htm)|Fist|Puño|modificada|
|[pVOVd7IIOsuNw3fo.htm](pathfinder-bestiary-items/pVOVd7IIOsuNw3fo.htm)|Rock|Roca|modificada|
|[PVrWJHEarOi9LFbD.htm](pathfinder-bestiary-items/PVrWJHEarOi9LFbD.htm)|Rope Snare|Atrapar con cuerda|modificada|
|[pWL13AdPcgKCTD9i.htm](pathfinder-bestiary-items/pWL13AdPcgKCTD9i.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[pWm5TTC2umXroqjo.htm](pathfinder-bestiary-items/pWm5TTC2umXroqjo.htm)|Pack Attack|Ataque en manada|modificada|
|[pWMLYiykJTfDP0Lf.htm](pathfinder-bestiary-items/pWMLYiykJTfDP0Lf.htm)|Fire Ray|Rayo de fuego|modificada|
|[PWV65LaMZJtQOg9d.htm](pathfinder-bestiary-items/PWV65LaMZJtQOg9d.htm)|Entropy Sense (Imprecise) 30 feet|Sentido de Entropía (Impreciso) 30 pies|modificada|
|[pwxCJBVZsNZOzHqr.htm](pathfinder-bestiary-items/pwxCJBVZsNZOzHqr.htm)|Swarming|Enjambre|modificada|
|[pxfevBUBi49C9LID.htm](pathfinder-bestiary-items/pxfevBUBi49C9LID.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[PxvHno73t8NaLGmq.htm](pathfinder-bestiary-items/PxvHno73t8NaLGmq.htm)|Tail|Tail|modificada|
|[pXWT2pfQnO8lkVNs.htm](pathfinder-bestiary-items/pXWT2pfQnO8lkVNs.htm)|Wavesense (Imprecise) 60 feet|Sentir ondulaciones (Impreciso) 60 pies|modificada|
|[PycbJPVh46UXYc9R.htm](pathfinder-bestiary-items/PycbJPVh46UXYc9R.htm)|Matriarch's Caress|Caricia de la matriarca|modificada|
|[PyCQ7wcdOzNcvfQN.htm](pathfinder-bestiary-items/PyCQ7wcdOzNcvfQN.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[pyD5mpeBbbBmJcvA.htm](pathfinder-bestiary-items/pyD5mpeBbbBmJcvA.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[pydkk6U2LVFlha3A.htm](pathfinder-bestiary-items/pydkk6U2LVFlha3A.htm)|Dragon Chill|Frío dracónico|modificada|
|[pYg2FGHWnoS7Le78.htm](pathfinder-bestiary-items/pYg2FGHWnoS7Le78.htm)|Pack Attack|Ataque en manada|modificada|
|[pyVnqo4FLO0RWxrX.htm](pathfinder-bestiary-items/pyVnqo4FLO0RWxrX.htm)|Improved Push 20 feet (40 feet on a Critical Hit)|Empujón mejorado 20 pies (40 pies con un Golpe crítico).|modificada|
|[pzO0JwhFqVcRKzAN.htm](pathfinder-bestiary-items/pzO0JwhFqVcRKzAN.htm)|Giant Tarantula Venom|Veneno de Tarántula Gigante|modificada|
|[pZO6hvTQCaz319Or.htm](pathfinder-bestiary-items/pZO6hvTQCaz319Or.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[PzpzmSnCSWrwxzDO.htm](pathfinder-bestiary-items/PzpzmSnCSWrwxzDO.htm)|Longsword|Longsword|modificada|
|[PZQRsmRUDWR50Kiq.htm](pathfinder-bestiary-items/PZQRsmRUDWR50Kiq.htm)|Engulf|Envolver|modificada|
|[pzRCNxN7IHDbV6Wv.htm](pathfinder-bestiary-items/pzRCNxN7IHDbV6Wv.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[pZX2G1Vh8XNn8DCw.htm](pathfinder-bestiary-items/pZX2G1Vh8XNn8DCw.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[q09mldN5OwQ33WsU.htm](pathfinder-bestiary-items/q09mldN5OwQ33WsU.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[q0Md652kB4awIFj1.htm](pathfinder-bestiary-items/q0Md652kB4awIFj1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[q0wjPcj5k9jWlgJc.htm](pathfinder-bestiary-items/q0wjPcj5k9jWlgJc.htm)|Alchemist's Fire (Lesser)|Fuego de Alquimista (Menor)|modificada|
|[Q163iaXFcQOEmvkG.htm](pathfinder-bestiary-items/Q163iaXFcQOEmvkG.htm)|Rock|Roca|modificada|
|[q1LhwW1syAxORXNt.htm](pathfinder-bestiary-items/q1LhwW1syAxORXNt.htm)|Staccato Strike|Golpe en staccato|modificada|
|[q1OobVjFqRsc58KI.htm](pathfinder-bestiary-items/q1OobVjFqRsc58KI.htm)|Negative Healing|Curación negativa|modificada|
|[Q1WWRiadk7sX7Gor.htm](pathfinder-bestiary-items/Q1WWRiadk7sX7Gor.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Q2tEMGeuUtWJQfpV.htm](pathfinder-bestiary-items/Q2tEMGeuUtWJQfpV.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[Q3hmK1bo6Sh5wQEk.htm](pathfinder-bestiary-items/Q3hmK1bo6Sh5wQEk.htm)|Tail|Tail|modificada|
|[Q3WxbSH9pxgZ7vKf.htm](pathfinder-bestiary-items/Q3WxbSH9pxgZ7vKf.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[q4wAc8vDoq72HKvR.htm](pathfinder-bestiary-items/q4wAc8vDoq72HKvR.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Q5WTbR9lUXHMagS8.htm](pathfinder-bestiary-items/Q5WTbR9lUXHMagS8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Q7WZ4aNo40BCE8gQ.htm](pathfinder-bestiary-items/Q7WZ4aNo40BCE8gQ.htm)|Blue Flames|Llamas azulesígera|modificada|
|[q8ASqdiBd9SflYVU.htm](pathfinder-bestiary-items/q8ASqdiBd9SflYVU.htm)|Spectral Ripple|Ondulación espectral|modificada|
|[Q8ir96PaHjj9G372.htm](pathfinder-bestiary-items/Q8ir96PaHjj9G372.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[q9A4hwlPK1sYF6p1.htm](pathfinder-bestiary-items/q9A4hwlPK1sYF6p1.htm)|Keen Scythe|Guadaña Afilada|modificada|
|[q9pwZOGUR5waARMo.htm](pathfinder-bestiary-items/q9pwZOGUR5waARMo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Q9TQFtMzhVqywWrB.htm](pathfinder-bestiary-items/Q9TQFtMzhVqywWrB.htm)|Deadly Cleave|Hendedura Letal|modificada|
|[q9WAG2gpmBXA8cfi.htm](pathfinder-bestiary-items/q9WAG2gpmBXA8cfi.htm)|Luminous Spark|Chispa Luminosa|modificada|
|[qa0M1ZbNy1NNa2x3.htm](pathfinder-bestiary-items/qa0M1ZbNy1NNa2x3.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[qaEGHfAlvrP6CGwS.htm](pathfinder-bestiary-items/qaEGHfAlvrP6CGwS.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[qaf6YFqcLoAJ5CM4.htm](pathfinder-bestiary-items/qaf6YFqcLoAJ5CM4.htm)|Snowblind|Snowblind|modificada|
|[qaGJzO6SddpAXZKg.htm](pathfinder-bestiary-items/qaGJzO6SddpAXZKg.htm)|Animated Weapons|Armas Animadas|modificada|
|[QaHD4bOypv7qPbfa.htm](pathfinder-bestiary-items/QaHD4bOypv7qPbfa.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[QAKar9jh0ZBRftHz.htm](pathfinder-bestiary-items/QAKar9jh0ZBRftHz.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[Qb7bNn98jqqwe6S8.htm](pathfinder-bestiary-items/Qb7bNn98jqqwe6S8.htm)|Negative Healing|Curación negativa|modificada|
|[QbCC0DUye0oVFVc4.htm](pathfinder-bestiary-items/QbCC0DUye0oVFVc4.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[QblhIHkvSph67WTI.htm](pathfinder-bestiary-items/QblhIHkvSph67WTI.htm)|Grab|Agarrado|modificada|
|[QBOHDvvqEsUhdk0Y.htm](pathfinder-bestiary-items/QBOHDvvqEsUhdk0Y.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qBULiNM4mOxrHx1F.htm](pathfinder-bestiary-items/qBULiNM4mOxrHx1F.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qbw4q1VE5jAxSzTj.htm](pathfinder-bestiary-items/qbw4q1VE5jAxSzTj.htm)|Light Flash|Destello luminoso|modificada|
|[Qc7duDVeN4puhd9w.htm](pathfinder-bestiary-items/Qc7duDVeN4puhd9w.htm)|Electrical Burst|Explosión eléctrica|modificada|
|[qcaLKbJ4n5L9k9Vc.htm](pathfinder-bestiary-items/qcaLKbJ4n5L9k9Vc.htm)|Wretched Weeps|Wretched Weeps|modificada|
|[qCmKPruRObzVL1Dr.htm](pathfinder-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|Avance chirriante|modificada|
|[QcXlYliyTvqiEDHM.htm](pathfinder-bestiary-items/QcXlYliyTvqiEDHM.htm)|Throw Rock|Arrojar roca|modificada|
|[qd9QFtwsgr9c7yLU.htm](pathfinder-bestiary-items/qd9QFtwsgr9c7yLU.htm)|Water Healing|Curación de agua|modificada|
|[qDVNiRd6lW8OACXz.htm](pathfinder-bestiary-items/qDVNiRd6lW8OACXz.htm)|Talon|Talon|modificada|
|[qDypnfBHeFgNsDjH.htm](pathfinder-bestiary-items/qDypnfBHeFgNsDjH.htm)|Wrappings Lash|Envolturas Lash|modificada|
|[qe55DvvyEW3AC1KT.htm](pathfinder-bestiary-items/qe55DvvyEW3AC1KT.htm)|Thoughtsense (Imprecise) 60 feet|Percibir pensamientos (Impreciso) 60 pies|modificada|
|[QEdIkRZIUAbC6n2O.htm](pathfinder-bestiary-items/QEdIkRZIUAbC6n2O.htm)|Stunning Strike|Golpe Aturdidor|modificada|
|[QEvRlTqwOEac04ov.htm](pathfinder-bestiary-items/QEvRlTqwOEac04ov.htm)|Attach|Aferrarse|modificada|
|[QEvUj7nK4bGVmLIe.htm](pathfinder-bestiary-items/QEvUj7nK4bGVmLIe.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[qeZfMzOwr8GL4Lbh.htm](pathfinder-bestiary-items/qeZfMzOwr8GL4Lbh.htm)|Jaws|Fauces|modificada|
|[qf9VkXweMZaEf2DP.htm](pathfinder-bestiary-items/qf9VkXweMZaEf2DP.htm)|Breath Weapon|Breath Weapon|modificada|
|[QFbge8UGIYYOgoxR.htm](pathfinder-bestiary-items/QFbge8UGIYYOgoxR.htm)|Scimitar|Cimitarra|modificada|
|[qfEnUMdNzlNIipCp.htm](pathfinder-bestiary-items/qfEnUMdNzlNIipCp.htm)|Tentacle|Tentáculo|modificada|
|[qFvtSo4GpY09J6wr.htm](pathfinder-bestiary-items/qFvtSo4GpY09J6wr.htm)|Dagger|Daga|modificada|
|[qg3r6OKHjX8qHiNS.htm](pathfinder-bestiary-items/qg3r6OKHjX8qHiNS.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[QG473vzOkJRCm8Sm.htm](pathfinder-bestiary-items/QG473vzOkJRCm8Sm.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[qGnxEwt7F2yY8OvT.htm](pathfinder-bestiary-items/qGnxEwt7F2yY8OvT.htm)|Corpse|Cadáver|modificada|
|[qhS5ZEJcqNFytqWd.htm](pathfinder-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|Fauces|modificada|
|[QhyMp71NOcnUwizh.htm](pathfinder-bestiary-items/QhyMp71NOcnUwizh.htm)|Dagger|Daga|modificada|
|[QI8J1BJYyzaJqGgr.htm](pathfinder-bestiary-items/QI8J1BJYyzaJqGgr.htm)|Throw Rock|Arrojar roca|modificada|
|[qIe9DjokLrx7mSvK.htm](pathfinder-bestiary-items/qIe9DjokLrx7mSvK.htm)|Boulder|Boulder|modificada|
|[qiGtT3oDu82IOdB6.htm](pathfinder-bestiary-items/qiGtT3oDu82IOdB6.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[QIkp9jKMkUCmBsVW.htm](pathfinder-bestiary-items/QIkp9jKMkUCmBsVW.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[QIPvgEw3eXLxKT8d.htm](pathfinder-bestiary-items/QIPvgEw3eXLxKT8d.htm)|Self-Repair|Autorreparación|modificada|
|[qIqgF3PIeW7W6Fft.htm](pathfinder-bestiary-items/qIqgF3PIeW7W6Fft.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[qiTNUKllqrwozDmi.htm](pathfinder-bestiary-items/qiTNUKllqrwozDmi.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Qj4erun9wbqBExS0.htm](pathfinder-bestiary-items/Qj4erun9wbqBExS0.htm)|Skittering Reposition|Recolocación correteante.|modificada|
|[qjRGAO0ktmv3fxHQ.htm](pathfinder-bestiary-items/qjRGAO0ktmv3fxHQ.htm)|Drain Bonded Item|Drenar objeto vinculado|modificada|
|[qJvFeMBK4WEJThI3.htm](pathfinder-bestiary-items/qJvFeMBK4WEJThI3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qJWbB2v6gs9de4fv.htm](pathfinder-bestiary-items/qJWbB2v6gs9de4fv.htm)|Horn|Cuerno|modificada|
|[qjYI8SCJrXWZopn6.htm](pathfinder-bestiary-items/qjYI8SCJrXWZopn6.htm)|Ogre Hook|Garfio de Ogro|modificada|
|[Qk0N8XSgIpwlNKmf.htm](pathfinder-bestiary-items/Qk0N8XSgIpwlNKmf.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Qk0PYD1Is307XwmU.htm](pathfinder-bestiary-items/Qk0PYD1Is307XwmU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[QK1RgrbqCLvwpciW.htm](pathfinder-bestiary-items/QK1RgrbqCLvwpciW.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[QK3Y9sXcSwM0Bsm8.htm](pathfinder-bestiary-items/QK3Y9sXcSwM0Bsm8.htm)|Claw|Garra|modificada|
|[qkKWOWe1wAc9Jffg.htm](pathfinder-bestiary-items/qkKWOWe1wAc9Jffg.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[QkmDtTbsggbI0MQy.htm](pathfinder-bestiary-items/QkmDtTbsggbI0MQy.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Qknp3UNQSMjNTUmL.htm](pathfinder-bestiary-items/Qknp3UNQSMjNTUmL.htm)|Grab|Agarrado|modificada|
|[QKo6NtH208FCd8UB.htm](pathfinder-bestiary-items/QKo6NtH208FCd8UB.htm)|Gnashing Grip|Agarre mordedor|modificada|
|[qKQ3f2hl6CPJXoKV.htm](pathfinder-bestiary-items/qKQ3f2hl6CPJXoKV.htm)|Jaws|Fauces|modificada|
|[QKtfubIn7OYITmUL.htm](pathfinder-bestiary-items/QKtfubIn7OYITmUL.htm)|Animated Weapon|Arma Animada|modificada|
|[QL3594a7048GPCem.htm](pathfinder-bestiary-items/QL3594a7048GPCem.htm)|Wild Empathy|Empatía salvaje|modificada|
|[QllK5fKffk7mVWTW.htm](pathfinder-bestiary-items/QllK5fKffk7mVWTW.htm)|Ground Manipulation|Manipular el suelo|modificada|
|[QMCJ4eMLrKIZq31c.htm](pathfinder-bestiary-items/QMCJ4eMLrKIZq31c.htm)|Infestation Aura|Aura de infestación|modificada|
|[qmffmrGzEQLV9DUw.htm](pathfinder-bestiary-items/qmffmrGzEQLV9DUw.htm)|Tail|Tail|modificada|
|[QmJkEvQgW1G9i7g9.htm](pathfinder-bestiary-items/QmJkEvQgW1G9i7g9.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[qN64iiPewb1CIOTo.htm](pathfinder-bestiary-items/qN64iiPewb1CIOTo.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[qniHJ3WvDHkUKTJ3.htm](pathfinder-bestiary-items/qniHJ3WvDHkUKTJ3.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[QnuvWppYTYaOJTUu.htm](pathfinder-bestiary-items/QnuvWppYTYaOJTUu.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[QnXBLcuv0HgvOjdo.htm](pathfinder-bestiary-items/QnXBLcuv0HgvOjdo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qOhxTyF0N56kRCTR.htm](pathfinder-bestiary-items/qOhxTyF0N56kRCTR.htm)|All-Around Vision|All-Around Vision|modificada|
|[qoJNaA8ESyVt6CJS.htm](pathfinder-bestiary-items/qoJNaA8ESyVt6CJS.htm)|Claw|Garra|modificada|
|[QotCsUsiVM6VxMDP.htm](pathfinder-bestiary-items/QotCsUsiVM6VxMDP.htm)|Frightful Presence|Frightful Presence|modificada|
|[QphofWAqcVVsApgN.htm](pathfinder-bestiary-items/QphofWAqcVVsApgN.htm)|Calming Bioluminescence|Bioluminiscencia calmante|modificada|
|[qpVAENrZzV6kOBCY.htm](pathfinder-bestiary-items/qpVAENrZzV6kOBCY.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[QPywhw5X98V0NX5g.htm](pathfinder-bestiary-items/QPywhw5X98V0NX5g.htm)|High Winds|Fuertes vientos|modificada|
|[QpzMidSalFaHGivw.htm](pathfinder-bestiary-items/QpzMidSalFaHGivw.htm)|Change Shape|Change Shape|modificada|
|[qqGsZZCFNTIdlk8l.htm](pathfinder-bestiary-items/qqGsZZCFNTIdlk8l.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[qqpgMalbjoAykhjP.htm](pathfinder-bestiary-items/qqpgMalbjoAykhjP.htm)|Keen Scythe|Guadaña Afilada|modificada|
|[QqqqheeizksJ9NkJ.htm](pathfinder-bestiary-items/QqqqheeizksJ9NkJ.htm)|Aura of Peace|Aura de Paz|modificada|
|[qqqYIHulqpSQmjAc.htm](pathfinder-bestiary-items/qqqYIHulqpSQmjAc.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qqY5IlOQecESUHZP.htm](pathfinder-bestiary-items/qqY5IlOQecESUHZP.htm)|Tail Lash|Azote con la cola|modificada|
|[qR2C9ASEA77YOzgK.htm](pathfinder-bestiary-items/qR2C9ASEA77YOzgK.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[QRArjmTp1QFxOrMG.htm](pathfinder-bestiary-items/QRArjmTp1QFxOrMG.htm)|Poison Tooth|Diente venenoso|modificada|
|[QReEjgBjoIXn5NnC.htm](pathfinder-bestiary-items/QReEjgBjoIXn5NnC.htm)|Bladestorm|Tormenta de cuchillas|modificada|
|[qrxSbCbHPvFU1sVs.htm](pathfinder-bestiary-items/qrxSbCbHPvFU1sVs.htm)|Stinger|Aguijón|modificada|
|[qSJwECMRJYL4xDUN.htm](pathfinder-bestiary-items/qSJwECMRJYL4xDUN.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[qssb4IVNo6C2zIwe.htm](pathfinder-bestiary-items/qssb4IVNo6C2zIwe.htm)|Self-Resurrection|Autorresurrección|modificada|
|[QtDv0dYBxOQuvIS5.htm](pathfinder-bestiary-items/QtDv0dYBxOQuvIS5.htm)|Rock|Roca|modificada|
|[QTipkgJAwLBZFcwu.htm](pathfinder-bestiary-items/QTipkgJAwLBZFcwu.htm)|Focused Assault|Asalto concentrado|modificada|
|[QtiX9tEriHb92lTU.htm](pathfinder-bestiary-items/QtiX9tEriHb92lTU.htm)|Pounce|Abalanzarse|modificada|
|[qtsF75k2Zterahrt.htm](pathfinder-bestiary-items/qtsF75k2Zterahrt.htm)|Vulnerable to Sunlight|Vulnerabilidad a la luz del sol|modificada|
|[qtXj0TB36n30L7my.htm](pathfinder-bestiary-items/qtXj0TB36n30L7my.htm)|Disarm|Desarmar|modificada|
|[qu3FtGya8YY0lQf6.htm](pathfinder-bestiary-items/qu3FtGya8YY0lQf6.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[qUaqzRZJuoid0Hha.htm](pathfinder-bestiary-items/qUaqzRZJuoid0Hha.htm)|Ferocity|Ferocidad|modificada|
|[QUcIjJglRfQBcSRB.htm](pathfinder-bestiary-items/QUcIjJglRfQBcSRB.htm)|Jaws|Fauces|modificada|
|[qUGNxVPDLyqLK3is.htm](pathfinder-bestiary-items/qUGNxVPDLyqLK3is.htm)|Fist|Puño|modificada|
|[qugqAWTLubz9fcj2.htm](pathfinder-bestiary-items/qugqAWTLubz9fcj2.htm)|Speed Surge|Arranque de velocidad|modificada|
|[QulwvQlkERm3i0aV.htm](pathfinder-bestiary-items/QulwvQlkERm3i0aV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qV0AlDGF3k44wWNb.htm](pathfinder-bestiary-items/qV0AlDGF3k44wWNb.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[Qv3klzQNlAwTTmrJ.htm](pathfinder-bestiary-items/Qv3klzQNlAwTTmrJ.htm)|Tail|Tail|modificada|
|[QVSO6xmJIGdqfP0G.htm](pathfinder-bestiary-items/QVSO6xmJIGdqfP0G.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[qWBeJdRzUrGmE3Di.htm](pathfinder-bestiary-items/qWBeJdRzUrGmE3Di.htm)|Wendigo Torment|Tormento Wendigo|modificada|
|[QWBpk7SbuXW6RHU1.htm](pathfinder-bestiary-items/QWBpk7SbuXW6RHU1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qwvZRNMyaVviFlN8.htm](pathfinder-bestiary-items/qwvZRNMyaVviFlN8.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[QX0Ta7fQzEhuiCCB.htm](pathfinder-bestiary-items/QX0Ta7fQzEhuiCCB.htm)|Dagger|Daga|modificada|
|[qX4zB0ycmWv3B6wH.htm](pathfinder-bestiary-items/qX4zB0ycmWv3B6wH.htm)|Trample|Trample|modificada|
|[QxEVY9qhM6ZZyiar.htm](pathfinder-bestiary-items/QxEVY9qhM6ZZyiar.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[qXMpQcdrpm3l454K.htm](pathfinder-bestiary-items/qXMpQcdrpm3l454K.htm)|Greataxe|Greataxe|modificada|
|[qY4Gqits5ZaytjDY.htm](pathfinder-bestiary-items/qY4Gqits5ZaytjDY.htm)|Pain Frenzy|Enfurecido por el dolor|modificada|
|[Qy7s2ZDFTvr7xR1m.htm](pathfinder-bestiary-items/Qy7s2ZDFTvr7xR1m.htm)|Web Trap|Trampa telara|modificada|
|[qyAQlXz42QsRrsCo.htm](pathfinder-bestiary-items/qyAQlXz42QsRrsCo.htm)|Flaming Scimitar|Cimitarra Flamígera|modificada|
|[QySe64WqYlaY7p5g.htm](pathfinder-bestiary-items/QySe64WqYlaY7p5g.htm)|Cloud Walk|Cloud Walk|modificada|
|[Qz0ZCScbiX2aU9gz.htm](pathfinder-bestiary-items/Qz0ZCScbiX2aU9gz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qZG63nOxzyyLxiMs.htm](pathfinder-bestiary-items/qZG63nOxzyyLxiMs.htm)|Trample|Trample|modificada|
|[Qzt1bWHKvD7X2ua8.htm](pathfinder-bestiary-items/Qzt1bWHKvD7X2ua8.htm)|Change Shape|Change Shape|modificada|
|[r035iPNnkvHhoCqt.htm](pathfinder-bestiary-items/r035iPNnkvHhoCqt.htm)|Flail|Mayal|modificada|
|[r04hQrUajd4T3K6Z.htm](pathfinder-bestiary-items/r04hQrUajd4T3K6Z.htm)|Wing Deflection|Desvío con el ala|modificada|
|[r0h2w4y8aEPKbrb1.htm](pathfinder-bestiary-items/r0h2w4y8aEPKbrb1.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[r0zMXxalwdgrXzf3.htm](pathfinder-bestiary-items/r0zMXxalwdgrXzf3.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[r1b2EOtm5LT3mHWK.htm](pathfinder-bestiary-items/r1b2EOtm5LT3mHWK.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[R1hhANtemlX6gt4D.htm](pathfinder-bestiary-items/R1hhANtemlX6gt4D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r1SlNgELge3BicQv.htm](pathfinder-bestiary-items/r1SlNgELge3BicQv.htm)|Stench|Hedor|modificada|
|[R2OzRzNgkA2Ivtk0.htm](pathfinder-bestiary-items/R2OzRzNgkA2Ivtk0.htm)|Jaws|Fauces|modificada|
|[R3u6nnIK33qXVJNK.htm](pathfinder-bestiary-items/R3u6nnIK33qXVJNK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[r3WeFaPkohKTnc3H.htm](pathfinder-bestiary-items/r3WeFaPkohKTnc3H.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[r4Qkdy83VOkw6r0p.htm](pathfinder-bestiary-items/r4Qkdy83VOkw6r0p.htm)|Boot|Boot|modificada|
|[R4yDJ8qRT5NaKyDL.htm](pathfinder-bestiary-items/R4yDJ8qRT5NaKyDL.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[r5JyVLxepP2bpNAq.htm](pathfinder-bestiary-items/r5JyVLxepP2bpNAq.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[R5LM2Ijl78zvdKkF.htm](pathfinder-bestiary-items/R5LM2Ijl78zvdKkF.htm)|Change Shape|Change Shape|modificada|
|[r5yruYZzaDx9MoHR.htm](pathfinder-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[r6CoZc9HQihbtMvD.htm](pathfinder-bestiary-items/r6CoZc9HQihbtMvD.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[r7eZQc9QrFmYE1sw.htm](pathfinder-bestiary-items/r7eZQc9QrFmYE1sw.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[r7vHhfdRSulBJtov.htm](pathfinder-bestiary-items/r7vHhfdRSulBJtov.htm)|Truth Vulnerability|Vulnerabilidad a la verdad|modificada|
|[R7wSakgDTKzgP8Eo.htm](pathfinder-bestiary-items/R7wSakgDTKzgP8Eo.htm)|Lifedrinker|Absorbedor vida|modificada|
|[r89JPcf2a0fUQOAP.htm](pathfinder-bestiary-items/r89JPcf2a0fUQOAP.htm)|Frightful Presence|Frightful Presence|modificada|
|[R8f6nx9jMnFB03qP.htm](pathfinder-bestiary-items/R8f6nx9jMnFB03qP.htm)|Savage|Salvajismo|modificada|
|[r95YqiTkdXz73fSA.htm](pathfinder-bestiary-items/r95YqiTkdXz73fSA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[r9cb3wi4i5kmkIVZ.htm](pathfinder-bestiary-items/r9cb3wi4i5kmkIVZ.htm)|Wing|Ala|modificada|
|[R9CYDurnZPOlDiFg.htm](pathfinder-bestiary-items/R9CYDurnZPOlDiFg.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[R9oCMdMtlo1aed3A.htm](pathfinder-bestiary-items/R9oCMdMtlo1aed3A.htm)|Wing Deflection|Desvío con el ala|modificada|
|[Ra5fO9dQyDnBzGeh.htm](pathfinder-bestiary-items/Ra5fO9dQyDnBzGeh.htm)|Horn|Cuerno|modificada|
|[rA69Ao7ZsTSnYPbJ.htm](pathfinder-bestiary-items/rA69Ao7ZsTSnYPbJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RAFn5P8tJAjf5eEe.htm](pathfinder-bestiary-items/RAFn5P8tJAjf5eEe.htm)|Retaliatory Strike|Golpe en represalia|modificada|
|[RaYfxxlT031lUgCP.htm](pathfinder-bestiary-items/RaYfxxlT031lUgCP.htm)|Coffin Restoration|Restauración de ataúdes|modificada|
|[RB4XdKuhVpQcNu9I.htm](pathfinder-bestiary-items/RB4XdKuhVpQcNu9I.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rbhTSJX35TItIjZX.htm](pathfinder-bestiary-items/rbhTSJX35TItIjZX.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[rBKIlYFoV3vW1kEN.htm](pathfinder-bestiary-items/rBKIlYFoV3vW1kEN.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[rbRI99thbIthUP1M.htm](pathfinder-bestiary-items/rbRI99thbIthUP1M.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RBSzDryfenHBXU42.htm](pathfinder-bestiary-items/RBSzDryfenHBXU42.htm)|Claw|Garra|modificada|
|[rbYlQJlNI4Qk0aXW.htm](pathfinder-bestiary-items/rbYlQJlNI4Qk0aXW.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[rc2RmYmto2nlnmbA.htm](pathfinder-bestiary-items/rc2RmYmto2nlnmbA.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[RCcD66otbEEUJwke.htm](pathfinder-bestiary-items/RCcD66otbEEUJwke.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[rcFKhekRBFACg0ED.htm](pathfinder-bestiary-items/rcFKhekRBFACg0ED.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[rCo3WVukDgsbPhLD.htm](pathfinder-bestiary-items/rCo3WVukDgsbPhLD.htm)|Spore Cloud|Nube de esporas|modificada|
|[RDAI0FQdtpHFrFa5.htm](pathfinder-bestiary-items/RDAI0FQdtpHFrFa5.htm)|Intense Heat|Calor intenso|modificada|
|[RDDSANqANJv09ktQ.htm](pathfinder-bestiary-items/RDDSANqANJv09ktQ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[RdYqZ3Dt6i0ca94C.htm](pathfinder-bestiary-items/RdYqZ3Dt6i0ca94C.htm)|Negative Healing|Curación negativa|modificada|
|[rE3Xk58S3o4oHxdo.htm](pathfinder-bestiary-items/rE3Xk58S3o4oHxdo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Re5HgCWQJTyzGewL.htm](pathfinder-bestiary-items/Re5HgCWQJTyzGewL.htm)|Web Trap|Trampa telara|modificada|
|[Re9Ws4fM67Zs7ZLW.htm](pathfinder-bestiary-items/Re9Ws4fM67Zs7ZLW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[reBqKuVcCW8B0Kfe.htm](pathfinder-bestiary-items/reBqKuVcCW8B0Kfe.htm)|Knockdown|Derribo|modificada|
|[REiNYp4cWbH6OcX2.htm](pathfinder-bestiary-items/REiNYp4cWbH6OcX2.htm)|+3 Status to All Saves vs. Divine Magic|+3 situación a todas las salvaciones contra magia divina.|modificada|
|[reoPbPNpXyLy56Ny.htm](pathfinder-bestiary-items/reoPbPNpXyLy56Ny.htm)|Shroud of Flame|Sudario de Flamígera|modificada|
|[RFk0rK9Iw5uP4Q4K.htm](pathfinder-bestiary-items/RFk0rK9Iw5uP4Q4K.htm)|Tentacle|Tentáculo|modificada|
|[rfNAJhXoI4vQWbVg.htm](pathfinder-bestiary-items/rfNAJhXoI4vQWbVg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RG4odvzk9bSpL19C.htm](pathfinder-bestiary-items/RG4odvzk9bSpL19C.htm)|Flame Jet|Flame Jet|modificada|
|[rGbtqEmUFQBjGvOd.htm](pathfinder-bestiary-items/rGbtqEmUFQBjGvOd.htm)|Hungersense (Imprecise) 30 feet|Sentir el hambre (Impreciso) 30 pies|modificada|
|[rGgLU8CkJNTv7kLC.htm](pathfinder-bestiary-items/rGgLU8CkJNTv7kLC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rGTmjeER3V1KY02w.htm](pathfinder-bestiary-items/rGTmjeER3V1KY02w.htm)|Fast Swallow|Tragar de golpe|modificada|
|[RgUtXbdxY4ir0GNG.htm](pathfinder-bestiary-items/RgUtXbdxY4ir0GNG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rGYOx7SHsrayrSsA.htm](pathfinder-bestiary-items/rGYOx7SHsrayrSsA.htm)|Jaws|Fauces|modificada|
|[rh2quedjlo5wYnpP.htm](pathfinder-bestiary-items/rh2quedjlo5wYnpP.htm)|+1 Status to All Saves vs. Death Effects|+1 situación a todas las salvaciones contra efectos de muerte.|modificada|
|[RhGXKyPEo0bWS5D1.htm](pathfinder-bestiary-items/RhGXKyPEo0bWS5D1.htm)|Zombie Rot|Zombie Rot|modificada|
|[RHN04hf0GJqs0PN9.htm](pathfinder-bestiary-items/RHN04hf0GJqs0PN9.htm)|Constant Spells|Constant Spells|modificada|
|[RHsGeVOoFFoGYyZ1.htm](pathfinder-bestiary-items/RHsGeVOoFFoGYyZ1.htm)|Change Shape|Change Shape|modificada|
|[rHVSJCwiDlcfs7Ms.htm](pathfinder-bestiary-items/rHVSJCwiDlcfs7Ms.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[RI07SPUsr3EfIXQX.htm](pathfinder-bestiary-items/RI07SPUsr3EfIXQX.htm)|Tail|Tail|modificada|
|[RI1LAadtF6vq96g2.htm](pathfinder-bestiary-items/RI1LAadtF6vq96g2.htm)|Claw|Garra|modificada|
|[ri68Bk0TcV1k9lqS.htm](pathfinder-bestiary-items/ri68Bk0TcV1k9lqS.htm)|Altered Weather|Clima alterado|modificada|
|[RibPgalaxUxyuwym.htm](pathfinder-bestiary-items/RibPgalaxUxyuwym.htm)|Pack Attack|Ataque en manada|modificada|
|[RiC8zTuKfW44G8Mp.htm](pathfinder-bestiary-items/RiC8zTuKfW44G8Mp.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[RiTHbEGvltSHb56S.htm](pathfinder-bestiary-items/RiTHbEGvltSHb56S.htm)|Draft Contract|Redactar contrato|modificada|
|[rjGVnVgJiMZZR6zk.htm](pathfinder-bestiary-items/rjGVnVgJiMZZR6zk.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[RJHEA3Ugy5eo8bGd.htm](pathfinder-bestiary-items/RJHEA3Ugy5eo8bGd.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[rJiIUBXia2sB40kR.htm](pathfinder-bestiary-items/rJiIUBXia2sB40kR.htm)|Woodland Stride|Paso forestal|modificada|
|[RJlLjrzqdXREWyPR.htm](pathfinder-bestiary-items/RJlLjrzqdXREWyPR.htm)|Telekinetic Storm|Tormenta Telequinética|modificada|
|[RjqqCquV2ghiNEdO.htm](pathfinder-bestiary-items/RjqqCquV2ghiNEdO.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[Rk71VDo2VsvkYsEO.htm](pathfinder-bestiary-items/Rk71VDo2VsvkYsEO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rk9Qa3TRqh7vnR4r.htm](pathfinder-bestiary-items/rk9Qa3TRqh7vnR4r.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[RKCbcbBugZzIrxdw.htm](pathfinder-bestiary-items/RKCbcbBugZzIrxdw.htm)|Menacing Guardian|Guardián amenazante|modificada|
|[rkM1yAFkUMEFUnsZ.htm](pathfinder-bestiary-items/rkM1yAFkUMEFUnsZ.htm)|Jaws|Fauces|modificada|
|[RKyutDZZD9yAhzjX.htm](pathfinder-bestiary-items/RKyutDZZD9yAhzjX.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[rL7A4RH2uX8GGbWM.htm](pathfinder-bestiary-items/rL7A4RH2uX8GGbWM.htm)|Wild Empathy|Empatía salvaje|modificada|
|[RlajqjwuSiKB8NGC.htm](pathfinder-bestiary-items/RlajqjwuSiKB8NGC.htm)|Impose Paralysis|Imponer parálisis|modificada|
|[rlBRYOIKCiyQfcQ8.htm](pathfinder-bestiary-items/rlBRYOIKCiyQfcQ8.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[rLFEG0L8t229JOcM.htm](pathfinder-bestiary-items/rLFEG0L8t229JOcM.htm)|Jaws|Fauces|modificada|
|[RLggEzBamBn1M5sX.htm](pathfinder-bestiary-items/RLggEzBamBn1M5sX.htm)|Breath Weapon|Breath Weapon|modificada|
|[RLXxZqgZH5Ym4SGI.htm](pathfinder-bestiary-items/RLXxZqgZH5Ym4SGI.htm)|Blinding Aura|Aura cegadora|modificada|
|[RMbB0Cy1iBEXCD5A.htm](pathfinder-bestiary-items/RMbB0Cy1iBEXCD5A.htm)|Keen Longsword|Afilada Longsword|modificada|
|[RmFwZAbN0gQEv92W.htm](pathfinder-bestiary-items/RmFwZAbN0gQEv92W.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[rMKHbVkOcGipVlzt.htm](pathfinder-bestiary-items/rMKHbVkOcGipVlzt.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[RN3i79HbKaXreAiG.htm](pathfinder-bestiary-items/RN3i79HbKaXreAiG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rn3k17gHgz0K9teg.htm](pathfinder-bestiary-items/rn3k17gHgz0K9teg.htm)|Claw|Garra|modificada|
|[rNMUpQjQil41A67R.htm](pathfinder-bestiary-items/rNMUpQjQil41A67R.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[RnOot5zAIUVb7mhI.htm](pathfinder-bestiary-items/RnOot5zAIUVb7mhI.htm)|Greataxe|Greataxe|modificada|
|[RNVvCjM6WpoASTKg.htm](pathfinder-bestiary-items/RNVvCjM6WpoASTKg.htm)|Constrict|Restringir|modificada|
|[rog22HpLDkUnfolA.htm](pathfinder-bestiary-items/rog22HpLDkUnfolA.htm)|Goat Horns|Cuernos de cabra|modificada|
|[RoHDkOS2aYfoab02.htm](pathfinder-bestiary-items/RoHDkOS2aYfoab02.htm)|Reflect Spell|Reflejar conjuros|modificada|
|[ROihL0GMWnjLTGFx.htm](pathfinder-bestiary-items/ROihL0GMWnjLTGFx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[ROtcz4ACRnmedQua.htm](pathfinder-bestiary-items/ROtcz4ACRnmedQua.htm)|Darting Strike|Golpe de Dardo|modificada|
|[Rp6sCg2LppmsJwKC.htm](pathfinder-bestiary-items/Rp6sCg2LppmsJwKC.htm)|Jaws|Fauces|modificada|
|[rpjuMiQRY2h0zJ1H.htm](pathfinder-bestiary-items/rpjuMiQRY2h0zJ1H.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[RQ0SxaYgQ0WCzXyb.htm](pathfinder-bestiary-items/RQ0SxaYgQ0WCzXyb.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[rQ1dAhk5hHaVicBP.htm](pathfinder-bestiary-items/rQ1dAhk5hHaVicBP.htm)|Grab|Agarrado|modificada|
|[rQa09Nb9LtSI9UYi.htm](pathfinder-bestiary-items/rQa09Nb9LtSI9UYi.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[rQT7KUvA4yzEm4PO.htm](pathfinder-bestiary-items/rQT7KUvA4yzEm4PO.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[rQVfc5bR7Xpe25cT.htm](pathfinder-bestiary-items/rQVfc5bR7Xpe25cT.htm)|Fling|Lanzar por los aires|modificada|
|[RQyE6xYaEg8W7C38.htm](pathfinder-bestiary-items/RQyE6xYaEg8W7C38.htm)|Invigorating Passion|Pasión vigorizante|modificada|
|[RRFUmIxRsdfJqd8F.htm](pathfinder-bestiary-items/RRFUmIxRsdfJqd8F.htm)|Breath Weapon|Breath Weapon|modificada|
|[RRokxFBvuXGVzodp.htm](pathfinder-bestiary-items/RRokxFBvuXGVzodp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RrUfNcdLaL1BhWLR.htm](pathfinder-bestiary-items/RrUfNcdLaL1BhWLR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rs9VqajU6hTlLt5o.htm](pathfinder-bestiary-items/rs9VqajU6hTlLt5o.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RSzJjCjwpz8jIS3J.htm](pathfinder-bestiary-items/RSzJjCjwpz8jIS3J.htm)|Iron Golem Poison|Veneno de gólem de hierro|modificada|
|[rtbwOcOPVCHoeTWz.htm](pathfinder-bestiary-items/rtbwOcOPVCHoeTWz.htm)|Constant Spells|Constant Spells|modificada|
|[rTDme0840mDZkokU.htm](pathfinder-bestiary-items/rTDme0840mDZkokU.htm)|Swallow Whole|Engullir Todo|modificada|
|[rtgmVsdePPj1Qutv.htm](pathfinder-bestiary-items/rtgmVsdePPj1Qutv.htm)|Fist|Puño|modificada|
|[RUcX4XrsDpkjIiPy.htm](pathfinder-bestiary-items/RUcX4XrsDpkjIiPy.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[rUzXVdDT4B94gK6i.htm](pathfinder-bestiary-items/rUzXVdDT4B94gK6i.htm)|Trident|Trident|modificada|
|[rvraBop9oOSwcjkq.htm](pathfinder-bestiary-items/rvraBop9oOSwcjkq.htm)|Swift Leap|Salto veloz|modificada|
|[rw75XVfj9XlY7JGq.htm](pathfinder-bestiary-items/rw75XVfj9XlY7JGq.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[rWeRpLhEFS7p5VCy.htm](pathfinder-bestiary-items/rWeRpLhEFS7p5VCy.htm)|Formation|Formación|modificada|
|[Rwgsw9DmxxBRN4NY.htm](pathfinder-bestiary-items/Rwgsw9DmxxBRN4NY.htm)|Constrict|Restringir|modificada|
|[rwK2kJnSkkWTKSwP.htm](pathfinder-bestiary-items/rwK2kJnSkkWTKSwP.htm)|Mandibles|Mandíbulas|modificada|
|[RWLfly211783dq5i.htm](pathfinder-bestiary-items/RWLfly211783dq5i.htm)|Tactician of Cocytus|Táctico de Cocito|modificada|
|[RX0OsA2SH1cSF0rn.htm](pathfinder-bestiary-items/RX0OsA2SH1cSF0rn.htm)|Horn|Cuerno|modificada|
|[rXhfsoFPa9y2NdHs.htm](pathfinder-bestiary-items/rXhfsoFPa9y2NdHs.htm)|Grab|Agarrado|modificada|
|[rxoyBugXyIn6mRoC.htm](pathfinder-bestiary-items/rxoyBugXyIn6mRoC.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Rxv5Fh6XLsy8yyXk.htm](pathfinder-bestiary-items/Rxv5Fh6XLsy8yyXk.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[rYCzNNJekAP07yuk.htm](pathfinder-bestiary-items/rYCzNNJekAP07yuk.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[RYEcc50Tn0g5GE6m.htm](pathfinder-bestiary-items/RYEcc50Tn0g5GE6m.htm)|Tail Trip|Derribar cola|modificada|
|[ryo9HmpaThFAUxD6.htm](pathfinder-bestiary-items/ryo9HmpaThFAUxD6.htm)|Constrict|Restringir|modificada|
|[RyRiVUoP2scafR6g.htm](pathfinder-bestiary-items/RyRiVUoP2scafR6g.htm)|Hateful Tide|Marea Odiosa|modificada|
|[rYzzaXPAJ15uHtmD.htm](pathfinder-bestiary-items/rYzzaXPAJ15uHtmD.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[rz1sIDAc7CEOMR8R.htm](pathfinder-bestiary-items/rz1sIDAc7CEOMR8R.htm)|Claw|Garra|modificada|
|[rZ6DDkGMLvCQH70m.htm](pathfinder-bestiary-items/rZ6DDkGMLvCQH70m.htm)|Hungry Flurry|Ráfaga hambrienta|modificada|
|[RZBxog3E2iCbQY5r.htm](pathfinder-bestiary-items/RZBxog3E2iCbQY5r.htm)|Main-Gauche|Main-Gauche|modificada|
|[RZijNBizRW62zYQN.htm](pathfinder-bestiary-items/RZijNBizRW62zYQN.htm)|Flame Of Justice|Llama de la justiciaígera|modificada|
|[RZv15pm5vEmZAZhK.htm](pathfinder-bestiary-items/RZv15pm5vEmZAZhK.htm)|Infernal Temptation|Tentación Infernal|modificada|
|[s1vorlaJQl2JXBN2.htm](pathfinder-bestiary-items/s1vorlaJQl2JXBN2.htm)|Baton|Baton|modificada|
|[s2CoilBTqkwO72Wu.htm](pathfinder-bestiary-items/s2CoilBTqkwO72Wu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s2SFLAZVqryeGl0O.htm](pathfinder-bestiary-items/s2SFLAZVqryeGl0O.htm)|Trident|Trident|modificada|
|[S2UbYSN0YRKJbMS0.htm](pathfinder-bestiary-items/S2UbYSN0YRKJbMS0.htm)|Trample|Trample|modificada|
|[S2UO1rBCsuy3H5Ii.htm](pathfinder-bestiary-items/S2UO1rBCsuy3H5Ii.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S32QCqZ8XlolYIfE.htm](pathfinder-bestiary-items/S32QCqZ8XlolYIfE.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[s4VdQZkBvye1Qm6o.htm](pathfinder-bestiary-items/s4VdQZkBvye1Qm6o.htm)|Scent (Imprecise) 100 feet|Olor (Impreciso) 100 pies|modificada|
|[s5DYWIxSytUTy1x2.htm](pathfinder-bestiary-items/s5DYWIxSytUTy1x2.htm)|Fangs|Colmillos|modificada|
|[S5RuYzvwzPUK3wzT.htm](pathfinder-bestiary-items/S5RuYzvwzPUK3wzT.htm)|Throw Rock|Arrojar roca|modificada|
|[S5TEXAbFO5yNQ9JP.htm](pathfinder-bestiary-items/S5TEXAbFO5yNQ9JP.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[S6DALcZPm4YWrvWR.htm](pathfinder-bestiary-items/S6DALcZPm4YWrvWR.htm)|Jaws|Fauces|modificada|
|[S6FJYbxVlzTTwn0T.htm](pathfinder-bestiary-items/S6FJYbxVlzTTwn0T.htm)|Smoke Vision|Visión de Humo|modificada|
|[S7hKN8DRaICYTRDX.htm](pathfinder-bestiary-items/S7hKN8DRaICYTRDX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[S7Khn0wjpXjjyPAp.htm](pathfinder-bestiary-items/S7Khn0wjpXjjyPAp.htm)|Mist Escape|Niebla Huir|modificada|
|[s9Lpslvp7eJbAjWr.htm](pathfinder-bestiary-items/s9Lpslvp7eJbAjWr.htm)|Redirect Fire|Redirigir fuego|modificada|
|[S9z7EwyzhGnCFZDI.htm](pathfinder-bestiary-items/S9z7EwyzhGnCFZDI.htm)|Enormous Inhalation|Inhalación colosal|modificada|
|[sA9IvnRiBKdOegEF.htm](pathfinder-bestiary-items/sA9IvnRiBKdOegEF.htm)|Wing|Ala|modificada|
|[Saaz9FC4uuWqbzUs.htm](pathfinder-bestiary-items/Saaz9FC4uuWqbzUs.htm)|Swallow Whole|Engullir Todo|modificada|
|[SaMUZistsZa0Bskd.htm](pathfinder-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|All-Around Vision|modificada|
|[saoVl2MlopNbuTo8.htm](pathfinder-bestiary-items/saoVl2MlopNbuTo8.htm)|Aklys|Aklys|modificada|
|[saqRnmZhtXKwqFed.htm](pathfinder-bestiary-items/saqRnmZhtXKwqFed.htm)|Demilich Eye Gems|Gemas oculares de semiliche|modificada|
|[SbBp90Oz1LZfllYh.htm](pathfinder-bestiary-items/SbBp90Oz1LZfllYh.htm)|Spear|Lanza|modificada|
|[sBRqXEDoaHJdg0SF.htm](pathfinder-bestiary-items/sBRqXEDoaHJdg0SF.htm)|Blackaxe|Blackaxe|modificada|
|[SC5giI24l8gEnx5E.htm](pathfinder-bestiary-items/SC5giI24l8gEnx5E.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[SCGxYeDTWb4Hznwu.htm](pathfinder-bestiary-items/SCGxYeDTWb4Hznwu.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[sCLiSoe59ubmpiPk.htm](pathfinder-bestiary-items/sCLiSoe59ubmpiPk.htm)|Change Shape|Change Shape|modificada|
|[sClpmySLDIh95u0m.htm](pathfinder-bestiary-items/sClpmySLDIh95u0m.htm)|Wavesense 30 feet|Wavesense 30 pies|modificada|
|[scTXJJONiH5Y5Rpf.htm](pathfinder-bestiary-items/scTXJJONiH5Y5Rpf.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[scw9HnbCVwAyFzok.htm](pathfinder-bestiary-items/scw9HnbCVwAyFzok.htm)|Rend|Rasgadura|modificada|
|[SDPanMrj394qVPdp.htm](pathfinder-bestiary-items/SDPanMrj394qVPdp.htm)|Mandibles|Mandíbulas|modificada|
|[SdRYcTTnrhfiMQBr.htm](pathfinder-bestiary-items/SdRYcTTnrhfiMQBr.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[SdvOPdD36kKFhSXc.htm](pathfinder-bestiary-items/SdvOPdD36kKFhSXc.htm)|Ward Contract|Custodiar contrato|modificada|
|[Se0RGAQyLYAwETrV.htm](pathfinder-bestiary-items/Se0RGAQyLYAwETrV.htm)|Beak|Beak|modificada|
|[SE3QsBzRX2bmkrnR.htm](pathfinder-bestiary-items/SE3QsBzRX2bmkrnR.htm)|Brain Loss|Pérdida de cerebros|modificada|
|[sE5xyEupWe7f3yot.htm](pathfinder-bestiary-items/sE5xyEupWe7f3yot.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[sEBLtOOpyQCNzmBJ.htm](pathfinder-bestiary-items/sEBLtOOpyQCNzmBJ.htm)|Jaws|Fauces|modificada|
|[SeJXe222cuooTJUx.htm](pathfinder-bestiary-items/SeJXe222cuooTJUx.htm)|Split|Split|modificada|
|[sElxdOJLECdfNADx.htm](pathfinder-bestiary-items/sElxdOJLECdfNADx.htm)|Constant Spells|Constant Spells|modificada|
|[SFhZjJoSIAYrPWoO.htm](pathfinder-bestiary-items/SFhZjJoSIAYrPWoO.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[SFJ6XAQzgQGC5zVV.htm](pathfinder-bestiary-items/SFJ6XAQzgQGC5zVV.htm)|Ground Slam|Aporrear el suelo|modificada|
|[SfTYucTXJbISe6Da.htm](pathfinder-bestiary-items/SfTYucTXJbISe6Da.htm)|All-Around Vision|All-Around Vision|modificada|
|[sG3tFge2ls3X5eek.htm](pathfinder-bestiary-items/sG3tFge2ls3X5eek.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[sGGqzFbYP70AiQpk.htm](pathfinder-bestiary-items/sGGqzFbYP70AiQpk.htm)|Jaws|Fauces|modificada|
|[sgKONsx24qNmQXvo.htm](pathfinder-bestiary-items/sgKONsx24qNmQXvo.htm)|Breath Weapon|Breath Weapon|modificada|
|[sgLK6WwDNZXG3X0e.htm](pathfinder-bestiary-items/sgLK6WwDNZXG3X0e.htm)|Trample|Trample|modificada|
|[SHBduQHQxTGNFYyK.htm](pathfinder-bestiary-items/SHBduQHQxTGNFYyK.htm)|Constant Spells|Constant Spells|modificada|
|[sHRh4SLqM1rBlpHb.htm](pathfinder-bestiary-items/sHRh4SLqM1rBlpHb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[siaeh3ZOXYXVsYlo.htm](pathfinder-bestiary-items/siaeh3ZOXYXVsYlo.htm)|Ether Spider Venom|Veneno de ara de éter|modificada|
|[siaZvbqZwz1jMZvH.htm](pathfinder-bestiary-items/siaZvbqZwz1jMZvH.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[sIfVG6GaZyAaZ3xW.htm](pathfinder-bestiary-items/sIfVG6GaZyAaZ3xW.htm)|Pharyngeal Jaws|Mandíbulas faríngeas|modificada|
|[Sj3qaBkh1exbNXJY.htm](pathfinder-bestiary-items/Sj3qaBkh1exbNXJY.htm)|Dagger|Daga|modificada|
|[sj5zKlTpK679kiLP.htm](pathfinder-bestiary-items/sj5zKlTpK679kiLP.htm)|Constant Spells|Constant Spells|modificada|
|[sJEzuYy6exvJPlpq.htm](pathfinder-bestiary-items/sJEzuYy6exvJPlpq.htm)|Fangs|Colmillos|modificada|
|[Sjf1HpLuxn9U6lr2.htm](pathfinder-bestiary-items/Sjf1HpLuxn9U6lr2.htm)|Improved Grab|Agarrado mejorado|modificada|
|[SJraLZmz7x1C04ki.htm](pathfinder-bestiary-items/SJraLZmz7x1C04ki.htm)|Vulnerable to Rust|Vulnerable a Rust|modificada|
|[SjtL4vaiiGNkeXDm.htm](pathfinder-bestiary-items/SjtL4vaiiGNkeXDm.htm)|Grab|Agarrado|modificada|
|[SksyTvUlilzyBVLq.htm](pathfinder-bestiary-items/SksyTvUlilzyBVLq.htm)|Tail|Tail|modificada|
|[slCMgd2stlwxBKdg.htm](pathfinder-bestiary-items/slCMgd2stlwxBKdg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SlsCmJyWx3dgIo2X.htm](pathfinder-bestiary-items/SlsCmJyWx3dgIo2X.htm)|Knockdown|Derribo|modificada|
|[sm8H18uHC5Td5p0Q.htm](pathfinder-bestiary-items/sm8H18uHC5Td5p0Q.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[smEk0yh1Pe95Nvsc.htm](pathfinder-bestiary-items/smEk0yh1Pe95Nvsc.htm)|Natural Invisibility|Invisibilidad Natural|modificada|
|[smLfVxCrxxL5qnzw.htm](pathfinder-bestiary-items/smLfVxCrxxL5qnzw.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[SmRyQEGPbRIR82tT.htm](pathfinder-bestiary-items/SmRyQEGPbRIR82tT.htm)|Tsunami Jet|Chorro tsunami|modificada|
|[SNb64KWDBMrTorTI.htm](pathfinder-bestiary-items/SNb64KWDBMrTorTI.htm)|Jaws|Fauces|modificada|
|[SnCGhLws82aS8xpS.htm](pathfinder-bestiary-items/SnCGhLws82aS8xpS.htm)|Jaws|Fauces|modificada|
|[SNJ0RSCREyP9YYKY.htm](pathfinder-bestiary-items/SNJ0RSCREyP9YYKY.htm)|Infernal Wound|Herida infernal|modificada|
|[snOrUVs5f8v8gtuP.htm](pathfinder-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[sNxY0Neqlt313iVL.htm](pathfinder-bestiary-items/sNxY0Neqlt313iVL.htm)|Change Shape|Change Shape|modificada|
|[so5dV8CQ2KVtaKsR.htm](pathfinder-bestiary-items/so5dV8CQ2KVtaKsR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[soOhuJxyNNP6AHYc.htm](pathfinder-bestiary-items/soOhuJxyNNP6AHYc.htm)|Flame Maw|Fauces de Flamígera|modificada|
|[SoUCD4VSzt7y6NH9.htm](pathfinder-bestiary-items/SoUCD4VSzt7y6NH9.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[spBsMqVOA8rbhE2e.htm](pathfinder-bestiary-items/spBsMqVOA8rbhE2e.htm)|Aura of Corruption|Aura de corrupción|modificada|
|[sPdAzqQnoaixFizq.htm](pathfinder-bestiary-items/sPdAzqQnoaixFizq.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[SPTwJf1wzeADk4sn.htm](pathfinder-bestiary-items/SPTwJf1wzeADk4sn.htm)|Stinger|Aguijón|modificada|
|[sqceAJWlpdKGnHvx.htm](pathfinder-bestiary-items/sqceAJWlpdKGnHvx.htm)|Talon|Talon|modificada|
|[sqpUsCajoWHMhg1o.htm](pathfinder-bestiary-items/sqpUsCajoWHMhg1o.htm)|Rush|Embestida|modificada|
|[sqZIq7wiLeITXB6q.htm](pathfinder-bestiary-items/sqZIq7wiLeITXB6q.htm)|Azure Worm Venom|Veneno de gusano azul|modificada|
|[sr3LyPHlpXyJIW1Z.htm](pathfinder-bestiary-items/sr3LyPHlpXyJIW1Z.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[sRHkNxD3UkLsrrJp.htm](pathfinder-bestiary-items/sRHkNxD3UkLsrrJp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SRv8Gjga6DHY4U8G.htm](pathfinder-bestiary-items/SRv8Gjga6DHY4U8G.htm)|Corrupt Water|Corromper agua|modificada|
|[sS1tOJIttfEYCFaT.htm](pathfinder-bestiary-items/sS1tOJIttfEYCFaT.htm)|Disperse|Dispersarse|modificada|
|[sSd80IXHhedyt6q5.htm](pathfinder-bestiary-items/sSd80IXHhedyt6q5.htm)|Dorsal Deflection|Dorsal Deflection|modificada|
|[SsJk8HtdVTC3HZUK.htm](pathfinder-bestiary-items/SsJk8HtdVTC3HZUK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sslSR9PEr8cWdVop.htm](pathfinder-bestiary-items/sslSR9PEr8cWdVop.htm)|Steal Shadow|Robar sombra|modificada|
|[ST2LDOW64VO0qAVl.htm](pathfinder-bestiary-items/ST2LDOW64VO0qAVl.htm)|Pounce|Abalanzarse|modificada|
|[sT4lUsg0seVtpUKQ.htm](pathfinder-bestiary-items/sT4lUsg0seVtpUKQ.htm)|Claw|Garra|modificada|
|[sTnyQQeOWiyHDhNt.htm](pathfinder-bestiary-items/sTnyQQeOWiyHDhNt.htm)|Guardian Naga Venom|Guardian Naga Venom|modificada|
|[svdaEp5rN3vr0zjk.htm](pathfinder-bestiary-items/svdaEp5rN3vr0zjk.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[svh3YWWgNYDh2wu5.htm](pathfinder-bestiary-items/svh3YWWgNYDh2wu5.htm)|Stone Longsword|Espada larga de piedra|modificada|
|[sW7nJD5OXK3Qshxd.htm](pathfinder-bestiary-items/sW7nJD5OXK3Qshxd.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[SWBrXQjy0PikJ1wd.htm](pathfinder-bestiary-items/SWBrXQjy0PikJ1wd.htm)|Wing|Ala|modificada|
|[swIJOeM7FXkgQV1H.htm](pathfinder-bestiary-items/swIJOeM7FXkgQV1H.htm)|Jaws|Fauces|modificada|
|[SWo4EQT4exBUG1zV.htm](pathfinder-bestiary-items/SWo4EQT4exBUG1zV.htm)|Status Sight|Ver signos vitales|modificada|
|[sX53J0I5Baihn9qO.htm](pathfinder-bestiary-items/sX53J0I5Baihn9qO.htm)|Claw|Garra|modificada|
|[sx9bfcjoYVEdb5Ra.htm](pathfinder-bestiary-items/sx9bfcjoYVEdb5Ra.htm)|Tail|Tail|modificada|
|[sylWtqbDKbIIyCtj.htm](pathfinder-bestiary-items/sylWtqbDKbIIyCtj.htm)|Fangs|Colmillos|modificada|
|[SyWmilYaLnrtDFVe.htm](pathfinder-bestiary-items/SyWmilYaLnrtDFVe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sz9GGQOjyrluDIX3.htm](pathfinder-bestiary-items/sz9GGQOjyrluDIX3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SZafYEjKmu9zC1XL.htm](pathfinder-bestiary-items/SZafYEjKmu9zC1XL.htm)|Spell Ambush|Emboscada sortílega.|modificada|
|[SzaJgzszNSa3qiDA.htm](pathfinder-bestiary-items/SzaJgzszNSa3qiDA.htm)|Site Bound|Ligado a una ubicación|modificada|
|[SzfxfFohnnwRyEeK.htm](pathfinder-bestiary-items/SzfxfFohnnwRyEeK.htm)|Glutton's Feast|Festín del Glotón|modificada|
|[SzReeoNa9jYsialH.htm](pathfinder-bestiary-items/SzReeoNa9jYsialH.htm)|Inexorable|Inexorable|modificada|
|[T0L16oJLdZYthupK.htm](pathfinder-bestiary-items/T0L16oJLdZYthupK.htm)|Horns|Cuernos|modificada|
|[t0slPYAl6mytNXG7.htm](pathfinder-bestiary-items/t0slPYAl6mytNXG7.htm)|Vortex|Vortex|modificada|
|[T108hwN1OhACYQdQ.htm](pathfinder-bestiary-items/T108hwN1OhACYQdQ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[t1G2FbWy88tYUk7w.htm](pathfinder-bestiary-items/t1G2FbWy88tYUk7w.htm)|Crimson Worm Venom|Veneno de gusano carmesí.|modificada|
|[t1tq9YiGmfqQQV5C.htm](pathfinder-bestiary-items/t1tq9YiGmfqQQV5C.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[T1WSPSyMbIh6gzFQ.htm](pathfinder-bestiary-items/T1WSPSyMbIh6gzFQ.htm)|Catch Rock|Atrapar roca|modificada|
|[T2hVNG1lQgAAAy70.htm](pathfinder-bestiary-items/T2hVNG1lQgAAAy70.htm)|Swallow Whole|Engullir Todo|modificada|
|[t2Yd0tHatTjeSUcB.htm](pathfinder-bestiary-items/t2Yd0tHatTjeSUcB.htm)|Dagger|Daga|modificada|
|[t3USaq2CKWLoaP9A.htm](pathfinder-bestiary-items/t3USaq2CKWLoaP9A.htm)|Regeneration 50 (Deactivated by Good)|Regeneración 50 (Desactivado por Bueno)|modificada|
|[t4Gnn9gqqcKWerhM.htm](pathfinder-bestiary-items/t4Gnn9gqqcKWerhM.htm)|Thorny Mass|Thorny Mass|modificada|
|[T4pc6k7hdxLa8syv.htm](pathfinder-bestiary-items/T4pc6k7hdxLa8syv.htm)|Jaws|Fauces|modificada|
|[T50Unm8nAUbNCLxu.htm](pathfinder-bestiary-items/T50Unm8nAUbNCLxu.htm)|Fist|Puño|modificada|
|[T56PSO8KBNKDvOCc.htm](pathfinder-bestiary-items/T56PSO8KBNKDvOCc.htm)|Light Ray|Rayo de luz|modificada|
|[T5f6H4ZAcl4EmG7i.htm](pathfinder-bestiary-items/T5f6H4ZAcl4EmG7i.htm)|Longsword|Longsword|modificada|
|[t5Pe2D0CJm0NY3E4.htm](pathfinder-bestiary-items/t5Pe2D0CJm0NY3E4.htm)|Claw|Garra|modificada|
|[T6CjPHrd4XMeWpiE.htm](pathfinder-bestiary-items/T6CjPHrd4XMeWpiE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[t6KHJCiZXdNJnqxB.htm](pathfinder-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|Maestro de armas|modificada|
|[T8jQXV4jxIhvfS14.htm](pathfinder-bestiary-items/T8jQXV4jxIhvfS14.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[t8uaryivfTb4jrjH.htm](pathfinder-bestiary-items/t8uaryivfTb4jrjH.htm)|Leaf|Hoja|modificada|
|[t98B5FOZI1a1Ye46.htm](pathfinder-bestiary-items/t98B5FOZI1a1Ye46.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[TaJezoHLHAlyZmLU.htm](pathfinder-bestiary-items/TaJezoHLHAlyZmLU.htm)|Binding Contract|Contrato vinculante|modificada|
|[TAr3uDoat4fR02qp.htm](pathfinder-bestiary-items/TAr3uDoat4fR02qp.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[tBCaXMwzwsRxvg1U.htm](pathfinder-bestiary-items/tBCaXMwzwsRxvg1U.htm)|Dagger|Daga|modificada|
|[TbFZxJr8aSgbgTZ6.htm](pathfinder-bestiary-items/TbFZxJr8aSgbgTZ6.htm)|Double Bite|Doble Muerdemuerde|modificada|
|[TBlBPLZKvFBHjjgU.htm](pathfinder-bestiary-items/TBlBPLZKvFBHjjgU.htm)|Slow|Lentificado/a|modificada|
|[TBlcp9L0tEkA3jyF.htm](pathfinder-bestiary-items/TBlcp9L0tEkA3jyF.htm)|Double Slice|Doble tajo|modificada|
|[Tc806odnsrBWGupY.htm](pathfinder-bestiary-items/Tc806odnsrBWGupY.htm)|Claw|Garra|modificada|
|[tcjS7ZEu6Fppiy3z.htm](pathfinder-bestiary-items/tcjS7ZEu6Fppiy3z.htm)|Grab|Agarrado|modificada|
|[TCZQdv29obsz4Rie.htm](pathfinder-bestiary-items/TCZQdv29obsz4Rie.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[td7P2EcI7SbN65De.htm](pathfinder-bestiary-items/td7P2EcI7SbN65De.htm)|Tail|Tail|modificada|
|[Tdu73gvdkQpD4ZFK.htm](pathfinder-bestiary-items/Tdu73gvdkQpD4ZFK.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[tDvcon2bK9xEGh90.htm](pathfinder-bestiary-items/tDvcon2bK9xEGh90.htm)|Deadly Throw|Lanzamiento letal|modificada|
|[tdX5F9iFFF0sboNA.htm](pathfinder-bestiary-items/tdX5F9iFFF0sboNA.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[TEn7gmE6LHEqbMLJ.htm](pathfinder-bestiary-items/TEn7gmE6LHEqbMLJ.htm)|Daeodon Charge|Daeodon Charge|modificada|
|[tEPc3Xc3Pufff6D5.htm](pathfinder-bestiary-items/tEPc3Xc3Pufff6D5.htm)|Drench|Sofocar|modificada|
|[Tf3oOPGPRrko1wvW.htm](pathfinder-bestiary-items/Tf3oOPGPRrko1wvW.htm)|Entangling Slime|Enredar Limo|modificada|
|[TfNCbBu8oaMSaeTO.htm](pathfinder-bestiary-items/TfNCbBu8oaMSaeTO.htm)|Claw|Garra|modificada|
|[tFRnP7KsOLmYb44P.htm](pathfinder-bestiary-items/tFRnP7KsOLmYb44P.htm)|Archon's Door|Puerta del arconte|modificada|
|[TfSlL9Vupc8WC8jy.htm](pathfinder-bestiary-items/TfSlL9Vupc8WC8jy.htm)|Buck|Encabritarse|modificada|
|[TfUyAj18NAaa1ZGV.htm](pathfinder-bestiary-items/TfUyAj18NAaa1ZGV.htm)|Ice Linnorm Venom|Veneno de linnorm del hielo|modificada|
|[tFYFPReEO8BIA94l.htm](pathfinder-bestiary-items/tFYFPReEO8BIA94l.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Tg3cDQ85HTqlUqQV.htm](pathfinder-bestiary-items/Tg3cDQ85HTqlUqQV.htm)|Claw|Garra|modificada|
|[tGAFUCC4Emx6CfMD.htm](pathfinder-bestiary-items/tGAFUCC4Emx6CfMD.htm)|Entropy Sense (Imprecise) 30 feet|Sentido de Entropía (Impreciso) 30 pies|modificada|
|[TGIpZcekBlTtMIW2.htm](pathfinder-bestiary-items/TGIpZcekBlTtMIW2.htm)|Massive Rush|Massive Embestida|modificada|
|[TGYOp1PUpIUNK6f9.htm](pathfinder-bestiary-items/TGYOp1PUpIUNK6f9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tHBnPDgoj9trVlBk.htm](pathfinder-bestiary-items/tHBnPDgoj9trVlBk.htm)|Descend on a Web|Descender por una telara|modificada|
|[THputvyP8JOKYysA.htm](pathfinder-bestiary-items/THputvyP8JOKYysA.htm)|Dagger|Daga|modificada|
|[thy0OpcI6dFq3W3j.htm](pathfinder-bestiary-items/thy0OpcI6dFq3W3j.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[tiCwm2g4h6CfLncH.htm](pathfinder-bestiary-items/tiCwm2g4h6CfLncH.htm)|Jaws|Fauces|modificada|
|[TIGvNHFixJK8sJCg.htm](pathfinder-bestiary-items/TIGvNHFixJK8sJCg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[tiL9cpK0I2gzpQne.htm](pathfinder-bestiary-items/tiL9cpK0I2gzpQne.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Tj3MRsA6XVrZ9zQg.htm](pathfinder-bestiary-items/Tj3MRsA6XVrZ9zQg.htm)|Jaws|Fauces|modificada|
|[tJF3MvlHvCalGkXk.htm](pathfinder-bestiary-items/tJF3MvlHvCalGkXk.htm)|Horns|Cuernos|modificada|
|[Tji1ev92INkjtI3U.htm](pathfinder-bestiary-items/Tji1ev92INkjtI3U.htm)|Smoke Vision|Visión de Humo|modificada|
|[TjOmyfilNvHKI58M.htm](pathfinder-bestiary-items/TjOmyfilNvHKI58M.htm)|Children of the Night|Niños de la Noche|modificada|
|[tjX3QfENUkKVi1tn.htm](pathfinder-bestiary-items/tjX3QfENUkKVi1tn.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Tk7abWUpvDDV6Qg0.htm](pathfinder-bestiary-items/Tk7abWUpvDDV6Qg0.htm)|Succor Vulnerability|Vulnerabilidad al auxilio|modificada|
|[TKaqHg2KqjNmg24t.htm](pathfinder-bestiary-items/TKaqHg2KqjNmg24t.htm)|Improved Grab|Agarrado mejorado|modificada|
|[TKC7njhX3a3mq4eY.htm](pathfinder-bestiary-items/TKC7njhX3a3mq4eY.htm)|Negative Healing|Curación negativa|modificada|
|[TkDV0wPhRzYy8sii.htm](pathfinder-bestiary-items/TkDV0wPhRzYy8sii.htm)|Tremorsense (Imprecise) 120 feet|Sentido del Temblor (Impreciso) 120 pies|modificada|
|[tKTCH6a5g78miSHu.htm](pathfinder-bestiary-items/tKTCH6a5g78miSHu.htm)|Commander's Aura|Aura de comandante|modificada|
|[TLRgHlg3hxjekxoB.htm](pathfinder-bestiary-items/TLRgHlg3hxjekxoB.htm)|Claw|Garra|modificada|
|[tMGdCYYP6CA506om.htm](pathfinder-bestiary-items/tMGdCYYP6CA506om.htm)|Morphic Hands|Manos mórficas|modificada|
|[tMZ3S5BdHrl0iX7R.htm](pathfinder-bestiary-items/tMZ3S5BdHrl0iX7R.htm)|Fist|Puño|modificada|
|[TN0QOVJVRLV73CWR.htm](pathfinder-bestiary-items/TN0QOVJVRLV73CWR.htm)|Fist|Puño|modificada|
|[TnaXi3v3iBDjotwd.htm](pathfinder-bestiary-items/TnaXi3v3iBDjotwd.htm)|Viper Venom|Viper Venom|modificada|
|[tNVPutTbdl3sPaGf.htm](pathfinder-bestiary-items/tNVPutTbdl3sPaGf.htm)|Death Flare|Brillo fúnebre de la muerte|modificada|
|[tOCS9mkYR5K1LuOZ.htm](pathfinder-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[TOGa0vvktQjKmJ7K.htm](pathfinder-bestiary-items/TOGa0vvktQjKmJ7K.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TpEz65tv1dwJ91cy.htm](pathfinder-bestiary-items/TpEz65tv1dwJ91cy.htm)|Alchemical Chambers|Compartimentos alquímicos|modificada|
|[tphsLfLbrYV16pth.htm](pathfinder-bestiary-items/tphsLfLbrYV16pth.htm)|Inexorable|Inexorable|modificada|
|[TpHVJUZhgcIfmmBv.htm](pathfinder-bestiary-items/TpHVJUZhgcIfmmBv.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[TQ0FOP3X7LrgaS6J.htm](pathfinder-bestiary-items/TQ0FOP3X7LrgaS6J.htm)|Ferocity|Ferocidad|modificada|
|[tQ1m9uzdwC96pkCY.htm](pathfinder-bestiary-items/tQ1m9uzdwC96pkCY.htm)|Hydraulic Asphyxiation|Asfixia hidráulica|modificada|
|[tq3HesFWx5P7xPWl.htm](pathfinder-bestiary-items/tq3HesFWx5P7xPWl.htm)|Vulnerable to Shatter|Vulnerable a estallar|modificada|
|[TQcjKBbkOHwvxvhN.htm](pathfinder-bestiary-items/TQcjKBbkOHwvxvhN.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[tqL5vKXpeLAzq2JR.htm](pathfinder-bestiary-items/tqL5vKXpeLAzq2JR.htm)|Frightful Presence|Frightful Presence|modificada|
|[TRcRCZ4v7cyZtfSF.htm](pathfinder-bestiary-items/TRcRCZ4v7cyZtfSF.htm)|Sandstorm Wrath|Ira de la tormenta de arena|modificada|
|[tRvUNo3EA7WVnZc5.htm](pathfinder-bestiary-items/tRvUNo3EA7WVnZc5.htm)|Frightful Presence|Frightful Presence|modificada|
|[TS7iVUZNmP0Szdfh.htm](pathfinder-bestiary-items/TS7iVUZNmP0Szdfh.htm)|Javelin|Javelin|modificada|
|[TsCewvMqGGAfetXh.htm](pathfinder-bestiary-items/TsCewvMqGGAfetXh.htm)|Trident|Trident|modificada|
|[tsqJ5ZQfUx0W3mvO.htm](pathfinder-bestiary-items/tsqJ5ZQfUx0W3mvO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TtcFIJZjrZ4Vaaml.htm](pathfinder-bestiary-items/TtcFIJZjrZ4Vaaml.htm)|Slow|Lentificado/a|modificada|
|[ttFxkO2qOIlHaSJc.htm](pathfinder-bestiary-items/ttFxkO2qOIlHaSJc.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[TTlpPh49DdSgaBxG.htm](pathfinder-bestiary-items/TTlpPh49DdSgaBxG.htm)|Horns|Cuernos|modificada|
|[TTpnkaK9B6Lsytya.htm](pathfinder-bestiary-items/TTpnkaK9B6Lsytya.htm)|Rat Empathy|Rata Empatía|modificada|
|[TuTut6BPwHgd3Xp7.htm](pathfinder-bestiary-items/TuTut6BPwHgd3Xp7.htm)|Emotional Focus|Enfoque Emocional|modificada|
|[tV18raweChPRFzPo.htm](pathfinder-bestiary-items/tV18raweChPRFzPo.htm)|Claw|Garra|modificada|
|[tvFxmbJKPxnCnTbb.htm](pathfinder-bestiary-items/tvFxmbJKPxnCnTbb.htm)|Vermin Empathy|Empatía con las sabandijas|modificada|
|[TVLAYoQrAgNovOAr.htm](pathfinder-bestiary-items/TVLAYoQrAgNovOAr.htm)|Debris|Escombros|modificada|
|[tvnn3d6VCprLkoQ2.htm](pathfinder-bestiary-items/tvnn3d6VCprLkoQ2.htm)|Fist|Puño|modificada|
|[TvxcP8kfqmDOK1GX.htm](pathfinder-bestiary-items/TvxcP8kfqmDOK1GX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TWiy088AD8uOCDGQ.htm](pathfinder-bestiary-items/TWiy088AD8uOCDGQ.htm)|Speed Surge|Arranque de velocidad|modificada|
|[TwSJCzLZ4KcJKaWT.htm](pathfinder-bestiary-items/TwSJCzLZ4KcJKaWT.htm)|Breath Weapon|Breath Weapon|modificada|
|[TXy3zvlvlJECNrWn.htm](pathfinder-bestiary-items/TXy3zvlvlJECNrWn.htm)|Shell Block|Bloqueo con caparazón|modificada|
|[Tyan53wuqYSDd7rE.htm](pathfinder-bestiary-items/Tyan53wuqYSDd7rE.htm)|Tentacle|Tentáculo|modificada|
|[TYcAOZDcSQlHGVZx.htm](pathfinder-bestiary-items/TYcAOZDcSQlHGVZx.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[TyHmjC5wvxo29J5L.htm](pathfinder-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|Agarrado|modificada|
|[tyjUqs7S8mA70wiw.htm](pathfinder-bestiary-items/tyjUqs7S8mA70wiw.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[TyonmqwWHejYuBAN.htm](pathfinder-bestiary-items/TyonmqwWHejYuBAN.htm)|Dervish Strike|Golpe de derviche|modificada|
|[tZ39PkV2oWLySkOs.htm](pathfinder-bestiary-items/tZ39PkV2oWLySkOs.htm)|Swarming Stings|Picadura de enjambre|modificada|
|[tz3Jv4IQgO8ygZjo.htm](pathfinder-bestiary-items/tz3Jv4IQgO8ygZjo.htm)|Spray Acid|Rociada ácida|modificada|
|[TzBbRiqiSDZj6VdK.htm](pathfinder-bestiary-items/TzBbRiqiSDZj6VdK.htm)|Death Strike|Golpe fulminante|modificada|
|[tzGgSX4dpcTwYYag.htm](pathfinder-bestiary-items/tzGgSX4dpcTwYYag.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[TziDQnLmJx7Ty2df.htm](pathfinder-bestiary-items/TziDQnLmJx7Ty2df.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[tzsc7ldZ2eR2z0XM.htm](pathfinder-bestiary-items/tzsc7ldZ2eR2z0XM.htm)|Regeneration 30 (Deactivated by Good)|Regeneración 30 (Desactivado por Bueno)|modificada|
|[u0GbWrE4hwJyEA08.htm](pathfinder-bestiary-items/u0GbWrE4hwJyEA08.htm)|Hunk Of Meat|Hunk Of Meat|modificada|
|[u0WWqexz5TB2gEqn.htm](pathfinder-bestiary-items/u0WWqexz5TB2gEqn.htm)|Claws|Garras|modificada|
|[u1eOXRbLwSvouuEB.htm](pathfinder-bestiary-items/u1eOXRbLwSvouuEB.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[U1ORlKZKu61bng75.htm](pathfinder-bestiary-items/U1ORlKZKu61bng75.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[U1y2WggaYWJc6YHr.htm](pathfinder-bestiary-items/U1y2WggaYWJc6YHr.htm)|Horns|Cuernos|modificada|
|[u3YnULJ9YqMBkD6D.htm](pathfinder-bestiary-items/u3YnULJ9YqMBkD6D.htm)|Wave|Wave|modificada|
|[u59sSnB7GuFn9ofv.htm](pathfinder-bestiary-items/u59sSnB7GuFn9ofv.htm)|Shark Commune 150 feet|Tiburón comunión 150 pies|modificada|
|[u5YlFZyly3oBsknm.htm](pathfinder-bestiary-items/u5YlFZyly3oBsknm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[u5ZGQOwAVraOXVuo.htm](pathfinder-bestiary-items/u5ZGQOwAVraOXVuo.htm)|Compression|Compresión|modificada|
|[u71Wtb03wVLdrSUO.htm](pathfinder-bestiary-items/u71Wtb03wVLdrSUO.htm)|Truespeech|Truespeech|modificada|
|[U7gmP0tQ6eQor3xT.htm](pathfinder-bestiary-items/U7gmP0tQ6eQor3xT.htm)|Fist|Puño|modificada|
|[u9SS7QpWokulA8Af.htm](pathfinder-bestiary-items/u9SS7QpWokulA8Af.htm)|Jaws|Fauces|modificada|
|[u9tY2pkbZxNzmxoe.htm](pathfinder-bestiary-items/u9tY2pkbZxNzmxoe.htm)|Claw|Garra|modificada|
|[UakPnn8jYjY9gCdo.htm](pathfinder-bestiary-items/UakPnn8jYjY9gCdo.htm)|Rope|Cuerda|modificada|
|[UB2ChrMM1B3cwlo2.htm](pathfinder-bestiary-items/UB2ChrMM1B3cwlo2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uB2gaJscmmSbNNVZ.htm](pathfinder-bestiary-items/uB2gaJscmmSbNNVZ.htm)|Shortsword|Espada corta|modificada|
|[ubptAi7h2I8spy2V.htm](pathfinder-bestiary-items/ubptAi7h2I8spy2V.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uC55H6tJNTniSzuC.htm](pathfinder-bestiary-items/uC55H6tJNTniSzuC.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[UC8gJQCeqcEKOOfI.htm](pathfinder-bestiary-items/UC8gJQCeqcEKOOfI.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[uCBBqgflwHqvZeg5.htm](pathfinder-bestiary-items/uCBBqgflwHqvZeg5.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[UCRM8G3tekG92tUj.htm](pathfinder-bestiary-items/UCRM8G3tekG92tUj.htm)|Hoof|Hoof|modificada|
|[uCvZSYozUXxyyf1b.htm](pathfinder-bestiary-items/uCvZSYozUXxyyf1b.htm)|Death's Grace|Gracia de la muerte|modificada|
|[uD9MleR0PzjUXugC.htm](pathfinder-bestiary-items/uD9MleR0PzjUXugC.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[uDEIHmI6TZbQLzHX.htm](pathfinder-bestiary-items/uDEIHmI6TZbQLzHX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[UDSpeDfwP5gNIdMY.htm](pathfinder-bestiary-items/UDSpeDfwP5gNIdMY.htm)|Dagger|Daga|modificada|
|[uEjliLhQWBsVYCsE.htm](pathfinder-bestiary-items/uEjliLhQWBsVYCsE.htm)|Maul|Zarpazo doble|modificada|
|[UemgQLIDasOGrvNC.htm](pathfinder-bestiary-items/UemgQLIDasOGrvNC.htm)|Archon's Door|Puerta del arconte|modificada|
|[ufap97U4FyUOrJZF.htm](pathfinder-bestiary-items/ufap97U4FyUOrJZF.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[UfLxn8BgeR2BOqee.htm](pathfinder-bestiary-items/UfLxn8BgeR2BOqee.htm)|Fist|Puño|modificada|
|[UfQd4obnmoY6nVDf.htm](pathfinder-bestiary-items/UfQd4obnmoY6nVDf.htm)|Fist|Puño|modificada|
|[uFWe6jAjZ0J95ox9.htm](pathfinder-bestiary-items/uFWe6jAjZ0J95ox9.htm)|Battle Axe|Hacha de batalla|modificada|
|[uGne7svJszeghy3d.htm](pathfinder-bestiary-items/uGne7svJszeghy3d.htm)|Plaguesense 60 feet|Sentir pestilencias 60 pies|modificada|
|[uGSBKzZHNc69P1ub.htm](pathfinder-bestiary-items/uGSBKzZHNc69P1ub.htm)|Telepathy 1 mile|Telepatía 1 milla|modificada|
|[ugytuxGqE0wsYOff.htm](pathfinder-bestiary-items/ugytuxGqE0wsYOff.htm)|Claw|Garra|modificada|
|[UgyxwE6dj48w1zhM.htm](pathfinder-bestiary-items/UgyxwE6dj48w1zhM.htm)|Retributive Strike|Golpe retributivo|modificada|
|[uh1IYK2wwc48sFKE.htm](pathfinder-bestiary-items/uh1IYK2wwc48sFKE.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Uhi5bxlJXwqkbpIT.htm](pathfinder-bestiary-items/Uhi5bxlJXwqkbpIT.htm)|Fist|Puño|modificada|
|[UhtLIPQpF9818xr2.htm](pathfinder-bestiary-items/UhtLIPQpF9818xr2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UHxzFiyQ9sL32HBv.htm](pathfinder-bestiary-items/UHxzFiyQ9sL32HBv.htm)|Frightful Presence|Frightful Presence|modificada|
|[ui6XZl3S0T45fJFu.htm](pathfinder-bestiary-items/ui6XZl3S0T45fJFu.htm)|Claw|Garra|modificada|
|[uIMjt4HR6GpJ6PS1.htm](pathfinder-bestiary-items/uIMjt4HR6GpJ6PS1.htm)|Fangs|Colmillos|modificada|
|[UipChONJlSGScaQZ.htm](pathfinder-bestiary-items/UipChONJlSGScaQZ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[uj64Am9tI0929tfx.htm](pathfinder-bestiary-items/uj64Am9tI0929tfx.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[UJlQ2v4Mw6QsmvJY.htm](pathfinder-bestiary-items/UJlQ2v4Mw6QsmvJY.htm)|Holy Warhammer|Sagrada Warhammer|modificada|
|[UJUSN0sChxOuRWw8.htm](pathfinder-bestiary-items/UJUSN0sChxOuRWw8.htm)|Infuse Weapon|Infundir armas|modificada|
|[UK0INZ5G8eq5ytop.htm](pathfinder-bestiary-items/UK0INZ5G8eq5ytop.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uK5eIAdOWiayEgWr.htm](pathfinder-bestiary-items/uK5eIAdOWiayEgWr.htm)|Light Ray|Rayo de luz|modificada|
|[UKfFDeN7IDKW1okj.htm](pathfinder-bestiary-items/UKfFDeN7IDKW1okj.htm)|Dancer's Curse|Dancer's Curse|modificada|
|[UKNPyXkarfzkXTHO.htm](pathfinder-bestiary-items/UKNPyXkarfzkXTHO.htm)|Longspear|Longspear|modificada|
|[UkuRoGDf37QBLm8k.htm](pathfinder-bestiary-items/UkuRoGDf37QBLm8k.htm)|Ice Stride|Zancada de hielo|modificada|
|[uldVZLVdwiLD1q6v.htm](pathfinder-bestiary-items/uldVZLVdwiLD1q6v.htm)|Vulnerable to Sunlight|Vulnerabilidad a la luz del sol|modificada|
|[uLEPJWbbXAY2E4KJ.htm](pathfinder-bestiary-items/uLEPJWbbXAY2E4KJ.htm)|Push 5 feet|Empujar 5 pies|modificada|
|[uLf1dk5gMycZYjBy.htm](pathfinder-bestiary-items/uLf1dk5gMycZYjBy.htm)|+2 to All Saves vs. Grapple or Shove|+2 a todas las salvaciones contra Presa o Empujón.|modificada|
|[uLu0KGwcUUj5bvhl.htm](pathfinder-bestiary-items/uLu0KGwcUUj5bvhl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UluC5rRvQl0yriAj.htm](pathfinder-bestiary-items/UluC5rRvQl0yriAj.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[UmeNuzExQLQPFzTE.htm](pathfinder-bestiary-items/UmeNuzExQLQPFzTE.htm)|Guarded Thoughts|Pensamientos Guardados|modificada|
|[Umi21NTMrg5JA3IQ.htm](pathfinder-bestiary-items/Umi21NTMrg5JA3IQ.htm)|Wight Spawn|Wight Spawn|modificada|
|[umShxoDTXECH3WCs.htm](pathfinder-bestiary-items/umShxoDTXECH3WCs.htm)|Snow Vision|Snow Vision|modificada|
|[UMX8GkJ4L6G9UQGO.htm](pathfinder-bestiary-items/UMX8GkJ4L6G9UQGO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UnFKmdjf3kYOmNvW.htm](pathfinder-bestiary-items/UnFKmdjf3kYOmNvW.htm)|Natural Cunning|Astucia Natural|modificada|
|[uoJMsbcXlm58cnei.htm](pathfinder-bestiary-items/uoJMsbcXlm58cnei.htm)|Longsword|Longsword|modificada|
|[UoKQ5xYxwkzldwhR.htm](pathfinder-bestiary-items/UoKQ5xYxwkzldwhR.htm)|Constant Spells|Constant Spells|modificada|
|[uOtLx35G2OFWHztX.htm](pathfinder-bestiary-items/uOtLx35G2OFWHztX.htm)|Tentacle Transfer|Transferir a tentáculos|modificada|
|[UP6X4AexjR3Xwwq4.htm](pathfinder-bestiary-items/UP6X4AexjR3Xwwq4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[up8Xcig2FrwgbFfb.htm](pathfinder-bestiary-items/up8Xcig2FrwgbFfb.htm)|Terrifying Croak|Aterrador Croak|modificada|
|[uPAAssuXj8CktZTI.htm](pathfinder-bestiary-items/uPAAssuXj8CktZTI.htm)|Push 40 feet|Empuja 40 pies|modificada|
|[upJaEvHTKGpsW57H.htm](pathfinder-bestiary-items/upJaEvHTKGpsW57H.htm)|Flaming Longsword|Espada Larga Flamígera|modificada|
|[uPjMU5WZPboQnmTu.htm](pathfinder-bestiary-items/uPjMU5WZPboQnmTu.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[uPrkt0x7Qt0nbh7U.htm](pathfinder-bestiary-items/uPrkt0x7Qt0nbh7U.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[UpXcSbQhJBN069fE.htm](pathfinder-bestiary-items/UpXcSbQhJBN069fE.htm)|Horn|Cuerno|modificada|
|[UQ7yTknJYCFghTF4.htm](pathfinder-bestiary-items/UQ7yTknJYCFghTF4.htm)|+2 to Will Saves vs. Emotion|+2 a Salvaciones de Voluntad vs. Emoción|modificada|
|[uqfegosZFIHA1J7G.htm](pathfinder-bestiary-items/uqfegosZFIHA1J7G.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[uQywdK0j1lggScR1.htm](pathfinder-bestiary-items/uQywdK0j1lggScR1.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[uR0sxFBmOFXxYqZi.htm](pathfinder-bestiary-items/uR0sxFBmOFXxYqZi.htm)|Claw|Garra|modificada|
|[urDSEWopXr5z3krl.htm](pathfinder-bestiary-items/urDSEWopXr5z3krl.htm)|Attack of Opportunity (Stinger Only)|Ataque de oportunidad (sólo aguijón).|modificada|
|[uRZywk7870Tfgxnp.htm](pathfinder-bestiary-items/uRZywk7870Tfgxnp.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[UsbbZtRGFg0fzukO.htm](pathfinder-bestiary-items/UsbbZtRGFg0fzukO.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[USCoVo5LJNs12zB4.htm](pathfinder-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|Oscurecerse|modificada|
|[UsE3E2jD964UvWQw.htm](pathfinder-bestiary-items/UsE3E2jD964UvWQw.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[uss6H3otq5ggwFWb.htm](pathfinder-bestiary-items/uss6H3otq5ggwFWb.htm)|Tremorsense (Imprecise) 80 feet|Sentido del Temblor (Impreciso) 80 pies|modificada|
|[uT4KswZKvsTixIVs.htm](pathfinder-bestiary-items/uT4KswZKvsTixIVs.htm)|Improved Grab|Agarrado mejorado|modificada|
|[uT8zq93Vx5vOdzkD.htm](pathfinder-bestiary-items/uT8zq93Vx5vOdzkD.htm)|Breath Weapon|Breath Weapon|modificada|
|[uTobNtEZnb13t0PA.htm](pathfinder-bestiary-items/uTobNtEZnb13t0PA.htm)|Choose Weakness|Elegir debilidad|modificada|
|[utu2oawY1G8K7q6g.htm](pathfinder-bestiary-items/utu2oawY1G8K7q6g.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UufKTkmVCcXkrBVb.htm](pathfinder-bestiary-items/UufKTkmVCcXkrBVb.htm)|Wing Thrash|Golpeteo con alas|modificada|
|[uuI4sId0T1tDVCla.htm](pathfinder-bestiary-items/uuI4sId0T1tDVCla.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uuMoWLbelNwKPIUM.htm](pathfinder-bestiary-items/uuMoWLbelNwKPIUM.htm)|Armor-Rending|Rasgadura de armadura|modificada|
|[UUnzaMbka2DY7uDu.htm](pathfinder-bestiary-items/UUnzaMbka2DY7uDu.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[UupBqSBXKanyWoA6.htm](pathfinder-bestiary-items/UupBqSBXKanyWoA6.htm)|Dispelling Strike|Golpe disipador|modificada|
|[uv0UDH4jDwetNZol.htm](pathfinder-bestiary-items/uv0UDH4jDwetNZol.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[UVFdUW8UmQmQjvIs.htm](pathfinder-bestiary-items/UVFdUW8UmQmQjvIs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[UvKqCH4Y10AWYyTl.htm](pathfinder-bestiary-items/UvKqCH4Y10AWYyTl.htm)|Fist|Puño|modificada|
|[UVREJWrU0MjhSxLs.htm](pathfinder-bestiary-items/UVREJWrU0MjhSxLs.htm)|Protean Anatomy|Anatomía Proteica|modificada|
|[UVwLwFSrhe9YIUEC.htm](pathfinder-bestiary-items/UVwLwFSrhe9YIUEC.htm)|Tighten Coils|Tensar espirales|modificada|
|[uVyonJirqXV46pFg.htm](pathfinder-bestiary-items/uVyonJirqXV46pFg.htm)|Breath Weapon|Breath Weapon|modificada|
|[UW8qbPcbWR2N4HcI.htm](pathfinder-bestiary-items/UW8qbPcbWR2N4HcI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[UwaLPxPywao4fbN3.htm](pathfinder-bestiary-items/UwaLPxPywao4fbN3.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[uwBAscSuJG489hpI.htm](pathfinder-bestiary-items/uwBAscSuJG489hpI.htm)|Command Giants|Orden imperiosa Gigantes|modificada|
|[UWcKX7DmlsSP933a.htm](pathfinder-bestiary-items/UWcKX7DmlsSP933a.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[Ux1Z45Ai509H8jm7.htm](pathfinder-bestiary-items/Ux1Z45Ai509H8jm7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uXbNGzD4guadWLp9.htm](pathfinder-bestiary-items/uXbNGzD4guadWLp9.htm)|Longsword|Longsword|modificada|
|[uyDcXzwKNRuKIzY5.htm](pathfinder-bestiary-items/uyDcXzwKNRuKIzY5.htm)|Blackaxe - Restoration|Blackaxe - Restablecimiento|modificada|
|[UYnNMvrWFg5VBhO2.htm](pathfinder-bestiary-items/UYnNMvrWFg5VBhO2.htm)|Invisible|Invisibilidad|modificada|
|[uYQrEbjjw3XIQ1cO.htm](pathfinder-bestiary-items/uYQrEbjjw3XIQ1cO.htm)|Frightful Moan|Frightful Moan|modificada|
|[UYwBYJoDNWHyjeIa.htm](pathfinder-bestiary-items/UYwBYJoDNWHyjeIa.htm)|Spear|Lanza|modificada|
|[uyX0PXYhDtx37aB0.htm](pathfinder-bestiary-items/uyX0PXYhDtx37aB0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uZBUxDTyFWbpARud.htm](pathfinder-bestiary-items/uZBUxDTyFWbpARud.htm)|Frightful Presence|Frightful Presence|modificada|
|[uZdepeycCi1DBYTW.htm](pathfinder-bestiary-items/uZdepeycCi1DBYTW.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[Uzinvon7QqcS48NH.htm](pathfinder-bestiary-items/Uzinvon7QqcS48NH.htm)|Improved Push 20 feet|Empuje mejorado 20 pies|modificada|
|[uznmLfSkP5j9qOEA.htm](pathfinder-bestiary-items/uznmLfSkP5j9qOEA.htm)|Quick Draw|Desenvainado rápido|modificada|
|[UZp9NX0EnCBPGns5.htm](pathfinder-bestiary-items/UZp9NX0EnCBPGns5.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[uzwUjI3gD8tO8Chh.htm](pathfinder-bestiary-items/uzwUjI3gD8tO8Chh.htm)|Vulnerable to Flesh to Stone|Vulnerable a De la carne a la piedra|modificada|
|[V0DnCsH65jBlStiD.htm](pathfinder-bestiary-items/V0DnCsH65jBlStiD.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[v0n7ntTptPAp7TPz.htm](pathfinder-bestiary-items/v0n7ntTptPAp7TPz.htm)|Brazier|Brasero|modificada|
|[V1nFWeg7OxNxkntU.htm](pathfinder-bestiary-items/V1nFWeg7OxNxkntU.htm)|Dagger|Daga|modificada|
|[v27XHQrEZwrrEcBP.htm](pathfinder-bestiary-items/v27XHQrEZwrrEcBP.htm)|Constant Spells|Constant Spells|modificada|
|[v2Ifd6tb1IDRFqFO.htm](pathfinder-bestiary-items/v2Ifd6tb1IDRFqFO.htm)|Drain Life|Drenar Vida|modificada|
|[v2MAVdLprfTUs4jh.htm](pathfinder-bestiary-items/v2MAVdLprfTUs4jh.htm)|Fist|Puño|modificada|
|[v2Vcuxvy5icZyHkj.htm](pathfinder-bestiary-items/v2Vcuxvy5icZyHkj.htm)|All-Around Vision|All-Around Vision|modificada|
|[V45K2AmQDPCGIMET.htm](pathfinder-bestiary-items/V45K2AmQDPCGIMET.htm)|Blood Scent|Blood Scent|modificada|
|[V6anmpeGZ7JM0rNx.htm](pathfinder-bestiary-items/V6anmpeGZ7JM0rNx.htm)|Darkvision|Visión en la oscuridad|modificada|
|[V6eYuUdSaU2MbSi5.htm](pathfinder-bestiary-items/V6eYuUdSaU2MbSi5.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[V6Y4yCrWba6tmSL9.htm](pathfinder-bestiary-items/V6Y4yCrWba6tmSL9.htm)|Coven|Coven|modificada|
|[v77PLs3ZMXpMZOWj.htm](pathfinder-bestiary-items/v77PLs3ZMXpMZOWj.htm)|Go for the Eyes|Go for the Eyes|modificada|
|[v7vwMkZTAxAiZu1m.htm](pathfinder-bestiary-items/v7vwMkZTAxAiZu1m.htm)|Constant Spells|Constant Spells|modificada|
|[v8RLawIxRUNc3rnR.htm](pathfinder-bestiary-items/v8RLawIxRUNc3rnR.htm)|Powerful Blows|Golpes formidables|modificada|
|[v8X1W0LmbkWRpOS6.htm](pathfinder-bestiary-items/v8X1W0LmbkWRpOS6.htm)|Frighten|Asustar|modificada|
|[v8yr3bjEd1JafWFX.htm](pathfinder-bestiary-items/v8yr3bjEd1JafWFX.htm)|Breath Weapon|Breath Weapon|modificada|
|[v90WRjELLiKF57vr.htm](pathfinder-bestiary-items/v90WRjELLiKF57vr.htm)|Jaws|Fauces|modificada|
|[v9Xt73Wj9wclc3ef.htm](pathfinder-bestiary-items/v9Xt73Wj9wclc3ef.htm)|Javelin|Javelin|modificada|
|[V9zIFbIReO27zwGu.htm](pathfinder-bestiary-items/V9zIFbIReO27zwGu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VB81WcGgMngyyh3E.htm](pathfinder-bestiary-items/VB81WcGgMngyyh3E.htm)|Slink in Shadows|Escabullirse entre las sombras|modificada|
|[VbE9aK6V1gA4Fbv9.htm](pathfinder-bestiary-items/VbE9aK6V1gA4Fbv9.htm)|Foot|Pie|modificada|
|[vbhEyAI6chCq7BJL.htm](pathfinder-bestiary-items/vbhEyAI6chCq7BJL.htm)|Claw|Garra|modificada|
|[VBMoJbMPEmzIPu5X.htm](pathfinder-bestiary-items/VBMoJbMPEmzIPu5X.htm)|Chill Breath|Aliento de frío|modificada|
|[VBQJPk4ZTjKYg7E1.htm](pathfinder-bestiary-items/VBQJPk4ZTjKYg7E1.htm)|Strand|Strand|modificada|
|[vC0V00q445OAIDix.htm](pathfinder-bestiary-items/vC0V00q445OAIDix.htm)|Negative Healing|Curación negativa|modificada|
|[vcD6GpR7alODPFzn.htm](pathfinder-bestiary-items/vcD6GpR7alODPFzn.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[vCeTTDUcG0aI8C9H.htm](pathfinder-bestiary-items/vCeTTDUcG0aI8C9H.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[VcfZ8SToykQ7MnHU.htm](pathfinder-bestiary-items/VcfZ8SToykQ7MnHU.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[VcMwjaxqor5ucwyE.htm](pathfinder-bestiary-items/VcMwjaxqor5ucwyE.htm)|Javelin|Javelin|modificada|
|[VCXj9VT8SpqcHdZS.htm](pathfinder-bestiary-items/VCXj9VT8SpqcHdZS.htm)|Lifesense 120 feet|Lifesense 120 pies|modificada|
|[VdbR3tZZovSqgWq8.htm](pathfinder-bestiary-items/VdbR3tZZovSqgWq8.htm)|Axe Swipe|Vaivén con hacha|modificada|
|[VdlfnhKj8BudHVMB.htm](pathfinder-bestiary-items/VdlfnhKj8BudHVMB.htm)|Trickster's Step|Paso del embaucador|modificada|
|[Vdx4lOd9PGzzo18M.htm](pathfinder-bestiary-items/Vdx4lOd9PGzzo18M.htm)|Fling Offal|Lanzar por los aires despojos|modificada|
|[vdYowEKgmhYMzYaA.htm](pathfinder-bestiary-items/vdYowEKgmhYMzYaA.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[veNUDgghA30FdVNB.htm](pathfinder-bestiary-items/veNUDgghA30FdVNB.htm)|Dispelling Strike|Golpe disipador|modificada|
|[vEwagos32fLnf4Zc.htm](pathfinder-bestiary-items/vEwagos32fLnf4Zc.htm)|Scent 30 feet|Scent 30 pies|modificada|
|[VEZWJVB54nkdpnVP.htm](pathfinder-bestiary-items/VEZWJVB54nkdpnVP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VfbM5E2GxtRqzswt.htm](pathfinder-bestiary-items/VfbM5E2GxtRqzswt.htm)|Constant Spells|Constant Spells|modificada|
|[VfC8rvpO3IN8NVBE.htm](pathfinder-bestiary-items/VfC8rvpO3IN8NVBE.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[vfDqURmKtNWQI7IJ.htm](pathfinder-bestiary-items/vfDqURmKtNWQI7IJ.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[VfGIlE5o88e6ZuW6.htm](pathfinder-bestiary-items/VfGIlE5o88e6ZuW6.htm)|Pack Attack|Ataque en manada|modificada|
|[vgbSuZiK389nv7Tm.htm](pathfinder-bestiary-items/vgbSuZiK389nv7Tm.htm)|Profane Gift|Don profano|modificada|
|[vGCCZICFjOj2u4Dh.htm](pathfinder-bestiary-items/vGCCZICFjOj2u4Dh.htm)|Wing|Ala|modificada|
|[VgEW660oBhpCgGPZ.htm](pathfinder-bestiary-items/VgEW660oBhpCgGPZ.htm)|Claw|Garra|modificada|
|[vgW6UmniZ0N09eeE.htm](pathfinder-bestiary-items/vgW6UmniZ0N09eeE.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[vh1w0Pk1NCyqaccM.htm](pathfinder-bestiary-items/vh1w0Pk1NCyqaccM.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[vhcrEIRKkfTwrwDs.htm](pathfinder-bestiary-items/vhcrEIRKkfTwrwDs.htm)|Fetid Fumes|Vapores fétidos|modificada|
|[VhgpqA8362l8qc64.htm](pathfinder-bestiary-items/VhgpqA8362l8qc64.htm)|Wing|Ala|modificada|
|[vhMuwHfxhvT3a6e2.htm](pathfinder-bestiary-items/vhMuwHfxhvT3a6e2.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[VHQNRgFkCUkpPh9g.htm](pathfinder-bestiary-items/VHQNRgFkCUkpPh9g.htm)|Swift Tracker|Rastreador rápido|modificada|
|[vHVrBdzxAjZ3RShK.htm](pathfinder-bestiary-items/vHVrBdzxAjZ3RShK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VhwoW1Wd9SqC4MVO.htm](pathfinder-bestiary-items/VhwoW1Wd9SqC4MVO.htm)|Slither|Slither|modificada|
|[VhZJBu1NXWNgA9ey.htm](pathfinder-bestiary-items/VhZJBu1NXWNgA9ey.htm)|Grab|Agarrado|modificada|
|[Vib8DhTL418GaOhc.htm](pathfinder-bestiary-items/Vib8DhTL418GaOhc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VidlqkBOGMdBlbUB.htm](pathfinder-bestiary-items/VidlqkBOGMdBlbUB.htm)|Tail|Tail|modificada|
|[vipJW7r7EquegImq.htm](pathfinder-bestiary-items/vipJW7r7EquegImq.htm)|Adamantine Fangs|Colmillos Adamantinos|modificada|
|[vius8NahqzVZYRSE.htm](pathfinder-bestiary-items/vius8NahqzVZYRSE.htm)|Play the Pipes|Tocar la gaita|modificada|
|[vjCJtuDBXH0SYNN7.htm](pathfinder-bestiary-items/vjCJtuDBXH0SYNN7.htm)|Blood Frenzy|Frenesí de Sangre|modificada|
|[vJdzS91Cmz3WZZXD.htm](pathfinder-bestiary-items/vJdzS91Cmz3WZZXD.htm)|Soul Siphon|Succionar almas|modificada|
|[vJesB2gBXUdU1CMN.htm](pathfinder-bestiary-items/vJesB2gBXUdU1CMN.htm)|Flame Of Justice|Llama de la justiciaígera|modificada|
|[vJoGCQ4Ay2Cdm85A.htm](pathfinder-bestiary-items/vJoGCQ4Ay2Cdm85A.htm)|Swiftness|Celeridad|modificada|
|[VJqYJwxq4m0v6j2M.htm](pathfinder-bestiary-items/VJqYJwxq4m0v6j2M.htm)|Spores|Esporas|modificada|
|[vJxCPyKdG9Aaedkx.htm](pathfinder-bestiary-items/vJxCPyKdG9Aaedkx.htm)|+2 Status to All Saves vs. Magic|+2 situación a todas las salvaciones contra magia|modificada|
|[vKdhNyXTKHDMUfAc.htm](pathfinder-bestiary-items/vKdhNyXTKHDMUfAc.htm)|Flames of Fury|Llamas de la furia|modificada|
|[vkMvsL1nrlHkiJQQ.htm](pathfinder-bestiary-items/vkMvsL1nrlHkiJQQ.htm)|Embrace|Abrazo|modificada|
|[vknZjhozz53haMJZ.htm](pathfinder-bestiary-items/vknZjhozz53haMJZ.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[Vkvj7g0PCVVufjEu.htm](pathfinder-bestiary-items/Vkvj7g0PCVVufjEu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VKxXRGAQaygpB7SH.htm](pathfinder-bestiary-items/VKxXRGAQaygpB7SH.htm)|Jaws|Fauces|modificada|
|[vM8KFrCj03Tm1Odl.htm](pathfinder-bestiary-items/vM8KFrCj03Tm1Odl.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[VMzhTqEkTw0QRQvq.htm](pathfinder-bestiary-items/VMzhTqEkTw0QRQvq.htm)|Uncanny Climber|Escalador asombroso|modificada|
|[vNaUr8lkk7o1zXWJ.htm](pathfinder-bestiary-items/vNaUr8lkk7o1zXWJ.htm)|Ghost Touch|Toque fantasmal|modificada|
|[VNCpyxuyRZEyA5HG.htm](pathfinder-bestiary-items/VNCpyxuyRZEyA5HG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vNxkTMY4P5VmwDyM.htm](pathfinder-bestiary-items/vNxkTMY4P5VmwDyM.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[vNXYpdzNtNydM0Yh.htm](pathfinder-bestiary-items/vNXYpdzNtNydM0Yh.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[vOd0Oxhr36N6hj2R.htm](pathfinder-bestiary-items/vOd0Oxhr36N6hj2R.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VONgaGyF9aRjoaok.htm](pathfinder-bestiary-items/VONgaGyF9aRjoaok.htm)|Blizzard|Blizzard|modificada|
|[VPh2h7TJhQU8ugDu.htm](pathfinder-bestiary-items/VPh2h7TJhQU8ugDu.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Vpr4X9t4ia5n8MLO.htm](pathfinder-bestiary-items/Vpr4X9t4ia5n8MLO.htm)|Jaws|Fauces|modificada|
|[VpVNCwsAHOKuj0UI.htm](pathfinder-bestiary-items/VpVNCwsAHOKuj0UI.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[VQFM8xePgKsrXydY.htm](pathfinder-bestiary-items/VQFM8xePgKsrXydY.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[vQNY731fleWIGeZp.htm](pathfinder-bestiary-items/vQNY731fleWIGeZp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vqr3Mssyf2Td2T1D.htm](pathfinder-bestiary-items/vqr3Mssyf2Td2T1D.htm)|Frightful Presence|Frightful Presence|modificada|
|[VQW9KJrLdz5lDjJ9.htm](pathfinder-bestiary-items/VQW9KJrLdz5lDjJ9.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[vr6e0VYbUOGnEU3M.htm](pathfinder-bestiary-items/vr6e0VYbUOGnEU3M.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[vRByig4Tnrtjds3I.htm](pathfinder-bestiary-items/vRByig4Tnrtjds3I.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VRVvTkZmbfIqx7uw.htm](pathfinder-bestiary-items/VRVvTkZmbfIqx7uw.htm)|Summon Steed|Convocar corcel|modificada|
|[VRZR3SXqGz9bOL7Z.htm](pathfinder-bestiary-items/VRZR3SXqGz9bOL7Z.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[vskdCA5sskgL7g3S.htm](pathfinder-bestiary-items/vskdCA5sskgL7g3S.htm)|Dream Haunting|Acoso onírico del Sueño|modificada|
|[Vst7qvKg3u5uhWKG.htm](pathfinder-bestiary-items/Vst7qvKg3u5uhWKG.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[vt7bc5rxJY8WxTy0.htm](pathfinder-bestiary-items/vt7bc5rxJY8WxTy0.htm)|Acid Spit|Acid Spit|modificada|
|[vTjkwNzpCJmsbg6R.htm](pathfinder-bestiary-items/vTjkwNzpCJmsbg6R.htm)|Shortsword|Espada corta|modificada|
|[vTXaShzYmcNbRjzk.htm](pathfinder-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|Bloodcurdling Screech|modificada|
|[vudcfpUT9un2pDOP.htm](pathfinder-bestiary-items/vudcfpUT9un2pDOP.htm)|Gnaw|Gnaw|modificada|
|[vUwGp8Kc7J2fDUxa.htm](pathfinder-bestiary-items/vUwGp8Kc7J2fDUxa.htm)|Shortsword|Espada corta|modificada|
|[VVvneLCIn09BPxX7.htm](pathfinder-bestiary-items/VVvneLCIn09BPxX7.htm)|Erosion Aura|Aura de erosión|modificada|
|[VWI6L5E7GsAtSrfF.htm](pathfinder-bestiary-items/VWI6L5E7GsAtSrfF.htm)|All-Around Vision|All-Around Vision|modificada|
|[vwO9JU83wAh6vmts.htm](pathfinder-bestiary-items/vwO9JU83wAh6vmts.htm)|Crag Linnorm Venom|Veneno de linnorm del risco|modificada|
|[VwrlDdZczBGO5Ps4.htm](pathfinder-bestiary-items/VwrlDdZczBGO5Ps4.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[VwuUYBwpcwv3BGFP.htm](pathfinder-bestiary-items/VwuUYBwpcwv3BGFP.htm)|Branch|Rama|modificada|
|[vwWsYBBoZjtVHTuT.htm](pathfinder-bestiary-items/vwWsYBBoZjtVHTuT.htm)|Snow Vision|Snow Vision|modificada|
|[VxA7WerDGElBkdYj.htm](pathfinder-bestiary-items/VxA7WerDGElBkdYj.htm)|Swamp Stride|Swamp Stride|modificada|
|[VXAdFMEIJiPWyhRq.htm](pathfinder-bestiary-items/VXAdFMEIJiPWyhRq.htm)|Negative Healing|Curación negativa|modificada|
|[VXiR6GGa6NxxxBMb.htm](pathfinder-bestiary-items/VXiR6GGa6NxxxBMb.htm)|Head Hunter|Cazacabezas|modificada|
|[vXxTQUxFEXV4Ldxm.htm](pathfinder-bestiary-items/vXxTQUxFEXV4Ldxm.htm)|Leaping Grab|Agarrón en salto|modificada|
|[vXYYcxC29lm6Xqml.htm](pathfinder-bestiary-items/vXYYcxC29lm6Xqml.htm)|Earthbound|Ligado a la tierra|modificada|
|[Vy2pPgrGJ3SguoOn.htm](pathfinder-bestiary-items/Vy2pPgrGJ3SguoOn.htm)|Shadow Hand|Mano de Sombra|modificada|
|[Vy7eGgsZwEtFCJod.htm](pathfinder-bestiary-items/Vy7eGgsZwEtFCJod.htm)|Improved Grab|Agarrado mejorado|modificada|
|[VYBvEnOj0TibKdci.htm](pathfinder-bestiary-items/VYBvEnOj0TibKdci.htm)|Breath Weapon|Breath Weapon|modificada|
|[Vyc7HceERBcZQuXT.htm](pathfinder-bestiary-items/Vyc7HceERBcZQuXT.htm)|Warding Glyph|Glifo custodio|modificada|
|[VYGyp2X6IJdSS0b2.htm](pathfinder-bestiary-items/VYGyp2X6IJdSS0b2.htm)|Monitor Lizard Venom|Monitor de lagarto veneno|modificada|
|[VYK3ix2hBaIvUNXp.htm](pathfinder-bestiary-items/VYK3ix2hBaIvUNXp.htm)|Fist|Puño|modificada|
|[VyoEzllElTOu9fmG.htm](pathfinder-bestiary-items/VyoEzllElTOu9fmG.htm)|Change Size|Cambiar tama|modificada|
|[VyTDZWtEYeQGBrXk.htm](pathfinder-bestiary-items/VyTDZWtEYeQGBrXk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[vZOKYoSD979v70vq.htm](pathfinder-bestiary-items/vZOKYoSD979v70vq.htm)|Corpse Wave|Ola de Cadáveres|modificada|
|[vZVJQzztFpSjzbNW.htm](pathfinder-bestiary-items/vZVJQzztFpSjzbNW.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[w14YL6ltyHxsjfDO.htm](pathfinder-bestiary-items/w14YL6ltyHxsjfDO.htm)|Reality Twist|Distorsión de realidad|modificada|
|[W1EiRfnWuUIq6KoC.htm](pathfinder-bestiary-items/W1EiRfnWuUIq6KoC.htm)|Speed Surge|Arranque de velocidad|modificada|
|[W1uyRdJuvABJ7epP.htm](pathfinder-bestiary-items/W1uyRdJuvABJ7epP.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[W3a4Bkc73B2bUZDE.htm](pathfinder-bestiary-items/W3a4Bkc73B2bUZDE.htm)|Energy Drain|Drenaje de Energía|modificada|
|[w4Cd5RsR9MjHTRDj.htm](pathfinder-bestiary-items/w4Cd5RsR9MjHTRDj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[w5FhuQgOnX0i92Y3.htm](pathfinder-bestiary-items/w5FhuQgOnX0i92Y3.htm)|Jaws|Fauces|modificada|
|[w6BFsw2xw1ZKxz55.htm](pathfinder-bestiary-items/w6BFsw2xw1ZKxz55.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[W6dNim4KEjBzAlMj.htm](pathfinder-bestiary-items/W6dNim4KEjBzAlMj.htm)|Frightful Presence|Frightful Presence|modificada|
|[W8WLNath7obs8p6Z.htm](pathfinder-bestiary-items/W8WLNath7obs8p6Z.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W9vP5WtfdUoAWPDY.htm](pathfinder-bestiary-items/W9vP5WtfdUoAWPDY.htm)|Jaws|Fauces|modificada|
|[Wa8dbVwSNL2tEHKt.htm](pathfinder-bestiary-items/Wa8dbVwSNL2tEHKt.htm)|Encircling Command|Orden de rodear|modificada|
|[waAgRDWjrJU73Mop.htm](pathfinder-bestiary-items/waAgRDWjrJU73Mop.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[waCZVvDN6vAn7yCi.htm](pathfinder-bestiary-items/waCZVvDN6vAn7yCi.htm)|Spray Acid|Rociada ácida|modificada|
|[wAhBz8qI1QWAKLEv.htm](pathfinder-bestiary-items/wAhBz8qI1QWAKLEv.htm)|Beak|Beak|modificada|
|[wAitzoQtepFmPhqR.htm](pathfinder-bestiary-items/wAitzoQtepFmPhqR.htm)|Twisting Tail|Enredar con la cola|modificada|
|[WAp1bEkhC2Ag8cwo.htm](pathfinder-bestiary-items/WAp1bEkhC2Ag8cwo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wAPCN756UU6AV2AN.htm](pathfinder-bestiary-items/wAPCN756UU6AV2AN.htm)|Courageous Switch|Intercambio valeroso|modificada|
|[WaVlCwJbdjhIEPYG.htm](pathfinder-bestiary-items/WaVlCwJbdjhIEPYG.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[wb9jgdwbjkdgmDPc.htm](pathfinder-bestiary-items/wb9jgdwbjkdgmDPc.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[wbhIx2xtiqZn73im.htm](pathfinder-bestiary-items/wbhIx2xtiqZn73im.htm)|+3 Status to All Saves vs. Divine Magic|+3 situación a todas las salvaciones contra magia divina.|modificada|
|[WbJQr5XhkIENzNfG.htm](pathfinder-bestiary-items/WbJQr5XhkIENzNfG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wbl4QeudXG1wsK2j.htm](pathfinder-bestiary-items/wbl4QeudXG1wsK2j.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[wBppFKxjdSadJQj2.htm](pathfinder-bestiary-items/wBppFKxjdSadJQj2.htm)|Shortbow|Arco corto|modificada|
|[wBs1NoPWhm38PmMP.htm](pathfinder-bestiary-items/wBs1NoPWhm38PmMP.htm)|Club|Club|modificada|
|[WBvBXluaSwwxLaJ4.htm](pathfinder-bestiary-items/WBvBXluaSwwxLaJ4.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[WC9rcwU4DaoOfejU.htm](pathfinder-bestiary-items/WC9rcwU4DaoOfejU.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[wChn2PEw9GYziq2n.htm](pathfinder-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|Glow|modificada|
|[wcP5j8v9fGg91KoQ.htm](pathfinder-bestiary-items/wcP5j8v9fGg91KoQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WCpWLcuShftaYr4k.htm](pathfinder-bestiary-items/WCpWLcuShftaYr4k.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WCwa6PfOl2tfM35F.htm](pathfinder-bestiary-items/WCwa6PfOl2tfM35F.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WD2TqHcVPCybwkQT.htm](pathfinder-bestiary-items/WD2TqHcVPCybwkQT.htm)|Vortex|Vortex|modificada|
|[wd9cXI5wKeM8sebH.htm](pathfinder-bestiary-items/wd9cXI5wKeM8sebH.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[wDlRAMig5HNlqg3q.htm](pathfinder-bestiary-items/wDlRAMig5HNlqg3q.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WdOsTqGjiU4ZnX5H.htm](pathfinder-bestiary-items/WdOsTqGjiU4ZnX5H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wdsdNrRHG7tMUw5P.htm](pathfinder-bestiary-items/wdsdNrRHG7tMUw5P.htm)|Fist|Puño|modificada|
|[wDyI0HWtmrF7nCK9.htm](pathfinder-bestiary-items/wDyI0HWtmrF7nCK9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[WDz6pEmszokFTO25.htm](pathfinder-bestiary-items/WDz6pEmszokFTO25.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[WE5R0TF08XSDSPjw.htm](pathfinder-bestiary-items/WE5R0TF08XSDSPjw.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[We6LladGR4C687q0.htm](pathfinder-bestiary-items/We6LladGR4C687q0.htm)|Reefclaw Venom|Reefclaw Venom|modificada|
|[WegG5uFcFYKrHbBF.htm](pathfinder-bestiary-items/WegG5uFcFYKrHbBF.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[wEHW7Aj6HAGWNLXA.htm](pathfinder-bestiary-items/wEHW7Aj6HAGWNLXA.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[wEVWlNgGdVwq51Y9.htm](pathfinder-bestiary-items/wEVWlNgGdVwq51Y9.htm)|Claw|Garra|modificada|
|[WeXirhh6gD0DCTGB.htm](pathfinder-bestiary-items/WeXirhh6gD0DCTGB.htm)|Root|Enraizarse|modificada|
|[Wf7eTH5SNYNO98tC.htm](pathfinder-bestiary-items/Wf7eTH5SNYNO98tC.htm)|Evasion|Evasión|modificada|
|[wfIAFcaANDUUhMSm.htm](pathfinder-bestiary-items/wfIAFcaANDUUhMSm.htm)|Vanish|Vanish|modificada|
|[wfKBAQHE6bHpnSve.htm](pathfinder-bestiary-items/wfKBAQHE6bHpnSve.htm)|Grab|Agarrado|modificada|
|[wfMs8RYiuhFeQMau.htm](pathfinder-bestiary-items/wfMs8RYiuhFeQMau.htm)|Mauler|Vapulear|modificada|
|[wGDHR7b7nFvTcaRz.htm](pathfinder-bestiary-items/wGDHR7b7nFvTcaRz.htm)|Sphere of Oblivion|Esfera del Olvido|modificada|
|[WGKxmcUB8pYq06Qv.htm](pathfinder-bestiary-items/WGKxmcUB8pYq06Qv.htm)|Constant Spells|Constant Spells|modificada|
|[wgMagyELbs4gcxl2.htm](pathfinder-bestiary-items/wgMagyELbs4gcxl2.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[wgSeXMtV7TUrqAqJ.htm](pathfinder-bestiary-items/wgSeXMtV7TUrqAqJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WH7tGqprqYb1tk4C.htm](pathfinder-bestiary-items/WH7tGqprqYb1tk4C.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[WhTbPOtlxlYvCpJ6.htm](pathfinder-bestiary-items/WhTbPOtlxlYvCpJ6.htm)|Homunculus Poison|Veneno de homúnculo|modificada|
|[wHTcTY8XLxxGwUU9.htm](pathfinder-bestiary-items/wHTcTY8XLxxGwUU9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WI20ZnQYqmSIaz3B.htm](pathfinder-bestiary-items/WI20ZnQYqmSIaz3B.htm)|Drawn to Service|Obligado a servir|modificada|
|[wi645CsJxPLHzqhq.htm](pathfinder-bestiary-items/wi645CsJxPLHzqhq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WIHK77N13bnY09Iq.htm](pathfinder-bestiary-items/WIHK77N13bnY09Iq.htm)|Aqueous Fist|Puño acuoso|modificada|
|[Wir6Og85EjTj5jKg.htm](pathfinder-bestiary-items/Wir6Og85EjTj5jKg.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[WjBhdxKcekKdof75.htm](pathfinder-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wJnJT5Ecr1w6xix3.htm](pathfinder-bestiary-items/wJnJT5Ecr1w6xix3.htm)|Shortbow|Arco corto|modificada|
|[WJYznUjqpKPO2kQz.htm](pathfinder-bestiary-items/WJYznUjqpKPO2kQz.htm)|Grab|Agarrado|modificada|
|[WkB3ob6zOZXGPfnf.htm](pathfinder-bestiary-items/WkB3ob6zOZXGPfnf.htm)|Fog Vision|Fog Vision|modificada|
|[WkPeg600zGONsuJz.htm](pathfinder-bestiary-items/WkPeg600zGONsuJz.htm)|Jaws|Fauces|modificada|
|[WLCM7iPJWfoRusL5.htm](pathfinder-bestiary-items/WLCM7iPJWfoRusL5.htm)|Death Roll|Giro mortal|modificada|
|[WLKYWXEiwmc9uCr5.htm](pathfinder-bestiary-items/WLKYWXEiwmc9uCr5.htm)|Master Link|Vínculo con el amo|modificada|
|[wLRqThGwPUufb2HR.htm](pathfinder-bestiary-items/wLRqThGwPUufb2HR.htm)|Raking Sand|Raking Sand|modificada|
|[WLWmgPx0MfGb1F5z.htm](pathfinder-bestiary-items/WLWmgPx0MfGb1F5z.htm)|Light Pick|Púa de luz|modificada|
|[WmmDzTWb78QbaymG.htm](pathfinder-bestiary-items/WmmDzTWb78QbaymG.htm)|Claw|Garra|modificada|
|[wmnFY6ylzPcsOlEb.htm](pathfinder-bestiary-items/wmnFY6ylzPcsOlEb.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[WnbC97emFRGYOPR9.htm](pathfinder-bestiary-items/WnbC97emFRGYOPR9.htm)|Sphere Of Oblivion|Esfera Del Olvido|modificada|
|[WnjXiMvqjEQAuxnZ.htm](pathfinder-bestiary-items/WnjXiMvqjEQAuxnZ.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[woCB9gbBdvxg3WX0.htm](pathfinder-bestiary-items/woCB9gbBdvxg3WX0.htm)|Leaping Charge|Carga en salto|modificada|
|[wOkHQN8ELSDh7T1f.htm](pathfinder-bestiary-items/wOkHQN8ELSDh7T1f.htm)|Fist|Puño|modificada|
|[woTn67ogkZRQUfvH.htm](pathfinder-bestiary-items/woTn67ogkZRQUfvH.htm)|Claw|Garra|modificada|
|[Wp8326gStt6XrGVS.htm](pathfinder-bestiary-items/Wp8326gStt6XrGVS.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[wPoobMAF8dEMbXzS.htm](pathfinder-bestiary-items/wPoobMAF8dEMbXzS.htm)|Tendril|Tendril|modificada|
|[WPtFVQLHQ2T2WxY7.htm](pathfinder-bestiary-items/WPtFVQLHQ2T2WxY7.htm)|Deafening Blow|Impacto ensordecedor|modificada|
|[WQED2R71AMrx1suT.htm](pathfinder-bestiary-items/WQED2R71AMrx1suT.htm)|Golem Antimagic|Antimagia de gólem|modificada|
|[wqhHoIIgE3jfAgCx.htm](pathfinder-bestiary-items/wqhHoIIgE3jfAgCx.htm)|Smoke Vision|Visión de Humo|modificada|
|[WQlpCiZyHiNu4LE0.htm](pathfinder-bestiary-items/WQlpCiZyHiNu4LE0.htm)|Grab|Agarrado|modificada|
|[WQqo16zlXzVAyGHX.htm](pathfinder-bestiary-items/WQqo16zlXzVAyGHX.htm)|Jungle Drake Venom|Veneno de draco selvático|modificada|
|[WqyBNiwVl76DPQYF.htm](pathfinder-bestiary-items/WqyBNiwVl76DPQYF.htm)|Leg|Pierna|modificada|
|[wR8rvUNTkSFlNhUv.htm](pathfinder-bestiary-items/wR8rvUNTkSFlNhUv.htm)|Fist|Puño|modificada|
|[wrhgHCC4FVBo4mrU.htm](pathfinder-bestiary-items/wrhgHCC4FVBo4mrU.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[wrHyzkgKb8tdmukX.htm](pathfinder-bestiary-items/wrHyzkgKb8tdmukX.htm)|Claw|Garra|modificada|
|[wRiugNyM38JRtkeI.htm](pathfinder-bestiary-items/wRiugNyM38JRtkeI.htm)|Coven Spells|Coven Spells|modificada|
|[Wsl0Qe07OtlNGmr0.htm](pathfinder-bestiary-items/Wsl0Qe07OtlNGmr0.htm)|Subservience|Servilismo|modificada|
|[wsuET1ePJr0p7cTd.htm](pathfinder-bestiary-items/wsuET1ePJr0p7cTd.htm)|Fist|Puño|modificada|
|[wTyVptTaqC4Su8lm.htm](pathfinder-bestiary-items/wTyVptTaqC4Su8lm.htm)|Energy Touch|Toque de Energía|modificada|
|[WU4cXLzhnoCczalO.htm](pathfinder-bestiary-items/WU4cXLzhnoCczalO.htm)|Irritating Dander|Caspa irritante|modificada|
|[Wu9GLINqkqLPEjJ7.htm](pathfinder-bestiary-items/Wu9GLINqkqLPEjJ7.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[WUUFHNKmZ2orqvtO.htm](pathfinder-bestiary-items/WUUFHNKmZ2orqvtO.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[WuUxcHe2LEnb3BVQ.htm](pathfinder-bestiary-items/WuUxcHe2LEnb3BVQ.htm)|Giant Scorpion Venom|Veneno de escorppión gigante.|modificada|
|[Wvb6MKl733cfKJ64.htm](pathfinder-bestiary-items/Wvb6MKl733cfKJ64.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[wVxhMkTfMFAVDviB.htm](pathfinder-bestiary-items/wVxhMkTfMFAVDviB.htm)|Maul|Zarpazo doble|modificada|
|[wW41T2Rqe1HsYWiK.htm](pathfinder-bestiary-items/wW41T2Rqe1HsYWiK.htm)|Repair Mode|Modo de reparación|modificada|
|[WW8hndG3qvu25VrH.htm](pathfinder-bestiary-items/WW8hndG3qvu25VrH.htm)|Cloud Walk|Cloud Walk|modificada|
|[WWnDflwvjpP5jAyW.htm](pathfinder-bestiary-items/WWnDflwvjpP5jAyW.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[wwQOKHAUPw4ltzVK.htm](pathfinder-bestiary-items/wwQOKHAUPw4ltzVK.htm)|Change Shape|Change Shape|modificada|
|[wWrvIQM5fOYBSMRQ.htm](pathfinder-bestiary-items/wWrvIQM5fOYBSMRQ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[WX7zOVpbKUqeICJ0.htm](pathfinder-bestiary-items/WX7zOVpbKUqeICJ0.htm)|Desert Wind|Viento del Desierto|modificada|
|[wXAUYhVyZP3okwuu.htm](pathfinder-bestiary-items/wXAUYhVyZP3okwuu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wya8y6adOGTfI5jX.htm](pathfinder-bestiary-items/wya8y6adOGTfI5jX.htm)|Change Shape|Change Shape|modificada|
|[wYcs9eIcJSh0thsP.htm](pathfinder-bestiary-items/wYcs9eIcJSh0thsP.htm)|Centipede Swarm Venom|Enjambre Ciempiés Veneno|modificada|
|[WYRi6VyNroGIrEk6.htm](pathfinder-bestiary-items/WYRi6VyNroGIrEk6.htm)|Crystalline Dust Form|Forma Polvo Cristalino|modificada|
|[WzNNAcPz7ibjxcS8.htm](pathfinder-bestiary-items/WzNNAcPz7ibjxcS8.htm)|Claw|Garra|modificada|
|[WZrWbGf8dD3tbKfR.htm](pathfinder-bestiary-items/WZrWbGf8dD3tbKfR.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[x0anEXm5LMDe02cY.htm](pathfinder-bestiary-items/x0anEXm5LMDe02cY.htm)|Darkvision|Visión en la oscuridad|modificada|
|[X0BfkEsL9YNikfyR.htm](pathfinder-bestiary-items/X0BfkEsL9YNikfyR.htm)|Wave|Wave|modificada|
|[x1hzvAiCUjfrEaZO.htm](pathfinder-bestiary-items/x1hzvAiCUjfrEaZO.htm)|Descend on a Web|Descender por una telara|modificada|
|[x3yCpybINerCQY3Q.htm](pathfinder-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|Toque Corrosivo|modificada|
|[x47iDfIE9albveGl.htm](pathfinder-bestiary-items/x47iDfIE9albveGl.htm)|Shadow Hand|Mano de Sombra|modificada|
|[X4qGxyWYxGqKvBsG.htm](pathfinder-bestiary-items/X4qGxyWYxGqKvBsG.htm)|Mandibles|Mandíbulas|modificada|
|[X4U013pt6H6xdUSt.htm](pathfinder-bestiary-items/X4U013pt6H6xdUSt.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[X5DctZ32pPLyBZnM.htm](pathfinder-bestiary-items/X5DctZ32pPLyBZnM.htm)|Entropy Sense (Imprecise) 60 feet|Sentido de Entropía (Impreciso) 60 pies|modificada|
|[X7eyVZD1ZflrSoj8.htm](pathfinder-bestiary-items/X7eyVZD1ZflrSoj8.htm)|Mucus Cloud|Nube de mucosa|modificada|
|[x7U8W2mn02rKkpEU.htm](pathfinder-bestiary-items/x7U8W2mn02rKkpEU.htm)|Scythe|Guadaña|modificada|
|[X8djlbeUDOZqxfAS.htm](pathfinder-bestiary-items/X8djlbeUDOZqxfAS.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[x8uwJOVGskq8pjOH.htm](pathfinder-bestiary-items/x8uwJOVGskq8pjOH.htm)|Circling Attack|Ataque circular|modificada|
|[X9BvGRPY6jnbRCrG.htm](pathfinder-bestiary-items/X9BvGRPY6jnbRCrG.htm)|Throw Rock|Arrojar roca|modificada|
|[X9lnoAMRNp4d62Iu.htm](pathfinder-bestiary-items/X9lnoAMRNp4d62Iu.htm)|Rush|Embestida|modificada|
|[xa1KRhMM6JnsIsgl.htm](pathfinder-bestiary-items/xa1KRhMM6JnsIsgl.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[xa72ZqFCCn9Ps5lD.htm](pathfinder-bestiary-items/xa72ZqFCCn9Ps5lD.htm)|Curse of Fire|Curse of Fire|modificada|
|[XAY63LFVYjJCN65F.htm](pathfinder-bestiary-items/XAY63LFVYjJCN65F.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[XbEOrTGzNN49cBsV.htm](pathfinder-bestiary-items/XbEOrTGzNN49cBsV.htm)|Jaws|Fauces|modificada|
|[xBi3GMrwnT7AE4mq.htm](pathfinder-bestiary-items/xBi3GMrwnT7AE4mq.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[xBIoPO0Yl00JwtXW.htm](pathfinder-bestiary-items/xBIoPO0Yl00JwtXW.htm)|Swamp Stride|Swamp Stride|modificada|
|[xBWfY8gxHjGavgBq.htm](pathfinder-bestiary-items/xBWfY8gxHjGavgBq.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[xby0iVqmufLqYCMA.htm](pathfinder-bestiary-items/xby0iVqmufLqYCMA.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[XbyGgxrnfchvOdm3.htm](pathfinder-bestiary-items/XbyGgxrnfchvOdm3.htm)|Pounce|Abalanzarse|modificada|
|[XCbPDCIugQCZ6Euc.htm](pathfinder-bestiary-items/XCbPDCIugQCZ6Euc.htm)|Trample|Trample|modificada|
|[XCfMmPXvB7LJznlU.htm](pathfinder-bestiary-items/XCfMmPXvB7LJznlU.htm)|Frost Composite Longbow|Arco largo compuesto de Gélida.|modificada|
|[xCmDwMnAF2ZdjWhU.htm](pathfinder-bestiary-items/xCmDwMnAF2ZdjWhU.htm)|Twisting Tail|Enredar con la cola|modificada|
|[xCmuYVEvMqVlGxIw.htm](pathfinder-bestiary-items/xCmuYVEvMqVlGxIw.htm)|Claw|Garra|modificada|
|[xD2fbKZSefTlgsmQ.htm](pathfinder-bestiary-items/xD2fbKZSefTlgsmQ.htm)|Failure Vulnerability|Vulnerabilidad al fracaso|modificada|
|[XD6GRQzVXAnrWid6.htm](pathfinder-bestiary-items/XD6GRQzVXAnrWid6.htm)|Tail|Tail|modificada|
|[Xd6tMydNtV3GyWPm.htm](pathfinder-bestiary-items/Xd6tMydNtV3GyWPm.htm)|Scrap Ball|Bola de Chatarra|modificada|
|[xdClMhhs1mXbf26g.htm](pathfinder-bestiary-items/xdClMhhs1mXbf26g.htm)|Wing Deflection|Desvío con el ala|modificada|
|[XDq62TH6LQA4rgny.htm](pathfinder-bestiary-items/XDq62TH6LQA4rgny.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[xDUpwWQaZevQOkYQ.htm](pathfinder-bestiary-items/xDUpwWQaZevQOkYQ.htm)|Ferocity|Ferocidad|modificada|
|[xDzg2v4rQ9wNAK3b.htm](pathfinder-bestiary-items/xDzg2v4rQ9wNAK3b.htm)|Death's Grace|Gracia de la muerte|modificada|
|[Xe4Ca7mHwdqwZ7wZ.htm](pathfinder-bestiary-items/Xe4Ca7mHwdqwZ7wZ.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[xeAR997MOrxz4cyA.htm](pathfinder-bestiary-items/xeAR997MOrxz4cyA.htm)|Gallop|Galope|modificada|
|[xECw5sCWsHZAUjFv.htm](pathfinder-bestiary-items/xECw5sCWsHZAUjFv.htm)|Longsword|Longsword|modificada|
|[xEHY10Oxi5BJJCmQ.htm](pathfinder-bestiary-items/xEHY10Oxi5BJJCmQ.htm)|Wide Swing|Barrido ancho|modificada|
|[Xem818Dy2SdT8fjI.htm](pathfinder-bestiary-items/Xem818Dy2SdT8fjI.htm)|Glaring Ray|Rayo Deslumbrante|modificada|
|[XfQQjjOdt5sRiSb6.htm](pathfinder-bestiary-items/XfQQjjOdt5sRiSb6.htm)|Lion Jaws|Fauces de León|modificada|
|[xFztWiaMY5iGr24T.htm](pathfinder-bestiary-items/xFztWiaMY5iGr24T.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Xg50UZe4CSts7Kvw.htm](pathfinder-bestiary-items/Xg50UZe4CSts7Kvw.htm)|Horns|Cuernos|modificada|
|[XGgKDNKukruETANT.htm](pathfinder-bestiary-items/XGgKDNKukruETANT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XgjH4JAk9jbj9B8a.htm](pathfinder-bestiary-items/XgjH4JAk9jbj9B8a.htm)|Shamble|Shamble|modificada|
|[XGl21E5BQZXRMicO.htm](pathfinder-bestiary-items/XGl21E5BQZXRMicO.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[XGMdT1ANHdPUE8UO.htm](pathfinder-bestiary-items/XGMdT1ANHdPUE8UO.htm)|Flaming Stroke|Tajo llameante|modificada|
|[XgoaORovhBAB82vg.htm](pathfinder-bestiary-items/XgoaORovhBAB82vg.htm)|Constant Spells|Constant Spells|modificada|
|[xGPdcgIZR0tAI9IY.htm](pathfinder-bestiary-items/xGPdcgIZR0tAI9IY.htm)|Hair Barrage|Hair Barrage|modificada|
|[XgwBNbRPKr9hX8NS.htm](pathfinder-bestiary-items/XgwBNbRPKr9hX8NS.htm)|Shield Block|Bloquear con escudo|modificada|
|[xgyBlDJ39BUcAINW.htm](pathfinder-bestiary-items/xgyBlDJ39BUcAINW.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[XgycaDcxnUeoby8d.htm](pathfinder-bestiary-items/XgycaDcxnUeoby8d.htm)|Fist|Puño|modificada|
|[xH6NrBw4bYEHU7e3.htm](pathfinder-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[xH8E3TYe6R3G6KEv.htm](pathfinder-bestiary-items/xH8E3TYe6R3G6KEv.htm)|Locate Inevitable|Localizar Inevitable|modificada|
|[xheziaVMnj1iAjP5.htm](pathfinder-bestiary-items/xheziaVMnj1iAjP5.htm)|Caustic Mucus|Mucosa cáustica|modificada|
|[XHFapZJB0eCngkyn.htm](pathfinder-bestiary-items/XHFapZJB0eCngkyn.htm)|Dune|Dune|modificada|
|[xHg8hrfrTg35erxf.htm](pathfinder-bestiary-items/xHg8hrfrTg35erxf.htm)|Contingency|Contingencia|modificada|
|[XhN7A3RmBONMzZUF.htm](pathfinder-bestiary-items/XhN7A3RmBONMzZUF.htm)|Foot|Pie|modificada|
|[xhqKU8Bdnig3dLJs.htm](pathfinder-bestiary-items/xhqKU8Bdnig3dLJs.htm)|Water-Bound|Ligado al agua|modificada|
|[xHsqXGMqu6bMLRTB.htm](pathfinder-bestiary-items/xHsqXGMqu6bMLRTB.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[xHuOFLF02tZy7mwZ.htm](pathfinder-bestiary-items/xHuOFLF02tZy7mwZ.htm)|Claw|Garra|modificada|
|[XIYonpEs96Js86vZ.htm](pathfinder-bestiary-items/XIYonpEs96Js86vZ.htm)|Rapid Strikes|Golpes Rápidos|modificada|
|[XJ5wfV6LNdfMn7EH.htm](pathfinder-bestiary-items/XJ5wfV6LNdfMn7EH.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[XJHXZOImLOORwInT.htm](pathfinder-bestiary-items/XJHXZOImLOORwInT.htm)|Jaws|Fauces|modificada|
|[XJKtPma1TpZuVUUA.htm](pathfinder-bestiary-items/XJKtPma1TpZuVUUA.htm)|Beak|Beak|modificada|
|[XK9Va6QKy2g8z1dZ.htm](pathfinder-bestiary-items/XK9Va6QKy2g8z1dZ.htm)|Giant Octopus Venom|Veneno de pulpo gigante|modificada|
|[xkIBsOSynEo9Ii6k.htm](pathfinder-bestiary-items/xkIBsOSynEo9Ii6k.htm)|Staggering Strike|Golpe Tambaleante|modificada|
|[xkMpFGfoqM0o6IYm.htm](pathfinder-bestiary-items/xkMpFGfoqM0o6IYm.htm)|Telepathy 300 feet|Telepatía 300 pies.|modificada|
|[xkve6W8Rg8tqIPX0.htm](pathfinder-bestiary-items/xkve6W8Rg8tqIPX0.htm)|Tail|Tail|modificada|
|[xl5RfS1yyVh85doM.htm](pathfinder-bestiary-items/xl5RfS1yyVh85doM.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[xlEh7Hz3HcY47r4R.htm](pathfinder-bestiary-items/xlEh7Hz3HcY47r4R.htm)|Tremorsense (Imprecise) 90 feet|Sentido del Temblor (Impreciso) 90 pies|modificada|
|[XLiAwm5JWDoSNIPF.htm](pathfinder-bestiary-items/XLiAwm5JWDoSNIPF.htm)|Beak|Beak|modificada|
|[xlUnQcjrxeMXu5Cd.htm](pathfinder-bestiary-items/xlUnQcjrxeMXu5Cd.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[Xm0r5sx1GTiEAKc4.htm](pathfinder-bestiary-items/Xm0r5sx1GTiEAKc4.htm)|Cough Spores|Toser esporas|modificada|
|[Xmaw18x2qDwDq7tB.htm](pathfinder-bestiary-items/Xmaw18x2qDwDq7tB.htm)|Curse of the Werewolf|Maldición del Hombre Lobo|modificada|
|[xMc6TnmwsR72HZR7.htm](pathfinder-bestiary-items/xMc6TnmwsR72HZR7.htm)|Dream Haunting|Acoso onírico del Sueño|modificada|
|[XMcVGkoaO2Xaj18P.htm](pathfinder-bestiary-items/XMcVGkoaO2Xaj18P.htm)|Grab|Agarrado|modificada|
|[xoD1yB3vzy3xf2iD.htm](pathfinder-bestiary-items/xoD1yB3vzy3xf2iD.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[xOGJ7uK7SF5xcrH2.htm](pathfinder-bestiary-items/xOGJ7uK7SF5xcrH2.htm)|Vulnerable to Disintegrate|Vulnerable a desintegrar|modificada|
|[xolLzAgsgpzS6Nvn.htm](pathfinder-bestiary-items/xolLzAgsgpzS6Nvn.htm)|Shield Block|Bloquear con escudo|modificada|
|[XoQjkRouHO2yUuKc.htm](pathfinder-bestiary-items/XoQjkRouHO2yUuKc.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[XozXyBaOh9vAKQlv.htm](pathfinder-bestiary-items/XozXyBaOh9vAKQlv.htm)|Constant Spells|Constant Spells|modificada|
|[xpcbUoWx3vMpIFDi.htm](pathfinder-bestiary-items/xpcbUoWx3vMpIFDi.htm)|Regeneration 25 (Deactivated by Cold)|Regeneración 25 (Desactivado por el frío)|modificada|
|[XpcWHnvqjcngSTyK.htm](pathfinder-bestiary-items/XpcWHnvqjcngSTyK.htm)|Jaws|Fauces|modificada|
|[XPgz4pQn9aYVvddX.htm](pathfinder-bestiary-items/XPgz4pQn9aYVvddX.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[XPQEvEN2tEEPYRWA.htm](pathfinder-bestiary-items/XPQEvEN2tEEPYRWA.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[XQKbqAKqILs3dbvQ.htm](pathfinder-bestiary-items/XQKbqAKqILs3dbvQ.htm)|Water Dependent|Water Dependent|modificada|
|[xqRgagTvArv2abwD.htm](pathfinder-bestiary-items/xqRgagTvArv2abwD.htm)|Constant Spells|Constant Spells|modificada|
|[XqSpTVGRCbtpPfJU.htm](pathfinder-bestiary-items/XqSpTVGRCbtpPfJU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xr7UgtUP8wXPdHah.htm](pathfinder-bestiary-items/xr7UgtUP8wXPdHah.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[xRQZegRo2JOZB7hU.htm](pathfinder-bestiary-items/xRQZegRo2JOZB7hU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[xrTNYpA1abm4LPwG.htm](pathfinder-bestiary-items/xrTNYpA1abm4LPwG.htm)|Coven|Coven|modificada|
|[XsEpINoWybq0qnQN.htm](pathfinder-bestiary-items/XsEpINoWybq0qnQN.htm)|Jaws|Fauces|modificada|
|[xsknvVvLR4sL1ugG.htm](pathfinder-bestiary-items/xsknvVvLR4sL1ugG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XsMSamN905Cp8CUb.htm](pathfinder-bestiary-items/XsMSamN905Cp8CUb.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[XSsZeFbx7x1xTI4H.htm](pathfinder-bestiary-items/XSsZeFbx7x1xTI4H.htm)|Horn|Cuerno|modificada|
|[xswRs14UWcUn7b5S.htm](pathfinder-bestiary-items/xswRs14UWcUn7b5S.htm)|Buck|Encabritarse|modificada|
|[XTXRL6O388nfibXc.htm](pathfinder-bestiary-items/XTXRL6O388nfibXc.htm)|Grab|Agarrado|modificada|
|[xuSwO5OCFxK9XjiS.htm](pathfinder-bestiary-items/xuSwO5OCFxK9XjiS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xUwxwWrzgPri548T.htm](pathfinder-bestiary-items/xUwxwWrzgPri548T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Xv9vTEwIVBxtTfxZ.htm](pathfinder-bestiary-items/Xv9vTEwIVBxtTfxZ.htm)|Club|Club|modificada|
|[xVw1eoXAtuPKjE3x.htm](pathfinder-bestiary-items/xVw1eoXAtuPKjE3x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XwFOwvH7yRyD2Pn6.htm](pathfinder-bestiary-items/XwFOwvH7yRyD2Pn6.htm)|Regeneration 15 (Deactivated by Fire)|Regeneración 15 (Desactivado por Fuego)|modificada|
|[xwHcXDjVTSiAD1kB.htm](pathfinder-bestiary-items/xwHcXDjVTSiAD1kB.htm)|Wing Rebuff|Wing Rebuff|modificada|
|[xWNlGD6uekL83QTY.htm](pathfinder-bestiary-items/xWNlGD6uekL83QTY.htm)|Grab|Agarrado|modificada|
|[xX27SX9rrvhMXhIZ.htm](pathfinder-bestiary-items/xX27SX9rrvhMXhIZ.htm)|Change Shape|Change Shape|modificada|
|[xx2paGql0oW1VL3k.htm](pathfinder-bestiary-items/xx2paGql0oW1VL3k.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[xx3DbCuNgwUZWSha.htm](pathfinder-bestiary-items/xx3DbCuNgwUZWSha.htm)|Stinger|Aguijón|modificada|
|[xxGqls5RMZVYvmHf.htm](pathfinder-bestiary-items/xxGqls5RMZVYvmHf.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[XXiHxjwFvBeHoJUK.htm](pathfinder-bestiary-items/XXiHxjwFvBeHoJUK.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[XxKXmsn4wSfWyyXp.htm](pathfinder-bestiary-items/XxKXmsn4wSfWyyXp.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[xxUW9YkqVCI57nCo.htm](pathfinder-bestiary-items/xxUW9YkqVCI57nCo.htm)|Aqueous Fist|Puño acuoso|modificada|
|[xxW4UJbrz9uwTSQs.htm](pathfinder-bestiary-items/xxW4UJbrz9uwTSQs.htm)|Fangs|Colmillos|modificada|
|[xYlf7HmaIqlHhW8O.htm](pathfinder-bestiary-items/xYlf7HmaIqlHhW8O.htm)|Alchemist's Fire|Alchemist's Fire|modificada|
|[XYmhvkJxbC5BrIJG.htm](pathfinder-bestiary-items/XYmhvkJxbC5BrIJG.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[xYOVclWnoxsqGBCY.htm](pathfinder-bestiary-items/xYOVclWnoxsqGBCY.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[xYPB38g2HeJUfkGt.htm](pathfinder-bestiary-items/xYPB38g2HeJUfkGt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[XZcafYrLS6wcyUVG.htm](pathfinder-bestiary-items/XZcafYrLS6wcyUVG.htm)|Burn Eyes|Escozor de ojos|modificada|
|[xzuGv08xYVhfHdXW.htm](pathfinder-bestiary-items/xzuGv08xYVhfHdXW.htm)|Tail|Tail|modificada|
|[y03xyojSlEvE4nQh.htm](pathfinder-bestiary-items/y03xyojSlEvE4nQh.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[y0G6Hcirsc4uakIL.htm](pathfinder-bestiary-items/y0G6Hcirsc4uakIL.htm)|Shake It Off|Recupérate|modificada|
|[Y0hXaOxr6GsRFfoz.htm](pathfinder-bestiary-items/Y0hXaOxr6GsRFfoz.htm)|+1 Status to All Saves and AC vs. Evil Creatures|+1 situación a todas las salvaciones y CA contra criaturas malignas.|modificada|
|[y0ncwssgPJfLWMgt.htm](pathfinder-bestiary-items/y0ncwssgPJfLWMgt.htm)|Claw|Garra|modificada|
|[y0vt5wkSLNErceZF.htm](pathfinder-bestiary-items/y0vt5wkSLNErceZF.htm)|Freezing Blood|Sangre congelante|modificada|
|[Y198r9S2vz19SRbK.htm](pathfinder-bestiary-items/Y198r9S2vz19SRbK.htm)|Drag|Arrastre|modificada|
|[y1CWoANqqDtCbae7.htm](pathfinder-bestiary-items/y1CWoANqqDtCbae7.htm)|Frightful Presence|Frightful Presence|modificada|
|[Y1v59ZBvVAomH20U.htm](pathfinder-bestiary-items/Y1v59ZBvVAomH20U.htm)|Broad Swipe|Vaivén amplio|modificada|
|[y22Jw3B6gV1YXd2p.htm](pathfinder-bestiary-items/y22Jw3B6gV1YXd2p.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[Y2DikEsO4R95MHFo.htm](pathfinder-bestiary-items/Y2DikEsO4R95MHFo.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[Y2DYcAAQLTtolq5o.htm](pathfinder-bestiary-items/Y2DYcAAQLTtolq5o.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[Y2ewuQtDMN0O0bSp.htm](pathfinder-bestiary-items/Y2ewuQtDMN0O0bSp.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[y2ok0WY6i66HeQwf.htm](pathfinder-bestiary-items/y2ok0WY6i66HeQwf.htm)|Quick Bomber|Bombardero rápido|modificada|
|[Y36eYYeuCCJ8eXM0.htm](pathfinder-bestiary-items/Y36eYYeuCCJ8eXM0.htm)|Electricity Aura|Aura de Electricidad|modificada|
|[Y3XpZKuJPVRIacbx.htm](pathfinder-bestiary-items/Y3XpZKuJPVRIacbx.htm)|Jaws|Fauces|modificada|
|[Y44FxT7CY4R6SkdW.htm](pathfinder-bestiary-items/Y44FxT7CY4R6SkdW.htm)|Nymph's Beauty|Nymph's Beauty|modificada|
|[y4sYzueGiKy1QX4F.htm](pathfinder-bestiary-items/y4sYzueGiKy1QX4F.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[Y5momV4CWpuw5vhk.htm](pathfinder-bestiary-items/Y5momV4CWpuw5vhk.htm)|Shake It Off|Recupérate|modificada|
|[y5Na5QhVKW187D0K.htm](pathfinder-bestiary-items/y5Na5QhVKW187D0K.htm)|Mental Magic|Magia mental|modificada|
|[y6bETzshaXTYx4Wq.htm](pathfinder-bestiary-items/y6bETzshaXTYx4Wq.htm)|Shell Rake|Caparazón desgarrador|modificada|
|[Y6FgWopPB5QwPfF4.htm](pathfinder-bestiary-items/Y6FgWopPB5QwPfF4.htm)|Jaws|Fauces|modificada|
|[Y6X7el3YHox3syH1.htm](pathfinder-bestiary-items/Y6X7el3YHox3syH1.htm)|Disperse|Dispersarse|modificada|
|[y7q6NFl7HXFqMYWW.htm](pathfinder-bestiary-items/y7q6NFl7HXFqMYWW.htm)|Shortsword|Espada corta|modificada|
|[y8SLwokMHGSHm0S9.htm](pathfinder-bestiary-items/y8SLwokMHGSHm0S9.htm)|Jaws|Fauces|modificada|
|[y8ve2tORULDkXFSu.htm](pathfinder-bestiary-items/y8ve2tORULDkXFSu.htm)|Tail|Tail|modificada|
|[Y9YG23TX2Se8zYaw.htm](pathfinder-bestiary-items/Y9YG23TX2Se8zYaw.htm)|Push or Pull 10 feet|Empuja o tira 3 metros|modificada|
|[yA3EfugwTMvPqKVg.htm](pathfinder-bestiary-items/yA3EfugwTMvPqKVg.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ya7kmwsMyQI69FrG.htm](pathfinder-bestiary-items/ya7kmwsMyQI69FrG.htm)|Far Lobber|Lanzar a distancia|modificada|
|[YaAoatEoLAq9KaoR.htm](pathfinder-bestiary-items/YaAoatEoLAq9KaoR.htm)|Confounding Slam|Confounding Slam|modificada|
|[yAj0s7l7lPJLSE8C.htm](pathfinder-bestiary-items/yAj0s7l7lPJLSE8C.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[YAjAjrcDtGXjLBD7.htm](pathfinder-bestiary-items/YAjAjrcDtGXjLBD7.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[yaVkTfBp829HhTYC.htm](pathfinder-bestiary-items/yaVkTfBp829HhTYC.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[yB58WUUckQC7pMxy.htm](pathfinder-bestiary-items/yB58WUUckQC7pMxy.htm)|Grab|Agarrado|modificada|
|[YBLnDuhexFtEMrfc.htm](pathfinder-bestiary-items/YBLnDuhexFtEMrfc.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[yBOjYe8hHHRkhUyZ.htm](pathfinder-bestiary-items/yBOjYe8hHHRkhUyZ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[yC4YZ8E2MfUoLnzI.htm](pathfinder-bestiary-items/yC4YZ8E2MfUoLnzI.htm)|Rapier|Estoque|modificada|
|[YcE1Kg1c0njojA8b.htm](pathfinder-bestiary-items/YcE1Kg1c0njojA8b.htm)|Grab|Agarrado|modificada|
|[ydBVkEJ8CwcPFkPB.htm](pathfinder-bestiary-items/ydBVkEJ8CwcPFkPB.htm)|Blood Feast|Festín de sangre|modificada|
|[ydbvsWl1BoVazVZj.htm](pathfinder-bestiary-items/ydbvsWl1BoVazVZj.htm)|Sea Hag's Bargain|Trato con la saga marina maléfico|modificada|
|[YdD2kpKW2TNfcupL.htm](pathfinder-bestiary-items/YdD2kpKW2TNfcupL.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[yDIuApGqKF4695Oq.htm](pathfinder-bestiary-items/yDIuApGqKF4695Oq.htm)|Spear|Lanza|modificada|
|[ydL0czc2qFw4RnCW.htm](pathfinder-bestiary-items/ydL0czc2qFw4RnCW.htm)|Grab|Agarrado|modificada|
|[ydtRuz8pJkmMi4Uh.htm](pathfinder-bestiary-items/ydtRuz8pJkmMi4Uh.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[YdUFMjejXRVik0Ql.htm](pathfinder-bestiary-items/YdUFMjejXRVik0Ql.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[YdXTRnJYFdbi7JTh.htm](pathfinder-bestiary-items/YdXTRnJYFdbi7JTh.htm)|Assisted Mount|Montaje asistido|modificada|
|[YEfhznRCKL956Fzx.htm](pathfinder-bestiary-items/YEfhznRCKL956Fzx.htm)|Jaws|Fauces|modificada|
|[yEhFyHlybvFRrc0k.htm](pathfinder-bestiary-items/yEhFyHlybvFRrc0k.htm)|Lunging Strike|Golpe en acometida|modificada|
|[yeHohyJSYfiTwghO.htm](pathfinder-bestiary-items/yeHohyJSYfiTwghO.htm)|Battle Cry|Grito de guerra|modificada|
|[YeXnVtwRynKtKDVy.htm](pathfinder-bestiary-items/YeXnVtwRynKtKDVy.htm)|Defensive Assault|Asalto defensivo|modificada|
|[yEzDmYi6aYTiIA8Y.htm](pathfinder-bestiary-items/yEzDmYi6aYTiIA8Y.htm)|Ride the Wind|Montar el Viento|modificada|
|[yfHpnY9G1GCp6VVb.htm](pathfinder-bestiary-items/yfHpnY9G1GCp6VVb.htm)|Greatsword|Greatsword|modificada|
|[YfPJcSHzuZ8KIdff.htm](pathfinder-bestiary-items/YfPJcSHzuZ8KIdff.htm)|Druid Order Spells|Conjuros de orden druida|modificada|
|[yFqMLM1iygDqwiZC.htm](pathfinder-bestiary-items/yFqMLM1iygDqwiZC.htm)|Shift Form|Alterar forma|modificada|
|[yfZjkf1LqlK3U7HM.htm](pathfinder-bestiary-items/yfZjkf1LqlK3U7HM.htm)|Tusk|Tusk|modificada|
|[ygeIx11fATpKbQRh.htm](pathfinder-bestiary-items/ygeIx11fATpKbQRh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yGiXa7MzVzw31w7B.htm](pathfinder-bestiary-items/yGiXa7MzVzw31w7B.htm)|Shield Block|Bloquear con escudo|modificada|
|[Yh9EKYQqgpz6wFJx.htm](pathfinder-bestiary-items/Yh9EKYQqgpz6wFJx.htm)|Tail|Tail|modificada|
|[yHE4jDWkyfO5E7mM.htm](pathfinder-bestiary-items/yHE4jDWkyfO5E7mM.htm)|Constrict|Restringir|modificada|
|[YHERBUpSRZoR3Ox8.htm](pathfinder-bestiary-items/YHERBUpSRZoR3Ox8.htm)|Grab|Agarrado|modificada|
|[yhjUEa7dDonD0fSh.htm](pathfinder-bestiary-items/yhjUEa7dDonD0fSh.htm)|Grab|Agarrado|modificada|
|[Yi4K7PbKaZWlXxWj.htm](pathfinder-bestiary-items/Yi4K7PbKaZWlXxWj.htm)|Site Bound|Ligado a una ubicación|modificada|
|[YIHoSZN2nTp139Dg.htm](pathfinder-bestiary-items/YIHoSZN2nTp139Dg.htm)|Slink|Slink|modificada|
|[yIloVqSs48TDwk9P.htm](pathfinder-bestiary-items/yIloVqSs48TDwk9P.htm)|Frost Longspear|Gélida Longspear|modificada|
|[yk9pyF9iVrRoe307.htm](pathfinder-bestiary-items/yk9pyF9iVrRoe307.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[yKfR5JdxSC3YITNP.htm](pathfinder-bestiary-items/yKfR5JdxSC3YITNP.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ykjhYVy331qKqLO7.htm](pathfinder-bestiary-items/ykjhYVy331qKqLO7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ykw7UpzKukRpwEcU.htm](pathfinder-bestiary-items/ykw7UpzKukRpwEcU.htm)|Slow Aura|Aura de lentitud|modificada|
|[yLFnxHxGno0eHAjr.htm](pathfinder-bestiary-items/yLFnxHxGno0eHAjr.htm)|Energy Drain|Drenaje de Energía|modificada|
|[yLnwKltbJHYpn8qC.htm](pathfinder-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|Fauces|modificada|
|[yLq2DsGvZs0Z0aZN.htm](pathfinder-bestiary-items/yLq2DsGvZs0Z0aZN.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[yM2uUCjL6GUDVlF0.htm](pathfinder-bestiary-items/yM2uUCjL6GUDVlF0.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[ymEZhbkIeDn7RKGI.htm](pathfinder-bestiary-items/ymEZhbkIeDn7RKGI.htm)|Trap Soul|Atrapar alma|modificada|
|[YMj2YteKtdg79DoF.htm](pathfinder-bestiary-items/YMj2YteKtdg79DoF.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[YMwe0HIrPrtpRdsx.htm](pathfinder-bestiary-items/YMwe0HIrPrtpRdsx.htm)|Negative Healing|Curación negativa|modificada|
|[YNmQSSXzIEUJ3yrh.htm](pathfinder-bestiary-items/YNmQSSXzIEUJ3yrh.htm)|Frightful Presence|Frightful Presence|modificada|
|[yntcM5gKBjUuNen1.htm](pathfinder-bestiary-items/yntcM5gKBjUuNen1.htm)|Javelin|Javelin|modificada|
|[Yo23Art466hO6dEO.htm](pathfinder-bestiary-items/Yo23Art466hO6dEO.htm)|Bomb|Bomba|modificada|
|[YoEkR6sBza4UBMYy.htm](pathfinder-bestiary-items/YoEkR6sBza4UBMYy.htm)|Essence Drain|Drenar esencia|modificada|
|[YOFEFZQFoVYO9alj.htm](pathfinder-bestiary-items/YOFEFZQFoVYO9alj.htm)|Catch Rock|Atrapar roca|modificada|
|[YoKSGUjwXmt7kxnq.htm](pathfinder-bestiary-items/YoKSGUjwXmt7kxnq.htm)|Curse of Death|Maldición de la Muerte|modificada|
|[YOkyQZ6amn9r15G3.htm](pathfinder-bestiary-items/YOkyQZ6amn9r15G3.htm)|Maul|Zarpazo doble|modificada|
|[yoSPlCHmFaXCr9Th.htm](pathfinder-bestiary-items/yoSPlCHmFaXCr9Th.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yOxa8PKiBE6PbRBA.htm](pathfinder-bestiary-items/yOxa8PKiBE6PbRBA.htm)|Body|Cuerpo|modificada|
|[yp7FDjO7715BHNPw.htm](pathfinder-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YpgcfJpD1jMhcu1X.htm](pathfinder-bestiary-items/YpgcfJpD1jMhcu1X.htm)|Devil Shaping|Moldear diablos|modificada|
|[ypKdaBzYAIDgW2KT.htm](pathfinder-bestiary-items/ypKdaBzYAIDgW2KT.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[yPMc4avP8IHjveQY.htm](pathfinder-bestiary-items/yPMc4avP8IHjveQY.htm)|Constant Spells|Constant Spells|modificada|
|[yQ0WKMlep02TPvLi.htm](pathfinder-bestiary-items/yQ0WKMlep02TPvLi.htm)|Change Shape|Change Shape|modificada|
|[YqWqB6EYv3yk0LPh.htm](pathfinder-bestiary-items/YqWqB6EYv3yk0LPh.htm)|Frightful Moan|Frightful Moan|modificada|
|[YR4Hu0QGXr4QHQyq.htm](pathfinder-bestiary-items/YR4Hu0QGXr4QHQyq.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[yrctbhF7nTGEPJSK.htm](pathfinder-bestiary-items/yrctbhF7nTGEPJSK.htm)|Longspear|Longspear|modificada|
|[yrfY15KIhcsy07PS.htm](pathfinder-bestiary-items/yrfY15KIhcsy07PS.htm)|Pseudopod|Pseudópodo|modificada|
|[yrKXLmbaekza05cd.htm](pathfinder-bestiary-items/yrKXLmbaekza05cd.htm)|Improved Push 10 feet|Empuje mejorado 10 pies|modificada|
|[YS1lNceoORGTpa2L.htm](pathfinder-bestiary-items/YS1lNceoORGTpa2L.htm)|Infused Items|Equipos infundidos|modificada|
|[ys1ZCX9tF6g75EH0.htm](pathfinder-bestiary-items/ys1ZCX9tF6g75EH0.htm)|Tail|Tail|modificada|
|[ySh7mxtzIvc9onNT.htm](pathfinder-bestiary-items/ySh7mxtzIvc9onNT.htm)|Tail|Tail|modificada|
|[YSjgR6s49HwnqEi6.htm](pathfinder-bestiary-items/YSjgR6s49HwnqEi6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ysMN796BHlk39kv8.htm](pathfinder-bestiary-items/ysMN796BHlk39kv8.htm)|Frightful Presence|Frightful Presence|modificada|
|[ySnTknzJunTGUgYX.htm](pathfinder-bestiary-items/ySnTknzJunTGUgYX.htm)|Tail|Tail|modificada|
|[YsodmcYoo9fl86Bj.htm](pathfinder-bestiary-items/YsodmcYoo9fl86Bj.htm)|Claw|Garra|modificada|
|[YsSqsOvXgRemLPt6.htm](pathfinder-bestiary-items/YsSqsOvXgRemLPt6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YTDggskczm13sYCy.htm](pathfinder-bestiary-items/YTDggskczm13sYCy.htm)|Scent (Imprecise) 80 feet|Olor (Impreciso) 80 pies|modificada|
|[Ytry4aEXlZIFroqA.htm](pathfinder-bestiary-items/Ytry4aEXlZIFroqA.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[YttgSizfr7L607th.htm](pathfinder-bestiary-items/YttgSizfr7L607th.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[yTXiHzmRavI1BmKJ.htm](pathfinder-bestiary-items/yTXiHzmRavI1BmKJ.htm)|Rend|Rasgadura|modificada|
|[YuejnaqH1UUSFEtm.htm](pathfinder-bestiary-items/YuejnaqH1UUSFEtm.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[yUMh1Oa8g3zZ6utm.htm](pathfinder-bestiary-items/yUMh1Oa8g3zZ6utm.htm)|Wide Swing|Barrido ancho|modificada|
|[yuSihDZaTWtIZbzU.htm](pathfinder-bestiary-items/yuSihDZaTWtIZbzU.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[yuu0RBqGlI0PYbGU.htm](pathfinder-bestiary-items/yuu0RBqGlI0PYbGU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YuzoCdKpCzawAPTJ.htm](pathfinder-bestiary-items/YuzoCdKpCzawAPTJ.htm)|Spike|Spike|modificada|
|[yvHOZTfOKMN7JNu7.htm](pathfinder-bestiary-items/yvHOZTfOKMN7JNu7.htm)|Hunt Prey|Perseguir presa|modificada|
|[YW6KUycT9oCKgOAo.htm](pathfinder-bestiary-items/YW6KUycT9oCKgOAo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YWBW2zZbIggEH4rW.htm](pathfinder-bestiary-items/YWBW2zZbIggEH4rW.htm)|Knockdown|Derribo|modificada|
|[YwidZ8QmOJ85iLcw.htm](pathfinder-bestiary-items/YwidZ8QmOJ85iLcw.htm)|Blood Drain|Drenar sangre|modificada|
|[yWipQYXVRovUBD3A.htm](pathfinder-bestiary-items/yWipQYXVRovUBD3A.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yWxsd7SGgC3z0IoN.htm](pathfinder-bestiary-items/yWxsd7SGgC3z0IoN.htm)|Twisting Tail|Enredar con la cola|modificada|
|[YXiHAcR16OtpzApO.htm](pathfinder-bestiary-items/YXiHAcR16OtpzApO.htm)|Speed Surge|Arranque de velocidad|modificada|
|[yxQPTCjmsUlWSm7n.htm](pathfinder-bestiary-items/yxQPTCjmsUlWSm7n.htm)|Revert Form|Revertir forma|modificada|
|[YXsM6AcguDWAwVIt.htm](pathfinder-bestiary-items/YXsM6AcguDWAwVIt.htm)|Pseudopod|Pseudópodo|modificada|
|[yYFISOz912MYdEDd.htm](pathfinder-bestiary-items/yYFISOz912MYdEDd.htm)|Breath Weapon|Breath Weapon|modificada|
|[yyKQP4J4yplKGLL0.htm](pathfinder-bestiary-items/yyKQP4J4yplKGLL0.htm)|Frightful Presence|Frightful Presence|modificada|
|[YYsOPt3KeLlEyyOR.htm](pathfinder-bestiary-items/YYsOPt3KeLlEyyOR.htm)|Pincer|Pinza|modificada|
|[yYXID6QLMJVxA2xM.htm](pathfinder-bestiary-items/yYXID6QLMJVxA2xM.htm)|Negative Healing|Curación negativa|modificada|
|[YyxKJRRXl2GdUfTt.htm](pathfinder-bestiary-items/YyxKJRRXl2GdUfTt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yzX7FLSgvEvMeqQY.htm](pathfinder-bestiary-items/yzX7FLSgvEvMeqQY.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[YZxhBg9R5m8ydWm1.htm](pathfinder-bestiary-items/YZxhBg9R5m8ydWm1.htm)|Claw|Garra|modificada|
|[z07apLapCrVoCKOc.htm](pathfinder-bestiary-items/z07apLapCrVoCKOc.htm)|Dagger|Daga|modificada|
|[z0QT6nvmIHbeoGHa.htm](pathfinder-bestiary-items/z0QT6nvmIHbeoGHa.htm)|Jaws|Fauces|modificada|
|[z0XDoZaPAzbT1buS.htm](pathfinder-bestiary-items/z0XDoZaPAzbT1buS.htm)|Tentacle|Tentáculo|modificada|
|[Z0xjQ4vMpyymQS0z.htm](pathfinder-bestiary-items/Z0xjQ4vMpyymQS0z.htm)|Fist|Puño|modificada|
|[z1Wnid4gCPgKMoV8.htm](pathfinder-bestiary-items/z1Wnid4gCPgKMoV8.htm)|Water Healing|Curación de agua|modificada|
|[z42GiliLLdEVPTiX.htm](pathfinder-bestiary-items/z42GiliLLdEVPTiX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[z4Mouf0sgt0pO2Vz.htm](pathfinder-bestiary-items/z4Mouf0sgt0pO2Vz.htm)|Avenging Bite|Muerdemuerde Vengador|modificada|
|[Z4NC0zq5T9vj6Gu8.htm](pathfinder-bestiary-items/Z4NC0zq5T9vj6Gu8.htm)|Brand of the Impenitent|Marca del impenitente|modificada|
|[z4WS8tZWux7BE3D7.htm](pathfinder-bestiary-items/z4WS8tZWux7BE3D7.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[Z5lrTod89eL9ROFq.htm](pathfinder-bestiary-items/Z5lrTod89eL9ROFq.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[Z6GLjJXLZ7y8eIDF.htm](pathfinder-bestiary-items/Z6GLjJXLZ7y8eIDF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Z78ylUwriAT6RClH.htm](pathfinder-bestiary-items/Z78ylUwriAT6RClH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[z7GewXbjOdMCjk6w.htm](pathfinder-bestiary-items/z7GewXbjOdMCjk6w.htm)|Holy Greatsword|Sagrada Gran Espada|modificada|
|[z8bl09rqF4yCJDy4.htm](pathfinder-bestiary-items/z8bl09rqF4yCJDy4.htm)|Terrifying Croak|Aterrador Croak|modificada|
|[z8Vh23jCIh7iroYs.htm](pathfinder-bestiary-items/z8Vh23jCIh7iroYs.htm)|Swarming Stings|Picadura de enjambre|modificada|
|[z9bTah65DoASEDhP.htm](pathfinder-bestiary-items/z9bTah65DoASEDhP.htm)|Shortsword|Espada corta|modificada|
|[z9EzpXZgqR4vRkyV.htm](pathfinder-bestiary-items/z9EzpXZgqR4vRkyV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZA1vPfnoLP9PkAG2.htm](pathfinder-bestiary-items/ZA1vPfnoLP9PkAG2.htm)|Tremorsense (Imprecise) 10 feet|Sentido del Temblor (Impreciso) 10 pies|modificada|
|[zadQdUq4eeiDSHSq.htm](pathfinder-bestiary-items/zadQdUq4eeiDSHSq.htm)|Shadow Spawn|Sombra Spawn|modificada|
|[zb5Fwjfb2emTLKdL.htm](pathfinder-bestiary-items/zb5Fwjfb2emTLKdL.htm)|Grab|Agarrado|modificada|
|[zbg9eaOONakDkuma.htm](pathfinder-bestiary-items/zbg9eaOONakDkuma.htm)|Aquatic Opportunity (Special)|Oportunidad Acuática (Especial)|modificada|
|[Zc2k22zs45gbuybT.htm](pathfinder-bestiary-items/Zc2k22zs45gbuybT.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Zc5P1zPH0GL9ZDWH.htm](pathfinder-bestiary-items/Zc5P1zPH0GL9ZDWH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zCkhc1TIl4lLnx0Y.htm](pathfinder-bestiary-items/zCkhc1TIl4lLnx0Y.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[ZCRCrZQCOFtODRBG.htm](pathfinder-bestiary-items/ZCRCrZQCOFtODRBG.htm)|+2 Status to All Saves vs. Auditory and Visual|+2 situación a todos los guardados vs auditiva y visual.|modificada|
|[Zcyxz8R468nruWI9.htm](pathfinder-bestiary-items/Zcyxz8R468nruWI9.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[zczHtXzGMeNhSQGF.htm](pathfinder-bestiary-items/zczHtXzGMeNhSQGF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zD3l2lGfHmnHF8iq.htm](pathfinder-bestiary-items/zD3l2lGfHmnHF8iq.htm)|Drain Life|Drenar Vida|modificada|
|[zDDzvsB3OrJU9QXn.htm](pathfinder-bestiary-items/zDDzvsB3OrJU9QXn.htm)|Vigorous Shake|Sacudida vigorosa|modificada|
|[ZDJkjnIor7qxsiSU.htm](pathfinder-bestiary-items/ZDJkjnIor7qxsiSU.htm)|Strangle|Estrangular|modificada|
|[ZeFESJTxEPFbLeiy.htm](pathfinder-bestiary-items/ZeFESJTxEPFbLeiy.htm)|Negative Healing|Curación negativa|modificada|
|[zEMUKIwgy7ajyUpa.htm](pathfinder-bestiary-items/zEMUKIwgy7ajyUpa.htm)|Claw|Garra|modificada|
|[zeopy130t1G5DRUp.htm](pathfinder-bestiary-items/zeopy130t1G5DRUp.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[zeWfNg7lIyopCbmn.htm](pathfinder-bestiary-items/zeWfNg7lIyopCbmn.htm)|Fist|Puño|modificada|
|[zEz9gGlpNBUnyF2Y.htm](pathfinder-bestiary-items/zEz9gGlpNBUnyF2Y.htm)|Change Shape|Change Shape|modificada|
|[zEZW1atU8KQCSsNi.htm](pathfinder-bestiary-items/zEZW1atU8KQCSsNi.htm)|Thrash|Sacudirse|modificada|
|[ZfDN0qrqMsIVdq7W.htm](pathfinder-bestiary-items/ZfDN0qrqMsIVdq7W.htm)|Improved Grab|Agarrado mejorado|modificada|
|[zFNrz9di2EnI53hz.htm](pathfinder-bestiary-items/zFNrz9di2EnI53hz.htm)|Pack Attack|Ataque en manada|modificada|
|[ZfNWabALFufsHCUi.htm](pathfinder-bestiary-items/ZfNWabALFufsHCUi.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[ZfVwcTmrbWAVaOCX.htm](pathfinder-bestiary-items/ZfVwcTmrbWAVaOCX.htm)|Improved Grab|Agarrado mejorado|modificada|
|[zGEOz9aVguJujbSi.htm](pathfinder-bestiary-items/zGEOz9aVguJujbSi.htm)|Mutations|Mutaciones|modificada|
|[ZgpdfTBdMYYRwcfP.htm](pathfinder-bestiary-items/ZgpdfTBdMYYRwcfP.htm)|Web|Telara|modificada|
|[zgR8Rwk8qwhkLVy6.htm](pathfinder-bestiary-items/zgR8Rwk8qwhkLVy6.htm)|Death Throes|Estertores de muerte|modificada|
|[zGWjXyKd5cavgSYJ.htm](pathfinder-bestiary-items/zGWjXyKd5cavgSYJ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[zGXZKhk95VPsSdKN.htm](pathfinder-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|Serpientes mordedoras|modificada|
|[Zh4UT6i2zsYug3c7.htm](pathfinder-bestiary-items/Zh4UT6i2zsYug3c7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zHIioB6mgFk3W6Uz.htm](pathfinder-bestiary-items/zHIioB6mgFk3W6Uz.htm)|Blood Frenzy|Frenesí de Sangre|modificada|
|[zHL2E4bkH3vVvqSD.htm](pathfinder-bestiary-items/zHL2E4bkH3vVvqSD.htm)|Change Size|Cambiar tama|modificada|
|[zHzxiV7sS8c8LmWL.htm](pathfinder-bestiary-items/zHzxiV7sS8c8LmWL.htm)|Fist|Puño|modificada|
|[Zi3UHzrDTQBgI0SW.htm](pathfinder-bestiary-items/Zi3UHzrDTQBgI0SW.htm)|Telekinetic Object (Piercing)|Telekinetic Object (Piercing)|modificada|
|[ziFuQqCKpWrgkyes.htm](pathfinder-bestiary-items/ziFuQqCKpWrgkyes.htm)|Rock|Roca|modificada|
|[zIgoZ6UuenqL0UvX.htm](pathfinder-bestiary-items/zIgoZ6UuenqL0UvX.htm)|Dragon Heat|Calor dracónico|modificada|
|[ziSz2omKZDCD4A0e.htm](pathfinder-bestiary-items/ziSz2omKZDCD4A0e.htm)|Claw|Garra|modificada|
|[zIyme0AGuphXNgSa.htm](pathfinder-bestiary-items/zIyme0AGuphXNgSa.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[zK2aR7Xwx6ctIIxv.htm](pathfinder-bestiary-items/zK2aR7Xwx6ctIIxv.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ZKFDBSkBr0B38G7d.htm](pathfinder-bestiary-items/ZKFDBSkBr0B38G7d.htm)|Constant Spells|Constant Spells|modificada|
|[ZktMGhL2mkH1daBn.htm](pathfinder-bestiary-items/ZktMGhL2mkH1daBn.htm)|Ice Climb|Trepar por el hielo|modificada|
|[zKwj4CDPZ698YJci.htm](pathfinder-bestiary-items/zKwj4CDPZ698YJci.htm)|Hoof|Hoof|modificada|
|[zl24K9E3qEAfVXrR.htm](pathfinder-bestiary-items/zl24K9E3qEAfVXrR.htm)|Turn to Mist|Turn to Mist|modificada|
|[zlPeMmaUBpOYNjEw.htm](pathfinder-bestiary-items/zlPeMmaUBpOYNjEw.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[zlRfRzTTm14EvZIG.htm](pathfinder-bestiary-items/zlRfRzTTm14EvZIG.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[zLUOcpj85OOnwvRh.htm](pathfinder-bestiary-items/zLUOcpj85OOnwvRh.htm)|Curse of Boiling Blood|Maldición de sangre hirviente|modificada|
|[ZlxZ2pDpBDTkPZ6M.htm](pathfinder-bestiary-items/ZlxZ2pDpBDTkPZ6M.htm)|Sling|Sling|modificada|
|[zlYuXPKypj1updU4.htm](pathfinder-bestiary-items/zlYuXPKypj1updU4.htm)|Jaws|Fauces|modificada|
|[zm5hc0YuNjjknTeP.htm](pathfinder-bestiary-items/zm5hc0YuNjjknTeP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Zn0AZHlLWAhHykEg.htm](pathfinder-bestiary-items/Zn0AZHlLWAhHykEg.htm)|Claw|Garra|modificada|
|[zNcTKoBJlpFDMTec.htm](pathfinder-bestiary-items/zNcTKoBJlpFDMTec.htm)|Berserk Slam|Porrazo bersérker|modificada|
|[ZNWKdUJvc61evvBc.htm](pathfinder-bestiary-items/ZNWKdUJvc61evvBc.htm)|Wide Swing|Barrido ancho|modificada|
|[zoTuvqovtFZqbEp6.htm](pathfinder-bestiary-items/zoTuvqovtFZqbEp6.htm)|Adaptive Strike|Golpe Adaptativo|modificada|
|[zOzr7HCfJlCH6I7z.htm](pathfinder-bestiary-items/zOzr7HCfJlCH6I7z.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[ZP1PiehJLtBoKijM.htm](pathfinder-bestiary-items/ZP1PiehJLtBoKijM.htm)|Shortbow|Arco corto|modificada|
|[ZP2IXAQnslTJ2Yfq.htm](pathfinder-bestiary-items/ZP2IXAQnslTJ2Yfq.htm)|Jaws|Fauces|modificada|
|[Zp7ozjV5uBLnBo41.htm](pathfinder-bestiary-items/Zp7ozjV5uBLnBo41.htm)|Final Death|Muerte definitiva|modificada|
|[ZpBdjMgBgOgkeGA8.htm](pathfinder-bestiary-items/ZpBdjMgBgOgkeGA8.htm)|Claw|Garra|modificada|
|[Zpn8XChRDGw65K4B.htm](pathfinder-bestiary-items/Zpn8XChRDGw65K4B.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[zpUaHpTkfVbbw7sX.htm](pathfinder-bestiary-items/zpUaHpTkfVbbw7sX.htm)|Overpowering Jaws|Fauces abrumadoras|modificada|
|[ZpVuzT55hKotfiRz.htm](pathfinder-bestiary-items/ZpVuzT55hKotfiRz.htm)|Throw Rock|Arrojar roca|modificada|
|[zQRbKmxdzecknTDu.htm](pathfinder-bestiary-items/zQRbKmxdzecknTDu.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ZQs5C40PhQ6fLEOd.htm](pathfinder-bestiary-items/ZQs5C40PhQ6fLEOd.htm)|Jaws|Fauces|modificada|
|[zQUhdB7kvudfGv5Z.htm](pathfinder-bestiary-items/zQUhdB7kvudfGv5Z.htm)|Reposition|Reposicionar|modificada|
|[zqXzm6YAzfHGm8sq.htm](pathfinder-bestiary-items/zqXzm6YAzfHGm8sq.htm)|Goblin Song|Canción de goblin|modificada|
|[ZQY7WcFFZtddDBnh.htm](pathfinder-bestiary-items/ZQY7WcFFZtddDBnh.htm)|Precision Edge|Borde de precisión|modificada|
|[ZREK2bLfJEVr6kpv.htm](pathfinder-bestiary-items/ZREK2bLfJEVr6kpv.htm)|Hatchet|Hacha|modificada|
|[zRI7pbJvbWWPBJQr.htm](pathfinder-bestiary-items/zRI7pbJvbWWPBJQr.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZrnTtQzYoMMf79j0.htm](pathfinder-bestiary-items/ZrnTtQzYoMMf79j0.htm)|Foot|Pie|modificada|
|[ZRU4JYMsdXENmG5Q.htm](pathfinder-bestiary-items/ZRU4JYMsdXENmG5Q.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ZrxAcxdGyfkm2kZf.htm](pathfinder-bestiary-items/ZrxAcxdGyfkm2kZf.htm)|Pseudopod|Pseudópodo|modificada|
|[ZSlJL6w6J5g6kFBN.htm](pathfinder-bestiary-items/ZSlJL6w6J5g6kFBN.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[zSN2IyI2qyW456x3.htm](pathfinder-bestiary-items/zSN2IyI2qyW456x3.htm)|Longspear|Longspear|modificada|
|[zSXEX1BIW9Kopq1o.htm](pathfinder-bestiary-items/zSXEX1BIW9Kopq1o.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ztAIkCm5tn8PbtUr.htm](pathfinder-bestiary-items/ztAIkCm5tn8PbtUr.htm)|Claw|Garra|modificada|
|[ZTPzGQcaC9zUdjD6.htm](pathfinder-bestiary-items/ZTPzGQcaC9zUdjD6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ztr88bB5WvLP8Ykt.htm](pathfinder-bestiary-items/ztr88bB5WvLP8Ykt.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[ZTtP9ifJTYHBXH8d.htm](pathfinder-bestiary-items/ZTtP9ifJTYHBXH8d.htm)|Hatchet|Hacha|modificada|
|[ZtVK4wU2SnZ0H3zl.htm](pathfinder-bestiary-items/ZtVK4wU2SnZ0H3zl.htm)|Flaming Coal|Carbón Flamígera|modificada|
|[zTWAKdDMdVslc62l.htm](pathfinder-bestiary-items/zTWAKdDMdVslc62l.htm)|Grab|Agarrado|modificada|
|[ztyTHwNIKhCgnOc2.htm](pathfinder-bestiary-items/ztyTHwNIKhCgnOc2.htm)|Bone Javelin|Jabalina de Hueso|modificada|
|[zu0tOUwHfZJlaGQq.htm](pathfinder-bestiary-items/zu0tOUwHfZJlaGQq.htm)|Seedpod|Vaina de semillas|modificada|
|[ZUBqUDHdZhSOTiQD.htm](pathfinder-bestiary-items/ZUBqUDHdZhSOTiQD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zucHNDwfKldM9TpE.htm](pathfinder-bestiary-items/zucHNDwfKldM9TpE.htm)|Jaws|Fauces|modificada|
|[ZuFNPk5MmcRIzGRR.htm](pathfinder-bestiary-items/ZuFNPk5MmcRIzGRR.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[zuuG6E7754pHBLsE.htm](pathfinder-bestiary-items/zuuG6E7754pHBLsE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZuxcOScsfkdklfi8.htm](pathfinder-bestiary-items/ZuxcOScsfkdklfi8.htm)|Breath of the Sea|Hálito del mar|modificada|
|[zUyuIoJBzk4LNvK2.htm](pathfinder-bestiary-items/zUyuIoJBzk4LNvK2.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[zwfFiuZdh5gkdUpi.htm](pathfinder-bestiary-items/zwfFiuZdh5gkdUpi.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[ZWzIyAxYv8ApHu3K.htm](pathfinder-bestiary-items/ZWzIyAxYv8ApHu3K.htm)|Shield Push|Escudo Push|modificada|
|[zxbJYDbwhz45mMSX.htm](pathfinder-bestiary-items/zxbJYDbwhz45mMSX.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[zXDFVZKb5GlrVnod.htm](pathfinder-bestiary-items/zXDFVZKb5GlrVnod.htm)|Dust|Polvo|modificada|
|[ZxIVdGLdVzJ1RVHi.htm](pathfinder-bestiary-items/ZxIVdGLdVzJ1RVHi.htm)|Mangling Rend|Desgarro mutilador|modificada|
|[zxL9Jy3Yap38BQBi.htm](pathfinder-bestiary-items/zxL9Jy3Yap38BQBi.htm)|Manipulate Flames|Manipular llamasígera.|modificada|
|[ZXotvZV2fYBkhfUy.htm](pathfinder-bestiary-items/ZXotvZV2fYBkhfUy.htm)|Skewer|Ensartar|modificada|
|[zxrTBcJcqEnRKZyA.htm](pathfinder-bestiary-items/zxrTBcJcqEnRKZyA.htm)|Fist|Puño|modificada|
|[zYGuLe2x3OTlMc06.htm](pathfinder-bestiary-items/zYGuLe2x3OTlMc06.htm)|Jaunt|Jaunt|modificada|
|[zyK8S6224cjBrzpM.htm](pathfinder-bestiary-items/zyK8S6224cjBrzpM.htm)|Whip Reposition|Reposición con látigo|modificada|
|[ZyTjl37PC8e0rtsy.htm](pathfinder-bestiary-items/ZyTjl37PC8e0rtsy.htm)|Rending Mandibles|Mandíbulas rasgaarmaduras|modificada|
|[zyYihJ8eYGtYRh9u.htm](pathfinder-bestiary-items/zyYihJ8eYGtYRh9u.htm)|Bastard Sword|Espada Bastarda|modificada|
|[zZ7YQvZHZDePNYfh.htm](pathfinder-bestiary-items/zZ7YQvZHZDePNYfh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zzGNgcKnTWZFH3Jk.htm](pathfinder-bestiary-items/zzGNgcKnTWZFH3Jk.htm)|Warhammer|Warhammer|modificada|
|[ZZi8bWk8Oz1nqeYY.htm](pathfinder-bestiary-items/ZZi8bWk8Oz1nqeYY.htm)|Claw|Garra|modificada|
|[zzzLK4jcgGesR9Yt.htm](pathfinder-bestiary-items/zzzLK4jcgGesR9Yt.htm)|Darkvision|Visión en la oscuridad|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0M6FpfseLeWc1yHA.htm](pathfinder-bestiary-items/0M6FpfseLeWc1yHA.htm)|Athletics|vacía|
|[0MG0h9aFc2pBo8d3.htm](pathfinder-bestiary-items/0MG0h9aFc2pBo8d3.htm)|Deception|vacía|
|[1h9v3vILKQYAVpKr.htm](pathfinder-bestiary-items/1h9v3vILKQYAVpKr.htm)|Dwelling Lore (applies to the place the ghost is bound to)|vacía|
|[1pcQlMiq7wKREOZC.htm](pathfinder-bestiary-items/1pcQlMiq7wKREOZC.htm)|Crafting|vacía|
|[22HJYuw6ET85gOrj.htm](pathfinder-bestiary-items/22HJYuw6ET85gOrj.htm)|Athletics|vacía|
|[40tsiDz5txNIXOqI.htm](pathfinder-bestiary-items/40tsiDz5txNIXOqI.htm)|Athletics|vacía|
|[4gDOqdD5endao3AS.htm](pathfinder-bestiary-items/4gDOqdD5endao3AS.htm)|Survival|vacía|
|[4jjv1SXoMY3MpGjE.htm](pathfinder-bestiary-items/4jjv1SXoMY3MpGjE.htm)|Heaven Lore|vacía|
|[4uhmqqCnN4EAqpVy.htm](pathfinder-bestiary-items/4uhmqqCnN4EAqpVy.htm)|Rock|vacía|
|[5E3LlWq9tofQ39za.htm](pathfinder-bestiary-items/5E3LlWq9tofQ39za.htm)|Rock|vacía|
|[5HEJugVADSVpGXgp.htm](pathfinder-bestiary-items/5HEJugVADSVpGXgp.htm)|Athletics|vacía|
|[5kSNwp8DyNYR62vW.htm](pathfinder-bestiary-items/5kSNwp8DyNYR62vW.htm)|Stealth|vacía|
|[5uceub19IdfqJ0Ze.htm](pathfinder-bestiary-items/5uceub19IdfqJ0Ze.htm)|Warfare Lore|vacía|
|[6dKQD6Kq8sV07jw6.htm](pathfinder-bestiary-items/6dKQD6Kq8sV07jw6.htm)|Abyss Lore|vacía|
|[6fjCT3m2EESdN8jy.htm](pathfinder-bestiary-items/6fjCT3m2EESdN8jy.htm)|Sack for Holding Rocks|vacía|
|[6GOUhOX6Gf4iNJ4w.htm](pathfinder-bestiary-items/6GOUhOX6Gf4iNJ4w.htm)|Athletics|vacía|
|[6qvzXeFuSeJ64OSj.htm](pathfinder-bestiary-items/6qvzXeFuSeJ64OSj.htm)|Axis Lore|vacía|
|[7ABEQLk7SMoCzea5.htm](pathfinder-bestiary-items/7ABEQLk7SMoCzea5.htm)|Stealth|vacía|
|[7jkFukrBwG3G4TQw.htm](pathfinder-bestiary-items/7jkFukrBwG3G4TQw.htm)|Boneyard Lore|vacía|
|[86Ad9HMl7dZTCt1k.htm](pathfinder-bestiary-items/86Ad9HMl7dZTCt1k.htm)|Infused Reagents|vacía|
|[8CznKBBokxBMcUVf.htm](pathfinder-bestiary-items/8CznKBBokxBMcUVf.htm)|Warfare Lore|vacía|
|[8Ga9mRyTf82SeeOx.htm](pathfinder-bestiary-items/8Ga9mRyTf82SeeOx.htm)|Stealth|vacía|
|[8lgMCjFFA7C5mDWQ.htm](pathfinder-bestiary-items/8lgMCjFFA7C5mDWQ.htm)|Athletics|vacía|
|[9K08qcNrvT5fefj0.htm](pathfinder-bestiary-items/9K08qcNrvT5fefj0.htm)|Iron Boots|vacía|
|[atS4s8EW4fvN6Zz5.htm](pathfinder-bestiary-items/atS4s8EW4fvN6Zz5.htm)|Large Bark Shield|vacía|
|[b8MFwyWeJ5rqO3Vk.htm](pathfinder-bestiary-items/b8MFwyWeJ5rqO3Vk.htm)|Stealth|vacía|
|[BmNxcRZobjNuBKwb.htm](pathfinder-bestiary-items/BmNxcRZobjNuBKwb.htm)|Survival|vacía|
|[C0e1jORAz4OfKt0g.htm](pathfinder-bestiary-items/C0e1jORAz4OfKt0g.htm)|Axis Lore|vacía|
|[CAOop96qtFJsPw1k.htm](pathfinder-bestiary-items/CAOop96qtFJsPw1k.htm)|Athletics|vacía|
|[cBVzpBcezluNMdq5.htm](pathfinder-bestiary-items/cBVzpBcezluNMdq5.htm)|Survival|vacía|
|[CfnwLhzQDT3H69j1.htm](pathfinder-bestiary-items/CfnwLhzQDT3H69j1.htm)|Warfare Lore|vacía|
|[cOLbXO3zU7Pc0b9h.htm](pathfinder-bestiary-items/cOLbXO3zU7Pc0b9h.htm)|Stealth|vacía|
|[cszQAZpBpMXrkQrA.htm](pathfinder-bestiary-items/cszQAZpBpMXrkQrA.htm)|Elemental Lore|vacía|
|[cuoRWPoyJkfPoJ58.htm](pathfinder-bestiary-items/cuoRWPoyJkfPoJ58.htm)|Stealth|vacía|
|[D8YVoU1kV2jo3cyx.htm](pathfinder-bestiary-items/D8YVoU1kV2jo3cyx.htm)|Fortune-Telling Lore|vacía|
|[dknkWfjE6V1wz9bK.htm](pathfinder-bestiary-items/dknkWfjE6V1wz9bK.htm)|Sack for Holding Rocks|vacía|
|[DMO6UkPLSSsCSybX.htm](pathfinder-bestiary-items/DMO6UkPLSSsCSybX.htm)|Athletics|vacía|
|[epm1yalbnFyUfJMs.htm](pathfinder-bestiary-items/epm1yalbnFyUfJMs.htm)|Athletics|vacía|
|[ErH5biSmkMOD58yj.htm](pathfinder-bestiary-items/ErH5biSmkMOD58yj.htm)|Nature|vacía|
|[fLfqqrUuzDy1YKfH.htm](pathfinder-bestiary-items/fLfqqrUuzDy1YKfH.htm)|Survival|vacía|
|[FSP3kFRIx0dZ9JVj.htm](pathfinder-bestiary-items/FSP3kFRIx0dZ9JVj.htm)|Rock|vacía|
|[fYdweFlxcKABfwpC.htm](pathfinder-bestiary-items/fYdweFlxcKABfwpC.htm)|Rock|vacía|
|[GauPueVChScKx1eH.htm](pathfinder-bestiary-items/GauPueVChScKx1eH.htm)|Survival|vacía|
|[GQ7IJIi6ResKuBS9.htm](pathfinder-bestiary-items/GQ7IJIi6ResKuBS9.htm)|Stealth|vacía|
|[GRUZiIR1Uu91DaoY.htm](pathfinder-bestiary-items/GRUZiIR1Uu91DaoY.htm)|Stealth|vacía|
|[GXAVCqQeEroRFdal.htm](pathfinder-bestiary-items/GXAVCqQeEroRFdal.htm)|Crafting|vacía|
|[gZq9ezLP84ZJ7duZ.htm](pathfinder-bestiary-items/gZq9ezLP84ZJ7duZ.htm)|Athletics|vacía|
|[h5kqITuSgXelPoIz.htm](pathfinder-bestiary-items/h5kqITuSgXelPoIz.htm)|Axis Lore|vacía|
|[hbOaUWBaLQy0R2Bn.htm](pathfinder-bestiary-items/hbOaUWBaLQy0R2Bn.htm)|Athletics|vacía|
|[hE4DVzxWFecGtuNZ.htm](pathfinder-bestiary-items/hE4DVzxWFecGtuNZ.htm)|Stealth|vacía|
|[HhvCZz6YcQkegNS0.htm](pathfinder-bestiary-items/HhvCZz6YcQkegNS0.htm)|Library Lore|vacía|
|[HSGwNc76nv12D70v.htm](pathfinder-bestiary-items/HSGwNc76nv12D70v.htm)|Cult Lore|vacía|
|[HWoliZSTLItugTKi.htm](pathfinder-bestiary-items/HWoliZSTLItugTKi.htm)|Athletics|vacía|
|[HZjTjHMm6PiqprBW.htm](pathfinder-bestiary-items/HZjTjHMm6PiqprBW.htm)|Stealth|vacía|
|[isDFExlTeVfhuE1S.htm](pathfinder-bestiary-items/isDFExlTeVfhuE1S.htm)|Stealth|vacía|
|[iUzYuqnjkPvxjVq6.htm](pathfinder-bestiary-items/iUzYuqnjkPvxjVq6.htm)|Stealth|vacía|
|[IXKSet09gpO7nCje.htm](pathfinder-bestiary-items/IXKSet09gpO7nCje.htm)|Iruxi Lore|vacía|
|[j0CQH72Y8ZIJSLQK.htm](pathfinder-bestiary-items/j0CQH72Y8ZIJSLQK.htm)|Athletics|vacía|
|[JCTMNa7QfednvGsM.htm](pathfinder-bestiary-items/JCTMNa7QfednvGsM.htm)|Acrobatics|vacía|
|[JotouX7ikK4QZh6H.htm](pathfinder-bestiary-items/JotouX7ikK4QZh6H.htm)|Lore (all subcategories)|vacía|
|[k1wz9hBi74eZjENe.htm](pathfinder-bestiary-items/k1wz9hBi74eZjENe.htm)|Sack for Holding Rocks|vacía|
|[K5npZtgqQqoK6CQZ.htm](pathfinder-bestiary-items/K5npZtgqQqoK6CQZ.htm)|Rock|vacía|
|[kC6oFte0xKiH0PxI.htm](pathfinder-bestiary-items/kC6oFte0xKiH0PxI.htm)|Stealth|vacía|
|[kq8avsOyetNPoxAy.htm](pathfinder-bestiary-items/kq8avsOyetNPoxAy.htm)|Stealth|vacía|
|[Ktfy8eNRSg3A28w4.htm](pathfinder-bestiary-items/Ktfy8eNRSg3A28w4.htm)|Bardic Lore|vacía|
|[kwfQBdNFEco6T8zy.htm](pathfinder-bestiary-items/kwfQBdNFEco6T8zy.htm)|Stealth|vacía|
|[kXXO2coSLlDjiCDu.htm](pathfinder-bestiary-items/kXXO2coSLlDjiCDu.htm)|Survival|vacía|
|[KYeYaMrxJZuncqJp.htm](pathfinder-bestiary-items/KYeYaMrxJZuncqJp.htm)|Dragon Lore|vacía|
|[L803Ke1jnhgZ8lBH.htm](pathfinder-bestiary-items/L803Ke1jnhgZ8lBH.htm)|Legal Lore|vacía|
|[leFRHEa4dIOric6F.htm](pathfinder-bestiary-items/leFRHEa4dIOric6F.htm)|Athletics|vacía|
|[LhJUXHtFD2J1svlu.htm](pathfinder-bestiary-items/LhJUXHtFD2J1svlu.htm)|Stealth|vacía|
|[LQ0EYIyjnsQ2WbOh.htm](pathfinder-bestiary-items/LQ0EYIyjnsQ2WbOh.htm)|Crafting|vacía|
|[m7T92Y6WGGbnqEFc.htm](pathfinder-bestiary-items/m7T92Y6WGGbnqEFc.htm)|Skeletal Lore|vacía|
|[N5MfzopIegJDrl6g.htm](pathfinder-bestiary-items/N5MfzopIegJDrl6g.htm)|Athletics|vacía|
|[NGvH5FW9ME8rdGTi.htm](pathfinder-bestiary-items/NGvH5FW9ME8rdGTi.htm)|Boneyard Lore|vacía|
|[nW50DwpfdKVVmbOK.htm](pathfinder-bestiary-items/nW50DwpfdKVVmbOK.htm)|Stealth|vacía|
|[OEhg2gie7GWSfArM.htm](pathfinder-bestiary-items/OEhg2gie7GWSfArM.htm)|Stealth|vacía|
|[ofZG0kDOYskLWahT.htm](pathfinder-bestiary-items/ofZG0kDOYskLWahT.htm)|Lore (any one subcategory)|vacía|
|[or9Nt7DdSkg7avHE.htm](pathfinder-bestiary-items/or9Nt7DdSkg7avHE.htm)|Stealth|vacía|
|[osRz4echVjI4mKL6.htm](pathfinder-bestiary-items/osRz4echVjI4mKL6.htm)|Athletics|vacía|
|[oyoc1hZoDt82qJP0.htm](pathfinder-bestiary-items/oyoc1hZoDt82qJP0.htm)|Warfare Lore|vacía|
|[P0KjUhwFwNtZ0DY7.htm](pathfinder-bestiary-items/P0KjUhwFwNtZ0DY7.htm)|Sack for Holding Rocks|vacía|
|[PuwbojJVdeWhRjeH.htm](pathfinder-bestiary-items/PuwbojJVdeWhRjeH.htm)|Vampire Lore|vacía|
|[pY7DRATVeNfGLzvD.htm](pathfinder-bestiary-items/pY7DRATVeNfGLzvD.htm)|Fire Lore|vacía|
|[qcCBWdNoDKQOL8YM.htm](pathfinder-bestiary-items/qcCBWdNoDKQOL8YM.htm)|Athletics|vacía|
|[qDRdTqUJoBOjEZCq.htm](pathfinder-bestiary-items/qDRdTqUJoBOjEZCq.htm)|Stealth|vacía|
|[qGVF829Lwi4ZemA9.htm](pathfinder-bestiary-items/qGVF829Lwi4ZemA9.htm)|Dwelling Lore (applies only to the dungeon it lives in)|vacía|
|[qxeMxKzn8k64O7D0.htm](pathfinder-bestiary-items/qxeMxKzn8k64O7D0.htm)|Red Cap|vacía|
|[r0Jfu2EKhpHiK8Io.htm](pathfinder-bestiary-items/r0Jfu2EKhpHiK8Io.htm)|Athletics|vacía|
|[r5bsJhi6sEolEyLe.htm](pathfinder-bestiary-items/r5bsJhi6sEolEyLe.htm)|Rock|vacía|
|[rUAZYpNlsXhklPKa.htm](pathfinder-bestiary-items/rUAZYpNlsXhklPKa.htm)|Crafting|vacía|
|[sGOjcBjzMl3GBzJA.htm](pathfinder-bestiary-items/sGOjcBjzMl3GBzJA.htm)|Any One Lore|vacía|
|[skdB8eSpcqrot5k6.htm](pathfinder-bestiary-items/skdB8eSpcqrot5k6.htm)|Crafting|vacía|
|[tBNqq8ckTUXGr6OA.htm](pathfinder-bestiary-items/tBNqq8ckTUXGr6OA.htm)|Survival|vacía|
|[tDk92ryyiwtL1nav.htm](pathfinder-bestiary-items/tDk92ryyiwtL1nav.htm)|Acrobatics|vacía|
|[tNpOMZZqsUcl5sRQ.htm](pathfinder-bestiary-items/tNpOMZZqsUcl5sRQ.htm)|Cult Lore|vacía|
|[tUc2FVPtgBETUoR1.htm](pathfinder-bestiary-items/tUc2FVPtgBETUoR1.htm)|Stealth|vacía|
|[ubBKia3cehXEewHz.htm](pathfinder-bestiary-items/ubBKia3cehXEewHz.htm)|Athletics|vacía|
|[UdfdvJASTQ867hij.htm](pathfinder-bestiary-items/UdfdvJASTQ867hij.htm)|Stealth|vacía|
|[UedMQrrYjW7RxjuG.htm](pathfinder-bestiary-items/UedMQrrYjW7RxjuG.htm)|Crafting|vacía|
|[uRp6lGDTd0IPfdWI.htm](pathfinder-bestiary-items/uRp6lGDTd0IPfdWI.htm)|Panpipes|vacía|
|[V2B3ENZzFY5uoX2u.htm](pathfinder-bestiary-items/V2B3ENZzFY5uoX2u.htm)|Survival|vacía|
|[vBFHfrDtlZ5vYw2q.htm](pathfinder-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|vacía|
|[vBXkfU5VjP7BNbGf.htm](pathfinder-bestiary-items/vBXkfU5VjP7BNbGf.htm)|Crafting|vacía|
|[vln9tGlYOAxkVD0U.htm](pathfinder-bestiary-items/vln9tGlYOAxkVD0U.htm)|Stealth|vacía|
|[VQu2kLUgSRrCTMRP.htm](pathfinder-bestiary-items/VQu2kLUgSRrCTMRP.htm)|Deception|vacía|
|[WWQwqsaUXwuRwu4b.htm](pathfinder-bestiary-items/WWQwqsaUXwuRwu4b.htm)|Stealth|vacía|
|[WYP5ilxerdImWTrf.htm](pathfinder-bestiary-items/WYP5ilxerdImWTrf.htm)|Athletics|vacía|
|[xAxV9yRmp780wKql.htm](pathfinder-bestiary-items/xAxV9yRmp780wKql.htm)|Stealth|vacía|
|[XfysylreSHBRTKzu.htm](pathfinder-bestiary-items/XfysylreSHBRTKzu.htm)|Performance|vacía|
|[Xh3IQKDYMKjCIEeb.htm](pathfinder-bestiary-items/Xh3IQKDYMKjCIEeb.htm)|Survival|vacía|
|[XiR8kBhdXvv7AE8L.htm](pathfinder-bestiary-items/XiR8kBhdXvv7AE8L.htm)|Athletics|vacía|
|[xmfPKEjapaeysAHV.htm](pathfinder-bestiary-items/xmfPKEjapaeysAHV.htm)|Geology Lore|vacía|
|[XwujWRcPvBs2BAHX.htm](pathfinder-bestiary-items/XwujWRcPvBs2BAHX.htm)|Sack for Holding Rocks|vacía|
|[xXuKG36gOYgi1D2d.htm](pathfinder-bestiary-items/xXuKG36gOYgi1D2d.htm)|Stealth|vacía|
|[Y1GjnjSidGi1JZ8O.htm](pathfinder-bestiary-items/Y1GjnjSidGi1JZ8O.htm)|Sack for Holding Rocks|vacía|
|[y4RJFccf3Sw5GwrU.htm](pathfinder-bestiary-items/y4RJFccf3Sw5GwrU.htm)|Lore (any one subcategory)|vacía|
|[yh5j8rAspmjIzsel.htm](pathfinder-bestiary-items/yh5j8rAspmjIzsel.htm)|Crafting|vacía|
|[z8haj2djGN404zAS.htm](pathfinder-bestiary-items/z8haj2djGN404zAS.htm)|Diplomacy|vacía|
|[Zo0FP9W2ApyQ1WcK.htm](pathfinder-bestiary-items/Zo0FP9W2ApyQ1WcK.htm)|Signet Ring|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
