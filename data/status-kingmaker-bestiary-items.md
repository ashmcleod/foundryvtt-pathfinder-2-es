# Estado de la traducción (kingmaker-bestiary-items)

 * **modificada**: 1343
 * **ninguna**: 224
 * **vacía**: 111


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0bBCzMxj06H1x2EI.htm](kingmaker-bestiary-items/0bBCzMxj06H1x2EI.htm)|Composite Longbow|+1,striking|
|[0bbJhjr4XkyXm2cv.htm](kingmaker-bestiary-items/0bbJhjr4XkyXm2cv.htm)|Mistform Elixir (Greater) (Infused)|
|[0bQTICfElBoz3XPf.htm](kingmaker-bestiary-items/0bQTICfElBoz3XPf.htm)|+1 Breastplate|
|[0NDexqBHwunzi360.htm](kingmaker-bestiary-items/0NDexqBHwunzi360.htm)|Dagger|+2,greaterStriking|
|[0qWcKzKBUEwvLvdH.htm](kingmaker-bestiary-items/0qWcKzKBUEwvLvdH.htm)|Pipes|
|[0rmDD9ymGXaLDR7A.htm](kingmaker-bestiary-items/0rmDD9ymGXaLDR7A.htm)|Longsword|+1|
|[0ShB5n7PzRHdtWVF.htm](kingmaker-bestiary-items/0ShB5n7PzRHdtWVF.htm)|Primal Call (Creatures with the Wild Hunt Trait Only)|
|[1bZpLXmtzfGcJX8S.htm](kingmaker-bestiary-items/1bZpLXmtzfGcJX8S.htm)|+2 Resilient Studded Leather Armor|
|[22UiypLQhIBbjWDe.htm](kingmaker-bestiary-items/22UiypLQhIBbjWDe.htm)|Religious Text of Saranrae|
|[2cPfluVOMi5nDGgx.htm](kingmaker-bestiary-items/2cPfluVOMi5nDGgx.htm)|Trident|+1,striking|
|[2jUfYxc6d6U4w3Z7.htm](kingmaker-bestiary-items/2jUfYxc6d6U4w3Z7.htm)|Dimension Door (At Will) (To any location in Armag's Tomb)|
|[2xC1UooYAPVhcemO.htm](kingmaker-bestiary-items/2xC1UooYAPVhcemO.htm)|Religious Symbol of Gogunta (Silver)|
|[3Oyvp6SK64TaOXVq.htm](kingmaker-bestiary-items/3Oyvp6SK64TaOXVq.htm)|Bastard Sword|+2,striking,wounding|
|[3WcqrLbeRK1eZWeJ.htm](kingmaker-bestiary-items/3WcqrLbeRK1eZWeJ.htm)|+1 Leather Armor|
|[432AQ0jbevEtWKdG.htm](kingmaker-bestiary-items/432AQ0jbevEtWKdG.htm)|Formula Book|
|[4zR81aivcsDVQxAp.htm](kingmaker-bestiary-items/4zR81aivcsDVQxAp.htm)|Whip|+1,striking|
|[57nrcKoJBLBDALst.htm](kingmaker-bestiary-items/57nrcKoJBLBDALst.htm)|Living Bow|
|[5juLpMpruoLCJ9JZ.htm](kingmaker-bestiary-items/5juLpMpruoLCJ9JZ.htm)|Hatchet|+2,striking|
|[5mJxaR7sQGjBWkRc.htm](kingmaker-bestiary-items/5mJxaR7sQGjBWkRc.htm)|+2 Resilient Leather Armor|
|[5Tlw4BDzoIJ4STyR.htm](kingmaker-bestiary-items/5Tlw4BDzoIJ4STyR.htm)|Battered Breastplate|
|[67Qch1f76P2Ze8KM.htm](kingmaker-bestiary-items/67Qch1f76P2Ze8KM.htm)|Religious Symbol of Gyronna (Silver)|
|[6jXk3sASWaQvOH5n.htm](kingmaker-bestiary-items/6jXk3sASWaQvOH5n.htm)|Dagger|+1|
|[6TCoYo76CHcPURpz.htm](kingmaker-bestiary-items/6TCoYo76CHcPURpz.htm)|Resilient Splint Mail|
|[76TiztNcrVVUhZQl.htm](kingmaker-bestiary-items/76TiztNcrVVUhZQl.htm)|Speak with Animals (Constant)|
|[7bd1h5tOi35bvK7Q.htm](kingmaker-bestiary-items/7bd1h5tOi35bvK7Q.htm)|Wand of Shocking Grasp (Level 2)|
|[7N8Klhs5wpgJ6RvM.htm](kingmaker-bestiary-items/7N8Klhs5wpgJ6RvM.htm)|Hunting Horn|
|[7PNhfO9yF8jlxc97.htm](kingmaker-bestiary-items/7PNhfO9yF8jlxc97.htm)|Rapier|+1|
|[88uOslPj6NpTUQNF.htm](kingmaker-bestiary-items/88uOslPj6NpTUQNF.htm)|Shortsword|+1,striking|
|[97i0qo04YJEOlBSn.htm](kingmaker-bestiary-items/97i0qo04YJEOlBSn.htm)|Rare Candles and Incense|
|[9RNGJOLNS21hY10V.htm](kingmaker-bestiary-items/9RNGJOLNS21hY10V.htm)|Falchion|+1,striking|
|[9TeqIJRQyjU0d2eg.htm](kingmaker-bestiary-items/9TeqIJRQyjU0d2eg.htm)|+2 Greater Fire Resistant Greater Resilient Leather Armor|
|[9xLtvFQAoJh8tSOd.htm](kingmaker-bestiary-items/9xLtvFQAoJh8tSOd.htm)|Staff of Power|+2,greaterStriking|
|[9XpL2lVavNQF91Zx.htm](kingmaker-bestiary-items/9XpL2lVavNQF91Zx.htm)|True Seeing (Constant)|
|[abr9bhWIMzRT7FHN.htm](kingmaker-bestiary-items/abr9bhWIMzRT7FHN.htm)|Grisly Scythe|+1,striking,wounding|
|[Aeu3hLK3x9HeXzhD.htm](kingmaker-bestiary-items/Aeu3hLK3x9HeXzhD.htm)|Darkness (At Will)|
|[AFXLJNAW0dhc0PkH.htm](kingmaker-bestiary-items/AFXLJNAW0dhc0PkH.htm)|Wineskin|
|[AfZgCzOcPYkL5eV2.htm](kingmaker-bestiary-items/AfZgCzOcPYkL5eV2.htm)|Kukri|+1|
|[aGPW7GAY6qDJQHh8.htm](kingmaker-bestiary-items/aGPW7GAY6qDJQHh8.htm)|Ring of Energy Resistance (Major) (Fire)|
|[ap4RPRdJx6CtkxmL.htm](kingmaker-bestiary-items/ap4RPRdJx6CtkxmL.htm)|Ovinrbaane|+2,striking,wounding|
|[Aq3vYXMGDf6pFo4D.htm](kingmaker-bestiary-items/Aq3vYXMGDf6pFo4D.htm)|Plane Shift (to or from the First World only)|
|[b2b4en2ylHZagF2u.htm](kingmaker-bestiary-items/b2b4en2ylHZagF2u.htm)|Wand of Burning Hands (Level 1)|
|[B2sQYrMGR7VibDvV.htm](kingmaker-bestiary-items/B2sQYrMGR7VibDvV.htm)|Dagger|+1,striking,silver|
|[b5j9aIeq7ERjg1cn.htm](kingmaker-bestiary-items/b5j9aIeq7ERjg1cn.htm)|Religious Symbol of Gozreh (Wooden and Bloodstained)|
|[BDyM0mqNVmkzGKe2.htm](kingmaker-bestiary-items/BDyM0mqNVmkzGKe2.htm)|Carpenter's Tools|
|[BvsSj4ZTgepDO6JN.htm](kingmaker-bestiary-items/BvsSj4ZTgepDO6JN.htm)|Battle Axe|+1,striking|
|[BXDlaMPWYXlB5Few.htm](kingmaker-bestiary-items/BXDlaMPWYXlB5Few.htm)|+1 Full Plate|
|[C3EpruyM244f0JVf.htm](kingmaker-bestiary-items/C3EpruyM244f0JVf.htm)|Greatsword|+2,striking|
|[c3NPgUPsH0WkQZXG.htm](kingmaker-bestiary-items/c3NPgUPsH0WkQZXG.htm)|Composite Shortbow|+1,striking|
|[CC2IGMZw3wpLt6Av.htm](kingmaker-bestiary-items/CC2IGMZw3wpLt6Av.htm)|Composite Longbow|+1|
|[cG10UmeLEy9eANOc.htm](kingmaker-bestiary-items/cG10UmeLEy9eANOc.htm)|Acid Flask (Greater) (Infused)|
|[cIFXkQ4Qhht73I7F.htm](kingmaker-bestiary-items/cIFXkQ4Qhht73I7F.htm)|Longsword|+1,striking|
|[CLhR4qg5HhAxRnYA.htm](kingmaker-bestiary-items/CLhR4qg5HhAxRnYA.htm)|Suggestion (At Will)|
|[cOFKoOU0dk3bhJeX.htm](kingmaker-bestiary-items/cOFKoOU0dk3bhJeX.htm)|Religious Symbol of Calistra (Silver)|
|[crdKpfEf0xqRNZES.htm](kingmaker-bestiary-items/crdKpfEf0xqRNZES.htm)|Greataxe|+2,striking|
|[CtzLyJ8UMGgt86Xw.htm](kingmaker-bestiary-items/CtzLyJ8UMGgt86Xw.htm)|+2 Greater Resilient Mithral Chain Mail|
|[CX6zUhlF06g9iSjB.htm](kingmaker-bestiary-items/CX6zUhlF06g9iSjB.htm)|Wand of Invisibility (Level 2)|
|[D2qG6bBAwV0Xu4iO.htm](kingmaker-bestiary-items/D2qG6bBAwV0Xu4iO.htm)|+1 Full Plate|
|[D8DPsCfECCJBxsBs.htm](kingmaker-bestiary-items/D8DPsCfECCJBxsBs.htm)|Speak with Plants (Constant)|
|[d8jlUApm6erhQTpM.htm](kingmaker-bestiary-items/d8jlUApm6erhQTpM.htm)|Whip|+1|
|[dBd9trOqG8wBEYZ6.htm](kingmaker-bestiary-items/dBd9trOqG8wBEYZ6.htm)|+1 Resilient Hide Armor|
|[dEj5HxpnzeRCKekU.htm](kingmaker-bestiary-items/dEj5HxpnzeRCKekU.htm)|Wand of Glitterdust (Level 2)|
|[DN5RQpUoYCp3qWou.htm](kingmaker-bestiary-items/DN5RQpUoYCp3qWou.htm)|Dagger|+1|
|[EH6c2qLG9oWj3bcW.htm](kingmaker-bestiary-items/EH6c2qLG9oWj3bcW.htm)|Greataxe|+2,striking|
|[f0ReT77CQpNnH0wr.htm](kingmaker-bestiary-items/f0ReT77CQpNnH0wr.htm)|+2 Resilient Full Plate|
|[f1KaRpZo7GoKPttu.htm](kingmaker-bestiary-items/f1KaRpZo7GoKPttu.htm)|Wand of Magic Missile (Level 1)|
|[F6NTNYwrbXvVsHwE.htm](kingmaker-bestiary-items/F6NTNYwrbXvVsHwE.htm)|Robes|
|[FEvGGdluzakeGdBF.htm](kingmaker-bestiary-items/FEvGGdluzakeGdBF.htm)|+2 Greater Resilient Clothing (Explorer's)|
|[FjxVn3oLSCrfIIIz.htm](kingmaker-bestiary-items/FjxVn3oLSCrfIIIz.htm)|Religious Symbol of Yog-Sothoth|
|[FoPN2th3v5NxtKds.htm](kingmaker-bestiary-items/FoPN2th3v5NxtKds.htm)|Maul|+1|
|[FS1iX2ShhtCPk1Wr.htm](kingmaker-bestiary-items/FS1iX2ShhtCPk1Wr.htm)|Shortsword|+1|
|[FZZwIPdH86woV8uz.htm](kingmaker-bestiary-items/FZZwIPdH86woV8uz.htm)|+1 Chain Mail|
|[g9hAlndNzXTVzNDu.htm](kingmaker-bestiary-items/g9hAlndNzXTVzNDu.htm)|Basic Woodworker's Book|
|[gEF9138FUjhpAL9X.htm](kingmaker-bestiary-items/gEF9138FUjhpAL9X.htm)|True Seeing (Constant)|
|[Gk2AECC4rYPqsJbg.htm](kingmaker-bestiary-items/Gk2AECC4rYPqsJbg.htm)|Restore Senses (At Will)|
|[GLcjMCpmzoaId5jx.htm](kingmaker-bestiary-items/GLcjMCpmzoaId5jx.htm)|Imprisonment (Cannot Currently Cast)|
|[gsd3hcxt7GSe5WzZ.htm](kingmaker-bestiary-items/gsd3hcxt7GSe5WzZ.htm)|Aldori Dueling Sword|+1|
|[gVhcOspe8h7FkmF1.htm](kingmaker-bestiary-items/gVhcOspe8h7FkmF1.htm)|Robes|
|[gvKFoNQurhUZnlWh.htm](kingmaker-bestiary-items/gvKFoNQurhUZnlWh.htm)|Ring of Energy Resistance (Fire)|
|[gymUtf0OxE6gzjqN.htm](kingmaker-bestiary-items/gymUtf0OxE6gzjqN.htm)|+1 Glamered Chain Shirt|
|[hCCNqP3969tyxk96.htm](kingmaker-bestiary-items/hCCNqP3969tyxk96.htm)|+2 Greater Striking Spiked Greatclub|+2,greaterStriking|
|[hdEvwgLsMiLlJbMm.htm](kingmaker-bestiary-items/hdEvwgLsMiLlJbMm.htm)|+2 Greater Resilient Hide Armor|
|[hjfvtpOl2IljcOOH.htm](kingmaker-bestiary-items/hjfvtpOl2IljcOOH.htm)|Outcast's Curse (At Will)|
|[HplaolUyjv0ldmah.htm](kingmaker-bestiary-items/HplaolUyjv0ldmah.htm)|Eagle Eye Elixir (Major) (Infused)|
|[HsvS5P4EDcjC5Y5A.htm](kingmaker-bestiary-items/HsvS5P4EDcjC5Y5A.htm)|Outcast's Curse (At Will)|
|[hvCc4reISEOqsFIa.htm](kingmaker-bestiary-items/hvCc4reISEOqsFIa.htm)|Darkness (At Will)|
|[HvSxWkSSm2CCuJHu.htm](kingmaker-bestiary-items/HvSxWkSSm2CCuJHu.htm)|+1 Cold-Resistant Leather Armor|
|[hXe1xaYFIHJ8f7tw.htm](kingmaker-bestiary-items/hXe1xaYFIHJ8f7tw.htm)|Trident|+1,striking|
|[Hyozh14mnCpe6YVy.htm](kingmaker-bestiary-items/Hyozh14mnCpe6YVy.htm)|Shortsword|+1,striking|
|[I9tYnS5FyCSmLBYY.htm](kingmaker-bestiary-items/I9tYnS5FyCSmLBYY.htm)|Tree Shape (At Will)|
|[iHz93nPS055VlpTQ.htm](kingmaker-bestiary-items/iHz93nPS055VlpTQ.htm)|+1 Crossbow|+1,striking|
|[IJyUjXFsO9M8LpUs.htm](kingmaker-bestiary-items/IJyUjXFsO9M8LpUs.htm)|Elegant Robes|
|[Ik3rJsTnSTRAi671.htm](kingmaker-bestiary-items/Ik3rJsTnSTRAi671.htm)|Invisibility (Self Only)|
|[IOZpqdcbetYaemZ3.htm](kingmaker-bestiary-items/IOZpqdcbetYaemZ3.htm)|Monstrous Bloom|
|[j8PYlRTNy6uxScHw.htm](kingmaker-bestiary-items/j8PYlRTNy6uxScHw.htm)|Wand of Dimension Door (Level 4)|
|[JHBWAs0dwbsbDu7J.htm](kingmaker-bestiary-items/JHBWAs0dwbsbDu7J.htm)|+2 Greater Resilient Full Plate|
|[jkJ5X9sX7YU7UA2F.htm](kingmaker-bestiary-items/jkJ5X9sX7YU7UA2F.htm)|+1 Resilient Breastplate|
|[JlGJE2TMv5okfuks.htm](kingmaker-bestiary-items/JlGJE2TMv5okfuks.htm)|Dagger|+1,greaterStriking,greaterFrost|
|[JloIn4LDx9M1fiyw.htm](kingmaker-bestiary-items/JloIn4LDx9M1fiyw.htm)|Eclipsed Incantations|
|[JRCxllIuf9pbk0QY.htm](kingmaker-bestiary-items/JRCxllIuf9pbk0QY.htm)|Kukri|+2,greaterStriking,keen|
|[k8GjK7psOKSAVUhr.htm](kingmaker-bestiary-items/k8GjK7psOKSAVUhr.htm)|Prismatic Spray (At Will)|
|[kEk8UpJ0Ow9EnnTr.htm](kingmaker-bestiary-items/kEk8UpJ0Ow9EnnTr.htm)|Warhammer|+2,greaterFrost|
|[kgHoUAo23IIBJKPK.htm](kingmaker-bestiary-items/kgHoUAo23IIBJKPK.htm)|+1 Striking Staff of Fire (Major)|+1,striking|
|[kTPMsO2Nauv9vM6Y.htm](kingmaker-bestiary-items/kTPMsO2Nauv9vM6Y.htm)|Speak with Plants (Constant)|
|[kVgdUKwhGxbwACnL.htm](kingmaker-bestiary-items/kVgdUKwhGxbwACnL.htm)|Hunting Spider Venom (On Darts)|
|[kXmoXkc5myLgiFoy.htm](kingmaker-bestiary-items/kXmoXkc5myLgiFoy.htm)|Mind Probe (At Will)|
|[l47LUxjBXwp06JZ8.htm](kingmaker-bestiary-items/l47LUxjBXwp06JZ8.htm)|Glaive|+1|
|[L9sxOR3SnpDYfZHq.htm](kingmaker-bestiary-items/L9sxOR3SnpDYfZHq.htm)|Tree Stride (Self plus Willing Rider)|
|[lH0kmQP6fJWkOqZ5.htm](kingmaker-bestiary-items/lH0kmQP6fJWkOqZ5.htm)|+2 Resilient White Dragonhide Breastplate|
|[lo9zjzoOZSRs5svZ.htm](kingmaker-bestiary-items/lo9zjzoOZSRs5svZ.htm)|Darkness (At Will)|
|[lta8LCkZJo5xDrQ5.htm](kingmaker-bestiary-items/lta8LCkZJo5xDrQ5.htm)|+1 Resilient Full Plate|
|[mcvnk6mFrYyK92Mb.htm](kingmaker-bestiary-items/mcvnk6mFrYyK92Mb.htm)|Morningstar|+1,darkwood,thundering|
|[MeD83DvPWfOBUylm.htm](kingmaker-bestiary-items/MeD83DvPWfOBUylm.htm)|Dagger|+2,greaterStriking,spellStoring|
|[mGvIyNKk0KBZu3BK.htm](kingmaker-bestiary-items/mGvIyNKk0KBZu3BK.htm)|Aldori Dueling Sword|+2,striking|
|[MHLJGTn6eQAmWIUX.htm](kingmaker-bestiary-items/MHLJGTn6eQAmWIUX.htm)|True Seeing (Constant)|
|[MHRiahLowofy8ek9.htm](kingmaker-bestiary-items/MHRiahLowofy8ek9.htm)|Invisibility (At Will)|
|[mo5giist2lOlgVh5.htm](kingmaker-bestiary-items/mo5giist2lOlgVh5.htm)|Longsword|+1,striking,grievous|
|[MxGd3lTVgUVyx8lw.htm](kingmaker-bestiary-items/MxGd3lTVgUVyx8lw.htm)|Handwraps of Mighty Blows|+2,striking|
|[mxTIF80CDuT8kywe.htm](kingmaker-bestiary-items/mxTIF80CDuT8kywe.htm)|Ring of Energy Resistance (Fire)|
|[myUDdwt1FjfMMl9p.htm](kingmaker-bestiary-items/myUDdwt1FjfMMl9p.htm)|Staff|+1|
|[N4FRJPLjGNaWpvxp.htm](kingmaker-bestiary-items/N4FRJPLjGNaWpvxp.htm)|Religious Symbol of Yog-Sothoth|
|[NcDY8Koc4BA35pJ3.htm](kingmaker-bestiary-items/NcDY8Koc4BA35pJ3.htm)|The Inward Flowing Source (Spellbook)|
|[nGVNV8Ln1yOs1ff4.htm](kingmaker-bestiary-items/nGVNV8Ln1yOs1ff4.htm)|Scimitar|+1,striking,disrupting|
|[nOFWQRAuYiD4lmUi.htm](kingmaker-bestiary-items/nOFWQRAuYiD4lmUi.htm)|+1 Resilient Clothing (Explorer's)|
|[NsbWotgycMOTGuGT.htm](kingmaker-bestiary-items/NsbWotgycMOTGuGT.htm)|Suggestion (At Will)|
|[NTO1vgINd90By1LE.htm](kingmaker-bestiary-items/NTO1vgINd90By1LE.htm)|Journal|
|[NzVLRMB9AI3b1pBg.htm](kingmaker-bestiary-items/NzVLRMB9AI3b1pBg.htm)|Tattered Robes|
|[O3DozXqnlvka4VIK.htm](kingmaker-bestiary-items/O3DozXqnlvka4VIK.htm)|Choker of Elocution (Sylvan)|
|[o5RWcZEKhSBVRSCd.htm](kingmaker-bestiary-items/o5RWcZEKhSBVRSCd.htm)|See Invisibility (Constant)|
|[oBUF4qu6Shoi0A37.htm](kingmaker-bestiary-items/oBUF4qu6Shoi0A37.htm)|Musical Instrument (Lute)|
|[OFUhWZezAxCDljSA.htm](kingmaker-bestiary-items/OFUhWZezAxCDljSA.htm)|Dogslicer|+1,striking|
|[OGDCzsyhNsf5h3j3.htm](kingmaker-bestiary-items/OGDCzsyhNsf5h3j3.htm)|Artisan's Tools (Woodworker's)|
|[oK1SNnCrHiQzSXJH.htm](kingmaker-bestiary-items/oK1SNnCrHiQzSXJH.htm)|True Seeing (Constant)|
|[OLgE1eKYfChrP7ip.htm](kingmaker-bestiary-items/OLgE1eKYfChrP7ip.htm)|Ring of Energy Resistance (Fire)|
|[oM6MOLJCVEE8WTTc.htm](kingmaker-bestiary-items/oM6MOLJCVEE8WTTc.htm)|Speak with Animals (Constant)|
|[oMIhKijss8NvtV5O.htm](kingmaker-bestiary-items/oMIhKijss8NvtV5O.htm)|Sickle|cold-iron|
|[OQrd4dnZRVS4hQEO.htm](kingmaker-bestiary-items/OQrd4dnZRVS4hQEO.htm)|Maestro's Instrument (Lesser) (Lute)|
|[OyvZeoS5KOjNU1P7.htm](kingmaker-bestiary-items/OyvZeoS5KOjNU1P7.htm)|Heavy Wooden Shield|
|[p4u7kb7SoMyLXv5z.htm](kingmaker-bestiary-items/p4u7kb7SoMyLXv5z.htm)|Wand of Paralyze (Level 3)|
|[P65Jvc5wODFPZ00e.htm](kingmaker-bestiary-items/P65Jvc5wODFPZ00e.htm)|Dagger|+1,striking,wounding|
|[pahKe1LJWdfkKj6M.htm](kingmaker-bestiary-items/pahKe1LJWdfkKj6M.htm)|Mind Reading (At Will)|
|[pF8Ngkhp4Zxkwqhd.htm](kingmaker-bestiary-items/pF8Ngkhp4Zxkwqhd.htm)|+2 Glamered Fortification Greater Resilient Leather Armor|
|[pIQHLI7rmcBAcJql.htm](kingmaker-bestiary-items/pIQHLI7rmcBAcJql.htm)|Wand of Heal (Level 1)|
|[PmwxtRI969adfqM7.htm](kingmaker-bestiary-items/PmwxtRI969adfqM7.htm)|Key Ring|
|[pNlVk6Eq16L9OBYt.htm](kingmaker-bestiary-items/pNlVk6Eq16L9OBYt.htm)|Tongues (Constant)|
|[pO1coGfLkTGz4KtP.htm](kingmaker-bestiary-items/pO1coGfLkTGz4KtP.htm)|Speak with Plants (Constant)|
|[q0qbUmAXaCjmrWfl.htm](kingmaker-bestiary-items/q0qbUmAXaCjmrWfl.htm)|Water Walk (Constant) (Self Only)|
|[q2Yeh8EgyWJTHR2E.htm](kingmaker-bestiary-items/q2Yeh8EgyWJTHR2E.htm)|Greataxe|+1|
|[Q3rzVNhMmKJUv8Hd.htm](kingmaker-bestiary-items/Q3rzVNhMmKJUv8Hd.htm)|Rusted Iron Holy Symbol of Erastil|
|[Q6aECCBC8XJs3Eke.htm](kingmaker-bestiary-items/Q6aECCBC8XJs3Eke.htm)|Water Breathing (Constant)|
|[QLpCft5YSm98kbx4.htm](kingmaker-bestiary-items/QLpCft5YSm98kbx4.htm)|Speak with Animals (At Will) (Arthropods Only)|
|[qnW0tYvY2eKqxqcI.htm](kingmaker-bestiary-items/qnW0tYvY2eKqxqcI.htm)|+1 Glamered Resilient reastplate|
|[r4ZnY6A2rOJTCSSI.htm](kingmaker-bestiary-items/r4ZnY6A2rOJTCSSI.htm)|Battle Axe|+1,striking|
|[R7WS5l7jScWRr53y.htm](kingmaker-bestiary-items/R7WS5l7jScWRr53y.htm)|+1 Resilient Hide Armor|
|[r9vIYF8LhOmQFT2b.htm](kingmaker-bestiary-items/r9vIYF8LhOmQFT2b.htm)|Dagger|+1|
|[rA1w7Wi3HDq5IpD3.htm](kingmaker-bestiary-items/rA1w7Wi3HDq5IpD3.htm)|Longsword|+1,striking,thundering|
|[rwoTbwYfMVqkiolf.htm](kingmaker-bestiary-items/rwoTbwYfMVqkiolf.htm)|Tongues (Tongues)|
|[RyEkbNlf2v3XdUOC.htm](kingmaker-bestiary-items/RyEkbNlf2v3XdUOC.htm)|Filthy Rags|
|[S01l0m7wdPHytTG5.htm](kingmaker-bestiary-items/S01l0m7wdPHytTG5.htm)|+1 Resilient Breastplate|
|[S0kF01v0eYuEyyQj.htm](kingmaker-bestiary-items/S0kF01v0eYuEyyQj.htm)|Charm (At Will)|
|[S3gLjkwIbPv8hrwC.htm](kingmaker-bestiary-items/S3gLjkwIbPv8hrwC.htm)|True Seeing (Constant)|
|[sAPqpw1AmafiS0qn.htm](kingmaker-bestiary-items/sAPqpw1AmafiS0qn.htm)|Palace Key|
|[SIvuOyYTHG1nqVDM.htm](kingmaker-bestiary-items/SIvuOyYTHG1nqVDM.htm)|Telekinetic Haul (At Will)|
|[SMqPkvP3sngKVR0q.htm](kingmaker-bestiary-items/SMqPkvP3sngKVR0q.htm)|Giant Wasp Venom (On Darts)|
|[SnhDyvqRiwCT2xFp.htm](kingmaker-bestiary-items/SnhDyvqRiwCT2xFp.htm)|Greatsword|+1,striking|
|[Sq7PsPkZ8LUfigSz.htm](kingmaker-bestiary-items/Sq7PsPkZ8LUfigSz.htm)|+1 Resilient Studded Leather Armor|
|[sUGKCp2vAoGiIFn9.htm](kingmaker-bestiary-items/sUGKCp2vAoGiIFn9.htm)|Oathbow|+2,striking|
|[SviievXlIEecohMT.htm](kingmaker-bestiary-items/SviievXlIEecohMT.htm)|Glaive|+2,greaterStriking|
|[sWBDDbGvVypQmjhH.htm](kingmaker-bestiary-items/sWBDDbGvVypQmjhH.htm)|Robes|
|[SzjP0fTFs7v3U74Q.htm](kingmaker-bestiary-items/SzjP0fTFs7v3U74Q.htm)|Composite Shortbow|+1,striking|
|[T2CxMAShv0ikuq5z.htm](kingmaker-bestiary-items/T2CxMAShv0ikuq5z.htm)|Ranseur|+1|
|[TgxTGQ9ZPvKuOKzP.htm](kingmaker-bestiary-items/TgxTGQ9ZPvKuOKzP.htm)|Elixir of Life (Major) (Infused)|
|[tKaxQW2SwxusRLDJ.htm](kingmaker-bestiary-items/tKaxQW2SwxusRLDJ.htm)|Mace|+1,striking,frost|
|[tt77yIY913w4s6Qb.htm](kingmaker-bestiary-items/tt77yIY913w4s6Qb.htm)|Rod of Razors|+2,greaterStriking,adamantine|
|[TXyxtLOPRcUESvfj.htm](kingmaker-bestiary-items/TXyxtLOPRcUESvfj.htm)|Tongues (Constant)|
|[TZKyD7ZHyYEDKtws.htm](kingmaker-bestiary-items/TZKyD7ZHyYEDKtws.htm)|Tongues (Constant)|
|[UCNMMptie1HfmWdu.htm](kingmaker-bestiary-items/UCNMMptie1HfmWdu.htm)|Rapier|+3,majorStriking,greaterShock,wounding|
|[UeSFgZM3o03iSxuq.htm](kingmaker-bestiary-items/UeSFgZM3o03iSxuq.htm)|+2 Resilient Full Plate|
|[UKYROFSj3c464waM.htm](kingmaker-bestiary-items/UKYROFSj3c464waM.htm)|Battle Axe|+2,striking|
|[Um8WRHtbayEPwLGn.htm](kingmaker-bestiary-items/Um8WRHtbayEPwLGn.htm)|Shortsword|cold-iron|
|[uq6NvrLNEXMkTsQf.htm](kingmaker-bestiary-items/uq6NvrLNEXMkTsQf.htm)|Pass Without Trace (Constant)|
|[UqyHj2l5UsVe9uNp.htm](kingmaker-bestiary-items/UqyHj2l5UsVe9uNp.htm)|+1 Leather Armor|
|[Us5NJn2RFIpZuqkH.htm](kingmaker-bestiary-items/Us5NJn2RFIpZuqkH.htm)|Crushing Despair (At Will)|
|[usScZt5t8JLT5Ipy.htm](kingmaker-bestiary-items/usScZt5t8JLT5Ipy.htm)|Illusory Disguise (At Will)|
|[UsxGNp0GLCHolNw9.htm](kingmaker-bestiary-items/UsxGNp0GLCHolNw9.htm)|Warhammer|+1|
|[UV0GwxK9jPNBGPkx.htm](kingmaker-bestiary-items/UV0GwxK9jPNBGPkx.htm)|Composite Longbow|+1|
|[uwaEfaQp2MZCrkyx.htm](kingmaker-bestiary-items/uwaEfaQp2MZCrkyx.htm)|Greataxe|cold-iron|
|[UZkmL2WnxA4MOAUK.htm](kingmaker-bestiary-items/UZkmL2WnxA4MOAUK.htm)|Composite Shortbow|+2,greaterStriking,darkwood,grievous,thundering|
|[vb6tOxigCXGBNO8n.htm](kingmaker-bestiary-items/vb6tOxigCXGBNO8n.htm)|+2 Resilient Leather Armor|
|[vbjtlEObLLpIhfeu.htm](kingmaker-bestiary-items/vbjtlEObLLpIhfeu.htm)|Rapier|+1|
|[vFhm5NJpp6V7lfc8.htm](kingmaker-bestiary-items/vFhm5NJpp6V7lfc8.htm)|Composite Shortbow|+1,striking|
|[VgQyT5O8X59xTXWs.htm](kingmaker-bestiary-items/VgQyT5O8X59xTXWs.htm)|Summon Elemental (Water Elementals only)|
|[vO3Jnytxei8sWTZC.htm](kingmaker-bestiary-items/vO3Jnytxei8sWTZC.htm)|Kukri|+1|
|[vVOwyQHVQvmtGjkx.htm](kingmaker-bestiary-items/vVOwyQHVQvmtGjkx.htm)|+1 Greater Invisibility Resilient Leather Armor|
|[VwMYfCPLMmuJQ2GH.htm](kingmaker-bestiary-items/VwMYfCPLMmuJQ2GH.htm)|+1 Resilient Breastplate|
|[vwnAqdmQtoc2jUsW.htm](kingmaker-bestiary-items/vwnAqdmQtoc2jUsW.htm)|Broken +1 Cold Iron Flaming Bastard Sword|+1,cold-iron,flaming|
|[vXW5poBVgyqzdM9r.htm](kingmaker-bestiary-items/vXW5poBVgyqzdM9r.htm)|Pass Without Trace (Constant)|
|[vyta0B6y63mLUnmx.htm](kingmaker-bestiary-items/vyta0B6y63mLUnmx.htm)|Maestro's Lute (Lesser)|
|[VzPueq3VgRjoXiJE.htm](kingmaker-bestiary-items/VzPueq3VgRjoXiJE.htm)|+1 Resilient Hide Armor|
|[W8NvzJ9eGCBEL7cH.htm](kingmaker-bestiary-items/W8NvzJ9eGCBEL7cH.htm)|Robes|
|[wd4WShZlAcAc3dLe.htm](kingmaker-bestiary-items/wd4WShZlAcAc3dLe.htm)|Religious Symbol of Yog-Sothoth|
|[wEUwtWWTglvCsbrr.htm](kingmaker-bestiary-items/wEUwtWWTglvCsbrr.htm)|Ring of Energy Resistance (Fire)|
|[Wj4PwJFZfcVXdLml.htm](kingmaker-bestiary-items/Wj4PwJFZfcVXdLml.htm)|Mind Reading (At Will)|
|[WlQZsDquZL824cDy.htm](kingmaker-bestiary-items/WlQZsDquZL824cDy.htm)|+1 Hide Armor|
|[wrFzv3AXMax8ZXTt.htm](kingmaker-bestiary-items/wrFzv3AXMax8ZXTt.htm)|Illusory Disguise (At Will)|
|[X3z6nTBllD8cR58y.htm](kingmaker-bestiary-items/X3z6nTBllD8cR58y.htm)|Ring of Energy Resistance (Cold)|
|[x9smkTQ1YyyY2Zcx.htm](kingmaker-bestiary-items/x9smkTQ1YyyY2Zcx.htm)|Invisibility (At Will)|
|[XgD0k33XDvhefMEQ.htm](kingmaker-bestiary-items/XgD0k33XDvhefMEQ.htm)|Tongues (Constant)|
|[XI80UWdBffkVI3qW.htm](kingmaker-bestiary-items/XI80UWdBffkVI3qW.htm)|Maul|+2,greaterShock|
|[XJFqRYGx02Um1W4Y.htm](kingmaker-bestiary-items/XJFqRYGx02Um1W4Y.htm)|Spear|+2,striking,anarchic|
|[xrKfhlv0heNZjJQl.htm](kingmaker-bestiary-items/xrKfhlv0heNZjJQl.htm)|+1 Morningstar|+1|
|[xT9pOWbS31HQus3Y.htm](kingmaker-bestiary-items/xT9pOWbS31HQus3Y.htm)|Hatchet|+1,striking|
|[xy0RcI3dflBZepLE.htm](kingmaker-bestiary-items/xy0RcI3dflBZepLE.htm)|Shortsword|+1|
|[Y9hSg33JjPCL2a3D.htm](kingmaker-bestiary-items/Y9hSg33JjPCL2a3D.htm)|+2 Resilient Leather Armor|
|[yGxF0q0Wjk3mF2Mo.htm](kingmaker-bestiary-items/yGxF0q0Wjk3mF2Mo.htm)|Air Walk (Constant)|
|[YJR2VSzinZXYbwkd.htm](kingmaker-bestiary-items/YJR2VSzinZXYbwkd.htm)|Greatclub|+1,striking|
|[YjsZVkEOo90ZoPjJ.htm](kingmaker-bestiary-items/YjsZVkEOo90ZoPjJ.htm)|Wand of Heal (Level 3)|
|[YmkfgSgetEPoWRKA.htm](kingmaker-bestiary-items/YmkfgSgetEPoWRKA.htm)|Religious Symbol of Gyronna|
|[ynDcPwSchoDdTX0R.htm](kingmaker-bestiary-items/ynDcPwSchoDdTX0R.htm)|Dimension Door (At Will)|
|[YtRTdI3UAqH6vZVL.htm](kingmaker-bestiary-items/YtRTdI3UAqH6vZVL.htm)|Elemental Form (Water only)|
|[z97KUUoWcBLn9aga.htm](kingmaker-bestiary-items/z97KUUoWcBLn9aga.htm)|+1 Leather Armor|
|[Zb5uVpmuHCmlnVkv.htm](kingmaker-bestiary-items/Zb5uVpmuHCmlnVkv.htm)|+2 Resilient Mithral Breastplate|
|[zGifR2PUMCuF3jab.htm](kingmaker-bestiary-items/zGifR2PUMCuF3jab.htm)|Religious Symbol of Yog-Sothoth|
|[ZixFLnOGrgPgMXgK.htm](kingmaker-bestiary-items/ZixFLnOGrgPgMXgK.htm)|Shortbow|+1,striking|
|[ZozpoIHVu6M6hsy6.htm](kingmaker-bestiary-items/ZozpoIHVu6M6hsy6.htm)|+2 Resilient Hide Armor|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00zSyT0a2tdDQnmZ.htm](kingmaker-bestiary-items/00zSyT0a2tdDQnmZ.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[093ZnioHRqaohRiq.htm](kingmaker-bestiary-items/093ZnioHRqaohRiq.htm)|Throw Rock|Arrojar roca|modificada|
|[0BMCA9P7mLMAMs10.htm](kingmaker-bestiary-items/0BMCA9P7mLMAMs10.htm)|Sunder Objects|Quebrar objetos|modificada|
|[0F1cIENDKxVfgMvZ.htm](kingmaker-bestiary-items/0F1cIENDKxVfgMvZ.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[0gCnA8mF12gNsipi.htm](kingmaker-bestiary-items/0gCnA8mF12gNsipi.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[0ivwHldip6F8T0gA.htm](kingmaker-bestiary-items/0ivwHldip6F8T0gA.htm)|Attack of Opportunity (Fangs Only)|Ataque de oportunidad (sólo colmillos)|modificada|
|[0KLll6op6LwSp19s.htm](kingmaker-bestiary-items/0KLll6op6LwSp19s.htm)|Hydra Regeneration|Regeneración de hidra|modificada|
|[0L6qwj5JpEiwbnph.htm](kingmaker-bestiary-items/0L6qwj5JpEiwbnph.htm)|Storm of Jaws|Tormenta de mordiscos|modificada|
|[0mCmhGYAI47n4kdy.htm](kingmaker-bestiary-items/0mCmhGYAI47n4kdy.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[0Pe1uBGVehRLhBim.htm](kingmaker-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[0pHw83Qkw39rMcIx.htm](kingmaker-bestiary-items/0pHw83Qkw39rMcIx.htm)|Contingency|Contingencia|modificada|
|[0rYILxhM4TiZQaeA.htm](kingmaker-bestiary-items/0rYILxhM4TiZQaeA.htm)|Sudden Charge|Carga súbita|modificada|
|[0sXAwqL8Tb6XqJ0H.htm](kingmaker-bestiary-items/0sXAwqL8Tb6XqJ0H.htm)|Lantern King's Glow|Resplandor del Rey Linterna|modificada|
|[0TyxIsxCmJlWFTk6.htm](kingmaker-bestiary-items/0TyxIsxCmJlWFTk6.htm)|Evasion|Evasión|modificada|
|[0uxUMo5bNxKA5eIg.htm](kingmaker-bestiary-items/0uxUMo5bNxKA5eIg.htm)|Change Shape|Change Shape|modificada|
|[0VYJH6vuFSZm9NIX.htm](kingmaker-bestiary-items/0VYJH6vuFSZm9NIX.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[0WYsG1PdrVoJN2Ue.htm](kingmaker-bestiary-items/0WYsG1PdrVoJN2Ue.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[137OFGe9srUCXPpN.htm](kingmaker-bestiary-items/137OFGe9srUCXPpN.htm)|Closing Volley|Volea de cierre|modificada|
|[1AbBtZhPY7lYpSm0.htm](kingmaker-bestiary-items/1AbBtZhPY7lYpSm0.htm)|Goblin Pox|Viruela de goblin|modificada|
|[1AemwaFBoSAJ1UHj.htm](kingmaker-bestiary-items/1AemwaFBoSAJ1UHj.htm)|Change Shape|Change Shape|modificada|
|[1BD5jRohvhbGdDwq.htm](kingmaker-bestiary-items/1BD5jRohvhbGdDwq.htm)|Fangs|Colmillos|modificada|
|[1bKTp47s4lhPF0Ix.htm](kingmaker-bestiary-items/1bKTp47s4lhPF0Ix.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1ChXT4DL7N5JhHEq.htm](kingmaker-bestiary-items/1ChXT4DL7N5JhHEq.htm)|Tail|Tail|modificada|
|[1CVzuimA1fYyvCBq.htm](kingmaker-bestiary-items/1CVzuimA1fYyvCBq.htm)|Cruel Anatomist|Anatomista Cruel|modificada|
|[1fxLVAyfm2PZ7Zxg.htm](kingmaker-bestiary-items/1fxLVAyfm2PZ7Zxg.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[1Ht8zfzmHL4tULd5.htm](kingmaker-bestiary-items/1Ht8zfzmHL4tULd5.htm)|Shield Rip and Chew|Escudo rasgar y masticar|modificada|
|[1IvCoMHs0qGTlw8J.htm](kingmaker-bestiary-items/1IvCoMHs0qGTlw8J.htm)|Ripping Gaze|Mirada desgarradora|modificada|
|[1jsdM4qpD0smQsMV.htm](kingmaker-bestiary-items/1jsdM4qpD0smQsMV.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[1kX9bpy0yIk83pGw.htm](kingmaker-bestiary-items/1kX9bpy0yIk83pGw.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[1lrxLnSpTkBwcSli.htm](kingmaker-bestiary-items/1lrxLnSpTkBwcSli.htm)|Ghostly Battle Axe|Ghostly Battle Axe|modificada|
|[1Lvngo9gteYE4ep9.htm](kingmaker-bestiary-items/1Lvngo9gteYE4ep9.htm)|Dagger|Daga|modificada|
|[1PdKOeeXtOPcFYCb.htm](kingmaker-bestiary-items/1PdKOeeXtOPcFYCb.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[1RIzUNU0v2cfgsjD.htm](kingmaker-bestiary-items/1RIzUNU0v2cfgsjD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1UmNfowZ4HWxS9rx.htm](kingmaker-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|Explosión devastadora|modificada|
|[1V9pafG73kkZDmNM.htm](kingmaker-bestiary-items/1V9pafG73kkZDmNM.htm)|Negative Healing|Curación negativa|modificada|
|[1V9uaQOnCMGgMxl2.htm](kingmaker-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1VH2ArAHvIhMi57j.htm](kingmaker-bestiary-items/1VH2ArAHvIhMi57j.htm)|Fist|Puño|modificada|
|[1VqUGL35cW3NbTOz.htm](kingmaker-bestiary-items/1VqUGL35cW3NbTOz.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[1Y7PAPhO3aVpgKoq.htm](kingmaker-bestiary-items/1Y7PAPhO3aVpgKoq.htm)|Ghostly Rapier|Ghostly Rapier|modificada|
|[27wudVSiScZejHwt.htm](kingmaker-bestiary-items/27wudVSiScZejHwt.htm)|Negative Healing|Curación negativa|modificada|
|[28VxmNDvpa40sH4P.htm](kingmaker-bestiary-items/28VxmNDvpa40sH4P.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[2A5WqL14A7eKaElj.htm](kingmaker-bestiary-items/2A5WqL14A7eKaElj.htm)|Claw|Garra|modificada|
|[2bthXOLfHVbIarRo.htm](kingmaker-bestiary-items/2bthXOLfHVbIarRo.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2c38HSvjdnaJgeI2.htm](kingmaker-bestiary-items/2c38HSvjdnaJgeI2.htm)|Awesome Blow|Golpe asombroso|modificada|
|[2dHPDZu3BYN1GauB.htm](kingmaker-bestiary-items/2dHPDZu3BYN1GauB.htm)|No Breath|No Breath|modificada|
|[2ECIwvcXVQ6w8vi5.htm](kingmaker-bestiary-items/2ECIwvcXVQ6w8vi5.htm)|Punishing Momentum|Impulso castigador|modificada|
|[2GNdGYBXQOD8VlC7.htm](kingmaker-bestiary-items/2GNdGYBXQOD8VlC7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2hG90mAiBnKHWoxc.htm](kingmaker-bestiary-items/2hG90mAiBnKHWoxc.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[2hjq0oo2EsTcXPJe.htm](kingmaker-bestiary-items/2hjq0oo2EsTcXPJe.htm)|Tail|Tail|modificada|
|[2I9ZCEqUKoC0AAqQ.htm](kingmaker-bestiary-items/2I9ZCEqUKoC0AAqQ.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[2IqWrJj9edOVS3Gp.htm](kingmaker-bestiary-items/2IqWrJj9edOVS3Gp.htm)|Triple Opportunity|Oportunista triple|modificada|
|[2lGYb3ZO8pwxgMhD.htm](kingmaker-bestiary-items/2lGYb3ZO8pwxgMhD.htm)|Focus Gaze|Centrar mirada|modificada|
|[2m9M9QruB9jYdEjd.htm](kingmaker-bestiary-items/2m9M9QruB9jYdEjd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2n6nRtgDNRWONgvp.htm](kingmaker-bestiary-items/2n6nRtgDNRWONgvp.htm)|Drain Luck|Drenar Suerte|modificada|
|[2nd3Ete1Wir3K3bj.htm](kingmaker-bestiary-items/2nd3Ete1Wir3K3bj.htm)|Wave|Wave|modificada|
|[2pEzXozrOXVdVg0A.htm](kingmaker-bestiary-items/2pEzXozrOXVdVg0A.htm)|Life Bloom|Life Bloom|modificada|
|[2prGxYGRkpCNqmdI.htm](kingmaker-bestiary-items/2prGxYGRkpCNqmdI.htm)|Shelyn's Thanks|Shelyn's Thanks|modificada|
|[2rAKighB6Ywhk5s3.htm](kingmaker-bestiary-items/2rAKighB6Ywhk5s3.htm)|Shock|Electrizante|modificada|
|[2YaktCYgfrnvuUbE.htm](kingmaker-bestiary-items/2YaktCYgfrnvuUbE.htm)|Slash Throat|Tajo Garganta|modificada|
|[312Fug37MtFpvG0P.htm](kingmaker-bestiary-items/312Fug37MtFpvG0P.htm)|Tongue|Lengua|modificada|
|[3AidCm7XJT968Swi.htm](kingmaker-bestiary-items/3AidCm7XJT968Swi.htm)|Choke|Choke|modificada|
|[3ANSkECTu3y2Lt3C.htm](kingmaker-bestiary-items/3ANSkECTu3y2Lt3C.htm)|Dogslicer|Dogslicer|modificada|
|[3BSxL95mz6QjoWi1.htm](kingmaker-bestiary-items/3BSxL95mz6QjoWi1.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[3Eksrmh390T4SrP7.htm](kingmaker-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|Inmunidad Mágica|modificada|
|[3gmJPQIKZq7dcelo.htm](kingmaker-bestiary-items/3gmJPQIKZq7dcelo.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[3GqX6rg7gxLsmuvd.htm](kingmaker-bestiary-items/3GqX6rg7gxLsmuvd.htm)|Rock|Roca|modificada|
|[3hPEjn6DWOXtuQ2I.htm](kingmaker-bestiary-items/3hPEjn6DWOXtuQ2I.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[3jVmH3McNsX8B0Od.htm](kingmaker-bestiary-items/3jVmH3McNsX8B0Od.htm)|Jaws|Fauces|modificada|
|[3pzII2xLdduTjJrG.htm](kingmaker-bestiary-items/3pzII2xLdduTjJrG.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[3t3ulXs6r1EL76F0.htm](kingmaker-bestiary-items/3t3ulXs6r1EL76F0.htm)|Breath Weapon|Breath Weapon|modificada|
|[3TfIMe12rJpDf9LZ.htm](kingmaker-bestiary-items/3TfIMe12rJpDf9LZ.htm)|Self-Loathing|Autodesprecio|modificada|
|[3tyBaMgyINlZCj76.htm](kingmaker-bestiary-items/3tyBaMgyINlZCj76.htm)|Fist|Puño|modificada|
|[3VDIY4vCKlW0qvFl.htm](kingmaker-bestiary-items/3VDIY4vCKlW0qvFl.htm)|Fist|Puño|modificada|
|[3VX47OPvg5PAjzsD.htm](kingmaker-bestiary-items/3VX47OPvg5PAjzsD.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[3wMfK5zZSqFhhTgs.htm](kingmaker-bestiary-items/3wMfK5zZSqFhhTgs.htm)|Primordial Roar|Primordial Roar|modificada|
|[3Ygn2F1yeN3UTO2S.htm](kingmaker-bestiary-items/3Ygn2F1yeN3UTO2S.htm)|Pain|Dolor|modificada|
|[3ZL7oyw6PKNMzEhf.htm](kingmaker-bestiary-items/3ZL7oyw6PKNMzEhf.htm)|Grant Desire|Grant Desire|modificada|
|[45aANCYgNwQS8Ega.htm](kingmaker-bestiary-items/45aANCYgNwQS8Ega.htm)|Hooktongue Venom|Hooktongue Venom|modificada|
|[48zaDpaZFzUKLJHL.htm](kingmaker-bestiary-items/48zaDpaZFzUKLJHL.htm)|Sudden Charge|Carga súbita|modificada|
|[49xv61tdfV6J6P29.htm](kingmaker-bestiary-items/49xv61tdfV6J6P29.htm)|Discorporate|Discorporate|modificada|
|[4bbZ6NaZSeW5sE1f.htm](kingmaker-bestiary-items/4bbZ6NaZSeW5sE1f.htm)|Trident|Trident|modificada|
|[4BNXIgVORiBDrL95.htm](kingmaker-bestiary-items/4BNXIgVORiBDrL95.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[4C0eLAnhYCsQz9KO.htm](kingmaker-bestiary-items/4C0eLAnhYCsQz9KO.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[4EsbaXbLbmfGNLmn.htm](kingmaker-bestiary-items/4EsbaXbLbmfGNLmn.htm)|Constant Spells|Constant Spells|modificada|
|[4gO7xPscU8GkItt4.htm](kingmaker-bestiary-items/4gO7xPscU8GkItt4.htm)|Spear|Lanza|modificada|
|[4gP2dSNnL1rudfAt.htm](kingmaker-bestiary-items/4gP2dSNnL1rudfAt.htm)|Tongue|Lengua|modificada|
|[4hjXOaTYgTu9yoad.htm](kingmaker-bestiary-items/4hjXOaTYgTu9yoad.htm)|Claws|Garras|modificada|
|[4mK32Hq1EHPgHPW4.htm](kingmaker-bestiary-items/4mK32Hq1EHPgHPW4.htm)|Dread Striker|Hostigador temible|modificada|
|[4n19omVeZZAAGXBk.htm](kingmaker-bestiary-items/4n19omVeZZAAGXBk.htm)|Coven|Coven|modificada|
|[4N1vw9fFoiAgxSn1.htm](kingmaker-bestiary-items/4N1vw9fFoiAgxSn1.htm)|Flying Strafe|Pasada en vuelo|modificada|
|[4OphdUKxBpxPyBKN.htm](kingmaker-bestiary-items/4OphdUKxBpxPyBKN.htm)|Tail Drag|Tail Drag|modificada|
|[4QNftOG1O0v3IrVA.htm](kingmaker-bestiary-items/4QNftOG1O0v3IrVA.htm)|Trip Up|Zancadilla|modificada|
|[4S2A9APP31jlMK4Q.htm](kingmaker-bestiary-items/4S2A9APP31jlMK4Q.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[4SgwfAqj7PXlo4NI.htm](kingmaker-bestiary-items/4SgwfAqj7PXlo4NI.htm)|Kob's Ruinous Strike|Golpe Ruinoso de Kob|modificada|
|[4WaSsGcstf79Y44R.htm](kingmaker-bestiary-items/4WaSsGcstf79Y44R.htm)|Jaws|Fauces|modificada|
|[4WbCPkQrjR5xvIUs.htm](kingmaker-bestiary-items/4WbCPkQrjR5xvIUs.htm)|Temporal Strikes|Golpes Temporales|modificada|
|[4z1JD9bhf1R5wpmX.htm](kingmaker-bestiary-items/4z1JD9bhf1R5wpmX.htm)|Trident|Trident|modificada|
|[54mSt8tnFXfSUjhU.htm](kingmaker-bestiary-items/54mSt8tnFXfSUjhU.htm)|Cling|Cling|modificada|
|[55Hvc61MUgVoahwb.htm](kingmaker-bestiary-items/55Hvc61MUgVoahwb.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[5AVIS7Cfz12DClSG.htm](kingmaker-bestiary-items/5AVIS7Cfz12DClSG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5dKTN8eobwXG2iGV.htm](kingmaker-bestiary-items/5dKTN8eobwXG2iGV.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[5dvPmX8OUqLml3m6.htm](kingmaker-bestiary-items/5dvPmX8OUqLml3m6.htm)|Claw|Garra|modificada|
|[5EExyaDy8xArOFh0.htm](kingmaker-bestiary-items/5EExyaDy8xArOFh0.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[5EvbvNghD1THDwUR.htm](kingmaker-bestiary-items/5EvbvNghD1THDwUR.htm)|Negative Healing|Curación negativa|modificada|
|[5f6mJgvaVADXCaAz.htm](kingmaker-bestiary-items/5f6mJgvaVADXCaAz.htm)|Scent|Scent|modificada|
|[5G6Es9pOfgRiXtpy.htm](kingmaker-bestiary-items/5G6Es9pOfgRiXtpy.htm)|Power Attack|Ataque poderoso|modificada|
|[5hqdjh4Jn5wtzqGs.htm](kingmaker-bestiary-items/5hqdjh4Jn5wtzqGs.htm)|Tendril|Tendril|modificada|
|[5LclMp6q589Sl0Mn.htm](kingmaker-bestiary-items/5LclMp6q589Sl0Mn.htm)|Claw|Garra|modificada|
|[5LgL3UMxFZRmIgXy.htm](kingmaker-bestiary-items/5LgL3UMxFZRmIgXy.htm)|Vulture Beak|Pico de buitre|modificada|
|[5m1MoZTkyC5yL2jL.htm](kingmaker-bestiary-items/5m1MoZTkyC5yL2jL.htm)|Double Strike|Golpe Doble|modificada|
|[5nbfijONNe4T5HFM.htm](kingmaker-bestiary-items/5nbfijONNe4T5HFM.htm)|Draining Touch|Toque Drenante|modificada|
|[5O8YD9thn6myrfWy.htm](kingmaker-bestiary-items/5O8YD9thn6myrfWy.htm)|Pollen Burst|Ráfaga de Polen|modificada|
|[5qgx9qhXAceS6uLh.htm](kingmaker-bestiary-items/5qgx9qhXAceS6uLh.htm)|Enraged Cunning|Astucia furibunda|modificada|
|[5rxGka9M6pYlIoVb.htm](kingmaker-bestiary-items/5rxGka9M6pYlIoVb.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[5uF0UI8Ihj8qhfy5.htm](kingmaker-bestiary-items/5uF0UI8Ihj8qhfy5.htm)|Support Benefit|Beneficio de Apoyo|modificada|
|[5uI2L7wMgW34OuXg.htm](kingmaker-bestiary-items/5uI2L7wMgW34OuXg.htm)|Vengeful Rage|Furia vengativa|modificada|
|[5ulvx0zqprJryzvq.htm](kingmaker-bestiary-items/5ulvx0zqprJryzvq.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[5UTWzAXIsjMqThHw.htm](kingmaker-bestiary-items/5UTWzAXIsjMqThHw.htm)|Unfair Aim|Puntería desleal|modificada|
|[5w3fouC3WVuTBtWK.htm](kingmaker-bestiary-items/5w3fouC3WVuTBtWK.htm)|Trample|Trample|modificada|
|[5xktm2PrBdKEX5oh.htm](kingmaker-bestiary-items/5xktm2PrBdKEX5oh.htm)|Dagger|Daga|modificada|
|[5z29EkkGtyaSoB1a.htm](kingmaker-bestiary-items/5z29EkkGtyaSoB1a.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[61GyOh9BdFBPZCUD.htm](kingmaker-bestiary-items/61GyOh9BdFBPZCUD.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[61pWFfMaO4glQNlI.htm](kingmaker-bestiary-items/61pWFfMaO4glQNlI.htm)|Fetch Weapon|Fetch Weapon|modificada|
|[66WdJU8fGY3RHFZN.htm](kingmaker-bestiary-items/66WdJU8fGY3RHFZN.htm)|Blinded|Ceguera|modificada|
|[6aSDaFMEcz58NAqP.htm](kingmaker-bestiary-items/6aSDaFMEcz58NAqP.htm)|Despairing Breath|Aliento desesperado|modificada|
|[6cprEKOmbMfF47Lu.htm](kingmaker-bestiary-items/6cprEKOmbMfF47Lu.htm)|Shame|Vergüenza|modificada|
|[6e66VeO6FBdw8Fjn.htm](kingmaker-bestiary-items/6e66VeO6FBdw8Fjn.htm)|Hatchet|Hacha|modificada|
|[6ESE4WVjACGF6VGi.htm](kingmaker-bestiary-items/6ESE4WVjACGF6VGi.htm)|Coven Spells|Coven Spells|modificada|
|[6gDHZ6VPfZDrKCnp.htm](kingmaker-bestiary-items/6gDHZ6VPfZDrKCnp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[6gwNFiImkw2H0QWA.htm](kingmaker-bestiary-items/6gwNFiImkw2H0QWA.htm)|Grab|Agarrado|modificada|
|[6ILB5ny6782Ujs7o.htm](kingmaker-bestiary-items/6ILB5ny6782Ujs7o.htm)|Change Shape|Change Shape|modificada|
|[6k6LtJmrZAfTImSh.htm](kingmaker-bestiary-items/6k6LtJmrZAfTImSh.htm)|Staff|Báculo|modificada|
|[6k7jBeAiFp27Wcv1.htm](kingmaker-bestiary-items/6k7jBeAiFp27Wcv1.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[6kvbcAoDPilYf0fI.htm](kingmaker-bestiary-items/6kvbcAoDPilYf0fI.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6mO0FIwfW82Cszej.htm](kingmaker-bestiary-items/6mO0FIwfW82Cszej.htm)|Curse of the Weretiger|Maldición del Weretiger|modificada|
|[6MwtMijY3R7DbxaJ.htm](kingmaker-bestiary-items/6MwtMijY3R7DbxaJ.htm)|Ghostly Attack|Ghostly Attack|modificada|
|[6ORqPbdQEDKtPs55.htm](kingmaker-bestiary-items/6ORqPbdQEDKtPs55.htm)|Grab|Agarrado|modificada|
|[6qAaC77CfOknbaMv.htm](kingmaker-bestiary-items/6qAaC77CfOknbaMv.htm)|Quick Recovery|Recuperación rápida|modificada|
|[6qaFJjq9JjlhZbaw.htm](kingmaker-bestiary-items/6qaFJjq9JjlhZbaw.htm)|Ogre Spider Venom|Veneno de Araña Ogro|modificada|
|[6qhXb73HUuWsXVlw.htm](kingmaker-bestiary-items/6qhXb73HUuWsXVlw.htm)|Aldori Riposte|Aldori Riposte|modificada|
|[6QtfXfEtO9Jk9Ceg.htm](kingmaker-bestiary-items/6QtfXfEtO9Jk9Ceg.htm)|Greensight|Greensight|modificada|
|[6SHvRrPDTa74t0cD.htm](kingmaker-bestiary-items/6SHvRrPDTa74t0cD.htm)|Rock|Roca|modificada|
|[6SNxPBIEy6G6pJ5S.htm](kingmaker-bestiary-items/6SNxPBIEy6G6pJ5S.htm)|Fist|Puño|modificada|
|[6UhOo5jcpQz76GcZ.htm](kingmaker-bestiary-items/6UhOo5jcpQz76GcZ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[6VrL0WH1rLsYXFpD.htm](kingmaker-bestiary-items/6VrL0WH1rLsYXFpD.htm)|Claw|Garra|modificada|
|[6XfWpghskVUmJSVG.htm](kingmaker-bestiary-items/6XfWpghskVUmJSVG.htm)|Aldori Dueling Sword|Aldori Dueling Sword|modificada|
|[6xM67gan9MlmS1dN.htm](kingmaker-bestiary-items/6xM67gan9MlmS1dN.htm)|Tilt and Roll|Inclinar y Rodar|modificada|
|[6YFC1LK4zBkiKg1N.htm](kingmaker-bestiary-items/6YFC1LK4zBkiKg1N.htm)|Beyond the Barrier|Más allá de la barrera|modificada|
|[6znbpXWxevbdvTl2.htm](kingmaker-bestiary-items/6znbpXWxevbdvTl2.htm)|Rip and Rend|Rasgar y Rasgadura|modificada|
|[70wPP2Jajb7NN04d.htm](kingmaker-bestiary-items/70wPP2Jajb7NN04d.htm)|Change Shape|Change Shape|modificada|
|[73DVUYU49Oqo474k.htm](kingmaker-bestiary-items/73DVUYU49Oqo474k.htm)|Nymph's Tragedy|Nymph's Tragedy|modificada|
|[73zfyyrBBuzM9Dh9.htm](kingmaker-bestiary-items/73zfyyrBBuzM9Dh9.htm)|Terrifying Croak|Aterrador Croak|modificada|
|[77XHZv9nEQwJTaiO.htm](kingmaker-bestiary-items/77XHZv9nEQwJTaiO.htm)|Drain Oculus|Drenar Oculus|modificada|
|[79pJPNMXOyazfHy6.htm](kingmaker-bestiary-items/79pJPNMXOyazfHy6.htm)|Accelerated Existence|Existencia Acelerada|modificada|
|[7aIGZtbifYYVXpiV.htm](kingmaker-bestiary-items/7aIGZtbifYYVXpiV.htm)|Grab|Agarrado|modificada|
|[7CQGpLGBAg2Jbtjv.htm](kingmaker-bestiary-items/7CQGpLGBAg2Jbtjv.htm)|Three Headed|Tres cabezas|modificada|
|[7ERyflHHinGfODO2.htm](kingmaker-bestiary-items/7ERyflHHinGfODO2.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[7ff6eVjIgZUezVKm.htm](kingmaker-bestiary-items/7ff6eVjIgZUezVKm.htm)|Jabberwock Bloodline|Linaje de Jabberwock|modificada|
|[7fPsSiHuHlM11fhF.htm](kingmaker-bestiary-items/7fPsSiHuHlM11fhF.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[7fVgYQK2ZSqvG5KV.htm](kingmaker-bestiary-items/7fVgYQK2ZSqvG5KV.htm)|Twin Feint|Doble finta gemela|modificada|
|[7fwg0KKHJSx50lBW.htm](kingmaker-bestiary-items/7fwg0KKHJSx50lBW.htm)|Battlefield Command|Orden imperiosa del campo de batalla|modificada|
|[7iMYJiq8cs6BSSdN.htm](kingmaker-bestiary-items/7iMYJiq8cs6BSSdN.htm)|Nature's Edge|Ventaja de la Naturaleza|modificada|
|[7j0otQgc8z5zzALv.htm](kingmaker-bestiary-items/7j0otQgc8z5zzALv.htm)|Site Bound|Ligado a una ubicación|modificada|
|[7kGu1ORgE4Nay79B.htm](kingmaker-bestiary-items/7kGu1ORgE4Nay79B.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[7Lzy2YrsBlDo3O23.htm](kingmaker-bestiary-items/7Lzy2YrsBlDo3O23.htm)|Regeneration 25 (Deactivated by Acid or Fire)|Regeneración 25 (Desactivado por Ácido o Fuego)|modificada|
|[7MElwF26Lym91Wch.htm](kingmaker-bestiary-items/7MElwF26Lym91Wch.htm)|Longsword|Longsword|modificada|
|[7N9zMdYcPW2J4Hzt.htm](kingmaker-bestiary-items/7N9zMdYcPW2J4Hzt.htm)|Nyrissa's Favor|Favor de Nyrissa|modificada|
|[7O2rhLBXoMyayaTV.htm](kingmaker-bestiary-items/7O2rhLBXoMyayaTV.htm)|Distracting Yipping|Yipping de distracción|modificada|
|[7omOmpvwPwKMmBfB.htm](kingmaker-bestiary-items/7omOmpvwPwKMmBfB.htm)|Intimidating Strike|Golpe intimidante|modificada|
|[7pIVt1wT5IWY1dOP.htm](kingmaker-bestiary-items/7pIVt1wT5IWY1dOP.htm)|Flurry of Blows|Ráfaga de golpes.|modificada|
|[7pRxaQFmTGlbnKEr.htm](kingmaker-bestiary-items/7pRxaQFmTGlbnKEr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7PTn0lPspj6Mo8E7.htm](kingmaker-bestiary-items/7PTn0lPspj6Mo8E7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7QpzxU7FsF8lN6FP.htm](kingmaker-bestiary-items/7QpzxU7FsF8lN6FP.htm)|Shameful Memories|Shameful Memories|modificada|
|[7s2ezouHGaqFCuhW.htm](kingmaker-bestiary-items/7s2ezouHGaqFCuhW.htm)|Corrupting Gaze|Corrupting Gaze|modificada|
|[7SDmYIdMAER2dLGK.htm](kingmaker-bestiary-items/7SDmYIdMAER2dLGK.htm)|Invoke Stisshak|Invocar Stisshak|modificada|
|[7SmLN0kqsTk5kyLK.htm](kingmaker-bestiary-items/7SmLN0kqsTk5kyLK.htm)|Crystal Scimitar|Cimitarra de Cristal|modificada|
|[7TT0BqubK5ErNyN3.htm](kingmaker-bestiary-items/7TT0BqubK5ErNyN3.htm)|Ranseur|Ranseur|modificada|
|[7tW8esj9IVdwLqwS.htm](kingmaker-bestiary-items/7tW8esj9IVdwLqwS.htm)|Primal Weapon|Arma Primal|modificada|
|[7Y8YE3i8NiZAOPkt.htm](kingmaker-bestiary-items/7Y8YE3i8NiZAOPkt.htm)|Collapse|Colapso|modificada|
|[7ZhJ9zRnsiGY2LMq.htm](kingmaker-bestiary-items/7ZhJ9zRnsiGY2LMq.htm)|Change Shape|Change Shape|modificada|
|[83BVCoUzow3GIr5W.htm](kingmaker-bestiary-items/83BVCoUzow3GIr5W.htm)|Claw|Garra|modificada|
|[873UBJDHNOwZqAn0.htm](kingmaker-bestiary-items/873UBJDHNOwZqAn0.htm)|Longsword|Longsword|modificada|
|[88hwkEhv8cx338VM.htm](kingmaker-bestiary-items/88hwkEhv8cx338VM.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[89ANJyvglhWZiKZv.htm](kingmaker-bestiary-items/89ANJyvglhWZiKZv.htm)|Deploy Flytraps|Deploy Flytraps|modificada|
|[8F42ZUECGwbl9nmU.htm](kingmaker-bestiary-items/8F42ZUECGwbl9nmU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8FamBadz3fY1Ktf1.htm](kingmaker-bestiary-items/8FamBadz3fY1Ktf1.htm)|Javelin|Javelin|modificada|
|[8fXr0ygvqmKMJQ7y.htm](kingmaker-bestiary-items/8fXr0ygvqmKMJQ7y.htm)|Ambush Strike|Emboscar Golpe|modificada|
|[8GzTttsR8tPUpy3a.htm](kingmaker-bestiary-items/8GzTttsR8tPUpy3a.htm)|Manifest Fetch Weapon|Manifiesto Fetch Arma|modificada|
|[8Ib2hxgIftGdnnC4.htm](kingmaker-bestiary-items/8Ib2hxgIftGdnnC4.htm)|Occult Focus Spells|Conjuros de foco oculto|modificada|
|[8k6w6YpDwJuyU7Lj.htm](kingmaker-bestiary-items/8k6w6YpDwJuyU7Lj.htm)|Twin Feint|Doble finta gemela|modificada|
|[8lbGHlEEQstmS0rd.htm](kingmaker-bestiary-items/8lbGHlEEQstmS0rd.htm)|Club|Club|modificada|
|[8PkUYYXKF3YdJiBM.htm](kingmaker-bestiary-items/8PkUYYXKF3YdJiBM.htm)|Draconic Bite|Mordisco dracónico|modificada|
|[8R3jmOcyIfgNNdKr.htm](kingmaker-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|Curación negativa|modificada|
|[8RIFanFu6JDiIRH1.htm](kingmaker-bestiary-items/8RIFanFu6JDiIRH1.htm)|Thrash|Sacudirse|modificada|
|[8s4nFCFF2c0eNDnN.htm](kingmaker-bestiary-items/8s4nFCFF2c0eNDnN.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[8SCH000Zv6X9BFPf.htm](kingmaker-bestiary-items/8SCH000Zv6X9BFPf.htm)|Fist|Puño|modificada|
|[8T1NpFqpuMXKzB51.htm](kingmaker-bestiary-items/8T1NpFqpuMXKzB51.htm)|Jaws|Fauces|modificada|
|[8W67Bxaonilv5KO0.htm](kingmaker-bestiary-items/8W67Bxaonilv5KO0.htm)|Exhale Miasma|Exhalar miasma|modificada|
|[8wBdJ7S9sRjGCmmT.htm](kingmaker-bestiary-items/8wBdJ7S9sRjGCmmT.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[8wo9GPXQVySZtBb5.htm](kingmaker-bestiary-items/8wo9GPXQVySZtBb5.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[8x5ve1XWJQ7yIA7F.htm](kingmaker-bestiary-items/8x5ve1XWJQ7yIA7F.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[8xYRzzHeY6uZztPe.htm](kingmaker-bestiary-items/8xYRzzHeY6uZztPe.htm)|Unstoppable Charge|Carga Imparable|modificada|
|[8yXu3fI5NfUS8lDH.htm](kingmaker-bestiary-items/8yXu3fI5NfUS8lDH.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[8zIAec8NlrwPEbab.htm](kingmaker-bestiary-items/8zIAec8NlrwPEbab.htm)|Trident|Trident|modificada|
|[907AYCZJGYIzNC2a.htm](kingmaker-bestiary-items/907AYCZJGYIzNC2a.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[95EZclhH2zhKgoxm.htm](kingmaker-bestiary-items/95EZclhH2zhKgoxm.htm)|Dagger|Daga|modificada|
|[9a3n2YfibJlQxctv.htm](kingmaker-bestiary-items/9a3n2YfibJlQxctv.htm)|Unseen Sight|Unseen Sight|modificada|
|[9AVnb90miimzc7rS.htm](kingmaker-bestiary-items/9AVnb90miimzc7rS.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[9bomxaBEHSLvRrkq.htm](kingmaker-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|Estatua|modificada|
|[9cHX93V0XpVb7b7b.htm](kingmaker-bestiary-items/9cHX93V0XpVb7b7b.htm)|Lashing Tongues|Lenguas fustigantes|modificada|
|[9F78hYb3XMI7z4ai.htm](kingmaker-bestiary-items/9F78hYb3XMI7z4ai.htm)|Rapier|Estoque|modificada|
|[9il80y0VHh8ZAWdY.htm](kingmaker-bestiary-items/9il80y0VHh8ZAWdY.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[9jMNfcA0u8pECJhS.htm](kingmaker-bestiary-items/9jMNfcA0u8pECJhS.htm)|Dagger|Daga|modificada|
|[9JQwWcFOpgrzmEIy.htm](kingmaker-bestiary-items/9JQwWcFOpgrzmEIy.htm)|Dagger|Daga|modificada|
|[9k7o784yI4Bka8ys.htm](kingmaker-bestiary-items/9k7o784yI4Bka8ys.htm)|Crossbow|Ballesta|modificada|
|[9khUkSjPZRFIilUZ.htm](kingmaker-bestiary-items/9khUkSjPZRFIilUZ.htm)|Rend|Rasgadura|modificada|
|[9kVpK6J5EVHZcp0K.htm](kingmaker-bestiary-items/9kVpK6J5EVHZcp0K.htm)|Major Staff of Transmutation|Bastón de transmutación, superior.|modificada|
|[9ODQkquyZvS7PFNK.htm](kingmaker-bestiary-items/9ODQkquyZvS7PFNK.htm)|Knockdown|Derribo|modificada|
|[9OiXFTpXGJPjEwUL.htm](kingmaker-bestiary-items/9OiXFTpXGJPjEwUL.htm)|Grab|Agarrado|modificada|
|[9oO34owbXFTjCGng.htm](kingmaker-bestiary-items/9oO34owbXFTjCGng.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9PUgsYyJWDJ9J0sE.htm](kingmaker-bestiary-items/9PUgsYyJWDJ9J0sE.htm)|+1 Status vs. Posion|+1 situación vs posición.|modificada|
|[9qzHAK95CCa74dtM.htm](kingmaker-bestiary-items/9qzHAK95CCa74dtM.htm)|Absorb the Bloom|Absorb the Bloom|modificada|
|[9rDjTECEOqQb11SQ.htm](kingmaker-bestiary-items/9rDjTECEOqQb11SQ.htm)|Dagger|Daga|modificada|
|[9RDkmFyfywWSOj2V.htm](kingmaker-bestiary-items/9RDkmFyfywWSOj2V.htm)|Breath Weapon|Breath Weapon|modificada|
|[9S7ODMh2ZM6NXVpF.htm](kingmaker-bestiary-items/9S7ODMh2ZM6NXVpF.htm)|Enfeebling Humors|Humores debilitantes|modificada|
|[9ST6BDxKH8vXvpEq.htm](kingmaker-bestiary-items/9ST6BDxKH8vXvpEq.htm)|Attack of Opportunity (Worm Jaws Only)|Ataque de oportunidad (Sólo Fauces de Gusano)|modificada|
|[9uNCXDfw6yzI9yL5.htm](kingmaker-bestiary-items/9uNCXDfw6yzI9yL5.htm)|Release Spell|Hechizo Soltar|modificada|
|[9VA9Qjcx5YJxs4P1.htm](kingmaker-bestiary-items/9VA9Qjcx5YJxs4P1.htm)|Jangle the Chain|Jangle the Chain|modificada|
|[9VgJpOQ6xJ9gTgCU.htm](kingmaker-bestiary-items/9VgJpOQ6xJ9gTgCU.htm)|Bard Spontaneous Spells|Hechizos Espontáneos de Bardo|modificada|
|[9yPuatISy0aUTsVE.htm](kingmaker-bestiary-items/9yPuatISy0aUTsVE.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[A33mGw1DCgRJRDSe.htm](kingmaker-bestiary-items/A33mGw1DCgRJRDSe.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[a7WtcnRg6JlsVIDg.htm](kingmaker-bestiary-items/a7WtcnRg6JlsVIDg.htm)|Maul|Zarpazo doble|modificada|
|[A8rxcOUACAmsVmTH.htm](kingmaker-bestiary-items/A8rxcOUACAmsVmTH.htm)|Command the Undead|Comandar muertos vivientes|modificada|
|[A8YjaMzEWbFC9JWF.htm](kingmaker-bestiary-items/A8YjaMzEWbFC9JWF.htm)|Grab|Agarrado|modificada|
|[A9Hfkb1IhZNWZW7U.htm](kingmaker-bestiary-items/A9Hfkb1IhZNWZW7U.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[AaMjBE4xQc5ZqroJ.htm](kingmaker-bestiary-items/AaMjBE4xQc5ZqroJ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[aan6tOAkYHhETUdL.htm](kingmaker-bestiary-items/aan6tOAkYHhETUdL.htm)|Deadly Aim|Puntería mortal|modificada|
|[AB7mMjajOUtvqlxD.htm](kingmaker-bestiary-items/AB7mMjajOUtvqlxD.htm)|Evasion|Evasión|modificada|
|[AcBYEH0GxgM4iV5v.htm](kingmaker-bestiary-items/AcBYEH0GxgM4iV5v.htm)|Ice Tunneler|Ice Tunneler|modificada|
|[aDF1WE7DJM17CR29.htm](kingmaker-bestiary-items/aDF1WE7DJM17CR29.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Aern77CIRMOy8Se3.htm](kingmaker-bestiary-items/Aern77CIRMOy8Se3.htm)|Claw|Garra|modificada|
|[AFv5FWlpT2zyryiG.htm](kingmaker-bestiary-items/AFv5FWlpT2zyryiG.htm)|Swarm Mind|Swarm Mind|modificada|
|[AfvMfBPmJB8eecwr.htm](kingmaker-bestiary-items/AfvMfBPmJB8eecwr.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[afWpHJ6qqzsRORUX.htm](kingmaker-bestiary-items/afWpHJ6qqzsRORUX.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[AFz5oCbdGYZQVrDD.htm](kingmaker-bestiary-items/AFz5oCbdGYZQVrDD.htm)|Agonized Wail|Lamento agonizante|modificada|
|[Ag07kBthiVumPkcV.htm](kingmaker-bestiary-items/Ag07kBthiVumPkcV.htm)|Focus Hatred|Focus Hatred|modificada|
|[aHamTcS3vkrZLHGN.htm](kingmaker-bestiary-items/aHamTcS3vkrZLHGN.htm)|Instant Suggestion|Sugestión instantónea.|modificada|
|[aHuQg1Wi1gb8KAbH.htm](kingmaker-bestiary-items/aHuQg1Wi1gb8KAbH.htm)|Time Siphon|Time Siphon|modificada|
|[ai3N8T91JXo3Hx5e.htm](kingmaker-bestiary-items/ai3N8T91JXo3Hx5e.htm)|Negative Healing|Curación negativa|modificada|
|[AIde6VUDBdxDlQQS.htm](kingmaker-bestiary-items/AIde6VUDBdxDlQQS.htm)|Dagger|Daga|modificada|
|[AiKIcXFJx9lEEfnt.htm](kingmaker-bestiary-items/AiKIcXFJx9lEEfnt.htm)|Surge|Oleaje|modificada|
|[AjjeSlBUiJKE8SOB.htm](kingmaker-bestiary-items/AjjeSlBUiJKE8SOB.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[AkE2QdLe1wysN32X.htm](kingmaker-bestiary-items/AkE2QdLe1wysN32X.htm)|Blindness|Ceguera|modificada|
|[AksHJ5xCuwQbdOmq.htm](kingmaker-bestiary-items/AksHJ5xCuwQbdOmq.htm)|Claw|Garra|modificada|
|[ALbbJNuSFbicqOfo.htm](kingmaker-bestiary-items/ALbbJNuSFbicqOfo.htm)|Ripping Disarm|Desarme desgarrador|modificada|
|[aLcLFpXMpgdiDrzO.htm](kingmaker-bestiary-items/aLcLFpXMpgdiDrzO.htm)|Symbiotic Swarm|Enjambre Simbiótico|modificada|
|[aM4rXRFUMH2Ufswh.htm](kingmaker-bestiary-items/aM4rXRFUMH2Ufswh.htm)|Improved Grab|Agarrado mejorado|modificada|
|[AMuN6VT4RTloaWQk.htm](kingmaker-bestiary-items/AMuN6VT4RTloaWQk.htm)|Bomber|Bombardero|modificada|
|[ao5De1dHxQjNJ8za.htm](kingmaker-bestiary-items/ao5De1dHxQjNJ8za.htm)|Souleater|Souleater|modificada|
|[AoTJ2NwBcO5rItKq.htm](kingmaker-bestiary-items/AoTJ2NwBcO5rItKq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ApHre7Dw1aJJlSiv.htm](kingmaker-bestiary-items/ApHre7Dw1aJJlSiv.htm)|Hatchet|Hacha|modificada|
|[apQdVdQWSEbQzCw2.htm](kingmaker-bestiary-items/apQdVdQWSEbQzCw2.htm)|Focus Gaze|Centrar mirada|modificada|
|[aQsTI9u9o9zMbaHa.htm](kingmaker-bestiary-items/aQsTI9u9o9zMbaHa.htm)|Resolve|Resolución|modificada|
|[Ar58HUSjCtqUCBWd.htm](kingmaker-bestiary-items/Ar58HUSjCtqUCBWd.htm)|Jaws|Fauces|modificada|
|[Asj3JNQgw7B1ZXXC.htm](kingmaker-bestiary-items/Asj3JNQgw7B1ZXXC.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[atdIIgSxVkVrP1Ti.htm](kingmaker-bestiary-items/atdIIgSxVkVrP1Ti.htm)|Druid Order Spells|Conjuros de orden druida|modificada|
|[AtiJbhL8pY43Rvv3.htm](kingmaker-bestiary-items/AtiJbhL8pY43Rvv3.htm)|Smash Kneecaps|Aplastar Rodillas|modificada|
|[aTtxivkD4mgfosfH.htm](kingmaker-bestiary-items/aTtxivkD4mgfosfH.htm)|Change Shape|Change Shape|modificada|
|[auHvPa2qajS0vG6w.htm](kingmaker-bestiary-items/auHvPa2qajS0vG6w.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[auvnM9kZNqXdqAIR.htm](kingmaker-bestiary-items/auvnM9kZNqXdqAIR.htm)|Outside of Time|Fuera del Tiempo|modificada|
|[AvThDzMVhWejEVsL.htm](kingmaker-bestiary-items/AvThDzMVhWejEVsL.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[aW63u7ufl1uEOBQh.htm](kingmaker-bestiary-items/aW63u7ufl1uEOBQh.htm)|Furious Finish|Remate furioso|modificada|
|[AwAeIIlDkmCoVoca.htm](kingmaker-bestiary-items/AwAeIIlDkmCoVoca.htm)|Regeneration 20 (Deactivated by Acid or Fire)|Regeneración 20 (Desactivado por Ácido o Fuego)|modificada|
|[AwKlC3yGdtDzjPWM.htm](kingmaker-bestiary-items/AwKlC3yGdtDzjPWM.htm)|Masterful Hunter|Cazador maestro|modificada|
|[AWNveQn0EQ1aYORU.htm](kingmaker-bestiary-items/AWNveQn0EQ1aYORU.htm)|Rise Up|Levántate|modificada|
|[aWo3aVa2XXkGsrLY.htm](kingmaker-bestiary-items/aWo3aVa2XXkGsrLY.htm)|Final Spite|Final Spite|modificada|
|[AX9lSAPNk2PJCat1.htm](kingmaker-bestiary-items/AX9lSAPNk2PJCat1.htm)|Outside of Time|Fuera del Tiempo|modificada|
|[axy7Bj9PMjBKlFdd.htm](kingmaker-bestiary-items/axy7Bj9PMjBKlFdd.htm)|Club|Club|modificada|
|[ay85G9ub5DX6nf12.htm](kingmaker-bestiary-items/ay85G9ub5DX6nf12.htm)|No Escape|Sin huida posible|modificada|
|[AYVzOquWiUdDuNN3.htm](kingmaker-bestiary-items/AYVzOquWiUdDuNN3.htm)|Improved Grab|Agarrado mejorado|modificada|
|[Az48BJ0aNRNzQotH.htm](kingmaker-bestiary-items/Az48BJ0aNRNzQotH.htm)|Wounded Arm|Brazo Hiriente|modificada|
|[azD5OELMU31IrNwz.htm](kingmaker-bestiary-items/azD5OELMU31IrNwz.htm)|Dagger|Daga|modificada|
|[aZke0WE5mklrrBK7.htm](kingmaker-bestiary-items/aZke0WE5mklrrBK7.htm)|Draining Inhalation|Inhalación Drenante|modificada|
|[b07B1akTo84FKZUe.htm](kingmaker-bestiary-items/b07B1akTo84FKZUe.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[BA3fqrqPyWZ59YSs.htm](kingmaker-bestiary-items/BA3fqrqPyWZ59YSs.htm)|Deadeye's Shame|Vergüenza de Deadeye|modificada|
|[bC1xvgHpQUnix9Bt.htm](kingmaker-bestiary-items/bC1xvgHpQUnix9Bt.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[bekh1R8LAluGLNaA.htm](kingmaker-bestiary-items/bekh1R8LAluGLNaA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bEsmMOS1PqhLHHbh.htm](kingmaker-bestiary-items/bEsmMOS1PqhLHHbh.htm)|Chewing Bites|Muerdemuerde|modificada|
|[BFpgenVI0pBLyV0S.htm](kingmaker-bestiary-items/BFpgenVI0pBLyV0S.htm)|Infused Items|Equipos infundidos|modificada|
|[BGZlipUdtmQIe6By.htm](kingmaker-bestiary-items/BGZlipUdtmQIe6By.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[BJiQlSKAx6WhMT1p.htm](kingmaker-bestiary-items/BJiQlSKAx6WhMT1p.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[bJUTz62klSlUEuTo.htm](kingmaker-bestiary-items/bJUTz62klSlUEuTo.htm)|Drink Blood|Beber Sangre|modificada|
|[bKPOLbNfdVcrEpjV.htm](kingmaker-bestiary-items/bKPOLbNfdVcrEpjV.htm)|Bonds of Iron|Ligaduras de hierro|modificada|
|[BKsKbanuSDh8osp9.htm](kingmaker-bestiary-items/BKsKbanuSDh8osp9.htm)|Hope or Despair|Esperanza o desesperación|modificada|
|[bl1xW54BlfiFnU8h.htm](kingmaker-bestiary-items/bl1xW54BlfiFnU8h.htm)|Catch Rock|Atrapar roca|modificada|
|[BleHEEpt3dwEMeDo.htm](kingmaker-bestiary-items/BleHEEpt3dwEMeDo.htm)|Thorny Aura|Aura espinosa|modificada|
|[BLleGAYzhc0cIyIz.htm](kingmaker-bestiary-items/BLleGAYzhc0cIyIz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bLrblJlMOfhn39My.htm](kingmaker-bestiary-items/bLrblJlMOfhn39My.htm)|Swarm Mind|Swarm Mind|modificada|
|[BltNCa3ZEsbxzhf7.htm](kingmaker-bestiary-items/BltNCa3ZEsbxzhf7.htm)|Buck|Encabritarse|modificada|
|[bMbUyfSqVR9lWDB7.htm](kingmaker-bestiary-items/bMbUyfSqVR9lWDB7.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[BMGyGT2KQinKwWfe.htm](kingmaker-bestiary-items/BMGyGT2KQinKwWfe.htm)|Shortsword|Espada corta|modificada|
|[bmmCoxENGAgHtWP1.htm](kingmaker-bestiary-items/bmmCoxENGAgHtWP1.htm)|Web Sense|Web Sense|modificada|
|[BO3hN1F0bkrNu60q.htm](kingmaker-bestiary-items/BO3hN1F0bkrNu60q.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[bOpAJRLpemL4HwNd.htm](kingmaker-bestiary-items/bOpAJRLpemL4HwNd.htm)|Swamp Stride|Swamp Stride|modificada|
|[BOya1lhwnf2RNvGy.htm](kingmaker-bestiary-items/BOya1lhwnf2RNvGy.htm)|Juggernaut|Juggernaut|modificada|
|[Bpm9xvzcp3dBpCvp.htm](kingmaker-bestiary-items/Bpm9xvzcp3dBpCvp.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[BqhAFrC5aDQ1BKoT.htm](kingmaker-bestiary-items/BqhAFrC5aDQ1BKoT.htm)|Blowgun|Blowgun|modificada|
|[BtJvNGAGxO2VOuXB.htm](kingmaker-bestiary-items/BtJvNGAGxO2VOuXB.htm)|Mind-Numbing Breath|Aliento Adormecedor|modificada|
|[BuBkGYAPCPnJ03go.htm](kingmaker-bestiary-items/BuBkGYAPCPnJ03go.htm)|Claw|Garra|modificada|
|[bvBZPt5ocFCNJflm.htm](kingmaker-bestiary-items/bvBZPt5ocFCNJflm.htm)|Ready and Load|Ready and Load|modificada|
|[BxuCFIo6yswNsArJ.htm](kingmaker-bestiary-items/BxuCFIo6yswNsArJ.htm)|Hunting Cry|Grito de Caza|modificada|
|[by7p3pdmrMKbVYk8.htm](kingmaker-bestiary-items/by7p3pdmrMKbVYk8.htm)|Tail|Tail|modificada|
|[bZEeE1MlmmqIJiiK.htm](kingmaker-bestiary-items/bZEeE1MlmmqIJiiK.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[c1sQmMq46Aeg6en6.htm](kingmaker-bestiary-items/c1sQmMq46Aeg6en6.htm)|Axe Specialization|Especialización Hacha|modificada|
|[c2r3T8G5zXq6UOBS.htm](kingmaker-bestiary-items/c2r3T8G5zXq6UOBS.htm)|Mobility|Movilidad|modificada|
|[C31XU8XsrO1R112R.htm](kingmaker-bestiary-items/C31XU8XsrO1R112R.htm)|Focused Target|Objetivo Enfocado|modificada|
|[c3vIbIBnVpy9fjsE.htm](kingmaker-bestiary-items/c3vIbIBnVpy9fjsE.htm)|Rage|Furia|modificada|
|[C4fZA5gB6JeZf08P.htm](kingmaker-bestiary-items/C4fZA5gB6JeZf08P.htm)|Club|Club|modificada|
|[C91ggah5Ye65LrMH.htm](kingmaker-bestiary-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|Leng Ruby|modificada|
|[C9Ds23BokMiIUMi5.htm](kingmaker-bestiary-items/C9Ds23BokMiIUMi5.htm)|Release Jewel|Soltar Joya|modificada|
|[C9ESEAYQaJUciIba.htm](kingmaker-bestiary-items/C9ESEAYQaJUciIba.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[cA2cBXkewzB0Fwtj.htm](kingmaker-bestiary-items/cA2cBXkewzB0Fwtj.htm)|Change Shape|Change Shape|modificada|
|[CBuSOJ26ZXTgXAze.htm](kingmaker-bestiary-items/CBuSOJ26ZXTgXAze.htm)|Constant Spells|Constant Spells|modificada|
|[cc2QQL18PozIgxVb.htm](kingmaker-bestiary-items/cc2QQL18PozIgxVb.htm)|Wizard School Spells|Hechizos de la Escuela de Magos|modificada|
|[cC853WQiGJtDTkRc.htm](kingmaker-bestiary-items/cC853WQiGJtDTkRc.htm)|Frightful Presence|Frightful Presence|modificada|
|[CcrqR1iP71tzTA5V.htm](kingmaker-bestiary-items/CcrqR1iP71tzTA5V.htm)|Collapse|Colapso|modificada|
|[cD3KZDeLep4rM0aB.htm](kingmaker-bestiary-items/cD3KZDeLep4rM0aB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cDS4azJaDaRZITrQ.htm](kingmaker-bestiary-items/cDS4azJaDaRZITrQ.htm)|Cold Vulnerability|Vulnerabilidad al frío|modificada|
|[CDUrtwX9v7aZEVjj.htm](kingmaker-bestiary-items/CDUrtwX9v7aZEVjj.htm)|Change Shape|Change Shape|modificada|
|[CeBgXo66gTiufqL5.htm](kingmaker-bestiary-items/CeBgXo66gTiufqL5.htm)|Nature's Edge|Ventaja de la Naturaleza|modificada|
|[cFKmkUCecFtQweau.htm](kingmaker-bestiary-items/cFKmkUCecFtQweau.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[Ch2vwQBo7w7hd8ou.htm](kingmaker-bestiary-items/Ch2vwQBo7w7hd8ou.htm)|Harrying Swordfighter|Harrying Swordfighter|modificada|
|[CiirPvlIj0ZtuHce.htm](kingmaker-bestiary-items/CiirPvlIj0ZtuHce.htm)|Vengeful Anger|Furia vengativa|modificada|
|[cJIYg8gvnBbvoiuI.htm](kingmaker-bestiary-items/cJIYg8gvnBbvoiuI.htm)|Mobility|Movilidad|modificada|
|[CJravyxL4AFAla1i.htm](kingmaker-bestiary-items/CJravyxL4AFAla1i.htm)|Curse of the Wererat|Curse of the Wererat|modificada|
|[ckg1JdT2pmJlw8zC.htm](kingmaker-bestiary-items/ckg1JdT2pmJlw8zC.htm)|Dagger|Daga|modificada|
|[ckkFk6fhxAXrQHzv.htm](kingmaker-bestiary-items/ckkFk6fhxAXrQHzv.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[ComzTzx9Gu4QDQxN.htm](kingmaker-bestiary-items/ComzTzx9Gu4QDQxN.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[cpFEKCq7CWkJ3qrw.htm](kingmaker-bestiary-items/cpFEKCq7CWkJ3qrw.htm)|Trident|Trident|modificada|
|[cprgu1LyMWRqzI0w.htm](kingmaker-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|Garra|modificada|
|[cQluYUjYjTeHDhTp.htm](kingmaker-bestiary-items/cQluYUjYjTeHDhTp.htm)|Spinning Sweep|Barrido Giratorio|modificada|
|[CquGBdxcC31YxWhu.htm](kingmaker-bestiary-items/CquGBdxcC31YxWhu.htm)|Rotting Stench|Hedor Putrefacto|modificada|
|[cr8chy1goN1ZSrnT.htm](kingmaker-bestiary-items/cr8chy1goN1ZSrnT.htm)|Mound|Montículo|modificada|
|[cRTNLnVuzisUG7kl.htm](kingmaker-bestiary-items/cRTNLnVuzisUG7kl.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[CvfLXQsndA3wb5S1.htm](kingmaker-bestiary-items/CvfLXQsndA3wb5S1.htm)|Foot|Pie|modificada|
|[czVzxuwWmCRLgnuc.htm](kingmaker-bestiary-items/czVzxuwWmCRLgnuc.htm)|Forced Regeneration|Regeneración vigorosa|modificada|
|[d2pAqCh02beVW16z.htm](kingmaker-bestiary-items/d2pAqCh02beVW16z.htm)|Jaws|Fauces|modificada|
|[D4C1yHkCqpQckxgr.htm](kingmaker-bestiary-items/D4C1yHkCqpQckxgr.htm)|Tail Lash|Azote con la cola|modificada|
|[D4f8OzwwMvBLI86y.htm](kingmaker-bestiary-items/D4f8OzwwMvBLI86y.htm)|Cold Iron Shortsword|Cold Iron Shortsword|modificada|
|[D6qVDUiJjJhA5CIX.htm](kingmaker-bestiary-items/D6qVDUiJjJhA5CIX.htm)|Wild Reincarnation|Reencarnar Salvaje|modificada|
|[d7lY0T9fiGJzH2O1.htm](kingmaker-bestiary-items/d7lY0T9fiGJzH2O1.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[d7qSq3HVLR2AAayU.htm](kingmaker-bestiary-items/d7qSq3HVLR2AAayU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[d8wBT2ExQFfDG2Mt.htm](kingmaker-bestiary-items/d8wBT2ExQFfDG2Mt.htm)|Wild Hunt Link|Wild Hunt Link|modificada|
|[d9FqMXrNIAm6rnHs.htm](kingmaker-bestiary-items/d9FqMXrNIAm6rnHs.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[dahtG8lHhjQBQemo.htm](kingmaker-bestiary-items/dahtG8lHhjQBQemo.htm)|Hurled Thorn|Espina lanzada|modificada|
|[DbU5apk8FlJos4xz.htm](kingmaker-bestiary-items/DbU5apk8FlJos4xz.htm)|Critical Debilitating Strike|Golpe Debilitación crítica.|modificada|
|[DdEls9nTJvA7pEi0.htm](kingmaker-bestiary-items/DdEls9nTJvA7pEi0.htm)|Blowgun|Blowgun|modificada|
|[DEac5zIyNqdQYdnc.htm](kingmaker-bestiary-items/DEac5zIyNqdQYdnc.htm)|Grab|Agarrado|modificada|
|[DeAD5IPwOAtQ1Zjr.htm](kingmaker-bestiary-items/DeAD5IPwOAtQ1Zjr.htm)|Kukri|Kukri|modificada|
|[deytOmtiZjHFzGRU.htm](kingmaker-bestiary-items/deytOmtiZjHFzGRU.htm)|Athach Venom|Athach Venom|modificada|
|[DEz7gcf0AumMmDO8.htm](kingmaker-bestiary-items/DEz7gcf0AumMmDO8.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[DFgNddcsAbZVlWSH.htm](kingmaker-bestiary-items/DFgNddcsAbZVlWSH.htm)|Pounce|Abalanzarse|modificada|
|[DfyikbxEMRuzW77m.htm](kingmaker-bestiary-items/DfyikbxEMRuzW77m.htm)|Swarm Shape|Forma de Enjambre|modificada|
|[DgApO6iOQpwx3KfR.htm](kingmaker-bestiary-items/DgApO6iOQpwx3KfR.htm)|Resolve|Resolución|modificada|
|[dGglOjtXAyHqoW5X.htm](kingmaker-bestiary-items/dGglOjtXAyHqoW5X.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[DGNIDiy16GZYbh8b.htm](kingmaker-bestiary-items/DGNIDiy16GZYbh8b.htm)|Trapdoor Lunge|Trapdoor Acometer|modificada|
|[dH7eKw8pDJQDkR3P.htm](kingmaker-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|Restringir|modificada|
|[dhTLYiCmWDkONUN5.htm](kingmaker-bestiary-items/dhTLYiCmWDkONUN5.htm)|Jaws|Fauces|modificada|
|[DI6Nw0XKEZZJLY8D.htm](kingmaker-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[DJVCW8WVRR0Prvrs.htm](kingmaker-bestiary-items/DJVCW8WVRR0Prvrs.htm)|Prismatic Burst|Ráfaga Prismática|modificada|
|[dJzydVxO1TtPnaho.htm](kingmaker-bestiary-items/dJzydVxO1TtPnaho.htm)|Deny Advantage|Denegar ventaja|modificada|
|[DksWsSv1BvpLeNhJ.htm](kingmaker-bestiary-items/DksWsSv1BvpLeNhJ.htm)|Jaws|Fauces|modificada|
|[DLI2axhKyDzeg5Ub.htm](kingmaker-bestiary-items/DLI2axhKyDzeg5Ub.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[DNaScDDIg4t6u0Hw.htm](kingmaker-bestiary-items/DNaScDDIg4t6u0Hw.htm)|Ballista Bolt|Ballista Bolt|modificada|
|[dnLiUmNpLT4ZnUQ6.htm](kingmaker-bestiary-items/dnLiUmNpLT4ZnUQ6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dNOthX618UT1hZN6.htm](kingmaker-bestiary-items/dNOthX618UT1hZN6.htm)|Distracting Shot|Tiro de distracción|modificada|
|[DoCmnJxIOLLTb9Xm.htm](kingmaker-bestiary-items/DoCmnJxIOLLTb9Xm.htm)|Greater Acid Flask|Mayor frasco de ácido|modificada|
|[DOviMcCQeyu6rmjx.htm](kingmaker-bestiary-items/DOviMcCQeyu6rmjx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[DOWxg5PQXXUG8Ddy.htm](kingmaker-bestiary-items/DOWxg5PQXXUG8Ddy.htm)|Beak|Beak|modificada|
|[DPcRdZIL38qcDj2I.htm](kingmaker-bestiary-items/DPcRdZIL38qcDj2I.htm)|Snatch|Arrebatar|modificada|
|[DrW6hlIWaGCF1pU4.htm](kingmaker-bestiary-items/DrW6hlIWaGCF1pU4.htm)|Juggernaut|Juggernaut|modificada|
|[DsvyLX53TeuZHMQ3.htm](kingmaker-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|Pies con garras|modificada|
|[dtTve1TvHU0al6Es.htm](kingmaker-bestiary-items/dtTve1TvHU0al6Es.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DTXq4teaiLU9haNE.htm](kingmaker-bestiary-items/DTXq4teaiLU9haNE.htm)|Wide Swing|Barrido ancho|modificada|
|[du4VcQ2P0kOpRVrS.htm](kingmaker-bestiary-items/du4VcQ2P0kOpRVrS.htm)|Buck|Encabritarse|modificada|
|[DuDuSE2EKfZt9r6m.htm](kingmaker-bestiary-items/DuDuSE2EKfZt9r6m.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[DUWbE68XkGU2jM9J.htm](kingmaker-bestiary-items/DUWbE68XkGU2jM9J.htm)|Fist|Puño|modificada|
|[DVsGmUvb4tqHJrCw.htm](kingmaker-bestiary-items/DVsGmUvb4tqHJrCw.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[dW28Q4hMeEWpDtay.htm](kingmaker-bestiary-items/dW28Q4hMeEWpDtay.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[dWpKdYaRxbAJiHSi.htm](kingmaker-bestiary-items/dWpKdYaRxbAJiHSi.htm)|Bloodbird|Bloodbird|modificada|
|[dX22wsxoPsGLzOQ0.htm](kingmaker-bestiary-items/dX22wsxoPsGLzOQ0.htm)|Dagger|Daga|modificada|
|[DxTspLZpQI4zVPoa.htm](kingmaker-bestiary-items/DxTspLZpQI4zVPoa.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[dynvtBdr79KGnqBo.htm](kingmaker-bestiary-items/dynvtBdr79KGnqBo.htm)|Can't Catch Me|Can't Catch Me|modificada|
|[e0fcFHdf91dsur91.htm](kingmaker-bestiary-items/e0fcFHdf91dsur91.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[e1txRyfONnCuYPny.htm](kingmaker-bestiary-items/e1txRyfONnCuYPny.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[E2BfR5UhdImAFitO.htm](kingmaker-bestiary-items/E2BfR5UhdImAFitO.htm)|Shyka's Judgement|Juicio de Shyka|modificada|
|[e2cSvAso2wePkLAK.htm](kingmaker-bestiary-items/e2cSvAso2wePkLAK.htm)|Korog's Command|Orden imperiosa de Korog|modificada|
|[E3C47mJQhyySioGh.htm](kingmaker-bestiary-items/E3C47mJQhyySioGh.htm)|Hatchet|Hacha|modificada|
|[e3mEtYHIO4f0NZZc.htm](kingmaker-bestiary-items/e3mEtYHIO4f0NZZc.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[E3r5U0LkfWOXhgLt.htm](kingmaker-bestiary-items/E3r5U0LkfWOXhgLt.htm)|Shortbow|Arco corto|modificada|
|[E4bINR3oniilC0ba.htm](kingmaker-bestiary-items/E4bINR3oniilC0ba.htm)|Shell Defense|Defensa con caparazón|modificada|
|[E5KXLULpkukpqygI.htm](kingmaker-bestiary-items/E5KXLULpkukpqygI.htm)|Claw|Garra|modificada|
|[E8ICm8MwDDWyjgKR.htm](kingmaker-bestiary-items/E8ICm8MwDDWyjgKR.htm)|Rock|Roca|modificada|
|[EaWA2WkRjbpm2GiI.htm](kingmaker-bestiary-items/EaWA2WkRjbpm2GiI.htm)|Duplicate Victim|Víctima Duplicada|modificada|
|[ECXG4lnlaSBXuqcP.htm](kingmaker-bestiary-items/ECXG4lnlaSBXuqcP.htm)|Defensive Slice|Rodaja defensiva|modificada|
|[EdlfzUPi8lEnewu1.htm](kingmaker-bestiary-items/EdlfzUPi8lEnewu1.htm)|Dual Tusks|Dobles colmillos|modificada|
|[EdoAyjfImWlnNF3Z.htm](kingmaker-bestiary-items/EdoAyjfImWlnNF3Z.htm)|End the Charade|Poner fin a la farsa|modificada|
|[Eeclk1UrcRUe4od5.htm](kingmaker-bestiary-items/Eeclk1UrcRUe4od5.htm)|Lock and Shriek|Cerrar y Shriek|modificada|
|[eG5iE2SpiXdvquWh.htm](kingmaker-bestiary-items/eG5iE2SpiXdvquWh.htm)|Funereal Dirge|Funereal Dirge|modificada|
|[egV0y60X9Okkmuoc.htm](kingmaker-bestiary-items/egV0y60X9Okkmuoc.htm)|Giant Trapdoor Spider Venom|Araña gigante veneno de trampilla|modificada|
|[EGY18Yebn9ZVwEbP.htm](kingmaker-bestiary-items/EGY18Yebn9ZVwEbP.htm)|Rat Empathy|Rata Empatía|modificada|
|[EgymgxzMTQGUvdBD.htm](kingmaker-bestiary-items/EgymgxzMTQGUvdBD.htm)|Telekinetic Assault|Asalto telecinético|modificada|
|[eHtfWWTVUSw5Zu4F.htm](kingmaker-bestiary-items/eHtfWWTVUSw5Zu4F.htm)|Forceful Focus|Enfoque vigoroso|modificada|
|[EihtJU42yTV6LfVg.htm](kingmaker-bestiary-items/EihtJU42yTV6LfVg.htm)|Fetch Weapon|Fetch Weapon|modificada|
|[ek9L2OPkQLBaP0Ml.htm](kingmaker-bestiary-items/ek9L2OPkQLBaP0Ml.htm)|Bound to the Mortal Night|Atado a la Noche Mortal|modificada|
|[ELQWppSD9MT72Ci1.htm](kingmaker-bestiary-items/ELQWppSD9MT72Ci1.htm)|Staff of Fire (Major)|Bastón de fuego (superior).|modificada|
|[emb5f7dGdRTiUTnN.htm](kingmaker-bestiary-items/emb5f7dGdRTiUTnN.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[eMFelq83Tb6uIwqb.htm](kingmaker-bestiary-items/eMFelq83Tb6uIwqb.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[eN9AmHu7Wb5ZVY5F.htm](kingmaker-bestiary-items/eN9AmHu7Wb5ZVY5F.htm)|Whiffling|Whiffling|modificada|
|[Enk2lR0Vry7rabGe.htm](kingmaker-bestiary-items/Enk2lR0Vry7rabGe.htm)|Site Bound|Ligado a una ubicación|modificada|
|[Epu5XvonDsZQClpu.htm](kingmaker-bestiary-items/Epu5XvonDsZQClpu.htm)|Flechette|Flechette|modificada|
|[eqCGFR9UmHOuUpMI.htm](kingmaker-bestiary-items/eqCGFR9UmHOuUpMI.htm)|Hatchet|Hacha|modificada|
|[eQQryZfmKwV4meJL.htm](kingmaker-bestiary-items/eQQryZfmKwV4meJL.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[esNHCG2RP8SOfTuA.htm](kingmaker-bestiary-items/esNHCG2RP8SOfTuA.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[ESPLHQJWpOyak6Wa.htm](kingmaker-bestiary-items/ESPLHQJWpOyak6Wa.htm)|Battle Axe|Hacha de batalla|modificada|
|[eSXITfBnqJ3s71nu.htm](kingmaker-bestiary-items/eSXITfBnqJ3s71nu.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[EtUgChhYGZ37rp9z.htm](kingmaker-bestiary-items/EtUgChhYGZ37rp9z.htm)|Sure Possession|Posesión Segura|modificada|
|[eUDWR9NZHLWYc7Dj.htm](kingmaker-bestiary-items/eUDWR9NZHLWYc7Dj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EUf1gqfe5un9Aprz.htm](kingmaker-bestiary-items/EUf1gqfe5un9Aprz.htm)|Vine|Vid|modificada|
|[EVaLrmDugpw1gfO1.htm](kingmaker-bestiary-items/EVaLrmDugpw1gfO1.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[EVtI5ReFhXPbGsa8.htm](kingmaker-bestiary-items/EVtI5ReFhXPbGsa8.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[EwwHk83vJUlZ0cbV.htm](kingmaker-bestiary-items/EwwHk83vJUlZ0cbV.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ex0dTLedYlwxNB3T.htm](kingmaker-bestiary-items/ex0dTLedYlwxNB3T.htm)|Swallow Whole|Engullir Todo|modificada|
|[exrZ9BjFfwPvWYPf.htm](kingmaker-bestiary-items/exrZ9BjFfwPvWYPf.htm)|Ripping Grab|Ripping Grab|modificada|
|[ExsxFy6Bmcgsy0GX.htm](kingmaker-bestiary-items/ExsxFy6Bmcgsy0GX.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Ez68XkpqwOnuPfr3.htm](kingmaker-bestiary-items/Ez68XkpqwOnuPfr3.htm)|Shield Block|Bloquear con escudo|modificada|
|[eznh9ydPPecUq1K1.htm](kingmaker-bestiary-items/eznh9ydPPecUq1K1.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[F0yeghQAPNgRWKCf.htm](kingmaker-bestiary-items/F0yeghQAPNgRWKCf.htm)|Deafening Cry|Deafening Cry|modificada|
|[f42wFnhs1khcDf8G.htm](kingmaker-bestiary-items/f42wFnhs1khcDf8G.htm)|Fast Healing 20|Curación rápida 20|modificada|
|[F4qi1llZbd9QZy4d.htm](kingmaker-bestiary-items/F4qi1llZbd9QZy4d.htm)|Raise a Shield|Alzar un escudo|modificada|
|[F4z6xO5BYtoag9hK.htm](kingmaker-bestiary-items/F4z6xO5BYtoag9hK.htm)|Jaws|Fauces|modificada|
|[f7AUQc1tVLR8FHEA.htm](kingmaker-bestiary-items/f7AUQc1tVLR8FHEA.htm)|Fog Vision|Fog Vision|modificada|
|[F8xpT7h5a5ZviXU5.htm](kingmaker-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|Alimentarse de Miedo|modificada|
|[FaGxK1ootpG6uFmP.htm](kingmaker-bestiary-items/FaGxK1ootpG6uFmP.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[fakAfk3puTMpw1vT.htm](kingmaker-bestiary-items/fakAfk3puTMpw1vT.htm)|Slow Metabolism|Metabolismo lentificado/a|modificada|
|[FapwAUPhHeXc1NKj.htm](kingmaker-bestiary-items/FapwAUPhHeXc1NKj.htm)|Warlord's Training|Warlord's Training|modificada|
|[fbNFZNeEcQgHVKW4.htm](kingmaker-bestiary-items/fbNFZNeEcQgHVKW4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[fckcc8VGCaiue1sX.htm](kingmaker-bestiary-items/fckcc8VGCaiue1sX.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[FCU5JyYgiHDot5rm.htm](kingmaker-bestiary-items/FCU5JyYgiHDot5rm.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[FdJtaUOMkZdu6F7C.htm](kingmaker-bestiary-items/FdJtaUOMkZdu6F7C.htm)|Fangs|Colmillos|modificada|
|[FdQL1f8wOuuy69Tz.htm](kingmaker-bestiary-items/FdQL1f8wOuuy69Tz.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[FEC49j9OjVBivyUX.htm](kingmaker-bestiary-items/FEC49j9OjVBivyUX.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[FGm1xbUPgE0BRiSf.htm](kingmaker-bestiary-items/FGm1xbUPgE0BRiSf.htm)|Focused Assault|Asalto concentrado|modificada|
|[FH5MdxzW5lzQE9Hf.htm](kingmaker-bestiary-items/FH5MdxzW5lzQE9Hf.htm)|Regeneration 15 (deactivated by Acid or Fire)|Regeneración 15 (desactivada por Ácido o Fuego).|modificada|
|[FKNzpxoNtzzC5ytc.htm](kingmaker-bestiary-items/FKNzpxoNtzzC5ytc.htm)|Dagger|Daga|modificada|
|[FktubLHfxuouP7QK.htm](kingmaker-bestiary-items/FktubLHfxuouP7QK.htm)|Stubborn Conviction|Stubborn Conviction|modificada|
|[Fl8QSyuys5Y8YCrK.htm](kingmaker-bestiary-items/Fl8QSyuys5Y8YCrK.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[fLIOuEGoDxmXYqNf.htm](kingmaker-bestiary-items/fLIOuEGoDxmXYqNf.htm)|Snack|Snack|modificada|
|[fm5cOYL11qhzHV3G.htm](kingmaker-bestiary-items/fm5cOYL11qhzHV3G.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FMwhWB2KeMuU3kYQ.htm](kingmaker-bestiary-items/FMwhWB2KeMuU3kYQ.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[FMyBd24spHonNXKf.htm](kingmaker-bestiary-items/FMyBd24spHonNXKf.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[FpcxdJ4oqnyX8sBs.htm](kingmaker-bestiary-items/FpcxdJ4oqnyX8sBs.htm)|Long Neck|Cuello largo|modificada|
|[fqcdNor1vTHZe5Zl.htm](kingmaker-bestiary-items/fqcdNor1vTHZe5Zl.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[fqO0yIYBu1FrLvB9.htm](kingmaker-bestiary-items/fqO0yIYBu1FrLvB9.htm)|Scythe|Guadaña|modificada|
|[FQyPQNxKCzuNCQHu.htm](kingmaker-bestiary-items/FQyPQNxKCzuNCQHu.htm)|Wolf Empathy|Wolf Empathy|modificada|
|[fRaI1Z66oWzSAz2Q.htm](kingmaker-bestiary-items/fRaI1Z66oWzSAz2Q.htm)|Hit 'Em Hard|Hit 'Em Hard|modificada|
|[FrDnLG8ehjXFC6Ey.htm](kingmaker-bestiary-items/FrDnLG8ehjXFC6Ey.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[frIks1GEvtVQAPPw.htm](kingmaker-bestiary-items/frIks1GEvtVQAPPw.htm)|Vine|Vid|modificada|
|[FrlYBDNV4RyfqLaY.htm](kingmaker-bestiary-items/FrlYBDNV4RyfqLaY.htm)|Jaws|Fauces|modificada|
|[FrmCI6DzaoSpGyxI.htm](kingmaker-bestiary-items/FrmCI6DzaoSpGyxI.htm)|Twin Parry|Parada gemela|modificada|
|[FRSknzZLlvXTktV6.htm](kingmaker-bestiary-items/FRSknzZLlvXTktV6.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[fryglhBup2tRHWFS.htm](kingmaker-bestiary-items/fryglhBup2tRHWFS.htm)|Telepathy 300 feet|Telepatía 300 pies.|modificada|
|[fS0rXdbVRA1IMM8E.htm](kingmaker-bestiary-items/fS0rXdbVRA1IMM8E.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[FsgtuZo9KF7fxlp5.htm](kingmaker-bestiary-items/FsgtuZo9KF7fxlp5.htm)|Jaws|Fauces|modificada|
|[FTMgXssm5HK4Io0O.htm](kingmaker-bestiary-items/FTMgXssm5HK4Io0O.htm)|Vortex|Vortex|modificada|
|[ftnV0syBwJfqGk6Z.htm](kingmaker-bestiary-items/ftnV0syBwJfqGk6Z.htm)|Thorn|Espina|modificada|
|[fTVvztwtM7PZgob6.htm](kingmaker-bestiary-items/fTVvztwtM7PZgob6.htm)|Grab|Agarrado|modificada|
|[FuDdgAP7wwq6LCFO.htm](kingmaker-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Phomandala's Venom|Veneno de Phomandala|modificada|
|[FuvkjEpm8ZZnjZ46.htm](kingmaker-bestiary-items/FuvkjEpm8ZZnjZ46.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[Fv9HoAW6MyuvLWBs.htm](kingmaker-bestiary-items/Fv9HoAW6MyuvLWBs.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Fw6GAT5RmaN6VMII.htm](kingmaker-bestiary-items/Fw6GAT5RmaN6VMII.htm)|Giant's Lunge|Acometer Gigante|modificada|
|[FwhFzYJLEkQ3J68c.htm](kingmaker-bestiary-items/FwhFzYJLEkQ3J68c.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[FwN998JxnTCCkJw3.htm](kingmaker-bestiary-items/FwN998JxnTCCkJw3.htm)|Reveal the Enemy|Revelar al enemigo|modificada|
|[fwojd1PspBIG8CEY.htm](kingmaker-bestiary-items/fwojd1PspBIG8CEY.htm)|Shortsword|Espada corta|modificada|
|[fxgXQNNgTLwJ2JoU.htm](kingmaker-bestiary-items/fxgXQNNgTLwJ2JoU.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[fXlB9FRj3C3aY3Vk.htm](kingmaker-bestiary-items/fXlB9FRj3C3aY3Vk.htm)|Form Control|Control de forma|modificada|
|[fxuqMIJcFuZQworq.htm](kingmaker-bestiary-items/fxuqMIJcFuZQworq.htm)|Fangs|Colmillos|modificada|
|[fxZztiCJybeYYeOI.htm](kingmaker-bestiary-items/fxZztiCJybeYYeOI.htm)|Captivating Dance|Danzante cautivador|modificada|
|[fYhoTq6AnWKAwKBM.htm](kingmaker-bestiary-items/fYhoTq6AnWKAwKBM.htm)|Release the Inmost Worm|Soltar el Gusano Intimo|modificada|
|[FZAkeuRb3tyRnlmH.htm](kingmaker-bestiary-items/FZAkeuRb3tyRnlmH.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[FzjT88a1Bn4XsEYM.htm](kingmaker-bestiary-items/FzjT88a1Bn4XsEYM.htm)|Battle Axe|Hacha de batalla|modificada|
|[G0x7fr9ZPMuqw2RG.htm](kingmaker-bestiary-items/G0x7fr9ZPMuqw2RG.htm)|Weeping Aura|Weeping Aura|modificada|
|[g1KH4siW7BJpYTfM.htm](kingmaker-bestiary-items/g1KH4siW7BJpYTfM.htm)|Claw|Garra|modificada|
|[g2qQpMZqRaylNASw.htm](kingmaker-bestiary-items/g2qQpMZqRaylNASw.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[g4bJ3hTpL9J2uQKp.htm](kingmaker-bestiary-items/g4bJ3hTpL9J2uQKp.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[GA9tSWJmy7KFgUjB.htm](kingmaker-bestiary-items/GA9tSWJmy7KFgUjB.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[GAwPcYxItK5M24Ve.htm](kingmaker-bestiary-items/GAwPcYxItK5M24Ve.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[gbg7dpE5qccG0uh1.htm](kingmaker-bestiary-items/gbg7dpE5qccG0uh1.htm)|Greater Shock Maul|Zarpazo Electrizante doble|modificada|
|[GcSM5ZkG8OONvY0G.htm](kingmaker-bestiary-items/GcSM5ZkG8OONvY0G.htm)|Fey Conduit|Conducto feérico|modificada|
|[gDENaXLY2aYA7jYG.htm](kingmaker-bestiary-items/gDENaXLY2aYA7jYG.htm)|Greataxe|Greataxe|modificada|
|[GeAJNEdtRdXSvZvk.htm](kingmaker-bestiary-items/GeAJNEdtRdXSvZvk.htm)|Breath Weapon|Breath Weapon|modificada|
|[gfH6Km7ijznOKHzF.htm](kingmaker-bestiary-items/gfH6Km7ijznOKHzF.htm)|Negative Healing|Curación negativa|modificada|
|[ghqAZgEVPVGBLtZW.htm](kingmaker-bestiary-items/ghqAZgEVPVGBLtZW.htm)|Tongue Grab|Apresar con la lengua|modificada|
|[gixaFzYqsyV9bsLi.htm](kingmaker-bestiary-items/gixaFzYqsyV9bsLi.htm)|Cairn Wight Spawn|Cairn Wight Spawn|modificada|
|[gj9hDQvaXekxyv1Q.htm](kingmaker-bestiary-items/gj9hDQvaXekxyv1Q.htm)|Longsword|Longsword|modificada|
|[GJxtjkowvzk3cvaE.htm](kingmaker-bestiary-items/GJxtjkowvzk3cvaE.htm)|Constant Spells|Constant Spells|modificada|
|[GMKTkSGoth8sggbH.htm](kingmaker-bestiary-items/GMKTkSGoth8sggbH.htm)|Call Glaive|Llamada Glaive|modificada|
|[gNhak3Nf3ve1ouhK.htm](kingmaker-bestiary-items/gNhak3Nf3ve1ouhK.htm)|Claw|Garra|modificada|
|[gpZkw015Ns4Dbplk.htm](kingmaker-bestiary-items/gpZkw015Ns4Dbplk.htm)|Drowning Touch|Drowning Touch|modificada|
|[gRn39hK7VbDU7fip.htm](kingmaker-bestiary-items/gRn39hK7VbDU7fip.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[gRnmV8LgZS6qZ09j.htm](kingmaker-bestiary-items/gRnmV8LgZS6qZ09j.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[gsoYzQxRygC654Xf.htm](kingmaker-bestiary-items/gsoYzQxRygC654Xf.htm)|Pounce|Abalanzarse|modificada|
|[GxlQBNruDU25fJkQ.htm](kingmaker-bestiary-items/GxlQBNruDU25fJkQ.htm)|Open Doors|Puertas Abiertas|modificada|
|[GxSpJFY9UJiwry54.htm](kingmaker-bestiary-items/GxSpJFY9UJiwry54.htm)|Jaws|Fauces|modificada|
|[GZ9qL58QMmBtlHul.htm](kingmaker-bestiary-items/GZ9qL58QMmBtlHul.htm)|Electric Healing|Curación Eléctrica|modificada|
|[gZfWtX8zkOoe49o7.htm](kingmaker-bestiary-items/gZfWtX8zkOoe49o7.htm)|Regeneration 25 (deactivated by Vorpal Weapons)|Regeneración 25 (desactivada por armas Vorpalinas)|modificada|
|[gzziZdYQCjJ1pCM1.htm](kingmaker-bestiary-items/gzziZdYQCjJ1pCM1.htm)|Reminder of Doom|Recordatorio de la Perdición|modificada|
|[h0MxDLIAI1CLU61G.htm](kingmaker-bestiary-items/h0MxDLIAI1CLU61G.htm)|Thorny Lash|Thorny Lash|modificada|
|[h2wSXBnK6CA96ypS.htm](kingmaker-bestiary-items/h2wSXBnK6CA96ypS.htm)|Reach Spell|Conjuro de alcance|modificada|
|[h4qPfDnAjrfUEFX6.htm](kingmaker-bestiary-items/h4qPfDnAjrfUEFX6.htm)|Sylvan Wine|Sylvan Wine|modificada|
|[h4yqrCg7sP5DCU4F.htm](kingmaker-bestiary-items/h4yqrCg7sP5DCU4F.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H5KqV1tEsBEfhfvU.htm](kingmaker-bestiary-items/H5KqV1tEsBEfhfvU.htm)|Hunt Prey|Perseguir presa|modificada|
|[h8HPqnXI3dI7KFxV.htm](kingmaker-bestiary-items/h8HPqnXI3dI7KFxV.htm)|Hoof|Hoof|modificada|
|[H8mIfS4BQ0XpEAtY.htm](kingmaker-bestiary-items/H8mIfS4BQ0XpEAtY.htm)|Launch Marbles|Lanzar Canicas|modificada|
|[H8UomLORlnQErtR4.htm](kingmaker-bestiary-items/H8UomLORlnQErtR4.htm)|Gnawing Bite|Muerdemuerde|modificada|
|[H9cKHc9OAHiXKDzf.htm](kingmaker-bestiary-items/H9cKHc9OAHiXKDzf.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[h9Ugyirm1B1g6pt6.htm](kingmaker-bestiary-items/h9Ugyirm1B1g6pt6.htm)|Jaws|Fauces|modificada|
|[hbBhekpAkC55ljY1.htm](kingmaker-bestiary-items/hbBhekpAkC55ljY1.htm)|Fleet Performer|Pies ligeros Interpretar|modificada|
|[hbOLSP9RZmA3hyU8.htm](kingmaker-bestiary-items/hbOLSP9RZmA3hyU8.htm)|+1 Status to All Saves vs. Poison|+1 situación a todas las salvaciones contra veneno.|modificada|
|[Hczu635vDDik3NIJ.htm](kingmaker-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|Agarrado|modificada|
|[hE1MVc0Pxfi3nERv.htm](kingmaker-bestiary-items/hE1MVc0Pxfi3nERv.htm)|Greatsword|Greatsword|modificada|
|[hFDR91zB1cjh1UHv.htm](kingmaker-bestiary-items/hFDR91zB1cjh1UHv.htm)|Dead Tree|Árbol Muerto|modificada|
|[HfIRzOBnqZx60iSO.htm](kingmaker-bestiary-items/HfIRzOBnqZx60iSO.htm)|Club|Club|modificada|
|[hhOHcOSPkhLdvric.htm](kingmaker-bestiary-items/hhOHcOSPkhLdvric.htm)|Confusing Gaze|Mirada confusión|modificada|
|[hID13CF39qYYQ2rX.htm](kingmaker-bestiary-items/hID13CF39qYYQ2rX.htm)|Collapse|Colapso|modificada|
|[HiL2TCMI2Rzmy2SW.htm](kingmaker-bestiary-items/HiL2TCMI2Rzmy2SW.htm)|Juggernaut|Juggernaut|modificada|
|[HiZTEnboQEoek6kL.htm](kingmaker-bestiary-items/HiZTEnboQEoek6kL.htm)|Three-Headed Strike|Golpe tricéfalo|modificada|
|[hJ1mAKCkNvCy1GY0.htm](kingmaker-bestiary-items/hJ1mAKCkNvCy1GY0.htm)|Fangs|Colmillos|modificada|
|[HjqV5crA9cRbpUVj.htm](kingmaker-bestiary-items/HjqV5crA9cRbpUVj.htm)|Electric Surge|Oleaje Eléctrico|modificada|
|[HjxDZjwRHwjQx54V.htm](kingmaker-bestiary-items/HjxDZjwRHwjQx54V.htm)|Spear|Lanza|modificada|
|[HKEKbaIpMla2LGS8.htm](kingmaker-bestiary-items/HKEKbaIpMla2LGS8.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[hkgYTbnzCzYBfP7k.htm](kingmaker-bestiary-items/hkgYTbnzCzYBfP7k.htm)|Voice Imitation|Imitación de voz|modificada|
|[HLcMADTgT1nixxce.htm](kingmaker-bestiary-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|Aliento de la ciénaga|modificada|
|[HNfXE0yusiYA2juN.htm](kingmaker-bestiary-items/HNfXE0yusiYA2juN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hO1dPgyMTWEGJRxH.htm](kingmaker-bestiary-items/hO1dPgyMTWEGJRxH.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[hook1XGZNlxXtjvJ.htm](kingmaker-bestiary-items/hook1XGZNlxXtjvJ.htm)|Fangs|Colmillos|modificada|
|[HQAGq6saSYHzJC0I.htm](kingmaker-bestiary-items/HQAGq6saSYHzJC0I.htm)|Three Headed|Tres cabezas|modificada|
|[hqm0z6fZAXCCTDOs.htm](kingmaker-bestiary-items/hqm0z6fZAXCCTDOs.htm)|Wild Stride|Zancada salvaje|modificada|
|[hr8UQcvJzWfTNGoH.htm](kingmaker-bestiary-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 15|Curación rápida planar 15|modificada|
|[HrP24kDK1jVss45v.htm](kingmaker-bestiary-items/HrP24kDK1jVss45v.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[hSGaU1UT8mzmPFWl.htm](kingmaker-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[hsn4E9lEPjjDCMAN.htm](kingmaker-bestiary-items/hsn4E9lEPjjDCMAN.htm)|Occult Focus Spells|Conjuros de foco oculto|modificada|
|[htpcG9CC3rJNay4j.htm](kingmaker-bestiary-items/htpcG9CC3rJNay4j.htm)|Fist|Puño|modificada|
|[HtScT1S9mxNqMylg.htm](kingmaker-bestiary-items/HtScT1S9mxNqMylg.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[HWNQMuf5GV8rRSMu.htm](kingmaker-bestiary-items/HWNQMuf5GV8rRSMu.htm)|Frightful Presence|Frightful Presence|modificada|
|[hwXxwT85s7KyKoWq.htm](kingmaker-bestiary-items/hwXxwT85s7KyKoWq.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[hxeZesdJLgHPkEJ5.htm](kingmaker-bestiary-items/hxeZesdJLgHPkEJ5.htm)|Spectral Hand|Mano espectral|modificada|
|[HYAfgOkdktmrrKGf.htm](kingmaker-bestiary-items/HYAfgOkdktmrrKGf.htm)|Stygian Fire|Stygian Fire|modificada|
|[HYnUw0h1bJQMkzBc.htm](kingmaker-bestiary-items/HYnUw0h1bJQMkzBc.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[hZbArhODPV9EmZ0m.htm](kingmaker-bestiary-items/hZbArhODPV9EmZ0m.htm)|Sound Imitation|Imitación de sonidos|modificada|
|[hznpxgtC8SziycgN.htm](kingmaker-bestiary-items/hznpxgtC8SziycgN.htm)|Hatchet Onslaught|Hatchet Onslaught|modificada|
|[HZvbzYmbccVoGV1B.htm](kingmaker-bestiary-items/HZvbzYmbccVoGV1B.htm)|Maul Mastery|Zarpazo doble Mastery|modificada|
|[HZwLWbZdPHnymsUr.htm](kingmaker-bestiary-items/HZwLWbZdPHnymsUr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[i42oMxLUkfr8rkMc.htm](kingmaker-bestiary-items/i42oMxLUkfr8rkMc.htm)|Open Doors|Puertas Abiertas|modificada|
|[i5DVxzz2vN9zcxsC.htm](kingmaker-bestiary-items/i5DVxzz2vN9zcxsC.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[i8ykQ7pFedbfWxeV.htm](kingmaker-bestiary-items/i8ykQ7pFedbfWxeV.htm)|Staff|Báculo|modificada|
|[i98RWuQkObalnsWO.htm](kingmaker-bestiary-items/i98RWuQkObalnsWO.htm)|Shortbow|Arco corto|modificada|
|[IDIM2wkSDDgMI06V.htm](kingmaker-bestiary-items/IDIM2wkSDDgMI06V.htm)|Grasping Roots|Enraizamiento|modificada|
|[iE3cMVqsLzS8aqvH.htm](kingmaker-bestiary-items/iE3cMVqsLzS8aqvH.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[ifhXNlwgfGtUNABs.htm](kingmaker-bestiary-items/ifhXNlwgfGtUNABs.htm)|Reactive Charge|Carga reactiva|modificada|
|[IFp4VK6X80nXiTJu.htm](kingmaker-bestiary-items/IFp4VK6X80nXiTJu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iGnRoOsUlKx1zO7Q.htm](kingmaker-bestiary-items/iGnRoOsUlKx1zO7Q.htm)|Death Throes|Estertores de muerte|modificada|
|[ihGm6kH3jaNxYb3p.htm](kingmaker-bestiary-items/ihGm6kH3jaNxYb3p.htm)|Tremorsense (Precise) 30 feet|Sentido del temblor (precisión) 30 pies.|modificada|
|[ihI9TF3YdNJFYytf.htm](kingmaker-bestiary-items/ihI9TF3YdNJFYytf.htm)|Trample|Trample|modificada|
|[ik0FmQbY7s9wzZh2.htm](kingmaker-bestiary-items/ik0FmQbY7s9wzZh2.htm)|Wild Shape|Forma salvaje|modificada|
|[IkPoWCmWZcyW5osI.htm](kingmaker-bestiary-items/IkPoWCmWZcyW5osI.htm)|Staff|Báculo|modificada|
|[IKXHj9lMhQ6Qy07y.htm](kingmaker-bestiary-items/IKXHj9lMhQ6Qy07y.htm)|Rend|Rasgadura|modificada|
|[ILS10PAagocQkyqT.htm](kingmaker-bestiary-items/ILS10PAagocQkyqT.htm)|Hunt Prey|Perseguir presa|modificada|
|[ImYiua3KKxG5XU61.htm](kingmaker-bestiary-items/ImYiua3KKxG5XU61.htm)|Play the Pipes|Tocar la gaita|modificada|
|[ImzpAPq1SgMcf7t3.htm](kingmaker-bestiary-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|Perseguir presa|modificada|
|[In6OZtLo0Avr0mV1.htm](kingmaker-bestiary-items/In6OZtLo0Avr0mV1.htm)|Trident|Trident|modificada|
|[inAbFfjyYxWyFk2e.htm](kingmaker-bestiary-items/inAbFfjyYxWyFk2e.htm)|Prostrate|Postrado|modificada|
|[Io2JD6hMiVboalmM.htm](kingmaker-bestiary-items/Io2JD6hMiVboalmM.htm)|Cunning|Astucia|modificada|
|[iOA6wNMnIzBStwhB.htm](kingmaker-bestiary-items/iOA6wNMnIzBStwhB.htm)|Return to Pavetta|Retornar a Pavetta|modificada|
|[IoKSTbFElqXPiGiX.htm](kingmaker-bestiary-items/IoKSTbFElqXPiGiX.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ipARSA7VFfyApjl1.htm](kingmaker-bestiary-items/ipARSA7VFfyApjl1.htm)|Throw Rock|Arrojar roca|modificada|
|[iqKVeVAvsVEgW29e.htm](kingmaker-bestiary-items/iqKVeVAvsVEgW29e.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[iT766eWfAydCiJYK.htm](kingmaker-bestiary-items/iT766eWfAydCiJYK.htm)|Mandibles|Mandíbulas|modificada|
|[iTAZj4rMOPRWWJDP.htm](kingmaker-bestiary-items/iTAZj4rMOPRWWJDP.htm)|Corpse Throwing|Lanzamiento de cadáveres|modificada|
|[ITgYWyiUVLD2SpLl.htm](kingmaker-bestiary-items/ITgYWyiUVLD2SpLl.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[iu1YHlbvh64HBaor.htm](kingmaker-bestiary-items/iu1YHlbvh64HBaor.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[IUA7ZWVTepsameDv.htm](kingmaker-bestiary-items/IUA7ZWVTepsameDv.htm)|Fangs|Colmillos|modificada|
|[IuK4DrLEyP6BmKi2.htm](kingmaker-bestiary-items/IuK4DrLEyP6BmKi2.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[iVaCyzg2MMKJ1SIu.htm](kingmaker-bestiary-items/iVaCyzg2MMKJ1SIu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IvFL4oT7wjrxQZqw.htm](kingmaker-bestiary-items/IvFL4oT7wjrxQZqw.htm)|Claw|Garra|modificada|
|[IW0bpDD32dNAHnum.htm](kingmaker-bestiary-items/IW0bpDD32dNAHnum.htm)|Frumious Charge|Carga Frumiosa|modificada|
|[IYbU9IiWVPm4CGyc.htm](kingmaker-bestiary-items/IYbU9IiWVPm4CGyc.htm)|Fist|Puño|modificada|
|[Iyw7GZBX5oMU1kR0.htm](kingmaker-bestiary-items/Iyw7GZBX5oMU1kR0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IzbfeuZKm6hz1cWS.htm](kingmaker-bestiary-items/IzbfeuZKm6hz1cWS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IzVf6cehi9NajoXy.htm](kingmaker-bestiary-items/IzVf6cehi9NajoXy.htm)|Trident Twist|Trident Twist|modificada|
|[j1GC3KLLJa3BPe0o.htm](kingmaker-bestiary-items/j1GC3KLLJa3BPe0o.htm)|Battle Axe|Hacha de batalla|modificada|
|[J2LUtQlsnbpMEQ1r.htm](kingmaker-bestiary-items/J2LUtQlsnbpMEQ1r.htm)|Tongue|Lengua|modificada|
|[j2XC7qI2EMqGvGSs.htm](kingmaker-bestiary-items/j2XC7qI2EMqGvGSs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[J3lzyo3f2n6RNBG4.htm](kingmaker-bestiary-items/J3lzyo3f2n6RNBG4.htm)|Rushing Water|Embestida de agua|modificada|
|[j4kcdayowXPW4Avm.htm](kingmaker-bestiary-items/j4kcdayowXPW4Avm.htm)|Horns|Cuernos|modificada|
|[J4SHx8p5wCH9xlMo.htm](kingmaker-bestiary-items/J4SHx8p5wCH9xlMo.htm)|Clench Jaws|Cerrar la mandíbula|modificada|
|[J73AKz9wgVvUQWFo.htm](kingmaker-bestiary-items/J73AKz9wgVvUQWFo.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[j8nRwfwxCyxXRet5.htm](kingmaker-bestiary-items/j8nRwfwxCyxXRet5.htm)|Dagger|Daga|modificada|
|[JAh7SWOTsIXvrckz.htm](kingmaker-bestiary-items/JAh7SWOTsIXvrckz.htm)|Water Travel|Water Travel|modificada|
|[jAX00kRCiI1ahLJA.htm](kingmaker-bestiary-items/jAX00kRCiI1ahLJA.htm)|Bow Specialist|Especialista en arcos|modificada|
|[jBAYmOeepk3w4EgS.htm](kingmaker-bestiary-items/jBAYmOeepk3w4EgS.htm)|Rend|Rasgadura|modificada|
|[jCQO2RfqPLKqDeuF.htm](kingmaker-bestiary-items/jCQO2RfqPLKqDeuF.htm)|Slow Susceptibility|Susceptibilidad lentificada/a|modificada|
|[JdsDXPVL1v4JEQt5.htm](kingmaker-bestiary-items/JdsDXPVL1v4JEQt5.htm)|Wing|Ala|modificada|
|[jEkasU1qDlN76A9i.htm](kingmaker-bestiary-items/jEkasU1qDlN76A9i.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[JfR3IkhT8g6aU9ry.htm](kingmaker-bestiary-items/JfR3IkhT8g6aU9ry.htm)|Scythe Branch|Rama Guadaña|modificada|
|[jga4jzrMzJRkNouq.htm](kingmaker-bestiary-items/jga4jzrMzJRkNouq.htm)|Formation Fighters|Formación Combatientes|modificada|
|[JjphR7198vaXiAJo.htm](kingmaker-bestiary-items/JjphR7198vaXiAJo.htm)|Defensive Roll|Rodar a la defensiva|modificada|
|[JJTobEFnvJPV524S.htm](kingmaker-bestiary-items/JJTobEFnvJPV524S.htm)|Vorpal Fear|Miedo Vorpalina|modificada|
|[jlJLCgnS5JMrSDgN.htm](kingmaker-bestiary-items/jlJLCgnS5JMrSDgN.htm)|Tear Free|Tear Free|modificada|
|[jmgh011n8MXGN953.htm](kingmaker-bestiary-items/jmgh011n8MXGN953.htm)|Dagger|Daga|modificada|
|[jmL1vjBAG3jha7pe.htm](kingmaker-bestiary-items/jmL1vjBAG3jha7pe.htm)|Stabbing Fit|Ajuste de apuñalamiento|modificada|
|[Jn2HBbRFuiRxKRbB.htm](kingmaker-bestiary-items/Jn2HBbRFuiRxKRbB.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[JoOzphwUQC4ZFm7J.htm](kingmaker-bestiary-items/JoOzphwUQC4ZFm7J.htm)|Saving Slash|Saving Slash|modificada|
|[jp5lLzCnJEJ3qCUl.htm](kingmaker-bestiary-items/jp5lLzCnJEJ3qCUl.htm)|Arcane Spells Prepared|Hechizos Arcanos Preparados|modificada|
|[JP9GiA6aaQlXRKAE.htm](kingmaker-bestiary-items/JP9GiA6aaQlXRKAE.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[JPmOqF1x4Au8drWv.htm](kingmaker-bestiary-items/JPmOqF1x4Au8drWv.htm)|Spectral Uprising|Sublevación Espectral|modificada|
|[jppcxZjbxQ0e8W30.htm](kingmaker-bestiary-items/jppcxZjbxQ0e8W30.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[Jspfa7SNI6gNhPK4.htm](kingmaker-bestiary-items/Jspfa7SNI6gNhPK4.htm)|Coven|Coven|modificada|
|[jTkhIBakBrDpEBuu.htm](kingmaker-bestiary-items/jTkhIBakBrDpEBuu.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[jU73I507kdECl13G.htm](kingmaker-bestiary-items/jU73I507kdECl13G.htm)|Contingency|Contingencia|modificada|
|[jUFQA2dWOvLpv6zg.htm](kingmaker-bestiary-items/jUFQA2dWOvLpv6zg.htm)|Fist|Puño|modificada|
|[jvDyUbBwbo3R1H4x.htm](kingmaker-bestiary-items/jvDyUbBwbo3R1H4x.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[jVnAGrnHMY0ZZTAt.htm](kingmaker-bestiary-items/jVnAGrnHMY0ZZTAt.htm)|Negative Healing|Curación negativa|modificada|
|[JWMAFJOdy4Odx7RY.htm](kingmaker-bestiary-items/JWMAFJOdy4Odx7RY.htm)|Playful Switch|Playful Switch|modificada|
|[JXtD0GzHXfGW7Aan.htm](kingmaker-bestiary-items/JXtD0GzHXfGW7Aan.htm)|Deny Advantage|Denegar ventaja|modificada|
|[jXwGpQmNRAqEcUQN.htm](kingmaker-bestiary-items/jXwGpQmNRAqEcUQN.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[jyTZDCNVazHG1tSk.htm](kingmaker-bestiary-items/jyTZDCNVazHG1tSk.htm)|Burst|Ráfaga|modificada|
|[jZ8U8eTiEuKU6toZ.htm](kingmaker-bestiary-items/jZ8U8eTiEuKU6toZ.htm)|Heat|Calor|modificada|
|[JzdZZiLkLG0WmOid.htm](kingmaker-bestiary-items/JzdZZiLkLG0WmOid.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[jZfqY5quDvHq9tmh.htm](kingmaker-bestiary-items/jZfqY5quDvHq9tmh.htm)|Wild Empathy|Empatía salvaje|modificada|
|[jzVssayBq4mzMU9u.htm](kingmaker-bestiary-items/jzVssayBq4mzMU9u.htm)|Darkvision|Visión en la oscuridad|modificada|
|[k1LT9AcDMUPeGPOy.htm](kingmaker-bestiary-items/k1LT9AcDMUPeGPOy.htm)|Righteous Certainty|Righteous Certainty|modificada|
|[k2HAX2N3qaYcbpb8.htm](kingmaker-bestiary-items/k2HAX2N3qaYcbpb8.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[k2R72XvX3USofjXB.htm](kingmaker-bestiary-items/k2R72XvX3USofjXB.htm)|Lash Out|Arremeter|modificada|
|[K3AfPmsD7hzdbhv0.htm](kingmaker-bestiary-items/K3AfPmsD7hzdbhv0.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[K470oCZYTiRoTfqf.htm](kingmaker-bestiary-items/K470oCZYTiRoTfqf.htm)|Jaws|Fauces|modificada|
|[K5qMhCPST2MiSV7J.htm](kingmaker-bestiary-items/K5qMhCPST2MiSV7J.htm)|Inveigled|Embaucamiento|modificada|
|[k6dnEhWODfyRtg4b.htm](kingmaker-bestiary-items/k6dnEhWODfyRtg4b.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[K7J1kJsdpeoiTUqZ.htm](kingmaker-bestiary-items/K7J1kJsdpeoiTUqZ.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[k9kbOoeLWV3Bx8l5.htm](kingmaker-bestiary-items/k9kbOoeLWV3Bx8l5.htm)|Moment of Solitude|Momento de soledad|modificada|
|[Ka4LviiNUhy4FNzo.htm](kingmaker-bestiary-items/Ka4LviiNUhy4FNzo.htm)|Talon|Talon|modificada|
|[kaPLdey2Dv3F1VUN.htm](kingmaker-bestiary-items/kaPLdey2Dv3F1VUN.htm)|Oathbow|Oathbow|modificada|
|[kBr3AKRl4vlCwVK2.htm](kingmaker-bestiary-items/kBr3AKRl4vlCwVK2.htm)|Spiked Greatclub|Spiked Greatclub|modificada|
|[KbYGZddUNQGhrhEf.htm](kingmaker-bestiary-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[KCOXqXVayJMKPZVz.htm](kingmaker-bestiary-items/KCOXqXVayJMKPZVz.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[kdl1FTleQZfZf6eP.htm](kingmaker-bestiary-items/kdl1FTleQZfZf6eP.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[KEm1y8hEl4sjQ6Cv.htm](kingmaker-bestiary-items/KEm1y8hEl4sjQ6Cv.htm)|Talon|Talon|modificada|
|[KF8TJluc2GTgVJhl.htm](kingmaker-bestiary-items/KF8TJluc2GTgVJhl.htm)|Paddle|Paddle|modificada|
|[kfdzBPs02IvVMabf.htm](kingmaker-bestiary-items/kfdzBPs02IvVMabf.htm)|Focus Gaze|Centrar mirada|modificada|
|[kh9EPJwZWvajnZBV.htm](kingmaker-bestiary-items/kh9EPJwZWvajnZBV.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[KhgTIBtP3uB7Ls0w.htm](kingmaker-bestiary-items/KhgTIBtP3uB7Ls0w.htm)|Fireball Breath|Aliento de bola de fuego|modificada|
|[khvFVWc6itmsBv7T.htm](kingmaker-bestiary-items/khvFVWc6itmsBv7T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[khwh43CDMROkIfgf.htm](kingmaker-bestiary-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|Sentido del Temblor (Impreciso) 30 pies|modificada|
|[KIhN4LqmxLYnSLJj.htm](kingmaker-bestiary-items/KIhN4LqmxLYnSLJj.htm)|Change Shape|Change Shape|modificada|
|[kJ9e1DXfbX04Qfoe.htm](kingmaker-bestiary-items/kJ9e1DXfbX04Qfoe.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[klmOYsNgGYaAMROh.htm](kingmaker-bestiary-items/klmOYsNgGYaAMROh.htm)|Knockback|Arredrar|modificada|
|[kMVtB4pZd0LYoYOm.htm](kingmaker-bestiary-items/kMVtB4pZd0LYoYOm.htm)|Slam Doors|Portazo|modificada|
|[KngnsUrW0RbmKrce.htm](kingmaker-bestiary-items/KngnsUrW0RbmKrce.htm)|Focus Beauty|Focus Belleza|modificada|
|[KNywleLC7uqltyig.htm](kingmaker-bestiary-items/KNywleLC7uqltyig.htm)|Focus Gaze|Centrar mirada|modificada|
|[Kok7cEEC0PeallmB.htm](kingmaker-bestiary-items/Kok7cEEC0PeallmB.htm)|Horns|Cuernos|modificada|
|[kooXn2rvr04D9fM6.htm](kingmaker-bestiary-items/kooXn2rvr04D9fM6.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[koqEAUHXo9t7wqVS.htm](kingmaker-bestiary-items/koqEAUHXo9t7wqVS.htm)|Mace|Maza|modificada|
|[kPaYMeS20gIcKi3E.htm](kingmaker-bestiary-items/kPaYMeS20gIcKi3E.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[KpFmMp8bJz2ZkMQN.htm](kingmaker-bestiary-items/KpFmMp8bJz2ZkMQN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[kq0boAEZsA01NsEH.htm](kingmaker-bestiary-items/kq0boAEZsA01NsEH.htm)|Powerful Dive|Picado poderoso|modificada|
|[krTcoq8OKd5gFAL0.htm](kingmaker-bestiary-items/krTcoq8OKd5gFAL0.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[KtA9MV7RA5nWRmko.htm](kingmaker-bestiary-items/KtA9MV7RA5nWRmko.htm)|Blinding Sickness|Enfermedad cegadora|modificada|
|[kTN86guVl5qzlzIK.htm](kingmaker-bestiary-items/kTN86guVl5qzlzIK.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[KTxjm0shb9zyrh0z.htm](kingmaker-bestiary-items/KTxjm0shb9zyrh0z.htm)|Drain Life|Drenar Vida|modificada|
|[kU6LiydL3jldv5KQ.htm](kingmaker-bestiary-items/kU6LiydL3jldv5KQ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[KUcCUd82J1JqXWE5.htm](kingmaker-bestiary-items/KUcCUd82J1JqXWE5.htm)|Breath Weapon|Breath Weapon|modificada|
|[KuURUnbzeAf0EaR1.htm](kingmaker-bestiary-items/KuURUnbzeAf0EaR1.htm)|Catch Rock|Atrapar roca|modificada|
|[kuV1kCq4ZEiH9DzH.htm](kingmaker-bestiary-items/kuV1kCq4ZEiH9DzH.htm)|Vulnerable to Curved Space|Vulnerable al espacio curvo|modificada|
|[kv7A4qqpl2mG1nHt.htm](kingmaker-bestiary-items/kv7A4qqpl2mG1nHt.htm)|Quickened Casting|Lanzamiento apresurado/a|modificada|
|[kX089KrOjeO3aDf3.htm](kingmaker-bestiary-items/kX089KrOjeO3aDf3.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[KXuuj04cishnGNpk.htm](kingmaker-bestiary-items/KXuuj04cishnGNpk.htm)|Trapdoor Lunge|Trapdoor Acometer|modificada|
|[kZiYWe3uWt1JBdbS.htm](kingmaker-bestiary-items/kZiYWe3uWt1JBdbS.htm)|Familiar|Familiar|modificada|
|[L1NeMv3ZDcttKLw2.htm](kingmaker-bestiary-items/L1NeMv3ZDcttKLw2.htm)|Snatch|Arrebatar|modificada|
|[L4dKqPtdmyspmZtd.htm](kingmaker-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|Fauces|modificada|
|[l4eGICy0QK1EEERs.htm](kingmaker-bestiary-items/l4eGICy0QK1EEERs.htm)|Jaws|Fauces|modificada|
|[l6mRpAGFCZ7gTIb1.htm](kingmaker-bestiary-items/l6mRpAGFCZ7gTIb1.htm)|Rasping Tongue|Rasping Tongue|modificada|
|[LA4f99Yw7Dk1e16d.htm](kingmaker-bestiary-items/LA4f99Yw7Dk1e16d.htm)|Fast Healing 10 (in dust or sand)|Curación rápida 10 (en polvo o arena)|modificada|
|[laXpsxFAPFs8EXkA.htm](kingmaker-bestiary-items/laXpsxFAPFs8EXkA.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[LbdwyQgOrSBqDgv9.htm](kingmaker-bestiary-items/LbdwyQgOrSBqDgv9.htm)|Dagger|Daga|modificada|
|[LC8WDqxmulYb1L4q.htm](kingmaker-bestiary-items/LC8WDqxmulYb1L4q.htm)|Dagger|Daga|modificada|
|[LcG0o3PUyemEv5oK.htm](kingmaker-bestiary-items/LcG0o3PUyemEv5oK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[lCn44iSRdDgu2GdC.htm](kingmaker-bestiary-items/lCn44iSRdDgu2GdC.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[LCVw81WewWAm0wOB.htm](kingmaker-bestiary-items/LCVw81WewWAm0wOB.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[LDLwyqZVioLuytc9.htm](kingmaker-bestiary-items/LDLwyqZVioLuytc9.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LEDnvueR2NzH8zkD.htm](kingmaker-bestiary-items/LEDnvueR2NzH8zkD.htm)|Dagger|Daga|modificada|
|[leYjDFOKDNCIZOBf.htm](kingmaker-bestiary-items/leYjDFOKDNCIZOBf.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[lffL96H4Twel1YRH.htm](kingmaker-bestiary-items/lffL96H4Twel1YRH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LFvM7oAwEJD1soNx.htm](kingmaker-bestiary-items/LFvM7oAwEJD1soNx.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[lGRlzXnOSNGW7djU.htm](kingmaker-bestiary-items/lGRlzXnOSNGW7djU.htm)|Shame|Vergüenza|modificada|
|[LHmME2otANk4a79s.htm](kingmaker-bestiary-items/LHmME2otANk4a79s.htm)|Worm Jaws|Fauces de Gusano|modificada|
|[LHPXbhgsSYCjB4uG.htm](kingmaker-bestiary-items/LHPXbhgsSYCjB4uG.htm)|Club|Club|modificada|
|[LIdmD7VqztundfTh.htm](kingmaker-bestiary-items/LIdmD7VqztundfTh.htm)|Talon|Talon|modificada|
|[lim8vhAnHsss3MkL.htm](kingmaker-bestiary-items/lim8vhAnHsss3MkL.htm)|Light Hammer|Martillo ligero|modificada|
|[LK0wAtZtoKTLOgnz.htm](kingmaker-bestiary-items/LK0wAtZtoKTLOgnz.htm)|Claw|Garra|modificada|
|[lLQgAigQauz0Z3sx.htm](kingmaker-bestiary-items/lLQgAigQauz0Z3sx.htm)|Designate Blood Foe|Designar Enemigo de Sangre|modificada|
|[lnlNJpjHNjw06Wzi.htm](kingmaker-bestiary-items/lnlNJpjHNjw06Wzi.htm)|Tusk|Tusk|modificada|
|[lOVrcn3AJGm4KWma.htm](kingmaker-bestiary-items/lOVrcn3AJGm4KWma.htm)|Eyes Of Flame|Eyes Of Flamígera|modificada|
|[lP5ul9ZfSW3FEFpa.htm](kingmaker-bestiary-items/lP5ul9ZfSW3FEFpa.htm)|Swarm Mind|Swarm Mind|modificada|
|[Lq9tywTCezzL2UYy.htm](kingmaker-bestiary-items/Lq9tywTCezzL2UYy.htm)|Lamashtu's Bloom|Florecimiento de Lamashtu|modificada|
|[lqfoMvtMondVCDSV.htm](kingmaker-bestiary-items/lqfoMvtMondVCDSV.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[Lri10NA5TEU7fZeX.htm](kingmaker-bestiary-items/Lri10NA5TEU7fZeX.htm)|Call to the Hunt|Llamada, la Caza|modificada|
|[LRT2YJWR1S18u4vp.htm](kingmaker-bestiary-items/LRT2YJWR1S18u4vp.htm)|Shortsword|Espada corta|modificada|
|[LsUtyTnBZPF6Hvnp.htm](kingmaker-bestiary-items/LsUtyTnBZPF6Hvnp.htm)|Frightful Presence|Frightful Presence|modificada|
|[LTaIE8s2797qElxg.htm](kingmaker-bestiary-items/LTaIE8s2797qElxg.htm)|Dagger|Daga|modificada|
|[LTLa6hS15bgHkOBi.htm](kingmaker-bestiary-items/LTLa6hS15bgHkOBi.htm)|Protection from Decapitation|Protección contra decapitación.|modificada|
|[ltvMN7K0itu2l3WZ.htm](kingmaker-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|Colmillos de Serpiente|modificada|
|[luiDDR5D3oiCAEZh.htm](kingmaker-bestiary-items/luiDDR5D3oiCAEZh.htm)|Hatchet|Hacha|modificada|
|[lUMpyYokJ5dTTdu9.htm](kingmaker-bestiary-items/lUMpyYokJ5dTTdu9.htm)|Blood Drain|Drenar sangre|modificada|
|[luyr4NvU4euUL7Dh.htm](kingmaker-bestiary-items/luyr4NvU4euUL7Dh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LxhUmOCBKZi7FNfa.htm](kingmaker-bestiary-items/LxhUmOCBKZi7FNfa.htm)|Betraying Touch|Toque traicionero|modificada|
|[lxsWZN09WFvanWVx.htm](kingmaker-bestiary-items/lxsWZN09WFvanWVx.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[LXvqvVhorWXVewtI.htm](kingmaker-bestiary-items/LXvqvVhorWXVewtI.htm)|Twist Time|Twist Time|modificada|
|[lYCCv7NRnl6ZnNjT.htm](kingmaker-bestiary-items/lYCCv7NRnl6ZnNjT.htm)|Constant Spells|Constant Spells|modificada|
|[lYx7vXEHaPIDLbWp.htm](kingmaker-bestiary-items/lYx7vXEHaPIDLbWp.htm)|Sickle|Hoz|modificada|
|[Lz7LqoEOMbBrTosy.htm](kingmaker-bestiary-items/Lz7LqoEOMbBrTosy.htm)|Repulsing Blow|Golpe repulsión|modificada|
|[LZhDKd6gpJJ5o2RO.htm](kingmaker-bestiary-items/LZhDKd6gpJJ5o2RO.htm)|Aura of Disquietude|Aura de Inquietud|modificada|
|[m0MCsCQ2HU7X7Rr2.htm](kingmaker-bestiary-items/m0MCsCQ2HU7X7Rr2.htm)|Claw|Garra|modificada|
|[m2aEAnMnepSKN57G.htm](kingmaker-bestiary-items/m2aEAnMnepSKN57G.htm)|Head Regrowth|Crecer cabezas|modificada|
|[M4f06NOUIcSyQNr2.htm](kingmaker-bestiary-items/M4f06NOUIcSyQNr2.htm)|Regeneration 45 (Deactivated by Acid or Fire)|Regeneración 45 (Desactivado por Ácido o Fuego)|modificada|
|[m4LUwAovVkqwRsiG.htm](kingmaker-bestiary-items/m4LUwAovVkqwRsiG.htm)|Vengeful Rage|Furia vengativa|modificada|
|[m5He7mR6hyUEterZ.htm](kingmaker-bestiary-items/m5He7mR6hyUEterZ.htm)|Regeneration 30 (Deactivated by Fire or Good)|Regeneración 30 (Desactivado por Fuego o Bien).|modificada|
|[M7sZToYvFNrig8TH.htm](kingmaker-bestiary-items/M7sZToYvFNrig8TH.htm)|Constant Spells|Constant Spells|modificada|
|[M9wyjXrmWfCD7nBk.htm](kingmaker-bestiary-items/M9wyjXrmWfCD7nBk.htm)|Blinding Beauty|Belleza cegadora|modificada|
|[MBYJQAMX2qMwwp1o.htm](kingmaker-bestiary-items/MBYJQAMX2qMwwp1o.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[mc4qvML9Y9Vngpwo.htm](kingmaker-bestiary-items/mc4qvML9Y9Vngpwo.htm)|Thorny Vine|Thorny Vine|modificada|
|[mcMNYM92nivzmq61.htm](kingmaker-bestiary-items/mcMNYM92nivzmq61.htm)|+2 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[mCooSIipdGY8hP2o.htm](kingmaker-bestiary-items/mCooSIipdGY8hP2o.htm)|Living Bow|Living Bow|modificada|
|[mDmFiG2gsRPiENrP.htm](kingmaker-bestiary-items/mDmFiG2gsRPiENrP.htm)|Warp Teleportation|Teletransporte Warp|modificada|
|[MfVHl3j1UyN4VFks.htm](kingmaker-bestiary-items/MfVHl3j1UyN4VFks.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[MgV9p8g40O0uwDjs.htm](kingmaker-bestiary-items/MgV9p8g40O0uwDjs.htm)|Wild Gaze|Mirada salvaje|modificada|
|[MgVuD5shNkwpUCdM.htm](kingmaker-bestiary-items/MgVuD5shNkwpUCdM.htm)|Frightful Moan|Frightful Moan|modificada|
|[MGX1U8Kv59BVYAOI.htm](kingmaker-bestiary-items/MGX1U8Kv59BVYAOI.htm)|Push or Pull 10 feet|Empuja o tira 3 metros|modificada|
|[mGzWQjNPCtlz97Cn.htm](kingmaker-bestiary-items/mGzWQjNPCtlz97Cn.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[mJ4kL0oQk4xWhXtX.htm](kingmaker-bestiary-items/mJ4kL0oQk4xWhXtX.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Mj7eIolH2KmKRos4.htm](kingmaker-bestiary-items/Mj7eIolH2KmKRos4.htm)|Wyvern Venom|Wyvern Veneno|modificada|
|[MJktJ33e2ejK5I1o.htm](kingmaker-bestiary-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|Muerdemuerde|modificada|
|[mkO8uUk9bttHYIRn.htm](kingmaker-bestiary-items/mkO8uUk9bttHYIRn.htm)|Snake Fangs|Colmillos de Serpiente|modificada|
|[Mkzdftre3Tzz3aVM.htm](kingmaker-bestiary-items/Mkzdftre3Tzz3aVM.htm)|Drop From Ceiling|Drop From Ceiling|modificada|
|[MMamQCPSgaxFWQd4.htm](kingmaker-bestiary-items/MMamQCPSgaxFWQd4.htm)|Whip|Látigo|modificada|
|[mMCpgVRLUjGayWTB.htm](kingmaker-bestiary-items/mMCpgVRLUjGayWTB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mmgcrA8q8AjJiPoF.htm](kingmaker-bestiary-items/mmgcrA8q8AjJiPoF.htm)|Shuriken|Shuriken|modificada|
|[mMPWudSAzEWfl1eN.htm](kingmaker-bestiary-items/mMPWudSAzEWfl1eN.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[mNfAyKfUwSuMResA.htm](kingmaker-bestiary-items/mNfAyKfUwSuMResA.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[moA510VYoGxOfach.htm](kingmaker-bestiary-items/moA510VYoGxOfach.htm)|Recall Lintwerth|Recall Lintwerth|modificada|
|[MP8qtlyLZ6pRMVZl.htm](kingmaker-bestiary-items/MP8qtlyLZ6pRMVZl.htm)|Dagger|Daga|modificada|
|[MQbQ4WqtcjmvDsJS.htm](kingmaker-bestiary-items/MQbQ4WqtcjmvDsJS.htm)|Seeking Shots|Buscar disparos|modificada|
|[mqqrT74LZMyFyuic.htm](kingmaker-bestiary-items/mqqrT74LZMyFyuic.htm)|Jaws|Fauces|modificada|
|[Mqryx0fWVqgV0RiU.htm](kingmaker-bestiary-items/Mqryx0fWVqgV0RiU.htm)|Mauler|Vapulear|modificada|
|[MqUTUiuHEWGtwSkQ.htm](kingmaker-bestiary-items/MqUTUiuHEWGtwSkQ.htm)|Instinctive Cooperation|Cooperación instintiva|modificada|
|[MrbeKdSngiqGczPC.htm](kingmaker-bestiary-items/MrbeKdSngiqGczPC.htm)|Glaive|Glaive|modificada|
|[mSjLHeaPtyB5Sjcm.htm](kingmaker-bestiary-items/mSjLHeaPtyB5Sjcm.htm)|Fist|Puño|modificada|
|[mTecTRC1ab5f2qOh.htm](kingmaker-bestiary-items/mTecTRC1ab5f2qOh.htm)|Negative Healing|Curación negativa|modificada|
|[MUZm9jerelh5o9lK.htm](kingmaker-bestiary-items/MUZm9jerelh5o9lK.htm)|Royal Command|Orden imperiosa|modificada|
|[MuzXu4bDzodvaZ7w.htm](kingmaker-bestiary-items/MuzXu4bDzodvaZ7w.htm)|Watery Transparency|Transparencia Acuosa|modificada|
|[mVgSuJseTKH5wKIO.htm](kingmaker-bestiary-items/mVgSuJseTKH5wKIO.htm)|Twin Takedown|Derribo gemelo|modificada|
|[MWff0Ro0CLotg930.htm](kingmaker-bestiary-items/MWff0Ro0CLotg930.htm)|Constrict|Restringir|modificada|
|[mwIVxTyDPXVZaEcf.htm](kingmaker-bestiary-items/mwIVxTyDPXVZaEcf.htm)|Fearsome Gaze|Fearsome Gaze|modificada|
|[MwxI4D0saKJpksvK.htm](kingmaker-bestiary-items/MwxI4D0saKJpksvK.htm)|Shoulder Slam|Golpe de hombro|modificada|
|[MxkYw0L8AzlUayt9.htm](kingmaker-bestiary-items/MxkYw0L8AzlUayt9.htm)|Rush|Embestida|modificada|
|[MxQXSU7ZH7FLyYf5.htm](kingmaker-bestiary-items/MxQXSU7ZH7FLyYf5.htm)|Scythe|Guadaña|modificada|
|[MZGshvRJ0ZlhdQ5k.htm](kingmaker-bestiary-items/MZGshvRJ0ZlhdQ5k.htm)|Rhetorical Spell|Hechizo Retórico|modificada|
|[MzmPJ00qwdaxKPhE.htm](kingmaker-bestiary-items/MzmPJ00qwdaxKPhE.htm)|Grab|Agarrado|modificada|
|[n1fjlb5osV2yl7as.htm](kingmaker-bestiary-items/n1fjlb5osV2yl7as.htm)|+4 Status to All Saves vs. Mental|+4 a la situación a todas las salvaciones contra mental.|modificada|
|[n1p0bJc7I91avM4S.htm](kingmaker-bestiary-items/n1p0bJc7I91avM4S.htm)|Wild Empathy|Empatía salvaje|modificada|
|[n1wsbA3ExF7FB1bG.htm](kingmaker-bestiary-items/n1wsbA3ExF7FB1bG.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[n2BHwPEdFYp07VvG.htm](kingmaker-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|Pseudópodo|modificada|
|[n4Cc3VF0F86FO0sG.htm](kingmaker-bestiary-items/n4Cc3VF0F86FO0sG.htm)|Cold Adaptation|Adaptación al frío|modificada|
|[N4dy1EmZsnQD8XR3.htm](kingmaker-bestiary-items/N4dy1EmZsnQD8XR3.htm)|Chill Breath|Aliento de frío|modificada|
|[N4EVKfsc2JGoy6hL.htm](kingmaker-bestiary-items/N4EVKfsc2JGoy6hL.htm)|Rider Synergy|Rider Synergy|modificada|
|[n4uBILCtoQcDwVBY.htm](kingmaker-bestiary-items/n4uBILCtoQcDwVBY.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[n5KuZKOcFvPmlkze.htm](kingmaker-bestiary-items/n5KuZKOcFvPmlkze.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[n5PTMIe2knU6k8WD.htm](kingmaker-bestiary-items/n5PTMIe2knU6k8WD.htm)|Spiked Pitfall|Trampa de caída con pinchos|modificada|
|[N7BomXiXSkV8IDmE.htm](kingmaker-bestiary-items/N7BomXiXSkV8IDmE.htm)|Perpetual Hangover|Resaca Perpetua|modificada|
|[N8EKVbUAmiDxtOlF.htm](kingmaker-bestiary-items/N8EKVbUAmiDxtOlF.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[NaDfoZlbd0uAIh3t.htm](kingmaker-bestiary-items/NaDfoZlbd0uAIh3t.htm)|Trunk|Tronco|modificada|
|[nbVDArIMsO5ljcOd.htm](kingmaker-bestiary-items/nbVDArIMsO5ljcOd.htm)|Claw|Garra|modificada|
|[nC28cnIAmXUrJhdf.htm](kingmaker-bestiary-items/nC28cnIAmXUrJhdf.htm)|Rage|Furia|modificada|
|[nCPtTG7EkbC5KQpL.htm](kingmaker-bestiary-items/nCPtTG7EkbC5KQpL.htm)|Furious Power Attack|Ataque poderoso furioso|modificada|
|[NcZMn3pf8nbMeY8x.htm](kingmaker-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nd0M4055DPNvjNvf.htm](kingmaker-bestiary-items/nd0M4055DPNvjNvf.htm)|Manifest Shawl|Manto Manifiesto|modificada|
|[NDm80a7dNC5LKMIS.htm](kingmaker-bestiary-items/NDm80a7dNC5LKMIS.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[NHDhjNbpULppyjkP.htm](kingmaker-bestiary-items/NHDhjNbpULppyjkP.htm)|Darkvision|Visión en la oscuridad|modificada|
|[nHs2Wp0Ifqpenmxk.htm](kingmaker-bestiary-items/nHs2Wp0Ifqpenmxk.htm)|Warhammer|Warhammer|modificada|
|[nHt7MnXhByHOX78E.htm](kingmaker-bestiary-items/nHt7MnXhByHOX78E.htm)|Tandem Chop|Tandem Chop|modificada|
|[NIgj8uAlgosxMdVf.htm](kingmaker-bestiary-items/NIgj8uAlgosxMdVf.htm)|Chew Spider Venom|Chew Spider Venom|modificada|
|[nkAaDbVaSoGjlqCp.htm](kingmaker-bestiary-items/nkAaDbVaSoGjlqCp.htm)|Poison Spray|Poison Spray|modificada|
|[NKFCPYANKZVEHAVK.htm](kingmaker-bestiary-items/NKFCPYANKZVEHAVK.htm)|No Escape|Sin huida posible|modificada|
|[nkFUtIxpiWKJ11ko.htm](kingmaker-bestiary-items/nkFUtIxpiWKJ11ko.htm)|Hand|Mano|modificada|
|[Nl15bkRTLq869IPi.htm](kingmaker-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[nNdV2vrrzVUlwrhp.htm](kingmaker-bestiary-items/nNdV2vrrzVUlwrhp.htm)|Fast Healing 15|Curación rápida 15|modificada|
|[NO9VvuwakH04tpcx.htm](kingmaker-bestiary-items/NO9VvuwakH04tpcx.htm)|Hatchet|Hacha|modificada|
|[nP9Bi95YRl4E9QMt.htm](kingmaker-bestiary-items/nP9Bi95YRl4E9QMt.htm)|Greataxe|Greataxe|modificada|
|[nrmfu7IJT3ecM1M8.htm](kingmaker-bestiary-items/nrmfu7IJT3ecM1M8.htm)|Beak|Beak|modificada|
|[nsDXE0HKZhrqgV54.htm](kingmaker-bestiary-items/nsDXE0HKZhrqgV54.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[ntcF2Dw0m1DqvDnw.htm](kingmaker-bestiary-items/ntcF2Dw0m1DqvDnw.htm)|Dream of Rulership|Sueño de Gobernación|modificada|
|[NTFiXa55jt87djrs.htm](kingmaker-bestiary-items/NTFiXa55jt87djrs.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[NTRKdsufuPkE5ERS.htm](kingmaker-bestiary-items/NTRKdsufuPkE5ERS.htm)|Aldori Parry|Aldori Parry|modificada|
|[Nu5hdhS5ABZ0CZjq.htm](kingmaker-bestiary-items/Nu5hdhS5ABZ0CZjq.htm)|Twin Butchery|Twin Butchery|modificada|
|[nUhHPT76LVNXNYRg.htm](kingmaker-bestiary-items/nUhHPT76LVNXNYRg.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[nvkbBLwKlX05vPo2.htm](kingmaker-bestiary-items/nvkbBLwKlX05vPo2.htm)|Breath Weapon|Breath Weapon|modificada|
|[NWflOL2JDt8KPMH6.htm](kingmaker-bestiary-items/NWflOL2JDt8KPMH6.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[Nzx1vI7LkdpuIwph.htm](kingmaker-bestiary-items/Nzx1vI7LkdpuIwph.htm)|Unseen Sight|Unseen Sight|modificada|
|[o1Dh44Cl7px3KGea.htm](kingmaker-bestiary-items/o1Dh44Cl7px3KGea.htm)|Grab|Agarrado|modificada|
|[O2EsiBYHC57XSjTz.htm](kingmaker-bestiary-items/O2EsiBYHC57XSjTz.htm)|Jaws|Fauces|modificada|
|[o2schqbpr94UUldL.htm](kingmaker-bestiary-items/o2schqbpr94UUldL.htm)|Hatchet|Hacha|modificada|
|[O2U2on80bNw6GMEe.htm](kingmaker-bestiary-items/O2U2on80bNw6GMEe.htm)|Trample|Trample|modificada|
|[o38MzuC03KOCw8wS.htm](kingmaker-bestiary-items/o38MzuC03KOCw8wS.htm)|Jaws That Bite|Mandíbulas que muerden|modificada|
|[o3sdERN8Ehqn7jo0.htm](kingmaker-bestiary-items/o3sdERN8Ehqn7jo0.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[O6kjx7rDPPICVkym.htm](kingmaker-bestiary-items/O6kjx7rDPPICVkym.htm)|Rapier|Estoque|modificada|
|[oAdazPfRl3wntiS1.htm](kingmaker-bestiary-items/oAdazPfRl3wntiS1.htm)|Attack of Opportunity (Fangs Only)|Ataque de oportunidad (sólo colmillos)|modificada|
|[ObX0PxwHHLMvaTJn.htm](kingmaker-bestiary-items/ObX0PxwHHLMvaTJn.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ocYHO8rNnbCQGplD.htm](kingmaker-bestiary-items/ocYHO8rNnbCQGplD.htm)|Bewildering Hoofbeats|Bewildering Hoofbeats|modificada|
|[oE4IMiBnWcfVyg7X.htm](kingmaker-bestiary-items/oE4IMiBnWcfVyg7X.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[Oe7KNrqiGTrNfkV9.htm](kingmaker-bestiary-items/Oe7KNrqiGTrNfkV9.htm)|Jaws|Fauces|modificada|
|[OenA92WNHdH1I94w.htm](kingmaker-bestiary-items/OenA92WNHdH1I94w.htm)|Conjure Daemon|Conjurar Daimonion|modificada|
|[Oevi96zftiaBjAOH.htm](kingmaker-bestiary-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[OFtZ11CEYiceib4W.htm](kingmaker-bestiary-items/OFtZ11CEYiceib4W.htm)|Rend|Rasgadura|modificada|
|[ogXJyvypuEP8U1hd.htm](kingmaker-bestiary-items/ogXJyvypuEP8U1hd.htm)|Bloom Venom|Bloom Venom|modificada|
|[ohbFrIxS5WF1abVb.htm](kingmaker-bestiary-items/ohbFrIxS5WF1abVb.htm)|Dagger|Daga|modificada|
|[oHxfnh9KIO2dQcTN.htm](kingmaker-bestiary-items/oHxfnh9KIO2dQcTN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[OiCu7wtLBEPD8y4D.htm](kingmaker-bestiary-items/OiCu7wtLBEPD8y4D.htm)|Spear|Lanza|modificada|
|[oka4i9kQ0yTjUsVF.htm](kingmaker-bestiary-items/oka4i9kQ0yTjUsVF.htm)|Trident|Trident|modificada|
|[OKH1R1FoBSEm3oKP.htm](kingmaker-bestiary-items/OKH1R1FoBSEm3oKP.htm)|Enraged Growth|Crecimiento Enfurecido|modificada|
|[oKuYToU9Nv9ALIIv.htm](kingmaker-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[okVVkEEDYOrrlhfS.htm](kingmaker-bestiary-items/okVVkEEDYOrrlhfS.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[OlnWMyIX3qedGUTu.htm](kingmaker-bestiary-items/OlnWMyIX3qedGUTu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[on7BQSeRUgbsFSSE.htm](kingmaker-bestiary-items/on7BQSeRUgbsFSSE.htm)|Knockdown|Derribo|modificada|
|[ONo2JspAa1GnoAlR.htm](kingmaker-bestiary-items/ONo2JspAa1GnoAlR.htm)|Pierce Armor|Perforar armadura|modificada|
|[onRMNTdGzo2Qq4rE.htm](kingmaker-bestiary-items/onRMNTdGzo2Qq4rE.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[oORyGKlMVkIFWGHJ.htm](kingmaker-bestiary-items/oORyGKlMVkIFWGHJ.htm)|Greatsword|Greatsword|modificada|
|[OpUEqqVcx7trG4mu.htm](kingmaker-bestiary-items/OpUEqqVcx7trG4mu.htm)|Kukri|Kukri|modificada|
|[OqVIaOEX3mbFTW9O.htm](kingmaker-bestiary-items/OqVIaOEX3mbFTW9O.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[OSCP7JLtSYLDVYrO.htm](kingmaker-bestiary-items/OSCP7JLtSYLDVYrO.htm)|Jaws|Fauces|modificada|
|[oSq2qYAln4B4sFY0.htm](kingmaker-bestiary-items/oSq2qYAln4B4sFY0.htm)|Branch|Rama|modificada|
|[OtqpSI4VXEPSxQox.htm](kingmaker-bestiary-items/OtqpSI4VXEPSxQox.htm)|Intimidating Prowess|Poderío intimidante|modificada|
|[otVXldOceOZtEV2x.htm](kingmaker-bestiary-items/otVXldOceOZtEV2x.htm)|Frightening Rattle|Frightening Rattle|modificada|
|[oVDdlUuHM54d6gAP.htm](kingmaker-bestiary-items/oVDdlUuHM54d6gAP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[OvgHtZCLvGtWHyg9.htm](kingmaker-bestiary-items/OvgHtZCLvGtWHyg9.htm)|Raging Resistance|Resistencia furiosa|modificada|
|[OvqcWkhFk95fFEEX.htm](kingmaker-bestiary-items/OvqcWkhFk95fFEEX.htm)|Shoulder Spikes|Púas de hombro|modificada|
|[owZSVQs8IkCQjNLE.htm](kingmaker-bestiary-items/owZSVQs8IkCQjNLE.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[oXZ9gBxLTC4PuQec.htm](kingmaker-bestiary-items/oXZ9gBxLTC4PuQec.htm)|Horns|Cuernos|modificada|
|[OYTKATaPDdautdNt.htm](kingmaker-bestiary-items/OYTKATaPDdautdNt.htm)|Wing|Ala|modificada|
|[OZRGnwXPUQq6AsKe.htm](kingmaker-bestiary-items/OZRGnwXPUQq6AsKe.htm)|Force Bolt|Rayo de fuerza|modificada|
|[P0EyFCnT4og9udgU.htm](kingmaker-bestiary-items/P0EyFCnT4og9udgU.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[P143e4Kl2GktAt4b.htm](kingmaker-bestiary-items/P143e4Kl2GktAt4b.htm)|Greatsword Critical Specialization|Greatsword Critical Specialization|modificada|
|[p3LP4p7cOQkgP2pl.htm](kingmaker-bestiary-items/p3LP4p7cOQkgP2pl.htm)|Befuddle|Befuddle|modificada|
|[p4gHmixTKN3Gb87J.htm](kingmaker-bestiary-items/p4gHmixTKN3Gb87J.htm)|Dragon Jaws|Fauces de Dragón|modificada|
|[P5jo7Pf9aXvKHvel.htm](kingmaker-bestiary-items/P5jo7Pf9aXvKHvel.htm)|Unseen Sight|Unseen Sight|modificada|
|[P8P9UmFHbSmMnTnd.htm](kingmaker-bestiary-items/P8P9UmFHbSmMnTnd.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[p9eUNDT8mPVTSS1D.htm](kingmaker-bestiary-items/p9eUNDT8mPVTSS1D.htm)|Dimensional Wormhole|Agujero de gusano dimensional|modificada|
|[PAN3R11XOGsaiIbK.htm](kingmaker-bestiary-items/PAN3R11XOGsaiIbK.htm)|Claw|Garra|modificada|
|[paPaJBwymUtXLyYF.htm](kingmaker-bestiary-items/paPaJBwymUtXLyYF.htm)|Whip|Látigo|modificada|
|[PBGOOwAe8HedCuGy.htm](kingmaker-bestiary-items/PBGOOwAe8HedCuGy.htm)|Longsword|Longsword|modificada|
|[pc3Nk8nioNXX1wuM.htm](kingmaker-bestiary-items/pc3Nk8nioNXX1wuM.htm)|Flash of Insight|Destello de perspicacia|modificada|
|[peHpZeJ7VwkuSurw.htm](kingmaker-bestiary-items/peHpZeJ7VwkuSurw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pEpgPRttHusG3elF.htm](kingmaker-bestiary-items/pEpgPRttHusG3elF.htm)|Three-Headed Strike|Golpe tricéfalo|modificada|
|[PF6tfwJNh5sVOavs.htm](kingmaker-bestiary-items/PF6tfwJNh5sVOavs.htm)|Thrashing Retreat|Retirada a golpes|modificada|
|[PfK4YgueBXw7cjqo.htm](kingmaker-bestiary-items/PfK4YgueBXw7cjqo.htm)|Vine Lash|Vine Lash|modificada|
|[Pfn9Ff6lgtXDae3e.htm](kingmaker-bestiary-items/Pfn9Ff6lgtXDae3e.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[pFruogrBBB4pvImp.htm](kingmaker-bestiary-items/pFruogrBBB4pvImp.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[pg54945zKnAqECAM.htm](kingmaker-bestiary-items/pg54945zKnAqECAM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[PG62mZgFRnBReYcw.htm](kingmaker-bestiary-items/PG62mZgFRnBReYcw.htm)|Word of Recall|Palabra de regreso|modificada|
|[PhsmxJZ4aqEowjOn.htm](kingmaker-bestiary-items/PhsmxJZ4aqEowjOn.htm)|Claws|Garras|modificada|
|[PjBdBCA31o7FcQvI.htm](kingmaker-bestiary-items/PjBdBCA31o7FcQvI.htm)|Telepathy|Telepatía|modificada|
|[pke6bvuCCSuidMVb.htm](kingmaker-bestiary-items/pke6bvuCCSuidMVb.htm)|Poisonous Touch|Toque venenoso|modificada|
|[pllCGrR7E0eAP35u.htm](kingmaker-bestiary-items/pllCGrR7E0eAP35u.htm)|Dagger|Daga|modificada|
|[PmcZB53eH1gUzaBq.htm](kingmaker-bestiary-items/PmcZB53eH1gUzaBq.htm)|Water-Bound|Ligado al agua|modificada|
|[pMKjMXZobeZ6zFPm.htm](kingmaker-bestiary-items/pMKjMXZobeZ6zFPm.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[po8aw9EkLZ78IlVy.htm](kingmaker-bestiary-items/po8aw9EkLZ78IlVy.htm)|Hatchet|Hacha|modificada|
|[pPppryZClUfw47fK.htm](kingmaker-bestiary-items/pPppryZClUfw47fK.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[PQMwXaG8Zqcd02Oe.htm](kingmaker-bestiary-items/PQMwXaG8Zqcd02Oe.htm)|Triple Opportunity|Oportunista triple|modificada|
|[pQowCb4EVsOhUgK2.htm](kingmaker-bestiary-items/pQowCb4EVsOhUgK2.htm)|Breath Weapon|Breath Weapon|modificada|
|[PqqO1TraA4ak7FIy.htm](kingmaker-bestiary-items/PqqO1TraA4ak7FIy.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[pQr1ecKdOpSlfSnw.htm](kingmaker-bestiary-items/pQr1ecKdOpSlfSnw.htm)|Spiteful Command|Orden imperiosa|modificada|
|[pQrymwjXMI3vqcOk.htm](kingmaker-bestiary-items/pQrymwjXMI3vqcOk.htm)|Negative Healing|Curación negativa|modificada|
|[pR9Yyd3sYM5iPspf.htm](kingmaker-bestiary-items/pR9Yyd3sYM5iPspf.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[pRRtkR99srCWUO1j.htm](kingmaker-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|Masa Corrosiva|modificada|
|[pSKjTPdO8e9KO9LI.htm](kingmaker-bestiary-items/pSKjTPdO8e9KO9LI.htm)|Multiple Opportunities|Oportunidades múltiple|modificada|
|[psRycYQDP439XLGm.htm](kingmaker-bestiary-items/psRycYQDP439XLGm.htm)|Wavesense (Imprecise) 30 feet|Sentir ondulaciones (Impreciso) 30 pies|modificada|
|[Ptn3Zb1Syk7YdL4y.htm](kingmaker-bestiary-items/Ptn3Zb1Syk7YdL4y.htm)|Claw|Garra|modificada|
|[PtwaILORaUXLPA4E.htm](kingmaker-bestiary-items/PtwaILORaUXLPA4E.htm)|+2 Status to All Saves vs. Cold|+2 situación a todas las salvaciones contra el Frío.|modificada|
|[pu2uW299sl7fVAeT.htm](kingmaker-bestiary-items/pu2uW299sl7fVAeT.htm)|Hoof|Hoof|modificada|
|[pu63PF0pspvou1kE.htm](kingmaker-bestiary-items/pu63PF0pspvou1kE.htm)|Whiplashing Tail|Whiplashing Tail|modificada|
|[PueAKB1MJBXmbhHV.htm](kingmaker-bestiary-items/PueAKB1MJBXmbhHV.htm)|Formation Fighter|Formación de Luchador|modificada|
|[pugi4AZBmL8aH1oF.htm](kingmaker-bestiary-items/pugi4AZBmL8aH1oF.htm)|Vengeful Sting|Aguijón vengativo|modificada|
|[pWFsPT5b6xUAwvpD.htm](kingmaker-bestiary-items/pWFsPT5b6xUAwvpD.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[PxCKKRzmpOzQRHlN.htm](kingmaker-bestiary-items/PxCKKRzmpOzQRHlN.htm)|Tiger Empathy|Empatía con los tigres|modificada|
|[pyyskEw6HoHX0DBp.htm](kingmaker-bestiary-items/pyyskEw6HoHX0DBp.htm)|Site Bound|Ligado a una ubicación|modificada|
|[PzkoaPhLG2JLClMM.htm](kingmaker-bestiary-items/PzkoaPhLG2JLClMM.htm)|Longsword|Longsword|modificada|
|[pzRCNxN7IHDbV6Wv.htm](kingmaker-bestiary-items/pzRCNxN7IHDbV6Wv.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[Q0VRtsqm6etoZxCa.htm](kingmaker-bestiary-items/Q0VRtsqm6etoZxCa.htm)|Shadow Doubles|Sombra Dobles|modificada|
|[Q0WHpEJkqbeT0KZr.htm](kingmaker-bestiary-items/Q0WHpEJkqbeT0KZr.htm)|Waves of Fear|Olas de Miedo|modificada|
|[Q0zvzLaylr0AYBGR.htm](kingmaker-bestiary-items/Q0zvzLaylr0AYBGR.htm)|Attack of Opportunity (Jaws Only)|Ataque de oportunidad (sólo fauces)|modificada|
|[q1z16jklLBVTY0A8.htm](kingmaker-bestiary-items/q1z16jklLBVTY0A8.htm)|Glaive|Glaive|modificada|
|[q5MeF25mWxrHIqg3.htm](kingmaker-bestiary-items/q5MeF25mWxrHIqg3.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[q6QpDgIvY1kfKSWc.htm](kingmaker-bestiary-items/q6QpDgIvY1kfKSWc.htm)|Dagger|Daga|modificada|
|[q6ZsHR2exUj0eCp7.htm](kingmaker-bestiary-items/q6ZsHR2exUj0eCp7.htm)|Aldori Dueling Sword|Aldori Dueling Sword|modificada|
|[Q78tMeRUfO7vehcO.htm](kingmaker-bestiary-items/Q78tMeRUfO7vehcO.htm)|Regeneration 45 (Deactivated by Fire or Negative)|Regeneración 45 (Desactivado por Fuego o Negativo)|modificada|
|[Q7g8pfqUSNczyqNo.htm](kingmaker-bestiary-items/Q7g8pfqUSNczyqNo.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[q7jtffxL9IJqb4Pq.htm](kingmaker-bestiary-items/q7jtffxL9IJqb4Pq.htm)|Change Shape|Change Shape|modificada|
|[Q8CDVoVPdXpwkoEA.htm](kingmaker-bestiary-items/Q8CDVoVPdXpwkoEA.htm)|Constant Spells|Constant Spells|modificada|
|[qAjzrkRNotMQvgjw.htm](kingmaker-bestiary-items/qAjzrkRNotMQvgjw.htm)|Infuse Arrow|Infuse Arrow|modificada|
|[QaqXp54jr3aKQjWN.htm](kingmaker-bestiary-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|Curación negativa|modificada|
|[QB62e4FfQvymENul.htm](kingmaker-bestiary-items/QB62e4FfQvymENul.htm)|Tail|Tail|modificada|
|[qCmKPruRObzVL1Dr.htm](kingmaker-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|Avance chirriante|modificada|
|[QDw6f81ZeMuIueQP.htm](kingmaker-bestiary-items/QDw6f81ZeMuIueQP.htm)|Tail|Tail|modificada|
|[QebQalR7fIq41wKr.htm](kingmaker-bestiary-items/QebQalR7fIq41wKr.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[qEenLpsXQfSrqeJh.htm](kingmaker-bestiary-items/qEenLpsXQfSrqeJh.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[qeZfMzOwr8GL4Lbh.htm](kingmaker-bestiary-items/qeZfMzOwr8GL4Lbh.htm)|Jaws|Fauces|modificada|
|[Qg6K3SNkS9IdmxUm.htm](kingmaker-bestiary-items/Qg6K3SNkS9IdmxUm.htm)|Blessed Life|Vida bendecida|modificada|
|[qgyml1ToPqFdJIKa.htm](kingmaker-bestiary-items/qgyml1ToPqFdJIKa.htm)|Drag Below|Arrastrar Abajo|modificada|
|[qhgcqwFp1hE2jFbu.htm](kingmaker-bestiary-items/qhgcqwFp1hE2jFbu.htm)|Claw|Garra|modificada|
|[qhS5ZEJcqNFytqWd.htm](kingmaker-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|Fauces|modificada|
|[qiU4iKX6FIHuopg7.htm](kingmaker-bestiary-items/qiU4iKX6FIHuopg7.htm)|Sting|Sting|modificada|
|[QIzXNCE3GPZBj5sy.htm](kingmaker-bestiary-items/QIzXNCE3GPZBj5sy.htm)|Shortsword|Espada corta|modificada|
|[QLqI8GHyMhlnlL3h.htm](kingmaker-bestiary-items/QLqI8GHyMhlnlL3h.htm)|Rock|Roca|modificada|
|[qn7XE1djceSKfkuH.htm](kingmaker-bestiary-items/qn7XE1djceSKfkuH.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[QNLWcv6w045fE1oE.htm](kingmaker-bestiary-items/QNLWcv6w045fE1oE.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qOhxTyF0N56kRCTR.htm](kingmaker-bestiary-items/qOhxTyF0N56kRCTR.htm)|All-Around Vision|All-Around Vision|modificada|
|[qpAN2omg7HvCwOzk.htm](kingmaker-bestiary-items/qpAN2omg7HvCwOzk.htm)|Undetectable|No detectado/a|modificada|
|[QQaSJEmqMeqq6kd9.htm](kingmaker-bestiary-items/QQaSJEmqMeqq6kd9.htm)|Lurker Claw|Lurker Claw|modificada|
|[qQD7sRQC5iCPloAd.htm](kingmaker-bestiary-items/qQD7sRQC5iCPloAd.htm)|Ectoplasmic Maneuver|Ectoplasmic Maneuver|modificada|
|[qqqYIHulqpSQmjAc.htm](kingmaker-bestiary-items/qqqYIHulqpSQmjAc.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[qQY3snkHHYYEQDxW.htm](kingmaker-bestiary-items/qQY3snkHHYYEQDxW.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[QrCROiN8OwsCLDUj.htm](kingmaker-bestiary-items/QrCROiN8OwsCLDUj.htm)|Skirmish Strike|Golpe hostigador|modificada|
|[QryAkNRvJCn0uaSN.htm](kingmaker-bestiary-items/QryAkNRvJCn0uaSN.htm)|Dagger|Daga|modificada|
|[QsQBz0EKCXO9IzXT.htm](kingmaker-bestiary-items/QsQBz0EKCXO9IzXT.htm)|Rapier|Estoque|modificada|
|[QtiX9tEriHb92lTU.htm](kingmaker-bestiary-items/QtiX9tEriHb92lTU.htm)|Pounce|Abalanzarse|modificada|
|[qTKs06NZl2BkZiYP.htm](kingmaker-bestiary-items/qTKs06NZl2BkZiYP.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[QTnEqxIt2MaL0WlZ.htm](kingmaker-bestiary-items/QTnEqxIt2MaL0WlZ.htm)|Spirit Naga Venom|Spirit Naga Venom|modificada|
|[qtXj0TB36n30L7my.htm](kingmaker-bestiary-items/qtXj0TB36n30L7my.htm)|Disarm|Desarmar|modificada|
|[qVOjbIdihxei6lTm.htm](kingmaker-bestiary-items/qVOjbIdihxei6lTm.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[QXYemYqhU6zdoBsz.htm](kingmaker-bestiary-items/QXYemYqhU6zdoBsz.htm)|Reach Spell|Conjuro de alcance|modificada|
|[QXYKDwpj06Pd7sEo.htm](kingmaker-bestiary-items/QXYKDwpj06Pd7sEo.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[QXyRODDmF4tozV5P.htm](kingmaker-bestiary-items/QXyRODDmF4tozV5P.htm)|Longsword|Longsword|modificada|
|[qYTvq8RiCpkT6wG7.htm](kingmaker-bestiary-items/qYTvq8RiCpkT6wG7.htm)|Dagger|Daga|modificada|
|[qztKctN0HIhpmOSy.htm](kingmaker-bestiary-items/qztKctN0HIhpmOSy.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[r0B1Lotd2T85M2u2.htm](kingmaker-bestiary-items/r0B1Lotd2T85M2u2.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[R2ZrKwkuQb3XIG4z.htm](kingmaker-bestiary-items/R2ZrKwkuQb3XIG4z.htm)|Dagger|Daga|modificada|
|[R3HSbrKclMuhH014.htm](kingmaker-bestiary-items/R3HSbrKclMuhH014.htm)|Planar Acclimation|Planar Acclimation|modificada|
|[r5yruYZzaDx9MoHR.htm](kingmaker-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[R6qqK2TgPEbgEcH0.htm](kingmaker-bestiary-items/R6qqK2TgPEbgEcH0.htm)|Defensive Burst|Ráfaga defensiva|modificada|
|[R8f6nx9jMnFB03qP.htm](kingmaker-bestiary-items/R8f6nx9jMnFB03qP.htm)|Savage|Salvajismo|modificada|
|[Ral7Zi8ooe0f3emu.htm](kingmaker-bestiary-items/Ral7Zi8ooe0f3emu.htm)|Moon Frenzy|Frenesí lunar|modificada|
|[rbZIdY30oD5udoGi.htm](kingmaker-bestiary-items/rbZIdY30oD5udoGi.htm)|Occult Focus Spells|Conjuros de foco oculto|modificada|
|[rcNrrQtcdNppDhtd.htm](kingmaker-bestiary-items/rcNrrQtcdNppDhtd.htm)|Feasting Bite|Muerdemuerde|modificada|
|[rdf4aOsP7jywGEIC.htm](kingmaker-bestiary-items/rdf4aOsP7jywGEIC.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[RflctXAVkddCvSeq.htm](kingmaker-bestiary-items/RflctXAVkddCvSeq.htm)|Jaws|Fauces|modificada|
|[rGbtqEmUFQBjGvOd.htm](kingmaker-bestiary-items/rGbtqEmUFQBjGvOd.htm)|Hungersense (Imprecise) 30 feet|Sentir el hambre (Impreciso) 30 pies|modificada|
|[RiC8zTuKfW44G8Mp.htm](kingmaker-bestiary-items/RiC8zTuKfW44G8Mp.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[RjfMaoY3Fp7Ouab8.htm](kingmaker-bestiary-items/RjfMaoY3Fp7Ouab8.htm)|Tremorsense|Tremorsense|modificada|
|[rk9Qa3TRqh7vnR4r.htm](kingmaker-bestiary-items/rk9Qa3TRqh7vnR4r.htm)|Deep Breath|Contener la respiración profunda|modificada|
|[rkdBPs8QgQ9Tfvpj.htm](kingmaker-bestiary-items/rkdBPs8QgQ9Tfvpj.htm)|Ovinrbaane|Ovinrbaane|modificada|
|[rl3BJy5yXHIijmZz.htm](kingmaker-bestiary-items/rl3BJy5yXHIijmZz.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RLutvx6GldUXGhjl.htm](kingmaker-bestiary-items/RLutvx6GldUXGhjl.htm)|Twin Parry|Parada gemela|modificada|
|[rlwsvNyOSTobwhA9.htm](kingmaker-bestiary-items/rlwsvNyOSTobwhA9.htm)|Centipede Swarm Venom|Enjambre Ciempiés Veneno|modificada|
|[rNPNkpT1CEzgR8Ry.htm](kingmaker-bestiary-items/rNPNkpT1CEzgR8Ry.htm)|Unnerving Prowess|Proeza enervante|modificada|
|[rnQSJr976QIURT9w.htm](kingmaker-bestiary-items/rnQSJr976QIURT9w.htm)|Songstrike|Songstrike|modificada|
|[rog22HpLDkUnfolA.htm](kingmaker-bestiary-items/rog22HpLDkUnfolA.htm)|Goat Horns|Cuernos de cabra|modificada|
|[roURyHTV2DxU7WTQ.htm](kingmaker-bestiary-items/roURyHTV2DxU7WTQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RrDmY99gWZJtFtjo.htm](kingmaker-bestiary-items/RrDmY99gWZJtFtjo.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[rTLmI9nr5Sji2tZD.htm](kingmaker-bestiary-items/rTLmI9nr5Sji2tZD.htm)|Axe Critical Specialization|Hacha Especialización Crítica|modificada|
|[rtPLskLwq9ldQzDC.htm](kingmaker-bestiary-items/rtPLskLwq9ldQzDC.htm)|Scalding Burst|Scalding Burst|modificada|
|[RUcX4XrsDpkjIiPy.htm](kingmaker-bestiary-items/RUcX4XrsDpkjIiPy.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[rwJegRqmKknX4v9P.htm](kingmaker-bestiary-items/rwJegRqmKknX4v9P.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[rWVM81zPBmywUXvF.htm](kingmaker-bestiary-items/rWVM81zPBmywUXvF.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[RXMeyIF1blCFYaFi.htm](kingmaker-bestiary-items/RXMeyIF1blCFYaFi.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rZftbooG5W57BA0g.htm](kingmaker-bestiary-items/rZftbooG5W57BA0g.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[S2bjckIBFgZGuliH.htm](kingmaker-bestiary-items/S2bjckIBFgZGuliH.htm)|Ogre Hook|Garfio de Ogro|modificada|
|[s2robi6KwZM7XeVw.htm](kingmaker-bestiary-items/s2robi6KwZM7XeVw.htm)|Falling Portcullis|Falling Portcullis|modificada|
|[s5DYWIxSytUTy1x2.htm](kingmaker-bestiary-items/s5DYWIxSytUTy1x2.htm)|Fangs|Colmillos|modificada|
|[S6FJYbxVlzTTwn0T.htm](kingmaker-bestiary-items/S6FJYbxVlzTTwn0T.htm)|Smoke Vision|Visión de Humo|modificada|
|[S6sguxBDLkRhBNAS.htm](kingmaker-bestiary-items/S6sguxBDLkRhBNAS.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[S7QInhdroOaf7k84.htm](kingmaker-bestiary-items/S7QInhdroOaf7k84.htm)|Tremorsense (Precise in Area A7, Otherwise Imprecise) 60 feet|Sentido del Temblor (Precisión en el ea A7, Impreciso en caso contrario) 60 pies|modificada|
|[S9OYItI2DPSsb4Za.htm](kingmaker-bestiary-items/S9OYItI2DPSsb4Za.htm)|Switch Fables|Switch Fables|modificada|
|[sA9IvnRiBKdOegEF.htm](kingmaker-bestiary-items/sA9IvnRiBKdOegEF.htm)|Wing|Ala|modificada|
|[SaMUZistsZa0Bskd.htm](kingmaker-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|All-Around Vision|modificada|
|[SaS7qHeGtLYWa1rX.htm](kingmaker-bestiary-items/SaS7qHeGtLYWa1rX.htm)|Tail Claw|Garra de Cola|modificada|
|[SbBp90Oz1LZfllYh.htm](kingmaker-bestiary-items/SbBp90Oz1LZfllYh.htm)|Spear|Lanza|modificada|
|[sCOLNwAp0vM1QRy9.htm](kingmaker-bestiary-items/sCOLNwAp0vM1QRy9.htm)|Baleful Gaze|Mirada torva|modificada|
|[Sd3o6E29DT50HqQ5.htm](kingmaker-bestiary-items/Sd3o6E29DT50HqQ5.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[sED3umw26WGHjqa6.htm](kingmaker-bestiary-items/sED3umw26WGHjqa6.htm)|Consume Memories|Consumir recuerdos|modificada|
|[SeJXe222cuooTJUx.htm](kingmaker-bestiary-items/SeJXe222cuooTJUx.htm)|Split|Split|modificada|
|[sfWdhP3dRBdtmiol.htm](kingmaker-bestiary-items/sfWdhP3dRBdtmiol.htm)|Blood Drain|Drenar sangre|modificada|
|[sHdlA1TMLwvXBRX0.htm](kingmaker-bestiary-items/sHdlA1TMLwvXBRX0.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[sHRh4SLqM1rBlpHb.htm](kingmaker-bestiary-items/sHRh4SLqM1rBlpHb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[sIdAMHJe8g1PQUF7.htm](kingmaker-bestiary-items/sIdAMHJe8g1PQUF7.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[SKOscQwReKF5ZqAY.htm](kingmaker-bestiary-items/SKOscQwReKF5ZqAY.htm)|Maming Chop|Maming Chop|modificada|
|[smt8XqXd55pl6Gwi.htm](kingmaker-bestiary-items/smt8XqXd55pl6Gwi.htm)|Clamp Shut|Cerrar pinza|modificada|
|[Sn0vYKbSWgQmv5g7.htm](kingmaker-bestiary-items/Sn0vYKbSWgQmv5g7.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[snOrUVs5f8v8gtuP.htm](kingmaker-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[sNUoqbThXZbfj5S7.htm](kingmaker-bestiary-items/sNUoqbThXZbfj5S7.htm)|Fade from View|Esfumarse a plena vista|modificada|
|[snWh3FVKpgmQAHQz.htm](kingmaker-bestiary-items/snWh3FVKpgmQAHQz.htm)|Disfigure|Desfigurar|modificada|
|[spF1Kzai1HQbME5t.htm](kingmaker-bestiary-items/spF1Kzai1HQbME5t.htm)|Bloom Regeneration|Regeneración Bloom|modificada|
|[SPiKrNADm6u2qbhl.htm](kingmaker-bestiary-items/SPiKrNADm6u2qbhl.htm)|Triumphant Roar|Rugido Triunfante|modificada|
|[SPkIUm3665kXInNU.htm](kingmaker-bestiary-items/SPkIUm3665kXInNU.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[SPTwJf1wzeADk4sn.htm](kingmaker-bestiary-items/SPTwJf1wzeADk4sn.htm)|Stinger|Aguijón|modificada|
|[SpVGiNHO8YKLWtpn.htm](kingmaker-bestiary-items/SpVGiNHO8YKLWtpn.htm)|Quick Alchemy|Alquimia rápida|modificada|
|[SQ4DUNap1xbzs5Sq.htm](kingmaker-bestiary-items/SQ4DUNap1xbzs5Sq.htm)|Jaws|Fauces|modificada|
|[sqceAJWlpdKGnHvx.htm](kingmaker-bestiary-items/sqceAJWlpdKGnHvx.htm)|Beak|Beak|modificada|
|[SqomM8yPUdX1yPY8.htm](kingmaker-bestiary-items/SqomM8yPUdX1yPY8.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[SRv8Gjga6DHY4U8G.htm](kingmaker-bestiary-items/SRv8Gjga6DHY4U8G.htm)|Corrupt Water|Corromper agua|modificada|
|[ssgy6SBt7L8uYlUi.htm](kingmaker-bestiary-items/ssgy6SBt7L8uYlUi.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[sT3ZX2Zurexp4hLH.htm](kingmaker-bestiary-items/sT3ZX2Zurexp4hLH.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[sT4lUsg0seVtpUKQ.htm](kingmaker-bestiary-items/sT4lUsg0seVtpUKQ.htm)|Claw|Garra|modificada|
|[sU4JRJE89RWZ5Z6a.htm](kingmaker-bestiary-items/sU4JRJE89RWZ5Z6a.htm)|Scent (Precise) 120 feet|Olor (Precisión) 120 pies.|modificada|
|[sw2mWVFaQb93J9el.htm](kingmaker-bestiary-items/sw2mWVFaQb93J9el.htm)|Forager|Forrajeador|modificada|
|[swHSxsAoATAgMMX2.htm](kingmaker-bestiary-items/swHSxsAoATAgMMX2.htm)|Ride Tick|Montar Garrapata|modificada|
|[Swn94Gh9zgVIcHmO.htm](kingmaker-bestiary-items/Swn94Gh9zgVIcHmO.htm)|Ankle Bite|Muerdemuerde el Tobillo|modificada|
|[SYMyLr9FwZW5kVcr.htm](kingmaker-bestiary-items/SYMyLr9FwZW5kVcr.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[SZOur85Nka45xVe7.htm](kingmaker-bestiary-items/SZOur85Nka45xVe7.htm)|Frightful Moan|Frightful Moan|modificada|
|[T0IRXqqR5Zr2FXsp.htm](kingmaker-bestiary-items/T0IRXqqR5Zr2FXsp.htm)|Negative Healing|Curación negativa|modificada|
|[T0KOWsF7QwJ1oDQq.htm](kingmaker-bestiary-items/T0KOWsF7QwJ1oDQq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[T1dMCJU34dJYVgZw.htm](kingmaker-bestiary-items/T1dMCJU34dJYVgZw.htm)|Club|Club|modificada|
|[T1JhJ4gf00b500cI.htm](kingmaker-bestiary-items/T1JhJ4gf00b500cI.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[T1WSPSyMbIh6gzFQ.htm](kingmaker-bestiary-items/T1WSPSyMbIh6gzFQ.htm)|Catch Rock|Atrapar roca|modificada|
|[T3OviChVRRywo6sB.htm](kingmaker-bestiary-items/T3OviChVRRywo6sB.htm)|Mocking Laughter|Risa burlona|modificada|
|[t49UcMd23mTLwrj5.htm](kingmaker-bestiary-items/t49UcMd23mTLwrj5.htm)|Paralyzing Touch|Toque paralizante|modificada|
|[t4BxYpWMm3lEqjxG.htm](kingmaker-bestiary-items/t4BxYpWMm3lEqjxG.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[T5dyuJhTrUcC9fqv.htm](kingmaker-bestiary-items/T5dyuJhTrUcC9fqv.htm)|Claw|Garra|modificada|
|[t5Pe2D0CJm0NY3E4.htm](kingmaker-bestiary-items/t5Pe2D0CJm0NY3E4.htm)|Claw|Garra|modificada|
|[t6KHJCiZXdNJnqxB.htm](kingmaker-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|Maestro de armas|modificada|
|[T9wwE2uJLrI8NxyM.htm](kingmaker-bestiary-items/T9wwE2uJLrI8NxyM.htm)|Bear Trap|Trampa para osos|modificada|
|[taxWERmxNkRViJLR.htm](kingmaker-bestiary-items/taxWERmxNkRViJLR.htm)|Greataxe|Greataxe|modificada|
|[TBlBPLZKvFBHjjgU.htm](kingmaker-bestiary-items/TBlBPLZKvFBHjjgU.htm)|Slow|Lentificado/a|modificada|
|[TCbRZkzWxt6vXhCx.htm](kingmaker-bestiary-items/TCbRZkzWxt6vXhCx.htm)|+2 Status to All Saves vs. Fear Effects|+2 de situación a todas las salvaciones contra efectos de miedo.|modificada|
|[tDlyF5IZNsYAYYhP.htm](kingmaker-bestiary-items/tDlyF5IZNsYAYYhP.htm)|Thrown Object|Objeto Arrojado|modificada|
|[tEPc3Xc3Pufff6D5.htm](kingmaker-bestiary-items/tEPc3Xc3Pufff6D5.htm)|Drench|Sofocar|modificada|
|[TfSlL9Vupc8WC8jy.htm](kingmaker-bestiary-items/TfSlL9Vupc8WC8jy.htm)|Buck|Encabritarse|modificada|
|[TGbnXBgCIKg1wFwT.htm](kingmaker-bestiary-items/TGbnXBgCIKg1wFwT.htm)|Targeting Shot|Tiro de apuntar|modificada|
|[thieXYs44gA9P3Uo.htm](kingmaker-bestiary-items/thieXYs44gA9P3Uo.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[TIGvNHFixJK8sJCg.htm](kingmaker-bestiary-items/TIGvNHFixJK8sJCg.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TILteUNBjDkjtfg6.htm](kingmaker-bestiary-items/TILteUNBjDkjtfg6.htm)|Inspiration|Inspiración|modificada|
|[TIWU41UBtl2ksfmx.htm](kingmaker-bestiary-items/TIWU41UBtl2ksfmx.htm)|Twin Riposte|Réplica gemela|modificada|
|[tJSXokhRWx6GoMJe.htm](kingmaker-bestiary-items/tJSXokhRWx6GoMJe.htm)|Bleed for Lamashtu|Sangrado para Lamashtu.|modificada|
|[tKFw0mI1NIfpLYBn.htm](kingmaker-bestiary-items/tKFw0mI1NIfpLYBn.htm)|Unbalancing Blow|Golpe desequilibrante|modificada|
|[TKjiZlqXKYAz5MTX.htm](kingmaker-bestiary-items/TKjiZlqXKYAz5MTX.htm)|All-Around Vision|All-Around Vision|modificada|
|[tkP4RVEwAR4aHYbk.htm](kingmaker-bestiary-items/tkP4RVEwAR4aHYbk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tOCS9mkYR5K1LuOZ.htm](kingmaker-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[tqOuI2ayN5VhcNyW.htm](kingmaker-bestiary-items/tqOuI2ayN5VhcNyW.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[TqvPH7FdjdwawFbW.htm](kingmaker-bestiary-items/TqvPH7FdjdwawFbW.htm)|Shield Block|Bloquear con escudo|modificada|
|[TR472pgOTxUJrN9B.htm](kingmaker-bestiary-items/TR472pgOTxUJrN9B.htm)|Club|Club|modificada|
|[ttXiy5Zys3PzCa8f.htm](kingmaker-bestiary-items/ttXiy5Zys3PzCa8f.htm)|Tortuous Touch|Toque Tortuoso|modificada|
|[TUJZ5ff9G3KgBsCN.htm](kingmaker-bestiary-items/TUJZ5ff9G3KgBsCN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TvcbacSr4PLH15CQ.htm](kingmaker-bestiary-items/TvcbacSr4PLH15CQ.htm)|Curse of the Werewolf|Maldición del Hombre Lobo|modificada|
|[tvFxmbJKPxnCnTbb.htm](kingmaker-bestiary-items/tvFxmbJKPxnCnTbb.htm)|Vermin Empathy|Empatía con las sabandijas|modificada|
|[tvr5xWTm58t3Dgy3.htm](kingmaker-bestiary-items/tvr5xWTm58t3Dgy3.htm)|Crumbling Edge|Desmoronarse Edge|modificada|
|[TwMRnjbKEUPZwBKx.htm](kingmaker-bestiary-items/TwMRnjbKEUPZwBKx.htm)|Divine Font|Fuente divina|modificada|
|[TwVHeJqYT1io5LxX.htm](kingmaker-bestiary-items/TwVHeJqYT1io5LxX.htm)|Summon Pack|Summon Pack|modificada|
|[TxCWNmfsEkdTzNvw.htm](kingmaker-bestiary-items/TxCWNmfsEkdTzNvw.htm)|Outside of Time|Fuera del Tiempo|modificada|
|[TxKSVVgJ5OZw3cy0.htm](kingmaker-bestiary-items/TxKSVVgJ5OZw3cy0.htm)|Outside of Time|Fuera del Tiempo|modificada|
|[TyHmjC5wvxo29J5L.htm](kingmaker-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|Agarrado|modificada|
|[tz6uqrNAY2ySZ2TY.htm](kingmaker-bestiary-items/tz6uqrNAY2ySZ2TY.htm)|Of Three Minds|Of Three Minds|modificada|
|[Tzi4lR0Oigo4U8Wu.htm](kingmaker-bestiary-items/Tzi4lR0Oigo4U8Wu.htm)|Emerald Beam|Rayo Esmeralda|modificada|
|[u0gmeeDEvRzdbvFs.htm](kingmaker-bestiary-items/u0gmeeDEvRzdbvFs.htm)|Negative Healing|Curación negativa|modificada|
|[U22LSFuhWJzxq7TO.htm](kingmaker-bestiary-items/U22LSFuhWJzxq7TO.htm)|Damsel Act|Damsel Act|modificada|
|[u6tuxFGclP3Cf3u2.htm](kingmaker-bestiary-items/u6tuxFGclP3Cf3u2.htm)|Otherworldly Mind|Mente de otro mundo|modificada|
|[u8YE1BntdMHM1ks0.htm](kingmaker-bestiary-items/u8YE1BntdMHM1ks0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[uAlQecVurdHtCKds.htm](kingmaker-bestiary-items/uAlQecVurdHtCKds.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[ubHtu9kCddCNuEjt.htm](kingmaker-bestiary-items/ubHtu9kCddCNuEjt.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[Ubytn72fPKxfwdab.htm](kingmaker-bestiary-items/Ubytn72fPKxfwdab.htm)|Dagger|Daga|modificada|
|[ucBBQrbesTH1rIdv.htm](kingmaker-bestiary-items/ucBBQrbesTH1rIdv.htm)|Light Hammer|Martillo ligero|modificada|
|[uCIO2gyqCCw5HMiZ.htm](kingmaker-bestiary-items/uCIO2gyqCCw5HMiZ.htm)|Quick Bomber|Bombardero rápido|modificada|
|[UcQu3REAJGDNFfE0.htm](kingmaker-bestiary-items/UcQu3REAJGDNFfE0.htm)|Captivating Pollen|Polen cautivador|modificada|
|[uD9MleR0PzjUXugC.htm](kingmaker-bestiary-items/uD9MleR0PzjUXugC.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[UdmjBD1i3DxTYEep.htm](kingmaker-bestiary-items/UdmjBD1i3DxTYEep.htm)|Telekinetic Assault|Asalto telecinético|modificada|
|[UGbMxXUr68ITj7ND.htm](kingmaker-bestiary-items/UGbMxXUr68ITj7ND.htm)|Drain Lintwerth|Drena a Lintwerth|modificada|
|[UiApvNrZOpUiRTze.htm](kingmaker-bestiary-items/UiApvNrZOpUiRTze.htm)|Centipede Mandibles|Centipede Mandibles|modificada|
|[UIGEr5LUogZmy8ln.htm](kingmaker-bestiary-items/UIGEr5LUogZmy8ln.htm)|Phantasmagoric Fog|Niebla Fantasmagórica|modificada|
|[uiZk5k4gZbLlAo44.htm](kingmaker-bestiary-items/uiZk5k4gZbLlAo44.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[UJnXwBbuFjAcC01q.htm](kingmaker-bestiary-items/UJnXwBbuFjAcC01q.htm)|Unsettling Attention|Atención Inquietante|modificada|
|[UkuRoGDf37QBLm8k.htm](kingmaker-bestiary-items/UkuRoGDf37QBLm8k.htm)|Ice Stride|Zancada de hielo|modificada|
|[ulr3xQ9vjVzN0JcV.htm](kingmaker-bestiary-items/ulr3xQ9vjVzN0JcV.htm)|Constant Spells|Constant Spells|modificada|
|[UlSgJk73FttvDVKx.htm](kingmaker-bestiary-items/UlSgJk73FttvDVKx.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[uMg5j8kbgsMRkJNO.htm](kingmaker-bestiary-items/uMg5j8kbgsMRkJNO.htm)|Invoke Old Sharptooth|Invoke Old Sharptooth|modificada|
|[uox8BdqT7SHwPAXF.htm](kingmaker-bestiary-items/uox8BdqT7SHwPAXF.htm)|Backlash|Backlash|modificada|
|[Up8YexckH0qYu7Mg.htm](kingmaker-bestiary-items/Up8YexckH0qYu7Mg.htm)|Hatchet|Hacha|modificada|
|[uQFiXxjHXOCxLHe8.htm](kingmaker-bestiary-items/uQFiXxjHXOCxLHe8.htm)|Vulnerability to Supernatural Darkness|Vulnerabilidad a la oscuridad sobrenatural.|modificada|
|[UqivmeGpdoq5TEFp.htm](kingmaker-bestiary-items/UqivmeGpdoq5TEFp.htm)|Manifest Fetch Weapon|Manifiesto Fetch Arma|modificada|
|[Us7BcRckPUy5WuaZ.htm](kingmaker-bestiary-items/Us7BcRckPUy5WuaZ.htm)|Swallow Whole|Engullir Todo|modificada|
|[USCoVo5LJNs12zB4.htm](kingmaker-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|Oscurecerse|modificada|
|[utrBYZarwPMS7ACv.htm](kingmaker-bestiary-items/utrBYZarwPMS7ACv.htm)|Resolve|Resolución|modificada|
|[UtXatkaqCOacbbnj.htm](kingmaker-bestiary-items/UtXatkaqCOacbbnj.htm)|Jaws|Fauces|modificada|
|[uuI4sId0T1tDVCla.htm](kingmaker-bestiary-items/uuI4sId0T1tDVCla.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uv3BAfwMvrlyZDI6.htm](kingmaker-bestiary-items/uv3BAfwMvrlyZDI6.htm)|Fist|Puño|modificada|
|[UVFdUW8UmQmQjvIs.htm](kingmaker-bestiary-items/UVFdUW8UmQmQjvIs.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[UW8qbPcbWR2N4HcI.htm](kingmaker-bestiary-items/UW8qbPcbWR2N4HcI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uYqNB5URDvfKjzHe.htm](kingmaker-bestiary-items/uYqNB5URDvfKjzHe.htm)|Morningstar|Morningstar|modificada|
|[v0ppjSET59hXq9Z5.htm](kingmaker-bestiary-items/v0ppjSET59hXq9Z5.htm)|Dagger|Daga|modificada|
|[v4dtDXWYnbH5R47w.htm](kingmaker-bestiary-items/v4dtDXWYnbH5R47w.htm)|Fearsome Blizzard|Ventisca Temible|modificada|
|[V4WvWjukxtroI8Xs.htm](kingmaker-bestiary-items/V4WvWjukxtroI8Xs.htm)|Swallow Whole|Engullir Todo|modificada|
|[v65HAbd7O3gZv2VP.htm](kingmaker-bestiary-items/v65HAbd7O3gZv2VP.htm)|Eerie Flexibility|Flexibilidad inquietante|modificada|
|[V6fjZPcQEMD98LxA.htm](kingmaker-bestiary-items/V6fjZPcQEMD98LxA.htm)|Dramatic Disarm|Dramatic Desarmar|modificada|
|[v76tHhlGlywyinPF.htm](kingmaker-bestiary-items/v76tHhlGlywyinPF.htm)|Shortbow|Arco corto|modificada|
|[V7siOMUXReEpRR0u.htm](kingmaker-bestiary-items/V7siOMUXReEpRR0u.htm)|Dagger|Daga|modificada|
|[v8fWAG80JbMt4nOy.htm](kingmaker-bestiary-items/v8fWAG80JbMt4nOy.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[V8J7D9aK5N4MpaBI.htm](kingmaker-bestiary-items/V8J7D9aK5N4MpaBI.htm)|Lifesense|Lifesense|modificada|
|[VBMoJbMPEmzIPu5X.htm](kingmaker-bestiary-items/VBMoJbMPEmzIPu5X.htm)|Chill Breath|Aliento de frío|modificada|
|[vCJLJPrm6qXxNRzd.htm](kingmaker-bestiary-items/vCJLJPrm6qXxNRzd.htm)|Divine Font (Harm)|Fuente divina (dar).|modificada|
|[VCxhoISThjwcy55r.htm](kingmaker-bestiary-items/VCxhoISThjwcy55r.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[Vd2tGIDTvBddHJ36.htm](kingmaker-bestiary-items/Vd2tGIDTvBddHJ36.htm)|Arcane Focus Spells|Conjuros de foco arcano|modificada|
|[VDa8nmlZXUFf1Y7z.htm](kingmaker-bestiary-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|Fauces|modificada|
|[VdKXa5UE8pjAkhRN.htm](kingmaker-bestiary-items/VdKXa5UE8pjAkhRN.htm)|Staff|Báculo|modificada|
|[VdlfnhKj8BudHVMB.htm](kingmaker-bestiary-items/VdlfnhKj8BudHVMB.htm)|Trickster's Step|Paso del embaucador|modificada|
|[VEZWJVB54nkdpnVP.htm](kingmaker-bestiary-items/VEZWJVB54nkdpnVP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[VGrJpWKaRTR27Qyu.htm](kingmaker-bestiary-items/VGrJpWKaRTR27Qyu.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[viDna1Oudpf22Ieg.htm](kingmaker-bestiary-items/viDna1Oudpf22Ieg.htm)|Blowgun|Blowgun|modificada|
|[vius8NahqzVZYRSE.htm](kingmaker-bestiary-items/vius8NahqzVZYRSE.htm)|Play the Pipes|Tocar la gaita|modificada|
|[vJiFJxTfxGmRSWe0.htm](kingmaker-bestiary-items/vJiFJxTfxGmRSWe0.htm)|Composite Shortbow|Arco corto Composición|modificada|
|[VKxXRGAQaygpB7SH.htm](kingmaker-bestiary-items/VKxXRGAQaygpB7SH.htm)|Jaws|Fauces|modificada|
|[vLKZN5X8t0pUwZJd.htm](kingmaker-bestiary-items/vLKZN5X8t0pUwZJd.htm)|Improved Knockdown|Derribo mejorado|modificada|
|[vllPNnBmoSwFEtl9.htm](kingmaker-bestiary-items/vllPNnBmoSwFEtl9.htm)|Guarded Movement|Movimiento protegido|modificada|
|[VmmlUbjFsbcN9dM8.htm](kingmaker-bestiary-items/VmmlUbjFsbcN9dM8.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[vMozJKWMEqEZvBOl.htm](kingmaker-bestiary-items/vMozJKWMEqEZvBOl.htm)|Jaws|Fauces|modificada|
|[VPg7zeVby8h8yTMX.htm](kingmaker-bestiary-items/VPg7zeVby8h8yTMX.htm)|Bark Orders|Órdenes de Ladrido|modificada|
|[VPpeBvTnuLzMsNon.htm](kingmaker-bestiary-items/VPpeBvTnuLzMsNon.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[VPVWe9JZVLpTixZC.htm](kingmaker-bestiary-items/VPVWe9JZVLpTixZC.htm)|Change Shape|Change Shape|modificada|
|[VQW9KJrLdz5lDjJ9.htm](kingmaker-bestiary-items/VQW9KJrLdz5lDjJ9.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[VQzmSFhPlPCnWRYb.htm](kingmaker-bestiary-items/VQzmSFhPlPCnWRYb.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[vRSrxRGekX69z1xM.htm](kingmaker-bestiary-items/vRSrxRGekX69z1xM.htm)|Greatsword|Greatsword|modificada|
|[vrZuMJAZcSYNFRp1.htm](kingmaker-bestiary-items/vrZuMJAZcSYNFRp1.htm)|Woodland Stride|Paso forestal|modificada|
|[VTjvgSGDhiMzsKdY.htm](kingmaker-bestiary-items/VTjvgSGDhiMzsKdY.htm)|Frenzied Fangs|Colmillos Frenéticos|modificada|
|[vTXaShzYmcNbRjzk.htm](kingmaker-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|Bloodcurdling Screech|modificada|
|[vudcfpUT9un2pDOP.htm](kingmaker-bestiary-items/vudcfpUT9un2pDOP.htm)|Gnaw|Gnaw|modificada|
|[VUyMebZxthD1vUj3.htm](kingmaker-bestiary-items/VUyMebZxthD1vUj3.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[vvB4kICd5X8HaFue.htm](kingmaker-bestiary-items/vvB4kICd5X8HaFue.htm)|Rage|Furia|modificada|
|[VViv6cOIpMtNUZzW.htm](kingmaker-bestiary-items/VViv6cOIpMtNUZzW.htm)|Brutal Gore|Brutal Gore|modificada|
|[vVt7KeVPbqT0jHZQ.htm](kingmaker-bestiary-items/vVt7KeVPbqT0jHZQ.htm)|Clutch|Embrague|modificada|
|[VWC3NNUYpN6qbf09.htm](kingmaker-bestiary-items/VWC3NNUYpN6qbf09.htm)|Breath Weapon|Breath Weapon|modificada|
|[vYdUPrjuB8HrzGBA.htm](kingmaker-bestiary-items/vYdUPrjuB8HrzGBA.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[VyJyNz5V3NR5Gylr.htm](kingmaker-bestiary-items/VyJyNz5V3NR5Gylr.htm)|Axe Vulnerability 20|Vulnerabilidad a las hachas 20|modificada|
|[vYngXSj2DDVpfWFe.htm](kingmaker-bestiary-items/vYngXSj2DDVpfWFe.htm)|Meddling Tail|Cola Entrometida|modificada|
|[vYo1emHFZe2lLLBo.htm](kingmaker-bestiary-items/vYo1emHFZe2lLLBo.htm)|Constant Spells|Constant Spells|modificada|
|[VyTDZWtEYeQGBrXk.htm](kingmaker-bestiary-items/VyTDZWtEYeQGBrXk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VzK5CD8g2ofWRIkk.htm](kingmaker-bestiary-items/VzK5CD8g2ofWRIkk.htm)|Improved Grab|Agarrado mejorado|modificada|
|[VzrK9L3fTzGmyHto.htm](kingmaker-bestiary-items/VzrK9L3fTzGmyHto.htm)|Druid Order Spells|Conjuros de orden druida|modificada|
|[w0XMZ6GmkzJcCRyO.htm](kingmaker-bestiary-items/w0XMZ6GmkzJcCRyO.htm)|Apocalyptic Shriek|Apocalyptic Shriek|modificada|
|[w4Cd5RsR9MjHTRDj.htm](kingmaker-bestiary-items/w4Cd5RsR9MjHTRDj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[w5FhuQgOnX0i92Y3.htm](kingmaker-bestiary-items/w5FhuQgOnX0i92Y3.htm)|Jaws|Fauces|modificada|
|[w5ySqTQIaFATIjDM.htm](kingmaker-bestiary-items/w5ySqTQIaFATIjDM.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[wAjwXDiTkvisDxZR.htm](kingmaker-bestiary-items/wAjwXDiTkvisDxZR.htm)|Survival of the Fittest|Supervivencia del más fuerte|modificada|
|[wb6prflG67LiVXh1.htm](kingmaker-bestiary-items/wb6prflG67LiVXh1.htm)|Trackless Step|Pisada sin rastro|modificada|
|[wbtt2m0nhK330JwC.htm](kingmaker-bestiary-items/wbtt2m0nhK330JwC.htm)|Hatchet|Hacha|modificada|
|[WBvBXluaSwwxLaJ4.htm](kingmaker-bestiary-items/WBvBXluaSwwxLaJ4.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[wChn2PEw9GYziq2n.htm](kingmaker-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|Glow|modificada|
|[WdOsTqGjiU4ZnX5H.htm](kingmaker-bestiary-items/WdOsTqGjiU4ZnX5H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WDpA7ff1gJiokx9N.htm](kingmaker-bestiary-items/WDpA7ff1gJiokx9N.htm)|Hunted Shot|Tiro cazado|modificada|
|[we4aKsadr3FYXQCP.htm](kingmaker-bestiary-items/we4aKsadr3FYXQCP.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[WeXirhh6gD0DCTGB.htm](kingmaker-bestiary-items/WeXirhh6gD0DCTGB.htm)|Root|Enraizarse|modificada|
|[Wez6v2oJoatkzzVp.htm](kingmaker-bestiary-items/Wez6v2oJoatkzzVp.htm)|Throw Rock|Arrojar roca|modificada|
|[WF3kud5WAVewvKL0.htm](kingmaker-bestiary-items/WF3kud5WAVewvKL0.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[wFieL2rsS0tFWUAR.htm](kingmaker-bestiary-items/wFieL2rsS0tFWUAR.htm)|Regeneration 20 (Deactivated by Cold Iron or Lawful)|Regeneración 20 (Desactivado por Hierro Frío o Lawful).|modificada|
|[WhCqHDOg0A8FK3IF.htm](kingmaker-bestiary-items/WhCqHDOg0A8FK3IF.htm)|Relentless Tracker|Relentless Tracker|modificada|
|[WIPFxMzyJ87R3t7k.htm](kingmaker-bestiary-items/WIPFxMzyJ87R3t7k.htm)|Supernatural Speed|Velocidad Sobrenatural|modificada|
|[WIPJ7Xl3dOhNuLlq.htm](kingmaker-bestiary-items/WIPJ7Xl3dOhNuLlq.htm)|Counterspell|Contraconjuro|modificada|
|[WJaww98OCHbzRIIP.htm](kingmaker-bestiary-items/WJaww98OCHbzRIIP.htm)|Jaws|Fauces|modificada|
|[WjBhdxKcekKdof75.htm](kingmaker-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Wjfmdyuddn5fCyVZ.htm](kingmaker-bestiary-items/Wjfmdyuddn5fCyVZ.htm)|Insult Challenges|Desafíos de insultos|modificada|
|[wkahGwEkL5eYhtZy.htm](kingmaker-bestiary-items/wkahGwEkL5eYhtZy.htm)|Tail|Tail|modificada|
|[WNANaLjOqPHdTPjN.htm](kingmaker-bestiary-items/WNANaLjOqPHdTPjN.htm)|Dagger|Daga|modificada|
|[wO3hSmCvgcCzSah3.htm](kingmaker-bestiary-items/wO3hSmCvgcCzSah3.htm)|Bloom Curse|Bloom Curse|modificada|
|[WOqo2UC99RfWFqA0.htm](kingmaker-bestiary-items/WOqo2UC99RfWFqA0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WpEtlM3mpICnsXPm.htm](kingmaker-bestiary-items/WpEtlM3mpICnsXPm.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[wrisTXltKN2ggF1B.htm](kingmaker-bestiary-items/wrisTXltKN2ggF1B.htm)|Immense Mandragora Venom|Inmenso Veneno Mandragora|modificada|
|[wRiugNyM38JRtkeI.htm](kingmaker-bestiary-items/wRiugNyM38JRtkeI.htm)|Coven Spells|Coven Spells|modificada|
|[WRLKbO1wNweUuW4n.htm](kingmaker-bestiary-items/WRLKbO1wNweUuW4n.htm)|Longspear|Longspear|modificada|
|[WrmnFHWCqCQMpQlB.htm](kingmaker-bestiary-items/WrmnFHWCqCQMpQlB.htm)|Pin to the Sky|Pin to the Sky|modificada|
|[wSYY3PhHktTGw8ua.htm](kingmaker-bestiary-items/wSYY3PhHktTGw8ua.htm)|Baroness's Scream|Grito de la Baronesa|modificada|
|[Wt9KDemV1jGIVjE5.htm](kingmaker-bestiary-items/Wt9KDemV1jGIVjE5.htm)|Catch Rock|Atrapar roca|modificada|
|[WTjCVJwNHH8vUKzE.htm](kingmaker-bestiary-items/WTjCVJwNHH8vUKzE.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[WU4cXLzhnoCczalO.htm](kingmaker-bestiary-items/WU4cXLzhnoCczalO.htm)|Irritating Dander|Caspa irritante|modificada|
|[wuin1zchdZrKVQEY.htm](kingmaker-bestiary-items/wuin1zchdZrKVQEY.htm)|Dagger|Daga|modificada|
|[WVChw6DFgtcfsrAf.htm](kingmaker-bestiary-items/WVChw6DFgtcfsrAf.htm)|Swarming Beaks|Swarming Beaks|modificada|
|[WvKgLpntjekJJHp7.htm](kingmaker-bestiary-items/WvKgLpntjekJJHp7.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[wXPOAgEeixFbagOW.htm](kingmaker-bestiary-items/wXPOAgEeixFbagOW.htm)|Swift Summon|Celeridad Invocar|modificada|
|[wyAVtZ4X5ErNqi3C.htm](kingmaker-bestiary-items/wyAVtZ4X5ErNqi3C.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[wYKABRKZFADU4eFV.htm](kingmaker-bestiary-items/wYKABRKZFADU4eFV.htm)|Fortune's Friend|Amigo de la Fortuna|modificada|
|[WyzkB1NHDFF0YsAm.htm](kingmaker-bestiary-items/WyzkB1NHDFF0YsAm.htm)|Improved Grab|Agarrado mejorado|modificada|
|[wzltAJwbP73KlolU.htm](kingmaker-bestiary-items/wzltAJwbP73KlolU.htm)|Itchy|Itchy|modificada|
|[x1BxQoUjfPHH06yh.htm](kingmaker-bestiary-items/x1BxQoUjfPHH06yh.htm)|Dagger|Daga|modificada|
|[X1qtaRb6bkYt1N3s.htm](kingmaker-bestiary-items/X1qtaRb6bkYt1N3s.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[X3C10c6qy59DlNdh.htm](kingmaker-bestiary-items/X3C10c6qy59DlNdh.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[X3HpUq80mQWyiHZ3.htm](kingmaker-bestiary-items/X3HpUq80mQWyiHZ3.htm)|Hunt Prey|Perseguir presa|modificada|
|[x3yCpybINerCQY3Q.htm](kingmaker-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|Toque Corrosivo|modificada|
|[X41cWt7idOHqasPx.htm](kingmaker-bestiary-items/X41cWt7idOHqasPx.htm)|Greatsword|Greatsword|modificada|
|[x46bcv7xjs0irbqk.htm](kingmaker-bestiary-items/x46bcv7xjs0irbqk.htm)|Morningstar|Morningstar|modificada|
|[x46WnJD9YKQXpRkE.htm](kingmaker-bestiary-items/x46WnJD9YKQXpRkE.htm)|Raging Storm|Tormenta Furia|modificada|
|[x8mIj7POmkmnt5WS.htm](kingmaker-bestiary-items/x8mIj7POmkmnt5WS.htm)|Divine Recovery|Divina Recuperación|modificada|
|[X9BvGRPY6jnbRCrG.htm](kingmaker-bestiary-items/X9BvGRPY6jnbRCrG.htm)|Throw Rock|Arrojar roca|modificada|
|[xBi3GMrwnT7AE4mq.htm](kingmaker-bestiary-items/xBi3GMrwnT7AE4mq.htm)|Grabbing Trunk|Agarrar con la trompa|modificada|
|[xBIoPO0Yl00JwtXW.htm](kingmaker-bestiary-items/xBIoPO0Yl00JwtXW.htm)|Swamp Stride|Swamp Stride|modificada|
|[xBvjgSrMNlbRs2vt.htm](kingmaker-bestiary-items/xBvjgSrMNlbRs2vt.htm)|Crossbow|Ballesta|modificada|
|[Xc15ip4V31NZTNdq.htm](kingmaker-bestiary-items/Xc15ip4V31NZTNdq.htm)|Frightful Presence|Frightful Presence|modificada|
|[XCFOQ29kYuqzushV.htm](kingmaker-bestiary-items/XCFOQ29kYuqzushV.htm)|Claw|Garra|modificada|
|[XCr9jxxE59Egb2R1.htm](kingmaker-bestiary-items/XCr9jxxE59Egb2R1.htm)|Woodland Ambush|Emboscar en el bosque|modificada|
|[XCuiusaM9Pgov7Hy.htm](kingmaker-bestiary-items/XCuiusaM9Pgov7Hy.htm)|Reactive|Reactive|modificada|
|[XCvqYacJqKAyKyBy.htm](kingmaker-bestiary-items/XCvqYacJqKAyKyBy.htm)|Change Shape|Change Shape|modificada|
|[xdpY6vYKdagXO6bk.htm](kingmaker-bestiary-items/xdpY6vYKdagXO6bk.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[XdUyVqH2LISYTKP4.htm](kingmaker-bestiary-items/XdUyVqH2LISYTKP4.htm)|Sickle|Hoz|modificada|
|[xeAR997MOrxz4cyA.htm](kingmaker-bestiary-items/xeAR997MOrxz4cyA.htm)|Gallop|Galope|modificada|
|[XEczLnuwuxklAds8.htm](kingmaker-bestiary-items/XEczLnuwuxklAds8.htm)|Adjust Temperature|Ajustar temperatura|modificada|
|[xeEI4dqDE4PmxOeA.htm](kingmaker-bestiary-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|Caminante del Bosque|modificada|
|[XEeQ6n37g3yEASJB.htm](kingmaker-bestiary-items/XEeQ6n37g3yEASJB.htm)|Dagger|Daga|modificada|
|[xEHY10Oxi5BJJCmQ.htm](kingmaker-bestiary-items/xEHY10Oxi5BJJCmQ.htm)|Wide Swing|Barrido ancho|modificada|
|[XfQQjjOdt5sRiSb6.htm](kingmaker-bestiary-items/XfQQjjOdt5sRiSb6.htm)|Lion Jaws|Fauces de León|modificada|
|[XFTxhLhYbwJ3pxdt.htm](kingmaker-bestiary-items/XFTxhLhYbwJ3pxdt.htm)|Unseen Sight|Unseen Sight|modificada|
|[XfUDUv0iccZ8hNjg.htm](kingmaker-bestiary-items/XfUDUv0iccZ8hNjg.htm)|Negative Healing|Curación negativa|modificada|
|[XFWwEVmfkf4Naohf.htm](kingmaker-bestiary-items/XFWwEVmfkf4Naohf.htm)|Jaws|Fauces|modificada|
|[XgjH4JAk9jbj9B8a.htm](kingmaker-bestiary-items/XgjH4JAk9jbj9B8a.htm)|Shamble|Shamble|modificada|
|[XgoaORovhBAB82vg.htm](kingmaker-bestiary-items/XgoaORovhBAB82vg.htm)|Constant Spells|Constant Spells|modificada|
|[xH6NrBw4bYEHU7e3.htm](kingmaker-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[XhhOGnxq5cUHkcEd.htm](kingmaker-bestiary-items/XhhOGnxq5cUHkcEd.htm)|Burble|Burble|modificada|
|[XHQxmGEm71C14Nci.htm](kingmaker-bestiary-items/XHQxmGEm71C14Nci.htm)|Grab|Agarrado|modificada|
|[XhrxL6SNJhTojhys.htm](kingmaker-bestiary-items/XhrxL6SNJhTojhys.htm)|Manifest Crystals|Manifestar Cristales|modificada|
|[xI9jqkNAsWJ8GrxC.htm](kingmaker-bestiary-items/xI9jqkNAsWJ8GrxC.htm)|Collapse|Colapso|modificada|
|[xIXTT704xBEHdGIn.htm](kingmaker-bestiary-items/xIXTT704xBEHdGIn.htm)|Dagger|Daga|modificada|
|[XJXxOQSoroFvm4m9.htm](kingmaker-bestiary-items/XJXxOQSoroFvm4m9.htm)|Constant Spells|Constant Spells|modificada|
|[xkPOrd9a16OIHTyl.htm](kingmaker-bestiary-items/xkPOrd9a16OIHTyl.htm)|Second Wind|Nuevas energías|modificada|
|[XKVb3v9QlngaKRQV.htm](kingmaker-bestiary-items/XKVb3v9QlngaKRQV.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[XmKUjz23s0dNFOYW.htm](kingmaker-bestiary-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|Podredumbre del pantano|modificada|
|[xMM6S0SqKD29GPxJ.htm](kingmaker-bestiary-items/xMM6S0SqKD29GPxJ.htm)|Site Bound|Ligado a una ubicación|modificada|
|[Xmq08HzwUTnliDuF.htm](kingmaker-bestiary-items/Xmq08HzwUTnliDuF.htm)|Fist|Puño|modificada|
|[XnVq5NAwuLiDa4dD.htm](kingmaker-bestiary-items/XnVq5NAwuLiDa4dD.htm)|Sudden Stormburst|Estallido repentino|modificada|
|[xq3V1P1OXiEGcqZs.htm](kingmaker-bestiary-items/xq3V1P1OXiEGcqZs.htm)|Angled Entry|Angled Entry|modificada|
|[xqB9FvxJUsDUDs15.htm](kingmaker-bestiary-items/xqB9FvxJUsDUDs15.htm)|Thorny Vine|Thorny Vine|modificada|
|[xqRgagTvArv2abwD.htm](kingmaker-bestiary-items/xqRgagTvArv2abwD.htm)|Constant Spells|Constant Spells|modificada|
|[XR8SUq7u8lPNwfsz.htm](kingmaker-bestiary-items/XR8SUq7u8lPNwfsz.htm)|Overhand Smash|Overhand Smash|modificada|
|[xRQZegRo2JOZB7hU.htm](kingmaker-bestiary-items/xRQZegRo2JOZB7hU.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[xrTNYpA1abm4LPwG.htm](kingmaker-bestiary-items/xrTNYpA1abm4LPwG.htm)|Coven|Coven|modificada|
|[xtGHTPAuSTsudXMG.htm](kingmaker-bestiary-items/xtGHTPAuSTsudXMG.htm)|Enhanced Familiar|Familiar potenciado|modificada|
|[xtitjNKcLXuYUcnS.htm](kingmaker-bestiary-items/xtitjNKcLXuYUcnS.htm)|Reel In|Arrastrar presa|modificada|
|[XU7E8CJIc6dwyRml.htm](kingmaker-bestiary-items/XU7E8CJIc6dwyRml.htm)|Knockback|Arredrar|modificada|
|[Xva9qQPkH1v1bsG1.htm](kingmaker-bestiary-items/Xva9qQPkH1v1bsG1.htm)|Floor Collapse|Colapso del suelo|modificada|
|[xwHcXDjVTSiAD1kB.htm](kingmaker-bestiary-items/xwHcXDjVTSiAD1kB.htm)|Wing Rebuff|Wing Rebuff|modificada|
|[xxUW9YkqVCI57nCo.htm](kingmaker-bestiary-items/xxUW9YkqVCI57nCo.htm)|Painful Caress|Caricia Dolorosa|modificada|
|[xXv6eUeKYXjO80rc.htm](kingmaker-bestiary-items/xXv6eUeKYXjO80rc.htm)|Swoop|Swoop|modificada|
|[xYyZLfzVCUdoVnQn.htm](kingmaker-bestiary-items/xYyZLfzVCUdoVnQn.htm)|Reach Spell|Conjuro de alcance|modificada|
|[xzuGv08xYVhfHdXW.htm](kingmaker-bestiary-items/xzuGv08xYVhfHdXW.htm)|Tail|Tail|modificada|
|[xZwLtaE6SEA50Rqf.htm](kingmaker-bestiary-items/xZwLtaE6SEA50Rqf.htm)|Twin Chop|Twin Chop|modificada|
|[Y0nwjk1NkdGHxBxi.htm](kingmaker-bestiary-items/Y0nwjk1NkdGHxBxi.htm)|Change Shape|Change Shape|modificada|
|[y1uZ9I98YpDpmpQx.htm](kingmaker-bestiary-items/y1uZ9I98YpDpmpQx.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[y6KKrN8Aiff22yLS.htm](kingmaker-bestiary-items/y6KKrN8Aiff22yLS.htm)|Bloodline Magic|Magia de linaje|modificada|
|[y8OK5g64U5E8qC0q.htm](kingmaker-bestiary-items/y8OK5g64U5E8qC0q.htm)|Vicious Ranseur|Vicious Ranseur|modificada|
|[Y9nkyrHVb6qQd6hs.htm](kingmaker-bestiary-items/Y9nkyrHVb6qQd6hs.htm)|Shame|Vergüenza|modificada|
|[YaqUxn6Ybbvxv3gB.htm](kingmaker-bestiary-items/YaqUxn6Ybbvxv3gB.htm)|Quill|Quill|modificada|
|[yaWanh7jgvzb4bNA.htm](kingmaker-bestiary-items/yaWanh7jgvzb4bNA.htm)|Jaws|Fauces|modificada|
|[YBRIxpMXUrxJfeV1.htm](kingmaker-bestiary-items/YBRIxpMXUrxJfeV1.htm)|Grab|Agarrado|modificada|
|[YCWMeM1zCWZqP48p.htm](kingmaker-bestiary-items/YCWMeM1zCWZqP48p.htm)|Rod of Razors|Vara de Navajas|modificada|
|[YdimsTpSLxkBXaBh.htm](kingmaker-bestiary-items/YdimsTpSLxkBXaBh.htm)|Arcane Spontaneous Spells|Hechizos Arcanos Espontáneos|modificada|
|[yEmvQZDwtr985C2Y.htm](kingmaker-bestiary-items/yEmvQZDwtr985C2Y.htm)|Hatchet Flurry|Ráfaga de hachazos|modificada|
|[YeVYuXNHpkcHdBo2.htm](kingmaker-bestiary-items/YeVYuXNHpkcHdBo2.htm)|Attack of Opportunity (Tail Only)|Ataque de oportunidad (sólo cola)|modificada|
|[YfFUNMAKaJ0wspVA.htm](kingmaker-bestiary-items/YfFUNMAKaJ0wspVA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ygeIx11fATpKbQRh.htm](kingmaker-bestiary-items/ygeIx11fATpKbQRh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yGkx5YIsqb7P1b34.htm](kingmaker-bestiary-items/yGkx5YIsqb7P1b34.htm)|Effortless Concentration|Concentración sin esfuerzo|modificada|
|[Yh6PceHFS60uaThW.htm](kingmaker-bestiary-items/Yh6PceHFS60uaThW.htm)|Release Boulders|Soltar cantos rodados|modificada|
|[YHmyiWDMvEgujfl1.htm](kingmaker-bestiary-items/YHmyiWDMvEgujfl1.htm)|Bully's Bludgeon|Bully's Bludgeon|modificada|
|[yI8AXFiHc0FyemjU.htm](kingmaker-bestiary-items/yI8AXFiHc0FyemjU.htm)|Ominous Mien|Ominous Mien|modificada|
|[yIiN4CzNAnrIGY5R.htm](kingmaker-bestiary-items/yIiN4CzNAnrIGY5R.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[yJ37AcaU1dl5m7KT.htm](kingmaker-bestiary-items/yJ37AcaU1dl5m7KT.htm)|Dagger|Daga|modificada|
|[yJ6tSdgHei2VkcFn.htm](kingmaker-bestiary-items/yJ6tSdgHei2VkcFn.htm)|Swift Claw|Garra Celeridad|modificada|
|[YJesTEWYLdHelRVi.htm](kingmaker-bestiary-items/YJesTEWYLdHelRVi.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[ykE4J2hEBa7gWmVx.htm](kingmaker-bestiary-items/ykE4J2hEBa7gWmVx.htm)|Bloom Step|Bloom Paso|modificada|
|[yKwcZmq0W6TpvY7z.htm](kingmaker-bestiary-items/yKwcZmq0W6TpvY7z.htm)|Lance|Lance|modificada|
|[yLgcfIF0Rbavu6Tr.htm](kingmaker-bestiary-items/yLgcfIF0Rbavu6Tr.htm)|Glaive|Glaive|modificada|
|[Ym0WhQCJHDGT3v4z.htm](kingmaker-bestiary-items/Ym0WhQCJHDGT3v4z.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[YMNKladez96kdBX8.htm](kingmaker-bestiary-items/YMNKladez96kdBX8.htm)|Nimble Dodge|Esquiva ágil|modificada|
|[yp7FDjO7715BHNPw.htm](kingmaker-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yPpDjeNgrvnyJ092.htm](kingmaker-bestiary-items/yPpDjeNgrvnyJ092.htm)|Distort Senses|Distorsionar Sentidos|modificada|
|[Yq84rD7Ts0Wko9Pf.htm](kingmaker-bestiary-items/Yq84rD7Ts0Wko9Pf.htm)|Rip and Drag|Rip and Drag|modificada|
|[yR0motfxtDNCXbQv.htm](kingmaker-bestiary-items/yR0motfxtDNCXbQv.htm)|Felling Blow|Felling Blow|modificada|
|[yrKXLmbaekza05cd.htm](kingmaker-bestiary-items/yrKXLmbaekza05cd.htm)|Improved Push 10 feet|Empuje mejorado 10 pies|modificada|
|[yS5bYDhKr2Koa3mm.htm](kingmaker-bestiary-items/yS5bYDhKr2Koa3mm.htm)|No Time To Die|No Time To Die|modificada|
|[yTVotZYCI6abxOvt.htm](kingmaker-bestiary-items/yTVotZYCI6abxOvt.htm)|Bastard Sword|Espada Bastarda|modificada|
|[yTWeFCTfaEECHhCs.htm](kingmaker-bestiary-items/yTWeFCTfaEECHhCs.htm)|Falchion|Falchion|modificada|
|[yvOTSeGrhPgBSOBZ.htm](kingmaker-bestiary-items/yvOTSeGrhPgBSOBZ.htm)|Capsize|Volcar|modificada|
|[YwEDGRZpJYEj2PKw.htm](kingmaker-bestiary-items/YwEDGRZpJYEj2PKw.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[yWipQYXVRovUBD3A.htm](kingmaker-bestiary-items/yWipQYXVRovUBD3A.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[yX5Z8bj03S67Thb5.htm](kingmaker-bestiary-items/yX5Z8bj03S67Thb5.htm)|Primal Spontaneous Spells|Primal Hechizos espontáneos|modificada|
|[YXiHAcR16OtpzApO.htm](kingmaker-bestiary-items/YXiHAcR16OtpzApO.htm)|Speed Surge|Arranque de velocidad|modificada|
|[YXm2W59CMTMhb25j.htm](kingmaker-bestiary-items/YXm2W59CMTMhb25j.htm)|Jaws|Fauces|modificada|
|[YxX6oMlTOXzTGheb.htm](kingmaker-bestiary-items/YxX6oMlTOXzTGheb.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[YyxKJRRXl2GdUfTt.htm](kingmaker-bestiary-items/YyxKJRRXl2GdUfTt.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yZCKHSiJwqO7N1sy.htm](kingmaker-bestiary-items/yZCKHSiJwqO7N1sy.htm)|Claws That Catch|Garras que atrapan|modificada|
|[Z08HAZUvnJBGvImG.htm](kingmaker-bestiary-items/Z08HAZUvnJBGvImG.htm)|Swarm Mind|Swarm Mind|modificada|
|[z0QT6nvmIHbeoGHa.htm](kingmaker-bestiary-items/z0QT6nvmIHbeoGHa.htm)|Jaws|Fauces|modificada|
|[z3giiwwqKX9RBgD8.htm](kingmaker-bestiary-items/z3giiwwqKX9RBgD8.htm)|Spew Seed Pod|Spew Seed Pod|modificada|
|[Z5F5Xx9nQAfj1TtM.htm](kingmaker-bestiary-items/Z5F5Xx9nQAfj1TtM.htm)|Fireball|Bola de fuego|modificada|
|[Z5Ph0zkg0xMl6iK6.htm](kingmaker-bestiary-items/Z5Ph0zkg0xMl6iK6.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[Z65zRWuULtfR4enK.htm](kingmaker-bestiary-items/Z65zRWuULtfR4enK.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[z8bl09rqF4yCJDy4.htm](kingmaker-bestiary-items/z8bl09rqF4yCJDy4.htm)|Gogunta's Croak|Croak de Gogunta|modificada|
|[z9bTah65DoASEDhP.htm](kingmaker-bestiary-items/z9bTah65DoASEDhP.htm)|Cobra Fangs|Colmillos de Cobra|modificada|
|[Z9mCi4uEoEQi6uMv.htm](kingmaker-bestiary-items/Z9mCi4uEoEQi6uMv.htm)|Quick Block|Bloqueo rápido|modificada|
|[ZaZTsIwbHhL3ZcQy.htm](kingmaker-bestiary-items/ZaZTsIwbHhL3ZcQy.htm)|Battle Axe|Hacha de batalla|modificada|
|[ZBxbAEeCdREK43Yu.htm](kingmaker-bestiary-items/ZBxbAEeCdREK43Yu.htm)|Bard Composition Spells|Conjuros de composición de bardo|modificada|
|[Zc2k22zs45gbuybT.htm](kingmaker-bestiary-items/Zc2k22zs45gbuybT.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZCOJIf8Nr9RxA4Pj.htm](kingmaker-bestiary-items/ZCOJIf8Nr9RxA4Pj.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[zDDzvsB3OrJU9QXn.htm](kingmaker-bestiary-items/zDDzvsB3OrJU9QXn.htm)|Vigorous Shake|Sacudida vigorosa|modificada|
|[zdtGqzcFir4opahs.htm](kingmaker-bestiary-items/zdtGqzcFir4opahs.htm)|Refocus Curse|Reenfocar Maldición|modificada|
|[zeyB4l2CYjcqiDrH.htm](kingmaker-bestiary-items/zeyB4l2CYjcqiDrH.htm)|Claw|Garra|modificada|
|[zfVISHBP6rc5o8Rv.htm](kingmaker-bestiary-items/zfVISHBP6rc5o8Rv.htm)|Exile's Curse|Exile's Curse|modificada|
|[ZfVwcTmrbWAVaOCX.htm](kingmaker-bestiary-items/ZfVwcTmrbWAVaOCX.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ZG61qjm93pm36Iw5.htm](kingmaker-bestiary-items/ZG61qjm93pm36Iw5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zgdHnMuj8S2a5m8L.htm](kingmaker-bestiary-items/zgdHnMuj8S2a5m8L.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[zGXZKhk95VPsSdKN.htm](kingmaker-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|Serpientes mordedoras|modificada|
|[ZH9w82rcAvicGUV6.htm](kingmaker-bestiary-items/ZH9w82rcAvicGUV6.htm)|Menace Prey|Menace Prey|modificada|
|[ziktYNU02MOBNRm7.htm](kingmaker-bestiary-items/ziktYNU02MOBNRm7.htm)|Infectious Wrath|Ira Infecciosa|modificada|
|[ZiobchoAZw8EGQAl.htm](kingmaker-bestiary-items/ZiobchoAZw8EGQAl.htm)|Recovery|Recuperación|modificada|
|[zKvBvpelIhRXPoSW.htm](kingmaker-bestiary-items/zKvBvpelIhRXPoSW.htm)|Thoughtsense|Percibir pensamientos|modificada|
|[ZlCrr2J1Yth6IZEj.htm](kingmaker-bestiary-items/ZlCrr2J1Yth6IZEj.htm)|Lamashtu's Bloom|Florecimiento de Lamashtu|modificada|
|[zm4JTE61At7s9UrX.htm](kingmaker-bestiary-items/zm4JTE61At7s9UrX.htm)|Blow Mists|Blow Mists|modificada|
|[zMgcdf4D6sTVSdON.htm](kingmaker-bestiary-items/zMgcdf4D6sTVSdON.htm)|Scent|Scent|modificada|
|[zNeDe02hwHzcyE1s.htm](kingmaker-bestiary-items/zNeDe02hwHzcyE1s.htm)|Squirming Embrace|Abrazo Retorcido|modificada|
|[zNJKK7fb7gXv78OV.htm](kingmaker-bestiary-items/zNJKK7fb7gXv78OV.htm)|Warhammer|Warhammer|modificada|
|[ZNWj00PO55PVPFmc.htm](kingmaker-bestiary-items/ZNWj00PO55PVPFmc.htm)|Shortsword|Espada corta|modificada|
|[ZNWKdUJvc61evvBc.htm](kingmaker-bestiary-items/ZNWKdUJvc61evvBc.htm)|Wide Swing|Barrido ancho|modificada|
|[zOBaEVlReAlrAIKE.htm](kingmaker-bestiary-items/zOBaEVlReAlrAIKE.htm)|Lonely Dirge|Lonely Dirge|modificada|
|[ZOJd2q3zsGLrA8Xw.htm](kingmaker-bestiary-items/ZOJd2q3zsGLrA8Xw.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[ZPVlL1UH75oMFTCB.htm](kingmaker-bestiary-items/ZPVlL1UH75oMFTCB.htm)|Armag's Rage|Furia de Armag|modificada|
|[zquNaeOnacgZG38j.htm](kingmaker-bestiary-items/zquNaeOnacgZG38j.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZQWoQnlsIHN6EyFc.htm](kingmaker-bestiary-items/ZQWoQnlsIHN6EyFc.htm)|Wolf Jaws|Fauces de Lobo|modificada|
|[ZR6JE7Gh0fgDobEm.htm](kingmaker-bestiary-items/ZR6JE7Gh0fgDobEm.htm)|Lurker Stance|Posición de Merodeador|modificada|
|[zRI7pbJvbWWPBJQr.htm](kingmaker-bestiary-items/zRI7pbJvbWWPBJQr.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[ZrwMBFIWJnpx6Utu.htm](kingmaker-bestiary-items/ZrwMBFIWJnpx6Utu.htm)|Dread Striker|Hostigador temible|modificada|
|[ZSab4ZN954h6Ckqb.htm](kingmaker-bestiary-items/ZSab4ZN954h6Ckqb.htm)|Atrophied Lich|Atrophied Lich|modificada|
|[ztl64mYGjNx9Qab6.htm](kingmaker-bestiary-items/ztl64mYGjNx9Qab6.htm)|Sorcerer Bloodline Spells|Conjuros de linaje hechicero|modificada|
|[ZvqnxVWvm9QdJZVi.htm](kingmaker-bestiary-items/ZvqnxVWvm9QdJZVi.htm)|Blood Scent|Blood Scent|modificada|
|[zWVvmrN2Kq0dZWsu.htm](kingmaker-bestiary-items/zWVvmrN2Kq0dZWsu.htm)|Freezing Wind|Freezing Wind|modificada|
|[ZYFSO8d8sBeN7WO7.htm](kingmaker-bestiary-items/ZYFSO8d8sBeN7WO7.htm)|Unsettling Revelation|Revelación inquietante|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Rjcnl6ynsH5LKjO.htm](kingmaker-bestiary-items/0Rjcnl6ynsH5LKjO.htm)|Warfare Lore|vacía|
|[0UqqnUjTvhlc7sGv.htm](kingmaker-bestiary-items/0UqqnUjTvhlc7sGv.htm)|Bardic Lore|vacía|
|[2wByf3NmPbP0YRzy.htm](kingmaker-bestiary-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|vacía|
|[38JeDzTzMbmPrydc.htm](kingmaker-bestiary-items/38JeDzTzMbmPrydc.htm)|Survival|vacía|
|[4KoYrmcXEdMKXapg.htm](kingmaker-bestiary-items/4KoYrmcXEdMKXapg.htm)|Academia Lore|vacía|
|[4uhmqqCnN4EAqpVy.htm](kingmaker-bestiary-items/4uhmqqCnN4EAqpVy.htm)|Rock|vacía|
|[5c6243RybWvondkY.htm](kingmaker-bestiary-items/5c6243RybWvondkY.htm)|Silver Stag Lord Amulet|vacía|
|[5MTbqNxF7xLIRqQY.htm](kingmaker-bestiary-items/5MTbqNxF7xLIRqQY.htm)|Abaddon Lore|vacía|
|[5yj5ekSognH3S3RW.htm](kingmaker-bestiary-items/5yj5ekSognH3S3RW.htm)|Silver Stag Lord Amulet|vacía|
|[61i4ipb2qXp6RshD.htm](kingmaker-bestiary-items/61i4ipb2qXp6RshD.htm)|Lumber Lore|vacía|
|[64snLNQ9P2c8IffF.htm](kingmaker-bestiary-items/64snLNQ9P2c8IffF.htm)|Guild Lore|vacía|
|[6ofr6YtODR30jRp0.htm](kingmaker-bestiary-items/6ofr6YtODR30jRp0.htm)|Lice-Infested Furs|vacía|
|[7ovDTTZtTBqn2eJ6.htm](kingmaker-bestiary-items/7ovDTTZtTBqn2eJ6.htm)|Performance|vacía|
|[7zyckBxX0Qo3fPo5.htm](kingmaker-bestiary-items/7zyckBxX0Qo3fPo5.htm)|Chain of Lovelies|vacía|
|[8EaMfqK0UpEoUqcg.htm](kingmaker-bestiary-items/8EaMfqK0UpEoUqcg.htm)|Acrobatics|vacía|
|[8I1oL3XnWQjb1WeF.htm](kingmaker-bestiary-items/8I1oL3XnWQjb1WeF.htm)|Torture Lore|vacía|
|[8Ss1d21OHToUfFNl.htm](kingmaker-bestiary-items/8Ss1d21OHToUfFNl.htm)|Yog-Sothoth Lore|vacía|
|[9h5jUpLhqi52Tedi.htm](kingmaker-bestiary-items/9h5jUpLhqi52Tedi.htm)|Brevoy Lore|vacía|
|[a4xj4dNHkUjHRH4O.htm](kingmaker-bestiary-items/a4xj4dNHkUjHRH4O.htm)|Silver Ring|vacía|
|[aDli69WFeRXUP2U3.htm](kingmaker-bestiary-items/aDli69WFeRXUP2U3.htm)|Ceremonial Robes|vacía|
|[AJYRmsDmV1LVU4Rv.htm](kingmaker-bestiary-items/AJYRmsDmV1LVU4Rv.htm)|Silver Stag Lord Amulet|vacía|
|[ap7UmDIuWShLoSbJ.htm](kingmaker-bestiary-items/ap7UmDIuWShLoSbJ.htm)|Forest Lore|vacía|
|[AYlNJO8ziqGDNG6F.htm](kingmaker-bestiary-items/AYlNJO8ziqGDNG6F.htm)|Forest Lore|vacía|
|[b8MFwyWeJ5rqO3Vk.htm](kingmaker-bestiary-items/b8MFwyWeJ5rqO3Vk.htm)|Stealth|vacía|
|[BbhEZOpQbLUnfahO.htm](kingmaker-bestiary-items/BbhEZOpQbLUnfahO.htm)|Ruby Ring|vacía|
|[BMzs4OQEu3Mt6iZQ.htm](kingmaker-bestiary-items/BMzs4OQEu3Mt6iZQ.htm)|Gambling Lore|vacía|
|[bo6bsp0fKrJ9ddNK.htm](kingmaker-bestiary-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|vacía|
|[BVk9QB1imJ9KD0Vq.htm](kingmaker-bestiary-items/BVk9QB1imJ9KD0Vq.htm)|Silver Stag Lord Amulet|vacía|
|[CBKkrJnJ9DcFA6p0.htm](kingmaker-bestiary-items/CBKkrJnJ9DcFA6p0.htm)|Hunting Lore|vacía|
|[cjGRYigrrey1TIiS.htm](kingmaker-bestiary-items/cjGRYigrrey1TIiS.htm)|Underworld Lore|vacía|
|[cu5gzpbsORryK2Zs.htm](kingmaker-bestiary-items/cu5gzpbsORryK2Zs.htm)|Athletics|vacía|
|[cuoRWPoyJkfPoJ58.htm](kingmaker-bestiary-items/cuoRWPoyJkfPoJ58.htm)|Stealth|vacía|
|[dCRhF1JVyPGvXOfW.htm](kingmaker-bestiary-items/dCRhF1JVyPGvXOfW.htm)|First World Lore|vacía|
|[dGa7fiQkgtKN8PDA.htm](kingmaker-bestiary-items/dGa7fiQkgtKN8PDA.htm)|Crafting|vacía|
|[dhHfUP1VQHRPtvdU.htm](kingmaker-bestiary-items/dhHfUP1VQHRPtvdU.htm)|Stealth|vacía|
|[DiARocJoBMTbmCN9.htm](kingmaker-bestiary-items/DiARocJoBMTbmCN9.htm)|Key to Area E4|vacía|
|[dsVVZHTsFi9iFwgD.htm](kingmaker-bestiary-items/dsVVZHTsFi9iFwgD.htm)|Hill Lore|vacía|
|[Ekik19hqrAXIezul.htm](kingmaker-bestiary-items/Ekik19hqrAXIezul.htm)|Lock of green hair bound with red twine|vacía|
|[fYdweFlxcKABfwpC.htm](kingmaker-bestiary-items/fYdweFlxcKABfwpC.htm)|Rock|vacía|
|[g6XydGLF6RtVUp5L.htm](kingmaker-bestiary-items/g6XydGLF6RtVUp5L.htm)|Stealth|vacía|
|[gS87nm7lmCmY8pzz.htm](kingmaker-bestiary-items/gS87nm7lmCmY8pzz.htm)|First World Lore|vacía|
|[GZ3aIv1wg9M7gGFU.htm](kingmaker-bestiary-items/GZ3aIv1wg9M7gGFU.htm)|Giant Lore|vacía|
|[hqd0OwbFVtZ6VGi3.htm](kingmaker-bestiary-items/hqd0OwbFVtZ6VGi3.htm)|Sapphire Earrings|vacía|
|[HZjTjHMm6PiqprBW.htm](kingmaker-bestiary-items/HZjTjHMm6PiqprBW.htm)|Stealth|vacía|
|[iGGr29rdFCwRoCHG.htm](kingmaker-bestiary-items/iGGr29rdFCwRoCHG.htm)|Turquise Earrings|vacía|
|[IP2DHIvoqbsWaRVU.htm](kingmaker-bestiary-items/IP2DHIvoqbsWaRVU.htm)|Cleaver|vacía|
|[ItuwvVtjFsk5P6Rk.htm](kingmaker-bestiary-items/ItuwvVtjFsk5P6Rk.htm)|Knight and Dragon Toys|vacía|
|[j0CQH72Y8ZIJSLQK.htm](kingmaker-bestiary-items/j0CQH72Y8ZIJSLQK.htm)|Athletics|vacía|
|[jkgZ1UCUT3j7cOf2.htm](kingmaker-bestiary-items/jkgZ1UCUT3j7cOf2.htm)|Restov Lore|vacía|
|[kCwijuNrHVlRHQr0.htm](kingmaker-bestiary-items/kCwijuNrHVlRHQr0.htm)|Flask of Wine|vacía|
|[khNk0fYgnDMF7kKq.htm](kingmaker-bestiary-items/khNk0fYgnDMF7kKq.htm)|First World Lore|vacía|
|[kJR445IOTwrpVfxm.htm](kingmaker-bestiary-items/kJR445IOTwrpVfxm.htm)|Emerald Ring|vacía|
|[KpHIq12RL6K9ukG2.htm](kingmaker-bestiary-items/KpHIq12RL6K9ukG2.htm)|First World Lore|vacía|
|[kTXlEGWWz4WnsrwF.htm](kingmaker-bestiary-items/kTXlEGWWz4WnsrwF.htm)|Warfare Lore|vacía|
|[KV65DhNy4Axid8kc.htm](kingmaker-bestiary-items/KV65DhNy4Axid8kc.htm)|Cleaver|vacía|
|[KwNXLXzskiVB9RLh.htm](kingmaker-bestiary-items/KwNXLXzskiVB9RLh.htm)|Stealth|vacía|
|[kynHGhmwcc8qJ5T8.htm](kingmaker-bestiary-items/kynHGhmwcc8qJ5T8.htm)|Stealth|vacía|
|[lbWYtWXNDRTmYH9O.htm](kingmaker-bestiary-items/lbWYtWXNDRTmYH9O.htm)|Gold Signet Ring|vacía|
|[LUdvij5KTX4oc3yH.htm](kingmaker-bestiary-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|vacía|
|[M0O2aUtdSXT0HpSQ.htm](kingmaker-bestiary-items/M0O2aUtdSXT0HpSQ.htm)|Chieftain's Necklace|vacía|
|[M5azwdU2fCtU2eR5.htm](kingmaker-bestiary-items/M5azwdU2fCtU2eR5.htm)|Gorum Lore|vacía|
|[MDHk8qTYvCueXiiR.htm](kingmaker-bestiary-items/MDHk8qTYvCueXiiR.htm)|Dwelling Lore|vacía|
|[mKdD7XKIAXJx8KnC.htm](kingmaker-bestiary-items/mKdD7XKIAXJx8KnC.htm)|Athletics|vacía|
|[n8TGGjTIP2jpXLiC.htm](kingmaker-bestiary-items/n8TGGjTIP2jpXLiC.htm)|Monstrous Bloom|vacía|
|[neZCdGIJBKYWKN5Q.htm](kingmaker-bestiary-items/neZCdGIJBKYWKN5Q.htm)|Deception|vacía|
|[nnU6e1a4DLTuRnUV.htm](kingmaker-bestiary-items/nnU6e1a4DLTuRnUV.htm)|Academia Lore|vacía|
|[o82vOevA36JTvcnU.htm](kingmaker-bestiary-items/o82vOevA36JTvcnU.htm)|Forest Lore|vacía|
|[OQv2VZ49tvgCKDhW.htm](kingmaker-bestiary-items/OQv2VZ49tvgCKDhW.htm)|First World Lore|vacía|
|[orJLwvKxUPCxgkWs.htm](kingmaker-bestiary-items/orJLwvKxUPCxgkWs.htm)|Monarch's Horn|vacía|
|[pfic0QiF1jUkDVwR.htm](kingmaker-bestiary-items/pfic0QiF1jUkDVwR.htm)|River Lore|vacía|
|[PLl0lrfdaaqgezrn.htm](kingmaker-bestiary-items/PLl0lrfdaaqgezrn.htm)|Clothing and Crown made of thorny vines and weeds|vacía|
|[PmFUHtH32XT97IlJ.htm](kingmaker-bestiary-items/PmFUHtH32XT97IlJ.htm)|Warfare Lore|vacía|
|[pmVLSDQuvnEqYH5y.htm](kingmaker-bestiary-items/pmVLSDQuvnEqYH5y.htm)|Fey Lore|vacía|
|[pu7iTks4TUgGpglG.htm](kingmaker-bestiary-items/pu7iTks4TUgGpglG.htm)|First World Lore|vacía|
|[Q2aaq7yspaPD9kAk.htm](kingmaker-bestiary-items/Q2aaq7yspaPD9kAk.htm)|Nobility Lore|vacía|
|[qesdwQLA0OfebLki.htm](kingmaker-bestiary-items/qesdwQLA0OfebLki.htm)|Intimidation|vacía|
|[qvps9GmZSlGuhdrK.htm](kingmaker-bestiary-items/qvps9GmZSlGuhdrK.htm)|Royal Outfit|vacía|
|[QW0dGg59W0r9tJtU.htm](kingmaker-bestiary-items/QW0dGg59W0r9tJtU.htm)|Stealth|vacía|
|[r0Jfu2EKhpHiK8Io.htm](kingmaker-bestiary-items/r0Jfu2EKhpHiK8Io.htm)|Athletics|vacía|
|[rf2VoAlQi6dlp1Yq.htm](kingmaker-bestiary-items/rf2VoAlQi6dlp1Yq.htm)|Silver Stag Lord Amulet|vacía|
|[RNxWSJIolrbdRbNy.htm](kingmaker-bestiary-items/RNxWSJIolrbdRbNy.htm)|Bardic Lore|vacía|
|[rs8cxUHAMnuhP7nv.htm](kingmaker-bestiary-items/rs8cxUHAMnuhP7nv.htm)|Stealth|vacía|
|[sTjHob8UlAoUPu20.htm](kingmaker-bestiary-items/sTjHob8UlAoUPu20.htm)|Yog-Sothoth Lore|vacía|
|[TSDKiQYnEhZxGO3K.htm](kingmaker-bestiary-items/TSDKiQYnEhZxGO3K.htm)|Warfare Lore|vacía|
|[tsZqAzdWTSJww5o9.htm](kingmaker-bestiary-items/tsZqAzdWTSJww5o9.htm)|First World Lore|vacía|
|[TWA2MXZuX91plGrL.htm](kingmaker-bestiary-items/TWA2MXZuX91plGrL.htm)|Art Lore|vacía|
|[U69KdW5yZ21jJT4a.htm](kingmaker-bestiary-items/U69KdW5yZ21jJT4a.htm)|Yog-Sothoth Lore|vacía|
|[UGAcwTdmLcqn2A5C.htm](kingmaker-bestiary-items/UGAcwTdmLcqn2A5C.htm)|River Lore|vacía|
|[UiU7w5ovykgz63Vf.htm](kingmaker-bestiary-items/UiU7w5ovykgz63Vf.htm)|First World Lore|vacía|
|[UZ4VlSGBzBXCEdfz.htm](kingmaker-bestiary-items/UZ4VlSGBzBXCEdfz.htm)|Religious Symbol of Lamashtu (Gold)|vacía|
|[VaiIAm2nnOW5bySh.htm](kingmaker-bestiary-items/VaiIAm2nnOW5bySh.htm)|Silver Stag Lord Amulet|vacía|
|[viyrIe0gXqX4x1ZG.htm](kingmaker-bestiary-items/viyrIe0gXqX4x1ZG.htm)|Mithral Necklace|vacía|
|[VVezMNwhjbCrp0yX.htm](kingmaker-bestiary-items/VVezMNwhjbCrp0yX.htm)|Warfare Lore|vacía|
|[WD2RKxV7k6NLl0Bn.htm](kingmaker-bestiary-items/WD2RKxV7k6NLl0Bn.htm)|Ragged Robes|vacía|
|[wFE3Lth938xGYdIL.htm](kingmaker-bestiary-items/wFE3Lth938xGYdIL.htm)|Warfare Lore|vacía|
|[wll4JHYsNNNsx4lF.htm](kingmaker-bestiary-items/wll4JHYsNNNsx4lF.htm)|Academia Lore|vacía|
|[X7v48sCoD3y3bAyO.htm](kingmaker-bestiary-items/X7v48sCoD3y3bAyO.htm)|Cultist Robes|vacía|
|[xAxV9yRmp780wKql.htm](kingmaker-bestiary-items/xAxV9yRmp780wKql.htm)|Stealth|vacía|
|[xj83ymsPlY7u87j1.htm](kingmaker-bestiary-items/xj83ymsPlY7u87j1.htm)|Athletics|vacía|
|[XlxlWOl8QpPb2Inn.htm](kingmaker-bestiary-items/XlxlWOl8QpPb2Inn.htm)|Sack of Flytrap Pods|vacía|
|[XwujWRcPvBs2BAHX.htm](kingmaker-bestiary-items/XwujWRcPvBs2BAHX.htm)|Sack for Holding Rocks|vacía|
|[xYt0Yl9LAnVB1BZ6.htm](kingmaker-bestiary-items/xYt0Yl9LAnVB1BZ6.htm)|Warfare Lore|vacía|
|[y57fpMJDVfTQ3moG.htm](kingmaker-bestiary-items/y57fpMJDVfTQ3moG.htm)|Brevoy Lore|vacía|
|[YJbZ9CTWY2HNVBXB.htm](kingmaker-bestiary-items/YJbZ9CTWY2HNVBXB.htm)|Silver Stag Lord Amulet|vacía|
|[ypEVZiKiaV4PXVPe.htm](kingmaker-bestiary-items/ypEVZiKiaV4PXVPe.htm)|Yog-Sothoth Lore|vacía|
|[ysZ3MQQymY3E22BQ.htm](kingmaker-bestiary-items/ysZ3MQQymY3E22BQ.htm)|Survival|vacía|
|[Yutc2Syx2q0EKb1P.htm](kingmaker-bestiary-items/Yutc2Syx2q0EKb1P.htm)|Heraldry Lore|vacía|
|[YXGsH5DgEDFiRIu7.htm](kingmaker-bestiary-items/YXGsH5DgEDFiRIu7.htm)|Lumber Lore|vacía|
|[YyYWWpan1IChNfUA.htm](kingmaker-bestiary-items/YyYWWpan1IChNfUA.htm)|Jade and Pearl Necklace|vacía|
|[z8haj2djGN404zAS.htm](kingmaker-bestiary-items/z8haj2djGN404zAS.htm)|Diplomacy|vacía|
|[zWFmuptIJBa1t8FT.htm](kingmaker-bestiary-items/zWFmuptIJBa1t8FT.htm)|Warfare Lore|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
