# Estado de la traducción (troubles-in-otari-bestiary)

 * **modificada**: 18


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0Fvulq5Zv4BQPvYV.htm](troubles-in-otari-bestiary/0Fvulq5Zv4BQPvYV.htm)|Collapsing Porch|Porche Colapsable|modificada|
|[3Pa5JoY9PYdV5x1e.htm](troubles-in-otari-bestiary/3Pa5JoY9PYdV5x1e.htm)|Brimstone Rat|Rata de azufre|modificada|
|[4GZsTkvuYADpcdmV.htm](troubles-in-otari-bestiary/4GZsTkvuYADpcdmV.htm)|Kurnugian Jackal|Chacal Kurnugiano|modificada|
|[5iOgKbhW9zVnV1cr.htm](troubles-in-otari-bestiary/5iOgKbhW9zVnV1cr.htm)|Bugbear Marauder|Bugbear Marauder|modificada|
|[6Ut7YjjLZADMU7NY.htm](troubles-in-otari-bestiary/6Ut7YjjLZADMU7NY.htm)|Hargrit Leadbuster|Hargrit Leadbuster|modificada|
|[aqDFxmLMx6cOnGlx.htm](troubles-in-otari-bestiary/aqDFxmLMx6cOnGlx.htm)|Tongues of Flame|Lenguas de fuegoígera|modificada|
|[b8Wgun7T7sh7NVrd.htm](troubles-in-otari-bestiary/b8Wgun7T7sh7NVrd.htm)|Web Lurker Noose|Lazo acechador de telara|modificada|
|[fhItdltCmcAZkvcI.htm](troubles-in-otari-bestiary/fhItdltCmcAZkvcI.htm)|Kobold Trapmaster|Kobold Trapmaster|modificada|
|[hv50ovLKV6lu49dF.htm](troubles-in-otari-bestiary/hv50ovLKV6lu49dF.htm)|Nightmare Terrain|Terreno de pesadilla|modificada|
|[l7wng1EXY7np8Hcu.htm](troubles-in-otari-bestiary/l7wng1EXY7np8Hcu.htm)|Scalliwing|Escama alada|modificada|
|[lTVgdXuSv2B0yVVW.htm](troubles-in-otari-bestiary/lTVgdXuSv2B0yVVW.htm)|Morgrym Leadbuster|Morgrym Leadbuster|modificada|
|[m4jitI3J1QkjJq3H.htm](troubles-in-otari-bestiary/m4jitI3J1QkjJq3H.htm)|Omblin Leadbuster|Omblin Leadbuster|modificada|
|[MTI298bhLGYktyZX.htm](troubles-in-otari-bestiary/MTI298bhLGYktyZX.htm)|Orc Scrapper|Orc Scrapper|modificada|
|[NeFSJTGROSgGIKNd.htm](troubles-in-otari-bestiary/NeFSJTGROSgGIKNd.htm)|Viper Urn|Viper Urn|modificada|
|[PhLxqnAIhCovfcXy.htm](troubles-in-otari-bestiary/PhLxqnAIhCovfcXy.htm)|Kotgar Leadbuster|Kotgar Leadbuster|modificada|
|[rRSIqZ5g3QRfaAQ0.htm](troubles-in-otari-bestiary/rRSIqZ5g3QRfaAQ0.htm)|Orc Commander|Comandante orco|modificada|
|[TIX9yDasefwJ4PxI.htm](troubles-in-otari-bestiary/TIX9yDasefwJ4PxI.htm)|Stinkweed Shambler|Brozas hierba podrida|modificada|
|[vg6fEjWSj4jilXRn.htm](troubles-in-otari-bestiary/vg6fEjWSj4jilXRn.htm)|Magic Starknives Trap|Magic Starknives Trampa|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
