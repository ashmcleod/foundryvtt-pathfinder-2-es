# Estado de la traducción (menace-under-otari-bestiary-items)

 * **modificada**: 268
 * **vacía**: 13
 * **ninguna**: 1
 * **oficial**: 3


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[fx6MEhp40JvDajQh.htm](menace-under-otari-bestiary-items/fx6MEhp40JvDajQh.htm)|Spike Trap|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0c1Yjs4t9BKCpjmr.htm](menace-under-otari-bestiary-items/0c1Yjs4t9BKCpjmr.htm)|Falling Stones|Falling Stones|modificada|
|[0j2Oa8lKuFbrPVH4.htm](menace-under-otari-bestiary-items/0j2Oa8lKuFbrPVH4.htm)|Wizard Spells|Hechizos de Mago|modificada|
|[0lvwb1cNAaOUc6Be.htm](menace-under-otari-bestiary-items/0lvwb1cNAaOUc6Be.htm)|End the Charade|Poner fin a la farsa|modificada|
|[0Qt9ULbGzJV8ZG3W.htm](menace-under-otari-bestiary-items/0Qt9ULbGzJV8ZG3W.htm)|Shadow Spawn|Sombra Spawn|modificada|
|[0qvSsLdTulsncbsi.htm](menace-under-otari-bestiary-items/0qvSsLdTulsncbsi.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[0QWVJ7Of8cQNKp0N.htm](menace-under-otari-bestiary-items/0QWVJ7Of8cQNKp0N.htm)|Slink in Shadows|Escabullirse entre las sombras|modificada|
|[1fzwu2J1jldVzBPc.htm](menace-under-otari-bestiary-items/1fzwu2J1jldVzBPc.htm)|Coil|Retorcer|modificada|
|[1GBTPnqIIGld3Wu1.htm](menace-under-otari-bestiary-items/1GBTPnqIIGld3Wu1.htm)|Dragonscaled|Escamas de dragón|modificada|
|[1U37Lsr3NMgzLqLM.htm](menace-under-otari-bestiary-items/1U37Lsr3NMgzLqLM.htm)|Mandibles|Mandíbulas|modificada|
|[1V9uaQOnCMGgMxl2.htm](menace-under-otari-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[20wHQZMjGAPl30DH.htm](menace-under-otari-bestiary-items/20wHQZMjGAPl30DH.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2d3ZGRIXERcjqnmJ.htm](menace-under-otari-bestiary-items/2d3ZGRIXERcjqnmJ.htm)|Grab|Agarrado|modificada|
|[38uEEYKxUvG8TDFw.htm](menace-under-otari-bestiary-items/38uEEYKxUvG8TDFw.htm)|Crossbow|Ballesta|modificada|
|[3g9yO52wil4VC4Dz.htm](menace-under-otari-bestiary-items/3g9yO52wil4VC4Dz.htm)|Fangs|Colmillos|modificada|
|[3USivzG0qztGd1dK.htm](menace-under-otari-bestiary-items/3USivzG0qztGd1dK.htm)|Knockdown|Derribo|modificada|
|[3w5kvSRp3MF3snpw.htm](menace-under-otari-bestiary-items/3w5kvSRp3MF3snpw.htm)|Spring|Primavera|modificada|
|[4736AvChlsLG9HVa.htm](menace-under-otari-bestiary-items/4736AvChlsLG9HVa.htm)|Horn|Cuerno|modificada|
|[4EXgRMqNIRLXxB1c.htm](menace-under-otari-bestiary-items/4EXgRMqNIRLXxB1c.htm)|Shortbow|Arco corto|modificada|
|[4gkC6A8D27Jzv7nH.htm](menace-under-otari-bestiary-items/4gkC6A8D27Jzv7nH.htm)|Broad Swipe|Vaivén amplio|modificada|
|[4NqTHQqAkLmiUovi.htm](menace-under-otari-bestiary-items/4NqTHQqAkLmiUovi.htm)|Stench|Hedor|modificada|
|[4tRvd9W1xA0JvgPJ.htm](menace-under-otari-bestiary-items/4tRvd9W1xA0JvgPJ.htm)|Claw|Garra|modificada|
|[4uWQE7ermpG8oEr3.htm](menace-under-otari-bestiary-items/4uWQE7ermpG8oEr3.htm)|Greatsword|Greatsword|modificada|
|[50Meu5uWTOPYtMu6.htm](menace-under-otari-bestiary-items/50Meu5uWTOPYtMu6.htm)|Regeneration 20 (Deactivated by Acid or Fire)|Regeneración 20 (Desactivado por Ácido o Fuego)|modificada|
|[54j0gybWlIAueFkZ.htm](menace-under-otari-bestiary-items/54j0gybWlIAueFkZ.htm)|Shortsword|Espada corta|modificada|
|[59wy5xK7MEQmYRxg.htm](menace-under-otari-bestiary-items/59wy5xK7MEQmYRxg.htm)|Earth Glide|Deslizamiento Terrestre|modificada|
|[5e9xpAPgJWQxMqDB.htm](menace-under-otari-bestiary-items/5e9xpAPgJWQxMqDB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[5HpQse2MIBQbs10B.htm](menace-under-otari-bestiary-items/5HpQse2MIBQbs10B.htm)|Web|Telara|modificada|
|[5IHmSdZpGxrw76lc.htm](menace-under-otari-bestiary-items/5IHmSdZpGxrw76lc.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5o4Qh26axxAvJeUp.htm](menace-under-otari-bestiary-items/5o4Qh26axxAvJeUp.htm)|Grab|Agarrado|modificada|
|[6ii2IeWpDJ46u3xM.htm](menace-under-otari-bestiary-items/6ii2IeWpDJ46u3xM.htm)|Rapier|Estoque|modificada|
|[6yBN6jKH0nlzgvOT.htm](menace-under-otari-bestiary-items/6yBN6jKH0nlzgvOT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[7ODNVUAaNraSivsT.htm](menace-under-otari-bestiary-items/7ODNVUAaNraSivsT.htm)|Shortbow|Arco corto|modificada|
|[7ogTgoQe4WAvtdJ7.htm](menace-under-otari-bestiary-items/7ogTgoQe4WAvtdJ7.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[7RTjFlVzsxhjtwX4.htm](menace-under-otari-bestiary-items/7RTjFlVzsxhjtwX4.htm)|Consume Flesh|Consumir carne|modificada|
|[7swXlOjSl2oHLuDv.htm](menace-under-otari-bestiary-items/7swXlOjSl2oHLuDv.htm)|Greatsword|Greatsword|modificada|
|[83QmDBGigZxaHbbd.htm](menace-under-otari-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8akVcjr4Y8JZR5Ht.htm](menace-under-otari-bestiary-items/8akVcjr4Y8JZR5Ht.htm)|Club|Club|modificada|
|[8Hc25NUrYBwSHQuO.htm](menace-under-otari-bestiary-items/8Hc25NUrYBwSHQuO.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[8jwjSmQRQW8Nk5NY.htm](menace-under-otari-bestiary-items/8jwjSmQRQW8Nk5NY.htm)|Fist|Puño|modificada|
|[8LYUZymM9ZN8WDw9.htm](menace-under-otari-bestiary-items/8LYUZymM9ZN8WDw9.htm)|Claw|Garra|modificada|
|[90eQFUneXcsHeKSK.htm](menace-under-otari-bestiary-items/90eQFUneXcsHeKSK.htm)|Staff|Báculo|modificada|
|[91iOrkVbzeP7cK2M.htm](menace-under-otari-bestiary-items/91iOrkVbzeP7cK2M.htm)|Adhesive|Adhesivo|modificada|
|[94P2S8OyxiovGujz.htm](menace-under-otari-bestiary-items/94P2S8OyxiovGujz.htm)|Twisting Tail|Enredar con la cola|modificada|
|[9bomxaBEHSLvRrkq.htm](menace-under-otari-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|Estatua|modificada|
|[9IuPTxCvRCR7HHp2.htm](menace-under-otari-bestiary-items/9IuPTxCvRCR7HHp2.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[9nfzrmPzAAStRjAe.htm](menace-under-otari-bestiary-items/9nfzrmPzAAStRjAe.htm)|Quick Draw|Desenvainado rápido|modificada|
|[9TCF9GcaMoTKkPSj.htm](menace-under-otari-bestiary-items/9TCF9GcaMoTKkPSj.htm)|Paralysis|Parálisis|modificada|
|[9yktjCbx8rbvpSZe.htm](menace-under-otari-bestiary-items/9yktjCbx8rbvpSZe.htm)|Spear Barrage|Andanada de lanzas|modificada|
|[a4I77q6CzdLCZCG6.htm](menace-under-otari-bestiary-items/a4I77q6CzdLCZCG6.htm)|Shortbow|Arco corto|modificada|
|[A97t9GcQF8k7V7By.htm](menace-under-otari-bestiary-items/A97t9GcQF8k7V7By.htm)|Claw|Garra|modificada|
|[ACHlnYzXVxizzqPw.htm](menace-under-otari-bestiary-items/ACHlnYzXVxizzqPw.htm)|Light Vulnerability|Vulnerabilidad a la luz|modificada|
|[ade7S9RWaPXF7KmF.htm](menace-under-otari-bestiary-items/ade7S9RWaPXF7KmF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ae0gibnuQy3PwVCV.htm](menace-under-otari-bestiary-items/Ae0gibnuQy3PwVCV.htm)|Spear|Lanza|modificada|
|[Af1bf54ub8OEzyPl.htm](menace-under-otari-bestiary-items/Af1bf54ub8OEzyPl.htm)|Pseudopod|Pseudópodo|modificada|
|[AGjtcz0cAWzBXlEx.htm](menace-under-otari-bestiary-items/AGjtcz0cAWzBXlEx.htm)|Unluck Aura|Aura de mala suerte|modificada|
|[ANrh02werSO57j4X.htm](menace-under-otari-bestiary-items/ANrh02werSO57j4X.htm)|Wing|Ala|modificada|
|[aoyjyhv7Dw4gIwd3.htm](menace-under-otari-bestiary-items/aoyjyhv7Dw4gIwd3.htm)|Claw|Garra|modificada|
|[AtTvfif8mNvTl8HD.htm](menace-under-otari-bestiary-items/AtTvfif8mNvTl8HD.htm)|Ferocity|Ferocidad|modificada|
|[B1zHLf1i3W3pgrBz.htm](menace-under-otari-bestiary-items/B1zHLf1i3W3pgrBz.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[b2WLYPv43nso3X3U.htm](menace-under-otari-bestiary-items/b2WLYPv43nso3X3U.htm)|Jaws|Fauces|modificada|
|[B9C716BDIWeqDrLI.htm](menace-under-otari-bestiary-items/B9C716BDIWeqDrLI.htm)|Jaws|Fauces|modificada|
|[bA2QzO0CkyheAgub.htm](menace-under-otari-bestiary-items/bA2QzO0CkyheAgub.htm)|Claw|Garra|modificada|
|[BmhB6hl9Ivt5pnw6.htm](menace-under-otari-bestiary-items/BmhB6hl9Ivt5pnw6.htm)|Talon|Talon|modificada|
|[bz1ZQoFYo4soy8jN.htm](menace-under-otari-bestiary-items/bz1ZQoFYo4soy8jN.htm)|Darkvision|Visión en la oscuridad|modificada|
|[C4dwFfQQc0M5gZBd.htm](menace-under-otari-bestiary-items/C4dwFfQQc0M5gZBd.htm)|Fangs|Colmillos|modificada|
|[C77jgOGQyRA9OepX.htm](menace-under-otari-bestiary-items/C77jgOGQyRA9OepX.htm)|Claw|Garra|modificada|
|[CellesdPVQW5Dk8g.htm](menace-under-otari-bestiary-items/CellesdPVQW5Dk8g.htm)|Ferocity|Ferocidad|modificada|
|[CfzB5r8CSHETuXfA.htm](menace-under-otari-bestiary-items/CfzB5r8CSHETuXfA.htm)|Site Bound|Ligado a una ubicación|modificada|
|[CHS0DAe3ekrw8ve1.htm](menace-under-otari-bestiary-items/CHS0DAe3ekrw8ve1.htm)|Shortbow|Arco corto|modificada|
|[CnmgzGmi2Avq67QA.htm](menace-under-otari-bestiary-items/CnmgzGmi2Avq67QA.htm)|Shortsword|Espada corta|modificada|
|[cprgu1LyMWRqzI0w.htm](menace-under-otari-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|Garra|modificada|
|[czwGWW73rkPW7vBg.htm](menace-under-otari-bestiary-items/czwGWW73rkPW7vBg.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[D0BJOT03V2FE7mKT.htm](menace-under-otari-bestiary-items/D0BJOT03V2FE7mKT.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[d5UuF56WuHzskPO4.htm](menace-under-otari-bestiary-items/d5UuF56WuHzskPO4.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[dB4Pr3yiNKH32fE8.htm](menace-under-otari-bestiary-items/dB4Pr3yiNKH32fE8.htm)|Knockdown|Derribo|modificada|
|[dBX2IpwGpcj3w0iE.htm](menace-under-otari-bestiary-items/dBX2IpwGpcj3w0iE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dF7ibA1On3Nvjxqk.htm](menace-under-otari-bestiary-items/dF7ibA1On3Nvjxqk.htm)|Hurried Retreat|Retirada apresurada|modificada|
|[dKlUXGzoOpojAKGv.htm](menace-under-otari-bestiary-items/dKlUXGzoOpojAKGv.htm)|Spear|Lanza|modificada|
|[Dkveyw8m1JIyOZch.htm](menace-under-otari-bestiary-items/Dkveyw8m1JIyOZch.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DmNGJHLmXcMGdmUS.htm](menace-under-otari-bestiary-items/DmNGJHLmXcMGdmUS.htm)|Battle Axe|Hacha de batalla|modificada|
|[dp1n1eKrdXWBGI47.htm](menace-under-otari-bestiary-items/dp1n1eKrdXWBGI47.htm)|Jaws|Fauces|modificada|
|[DqJFXur9S0e0C54j.htm](menace-under-otari-bestiary-items/DqJFXur9S0e0C54j.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DsvyLX53TeuZHMQ3.htm](menace-under-otari-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|Pies con garras|modificada|
|[dvuK2slsg05eHdpZ.htm](menace-under-otari-bestiary-items/dvuK2slsg05eHdpZ.htm)|Fist|Puño|modificada|
|[DW4EvhApIul7gVaU.htm](menace-under-otari-bestiary-items/DW4EvhApIul7gVaU.htm)|Tail|Tail|modificada|
|[DyABhSgZNJ3ICo85.htm](menace-under-otari-bestiary-items/DyABhSgZNJ3ICo85.htm)|Spear|Lanza|modificada|
|[e8BUlHLevzRJnHEc.htm](menace-under-otari-bestiary-items/e8BUlHLevzRJnHEc.htm)|Bushwhack|Emboscar|modificada|
|[EajnVKV3NKyICaQ8.htm](menace-under-otari-bestiary-items/EajnVKV3NKyICaQ8.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[EC797WDulwqw96ch.htm](menace-under-otari-bestiary-items/EC797WDulwqw96ch.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Eejxi0k01WzbvUUh.htm](menace-under-otari-bestiary-items/Eejxi0k01WzbvUUh.htm)|Shield Block|Bloquear con escudo|modificada|
|[ELWu5zIODys6k1lo.htm](menace-under-otari-bestiary-items/ELWu5zIODys6k1lo.htm)|Web|Telara|modificada|
|[emPcqeSFXdyr6CgV.htm](menace-under-otari-bestiary-items/emPcqeSFXdyr6CgV.htm)|Terrifying Charge|Carga terrorífica|modificada|
|[eulyI60JHNUYs39w.htm](menace-under-otari-bestiary-items/eulyI60JHNUYs39w.htm)|Slow|Lentificado/a|modificada|
|[EzSKWtWM2tSTtZwA.htm](menace-under-otari-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|Garra|modificada|
|[f4rcDncEwu2OdC1a.htm](menace-under-otari-bestiary-items/f4rcDncEwu2OdC1a.htm)|Jaws|Fauces|modificada|
|[Fc10tuSKXi4OAWw6.htm](menace-under-otari-bestiary-items/Fc10tuSKXi4OAWw6.htm)|Deep Plunge|Zambullida profunda|modificada|
|[fI8bsqTQlNTlSpdF.htm](menace-under-otari-bestiary-items/fI8bsqTQlNTlSpdF.htm)|Maul|Zarpazo doble|modificada|
|[fIAhNhYo8hJML5xU.htm](menace-under-otari-bestiary-items/fIAhNhYo8hJML5xU.htm)|Mind Reading|Leer la mente|modificada|
|[FMmsMdd150OLekUM.htm](menace-under-otari-bestiary-items/FMmsMdd150OLekUM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[freYXZtmDhD5zIrA.htm](menace-under-otari-bestiary-items/freYXZtmDhD5zIrA.htm)|Javelin|Javelin|modificada|
|[gCm9tVCYX0lSCKOq.htm](menace-under-otari-bestiary-items/gCm9tVCYX0lSCKOq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gEfOwadNLpPfyfQ8.htm](menace-under-otari-bestiary-items/gEfOwadNLpPfyfQ8.htm)|Claw|Garra|modificada|
|[GhnXpFtWM1vyvu8V.htm](menace-under-otari-bestiary-items/GhnXpFtWM1vyvu8V.htm)|Construct Armor|Construir Armadura|modificada|
|[GhRRqxvK6T21zheo.htm](menace-under-otari-bestiary-items/GhRRqxvK6T21zheo.htm)|Pack Attack|Ataque en manada|modificada|
|[gjrtuvnDxfzWNWcA.htm](menace-under-otari-bestiary-items/gjrtuvnDxfzWNWcA.htm)|Darkvision|Visión en la oscuridad|modificada|
|[GsQeeSV97T6FPvoY.htm](menace-under-otari-bestiary-items/GsQeeSV97T6FPvoY.htm)|Breath Weapon|Breath Weapon|modificada|
|[gV8plzH46OeVvlNG.htm](menace-under-otari-bestiary-items/gV8plzH46OeVvlNG.htm)|Stench|Hedor|modificada|
|[gwswBzTnckFDYGZR.htm](menace-under-otari-bestiary-items/gwswBzTnckFDYGZR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HAaABYYDbsq8LbrU.htm](menace-under-otari-bestiary-items/HAaABYYDbsq8LbrU.htm)|Jaws|Fauces|modificada|
|[haWc7Vo7IDgvIPU9.htm](menace-under-otari-bestiary-items/haWc7Vo7IDgvIPU9.htm)|No MAP|No MAP|modificada|
|[hDYxH5VuENin7Q0I.htm](menace-under-otari-bestiary-items/hDYxH5VuENin7Q0I.htm)|Crystal Sense 60 feet|Sentir cristales 60 pies|modificada|
|[hnjL5DfkOGkcdjPb.htm](menace-under-otari-bestiary-items/hnjL5DfkOGkcdjPb.htm)|Cleric Spells|Cleric Spells|modificada|
|[hQCxv2A5wNHVDzkj.htm](menace-under-otari-bestiary-items/hQCxv2A5wNHVDzkj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[HvOzFzF3j47WTzIv.htm](menace-under-otari-bestiary-items/HvOzFzF3j47WTzIv.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[I6lxEQY0rHjkqhZ1.htm](menace-under-otari-bestiary-items/I6lxEQY0rHjkqhZ1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[iF6CI9twYSN48Gfj.htm](menace-under-otari-bestiary-items/iF6CI9twYSN48Gfj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IgVFPfZjnGqYSVAI.htm](menace-under-otari-bestiary-items/IgVFPfZjnGqYSVAI.htm)|Animal Talker|El que habla con los animales|modificada|
|[iHa81hIBKMF5eXxi.htm](menace-under-otari-bestiary-items/iHa81hIBKMF5eXxi.htm)|Fangs|Colmillos|modificada|
|[iTaObjgnXpF66Sz5.htm](menace-under-otari-bestiary-items/iTaObjgnXpF66Sz5.htm)|Rapier|Estoque|modificada|
|[iuFRjKw2PLJl3E3F.htm](menace-under-otari-bestiary-items/iuFRjKw2PLJl3E3F.htm)|Pounce|Abalanzarse|modificada|
|[IvEp0FjtG1bxA4Xw.htm](menace-under-otari-bestiary-items/IvEp0FjtG1bxA4Xw.htm)|Jaws|Fauces|modificada|
|[ix1KQX8Ji2g1e9ID.htm](menace-under-otari-bestiary-items/ix1KQX8Ji2g1e9ID.htm)|Skewer|Ensartar|modificada|
|[J7jljAzKdEbTuHtB.htm](menace-under-otari-bestiary-items/J7jljAzKdEbTuHtB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[j90G6J5zvaIVNb1q.htm](menace-under-otari-bestiary-items/j90G6J5zvaIVNb1q.htm)|Breath Weapon|Breath Weapon|modificada|
|[JaJLITtJG6QOyapG.htm](menace-under-otari-bestiary-items/JaJLITtJG6QOyapG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[jAZY8WC1BtImPyEF.htm](menace-under-otari-bestiary-items/jAZY8WC1BtImPyEF.htm)|Armored Fist|Puño Blindado|modificada|
|[jL9PDQ27Dz6UAMOH.htm](menace-under-otari-bestiary-items/jL9PDQ27Dz6UAMOH.htm)|Scimitar|Cimitarra|modificada|
|[jmPDVPebOQ3XVOwA.htm](menace-under-otari-bestiary-items/jmPDVPebOQ3XVOwA.htm)|Boar Charge|Carga de jabalí|modificada|
|[jvZUoPLS8OA1SQTw.htm](menace-under-otari-bestiary-items/jvZUoPLS8OA1SQTw.htm)|Shortsword|Espada corta|modificada|
|[JWlYLXxlI4IRYbeF.htm](menace-under-otari-bestiary-items/JWlYLXxlI4IRYbeF.htm)|Greataxe|Greataxe|modificada|
|[jzku4qcmz6H3dk9C.htm](menace-under-otari-bestiary-items/jzku4qcmz6H3dk9C.htm)|Torch|Antorcha|modificada|
|[kFgfAbXaJbI8otfZ.htm](menace-under-otari-bestiary-items/kFgfAbXaJbI8otfZ.htm)|Shortsword|Espada corta|modificada|
|[Kj1uqisygxPxuaC9.htm](menace-under-otari-bestiary-items/Kj1uqisygxPxuaC9.htm)|Spider Speak|Spider Speak|modificada|
|[Kjifw1jgDvfmrEGU.htm](menace-under-otari-bestiary-items/Kjifw1jgDvfmrEGU.htm)|Steal Shadow|Robar sombra|modificada|
|[kkKlT3cO51Wj2tz5.htm](menace-under-otari-bestiary-items/kkKlT3cO51Wj2tz5.htm)|Draconic Momentum|Impulso dracónico|modificada|
|[KlVVVsJad72RahoI.htm](menace-under-otari-bestiary-items/KlVVVsJad72RahoI.htm)|Javelin|Javelin|modificada|
|[kN3B1jnfTS7LsbUS.htm](menace-under-otari-bestiary-items/kN3B1jnfTS7LsbUS.htm)|Javelin|Javelin|modificada|
|[Kpl3lLjfHM0hWhT5.htm](menace-under-otari-bestiary-items/Kpl3lLjfHM0hWhT5.htm)|Final Spite|Final Spite|modificada|
|[ksGHYkwsnnxc2Vmk.htm](menace-under-otari-bestiary-items/ksGHYkwsnnxc2Vmk.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KYq91Bu5HDnQoSkG.htm](menace-under-otari-bestiary-items/KYq91Bu5HDnQoSkG.htm)|Club|Club|modificada|
|[L4dKqPtdmyspmZtd.htm](menace-under-otari-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|Fauces|modificada|
|[l4eldMZrkZle4K3Y.htm](menace-under-otari-bestiary-items/l4eldMZrkZle4K3Y.htm)|Horns|Cuernos|modificada|
|[l6oPZE6Gcn9lJVoU.htm](menace-under-otari-bestiary-items/l6oPZE6Gcn9lJVoU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[L7wqc9Ph3bPMKfRq.htm](menace-under-otari-bestiary-items/L7wqc9Ph3bPMKfRq.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[LIdmD7VqztundfTh.htm](menace-under-otari-bestiary-items/LIdmD7VqztundfTh.htm)|Beak|Beak|modificada|
|[LJKNy957UOv4LqE5.htm](menace-under-otari-bestiary-items/LJKNy957UOv4LqE5.htm)|Change Shape|Change Shape|modificada|
|[lOUQcokkfOtGgPNG.htm](menace-under-otari-bestiary-items/lOUQcokkfOtGgPNG.htm)|Jaws|Fauces|modificada|
|[LPBpvhfnfUxz7nWz.htm](menace-under-otari-bestiary-items/LPBpvhfnfUxz7nWz.htm)|Scythe|Guadaña|modificada|
|[lqMgsu5wZAOsEwK2.htm](menace-under-otari-bestiary-items/lqMgsu5wZAOsEwK2.htm)|Giant Viper Venom|Veneno de Víbora Gigante|modificada|
|[lRaDhQKMgeR6IjAB.htm](menace-under-otari-bestiary-items/lRaDhQKMgeR6IjAB.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[lRYUK2abQpyhIlbv.htm](menace-under-otari-bestiary-items/lRYUK2abQpyhIlbv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LUPkzJMY0W8x0Vsd.htm](menace-under-otari-bestiary-items/LUPkzJMY0W8x0Vsd.htm)|Fist|Puño|modificada|
|[M3QwWIU2rzRHWndo.htm](menace-under-otari-bestiary-items/M3QwWIU2rzRHWndo.htm)|Mimic Object|Objeto Mímico|modificada|
|[Mcbgf9KdTTBPuio6.htm](menace-under-otari-bestiary-items/Mcbgf9KdTTBPuio6.htm)|Quick Trap|Trampa Rápida|modificada|
|[MCgl5uKpwtTh48E4.htm](menace-under-otari-bestiary-items/MCgl5uKpwtTh48E4.htm)|Longsword|Longsword|modificada|
|[mdqK7RuSjtezVyS8.htm](menace-under-otari-bestiary-items/mdqK7RuSjtezVyS8.htm)|Filth Wave|Ola de suciedad|modificada|
|[MEjtnkYdPhtWGyzF.htm](menace-under-otari-bestiary-items/MEjtnkYdPhtWGyzF.htm)|Coiled Opportunity|Oportunidad Retorcer|modificada|
|[MfBsuZaAMldux4Xb.htm](menace-under-otari-bestiary-items/MfBsuZaAMldux4Xb.htm)|Javelin|Javelin|modificada|
|[mHS7Uj0GKGKhQiNo.htm](menace-under-otari-bestiary-items/mHS7Uj0GKGKhQiNo.htm)|Battle Axe|Hacha de batalla|modificada|
|[MO9W8NpBALoHHtTS.htm](menace-under-otari-bestiary-items/MO9W8NpBALoHHtTS.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[MrC7xvsyL1EpWyqb.htm](menace-under-otari-bestiary-items/MrC7xvsyL1EpWyqb.htm)|Falling Scythes|Guadañas Caídas|modificada|
|[naotn6A029XPnJc1.htm](menace-under-otari-bestiary-items/naotn6A029XPnJc1.htm)|Mauler|Vapulear|modificada|
|[Nl15bkRTLq869IPi.htm](menace-under-otari-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|Petrificación de reojo|modificada|
|[nQAtVtNKfvmpm1T4.htm](menace-under-otari-bestiary-items/nQAtVtNKfvmpm1T4.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NQF0fD75864Ytav1.htm](menace-under-otari-bestiary-items/NQF0fD75864Ytav1.htm)|Captivating Song|Canción cautivadora|modificada|
|[Ns2pok7e6UtuKUd6.htm](menace-under-otari-bestiary-items/Ns2pok7e6UtuKUd6.htm)|Object Lesson|Lección práctica|modificada|
|[NS7yHszQCqQS0Bfj.htm](menace-under-otari-bestiary-items/NS7yHszQCqQS0Bfj.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[nWtiD4ASp2FFsobD.htm](menace-under-otari-bestiary-items/nWtiD4ASp2FFsobD.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[NX0owCPUZbsVJ4az.htm](menace-under-otari-bestiary-items/NX0owCPUZbsVJ4az.htm)|Jaws|Fauces|modificada|
|[Odl9UZ1OKOYG044E.htm](menace-under-otari-bestiary-items/Odl9UZ1OKOYG044E.htm)|Descend on a Web|Descender por una telara|modificada|
|[OgkLVLhW0uOMdbGj.htm](menace-under-otari-bestiary-items/OgkLVLhW0uOMdbGj.htm)|Formation|Formación|modificada|
|[OmVVuah2jgqtqVVo.htm](menace-under-otari-bestiary-items/OmVVuah2jgqtqVVo.htm)|Shortsword|Espada corta|modificada|
|[Onifi8y9OVLqK3Ea.htm](menace-under-otari-bestiary-items/Onifi8y9OVLqK3Ea.htm)|Ferocity|Ferocidad|modificada|
|[oqGxkGNFqdTqq3p0.htm](menace-under-otari-bestiary-items/oqGxkGNFqdTqq3p0.htm)|Darkvision|Visión en la oscuridad|modificada|
|[otqsXx9gjh7rnOpU.htm](menace-under-otari-bestiary-items/otqsXx9gjh7rnOpU.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[OWlPaR3WuuSoTaGl.htm](menace-under-otari-bestiary-items/OWlPaR3WuuSoTaGl.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[p1OayYNJswICCP7g.htm](menace-under-otari-bestiary-items/p1OayYNJswICCP7g.htm)|Jaws|Fauces|modificada|
|[p27dLxTbXpewe8ZR.htm](menace-under-otari-bestiary-items/p27dLxTbXpewe8ZR.htm)|Spear|Lanza|modificada|
|[P7JMDrm2HB1QPV3N.htm](menace-under-otari-bestiary-items/P7JMDrm2HB1QPV3N.htm)|Web Lurker Venom|Veneno del merodeador de telara.|modificada|
|[pChhLSd048EpUiL0.htm](menace-under-otari-bestiary-items/pChhLSd048EpUiL0.htm)|Fangs|Colmillos|modificada|
|[PIjZTZsbQlriq5X3.htm](menace-under-otari-bestiary-items/PIjZTZsbQlriq5X3.htm)|Giant Centipede Venom|Veneno de ciempiés gigante|modificada|
|[puh52kwYDM4gZi1Z.htm](menace-under-otari-bestiary-items/puh52kwYDM4gZi1Z.htm)|Club|Club|modificada|
|[PzpzmSnCSWrwxzDO.htm](menace-under-otari-bestiary-items/PzpzmSnCSWrwxzDO.htm)|Longsword|Longsword|modificada|
|[Q5WTbR9lUXHMagS8.htm](menace-under-otari-bestiary-items/Q5WTbR9lUXHMagS8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qCmKPruRObzVL1Dr.htm](menace-under-otari-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|Avance chirriante|modificada|
|[qG3VBB7HAWZksxvT.htm](menace-under-otari-bestiary-items/qG3VBB7HAWZksxvT.htm)|Falling Block|Bloqueo Caída|modificada|
|[qhS5ZEJcqNFytqWd.htm](menace-under-otari-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|Fauces|modificada|
|[qjYI8SCJrXWZopn6.htm](menace-under-otari-bestiary-items/qjYI8SCJrXWZopn6.htm)|Greataxe|Greataxe|modificada|
|[Qknp3UNQSMjNTUmL.htm](menace-under-otari-bestiary-items/Qknp3UNQSMjNTUmL.htm)|Grab|Agarrado|modificada|
|[QpOsIDOXtvTTXsS8.htm](menace-under-otari-bestiary-items/QpOsIDOXtvTTXsS8.htm)|Staff|Báculo|modificada|
|[R1hhANtemlX6gt4D.htm](menace-under-otari-bestiary-items/R1hhANtemlX6gt4D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[r5yruYZzaDx9MoHR.htm](menace-under-otari-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[rA69Ao7ZsTSnYPbJ.htm](menace-under-otari-bestiary-items/rA69Ao7ZsTSnYPbJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RDDSANqANJv09ktQ.htm](menace-under-otari-bestiary-items/RDDSANqANJv09ktQ.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[rGgLU8CkJNTv7kLC.htm](menace-under-otari-bestiary-items/rGgLU8CkJNTv7kLC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rj9VxT0ANFzZgmXI.htm](menace-under-otari-bestiary-items/rj9VxT0ANFzZgmXI.htm)|Evil Damage|Daño maligno|modificada|
|[rs9VqajU6hTlLt5o.htm](menace-under-otari-bestiary-items/rs9VqajU6hTlLt5o.htm)|Darkvision|Visión en la oscuridad|modificada|
|[rvraBop9oOSwcjkq.htm](menace-under-otari-bestiary-items/rvraBop9oOSwcjkq.htm)|Swift Leap|Salto veloz|modificada|
|[RX6HAClLi39MgLLI.htm](menace-under-otari-bestiary-items/RX6HAClLi39MgLLI.htm)|Swipe|Vaivén|modificada|
|[RZBxog3E2iCbQY5r.htm](menace-under-otari-bestiary-items/RZBxog3E2iCbQY5r.htm)|Dagger|Daga|modificada|
|[scw9HnbCVwAyFzok.htm](menace-under-otari-bestiary-items/scw9HnbCVwAyFzok.htm)|Rend|Rasgadura|modificada|
|[SKxww784aC4E2gzW.htm](menace-under-otari-bestiary-items/SKxww784aC4E2gzW.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[SL9owkerqt7KHiAD.htm](menace-under-otari-bestiary-items/SL9owkerqt7KHiAD.htm)|Spear|Lanza|modificada|
|[slCMgd2stlwxBKdg.htm](menace-under-otari-bestiary-items/slCMgd2stlwxBKdg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[SNb64KWDBMrTorTI.htm](menace-under-otari-bestiary-items/SNb64KWDBMrTorTI.htm)|Jaws|Fauces|modificada|
|[soQbbQbVd6xMuJl6.htm](menace-under-otari-bestiary-items/soQbbQbVd6xMuJl6.htm)|Incorporeal|Incorporeal|modificada|
|[sqceAJWlpdKGnHvx.htm](menace-under-otari-bestiary-items/sqceAJWlpdKGnHvx.htm)|Talon|Talon|modificada|
|[sXZ80MEcWSNN1Ct0.htm](menace-under-otari-bestiary-items/sXZ80MEcWSNN1Ct0.htm)|Spike Trap|Trampa de pinchos|modificada|
|[SyWmilYaLnrtDFVe.htm](menace-under-otari-bestiary-items/SyWmilYaLnrtDFVe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sz9GGQOjyrluDIX3.htm](menace-under-otari-bestiary-items/sz9GGQOjyrluDIX3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[T6CjPHrd4XMeWpiE.htm](menace-under-otari-bestiary-items/T6CjPHrd4XMeWpiE.htm)|Darkvision|Visión en la oscuridad|modificada|
|[tC9lSVhNyxDRwaml.htm](menace-under-otari-bestiary-items/tC9lSVhNyxDRwaml.htm)|Wizard Spells|Hechizos de Mago|modificada|
|[TCZQdv29obsz4Rie.htm](menace-under-otari-bestiary-items/TCZQdv29obsz4Rie.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[TnaXi3v3iBDjotwd.htm](menace-under-otari-bestiary-items/TnaXi3v3iBDjotwd.htm)|Viper Venom|Viper Venom|modificada|
|[tOCS9mkYR5K1LuOZ.htm](menace-under-otari-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|Mirada petrificante|modificada|
|[TS7iVUZNmP0Szdfh.htm](menace-under-otari-bestiary-items/TS7iVUZNmP0Szdfh.htm)|Javelin|Javelin|modificada|
|[tS9Kv6K4kwYFjHCT.htm](menace-under-otari-bestiary-items/tS9Kv6K4kwYFjHCT.htm)|Incorporeal|Incorporeal|modificada|
|[TyHmjC5wvxo29J5L.htm](menace-under-otari-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|Agarrado|modificada|
|[U7gmP0tQ6eQor3xT.htm](menace-under-otari-bestiary-items/U7gmP0tQ6eQor3xT.htm)|Fist|Puño|modificada|
|[u9tY2pkbZxNzmxoe.htm](menace-under-otari-bestiary-items/u9tY2pkbZxNzmxoe.htm)|Claw|Garra|modificada|
|[uEjliLhQWBsVYCsE.htm](menace-under-otari-bestiary-items/uEjliLhQWBsVYCsE.htm)|Maul|Zarpazo doble|modificada|
|[ufap97U4FyUOrJZF.htm](menace-under-otari-bestiary-items/ufap97U4FyUOrJZF.htm)|Light Blindness|Ceguera ante la luz|modificada|
|[uYQrEbjjw3XIQ1cO.htm](menace-under-otari-bestiary-items/uYQrEbjjw3XIQ1cO.htm)|Frightful Moan|Frightful Moan|modificada|
|[uznmLfSkP5j9qOEA.htm](menace-under-otari-bestiary-items/uznmLfSkP5j9qOEA.htm)|Quick Draw|Desenvainado rápido|modificada|
|[v2Ifd6tb1IDRFqFO.htm](menace-under-otari-bestiary-items/v2Ifd6tb1IDRFqFO.htm)|Drain Life|Drenar Vida|modificada|
|[v90WRjELLiKF57vr.htm](menace-under-otari-bestiary-items/v90WRjELLiKF57vr.htm)|Jaws|Fauces|modificada|
|[v9Xt73Wj9wclc3ef.htm](menace-under-otari-bestiary-items/v9Xt73Wj9wclc3ef.htm)|Javelin|Javelin|modificada|
|[V9zIFbIReO27zwGu.htm](menace-under-otari-bestiary-items/V9zIFbIReO27zwGu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VcMwjaxqor5ucwyE.htm](menace-under-otari-bestiary-items/VcMwjaxqor5ucwyE.htm)|Javelin|Javelin|modificada|
|[VDL4WflLI1gxDylZ.htm](menace-under-otari-bestiary-items/VDL4WflLI1gxDylZ.htm)|Spine|Spine|modificada|
|[VeqnECTeAGCW8urK.htm](menace-under-otari-bestiary-items/VeqnECTeAGCW8urK.htm)|Low-Light Vision|Visión con poca luz|modificada|
|[vhcrEIRKkfTwrwDs.htm](menace-under-otari-bestiary-items/vhcrEIRKkfTwrwDs.htm)|Fetid Fumes|Vapores fétidos|modificada|
|[vTjkwNzpCJmsbg6R.htm](menace-under-otari-bestiary-items/vTjkwNzpCJmsbg6R.htm)|Shortsword|Espada corta|modificada|
|[vTXaShzYmcNbRjzk.htm](menace-under-otari-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|Bloodcurdling Screech|modificada|
|[VwrlDdZczBGO5Ps4.htm](menace-under-otari-bestiary-items/VwrlDdZczBGO5Ps4.htm)|Motion Sense 60 feet|Sentido del movimiento 60 pies|modificada|
|[WbJQr5XhkIENzNfG.htm](menace-under-otari-bestiary-items/WbJQr5XhkIENzNfG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wBppFKxjdSadJQj2.htm](menace-under-otari-bestiary-items/wBppFKxjdSadJQj2.htm)|Shortbow|Arco corto|modificada|
|[wEVWlNgGdVwq51Y9.htm](menace-under-otari-bestiary-items/wEVWlNgGdVwq51Y9.htm)|Claw|Garra|modificada|
|[WjBhdxKcekKdof75.htm](menace-under-otari-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|Visión en la oscuridad|modificada|
|[wOkHQN8ELSDh7T1f.htm](menace-under-otari-bestiary-items/wOkHQN8ELSDh7T1f.htm)|Fist|Puño|modificada|
|[WpW23oqzbYlPDDuP.htm](menace-under-otari-bestiary-items/WpW23oqzbYlPDDuP.htm)|Slam Shut|Slam Shut|modificada|
|[wqhHoIIgE3jfAgCx.htm](menace-under-otari-bestiary-items/wqhHoIIgE3jfAgCx.htm)|Smoke Vision|Visión de Humo|modificada|
|[x47iDfIE9albveGl.htm](menace-under-otari-bestiary-items/x47iDfIE9albveGl.htm)|Shadow Hand|Mano de Sombra|modificada|
|[x8uwJOVGskq8pjOH.htm](menace-under-otari-bestiary-items/x8uwJOVGskq8pjOH.htm)|Circling Attack|Ataque circular|modificada|
|[xa524Pmx1LWbJGLL.htm](menace-under-otari-bestiary-items/xa524Pmx1LWbJGLL.htm)|Wizard Spells|Hechizos de Mago|modificada|
|[XDq62TH6LQA4rgny.htm](menace-under-otari-bestiary-items/XDq62TH6LQA4rgny.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[xDUpwWQaZevQOkYQ.htm](menace-under-otari-bestiary-items/xDUpwWQaZevQOkYQ.htm)|Ferocity|Ferocidad|modificada|
|[XJHXZOImLOORwInT.htm](menace-under-otari-bestiary-items/XJHXZOImLOORwInT.htm)|Jaws|Fauces|modificada|
|[xN6zu0iwyGGunmVu.htm](menace-under-otari-bestiary-items/xN6zu0iwyGGunmVu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[xolLzAgsgpzS6Nvn.htm](menace-under-otari-bestiary-items/xolLzAgsgpzS6Nvn.htm)|Shield Block|Bloquear con escudo|modificada|
|[XPgz4pQn9aYVvddX.htm](menace-under-otari-bestiary-items/XPgz4pQn9aYVvddX.htm)|Hand Crossbow|Ballesta de mano|modificada|
|[XqSpTVGRCbtpPfJU.htm](menace-under-otari-bestiary-items/XqSpTVGRCbtpPfJU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[YdD2kpKW2TNfcupL.htm](menace-under-otari-bestiary-items/YdD2kpKW2TNfcupL.htm)|Goblin Scuttle|Paso rápido de goblin|modificada|
|[yDIuApGqKF4695Oq.htm](menace-under-otari-bestiary-items/yDIuApGqKF4695Oq.htm)|Spear|Lanza|modificada|
|[yDzPXpPBRfYG5UfY.htm](menace-under-otari-bestiary-items/yDzPXpPBRfYG5UfY.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[yeHohyJSYfiTwghO.htm](menace-under-otari-bestiary-items/yeHohyJSYfiTwghO.htm)|Battle Cry|Grito de guerra|modificada|
|[YfiKyZzHH0XtfjFx.htm](menace-under-otari-bestiary-items/YfiKyZzHH0XtfjFx.htm)|Spray|Spray|modificada|
|[yfZjkf1LqlK3U7HM.htm](menace-under-otari-bestiary-items/yfZjkf1LqlK3U7HM.htm)|Tusk|Tusk|modificada|
|[YIHoSZN2nTp139Dg.htm](menace-under-otari-bestiary-items/YIHoSZN2nTp139Dg.htm)|Slink|Slink|modificada|
|[yLnwKltbJHYpn8qC.htm](menace-under-otari-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|Fauces|modificada|
|[YXsM6AcguDWAwVIt.htm](menace-under-otari-bestiary-items/YXsM6AcguDWAwVIt.htm)|Pseudopod|Pseudópodo|modificada|
|[YZxhBg9R5m8ydWm1.htm](menace-under-otari-bestiary-items/YZxhBg9R5m8ydWm1.htm)|Claw|Garra|modificada|
|[Zdg1lW7CYLRDYorG.htm](menace-under-otari-bestiary-items/Zdg1lW7CYLRDYorG.htm)|Claw|Garra|modificada|
|[Zh4UT6i2zsYug3c7.htm](menace-under-otari-bestiary-items/Zh4UT6i2zsYug3c7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZTPzGQcaC9zUdjD6.htm](menace-under-otari-bestiary-items/ZTPzGQcaC9zUdjD6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ZY8Teu6y1ruEGOAn.htm](menace-under-otari-bestiary-items/ZY8Teu6y1ruEGOAn.htm)|Pitfall|Trampa de caída|modificada|
|[zyYihJ8eYGtYRh9u.htm](menace-under-otari-bestiary-items/zyYihJ8eYGtYRh9u.htm)|Longsword|Longsword|modificada|
|[zZ7YQvZHZDePNYfh.htm](menace-under-otari-bestiary-items/zZ7YQvZHZDePNYfh.htm)|Darkvision|Visión en la oscuridad|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4gDOqdD5endao3AS.htm](menace-under-otari-bestiary-items/4gDOqdD5endao3AS.htm)|Survival|vacía|
|[eh06w4tCybKWTx8g.htm](menace-under-otari-bestiary-items/eh06w4tCybKWTx8g.htm)|Eggshell Necklace|vacía|
|[eU0KcVkea4Q9goDX.htm](menace-under-otari-bestiary-items/eU0KcVkea4Q9goDX.htm)|Dragon Lore|vacía|
|[FRce1Q7000LFf9Gs.htm](menace-under-otari-bestiary-items/FRce1Q7000LFf9Gs.htm)|Iron Key|vacía|
|[GRUZiIR1Uu91DaoY.htm](menace-under-otari-bestiary-items/GRUZiIR1Uu91DaoY.htm)|Stealth|vacía|
|[iUzYuqnjkPvxjVq6.htm](menace-under-otari-bestiary-items/iUzYuqnjkPvxjVq6.htm)|Stealth|vacía|
|[kXXO2coSLlDjiCDu.htm](menace-under-otari-bestiary-items/kXXO2coSLlDjiCDu.htm)|Survival|vacía|
|[KYeYaMrxJZuncqJp.htm](menace-under-otari-bestiary-items/KYeYaMrxJZuncqJp.htm)|Dragon Lore|vacía|
|[pY7DRATVeNfGLzvD.htm](menace-under-otari-bestiary-items/pY7DRATVeNfGLzvD.htm)|Fire Lore|vacía|
|[UedMQrrYjW7RxjuG.htm](menace-under-otari-bestiary-items/UedMQrrYjW7RxjuG.htm)|Crafting|vacía|
|[vBFHfrDtlZ5vYw2q.htm](menace-under-otari-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|vacía|
|[XfysylreSHBRTKzu.htm](menace-under-otari-bestiary-items/XfysylreSHBRTKzu.htm)|Performance|vacía|
|[XiR8kBhdXvv7AE8L.htm](menace-under-otari-bestiary-items/XiR8kBhdXvv7AE8L.htm)|Athletics|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[KlwGH9hdWl3XpMYw.htm](menace-under-otari-bestiary-items/KlwGH9hdWl3XpMYw.htm)|Web Trap|Trampa de telaraña (BB)|oficial|
|[Re5HgCWQJTyzGewL.htm](menace-under-otari-bestiary-items/Re5HgCWQJTyzGewL.htm)|Web Trap|Trampa de telaraña (BB)|oficial|
|[vnaySgTDfC0slaHW.htm](menace-under-otari-bestiary-items/vnaySgTDfC0slaHW.htm)|Giant Spider Venom|Veneno de araña gigante|oficial|
