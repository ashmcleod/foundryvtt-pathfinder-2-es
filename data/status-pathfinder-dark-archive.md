# Estado de la traducción (pathfinder-dark-archive)

 * **vacía**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[Z9iID4f6DoEuVrLo.htm](pathfinder-dark-archive/Z9iID4f6DoEuVrLo.htm)|Thalassophobic Pool|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
