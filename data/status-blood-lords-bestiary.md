# Estado de la traducción (blood-lords-bestiary)

 * **vacía**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[ZzRnrcSstcvJx5Dg.htm](blood-lords-bestiary/ZzRnrcSstcvJx5Dg.htm)|Keystone Trap|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
