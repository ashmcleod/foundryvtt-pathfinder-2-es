# Estado de la traducción (outlaws-of-alkenstar-bestiary-items)

 * **vacía**: 4
 * **ninguna**: 7


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[4Vwu7Dx2GieS3Xns.htm](outlaws-of-alkenstar-bestiary-items/4Vwu7Dx2GieS3Xns.htm)|Belly Hatch|
|[akcOBJE7mzPgMkLb.htm](outlaws-of-alkenstar-bestiary-items/akcOBJE7mzPgMkLb.htm)|Gallop|
|[dPRbLumFIIG4T8Kl.htm](outlaws-of-alkenstar-bestiary-items/dPRbLumFIIG4T8Kl.htm)|Pick|
|[hiVEdBnoIdnSqV56.htm](outlaws-of-alkenstar-bestiary-items/hiVEdBnoIdnSqV56.htm)|Shift Form|
|[MdOgmdwFOpOboMdg.htm](outlaws-of-alkenstar-bestiary-items/MdOgmdwFOpOboMdg.htm)|Smog Cloud|
|[NEvPr09cnSEWEzjL.htm](outlaws-of-alkenstar-bestiary-items/NEvPr09cnSEWEzjL.htm)|Darkvision|
|[xTbmqvj8q9NXpTNJ.htm](outlaws-of-alkenstar-bestiary-items/xTbmqvj8q9NXpTNJ.htm)|Attack of Opportunity|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[255aEl1cZIFN1lnC.htm](outlaws-of-alkenstar-bestiary-items/255aEl1cZIFN1lnC.htm)|Hoof|vacía|
|[lr40eSk84v9ETRi6.htm](outlaws-of-alkenstar-bestiary-items/lr40eSk84v9ETRi6.htm)|Mining Lore|vacía|
|[RvHiGkpbgGQQVSfo.htm](outlaws-of-alkenstar-bestiary-items/RvHiGkpbgGQQVSfo.htm)|Claw|vacía|
|[vW3X6iTnWa52cj7M.htm](outlaws-of-alkenstar-bestiary-items/vW3X6iTnWa52cj7M.htm)|Pick|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
