# Estado de la traducción (book-of-the-dead-bestiary-items)

 * **modificada**: 822
 * **ninguna**: 62
 * **vacía**: 39


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0FiwKIU8kAmjUnyd.htm](book-of-the-dead-bestiary-items/0FiwKIU8kAmjUnyd.htm)|Rapier|+1,striking|
|[0PYT6BjBxO2OCUr4.htm](book-of-the-dead-bestiary-items/0PYT6BjBxO2OCUr4.htm)|Battle Axe|+2,greaterStriking,grievous|
|[1t9Hd470CiyR1VXm.htm](book-of-the-dead-bestiary-items/1t9Hd470CiyR1VXm.htm)|Command (Animals Only)|
|[2ASu2r4ddMMqE7Y8.htm](book-of-the-dead-bestiary-items/2ASu2r4ddMMqE7Y8.htm)|Religious Symbol of Urgathoa|
|[3DAs1iaOfMM4FE4c.htm](book-of-the-dead-bestiary-items/3DAs1iaOfMM4FE4c.htm)|Fiddle|
|[3rKCigKvwBeinC4a.htm](book-of-the-dead-bestiary-items/3rKCigKvwBeinC4a.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|
|[4DPXEcfNdnVqHXz2.htm](book-of-the-dead-bestiary-items/4DPXEcfNdnVqHXz2.htm)|Charm (Animals Only)|
|[4i9LmwMyEPvNox7y.htm](book-of-the-dead-bestiary-items/4i9LmwMyEPvNox7y.htm)|Scythe|+1|
|[4pMFJruJiXAxRCo4.htm](book-of-the-dead-bestiary-items/4pMFJruJiXAxRCo4.htm)|Air Walk (Constant)|
|[5wKokTxNYOw3NX8Y.htm](book-of-the-dead-bestiary-items/5wKokTxNYOw3NX8Y.htm)|Enthrall (At Will)|
|[8sm7vCUGufEzQ4YW.htm](book-of-the-dead-bestiary-items/8sm7vCUGufEzQ4YW.htm)|Defiled Religious Symbol of Kazutal|
|[AbEjyP0syA7mzlA8.htm](book-of-the-dead-bestiary-items/AbEjyP0syA7mzlA8.htm)|Air Walk (Constant)|
|[APlEPXARsPqzLzte.htm](book-of-the-dead-bestiary-items/APlEPXARsPqzLzte.htm)|Shuriken|+1,striking,returning|
|[awU8eYrcBF8rib52.htm](book-of-the-dead-bestiary-items/awU8eYrcBF8rib52.htm)|Invisibility (At Will)|
|[b2dqnLa8hK88Qb7g.htm](book-of-the-dead-bestiary-items/b2dqnLa8hK88Qb7g.htm)|Déjà Vu (At Will)|
|[b6SNymlSdtUpqtv0.htm](book-of-the-dead-bestiary-items/b6SNymlSdtUpqtv0.htm)|+2 Resilient Full Plate|
|[blRax867OLdVUMwk.htm](book-of-the-dead-bestiary-items/blRax867OLdVUMwk.htm)|Nature's Enmity (Only While Within the Area to Which the Bhuta is Bound)|
|[byft8BkxplVfvQqd.htm](book-of-the-dead-bestiary-items/byft8BkxplVfvQqd.htm)|Speak with Plants (Constant)|
|[Cvmy7uikSlZ9VNtN.htm](book-of-the-dead-bestiary-items/Cvmy7uikSlZ9VNtN.htm)|Spider Climb (Constant)|
|[dWDxhvABpzr9Iubs.htm](book-of-the-dead-bestiary-items/dWDxhvABpzr9Iubs.htm)|Mace|+1,striking|
|[DzKCI3ALLE9qQq79.htm](book-of-the-dead-bestiary-items/DzKCI3ALLE9qQq79.htm)|Shortbow|+1|
|[Fdv00C87iITzPRVA.htm](book-of-the-dead-bestiary-items/Fdv00C87iITzPRVA.htm)|Staff|+1|
|[fegyOi6VDq8sD5vj.htm](book-of-the-dead-bestiary-items/fegyOi6VDq8sD5vj.htm)|Wand of Dispel Magic (Level 6)|
|[fHKSsTCMOscHoG6D.htm](book-of-the-dead-bestiary-items/fHKSsTCMOscHoG6D.htm)|Speak with Animals (Constant)|
|[fN1JV4sjRyAtaSwd.htm](book-of-the-dead-bestiary-items/fN1JV4sjRyAtaSwd.htm)|Crossbow|+1,striking|
|[g3oCwdk0aWScgLBq.htm](book-of-the-dead-bestiary-items/g3oCwdk0aWScgLBq.htm)|Tongues (Constant)|
|[gdfTOvhcUidHz9fM.htm](book-of-the-dead-bestiary-items/gdfTOvhcUidHz9fM.htm)|Defiled Religious Symbol of Nethys|
|[gUJbIPYPpxPHfMRO.htm](book-of-the-dead-bestiary-items/gUJbIPYPpxPHfMRO.htm)|+1 Scale Mail|
|[H1YR8lmJxjQ8GKTf.htm](book-of-the-dead-bestiary-items/H1YR8lmJxjQ8GKTf.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|
|[HdJVXKg1EjumVlC0.htm](book-of-the-dead-bestiary-items/HdJVXKg1EjumVlC0.htm)|Dagger|+1,striking|
|[HPESx2nabKEHNgX2.htm](book-of-the-dead-bestiary-items/HPESx2nabKEHNgX2.htm)|Air Walk (Constant)|
|[kDcWkpiVlvQCKNaQ.htm](book-of-the-dead-bestiary-items/kDcWkpiVlvQCKNaQ.htm)|Religious Symbol of Set|
|[kgbHU2XWV7rcVSRL.htm](book-of-the-dead-bestiary-items/kgbHU2XWV7rcVSRL.htm)|Flute|
|[KSMsQZQvHWveKzzQ.htm](book-of-the-dead-bestiary-items/KSMsQZQvHWveKzzQ.htm)|Entangle (At-Will)|
|[kYeCwvGuYQYBi0jb.htm](book-of-the-dead-bestiary-items/kYeCwvGuYQYBi0jb.htm)|Tongues (Constant)|
|[lrHbm9UI1nZEYYkV.htm](book-of-the-dead-bestiary-items/lrHbm9UI1nZEYYkV.htm)|Enthrall (At Will)|
|[nvu6RAdwRrg2nEJ4.htm](book-of-the-dead-bestiary-items/nvu6RAdwRrg2nEJ4.htm)|+1 Resilient Full Plate|
|[Oh9OzqtV1OnzNNN3.htm](book-of-the-dead-bestiary-items/Oh9OzqtV1OnzNNN3.htm)|Haste (Self Only)|
|[PUNivyqo7fsbOn4U.htm](book-of-the-dead-bestiary-items/PUNivyqo7fsbOn4U.htm)|Blink (Constant)|
|[QNACTpWxBNKzhVKH.htm](book-of-the-dead-bestiary-items/QNACTpWxBNKzhVKH.htm)|Bag of Holding (Type 1)|
|[QOcBBYQLYIHAXHeX.htm](book-of-the-dead-bestiary-items/QOcBBYQLYIHAXHeX.htm)|Scroll of Teleport (Level 6)|
|[qU8ergQ43LOW1kIH.htm](book-of-the-dead-bestiary-items/qU8ergQ43LOW1kIH.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|
|[RM4jD8UptCfao2mw.htm](book-of-the-dead-bestiary-items/RM4jD8UptCfao2mw.htm)|Vampiric Exsanguination (Drawing in Moisture Rather than Blood)|
|[Rw4TQAjoro6E9MvZ.htm](book-of-the-dead-bestiary-items/Rw4TQAjoro6E9MvZ.htm)|Composite Longbow|+1|
|[SNMKCPwnj2TgJsVv.htm](book-of-the-dead-bestiary-items/SNMKCPwnj2TgJsVv.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|
|[TtXUPuadvachP5x2.htm](book-of-the-dead-bestiary-items/TtXUPuadvachP5x2.htm)|Spiked Chain|+2,striking|
|[uAiU3U0R80wN24HZ.htm](book-of-the-dead-bestiary-items/uAiU3U0R80wN24HZ.htm)|Visions of Danger (A Swarm of Sluagh Reapers)|
|[uNwNfEbvOaEcyhSd.htm](book-of-the-dead-bestiary-items/uNwNfEbvOaEcyhSd.htm)|Machete|+1,striking|
|[usz0U3YIXa4lehfb.htm](book-of-the-dead-bestiary-items/usz0U3YIXa4lehfb.htm)|Wand of Wall of Force (Level 6)|
|[uUZ8FSW9h8LqYa6l.htm](book-of-the-dead-bestiary-items/uUZ8FSW9h8LqYa6l.htm)|Flintlock Pistol|+1,striking|
|[uxhfyJ9MLgmRSKS9.htm](book-of-the-dead-bestiary-items/uxhfyJ9MLgmRSKS9.htm)|Darkness (At Will)|
|[vamz8Jb2yzFSh4rA.htm](book-of-the-dead-bestiary-items/vamz8Jb2yzFSh4rA.htm)|Dispel Magic (At Will)|
|[vwxfS6QSBbdY3B7C.htm](book-of-the-dead-bestiary-items/vwxfS6QSBbdY3B7C.htm)|Longsword|+1,striking|
|[WciUktftb9XKpzo5.htm](book-of-the-dead-bestiary-items/WciUktftb9XKpzo5.htm)|Religious Symbol of Urgathoa|
|[WI1TgZGTFtOzaOax.htm](book-of-the-dead-bestiary-items/WI1TgZGTFtOzaOax.htm)|Water Walk (Constant)|
|[WI4DJQA8o5zmepx3.htm](book-of-the-dead-bestiary-items/WI4DJQA8o5zmepx3.htm)|Spear|+1,striking|
|[WK7lEwsMK9Kmei2J.htm](book-of-the-dead-bestiary-items/WK7lEwsMK9Kmei2J.htm)|Freedom of Movement (Constant)|
|[X0l5lixcXAtLUYYZ.htm](book-of-the-dead-bestiary-items/X0l5lixcXAtLUYYZ.htm)|Chain Mail (Broken)|
|[xOyNtZslnRcM2xBf.htm](book-of-the-dead-bestiary-items/xOyNtZslnRcM2xBf.htm)|Staff|+1,striking|
|[YPP8yxLHuW6dBk3A.htm](book-of-the-dead-bestiary-items/YPP8yxLHuW6dBk3A.htm)|Darkness (At Will)|
|[yQiPlzwxHHKQB3D2.htm](book-of-the-dead-bestiary-items/yQiPlzwxHHKQB3D2.htm)|Scroll of True Seeing (Level 6)|
|[ZeX1y7UFISFL1frx.htm](book-of-the-dead-bestiary-items/ZeX1y7UFISFL1frx.htm)|Dwarven War Axe (Broken)|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[048p1dvtj51r50vq.htm](book-of-the-dead-bestiary-items/048p1dvtj51r50vq.htm)|Lignify|Lignify|modificada|
|[05F3xO5TlXZ9n8cg.htm](book-of-the-dead-bestiary-items/05F3xO5TlXZ9n8cg.htm)|Negative Healing|Curación negativa|modificada|
|[06rJwQpNQ1H9tOn3.htm](book-of-the-dead-bestiary-items/06rJwQpNQ1H9tOn3.htm)|Darkvision|Visión en la oscuridad|modificada|
|[09d8l407352ep4z4.htm](book-of-the-dead-bestiary-items/09d8l407352ep4z4.htm)|Claw|Garra|modificada|
|[0a0R5L9eHRL770lk.htm](book-of-the-dead-bestiary-items/0a0R5L9eHRL770lk.htm)|Breathsense (Precise) 60 feet|Sentido de la respiración (precisión) 60 pies.|modificada|
|[0AkVpzKeetsxnaM0.htm](book-of-the-dead-bestiary-items/0AkVpzKeetsxnaM0.htm)|Negative Healing|Curación negativa|modificada|
|[0aqvd9pAwLkOwZeo.htm](book-of-the-dead-bestiary-items/0aqvd9pAwLkOwZeo.htm)|Tortured Gaze|Mirada Torturada|modificada|
|[0cJtxKvPeAJC8lOc.htm](book-of-the-dead-bestiary-items/0cJtxKvPeAJC8lOc.htm)|Improved Grab|Agarrado mejorado|modificada|
|[0cQO5TZGYHxwoqee.htm](book-of-the-dead-bestiary-items/0cQO5TZGYHxwoqee.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[0e0uF4QWaN68KIlV.htm](book-of-the-dead-bestiary-items/0e0uF4QWaN68KIlV.htm)|Paralyzing Claws|Garras paralizantes|modificada|
|[0fo5e4nnx624my4t.htm](book-of-the-dead-bestiary-items/0fo5e4nnx624my4t.htm)|Weathering Aura|Aura de Intemperie|modificada|
|[0jGEzNSvC3mj1gAv.htm](book-of-the-dead-bestiary-items/0jGEzNSvC3mj1gAv.htm)|Swallow Whole|Engullir Todo|modificada|
|[0oBoPpJaPEder5Uj.htm](book-of-the-dead-bestiary-items/0oBoPpJaPEder5Uj.htm)|Scythe|Guadaña|modificada|
|[0VnHFn3wn3nNZ2Tp.htm](book-of-the-dead-bestiary-items/0VnHFn3wn3nNZ2Tp.htm)|Swallow Whole|Engullir Todo|modificada|
|[0xaci6r10kc1q6hz.htm](book-of-the-dead-bestiary-items/0xaci6r10kc1q6hz.htm)|Stunning Flurry|Ráfaga de golpes|modificada|
|[0yqQhSvO3kn23ESU.htm](book-of-the-dead-bestiary-items/0yqQhSvO3kn23ESU.htm)|Devastating Blast|Explosión devastadora|modificada|
|[0zj1icya9xc6t4m7.htm](book-of-the-dead-bestiary-items/0zj1icya9xc6t4m7.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[0zs0is6k2nndmc5h.htm](book-of-the-dead-bestiary-items/0zs0is6k2nndmc5h.htm)|Nerve Ending|Nerve Ending|modificada|
|[10pagf9v2vm39x4i.htm](book-of-the-dead-bestiary-items/10pagf9v2vm39x4i.htm)|Fist|Puño|modificada|
|[13h23z1r0zoaru7r.htm](book-of-the-dead-bestiary-items/13h23z1r0zoaru7r.htm)|Snatch|Arrebatar|modificada|
|[14ufiu26cnai5yer.htm](book-of-the-dead-bestiary-items/14ufiu26cnai5yer.htm)|Demolition|Demolición|modificada|
|[15ji0gz9dw3xxfog.htm](book-of-the-dead-bestiary-items/15ji0gz9dw3xxfog.htm)|Spray Black Bile|Spray Black Bile|modificada|
|[15qn60fnmbbmi7qd.htm](book-of-the-dead-bestiary-items/15qn60fnmbbmi7qd.htm)|Bone|Bone|modificada|
|[164uke5yf2ifcf8e.htm](book-of-the-dead-bestiary-items/164uke5yf2ifcf8e.htm)|Reap|Segar|modificada|
|[1a7SbMUxtcojgqLl.htm](book-of-the-dead-bestiary-items/1a7SbMUxtcojgqLl.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[1cs7olyf6afhvxbc.htm](book-of-the-dead-bestiary-items/1cs7olyf6afhvxbc.htm)|Onryo's Rancor|Onryo's Rancor|modificada|
|[1f5q331o2mt1wcdv.htm](book-of-the-dead-bestiary-items/1f5q331o2mt1wcdv.htm)|Soulbound Gallop|Galope de alma vinculada.|modificada|
|[1fa8oveae7es6ajn.htm](book-of-the-dead-bestiary-items/1fa8oveae7es6ajn.htm)|Fiddlestick|Fiddlestick|modificada|
|[1gmv4iqur5scrtnw.htm](book-of-the-dead-bestiary-items/1gmv4iqur5scrtnw.htm)|Soulscent (Imprecise) 200 feet|Soulscent (Impreciso) 200 pies|modificada|
|[1HdblgdIZkc4dN3e.htm](book-of-the-dead-bestiary-items/1HdblgdIZkc4dN3e.htm)|Vetalarana Vulnerabilities|Vetalarana Vulnerabilidades|modificada|
|[1lbqFqVnwC2oNkep.htm](book-of-the-dead-bestiary-items/1lbqFqVnwC2oNkep.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1mqppqclycua1e1x.htm](book-of-the-dead-bestiary-items/1mqppqclycua1e1x.htm)|Power of the Haunt|Power of the Haunt|modificada|
|[1NWUcNdniZkhKfCb.htm](book-of-the-dead-bestiary-items/1NWUcNdniZkhKfCb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[1otpt2d2gyuzglcy.htm](book-of-the-dead-bestiary-items/1otpt2d2gyuzglcy.htm)|Claw|Garra|modificada|
|[1rzixvxa00esxhop.htm](book-of-the-dead-bestiary-items/1rzixvxa00esxhop.htm)|Filth Fever|Filth Fever|modificada|
|[1T3U4mJBzZ7Nt8uF.htm](book-of-the-dead-bestiary-items/1T3U4mJBzZ7Nt8uF.htm)|Improved Grab|Agarrado mejorado|modificada|
|[1U8XEv359HH8seeK.htm](book-of-the-dead-bestiary-items/1U8XEv359HH8seeK.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[1VGFADh8jJIfVH94.htm](book-of-the-dead-bestiary-items/1VGFADh8jJIfVH94.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[1vq2jy6pqbxkwfde.htm](book-of-the-dead-bestiary-items/1vq2jy6pqbxkwfde.htm)|Servitor Realignment|Servitor Realignment|modificada|
|[1y7qe4eff55izld8.htm](book-of-the-dead-bestiary-items/1y7qe4eff55izld8.htm)|Stinger|Aguijón|modificada|
|[1zQfinUzk9ky6EKF.htm](book-of-the-dead-bestiary-items/1zQfinUzk9ky6EKF.htm)|Weapon Master|Maestro de armas|modificada|
|[20w584tt1vgcah4n.htm](book-of-the-dead-bestiary-items/20w584tt1vgcah4n.htm)|Claw|Garra|modificada|
|[21j0rnxdp8gf5zsf.htm](book-of-the-dead-bestiary-items/21j0rnxdp8gf5zsf.htm)|Jaws|Fauces|modificada|
|[21sp3kwgg8uh9p13.htm](book-of-the-dead-bestiary-items/21sp3kwgg8uh9p13.htm)|Dagger|Daga|modificada|
|[23gdXY5uSfl0Kp3I.htm](book-of-the-dead-bestiary-items/23gdXY5uSfl0Kp3I.htm)|Shifting Earth|Cambiante Tierra|modificada|
|[24C4VbeTjw3Ovvdn.htm](book-of-the-dead-bestiary-items/24C4VbeTjw3Ovvdn.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[25v45nqumj44pd4b.htm](book-of-the-dead-bestiary-items/25v45nqumj44pd4b.htm)|Wear Skin|Llevar Piel|modificada|
|[266u1f6h9sli518d.htm](book-of-the-dead-bestiary-items/266u1f6h9sli518d.htm)|Urveth Venom|Urveth Venom|modificada|
|[26yzjYUTRsWSJIeP.htm](book-of-the-dead-bestiary-items/26yzjYUTRsWSJIeP.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[26zb7felsd7fhdyg.htm](book-of-the-dead-bestiary-items/26zb7felsd7fhdyg.htm)|Float|Float|modificada|
|[272bll9hrxa49hmf.htm](book-of-the-dead-bestiary-items/272bll9hrxa49hmf.htm)|Ghostly Swoop|Ghostly Swoop|modificada|
|[2ahqZc1dTw700QB2.htm](book-of-the-dead-bestiary-items/2ahqZc1dTw700QB2.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2aUUvMiYk0FR7Cfr.htm](book-of-the-dead-bestiary-items/2aUUvMiYk0FR7Cfr.htm)|Negative Healing|Curación negativa|modificada|
|[2axamykyrsqyw3y1.htm](book-of-the-dead-bestiary-items/2axamykyrsqyw3y1.htm)|Undying Vendetta|Undying Vendetta|modificada|
|[2ccxczu33z7f67dc.htm](book-of-the-dead-bestiary-items/2ccxczu33z7f67dc.htm)|Premonition of Death|Premonición de muerte|modificada|
|[2d552nmuu8dx669n.htm](book-of-the-dead-bestiary-items/2d552nmuu8dx669n.htm)|Dagger|Daga|modificada|
|[2fv011dfxayfhevz.htm](book-of-the-dead-bestiary-items/2fv011dfxayfhevz.htm)|Water Vulnerability|Vulnerabilidad al agua|modificada|
|[2h8eb5k9fufiveqm.htm](book-of-the-dead-bestiary-items/2h8eb5k9fufiveqm.htm)|Sand Rot|Sand Rot|modificada|
|[2haw98dime2bu3qi.htm](book-of-the-dead-bestiary-items/2haw98dime2bu3qi.htm)|Shatter Block|Bloque de estallar|modificada|
|[2mNv4HeeYz6pnwor.htm](book-of-the-dead-bestiary-items/2mNv4HeeYz6pnwor.htm)|Negative Healing|Curación negativa|modificada|
|[2RyJ9XZZMEWMa6zK.htm](book-of-the-dead-bestiary-items/2RyJ9XZZMEWMa6zK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2s8zOCsruWILWVA9.htm](book-of-the-dead-bestiary-items/2s8zOCsruWILWVA9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[2tfn09ud7vyhndbb.htm](book-of-the-dead-bestiary-items/2tfn09ud7vyhndbb.htm)|Fossil Fury|Fossil Fury|modificada|
|[2ujjbxpmiiodb9o5.htm](book-of-the-dead-bestiary-items/2ujjbxpmiiodb9o5.htm)|Slow|Lentificado/a|modificada|
|[2xqc5getycsdpc49.htm](book-of-the-dead-bestiary-items/2xqc5getycsdpc49.htm)|Talon|Talon|modificada|
|[2xvk1ovgd1lsv1ze.htm](book-of-the-dead-bestiary-items/2xvk1ovgd1lsv1ze.htm)|Vital Transfusion|Transfusión Vital|modificada|
|[2ynj848nvp9c3igy.htm](book-of-the-dead-bestiary-items/2ynj848nvp9c3igy.htm)|Betray the Pack|Traicionar a la Manada|modificada|
|[2yp36AhxUGVlWXgq.htm](book-of-the-dead-bestiary-items/2yp36AhxUGVlWXgq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[324ljVcWLv8U3PwG.htm](book-of-the-dead-bestiary-items/324ljVcWLv8U3PwG.htm)|Negative Healing|Curación negativa|modificada|
|[32Oayck2ePyYYgli.htm](book-of-the-dead-bestiary-items/32Oayck2ePyYYgli.htm)|Negative Healing|Curación negativa|modificada|
|[32onun6vxolii6qa.htm](book-of-the-dead-bestiary-items/32onun6vxolii6qa.htm)|Jaws|Fauces|modificada|
|[35tm9k6kvybbcwkv.htm](book-of-the-dead-bestiary-items/35tm9k6kvybbcwkv.htm)|Aura of Whispers|Aura de susurros|modificada|
|[35ytvmm1cwqr4dbu.htm](book-of-the-dead-bestiary-items/35ytvmm1cwqr4dbu.htm)|Sudden Surge|Oleaje Repentino|modificada|
|[36cdvdo5yvfoxz7s.htm](book-of-the-dead-bestiary-items/36cdvdo5yvfoxz7s.htm)|Composite Longbow|Arco Largo Composición|modificada|
|[3BLhMeYajV3Rxi3L.htm](book-of-the-dead-bestiary-items/3BLhMeYajV3Rxi3L.htm)|Grab|Agarrado|modificada|
|[3G8FEWYsLeMf6cAv.htm](book-of-the-dead-bestiary-items/3G8FEWYsLeMf6cAv.htm)|Swallow Whole|Engullir Todo|modificada|
|[3gvb9wq37oq21be3.htm](book-of-the-dead-bestiary-items/3gvb9wq37oq21be3.htm)|Drain Life|Drenar Vida|modificada|
|[3hac6i5gkgu40340.htm](book-of-the-dead-bestiary-items/3hac6i5gkgu40340.htm)|Jaws|Fauces|modificada|
|[3j8u75qe0rn2330n.htm](book-of-the-dead-bestiary-items/3j8u75qe0rn2330n.htm)|Flintlock Pistol|Flintlock Pistol|modificada|
|[3kzbeblthgly6zkx.htm](book-of-the-dead-bestiary-items/3kzbeblthgly6zkx.htm)|Time Shift|Cambiante de Tiempo|modificada|
|[3qz1msK40lJzzqhO.htm](book-of-the-dead-bestiary-items/3qz1msK40lJzzqhO.htm)|Grab|Agarrado|modificada|
|[3spjg2szu482ix18.htm](book-of-the-dead-bestiary-items/3spjg2szu482ix18.htm)|Drain Magic|Drain Magic|modificada|
|[3t7zznqikrwn9cyb.htm](book-of-the-dead-bestiary-items/3t7zznqikrwn9cyb.htm)|Heavy Crossbow|Ballesta pesada|modificada|
|[3whr45ahot8zh9nf.htm](book-of-the-dead-bestiary-items/3whr45ahot8zh9nf.htm)|Abdomen Cache|Abdomen Cache|modificada|
|[3ybyexad76n3ge2g.htm](book-of-the-dead-bestiary-items/3ybyexad76n3ge2g.htm)|Servitor Assembly|Conjunto Servidor|modificada|
|[40McFjcRX4SifzsC.htm](book-of-the-dead-bestiary-items/40McFjcRX4SifzsC.htm)|Negative Healing|Curación negativa|modificada|
|[40tw7u6c1rckcl0u.htm](book-of-the-dead-bestiary-items/40tw7u6c1rckcl0u.htm)|Bone Javelin|Jabalina de Hueso|modificada|
|[41Dv5UzszJFoJotn.htm](book-of-the-dead-bestiary-items/41Dv5UzszJFoJotn.htm)|Negative Healing|Curación negativa|modificada|
|[42bls3q0amg2osjy.htm](book-of-the-dead-bestiary-items/42bls3q0amg2osjy.htm)|Jaws|Fauces|modificada|
|[42s7jyacejyvhl2u.htm](book-of-the-dead-bestiary-items/42s7jyacejyvhl2u.htm)|Infectious Visions|Visiones Infecciosas|modificada|
|[43aba9wneEhm4kYI.htm](book-of-the-dead-bestiary-items/43aba9wneEhm4kYI.htm)|Darkvision|Visión en la oscuridad|modificada|
|[43rtpaawc0hyjxs2.htm](book-of-the-dead-bestiary-items/43rtpaawc0hyjxs2.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[44AaXfBWgXV0uMpL.htm](book-of-the-dead-bestiary-items/44AaXfBWgXV0uMpL.htm)|Darkvision|Visión en la oscuridad|modificada|
|[479508pjo4uznsty.htm](book-of-the-dead-bestiary-items/479508pjo4uznsty.htm)|Final Snare|Final Snare|modificada|
|[47dj3m4gnycp9ovy.htm](book-of-the-dead-bestiary-items/47dj3m4gnycp9ovy.htm)|Vein Walker|Vein Walker|modificada|
|[4fpg4t23cn5sl7dn.htm](book-of-the-dead-bestiary-items/4fpg4t23cn5sl7dn.htm)|Flail|Mayal|modificada|
|[4KyzQNfgO9JMN6RH.htm](book-of-the-dead-bestiary-items/4KyzQNfgO9JMN6RH.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[4nn1859fvesmfedg.htm](book-of-the-dead-bestiary-items/4nn1859fvesmfedg.htm)|Shortsword|Espada corta|modificada|
|[4p83bgih48iq1inb.htm](book-of-the-dead-bestiary-items/4p83bgih48iq1inb.htm)|Claw|Garra|modificada|
|[4pt21hdckfee43y3.htm](book-of-the-dead-bestiary-items/4pt21hdckfee43y3.htm)|Mountain Sword|Espada de Montaña|modificada|
|[4qk5llfpop3z00fj.htm](book-of-the-dead-bestiary-items/4qk5llfpop3z00fj.htm)|Fist|Puño|modificada|
|[4r9QDElzXGCRVfhh.htm](book-of-the-dead-bestiary-items/4r9QDElzXGCRVfhh.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[4syq1lo2m1mx4330.htm](book-of-the-dead-bestiary-items/4syq1lo2m1mx4330.htm)|Agent of Despair|Agent of Despair|modificada|
|[4uc0r15oqcogaudt.htm](book-of-the-dead-bestiary-items/4uc0r15oqcogaudt.htm)|Voice Imitation|Imitación de voz|modificada|
|[4z31tccif8f7qaq7.htm](book-of-the-dead-bestiary-items/4z31tccif8f7qaq7.htm)|Spawn Hunter Wight|Cazador de engendros Wight|modificada|
|[4zbcbp0ersjqi242.htm](book-of-the-dead-bestiary-items/4zbcbp0ersjqi242.htm)|Curse Ye Scallywags!|¡Curse Ye Scallywags!|modificada|
|[517tUMEgs6I4camL.htm](book-of-the-dead-bestiary-items/517tUMEgs6I4camL.htm)|Negative Healing|Curación negativa|modificada|
|[53r0jhgz2tlvtcn9.htm](book-of-the-dead-bestiary-items/53r0jhgz2tlvtcn9.htm)|Accelerating Inquest|Accelerating Inquest|modificada|
|[55sqmv4kf6twrsh0.htm](book-of-the-dead-bestiary-items/55sqmv4kf6twrsh0.htm)|Foot|Pie|modificada|
|[56PK7jsheqUjQAKo.htm](book-of-the-dead-bestiary-items/56PK7jsheqUjQAKo.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[56vkqrstijfjdh6c.htm](book-of-the-dead-bestiary-items/56vkqrstijfjdh6c.htm)|Ground Slam|Aporrear el suelo|modificada|
|[57b0x2wmtxjbx2ww.htm](book-of-the-dead-bestiary-items/57b0x2wmtxjbx2ww.htm)|Psychic Superiority|Superioridad Psíquica|modificada|
|[581dy5lnt449p866.htm](book-of-the-dead-bestiary-items/581dy5lnt449p866.htm)|Spawn Wight Soldier|Spawn Wight Soldier|modificada|
|[58ox9o6ljire079b.htm](book-of-the-dead-bestiary-items/58ox9o6ljire079b.htm)|Wrath of the Haunt|Wrath of the Haunt|modificada|
|[5aGqEha0KK3U7pFi.htm](book-of-the-dead-bestiary-items/5aGqEha0KK3U7pFi.htm)|-2 to All Saves vs. Curses|-2 a todas las salvaciones contra maldiciones|modificada|
|[5cP06WMcn0WgZIXu.htm](book-of-the-dead-bestiary-items/5cP06WMcn0WgZIXu.htm)|Darkvision|Visión en la oscuridad|modificada|
|[5CQVRA4OFks4NZna.htm](book-of-the-dead-bestiary-items/5CQVRA4OFks4NZna.htm)|Negative Healing|Curación negativa|modificada|
|[5dpwpqolmjl4a6sn.htm](book-of-the-dead-bestiary-items/5dpwpqolmjl4a6sn.htm)|Collect Soul|Collect Soul|modificada|
|[5gjnv61c3nc5vbt6.htm](book-of-the-dead-bestiary-items/5gjnv61c3nc5vbt6.htm)|Spiked Chain|Cadena de pinchos|modificada|
|[5k5e5m4xanv71qep.htm](book-of-the-dead-bestiary-items/5k5e5m4xanv71qep.htm)|Smoldering Fist|Puño ardiente|modificada|
|[5l8z97yi0fc0gqoq.htm](book-of-the-dead-bestiary-items/5l8z97yi0fc0gqoq.htm)|Root|Enraizarse|modificada|
|[5lklhb6x3xhg0lri.htm](book-of-the-dead-bestiary-items/5lklhb6x3xhg0lri.htm)|Fist|Puño|modificada|
|[5mxcdpdeibtg3cig.htm](book-of-the-dead-bestiary-items/5mxcdpdeibtg3cig.htm)|Ghostly Blades|Cuchillas fantasmales|modificada|
|[5n8sgtk9eiwslacu.htm](book-of-the-dead-bestiary-items/5n8sgtk9eiwslacu.htm)|Sense Visitors|Sense Visitors|modificada|
|[5q1vmg0m797bxfoz.htm](book-of-the-dead-bestiary-items/5q1vmg0m797bxfoz.htm)|Claw|Garra|modificada|
|[5q62z2h5hdchg2s2.htm](book-of-the-dead-bestiary-items/5q62z2h5hdchg2s2.htm)|Awful Approach|Enfoque horrible|modificada|
|[5qil88xw16heonrg.htm](book-of-the-dead-bestiary-items/5qil88xw16heonrg.htm)|Smoke Vision|Visión de Humo|modificada|
|[5S4ZcAlTU2l4OVrx.htm](book-of-the-dead-bestiary-items/5S4ZcAlTU2l4OVrx.htm)|Wavesense (Imprecise) 100 feet|Sentir ondulaciones (Impreciso) 100 pies.|modificada|
|[5tla19582bslvej5.htm](book-of-the-dead-bestiary-items/5tla19582bslvej5.htm)|Death Grip|Death Grip|modificada|
|[6312irfg8l2y8wqs.htm](book-of-the-dead-bestiary-items/6312irfg8l2y8wqs.htm)|Foot|Pie|modificada|
|[68hcqX4h4YdgWr9o.htm](book-of-the-dead-bestiary-items/68hcqX4h4YdgWr9o.htm)|Fist|Puño|modificada|
|[6FVQCXo7u4wEwCcA.htm](book-of-the-dead-bestiary-items/6FVQCXo7u4wEwCcA.htm)|Constrict|Restringir|modificada|
|[6h26o4rywcidme60.htm](book-of-the-dead-bestiary-items/6h26o4rywcidme60.htm)|Time-shifting Touch|Toque Cambiante|modificada|
|[6Ho5G5qDA1od6BFM.htm](book-of-the-dead-bestiary-items/6Ho5G5qDA1od6BFM.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[6iygpHZZveJtpY3U.htm](book-of-the-dead-bestiary-items/6iygpHZZveJtpY3U.htm)|Feast|Festín|modificada|
|[6leqktn7qesbdoh1.htm](book-of-the-dead-bestiary-items/6leqktn7qesbdoh1.htm)|Desiccation Aura|Desiccation Aura|modificada|
|[6MBpOAXA2v4U8Ojh.htm](book-of-the-dead-bestiary-items/6MBpOAXA2v4U8Ojh.htm)|Constrict|Restringir|modificada|
|[6qp9o1qevi1znh6x.htm](book-of-the-dead-bestiary-items/6qp9o1qevi1znh6x.htm)|Twilight Spirit|Twilight Spirit|modificada|
|[6sig7ubrfqb7uiei.htm](book-of-the-dead-bestiary-items/6sig7ubrfqb7uiei.htm)|Channel Rot|Canalizar putridez|modificada|
|[6tism8ur5hq61x7v.htm](book-of-the-dead-bestiary-items/6tism8ur5hq61x7v.htm)|Steal Soul|Sustraer Alma|modificada|
|[6U0JQZ8ztbguo44B.htm](book-of-the-dead-bestiary-items/6U0JQZ8ztbguo44B.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[6UUahVCN4rUQxrW0.htm](book-of-the-dead-bestiary-items/6UUahVCN4rUQxrW0.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[6yud3tv7zitkw9k9.htm](book-of-the-dead-bestiary-items/6yud3tv7zitkw9k9.htm)|Slithering Strike|Golpe deslizante|modificada|
|[7499bjv6f655br8n.htm](book-of-the-dead-bestiary-items/7499bjv6f655br8n.htm)|Spawn Prowler Wight|Spawn Prowler Wight|modificada|
|[79lxx093izwrpuih.htm](book-of-the-dead-bestiary-items/79lxx093izwrpuih.htm)|Tusk|Tusk|modificada|
|[7at7cp1369l1wlf5.htm](book-of-the-dead-bestiary-items/7at7cp1369l1wlf5.htm)|Warrior's Mask|Máscara del combatiente|modificada|
|[7avv9wov0dsg3srf.htm](book-of-the-dead-bestiary-items/7avv9wov0dsg3srf.htm)|Ravenous Void|Ravenous Void|modificada|
|[7hy3wqu9oefod2vm.htm](book-of-the-dead-bestiary-items/7hy3wqu9oefod2vm.htm)|Mountain Slam|Golpe de Montaña|modificada|
|[7Iep6kSfoCj1UNVm.htm](book-of-the-dead-bestiary-items/7Iep6kSfoCj1UNVm.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[7ko9o8gxy2rnu262.htm](book-of-the-dead-bestiary-items/7ko9o8gxy2rnu262.htm)|Fist|Puño|modificada|
|[7lt4725ubnitaecg.htm](book-of-the-dead-bestiary-items/7lt4725ubnitaecg.htm)|Jaws|Fauces|modificada|
|[7m7b3dwyednyhqtu.htm](book-of-the-dead-bestiary-items/7m7b3dwyednyhqtu.htm)|Denounce Heretic|Denunciar Hereje|modificada|
|[7qQmNwIMFsY4kIjN.htm](book-of-the-dead-bestiary-items/7qQmNwIMFsY4kIjN.htm)|Defend Territory|Defender Territorio|modificada|
|[7rpcLcKUwgjQVUST.htm](book-of-the-dead-bestiary-items/7rpcLcKUwgjQVUST.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[7spg3bnxup4hapkm.htm](book-of-the-dead-bestiary-items/7spg3bnxup4hapkm.htm)|Branch|Rama|modificada|
|[7wp3tf6myiuh5zf6.htm](book-of-the-dead-bestiary-items/7wp3tf6myiuh5zf6.htm)|Mandible|Mandíbula|modificada|
|[80uvreq1zdclqkeo.htm](book-of-the-dead-bestiary-items/80uvreq1zdclqkeo.htm)|Tumultuous Flash|Flash Tumultuoso|modificada|
|[81Htx72275BlwBKF.htm](book-of-the-dead-bestiary-items/81Htx72275BlwBKF.htm)|Weep Blood|Llorar Sangre|modificada|
|[83bRw8wnX0fjOtf2.htm](book-of-the-dead-bestiary-items/83bRw8wnX0fjOtf2.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[83mbwwys64b7tvby.htm](book-of-the-dead-bestiary-items/83mbwwys64b7tvby.htm)|Sense Companion|Sense Companion|modificada|
|[88vGaWxuqoaedx3J.htm](book-of-the-dead-bestiary-items/88vGaWxuqoaedx3J.htm)|Arcane Innate Spells|Hechizos Arcanos Innatos|modificada|
|[88ymu96vgm67d1yi.htm](book-of-the-dead-bestiary-items/88ymu96vgm67d1yi.htm)|Vengeful Suffocation|Vengeful Suffocation|modificada|
|[8BdsrDdI55FRLHOf.htm](book-of-the-dead-bestiary-items/8BdsrDdI55FRLHOf.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[8cgtf6p2b39hnee5.htm](book-of-the-dead-bestiary-items/8cgtf6p2b39hnee5.htm)|Fragile Wings|Fragile Wings|modificada|
|[8dmnhjn5baw0ic6c.htm](book-of-the-dead-bestiary-items/8dmnhjn5baw0ic6c.htm)|Breath of Sand|Aliento de Arena|modificada|
|[8dn13bwe1ei77nhq.htm](book-of-the-dead-bestiary-items/8dn13bwe1ei77nhq.htm)|Shortbow|Arco corto|modificada|
|[8e4yMrXnRhkqRrxJ.htm](book-of-the-dead-bestiary-items/8e4yMrXnRhkqRrxJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[8GAYsczHou1vWlDL.htm](book-of-the-dead-bestiary-items/8GAYsczHou1vWlDL.htm)|Negative Healing|Curación negativa|modificada|
|[8h5yhlk6yupldtxp.htm](book-of-the-dead-bestiary-items/8h5yhlk6yupldtxp.htm)|Resurrection Vulnerability|Vulnerabilidad de resurrección|modificada|
|[8jhirjg4ivj0abbm.htm](book-of-the-dead-bestiary-items/8jhirjg4ivj0abbm.htm)|Crumbling Form|Desmoronarse Forma|modificada|
|[8ki7j1n996www1th.htm](book-of-the-dead-bestiary-items/8ki7j1n996www1th.htm)|Shadowless|Shadowless|modificada|
|[8lahe1p6o2mjwdip.htm](book-of-the-dead-bestiary-items/8lahe1p6o2mjwdip.htm)|Temporal Fracturing Ray|Rayo de Fractura Temporal|modificada|
|[8O9kWQZrwnaEV85K.htm](book-of-the-dead-bestiary-items/8O9kWQZrwnaEV85K.htm)|Improved Grab|Agarrado mejorado|modificada|
|[8paxHAcU5Do4oAI1.htm](book-of-the-dead-bestiary-items/8paxHAcU5Do4oAI1.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[8q0pqsiwe57oc0lw.htm](book-of-the-dead-bestiary-items/8q0pqsiwe57oc0lw.htm)|Sandstorm|Tormenta de Arena|modificada|
|[8QQmzAl2OQsH6V6D.htm](book-of-the-dead-bestiary-items/8QQmzAl2OQsH6V6D.htm)|Constant Spells|Constant Spells|modificada|
|[8t3kp7gfpp7qfl7q.htm](book-of-the-dead-bestiary-items/8t3kp7gfpp7qfl7q.htm)|Robe Tangle|Robe Tangle|modificada|
|[8ub8dfxzqtxj93eq.htm](book-of-the-dead-bestiary-items/8ub8dfxzqtxj93eq.htm)|Charge Chain|Cadena de Carga|modificada|
|[90hmtfy65sjru41o.htm](book-of-the-dead-bestiary-items/90hmtfy65sjru41o.htm)|Dissonant Chord|Dissonant Chord|modificada|
|[90j5smnm3ber9gfp.htm](book-of-the-dead-bestiary-items/90j5smnm3ber9gfp.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[92BrN8oQFaOLJ91I.htm](book-of-the-dead-bestiary-items/92BrN8oQFaOLJ91I.htm)|Darkvision|Visión en la oscuridad|modificada|
|[942tW0398ldkqoyk.htm](book-of-the-dead-bestiary-items/942tW0398ldkqoyk.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[95en1txwwqbdjtdr.htm](book-of-the-dead-bestiary-items/95en1txwwqbdjtdr.htm)|Implacable Advance|Implacable Advance|modificada|
|[9br1r4rn2iiosza8.htm](book-of-the-dead-bestiary-items/9br1r4rn2iiosza8.htm)|Battlefield Bound|Battlefield Bound|modificada|
|[9bucmn723g63yhg1.htm](book-of-the-dead-bestiary-items/9bucmn723g63yhg1.htm)|Jaws|Fauces|modificada|
|[9BZlVPtKWPrqj2a3.htm](book-of-the-dead-bestiary-items/9BZlVPtKWPrqj2a3.htm)|Negative Healing|Curación negativa|modificada|
|[9cot9pecrjfwekp6.htm](book-of-the-dead-bestiary-items/9cot9pecrjfwekp6.htm)|Vomit Blood|Vomitar Sangre|modificada|
|[9lqmxwvb8gf3pki2.htm](book-of-the-dead-bestiary-items/9lqmxwvb8gf3pki2.htm)|Siphon Vitality|Succionar vitalidad|modificada|
|[9nhauoark4hd73ic.htm](book-of-the-dead-bestiary-items/9nhauoark4hd73ic.htm)|Thoughtsense 60 feet|Percibir pensamientos 60 pies|modificada|
|[9Osaf1jaTvEKFmUM.htm](book-of-the-dead-bestiary-items/9Osaf1jaTvEKFmUM.htm)|Ruinous Weapons|Armas de la ruina|modificada|
|[9Qeobioo0LmSzjQg.htm](book-of-the-dead-bestiary-items/9Qeobioo0LmSzjQg.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9QFD9WobEytvbbov.htm](book-of-the-dead-bestiary-items/9QFD9WobEytvbbov.htm)|Negative Healing|Curación negativa|modificada|
|[9RsQKHAZSLUD8OaG.htm](book-of-the-dead-bestiary-items/9RsQKHAZSLUD8OaG.htm)|Darkvision|Visión en la oscuridad|modificada|
|[9uwteydd6ypzaa2e.htm](book-of-the-dead-bestiary-items/9uwteydd6ypzaa2e.htm)|Tactical Direction|Dirección Táctica|modificada|
|[9X9LnWN67AI3Wyjr.htm](book-of-the-dead-bestiary-items/9X9LnWN67AI3Wyjr.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[9xtciwtearfnw9g0.htm](book-of-the-dead-bestiary-items/9xtciwtearfnw9g0.htm)|Undying Vendetta|Undying Vendetta|modificada|
|[9y280jhc7njt1nez.htm](book-of-the-dead-bestiary-items/9y280jhc7njt1nez.htm)|Lair Sense|Sentido de Guarida|modificada|
|[a0e3JLzKlammUKQN.htm](book-of-the-dead-bestiary-items/a0e3JLzKlammUKQN.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[a34aowwyzlmdtpg4.htm](book-of-the-dead-bestiary-items/a34aowwyzlmdtpg4.htm)|Ghostly Grasp|Ghostly Grasp|modificada|
|[a380z0ervv71womf.htm](book-of-the-dead-bestiary-items/a380z0ervv71womf.htm)|Wing|Ala|modificada|
|[a3wMuTNkVSHLbp4T.htm](book-of-the-dead-bestiary-items/a3wMuTNkVSHLbp4T.htm)|Darkvision|Visión en la oscuridad|modificada|
|[a7gh1zpyblagcti4.htm](book-of-the-dead-bestiary-items/a7gh1zpyblagcti4.htm)|Death Gasp|Death Gasp|modificada|
|[a7w43julclvnfw63.htm](book-of-the-dead-bestiary-items/a7w43julclvnfw63.htm)|Drain Spell Tome|Drenar Hechizo Tome|modificada|
|[a9ir7xv426ririzq.htm](book-of-the-dead-bestiary-items/a9ir7xv426ririzq.htm)|Surge of Speed|Arranque de velocidad|modificada|
|[a9iRrDBwDQtFML4a.htm](book-of-the-dead-bestiary-items/a9iRrDBwDQtFML4a.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ad1bef6ocdexoep9.htm](book-of-the-dead-bestiary-items/ad1bef6ocdexoep9.htm)|Fangs|Colmillos|modificada|
|[aDtuEVLZvPSFSJTZ.htm](book-of-the-dead-bestiary-items/aDtuEVLZvPSFSJTZ.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[ai6gr48jowwpnc0m.htm](book-of-the-dead-bestiary-items/ai6gr48jowwpnc0m.htm)|Ravenous Undoing|Ravenous Undoing|modificada|
|[AjjzMtoQOxAQvqf9.htm](book-of-the-dead-bestiary-items/AjjzMtoQOxAQvqf9.htm)|Negative Healing|Curación negativa|modificada|
|[al020n2uibvv8exp.htm](book-of-the-dead-bestiary-items/al020n2uibvv8exp.htm)|Fist|Puño|modificada|
|[am4u24k7o7diqo0t.htm](book-of-the-dead-bestiary-items/am4u24k7o7diqo0t.htm)|Woodland Dependent|Dependiente del bosque|modificada|
|[AmOslrFKvYuv61w1.htm](book-of-the-dead-bestiary-items/AmOslrFKvYuv61w1.htm)|Control Comatose|Control Comatose|modificada|
|[AnBTVVvzCtNwFDSJ.htm](book-of-the-dead-bestiary-items/AnBTVVvzCtNwFDSJ.htm)|Grab|Agarrado|modificada|
|[aph86y82rdqgcn4k.htm](book-of-the-dead-bestiary-items/aph86y82rdqgcn4k.htm)|Control Body|Cuerpo de control|modificada|
|[apx5l6z5p09xanhv.htm](book-of-the-dead-bestiary-items/apx5l6z5p09xanhv.htm)|Feign Death|Fingir Muerte|modificada|
|[apXBdiwzInrE6nhf.htm](book-of-the-dead-bestiary-items/apXBdiwzInrE6nhf.htm)|Negative Healing|Curación negativa|modificada|
|[aqutya2asmrp8wxy.htm](book-of-the-dead-bestiary-items/aqutya2asmrp8wxy.htm)|Soul Theft|Soul Theft|modificada|
|[ars8cn2115cvrmwo.htm](book-of-the-dead-bestiary-items/ars8cn2115cvrmwo.htm)|Ghostly Swoop|Ghostly Swoop|modificada|
|[b146le3z824z27kz.htm](book-of-the-dead-bestiary-items/b146le3z824z27kz.htm)|Jaws|Fauces|modificada|
|[B2lJQF6Y6o4hnBeL.htm](book-of-the-dead-bestiary-items/B2lJQF6Y6o4hnBeL.htm)|Negative Healing|Curación negativa|modificada|
|[b44nuiFLtAaurkMZ.htm](book-of-the-dead-bestiary-items/b44nuiFLtAaurkMZ.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[b4tdi1u14a8xwxkz.htm](book-of-the-dead-bestiary-items/b4tdi1u14a8xwxkz.htm)|Change of Luck|Cambio de Suerte|modificada|
|[b4ti2yqe9ipiuxz2.htm](book-of-the-dead-bestiary-items/b4ti2yqe9ipiuxz2.htm)|Sudden Surge|Oleaje Repentino|modificada|
|[B4uyQyWXUec4RybT.htm](book-of-the-dead-bestiary-items/B4uyQyWXUec4RybT.htm)|Ghostly Touch|Toque fantasmal|modificada|
|[b9qjt3e0bvq6d5wx.htm](book-of-the-dead-bestiary-items/b9qjt3e0bvq6d5wx.htm)|Trunk|Tronco|modificada|
|[BcSNcGhcDxvtI9k5.htm](book-of-the-dead-bestiary-items/BcSNcGhcDxvtI9k5.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[bCT8pcPOyNG51I6r.htm](book-of-the-dead-bestiary-items/bCT8pcPOyNG51I6r.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bd5fpgu3trmh7764.htm](book-of-the-dead-bestiary-items/bd5fpgu3trmh7764.htm)|Eat Soul|Come Alma|modificada|
|[bftdv71wnr4786h4.htm](book-of-the-dead-bestiary-items/bftdv71wnr4786h4.htm)|Call of the Damned|Llamada, la Maldita|modificada|
|[bfVGbnQelJe4aMTO.htm](book-of-the-dead-bestiary-items/bfVGbnQelJe4aMTO.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[bh3hivovyu8ym9ay.htm](book-of-the-dead-bestiary-items/bh3hivovyu8ym9ay.htm)|Painful Touch|Toque Doloroso|modificada|
|[bhmy3f5zgxplyfru.htm](book-of-the-dead-bestiary-items/bhmy3f5zgxplyfru.htm)|Demesne Confinement|Demesne Confinement|modificada|
|[bj3PlHtgo2gfPTmO.htm](book-of-the-dead-bestiary-items/bj3PlHtgo2gfPTmO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bKHEkuv4wRPctK0n.htm](book-of-the-dead-bestiary-items/bKHEkuv4wRPctK0n.htm)|+26 to Initiative|+26 a Iniciativa|modificada|
|[BMogxn5MHTe3lvyK.htm](book-of-the-dead-bestiary-items/BMogxn5MHTe3lvyK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[bnaNGdsV5TrxiHNp.htm](book-of-the-dead-bestiary-items/bnaNGdsV5TrxiHNp.htm)|Constant Spells|Constant Spells|modificada|
|[bnge6fye2orz7wv1.htm](book-of-the-dead-bestiary-items/bnge6fye2orz7wv1.htm)|Luring Laugh|Luring Laugh|modificada|
|[bnqnkgeuejbypq0u.htm](book-of-the-dead-bestiary-items/bnqnkgeuejbypq0u.htm)|Horned Rush|Embestida de Cuernos|modificada|
|[bnQpqP6hIv1ISjen.htm](book-of-the-dead-bestiary-items/bnQpqP6hIv1ISjen.htm)|Negative Healing|Curación negativa|modificada|
|[bpw486wu7j66tqyz.htm](book-of-the-dead-bestiary-items/bpw486wu7j66tqyz.htm)|Bloody Handprint|Bloody Handprint|modificada|
|[brzonbw2amez88xj.htm](book-of-the-dead-bestiary-items/brzonbw2amez88xj.htm)|Possess Tree|Poseer Árbol|modificada|
|[bT4K3qMjcsV7JMaV.htm](book-of-the-dead-bestiary-items/bT4K3qMjcsV7JMaV.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[bwdkr5yl7ye33lip.htm](book-of-the-dead-bestiary-items/bwdkr5yl7ye33lip.htm)|Dagger|Daga|modificada|
|[bx92ttr6lo474e66.htm](book-of-the-dead-bestiary-items/bx92ttr6lo474e66.htm)|Heretic's Armaments|Armamento de Hereje|modificada|
|[bXrBIlfFPcGYTa2l.htm](book-of-the-dead-bestiary-items/bXrBIlfFPcGYTa2l.htm)|Mental Rebirth|Mental Rebirth|modificada|
|[bywAbicCfjMxO2MY.htm](book-of-the-dead-bestiary-items/bywAbicCfjMxO2MY.htm)|Negative Healing|Curación negativa|modificada|
|[c0cED5WAyOCC6wwV.htm](book-of-the-dead-bestiary-items/c0cED5WAyOCC6wwV.htm)|Constant Spells|Constant Spells|modificada|
|[c2iKZhgPad7G2R6c.htm](book-of-the-dead-bestiary-items/c2iKZhgPad7G2R6c.htm)|Darkvision|Visión en la oscuridad|modificada|
|[c2kagtputtatmo83.htm](book-of-the-dead-bestiary-items/c2kagtputtatmo83.htm)|Longbow|Arco largo|modificada|
|[c4od3wbgqhmgyp28.htm](book-of-the-dead-bestiary-items/c4od3wbgqhmgyp28.htm)|Voice of the Soul|Voz del Alma|modificada|
|[C6FyNCCwhcZO6sQX.htm](book-of-the-dead-bestiary-items/C6FyNCCwhcZO6sQX.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[cb4gc90xqcsm6n37.htm](book-of-the-dead-bestiary-items/cb4gc90xqcsm6n37.htm)|Sense Murderer|Sense Murderer|modificada|
|[cbmmwaxrz6a1z435.htm](book-of-the-dead-bestiary-items/cbmmwaxrz6a1z435.htm)|Divine Guardian|Guardián Divino|modificada|
|[CDMyFeuLiSZFWpoj.htm](book-of-the-dead-bestiary-items/CDMyFeuLiSZFWpoj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[cg2m9if702p5jhb2.htm](book-of-the-dead-bestiary-items/cg2m9if702p5jhb2.htm)|Totems of the Past|Tótems del Pasado|modificada|
|[cg7mnEAt7MBa8UwC.htm](book-of-the-dead-bestiary-items/cg7mnEAt7MBa8UwC.htm)|Vetalarana Vulnerabilities|Vetalarana Vulnerabilidades|modificada|
|[chmnd5AocqCFMCGT.htm](book-of-the-dead-bestiary-items/chmnd5AocqCFMCGT.htm)|Frightful Presence|Frightful Presence|modificada|
|[chqupAH7uMySkbAy.htm](book-of-the-dead-bestiary-items/chqupAH7uMySkbAy.htm)|Mental Rebirth|Mental Rebirth|modificada|
|[cifv4nupbufci3x9.htm](book-of-the-dead-bestiary-items/cifv4nupbufci3x9.htm)|Fangs|Colmillos|modificada|
|[cIJp4XSjGySMlIFb.htm](book-of-the-dead-bestiary-items/cIJp4XSjGySMlIFb.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[CItCjF3JMpPDPOGb.htm](book-of-the-dead-bestiary-items/CItCjF3JMpPDPOGb.htm)|Shamble Forth|Shamble Forth|modificada|
|[cMH2fN0W5f937Chd.htm](book-of-the-dead-bestiary-items/cMH2fN0W5f937Chd.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[cPUJUW23ZXZ9W7Wr.htm](book-of-the-dead-bestiary-items/cPUJUW23ZXZ9W7Wr.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[csccddk7b6n1491u.htm](book-of-the-dead-bestiary-items/csccddk7b6n1491u.htm)|Field of Undeath|Campo de la Muerte|modificada|
|[csqmqowngauhnll2.htm](book-of-the-dead-bestiary-items/csqmqowngauhnll2.htm)|Agonized Howl|Aullido Agonizante|modificada|
|[cuI5TrIQCtm5H6Bt.htm](book-of-the-dead-bestiary-items/cuI5TrIQCtm5H6Bt.htm)|Scythe|Guadaña|modificada|
|[cuvl6Xxa053PF1xX.htm](book-of-the-dead-bestiary-items/cuvl6Xxa053PF1xX.htm)|Negative Healing|Curación negativa|modificada|
|[cVXdT14wxKimZClN.htm](book-of-the-dead-bestiary-items/cVXdT14wxKimZClN.htm)|Jiang-Shi Vulnerabilities|Jiang-Shi Vulnerabilidades|modificada|
|[CwXMyTDGPy4bVI0z.htm](book-of-the-dead-bestiary-items/CwXMyTDGPy4bVI0z.htm)|Negative Healing|Curación negativa|modificada|
|[cxlq6wpb9in9z1s2.htm](book-of-the-dead-bestiary-items/cxlq6wpb9in9z1s2.htm)|Ghostly Hand Crossbow|Ballesta de mano fantasmal|modificada|
|[CZFYOk40GBWGe5U5.htm](book-of-the-dead-bestiary-items/CZFYOk40GBWGe5U5.htm)|Change Shape|Change Shape|modificada|
|[D0GXxbH8jAosuw4k.htm](book-of-the-dead-bestiary-items/D0GXxbH8jAosuw4k.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[D1dtdbWicwawxBM3.htm](book-of-the-dead-bestiary-items/D1dtdbWicwawxBM3.htm)|Constrict|Restringir|modificada|
|[d1u87ahqlsegvjml.htm](book-of-the-dead-bestiary-items/d1u87ahqlsegvjml.htm)|Javelin|Javelin|modificada|
|[D2cPnadhWWwabreZ.htm](book-of-the-dead-bestiary-items/D2cPnadhWWwabreZ.htm)|Greater Constrict|Mayor Restricción|modificada|
|[D5XHOv8oLEJdcY0i.htm](book-of-the-dead-bestiary-items/D5XHOv8oLEJdcY0i.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[d607zui6g37uqefz.htm](book-of-the-dead-bestiary-items/d607zui6g37uqefz.htm)|Pounce|Abalanzarse|modificada|
|[da71u2jzo19jfmuf.htm](book-of-the-dead-bestiary-items/da71u2jzo19jfmuf.htm)|Channel Rot|Canalizar putridez|modificada|
|[daAC9eNxuMlrDoZG.htm](book-of-the-dead-bestiary-items/daAC9eNxuMlrDoZG.htm)|Grab|Agarrado|modificada|
|[db4votcoqinbjj2s.htm](book-of-the-dead-bestiary-items/db4votcoqinbjj2s.htm)|Scythe|Guadaña|modificada|
|[dc6VyUkQp6pOJtSn.htm](book-of-the-dead-bestiary-items/dc6VyUkQp6pOJtSn.htm)|Constrict|Restringir|modificada|
|[df2mf1pstfhw330a.htm](book-of-the-dead-bestiary-items/df2mf1pstfhw330a.htm)|Tear Skin|Tear Skin|modificada|
|[df79dbzoljgbj0mq.htm](book-of-the-dead-bestiary-items/df79dbzoljgbj0mq.htm)|Self-Loathing|Autodesprecio|modificada|
|[dFiZIcxqM6qYIs6g.htm](book-of-the-dead-bestiary-items/dFiZIcxqM6qYIs6g.htm)|Darkvision|Visión en la oscuridad|modificada|
|[DHn9EnlQKZ1hypSV.htm](book-of-the-dead-bestiary-items/DHn9EnlQKZ1hypSV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dIjjf4Tqor8DWWHu.htm](book-of-the-dead-bestiary-items/dIjjf4Tqor8DWWHu.htm)|Swallow Whole|Engullir Todo|modificada|
|[dizkg5865hh5zv35.htm](book-of-the-dead-bestiary-items/dizkg5865hh5zv35.htm)|Wake the Dead|Despierta a los muertos|modificada|
|[Dj9dMn2OXHtfdyMz.htm](book-of-the-dead-bestiary-items/Dj9dMn2OXHtfdyMz.htm)|Whisper|Whisper|modificada|
|[djU8N0rJuOlO9qqb.htm](book-of-the-dead-bestiary-items/djU8N0rJuOlO9qqb.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[dKbCkV7uzuluzoo8.htm](book-of-the-dead-bestiary-items/dKbCkV7uzuluzoo8.htm)|Darkvision|Visión en la oscuridad|modificada|
|[dkckp2z4bbmspgcy.htm](book-of-the-dead-bestiary-items/dkckp2z4bbmspgcy.htm)|Beak|Beak|modificada|
|[DLfsz7D9pLNSqIlT.htm](book-of-the-dead-bestiary-items/DLfsz7D9pLNSqIlT.htm)|Stony Shards|Stony Shards|modificada|
|[dnu6ev9agkecmex6.htm](book-of-the-dead-bestiary-items/dnu6ev9agkecmex6.htm)|Share Blessings|Compartir bendiciones|modificada|
|[dOeCzneUCh2n8Vq5.htm](book-of-the-dead-bestiary-items/dOeCzneUCh2n8Vq5.htm)|Aura of Repose|Aura de reposo|modificada|
|[dp0m2eqjh44d7v4a.htm](book-of-the-dead-bestiary-items/dp0m2eqjh44d7v4a.htm)|Entropy's Shadow|Sombra de Entropía|modificada|
|[dqeg5ywheav84nwd.htm](book-of-the-dead-bestiary-items/dqeg5ywheav84nwd.htm)|Stench|Hedor|modificada|
|[drin3tt0epvjjsr9.htm](book-of-the-dead-bestiary-items/drin3tt0epvjjsr9.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[dSlveRiUSvMYcg8P.htm](book-of-the-dead-bestiary-items/dSlveRiUSvMYcg8P.htm)|Negative Healing|Curación negativa|modificada|
|[DSp7AdbdrabM470N.htm](book-of-the-dead-bestiary-items/DSp7AdbdrabM470N.htm)|Surge Through|Oleaje a través de|modificada|
|[dtbhcid8fzmfjtp7.htm](book-of-the-dead-bestiary-items/dtbhcid8fzmfjtp7.htm)|Paralyzing Spew|Disparo paralizante|modificada|
|[duqnb28a7oppci1f.htm](book-of-the-dead-bestiary-items/duqnb28a7oppci1f.htm)|Tail|Tail|modificada|
|[dXqGPFgT8WHpl72i.htm](book-of-the-dead-bestiary-items/dXqGPFgT8WHpl72i.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[dYKKjbhn2q7r853p.htm](book-of-the-dead-bestiary-items/dYKKjbhn2q7r853p.htm)|Negative Healing|Curación negativa|modificada|
|[DYQmHncbK51f0Cup.htm](book-of-the-dead-bestiary-items/DYQmHncbK51f0Cup.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[dz1w9j5ipev8z1v8.htm](book-of-the-dead-bestiary-items/dz1w9j5ipev8z1v8.htm)|Savvy Joinin' Me Crew?|¿Savvy Joinin' Me Crew?|modificada|
|[e0vfxgxi4fovzldn.htm](book-of-the-dead-bestiary-items/e0vfxgxi4fovzldn.htm)|Proboscis|Proboscis|modificada|
|[e68t84jixmehqnur.htm](book-of-the-dead-bestiary-items/e68t84jixmehqnur.htm)|Final Spite|Final Spite|modificada|
|[e7nXVy8HD6KkhPGf.htm](book-of-the-dead-bestiary-items/e7nXVy8HD6KkhPGf.htm)|One More Breath|One More Breath|modificada|
|[e7v5oimcd7h2wmj8.htm](book-of-the-dead-bestiary-items/e7v5oimcd7h2wmj8.htm)|Sand Form|Forma Arena|modificada|
|[ecn8ua35xgwp7abu.htm](book-of-the-dead-bestiary-items/ecn8ua35xgwp7abu.htm)|Corpse Consumption|Consumo de cadáveres|modificada|
|[EcZYs4KlcnQ09I0F.htm](book-of-the-dead-bestiary-items/EcZYs4KlcnQ09I0F.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ee5DMlbADQLHFvnV.htm](book-of-the-dead-bestiary-items/Ee5DMlbADQLHFvnV.htm)|Darkvision|Visión en la oscuridad|modificada|
|[eHad4TakoeYMeM8g.htm](book-of-the-dead-bestiary-items/eHad4TakoeYMeM8g.htm)|Sneak Attack|Movimiento furtivo|modificada|
|[eih755f7z0quy1bu.htm](book-of-the-dead-bestiary-items/eih755f7z0quy1bu.htm)|Scythe|Guadaña|modificada|
|[ej3i6scbur8axq00.htm](book-of-the-dead-bestiary-items/ej3i6scbur8axq00.htm)|Ghostly Cutlass|Sable fantasmal|modificada|
|[ejx371hd10a4rhfs.htm](book-of-the-dead-bestiary-items/ejx371hd10a4rhfs.htm)|Jaws|Fauces|modificada|
|[ekhnzl2vg8r8raia.htm](book-of-the-dead-bestiary-items/ekhnzl2vg8r8raia.htm)|Revenant Reload|Revenant Reload|modificada|
|[ekMIs1v4IBJG2qEg.htm](book-of-the-dead-bestiary-items/ekMIs1v4IBJG2qEg.htm)|Create Zombies|Elaborar Zombies|modificada|
|[enVGATk5fYLDSoEA.htm](book-of-the-dead-bestiary-items/enVGATk5fYLDSoEA.htm)|Negative Healing|Curación negativa|modificada|
|[eO23hQJnoj5vQUSL.htm](book-of-the-dead-bestiary-items/eO23hQJnoj5vQUSL.htm)|Negative Healing|Curación negativa|modificada|
|[EPaipnmixMdtaUeW.htm](book-of-the-dead-bestiary-items/EPaipnmixMdtaUeW.htm)|Negative Healing|Curación negativa|modificada|
|[eq0chkkfcwtghv3v.htm](book-of-the-dead-bestiary-items/eq0chkkfcwtghv3v.htm)|Horn|Cuerno|modificada|
|[EqqrJxTxtfnQL9mm.htm](book-of-the-dead-bestiary-items/EqqrJxTxtfnQL9mm.htm)|Sudden Siphon|Sudden Siphon|modificada|
|[equod2o7os2wz0wr.htm](book-of-the-dead-bestiary-items/equod2o7os2wz0wr.htm)|Claw|Garra|modificada|
|[es9ibq62fqcl30pn.htm](book-of-the-dead-bestiary-items/es9ibq62fqcl30pn.htm)|Jaws|Fauces|modificada|
|[eu6xmtw27h5dt4bt.htm](book-of-the-dead-bestiary-items/eu6xmtw27h5dt4bt.htm)|Rapier|Estoque|modificada|
|[EUiVBFw40fh2AfU0.htm](book-of-the-dead-bestiary-items/EUiVBFw40fh2AfU0.htm)|Negative Healing|Curación negativa|modificada|
|[EuLz7D8KQPASHSbM.htm](book-of-the-dead-bestiary-items/EuLz7D8KQPASHSbM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[EXqKm6CdVt3ufYyT.htm](book-of-the-dead-bestiary-items/EXqKm6CdVt3ufYyT.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[eyfrpq3bvxnh075k.htm](book-of-the-dead-bestiary-items/eyfrpq3bvxnh075k.htm)|Silent Aura|Aura silenciosa|modificada|
|[eymmc9qbqyp94ywu.htm](book-of-the-dead-bestiary-items/eymmc9qbqyp94ywu.htm)|Inspire the Faithless|Inspirar a los infieles|modificada|
|[ezj7d0ajdqts6n00.htm](book-of-the-dead-bestiary-items/ezj7d0ajdqts6n00.htm)|Tail|Tail|modificada|
|[f006egqkkbtjuqgv.htm](book-of-the-dead-bestiary-items/f006egqkkbtjuqgv.htm)|Blinding Breath|Aliento cegador|modificada|
|[f303t130pkbopzk0.htm](book-of-the-dead-bestiary-items/f303t130pkbopzk0.htm)|Jaws|Fauces|modificada|
|[f3HhM7YpmtEJwP4y.htm](book-of-the-dead-bestiary-items/f3HhM7YpmtEJwP4y.htm)|Negative Healing|Curación negativa|modificada|
|[f55now7bu1g173vg.htm](book-of-the-dead-bestiary-items/f55now7bu1g173vg.htm)|Entropy's Shadow|Sombra de Entropía|modificada|
|[f84cxpe3e8us7653.htm](book-of-the-dead-bestiary-items/f84cxpe3e8us7653.htm)|Claw|Garra|modificada|
|[fba14HId1lw0Kex9.htm](book-of-the-dead-bestiary-items/fba14HId1lw0Kex9.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FByIi64kp8hJjz6z.htm](book-of-the-dead-bestiary-items/FByIi64kp8hJjz6z.htm)|Tremorsense (Imprecise) 60 feet|Sentido del Temblor (Impreciso) 60 pies|modificada|
|[fczuoyvm5wqij2to.htm](book-of-the-dead-bestiary-items/fczuoyvm5wqij2to.htm)|Intense Heat|Calor intenso|modificada|
|[fet1zrdx8aapum04.htm](book-of-the-dead-bestiary-items/fet1zrdx8aapum04.htm)|Compression|Compresión|modificada|
|[Fg4PN7JgMiI91eH5.htm](book-of-the-dead-bestiary-items/Fg4PN7JgMiI91eH5.htm)|Negative Healing|Curación negativa|modificada|
|[FHXomEYheB5tF9Et.htm](book-of-the-dead-bestiary-items/FHXomEYheB5tF9Et.htm)|Aquatic Ambush|Emboscada acuática|modificada|
|[fIhgzWWiqFCcA6nV.htm](book-of-the-dead-bestiary-items/fIhgzWWiqFCcA6nV.htm)|Conjure Instruments|Instrumentos de conjuración|modificada|
|[fk6qtcr37ue5mbxw.htm](book-of-the-dead-bestiary-items/fk6qtcr37ue5mbxw.htm)|Terrible Foresight|Terrible presciencia|modificada|
|[FKLyRZ60trNAaU7a.htm](book-of-the-dead-bestiary-items/FKLyRZ60trNAaU7a.htm)|Negative Healing|Curación negativa|modificada|
|[fn7qja3kw9dw1kox.htm](book-of-the-dead-bestiary-items/fn7qja3kw9dw1kox.htm)|Incorporeal Wheel|Rueda incorpórea|modificada|
|[fp4p44l0wgn5altu.htm](book-of-the-dead-bestiary-items/fp4p44l0wgn5altu.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[FRqXWcdzhlxU8sMh.htm](book-of-the-dead-bestiary-items/FRqXWcdzhlxU8sMh.htm)|Darkvision|Visión en la oscuridad|modificada|
|[FRWKAh9vjYaNCqdT.htm](book-of-the-dead-bestiary-items/FRWKAh9vjYaNCqdT.htm)|Negative Healing|Curación negativa|modificada|
|[fSXafbSBzAADlGcn.htm](book-of-the-dead-bestiary-items/fSXafbSBzAADlGcn.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[FTPKGpv0QRKEpmnY.htm](book-of-the-dead-bestiary-items/FTPKGpv0QRKEpmnY.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[FUCaE5JHQFv1Gc0a.htm](book-of-the-dead-bestiary-items/FUCaE5JHQFv1Gc0a.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[fv0690n3y8ghmvd9.htm](book-of-the-dead-bestiary-items/fv0690n3y8ghmvd9.htm)|Claw|Garra|modificada|
|[fxc64qicfzi9l9lx.htm](book-of-the-dead-bestiary-items/fxc64qicfzi9l9lx.htm)|Rotten Fruit|Fruta podrida|modificada|
|[FzDSsJ65EnkMhwSx.htm](book-of-the-dead-bestiary-items/FzDSsJ65EnkMhwSx.htm)|Negative Healing|Curación negativa|modificada|
|[fzpmews5ufzi1k6e.htm](book-of-the-dead-bestiary-items/fzpmews5ufzi1k6e.htm)|Strangle|Estrangular|modificada|
|[g3UMdUkuFL5RdKPP.htm](book-of-the-dead-bestiary-items/g3UMdUkuFL5RdKPP.htm)|Grab|Agarrado|modificada|
|[g9hvfpvweahguxgh.htm](book-of-the-dead-bestiary-items/g9hvfpvweahguxgh.htm)|Create Zombies|Elaborar Zombies|modificada|
|[ga6hycubofuo1zyf.htm](book-of-the-dead-bestiary-items/ga6hycubofuo1zyf.htm)|Claw|Garra|modificada|
|[gB214qjiG2XEJPBR.htm](book-of-the-dead-bestiary-items/gB214qjiG2XEJPBR.htm)|Darkvision|Visión en la oscuridad|modificada|
|[gB36aH5yDgmXH6xc.htm](book-of-the-dead-bestiary-items/gB36aH5yDgmXH6xc.htm)|Squeeze|Escurrirse|modificada|
|[gDkkvEZZSC6HPnDm.htm](book-of-the-dead-bestiary-items/gDkkvEZZSC6HPnDm.htm)|Command Zombie|Orden imperiosa Zombie|modificada|
|[gE5SOeGR6RJsEJJ1.htm](book-of-the-dead-bestiary-items/gE5SOeGR6RJsEJJ1.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[GFbwZSSnTHj0YPuv.htm](book-of-the-dead-bestiary-items/GFbwZSSnTHj0YPuv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ggz12biwbgr3mv1o.htm](book-of-the-dead-bestiary-items/ggz12biwbgr3mv1o.htm)|Lesser Mummy Rot|Pudrición menor de la momia|modificada|
|[ghtfq7ukz7gwxwsb.htm](book-of-the-dead-bestiary-items/ghtfq7ukz7gwxwsb.htm)|Draconic Frenzy|Frenesí dracónico|modificada|
|[gi4s0lkv3mfb9loi.htm](book-of-the-dead-bestiary-items/gi4s0lkv3mfb9loi.htm)|Fist|Puño|modificada|
|[girs49ehz5bzw3df.htm](book-of-the-dead-bestiary-items/girs49ehz5bzw3df.htm)|Anticipatory Attack|Ataque Anticipatorio|modificada|
|[gizkkpi42rqcq7rz.htm](book-of-the-dead-bestiary-items/gizkkpi42rqcq7rz.htm)|Jaws|Fauces|modificada|
|[GJBlX4Lhocg26ELX.htm](book-of-the-dead-bestiary-items/GJBlX4Lhocg26ELX.htm)|Negative Healing|Curación negativa|modificada|
|[gkW3gWYUJOzFKerr.htm](book-of-the-dead-bestiary-items/gkW3gWYUJOzFKerr.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[gocrv4xvdtl8n7hl.htm](book-of-the-dead-bestiary-items/gocrv4xvdtl8n7hl.htm)|Blazing Howl|Blazing Howl|modificada|
|[gomtgkod7rqvcpxq.htm](book-of-the-dead-bestiary-items/gomtgkod7rqvcpxq.htm)|Stamping Foot|Stamping Foot|modificada|
|[GrurNCdRZ7stOOB7.htm](book-of-the-dead-bestiary-items/GrurNCdRZ7stOOB7.htm)|Grab|Agarrado|modificada|
|[gsUuAStZr4pb7zis.htm](book-of-the-dead-bestiary-items/gsUuAStZr4pb7zis.htm)|Improved Grab|Agarrado mejorado|modificada|
|[GtDhQviwk4x4rmKZ.htm](book-of-the-dead-bestiary-items/GtDhQviwk4x4rmKZ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[gw4pcdfvx6elv0g3.htm](book-of-the-dead-bestiary-items/gw4pcdfvx6elv0g3.htm)|Siphon Faith|Siphon Faith|modificada|
|[gwzami23yywnrl99.htm](book-of-the-dead-bestiary-items/gwzami23yywnrl99.htm)|Slow|Lentificado/a|modificada|
|[GX7ZYuLk1vIcTVZQ.htm](book-of-the-dead-bestiary-items/GX7ZYuLk1vIcTVZQ.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[gy0jf8etpgdnoddm.htm](book-of-the-dead-bestiary-items/gy0jf8etpgdnoddm.htm)|Natural Invisibility|Invisibilidad Natural|modificada|
|[H0qYKDck9uqk3ZiC.htm](book-of-the-dead-bestiary-items/H0qYKDck9uqk3ZiC.htm)|Capture|Captura|modificada|
|[h189cx7txluockka.htm](book-of-the-dead-bestiary-items/h189cx7txluockka.htm)|Shadow Rapier|Estoque de Sombra|modificada|
|[h305evdhewgfdv8w.htm](book-of-the-dead-bestiary-items/h305evdhewgfdv8w.htm)|Field of Undeath|Campo de la Muerte|modificada|
|[H3cDChRjDRA71MNI.htm](book-of-the-dead-bestiary-items/H3cDChRjDRA71MNI.htm)|Lifesense (Imprecise) 60 feet|Sentido de la Vida (Impreciso) 60 pies|modificada|
|[h4i7zh8y17d35lou.htm](book-of-the-dead-bestiary-items/h4i7zh8y17d35lou.htm)|Jaws|Fauces|modificada|
|[h4iw6qojb9gael8e.htm](book-of-the-dead-bestiary-items/h4iw6qojb9gael8e.htm)|Talon|Talon|modificada|
|[h6ms55ripngjxrjb.htm](book-of-the-dead-bestiary-items/h6ms55ripngjxrjb.htm)|Eroding Touch|Eroding Touch|modificada|
|[h7zcdyu7ovmxa2ya.htm](book-of-the-dead-bestiary-items/h7zcdyu7ovmxa2ya.htm)|Claw|Garra|modificada|
|[h9Qhc5vy2FgrUSrb.htm](book-of-the-dead-bestiary-items/h9Qhc5vy2FgrUSrb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[H9XeL3LQ06KgkEE1.htm](book-of-the-dead-bestiary-items/H9XeL3LQ06KgkEE1.htm)|Frenzy|Frenzy|modificada|
|[HeQVQ64EF8HjzyyO.htm](book-of-the-dead-bestiary-items/HeQVQ64EF8HjzyyO.htm)|Knockdown|Derribo|modificada|
|[hG2ocnUMjZCucqB2.htm](book-of-the-dead-bestiary-items/hG2ocnUMjZCucqB2.htm)|Horn|Cuerno|modificada|
|[hGLTi7qRn9Hbn8VM.htm](book-of-the-dead-bestiary-items/hGLTi7qRn9Hbn8VM.htm)|Rigor Mortis|Rigor Mortis|modificada|
|[HgX8EdBcuekHoaYB.htm](book-of-the-dead-bestiary-items/HgX8EdBcuekHoaYB.htm)|Constrict|Restringir|modificada|
|[hJqwMBdBQecpuWU8.htm](book-of-the-dead-bestiary-items/hJqwMBdBQecpuWU8.htm)|Negative Healing|Curación negativa|modificada|
|[hK1tPnyMOt0g3STo.htm](book-of-the-dead-bestiary-items/hK1tPnyMOt0g3STo.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[hma3kv808zk32z1k.htm](book-of-the-dead-bestiary-items/hma3kv808zk32z1k.htm)|Spear|Lanza|modificada|
|[hndwksut71ohgybj.htm](book-of-the-dead-bestiary-items/hndwksut71ohgybj.htm)|Midnight Depths|Midnight Depths|modificada|
|[hrwH6CDrMAcUOAA4.htm](book-of-the-dead-bestiary-items/hrwH6CDrMAcUOAA4.htm)|Constant Spells|Constant Spells|modificada|
|[htqhazfmsy64d5iz.htm](book-of-the-dead-bestiary-items/htqhazfmsy64d5iz.htm)|Entropy's Shadow|Sombra de Entropía|modificada|
|[HUgw20uZ51R7a9a4.htm](book-of-the-dead-bestiary-items/HUgw20uZ51R7a9a4.htm)|Topple Furniture|Topple Furniture|modificada|
|[HUIxZptlwIab6WRD.htm](book-of-the-dead-bestiary-items/HUIxZptlwIab6WRD.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hvmeWsfKjMqgYhBi.htm](book-of-the-dead-bestiary-items/hvmeWsfKjMqgYhBi.htm)|Negative Healing|Curación negativa|modificada|
|[HWy6SULhipZ1fnRK.htm](book-of-the-dead-bestiary-items/HWy6SULhipZ1fnRK.htm)|Darkvision|Visión en la oscuridad|modificada|
|[hz33scglipei6wyh.htm](book-of-the-dead-bestiary-items/hz33scglipei6wyh.htm)|Claw|Garra|modificada|
|[hz9E9TnkviNuCCnq.htm](book-of-the-dead-bestiary-items/hz9E9TnkviNuCCnq.htm)|Gasp|Gasp|modificada|
|[HZCvfp07Tr6TQA6s.htm](book-of-the-dead-bestiary-items/HZCvfp07Tr6TQA6s.htm)|Darkvision|Visión en la oscuridad|modificada|
|[i2zp6vcnxl3idh5a.htm](book-of-the-dead-bestiary-items/i2zp6vcnxl3idh5a.htm)|Tail|Tail|modificada|
|[i3b5R0drNc9jYAxQ.htm](book-of-the-dead-bestiary-items/i3b5R0drNc9jYAxQ.htm)|Monk Ki Spells|Conjuros de ki del monje|modificada|
|[i45nUr4U6ijZvV0X.htm](book-of-the-dead-bestiary-items/i45nUr4U6ijZvV0X.htm)|Negative Healing|Curación negativa|modificada|
|[i5QEKnRQOxdbucFa.htm](book-of-the-dead-bestiary-items/i5QEKnRQOxdbucFa.htm)|Grab|Agarrado|modificada|
|[i7d6smiJzgZMN4eM.htm](book-of-the-dead-bestiary-items/i7d6smiJzgZMN4eM.htm)|Negative Healing|Curación negativa|modificada|
|[IAkmYPAen89nAVOl.htm](book-of-the-dead-bestiary-items/IAkmYPAen89nAVOl.htm)|Constant Spells|Constant Spells|modificada|
|[ibAx8T7dlGLGUNfN.htm](book-of-the-dead-bestiary-items/ibAx8T7dlGLGUNfN.htm)|Negative Healing|Curación negativa|modificada|
|[Icnp6B80vyyxB58y.htm](book-of-the-dead-bestiary-items/Icnp6B80vyyxB58y.htm)|Lifesense 30 feet|Lifesense 30 pies|modificada|
|[ie93zawg4aydi426.htm](book-of-the-dead-bestiary-items/ie93zawg4aydi426.htm)|Longsword|Longsword|modificada|
|[IeIZXKmchA7BgVL3.htm](book-of-the-dead-bestiary-items/IeIZXKmchA7BgVL3.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[ifsi8iYNheUHJYSo.htm](book-of-the-dead-bestiary-items/ifsi8iYNheUHJYSo.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[igdqv17bti9ke6yv.htm](book-of-the-dead-bestiary-items/igdqv17bti9ke6yv.htm)|Dead Shot|Disparo mortal|modificada|
|[IgpRQqNu2wC1srux.htm](book-of-the-dead-bestiary-items/IgpRQqNu2wC1srux.htm)|Negative Healing|Curación negativa|modificada|
|[igSuS5pwnNIqzoiZ.htm](book-of-the-dead-bestiary-items/igSuS5pwnNIqzoiZ.htm)|Negative Healing|Curación negativa|modificada|
|[IhOPZKCB3vAG0fWj.htm](book-of-the-dead-bestiary-items/IhOPZKCB3vAG0fWj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[IiHW4mzCpj9P0zUh.htm](book-of-the-dead-bestiary-items/IiHW4mzCpj9P0zUh.htm)|Negative Healing|Curación negativa|modificada|
|[ijsqkg9v5y89e7nf.htm](book-of-the-dead-bestiary-items/ijsqkg9v5y89e7nf.htm)|Scream in Agony|Grito de agonía|modificada|
|[IMlXx0dxH2pyVYkZ.htm](book-of-the-dead-bestiary-items/IMlXx0dxH2pyVYkZ.htm)|Negative Healing|Curación negativa|modificada|
|[ioRiyfYOVSBwJU1I.htm](book-of-the-dead-bestiary-items/ioRiyfYOVSBwJU1I.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[ipzqyff0deghe3l9.htm](book-of-the-dead-bestiary-items/ipzqyff0deghe3l9.htm)|Funereal Touch|Toque fúnebre|modificada|
|[iqh47015p1pztflo.htm](book-of-the-dead-bestiary-items/iqh47015p1pztflo.htm)|Force Feed|Alimentarse a la fuerza|modificada|
|[irqrb4xjw4fid7ki.htm](book-of-the-dead-bestiary-items/irqrb4xjw4fid7ki.htm)|Fist|Puño|modificada|
|[isudkibiz4ctaaai.htm](book-of-the-dead-bestiary-items/isudkibiz4ctaaai.htm)|Mace|Maza|modificada|
|[IvNNdk1lrY4Xyd5o.htm](book-of-the-dead-bestiary-items/IvNNdk1lrY4Xyd5o.htm)|Troop Movement|Movimiento de tropas|modificada|
|[IWjuKGNq99Q9FpWw.htm](book-of-the-dead-bestiary-items/IWjuKGNq99Q9FpWw.htm)|Negative Healing|Curación negativa|modificada|
|[j0godn9ynpr21ye8.htm](book-of-the-dead-bestiary-items/j0godn9ynpr21ye8.htm)|Possess Animal|Poseer Animal|modificada|
|[j0tNYHtR2k2ufUJL.htm](book-of-the-dead-bestiary-items/j0tNYHtR2k2ufUJL.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[jajq24ckigdi8yla.htm](book-of-the-dead-bestiary-items/jajq24ckigdi8yla.htm)|Rosebriar Lash|Rosebriar Lash|modificada|
|[jdhshd6p6earqvr9.htm](book-of-the-dead-bestiary-items/jdhshd6p6earqvr9.htm)|Waves of Sorrow|Waves of Sorrow|modificada|
|[je6z12w9bwlsz6kk.htm](book-of-the-dead-bestiary-items/je6z12w9bwlsz6kk.htm)|Incessant Goading|Incessant Goading|modificada|
|[jeo96qnICvlBhjJ0.htm](book-of-the-dead-bestiary-items/jeo96qnICvlBhjJ0.htm)|+1 Status to All Saves vs. Magic|+1 situación a todas las salvaciones contra magia.|modificada|
|[jfD7HOJyLqdUeAsO.htm](book-of-the-dead-bestiary-items/jfD7HOJyLqdUeAsO.htm)|Negative Healing|Curación negativa|modificada|
|[jjm9aum1hxm0xf4c.htm](book-of-the-dead-bestiary-items/jjm9aum1hxm0xf4c.htm)|Command Zombie|Orden imperiosa Zombie|modificada|
|[JLuKV6PpCsAJSsQD.htm](book-of-the-dead-bestiary-items/JLuKV6PpCsAJSsQD.htm)|Champion Devotion Spells|Hechizos de devoción de campeones.|modificada|
|[jmhsum5l0sk7d9ru.htm](book-of-the-dead-bestiary-items/jmhsum5l0sk7d9ru.htm)|Polong Possession|Posesión de Polong|modificada|
|[JNoGBCYpptNAJ1M0.htm](book-of-the-dead-bestiary-items/JNoGBCYpptNAJ1M0.htm)|Shatter|Estallar|modificada|
|[jQEBpMjZVXEPDHQy.htm](book-of-the-dead-bestiary-items/jQEBpMjZVXEPDHQy.htm)|Drain Thoughts|Drenar Pensamientos|modificada|
|[jVvenC8VtGesd99v.htm](book-of-the-dead-bestiary-items/jVvenC8VtGesd99v.htm)|Graveknight's Curse|Maldición del caballero sepulcral|modificada|
|[jWKQduPw868WuVOj.htm](book-of-the-dead-bestiary-items/jWKQduPw868WuVOj.htm)|Negative Healing|Curación negativa|modificada|
|[JxEmM6BuyP2WDNYs.htm](book-of-the-dead-bestiary-items/JxEmM6BuyP2WDNYs.htm)|Grab|Agarrado|modificada|
|[k55F7rzQpnXtBEJV.htm](book-of-the-dead-bestiary-items/k55F7rzQpnXtBEJV.htm)|Rend|Rasgadura|modificada|
|[k7dcbgnjv5bmyic2.htm](book-of-the-dead-bestiary-items/k7dcbgnjv5bmyic2.htm)|Targeted Collapse|Designado como objetivo Colapso|modificada|
|[KB3dAuCJuP1fvJj1.htm](book-of-the-dead-bestiary-items/KB3dAuCJuP1fvJj1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KfMljjVvrcltFSTJ.htm](book-of-the-dead-bestiary-items/KfMljjVvrcltFSTJ.htm)|Negative Healing|Curación negativa|modificada|
|[kfufm3rf9agn7hj4.htm](book-of-the-dead-bestiary-items/kfufm3rf9agn7hj4.htm)|Hand|Mano|modificada|
|[KGi4YZQdbv764frU.htm](book-of-the-dead-bestiary-items/KGi4YZQdbv764frU.htm)|Trample|Trample|modificada|
|[khs8jsznmljq10no.htm](book-of-the-dead-bestiary-items/khs8jsznmljq10no.htm)|Talon|Talon|modificada|
|[ki62sc8x79ypbv1d.htm](book-of-the-dead-bestiary-items/ki62sc8x79ypbv1d.htm)|Foot|Pie|modificada|
|[kijygqC36kPqtu7H.htm](book-of-the-dead-bestiary-items/kijygqC36kPqtu7H.htm)|Darkvision|Visión en la oscuridad|modificada|
|[KKDdzLVqG8oTmkVx.htm](book-of-the-dead-bestiary-items/KKDdzLVqG8oTmkVx.htm)|Shield Block|Bloquear con escudo|modificada|
|[kl1jb22cgwiuukv9.htm](book-of-the-dead-bestiary-items/kl1jb22cgwiuukv9.htm)|Draining Gaze|Mirada drenante|modificada|
|[kpogcinxfxdtnd5j.htm](book-of-the-dead-bestiary-items/kpogcinxfxdtnd5j.htm)|Gallow Curse|Gallow Curse|modificada|
|[kPpdhRglPpemLa6O.htm](book-of-the-dead-bestiary-items/kPpdhRglPpemLa6O.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[krx5EeEB0stlZv3X.htm](book-of-the-dead-bestiary-items/krx5EeEB0stlZv3X.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[KY0t4zPgTeWUaAUM.htm](book-of-the-dead-bestiary-items/KY0t4zPgTeWUaAUM.htm)|Swift Leap|Salto veloz|modificada|
|[L0BqA6ioP9NTqswd.htm](book-of-the-dead-bestiary-items/L0BqA6ioP9NTqswd.htm)|Negative Healing|Curación negativa|modificada|
|[L4ewcxHDWUZozOIs.htm](book-of-the-dead-bestiary-items/L4ewcxHDWUZozOIs.htm)|Jiang-Shi Vulnerabilities|Jiang-Shi Vulnerabilidades|modificada|
|[l525qc20u09lsupe.htm](book-of-the-dead-bestiary-items/l525qc20u09lsupe.htm)|Rhapsodic Flourish|Floritura rapsódica|modificada|
|[l7w2peurzgbblvcv.htm](book-of-the-dead-bestiary-items/l7w2peurzgbblvcv.htm)|Bottle Bound|Bottle Bound|modificada|
|[L95LvvfAZVKzDqz8.htm](book-of-the-dead-bestiary-items/L95LvvfAZVKzDqz8.htm)|Occult Innate Spells|Ocultismo Hechizos Innatos|modificada|
|[ldn7og162ohnumw1.htm](book-of-the-dead-bestiary-items/ldn7og162ohnumw1.htm)|Lignifying Hand|Lignifying Hand|modificada|
|[LiEmzWVKW20OJoEm.htm](book-of-the-dead-bestiary-items/LiEmzWVKW20OJoEm.htm)|Darkvision|Visión en la oscuridad|modificada|
|[LigXwWUfSvwcEKm4.htm](book-of-the-dead-bestiary-items/LigXwWUfSvwcEKm4.htm)|Thoughtsense (Precise) 100 feet|Percibir pensamientos (precisión) 100 pies.|modificada|
|[lj7qJagCUYgoGe76.htm](book-of-the-dead-bestiary-items/lj7qJagCUYgoGe76.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[lLCa9xf3PhXLdbdq.htm](book-of-the-dead-bestiary-items/lLCa9xf3PhXLdbdq.htm)|Darkvision|Visión en la oscuridad|modificada|
|[llsd8tyjckikjmsy.htm](book-of-the-dead-bestiary-items/llsd8tyjckikjmsy.htm)|Fist|Puño|modificada|
|[lo0krhkeahlcxl4k.htm](book-of-the-dead-bestiary-items/lo0krhkeahlcxl4k.htm)|Servitor Lunge|Acometer Servidor|modificada|
|[loe7wunkt2aw95fw.htm](book-of-the-dead-bestiary-items/loe7wunkt2aw95fw.htm)|Blight|Asolar|modificada|
|[ls7l1xs99s4qiknb.htm](book-of-the-dead-bestiary-items/ls7l1xs99s4qiknb.htm)|Pallid Touch|Pallid Touch|modificada|
|[lsjXlNKiGE8mRABo.htm](book-of-the-dead-bestiary-items/lsjXlNKiGE8mRABo.htm)|Darkvision|Visión en la oscuridad|modificada|
|[m3t78jldn1s4cro7.htm](book-of-the-dead-bestiary-items/m3t78jldn1s4cro7.htm)|Brain Rot|Brain Rot|modificada|
|[M5lSM9eRGm6vSvNd.htm](book-of-the-dead-bestiary-items/M5lSM9eRGm6vSvNd.htm)|Negative Healing|Curación negativa|modificada|
|[M8YAKZOpPsPwvANR.htm](book-of-the-dead-bestiary-items/M8YAKZOpPsPwvANR.htm)|Negative Healing|Curación negativa|modificada|
|[mAgSJlfDWhy4ZhCH.htm](book-of-the-dead-bestiary-items/mAgSJlfDWhy4ZhCH.htm)|Drain Thoughts|Drenar Pensamientos|modificada|
|[mc334q1nfka7sngl.htm](book-of-the-dead-bestiary-items/mc334q1nfka7sngl.htm)|Fire Fist|Puño de Fuego|modificada|
|[mCgB8TMnu1WQOtQJ.htm](book-of-the-dead-bestiary-items/mCgB8TMnu1WQOtQJ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[md6wPL6GWR80z2Z6.htm](book-of-the-dead-bestiary-items/md6wPL6GWR80z2Z6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ME1twBqe6m8JvEtv.htm](book-of-the-dead-bestiary-items/ME1twBqe6m8JvEtv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MFbENdEsj2e5TGOd.htm](book-of-the-dead-bestiary-items/MFbENdEsj2e5TGOd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[MFUrmw36LIlgw5UX.htm](book-of-the-dead-bestiary-items/MFUrmw36LIlgw5UX.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[mgzsxl5d5chh9f17.htm](book-of-the-dead-bestiary-items/mgzsxl5d5chh9f17.htm)|Brew Tomb Juice|Brew Tomb Juice|modificada|
|[mHzpQhYPnyDQgnCL.htm](book-of-the-dead-bestiary-items/mHzpQhYPnyDQgnCL.htm)|Chains of the Dead|Cadenas de los Muertos|modificada|
|[MIBK76pXVm7hAU66.htm](book-of-the-dead-bestiary-items/MIBK76pXVm7hAU66.htm)|Thoughtsense (Precise) 100 feet|Percibir pensamientos (precisión) 100 pies.|modificada|
|[mjju0x3kql87c8d3.htm](book-of-the-dead-bestiary-items/mjju0x3kql87c8d3.htm)|Sacrilegious Aura|Sacrilegious Aura|modificada|
|[mjX6AVWz9h31QQlj.htm](book-of-the-dead-bestiary-items/mjX6AVWz9h31QQlj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[mkg6oo3hwvllmvw3.htm](book-of-the-dead-bestiary-items/mkg6oo3hwvllmvw3.htm)|Victory Celebration|Celebración de la victoria|modificada|
|[mkicpd4bhua50de0.htm](book-of-the-dead-bestiary-items/mkicpd4bhua50de0.htm)|Claw|Garra|modificada|
|[mlnuhck97ts6m986.htm](book-of-the-dead-bestiary-items/mlnuhck97ts6m986.htm)|Tail|Tail|modificada|
|[mlytlp3vwdiv4xev.htm](book-of-the-dead-bestiary-items/mlytlp3vwdiv4xev.htm)|Rise Again|Rise Again|modificada|
|[mmLtzGT7Xi5ll4Cd.htm](book-of-the-dead-bestiary-items/mmLtzGT7Xi5ll4Cd.htm)|Attack of Opportunity|Ataque de oportunidad|modificada|
|[mn56u513jsfshjfz.htm](book-of-the-dead-bestiary-items/mn56u513jsfshjfz.htm)|Machete|Machete|modificada|
|[mo4zs1e4rvsueimw.htm](book-of-the-dead-bestiary-items/mo4zs1e4rvsueimw.htm)|Jaws|Fauces|modificada|
|[mp4tj1xv12frp13e.htm](book-of-the-dead-bestiary-items/mp4tj1xv12frp13e.htm)|Final Blasphemy|Blasfemia final|modificada|
|[MRiU0bmGLDherRXE.htm](book-of-the-dead-bestiary-items/MRiU0bmGLDherRXE.htm)|Grab|Agarrado|modificada|
|[msrjzpke2pxhz0yk.htm](book-of-the-dead-bestiary-items/msrjzpke2pxhz0yk.htm)|Sand Vision|Visión de Arena|modificada|
|[msy9ijw3hmhu6ppi.htm](book-of-the-dead-bestiary-items/msy9ijw3hmhu6ppi.htm)|Staff|Báculo|modificada|
|[MuhMVZGSGDdlWbDo.htm](book-of-the-dead-bestiary-items/MuhMVZGSGDdlWbDo.htm)|Shove|Empujar|modificada|
|[mvCicXdmNRrAJwTJ.htm](book-of-the-dead-bestiary-items/mvCicXdmNRrAJwTJ.htm)|Rigor Mortis|Rigor Mortis|modificada|
|[mW6gZ5jbhxJRUo7Y.htm](book-of-the-dead-bestiary-items/mW6gZ5jbhxJRUo7Y.htm)|Negative Healing|Curación negativa|modificada|
|[mx6sscnvhtaiutid.htm](book-of-the-dead-bestiary-items/mx6sscnvhtaiutid.htm)|Meant to Live|Meant to Live|modificada|
|[MYPGBIYRPHho61zG.htm](book-of-the-dead-bestiary-items/MYPGBIYRPHho61zG.htm)|Ghoul Fever|Ghoul Fever|modificada|
|[n2cq3t3kbwnbsjkm.htm](book-of-the-dead-bestiary-items/n2cq3t3kbwnbsjkm.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[n3kan97tvbssofit.htm](book-of-the-dead-bestiary-items/n3kan97tvbssofit.htm)|Servitor Attack|Servitor Attack|modificada|
|[n468FPlhvooypdYU.htm](book-of-the-dead-bestiary-items/n468FPlhvooypdYU.htm)|Darkvision|Visión en la oscuridad|modificada|
|[n53sw596l0uo2mmy.htm](book-of-the-dead-bestiary-items/n53sw596l0uo2mmy.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[n5oh9urk0vokvmfv.htm](book-of-the-dead-bestiary-items/n5oh9urk0vokvmfv.htm)|Claw|Garra|modificada|
|[N5PKTuC1nPKbFmU9.htm](book-of-the-dead-bestiary-items/N5PKTuC1nPKbFmU9.htm)|Primal Prepared Spells|Hechizos Preparados Primal|modificada|
|[n7PNu3mHe1gYDzMO.htm](book-of-the-dead-bestiary-items/n7PNu3mHe1gYDzMO.htm)|Fast Healing 10|Curación rápida|modificada|
|[n8a2azRJSqQCGbOL.htm](book-of-the-dead-bestiary-items/n8a2azRJSqQCGbOL.htm)|Negative Healing|Curación negativa|modificada|
|[Nca0UI8dL6FmQNm5.htm](book-of-the-dead-bestiary-items/Nca0UI8dL6FmQNm5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ncmh5gpidrqgeqj5.htm](book-of-the-dead-bestiary-items/ncmh5gpidrqgeqj5.htm)|Negative Ray|Negative Ray|modificada|
|[NcuughOSy9EI249D.htm](book-of-the-dead-bestiary-items/NcuughOSy9EI249D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ndmnLmpNGYMdIz2H.htm](book-of-the-dead-bestiary-items/ndmnLmpNGYMdIz2H.htm)|Jaws|Fauces|modificada|
|[ngnwinz7rvkh1cvw.htm](book-of-the-dead-bestiary-items/ngnwinz7rvkh1cvw.htm)|Bound|Bound|modificada|
|[NIIRwklz4cR5uYjX.htm](book-of-the-dead-bestiary-items/NIIRwklz4cR5uYjX.htm)|Negative Healing|Curación negativa|modificada|
|[NjGL58OXlG8alhDr.htm](book-of-the-dead-bestiary-items/NjGL58OXlG8alhDr.htm)|Fast Healing 10|Curación rápida 10|modificada|
|[NkZfjGZXJvvzsjuj.htm](book-of-the-dead-bestiary-items/NkZfjGZXJvvzsjuj.htm)|Primal Focus Spells|Conjuros de foco primigenio|modificada|
|[nltFl2TPZ3VCCV0z.htm](book-of-the-dead-bestiary-items/nltFl2TPZ3VCCV0z.htm)|Darkvision|Visión en la oscuridad|modificada|
|[NrspwQc7xJGzHSfc.htm](book-of-the-dead-bestiary-items/NrspwQc7xJGzHSfc.htm)|Phantom Mount|Phantom Mount|modificada|
|[nua4eplxdmlfifpv.htm](book-of-the-dead-bestiary-items/nua4eplxdmlfifpv.htm)|Dance with Death|Danzante con la Muerte|modificada|
|[nwwxqa7o7t95veoj.htm](book-of-the-dead-bestiary-items/nwwxqa7o7t95veoj.htm)|Living Visage|Visaje Viviente|modificada|
|[nXUOVxkr2qzcOOP6.htm](book-of-the-dead-bestiary-items/nXUOVxkr2qzcOOP6.htm)|Negative Healing|Curación negativa|modificada|
|[nYA9w4d89kX8zkyS.htm](book-of-the-dead-bestiary-items/nYA9w4d89kX8zkyS.htm)|Swallow Whole|Engullir Todo|modificada|
|[nYgcNR6KtQIwzTTP.htm](book-of-the-dead-bestiary-items/nYgcNR6KtQIwzTTP.htm)|Shield Block|Bloquear con escudo|modificada|
|[O1MGe9IusxQ8m5IK.htm](book-of-the-dead-bestiary-items/O1MGe9IusxQ8m5IK.htm)|Scent (Imprecise) 60 feet|Olor (Impreciso) 60 pies|modificada|
|[o31uzfafqxg6retc.htm](book-of-the-dead-bestiary-items/o31uzfafqxg6retc.htm)|Jaws|Fauces|modificada|
|[o50ct5atowqid2nk.htm](book-of-the-dead-bestiary-items/o50ct5atowqid2nk.htm)|Claw|Garra|modificada|
|[o5vtcl40ekp9qv6n.htm](book-of-the-dead-bestiary-items/o5vtcl40ekp9qv6n.htm)|Starvation Aura|Starvation Aura|modificada|
|[o6I8Uh0qPM5ozyMW.htm](book-of-the-dead-bestiary-items/o6I8Uh0qPM5ozyMW.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[o7u2o263mumslgsb.htm](book-of-the-dead-bestiary-items/o7u2o263mumslgsb.htm)|War Flail|War Flail|modificada|
|[o82ozylu3ctjujip.htm](book-of-the-dead-bestiary-items/o82ozylu3ctjujip.htm)|Jaws|Fauces|modificada|
|[o8cbkspx01eskehx.htm](book-of-the-dead-bestiary-items/o8cbkspx01eskehx.htm)|Terrifying Laugh|Risa aterradora|modificada|
|[obhnbsaze5pkt16e.htm](book-of-the-dead-bestiary-items/obhnbsaze5pkt16e.htm)|Osseous Defense|Defensa Ósea|modificada|
|[objWEl4wPiQkNnho.htm](book-of-the-dead-bestiary-items/objWEl4wPiQkNnho.htm)|Negative Healing|Curación negativa|modificada|
|[oc46s540bthxaecp.htm](book-of-the-dead-bestiary-items/oc46s540bthxaecp.htm)|Combat Current|Combat Current|modificada|
|[oer9h35wciqfjj68.htm](book-of-the-dead-bestiary-items/oer9h35wciqfjj68.htm)|Claw|Garra|modificada|
|[OFQw1wCzWACEpFPh.htm](book-of-the-dead-bestiary-items/OFQw1wCzWACEpFPh.htm)|Negative Healing|Curación negativa|modificada|
|[ohpwpe4hf1byw99z.htm](book-of-the-dead-bestiary-items/ohpwpe4hf1byw99z.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[olrz72ebabiexxj8.htm](book-of-the-dead-bestiary-items/olrz72ebabiexxj8.htm)|Synaptic Overload|Sobrecarga sináptica|modificada|
|[om967vglnfob32of.htm](book-of-the-dead-bestiary-items/om967vglnfob32of.htm)|Frightful Battle Cry|Grito de guerra espantoso|modificada|
|[ombjfkyxjmwbb9xc.htm](book-of-the-dead-bestiary-items/ombjfkyxjmwbb9xc.htm)|Drain Life|Drenar Vida|modificada|
|[onemB0KzXTHPiaTQ.htm](book-of-the-dead-bestiary-items/onemB0KzXTHPiaTQ.htm)|Darkvision|Visión en la oscuridad|modificada|
|[onsl5qalhafgwyhe.htm](book-of-the-dead-bestiary-items/onsl5qalhafgwyhe.htm)|Compression|Compresión|modificada|
|[oramesvya495nwdm.htm](book-of-the-dead-bestiary-items/oramesvya495nwdm.htm)|Shuriken|Shuriken|modificada|
|[OrGqVpvK9Ri1osng.htm](book-of-the-dead-bestiary-items/OrGqVpvK9Ri1osng.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[ovW00fb5RQ0fss2D.htm](book-of-the-dead-bestiary-items/ovW00fb5RQ0fss2D.htm)|Darkvision|Visión en la oscuridad|modificada|
|[p1j4vk8x9pqsovd6.htm](book-of-the-dead-bestiary-items/p1j4vk8x9pqsovd6.htm)|Sense Companion|Sense Companion|modificada|
|[p2homhgdk4xiy7ve.htm](book-of-the-dead-bestiary-items/p2homhgdk4xiy7ve.htm)|Breath Weapon|Breath Weapon|modificada|
|[p2rb04uhqbleqtpy.htm](book-of-the-dead-bestiary-items/p2rb04uhqbleqtpy.htm)|Claw|Garra|modificada|
|[p3fyg7e66y8gq61g.htm](book-of-the-dead-bestiary-items/p3fyg7e66y8gq61g.htm)|Claw|Garra|modificada|
|[p44HhukqlJ7IVvti.htm](book-of-the-dead-bestiary-items/p44HhukqlJ7IVvti.htm)|Negative Healing|Curación negativa|modificada|
|[P5HJ6L1BVQCeHoJr.htm](book-of-the-dead-bestiary-items/P5HJ6L1BVQCeHoJr.htm)|+18 to Initiative|+18 a Iniciativa|modificada|
|[p5ohcfvydik0i2mz.htm](book-of-the-dead-bestiary-items/p5ohcfvydik0i2mz.htm)|Hungry Armor|Hungry Armor|modificada|
|[p7BEvZc5WuJeZIJf.htm](book-of-the-dead-bestiary-items/p7BEvZc5WuJeZIJf.htm)|Constrict|Restringir|modificada|
|[P7FUCg0CYhhrSYXp.htm](book-of-the-dead-bestiary-items/P7FUCg0CYhhrSYXp.htm)|Negative Healing|Curación negativa|modificada|
|[P9uhzDYGuPtmPcTt.htm](book-of-the-dead-bestiary-items/P9uhzDYGuPtmPcTt.htm)|Darkvision 60 feet|Visión en la oscuridad 60 pies.|modificada|
|[pbwnkebF8Azr64pn.htm](book-of-the-dead-bestiary-items/pbwnkebF8Azr64pn.htm)|Grab|Agarrado|modificada|
|[pcv2wjgq3skfdwr1.htm](book-of-the-dead-bestiary-items/pcv2wjgq3skfdwr1.htm)|Stench|Hedor|modificada|
|[Pd2a0LCF9zAkHizr.htm](book-of-the-dead-bestiary-items/Pd2a0LCF9zAkHizr.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[pdt8bn3rsl7rxryb.htm](book-of-the-dead-bestiary-items/pdt8bn3rsl7rxryb.htm)|Summon Weapon|Invocar Arma|modificada|
|[pdtNmk8Zc7lN4BFF.htm](book-of-the-dead-bestiary-items/pdtNmk8Zc7lN4BFF.htm)|Grab|Agarrado|modificada|
|[piqtoxtcbhe4b1a1.htm](book-of-the-dead-bestiary-items/piqtoxtcbhe4b1a1.htm)|Great Despair|Gran desesperación|modificada|
|[pJdmn8co20iXokx1.htm](book-of-the-dead-bestiary-items/pJdmn8co20iXokx1.htm)|Negative Healing|Curación negativa|modificada|
|[pKH0djjdIbsZYSyj.htm](book-of-the-dead-bestiary-items/pKH0djjdIbsZYSyj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[plczq6dno7lmsveb.htm](book-of-the-dead-bestiary-items/plczq6dno7lmsveb.htm)|Adopt Guise|Adoptar Guise|modificada|
|[pm509slp2godzutv.htm](book-of-the-dead-bestiary-items/pm509slp2godzutv.htm)|Roll the Bones|Roll the Bones|modificada|
|[pQh6aSqjZQoy6EAa.htm](book-of-the-dead-bestiary-items/pQh6aSqjZQoy6EAa.htm)|The Upper Hand|The Upper Hand|modificada|
|[PQnmOAwTpUMykmR6.htm](book-of-the-dead-bestiary-items/PQnmOAwTpUMykmR6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[pst72akzo45b7a0z.htm](book-of-the-dead-bestiary-items/pst72akzo45b7a0z.htm)|Parry Dance|Parry Danza|modificada|
|[pvl5y6qz89i11vc5.htm](book-of-the-dead-bestiary-items/pvl5y6qz89i11vc5.htm)|Sense Murderer|Sense Murderer|modificada|
|[pYCh0bsqXGiKu08p.htm](book-of-the-dead-bestiary-items/pYCh0bsqXGiKu08p.htm)|Darkvision|Visión en la oscuridad|modificada|
|[q6iakq314is9gj9c.htm](book-of-the-dead-bestiary-items/q6iakq314is9gj9c.htm)|Ecstatic Ululation|Ecstatic Ululation|modificada|
|[qbupjn2tlljiraow.htm](book-of-the-dead-bestiary-items/qbupjn2tlljiraow.htm)|Stone Antler|Stone Antler|modificada|
|[qDKNtEMBCrIlKW6A.htm](book-of-the-dead-bestiary-items/qDKNtEMBCrIlKW6A.htm)|Blight Mastery|Maestría Asolar|modificada|
|[qmkVNjGJoAddHkl7.htm](book-of-the-dead-bestiary-items/qmkVNjGJoAddHkl7.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[qmXjR6UjkPsa5Yrc.htm](book-of-the-dead-bestiary-items/qmXjR6UjkPsa5Yrc.htm)|Improved Grab|Agarrado mejorado|modificada|
|[qo8ae1c96drn6owz.htm](book-of-the-dead-bestiary-items/qo8ae1c96drn6owz.htm)|Claw|Garra|modificada|
|[qqbbtn1cc3yyrpdv.htm](book-of-the-dead-bestiary-items/qqbbtn1cc3yyrpdv.htm)|Slow|Lentificado/a|modificada|
|[QQXKnrwMluEorlEb.htm](book-of-the-dead-bestiary-items/QQXKnrwMluEorlEb.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qrob277923ccvj5f.htm](book-of-the-dead-bestiary-items/qrob277923ccvj5f.htm)|Flicker|Flicker|modificada|
|[QRR9beKx0noS0RfN.htm](book-of-the-dead-bestiary-items/QRR9beKx0noS0RfN.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[QTvQkFjvczOYx7t8.htm](book-of-the-dead-bestiary-items/QTvQkFjvczOYx7t8.htm)|One More Breath|One More Breath|modificada|
|[qvgyve9ubbhtjhlb.htm](book-of-the-dead-bestiary-items/qvgyve9ubbhtjhlb.htm)|Consecration Vulnerability|Vulnerabilidad de Consagración|modificada|
|[qViybdc6hq6JwJNP.htm](book-of-the-dead-bestiary-items/qViybdc6hq6JwJNP.htm)|Warped Fulu|Warped Fulu|modificada|
|[qVmgf9hKv2Wt0x9f.htm](book-of-the-dead-bestiary-items/qVmgf9hKv2Wt0x9f.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[qWb2hnLxcNgrwqy7.htm](book-of-the-dead-bestiary-items/qWb2hnLxcNgrwqy7.htm)|Darkvision|Visión en la oscuridad|modificada|
|[qxv9k072qjlqqrmv.htm](book-of-the-dead-bestiary-items/qxv9k072qjlqqrmv.htm)|Bite Back|Muerdemuerde|modificada|
|[r1hdbf8k93avvj7u.htm](book-of-the-dead-bestiary-items/r1hdbf8k93avvj7u.htm)|Forest Guardian|Guardián del Bosque|modificada|
|[r2m3ybfe440pmnlu.htm](book-of-the-dead-bestiary-items/r2m3ybfe440pmnlu.htm)|Terrain Advantage|Ventaja de terreno|modificada|
|[r5xddqaz12nw9vmw.htm](book-of-the-dead-bestiary-items/r5xddqaz12nw9vmw.htm)|Muscular Leap|Salto sin carrerilla muscular|modificada|
|[R7D68innrOxLHJAm.htm](book-of-the-dead-bestiary-items/R7D68innrOxLHJAm.htm)|Musical Assault|Musical Assault|modificada|
|[r7kfGxLnvo6gvY8J.htm](book-of-the-dead-bestiary-items/r7kfGxLnvo6gvY8J.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[r867EAQMTLMUJiHC.htm](book-of-the-dead-bestiary-items/r867EAQMTLMUJiHC.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[r9tJVChsQaE6jEtS.htm](book-of-the-dead-bestiary-items/r9tJVChsQaE6jEtS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RaJiotddHOyL70ol.htm](book-of-the-dead-bestiary-items/RaJiotddHOyL70ol.htm)|Negative Healing|Curación negativa|modificada|
|[rat6z2dx8o2ocr0x.htm](book-of-the-dead-bestiary-items/rat6z2dx8o2ocr0x.htm)|Aura of Peace|Aura de Paz|modificada|
|[rfeIYZBHpVkv6qJZ.htm](book-of-the-dead-bestiary-items/rfeIYZBHpVkv6qJZ.htm)|Negative Healing|Curación negativa|modificada|
|[rfqiqYvSTPZWOWVq.htm](book-of-the-dead-bestiary-items/rfqiqYvSTPZWOWVq.htm)|Negative Healing|Curación negativa|modificada|
|[rfs2498hafjirg6i.htm](book-of-the-dead-bestiary-items/rfs2498hafjirg6i.htm)|Claw|Garra|modificada|
|[rh6570cau7s1l659.htm](book-of-the-dead-bestiary-items/rh6570cau7s1l659.htm)|Arm Spike|Espiga del brazo|modificada|
|[ri32o76yg6cyuq05.htm](book-of-the-dead-bestiary-items/ri32o76yg6cyuq05.htm)|Glaive|Glaive|modificada|
|[rIOKUxnAXsQC39t6.htm](book-of-the-dead-bestiary-items/rIOKUxnAXsQC39t6.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Rk6vaUzdBA05Ipr4.htm](book-of-the-dead-bestiary-items/Rk6vaUzdBA05Ipr4.htm)|Negative Healing|Curación negativa|modificada|
|[RKJRHH3TlsIfcjRw.htm](book-of-the-dead-bestiary-items/RKJRHH3TlsIfcjRw.htm)|Negative Healing|Curación negativa|modificada|
|[rNWhoVEyE4tbJ7UF.htm](book-of-the-dead-bestiary-items/rNWhoVEyE4tbJ7UF.htm)|Darkvision|Visión en la oscuridad|modificada|
|[ROMNXYjyp9b5wU7t.htm](book-of-the-dead-bestiary-items/ROMNXYjyp9b5wU7t.htm)|Darkvision|Visión en la oscuridad|modificada|
|[RpSDL9pRSQiu3lu9.htm](book-of-the-dead-bestiary-items/RpSDL9pRSQiu3lu9.htm)|Cleric Domain Spells|Hechizos de Dominio de Clérigo|modificada|
|[rpzk7dfngnq6h4fj.htm](book-of-the-dead-bestiary-items/rpzk7dfngnq6h4fj.htm)|Crossbow|Ballesta|modificada|
|[rsHuZ4AV9kbemHYN.htm](book-of-the-dead-bestiary-items/rsHuZ4AV9kbemHYN.htm)|Negative Healing|Curación negativa|modificada|
|[RsT9PJggBJ9E7yXV.htm](book-of-the-dead-bestiary-items/RsT9PJggBJ9E7yXV.htm)|Shut In|Shut In|modificada|
|[rt3cft7ln3rvinzi.htm](book-of-the-dead-bestiary-items/rt3cft7ln3rvinzi.htm)|Teleporting Clothesline|Teletransporte tendedero|modificada|
|[RvTQTNsY192UMAWY.htm](book-of-the-dead-bestiary-items/RvTQTNsY192UMAWY.htm)|Drain Qi|Drenar Qi|modificada|
|[RZ8nmKkTFCoNYpw1.htm](book-of-the-dead-bestiary-items/RZ8nmKkTFCoNYpw1.htm)|Negative Healing|Curación negativa|modificada|
|[RZU7C9rxkfFtWjAK.htm](book-of-the-dead-bestiary-items/RZU7C9rxkfFtWjAK.htm)|Negative Healing|Curación negativa|modificada|
|[S03qKeYKeO3g8bMB.htm](book-of-the-dead-bestiary-items/S03qKeYKeO3g8bMB.htm)|Darkvision|Visión en la oscuridad|modificada|
|[s0ilcnpw3egzb7jx.htm](book-of-the-dead-bestiary-items/s0ilcnpw3egzb7jx.htm)|Jaws|Fauces|modificada|
|[S5HKepuRcLZyCgzt.htm](book-of-the-dead-bestiary-items/S5HKepuRcLZyCgzt.htm)|Paralysis|Parálisis|modificada|
|[s6celq1ixxucp8ro.htm](book-of-the-dead-bestiary-items/s6celq1ixxucp8ro.htm)|Death Gasp|Death Gasp|modificada|
|[SAnGJYM6pCkTSTFE.htm](book-of-the-dead-bestiary-items/SAnGJYM6pCkTSTFE.htm)|Chomp|Chomp|modificada|
|[sbuu4mk2ssfmalco.htm](book-of-the-dead-bestiary-items/sbuu4mk2ssfmalco.htm)|Claw|Garra|modificada|
|[sbw8eejalnwiv9pt.htm](book-of-the-dead-bestiary-items/sbw8eejalnwiv9pt.htm)|Soulscent (Imprecise) 100 feet|Soulscent (Impreciso) 100 pies|modificada|
|[sDG67LzJ4UKhFyfv.htm](book-of-the-dead-bestiary-items/sDG67LzJ4UKhFyfv.htm)|Carve in Flesh|Tallar en Carne|modificada|
|[sE93R44GXNoE5W3Y.htm](book-of-the-dead-bestiary-items/sE93R44GXNoE5W3Y.htm)|Drain Qi|Drenar Qi|modificada|
|[SeDr8IsuA0cd6pKi.htm](book-of-the-dead-bestiary-items/SeDr8IsuA0cd6pKi.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[sfOhU9Ljw1nJ9Vw8.htm](book-of-the-dead-bestiary-items/sfOhU9Ljw1nJ9Vw8.htm)|Negative Healing|Curación negativa|modificada|
|[SfwWBcVieSEQcUwE.htm](book-of-the-dead-bestiary-items/SfwWBcVieSEQcUwE.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[sg28ok3v73dn8dvi.htm](book-of-the-dead-bestiary-items/sg28ok3v73dn8dvi.htm)|Snatch|Arrebatar|modificada|
|[sgrD5tLKLJMdGbYH.htm](book-of-the-dead-bestiary-items/sgrD5tLKLJMdGbYH.htm)|Improved Grab|Agarrado mejorado|modificada|
|[ShZ2CAIzf05F41B2.htm](book-of-the-dead-bestiary-items/ShZ2CAIzf05F41B2.htm)|Stop Heart|Detener Corazón|modificada|
|[sjrL9SViifyZvj1G.htm](book-of-the-dead-bestiary-items/sjrL9SViifyZvj1G.htm)|Negative Healing|Curación negativa|modificada|
|[so0db4mc0q95h2lm.htm](book-of-the-dead-bestiary-items/so0db4mc0q95h2lm.htm)|Enshroud|Enshroud|modificada|
|[SQt7rET9Z2MgoK8E.htm](book-of-the-dead-bestiary-items/SQt7rET9Z2MgoK8E.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[ssmbr5eqwybx9xne.htm](book-of-the-dead-bestiary-items/ssmbr5eqwybx9xne.htm)|Endless Suffering|Endless Suffering|modificada|
|[ssocwemmugpprhuc.htm](book-of-the-dead-bestiary-items/ssocwemmugpprhuc.htm)|Sunlight Powerlessness|Sunlight Powerlessness|modificada|
|[suMVXLkMmzCGv2fJ.htm](book-of-the-dead-bestiary-items/suMVXLkMmzCGv2fJ.htm)|Fist|Puño|modificada|
|[supp5wizpe79eqi9.htm](book-of-the-dead-bestiary-items/supp5wizpe79eqi9.htm)|Bloody Spew|Vómito Sangriento|modificada|
|[sUsdrW2eEA7nzp6P.htm](book-of-the-dead-bestiary-items/sUsdrW2eEA7nzp6P.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[SwCl0ytYvoRbPKKM.htm](book-of-the-dead-bestiary-items/SwCl0ytYvoRbPKKM.htm)|Darkvision|Visión en la oscuridad|modificada|
|[sWtjwSeVA2Yi9URn.htm](book-of-the-dead-bestiary-items/sWtjwSeVA2Yi9URn.htm)|Jaws|Fauces|modificada|
|[SXOUzgOVcsJXmJtF.htm](book-of-the-dead-bestiary-items/SXOUzgOVcsJXmJtF.htm)|Gather Spirit|Gather Spirit|modificada|
|[syR8yFxw3sNEGkUO.htm](book-of-the-dead-bestiary-items/syR8yFxw3sNEGkUO.htm)|Negative Healing|Curación negativa|modificada|
|[SZNvFrnofD8dddxU.htm](book-of-the-dead-bestiary-items/SZNvFrnofD8dddxU.htm)|Divine Prepared Spells|Hechizos Divinos Preparados|modificada|
|[t1x1x6iwe8eqpsxc.htm](book-of-the-dead-bestiary-items/t1x1x6iwe8eqpsxc.htm)|Staff|Báculo|modificada|
|[t2vkqgxwqps1lc2c.htm](book-of-the-dead-bestiary-items/t2vkqgxwqps1lc2c.htm)|Plant|Planta|modificada|
|[t31wSSg27UyZRToz.htm](book-of-the-dead-bestiary-items/t31wSSg27UyZRToz.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[t4dd2bxdturlg45u.htm](book-of-the-dead-bestiary-items/t4dd2bxdturlg45u.htm)|Flaying Flurry|Ráfaga Desolladora|modificada|
|[t57awjzu5thorizx.htm](book-of-the-dead-bestiary-items/t57awjzu5thorizx.htm)|Pistol Whip|Pistol Whip|modificada|
|[t6tofj5bxmm53nld.htm](book-of-the-dead-bestiary-items/t6tofj5bxmm53nld.htm)|Death Gasp|Death Gasp|modificada|
|[TA1sQhusAvQaJjnY.htm](book-of-the-dead-bestiary-items/TA1sQhusAvQaJjnY.htm)|Change Shape|Change Shape|modificada|
|[tabg0vo3elkeri2r.htm](book-of-the-dead-bestiary-items/tabg0vo3elkeri2r.htm)|Horrifying Screech|Horrifying Screech|modificada|
|[TCn4mhscYuvLcXc1.htm](book-of-the-dead-bestiary-items/TCn4mhscYuvLcXc1.htm)|Darkvision|Visión en la oscuridad|modificada|
|[TDyZOrJxLY1hcPKJ.htm](book-of-the-dead-bestiary-items/TDyZOrJxLY1hcPKJ.htm)|Drain Life|Drenar Vida|modificada|
|[thmdq28mehsfvjtu.htm](book-of-the-dead-bestiary-items/thmdq28mehsfvjtu.htm)|Severed Trunk|Severed Trunk|modificada|
|[thmuyoj3asul9w8q.htm](book-of-the-dead-bestiary-items/thmuyoj3asul9w8q.htm)|Claw|Garra|modificada|
|[TkNnlQ5Y9tP89lcL.htm](book-of-the-dead-bestiary-items/TkNnlQ5Y9tP89lcL.htm)|Desperate Meal|Desperate Meal|modificada|
|[TN9YVnHaKRk3uwTv.htm](book-of-the-dead-bestiary-items/TN9YVnHaKRk3uwTv.htm)|Negative Healing|Curación negativa|modificada|
|[tOSvo39Ofu2f5ufu.htm](book-of-the-dead-bestiary-items/tOSvo39Ofu2f5ufu.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[tskaoz4x2uhz0ss3.htm](book-of-the-dead-bestiary-items/tskaoz4x2uhz0ss3.htm)|Bone Debris|Restos óseos|modificada|
|[tult7kj40ygqn8tz.htm](book-of-the-dead-bestiary-items/tult7kj40ygqn8tz.htm)|Ghostly Grasp|Ghostly Grasp|modificada|
|[tvvsxwr3fxjxf5ho.htm](book-of-the-dead-bestiary-items/tvvsxwr3fxjxf5ho.htm)|Broken Barb|Broken Barb|modificada|
|[U0g3auuSZ4pwJU82.htm](book-of-the-dead-bestiary-items/U0g3auuSZ4pwJU82.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[U34PxMHZ0fS0k12A.htm](book-of-the-dead-bestiary-items/U34PxMHZ0fS0k12A.htm)|Form Up|Form Up|modificada|
|[u35onovz82ogkm7o.htm](book-of-the-dead-bestiary-items/u35onovz82ogkm7o.htm)|Take Root|Enraízate|modificada|
|[u3b2rv8mv9k9rb23.htm](book-of-the-dead-bestiary-items/u3b2rv8mv9k9rb23.htm)|Great Despair|Gran desesperación|modificada|
|[u5fvma01emd3ped8.htm](book-of-the-dead-bestiary-items/u5fvma01emd3ped8.htm)|Jaws|Fauces|modificada|
|[u5q8d8r0yfjwcdoj.htm](book-of-the-dead-bestiary-items/u5q8d8r0yfjwcdoj.htm)|Ship Bound|Ship Bound|modificada|
|[u7gqkauw2hdfde3u.htm](book-of-the-dead-bestiary-items/u7gqkauw2hdfde3u.htm)|Involuntary Reaction|Reacción involuntaria|modificada|
|[uB6EJzJpTE3PRvS8.htm](book-of-the-dead-bestiary-items/uB6EJzJpTE3PRvS8.htm)|Negative Healing|Curación negativa|modificada|
|[ubqhrgnv2m0l1100.htm](book-of-the-dead-bestiary-items/ubqhrgnv2m0l1100.htm)|Claw|Garra|modificada|
|[ubsx377x3tktofot.htm](book-of-the-dead-bestiary-items/ubsx377x3tktofot.htm)|Claw|Garra|modificada|
|[UBzBGEYk2sLnPlbQ.htm](book-of-the-dead-bestiary-items/UBzBGEYk2sLnPlbQ.htm)|Occult Spontaneous Spells|Ocultismo Hechizos espontáneos|modificada|
|[Uc4DijFU9ApvpWFZ.htm](book-of-the-dead-bestiary-items/Uc4DijFU9ApvpWFZ.htm)|Consume Flesh|Consumir carne|modificada|
|[ucz1Sz93jpBvRWbp.htm](book-of-the-dead-bestiary-items/ucz1Sz93jpBvRWbp.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Ud7fmRAenQnd8Zxa.htm](book-of-the-dead-bestiary-items/Ud7fmRAenQnd8Zxa.htm)|Negative Healing|Curación negativa|modificada|
|[udo3xq7mjc590h1k.htm](book-of-the-dead-bestiary-items/udo3xq7mjc590h1k.htm)|Fire Mote|Mota de Fuego|modificada|
|[ugPuUFMgsKh55RQn.htm](book-of-the-dead-bestiary-items/ugPuUFMgsKh55RQn.htm)|Divine Focus Spells|Conjuros de foco divino.|modificada|
|[uh3gw9uok4g0fuzy.htm](book-of-the-dead-bestiary-items/uh3gw9uok4g0fuzy.htm)|Reap Faith|Segar la Fe|modificada|
|[UHjOHKWLHa1xT8ID.htm](book-of-the-dead-bestiary-items/UHjOHKWLHa1xT8ID.htm)|Slow|Lentificado/a|modificada|
|[uhTWUFxW0j3Yev1T.htm](book-of-the-dead-bestiary-items/uhTWUFxW0j3Yev1T.htm)|Negative Healing|Curación negativa|modificada|
|[UK03LEy2NrVO5Ir3.htm](book-of-the-dead-bestiary-items/UK03LEy2NrVO5Ir3.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[ukiq9bqccfjpoc72.htm](book-of-the-dead-bestiary-items/ukiq9bqccfjpoc72.htm)|Submission Lock|Cerradura de presentación|modificada|
|[un97s9dr8tua2f9t.htm](book-of-the-dead-bestiary-items/un97s9dr8tua2f9t.htm)|Fangs|Colmillos|modificada|
|[uovpjaklzm8wgqbt.htm](book-of-the-dead-bestiary-items/uovpjaklzm8wgqbt.htm)|Spectral Charge|Carga espectral|modificada|
|[uphuqzm8b7b57k4d.htm](book-of-the-dead-bestiary-items/uphuqzm8b7b57k4d.htm)|Final Blasphemy|Blasfemia final|modificada|
|[urw7slqqg18c2bqr.htm](book-of-the-dead-bestiary-items/urw7slqqg18c2bqr.htm)|Change Posture|Cambiar Postura|modificada|
|[UTArnl71MY7BVLQc.htm](book-of-the-dead-bestiary-items/UTArnl71MY7BVLQc.htm)|Negative Healing|Curación negativa|modificada|
|[UuMVTRKxoumdKVH4.htm](book-of-the-dead-bestiary-items/UuMVTRKxoumdKVH4.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[UVtZ0kwBh5CQGxkT.htm](book-of-the-dead-bestiary-items/UVtZ0kwBh5CQGxkT.htm)|Stalk|Stalk|modificada|
|[UwqJzuhMBvtDiE1L.htm](book-of-the-dead-bestiary-items/UwqJzuhMBvtDiE1L.htm)|Darkvision|Visión en la oscuridad|modificada|
|[uyk9qJPvkuv0JU5M.htm](book-of-the-dead-bestiary-items/uyk9qJPvkuv0JU5M.htm)|Mental Bind|Mental Bind|modificada|
|[uyym1abw28sm0iuq.htm](book-of-the-dead-bestiary-items/uyym1abw28sm0iuq.htm)|Crush Item|Crush Item|modificada|
|[v006gzbsc924nhz4.htm](book-of-the-dead-bestiary-items/v006gzbsc924nhz4.htm)|Exhale|Exhale|modificada|
|[v0q9b81a0anjrpfc.htm](book-of-the-dead-bestiary-items/v0q9b81a0anjrpfc.htm)|Chastise Heretic|Chastise Heretic|modificada|
|[v1cjchmhqo6lvdjy.htm](book-of-the-dead-bestiary-items/v1cjchmhqo6lvdjy.htm)|Coordinated Strike|Golpe Coordinado|modificada|
|[v1KNai0kJiubpLMO.htm](book-of-the-dead-bestiary-items/v1KNai0kJiubpLMO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[v3e78dwshaq5ch57.htm](book-of-the-dead-bestiary-items/v3e78dwshaq5ch57.htm)|Consult the Text|Consultar el Texto|modificada|
|[V5QNa5hnkOnSMJVv.htm](book-of-the-dead-bestiary-items/V5QNa5hnkOnSMJVv.htm)|Attack of Opportunity (Special)|Ataque de oportunidad (Especial)|modificada|
|[v6OYQ9mA3KrJqI4Q.htm](book-of-the-dead-bestiary-items/v6OYQ9mA3KrJqI4Q.htm)|Negative Healing|Curación negativa|modificada|
|[v7iYGiJ8yz2GeTHT.htm](book-of-the-dead-bestiary-items/v7iYGiJ8yz2GeTHT.htm)|Darkvision|Visión en la oscuridad|modificada|
|[VaJj5noJcvvTyDKp.htm](book-of-the-dead-bestiary-items/VaJj5noJcvvTyDKp.htm)|Grab|Agarrado|modificada|
|[vb6es4dznn8764o4.htm](book-of-the-dead-bestiary-items/vb6es4dznn8764o4.htm)|Fist|Puño|modificada|
|[vc4ulmssbysj8wam.htm](book-of-the-dead-bestiary-items/vc4ulmssbysj8wam.htm)|Catching Bite|Muerdemuerde|modificada|
|[vd5bzz1j83bmas2b.htm](book-of-the-dead-bestiary-items/vd5bzz1j83bmas2b.htm)|Spear|Lanza|modificada|
|[VdFWJZh6U3QHXfn7.htm](book-of-the-dead-bestiary-items/VdFWJZh6U3QHXfn7.htm)|Negative Healing|Curación negativa|modificada|
|[vghi4lo4jsex6xvg.htm](book-of-the-dead-bestiary-items/vghi4lo4jsex6xvg.htm)|Spellstealing Counter|Spellstealing Counter|modificada|
|[vgpzwfs5atbe56al.htm](book-of-the-dead-bestiary-items/vgpzwfs5atbe56al.htm)|Cold Rot|Cold Rot|modificada|
|[vj9p9alcrxuahtfn.htm](book-of-the-dead-bestiary-items/vj9p9alcrxuahtfn.htm)|Scythe Claw|Garra Guadaña|modificada|
|[VJtbJPj39575cPho.htm](book-of-the-dead-bestiary-items/VJtbJPj39575cPho.htm)|Change Shape|Change Shape|modificada|
|[vkuk04wj4sfihq3b.htm](book-of-the-dead-bestiary-items/vkuk04wj4sfihq3b.htm)|Corpse Scent (Imprecise) 1 mile|Olor a cadáver (Impreciso) 1 milla|modificada|
|[vlcr44dvupfosqoq.htm](book-of-the-dead-bestiary-items/vlcr44dvupfosqoq.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[vlnnscd18jfqcdq9.htm](book-of-the-dead-bestiary-items/vlnnscd18jfqcdq9.htm)|Scything Blade|Cuchilla de guada|modificada|
|[vM9qUCcmjXuCKUvw.htm](book-of-the-dead-bestiary-items/vM9qUCcmjXuCKUvw.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[VqcQnwyahELeDBwK.htm](book-of-the-dead-bestiary-items/VqcQnwyahELeDBwK.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[vqtr9yyxp561y34u.htm](book-of-the-dead-bestiary-items/vqtr9yyxp561y34u.htm)|Ghostly Hand|Mano fantasmal|modificada|
|[vrbzxalz7mler6oi.htm](book-of-the-dead-bestiary-items/vrbzxalz7mler6oi.htm)|Snow Vision|Snow Vision|modificada|
|[VuchmKXLlqk2wO9c.htm](book-of-the-dead-bestiary-items/VuchmKXLlqk2wO9c.htm)|Grab|Agarrado|modificada|
|[vvxq8pxshmt9yd29.htm](book-of-the-dead-bestiary-items/vvxq8pxshmt9yd29.htm)|Frozen Breath|Aliento gélido|modificada|
|[vW68sTmazYmk9G6Q.htm](book-of-the-dead-bestiary-items/vW68sTmazYmk9G6Q.htm)|Feral Leap|Salto sin carrerilla|modificada|
|[vX9cmoHPlrRAHeYs.htm](book-of-the-dead-bestiary-items/vX9cmoHPlrRAHeYs.htm)|Bullets of Vengeance|Balas de Venganza|modificada|
|[VZR6D452W8R3BDqd.htm](book-of-the-dead-bestiary-items/VZR6D452W8R3BDqd.htm)|Darkvision|Visión en la oscuridad|modificada|
|[W0TicentuZL4V7qS.htm](book-of-the-dead-bestiary-items/W0TicentuZL4V7qS.htm)|Darkvision|Visión en la oscuridad|modificada|
|[w4cvlv6bdzp5wcxx.htm](book-of-the-dead-bestiary-items/w4cvlv6bdzp5wcxx.htm)|Hand|Mano|modificada|
|[w8nbobeosq4s3a35.htm](book-of-the-dead-bestiary-items/w8nbobeosq4s3a35.htm)|Stance of Death|Posición de Muerte|modificada|
|[wauesfz491hsnkk4.htm](book-of-the-dead-bestiary-items/wauesfz491hsnkk4.htm)|Destructive Finale|Destructive Finale|modificada|
|[wbmhtkddtggv5bgk.htm](book-of-the-dead-bestiary-items/wbmhtkddtggv5bgk.htm)|Fist|Puño|modificada|
|[wbqipaniohqje45s.htm](book-of-the-dead-bestiary-items/wbqipaniohqje45s.htm)|Devour Flesh|Devorar Carne|modificada|
|[wjdkro5cizi5vre7.htm](book-of-the-dead-bestiary-items/wjdkro5cizi5vre7.htm)|Eyes of the Tyrant|Ojos del Tirano|modificada|
|[WkoSPIMoXbCHFRZj.htm](book-of-the-dead-bestiary-items/WkoSPIMoXbCHFRZj.htm)|Grab|Agarrado|modificada|
|[wln6jtro52i3xd9c.htm](book-of-the-dead-bestiary-items/wln6jtro52i3xd9c.htm)|Exhume|Exhume|modificada|
|[WnQDVEnbKG81zOIf.htm](book-of-the-dead-bestiary-items/WnQDVEnbKG81zOIf.htm)|Occult Prepared Spells|Ocultismo Hechizos Preparados|modificada|
|[wo29s0y8jcz4isnc.htm](book-of-the-dead-bestiary-items/wo29s0y8jcz4isnc.htm)|Claw|Garra|modificada|
|[woLgXjw2p6sRigIz.htm](book-of-the-dead-bestiary-items/woLgXjw2p6sRigIz.htm)|Grab|Agarrado|modificada|
|[woUqNS1copPPro5x.htm](book-of-the-dead-bestiary-items/woUqNS1copPPro5x.htm)|Negative Healing|Curación negativa|modificada|
|[WPhT2IXNFPoANk1z.htm](book-of-the-dead-bestiary-items/WPhT2IXNFPoANk1z.htm)|Grab|Agarrado|modificada|
|[WPV9eNOze72jDJV4.htm](book-of-the-dead-bestiary-items/WPV9eNOze72jDJV4.htm)|Frightful Presence|Frightful Presence|modificada|
|[wq8dsdznit8qtxcv.htm](book-of-the-dead-bestiary-items/wq8dsdznit8qtxcv.htm)|Fist|Puño|modificada|
|[wRVqT1QO1O1ru1SY.htm](book-of-the-dead-bestiary-items/wRVqT1QO1O1ru1SY.htm)|Trample|Trample|modificada|
|[wspi8nMzAlqEekgs.htm](book-of-the-dead-bestiary-items/wspi8nMzAlqEekgs.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[WthNDt9DjtNzlPes.htm](book-of-the-dead-bestiary-items/WthNDt9DjtNzlPes.htm)|Negative Healing|Curación negativa|modificada|
|[Wunpn2wRMKV7JLZr.htm](book-of-the-dead-bestiary-items/Wunpn2wRMKV7JLZr.htm)|Darkvision|Visión en la oscuridad|modificada|
|[WUYJjrYb5HbQgPNA.htm](book-of-the-dead-bestiary-items/WUYJjrYb5HbQgPNA.htm)|Negative Healing|Curación negativa|modificada|
|[wv50pg4p8i3xb12d.htm](book-of-the-dead-bestiary-items/wv50pg4p8i3xb12d.htm)|Putrid Assault|Putrid Assault|modificada|
|[wvs0vnvwndnjqq6u.htm](book-of-the-dead-bestiary-items/wvs0vnvwndnjqq6u.htm)|Chain Capture|Chain Capture|modificada|
|[ww6g31ronygtiit4.htm](book-of-the-dead-bestiary-items/ww6g31ronygtiit4.htm)|Primal Corruption|Corrupción Primal|modificada|
|[X1pEJ8DPmm00nq2l.htm](book-of-the-dead-bestiary-items/X1pEJ8DPmm00nq2l.htm)|Constant Spells|Constant Spells|modificada|
|[X22kke8lGbonOyuE.htm](book-of-the-dead-bestiary-items/X22kke8lGbonOyuE.htm)|Lifesense 60 feet|Lifesense 60 pies|modificada|
|[x9iTyqF9YiSbKaz7.htm](book-of-the-dead-bestiary-items/x9iTyqF9YiSbKaz7.htm)|Frightful Presence|Frightful Presence|modificada|
|[xat38aa9u2gh8xpz.htm](book-of-the-dead-bestiary-items/xat38aa9u2gh8xpz.htm)|Curse of Eternal Sleep|Maldición del sueño eterno|modificada|
|[xcdtl5hp12cdn0au.htm](book-of-the-dead-bestiary-items/xcdtl5hp12cdn0au.htm)|Necrotic Runoff|Necrotic Runoff|modificada|
|[xcxndcu0v1o1sayq.htm](book-of-the-dead-bestiary-items/xcxndcu0v1o1sayq.htm)|Fist|Puño|modificada|
|[xf1472qm9rkgerh8.htm](book-of-the-dead-bestiary-items/xf1472qm9rkgerh8.htm)|Aura of Doom|Aura de Perdición|modificada|
|[xicsjt4e3glrfz9g.htm](book-of-the-dead-bestiary-items/xicsjt4e3glrfz9g.htm)|Final Spite|Final Spite|modificada|
|[XlVJgIVvBqc5DGGe.htm](book-of-the-dead-bestiary-items/XlVJgIVvBqc5DGGe.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Xoqie6AhgsKZ7wJ3.htm](book-of-the-dead-bestiary-items/Xoqie6AhgsKZ7wJ3.htm)|Sudden Chill|Sudden Chill|modificada|
|[xrhhvmqzd4qp9cx9.htm](book-of-the-dead-bestiary-items/xrhhvmqzd4qp9cx9.htm)|Steady Spellcasting|Lanzamiento de conjuros estables|modificada|
|[xt70n62im3owz0y2.htm](book-of-the-dead-bestiary-items/xt70n62im3owz0y2.htm)|Crumble|Desmoronarse|modificada|
|[xukqweotozm6rcvt.htm](book-of-the-dead-bestiary-items/xukqweotozm6rcvt.htm)|Claw|Garra|modificada|
|[xw3lqxe7mwht4qcu.htm](book-of-the-dead-bestiary-items/xw3lqxe7mwht4qcu.htm)|Claw|Garra|modificada|
|[xw3tnwsyuflmy8kr.htm](book-of-the-dead-bestiary-items/xw3tnwsyuflmy8kr.htm)|Stored Items|Objetos almacenados|modificada|
|[XxTC7GYQhX1vxMLs.htm](book-of-the-dead-bestiary-items/XxTC7GYQhX1vxMLs.htm)|At-Will Spells|Hechizos a voluntad|modificada|
|[XyO3dvZ3nK3xea9x.htm](book-of-the-dead-bestiary-items/XyO3dvZ3nK3xea9x.htm)|Breathsense (Precise) 60 feet|Sentido de la respiración (Precisión) 60 pies.|modificada|
|[XyqShEf9KyXNmA9C.htm](book-of-the-dead-bestiary-items/XyqShEf9KyXNmA9C.htm)|Rejuvenation|Rejuvenecimiento|modificada|
|[y2p2f22o6cstzezq.htm](book-of-the-dead-bestiary-items/y2p2f22o6cstzezq.htm)|Wail|Lamento|modificada|
|[Y4U4NccPky46RHKa.htm](book-of-the-dead-bestiary-items/Y4U4NccPky46RHKa.htm)|Darkvision|Visión en la oscuridad|modificada|
|[Y5HEEWSpdfvhVsRo.htm](book-of-the-dead-bestiary-items/Y5HEEWSpdfvhVsRo.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[y75gjuac91xio97i.htm](book-of-the-dead-bestiary-items/y75gjuac91xio97i.htm)|Shortsword|Espada corta|modificada|
|[y7o3rimaf8qdtr7r.htm](book-of-the-dead-bestiary-items/y7o3rimaf8qdtr7r.htm)|Mystery Ingredients|Mystery Ingredients|modificada|
|[yarojxhc6r3jba98.htm](book-of-the-dead-bestiary-items/yarojxhc6r3jba98.htm)|Pallid Plague|Pallid Plague|modificada|
|[ydi6eab8julbghyg.htm](book-of-the-dead-bestiary-items/ydi6eab8julbghyg.htm)|Heretic's Smite|Heretic's Smite|modificada|
|[YG1rDVP0SPT5NVZR.htm](book-of-the-dead-bestiary-items/YG1rDVP0SPT5NVZR.htm)|Negative Healing|Curación negativa|modificada|
|[YgBTeOvZE7Aq4UvH.htm](book-of-the-dead-bestiary-items/YgBTeOvZE7Aq4UvH.htm)|Shambling Trample|Trample Shambling|modificada|
|[yl83ga21pc0ii43n.htm](book-of-the-dead-bestiary-items/yl83ga21pc0ii43n.htm)|Fist|Puño|modificada|
|[yM9uk6VXyMHmaP4c.htm](book-of-the-dead-bestiary-items/yM9uk6VXyMHmaP4c.htm)|Seep Blood|Seep Blood|modificada|
|[yn0676ooolga6aov.htm](book-of-the-dead-bestiary-items/yn0676ooolga6aov.htm)|Claw|Garra|modificada|
|[yof80btzc6ifgcl4.htm](book-of-the-dead-bestiary-items/yof80btzc6ifgcl4.htm)|Battle Axe|Hacha de batalla|modificada|
|[ypbtlxics2321ox1.htm](book-of-the-dead-bestiary-items/ypbtlxics2321ox1.htm)|Regenerating Bond|Lazo Regenerador|modificada|
|[yqze7ouayizxybn1.htm](book-of-the-dead-bestiary-items/yqze7ouayizxybn1.htm)|Aura of Charm|Aura de Hechizar|modificada|
|[yRj5K09fzH4SzWgC.htm](book-of-the-dead-bestiary-items/yRj5K09fzH4SzWgC.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yrk0vsss2csfbcxn.htm](book-of-the-dead-bestiary-items/yrk0vsss2csfbcxn.htm)|Self-Loathing|Autodesprecio|modificada|
|[Yv49fZ7FOxFBIwPP.htm](book-of-the-dead-bestiary-items/Yv49fZ7FOxFBIwPP.htm)|Divine Innate Spells|Hechizos divinos innatos|modificada|
|[yWkcNDsv41WwuTaJ.htm](book-of-the-dead-bestiary-items/yWkcNDsv41WwuTaJ.htm)|Primal Innate Spells|Primal Innate Spells|modificada|
|[YWKIM8D6A5hTxCGj.htm](book-of-the-dead-bestiary-items/YWKIM8D6A5hTxCGj.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yXCq63vokzp9doFR.htm](book-of-the-dead-bestiary-items/yXCq63vokzp9doFR.htm)|Negative Healing|Curación negativa|modificada|
|[YXtsCv8FMKUxjjDY.htm](book-of-the-dead-bestiary-items/YXtsCv8FMKUxjjDY.htm)|Telepathy 100 feet|Telepatía 100 pies.|modificada|
|[YykK3Rlmno0gXfji.htm](book-of-the-dead-bestiary-items/YykK3Rlmno0gXfji.htm)|Darkvision|Visión en la oscuridad|modificada|
|[yZSthxhBNrgEZuJQ.htm](book-of-the-dead-bestiary-items/yZSthxhBNrgEZuJQ.htm)|Grab|Agarrado|modificada|
|[z0m4exlsxdjib6e2.htm](book-of-the-dead-bestiary-items/z0m4exlsxdjib6e2.htm)|Shamble Forth!|¡Shamble Forth!|modificada|
|[z3ckvu9q74atbpee.htm](book-of-the-dead-bestiary-items/z3ckvu9q74atbpee.htm)|Feed on the Living|Alimentarse de los vivos|modificada|
|[Z47yWqdpdND95LJp.htm](book-of-the-dead-bestiary-items/Z47yWqdpdND95LJp.htm)|Regeneration 10 (Deactivated by Cold)|Regeneración 10 (Desactivado por el frío)|modificada|
|[z4v0g409xiuvao4u.htm](book-of-the-dead-bestiary-items/z4v0g409xiuvao4u.htm)|Baleful Gaze|Mirada torva|modificada|
|[z7svy61gizn3hcsn.htm](book-of-the-dead-bestiary-items/z7svy61gizn3hcsn.htm)|Jaws|Fauces|modificada|
|[Zb4v92oZHr85fHnO.htm](book-of-the-dead-bestiary-items/Zb4v92oZHr85fHnO.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zboaidBufdR4dpax.htm](book-of-the-dead-bestiary-items/zboaidBufdR4dpax.htm)|Whispering Scythe|Whispering Scythe|modificada|
|[zdy9kx6yjmJEYk7V.htm](book-of-the-dead-bestiary-items/zdy9kx6yjmJEYk7V.htm)|Arcane Prepared Spells|Hechizos Arcanos Preparados|modificada|
|[zFCU1ww4cUWHDUWa.htm](book-of-the-dead-bestiary-items/zFCU1ww4cUWHDUWa.htm)|Constant Spells|Constant Spells|modificada|
|[zg9njsvehofdrf4r.htm](book-of-the-dead-bestiary-items/zg9njsvehofdrf4r.htm)|Claw|Garra|modificada|
|[zhuzs5628vicodv5.htm](book-of-the-dead-bestiary-items/zhuzs5628vicodv5.htm)|Set Defense|Set Defense|modificada|
|[ZIt2oLTwffPTkqT4.htm](book-of-the-dead-bestiary-items/ZIt2oLTwffPTkqT4.htm)|+1 Status to All Saves vs. Positive|+1 situación a todas las salvaciones frente a positivos.|modificada|
|[ZmcRFvC9pG00P8gI.htm](book-of-the-dead-bestiary-items/ZmcRFvC9pG00P8gI.htm)|Greater Darkvision|Mayor visión en la oscuridad|modificada|
|[ZMfdgAIrZqi3yhIl.htm](book-of-the-dead-bestiary-items/ZMfdgAIrZqi3yhIl.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zmsoavgcearhti47.htm](book-of-the-dead-bestiary-items/zmsoavgcearhti47.htm)|Slow|Lentificado/a|modificada|
|[znu8ybij44epq6vh.htm](book-of-the-dead-bestiary-items/znu8ybij44epq6vh.htm)|Exemplar of Violence|Ejemplo de Violencia|modificada|
|[ZoUf0bRzsx7T7fTw.htm](book-of-the-dead-bestiary-items/ZoUf0bRzsx7T7fTw.htm)|Negative Healing|Curación negativa|modificada|
|[zp0eidljzesefa24.htm](book-of-the-dead-bestiary-items/zp0eidljzesefa24.htm)|Repartee Riposte|Repartee Riposte|modificada|
|[zpLChqdAZDg9x9Lg.htm](book-of-the-dead-bestiary-items/zpLChqdAZDg9x9Lg.htm)|Grab|Agarrado|modificada|
|[ZPZh59h8oJbpPBr5.htm](book-of-the-dead-bestiary-items/ZPZh59h8oJbpPBr5.htm)|Darkvision|Visión en la oscuridad|modificada|
|[zqe60ky12wasdnz6.htm](book-of-the-dead-bestiary-items/zqe60ky12wasdnz6.htm)|Drowning Grasp|Drowning Grasp|modificada|
|[ZRadUe9v3qyF3o2k.htm](book-of-the-dead-bestiary-items/ZRadUe9v3qyF3o2k.htm)|Troop Defenses|Troop Defenses|modificada|
|[zrycs1fp75o2l8h0.htm](book-of-the-dead-bestiary-items/zrycs1fp75o2l8h0.htm)|Bite|Muerdemuerde|modificada|
|[zu5kwhnrxafjmru7.htm](book-of-the-dead-bestiary-items/zu5kwhnrxafjmru7.htm)|Axe Vulnerability|Vulnerabilidad a las hachas|modificada|
|[ZuLQYlg9AglGAJp6.htm](book-of-the-dead-bestiary-items/ZuLQYlg9AglGAJp6.htm)|Scent (Imprecise) 30 feet|Scent (Imprecise) 30 feet|modificada|
|[zwnm8cclbjl2zo8c.htm](book-of-the-dead-bestiary-items/zwnm8cclbjl2zo8c.htm)|Heretic's Smite|Heretic's Smite|modificada|
|[zy1MsGurtjxz9LuB.htm](book-of-the-dead-bestiary-items/zy1MsGurtjxz9LuB.htm)|Warped Fulu|Warped Fulu|modificada|
|[Zyy2H5wK3HtqNuuy.htm](book-of-the-dead-bestiary-items/Zyy2H5wK3HtqNuuy.htm)|Negative Healing|Curación negativa|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0gikfhrrftrwkz2g.htm](book-of-the-dead-bestiary-items/0gikfhrrftrwkz2g.htm)|Intimidation|vacía|
|[3mhiog9le9ovojoy.htm](book-of-the-dead-bestiary-items/3mhiog9le9ovojoy.htm)|Soothsaying Lore|vacía|
|[3p1n4in9wahzixgz.htm](book-of-the-dead-bestiary-items/3p1n4in9wahzixgz.htm)|Ghost Lore|vacía|
|[62rba0y5m9752v4s.htm](book-of-the-dead-bestiary-items/62rba0y5m9752v4s.htm)|Shadow Plane Lore|vacía|
|[6c9470dhaj9941f5.htm](book-of-the-dead-bestiary-items/6c9470dhaj9941f5.htm)|Warfare Lore|vacía|
|[6litfyrvhbcm9pup.htm](book-of-the-dead-bestiary-items/6litfyrvhbcm9pup.htm)|Warfare Lore|vacía|
|[6qjxhm2zgc9z1z0o.htm](book-of-the-dead-bestiary-items/6qjxhm2zgc9z1z0o.htm)|Negative Energy Plane Lore|vacía|
|[abi4luruyxmz1ht3.htm](book-of-the-dead-bestiary-items/abi4luruyxmz1ht3.htm)|Survival|vacía|
|[ctcrjw6hzakchqgm.htm](book-of-the-dead-bestiary-items/ctcrjw6hzakchqgm.htm)|Athletics|vacía|
|[dIHvHkEhLKpQPvHh.htm](book-of-the-dead-bestiary-items/dIHvHkEhLKpQPvHh.htm)|Mercantile Lore|vacía|
|[dj6gpzc5rcds6hc4.htm](book-of-the-dead-bestiary-items/dj6gpzc5rcds6hc4.htm)|Irori Lore|vacía|
|[dpxt817prps7vv63.htm](book-of-the-dead-bestiary-items/dpxt817prps7vv63.htm)|Negative Energy Plane Lore|vacía|
|[ej7vkiygh97ejpx4.htm](book-of-the-dead-bestiary-items/ej7vkiygh97ejpx4.htm)|Woodland Lore (Applies Only to the Woodland the Waldgeist is Bonded To)|vacía|
|[fc9jut8qh45m7eoj.htm](book-of-the-dead-bestiary-items/fc9jut8qh45m7eoj.htm)|Gladiatorial Lore|vacía|
|[gb96b0rpdnd1o31t.htm](book-of-the-dead-bestiary-items/gb96b0rpdnd1o31t.htm)|Shadow Plane Lore|vacía|
|[gmcd6ukh14nzdcez.htm](book-of-the-dead-bestiary-items/gmcd6ukh14nzdcez.htm)|Intimidation|vacía|
|[hjg7z96c9j7w5prb.htm](book-of-the-dead-bestiary-items/hjg7z96c9j7w5prb.htm)|Warfare Lore|vacía|
|[jfbv6zyvbqlc7a83.htm](book-of-the-dead-bestiary-items/jfbv6zyvbqlc7a83.htm)|Shadow Plane Lore|vacía|
|[JKpaLr9UX648Hgix.htm](book-of-the-dead-bestiary-items/JKpaLr9UX648Hgix.htm)|Infused Reagents|vacía|
|[kg8pmvmx5h75yhea.htm](book-of-the-dead-bestiary-items/kg8pmvmx5h75yhea.htm)|Warfare Lore|vacía|
|[kyvbc251jzpofo6c.htm](book-of-the-dead-bestiary-items/kyvbc251jzpofo6c.htm)|Lore|vacía|
|[l13as6h1cnru8t59.htm](book-of-the-dead-bestiary-items/l13as6h1cnru8t59.htm)|Nethys Lore|vacía|
|[lig2yfl497n7dn8f.htm](book-of-the-dead-bestiary-items/lig2yfl497n7dn8f.htm)|Theater Lore|vacía|
|[mij1k2qxzdjs4q2z.htm](book-of-the-dead-bestiary-items/mij1k2qxzdjs4q2z.htm)|Athletics|vacía|
|[n1l2a2abx1ek0d09.htm](book-of-the-dead-bestiary-items/n1l2a2abx1ek0d09.htm)|Kazutal Lore|vacía|
|[oz8ouz3zgukkg8ga.htm](book-of-the-dead-bestiary-items/oz8ouz3zgukkg8ga.htm)|Driving Lore|vacía|
|[p8d8xn0icyozjet4.htm](book-of-the-dead-bestiary-items/p8d8xn0icyozjet4.htm)|Negative Energy Plane Lore|vacía|
|[pxfvn6ngmzu1k7ed.htm](book-of-the-dead-bestiary-items/pxfvn6ngmzu1k7ed.htm)|Crafting|vacía|
|[qd0w6wglev50gug2.htm](book-of-the-dead-bestiary-items/qd0w6wglev50gug2.htm)|Shadow Plane Lore|vacía|
|[rotlnf6iwle4td73.htm](book-of-the-dead-bestiary-items/rotlnf6iwle4td73.htm)|Acrobatics|vacía|
|[s4bb8j40iolivf58.htm](book-of-the-dead-bestiary-items/s4bb8j40iolivf58.htm)|Martial Arts Lore|vacía|
|[sb2b4fq0wls5slr7.htm](book-of-the-dead-bestiary-items/sb2b4fq0wls5slr7.htm)|Negative Energy Plane Lore|vacía|
|[t1csyjtz5anowgkm.htm](book-of-the-dead-bestiary-items/t1csyjtz5anowgkm.htm)|Athletics|vacía|
|[tlp3kuw1xc6wf30n.htm](book-of-the-dead-bestiary-items/tlp3kuw1xc6wf30n.htm)|Acrobatics|vacía|
|[x8nenxl6un8manoy.htm](book-of-the-dead-bestiary-items/x8nenxl6un8manoy.htm)|Acrobatics|vacía|
|[yfqqqxd6hvhoxdj9.htm](book-of-the-dead-bestiary-items/yfqqqxd6hvhoxdj9.htm)|Athletics|vacía|
|[ytyjg7p3zpuhfcl3.htm](book-of-the-dead-bestiary-items/ytyjg7p3zpuhfcl3.htm)|Games Lore|vacía|
|[zh8ndbyu6spjabea.htm](book-of-the-dead-bestiary-items/zh8ndbyu6spjabea.htm)|Sailing Lore|vacía|
|[zl5zkuozrgn1ttrm.htm](book-of-the-dead-bestiary-items/zl5zkuozrgn1ttrm.htm)|Crafting|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
