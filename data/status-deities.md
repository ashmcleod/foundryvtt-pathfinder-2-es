# Estado de la traducción (deities)

 * **oficial**: 66
 * **modificada**: 195
 * **ninguna**: 10


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[1xG0PDxn0rhmJhMS.htm](deities/1xG0PDxn0rhmJhMS.htm)|Yelayne|
|[7zyxg8TlXCDzHGMf.htm](deities/7zyxg8TlXCDzHGMf.htm)|The Last Breath|
|[9LwncUwHbG1f6qIt.htm](deities/9LwncUwHbG1f6qIt.htm)|Marishi|
|[CeBP002LcMfqgIRl.htm](deities/CeBP002LcMfqgIRl.htm)|Picoperi|
|[liXIbl7y8www2eAn.htm](deities/liXIbl7y8www2eAn.htm)|Findeladlara (The Guiding Hand)|
|[m6UYxDnwGkOo8IY5.htm](deities/m6UYxDnwGkOo8IY5.htm)|Thisamet|
|[Ons4JwstcXOlkaRF.htm](deities/Ons4JwstcXOlkaRF.htm)|The Perplexing Jest|
|[PIMd4yRGj8XQgGbW.htm](deities/PIMd4yRGj8XQgGbW.htm)|The Divine Dare|
|[RlOuyadtreI7pht1.htm](deities/RlOuyadtreI7pht1.htm)|The Resplendent Court|
|[w9vtltygWNG4eZDR.htm](deities/w9vtltygWNG4eZDR.htm)|Jin Li|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0R2BjVb7d9tYCIbk.htm](deities/0R2BjVb7d9tYCIbk.htm)|Tlehar|Tlehar|modificada|
|[0xCttKr5oqjirGCe.htm](deities/0xCttKr5oqjirGCe.htm)|Aakriti|Aakriti|modificada|
|[14GLsVobnZTEmJrs.htm](deities/14GLsVobnZTEmJrs.htm)|Kelizandri|Kelizandri|modificada|
|[1fmTXGRPa8fLXZKL.htm](deities/1fmTXGRPa8fLXZKL.htm)|Balumbdar|Balumbdar|modificada|
|[1LiO5bDQIlJmXk35.htm](deities/1LiO5bDQIlJmXk35.htm)|Lady Razor|Lady Razor|modificada|
|[1nQVLOyWv5kC0bCU.htm](deities/1nQVLOyWv5kC0bCU.htm)|Ravithra|Ravithra|modificada|
|[1NxkZf6ndijKeufX.htm](deities/1NxkZf6ndijKeufX.htm)|Sairazul|Sairazul|modificada|
|[1xHnoMlCyCV70TnT.htm](deities/1xHnoMlCyCV70TnT.htm)|Saloc|Saloc|modificada|
|[30uY6NkhYmPI3Oii.htm](deities/30uY6NkhYmPI3Oii.htm)|Bergelmir|Bergelmir|modificada|
|[39z55kowQFOUH2v0.htm](deities/39z55kowQFOUH2v0.htm)|Droskar|Droskar|modificada|
|[3ffVxwgtvR1imHm8.htm](deities/3ffVxwgtvR1imHm8.htm)|Gendowyn|Gendowyn|modificada|
|[3INt1X4ijBYnZYbU.htm](deities/3INt1X4ijBYnZYbU.htm)|Shyka|Shyka|modificada|
|[3lgzcg84J0fsVqjh.htm](deities/3lgzcg84J0fsVqjh.htm)|Esoteric Order of the Palatine Eye|Orden Esotérica del Ojo Palatino|modificada|
|[3nxIpMGeCvcBIVov.htm](deities/3nxIpMGeCvcBIVov.htm)|Nyarlathotep (The Crawling Chaos)|Nyarlathotep (El caos reptante)|modificada|
|[44fcXgp3fnkoyb4R.htm](deities/44fcXgp3fnkoyb4R.htm)|Rowdrosh|Rowdrosh|modificada|
|[4bEPoujxhYdUyUOg.htm](deities/4bEPoujxhYdUyUOg.htm)|Ydersius|Ydersius|modificada|
|[4I1aVKED08cwcC4H.htm](deities/4I1aVKED08cwcC4H.htm)|Laws of Mortality|Leyes de la mortalidad|modificada|
|[5YUNikBpJZ596MRD.htm](deities/5YUNikBpJZ596MRD.htm)|Anubis|Anubis|modificada|
|[5ZIvg8CkqRMepVcX.htm](deities/5ZIvg8CkqRMepVcX.htm)|Korada|Korada|modificada|
|[65o189p2eUPbqaDK.htm](deities/65o189p2eUPbqaDK.htm)|Narriseminek|Narriseminek|modificada|
|[6mgCX7lvKtaEYoa9.htm](deities/6mgCX7lvKtaEYoa9.htm)|Dwarven Pantheon|Panteón Enano|modificada|
|[6nq25AjImkT5GxDT.htm](deities/6nq25AjImkT5GxDT.htm)|The Prismatic Ray|El Rayo Prismático|modificada|
|[6O4UGWcYERDEugzk.htm](deities/6O4UGWcYERDEugzk.htm)|Fumeiyoshi|Fumeiyoshi|modificada|
|[7L9KIqixgseSCY3X.htm](deities/7L9KIqixgseSCY3X.htm)|Ymeri|Ymeri|modificada|
|[83jKKHQPoqVylmrF.htm](deities/83jKKHQPoqVylmrF.htm)|Yaezhing|Yaezhing|modificada|
|[8DXel4AvpyUy6oJ0.htm](deities/8DXel4AvpyUy6oJ0.htm)|Eritrice|Eritrice|modificada|
|[8Oy4qHylOOp79JxD.htm](deities/8Oy4qHylOOp79JxD.htm)|Nhimbaloth|Nhimbaloth|modificada|
|[8wOAwLMgB1HFLo9n.htm](deities/8wOAwLMgB1HFLo9n.htm)|Winlas|Winlas|modificada|
|[97Qe42wJN5EVUS14.htm](deities/97Qe42wJN5EVUS14.htm)|Sturovenen|Sturovenen|modificada|
|[9eltBjLngkR9x1Xm.htm](deities/9eltBjLngkR9x1Xm.htm)|Trelmarixian|Trelmarixian|modificada|
|[9FWBJcADLEeP8Krf.htm](deities/9FWBJcADLEeP8Krf.htm)|Valmallos|Valmallos|modificada|
|[9vcKBv8h5vugb8lL.htm](deities/9vcKBv8h5vugb8lL.htm)|Fandarra|Fandarra|modificada|
|[9Z1vOSaLsEiAs2yN.htm](deities/9Z1vOSaLsEiAs2yN.htm)|Thalaphyrr Martyr-Minder|Thalaphyrr Martyr-Minder|modificada|
|[A37XqON6QRPzQhp7.htm](deities/A37XqON6QRPzQhp7.htm)|Hshurha|Hshurha|modificada|
|[aChBtIDfA59QQ7SN.htm](deities/aChBtIDfA59QQ7SN.htm)|Ayrzul|Ayrzul|modificada|
|[AIcDyoff265z9285.htm](deities/AIcDyoff265z9285.htm)|Kofusachi|Kofusachi|modificada|
|[aievwuZalZ2abs2q.htm](deities/aievwuZalZ2abs2q.htm)|Thoth|Thoth|modificada|
|[AJ7Wgpj5Ext9wbtj.htm](deities/AJ7Wgpj5Ext9wbtj.htm)|Keepers of the Hearth|Keepers of the Hearth|modificada|
|[akcmQbvNWfhEfEhf.htm](deities/akcmQbvNWfhEfEhf.htm)|Arqueros|Arqueros|modificada|
|[aLCfEgt48SRUHnMC.htm](deities/aLCfEgt48SRUHnMC.htm)|Jaidi|Jaidi|modificada|
|[ArmPZQrl0SFAKvrY.htm](deities/ArmPZQrl0SFAKvrY.htm)|Ma'at|Ma'at|modificada|
|[b7H379HnCOv9UZtd.htm](deities/b7H379HnCOv9UZtd.htm)|Orcus|Orcus|modificada|
|[bF77ydtptI54STl9.htm](deities/bF77ydtptI54STl9.htm)|Jaidz|Jaidz|modificada|
|[BLNXYqvzmLgjMq2p.htm](deities/BLNXYqvzmLgjMq2p.htm)|Sun Wukong|Sun Wukong|modificada|
|[BrbXGyzCeORNyJT5.htm](deities/BrbXGyzCeORNyJT5.htm)|Sifkesh|Sifkesh|modificada|
|[Bzfc5mIUfMxOnQWu.htm](deities/Bzfc5mIUfMxOnQWu.htm)|Dahak|Dahak|modificada|
|[c9NFjRR8gr6CWpjl.htm](deities/c9NFjRR8gr6CWpjl.htm)|Black Butterfly|Mariposa Negra|modificada|
|[CDkr6eXR1lgu7g6A.htm](deities/CDkr6eXR1lgu7g6A.htm)|Andoletta|Andoletta|modificada|
|[cHAbE86pto5bvIWo.htm](deities/cHAbE86pto5bvIWo.htm)|Angazhan|Angazhan|modificada|
|[CnDEoo6WruQCXo1o.htm](deities/CnDEoo6WruQCXo1o.htm)|Lubaiko|Lubaiko|modificada|
|[d47XTSODGAKANu3d.htm](deities/d47XTSODGAKANu3d.htm)|Vineshvakhi|Vineshvakhi|modificada|
|[dcbf3pBSNa2q1Kkq.htm](deities/dcbf3pBSNa2q1Kkq.htm)|Dhalavei|Dhalavei|modificada|
|[DFxbPmQLOqcmbPTz.htm](deities/DFxbPmQLOqcmbPTz.htm)|Tanagaar|Tanagaar|modificada|
|[DJ7LDFTtns6YFodT.htm](deities/DJ7LDFTtns6YFodT.htm)|Ashava|Ashava|modificada|
|[Dm8qlzpUj7g82QFv.htm](deities/Dm8qlzpUj7g82QFv.htm)|Erecura|Erecura|modificada|
|[E1dUhoSEf2XfklU2.htm](deities/E1dUhoSEf2XfklU2.htm)|Mother Vulture|Madre buitre|modificada|
|[EE8sNM4JraOPN669.htm](deities/EE8sNM4JraOPN669.htm)|Cernunnos|Cernunnos|modificada|
|[EfHlurXdqkdRXaqv.htm](deities/EfHlurXdqkdRXaqv.htm)|Set|Set|modificada|
|[EFyDjlH7XjGq7Erd.htm](deities/EFyDjlH7XjGq7Erd.htm)|Isis|Isis|modificada|
|[EUOAV02rHz96jMPh.htm](deities/EUOAV02rHz96jMPh.htm)|Whispering Way|Whispering Way|modificada|
|[eyGEoggFlTEUQmvJ.htm](deities/eyGEoggFlTEUQmvJ.htm)|Bes|Bes|modificada|
|[FeMWwhaVbaoqu3q6.htm](deities/FeMWwhaVbaoqu3q6.htm)|Reshmit of the Heavy Voice|Reshmit de la Voz Pesada|modificada|
|[fTs56epghXwclPcX.htm](deities/fTs56epghXwclPcX.htm)|Stag Mother of the Forest of Stones|Ciervo Madre del Bosque de Piedras|modificada|
|[g1AVEGi8QibhS007.htm](deities/g1AVEGi8QibhS007.htm)|Walkena|Walkena|modificada|
|[GBdxyS79vTozcv3P.htm](deities/GBdxyS79vTozcv3P.htm)|The Laborer's Bastion|El bastión del bracero|modificada|
|[GEBaoeiv5CB9h2Bn.htm](deities/GEBaoeiv5CB9h2Bn.htm)|Ragathiel|Ragathiel|modificada|
|[giAsBYiL2omr0x2L.htm](deities/giAsBYiL2omr0x2L.htm)|Qi Zhong|Qi Zhong|modificada|
|[gpS3hv7LP2IRwEHT.htm](deities/gpS3hv7LP2IRwEHT.htm)|Sorrow's Sword|Sorrow's Sword|modificada|
|[gsFaxWHbZmc1moY6.htm](deities/gsFaxWHbZmc1moY6.htm)|Nalinivati|Nalinivati|modificada|
|[GUmYLD7PGmE2TOx6.htm](deities/GUmYLD7PGmE2TOx6.htm)|Wadjet|Wadjet|modificada|
|[gutbzYTZvRTnq4on.htm](deities/gutbzYTZvRTnq4on.htm)|Adanye|Adanye|modificada|
|[Gy78DCwfY4ltBGI6.htm](deities/Gy78DCwfY4ltBGI6.htm)|Husk|Husk|modificada|
|[H1VrZ1QX0d2aPXT8.htm](deities/H1VrZ1QX0d2aPXT8.htm)|Dammerich|Dammerich|modificada|
|[HbuynQRCjGKtbhcC.htm](deities/HbuynQRCjGKtbhcC.htm)|The Lost Prince|El Príncipe Perdido|modificada|
|[HD7g8T8ioy1EH3M8.htm](deities/HD7g8T8ioy1EH3M8.htm)|Bolka|Bolka|modificada|
|[hhV2g6aU8jKLEca1.htm](deities/hhV2g6aU8jKLEca1.htm)|Narakaas|Narakaas|modificada|
|[hWTTS0e7J9hJfsp3.htm](deities/hWTTS0e7J9hJfsp3.htm)|Matravash|Matravash|modificada|
|[idAu7puhPp3kFTnl.htm](deities/idAu7puhPp3kFTnl.htm)|Lorthact's Cult|Culto de Lorthact|modificada|
|[iEt6idGIEPXY2HzV.htm](deities/iEt6idGIEPXY2HzV.htm)|Ydajisk|Ydajisk|modificada|
|[Ij1KRM8ufkEjGnR3.htm](deities/Ij1KRM8ufkEjGnR3.htm)|Ranginori|Ranginori|modificada|
|[IK2AL3bL6htrrnnC.htm](deities/IK2AL3bL6htrrnnC.htm)|Skode|Skode|modificada|
|[iKgERiIc8rt3fatf.htm](deities/iKgERiIc8rt3fatf.htm)|Nyarlathotep (Haunter in the Dark)|Nyarlathotep (Acechador en la oscuridad)|modificada|
|[iMRXc519Uepf8yH3.htm](deities/iMRXc519Uepf8yH3.htm)|Magdh|Magdh|modificada|
|[ItYptsfjV8XvfhOV.htm](deities/ItYptsfjV8XvfhOV.htm)|Lahkgya|Lahkgya|modificada|
|[IXLzDSMGndFb5rGY.htm](deities/IXLzDSMGndFb5rGY.htm)|Falayna|Falayna|modificada|
|[JcIf4uwKLxEWdplI.htm](deities/JcIf4uwKLxEWdplI.htm)|Kostchtchie|Kostchtchie|modificada|
|[JDbrj8aKkLzdn8BU.htm](deities/JDbrj8aKkLzdn8BU.htm)|Atheism|Ateísmo|modificada|
|[JgDqFUB1NtQkGmzp.htm](deities/JgDqFUB1NtQkGmzp.htm)|Otolmens|Otolmens|modificada|
|[JndhOIHnQoMwnsw3.htm](deities/JndhOIHnQoMwnsw3.htm)|The Godclaw|El Godclaw|modificada|
|[jrPmEfubRDzMKbxl.htm](deities/jrPmEfubRDzMKbxl.htm)|Arundhat|Arundhat|modificada|
|[K1tzUClsozKcdpIe.htm](deities/K1tzUClsozKcdpIe.htm)|Halcamora|Halcamora|modificada|
|[KB6O6dvwJj7wuwO7.htm](deities/KB6O6dvwJj7wuwO7.htm)|Urban Prosperity|Prosperidad Urbana|modificada|
|[KDFujU3Yug6AwCOz.htm](deities/KDFujU3Yug6AwCOz.htm)|Arshea|Arshea|modificada|
|[KIEgpcmHYtFu1dpg.htm](deities/KIEgpcmHYtFu1dpg.htm)|Imot|Imot|modificada|
|[kiTcZxmKxGPCwDpg.htm](deities/kiTcZxmKxGPCwDpg.htm)|Kalekot|Kalekot|modificada|
|[KPrtgoIcgiBSKzS3.htm](deities/KPrtgoIcgiBSKzS3.htm)|The Freeing Flame|La llama liberadoraígera|modificada|
|[KT7HKL7vbZEwbzX4.htm](deities/KT7HKL7vbZEwbzX4.htm)|Green Faith|Fe Verde|modificada|
|[LGOkWwN6AZhLiZpl.htm](deities/LGOkWwN6AZhLiZpl.htm)|Touch of the Sun|Touch of the Sun|modificada|
|[LLMTMgARSTlO7S9U.htm](deities/LLMTMgARSTlO7S9U.htm)|Zyphus|Zyphus|modificada|
|[lSciGZGcbU9PJxQy.htm](deities/lSciGZGcbU9PJxQy.htm)|Irez|Irez|modificada|
|[lUFMwdv1UOyzFhIM.htm](deities/lUFMwdv1UOyzFhIM.htm)|Likha|Likha|modificada|
|[lZ74P8kUA1ti808w.htm](deities/lZ74P8kUA1ti808w.htm)|Prophecies of Kalistrade|Profecías de Kalistrade|modificada|
|[M3eb94Nd6uXti2IK.htm](deities/M3eb94Nd6uXti2IK.htm)|Daikitsu|Daikitsu|modificada|
|[MbJ93Nw39h2cBYi5.htm](deities/MbJ93Nw39h2cBYi5.htm)|Lao Shu Po|Lao Shu Po|modificada|
|[McUqN13BGLTpYWCg.htm](deities/McUqN13BGLTpYWCg.htm)|Green Man|Hombre Verde|modificada|
|[mebdNVKeKuTmDDHj.htm](deities/mebdNVKeKuTmDDHj.htm)|Xhamen-Dor|Xhamen-Dor|modificada|
|[N8a76dkGfUvxvSW0.htm](deities/N8a76dkGfUvxvSW0.htm)|Trudd|Trudd|modificada|
|[nsiDGdgbAzkTQzOt.htm](deities/nsiDGdgbAzkTQzOt.htm)|Magrim|Magrim|modificada|
|[nuqIimigyt986WLE.htm](deities/nuqIimigyt986WLE.htm)|The Lantern King|El Rey Linterna|modificada|
|[nuvfUC4TcOvFbKxx.htm](deities/nuvfUC4TcOvFbKxx.htm)|Osiris|Osiris|modificada|
|[o8ayCv3LAHWlu0vQ.htm](deities/o8ayCv3LAHWlu0vQ.htm)|Apsu|Apsu|modificada|
|[o9tK1T28LNEVzpFV.htm](deities/o9tK1T28LNEVzpFV.htm)|Ragdya|Ragdya|modificada|
|[Oci540vcXkJKR0gW.htm](deities/Oci540vcXkJKR0gW.htm)|The Enlightened Scholar's Path|El camino de los iluminados eruditos|modificada|
|[oduHa5yyTisFsIRD.htm](deities/oduHa5yyTisFsIRD.htm)|God Calling|Llamada de Dios|modificada|
|[oJPwMa9xG8oMnC14.htm](deities/oJPwMa9xG8oMnC14.htm)|Apollyon|Apollyon|modificada|
|[OlVCyRgOaPJji4qt.htm](deities/OlVCyRgOaPJji4qt.htm)|Chohar|Chohar|modificada|
|[oV5cnlPJQS8JfMIg.htm](deities/oV5cnlPJQS8JfMIg.htm)|Cosmic Caravan|Caravana Cósmica|modificada|
|[oyp2E685VsQFKMXi.htm](deities/oyp2E685VsQFKMXi.htm)|Dranngvit|Dranngvit|modificada|
|[p4TbCVOwHB4ccmT4.htm](deities/p4TbCVOwHB4ccmT4.htm)|The Endless Road|El camino sin fin|modificada|
|[pCtJONJlhAugUXaG.htm](deities/pCtJONJlhAugUXaG.htm)|Ahriman|Ahriman|modificada|
|[PFp4Sdp2TDDKD62l.htm](deities/PFp4Sdp2TDDKD62l.htm)|Thamir|Thamir|modificada|
|[pm8H2DbtrHM01KNV.htm](deities/pm8H2DbtrHM01KNV.htm)|Naderi|Naderi|modificada|
|[pqrmviqboqjeS4z8.htm](deities/pqrmviqboqjeS4z8.htm)|Eyes That Watch|Ojos que miran|modificada|
|[pVQzTL5elNTxvffu.htm](deities/pVQzTL5elNTxvffu.htm)|The Green Mother|La Madre Verde|modificada|
|[QArqJ387HeYgGZxG.htm](deities/QArqJ387HeYgGZxG.htm)|Gravelady's Guard|Guardia de Gravelady|modificada|
|[QibpLccM5bLUMopj.htm](deities/QibpLccM5bLUMopj.htm)|Lymnieris|Lymnieris|modificada|
|[QlDnoOwf0PppymTn.htm](deities/QlDnoOwf0PppymTn.htm)|Hanspur|Hanspur|modificada|
|[R0DvoLO8WbYIYhb3.htm](deities/R0DvoLO8WbYIYhb3.htm)|Atreia|Atreia|modificada|
|[r6SFB1PzKsagDXff.htm](deities/r6SFB1PzKsagDXff.htm)|Lady Nanbyo|Lady Nanbyo|modificada|
|[ra7mnjAZcEuJxI37.htm](deities/ra7mnjAZcEuJxI37.htm)|Shei|Shei|modificada|
|[RbslST0RZPXj6Eby.htm](deities/RbslST0RZPXj6Eby.htm)|Ashukharma|Ashukharma|modificada|
|[rDTXcBJ8LyqA2TkL.htm](deities/rDTXcBJ8LyqA2TkL.htm)|Szuriel|Szuriel|modificada|
|[rRuxBgC58PxrLzRv.htm](deities/rRuxBgC58PxrLzRv.htm)|Horus|Horus|modificada|
|[RzTd9PmgnfHNlFVf.htm](deities/RzTd9PmgnfHNlFVf.htm)|Ng|Ng|modificada|
|[s1PgIQ8Oi35oS2Et.htm](deities/s1PgIQ8Oi35oS2Et.htm)|Shoanti Animism|Animismo Shoanti|modificada|
|[s81N9hxffmRuy3ik.htm](deities/s81N9hxffmRuy3ik.htm)|Kerkamoth|Kerkamoth|modificada|
|[SCGdJmm7Wzle9jFP.htm](deities/SCGdJmm7Wzle9jFP.htm)|Zohls|Zohls|modificada|
|[SCMlQDcoU1QcF3AS.htm](deities/SCMlQDcoU1QcF3AS.htm)|Lysianassa|Lysianassa|modificada|
|[se2OQ8L4F4ku304o.htm](deities/se2OQ8L4F4ku304o.htm)|Arundhat (The Sacred Perfume)|Arundhat (El Perfume Sagrado)|modificada|
|[sILda7I8Ak20Ioj9.htm](deities/sILda7I8Ak20Ioj9.htm)|Azathoth|Azathoth|modificada|
|[smGfeOk8Jgcz0Qkv.htm](deities/smGfeOk8Jgcz0Qkv.htm)|Immonhiel|Immonhiel|modificada|
|[SUaGdvQXxSAQAw03.htm](deities/SUaGdvQXxSAQAw03.htm)|Yog-Sothoth|Yog-Sothoth|modificada|
|[tb0oADxOoMhyrYRA.htm](deities/tb0oADxOoMhyrYRA.htm)|Yamatsumi|Yamatsumi|modificada|
|[tcXrtRYCOr2EmgGB.htm](deities/tcXrtRYCOr2EmgGB.htm)|Charon|Charon|modificada|
|[tk7gRxXsqvBwQrUF.htm](deities/tk7gRxXsqvBwQrUF.htm)|Grundinnar|Grundinnar|modificada|
|[TpAMy8tb5LrRgvqN.htm](deities/TpAMy8tb5LrRgvqN.htm)|Hathor|Hathor|modificada|
|[TvxsVRztDlGcWpcI.htm](deities/TvxsVRztDlGcWpcI.htm)|Sobek|Sobek|modificada|
|[TW7Q6O928YWNME9r.htm](deities/TW7Q6O928YWNME9r.htm)|Kitumu|Kitumu|modificada|
|[u2tnG7fKkZ2KHMcT.htm](deities/u2tnG7fKkZ2KHMcT.htm)|Suyuddha|Suyuddha|modificada|
|[u3YfcJh1C4w6n75j.htm](deities/u3YfcJh1C4w6n75j.htm)|Raumya|Raumya|modificada|
|[ub0PJ5QkrdoGzzb2.htm](deities/ub0PJ5QkrdoGzzb2.htm)|Luhar|Luhar|modificada|
|[upRQcwjjaksc1g7h.htm](deities/upRQcwjjaksc1g7h.htm)|Enkaar, the Malformed Prisoner|Enkaar, el preso malformado|modificada|
|[UUcAqU3KeJSMNxGF.htm](deities/UUcAqU3KeJSMNxGF.htm)|Barzahk|Barzahk|modificada|
|[uXkMGx0OD896ZQlT.htm](deities/uXkMGx0OD896ZQlT.htm)|Alglenweis|Alglenweis|modificada|
|[uYpxTi4byHc5w78R.htm](deities/uYpxTi4byHc5w78R.htm)|Yuelral|Yuelral|modificada|
|[v1fsLP6nTkiy5ghB.htm](deities/v1fsLP6nTkiy5ghB.htm)|Mazludeh|Mazludeh|modificada|
|[VEyowuhC4FsBnG8c.htm](deities/VEyowuhC4FsBnG8c.htm)|Folgrit|Folgrit|modificada|
|[VG51y4FbTLdeGHZB.htm](deities/VG51y4FbTLdeGHZB.htm)|The Path of the Heavens|El camino de los cielos|modificada|
|[Vk1FM54rJx1RHtUA.htm](deities/Vk1FM54rJx1RHtUA.htm)|Seafarers' Hope|Esperanza de los marinos|modificada|
|[vnmoAGURp8oWCY6R.htm](deities/vnmoAGURp8oWCY6R.htm)|Soralyon|Soralyon|modificada|
|[VRPVppdQ4W0oXeB1.htm](deities/VRPVppdQ4W0oXeB1.htm)|Alocer|Alocer|modificada|
|[vSUz97Y29l1O50bC.htm](deities/vSUz97Y29l1O50bC.htm)|Count Ranalc|Conde Ranalc|modificada|
|[VvxEvEKhCGRwrMJp.htm](deities/VvxEvEKhCGRwrMJp.htm)|Imbrex|Imbrex|modificada|
|[Vzxm5FyIA40b2OSP.htm](deities/Vzxm5FyIA40b2OSP.htm)|Pulura|Pulura|modificada|
|[w4fDrwL8VIqJDPfR.htm](deities/w4fDrwL8VIqJDPfR.htm)|Uvuko|Uvuko|modificada|
|[W4QGkdn9V5ACRpkh.htm](deities/W4QGkdn9V5ACRpkh.htm)|Monad|Mónada|modificada|
|[w66gCNiZ7dslqG8K.htm](deities/w66gCNiZ7dslqG8K.htm)|Lissala|Lissala|modificada|
|[w7B4HD5XOFaLb3ZG.htm](deities/w7B4HD5XOFaLb3ZG.htm)|Hearth and Harvest|Hearth and Harvest|modificada|
|[WfqLY2Fm1LGCcG5X.htm](deities/WfqLY2Fm1LGCcG5X.htm)|Sangpotshi|Sangpotshi|modificada|
|[wjXNXFWBLbJaeZQR.htm](deities/wjXNXFWBLbJaeZQR.htm)|Ketephys|Ketephys|modificada|
|[WO9UsCY41oTtC2G0.htm](deities/WO9UsCY41oTtC2G0.htm)|Pillars of Knowledge|Pilares del conocimiento|modificada|
|[woDEgBs1MznhzpJ4.htm](deities/woDEgBs1MznhzpJ4.htm)|Ra|Ra|modificada|
|[WvoMr86bSSYxXKtI.htm](deities/WvoMr86bSSYxXKtI.htm)|Hastur|Hastur|modificada|
|[XaaDDVS4sEJd4m99.htm](deities/XaaDDVS4sEJd4m99.htm)|Dajermube|Dajermube|modificada|
|[Xb1N5YfxboJH26Jh.htm](deities/Xb1N5YfxboJH26Jh.htm)|Angradd|Angradd|modificada|
|[XHDlP7o1jNz9851v.htm](deities/XHDlP7o1jNz9851v.htm)|Diomazul|Diomazul|modificada|
|[xTD0XFaguBX6QQ2L.htm](deities/xTD0XFaguBX6QQ2L.htm)|Selket|Selket|modificada|
|[xu05XGotyT5st56H.htm](deities/xu05XGotyT5st56H.htm)|Lady Jingxi|Lady Jingxi|modificada|
|[XxIxEaW8NG952Bc0.htm](deities/XxIxEaW8NG952Bc0.htm)|Grasping Iovett|Agarrar Iovett|modificada|
|[xxUGdZYUj5IW7E0L.htm](deities/xxUGdZYUj5IW7E0L.htm)|Followers of Fate|Seguidores del Destino|modificada|
|[XYJupIPx0GW4s7eu.htm](deities/XYJupIPx0GW4s7eu.htm)|Ragadahn|Ragadahn|modificada|
|[y3RlZ3FnbZN8wv8j.htm](deities/y3RlZ3FnbZN8wv8j.htm)|Demon Bringers|Portadores de demonios|modificada|
|[y93whGnGZjFyw5A0.htm](deities/y93whGnGZjFyw5A0.htm)|Vildeis|Vildeis|modificada|
|[YeUhlOt3c98ocwjZ.htm](deities/YeUhlOt3c98ocwjZ.htm)|The Offering Plate|El plato de las ofrendas|modificada|
|[yIfdsL1788boOOYk.htm](deities/yIfdsL1788boOOYk.htm)|Sky Keepers|Guardianes del Cielo|modificada|
|[yiKle2URZ43KYVKQ.htm](deities/yiKle2URZ43KYVKQ.htm)|Chamidu|Chamidu|modificada|
|[yX5U1Uvdug9i08nU.htm](deities/yX5U1Uvdug9i08nU.htm)|Conqueror Worm|Gusano Conquistador|modificada|
|[YXmeBdOZfWgUljW1.htm](deities/YXmeBdOZfWgUljW1.htm)|Ylimancha|Ylimancha|modificada|
|[zgM2jsxw52HjALV0.htm](deities/zgM2jsxw52HjALV0.htm)|The Deliberate Journey|El viaje deliberado|modificada|
|[zhPhYge7VJbIlJvA.htm](deities/zhPhYge7VJbIlJvA.htm)|Kols|Kols|modificada|
|[Zi1UARhode3FSFLr.htm](deities/Zi1UARhode3FSFLr.htm)|General Susumu|General Susumu|modificada|
|[zlJGKpeLBwajP91o.htm](deities/zlJGKpeLBwajP91o.htm)|Gyronna|Gyronna|modificada|
|[zRmAXj7FSlrMfDeA.htm](deities/zRmAXj7FSlrMfDeA.htm)|Bastet|Bastet|modificada|
|[Zw8nhcpRxinoYRSa.htm](deities/Zw8nhcpRxinoYRSa.htm)|Elven Pantheon|Panteón de los Elfos|modificada|
|[ZynnoQJFz12FD0Xn.htm](deities/ZynnoQJFz12FD0Xn.htm)|Wards of the Pharaoh|Wards of the Pharaoh|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0BH54ZVR8NcrGlMu.htm](deities/0BH54ZVR8NcrGlMu.htm)|Findeladlara|Findeladlara|oficial|
|[0nQItOlce5ua6Eaf.htm](deities/0nQItOlce5ua6Eaf.htm)|Sekhmet|Sekhmet|oficial|
|[22n3N47sqbqbDxbp.htm](deities/22n3N47sqbqbDxbp.htm)|Urgathoa|Urgathoa|oficial|
|[4GO4iT585ynLJnc8.htm](deities/4GO4iT585ynLJnc8.htm)|Chaldira|Chaldira|oficial|
|[51kMMWaT6CqpGlGk.htm](deities/51kMMWaT6CqpGlGk.htm)|Achaekek|Achaekek|oficial|
|[88vRw2ZVPax4hhga.htm](deities/88vRw2ZVPax4hhga.htm)|Gorum|Gorum|oficial|
|[8roijlQ03l9rWFFe.htm](deities/8roijlQ03l9rWFFe.htm)|Zevgavizeb|Zevgavizeb|oficial|
|[8ZSywyo8YQPduo92.htm](deities/8ZSywyo8YQPduo92.htm)|Zura|Zura|oficial|
|[9EKyWmjLUu42evxC.htm](deities/9EKyWmjLUu42evxC.htm)|Nethys|Nethys|oficial|
|[9n2CI0B3EA4y3BuU.htm](deities/9n2CI0B3EA4y3BuU.htm)|Alseta|Alseta|oficial|
|[9WD0hbUT0IbnuTiM.htm](deities/9WD0hbUT0IbnuTiM.htm)|Baalzebul|Belcebú|oficial|
|[Af4TSJjutVi9Vdwi.htm](deities/Af4TSJjutVi9Vdwi.htm)|Mahathallah|Mahathallah|oficial|
|[AHX89nhuVhmg8k6w.htm](deities/AHX89nhuVhmg8k6w.htm)|Dagon|Dagon|oficial|
|[aipkJQxP4GBsTaGq.htm](deities/aipkJQxP4GBsTaGq.htm)|Lamashtu|Lamashtu|oficial|
|[aj4ChTlDcZCmrtJD.htm](deities/aj4ChTlDcZCmrtJD.htm)|Baphomet|Bafomet|oficial|
|[AlL9GtlTldbqNoVm.htm](deities/AlL9GtlTldbqNoVm.htm)|Calistria|Calistria|oficial|
|[AMFkYKUNQ3kCD0ob.htm](deities/AMFkYKUNQ3kCD0ob.htm)|Doloras|Doloras|oficial|
|[aTYy1WHwAZiuei4h.htm](deities/aTYy1WHwAZiuei4h.htm)|Tsukiyo|Tsukiyo|oficial|
|[aVyxfqY9GBjTqQul.htm](deities/aVyxfqY9GBjTqQul.htm)|Eiseth|Eiseth|oficial|
|[Bhxji0kxIbMKqdkw.htm](deities/Bhxji0kxIbMKqdkw.htm)|Rovagug|Rovagug|oficial|
|[bLI1JMpI2GXQT5AC.htm](deities/bLI1JMpI2GXQT5AC.htm)|Abraxas|Abraxas|oficial|
|[BNycwu3I21dTh4D9.htm](deities/BNycwu3I21dTh4D9.htm)|Sarenrae|Sarenrae|oficial|
|[CzHjFLx4hpIEIoWt.htm](deities/CzHjFLx4hpIEIoWt.htm)|Dispater|Dispater|oficial|
|[d56paSkcwvll2OhR.htm](deities/d56paSkcwvll2OhR.htm)|Abadar|Abadar|oficial|
|[DEKKbg6riAdL2ARf.htm](deities/DEKKbg6riAdL2ARf.htm)|Hei Feng|Hei Feng|oficial|
|[eErEiVmBqltYXFJ6.htm](deities/eErEiVmBqltYXFJ6.htm)|Iomedae|Iomedae|oficial|
|[ENzMYuzBZGIujSMx.htm](deities/ENzMYuzBZGIujSMx.htm)|Treerazer|Arrasador de árboles|oficial|
|[fTJLgTfEMZreJ19r.htm](deities/fTJLgTfEMZreJ19r.htm)|Milani|Milani|oficial|
|[fuPTR1laXO6EionS.htm](deities/fuPTR1laXO6EionS.htm)|Grandmother Spider|Abuela Araña|oficial|
|[g1VxllMjs2YRIJAm.htm](deities/g1VxllMjs2YRIJAm.htm)|Moloch|Moloch|oficial|
|[GX6PF9ifR47Xyjg9.htm](deities/GX6PF9ifR47Xyjg9.htm)|Norgorber|Norgorber|oficial|
|[gXHoxuuiAD54LnWV.htm](deities/gXHoxuuiAD54LnWV.htm)|Cyth-V'sug|Cyth-V'sug|oficial|
|[gxwmzxUWBOhdgFSN.htm](deities/gxwmzxUWBOhdgFSN.htm)|Shelyn|Shelyn|oficial|
|[HtRxo4J3elFteIqu.htm](deities/HtRxo4J3elFteIqu.htm)|Arazni|Arazni|oficial|
|[iJyhAFmHOLZBvWnJ.htm](deities/iJyhAFmHOLZBvWnJ.htm)|Sivanah|Sivanah|oficial|
|[iXvyp9rGnqvFfy4q.htm](deities/iXvyp9rGnqvFfy4q.htm)|Kazutal|Kazutal|oficial|
|[IZcdF1G8ZsnMotaM.htm](deities/IZcdF1G8ZsnMotaM.htm)|Pazuzu|Pazuzu|oficial|
|[JgqH3BhuEuA4Zyqs.htm](deities/JgqH3BhuEuA4Zyqs.htm)|Desna|Desna|oficial|
|[jL1qiVOZBKXETtVD.htm](deities/jL1qiVOZBKXETtVD.htm)|Nocticula|Nocticula|oficial|
|[KWVdoAm3b01M8Lcp.htm](deities/KWVdoAm3b01M8Lcp.htm)|Mephistopheles|Mefistófeles|oficial|
|[mm1j6UbHqFCI8fwB.htm](deities/mm1j6UbHqFCI8fwB.htm)|Erastil|Erastil|oficial|
|[Mv6uTJLh3VGEoGLH.htm](deities/Mv6uTJLh3VGEoGLH.htm)|Nivi Rhombodazzle|Nivi Rhombodazzle|oficial|
|[oiEo6tDP3gICHtfZ.htm](deities/oiEo6tDP3gICHtfZ.htm)|Barbatos|Barbatos|oficial|
|[oRvuLq3o2FxXizt9.htm](deities/oRvuLq3o2FxXizt9.htm)|Ghlaunder|Ghlaunder|oficial|
|[qqqrbq4uCrelQvgH.htm](deities/qqqrbq4uCrelQvgH.htm)|Irori|Irori|oficial|
|[QRkcFciCOmFoxF1B.htm](deities/QRkcFciCOmFoxF1B.htm)|Asmodeus|Asmodeo|oficial|
|[QZD0u1jxwz0kj8uI.htm](deities/QZD0u1jxwz0kj8uI.htm)|Pharasma|Farasma|oficial|
|[r5vooG7NfvA9prJK.htm](deities/r5vooG7NfvA9prJK.htm)|Kurgess|Kurgess|oficial|
|[remyY1BwhEVmBwya.htm](deities/remyY1BwhEVmBwya.htm)|Belial|Belial|oficial|
|[RUzMtIQal0drPwQY.htm](deities/RUzMtIQal0drPwQY.htm)|Gogunta|Gogunta|oficial|
|[s9opv94lGDRKnw6W.htm](deities/s9opv94lGDRKnw6W.htm)|Shizuru|Shizuru|oficial|
|[SAuFz101iP6VVwvw.htm](deities/SAuFz101iP6VVwvw.htm)|Nurgal|Nurgal|oficial|
|[sNjPI1iaS8Z43YJf.htm](deities/sNjPI1iaS8Z43YJf.htm)|Mammon|Mammon|oficial|
|[soiVh53y9RAeSLEk.htm](deities/soiVh53y9RAeSLEk.htm)|Casandalee|Casandalee|oficial|
|[swwwP7eVmlNuWTb7.htm](deities/swwwP7eVmlNuWTb7.htm)|Gozreh|Gozreh|oficial|
|[sZrdzh0ANQnZGXJb.htm](deities/sZrdzh0ANQnZGXJb.htm)|Torag|Torag|oficial|
|[tcvobm8O84qv0aO5.htm](deities/tcvobm8O84qv0aO5.htm)|Gruhastha|Gruhastha|oficial|
|[uydTiyN5AzhBnQY8.htm](deities/uydTiyN5AzhBnQY8.htm)|Ardad Lili|Ardad Lili|oficial|
|[V5PbbkZUP9N7kl6h.htm](deities/V5PbbkZUP9N7kl6h.htm)|Kabriri|Kabriri|oficial|
|[v67fHklTZ6LoU54q.htm](deities/v67fHklTZ6LoU54q.htm)|Cayden Cailean|Cayden Cailean|oficial|
|[vEMCpm7iidycRT5D.htm](deities/vEMCpm7iidycRT5D.htm)|Brigh|Brigh|oficial|
|[wkDOeK7ENz1ra8IC.htm](deities/wkDOeK7ENz1ra8IC.htm)|Besmara|Besmara|oficial|
|[WloVxo6HXJdS4YTt.htm](deities/WloVxo6HXJdS4YTt.htm)|Groetus|Groetus|oficial|
|[WOiSJ4IWjaRsSgRO.htm](deities/WOiSJ4IWjaRsSgRO.htm)|Geryon|Gerión|oficial|
|[ZfN7jkK6boU1FuiS.htm](deities/ZfN7jkK6boU1FuiS.htm)|Zon-Kuthon|Zon-Kuthon|oficial|
|[zzhYFcShT9JoE2Mp.htm](deities/zzhYFcShT9JoE2Mp.htm)|Shax|Shax|oficial|
