# Estado de la traducción (other-effects)

 * **modificada**: 32
 * **oficial**: 1
 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[ZXUOdZqUW22OX3ge.htm](other-effects/ZXUOdZqUW22OX3ge.htm)|Effect: Avert Gaze|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0r8MznJe5UugViee.htm](other-effects/0r8MznJe5UugViee.htm)|Effect: -15-foot status penalty to your land Speed|Efecto: -15 pies de situación de penalización a tu Velocidad terrestre.|modificada|
|[5gX3PZztMecmWxX9.htm](other-effects/5gX3PZztMecmWxX9.htm)|Effect: -2 circumstance penalty to attack rolls|Efecto: penalización de -2 de circunstancia a las tiradas de ataque.|modificada|
|[9c93NfZpENofiGUp.htm](other-effects/9c93NfZpENofiGUp.htm)|Effect: Mounted|Efecto: Montado|modificada|
|[AHMUpMbaVkZ5A1KX.htm](other-effects/AHMUpMbaVkZ5A1KX.htm)|Effect: Aid|Efecto: Prestar ayuda|modificada|
|[BjkhK1nFUw3vNKg2.htm](other-effects/BjkhK1nFUw3vNKg2.htm)|Effect: -2 circumstance penalty to spell attack rolls and spell DC's|Efecto: penalizador -2 de circunstancia a las tiradas de ataque y CC de hechizos.|modificada|
|[c6cwOc4Zri6QiqsR.htm](other-effects/c6cwOc4Zri6QiqsR.htm)|Effect: -2 circumstance penalty to checks and saving throws until healed|Efecto: penalización de -2 de circunstancia a los chequeos y tiradas de salvación hasta que se cure.|modificada|
|[Ck0MQP9og75AWix7.htm](other-effects/Ck0MQP9og75AWix7.htm)|Effect: -2 circumstance penalty to attack rolls until healed|Efecto: -2 penalizador de circunstancia a las tiradas de ataque hasta que sea curado.|modificada|
|[EMqGwUi3VMhCjTlF.htm](other-effects/EMqGwUi3VMhCjTlF.htm)|Effect: Scouting|Efecto: Explorar|modificada|
|[fqk0ESqJACXqz21k.htm](other-effects/fqk0ESqJACXqz21k.htm)|Effect: -1 circumstance penalty to attack rolls until you score a critical hit|Efecto: -1 penalizador de circunstancia a las tiradas de ataque hasta que consigas un golpe crítico.|modificada|
|[HLBhINtyRcHk6d7h.htm](other-effects/HLBhINtyRcHk6d7h.htm)|Effect: +2 circumstance bonus to attack rolls|Efecto: Bonificador +2 de circunstancia a las tiradas de ataque|modificada|
|[HTwG0F0LDu5ihBUm.htm](other-effects/HTwG0F0LDu5ihBUm.htm)|Effect: -5-foot status penalty to your land Speed|Efecto: -5 pies de situación de penalización a tu Velocidad terrestre.|modificada|
|[i5WccD9u62QPRZI8.htm](other-effects/i5WccD9u62QPRZI8.htm)|Effect: -2 circumstance penalty to attack rolls made with this attack until healed|Efecto: -2 de penalización de circunstancia a las tiradas de ataque realizadas con este ataque hasta que se cure.|modificada|
|[I9lfZUiCwMiGogVi.htm](other-effects/I9lfZUiCwMiGogVi.htm)|Effect: Cover|Efecto: Cobertura|modificada|
|[IUvO6CatanKAgmNr.htm](other-effects/IUvO6CatanKAgmNr.htm)|Effect: +1 circumstance bonus to attack rolls for 3 rounds|Efecto: Bonificador +1 de circunstancia a las tiradas de ataque durante 3 asaltos.|modificada|
|[lTwF7aCtQ6napqYu.htm](other-effects/lTwF7aCtQ6napqYu.htm)|Effect: +2 status bonus to attack rolls|Efecto: Bonificador +2 de situación a las tiradas de ataque.|modificada|
|[NU5wAbJLZO6T86eB.htm](other-effects/NU5wAbJLZO6T86eB.htm)|Effect: -10-foot circumstance penalty to your land Speed|Efecto: penalización de circunstancia de -10 pies a tu Velocidad terrestre.|modificada|
|[PaS2kwv4vueI4PC7.htm](other-effects/PaS2kwv4vueI4PC7.htm)|Effect: +2 status bonus to saving throws against spells for 1 min|Efecto: Bonificador +2 de situación a las tiradas de salvación contra hechizos durante 1 min.|modificada|
|[pbKvPM4cnmGTkDiZ.htm](other-effects/pbKvPM4cnmGTkDiZ.htm)|Effect: +4 circumstance bonus to AC against your ranged attacks|Efecto: Bonificador +4 de circunstancia a la CA contra tus ataques a distancia.|modificada|
|[QGTMOcJ1Dq5bk0GG.htm](other-effects/QGTMOcJ1Dq5bk0GG.htm)|Effect: +2 circumstance bonus to AC|Efecto: +2 bonus de circunstancia a la CA|modificada|
|[RcMZr0wSjrir1AnS.htm](other-effects/RcMZr0wSjrir1AnS.htm)|Effect: -5-foot circumstance penalty to your land Speed|Efecto: -5 pies de penalización de circunstancia a tu Velocidad terrestre.|modificada|
|[ScZGg91ri19H4IIc.htm](other-effects/ScZGg91ri19H4IIc.htm)|Effect: -10-foot circumstance penalty to all Speeds|Efecto: -10-foot circumstance penalty to all Velocidades|modificada|
|[sq8lhVfn0pl5WJ4t.htm](other-effects/sq8lhVfn0pl5WJ4t.htm)|Effect: +2 status bonus to AC and all saving throws|Efecto: Bonificador +2 de situación a la CA y a todas las tiradas de salvación.|modificada|
|[TPbr1kErAAJKBi3V.htm](other-effects/TPbr1kErAAJKBi3V.htm)|Effect: Aquatic Combat|Efecto: Combate Acuático|modificada|
|[uwYGEeqvt8pwjhXa.htm](other-effects/uwYGEeqvt8pwjhXa.htm)|Effect: Dazzled until end of your next turn|Efecto: Deslumbrado hasta el final de tu siguiente turno|modificada|
|[UZabiyynJWuUblMk.htm](other-effects/UZabiyynJWuUblMk.htm)|Effect: -2 circumstance penalty to attack rolls with this weapon|Efecto: penalización de -2 de circunstancia a las tiradas de ataque con esta arma.|modificada|
|[VCSpuc3Tf3XWMkd3.htm](other-effects/VCSpuc3Tf3XWMkd3.htm)|Effect: Follow The Expert|Efecto: Seguir al experto|modificada|
|[W2OF7VeLHqc7p3DO.htm](other-effects/W2OF7VeLHqc7p3DO.htm)|Effect: Deafened until end of your next turn|Efecto: Ensordecido hasta el final de tu siguiente turno|modificada|
|[wHWWHkjDXmJl4Ia6.htm](other-effects/wHWWHkjDXmJl4Ia6.htm)|Effect: Adverse Subsist Situation|Efecto: Situación Subsistente Adversa|modificada|
|[WqgtMWgM9nUjwQiI.htm](other-effects/WqgtMWgM9nUjwQiI.htm)|Effect: Resistance 5 to all damage|Efecto: Resistencia 5 a todo el daño.|modificada|
|[wVGSlZEGxsg1s8AZ.htm](other-effects/wVGSlZEGxsg1s8AZ.htm)|Effect: -2 circumstance penalty to ranged attacks|Efecto: -2 de penalización de circunstancia a los ataques a distancia.|modificada|
|[y1GwyXv7iOf8DhBg.htm](other-effects/y1GwyXv7iOf8DhBg.htm)|Effect: Flat-Footed until end of your next turn|Efecto: Pies planos hasta el final de tu siguiente turno.|modificada|
|[zDV1wo2ytNTbyTB0.htm](other-effects/zDV1wo2ytNTbyTB0.htm)|Effect: -10-foot status penalty to your land Speed|Efecto: penalización de situación de -10 pies a tu Velocidad terrestre.|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[la8rWwUtReElgTS6.htm](other-effects/la8rWwUtReElgTS6.htm)|Effect: Scouting (Incredible Scout)|Efecto: Explorar (Batidor increíble).|oficial|
