# Estado de la traducción (backgrounds)

 * **modificada**: 305
 * **ninguna**: 14
 * **oficial**: 36


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0ncxOTCDptu01nR8.htm](backgrounds/0ncxOTCDptu01nR8.htm)|Dauntless|
|[2UXBsyJFimwYOUFQ.htm](backgrounds/2UXBsyJFimwYOUFQ.htm)|Thrill-Seeker|
|[bdeuzUMiPjEdlPS8.htm](backgrounds/bdeuzUMiPjEdlPS8.htm)|Crown of Chaos|
|[GeIFY5C1BxuWQGc7.htm](backgrounds/GeIFY5C1BxuWQGc7.htm)|Bookish Providence|
|[gsyTHkbGRaiHgs6p.htm](backgrounds/gsyTHkbGRaiHgs6p.htm)|Firebrand Follower|
|[KboE8OcjCLD7oFWQ.htm](backgrounds/KboE8OcjCLD7oFWQ.htm)|Beast Blessed|
|[kkPdZsf61qySImQD.htm](backgrounds/kkPdZsf61qySImQD.htm)|Runner|
|[m2sWdTIXlyXmqsuC.htm](backgrounds/m2sWdTIXlyXmqsuC.htm)|Keys to Destiny|
|[maxaKunHAOKUrR4q.htm](backgrounds/maxaKunHAOKUrR4q.htm)|Writ in the Stars|
|[Mz7G3rTPxV2Xzy18.htm](backgrounds/Mz7G3rTPxV2Xzy18.htm)|Hammered by Fate|
|[r2ZRAcoDp6EdwNrh.htm](backgrounds/r2ZRAcoDp6EdwNrh.htm)|Free Spirit|
|[Vq6kOTBm114bV9y7.htm](backgrounds/Vq6kOTBm114bV9y7.htm)|Shielded Fortune|
|[xkRZUxWna7BcNnHZ.htm](backgrounds/xkRZUxWna7BcNnHZ.htm)|Unremarkable|
|[zwjoAOGkT44MmDKT.htm](backgrounds/zwjoAOGkT44MmDKT.htm)|Alloysmith|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0c9Np7Yq5JSxZ6Tb.htm](backgrounds/0c9Np7Yq5JSxZ6Tb.htm)|Alkenstar Tinker|Alkenstar Trastear|modificada|
|[0FgYkkKv9u8zxWiO.htm](backgrounds/0FgYkkKv9u8zxWiO.htm)|Child of the Twin Village|Niño de la aldea gemela|modificada|
|[0otxwSLobUCf0l1I.htm](backgrounds/0otxwSLobUCf0l1I.htm)|Surge Investigator|Oleaje Investigador|modificada|
|[0wzTmGUb8yvzMrO0.htm](backgrounds/0wzTmGUb8yvzMrO0.htm)|Back-Alley Doctor|Back-Alley Doctor|modificada|
|[18Jzk3gAsXspFOYC.htm](backgrounds/18Jzk3gAsXspFOYC.htm)|Haunted Citizen|Haunted Citizen|modificada|
|[1bjReaLqCD9pvBJ3.htm](backgrounds/1bjReaLqCD9pvBJ3.htm)|Academy Dropout|Academy Dropout|modificada|
|[1Fpf40RBd0EgwP7R.htm](backgrounds/1Fpf40RBd0EgwP7R.htm)|Sentinel Reflectance|Reflectancia centinela|modificada|
|[1M91pTjatEejBjEl.htm](backgrounds/1M91pTjatEejBjEl.htm)|Hired Killer|Hired Killer|modificada|
|[1VdLr4Qm8fv1m4tM.htm](backgrounds/1VdLr4Qm8fv1m4tM.htm)|Godless Graycloak|Godless Graycloak|modificada|
|[2bzqI0D4J3LUi8nq.htm](backgrounds/2bzqI0D4J3LUi8nq.htm)|Laborer|Bracero|modificada|
|[2FUdcPsXwoWT1bms.htm](backgrounds/2FUdcPsXwoWT1bms.htm)|Tech Reliant|Tech Reliant|modificada|
|[2jeonnY3GZTdEMsm.htm](backgrounds/2jeonnY3GZTdEMsm.htm)|Harrow-Led|Harrow-Led|modificada|
|[2lk5NOcu1aUglUdK.htm](backgrounds/2lk5NOcu1aUglUdK.htm)|Fireworks Performer|Intérprete de fuegos artificiales|modificada|
|[2Mm0BHMJZhqIqoQG.htm](backgrounds/2Mm0BHMJZhqIqoQG.htm)|Undertaker|Enterrador|modificada|
|[2qH61dLeaqgNOdOp.htm](backgrounds/2qH61dLeaqgNOdOp.htm)|Desert Tracker|Rastreador del Desierto|modificada|
|[2TImvKwcFLDVhzUC.htm](backgrounds/2TImvKwcFLDVhzUC.htm)|Circuit Judge|Juez de Circuito|modificada|
|[2wGmG7ntcnkvtw8l.htm](backgrounds/2wGmG7ntcnkvtw8l.htm)|Lost Loved One|Lost Loved One|modificada|
|[3frMfODIYFeqTl2k.htm](backgrounds/3frMfODIYFeqTl2k.htm)|Fortune Teller|Adivino|modificada|
|[3kXTGUvodNMnJTxb.htm](backgrounds/3kXTGUvodNMnJTxb.htm)|Aiudara Seeker|Aiudara Seeker|modificada|
|[3M2FRDlunjFshzbq.htm](backgrounds/3M2FRDlunjFshzbq.htm)|Fogfen Tale-Teller|Cuentacuentos de Marismaniebla|modificada|
|[3wLnNwWnZ2dHIbV4.htm](backgrounds/3wLnNwWnZ2dHIbV4.htm)|Diobel Pearl Diver|Diobel Pearl Diver|modificada|
|[3WPo7m6rJQh9L7MN.htm](backgrounds/3WPo7m6rJQh9L7MN.htm)|Emissary|Emisario|modificada|
|[3YQ1Wcjzk8ftoyo7.htm](backgrounds/3YQ1Wcjzk8ftoyo7.htm)|Astrologer|Astrólogo|modificada|
|[4aVFnYyRajog0mNl.htm](backgrounds/4aVFnYyRajog0mNl.htm)|Mantis Scion|Mantis Scion|modificada|
|[4fBIXtSVSRYn2ZGi.htm](backgrounds/4fBIXtSVSRYn2ZGi.htm)|Goblinblood Orphan|Goblinblood Orphan|modificada|
|[4II2KZpizlBBcfCy.htm](backgrounds/4II2KZpizlBBcfCy.htm)|Borderlands Pioneer|Borderlands Pioneer|modificada|
|[4KCa67gyxUn60zdf.htm](backgrounds/4KCa67gyxUn60zdf.htm)|Saloon Entertainer|Animador de Saloon|modificada|
|[4lbfPXwZEf3eE0ip.htm](backgrounds/4lbfPXwZEf3eE0ip.htm)|Snubbed Out Stoolie|Snubbed Out Stoolie|modificada|
|[4naQmCXBl0007c2W.htm](backgrounds/4naQmCXBl0007c2W.htm)|Touched by Dahak|Tocado por Dahak|modificada|
|[4Vc8uXVHonrasFnU.htm](backgrounds/4Vc8uXVHonrasFnU.htm)|Saboteur|Saboteador|modificada|
|[4XpWgGejwb2L6WpK.htm](backgrounds/4XpWgGejwb2L6WpK.htm)|Codebreaker|Codebreaker|modificada|
|[4yN5miHoMvKwZIsa.htm](backgrounds/4yN5miHoMvKwZIsa.htm)|Press-Ganged (LOWG)|Aprovechar (LOWG)|modificada|
|[5Bfzt3sZyiaKrBO0.htm](backgrounds/5Bfzt3sZyiaKrBO0.htm)|Sign Bound|Sign Bound|modificada|
|[5dezuNcZJ6wfecnG.htm](backgrounds/5dezuNcZJ6wfecnG.htm)|Gunsmith|Gunsmith|modificada|
|[5lcDlcXKD8eji6n3.htm](backgrounds/5lcDlcXKD8eji6n3.htm)|Astrological Augur|Astrological Augur|modificada|
|[5Oh9SdD4rhwlpHzg.htm](backgrounds/5Oh9SdD4rhwlpHzg.htm)|Tide Watcher|Vigilante de la marea|modificada|
|[5qUQOpdlNsJjpFVX.htm](backgrounds/5qUQOpdlNsJjpFVX.htm)|Ruin Delver|Ruin Cavador|modificada|
|[5RGLAPi5sLykRcmm.htm](backgrounds/5RGLAPi5sLykRcmm.htm)|Animal Whisperer|Susurrador de animales|modificada|
|[5RhHdnzWWKfh5faz.htm](backgrounds/5RhHdnzWWKfh5faz.htm)|Guest of Sedeq Lodge|Invitado de Sedeq Lodge|modificada|
|[5Z3cLEpsx9nHVwhM.htm](backgrounds/5Z3cLEpsx9nHVwhM.htm)|Hunter|Cazador|modificada|
|[6c0rsuiiAaVqGTu7.htm](backgrounds/6c0rsuiiAaVqGTu7.htm)|Rivethun Adherent|Rivethun Adherente|modificada|
|[6irgRkKZ8tRZzLvs.htm](backgrounds/6irgRkKZ8tRZzLvs.htm)|Artisan|Artesano|modificada|
|[6K6jJkjZ2MJYqQ6h.htm](backgrounds/6K6jJkjZ2MJYqQ6h.htm)|Bellflower Agent|Agente Bellflower|modificada|
|[6UmhTxOQeqFnppxx.htm](backgrounds/6UmhTxOQeqFnppxx.htm)|Guard|Guardia|modificada|
|[6vsoyCZKqxG0lVe8.htm](backgrounds/6vsoyCZKqxG0lVe8.htm)|Inlander|Inlander|modificada|
|[76j9ds5URXv1dqnm.htm](backgrounds/76j9ds5URXv1dqnm.htm)|Plant Whisperer|Susurrador de Plantas|modificada|
|[76RK9WizWYdyhMy5.htm](backgrounds/76RK9WizWYdyhMy5.htm)|Mammoth Speaker|Mammoth Speaker|modificada|
|[7AfixHrjbXgDPPkp.htm](backgrounds/7AfixHrjbXgDPPkp.htm)|Translator|Traductor|modificada|
|[7fCZTzmv5I2dI4sr.htm](backgrounds/7fCZTzmv5I2dI4sr.htm)|Discarded Duplicate|Descartado Duplicado|modificada|
|[7IrOApgShgnmp1A5.htm](backgrounds/7IrOApgShgnmp1A5.htm)|Rigger|Rigger|modificada|
|[7K6ZSWOoihZKSdyd.htm](backgrounds/7K6ZSWOoihZKSdyd.htm)|Ruby Phoenix Enthusiast|Entusiasta del Rubí Fénix|modificada|
|[7OAX5QMd15svZJzX.htm](backgrounds/7OAX5QMd15svZJzX.htm)|Relentless Dedication|Dedicación implacable|modificada|
|[7QkQQsHjv2iNFIsF.htm](backgrounds/7QkQQsHjv2iNFIsF.htm)|Grave Robber|Grave Robber|modificada|
|[7z33GaNsmSxel1xJ.htm](backgrounds/7z33GaNsmSxel1xJ.htm)|Doomcaller|Doomcaller|modificada|
|[84uVpQFCqn0Atfpo.htm](backgrounds/84uVpQFCqn0Atfpo.htm)|Legendary Parents|Padres Legendarios|modificada|
|[86TbxxwfpWjScwSQ.htm](backgrounds/86TbxxwfpWjScwSQ.htm)|Undercover Lotus Guard|Guardia de loto encubierto|modificada|
|[88WyCqU5x1eJ0MK2.htm](backgrounds/88WyCqU5x1eJ0MK2.htm)|Gladiator|Gladiador|modificada|
|[89LEOv97ZwsjnhNx.htm](backgrounds/89LEOv97ZwsjnhNx.htm)|Gambler|Jugador|modificada|
|[8CLTXqyjdzEMKnZC.htm](backgrounds/8CLTXqyjdzEMKnZC.htm)|Cannoneer|Cannoneer|modificada|
|[8hsMIh3lVGfZwjG5.htm](backgrounds/8hsMIh3lVGfZwjG5.htm)|Pillar|Pilar|modificada|
|[8q4PhvpmIxZD7rsV.htm](backgrounds/8q4PhvpmIxZD7rsV.htm)|Rostland Partisan|Rostland Partisan|modificada|
|[8UEKgUkagUDixkL2.htm](backgrounds/8UEKgUkagUDixkL2.htm)|Issian Partisan|Issian Partisan|modificada|
|[8usMHYAmFdqmmkTS.htm](backgrounds/8usMHYAmFdqmmkTS.htm)|Sponsored by a Village|Patrocinado por un Pueblo|modificada|
|[8UXahQfkP9GZ1TNW.htm](backgrounds/8UXahQfkP9GZ1TNW.htm)|Nomad|Nómada|modificada|
|[90vPOBrSXY27k0bL.htm](backgrounds/90vPOBrSXY27k0bL.htm)|Tyrant Witness|Testigo Tirano|modificada|
|[93icIDHD4IrqI2oV.htm](backgrounds/93icIDHD4IrqI2oV.htm)|Sodden Scavenger|Sodden Scavenger|modificada|
|[9l6q90sk8UM292CY.htm](backgrounds/9l6q90sk8UM292CY.htm)|Waste Walker|Waste Walker|modificada|
|[9LnXsMRwYcxi7nDO.htm](backgrounds/9LnXsMRwYcxi7nDO.htm)|Bekyar Restorer|Bekyar Restorer|modificada|
|[9lVw1JGl5ser6626.htm](backgrounds/9lVw1JGl5ser6626.htm)|Criminal|Delincuente|modificada|
|[9uqDWl8V2AgGMRXi.htm](backgrounds/9uqDWl8V2AgGMRXi.htm)|Propaganda Promoter|Promotor de Propaganda|modificada|
|[9uTdwJaj27F18ZDX.htm](backgrounds/9uTdwJaj27F18ZDX.htm)|Razmiran Faithful|Fiel a Razmiran|modificada|
|[a45LqkSRX07ljKdW.htm](backgrounds/a45LqkSRX07ljKdW.htm)|Merabite Prodigy|Merabite Prodigy|modificada|
|[a5dCSuAwGE2hqQjj.htm](backgrounds/a5dCSuAwGE2hqQjj.htm)|Freed Slave of Absalom|Esclavo liberado de Absalom|modificada|
|[a8BmnIIUR7AYog5B.htm](backgrounds/a8BmnIIUR7AYog5B.htm)|Barkeep|Barman|modificada|
|[a9Q4iIiAZryVWN27.htm](backgrounds/a9Q4iIiAZryVWN27.htm)|Bounty Hunter|Cazarrecompensas|modificada|
|[a9YjyQRNEOyespFf.htm](backgrounds/a9YjyQRNEOyespFf.htm)|Pyre Tender|Pyre Tender|modificada|
|[AfBCrHsw1xbRFejN.htm](backgrounds/AfBCrHsw1xbRFejN.htm)|Sleepless Suns Star|Estrella Soles Insomnes|modificada|
|[aisuJF1A98bHfkLH.htm](backgrounds/aisuJF1A98bHfkLH.htm)|Whispering Way Scion|Whispering Way Scion|modificada|
|[AJ41zFEYwlOUghXp.htm](backgrounds/AJ41zFEYwlOUghXp.htm)|Osirionologist|Osirionólogo|modificada|
|[ajcpRVb5EG00l7Y4.htm](backgrounds/ajcpRVb5EG00l7Y4.htm)|Cursed Family|Familia maldita|modificada|
|[Am8kwC9c2GQ5bJAW.htm](backgrounds/Am8kwC9c2GQ5bJAW.htm)|Undercover Contender|Undercover Contender|modificada|
|[ap25MWBuFGwwhYIG.htm](backgrounds/ap25MWBuFGwwhYIG.htm)|Aspiring Free-Captain|Aspirante a Capitán Libre|modificada|
|[apXTV7jJx6yJpj8D.htm](backgrounds/apXTV7jJx6yJpj8D.htm)|Prisoner|Preso|modificada|
|[AqhHif8mzYjlGMxJ.htm](backgrounds/AqhHif8mzYjlGMxJ.htm)|Sheriff|Sheriff|modificada|
|[aWAfj7bhTZM2oK81.htm](backgrounds/aWAfj7bhTZM2oK81.htm)|Hookclaw Digger|Hookclaw Digger|modificada|
|[b1588nhgRoYMef44.htm](backgrounds/b1588nhgRoYMef44.htm)|Wanderlust|Wanderlust|modificada|
|[b3UC18ueX8m9Ov0W.htm](backgrounds/b3UC18ueX8m9Ov0W.htm)|Hermit|Ermita|modificada|
|[B3Z5gbhMDRUATTrE.htm](backgrounds/B3Z5gbhMDRUATTrE.htm)|Gloriana's Fixer|Gloriana's Fixer|modificada|
|[B6kNrX1y8XNbQYea.htm](backgrounds/B6kNrX1y8XNbQYea.htm)|Goldhand Arms Dealer|Traficante de armas Goldhand|modificada|
|[B8kEwzPUMIjhofUm.htm](backgrounds/B8kEwzPUMIjhofUm.htm)|Sarkorian Survivor|Sarkorian Survivor|modificada|
|[b9EPEY09dYOVzdue.htm](backgrounds/b9EPEY09dYOVzdue.htm)|Shadow War Survivor|Superviviente de la Guerra de las Sombras|modificada|
|[BBeJA7n0xpSsBCGq.htm](backgrounds/BBeJA7n0xpSsBCGq.htm)|Lesser Scion|Vástago menor|modificada|
|[bCJ9p3P5uJDAtaUI.htm](backgrounds/bCJ9p3P5uJDAtaUI.htm)|Miner|Minero|modificada|
|[bDyb0k0rTfDTyhd8.htm](backgrounds/bDyb0k0rTfDTyhd8.htm)|Geb Crusader|Geb Cruzada|modificada|
|[bh6O2Ad5mkYwRngM.htm](backgrounds/bh6O2Ad5mkYwRngM.htm)|Hermean Expatriate|Hermean Expatriado|modificada|
|[bNsYkUGMKmtc28MX.htm](backgrounds/bNsYkUGMKmtc28MX.htm)|Issian Patriot|Patriota Issian|modificada|
|[BRXXlw03K6ZeB2Li.htm](backgrounds/BRXXlw03K6ZeB2Li.htm)|Eidolon Contact|Contacto Eidolon|modificada|
|[bxWID85noazU72O3.htm](backgrounds/bxWID85noazU72O3.htm)|Ratted-Out Gun Runner|Ratted-Out Gun Runner|modificada|
|[byyceQSVT0H5GfNB.htm](backgrounds/byyceQSVT0H5GfNB.htm)|Anti-Magical|Anti-Magical|modificada|
|[BZhPPw9VD9U2ur6B.htm](backgrounds/BZhPPw9VD9U2ur6B.htm)|Witchlight Follower|Seguidor de la luz espectral|modificada|
|[CAjQrHZZbALE7Qjy.htm](backgrounds/CAjQrHZZbALE7Qjy.htm)|Acolyte|Acólito|modificada|
|[cCi1Lsld9Umz1uHs.htm](backgrounds/cCi1Lsld9Umz1uHs.htm)|Sense of Belonging|Sentido de Pertenencia|modificada|
|[ChAoT1V3Nc4sKXz4.htm](backgrounds/ChAoT1V3Nc4sKXz4.htm)|Corpse Stitcher|Stitcher cadáver|modificada|
|[CjNA8ippwoAvpxeI.htm](backgrounds/CjNA8ippwoAvpxeI.htm)|Ozem Experience|Experiencia Ozem|modificada|
|[CKU1sbFofcwZUJMx.htm](backgrounds/CKU1sbFofcwZUJMx.htm)|Ex-Con Token Guard|Ex-Con Token Guardia|modificada|
|[cODrdTvko4Om26ik.htm](backgrounds/cODrdTvko4Om26ik.htm)|Mechanic|Mecánico|modificada|
|[D00NdG0WtUNMO546.htm](backgrounds/D00NdG0WtUNMO546.htm)|Mammoth Herder|Mammoth Herder|modificada|
|[d5fKB0ZMJQkwDF5p.htm](backgrounds/d5fKB0ZMJQkwDF5p.htm)|Otherworldly Mission|Misión de otro mundo|modificada|
|[DHrzVqB8f1ed3zTk.htm](backgrounds/DHrzVqB8f1ed3zTk.htm)|Clown|Payaso|modificada|
|[DpmWrtkrmmgnVtAc.htm](backgrounds/DpmWrtkrmmgnVtAc.htm)|Construction Occultist|Ocultista de la Construcción|modificada|
|[dSYel8ABTevOc5YA.htm](backgrounds/dSYel8ABTevOc5YA.htm)|Clockfighter|Clockfighter|modificada|
|[dVRDDjT4FOu6uLDR.htm](backgrounds/dVRDDjT4FOu6uLDR.htm)|Detective|Detective|modificada|
|[DVtZab19D1vD3a0n.htm](backgrounds/DVtZab19D1vD3a0n.htm)|Post Guard of All Trades|Guardia de puesto de todos los oficios|modificada|
|[E2ij2Cg8oMC0W0NS.htm](backgrounds/E2ij2Cg8oMC0W0NS.htm)|Nirmathi Guerrilla|Nirmathi Guerrilla|modificada|
|[ECKic2p6yIyoGYod.htm](backgrounds/ECKic2p6yIyoGYod.htm)|Songsinger in Training|Songsinger in Training|modificada|
|[eGPlKAoYBTUqjyrr.htm](backgrounds/eGPlKAoYBTUqjyrr.htm)|Tomb Born|Tomb Born|modificada|
|[eHfsMNiVeqwnrpG3.htm](backgrounds/eHfsMNiVeqwnrpG3.htm)|Framed in Ferrous Quarter|Framed in Ferrous Quarter|modificada|
|[EJRWGsPWzAhixuvQ.htm](backgrounds/EJRWGsPWzAhixuvQ.htm)|Belkzen Slayer|Belkzen Slayer|modificada|
|[EYPorNgoYcp37akm.htm](backgrounds/EYPorNgoYcp37akm.htm)|Magical Misfit|Magical Misfit|modificada|
|[eYY3bX7xSH7aicqT.htm](backgrounds/eYY3bX7xSH7aicqT.htm)|Atteran Rancher|Atteran Ranchero|modificada|
|[ffcNsTUBsxFwbNgJ.htm](backgrounds/ffcNsTUBsxFwbNgJ.htm)|Lastwall Survivor|Lastwall Survivor|modificada|
|[FKHut73XDUGTnKkP.htm](backgrounds/FKHut73XDUGTnKkP.htm)|Field Medic|Médico de campa|modificada|
|[FlXu29r5C4CborZv.htm](backgrounds/FlXu29r5C4CborZv.htm)|Friend of Greensteeples|Amigo de Greensteeples|modificada|
|[fML6YrXYDqQy0g7L.htm](backgrounds/fML6YrXYDqQy0g7L.htm)|Iolite Trainee Hobgoblin|Iolite Entrenador Hobgoblin|modificada|
|[fo2nGLI1t1b7nAHK.htm](backgrounds/fo2nGLI1t1b7nAHK.htm)|Dreams of Vengeance|Sueños de Venganza|modificada|
|[FovAoohhvG7vIbX9.htm](backgrounds/FovAoohhvG7vIbX9.htm)|Gold Falls Regular|Gold Falls Regular|modificada|
|[FVE2mg6EEyPVLz3M.htm](backgrounds/FVE2mg6EEyPVLz3M.htm)|Student of Magic|Estudiante de Magia|modificada|
|[fyKPDYFeIrgzADJB.htm](backgrounds/fyKPDYFeIrgzADJB.htm)|Alkenstar Outlaw|Alkenstar Outlaw|modificada|
|[FznrY4fZASquT6hY.htm](backgrounds/FznrY4fZASquT6hY.htm)|Sponsored by a Stranger|Apadrinado por un desconocido|modificada|
|[G3GIfGLY1xSNqj17.htm](backgrounds/G3GIfGLY1xSNqj17.htm)|Almas Clerk|Almas Clerk|modificada|
|[g8xUX7DAu2ShZ90Q.htm](backgrounds/g8xUX7DAu2ShZ90Q.htm)|Empty Whispers|Empty Whispers|modificada|
|[gfklP8ub45R4wXKe.htm](backgrounds/gfklP8ub45R4wXKe.htm)|Aspiring River Monarch|Aspirante a monarca del río|modificada|
|[GNidqGnSABx1rQUQ.htm](backgrounds/GNidqGnSABx1rQUQ.htm)|Missionary|Misionero|modificada|
|[H3E69w8Xg0T7rAqD.htm](backgrounds/H3E69w8Xg0T7rAqD.htm)|Shoanti Name-Bearer|Shoanti Name-Bearer|modificada|
|[h98cEl4DY75IL6KJ.htm](backgrounds/h98cEl4DY75IL6KJ.htm)|Teamster|Cochero|modificada|
|[HDquvQywAZimmcFF.htm](backgrounds/HDquvQywAZimmcFF.htm)|Refugee (Fall of Plaguestone)|Refugiado (Caída de Plaguestone)|modificada|
|[HEd2Lxgvl080nRxx.htm](backgrounds/HEd2Lxgvl080nRxx.htm)|Shadow Lodge Defector|Sombra Lodge Defector|modificada|
|[HiOvPmXEXBjUy0VZ.htm](backgrounds/HiOvPmXEXBjUy0VZ.htm)|Brevic Outcast|Brevic Outcast|modificada|
|[HKRcQO8Xj7xzBxAw.htm](backgrounds/HKRcQO8Xj7xzBxAw.htm)|Faction Opportunist|Facción Oportunista|modificada|
|[HMIfr9N4rco1dzBO.htm](backgrounds/HMIfr9N4rco1dzBO.htm)|Reclaimed|Reclaimed|modificada|
|[HNdGlDHkcczqNw2U.htm](backgrounds/HNdGlDHkcczqNw2U.htm)|Musical Prodigy|Prodigio Musical|modificada|
|[hPx0xiv00GQqPWUH.htm](backgrounds/hPx0xiv00GQqPWUH.htm)|Kalistrade Follower|Seguidor de Kalistrade|modificada|
|[HrWEyXTgIj16Z1RR.htm](backgrounds/HrWEyXTgIj16Z1RR.htm)|Reborn Soul|Alma renacida|modificada|
|[HsAZDAPpqynArFAp.htm](backgrounds/HsAZDAPpqynArFAp.htm)|Magaambya Academic|Magaambya Académico|modificada|
|[HSNUDvPgO1NESW7S.htm](backgrounds/HSNUDvPgO1NESW7S.htm)|Junk Collector|Junk Collector|modificada|
|[HZ3oBBdEnsH3fWrm.htm](backgrounds/HZ3oBBdEnsH3fWrm.htm)|Shory Seeker|Shory Seeker|modificada|
|[I0vuIFypx8ADSJQC.htm](backgrounds/I0vuIFypx8ADSJQC.htm)|Black Market Smuggler|Contrabandista del Mercado Negro|modificada|
|[i4hN6OYv8qmi3GLW.htm](backgrounds/i4hN6OYv8qmi3GLW.htm)|Entertainer|Animador|modificada|
|[I5cRrqrPCHsQqFI9.htm](backgrounds/I5cRrqrPCHsQqFI9.htm)|Magical Merchant|Mágico mercader|modificada|
|[i5G6E5dkGWiq838C.htm](backgrounds/i5G6E5dkGWiq838C.htm)|Scholar of the Ancients|Erudito de los Antiguos|modificada|
|[i6y4DiKvqitdE0PW.htm](backgrounds/i6y4DiKvqitdE0PW.htm)|Quick|Quick|modificada|
|[i79pgNIAtJfkkOiw.htm](backgrounds/i79pgNIAtJfkkOiw.htm)|Tinker|Trastear|modificada|
|[IFHYbU6Nu8BiTsRa.htm](backgrounds/IFHYbU6Nu8BiTsRa.htm)|Acrobat|Acróbata|modificada|
|[IfpYRxN8qyV4ym0o.htm](backgrounds/IfpYRxN8qyV4ym0o.htm)|Purveyor of the Bizarre|Purveyor of the Bizarre|modificada|
|[IoBhge83aYpq0pPV.htm](backgrounds/IoBhge83aYpq0pPV.htm)|Archaeologist|Arqueólogo|modificada|
|[IObZEUz8wneEMgR3.htm](backgrounds/IObZEUz8wneEMgR3.htm)|Deckhand|Deckhand|modificada|
|[IOcjPmcemrQFFb2b.htm](backgrounds/IOcjPmcemrQFFb2b.htm)|Time Traveler|Time Traveler|modificada|
|[irDibuV3Wi7T43sL.htm](backgrounds/irDibuV3Wi7T43sL.htm)|Child of the Puddles|Niño de los Charcos|modificada|
|[IuwNiQSSeRMQyDE7.htm](backgrounds/IuwNiQSSeRMQyDE7.htm)|Sword Scion|Espada Scion|modificada|
|[iWWg16f3re1YChiD.htm](backgrounds/iWWg16f3re1YChiD.htm)|Oenopion-Ooze Tender|Oenopion-Ooze Tender|modificada|
|[j9v38iHA0sVy59SR.htm](backgrounds/j9v38iHA0sVy59SR.htm)|Pathfinder Hopeful|Pathfinder Hopeful|modificada|
|[JauSkDtMV6dhDZS8.htm](backgrounds/JauSkDtMV6dhDZS8.htm)|Political Scion|Political Scion|modificada|
|[JBRBb2818DZ4hjXw.htm](backgrounds/JBRBb2818DZ4hjXw.htm)|Total Power|Potencia Total|modificada|
|[JfGVkZkaoz2lnmov.htm](backgrounds/JfGVkZkaoz2lnmov.htm)|Bright Lion|Leones brillantes|modificada|
|[jjkY6r7NNWhxDqja.htm](backgrounds/jjkY6r7NNWhxDqja.htm)|Once Bitten|Una vez Muerdemuerde|modificada|
|[jpgO5zofecGeyXd5.htm](backgrounds/jpgO5zofecGeyXd5.htm)|Occult Librarian|Ocultismo Bibliotecario|modificada|
|[K35I1WCbzT5xnJ6N.htm](backgrounds/K35I1WCbzT5xnJ6N.htm)|Animal Wrangler|Animal Wrangler|modificada|
|[KC5oI1bs6Wx8h91u.htm](backgrounds/KC5oI1bs6Wx8h91u.htm)|Deputy|Adjunto|modificada|
|[KEG5KFNrXKhTsj6J.htm](backgrounds/KEG5KFNrXKhTsj6J.htm)|Money Counter|Contador de dinero|modificada|
|[khGFmnQMBYmz2ONR.htm](backgrounds/khGFmnQMBYmz2ONR.htm)|Sarkorian Reclaimer|Sarkorian Reclaimer|modificada|
|[KHMEcCCG132ILkuU.htm](backgrounds/KHMEcCCG132ILkuU.htm)|Starless One|Starless One|modificada|
|[kLjqmylGOXeQ5o5Y.htm](backgrounds/kLjqmylGOXeQ5o5Y.htm)|Bonuwat Wavetouched|Bonuwat Wavetouched|modificada|
|[kmhZZgR7KlBzRBX0.htm](backgrounds/kmhZZgR7KlBzRBX0.htm)|Driver|Conductor|modificada|
|[KMv7ollLVaZ81XDV.htm](backgrounds/KMv7ollLVaZ81XDV.htm)|Merchant|Mercader|modificada|
|[l5Kj0owzxfPcTvIb.htm](backgrounds/l5Kj0owzxfPcTvIb.htm)|Unsponsored|Unsponsored|modificada|
|[lav3yRNPc7lQ7e9k.htm](backgrounds/lav3yRNPc7lQ7e9k.htm)|Senghor Sailor|Senghor Marino|modificada|
|[LbuWzGpCIP79UwVB.htm](backgrounds/LbuWzGpCIP79UwVB.htm)|Spell Seeker|Spell Seeker|modificada|
|[lCR8gyEZbwqh3RWi.htm](backgrounds/lCR8gyEZbwqh3RWi.htm)|Harbor Guard Moonlighter|Guardia del puerto Moonlighter|modificada|
|[LHk50lz5Kk5ZYTeo.htm](backgrounds/LHk50lz5Kk5ZYTeo.htm)|Abadar's Avenger|Vengador de Abadar|modificada|
|[locc0cjOmOQHe3j7.htm](backgrounds/locc0cjOmOQHe3j7.htm)|Savior of Air|Salvador del Aire|modificada|
|[LoeTd2SS6jfEgo1H.htm](backgrounds/LoeTd2SS6jfEgo1H.htm)|Genie-Blessed|Genio-Bendecido|modificada|
|[lqjmBmGHYRaSiglZ.htm](backgrounds/lqjmBmGHYRaSiglZ.htm)|Molthuni Mercenary|Mercenario Molthuni|modificada|
|[LwAu4r3uocYfpKA8.htm](backgrounds/LwAu4r3uocYfpKA8.htm)|Trailblazer|Trailblazer|modificada|
|[LWoPiYHAyLp8pvYx.htm](backgrounds/LWoPiYHAyLp8pvYx.htm)|Broken Tusk Recruiter|Broken Tusk Reclutador|modificada|
|[lX5KDS2hU5LihZRs.htm](backgrounds/lX5KDS2hU5LihZRs.htm)|Martial Disciple|Discípulo marcial|modificada|
|[m1vRLRHTpCrgk89G.htm](backgrounds/m1vRLRHTpCrgk89G.htm)|Thrune Loyalist|Leal Thrune|modificada|
|[M8EfhY6Knb2iQm6S.htm](backgrounds/M8EfhY6Knb2iQm6S.htm)|Press-Ganged (G&G)|Aprovechar (G&G)|modificada|
|[mcL4WLO2yxBGlvuG.htm](backgrounds/mcL4WLO2yxBGlvuG.htm)|Farmsteader|Granjero|modificada|
|[MgiZ25JdT6O3fLbO.htm](backgrounds/MgiZ25JdT6O3fLbO.htm)|Anti-Tech Activist|Anti-Tech Activist|modificada|
|[MiRWGXZnEdurMvVf.htm](backgrounds/MiRWGXZnEdurMvVf.htm)|Eldritch Anatomist|Anatomista sobrenatural|modificada|
|[moVRsnpjB5THCwxE.htm](backgrounds/moVRsnpjB5THCwxE.htm)|Street Urchin|Erizo callejero|modificada|
|[mrkgVjiEdlPjLUsN.htm](backgrounds/mrkgVjiEdlPjLUsN.htm)|Vidrian Reformer|Vidrian Reformer|modificada|
|[MrZvq1ebEgEN9cIv.htm](backgrounds/MrZvq1ebEgEN9cIv.htm)|Sandswept Survivor|Sandswept Survivor|modificada|
|[MslumKt6iwJ85GKZ.htm](backgrounds/MslumKt6iwJ85GKZ.htm)|Thuvian Unifier|Unificador Thuvian|modificada|
|[mWpNrTiREluJ6fLB.htm](backgrounds/mWpNrTiREluJ6fLB.htm)|Able Carter|Able Carter|modificada|
|[mxJRdRSMsyZfBf5c.htm](backgrounds/mxJRdRSMsyZfBf5c.htm)|Hermean Heritor|Hermean Heritor|modificada|
|[n2JN5Kiu7tOCAHPr.htm](backgrounds/n2JN5Kiu7tOCAHPr.htm)|Market Runner|Recadero del mercado|modificada|
|[n7tPT1SfVTcHjrj3.htm](backgrounds/n7tPT1SfVTcHjrj3.htm)|Tapestry Refugee|Tapestry Refugiado|modificada|
|[nAe65lvsOJAIHGGT.htm](backgrounds/nAe65lvsOJAIHGGT.htm)|False Medium|Falso Medio|modificada|
|[nhQKn1tVV6PKCurq.htm](backgrounds/nhQKn1tVV6PKCurq.htm)|Butcher|Carnicero|modificada|
|[nXnaV9JwUG1N2dsg.htm](backgrounds/nXnaV9JwUG1N2dsg.htm)|Attention Addict|Adicto a la atención|modificada|
|[NXYce9NAHls2fcIf.htm](backgrounds/NXYce9NAHls2fcIf.htm)|Pathfinder Recruiter|Pathfinder Recruiter|modificada|
|[NywLl1XMQmzA6rP7.htm](backgrounds/NywLl1XMQmzA6rP7.htm)|Demon Slayer|Cazador de Demonios|modificada|
|[NZY0r4Csjul6eVPp.htm](backgrounds/NZY0r4Csjul6eVPp.htm)|Finadar Leshy|Finadar Leshy (s), leshys (pl.)|modificada|
|[o3ArTg5c8pLe7iGm.htm](backgrounds/o3ArTg5c8pLe7iGm.htm)|Song of the Deep|Song of the Deep|modificada|
|[O4xKRtHb21DCTvQ0.htm](backgrounds/O4xKRtHb21DCTvQ0.htm)|Wished Alive|Deseado Vivo|modificada|
|[o7RbsQbv5iLRvd8j.htm](backgrounds/o7RbsQbv5iLRvd8j.htm)|Scout|Explorador|modificada|
|[OD8RTS7cTeMJJFcR.htm](backgrounds/OD8RTS7cTeMJJFcR.htm)|Learned Guard Prodigy|Prodigio de guardia aprendido.|modificada|
|[OhP7cqvNouFgHIdJ.htm](backgrounds/OhP7cqvNouFgHIdJ.htm)|Sewer Dragon|Dragón de Alcantarilla|modificada|
|[oj4B6KLAOlUbY0wr.htm](backgrounds/oj4B6KLAOlUbY0wr.htm)|Eclipseborn|Eclipseborn|modificada|
|[p27PSjFtHAWikKaw.htm](backgrounds/p27PSjFtHAWikKaw.htm)|Early Explorer|Explorador temprano|modificada|
|[P65AGDPkhD2B4JtG.htm](backgrounds/P65AGDPkhD2B4JtG.htm)|Perfection Seeker|Perfection Seeker|modificada|
|[p6Asr6f2cI2BWorr.htm](backgrounds/p6Asr6f2cI2BWorr.htm)|Sponsored by Teacher Ot|Patrocinado por el profesor Ot|modificada|
|[pBX18FI1grWwkWjk.htm](backgrounds/pBX18FI1grWwkWjk.htm)|Kyonin Emissary|Emisario Kyonin|modificada|
|[pChM2ApTVEQ8SohP.htm](backgrounds/pChM2ApTVEQ8SohP.htm)|Reclaimer Investigator|Reclamador Investigador|modificada|
|[PhqUBXLLkVXb6oUE.htm](backgrounds/PhqUBXLLkVXb6oUE.htm)|Taldan Schemer|Taldan Schemer|modificada|
|[q1OCPBAgYgvwy1Of.htm](backgrounds/q1OCPBAgYgvwy1Of.htm)|Megafauna Hunter|Cazador de Megafauna|modificada|
|[qB7g1OiZ8v8zgvkL.htm](backgrounds/qB7g1OiZ8v8zgvkL.htm)|Wonder Taster|Wonder Taster|modificada|
|[qJNmSgzq0ae29qCC.htm](backgrounds/qJNmSgzq0ae29qCC.htm)|Printer|Impresora|modificada|
|[QnL7hqUi9HPenrbC.htm](backgrounds/QnL7hqUi9HPenrbC.htm)|Newcomer In Need|Newcomer In Need|modificada|
|[qNpLqx6LhBo1jY4A.htm](backgrounds/qNpLqx6LhBo1jY4A.htm)|Blow-In|Blow-In|modificada|
|[qY4IUwVWIKPSFskP.htm](backgrounds/qY4IUwVWIKPSFskP.htm)|Aerialist|Aerialist|modificada|
|[qzKT7L1qldz14q8M.htm](backgrounds/qzKT7L1qldz14q8M.htm)|Legacy of the Hammer|Legado del Martillo|modificada|
|[r0kYIbN06Cv8eNG3.htm](backgrounds/r0kYIbN06Cv8eNG3.htm)|Warrior|Combatiente|modificada|
|[R1v4gUu8oRMoOASM.htm](backgrounds/R1v4gUu8oRMoOASM.htm)|Wildwood Local|Wildwood Local|modificada|
|[RC4l6WsxPn89a1f8.htm](backgrounds/RC4l6WsxPn89a1f8.htm)|Raised by Belief|Educado en la fe|modificada|
|[RPU1aNfVz3ZbymvV.htm](backgrounds/RPU1aNfVz3ZbymvV.htm)|Scion of Slayers|Vástago de los Asesinos|modificada|
|[RxhdWJUoRTBEeHYZ.htm](backgrounds/RxhdWJUoRTBEeHYZ.htm)|Night Watch|Vigilancia Nocturna|modificada|
|[SJ3nNOI5A8A4hK0Q.htm](backgrounds/SJ3nNOI5A8A4hK0Q.htm)|Winter's Child|Winter's Child|modificada|
|[Sj91CUyEUeWoPh2R.htm](backgrounds/Sj91CUyEUeWoPh2R.htm)|Toymaker|Juguetero|modificada|
|[SOmJyAtPOokesZoe.htm](backgrounds/SOmJyAtPOokesZoe.htm)|Farmhand|Labrador|modificada|
|[sR3S7Xn15drU6rOF.htm](backgrounds/sR3S7Xn15drU6rOF.htm)|Starwatcher|Vigilante estelar|modificada|
|[StWWI7Wi0WRgRmxS.htm](backgrounds/StWWI7Wi0WRgRmxS.htm)|Nocturnal Navigator|Navegante Nocturno|modificada|
|[su8y75pGMVTUsNHK.htm](backgrounds/su8y75pGMVTUsNHK.htm)|Thassilonian Traveler|Thassilonian Traveler|modificada|
|[t0t1ck8iKpaI4o5W.htm](backgrounds/t0t1ck8iKpaI4o5W.htm)|Charlatan|Charlatán|modificada|
|[T537wo3aem8GvnmR.htm](backgrounds/T537wo3aem8GvnmR.htm)|Hounded Thief|Ladrón acosado|modificada|
|[T8SlgHjj0hVjKW2Q.htm](backgrounds/T8SlgHjj0hVjKW2Q.htm)|Sponsored by Family|Patrocinado por la familia|modificada|
|[tA0nggrWfBEhvsKA.htm](backgrounds/tA0nggrWfBEhvsKA.htm)|Chelish Rebel|Chelish Rebel|modificada|
|[tcsSxwkl4wCsfO3k.htm](backgrounds/tcsSxwkl4wCsfO3k.htm)|Trade Consortium Underling|Subordinado del Consorcio de Comercio|modificada|
|[tQ9t7uIssRCR2y3W.htm](backgrounds/tQ9t7uIssRCR2y3W.htm)|Final Blade Survivor|Final Blade Survivor|modificada|
|[TQBYPTRTVGLMv7cx.htm](backgrounds/TQBYPTRTVGLMv7cx.htm)|Local Brigand|Local Brigand|modificada|
|[tqnrnXVTbdehohPL.htm](backgrounds/tqnrnXVTbdehohPL.htm)|Street Preacher|Street Preacher|modificada|
|[Tujic4RHrQJmEYX4.htm](backgrounds/Tujic4RHrQJmEYX4.htm)|Mystic Tutor|Tutor Místico|modificada|
|[Ty8FRM0k262xuHfF.htm](backgrounds/Ty8FRM0k262xuHfF.htm)|Undersea Enthusiast|Entusiasta Submarino|modificada|
|[U1gbRkmZqJ7SmpeF.htm](backgrounds/U1gbRkmZqJ7SmpeF.htm)|Banished Brighite|Brighite desterrado|modificada|
|[U8347JbRAjhowP1q.htm](backgrounds/U8347JbRAjhowP1q.htm)|Brevic Noble|Brevic Noble|modificada|
|[uF9nw15tK6b1bgre.htm](backgrounds/uF9nw15tK6b1bgre.htm)|Ruby Phoenix Fanatic|Ruby Phoenix Fanatic|modificada|
|[UFHezf1LXUwcQIAQ.htm](backgrounds/UFHezf1LXUwcQIAQ.htm)|Wandering Preacher|Predicador Errante|modificada|
|[uJcFanGjVranEarv.htm](backgrounds/uJcFanGjVranEarv.htm)|Lumber Consortium Laborer|Lumber Consorcio bracero|modificada|
|[UK40FVVVx6IKxipW.htm](backgrounds/UK40FVVVx6IKxipW.htm)|Inexplicably Expelled|Inexplicablemente expulsado|modificada|
|[Uk4W7mKQbLtDDHwo.htm](backgrounds/Uk4W7mKQbLtDDHwo.htm)|Curandero|Curandero|modificada|
|[UNbV4UwZEXxJy703.htm](backgrounds/UNbV4UwZEXxJy703.htm)|Sun Dancer|Sun Dancer|modificada|
|[uNhdcyhiog7YvXPT.htm](backgrounds/uNhdcyhiog7YvXPT.htm)|Varisian Wanderer|Varisian Wanderer|modificada|
|[uNvD4XK1kdvGjQVo.htm](backgrounds/uNvD4XK1kdvGjQVo.htm)|Child of Westcrown|Niño de Westcrown|modificada|
|[UU4MOe7Lozt9V8tg.htm](backgrounds/UU4MOe7Lozt9V8tg.htm)|Seer of the Dead|Vidente de los Muertos|modificada|
|[UURvnfwXypRYYXBI.htm](backgrounds/UURvnfwXypRYYXBI.htm)|Storm Survivor|Superviviente de la tormenta|modificada|
|[uwZ0emSBFNMGA74j.htm](backgrounds/uwZ0emSBFNMGA74j.htm)|Tall Tale|Tall Tale|modificada|
|[UyddtAwqDGjQ1SZK.htm](backgrounds/UyddtAwqDGjQ1SZK.htm)|Scholar of the Sky Key|Erudito de la Llave del Cielo|modificada|
|[v0WPfxN6G8XFfFZT.htm](backgrounds/v0WPfxN6G8XFfFZT.htm)|Scholar|Erudito|modificada|
|[V1RAIckpUJd2OzXi.htm](backgrounds/V1RAIckpUJd2OzXi.htm)|Mana Wastes Refugee|Mana Wastes Refugiado|modificada|
|[V31KRG7aA7xS0m8L.htm](backgrounds/V31KRG7aA7xS0m8L.htm)|Shadow Haunted|Sombra Embrujada|modificada|
|[V3nYEAhyA54RtYky.htm](backgrounds/V3nYEAhyA54RtYky.htm)|Ulfen Raider|Ulfen Raider|modificada|
|[V9VsomfjbPQNfaSL.htm](backgrounds/V9VsomfjbPQNfaSL.htm)|Disciple of the Gear|Disciple of the Gear|modificada|
|[vBPu7RwNXGDQ1ThL.htm](backgrounds/vBPu7RwNXGDQ1ThL.htm)|Dreamer of the Verdant Moon|Sodor de la luna verdosa.|modificada|
|[vE6nb2OSIXqprDXk.htm](backgrounds/vE6nb2OSIXqprDXk.htm)|Sally Guard Neophyte|Sally Guardia Neófito|modificada|
|[vEl3OBUsSmPh8b4N.htm](backgrounds/vEl3OBUsSmPh8b4N.htm)|Friendly Darkmoon Kobold|Amistoso Darkmoon Kobold|modificada|
|[vgin9ff2sUBMpuaI.htm](backgrounds/vgin9ff2sUBMpuaI.htm)|Former Aspis Agent|Former Aspis Agent|modificada|
|[vjhB0ZTV9OZgSuSz.htm](backgrounds/vjhB0ZTV9OZgSuSz.htm)|Thassilonian Delver|Thassilonian Cavador|modificada|
|[vmdhw37F22FCMm50.htm](backgrounds/vmdhw37F22FCMm50.htm)|Food Trader|Comerciante de comida|modificada|
|[vMyMUZpg8MYZT2AZ.htm](backgrounds/vMyMUZpg8MYZT2AZ.htm)|Wish for Riches|Deseo de riquezas|modificada|
|[vTyG1bQkEENZHroY.htm](backgrounds/vTyG1bQkEENZHroY.htm)|Junker|Junker|modificada|
|[w2GSRC5uMcSUfPGJ.htm](backgrounds/w2GSRC5uMcSUfPGJ.htm)|Chosen One|Elegido|modificada|
|[W6OIPZhjcV45gfoX.htm](backgrounds/W6OIPZhjcV45gfoX.htm)|Revenant|Revenant|modificada|
|[wGB222d5UIBGYWsK.htm](backgrounds/wGB222d5UIBGYWsK.htm)|Mechanical Symbiosis|Simbiosis Mecánica|modificada|
|[WIWR8jURAdSAzxIh.htm](backgrounds/WIWR8jURAdSAzxIh.htm)|Magical Experiment|Experimento Mágico|modificada|
|[wKiFedYlHCsM5wN4.htm](backgrounds/wKiFedYlHCsM5wN4.htm)|Rostlander|Rostlander|modificada|
|[wU1qd8tZNcYn43y2.htm](backgrounds/wU1qd8tZNcYn43y2.htm)|Lost and Alone|Perdido y solo|modificada|
|[WueM94C9JXk10jPd.htm](backgrounds/WueM94C9JXk10jPd.htm)|Onyx Trader|Onyx Trader|modificada|
|[wz4fDeZtCvxC4vyO.htm](backgrounds/wz4fDeZtCvxC4vyO.htm)|Second Chance Champion|Campeón de Segunda oportunidad|modificada|
|[wZikbWmCWYpjFxQF.htm](backgrounds/wZikbWmCWYpjFxQF.htm)|Wanted Witness|Testigo buscado|modificada|
|[x2y25cE98Eq4qxbu.htm](backgrounds/x2y25cE98Eq4qxbu.htm)|Ustalavic Academic|Ustalavic Académico|modificada|
|[x3fFxtLogoy5hZfe.htm](backgrounds/x3fFxtLogoy5hZfe.htm)|Medicinal Clocksmith|Medicinal Clocksmith|modificada|
|[xbyQ1RAF6x4ceXLf.htm](backgrounds/xbyQ1RAF6x4ceXLf.htm)|Energy Scarred|Energía Scarred|modificada|
|[XcacLiaGDgjzHS3T.htm](backgrounds/XcacLiaGDgjzHS3T.htm)|Spotter|Spotter|modificada|
|[xCCvT9tprRQVFVDq.htm](backgrounds/xCCvT9tprRQVFVDq.htm)|Grizzled Muckrucker|Grizzled Muckrucker|modificada|
|[XHY0xrSSbX0cTJKK.htm](backgrounds/XHY0xrSSbX0cTJKK.htm)|Droskari Disciple|Droskari Discípulo|modificada|
|[xKEQvxDMHWRUkL6i.htm](backgrounds/xKEQvxDMHWRUkL6i.htm)|Clockwork Researcher|Investigador Mecánico|modificada|
|[xvz5F7iYBWEIjz0r.htm](backgrounds/xvz5F7iYBWEIjz0r.htm)|Bibliophile|Bibliófilo|modificada|
|[xyHndp512sLU4UOL.htm](backgrounds/xyHndp512sLU4UOL.htm)|Northland Forager|Northland Forrajeador|modificada|
|[y0WZS51fSi6dILHq.htm](backgrounds/y0WZS51fSi6dILHq.htm)|Secular Medic|Secular Medic|modificada|
|[Y35nOXZRryiyHjlk.htm](backgrounds/Y35nOXZRryiyHjlk.htm)|Nexian Mystic|Nexian Mystic|modificada|
|[Y50ssWBBKSRVBpSa.htm](backgrounds/Y50ssWBBKSRVBpSa.htm)|Mystic Seer|Mystic Seer|modificada|
|[y9OyNsxGfmjqdcP0.htm](backgrounds/y9OyNsxGfmjqdcP0.htm)|Artist|Artista|modificada|
|[Yai061jIDzojHzsn.htm](backgrounds/Yai061jIDzojHzsn.htm)|Necromancer's Apprentice|Necromancer's Apprentice|modificada|
|[yAtyaKbcHZWCJlf5.htm](backgrounds/yAtyaKbcHZWCJlf5.htm)|Witch Wary|Witch Wary|modificada|
|[YJpEdmSOjlA2QZeu.htm](backgrounds/YJpEdmSOjlA2QZeu.htm)|Circus Born|Circus Born|modificada|
|[yK40c3082U30BUX5.htm](backgrounds/yK40c3082U30BUX5.htm)|Grand Council Bureaucrat|Gran Consejo Burócrata|modificada|
|[ykYaQwIX7sxvbhwc.htm](backgrounds/ykYaQwIX7sxvbhwc.htm)|Willing Host|Anfitrión Voluntario|modificada|
|[ynObDI0VbZ4sqeMI.htm](backgrounds/ynObDI0VbZ4sqeMI.htm)|Ex-Mendevian Crusader|Ex-Mendevian Crusader|modificada|
|[Yu7Cl0Lk94LdPRi6.htm](backgrounds/Yu7Cl0Lk94LdPRi6.htm)|Noble|Noble|modificada|
|[Yuwr2pT3z3WYTX9T.htm](backgrounds/Yuwr2pT3z3WYTX9T.htm)|Alkenstar Sojourner|Alkenstar Sojourner|modificada|
|[YyzIzLxn2UCFubj4.htm](backgrounds/YyzIzLxn2UCFubj4.htm)|Menagerie Dung Sweeper|Menagerie Dung Sweeper|modificada|
|[yZUzLCeuaIkNk4up.htm](backgrounds/yZUzLCeuaIkNk4up.htm)|Saved by Clockwork|Salvado por Clockwork|modificada|
|[z4cCsOT36MB7xldR.htm](backgrounds/z4cCsOT36MB7xldR.htm)|Barker|Barker|modificada|
|[ZdhPKEY9FfaOS8Wy.htm](backgrounds/ZdhPKEY9FfaOS8Wy.htm)|Herbalist|Herbolario|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0z0PSizHviOehdJF.htm](backgrounds/0z0PSizHviOehdJF.htm)|Haunting Vision|Visión inquietante|oficial|
|[0ZfBP7Tp2P3WN7Dp.htm](backgrounds/0ZfBP7Tp2P3WN7Dp.htm)|Amnesiac|Amnésico|oficial|
|[3gN09dOT2hMwGcK2.htm](backgrounds/3gN09dOT2hMwGcK2.htm)|Cook|Cocinero|oficial|
|[4a2sVO0o2mMTydN8.htm](backgrounds/4a2sVO0o2mMTydN8.htm)|Local Scion|Heredero local|oficial|
|[6abqATPjYoF946LD.htm](backgrounds/6abqATPjYoF946LD.htm)|Bandit|Bandido|oficial|
|[9pK15dQJVypSCjzO.htm](backgrounds/9pK15dQJVypSCjzO.htm)|Blessed|Bendecido|oficial|
|[dAvFZ5QmbAHgXcNp.htm](backgrounds/dAvFZ5QmbAHgXcNp.htm)|Outrider|Jinete|oficial|
|[DBxOUwM7qhGH8MrF.htm](backgrounds/DBxOUwM7qhGH8MrF.htm)|Courier|Mensajero|oficial|
|[fuTLDmihr9Z9e5wa.htm](backgrounds/fuTLDmihr9Z9e5wa.htm)|Cultist|Sectario|oficial|
|[GPI5kNu0xfom9kKa.htm](backgrounds/GPI5kNu0xfom9kKa.htm)|Dragon Scholar|Erudito de los dragones|oficial|
|[HdnmIaLadhRfZq8X.htm](backgrounds/HdnmIaLadhRfZq8X.htm)|Insurgent|Insurgente|oficial|
|[i28Z9JXhEvoc7BX5.htm](backgrounds/i28Z9JXhEvoc7BX5.htm)|Refugee (APG)|Refugiado (Guía del jugador avanzada)|oficial|
|[iaM6TjvijLCgiHeD.htm](backgrounds/iaM6TjvijLCgiHeD.htm)|Returning Descendant|Descendiente retornado|oficial|
|[ixluAGUDZciLEHtb.htm](backgrounds/ixluAGUDZciLEHtb.htm)|Tax Collector|Recaudador|oficial|
|[IXxdCzBS0xP20ckw.htm](backgrounds/IXxdCzBS0xP20ckw.htm)|Ward|Custodia|oficial|
|[j2G71vQahw1DiWpO.htm](backgrounds/j2G71vQahw1DiWpO.htm)|Cursed|Maldito|oficial|
|[ns9BOvCyjdapYhI0.htm](backgrounds/ns9BOvCyjdapYhI0.htm)|Root Worker|Trabajador de las raíces|oficial|
|[o1lhSKOpKPamTITI.htm](backgrounds/o1lhSKOpKPamTITI.htm)|Bookkeeper|Contable|oficial|
|[oEm937kNrP5sXxFD.htm](backgrounds/oEm937kNrP5sXxFD.htm)|Emancipated|Liberado|oficial|
|[pGOlKz4Krnh7MyUM.htm](backgrounds/pGOlKz4Krnh7MyUM.htm)|Haunted|Embrujado|oficial|
|[Phvnfdmz4bB7jrI3.htm](backgrounds/Phvnfdmz4bB7jrI3.htm)|Barrister|Abogado|oficial|
|[ppBGlWl0UkBKkJgE.htm](backgrounds/ppBGlWl0UkBKkJgE.htm)|Feybound|Vinculado a las hadas|oficial|
|[Q2brdDtEoI3cmpuD.htm](backgrounds/Q2brdDtEoI3cmpuD.htm)|Feral Child|Niño salvaje|oficial|
|[qbvzNG8hMjb8f66D.htm](backgrounds/qbvzNG8hMjb8f66D.htm)|Squire|Escudero|oficial|
|[r9fzNQEz33HyKTxm.htm](backgrounds/r9fzNQEz33HyKTxm.htm)|Pilgrim|Peregrino|oficial|
|[rzyRtasSTfHS3e0y.htm](backgrounds/rzyRtasSTfHS3e0y.htm)|Returned|Devuelto|oficial|
|[TC7jpN5EA4UBIYep.htm](backgrounds/TC7jpN5EA4UBIYep.htm)|Truth Seeker|Buscador de la verdad|oficial|
|[TPoP1mKpqUOpRQ5Y.htm](backgrounds/TPoP1mKpqUOpRQ5Y.htm)|Reputation Seeker|Buscador de reputación|oficial|
|[uC6D2nmDTATxXrV6.htm](backgrounds/uC6D2nmDTATxXrV6.htm)|Royalty|Regio|oficial|
|[UdOUj7i8XGTI72Zc.htm](backgrounds/UdOUj7i8XGTI72Zc.htm)|Servant|Sirviente|oficial|
|[UgityMZaujmYUpil.htm](backgrounds/UgityMZaujmYUpil.htm)|Out-Of-Towner|Foráneo|oficial|
|[vHeP960qjhfob4Je.htm](backgrounds/vHeP960qjhfob4Je.htm)|Scavenger|Carroñero|oficial|
|[vNWSzv36L1GBPPoc.htm](backgrounds/vNWSzv36L1GBPPoc.htm)|Hellknight Historian|Historiador de los caballeros infernales|oficial|
|[WrKVeUQ6KeRCm9uH.htm](backgrounds/WrKVeUQ6KeRCm9uH.htm)|Teacher|Profesor|oficial|
|[wudDO9OEsRjJsqhU.htm](backgrounds/wudDO9OEsRjJsqhU.htm)|Barber|Barbero|oficial|
|[Zmwyhsxe4i6rZN75.htm](backgrounds/Zmwyhsxe4i6rZN75.htm)|Sailor|Marino|oficial|
