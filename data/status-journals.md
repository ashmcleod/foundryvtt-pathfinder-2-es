# Estado de la traducción (journals)

 * **modificada**: 4


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[EEZvDB1Z7ezwaxIr.htm](journals/EEZvDB1Z7ezwaxIr.htm)|Domains|Dominios|modificada|
|[S55aqwWIzpQRFhcq.htm](journals/S55aqwWIzpQRFhcq.htm)|GM Screen|Pantalla GM|modificada|
|[vx5FGEG34AxI2dow.htm](journals/vx5FGEG34AxI2dow.htm)|Archetypes|Arquetipos|modificada|
|[xtrW5GEtPPuXR6k2.htm](journals/xtrW5GEtPPuXR6k2.htm)|Deep Backgrounds|Fondos profundos|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
