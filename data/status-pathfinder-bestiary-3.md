# Estado de la traducción (pathfinder-bestiary-3)

 * **modificada**: 365


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[01alXdlOUvAC6woS.htm](pathfinder-bestiary-3/01alXdlOUvAC6woS.htm)|Giant Vulture|Buitre gigante|modificada|
|[0A2XLkvOzDMOjC6Q.htm](pathfinder-bestiary-3/0A2XLkvOzDMOjC6Q.htm)|Clockwork Mage|Clockwork Mage|modificada|
|[0HTju4vf8ADAAh2g.htm](pathfinder-bestiary-3/0HTju4vf8ADAAh2g.htm)|Air Wisp|Air Wisp|modificada|
|[0JmtZzvzZU3HtYVp.htm](pathfinder-bestiary-3/0JmtZzvzZU3HtYVp.htm)|Squirrel Swarm|Enjambre de ardillas|modificada|
|[0laxaxLySatd0Uii.htm](pathfinder-bestiary-3/0laxaxLySatd0Uii.htm)|Adult Sovereign Dragon|Dragón Soberano Adulto|modificada|
|[0qqABck8p0lCe4xz.htm](pathfinder-bestiary-3/0qqABck8p0lCe4xz.htm)|Adult Sky Dragon|Dragón del Cielo Adulto|modificada|
|[0sQg5UM8dQY7fBhQ.htm](pathfinder-bestiary-3/0sQg5UM8dQY7fBhQ.htm)|Ganzi Martial Artist|Ganzi Artista Marcial|modificada|
|[0yisb2wbIvfNqciD.htm](pathfinder-bestiary-3/0yisb2wbIvfNqciD.htm)|Elysian Titan|Titán Elíseo|modificada|
|[1DzJjE7OnRDY5Pir.htm](pathfinder-bestiary-3/1DzJjE7OnRDY5Pir.htm)|Giant Opossum|Zarigüeya gigante|modificada|
|[2DrerJYEJon5U6Fx.htm](pathfinder-bestiary-3/2DrerJYEJon5U6Fx.htm)|Slithering Pit|Foso deslizante|modificada|
|[2L2iQ9X8tYPH2K9s.htm](pathfinder-bestiary-3/2L2iQ9X8tYPH2K9s.htm)|Tidehawk|Tidehawk|modificada|
|[2pQoqcUxyHsTLhjw.htm](pathfinder-bestiary-3/2pQoqcUxyHsTLhjw.htm)|Viper Swarm|Enjambre de víboras|modificada|
|[2vvPHlLhgDGr8fOF.htm](pathfinder-bestiary-3/2vvPHlLhgDGr8fOF.htm)|Shae|Shae|modificada|
|[324ZRtmDYulbw0CM.htm](pathfinder-bestiary-3/324ZRtmDYulbw0CM.htm)|Lifeleecher Brawler|Lifeleecher Brawler|modificada|
|[3OrdGXuPXSlrLlbf.htm](pathfinder-bestiary-3/3OrdGXuPXSlrLlbf.htm)|Wyrwood Sneak|Movimiento furtivo de Wyrwood|modificada|
|[3SYFPEaAl4g5G3GK.htm](pathfinder-bestiary-3/3SYFPEaAl4g5G3GK.htm)|Kitsune Trickster|Arlequín Kitsune|modificada|
|[3VTjHYQjp1aE27n4.htm](pathfinder-bestiary-3/3VTjHYQjp1aE27n4.htm)|Young Underworld Dragon|Joven Dragón del Inframundo|modificada|
|[4cfi0BksjHbFVY0A.htm](pathfinder-bestiary-3/4cfi0BksjHbFVY0A.htm)|Flumph|Flumph|modificada|
|[4glVe36VTR8HTjcE.htm](pathfinder-bestiary-3/4glVe36VTR8HTjcE.htm)|Girtablilu Sentry|Girtablilu Sentry|modificada|
|[4vwMHy39IQyb7I7p.htm](pathfinder-bestiary-3/4vwMHy39IQyb7I7p.htm)|Incutilis|Incutilis|modificada|
|[4WMThCqvHV1aaBwa.htm](pathfinder-bestiary-3/4WMThCqvHV1aaBwa.htm)|Animated Colossus|Coloso Animado|modificada|
|[5bBfMpcn0PuO1jTL.htm](pathfinder-bestiary-3/5bBfMpcn0PuO1jTL.htm)|Lava Worm Swarm|Enjambre de gusanos de lava|modificada|
|[5dZaPSltPYYIupeq.htm](pathfinder-bestiary-3/5dZaPSltPYYIupeq.htm)|Grioth Scout|Explorador Grioth|modificada|
|[5qtnZRnS3WF5oTUq.htm](pathfinder-bestiary-3/5qtnZRnS3WF5oTUq.htm)|Thanatotic Titan|Titán Thanatotic|modificada|
|[5WAy9PYWu1PuQKXg.htm](pathfinder-bestiary-3/5WAy9PYWu1PuQKXg.htm)|Clacking Skull Swarm|Clacking Skull Swarm|modificada|
|[60bknqrpHs2lL4pt.htm](pathfinder-bestiary-3/60bknqrpHs2lL4pt.htm)|Rat Snake Swarm|Enjambre de serpientes rata|modificada|
|[61atKNQVF73vWpqc.htm](pathfinder-bestiary-3/61atKNQVF73vWpqc.htm)|Divine Warden Of Nethys|Guardián divino de Nethys|modificada|
|[6A317pomsGPzW17M.htm](pathfinder-bestiary-3/6A317pomsGPzW17M.htm)|Munavri Spellblade|Munavri Spellblade|modificada|
|[6AvQH0XXccorLE6d.htm](pathfinder-bestiary-3/6AvQH0XXccorLE6d.htm)|Skinstitch|Skinstitch|modificada|
|[6bkqOecc1n0PulCu.htm](pathfinder-bestiary-3/6bkqOecc1n0PulCu.htm)|Abrikandilu|Abrikandilu|modificada|
|[6OxiStysMq65xKgS.htm](pathfinder-bestiary-3/6OxiStysMq65xKgS.htm)|Kongamato|Kongamato|modificada|
|[6rtLd2rN1qd6eCqK.htm](pathfinder-bestiary-3/6rtLd2rN1qd6eCqK.htm)|Hieracosphinx|Hieracosphinx|modificada|
|[6yc1exIcngUEvBQH.htm](pathfinder-bestiary-3/6yc1exIcngUEvBQH.htm)|Adhukait|Adhukait|modificada|
|[74Sw9n7c4sMaK9Rx.htm](pathfinder-bestiary-3/74Sw9n7c4sMaK9Rx.htm)|Stheno Harpist|Stheno Harpist|modificada|
|[79pfivF3swvfsJE4.htm](pathfinder-bestiary-3/79pfivF3swvfsJE4.htm)|Caligni Vanguard|Vanguardia Caligni (p.-s)|modificada|
|[7bTj2DC91yEdJiLq.htm](pathfinder-bestiary-3/7bTj2DC91yEdJiLq.htm)|Vilderavn|Vilderavn|modificada|
|[7lSwznbhNb7THfZo.htm](pathfinder-bestiary-3/7lSwznbhNb7THfZo.htm)|Terror Shrike|Alcaudón del Terror|modificada|
|[7M7mwhETGEJjYoiY.htm](pathfinder-bestiary-3/7M7mwhETGEJjYoiY.htm)|Kimenhul|Kimenhul|modificada|
|[8Ds7BAEjgSSB32wS.htm](pathfinder-bestiary-3/8Ds7BAEjgSSB32wS.htm)|Deimavigga|Deimavigga|modificada|
|[8HdbN5NwEolamidg.htm](pathfinder-bestiary-3/8HdbN5NwEolamidg.htm)|Vulpinal|Vulpinal|modificada|
|[8LBJ0jKca3vLaUhx.htm](pathfinder-bestiary-3/8LBJ0jKca3vLaUhx.htm)|Festrog|Festrog|modificada|
|[8lQf8PNcJvxwmqLd.htm](pathfinder-bestiary-3/8lQf8PNcJvxwmqLd.htm)|Kirin|Kirin|modificada|
|[8M91u7Q3javRQVEY.htm](pathfinder-bestiary-3/8M91u7Q3javRQVEY.htm)|Raktavarna|Raktavarna|modificada|
|[8S088wbZjUhx6IB7.htm](pathfinder-bestiary-3/8S088wbZjUhx6IB7.htm)|Khravgodon|Khravgodon|modificada|
|[8zdRS4uemz3LCEzi.htm](pathfinder-bestiary-3/8zdRS4uemz3LCEzi.htm)|Ancient Sea Dragon (Spellcaster)|Ancient Sea Dragon (Hechicero)|modificada|
|[92nVPdtlJR5uHzIl.htm](pathfinder-bestiary-3/92nVPdtlJR5uHzIl.htm)|Sabosan|Sabosan|modificada|
|[9HkUdRKoprwo27VN.htm](pathfinder-bestiary-3/9HkUdRKoprwo27VN.htm)|Clockwork Soldier|Soldado Mecánico|modificada|
|[9KZRWATLOGP7QGyM.htm](pathfinder-bestiary-3/9KZRWATLOGP7QGyM.htm)|Fire Wisp|Fuego Wisp|modificada|
|[9PSCqGy7e2GDQpfU.htm](pathfinder-bestiary-3/9PSCqGy7e2GDQpfU.htm)|Love Siktempora|Ama a Siktempora|modificada|
|[9RNisBYwGOCQan1S.htm](pathfinder-bestiary-3/9RNisBYwGOCQan1S.htm)|Haniver|Haniver|modificada|
|[9rugdliEg2udjROC.htm](pathfinder-bestiary-3/9rugdliEg2udjROC.htm)|Pufferfish|Pez globo|modificada|
|[9SW7NWlTisAjNwAu.htm](pathfinder-bestiary-3/9SW7NWlTisAjNwAu.htm)|Nyktera|Nyktera|modificada|
|[9uP6Dfv53H4Fa32c.htm](pathfinder-bestiary-3/9uP6Dfv53H4Fa32c.htm)|Consonite Choir|Coro Consonite|modificada|
|[A7TFAr1rBtKJyprn.htm](pathfinder-bestiary-3/A7TFAr1rBtKJyprn.htm)|Sulfur Zombie|Zombie de azufre|modificada|
|[aAU3gHWd1a9DyQPH.htm](pathfinder-bestiary-3/aAU3gHWd1a9DyQPH.htm)|Nosferatu Overlord|Nosferatu Overlord|modificada|
|[abSsuJM94EvvyQYw.htm](pathfinder-bestiary-3/abSsuJM94EvvyQYw.htm)|Giant Flying Squirrel|Ardilla voladora gigante|modificada|
|[ACTvXKXcw1RZQxrP.htm](pathfinder-bestiary-3/ACTvXKXcw1RZQxrP.htm)|Adult Forest Dragon|Dragón del Bosque Adulto|modificada|
|[aeLTz7wO9ajPaQ0V.htm](pathfinder-bestiary-3/aeLTz7wO9ajPaQ0V.htm)|Hermit Crab Swarm|Enjambre de cangrejos ermitas|modificada|
|[aepfNXXy2juozgzB.htm](pathfinder-bestiary-3/aepfNXXy2juozgzB.htm)|Weasel|Comadreja|modificada|
|[Al5OHM0hbWcqIplK.htm](pathfinder-bestiary-3/Al5OHM0hbWcqIplK.htm)|Wizard Sponge (Fiendish Temple)|Esponja hechicera (Templo diabólico)|modificada|
|[AlLBsYO3ax9OQzAK.htm](pathfinder-bestiary-3/AlLBsYO3ax9OQzAK.htm)|Tupilaq|Tupilaq|modificada|
|[AmkhY7NErzNgbsN0.htm](pathfinder-bestiary-3/AmkhY7NErzNgbsN0.htm)|Narwhal|Narval|modificada|
|[an7tww93Y4pQ8HP6.htm](pathfinder-bestiary-3/an7tww93Y4pQ8HP6.htm)|Baykok|Baykok|modificada|
|[aQusm2Uh1tw00IVb.htm](pathfinder-bestiary-3/aQusm2Uh1tw00IVb.htm)|Bore Worm Swarm|Enjambre de gusanos perforadores|modificada|
|[aqvMwn6885CJEmCO.htm](pathfinder-bestiary-3/aqvMwn6885CJEmCO.htm)|Garuda|Garuda|modificada|
|[aX0zhmJGzpnwCwMr.htm](pathfinder-bestiary-3/aX0zhmJGzpnwCwMr.htm)|Feral Skull Swarm|Enjambre de calaveras ferales|modificada|
|[AxfH7V8A38VVugYo.htm](pathfinder-bestiary-3/AxfH7V8A38VVugYo.htm)|Lampad|Lampad|modificada|
|[AybyC0n2CWNbXAbK.htm](pathfinder-bestiary-3/AybyC0n2CWNbXAbK.htm)|Popobawa|Popobawa|modificada|
|[aZBp1m5C9nXRgxHA.htm](pathfinder-bestiary-3/aZBp1m5C9nXRgxHA.htm)|Trilobite|Trilobite|modificada|
|[BEzfxlrA0CNfh4Fr.htm](pathfinder-bestiary-3/BEzfxlrA0CNfh4Fr.htm)|Nosferatu Malefactor|Nosferatu Malefactor|modificada|
|[BMHkCVNiEbnmUVft.htm](pathfinder-bestiary-3/BMHkCVNiEbnmUVft.htm)|Toshigami|Toshigami|modificada|
|[BPmotFI9EoIqSatr.htm](pathfinder-bestiary-3/BPmotFI9EoIqSatr.htm)|Japalisura|Japalisura|modificada|
|[BrGvmcM6jl3xUs4d.htm](pathfinder-bestiary-3/BrGvmcM6jl3xUs4d.htm)|Cobbleswarm|Cobbleswarm|modificada|
|[BZKNSyp1ATtn3JXj.htm](pathfinder-bestiary-3/BZKNSyp1ATtn3JXj.htm)|Adachros|Adachros|modificada|
|[c7kP2W6zaZA9oxAd.htm](pathfinder-bestiary-3/c7kP2W6zaZA9oxAd.htm)|Giant Pangolin|Pangolín gigante|modificada|
|[cbduxhlI7JbONLXF.htm](pathfinder-bestiary-3/cbduxhlI7JbONLXF.htm)|Shambler Troop|Tropa broza movediza|modificada|
|[CFzeAzMfGHkzCF7h.htm](pathfinder-bestiary-3/CFzeAzMfGHkzCF7h.htm)|Skunk|Skunk|modificada|
|[ChzqT42N5waJZ9VS.htm](pathfinder-bestiary-3/ChzqT42N5waJZ9VS.htm)|Spiny Eurypterid|Eurypterid espinoso|modificada|
|[CjAaXJDY4xpKqQEz.htm](pathfinder-bestiary-3/CjAaXJDY4xpKqQEz.htm)|Megalictis|Megalictis|modificada|
|[cKy95PZJt6lGCsJk.htm](pathfinder-bestiary-3/cKy95PZJt6lGCsJk.htm)|Kushtaka|Kushtaka|modificada|
|[cKZtOsBlN3Qu8Kyq.htm](pathfinder-bestiary-3/cKZtOsBlN3Qu8Kyq.htm)|Clockwork Spy|Espía Mecánico|modificada|
|[cMBXcfS0DuZ7O2vm.htm](pathfinder-bestiary-3/cMBXcfS0DuZ7O2vm.htm)|Pukwudgie|Pukwudgie|modificada|
|[cmzlnTgWcJjnISAK.htm](pathfinder-bestiary-3/cmzlnTgWcJjnISAK.htm)|Kokogiak|Kokogiak|modificada|
|[CSPuBqtPITQt43Md.htm](pathfinder-bestiary-3/CSPuBqtPITQt43Md.htm)|Gathlain Wanderer|Gathlain Wanderer|modificada|
|[cWlntwaa4HPFEf3u.htm](pathfinder-bestiary-3/cWlntwaa4HPFEf3u.htm)|Sturzstromer|Sturzstromer|modificada|
|[CXCdPqMRX58sBQ9G.htm](pathfinder-bestiary-3/CXCdPqMRX58sBQ9G.htm)|Hellknight Cavalry Brigade|Brigada de Caballería Hellknight|modificada|
|[cXz5nWwlbRQ1g90y.htm](pathfinder-bestiary-3/cXz5nWwlbRQ1g90y.htm)|Young Sky Dragon|Joven Dragón del Cielo|modificada|
|[CYzFHz8ZbU270z9N.htm](pathfinder-bestiary-3/CYzFHz8ZbU270z9N.htm)|Mokele-Mbembe|Mokele-Mbembe|modificada|
|[D0IY6UW5I0R4GprS.htm](pathfinder-bestiary-3/D0IY6UW5I0R4GprS.htm)|Ancient Sky Dragon (Spellcaster)|Ancient Sky Dragon (Hechicero)|modificada|
|[d1hv3x7syXaltxg4.htm](pathfinder-bestiary-3/d1hv3x7syXaltxg4.htm)|Young Sea Dragon (Spellcaster)|Joven Dragón Marino (Hechicero)|modificada|
|[d5UfBtz09fOXKSmr.htm](pathfinder-bestiary-3/d5UfBtz09fOXKSmr.htm)|Nemhaith|Nemhaith|modificada|
|[dCJDmNm6WSKf0AY4.htm](pathfinder-bestiary-3/dCJDmNm6WSKf0AY4.htm)|Piranha Swarm|Enjambre Piraña|modificada|
|[DCzr8qOfSg2K7e3z.htm](pathfinder-bestiary-3/DCzr8qOfSg2K7e3z.htm)|Ancient Sea Dragon|Dragón marino antiguo|modificada|
|[DIJQ1UvfDDnDP545.htm](pathfinder-bestiary-3/DIJQ1UvfDDnDP545.htm)|Cave Giant|Gigante de las cavernas|modificada|
|[dkDbXG0boTkddHSG.htm](pathfinder-bestiary-3/dkDbXG0boTkddHSG.htm)|Melixie|Melixie|modificada|
|[dKFzkdgTntTm8ydA.htm](pathfinder-bestiary-3/dKFzkdgTntTm8ydA.htm)|Wizard Sponge (Crypt)|Esponja Maga (Cripta)|modificada|
|[DmQM0QTSPJ7YtpMg.htm](pathfinder-bestiary-3/DmQM0QTSPJ7YtpMg.htm)|Girtablilu Seer|Girtablilu Vidente|modificada|
|[dniiLeUhXaq5CElX.htm](pathfinder-bestiary-3/dniiLeUhXaq5CElX.htm)|Arboreal Reaper|Parca arborícola (pl. -es)|modificada|
|[dtkq45qf18bENXBd.htm](pathfinder-bestiary-3/dtkq45qf18bENXBd.htm)|Kishi|Kishi|modificada|
|[dVJkFPCqJcPjImdG.htm](pathfinder-bestiary-3/dVJkFPCqJcPjImdG.htm)|Mix Couatl|Mezcla Couatl|modificada|
|[e6r8AGxfp8PDXaZk.htm](pathfinder-bestiary-3/e6r8AGxfp8PDXaZk.htm)|Levaloch|Levaloch|modificada|
|[eGNpuEg60STItyGz.htm](pathfinder-bestiary-3/eGNpuEg60STItyGz.htm)|Cactus Leshy|Cactus Leshy (s), leshys (pl.)|modificada|
|[EGyCQDseM9FaCl78.htm](pathfinder-bestiary-3/EGyCQDseM9FaCl78.htm)|Chouchin-Obake|Chouchin-Obake|modificada|
|[EMT6L7RarzkSiEOq.htm](pathfinder-bestiary-3/EMT6L7RarzkSiEOq.htm)|Living Rune (Arcane)|Runa Viviente (Arcana)|modificada|
|[eO3hNubEw16BC8UJ.htm](pathfinder-bestiary-3/eO3hNubEw16BC8UJ.htm)|Adult Sky Dragon (Spellcaster)|Dragón del Cielo Adulto (Hechicero)|modificada|
|[EP8xhzy46zPWvhQL.htm](pathfinder-bestiary-3/EP8xhzy46zPWvhQL.htm)|Squirming Swill|Squirming Swill|modificada|
|[epTO8fPDjyy2WhzD.htm](pathfinder-bestiary-3/epTO8fPDjyy2WhzD.htm)|Mithral Golem|Mithral Golem|modificada|
|[esw400AugH9XWq5p.htm](pathfinder-bestiary-3/esw400AugH9XWq5p.htm)|Hekatonkheires Titan|Hekatonkheires Titan|modificada|
|[EvLBhZSMrzVDk4mM.htm](pathfinder-bestiary-3/EvLBhZSMrzVDk4mM.htm)|Calikang|Calikang|modificada|
|[ew52XP0hjUACnidH.htm](pathfinder-bestiary-3/ew52XP0hjUACnidH.htm)|Blood Hag|La saga sanguínea del maléfico|modificada|
|[eYKWJCYNqqp1rp2i.htm](pathfinder-bestiary-3/eYKWJCYNqqp1rp2i.htm)|Nightgaunt|Nightgaunt|modificada|
|[F4aCBm1lfPlRQzZ1.htm](pathfinder-bestiary-3/F4aCBm1lfPlRQzZ1.htm)|Ghoran Manipulator|Ghoran Manipulador|modificada|
|[fcFQ2GDUZ9YAhiDC.htm](pathfinder-bestiary-3/fcFQ2GDUZ9YAhiDC.htm)|Animated Trebuchet|Trebuchet animado|modificada|
|[fHczwj1B8ULKRSJk.htm](pathfinder-bestiary-3/fHczwj1B8ULKRSJk.htm)|Triumph Siktempora|Triunfo Siktempora|modificada|
|[FHZPjbRmJCqnhdal.htm](pathfinder-bestiary-3/FHZPjbRmJCqnhdal.htm)|Mage-Eater Worm Swarm|Enjambre de gusanos come-magos|modificada|
|[Fj3CXqR0Y6kgvcBE.htm](pathfinder-bestiary-3/Fj3CXqR0Y6kgvcBE.htm)|Kodama|Kodama|modificada|
|[FjiovYhLSHDX0ODl.htm](pathfinder-bestiary-3/FjiovYhLSHDX0ODl.htm)|Tattoo Guardian|Tattoo Guardian|modificada|
|[FjZ737XIvSOSaYcA.htm](pathfinder-bestiary-3/FjZ737XIvSOSaYcA.htm)|Tyrannosaurus Skeleton|Esqueleto de tiranosaurio|modificada|
|[fLyIWlTCW3cUPjvd.htm](pathfinder-bestiary-3/fLyIWlTCW3cUPjvd.htm)|Animated Furnace|Horno animado|modificada|
|[FOG8qK1bWVxp7vmE.htm](pathfinder-bestiary-3/FOG8qK1bWVxp7vmE.htm)|Empress Bore Worm|Empress Bore Worm|modificada|
|[fuCPoL2OVcQf5uT9.htm](pathfinder-bestiary-3/fuCPoL2OVcQf5uT9.htm)|Ovinnik|Ovinnik|modificada|
|[fViWNHKjjL7fYbjW.htm](pathfinder-bestiary-3/fViWNHKjjL7fYbjW.htm)|Common Eurypterid|Común Eurypterid|modificada|
|[FvywQFbFzwLV8mvW.htm](pathfinder-bestiary-3/FvywQFbFzwLV8mvW.htm)|Flaming Skull|Cráneo Flamígera|modificada|
|[FXwgsLTRneGzclsw.htm](pathfinder-bestiary-3/FXwgsLTRneGzclsw.htm)|Ioton|Ioton|modificada|
|[GD8yAL8R8oft23Ml.htm](pathfinder-bestiary-3/GD8yAL8R8oft23Ml.htm)|Fading Fox|Fading Fox|modificada|
|[Gf3g6wQquSjJhFMC.htm](pathfinder-bestiary-3/Gf3g6wQquSjJhFMC.htm)|Samsaran Anchorite|Samsaran Anchorite|modificada|
|[GGuT6YsRH4aXhpl3.htm](pathfinder-bestiary-3/GGuT6YsRH4aXhpl3.htm)|Zombie Dragon|Dragón Zombie|modificada|
|[GHv9BN8JQEPJVp5n.htm](pathfinder-bestiary-3/GHv9BN8JQEPJVp5n.htm)|Guecubu|Guecubu|modificada|
|[GPP2YJd2CkXSlLok.htm](pathfinder-bestiary-3/GPP2YJd2CkXSlLok.htm)|Duende|Duende|modificada|
|[gQkqsdfkmws4oADW.htm](pathfinder-bestiary-3/gQkqsdfkmws4oADW.htm)|Werecrocodile|Werecrocodile|modificada|
|[H796hayJm3J7MYJg.htm](pathfinder-bestiary-3/H796hayJm3J7MYJg.htm)|Amphisbaena|Amphisbaena|modificada|
|[H7PDLOIbDG9zt4H1.htm](pathfinder-bestiary-3/H7PDLOIbDG9zt4H1.htm)|Yzobu|Yzobu|modificada|
|[H8PaAZanEOT85KjD.htm](pathfinder-bestiary-3/H8PaAZanEOT85KjD.htm)|Vine Leshy|Vine Leshy (s), leshys (pl.)|modificada|
|[HEgGta4uVmwdkw3E.htm](pathfinder-bestiary-3/HEgGta4uVmwdkw3E.htm)|Nikaramsa|Nikaramsa|modificada|
|[hh7OuFB5BQIrfeRT.htm](pathfinder-bestiary-3/hh7OuFB5BQIrfeRT.htm)|Kasa-Obake|Kasa-Obake|modificada|
|[HhuHFmaq69ekSgEl.htm](pathfinder-bestiary-3/HhuHFmaq69ekSgEl.htm)|Shantak|Shantak|modificada|
|[HiazGJoPkJ3gQVAO.htm](pathfinder-bestiary-3/HiazGJoPkJ3gQVAO.htm)|Grioth Cultist|Grioth Cultist|modificada|
|[hiDSC6gqneQTB106.htm](pathfinder-bestiary-3/hiDSC6gqneQTB106.htm)|Fuath|Fuath|modificada|
|[hNW3X8MbQQ9pUMiR.htm](pathfinder-bestiary-3/hNW3X8MbQQ9pUMiR.htm)|Buso Farmer|Buso Farmer|modificada|
|[HoBmSopFM5TjlmBj.htm](pathfinder-bestiary-3/HoBmSopFM5TjlmBj.htm)|Young Sovereign Dragon (Spellcaster)|Joven Dragón Soberano (Hechicero)|modificada|
|[HObVT8aJnsx5nnqu.htm](pathfinder-bestiary-3/HObVT8aJnsx5nnqu.htm)|Tikbalang|Tikbalang|modificada|
|[hOgYpdscvGo4MHHo.htm](pathfinder-bestiary-3/hOgYpdscvGo4MHHo.htm)|Krampus|Krampus|modificada|
|[HPVVewX9vqKH94xf.htm](pathfinder-bestiary-3/HPVVewX9vqKH94xf.htm)|Tooth Fairy|Hada de los Dientes|modificada|
|[i3Ui3hHIBZnHl0Le.htm](pathfinder-bestiary-3/i3Ui3hHIBZnHl0Le.htm)|Globster|Globster|modificada|
|[iD32uhsjUGLvC2q6.htm](pathfinder-bestiary-3/iD32uhsjUGLvC2q6.htm)|Bone Ship|Barco de hueso|modificada|
|[ie5MyxqTqGlxzgsH.htm](pathfinder-bestiary-3/ie5MyxqTqGlxzgsH.htm)|Ancient Forest Dragon|Dragón del Bosque Antiguo|modificada|
|[IijUBFE1vhvgowhD.htm](pathfinder-bestiary-3/IijUBFE1vhvgowhD.htm)|Giant Porcupine|Puercoespín Gigante|modificada|
|[iiXjQ1SchGiotpVp.htm](pathfinder-bestiary-3/iiXjQ1SchGiotpVp.htm)|Skeleton Infantry|Infantería esqueleto|modificada|
|[ilGMPBvjT9ovIiXB.htm](pathfinder-bestiary-3/ilGMPBvjT9ovIiXB.htm)|Gurgist Mauler|Gurgist Vapulear|modificada|
|[iLoVkzve6Nu3gErr.htm](pathfinder-bestiary-3/iLoVkzve6Nu3gErr.htm)|Herexen|Herexen|modificada|
|[IMrnOMr3GtUFKyuV.htm](pathfinder-bestiary-3/IMrnOMr3GtUFKyuV.htm)|Ice Worm Swarm|Enjambre de Gusanos de Hielo|modificada|
|[iNgKGHzMOAHjWQeI.htm](pathfinder-bestiary-3/iNgKGHzMOAHjWQeI.htm)|Android Infiltrator|Android Infiltrator|modificada|
|[Ir3N6RHfg6vXYkmN.htm](pathfinder-bestiary-3/Ir3N6RHfg6vXYkmN.htm)|Rosethorn Ram|Carnero Rosethorn|modificada|
|[IsE3PvvjFfxzuQtE.htm](pathfinder-bestiary-3/IsE3PvvjFfxzuQtE.htm)|Sorcerous Sea Skull Swarm|Sorcerous Sea Skull Swarm|modificada|
|[istUwJdW3Mlln2hb.htm](pathfinder-bestiary-3/istUwJdW3Mlln2hb.htm)|Giant Hermit Crab|Cangrejo ermita gigante|modificada|
|[IyxWVWAi9BjKPxop.htm](pathfinder-bestiary-3/IyxWVWAi9BjKPxop.htm)|Living Graffiti (Chalk)|Graffiti viviente (tiza)|modificada|
|[JD5sD7vwwlU2DwJI.htm](pathfinder-bestiary-3/JD5sD7vwwlU2DwJI.htm)|Angazhani|Angazhani|modificada|
|[jdqCEhsHDs4ABh2X.htm](pathfinder-bestiary-3/jdqCEhsHDs4ABh2X.htm)|Jorogumo|Jorogumo|modificada|
|[JGpz9B0QkkAcHT4e.htm](pathfinder-bestiary-3/JGpz9B0QkkAcHT4e.htm)|Empress Mage-Eater Worm|Emperatriz Gusano Come-Magos|modificada|
|[JHrINFX7a7fYhP4w.htm](pathfinder-bestiary-3/JHrINFX7a7fYhP4w.htm)|Gliminal|Gliminal|modificada|
|[JKF2cMQEWkA5avCO.htm](pathfinder-bestiary-3/JKF2cMQEWkA5avCO.htm)|Shikigami|Shikigami|modificada|
|[JkJRKxuIGJ3DOD9L.htm](pathfinder-bestiary-3/JkJRKxuIGJ3DOD9L.htm)|Owb|Owb|modificada|
|[JlFBu8zobNq3daVF.htm](pathfinder-bestiary-3/JlFBu8zobNq3daVF.htm)|Necral Worm Swarm|Enjambre de gusanos necral|modificada|
|[Jn35hqHlyzEyv0T7.htm](pathfinder-bestiary-3/Jn35hqHlyzEyv0T7.htm)|Ancient Forest Dragon (Spellcaster)|Dragón del Bosque Antiguo (Hechicero)|modificada|
|[JvT56DMG6vKiii0u.htm](pathfinder-bestiary-3/JvT56DMG6vKiii0u.htm)|Adult Forest Dragon (Spellcaster)|Dragón del bosque adulto (Hechicero)|modificada|
|[JZMK7j7WGWlB3Jhw.htm](pathfinder-bestiary-3/JZMK7j7WGWlB3Jhw.htm)|Young Sea Dragon|Joven dragón marino|modificada|
|[JZuQJnATcqljgGWn.htm](pathfinder-bestiary-3/JZuQJnATcqljgGWn.htm)|Adult Sea Dragon (Spellcaster)|Dragón de mar adulto (Hechicero)|modificada|
|[kBIl96jzJjOd0LkZ.htm](pathfinder-bestiary-3/kBIl96jzJjOd0LkZ.htm)|Namorrodor|Namorrodor|modificada|
|[kCaEwjZyHXy7cJre.htm](pathfinder-bestiary-3/kCaEwjZyHXy7cJre.htm)|Trilobite Swarm|Enjambre Trilobite|modificada|
|[KG1Zu3EK0wrNdsQC.htm](pathfinder-bestiary-3/KG1Zu3EK0wrNdsQC.htm)|Ittan-Momen|Ittan-Momen|modificada|
|[KGAuzJaKfPAdn0It.htm](pathfinder-bestiary-3/KGAuzJaKfPAdn0It.htm)|Aghash|Aghash|modificada|
|[KGiEFgiqiexNBfS3.htm](pathfinder-bestiary-3/KGiEFgiqiexNBfS3.htm)|Wihsaak|Wihsaak|modificada|
|[krbmyD1SuPQb4QfF.htm](pathfinder-bestiary-3/krbmyD1SuPQb4QfF.htm)|Blood Painter|Blood Painter|modificada|
|[kSk6QjH4wDGPPFrY.htm](pathfinder-bestiary-3/kSk6QjH4wDGPPFrY.htm)|Rokurokubi|Rokurokubi|modificada|
|[KSKettq5j3A7UsIh.htm](pathfinder-bestiary-3/KSKettq5j3A7UsIh.htm)|Phantom Beast|Bestia Fantasma|modificada|
|[KTzSpyphiJ78EnBd.htm](pathfinder-bestiary-3/KTzSpyphiJ78EnBd.htm)|Winter Hag|Hechicera invernal|modificada|
|[kU0ZXzbHqHUIND6m.htm](pathfinder-bestiary-3/kU0ZXzbHqHUIND6m.htm)|Fortune Eater|Fortune Eater|modificada|
|[kuh9DOsFXybRZRlj.htm](pathfinder-bestiary-3/kuh9DOsFXybRZRlj.htm)|Tolokand|Tolokand|modificada|
|[kXc11R18rF28AgIf.htm](pathfinder-bestiary-3/kXc11R18rF28AgIf.htm)|Green Man|Hombre Verde|modificada|
|[L5cNazEKC5gASp41.htm](pathfinder-bestiary-3/L5cNazEKC5gASp41.htm)|Mothman|Mothman|modificada|
|[lat5aZFMlWUR2Wbs.htm](pathfinder-bestiary-3/lat5aZFMlWUR2Wbs.htm)|Lampad Queen|Lampad Queen|modificada|
|[ldzHiLhLAHhwpVJe.htm](pathfinder-bestiary-3/ldzHiLhLAHhwpVJe.htm)|House Drake|Casa Drake|modificada|
|[lhNceV0kAJozCUCI.htm](pathfinder-bestiary-3/lhNceV0kAJozCUCI.htm)|Feral Sea Skull Swarm|Feral Sea Skull Swarm|modificada|
|[LjpOnADaKe9ormfL.htm](pathfinder-bestiary-3/LjpOnADaKe9ormfL.htm)|Kuchisake-Onna|Kuchisake-Onna|modificada|
|[lqDwO2xkBNNEZ57B.htm](pathfinder-bestiary-3/lqDwO2xkBNNEZ57B.htm)|Elder Wyrmwraith|Elder Wyrmwraith|modificada|
|[Lqm3acjjbLKuRUCf.htm](pathfinder-bestiary-3/Lqm3acjjbLKuRUCf.htm)|Peri|Peri|modificada|
|[lrSuRCrRjP3xBfRy.htm](pathfinder-bestiary-3/lrSuRCrRjP3xBfRy.htm)|Megatherium|Megatherium|modificada|
|[Ls2qYZDASu3VgXxo.htm](pathfinder-bestiary-3/Ls2qYZDASu3VgXxo.htm)|Ancient Sky Dragon|Ancient Sky Dragon|modificada|
|[LTSZ3LD1L7ZthoD2.htm](pathfinder-bestiary-3/LTSZ3LD1L7ZthoD2.htm)|Misery Siktempora|Misery Siktempora|modificada|
|[lYOUeRxFhbTdOOGC.htm](pathfinder-bestiary-3/lYOUeRxFhbTdOOGC.htm)|Clacking Sea Skull Swarm|Enjambre de calaveras marinas|modificada|
|[m0w5VfUTRvRcGfba.htm](pathfinder-bestiary-3/m0w5VfUTRvRcGfba.htm)|Living Graffiti (Ink)|Graffiti viviente (tinta)|modificada|
|[mbu0D45HPRNAwgEU.htm](pathfinder-bestiary-3/mbu0D45HPRNAwgEU.htm)|Munagola|Munagola|modificada|
|[MD6eXpxoSPO02fZY.htm](pathfinder-bestiary-3/MD6eXpxoSPO02fZY.htm)|Xiuh Couatl|Xiuh Couatl|modificada|
|[mgQSYE94vb2ICVjL.htm](pathfinder-bestiary-3/mgQSYE94vb2ICVjL.htm)|Locathah Hunter|Cazador Locathah|modificada|
|[MJhb44wEqSMWtrfe.htm](pathfinder-bestiary-3/MJhb44wEqSMWtrfe.htm)|Angheuvore Flesh-Gnawer|Angheuvore Flesh-Gnawer|modificada|
|[mK7FwVR1yyqeMlA4.htm](pathfinder-bestiary-3/mK7FwVR1yyqeMlA4.htm)|Seething Spirit|Espíritu hirviente|modificada|
|[MolnKBns2ePDFbAB.htm](pathfinder-bestiary-3/MolnKBns2ePDFbAB.htm)|Hellbound Attorney|Hellbound Attorney|modificada|
|[mRsiT9EtpbOQ7AeX.htm](pathfinder-bestiary-3/mRsiT9EtpbOQ7AeX.htm)|Draconal (Red)|Draconal (Rojo)|modificada|
|[MtCiCanKa8EgZOm9.htm](pathfinder-bestiary-3/MtCiCanKa8EgZOm9.htm)|Three-Toed Sloth|Perezoso de tres dedos|modificada|
|[mupW1Mgec250lQiZ.htm](pathfinder-bestiary-3/mupW1Mgec250lQiZ.htm)|Umasi|Umasi|modificada|
|[n1GmLT7b5Q579Tcf.htm](pathfinder-bestiary-3/n1GmLT7b5Q579Tcf.htm)|Trailgaunt|Trailgaunt|modificada|
|[N6nBHGfmlaeMiBMP.htm](pathfinder-bestiary-3/N6nBHGfmlaeMiBMP.htm)|Hellwasp Swarm|Enjambre de Hellwasp|modificada|
|[n9xSjQA1YSlYc9p3.htm](pathfinder-bestiary-3/n9xSjQA1YSlYc9p3.htm)|Millindemalion|Millindemalion|modificada|
|[NhHGDx6ChbrhAmbO.htm](pathfinder-bestiary-3/NhHGDx6ChbrhAmbO.htm)|Domovoi|Domovoi|modificada|
|[NikAalo85JWVE6d2.htm](pathfinder-bestiary-3/NikAalo85JWVE6d2.htm)|Nagaji Soldier|Nagaji Soldado|modificada|
|[nIXWanjtyklfwH7u.htm](pathfinder-bestiary-3/nIXWanjtyklfwH7u.htm)|Tiddalik|Tiddalik|modificada|
|[NldgVUIHB3asozHm.htm](pathfinder-bestiary-3/NldgVUIHB3asozHm.htm)|Wyrmwraith|Wyrmwraith|modificada|
|[nnI7oj1BcetLUTYo.htm](pathfinder-bestiary-3/nnI7oj1BcetLUTYo.htm)|Etioling Blightmage|Etioling Blightmage|modificada|
|[NoAyIhPpqJ1WE1pF.htm](pathfinder-bestiary-3/NoAyIhPpqJ1WE1pF.htm)|Kovintus Geomancer|Kovintus Geomancer|modificada|
|[Nq0MR5YsuglgPi0m.htm](pathfinder-bestiary-3/Nq0MR5YsuglgPi0m.htm)|Ostovite|Ostovita|modificada|
|[NQzkW5D28zIGESBt.htm](pathfinder-bestiary-3/NQzkW5D28zIGESBt.htm)|Sumbreiva|Sumbreiva|modificada|
|[nr4E59Xu10nWYsyO.htm](pathfinder-bestiary-3/nr4E59Xu10nWYsyO.htm)|Giant Skunk|Zorrillo Gigante|modificada|
|[nr8EXKLSejspbb3d.htm](pathfinder-bestiary-3/nr8EXKLSejspbb3d.htm)|Red Fox|Zorro Rojo|modificada|
|[NSD8H0xzbSWOgt7x.htm](pathfinder-bestiary-3/NSD8H0xzbSWOgt7x.htm)|Stone Lion Cub|Cachorro de león de piedra|modificada|
|[nSpM8CroNk3J78SR.htm](pathfinder-bestiary-3/nSpM8CroNk3J78SR.htm)|Draconal (Yellow)|Draconal (Amarillo)|modificada|
|[NUWL7LHDqmP0c7OB.htm](pathfinder-bestiary-3/NUWL7LHDqmP0c7OB.htm)|Tooth Fairy Swarm|Enjambre del Hada de los Dientes|modificada|
|[nX3LIuvHqiKNrth9.htm](pathfinder-bestiary-3/nX3LIuvHqiKNrth9.htm)|Ancient Underworld Dragon|Dragón antiguo del inframundo|modificada|
|[nzF46X8zcUG0CvpV.htm](pathfinder-bestiary-3/nzF46X8zcUG0CvpV.htm)|Pakalchi|Pakalchi|modificada|
|[o740a5FFLG834FkV.htm](pathfinder-bestiary-3/o740a5FFLG834FkV.htm)|Desert Giant|Gigante del desierto|modificada|
|[O8iynsWnjObQ8TJl.htm](pathfinder-bestiary-3/O8iynsWnjObQ8TJl.htm)|Soul Skelm|Soul Skelm|modificada|
|[O9TQjp3scKfW8SZK.htm](pathfinder-bestiary-3/O9TQjp3scKfW8SZK.htm)|Shulsaga|Shulsaga|modificada|
|[oE52gEbyrF4Dl3Go.htm](pathfinder-bestiary-3/oE52gEbyrF4Dl3Go.htm)|Yithian|Yithian|modificada|
|[oIM21UCyiabwwHfo.htm](pathfinder-bestiary-3/oIM21UCyiabwwHfo.htm)|Elder Sphinx|Esfinge Anciana|modificada|
|[OiMWiQQuBYt6Yc73.htm](pathfinder-bestiary-3/OiMWiQQuBYt6Yc73.htm)|Adult Underworld Dragon (Spellcaster)|Dragón adulto del inframundo (Hechicero)|modificada|
|[ooyJuLQ3AivRwLpa.htm](pathfinder-bestiary-3/ooyJuLQ3AivRwLpa.htm)|Dretch|Dretch|modificada|
|[OPeTxIUwkkAjC6T5.htm](pathfinder-bestiary-3/OPeTxIUwkkAjC6T5.htm)|Hyakume|Hyakume|modificada|
|[OPRuZ1cEuKkJGPMV.htm](pathfinder-bestiary-3/OPRuZ1cEuKkJGPMV.htm)|Earth Wisp|Wisp Tierra|modificada|
|[oR8cm0Aj5FIDF67w.htm](pathfinder-bestiary-3/oR8cm0Aj5FIDF67w.htm)|Zetogeki|Zetogeki|modificada|
|[OrO28a9h8kakTTj7.htm](pathfinder-bestiary-3/OrO28a9h8kakTTj7.htm)|Young Sovereign Dragon|Joven Dragón Soberano|modificada|
|[OsWFo87OQ4G67zMS.htm](pathfinder-bestiary-3/OsWFo87OQ4G67zMS.htm)|Kurobozu|Kurobozu|modificada|
|[OTEJFZ03NDqtrhcj.htm](pathfinder-bestiary-3/OTEJFZ03NDqtrhcj.htm)|Hadrinnex|Hadrinnex|modificada|
|[pCDp2hdim12wonW5.htm](pathfinder-bestiary-3/pCDp2hdim12wonW5.htm)|Living Rune (Occult)|Runa viviente (Ocultismo)|modificada|
|[pFUv7BzSjYTOo5mO.htm](pathfinder-bestiary-3/pFUv7BzSjYTOo5mO.htm)|Monkey|Mono|modificada|
|[phOYPM1OVAKPg68l.htm](pathfinder-bestiary-3/phOYPM1OVAKPg68l.htm)|Omox|Omox|modificada|
|[PJCeh8sj9Sm5Eqz8.htm](pathfinder-bestiary-3/PJCeh8sj9Sm5Eqz8.htm)|Draxie|Draxie|modificada|
|[Pnw71fJ41j6Wx62M.htm](pathfinder-bestiary-3/Pnw71fJ41j6Wx62M.htm)|Harmona|Harmona|modificada|
|[PVstJNeHeWLU2XoK.htm](pathfinder-bestiary-3/PVstJNeHeWLU2XoK.htm)|Cecaelia Trapper|Trampero Cecaelia|modificada|
|[Pvuvyd4RKVyiVWlJ.htm](pathfinder-bestiary-3/Pvuvyd4RKVyiVWlJ.htm)|Mezlan|Mezlan|modificada|
|[PX8tlo804y4I1C8S.htm](pathfinder-bestiary-3/PX8tlo804y4I1C8S.htm)|Tylosaurus|Tylosaurus|modificada|
|[Q02Io3eFQpBad3vD.htm](pathfinder-bestiary-3/Q02Io3eFQpBad3vD.htm)|Stone Lion|León de piedra|modificada|
|[q2ja0fkdr4PeZOTE.htm](pathfinder-bestiary-3/q2ja0fkdr4PeZOTE.htm)|Ximtal|Ximtal|modificada|
|[qE0iZoeeOp7og5A5.htm](pathfinder-bestiary-3/qE0iZoeeOp7og5A5.htm)|Giant Seahorse|Caballito de mar gigante|modificada|
|[QHl25x11pMTJ9SxN.htm](pathfinder-bestiary-3/QHl25x11pMTJ9SxN.htm)|Draconal (White)|Draconal (Blanco)|modificada|
|[QJRi6WzEm0LbXjAc.htm](pathfinder-bestiary-3/QJRi6WzEm0LbXjAc.htm)|Doru|Doru|modificada|
|[QLxcPfaHfc1vmF1Y.htm](pathfinder-bestiary-3/QLxcPfaHfc1vmF1Y.htm)|Terra-Cotta Garrison|Guarnición de Terracota|modificada|
|[qlYzd5vmhsFTNdbX.htm](pathfinder-bestiary-3/qlYzd5vmhsFTNdbX.htm)|Esipil|Esipil|modificada|
|[qm0YqO9ik2U2Gjdx.htm](pathfinder-bestiary-3/qm0YqO9ik2U2Gjdx.htm)|Moon Hag|Hechicera lunar|modificada|
|[qnFiUNUyH0zuG6hj.htm](pathfinder-bestiary-3/qnFiUNUyH0zuG6hj.htm)|Quintessivore|Quintessivore|modificada|
|[QrD96KPUHGjjkLbL.htm](pathfinder-bestiary-3/QrD96KPUHGjjkLbL.htm)|Kappa|Kappa|modificada|
|[QUzBzxRy6HLeK7ja.htm](pathfinder-bestiary-3/QUzBzxRy6HLeK7ja.htm)|Dybbuk|Dybbuk|modificada|
|[qvVmYuERMib26Atc.htm](pathfinder-bestiary-3/qvVmYuERMib26Atc.htm)|Crossroads Guardian|Crossroads Guardian|modificada|
|[QWx9cIVUeP7dMOez.htm](pathfinder-bestiary-3/QWx9cIVUeP7dMOez.htm)|Roiling Incant (Evocation)|Encantamiento Hirviente (Evocación)|modificada|
|[QYkuUQNVml878cIy.htm](pathfinder-bestiary-3/QYkuUQNVml878cIy.htm)|Ledalusca|Ledalusca|modificada|
|[QyuUubTtR38kafue.htm](pathfinder-bestiary-3/QyuUubTtR38kafue.htm)|Manticore Paaridar|Manticore Paaridar|modificada|
|[rBHAfYDWpEuzbzqV.htm](pathfinder-bestiary-3/rBHAfYDWpEuzbzqV.htm)|Street Skelm|Calle Skelm|modificada|
|[RCsFDD6nqNUX0gIa.htm](pathfinder-bestiary-3/RCsFDD6nqNUX0gIa.htm)|Chyzaedu|Chyzaedu|modificada|
|[rFpExrqWywVDPYdP.htm](pathfinder-bestiary-3/rFpExrqWywVDPYdP.htm)|Brainchild|Brainchild|modificada|
|[Rgy8OyRY5szgE6d0.htm](pathfinder-bestiary-3/Rgy8OyRY5szgE6d0.htm)|Empress Ice Worm|Emperatriz Gusano de Hielo|modificada|
|[RicPhk9hLC3dDjp5.htm](pathfinder-bestiary-3/RicPhk9hLC3dDjp5.htm)|Maharaja|Maharaja|modificada|
|[RjJzLQ1nuPtKvnXU.htm](pathfinder-bestiary-3/RjJzLQ1nuPtKvnXU.htm)|Shabti Redeemer|Shabti Redentor|modificada|
|[rnxg09tUCFPGW8IS.htm](pathfinder-bestiary-3/rnxg09tUCFPGW8IS.htm)|Terror Bird|Pájaro del Terror|modificada|
|[rOBXOcxMcnFR2P9y.htm](pathfinder-bestiary-3/rOBXOcxMcnFR2P9y.htm)|Skull Peeler|Skull Peeler|modificada|
|[rOFpnEic0eJdaxiM.htm](pathfinder-bestiary-3/rOFpnEic0eJdaxiM.htm)|Kangaroo|Canguro|modificada|
|[rqNNvJO0XEVHWwbW.htm](pathfinder-bestiary-3/rqNNvJO0XEVHWwbW.htm)|Wizard Sponge (Underwater)|Esponja hechicera (bajo el agua)|modificada|
|[rr48cUJxGpII7jWz.htm](pathfinder-bestiary-3/rr48cUJxGpII7jWz.htm)|Feathered Bear|Oso emplumado|modificada|
|[rrkjmcNlODuIpbz6.htm](pathfinder-bestiary-3/rrkjmcNlODuIpbz6.htm)|Ouroboros|Ouroboros|modificada|
|[RTTs4lvkcPz8u6IY.htm](pathfinder-bestiary-3/RTTs4lvkcPz8u6IY.htm)|Azarketi Explorer|Explorador Azarketi|modificada|
|[rVtBZrHnWM3lvSs7.htm](pathfinder-bestiary-3/rVtBZrHnWM3lvSs7.htm)|Brimorak|Brimorak|modificada|
|[rW6vTwkAEAH8AMGw.htm](pathfinder-bestiary-3/rW6vTwkAEAH8AMGw.htm)|Wayang Whisperblade|Wayang Whisperblade|modificada|
|[RyFqNQ86931Y5tqO.htm](pathfinder-bestiary-3/RyFqNQ86931Y5tqO.htm)|Lovelorn|Lovelorn|modificada|
|[RZ1SLG0gxrOYSKRs.htm](pathfinder-bestiary-3/RZ1SLG0gxrOYSKRs.htm)|Vanara Disciple|Vanara Disciple|modificada|
|[S2DdLrq9V7M5WzM3.htm](pathfinder-bestiary-3/S2DdLrq9V7M5WzM3.htm)|Dvorovoi|Dvorovoi|modificada|
|[S6V3z6bE6G9J68tE.htm](pathfinder-bestiary-3/S6V3z6bE6G9J68tE.htm)|Ringhorn Ram|Ringhorn Ram|modificada|
|[SaNOrmVDvQGX5U1D.htm](pathfinder-bestiary-3/SaNOrmVDvQGX5U1D.htm)|Harpy Skeleton|Esqueleto de Arpía|modificada|
|[sCzgD99DO10Koovq.htm](pathfinder-bestiary-3/sCzgD99DO10Koovq.htm)|Silvanshee|Silvanshee|modificada|
|[sESP4UwiJHeNUYEv.htm](pathfinder-bestiary-3/sESP4UwiJHeNUYEv.htm)|Monkey Swarm|Enjambre de monos|modificada|
|[sFePZ5s3vMtC4YPx.htm](pathfinder-bestiary-3/sFePZ5s3vMtC4YPx.htm)|Draconal (Black)|Draconal (Negro)|modificada|
|[sgZGrUQrfH3QPdXF.htm](pathfinder-bestiary-3/sgZGrUQrfH3QPdXF.htm)|Caulborn|Caulborn|modificada|
|[ShcTh3dSwjxbtoGG.htm](pathfinder-bestiary-3/ShcTh3dSwjxbtoGG.htm)|Tzitzimitl|Tzitzimitl|modificada|
|[SIE8CcEblKI9VwqO.htm](pathfinder-bestiary-3/SIE8CcEblKI9VwqO.htm)|Living Graffiti (Oil)|Graffiti viviente (aceite)|modificada|
|[SjvmOu8v9haaZE1p.htm](pathfinder-bestiary-3/SjvmOu8v9haaZE1p.htm)|Leng Ghoul|Leng Ghoul|modificada|
|[sNeIVL8w7NPFtpK2.htm](pathfinder-bestiary-3/sNeIVL8w7NPFtpK2.htm)|Wizard Sponge (Fey Domain)|Esponja hechicera (Dominio feérico)|modificada|
|[sNEvW8qBxztZcI8p.htm](pathfinder-bestiary-3/sNEvW8qBxztZcI8p.htm)|Shrine Skelm|Santuario Skelm|modificada|
|[SNOPmNj4ZEiFBAdv.htm](pathfinder-bestiary-3/SNOPmNj4ZEiFBAdv.htm)|Young Forest Dragon (Spellcaster)|Joven Dragón del Bosque (Hechicero)|modificada|
|[sp0CZ8B2IPw3bBVi.htm](pathfinder-bestiary-3/sp0CZ8B2IPw3bBVi.htm)|Huldra|Huldra|modificada|
|[SQKdvPIhWkrHlkbn.htm](pathfinder-bestiary-3/SQKdvPIhWkrHlkbn.htm)|City Guard Squadron|Escuadrón de la guardia de la ciudad|modificada|
|[ss5f2tMeJWO3u8GU.htm](pathfinder-bestiary-3/ss5f2tMeJWO3u8GU.htm)|Sasquatch|Sasquatch|modificada|
|[Su21mCjUxFJnoWGg.htm](pathfinder-bestiary-3/Su21mCjUxFJnoWGg.htm)|Azer|Azer|modificada|
|[SuI5sxy5cuc0lnsh.htm](pathfinder-bestiary-3/SuI5sxy5cuc0lnsh.htm)|Platecarpus|Platecarpus|modificada|
|[szEyrpElcYBagqL2.htm](pathfinder-bestiary-3/szEyrpElcYBagqL2.htm)|Draconal (Green)|Draconal (Verde)|modificada|
|[tdSnY3lgYJnsvB8n.htm](pathfinder-bestiary-3/tdSnY3lgYJnsvB8n.htm)|Wizard Sponge|Esponja hechicera|modificada|
|[TeaF0WreNshQxbe8.htm](pathfinder-bestiary-3/TeaF0WreNshQxbe8.htm)|Wizard Sponge (Toxic Lair)|Esponja hechicera (Guarida tóxica)|modificada|
|[tjtTHdIBP5QIAyS7.htm](pathfinder-bestiary-3/tjtTHdIBP5QIAyS7.htm)|Vishkanya Infiltrator|Infiltrador Vishkanya|modificada|
|[tQBUoh5wLJXiFdX6.htm](pathfinder-bestiary-3/tQBUoh5wLJXiFdX6.htm)|Seaweed Leshy|Alga Leshy (s), leshys (pl.)|modificada|
|[tQIYWBefvpemUVeJ.htm](pathfinder-bestiary-3/tQIYWBefvpemUVeJ.htm)|Nosferatu Thrall|Nosferatu Thrall|modificada|
|[tr9bmyZ6CYl2FPnr.htm](pathfinder-bestiary-3/tr9bmyZ6CYl2FPnr.htm)|Valkyrie|Valquiria|modificada|
|[tvAlNMQluKDpfXMz.htm](pathfinder-bestiary-3/tvAlNMQluKDpfXMz.htm)|Living Rune (Divine)|Runa Viviente (Divina)|modificada|
|[tWjY4BJMhawdqkD5.htm](pathfinder-bestiary-3/tWjY4BJMhawdqkD5.htm)|Ancient Sovereign Dragon (Spellcaster)|Ancient Sovereign Dragon (Hechicero)|modificada|
|[TzltHdjikojp7Um7.htm](pathfinder-bestiary-3/TzltHdjikojp7Um7.htm)|Penanggalan|Penanggalan|modificada|
|[U3rMc5sN05MempVX.htm](pathfinder-bestiary-3/U3rMc5sN05MempVX.htm)|Maftet Guardian|Maftet Guardian|modificada|
|[uDebX6flGwrviGZK.htm](pathfinder-bestiary-3/uDebX6flGwrviGZK.htm)|Hesperid Queen|Reina Hesperida|modificada|
|[uDNQyboLAiIxBatL.htm](pathfinder-bestiary-3/uDNQyboLAiIxBatL.htm)|Adult Sea Dragon|Dragón de mar adulto|modificada|
|[ULfACJrDBnZLNBKj.htm](pathfinder-bestiary-3/ULfACJrDBnZLNBKj.htm)|Terra-Cotta Soldier|Soldado de Terracota|modificada|
|[uP6dE5adlWJ9DrFY.htm](pathfinder-bestiary-3/uP6dE5adlWJ9DrFY.htm)|Corrupted Relic|Reliquia corrompida|modificada|
|[UTqProdd8LA0X1BQ.htm](pathfinder-bestiary-3/UTqProdd8LA0X1BQ.htm)|Sepid|Sepid|modificada|
|[UxiwQ2Nmvfk9Q9tC.htm](pathfinder-bestiary-3/UxiwQ2Nmvfk9Q9tC.htm)|Shaukeen|Shaukeen|modificada|
|[v0bKmqnUHxPTFQu4.htm](pathfinder-bestiary-3/v0bKmqnUHxPTFQu4.htm)|Hesperid|Hesperid|modificada|
|[v0nvME08U3mZJWB3.htm](pathfinder-bestiary-3/v0nvME08U3mZJWB3.htm)|Betobeto-San|Betobeto-San|modificada|
|[v7nu3NMYTS0YyxH4.htm](pathfinder-bestiary-3/v7nu3NMYTS0YyxH4.htm)|Adlet|Adlet|modificada|
|[V9SxfxUgljJR9xx5.htm](pathfinder-bestiary-3/V9SxfxUgljJR9xx5.htm)|Hatred Siktempora|Odio Siktempora|modificada|
|[Vf4uzrQYTbENFFFF.htm](pathfinder-bestiary-3/Vf4uzrQYTbENFFFF.htm)|Young Sky Dragon (Spellcaster)|Joven Dragón del Cielo (Hechicero)|modificada|
|[VF81W91YRFgtBLli.htm](pathfinder-bestiary-3/VF81W91YRFgtBLli.htm)|Einherji|Einherji|modificada|
|[Vi2p9VroteSHsSy9.htm](pathfinder-bestiary-3/Vi2p9VroteSHsSy9.htm)|Procyal|Procyal|modificada|
|[vJAPvmWCjVGKr06E.htm](pathfinder-bestiary-3/vJAPvmWCjVGKr06E.htm)|Arboreal Archive|Archivo arboral (pl. -es)|modificada|
|[VMZOQGY1x1of0XLm.htm](pathfinder-bestiary-3/VMZOQGY1x1of0XLm.htm)|Cunning Fox|Zorro Astuto|modificada|
|[vOTFqODTDDC2BDLx.htm](pathfinder-bestiary-3/vOTFqODTDDC2BDLx.htm)|Rancorous Priesthood|Sacerdocio Rencoroso|modificada|
|[Vt28ucE0FUJYl1fD.htm](pathfinder-bestiary-3/Vt28ucE0FUJYl1fD.htm)|Coral Capuchin|Coral Capuchino|modificada|
|[VUYg62jFjYB5Mxh0.htm](pathfinder-bestiary-3/VUYg62jFjYB5Mxh0.htm)|Scalescribe|Scalescribe|modificada|
|[VVSTEyAnm9OSbfJ5.htm](pathfinder-bestiary-3/VVSTEyAnm9OSbfJ5.htm)|Werebat|Werebat|modificada|
|[vWF79pVG3dbZIlId.htm](pathfinder-bestiary-3/vWF79pVG3dbZIlId.htm)|Young Forest Dragon|Joven Dragón del Bosque|modificada|
|[vwzfmjR0Me6xPdTP.htm](pathfinder-bestiary-3/vwzfmjR0Me6xPdTP.htm)|Tomb Giant|Tumba Gigante|modificada|
|[vzBsZqtGuj2FVLGj.htm](pathfinder-bestiary-3/vzBsZqtGuj2FVLGj.htm)|Nucol|Nucol|modificada|
|[WfCLrFwwWSA7KRUu.htm](pathfinder-bestiary-3/WfCLrFwwWSA7KRUu.htm)|Living Rune (Primal)|Runa viviente (Primal)|modificada|
|[wLG0f6J8cgyCA0w4.htm](pathfinder-bestiary-3/wLG0f6J8cgyCA0w4.htm)|Zuishin|Zuishin|modificada|
|[wmKIB7cgWdAZ29mv.htm](pathfinder-bestiary-3/wmKIB7cgWdAZ29mv.htm)|Adult Underworld Dragon|Dragón adulto del inframundo|modificada|
|[wnrgiB2PkaBC5gOQ.htm](pathfinder-bestiary-3/wnrgiB2PkaBC5gOQ.htm)|Animated Silverware Swarm|Enjambre de platería animada|modificada|
|[WPRjynFmJ2p1MnT3.htm](pathfinder-bestiary-3/WPRjynFmJ2p1MnT3.htm)|Adult Sovereign Dragon (Spellcaster)|Dragón Soberano Adulto (Hechicero)|modificada|
|[Wq0Euk0RK6rhRDsN.htm](pathfinder-bestiary-3/Wq0Euk0RK6rhRDsN.htm)|Severed Head|Cabeza cortada|modificada|
|[Wu6vmegKwR4bgLse.htm](pathfinder-bestiary-3/Wu6vmegKwR4bgLse.htm)|Dramofir|Dramofir|modificada|
|[wv0hiJIGQjTU1pnO.htm](pathfinder-bestiary-3/wv0hiJIGQjTU1pnO.htm)|Galvo|Galvo|modificada|
|[wVMMl1jfxWyqU4yq.htm](pathfinder-bestiary-3/wVMMl1jfxWyqU4yq.htm)|Storm Hag|Hechicera tormentosa|modificada|
|[XfHvnixZO9zVwZxC.htm](pathfinder-bestiary-3/XfHvnixZO9zVwZxC.htm)|Living Graffiti (Blood)|Graffiti viviente (Sangre)|modificada|
|[XgCQnswAedPcwLck.htm](pathfinder-bestiary-3/XgCQnswAedPcwLck.htm)|Pairaka|Pairaka|modificada|
|[xHMiDdTkZA3HzVSJ.htm](pathfinder-bestiary-3/xHMiDdTkZA3HzVSJ.htm)|Eunemvro|Eunemvro|modificada|
|[xIT2yHlwILLc5hgw.htm](pathfinder-bestiary-3/xIT2yHlwILLc5hgw.htm)|Amalgamite|Amalgamita|modificada|
|[XmOYhscNHFw7M2G0.htm](pathfinder-bestiary-3/XmOYhscNHFw7M2G0.htm)|Rhu-Chalik|Rhu-Chalik|modificada|
|[XnHmGsR7bQCHTMdA.htm](pathfinder-bestiary-3/XnHmGsR7bQCHTMdA.htm)|Empress Necral Worm|Emperatriz Gusano Necral|modificada|
|[xnrXf66rFvAfyhE9.htm](pathfinder-bestiary-3/xnrXf66rFvAfyhE9.htm)|Palace Skelm|Palacio Skelm|modificada|
|[XrSz2IIKbeYFGILW.htm](pathfinder-bestiary-3/XrSz2IIKbeYFGILW.htm)|Strix Kinmate|Strix Kinmate|modificada|
|[xswHz64371Sb9Let.htm](pathfinder-bestiary-3/xswHz64371Sb9Let.htm)|Nightmarchers|Nightmarchers|modificada|
|[XwtH5kP5gkY42yWp.htm](pathfinder-bestiary-3/XwtH5kP5gkY42yWp.htm)|Clockwork Dragon|Dragón Mecánico|modificada|
|[xwTZZAEs1sf5RWCq.htm](pathfinder-bestiary-3/xwTZZAEs1sf5RWCq.htm)|Danava Titan|Danava Titán|modificada|
|[XXsOK8ZUoqQATarG.htm](pathfinder-bestiary-3/XXsOK8ZUoqQATarG.htm)|Moose|Moose|modificada|
|[XyEvsURVDnJwb76F.htm](pathfinder-bestiary-3/XyEvsURVDnJwb76F.htm)|Bison|Bison|modificada|
|[Y8lQqtOgXYXDCPFg.htm](pathfinder-bestiary-3/Y8lQqtOgXYXDCPFg.htm)|Swordkeeper|Swordkeeper|modificada|
|[yCyZlDAaJ6cDYtB7.htm](pathfinder-bestiary-3/yCyZlDAaJ6cDYtB7.htm)|Wolliped|Wolliped|modificada|
|[YfosnJUwR0fSV7a8.htm](pathfinder-bestiary-3/YfosnJUwR0fSV7a8.htm)|Young Underworld Dragon (Spellcaster)|Joven dragón del inframundo (Hechicero)|modificada|
|[Yg7R4UgB1FjF2Euu.htm](pathfinder-bestiary-3/Yg7R4UgB1FjF2Euu.htm)|Bauble Beast|Bauble Beast|modificada|
|[yi9g7D49uGvAdGDP.htm](pathfinder-bestiary-3/yi9g7D49uGvAdGDP.htm)|Mi-Go|Mi-Go|modificada|
|[YLLyd6Jb4Zqmz0lo.htm](pathfinder-bestiary-3/YLLyd6Jb4Zqmz0lo.htm)|Aphorite Sharpshooter|Aphorite Sharpshooter|modificada|
|[YO3aTYRNGXWiSRal.htm](pathfinder-bestiary-3/YO3aTYRNGXWiSRal.htm)|Sorcerous Skull Swarm|Sorcerous Skull Swarm|modificada|
|[Yq9TcUof5D117yns.htm](pathfinder-bestiary-3/Yq9TcUof5D117yns.htm)|Camel|Camel|modificada|
|[YsgpbtWVT3q0OLWv.htm](pathfinder-bestiary-3/YsgpbtWVT3q0OLWv.htm)|Empress Lava Worm|Gusano de lava emperatriz|modificada|
|[ytYKPtPotjvsWoSl.htm](pathfinder-bestiary-3/ytYKPtPotjvsWoSl.htm)|Ancient Sovereign Dragon|Dragón Soberano Antiguo|modificada|
|[YX0CqhKKtRzoLkuP.htm](pathfinder-bestiary-3/YX0CqhKKtRzoLkuP.htm)|Water Wisp|Water Wisp|modificada|
|[YzWAuyEVPLTyRoAy.htm](pathfinder-bestiary-3/YzWAuyEVPLTyRoAy.htm)|Ancient Underworld Dragon (Spellcaster)|Ancient Underworld Dragon (Hechicero)|modificada|
|[Z5RBfl8x39uFpDUn.htm](pathfinder-bestiary-3/Z5RBfl8x39uFpDUn.htm)|Myceloid|Myceloid|modificada|
|[ZDGYrJ68aTzZ2EtT.htm](pathfinder-bestiary-3/ZDGYrJ68aTzZ2EtT.htm)|Phantom Knight|Phantom Knight|modificada|
|[zdJgaVe6VRSfEE1n.htm](pathfinder-bestiary-3/zdJgaVe6VRSfEE1n.htm)|Caligni Caller|Llamador de Caligni (p.-s)|modificada|
|[zGco5QmokZpFgLes.htm](pathfinder-bestiary-3/zGco5QmokZpFgLes.htm)|Grimple|Grimple|modificada|
|[zGtfiKku0td5E0VJ.htm](pathfinder-bestiary-3/zGtfiKku0td5E0VJ.htm)|Fossil Golem|Golem fósil|modificada|
|[zMoJh88vXJQwSHsX.htm](pathfinder-bestiary-3/zMoJh88vXJQwSHsX.htm)|Abandoned Zealot|Zealot Abandonado|modificada|
|[ZMrydoEfgGUAJGNI.htm](pathfinder-bestiary-3/ZMrydoEfgGUAJGNI.htm)|Plague Giant|Gigante de la peste|modificada|
|[ZOrG61DZ9aiv8poK.htm](pathfinder-bestiary-3/ZOrG61DZ9aiv8poK.htm)|Owb Prophet|Profeta Owb|modificada|
|[ZwtcCnW9CEs78WRC.htm](pathfinder-bestiary-3/ZwtcCnW9CEs78WRC.htm)|Mobogo|Mobogo|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
