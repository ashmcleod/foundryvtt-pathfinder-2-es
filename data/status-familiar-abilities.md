# Estado de la traducción (familiar-abilities)

 * **modificada**: 64
 * **ninguna**: 1


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[01-Rv1KTRioaZOWyQI6.htm](familiar-abilities/01-Rv1KTRioaZOWyQI6.htm)|Alchemical Gut|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[01-0Xrkk46IM43iI1Fv.htm](familiar-abilities/01-0Xrkk46IM43iI1Fv.htm)|Darkvision|Visión en la oscuridad|modificada|
|[01-2fiQzoEKu6YUnrU9.htm](familiar-abilities/01-2fiQzoEKu6YUnrU9.htm)|Independent|Independiente|modificada|
|[01-57b8u8s3fV0UCrgJ.htm](familiar-abilities/01-57b8u8s3fV0UCrgJ.htm)|Plant Form|Forma de planta|modificada|
|[01-5gwqSpkRqWzrbDDU.htm](familiar-abilities/01-5gwqSpkRqWzrbDDU.htm)|Damage Avoidance: Will|Evasión de Daño: Voluntad|modificada|
|[01-7QosmRHlyLLhU1hX.htm](familiar-abilities/01-7QosmRHlyLLhU1hX.htm)|Lab Assistant|Asistente de Laboratorio|modificada|
|[01-7ZxPS0UU7pf7wjp0.htm](familiar-abilities/01-7ZxPS0UU7pf7wjp0.htm)|Ambassador|Embajador|modificada|
|[01-8Z1UkLEWkFWIjOF8.htm](familiar-abilities/01-8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|Poison Reservoir|modificada|
|[01-92lgSEPFIDLvKOCF.htm](familiar-abilities/01-92lgSEPFIDLvKOCF.htm)|Accompanist|Acompañante|modificada|
|[01-9PsptrEoCC4QdM23.htm](familiar-abilities/01-9PsptrEoCC4QdM23.htm)|Valet|Valet|modificada|
|[01-A0C86V3MUECpX5jd.htm](familiar-abilities/01-A0C86V3MUECpX5jd.htm)|Amphibious|Anfibio|modificada|
|[01-aEKA3YWekLhEhuV8.htm](familiar-abilities/01-aEKA3YWekLhEhuV8.htm)|Threat Display|Pantalla de amenaza|modificada|
|[01-Amzezp93MZckBYRZ.htm](familiar-abilities/01-Amzezp93MZckBYRZ.htm)|Wavesense|Wavesense|modificada|
|[01-asOhEdyF8CWFbR96.htm](familiar-abilities/01-asOhEdyF8CWFbR96.htm)|Spellcasting|Spellcasting|modificada|
|[01-BXssJhTJjKrfojwG.htm](familiar-abilities/01-BXssJhTJjKrfojwG.htm)|Fast Movement: Land|Movimiento rápido: Tierra|modificada|
|[01-C16JgmeJJG249WXz.htm](familiar-abilities/01-C16JgmeJJG249WXz.htm)|Mask Freeze|Congelación de máscara|modificada|
|[01-cT5octWchU4gjrhP.htm](familiar-abilities/01-cT5octWchU4gjrhP.htm)|Manual Dexterity|Destreza Manual|modificada|
|[01-D0ltNUJnN7UjJpA1.htm](familiar-abilities/01-D0ltNUJnN7UjJpA1.htm)|Innate Surge|Oleaje Innato|modificada|
|[01-deC1yIM2S5szGdzT.htm](familiar-abilities/01-deC1yIM2S5szGdzT.htm)|Lifelink|Lifelink|modificada|
|[01-dpjf1CyMEILpJWyp.htm](familiar-abilities/01-dpjf1CyMEILpJWyp.htm)|Darkeater|Darkeater|modificada|
|[01-dWTfO5WbLkD5mw2H.htm](familiar-abilities/01-dWTfO5WbLkD5mw2H.htm)|Climber|Escalador|modificada|
|[01-FcQQLMAJMgOLjnSv.htm](familiar-abilities/01-FcQQLMAJMgOLjnSv.htm)|Resistance|Resistencia|modificada|
|[01-FlRUb8U13Crj3NaA.htm](familiar-abilities/01-FlRUb8U13Crj3NaA.htm)|Scent|Scent|modificada|
|[01-fmsVItn94FeY5Q3X.htm](familiar-abilities/01-fmsVItn94FeY5Q3X.htm)|Snoop|Snoop|modificada|
|[01-gPceRQqO847lvSnb.htm](familiar-abilities/01-gPceRQqO847lvSnb.htm)|Share Senses|Compartir Sentidos|modificada|
|[01-hMrxiUPHXKpKu1Ha.htm](familiar-abilities/01-hMrxiUPHXKpKu1Ha.htm)|Major Resistance|Resistencia superior|modificada|
|[01-j1qZiH50Bl2SJ8vT.htm](familiar-abilities/01-j1qZiH50Bl2SJ8vT.htm)|Shadow Step|Paso de Sombra|modificada|
|[01-j9vOSbF9kLibhSIf.htm](familiar-abilities/01-j9vOSbF9kLibhSIf.htm)|Second Opinion|Segunda Opinión|modificada|
|[01-jdlefpPcSCIe27vO.htm](familiar-abilities/01-jdlefpPcSCIe27vO.htm)|Familiar Focus|Enfoque Familiar|modificada|
|[01-jevzf9JbJJibpqaI.htm](familiar-abilities/01-jevzf9JbJJibpqaI.htm)|Skilled|Hábil|modificada|
|[01-JRP2bdkdCdj2JDrq.htm](familiar-abilities/01-JRP2bdkdCdj2JDrq.htm)|Master's Form|Master's Form|modificada|
|[01-K5OLRDsGCfPZ6mO6.htm](familiar-abilities/01-K5OLRDsGCfPZ6mO6.htm)|Damage Avoidance: Reflex|Evasión de Daño: Reflejo|modificada|
|[01-Le8UWr5BU8rV3iBf.htm](familiar-abilities/01-Le8UWr5BU8rV3iBf.htm)|Tough|Dureza|modificada|
|[01-lpyJAl5B4j2DC7mc.htm](familiar-abilities/01-lpyJAl5B4j2DC7mc.htm)|Gills|Agallas|modificada|
|[01-LrDnat1DsGJoAiKv.htm](familiar-abilities/01-LrDnat1DsGJoAiKv.htm)|Tremorsense|Tremorsense|modificada|
|[01-mK3mAUWiRLZZYNdz.htm](familiar-abilities/01-mK3mAUWiRLZZYNdz.htm)|Damage Avoidance: Fortitude|Evasión de Daño: Fortaleza|modificada|
|[01-mKmBgQmPmiLOlEvw.htm](familiar-abilities/01-mKmBgQmPmiLOlEvw.htm)|Augury|Augurio|modificada|
|[01-nrPl3Dz7fbnmas7T.htm](familiar-abilities/01-nrPl3Dz7fbnmas7T.htm)|Spirit Touch|Spirit Touch|modificada|
|[01-o0fxDDUu2ZSWYDTr.htm](familiar-abilities/01-o0fxDDUu2ZSWYDTr.htm)|Soul Sight|Soul Sight|modificada|
|[01-O5TIjqXAuta8iVSz.htm](familiar-abilities/01-O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|Focused Rejuvenation|modificada|
|[01-Q42nrq9LwDdbeZM5.htm](familiar-abilities/01-Q42nrq9LwDdbeZM5.htm)|Restorative Familiar|Familiar restablecimiento|modificada|
|[01-qTxH8mSOvc4PMzrP.htm](familiar-abilities/01-qTxH8mSOvc4PMzrP.htm)|Kinspeech|Kinspeech|modificada|
|[01-REJfFezELjc5Gzsy.htm](familiar-abilities/01-REJfFezELjc5Gzsy.htm)|Recall Familiar|Recall Familiar|modificada|
|[01-rs4Awf4k1e0Mj797.htm](familiar-abilities/01-rs4Awf4k1e0Mj797.htm)|Cantrip Connection|Conexión de truco|modificada|
|[01-SxWYVgqNMsq0OijU.htm](familiar-abilities/01-SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|Movimiento rápido: Trepar|modificada|
|[01-tEWAHPPZULvPgHnT.htm](familiar-abilities/01-tEWAHPPZULvPgHnT.htm)|Tattoo Transformation|Tatuaje Transformación|modificada|
|[01-uUrsZ4WvhjKjFjnt.htm](familiar-abilities/01-uUrsZ4WvhjKjFjnt.htm)|Toolbearer|Toolbearer|modificada|
|[01-uy15sDBuYNK48N3v.htm](familiar-abilities/01-uy15sDBuYNK48N3v.htm)|Burrower|Burrower|modificada|
|[01-v7zE3tKQb9G6PaYU.htm](familiar-abilities/01-v7zE3tKQb9G6PaYU.htm)|Partner in Crime|Partner in Crime|modificada|
|[01-VHQUZcjUxfC3GcJ9.htm](familiar-abilities/01-VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|Movimiento rápido: Volar|modificada|
|[01-vpw2ReYdcyQBpdqn.htm](familiar-abilities/01-vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|Movimiento rápido: Nadar|modificada|
|[01-wOgvBymJOVQDSm1Q.htm](familiar-abilities/01-wOgvBymJOVQDSm1Q.htm)|Spell Delivery|Spell Delivery|modificada|
|[01-Xanjwv4YU0CBnsMw.htm](familiar-abilities/01-Xanjwv4YU0CBnsMw.htm)|Spell Battery|Batería de Hechizos|modificada|
|[01-XCqYnlVbLGqEGPeX.htm](familiar-abilities/01-XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|Toque de telepatía|modificada|
|[01-ZHSzNt3NxkXbj1mI.htm](familiar-abilities/01-ZHSzNt3NxkXbj1mI.htm)|Flier|Volador|modificada|
|[01-zKTL9y9et0oTHEYS.htm](familiar-abilities/01-zKTL9y9et0oTHEYS.htm)|Greater Resistance|Mayor Resistencia|modificada|
|[01-ZtKb89o1PPhwJ3Lx.htm](familiar-abilities/01-ZtKb89o1PPhwJ3Lx.htm)|Extra Reagents|Reactivos Extra|modificada|
|[03-3y6GGXQgyJ4Hq4Yt.htm](familiar-abilities/03-3y6GGXQgyJ4Hq4Yt.htm)|Radiant|Radiante|modificada|
|[03-cy7bdQqVANipyljS.htm](familiar-abilities/03-cy7bdQqVANipyljS.htm)|Erudite|Erudito|modificada|
|[03-ou91pzf9TlOnIjYn.htm](familiar-abilities/03-ou91pzf9TlOnIjYn.htm)|Luminous|Luminoso|modificada|
|[03-ReIgBsaM95BTvHpN.htm](familiar-abilities/03-ReIgBsaM95BTvHpN.htm)|Medic|Medic|modificada|
|[04-43xB5UnexISlfRa5.htm](familiar-abilities/04-43xB5UnexISlfRa5.htm)|Purify Air|Purificar Aire|modificada|
|[04-LUBS9csNNgRTui4p.htm](familiar-abilities/04-LUBS9csNNgRTui4p.htm)|Grasping Tendrils|Agarrar con zarcillos|modificada|
|[04-SKIS1xexaOvrecdV.htm](familiar-abilities/04-SKIS1xexaOvrecdV.htm)|Verdant Burst|Estallido de vegetación|modificada|
|[zyMRLQnFCQVpltiR.htm](familiar-abilities/zyMRLQnFCQVpltiR.htm)|Speech|Discurso|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
