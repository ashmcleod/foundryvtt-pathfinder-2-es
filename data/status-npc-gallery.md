# Estado de la traducción (npc-gallery)

 * **modificada**: 99


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0Ex7rBuiJVu2NwCz.htm](npc-gallery/0Ex7rBuiJVu2NwCz.htm)|Demonologist|Demonologist|modificada|
|[0Kb4z4h8KVqfrIju.htm](npc-gallery/0Kb4z4h8KVqfrIju.htm)|Assassin|Asesino|modificada|
|[1iz7O6DLDJqStojd.htm](npc-gallery/1iz7O6DLDJqStojd.htm)|Servant|Servant|modificada|
|[1NZ1ZAgcUlWKmQSs.htm](npc-gallery/1NZ1ZAgcUlWKmQSs.htm)|Bosun|Bosun|modificada|
|[1sWw5OgmpazLVqRQ.htm](npc-gallery/1sWw5OgmpazLVqRQ.htm)|Mage For Hire|Mage For Hire|modificada|
|[1U1URD7IyddoD5zE.htm](npc-gallery/1U1URD7IyddoD5zE.htm)|Fence|Valla|modificada|
|[2SBKFCog4JY3WrOW.htm](npc-gallery/2SBKFCog4JY3WrOW.htm)|Rain-Scribe|Escriba de la lluvia|modificada|
|[3lZhmvNLQkiYGAof.htm](npc-gallery/3lZhmvNLQkiYGAof.htm)|Sage|Sage|modificada|
|[401MnHX5aO21P2Y8.htm](npc-gallery/401MnHX5aO21P2Y8.htm)|Stone Giant Monk|Monje Gigante de Piedra|modificada|
|[48bZvtRcd7T6FmA7.htm](npc-gallery/48bZvtRcd7T6FmA7.htm)|Privateer Captain|Capitán Corsario|modificada|
|[51E3fdESgGjQxcMv.htm](npc-gallery/51E3fdESgGjQxcMv.htm)|Astronomer|Astrónomo|modificada|
|[5LvpvIMhaYLcyAk6.htm](npc-gallery/5LvpvIMhaYLcyAk6.htm)|Pirate|Pirata|modificada|
|[5Rqh2dBxGU8Jwf56.htm](npc-gallery/5Rqh2dBxGU8Jwf56.htm)|Azarketi Sailor|Azarketi Marino|modificada|
|[6IADTZHYowxObqAk.htm](npc-gallery/6IADTZHYowxObqAk.htm)|Barkeep|Barman|modificada|
|[7eJJIIVEDB3EFFcZ.htm](npc-gallery/7eJJIIVEDB3EFFcZ.htm)|Bodyguard|Guardaespaldas|modificada|
|[7GGHuOlSzcaF2AdL.htm](npc-gallery/7GGHuOlSzcaF2AdL.htm)|Charlatan|Charlatán|modificada|
|[7RF95b3WHkvHWLrv.htm](npc-gallery/7RF95b3WHkvHWLrv.htm)|Physician|Médico|modificada|
|[8coHofIpLa5ZnjAF.htm](npc-gallery/8coHofIpLa5ZnjAF.htm)|Navigator|Navegante|modificada|
|[8WFGygPv7UHh7zdJ.htm](npc-gallery/8WFGygPv7UHh7zdJ.htm)|Apothecary|Boticario|modificada|
|[95IcOUvxABvj5lvo.htm](npc-gallery/95IcOUvxABvj5lvo.htm)|Changeling Hellknight|Changeling Hellknight|modificada|
|[9jDT7EhtlZtNpCz7.htm](npc-gallery/9jDT7EhtlZtNpCz7.htm)|False Priest|Falso Sacerdote|modificada|
|[aJR3f8YcAkmfx7im.htm](npc-gallery/aJR3f8YcAkmfx7im.htm)|Apprentice|Aprendiz|modificada|
|[Ap87yR4WOs0wKHx7.htm](npc-gallery/Ap87yR4WOs0wKHx7.htm)|Cultist|Cultista|modificada|
|[B09JfuBZHjcRXztU.htm](npc-gallery/B09JfuBZHjcRXztU.htm)|Burglar|Ladrón|modificada|
|[B0pZAGooj735FGfw.htm](npc-gallery/B0pZAGooj735FGfw.htm)|Tomb Raider|Tomb Raider|modificada|
|[B13dyXTo1xWVyj2R.htm](npc-gallery/B13dyXTo1xWVyj2R.htm)|Palace Guard|Guardia de Palacio|modificada|
|[bc1jeTvmzKeYGVw9.htm](npc-gallery/bc1jeTvmzKeYGVw9.htm)|Virtuous Defender|Virtuous Defender|modificada|
|[cAOWcPIjtZYXYZ3i.htm](npc-gallery/cAOWcPIjtZYXYZ3i.htm)|Merchant|Mercader|modificada|
|[Cnm5zWmuTEYy6mPx.htm](npc-gallery/Cnm5zWmuTEYy6mPx.htm)|Mastermind|Mastermind|modificada|
|[crTWewxna93vEt6B.htm](npc-gallery/crTWewxna93vEt6B.htm)|Acolyte of Nethys|Acólito de Nethys|modificada|
|[DD2JNeRxO79WFlOL.htm](npc-gallery/DD2JNeRxO79WFlOL.htm)|Archer Sentry|Archer Sentry|modificada|
|[DFurZlcpcNrUmmER.htm](npc-gallery/DFurZlcpcNrUmmER.htm)|Ruffian|Rufián|modificada|
|[DSA03902sWGot0ev.htm](npc-gallery/DSA03902sWGot0ev.htm)|Miner|Minero|modificada|
|[EMl8hARVJk8SNVyW.htm](npc-gallery/EMl8hARVJk8SNVyW.htm)|Charming Scoundrel|Granuja encantadora|modificada|
|[EslFhpdvQf7KN8W3.htm](npc-gallery/EslFhpdvQf7KN8W3.htm)|Chronicler|Cronista|modificada|
|[EzD6YlNXL48rN8nq.htm](npc-gallery/EzD6YlNXL48rN8nq.htm)|Despot|Déspota|modificada|
|[F8AzuPOCcveWasza.htm](npc-gallery/F8AzuPOCcveWasza.htm)|Ethereal Sailor|Etérea marino|modificada|
|[G2ftdkyJ5WDonL0C.htm](npc-gallery/G2ftdkyJ5WDonL0C.htm)|Urchin|Urchin|modificada|
|[GoGNtiHuYycppLPk.htm](npc-gallery/GoGNtiHuYycppLPk.htm)|Bounty Hunter|Cazarrecompensas|modificada|
|[GRtEwNQgKQ9j9JPK.htm](npc-gallery/GRtEwNQgKQ9j9JPK.htm)|Warden|Warden|modificada|
|[gzirsGA07yG6CaG8.htm](npc-gallery/gzirsGA07yG6CaG8.htm)|Jailer|Carcelero|modificada|
|[hK8Tpg3baKWzmPEv.htm](npc-gallery/hK8Tpg3baKWzmPEv.htm)|Tax Collector|Recaudador de Impuestos|modificada|
|[Hle05FibgOeZr7wF.htm](npc-gallery/Hle05FibgOeZr7wF.htm)|Hunter|Cazador|modificada|
|[hxyImo4ts3O0BrAY.htm](npc-gallery/hxyImo4ts3O0BrAY.htm)|Veteran Reclaimer|Veterano Recuperador|modificada|
|[IaSxoVNZFYatdfjI.htm](npc-gallery/IaSxoVNZFYatdfjI.htm)|Drunkard|Borracho|modificada|
|[ImdKLPgazv4MSI0F.htm](npc-gallery/ImdKLPgazv4MSI0F.htm)|Barrister|Abogado|modificada|
|[IQJT1Bg9FhvHHEap.htm](npc-gallery/IQJT1Bg9FhvHHEap.htm)|Torchbearer|Torchbearer|modificada|
|[Jg9OEmo68KC91PgC.htm](npc-gallery/Jg9OEmo68KC91PgC.htm)|Teacher|Profesor|modificada|
|[JsTI2SEZdg2j03gf.htm](npc-gallery/JsTI2SEZdg2j03gf.htm)|Beggar|Mendigo|modificada|
|[K2STan8izudm9eEn.htm](npc-gallery/K2STan8izudm9eEn.htm)|Antipaladin|Antipaladín|modificada|
|[K8mtLJ5jgxfqxTCv.htm](npc-gallery/K8mtLJ5jgxfqxTCv.htm)|Harrow Reader|Harrow Reader|modificada|
|[kmHYc2fvhd4QsUEV.htm](npc-gallery/kmHYc2fvhd4QsUEV.htm)|Necromancer|Nigromante|modificada|
|[KPUDfkVpemug2gKj.htm](npc-gallery/KPUDfkVpemug2gKj.htm)|Bandit|Bandido|modificada|
|[KUDsYCHduF0JE3yf.htm](npc-gallery/KUDsYCHduF0JE3yf.htm)|Ship Captain|Capitán de barco|modificada|
|[KvcFqH6H4TFCuBZA.htm](npc-gallery/KvcFqH6H4TFCuBZA.htm)|Azarketi Crab Catcher|Azarketi Crab Catcher|modificada|
|[ldaY3QcPczuFoqBC.htm](npc-gallery/ldaY3QcPczuFoqBC.htm)|Plague Doctor|Médico de la peste|modificada|
|[lemVxzg2Pnx9Nu3d.htm](npc-gallery/lemVxzg2Pnx9Nu3d.htm)|Troubadour|Trovador|modificada|
|[lfXQECIiN0zZdf95.htm](npc-gallery/lfXQECIiN0zZdf95.htm)|Dancer|Bailarina|modificada|
|[lTcX8tk6JjQBFcq1.htm](npc-gallery/lTcX8tk6JjQBFcq1.htm)|Zealot of Asmodeus|Zealot of Asmodeus|modificada|
|[M2Vi2mkwMZv1ZRka.htm](npc-gallery/M2Vi2mkwMZv1ZRka.htm)|Tempest-Sun Mage|Tempest-Sun Mage|modificada|
|[mblLgQ9NMR2mMI99.htm](npc-gallery/mblLgQ9NMR2mMI99.htm)|Reckless Scientist|Científico Temerario|modificada|
|[mJxgYD8TQg1W2oXC.htm](npc-gallery/mJxgYD8TQg1W2oXC.htm)|Hellknight Paravicar|Hellknight Paravicar|modificada|
|[ny37LcdsPLY9Osby.htm](npc-gallery/ny37LcdsPLY9Osby.htm)|Saboteur|Saboteador|modificada|
|[o4XTf77fEEoFVTdA.htm](npc-gallery/o4XTf77fEEoFVTdA.htm)|Pathfinder Venture-Captain|Pathfinder Venture-Capitán|modificada|
|[o9dAbSVn4Vi4ejjc.htm](npc-gallery/o9dAbSVn4Vi4ejjc.htm)|Smith|Smith|modificada|
|[OSCpJYTr6xNIxqZi.htm](npc-gallery/OSCpJYTr6xNIxqZi.htm)|Server|Servidor|modificada|
|[p94aKz7KsiAQJscm.htm](npc-gallery/p94aKz7KsiAQJscm.htm)|Prisoner|Preso|modificada|
|[PLOfWPKwB7pE4arv.htm](npc-gallery/PLOfWPKwB7pE4arv.htm)|Innkeeper|Posadero|modificada|
|[pMKrTXmrzDOc9avN.htm](npc-gallery/pMKrTXmrzDOc9avN.htm)|Cult Leader|Líder de culto|modificada|
|[PoIuzIWFnlAQ8pdH.htm](npc-gallery/PoIuzIWFnlAQ8pdH.htm)|Beast Tamer|Domador de bestias|modificada|
|[pZOgcQRwXrX9g0s8.htm](npc-gallery/pZOgcQRwXrX9g0s8.htm)|Guildmaster|Guildmaster|modificada|
|[QAodADCKmbkf53CE.htm](npc-gallery/QAodADCKmbkf53CE.htm)|Librarian|Bibliotecario|modificada|
|[QZmckb7O3PNgY7D6.htm](npc-gallery/QZmckb7O3PNgY7D6.htm)|Dockhand|Dockhand|modificada|
|[R5SWtNtQt8l7WLYk.htm](npc-gallery/R5SWtNtQt8l7WLYk.htm)|Noble|Noble|modificada|
|[rsATu823vatRe7QJ.htm](npc-gallery/rsATu823vatRe7QJ.htm)|Guard|Guardia|modificada|
|[saUg5rtaO9kI9Vir.htm](npc-gallery/saUg5rtaO9kI9Vir.htm)|Harbormaster|Capitán de puerto|modificada|
|[sKSfQmJMEsj8QN12.htm](npc-gallery/sKSfQmJMEsj8QN12.htm)|Adept|Adepto|modificada|
|[SwjcZsbkcq6PhiXc.htm](npc-gallery/SwjcZsbkcq6PhiXc.htm)|Spy|Espía|modificada|
|[sZ9RwN8zIzpztW3N.htm](npc-gallery/sZ9RwN8zIzpztW3N.htm)|Azarketi Tide Tamer|Azarketi Tide Tamer|modificada|
|[t7QwdZ2m7AbuRWqd.htm](npc-gallery/t7QwdZ2m7AbuRWqd.htm)|Captain Of The Guard|Capitán De La Guardia|modificada|
|[tbiThWX0gAVZAMor.htm](npc-gallery/tbiThWX0gAVZAMor.htm)|Judge|Juez|modificada|
|[TCzxsJQjUpy02CsJ.htm](npc-gallery/TCzxsJQjUpy02CsJ.htm)|Tracker|Rastreador|modificada|
|[TgeUj2IiyoTeZHIO.htm](npc-gallery/TgeUj2IiyoTeZHIO.htm)|Watch Officer|Vigilante|modificada|
|[Tj03FbN4SSr0o953.htm](npc-gallery/Tj03FbN4SSr0o953.htm)|Acrobat|Acróbata|modificada|
|[u3tXaX3sOtCvuHW3.htm](npc-gallery/u3tXaX3sOtCvuHW3.htm)|Farmer|Granjero|modificada|
|[UuPPceVcGk1RwSbB.htm](npc-gallery/UuPPceVcGk1RwSbB.htm)|Hellknight Armiger|Hellknight Armiger|modificada|
|[VkG5yl9xcmziwpQD.htm](npc-gallery/VkG5yl9xcmziwpQD.htm)|Pathfinder Field Agent|Pathfinder Field Agent|modificada|
|[vkLhqX5oR1t89puZ.htm](npc-gallery/vkLhqX5oR1t89puZ.htm)|Gang Leader|Líder de la banda|modificada|
|[w4VJ6h4mysbpdoN4.htm](npc-gallery/w4VJ6h4mysbpdoN4.htm)|Advisor|Asesor|modificada|
|[W9lhKuDeS670LzLx.htm](npc-gallery/W9lhKuDeS670LzLx.htm)|Prophet|Profeta|modificada|
|[wfsT2QDtQhsFXQfE.htm](npc-gallery/wfsT2QDtQhsFXQfE.htm)|Surgeon|Cirujano|modificada|
|[WTCFE1BYdZGWJHh7.htm](npc-gallery/WTCFE1BYdZGWJHh7.htm)|Gravedigger|Sepulturero|modificada|
|[X1cSs1jhTtx1zTI4.htm](npc-gallery/X1cSs1jhTtx1zTI4.htm)|Poacher|Poacher|modificada|
|[X7LmMMEOFUUicQ2O.htm](npc-gallery/X7LmMMEOFUUicQ2O.htm)|Guide|Guía|modificada|
|[XpkGaDlyMH2V5wxR.htm](npc-gallery/XpkGaDlyMH2V5wxR.htm)|Priest of Pharasma|Sacerdote de Pharasma|modificada|
|[xY2WjwebqTNXAP0q.htm](npc-gallery/xY2WjwebqTNXAP0q.htm)|Commoner|Común|modificada|
|[Za701s0CV37YPOyo.htm](npc-gallery/Za701s0CV37YPOyo.htm)|Executioner|Verdugo|modificada|
|[Zd0K8TOkthc4a4l7.htm](npc-gallery/Zd0K8TOkthc4a4l7.htm)|Grave Robber|Grave Robber|modificada|
|[zQfufnnLCTzQ165S.htm](npc-gallery/zQfufnnLCTzQ165S.htm)|Monster Hunter|Cazador de monstruos|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
