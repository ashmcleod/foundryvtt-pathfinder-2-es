# Estado de la traducción (pathfinder-bestiary)

 * **modificada**: 468


DÚltima actualización: 2024-02-08 11:13 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[024PqcF8yMRBrPuq.htm](pathfinder-bestiary/024PqcF8yMRBrPuq.htm)|Adult White Dragon|Dragón Blanco Adulto|modificada|
|[05E3kkjoLZVjFOeO.htm](pathfinder-bestiary/05E3kkjoLZVjFOeO.htm)|Brontosaurus|Brontosaurus|modificada|
|[05wwpHHsBlxBbdkN.htm](pathfinder-bestiary/05wwpHHsBlxBbdkN.htm)|Giant Anaconda|Anaconda gigante|modificada|
|[0FGz2eXm0SB04sJW.htm](pathfinder-bestiary/0FGz2eXm0SB04sJW.htm)|Ancient Green Dragon|Dragón Verde Antiguo|modificada|
|[0HjVFx8qIKDCfblg.htm](pathfinder-bestiary/0HjVFx8qIKDCfblg.htm)|Lantern Archon|Lantern Archon|modificada|
|[0hnnwyqLfYVIenzd.htm](pathfinder-bestiary/0hnnwyqLfYVIenzd.htm)|Deep Gnome Rockwarden|Gnomo subterráneo guardiapétreo profundo|modificada|
|[0MOWKI97bgGDf5Xi.htm](pathfinder-bestiary/0MOWKI97bgGDf5Xi.htm)|Dark Naga|Naga Oscura|modificada|
|[0plBflWwrCWQO2RO.htm](pathfinder-bestiary/0plBflWwrCWQO2RO.htm)|Zombie Shambler|Zombi tambaleante|modificada|
|[0rfropeocJWXC6pg.htm](pathfinder-bestiary/0rfropeocJWXC6pg.htm)|Goblin Dog|Goblin Dog|modificada|
|[0SAlss24nUMdX9r8.htm](pathfinder-bestiary/0SAlss24nUMdX9r8.htm)|Gancanagh|Gancanagh|modificada|
|[0SJqmk4ItwL31Rg9.htm](pathfinder-bestiary/0SJqmk4ItwL31Rg9.htm)|Dullahan|Dullahan|modificada|
|[1CzZINpYRcNBKDnO.htm](pathfinder-bestiary/1CzZINpYRcNBKDnO.htm)|Lizardfolk Scout|Explorador Lizardfolk|modificada|
|[2GRPw4VK6zfCS2Qw.htm](pathfinder-bestiary/2GRPw4VK6zfCS2Qw.htm)|Homunculus|Homunculus|modificada|
|[2H2AEwQnfKJC0nrd.htm](pathfinder-bestiary/2H2AEwQnfKJC0nrd.htm)|Ghost Commoner|Común fantasma de plebeyo|modificada|
|[2HvXtedQziTTfI0S.htm](pathfinder-bestiary/2HvXtedQziTTfI0S.htm)|Roc|Roc|modificada|
|[2IrbfdtWyXiGOLBA.htm](pathfinder-bestiary/2IrbfdtWyXiGOLBA.htm)|Tarn Linnorm|Tarn Linnorm|modificada|
|[2IrWQjtFvsen8ioo.htm](pathfinder-bestiary/2IrWQjtFvsen8ioo.htm)|Deinonychus|Deinonychus|modificada|
|[2T2cN4j4EzfDAIEe.htm](pathfinder-bestiary/2T2cN4j4EzfDAIEe.htm)|Ancient Red Dragon (Spellcaster)|Dragón Rojo Antiguo (Hechicero)|modificada|
|[31kXa1P0LUl38jYG.htm](pathfinder-bestiary/31kXa1P0LUl38jYG.htm)|Zaramuun|Zaramuun|modificada|
|[3kLXBdtKpUsU8ey5.htm](pathfinder-bestiary/3kLXBdtKpUsU8ey5.htm)|Satyr|Sátiro|modificada|
|[3nOBhH8j9I7ps6fC.htm](pathfinder-bestiary/3nOBhH8j9I7ps6fC.htm)|Green Hag|Maléfico verde|modificada|
|[3OPUMuBQThM5EXjz.htm](pathfinder-bestiary/3OPUMuBQThM5EXjz.htm)|Young White Dragon|Joven Dragón Blanco|modificada|
|[3VsQFEdIN5e1uWle.htm](pathfinder-bestiary/3VsQFEdIN5e1uWle.htm)|Giant Moray Eel|Morena gigante|modificada|
|[3W2PMV6IbVdouOB1.htm](pathfinder-bestiary/3W2PMV6IbVdouOB1.htm)|Young Copper Dragon|Joven Dragón de Cobre|modificada|
|[45Eo7MFWG3ShikvD.htm](pathfinder-bestiary/45Eo7MFWG3ShikvD.htm)|Kraken|Kraken|modificada|
|[45neevf5aLl0YPyk.htm](pathfinder-bestiary/45neevf5aLl0YPyk.htm)|Astral Deva|Astral Deva|modificada|
|[4B47SUq57pcl3U9u.htm](pathfinder-bestiary/4B47SUq57pcl3U9u.htm)|Legion Archon|Arconte de la Legión|modificada|
|[4BBzo72pHOpecoIp.htm](pathfinder-bestiary/4BBzo72pHOpecoIp.htm)|Goblin War Chanter|Goblin War Chanter|modificada|
|[4C3Sh2lO9oWi78Zo.htm](pathfinder-bestiary/4C3Sh2lO9oWi78Zo.htm)|Ancient Blue Dragon (Spellcaster)|Ancient Blue Dragon (Hechicero)|modificada|
|[4cPw8hZwW6uvyzvh.htm](pathfinder-bestiary/4cPw8hZwW6uvyzvh.htm)|Vrock|Vrock|modificada|
|[4h9jhODg2NwiYsPg.htm](pathfinder-bestiary/4h9jhODg2NwiYsPg.htm)|Purple Worm|Gusano Púrpura|modificada|
|[4htFfofrXLkbWMRg.htm](pathfinder-bestiary/4htFfofrXLkbWMRg.htm)|Bugbear Thug|Bugbear Thug|modificada|
|[4MwjCsa5O9aAjxSm.htm](pathfinder-bestiary/4MwjCsa5O9aAjxSm.htm)|Boar|Jabalí|modificada|
|[4npwV3fuBm3sBCPG.htm](pathfinder-bestiary/4npwV3fuBm3sBCPG.htm)|Xorn|Xorn|modificada|
|[4p07SH4zdmVZ405I.htm](pathfinder-bestiary/4p07SH4zdmVZ405I.htm)|Graveknight|Graveknight|modificada|
|[4zXn6xaaxo1DtIRk.htm](pathfinder-bestiary/4zXn6xaaxo1DtIRk.htm)|Xulgath Skulker|Xulgath Skulker|modificada|
|[5Azg87M6OnQ7Q4ZS.htm](pathfinder-bestiary/5Azg87M6OnQ7Q4ZS.htm)|Riding Dog|Perro de Montar|modificada|
|[5H8ZX7y5IkUBhvhF.htm](pathfinder-bestiary/5H8ZX7y5IkUBhvhF.htm)|Skeleton Guard|Guardia esqueleto|modificada|
|[5hQk5NJk4L10txyW.htm](pathfinder-bestiary/5hQk5NJk4L10txyW.htm)|Hobgoblin General|Hobgoblin General|modificada|
|[5MVBU86ZRw2ANMQn.htm](pathfinder-bestiary/5MVBU86ZRw2ANMQn.htm)|Skeletal Giant|Gigante esquelético|modificada|
|[5pk6bfodgnllSIOy.htm](pathfinder-bestiary/5pk6bfodgnllSIOy.htm)|Giant Flytrap|Atrapamoscas Gigante|modificada|
|[5U13zQ77DIcqpH9U.htm](pathfinder-bestiary/5U13zQ77DIcqpH9U.htm)|Werebear|Werebear|modificada|
|[5XtCOknYJuEl4SfD.htm](pathfinder-bestiary/5XtCOknYJuEl4SfD.htm)|Young Green Dragon (Spellcaster)|Joven Dragón Verde (Hechicero)|modificada|
|[6CQEelygt968CB7m.htm](pathfinder-bestiary/6CQEelygt968CB7m.htm)|Ankylosaurus|Ankylosaurus|modificada|
|[6eabIbxzYepfZAHX.htm](pathfinder-bestiary/6eabIbxzYepfZAHX.htm)|Ice Linnorm|Ice Linnorm|modificada|
|[6UczvqBDlGNXcIlW.htm](pathfinder-bestiary/6UczvqBDlGNXcIlW.htm)|Tidal Master|Ser de la marea|modificada|
|[6XlGTt3RveX49YbC.htm](pathfinder-bestiary/6XlGTt3RveX49YbC.htm)|Shambler|Broza movediza|modificada|
|[70JDH25JLTC4t5Ko.htm](pathfinder-bestiary/70JDH25JLTC4t5Ko.htm)|Tengu Sneak|Movimiento furtivo Tengu|modificada|
|[7EpLGBoddL8Vzlha.htm](pathfinder-bestiary/7EpLGBoddL8Vzlha.htm)|Soulbound Doll (Chaotic Evil)|Muca de alma vinculada (maligno caótico).|modificada|
|[7JvA7kTqCUwcJoNe.htm](pathfinder-bestiary/7JvA7kTqCUwcJoNe.htm)|Bugbear Tormentor|Bugbear Tormentor|modificada|
|[7ptWlh4t6oHvYOcj.htm](pathfinder-bestiary/7ptWlh4t6oHvYOcj.htm)|Soulbound Doll (Neutral Evil)|Muca de alma vinculada (maligno neutro).|modificada|
|[7ZgQuis8r8YQyUnI.htm](pathfinder-bestiary/7ZgQuis8r8YQyUnI.htm)|Yeti|Yeti|modificada|
|[80TiZrVvIBW7E6L2.htm](pathfinder-bestiary/80TiZrVvIBW7E6L2.htm)|Fire Giant|Gigante de fuego|modificada|
|[8BloAdRqlLpt5bNg.htm](pathfinder-bestiary/8BloAdRqlLpt5bNg.htm)|Treerazer|Treerazer|modificada|
|[8JvzSTwQWtOsxRfL.htm](pathfinder-bestiary/8JvzSTwQWtOsxRfL.htm)|Quetzalcoatlus|Quetzalcoatlus|modificada|
|[8meqlz36gPHTTvNz.htm](pathfinder-bestiary/8meqlz36gPHTTvNz.htm)|Voidworm|Voidworm|modificada|
|[8r8Ar08ojdJuPeiH.htm](pathfinder-bestiary/8r8Ar08ojdJuPeiH.htm)|Eagle|Águila|modificada|
|[8U8K0YEghIErml35.htm](pathfinder-bestiary/8U8K0YEghIErml35.htm)|Crimson Worm|Gusano Carmesí|modificada|
|[8uXLbKKzxN5O0ZhM.htm](pathfinder-bestiary/8uXLbKKzxN5O0ZhM.htm)|Elananx|Elananx|modificada|
|[93hZtLl9pRRfqI05.htm](pathfinder-bestiary/93hZtLl9pRRfqI05.htm)|Blue Kobold Dragon Mage|Mago dracónico kobold azul|modificada|
|[9AlfVoEMLwDODjxl.htm](pathfinder-bestiary/9AlfVoEMLwDODjxl.htm)|Veiled Master|Amo velado|modificada|
|[9FZMzpAu4XhCI0IB.htm](pathfinder-bestiary/9FZMzpAu4XhCI0IB.htm)|Deep Gnome Scout|Explorador gnomo subterráneo|modificada|
|[9H11QHnoKz8hHKSM.htm](pathfinder-bestiary/9H11QHnoKz8hHKSM.htm)|Ancient Red Dragon|Dragón Rojo Antiguo|modificada|
|[9hOuoOONmp6500GZ.htm](pathfinder-bestiary/9hOuoOONmp6500GZ.htm)|Giant Gecko|Gecko gigante|modificada|
|[9jF564DF6ylEovna.htm](pathfinder-bestiary/9jF564DF6ylEovna.htm)|Quasit|Quasit|modificada|
|[9llfviiJg5bJlBth.htm](pathfinder-bestiary/9llfviiJg5bJlBth.htm)|Mukradi|Mukradi|modificada|
|[9qjXP1Lho1UmAihJ.htm](pathfinder-bestiary/9qjXP1Lho1UmAihJ.htm)|Axiomite|Axiomita|modificada|
|[9sa2KE4Fbh3OPH7M.htm](pathfinder-bestiary/9sa2KE4Fbh3OPH7M.htm)|Brine Shark|Tiburón de salmuera|modificada|
|[9vNYtJZiseCEf4wt.htm](pathfinder-bestiary/9vNYtJZiseCEf4wt.htm)|Balor|Balor|modificada|
|[9X7hOvCKy1bqw0g6.htm](pathfinder-bestiary/9X7hOvCKy1bqw0g6.htm)|Giant Tarantula|Tarántula gigante|modificada|
|[9xdl2PpfMEfJWhSO.htm](pathfinder-bestiary/9xdl2PpfMEfJWhSO.htm)|Young Gold Dragon (Spellcaster)|Joven Dragón Dorado (Hechicero)|modificada|
|[aAiuQvMGPN9QXwKY.htm](pathfinder-bestiary/aAiuQvMGPN9QXwKY.htm)|Adult Gold Dragon|Dragón de Oro Adulto|modificada|
|[aD76W2uEQhFFUrom.htm](pathfinder-bestiary/aD76W2uEQhFFUrom.htm)|Chuul|Chuul|modificada|
|[AdQVjlOWB6rmBRVp.htm](pathfinder-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger|Doppelganger|modificada|
|[aeCoh4u6c5kt1iCs.htm](pathfinder-bestiary/aeCoh4u6c5kt1iCs.htm)|Gargoyle|Gárgola|modificada|
|[AiPXegCJ1leUslTm.htm](pathfinder-bestiary/AiPXegCJ1leUslTm.htm)|Succubus|Súcubo|modificada|
|[aNWiP985fISjClGo.htm](pathfinder-bestiary/aNWiP985fISjClGo.htm)|Arboreal Warden|Custodio arboral (pl. -es)|modificada|
|[aq2H1lRALUNMEGRG.htm](pathfinder-bestiary/aq2H1lRALUNMEGRG.htm)|Guardian Naga|Guardián Naga|modificada|
|[aqW5sp9HPgZgpXxs.htm](pathfinder-bestiary/aqW5sp9HPgZgpXxs.htm)|Hill Giant|Gigante de la colina|modificada|
|[AQwxOoYBwDUAwvfh.htm](pathfinder-bestiary/AQwxOoYBwDUAwvfh.htm)|Adult Black Dragon (Spellcaster)|Dragón Negro Adulto (Hechicero)|modificada|
|[ar0tszs8NAgQamHX.htm](pathfinder-bestiary/ar0tszs8NAgQamHX.htm)|Young Blue Dragon|Joven Dragón Azul|modificada|
|[ASc5sT2hU84tp1fx.htm](pathfinder-bestiary/ASc5sT2hU84tp1fx.htm)|Simurgh|Simurgh|modificada|
|[atrhmCtNKx1MR06I.htm](pathfinder-bestiary/atrhmCtNKx1MR06I.htm)|Faerie Dragon|Faerie Dragon|modificada|
|[AuCC04X2AO8oFN75.htm](pathfinder-bestiary/AuCC04X2AO8oFN75.htm)|Harpy|Arpía|modificada|
|[AYwdybUfm4meGUTJ.htm](pathfinder-bestiary/AYwdybUfm4meGUTJ.htm)|Giant Rat|Rata gigante|modificada|
|[B0QyZEjAXUG5TsJU.htm](pathfinder-bestiary/B0QyZEjAXUG5TsJU.htm)|Storm Giant|Gigante de la tormenta|modificada|
|[b6qiHvyx6ymROTBL.htm](pathfinder-bestiary/b6qiHvyx6ymROTBL.htm)|Deep Gnome Warrior|Gnomo subterráneo combatiente|modificada|
|[B7b0alybm5U34nFV.htm](pathfinder-bestiary/B7b0alybm5U34nFV.htm)|Nessian Warhound|Nessian Warhound|modificada|
|[B7eLG2k7qUo8HU6O.htm](pathfinder-bestiary/B7eLG2k7qUo8HU6O.htm)|Drider|Drider|modificada|
|[b8NQkby4QV4uOqFT.htm](pathfinder-bestiary/b8NQkby4QV4uOqFT.htm)|Cave Bear|Cave Bear|modificada|
|[B8QjalVNcWjuqgG7.htm](pathfinder-bestiary/B8QjalVNcWjuqgG7.htm)|Phistophilus|Phistophilus|modificada|
|[B9KJUdZre51J3E3e.htm](pathfinder-bestiary/B9KJUdZre51J3E3e.htm)|Ancient Copper Dragon|Dragón antiguo de cobre|modificada|
|[BAD7npndaooB3Pz1.htm](pathfinder-bestiary/BAD7npndaooB3Pz1.htm)|Redcap|Redcap|modificada|
|[bAjHCeyNcPRqOmLv.htm](pathfinder-bestiary/bAjHCeyNcPRqOmLv.htm)|Changeling Exile|Changeling Exile|modificada|
|[BeptBpCJ4Ny4biOH.htm](pathfinder-bestiary/BeptBpCJ4Ny4biOH.htm)|Leaf Leshy|Leshy de las hojas|modificada|
|[bIw7czN0E3rENrVd.htm](pathfinder-bestiary/bIw7czN0E3rENrVd.htm)|Giant Stag Beetle|Escarabajo Ciervo Gigante|modificada|
|[bIXfNKFWduf8MH0f.htm](pathfinder-bestiary/bIXfNKFWduf8MH0f.htm)|Ogre Boss|Ogro Jefe|modificada|
|[bjJUZKcA47Qp0ZwL.htm](pathfinder-bestiary/bjJUZKcA47Qp0ZwL.htm)|Pleroma|Pleroma|modificada|
|[bkaDwBD3mIBgvULs.htm](pathfinder-bestiary/bkaDwBD3mIBgvULs.htm)|Cloaker|Cloaker|modificada|
|[BKPRkJgq7ehsW7uX.htm](pathfinder-bestiary/BKPRkJgq7ehsW7uX.htm)|Giant Centipede|Ciempiés gigante|modificada|
|[BLFEu9jCKPAMko01.htm](pathfinder-bestiary/BLFEu9jCKPAMko01.htm)|Living Waterfall|Catarata viviente|modificada|
|[bLMoqt9xqTZKnjxr.htm](pathfinder-bestiary/bLMoqt9xqTZKnjxr.htm)|Werewolf|Hombre lobo|modificada|
|[Br1AtKUHe3nbzjnY.htm](pathfinder-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimic|Mimic|modificada|
|[BRo0snV2sH6TFuh6.htm](pathfinder-bestiary/BRo0snV2sH6TFuh6.htm)|Glabrezu|Glabrezu|modificada|
|[bSjF2lCgchtp2ocS.htm](pathfinder-bestiary/bSjF2lCgchtp2ocS.htm)|Ancient Brass Dragon|Antiguo Dragón de Latón|modificada|
|[bxAJWWKrEMjgNkUp.htm](pathfinder-bestiary/bxAJWWKrEMjgNkUp.htm)|Dero Stalker|Dero Stalker|modificada|
|[BzBFDaNj51PP97RZ.htm](pathfinder-bestiary/BzBFDaNj51PP97RZ.htm)|Ancient Bronze Dragon|Antiguo Dragón de Bronce|modificada|
|[c0zqasudrwZU3fdw.htm](pathfinder-bestiary/c0zqasudrwZU3fdw.htm)|Efreeti|Efreeti|modificada|
|[c3iA9lkU1QY4YCY6.htm](pathfinder-bestiary/c3iA9lkU1QY4YCY6.htm)|Unicorn|Unicornio|modificada|
|[c6AE2Mh8BRtBgbtz.htm](pathfinder-bestiary/c6AE2Mh8BRtBgbtz.htm)|Arboreal Regent|Regente arboral (pl. -es)|modificada|
|[C9s5tBxVValC2HTE.htm](pathfinder-bestiary/C9s5tBxVValC2HTE.htm)|Giant Bat|Murciélago gigante|modificada|
|[cBHpMcVaLRPZu9po.htm](pathfinder-bestiary/cBHpMcVaLRPZu9po.htm)|Zephyr Hawk|Halcón de céfiro|modificada|
|[cDgOfBCrWcpYwRVS.htm](pathfinder-bestiary/cDgOfBCrWcpYwRVS.htm)|Sprite|Sprite|modificada|
|[cDm6PzhO5nXlkGoi.htm](pathfinder-bestiary/cDm6PzhO5nXlkGoi.htm)|Orc Brute|Orc Brute|modificada|
|[CFHLgMj8zHLqcagc.htm](pathfinder-bestiary/CFHLgMj8zHLqcagc.htm)|Sea Serpent|Serpiente marina|modificada|
|[CHIh3vMssFixUlw8.htm](pathfinder-bestiary/CHIh3vMssFixUlw8.htm)|Salamander|Salamandra|modificada|
|[CJP3GGBXuGgkaj6C.htm](pathfinder-bestiary/CJP3GGBXuGgkaj6C.htm)|Grizzly Bear|Grizzly Bear|modificada|
|[CJuHwIRCAgTB1SEl.htm](pathfinder-bestiary/CJuHwIRCAgTB1SEl.htm)|Red Kobold Dragon Mage|Mago dracónico kobold rojo|modificada|
|[Cq8sRhVVF0hagBu6.htm](pathfinder-bestiary/Cq8sRhVVF0hagBu6.htm)|Vampire Count|Conde Vampiro|modificada|
|[CSefkWGVmA5yGxNR.htm](pathfinder-bestiary/CSefkWGVmA5yGxNR.htm)|Pteranodon|Pteranodon|modificada|
|[csfVqep68nYGHExr.htm](pathfinder-bestiary/csfVqep68nYGHExr.htm)|Young White Dragon (Spellcaster)|Joven Dragón Blanco (Hechicero)|modificada|
|[csRH8Fx0r6iMWlFc.htm](pathfinder-bestiary/csRH8Fx0r6iMWlFc.htm)|Ether Spider|Araéa de éter|modificada|
|[cuET2PHGcE7eL7DJ.htm](pathfinder-bestiary/cuET2PHGcE7eL7DJ.htm)|Marid|Marid|modificada|
|[cXSTuUJct5iepH75.htm](pathfinder-bestiary/cXSTuUJct5iepH75.htm)|Pridespawn|Pridespawn|modificada|
|[cZDiyluplFqRxmGy.htm](pathfinder-bestiary/cZDiyluplFqRxmGy.htm)|Animated Armor|Armadura animada|modificada|
|[cZsaAKlEYWZUO1CV.htm](pathfinder-bestiary/cZsaAKlEYWZUO1CV.htm)|Terotricus|Terotricus|modificada|
|[CzxQpB3p0d9hwPeR.htm](pathfinder-bestiary/CzxQpB3p0d9hwPeR.htm)|Bulette|Bulette|modificada|
|[d9W89Yv6zyvfxZuG.htm](pathfinder-bestiary/d9W89Yv6zyvfxZuG.htm)|Lillend|Lillend|modificada|
|[dEAneTvoPuQXZoLR.htm](pathfinder-bestiary/dEAneTvoPuQXZoLR.htm)|Gibbering Mouther|Gibbering Mouther|modificada|
|[dEecX0AEfl32KUVN.htm](pathfinder-bestiary/dEecX0AEfl32KUVN.htm)|Duergar Taskmaster|Duergar Taskmaster|modificada|
|[DlRe4c2XlBSpwmms.htm](pathfinder-bestiary/DlRe4c2XlBSpwmms.htm)|Greater Barghest|Mayor Barghest|modificada|
|[DolNTN9s2p79N8Cy.htm](pathfinder-bestiary/DolNTN9s2p79N8Cy.htm)|Young Gold Dragon|Joven Dragón Dorado|modificada|
|[Dp6dq9Jd0TanufdU.htm](pathfinder-bestiary/Dp6dq9Jd0TanufdU.htm)|Adult Blue Dragon (Spellcaster)|Dragón Azul Adulto (Hechicero)|modificada|
|[DPEmRRXYevk3ADqW.htm](pathfinder-bestiary/DPEmRRXYevk3ADqW.htm)|Morrigna|Morrigna|modificada|
|[dTz1SQvJIUsB9S2w.htm](pathfinder-bestiary/dTz1SQvJIUsB9S2w.htm)|Drow Fighter|Drow Fighter|modificada|
|[Dwgl1DzJAYE3ienu.htm](pathfinder-bestiary/Dwgl1DzJAYE3ienu.htm)|Cyclops|Cyclops|modificada|
|[DX1xNtucLTenn3P3.htm](pathfinder-bestiary/DX1xNtucLTenn3P3.htm)|River Drake|Draco fluvial|modificada|
|[dYbfZnB1ngiqPm4O.htm](pathfinder-bestiary/dYbfZnB1ngiqPm4O.htm)|Young Silver Dragon (Spellcaster)|Joven Dragón Plateado (Hechicero)|modificada|
|[E0LCMHVp4sxAbQYa.htm](pathfinder-bestiary/E0LCMHVp4sxAbQYa.htm)|Mitflit|Mitflit|modificada|
|[E0PIGtVfc5PFVT2C.htm](pathfinder-bestiary/E0PIGtVfc5PFVT2C.htm)|Cacodaemon|Cacodaemon|modificada|
|[E4ctF7Fvi3cdkgQq.htm](pathfinder-bestiary/E4ctF7Fvi3cdkgQq.htm)|Hunting Spider|Araña cazadora|modificada|
|[E5RDV3n7GnjAspQ5.htm](pathfinder-bestiary/E5RDV3n7GnjAspQ5.htm)|Mummy Guardian|Guardián de la momia|modificada|
|[e8rmI5xt6IANatfX.htm](pathfinder-bestiary/e8rmI5xt6IANatfX.htm)|Shemhazian|Shemhazian|modificada|
|[E9rT02pPDLq7rARq.htm](pathfinder-bestiary/E9rT02pPDLq7rARq.htm)|Minotaur|Minotauro|modificada|
|[EhB5Q98OO25DDOOl.htm](pathfinder-bestiary/EhB5Q98OO25DDOOl.htm)|Lizardfolk Defender|Lizardfolk Defender|modificada|
|[eHLDsL1LG3jQ1H6Y.htm](pathfinder-bestiary/eHLDsL1LG3jQ1H6Y.htm)|Slurk|Slurk|modificada|
|[EibxkD9y30YmPaLH.htm](pathfinder-bestiary/EibxkD9y30YmPaLH.htm)|Lich|Lich|modificada|
|[eP96NzLFSjua4NS5.htm](pathfinder-bestiary/eP96NzLFSjua4NS5.htm)|Stegosaurus|Stegosaurus|modificada|
|[ePa0KmNPpR4zUPfX.htm](pathfinder-bestiary/ePa0KmNPpR4zUPfX.htm)|Dero Magister|Dero Magister|modificada|
|[eQdLBzkluS1fvVC8.htm](pathfinder-bestiary/eQdLBzkluS1fvVC8.htm)|Caligni Dancer|Caligni bailarín|modificada|
|[ETNlci6VnSthx5V6.htm](pathfinder-bestiary/ETNlci6VnSthx5V6.htm)|Erinys|Erinys|modificada|
|[ETwmjdnmSkqGdD5r.htm](pathfinder-bestiary/ETwmjdnmSkqGdD5r.htm)|Animated Broom|Escoba animada|modificada|
|[Ey19J4nTn1dQvLtE.htm](pathfinder-bestiary/Ey19J4nTn1dQvLtE.htm)|Snapping Flytrap|Atrapamoscas mordedor|modificada|
|[f3c1CS2W8Tft3hW7.htm](pathfinder-bestiary/f3c1CS2W8Tft3hW7.htm)|Sea Hag|Hechicera de mar|modificada|
|[F99r7jcWX5YZB8xw.htm](pathfinder-bestiary/F99r7jcWX5YZB8xw.htm)|Adult Brass Dragon (Spellcaster)|Dragón de latón adulto (Hechicero).|modificada|
|[Fa1S0A8fAx3SkO9h.htm](pathfinder-bestiary/Fa1S0A8fAx3SkO9h.htm)|Brain Collector|Recolector de cerebros|modificada|
|[FaBHkmFGuEIqIYM1.htm](pathfinder-bestiary/FaBHkmFGuEIqIYM1.htm)|Drow Priestess|Sacerdotisa Drow|modificada|
|[fcE9PgnC82e9haL5.htm](pathfinder-bestiary/fcE9PgnC82e9haL5.htm)|Young Brass Dragon (Spellcaster)|Joven dragón de latón (Hechicero)|modificada|
|[fd3Bf10lloqMg2jW.htm](pathfinder-bestiary/fd3Bf10lloqMg2jW.htm)|Young Blue Dragon (Spellcaster)|Joven Dragón Azul (Hechicero)|modificada|
|[fgsDAeZHVbHRhSE8.htm](pathfinder-bestiary/fgsDAeZHVbHRhSE8.htm)|Cockatrice|Cockatrice|modificada|
|[FHfrIJCdKTzy2rrR.htm](pathfinder-bestiary/FHfrIJCdKTzy2rrR.htm)|Cauthooj|Cauthooj (s. y pl.)|modificada|
|[FIoRPHaHdYUPVKdT.htm](pathfinder-bestiary/FIoRPHaHdYUPVKdT.htm)|Barbazu|Barbazu|modificada|
|[FJo8VkrM7kLkHa5D.htm](pathfinder-bestiary/FJo8VkrM7kLkHa5D.htm)|Zombie Hulk|Zombie Hulk|modificada|
|[fkBcMpr3Yxxfvz9v.htm](pathfinder-bestiary/fkBcMpr3Yxxfvz9v.htm)|Astradaemon|Astradaemon|modificada|
|[FPKoiMXENk5FouXp.htm](pathfinder-bestiary/FPKoiMXENk5FouXp.htm)|Elemental Hurricane|Huracán elemental|modificada|
|[fsdCVADRbLxpBVT8.htm](pathfinder-bestiary/fsdCVADRbLxpBVT8.htm)|Young Bronze Dragon (Spellcaster)|Joven Dragón de Bronce (Hechicero)|modificada|
|[FwH05kDUlC8CwWTU.htm](pathfinder-bestiary/FwH05kDUlC8CwWTU.htm)|Troll King|Troll King|modificada|
|[fxYMucI5b2IUoBpw.htm](pathfinder-bestiary/fxYMucI5b2IUoBpw.htm)|Shuln|Shuln|modificada|
|[Fzn4jHusVeyytgfx.htm](pathfinder-bestiary/Fzn4jHusVeyytgfx.htm)|Adult Copper Dragon|Dragón de Cobre Adulto|modificada|
|[gDMPUL0UiOHrUUd3.htm](pathfinder-bestiary/gDMPUL0UiOHrUUd3.htm)|Aasimar Redeemer|Aasimar Redentor|modificada|
|[gdXok08bITkhowDJ.htm](pathfinder-bestiary/gdXok08bITkhowDJ.htm)|Ogre Warrior|Ogro combatiente|modificada|
|[gfRXFd24U633OC9r.htm](pathfinder-bestiary/gfRXFd24U633OC9r.htm)|Jinkin|Jinkin|modificada|
|[gioxLqV8N4p9iIAh.htm](pathfinder-bestiary/gioxLqV8N4p9iIAh.htm)|Phoenix|Phoenix|modificada|
|[GssFAdolUA3ghg2e.htm](pathfinder-bestiary/GssFAdolUA3ghg2e.htm)|Megaprimatus|Megaprimatus|modificada|
|[gvCCATlH9mPGWbsp.htm](pathfinder-bestiary/gvCCATlH9mPGWbsp.htm)|Troll|Troll|modificada|
|[gWxpeqOQ54Jd4HTG.htm](pathfinder-bestiary/gWxpeqOQ54Jd4HTG.htm)|Caligni Stalker|Caligni acechador (p. -s)|modificada|
|[gX66KyBxUOvMv5Sf.htm](pathfinder-bestiary/gX66KyBxUOvMv5Sf.htm)|Dragon Turtle|Tortuga Dragón|modificada|
|[GyigGu36XLPV72nW.htm](pathfinder-bestiary/GyigGu36XLPV72nW.htm)|Djinni|Djinni|modificada|
|[gztjujplfVCBVXv0.htm](pathfinder-bestiary/gztjujplfVCBVXv0.htm)|Lantern Archon (Gestalt)|Lantern Archon (Gestalt)|modificada|
|[H2ZxTHZOEigpH4LK.htm](pathfinder-bestiary/H2ZxTHZOEigpH4LK.htm)|Vampire Spawn Rogue|Vampire Spawn Rogue|modificada|
|[HbROgIcU9Z9m6XuD.htm](pathfinder-bestiary/HbROgIcU9Z9m6XuD.htm)|Wraith|Espectro|modificada|
|[HeoH8hi5iieKPuJ2.htm](pathfinder-bestiary/HeoH8hi5iieKPuJ2.htm)|Deadly Mantis|Mantis letal|modificada|
|[HFbZ580RDOG6Rxz2.htm](pathfinder-bestiary/HFbZ580RDOG6Rxz2.htm)|Sea Devil Baron|Sea Devil Baron|modificada|
|[hiGwRWdxAsoCII4f.htm](pathfinder-bestiary/hiGwRWdxAsoCII4f.htm)|Cinder Rat|Rata de ceniza|modificada|
|[Hkq9ZS2J2iKnT7vT.htm](pathfinder-bestiary/Hkq9ZS2J2iKnT7vT.htm)|Sewer Ooze|cieno de cloaca|modificada|
|[hNR4xZRsxUkGPI1v.htm](pathfinder-bestiary/hNR4xZRsxUkGPI1v.htm)|Dandasuka|Dandasuka|modificada|
|[hOXpmWmXU8N3n4Bw.htm](pathfinder-bestiary/hOXpmWmXU8N3n4Bw.htm)|Raja Rakshasa|Raja Rakshasa|modificada|
|[HpD0BTfid3hnUEWj.htm](pathfinder-bestiary/HpD0BTfid3hnUEWj.htm)|Bloodseeker|Buscasangre|modificada|
|[HpY0addhUqtHMgUN.htm](pathfinder-bestiary/HpY0addhUqtHMgUN.htm)|Elephant|Elefante|modificada|
|[hXpqjD3eBRxlemNs.htm](pathfinder-bestiary/hXpqjD3eBRxlemNs.htm)|Gnoll Cultist|Cultista Gnoll|modificada|
|[HyOf4CfAIhC3qWtz.htm](pathfinder-bestiary/HyOf4CfAIhC3qWtz.htm)|Gimmerling|Gimmerling|modificada|
|[i1HEQ6f15fMEcHQf.htm](pathfinder-bestiary/i1HEQ6f15fMEcHQf.htm)|Duergar Sharpshooter|Duergar Sharpshooter|modificada|
|[i3N3udPyTGVPLpoq.htm](pathfinder-bestiary/i3N3udPyTGVPLpoq.htm)|Adult Blue Dragon|Dragón Azul Adulto|modificada|
|[I4CpyMUsWfFYdpL5.htm](pathfinder-bestiary/I4CpyMUsWfFYdpL5.htm)|Imp|Imp|modificada|
|[I4o2Gqpr2ioiUXA9.htm](pathfinder-bestiary/I4o2Gqpr2ioiUXA9.htm)|Xulgath Leader|Líder Xulgath|modificada|
|[i6Rd1BE30hhyKxwo.htm](pathfinder-bestiary/i6Rd1BE30hhyKxwo.htm)|Flash Beetle|Flash Beetle|modificada|
|[iA9lbwH0qROTjCva.htm](pathfinder-bestiary/iA9lbwH0qROTjCva.htm)|Orc Warrior|Guerrero orco|modificada|
|[iD3YlM0QzI2SrjD6.htm](pathfinder-bestiary/iD3YlM0QzI2SrjD6.htm)|Azure Worm|Gusano azul|modificada|
|[iF5NeHC6ReqXLFmY.htm](pathfinder-bestiary/iF5NeHC6ReqXLFmY.htm)|Soulbound Doll (Neutral Good)|Muca de alma vinculada (Neutral Bueno)|modificada|
|[iII08V0HlvWGWSmu.htm](pathfinder-bestiary/iII08V0HlvWGWSmu.htm)|Horned Archon|Arconte con cuernos|modificada|
|[IMasNR02C74jy3cT.htm](pathfinder-bestiary/IMasNR02C74jy3cT.htm)|Adult Bronze Dragon|Dragón de Bronce Adulto|modificada|
|[ImueS9YFhV6sxqBP.htm](pathfinder-bestiary/ImueS9YFhV6sxqBP.htm)|Marilith|Marilith|modificada|
|[In2nNwo3JL1RXQhj.htm](pathfinder-bestiary/In2nNwo3JL1RXQhj.htm)|Riding Horse|Montar a Caballo|modificada|
|[io7johJlZinrSCiH.htm](pathfinder-bestiary/io7johJlZinrSCiH.htm)|Dryad Queen|Reina dríade|modificada|
|[IpzDMSmJ42alvf9F.htm](pathfinder-bestiary/IpzDMSmJ42alvf9F.htm)|Scorpion Swarm|Enjambre de Escorpiones|modificada|
|[IQsTNM8aXcCUmFu0.htm](pathfinder-bestiary/IQsTNM8aXcCUmFu0.htm)|Lamia|Lamia|modificada|
|[irrXrWxJ0LYSUCQB.htm](pathfinder-bestiary/irrXrWxJ0LYSUCQB.htm)|Adult Black Dragon|Dragón Negro Adulto|modificada|
|[iSwUKe7cEytclS7r.htm](pathfinder-bestiary/iSwUKe7cEytclS7r.htm)|Dryad|Dryad|modificada|
|[IUzKFRX0uHl1yxkn.htm](pathfinder-bestiary/IUzKFRX0uHl1yxkn.htm)|Greater Shadow|Sombra, mayor|modificada|
|[Iw2QccAkRc5Vnfzj.htm](pathfinder-bestiary/Iw2QccAkRc5Vnfzj.htm)|Ghaele|Ghaele|modificada|
|[Ix1PziAEk9IIMYBz.htm](pathfinder-bestiary/Ix1PziAEk9IIMYBz.htm)|Zombie Brute|Zombie Brute|modificada|
|[IXen98RbUlbxDWBD.htm](pathfinder-bestiary/IXen98RbUlbxDWBD.htm)|Gogiteth|Gogiteth|modificada|
|[J0dSbywBRgD2kf19.htm](pathfinder-bestiary/J0dSbywBRgD2kf19.htm)|Giant Wasp|Avispa gigante|modificada|
|[j0gCguJx3ILB1j9o.htm](pathfinder-bestiary/j0gCguJx3ILB1j9o.htm)|Soulbound Doll (Chaotic Good)|Muca de alma vinculada (Caótico Bueno)|modificada|
|[j1mm6bb3hZ56jSrK.htm](pathfinder-bestiary/j1mm6bb3hZ56jSrK.htm)|Grig|Grig|modificada|
|[j7NNPfZwD19BwSEZ.htm](pathfinder-bestiary/j7NNPfZwD19BwSEZ.htm)|Unseen Servant|Sirviente invisible|modificada|
|[jeAGl6OAVrrIPgu3.htm](pathfinder-bestiary/jeAGl6OAVrrIPgu3.htm)|Hell Hound|Sabueso del Infierno|modificada|
|[Jgh0WGVetNXi5jlM.htm](pathfinder-bestiary/Jgh0WGVetNXi5jlM.htm)|Quatoid|Cuatoide|modificada|
|[jGzVwekcRX5aQpbT.htm](pathfinder-bestiary/jGzVwekcRX5aQpbT.htm)|Goblin Commando|Goblin Commando|modificada|
|[JhQGNMlKARMx1n2D.htm](pathfinder-bestiary/JhQGNMlKARMx1n2D.htm)|Adult Red Dragon|Dragón Rojo Adulto|modificada|
|[JiThbhDfjUoPaTP1.htm](pathfinder-bestiary/JiThbhDfjUoPaTP1.htm)|Baomal|Baomal|modificada|
|[JkBJ8B07ElXrfDaG.htm](pathfinder-bestiary/JkBJ8B07ElXrfDaG.htm)|Boggard Warrior|Guerrero Boggard|modificada|
|[JKG3m8pFISF48Sfr.htm](pathfinder-bestiary/JKG3m8pFISF48Sfr.htm)|Adult Green Dragon (Spellcaster)|Dragón Verde Adulto (Hechicero)|modificada|
|[jlEFaUJjAK4MTsLc.htm](pathfinder-bestiary/jlEFaUJjAK4MTsLc.htm)|Ancient Silver Dragon (Spellcaster)|Ancient Silver Dragon (Hechicero)|modificada|
|[jMiiQDIDxW9ZMvCV.htm](pathfinder-bestiary/jMiiQDIDxW9ZMvCV.htm)|Dhampir Wizard|Dhampir Mago|modificada|
|[jnmUcTs4hn1c5bz9.htm](pathfinder-bestiary/jnmUcTs4hn1c5bz9.htm)|Pugwampi|Pugwampi|modificada|
|[JnOgG1xfWleFGNt9.htm](pathfinder-bestiary/JnOgG1xfWleFGNt9.htm)|Adult Brass Dragon|Dragón de latón adulto|modificada|
|[jP8CO6z7bNIhOuqQ.htm](pathfinder-bestiary/jP8CO6z7bNIhOuqQ.htm)|Ofalth|Pocilguero|modificada|
|[jVZRROs0GzDjVrgi.htm](pathfinder-bestiary/jVZRROs0GzDjVrgi.htm)|Goblin Warrior|Guerrero Goblin|modificada|
|[Jy2va0NTTbaUH1zP.htm](pathfinder-bestiary/Jy2va0NTTbaUH1zP.htm)|War Horse|War Horse|modificada|
|[k3Lt3bWBadXvlIbG.htm](pathfinder-bestiary/k3Lt3bWBadXvlIbG.htm)|Poltergeist|Poltergeist|modificada|
|[k9fM0vufbdsDPQul.htm](pathfinder-bestiary/k9fM0vufbdsDPQul.htm)|Fire Mephit|Mephit de fuego|modificada|
|[K9Hw43co8fhwmKkM.htm](pathfinder-bestiary/K9Hw43co8fhwmKkM.htm)|Grothlut|Grothlut|modificada|
|[kbZ3t5HhHRJJ4AMn.htm](pathfinder-bestiary/kbZ3t5HhHRJJ4AMn.htm)|Ancient Brass Dragon (Spellcaster)|Dragón antiguo de latón (Hechicero)|modificada|
|[KclNszYZ7sjwE9nX.htm](pathfinder-bestiary/KclNszYZ7sjwE9nX.htm)|Hobgoblin Archer|Hobgoblin Tirador|modificada|
|[kCRfBZqCugMQmdpd.htm](pathfinder-bestiary/kCRfBZqCugMQmdpd.htm)|White Kobold Dragon Mage|Mago dracónico kobold blanco|modificada|
|[KDRlxdIUADWHI6Vr.htm](pathfinder-bestiary/KDRlxdIUADWHI6Vr.htm)|Air Mephit|Aire Mephit|modificada|
|[keCgklXcy4HZgQIL.htm](pathfinder-bestiary/keCgklXcy4HZgQIL.htm)|Adamantine Golem|Golem Adamantino|modificada|
|[kfeL172Ix3x1YRc9.htm](pathfinder-bestiary/kfeL172Ix3x1YRc9.htm)|Mummy Pharaoh|Mummy Pharaoh|modificada|
|[KFf6pJmFmzj7HH9i.htm](pathfinder-bestiary/KFf6pJmFmzj7HH9i.htm)|Water Mephit|Agua Mephit|modificada|
|[kFQorgvvyozQVSKi.htm](pathfinder-bestiary/kFQorgvvyozQVSKi.htm)|Balisse|Balisse|modificada|
|[kkFVngQUGTACeggf.htm](pathfinder-bestiary/kkFVngQUGTACeggf.htm)|Medusa|Medusa|modificada|
|[kNmRn3WfiWLsuwoe.htm](pathfinder-bestiary/kNmRn3WfiWLsuwoe.htm)|Gelugon|Gelugon|modificada|
|[kPBSSOuR8n8vZHXB.htm](pathfinder-bestiary/kPBSSOuR8n8vZHXB.htm)|Otyugh|Otyugh|modificada|
|[KpxhSWRIhG7ns5NA.htm](pathfinder-bestiary/KpxhSWRIhG7ns5NA.htm)|Spider Swarm|Enjambre de arañas|modificada|
|[kR1wGS61ubIpH8A6.htm](pathfinder-bestiary/kR1wGS61ubIpH8A6.htm)|Young Black Dragon (Spellcaster)|Joven Dragón Negro (Hechicero)|modificada|
|[KsWAIXTTh3mfNWOY.htm](pathfinder-bestiary/KsWAIXTTh3mfNWOY.htm)|Giant Viper|Víbora gigante|modificada|
|[KxN9aGFGPxl6oLGF.htm](pathfinder-bestiary/KxN9aGFGPxl6oLGF.htm)|Plague Zombie|Zombie de Plaga|modificada|
|[l05LjJTXvFS4tYTE.htm](pathfinder-bestiary/l05LjJTXvFS4tYTE.htm)|Centaur|Centauro|modificada|
|[l17XDoK0UIjXUvOv.htm](pathfinder-bestiary/l17XDoK0UIjXUvOv.htm)|Cloud Giant|Gigante de las nubes|modificada|
|[l3Pe8FsFbLvft1Fq.htm](pathfinder-bestiary/l3Pe8FsFbLvft1Fq.htm)|Lion|León|modificada|
|[LDQpLwN40OGefZD0.htm](pathfinder-bestiary/LDQpLwN40OGefZD0.htm)|Tyrannosaurus|Tyrannosaurus|modificada|
|[lFlXmieuHTBIonhj.htm](pathfinder-bestiary/lFlXmieuHTBIonhj.htm)|Viper|Víbora|modificada|
|[LHHgGSs0ELCR4CYK.htm](pathfinder-bestiary/LHHgGSs0ELCR4CYK.htm)|Ghoul|Ghoul|modificada|
|[lo4fR4jDVzLdwwkH.htm](pathfinder-bestiary/lo4fR4jDVzLdwwkH.htm)|Skeletal Hulk|Hulk esquelético|modificada|
|[LUtSo30fQWj7mrDn.htm](pathfinder-bestiary/LUtSo30fQWj7mrDn.htm)|Storm Lord|Ser tormentoso|modificada|
|[Lvdykf6wfqzZBlZd.htm](pathfinder-bestiary/Lvdykf6wfqzZBlZd.htm)|Ancient Black Dragon|Dragón Negro Antiguo|modificada|
|[LVhVb7abhv4onzZZ.htm](pathfinder-bestiary/LVhVb7abhv4onzZZ.htm)|Arbiter|Arbiter|modificada|
|[m3x8q5rZ6zh9x82s.htm](pathfinder-bestiary/m3x8q5rZ6zh9x82s.htm)|Guthallath|Guthallath|modificada|
|[M6RknN77XTo23v45.htm](pathfinder-bestiary/M6RknN77XTo23v45.htm)|Banshee|Banshee|modificada|
|[mblGfyIXWhiaNpFw.htm](pathfinder-bestiary/mblGfyIXWhiaNpFw.htm)|Skeletal Horse|Caballo esquelético|modificada|
|[mEmWRqTRxLUZQYSh.htm](pathfinder-bestiary/mEmWRqTRxLUZQYSh.htm)|Rust Monster|Monstruo de óxido|modificada|
|[mGr4e6fH3w8ewcSX.htm](pathfinder-bestiary/mGr4e6fH3w8ewcSX.htm)|Grim Reaper|Parca, la|modificada|
|[MhEfUXkSfLtgpmfE.htm](pathfinder-bestiary/MhEfUXkSfLtgpmfE.htm)|Adult White Dragon (Spellcaster)|Dragón Blanco Adulto (Hechicero)|modificada|
|[MLmwdUIW4o7ZSUEG.htm](pathfinder-bestiary/MLmwdUIW4o7ZSUEG.htm)|Ancient Bronze Dragon (Spellcaster)|Antiguo Dragón de Bronce (Hechicero)|modificada|
|[mqz4MfBwFxlBQeHs.htm](pathfinder-bestiary/mqz4MfBwFxlBQeHs.htm)|Invisible Stalker|Acosador Invisible|modificada|
|[mXkMSlj3LldzKxB9.htm](pathfinder-bestiary/mXkMSlj3LldzKxB9.htm)|Ancient White Dragon|Dragón Blanco Antiguo|modificada|
|[myEeYWWAgnkLwtIb.htm](pathfinder-bestiary/myEeYWWAgnkLwtIb.htm)|Skulltaker|Skulltaker|modificada|
|[n1Mv0Q1MirfjBmfI.htm](pathfinder-bestiary/n1Mv0Q1MirfjBmfI.htm)|Uthul|Uthul|modificada|
|[N62zM3aTelygWIt2.htm](pathfinder-bestiary/N62zM3aTelygWIt2.htm)|Leukodaemon|Leukodaemon|modificada|
|[NbGLrlt7RYdFFBQ5.htm](pathfinder-bestiary/NbGLrlt7RYdFFBQ5.htm)|Elemental Inferno|Infierno elemental|modificada|
|[NeYU7wwCv0RUesZ1.htm](pathfinder-bestiary/NeYU7wwCv0RUesZ1.htm)|Stone Mauler|Vapuleador de piedra|modificada|
|[nFMZjWQL6pd9XdqR.htm](pathfinder-bestiary/nFMZjWQL6pd9XdqR.htm)|Caligni Creeper|Caligni rondador (p. -s)|modificada|
|[NgCKGq28Qvt6ZOSr.htm](pathfinder-bestiary/NgCKGq28Qvt6ZOSr.htm)|Adult Gold Dragon (Spellcaster)|Dragón de oro adulto (Hechicero)|modificada|
|[nkWnoEHWUsBLgNje.htm](pathfinder-bestiary/nkWnoEHWUsBLgNje.htm)|Chimera (White Dragon)|Quimera (Dragón Blanco)|modificada|
|[NpcS7iocNNsno6lE.htm](pathfinder-bestiary/NpcS7iocNNsno6lE.htm)|Warsworn|Warsworn|modificada|
|[NTXm3ee7WZJ92Sww.htm](pathfinder-bestiary/NTXm3ee7WZJ92Sww.htm)|Wemmuth|Wemmuth|modificada|
|[NVWaLagWOu5tCCZu.htm](pathfinder-bestiary/NVWaLagWOu5tCCZu.htm)|Sod Hound|Sabueso terroso|modificada|
|[NW1Ax1QGE9W4DmiN.htm](pathfinder-bestiary/NW1Ax1QGE9W4DmiN.htm)|Ceustodaemon|Ceustodaemon|modificada|
|[nWiibOE9wVSlTdkx.htm](pathfinder-bestiary/nWiibOE9wVSlTdkx.htm)|Ancient Gold Dragon|Dragón de Oro Antiguo|modificada|
|[NymSyXbXqfkGLFWF.htm](pathfinder-bestiary/NymSyXbXqfkGLFWF.htm)|Lamia Matriarch|Lamia Matriarch|modificada|
|[o3DRwRKeJrl83Wv9.htm](pathfinder-bestiary/o3DRwRKeJrl83Wv9.htm)|Gnoll Hunter|Cazador de gnolls|modificada|
|[O3J59mUJ6DHQZZ6F.htm](pathfinder-bestiary/O3J59mUJ6DHQZZ6F.htm)|Merfolk Warrior|Guerrero Merfolk|modificada|
|[O5YbsTSlX5VhciP4.htm](pathfinder-bestiary/O5YbsTSlX5VhciP4.htm)|Flame Drake|Flame Drake|modificada|
|[oBMIc2S8ekmDgPpi.htm](pathfinder-bestiary/oBMIc2S8ekmDgPpi.htm)|Awakened Tree|Animar árbol animado|modificada|
|[Oc5NXZmMkSDCRNlQ.htm](pathfinder-bestiary/Oc5NXZmMkSDCRNlQ.htm)|Griffon|Grifón|modificada|
|[oGIWTW0WqQxYNJOD.htm](pathfinder-bestiary/oGIWTW0WqQxYNJOD.htm)|Nosoi|Nosoi|modificada|
|[Oilfs8Atv2LjAsUS.htm](pathfinder-bestiary/Oilfs8Atv2LjAsUS.htm)|Wolf|Wolf|modificada|
|[oiXbo1VSfDrHpIQm.htm](pathfinder-bestiary/oiXbo1VSfDrHpIQm.htm)|Fungus Leshy|Leshy de los hongos|modificada|
|[oj0vzfjflUc3xMBo.htm](pathfinder-bestiary/oj0vzfjflUc3xMBo.htm)|Adult Bronze Dragon (Spellcaster)|Dragón de Bronce Adulto (Hechicero)|modificada|
|[oJaC1WbXQuQX2d2J.htm](pathfinder-bestiary/oJaC1WbXQuQX2d2J.htm)|Ogre Glutton|Ogro Glotón|modificada|
|[oLdrCmarnzd62kA8.htm](pathfinder-bestiary/oLdrCmarnzd62kA8.htm)|Young Red Dragon (Spellcaster)|Joven Dragón Rojo (Hechicero)|modificada|
|[oMcHaTX5unOHC2Pm.htm](pathfinder-bestiary/oMcHaTX5unOHC2Pm.htm)|Rune Giant|Gigante rúnico|modificada|
|[oMsOm06HhX1gG0Jz.htm](pathfinder-bestiary/oMsOm06HhX1gG0Jz.htm)|Gnoll Sergeant|Sargento Gnoll|modificada|
|[OPavstjKhgcp30fc.htm](pathfinder-bestiary/OPavstjKhgcp30fc.htm)|War Pony|Poni de guerra|modificada|
|[OqCiTKXZu7EWmAI0.htm](pathfinder-bestiary/OqCiTKXZu7EWmAI0.htm)|Ancient White Dragon (Spellcaster)|Dragón Blanco Antiguo (Hechicero)|modificada|
|[oSvWsqFnQLS5wlvg.htm](pathfinder-bestiary/oSvWsqFnQLS5wlvg.htm)|Frost Giant|Gélida Gigante|modificada|
|[ouOt6d8xUFk8NhJ9.htm](pathfinder-bestiary/ouOt6d8xUFk8NhJ9.htm)|Adult Copper Dragon (Spellcaster)|Dragón de Cobre Adulto (Hechicero)|modificada|
|[p2ECLKUzZ0GhFg22.htm](pathfinder-bestiary/p2ECLKUzZ0GhFg22.htm)|Ancient Green Dragon (Spellcaster)|Dragón Verde Antiguo (Hechicero)|modificada|
|[PCh2kxeSYWRit9TE.htm](pathfinder-bestiary/PCh2kxeSYWRit9TE.htm)|Pegasus|Pegasus|modificada|
|[PfcS6WzhMGzds5Wf.htm](pathfinder-bestiary/PfcS6WzhMGzds5Wf.htm)|Nilith|Nilith|modificada|
|[pFmaszqtsA2yt7dv.htm](pathfinder-bestiary/pFmaszqtsA2yt7dv.htm)|Adult Red Dragon (Spellcaster)|Dragón Rojo Adulto (Hechicero)|modificada|
|[pG3UPgbAxNCXAyQE.htm](pathfinder-bestiary/pG3UPgbAxNCXAyQE.htm)|Boggard Swampseer|Boggard Vidente de la ciénaga|modificada|
|[PgBsMyjMITjQnCs8.htm](pathfinder-bestiary/PgBsMyjMITjQnCs8.htm)|Kolyarut|Kolyarut|modificada|
|[PiAGhPEzJMC2egQk.htm](pathfinder-bestiary/PiAGhPEzJMC2egQk.htm)|Animated Statue|Estatua animada|modificada|
|[PiZkpRK23u89h82S.htm](pathfinder-bestiary/PiZkpRK23u89h82S.htm)|Hobgoblin Soldier|Hobgoblin Soldier|modificada|
|[pkNWilK2pHZ5TDsd.htm](pathfinder-bestiary/pkNWilK2pHZ5TDsd.htm)|Lesser Death|Parca menor|modificada|
|[Pl3J94dvVY159G64.htm](pathfinder-bestiary/Pl3J94dvVY159G64.htm)|Soulbound Doll (True Neutral)|Muca de alma vinculada (True Neutral)|modificada|
|[pL4sS2HZtGAryKnN.htm](pathfinder-bestiary/pL4sS2HZtGAryKnN.htm)|Naunet|Naunet|modificada|
|[pMoAlNjMJ7DArLPh.htm](pathfinder-bestiary/pMoAlNjMJ7DArLPh.htm)|Wrathspawn|Wrathspawn|modificada|
|[pTkz08ak9YlKRsOY.htm](pathfinder-bestiary/pTkz08ak9YlKRsOY.htm)|Hive Mother|Madre de colmena|modificada|
|[PXru127aJljwKhSy.htm](pathfinder-bestiary/PXru127aJljwKhSy.htm)|Sea Devil Scout|Explorador Diablo Marino|modificada|
|[pyFvLyQsyYjOz0xI.htm](pathfinder-bestiary/pyFvLyQsyYjOz0xI.htm)|Frost Drake|Gélida Drake|modificada|
|[pYNMxg5z2nRFmkMM.htm](pathfinder-bestiary/pYNMxg5z2nRFmkMM.htm)|Ancient Black Dragon (Spellcaster)|Dragón Negro Antiguo (Hechicero)|modificada|
|[pzG7Od1aHj0DkTzS.htm](pathfinder-bestiary/pzG7Od1aHj0DkTzS.htm)|Young Copper Dragon (Spellcaster)|Joven dragón de cobre (Hechicero)|modificada|
|[Q3EaaLLx5kDXb5vQ.htm](pathfinder-bestiary/Q3EaaLLx5kDXb5vQ.htm)|Shoggoth|Shoggoth|modificada|
|[q3LrTrfnCvoUXuRz.htm](pathfinder-bestiary/q3LrTrfnCvoUXuRz.htm)|Slothspawn|Slothspawn|modificada|
|[Q8FAcsuta4p6d8KS.htm](pathfinder-bestiary/Q8FAcsuta4p6d8KS.htm)|Firewyrm|Sierpe ígnea del fuego|modificada|
|[Qa7HaKfKiosEPr94.htm](pathfinder-bestiary/Qa7HaKfKiosEPr94.htm)|Sphinx|Esfinge|modificada|
|[QaldZV2p9RpMXzzn.htm](pathfinder-bestiary/QaldZV2p9RpMXzzn.htm)|Green Kobold Dragon Mage|Mago dracónico kobold verde|modificada|
|[qcZvFCAnslI9XNTR.htm](pathfinder-bestiary/qcZvFCAnslI9XNTR.htm)|Wasp Swarm|Enjambre de avispas|modificada|
|[qfuoFK2GXBJusQ33.htm](pathfinder-bestiary/qfuoFK2GXBJusQ33.htm)|Duergar Bombardier|Duergar Bombardier|modificada|
|[QGTSPki2eoLuavif.htm](pathfinder-bestiary/QGTSPki2eoLuavif.htm)|Envyspawn|Envyspawn|modificada|
|[qGvK4f8Clc6dOwLU.htm](pathfinder-bestiary/qGvK4f8Clc6dOwLU.htm)|Ancient Copper Dragon (Spellcaster)|Dragón Antiguo de Cobre (Hechicero)|modificada|
|[qHqhUWeNUZRET9xV.htm](pathfinder-bestiary/qHqhUWeNUZRET9xV.htm)|Dire Wolf|Dire Wolf|modificada|
|[qkWkshBHyTbLP07b.htm](pathfinder-bestiary/qkWkshBHyTbLP07b.htm)|Shield Archon|Escudo Archon|modificada|
|[qpRJOzx3bJ7rolHp.htm](pathfinder-bestiary/qpRJOzx3bJ7rolHp.htm)|Manticore|Manticore|modificada|
|[QQQhNnCit9XLMMoN.htm](pathfinder-bestiary/QQQhNnCit9XLMMoN.htm)|Greater Nightmare|Mayor pesadilla|modificada|
|[qr46S4VDnaUK0GcM.htm](pathfinder-bestiary/qr46S4VDnaUK0GcM.htm)|Faceless Stalker|Acechador sin rostro|modificada|
|[QRjjE4TJNfaDhhQC.htm](pathfinder-bestiary/QRjjE4TJNfaDhhQC.htm)|Giant Frilled Lizard|Lagarto Gigante|modificada|
|[QRRX82FIjBKd8pzs.htm](pathfinder-bestiary/QRRX82FIjBKd8pzs.htm)|Guard Dog|Perro guardián|modificada|
|[QT2gA8WMaT2cuXr7.htm](pathfinder-bestiary/QT2gA8WMaT2cuXr7.htm)|Pit Fiend|Pit Fiend|modificada|
|[quXuocHuT2US7cWz.htm](pathfinder-bestiary/quXuocHuT2US7cWz.htm)|Daeodon|Daeodon|modificada|
|[R9eoGwQ2tudxUKxS.htm](pathfinder-bestiary/R9eoGwQ2tudxUKxS.htm)|Black Kobold Dragon Mage|Mago dracónico kobold negro|modificada|
|[r9w1n85mp9Ip4QiS.htm](pathfinder-bestiary/r9w1n85mp9Ip4QiS.htm)|Kobold Warrior|Kóbold combatiente|modificada|
|[r9WAwtCLxoJMjd8J.htm](pathfinder-bestiary/r9WAwtCLxoJMjd8J.htm)|Poracha|Poracha|modificada|
|[RAjbVzWcfrPBbgkK.htm](pathfinder-bestiary/RAjbVzWcfrPBbgkK.htm)|Soulbound Doll (Lawful Good)|Muca de alma vinculada (Lawful Good)|modificada|
|[RDFvGocLW0OuHmlC.htm](pathfinder-bestiary/RDFvGocLW0OuHmlC.htm)|Wendigo|Wendigo|modificada|
|[RiKjpztTt7tZbOeo.htm](pathfinder-bestiary/RiKjpztTt7tZbOeo.htm)|Hydra|Hydra|modificada|
|[RizAUQHtPy0YRUyt.htm](pathfinder-bestiary/RizAUQHtPy0YRUyt.htm)|Soulbound Doll (Lawful Evil)|Muca de alma vinculada (maligno legal)|modificada|
|[rJgUpdXBCMBrsJFf.htm](pathfinder-bestiary/rJgUpdXBCMBrsJFf.htm)|Soulbound Doll (Lawful Neutral)|Muca de alma vinculada (Neutral Legal)|modificada|
|[RMSx2C27yty0MTva.htm](pathfinder-bestiary/RMSx2C27yty0MTva.htm)|Skeletal Champion|Campeón Esquelético|modificada|
|[rPaHIh0ICnTLnRO6.htm](pathfinder-bestiary/rPaHIh0ICnTLnRO6.htm)|Kobold Scout|Kóbold explorador|modificada|
|[rPHxXClTnoPYHYuZ.htm](pathfinder-bestiary/rPHxXClTnoPYHYuZ.htm)|Basilisk|Basilisk|modificada|
|[RTUp97xNDutzYpuY.htm](pathfinder-bestiary/RTUp97xNDutzYpuY.htm)|Chimera (Black Dragon)|Quimera (Dragón Negro)|modificada|
|[RTzFvmdSCf5yhguy.htm](pathfinder-bestiary/RTzFvmdSCf5yhguy.htm)|Xulgath Warrior|Xulgath combatiente|modificada|
|[RXEnAk6cbSnk3w7O.htm](pathfinder-bestiary/RXEnAk6cbSnk3w7O.htm)|Naiad|Náyade|modificada|
|[S35DifoycBwkxaGq.htm](pathfinder-bestiary/S35DifoycBwkxaGq.htm)|Demilich|Demilich|modificada|
|[S5z0mtoEhbz7BvE9.htm](pathfinder-bestiary/S5z0mtoEhbz7BvE9.htm)|Velociraptor|Velociraptor|modificada|
|[S8iX8sPaYFFYDoUq.htm](pathfinder-bestiary/S8iX8sPaYFFYDoUq.htm)|Bunyip|Bunyip|modificada|
|[saEPHCN5lDiylb5H.htm](pathfinder-bestiary/saEPHCN5lDiylb5H.htm)|Black Pudding|Black Pudding|modificada|
|[saKs2Qaor8QktboH.htm](pathfinder-bestiary/saKs2Qaor8QktboH.htm)|Catfolk Pouncer|Catfolk Pouncer|modificada|
|[ScNPruIwcIJNuSHb.htm](pathfinder-bestiary/ScNPruIwcIJNuSHb.htm)|Chimera (Red Dragon)|Quimera (Dragón Rojo)|modificada|
|[ScOT6QOlXIsevhNq.htm](pathfinder-bestiary/ScOT6QOlXIsevhNq.htm)|Deinosuchus|Deinosuchus|modificada|
|[sf42HB8VsWGlYixP.htm](pathfinder-bestiary/sf42HB8VsWGlYixP.htm)|Ball Python|Pitón bola|modificada|
|[Sft7n3LMmnTxhhYn.htm](pathfinder-bestiary/Sft7n3LMmnTxhhYn.htm)|Warg|Warg|modificada|
|[skwkJmgz7mqIcStF.htm](pathfinder-bestiary/skwkJmgz7mqIcStF.htm)|Ettin|Ettin|modificada|
|[skz8n8N9GEQY2c90.htm](pathfinder-bestiary/skz8n8N9GEQY2c90.htm)|Clay Golem|Golem de arcilla|modificada|
|[sNFBOw4z9KagWUHe.htm](pathfinder-bestiary/sNFBOw4z9KagWUHe.htm)|Flesh Golem|Golem de carne|modificada|
|[SP72xojHR0UGAWcs.htm](pathfinder-bestiary/SP72xojHR0UGAWcs.htm)|Smilodon|Smilodon|modificada|
|[sPClc6y3dT3XZupv.htm](pathfinder-bestiary/sPClc6y3dT3XZupv.htm)|Rat Swarm|Enjambre de ratas|modificada|
|[sUGKrcx7kbHkWnwG.htm](pathfinder-bestiary/sUGKrcx7kbHkWnwG.htm)|Young Red Dragon|Joven Dragón Rojo|modificada|
|[SUpy8sleRwi2Egsq.htm](pathfinder-bestiary/SUpy8sleRwi2Egsq.htm)|Drakauthix|Drakauthix|modificada|
|[SuWpn5yZdsHDHpL2.htm](pathfinder-bestiary/SuWpn5yZdsHDHpL2.htm)|Giant Monitor Lizard|Lagarto monitor gigante|modificada|
|[SWOZ0e2IQifxzlgL.htm](pathfinder-bestiary/SWOZ0e2IQifxzlgL.htm)|Ancient Silver Dragon|Ancient Silver Dragon|modificada|
|[t4bHhZBBDUyVzEmf.htm](pathfinder-bestiary/t4bHhZBBDUyVzEmf.htm)|Iron Golem|Golem de hierro|modificada|
|[t4JYGYJqT1CaqKvh.htm](pathfinder-bestiary/t4JYGYJqT1CaqKvh.htm)|Shining Child|Niño Brillante|modificada|
|[T5CUuPsMPb17d6Qy.htm](pathfinder-bestiary/T5CUuPsMPb17d6Qy.htm)|Goliath Spider|Araña Goliat|modificada|
|[t6Xh4fHdHi2GP87z.htm](pathfinder-bestiary/t6Xh4fHdHi2GP87z.htm)|Annis Hag|Annis Hag|modificada|
|[tKaOsbg8cmIUSjSE.htm](pathfinder-bestiary/tKaOsbg8cmIUSjSE.htm)|Giant Eagle|Giant Eagle|modificada|
|[tl1aGbwuPPUDKHbN.htm](pathfinder-bestiary/tl1aGbwuPPUDKHbN.htm)|Adult Silver Dragon (Spellcaster)|Dragón Plateado Adulto (Hechicero)|modificada|
|[tm3Tixb7IDoLdJ5k.htm](pathfinder-bestiary/tm3Tixb7IDoLdJ5k.htm)|Tiefling Adept|Tiefling Adepto|modificada|
|[tMxtnGthVV01wNQb.htm](pathfinder-bestiary/tMxtnGthVV01wNQb.htm)|Janni|Janni|modificada|
|[ToSwRvspZ0IB7SHQ.htm](pathfinder-bestiary/ToSwRvspZ0IB7SHQ.htm)|Ghost Mage|Mago fantasma|modificada|
|[Tpuqwt6Af29EMtqX.htm](pathfinder-bestiary/Tpuqwt6Af29EMtqX.htm)|Alchemical Golem|Golem alquímico|modificada|
|[tr75FAbdOkrfQviy.htm](pathfinder-bestiary/tr75FAbdOkrfQviy.htm)|Electric Eel|Anguila Eléctrica|modificada|
|[triuze3NMe4kWpdS.htm](pathfinder-bestiary/triuze3NMe4kWpdS.htm)|Gluttonyspawn|Gluttonyspawn|modificada|
|[tTmml7T2Knz2NrLd.htm](pathfinder-bestiary/tTmml7T2Knz2NrLd.htm)|Crag Linnorm|Crag Linnorm|modificada|
|[tuIqEvsct8EO33xs.htm](pathfinder-bestiary/tuIqEvsct8EO33xs.htm)|Alghollthu Master|Amo alghollthu|modificada|
|[tXHUr947sanB5tdN.htm](pathfinder-bestiary/tXHUr947sanB5tdN.htm)|Wererat|Wererat|modificada|
|[TZcDdN5o7s4alZNE.htm](pathfinder-bestiary/TZcDdN5o7s4alZNE.htm)|Gelatinous Cube|Cubo gelatinoso|modificada|
|[U1L3MFKHe0sNvLoU.htm](pathfinder-bestiary/U1L3MFKHe0sNvLoU.htm)|Dezullon|Dezullon|modificada|
|[uA0cv4OT5mQnym0V.htm](pathfinder-bestiary/uA0cv4OT5mQnym0V.htm)|Young Brass Dragon|Joven dragón de latón|modificada|
|[uCw15c4AnIrOy5AV.htm](pathfinder-bestiary/uCw15c4AnIrOy5AV.htm)|Merfolk Wavecaller|Orador de las olas Merfolk|modificada|
|[Upf2fOZ6QgGG3seI.htm](pathfinder-bestiary/Upf2fOZ6QgGG3seI.htm)|Stone Golem|Golem de piedra|modificada|
|[UPm2rwIevsX9Odbm.htm](pathfinder-bestiary/UPm2rwIevsX9Odbm.htm)|Grikkitog|Grikkitog|modificada|
|[uPxh3VpA80zZdWfx.htm](pathfinder-bestiary/uPxh3VpA80zZdWfx.htm)|Ancient Blue Dragon|Dragón Azul Antiguo|modificada|
|[uQBOJfjtzEXvpLQz.htm](pathfinder-bestiary/uQBOJfjtzEXvpLQz.htm)|Ochre Jelly|Jalea Ocre|modificada|
|[uQOTCiHslczvrNpt.htm](pathfinder-bestiary/uQOTCiHslczvrNpt.htm)|Young Bronze Dragon|Joven Dragón de Bronce|modificada|
|[URREWYZtc8QJ9ld6.htm](pathfinder-bestiary/URREWYZtc8QJ9ld6.htm)|Reefclaw|Reefclaw|modificada|
|[UrviURGu5o9LkhxN.htm](pathfinder-bestiary/UrviURGu5o9LkhxN.htm)|Sea Devil Brute|Sea Devil Brute|modificada|
|[UYHtIbN0JVaIYcgs.htm](pathfinder-bestiary/UYHtIbN0JVaIYcgs.htm)|Gourd Leshy|Leshy de los frutos de la calabaza|modificada|
|[v0GZ6zDWhD1Y3xs9.htm](pathfinder-bestiary/v0GZ6zDWhD1Y3xs9.htm)|Young Black Dragon|Joven Dragón Negro|modificada|
|[v92cB3RBUMhysOpD.htm](pathfinder-bestiary/v92cB3RBUMhysOpD.htm)|Winter Wolf|Lobo de Invierno|modificada|
|[v9eQFqibX6EYsmuX.htm](pathfinder-bestiary/v9eQFqibX6EYsmuX.htm)|Adult Silver Dragon|Dragón Plateado Adulto|modificada|
|[VAWmwDA08ZLQd8lW.htm](pathfinder-bestiary/VAWmwDA08ZLQd8lW.htm)|Vampire Bat Swarm|Enjambre de murciélagos vampiro|modificada|
|[VBXjZ1jYxdKf64B7.htm](pathfinder-bestiary/VBXjZ1jYxdKf64B7.htm)|Drow Rogue|Pícaro Drow|modificada|
|[VC7rtYR4hLxwg7WZ.htm](pathfinder-bestiary/VC7rtYR4hLxwg7WZ.htm)|Ghast|Ghast|modificada|
|[VcUdFYNaxauNr5Hn.htm](pathfinder-bestiary/VcUdFYNaxauNr5Hn.htm)|Hyaenodon|Hyaenodon|modificada|
|[vEFENJJixCdmBNl5.htm](pathfinder-bestiary/vEFENJJixCdmBNl5.htm)|Jungle Drake|Draco selvático|modificada|
|[VEnjCj4geJBzTgBJ.htm](pathfinder-bestiary/VEnjCj4geJBzTgBJ.htm)|Ancient Gold Dragon (Spellcaster)|Ancient Gold Dragon (Hechicero)|modificada|
|[vJwnApm0HkadGR7w.htm](pathfinder-bestiary/vJwnApm0HkadGR7w.htm)|Crocodile|Cocodrilo|modificada|
|[vmN9SCUJxN1MIXwp.htm](pathfinder-bestiary/vmN9SCUJxN1MIXwp.htm)|Elemental Tsunami|Tsunami elemental|modificada|
|[VoLW6eUxMSsXvgVP.htm](pathfinder-bestiary/VoLW6eUxMSsXvgVP.htm)|Duskwalker Ghost Hunter|Cazador de fantasmas crepuscular|modificada|
|[VUJrPHKOjYkIQnWn.htm](pathfinder-bestiary/VUJrPHKOjYkIQnWn.htm)|Mu Spore|Mu Espora|modificada|
|[vUKCuAgLQdz5akgp.htm](pathfinder-bestiary/vUKCuAgLQdz5akgp.htm)|Megalodon|Megalodon|modificada|
|[vxKqnzwcxNAgLp7C.htm](pathfinder-bestiary/vxKqnzwcxNAgLp7C.htm)|Giant Octopus|Pulpo gigante|modificada|
|[w20FfmKH7ukghczT.htm](pathfinder-bestiary/w20FfmKH7ukghczT.htm)|Greedspawn|Greedspawn|modificada|
|[WAgQt9pkzgPOlcJI.htm](pathfinder-bestiary/WAgQt9pkzgPOlcJI.htm)|Triceratops|Triceratops|modificada|
|[waPgKbjhijeZ00Zm.htm](pathfinder-bestiary/waPgKbjhijeZ00Zm.htm)|Nightmare|Pesadilla|modificada|
|[wifELOkkRO2634bc.htm](pathfinder-bestiary/wifELOkkRO2634bc.htm)|Boggard Scout|Explorador Boggard|modificada|
|[WiOY3YbiKEJKIQQz.htm](pathfinder-bestiary/WiOY3YbiKEJKIQQz.htm)|Stone Giant|Gigante de Piedra|modificada|
|[wjw8FQp4icafYash.htm](pathfinder-bestiary/wjw8FQp4icafYash.htm)|Living Whirlwind|Torbellino viviente|modificada|
|[wKVZdVVcXtvLxgsY.htm](pathfinder-bestiary/wKVZdVVcXtvLxgsY.htm)|Giant Animated Statue|Estatua Gigante Animada|modificada|
|[wMomrpcaC8QvIdlj.htm](pathfinder-bestiary/wMomrpcaC8QvIdlj.htm)|Ankhrav|Ankhrav|modificada|
|[WNiNj0Brn2LCYmwd.htm](pathfinder-bestiary/WNiNj0Brn2LCYmwd.htm)|Great White Shark|Gran tiburón blanco|modificada|
|[WNqPRMjKW0oCHZ8X.htm](pathfinder-bestiary/WNqPRMjKW0oCHZ8X.htm)|Krooth|Krooth|modificada|
|[wpmvdP5w936Kmq0e.htm](pathfinder-bestiary/wpmvdP5w936Kmq0e.htm)|Pixie|Pixie|modificada|
|[wqPYzMNgYvrO6oEP.htm](pathfinder-bestiary/wqPYzMNgYvrO6oEP.htm)|Leopard|Leopardo|modificada|
|[WQy7HBUcgDLsfVJd.htm](pathfinder-bestiary/WQy7HBUcgDLsfVJd.htm)|Night Hag|Maléfico nocturno|modificada|
|[wRQ7TZdd0n5UIIao.htm](pathfinder-bestiary/wRQ7TZdd0n5UIIao.htm)|Will-o'-Wisp|Will-o'-Wisp|modificada|
|[wuaSG22lLjQ6yali.htm](pathfinder-bestiary/wuaSG22lLjQ6yali.htm)|Wyvern|Wyvern|modificada|
|[X03lxGz9ELv8rG2w.htm](pathfinder-bestiary/X03lxGz9ELv8rG2w.htm)|Soulbound Doll (Chaotic Neutral)|Muca de alma vinculada (Caótico Neutral)|modificada|
|[X03vq2RWi2jiA6Ri.htm](pathfinder-bestiary/X03vq2RWi2jiA6Ri.htm)|Owlbear|Owlbear|modificada|
|[x23aXeWTo026pMui.htm](pathfinder-bestiary/x23aXeWTo026pMui.htm)|Earth Mephit|Mephit de tierra|modificada|
|[x26dyZZZRvZpzK2X.htm](pathfinder-bestiary/x26dyZZZRvZpzK2X.htm)|Quelaunt|Quelaunt|modificada|
|[x4mlZseBP5bWcy4H.htm](pathfinder-bestiary/x4mlZseBP5bWcy4H.htm)|Barghest|Barghest|modificada|
|[x6wfK4UCJ6wYok9t.htm](pathfinder-bestiary/x6wfK4UCJ6wYok9t.htm)|Tor Linnorm|Tor Linnorm del pe≥n|modificada|
|[x81PGKEsOtPquFVa.htm](pathfinder-bestiary/x81PGKEsOtPquFVa.htm)|Chimera (Blue Dragon)|Quimera (Dragón Azul)|modificada|
|[x8ZWNcFOfpJYjXOw.htm](pathfinder-bestiary/x8ZWNcFOfpJYjXOw.htm)|Lustspawn|Lustspawn|modificada|
|[XbClt5wkqECrQToJ.htm](pathfinder-bestiary/XbClt5wkqECrQToJ.htm)|Ratfolk Grenadier|Granadero Ratfolk|modificada|
|[XLqbEDjmGpIc4XoY.htm](pathfinder-bestiary/XLqbEDjmGpIc4XoY.htm)|Orc Warchief|Orc Warchief|modificada|
|[XoXf5ExS95Vv6lNf.htm](pathfinder-bestiary/XoXf5ExS95Vv6lNf.htm)|Elemental Avalanche|Avalancha elemental|modificada|
|[xqnl2eFzy5H2NZoQ.htm](pathfinder-bestiary/xqnl2eFzy5H2NZoQ.htm)|Shaitan|Shaitan|modificada|
|[XrmHgbKgcHDi4OnK.htm](pathfinder-bestiary/XrmHgbKgcHDi4OnK.htm)|Shadow|Sombra|modificada|
|[XUTUBrQixSs7VLov.htm](pathfinder-bestiary/XUTUBrQixSs7VLov.htm)|Lyrakien|Lyrakien|modificada|
|[XVX9Uhqb8shG5Pwm.htm](pathfinder-bestiary/XVX9Uhqb8shG5Pwm.htm)|Gorilla|Gorila|modificada|
|[xYlOudjXyTakF1m8.htm](pathfinder-bestiary/xYlOudjXyTakF1m8.htm)|Living Landslide|Desprendimiento viviente|modificada|
|[xZQIChzzIYbNIEoT.htm](pathfinder-bestiary/xZQIChzzIYbNIEoT.htm)|Young Green Dragon|Joven Dragón Verde|modificada|
|[Y3T7XfC2BeiNBmuS.htm](pathfinder-bestiary/Y3T7XfC2BeiNBmuS.htm)|Desert Drake|Draco desértico|modificada|
|[YdlmpfZso6GwPr2D.htm](pathfinder-bestiary/YdlmpfZso6GwPr2D.htm)|Chimera (Green Dragon)|Quimera (Dragón Verde)|modificada|
|[yIXRooXdsKtbcw2D.htm](pathfinder-bestiary/yIXRooXdsKtbcw2D.htm)|Mammoth|Mammoth|modificada|
|[ypLkUfuHHfNDsVUQ.htm](pathfinder-bestiary/ypLkUfuHHfNDsVUQ.htm)|Tiger|Tigre|modificada|
|[YPLZkAlOuaj3F3nZ.htm](pathfinder-bestiary/YPLZkAlOuaj3F3nZ.htm)|Young Silver Dragon|Joven Dragón Plateado|modificada|
|[yq1BgQi0BsAMbU69.htm](pathfinder-bestiary/yq1BgQi0BsAMbU69.htm)|Kapoacinth|Kapoacinth|modificada|
|[YUk9S6caKqheRsUQ.htm](pathfinder-bestiary/YUk9S6caKqheRsUQ.htm)|Riding Pony|Poni de monta|modificada|
|[yxCmLBpw6xqWFU3E.htm](pathfinder-bestiary/yxCmLBpw6xqWFU3E.htm)|Vampire Mastermind|Vampire Mastermind|modificada|
|[yzyaD2yGDrxmYh7P.htm](pathfinder-bestiary/yzyaD2yGDrxmYh7P.htm)|Lizardfolk Stargazer|Lizardfolk Stargazer|modificada|
|[z0l0lHc79NbMxiqZ.htm](pathfinder-bestiary/z0l0lHc79NbMxiqZ.htm)|Goblin Pyro|Goblin Pyro|modificada|
|[Z7xWkQKCHGyd02B1.htm](pathfinder-bestiary/Z7xWkQKCHGyd02B1.htm)|Giant Mantis|Mantis Gigante|modificada|
|[zA1I5YXI9GCSaksP.htm](pathfinder-bestiary/zA1I5YXI9GCSaksP.htm)|Naiad Queen|Reina Náyade|modificada|
|[zBPGUUP788b0g1Ng.htm](pathfinder-bestiary/zBPGUUP788b0g1Ng.htm)|Keketar|Keketar|modificada|
|[Zbvh6ChdD0TWv257.htm](pathfinder-bestiary/Zbvh6ChdD0TWv257.htm)|Remorhaz|Remorhaz|modificada|
|[zJro50sLFmOcDLdO.htm](pathfinder-bestiary/zJro50sLFmOcDLdO.htm)|Lemure|Lemure|modificada|
|[zJZqpx6pPW7dxEUV.htm](pathfinder-bestiary/zJZqpx6pPW7dxEUV.htm)|Giant Scorpion|Escorpión gigante|modificada|
|[ZMr28tFTA5NUcBTi.htm](pathfinder-bestiary/ZMr28tFTA5NUcBTi.htm)|Web Lurker|Web Lurker|modificada|
|[Zn0p5YjELMjEwkqx.htm](pathfinder-bestiary/Zn0p5YjELMjEwkqx.htm)|Great Cyclops|Gran Cíclope|modificada|
|[ZPAM4OavHmdgmGnw.htm](pathfinder-bestiary/ZPAM4OavHmdgmGnw.htm)|Centipede Swarm|Enjambre de Ciempiés|modificada|
|[ZPjQkKVMi3xoPcU0.htm](pathfinder-bestiary/ZPjQkKVMi3xoPcU0.htm)|Wight|Wight|modificada|
|[zq18QX6CBJNeUIgG.htm](pathfinder-bestiary/zq18QX6CBJNeUIgG.htm)|Gug|Gug|modificada|
|[zRNHsSxi1g3IFYFu.htm](pathfinder-bestiary/zRNHsSxi1g3IFYFu.htm)|Living Wildfire|Fuego salvaje viviente|modificada|
|[ZsduIlmluQe4ZxFy.htm](pathfinder-bestiary/ZsduIlmluQe4ZxFy.htm)|Dero Strangler|Estrangulador de Dero|modificada|
|[ZSQ4M8qeAv4dkLq1.htm](pathfinder-bestiary/ZSQ4M8qeAv4dkLq1.htm)|Skum|Skum|modificada|
|[zUvgAbgeQH5t6DWs.htm](pathfinder-bestiary/zUvgAbgeQH5t6DWs.htm)|Choral|Coral|modificada|
|[Zv6eaumsdz4HdxRV.htm](pathfinder-bestiary/Zv6eaumsdz4HdxRV.htm)|Hyena|Hyena|modificada|
|[ZXbr1ke1vF0ZFKY3.htm](pathfinder-bestiary/ZXbr1ke1vF0ZFKY3.htm)|Roper|Roper|modificada|
|[zXi6UhZ0mXWpKk4A.htm](pathfinder-bestiary/zXi6UhZ0mXWpKk4A.htm)|Adult Green Dragon|Dragón Verde Adulto|modificada|
|[ZzMJ7Y4qxapAVvlF.htm](pathfinder-bestiary/ZzMJ7Y4qxapAVvlF.htm)|Cassisian|Cassisian|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
